<?php
require_once("processSurveyFunctionsV6.php");

$gblShowErrorSurvey = true;
$gblEFormsErrorMessage = "";

//$registerID equivale al FactKey de la tabla de hechos de la encuesta
//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
//@JAPR 2012-11-27: Agregado el parámetro $srcFactKeyDimVal que equivale a $factKeyDimVal de otras funciones, para permitir que se herede
//el FactKeyDimVal al agregar nuevos registros porque durante una edición se determinó que el registro original ya no existe
//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
//Agregado el parámetro $aDynamicRecNum para indicar el número de registro de la sección dinámica del que se trata. Originalmente fué usado solo
//para secciones dinámicas, pero se pudiera aplicar en conjunto con $anEntrySectionID para usarlo también en secciones Maestro-detalle. Si se
//envía un -1 quiere decir que no es un registro de una sección múltiple (es el índice empezando en 0 tal como lo maneja el App de v4)
/*
//@JAPR 2014-10-29: Agregado el mapeo de Scores
//Agregado el parámetro $aMultiRecordNum para indicar el registro de cada sección que se esté procesando. Va de la mano con $anEntrySectionID y
$anEntryCarDimID y no aplica para secciones estándar en las cuales debería llegar el valor 0. Se decidió agregar a pesar de ya contar con el
parámetro $aDynamicRecNum que es lo mismo en secciones Inline o dinámicas, porque precisamente para dichas secciones ya se usaba $aDynamicRecNum
para algunos procesos que específicamente aplican para ellas, mientras que $aMultiRecordNum se usará para procesos de reemplazos de variables o otros
que requieran entre otras cosas acceso al objeto JSon original de la captura
*/
function updateData($aRepository, $surveyID, $arrayData, $dateID, $hourID, $userID, $thisDate, $registerID=null, $anEntrySectionID = 0, $anEntryCatDimID = 0, &$srcFactKeyDimVal=0, $aDynamicPageDSC = '', $bSaveSignature = true, $aDynamicRecNum = -1, $aMultiRecordNum = 0)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	global $useSourceFields;
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	global $useRecVersionField;
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	global $useDynamicPageField;
	//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
	global $usePhotoPaths;
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	global $syncDate;
	global $syncTime;
	//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
	global $blnWebMode;
	//@JAPR 2012-11-23: Corregida la fecha de inicio de actualización
	global $strLastUpdateDate;
	global $strLastUpdateEndDate;
	global $strLastUpdateTime;
	global $strLastUpdateEndTime;
	//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
	global $arrEditedFactKeys;
	//@JAPR 2013-01-28: Corregido un bug, no se estaba haciendo la referencia global de esta variable lo que causaba errores al grabar scores
	global $arrMasterDetSectionsIDs;
	//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	global $dynamicSectionWithMultiChoice;
	global $arrDynamicCatalogAttributes;
	//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	global $arrSectionDescByID;
	global $arrSectionDescStrByID;
	global $appVersion;
	//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
	global $intComboID;
	global $strIndicFieldsArrayCode;
	global $gbEnableIncrementalAggregations;
	global $gbTestIncrementalAggregationsData;
	global $arrDateValues;
	global $arrDimKeyValues;
	global $arrDimDescValues;
	global $arrIndicValues;
	global $arrDefDimValues;
	//@JAPR 2014-10-29: Agregado el mapeo de Scores
	global $aSectionEvalRecord;
	//@JAPR 2014-11-24: Agregadas variables para las secciones
	global $aSectionEvalPage;
	
	$aSectionEvalRecord = array();
	//Este array sólo se llena cuando se trata de una sección múlti-registro
	if ($anEntrySectionID > 0) {
		$aSectionEvalRecord[$anEntrySectionID] = $aMultiRecordNum;
	}
	//@JAPR 2014-11-24: Agregadas variables para las secciones
	$aSectionEvalPage = array();
	if ($anEntrySectionID > 0) {
		$aSectionEvalPage[$anEntrySectionID] = $aDynamicPageDSC;
	}
	//@JAPR
	
	$arrRowDimValues = array();
	$arrRowDimValuesOld = array();
	$arrRowDimDescValues = array();
	$arrRowDimDescValuesOld = array();
	$arrRowIndicValuesOld = array();
	if ($gbEnableIncrementalAggregations) {
		if ($strIndicFieldsArrayCode != '') {
			@eval($strIndicFieldsArrayCode);
			if (isset($arrRowIndicValues)) {
				foreach ($arrRowIndicValues as $anIndex => $aValue) {
					$arrRowIndicValuesOld[$anIndex] = $aValue;
				}
				$arrIndicValues[$intComboID] =& $arrRowIndicValuesOld;
				$arrIndicValues[$intComboID +1] =& $arrRowIndicValues;
			}
		}
		
		$arrDimKeyValues[$intComboID] =& $arrRowDimValuesOld;
		$arrDimKeyValues[$intComboID +1] =& $arrRowDimValues;
		$arrDimDescValues[$intComboID] =& $arrRowDimDescValuesOld;
		$arrDimDescValues[$intComboID +1] =& $arrRowDimDescValues;
		$arrDateValues[$intComboID] = substr($arrayData["surveyDate"], 0, 10);
		$arrDateValues[$intComboID +1] = substr($arrayData["surveyDate"], 0, 10);
	}
	
	$arrQuestionDimValues = array();
	$arrQuestionDimValuesDesc = array();
	$arrQuestionDimValuesOld = array();
	$arrQuestionDimValuesDescOld = array();
	global $arrLastCatClaDescrip;	
	global $arrLastCatValues;
	$arrLastCatClaDescrip = array();
	$arrLastCatValues = array();
	$arrCatalogDimValuesByCatalog = array();
	$arrCatalogDimValuesByCatalogOld = array();
	//@JAPR
	
	if ($registerID <= 0) {
		$registerID = null;
	}
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\nregisterID in updateData: $registerID");
	}
	$strOriginalWD = getcwd();

	require_once("survey.inc.php");
	require_once("question.inc.php");

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	require_once("../model_manager/indicatorkpi.inc.php");

	//Se genera instancia de encuesta dado un SurveyID
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->ModelID);
	chdir($strOriginalWD);
	
	//Obtenemos nombre de tabla y campo de llave subrogada de la dimension Encuesta ID
	$tableDimEncuestaID = "RIDIM_".$surveyInstance->FactKeyDimID;
	$fieldDimEncuestaID = $tableDimEncuestaID."KEY";
	
	//Obtenemos los valores de CaptureVia, CaptureEmail y SchedulerID
	//para verificar si dichos valores los estan dando de alta un correo electronico
	$captureVia = 0;
	if(isset($arrayData["CaptureVia"]))
	{
		$captureVia = (int)$arrayData["CaptureVia"];
	}
	
	$captureEmail = "";
	if(isset($arrayData["CaptureEmail"]))
	{
		$captureEmail = $arrayData["CaptureEmail"];
	}
	
	$schedulerID = "";
	if(isset($arrayData["SchedulerID"]))
	{
		$schedulerID = (int)$arrayData["SchedulerID"];
	}
	
	$statusKey = svstFinished;
	$tableDimStatus = "RIDIM_".$surveyInstance->StatusDimID;
	$fieldDimStatusKey = $tableDimStatus."KEY";
	$fieldDimStatusDesc = "DSC_".$surveyInstance->StatusDimID;
	
	$emailKey = 0;
	$tableDimEmail = "RIDIM_".$surveyInstance->EmailDimID;
	$fieldDimEMailKey = $tableDimEmail."KEY";
	
	$schedulerKey = 0;
	$schedulerDesc = '';
	$tableDimScheduler= "RIDIM_".$surveyInstance->SchedulerDimID;
	$fieldDimSchedulerKey = $tableDimScheduler."KEY";
	//@JAPR 2013-11-13: Agregado el grabado incremental de agregaciones
	$fieldDimSchedulerDsc = "DSC_".$surveyInstance->SchedulerDimID;;

	if($captureVia==1 && trim($captureEmail)!="" && $schedulerID>0)
	{
		$fieldDimEMailDsc = "DSC_".$surveyInstance->EmailDimID;
		$sqlEmail = "SELECT ".$fieldDimEMailKey." FROM ".$tableDimEmail." WHERE ".$fieldDimEMailDsc." = ".$aRepository->DataADOConnection->Quote($captureEmail);
		$aRS = $aRepository->DataADOConnection->Execute($sqlEmail);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimEmail." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlEmail);
			}
			else 
			{
				return("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimEmail." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlEmail);
			}
		}
		
		if(!$aRS->EOF)
		{
			$emailKey = (int)$aRS->fields[strtolower($fieldDimEMailKey)];
		}

		$fieldDimSchedulerClave = "KEY_".$surveyInstance->SchedulerDimID;
		//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
		$sqlScheduler = "SELECT ".$fieldDimSchedulerKey.', '.$fieldDimSchedulerDsc." FROM ".$tableDimScheduler." WHERE ".$fieldDimSchedulerClave." = ".$schedulerID;
		$aRS = $aRepository->DataADOConnection->Execute($sqlScheduler);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimScheduler." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlScheduler);
			}
			else 
			{
				return("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimScheduler." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlScheduler);
			}
		}
		
		if(!$aRS->EOF)
		{
			$schedulerKey = (int)$aRS->fields[strtolower($fieldDimSchedulerKey)];
			//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
			$schedulerDesc = (string)@$aRS->fields[strtolower($fieldDimSchedulerDsc)];
		}
		
		$userID = -1;
	}
	else 
	{
		//$arrayData["userID"] no es lo mismo que el $userID recibido en processSurvey.php el cual corresponde con el ApplicatorID, es decir 
		//el usuario que originalmente hizo la captura, en este caso $arrayData["userID"] es el usuario que está logeado en el Administrador, 
		//y que es quien invoca a la edición, lo cual sugeriría que un usuario diferente al que inicialmente hizo la captura pudiera editarla,
		//sin embargo en la práctica esto no parece ser posible
		//Sea como seal al final esta variable ya no se usó en el código original del UPDATE
		$userID = $arrayData["userID"];
		
		//@JAPR 2014-01-13: Corregido un bug, no se estaba cargando la información del Scheduler durante la edición
		//@JAPR 2012-08-27: Agregado el grabado automático del Scheduler para las capturas desde móviles. Esto es un parche temporal ya que los
		//móviles realmente no están ligados a Schedulers, así que por ahora se obtendrá el MAX del SchedulerID y eso es lo que se grabará. Se
		//atrapará cualquier error para no detener el proceso de grabado
		
		//Primero obtiene el SchedulerID asociado con esta encuesta, se le dará prioridad a los Schedulers que NO son de captura vía EMail ya
		//que esos actualmente ya graban el Scheduler asociado, así que si se usara el mismo aquí no habría forma de diferenciar entre una
		//captura vía EMail o móvil. Se tomará el último Scheduler creado para esta encuesta
		$sqlScheduler = "SELECT A.CaptureVia, MAX(A.SchedulerID) AS MaxSchedulerID 
			FROM SI_SV_SurveyScheduler A 
			WHERE A.SurveyID = $surveyID 
			GROUP BY A.SurveyID, A.CaptureVia
			ORDER BY A.CaptureVia";
		$aRS = $aRepository->DataADOConnection->Execute($sqlScheduler);
		if ($aRS) {
			if(!$aRS->EOF)
			{
				//Como viene ordenado por CaptureVia, primero se usa el Scheduler normal (si lo hay) y al final el de EMail
				$schedulerID = (int) @$aRS->fields["maxschedulerid"];
			}
		}
		
		if ((int) $schedulerID > 0) {
			$fieldDimSchedulerClave = "KEY_".$surveyInstance->SchedulerDimID;
			//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
			$sqlScheduler = "SELECT ".$fieldDimSchedulerKey.', '.$fieldDimSchedulerDsc." FROM ".$tableDimScheduler." WHERE ".$fieldDimSchedulerClave." = ".$schedulerID;
			$aRS = $aRepository->DataADOConnection->Execute($sqlScheduler);
			if ($aRS)
			{
				if(!$aRS->EOF)
				{
					$schedulerKey = (int) @$aRS->fields[strtolower($fieldDimSchedulerKey)];
					//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
					$schedulerDesc = (string)@$aRS->fields[strtolower($fieldDimSchedulerDsc)];
				}
			}
		}
		//@JAPR
	}
	
	if(isset($arrayData['surveyFinished'])) 
	{
		if($arrayData['surveyFinished']=='' || $arrayData['surveyFinished']<=1)
		{
			$statusKey = svstFinished;
		}
		else 
		{
			$statusKey = $arrayData['surveyFinished'];
		}
	}
	else 
	{
		$statusKey = svstFinished;
	}
	
	//Determinamos si la encuesta tiene seccion dinamica
	//la primera prueba es verificar si en el arreglo $arrayData contiene la llave "sectionValue"
	$hasDynamicSection = false;
	$dynamicSectionID = 0;
	$dynamicSectionCatID = 0;
	//@JAPR 2013-01-28: Corregido un bug, cuando no se grababa la sección dinámica pero esta de todas formas contenía una pregunta multiple-choice
	//tipo checkbox, el campo físicamente es un double, así que al no recibir un valor estaba intentando insertar un '' en lugar de un 0 y eso
	//generaba un error, así que se validará por la pregunta de sección dinámica incluso si no se recibió el sectionValue
	$dynamicSectionCatMemberID = 0;
	$dynamicSectionChildCatMemberID = 0;
	$dynamicSectionIDEmpty = 0;
	if(isset($arrayData['sectionValue']))
	{
		//Si entro a este if quiere decir que al menos si trae valor para las secciones dinamicas
		//ahora se corrobora realmente q si exista
		$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $surveyID);
		
		if($dynamicSectionID>0)
		{
			$hasDynamicSection = true;
			$dynamicSection = BITAMSection::NewInstanceWithID($aRepository, $dynamicSectionID);
			$dynamicSectionCatID = $dynamicSection->CatalogID;
			$dynamicSectionCatMemberID = $dynamicSection->CatMemberID;
			$dynamicSectionChildCatMemberID = $dynamicSection->ChildCatMemberID;
		}
	}
	else {
		$dynamicSectionIDEmpty = BITAMSection::existDynamicSection($aRepository, $surveyInstance->SurveyID);
		if ($dynamicSectionIDEmpty > 0) {
			$dynamicSection = BITAMSection::NewInstanceWithID($aRepository, $dynamicSectionIDEmpty);
			if (!is_null($dynamicSection)) {
				$dynamicSectionCatID = $dynamicSection->CatalogID;
				$dynamicSectionCatMemberID = $dynamicSection->CatMemberID;
				$dynamicSectionChildCatMemberID = $dynamicSection->ChildCatMemberID;
			}
		}
	}
	
	//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Genera un array indexado por el MemberID y cuyo valor es la posición del atributo, esto para el catálogo de la sección dinámica
	$arrDynamicCatalogAttributesColl = array();
	if ($dynamicSectionCatID > 0) {
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $dynamicSectionCatID);
		if (!is_null($objCatalogMembersColl)) {
			foreach ($objCatalogMembersColl as $objCatalogMember) {
				$arrDynamicCatalogAttributesColl[$objCatalogMember->MemberID] = $objCatalogMember->MemberOrder;
			}
		}
	}
	//@JAPR
	
	//Verificamos si hay un entryID, si lo hay obtenemos el FactKey por medio del EntryID
	$entryID = 0;
	if(isset($arrayData['entryID'])) 
	{
		$entryID = (int)$arrayData['entryID'];
	}
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\nentryID in updateData: $entryID");
	}
	
	//Obtenemos instancia del indicador AnsweredQuestions
	$answeredQuestionsIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->AnsweredQuestionsIndID);
	chdir($strOriginalWD);
	
	//Obtenemos instancia del indicador Duration
	$durationIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->DurationIndID);
	chdir($strOriginalWD);
	
	//Obtenemos instancia del indicador Latitude y Longitude
	$latitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->LatitudeIndID);
	$longitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->LongitudeIndID);
	chdir($strOriginalWD);
	
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	$accuracyIndicator = null;
	if (getMDVersion() >= esvAccuracyValues && $surveyInstance->AccuracyIndID) {
		//Obtenemos instancia del indicador Accuracy si es que existe
		$accuracyIndicator = @BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->AccuracyIndID);
		chdir($strOriginalWD);
	}
	//@JAPR
	
	//Obtenemos instancia de la dimension User
	$userDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->UserDimID);
	chdir($strOriginalWD);
	
	$tableName = $surveyInstance->SurveyTable;
	$factTable = $anInstanceModel->nom_tabla;
	
	//Modificar dateID para que incluya la hora, minuto y segundo
	$dateID = substr($dateID, 0, 10)." ".substr($hourID, 0, 8);
	
	//Para este primer if vamos a checar si estamos procesando un registro proveniente
	//de una encuesta con secciones dinamicas o Maestro-Detalle
	if($entryID>0 && !is_null($registerID))
	{
		$sql = "SELECT FactKey, FactKeyDimVal FROM ".$tableName." WHERE FactKeyDimVal = ".$entryID." AND FactKey = ".$registerID;
	}
	else if($entryID>0)
	{
		$sql = "SELECT FactKey, FactKeyDimVal FROM ".$tableName." WHERE FactKeyDimVal = ".$entryID;
	}
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			return("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	if(!$aRS->EOF)
	{
		//El registro existe, por lo que se procede a actualizar sólo las respuestas a las preguntas de dicho registro (si fuera de una 
		//sección dinámica o maestro-detalle, actualizaría sólo en dicho FactKey, de lo contrario lo haría en todos los registros
		$factKey = (int)$aRS->fields["factkey"];
		$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
		//@JAPR 2012-11-28: Agregada la eliminación de registros durante la edición de capturas (faltaba para la edición)
		if (!is_null($arrEditedFactKeys) && is_array($arrEditedFactKeys)) {
			$arrEditedFactKeys[$factKey] = $factKey;
		}
		//@JAPR
		
		//Se genera instancia de coleccion de preguntas dado un SurveyID pero se ignoraran los datos de tipo 3 Seleccion Multiple
		$anArrayTypes = array();
		$anArrayTypes[0] = qtpMulti;
		$dspMatrix = qtpMulti;
        $questionCollection = BITAMQuestionCollection::NewInstanceWithExceptTypeQDisplayMode($aRepository, $surveyID, $anArrayTypes, $dspMatrix);
		$countCollection = count($questionCollection->Collection);
		
		//Se van a obtener todas las preguntas de catalogo de la encuesta en las cuales dicho catalogo
		//sea el mismo que el catalogo de la encuesta ya que estas preguntas pueden venir repetidas
		//en este caso vamos igualarlas entre si para obtener el mismo valor ya que hacen referencia
		//al mismo campo de dimension de la tabla de hechos y tambien cada campo de la tabla paralela
		//de estas preguntas deben de tener el mismo valor
		$arrayQuestionSameCatalog = array();
		//@JAPR 2013-01-23: Corregido un bug, con la introducción de múltiples catálogos, cuando había mas de un par de preguntas del mismo catálogo
		//pero de dos o mas catálogos, sólo aquellas del catálogo de la encuesta estaban igualando su valor, siendo que lo correcto es que todos
		//los pares lo hubieran igualado (ver IF debajo del siguiente para referencia)
		$strQuestionCatalogValue = array();
		$strValue = "";
		//@JAPR 2013-07-18: Corregido un bug, a partir de la versión con catálogos independientes, ya no es necesario que se igualen las respuestas
		//de las preguntas de catálogo, porque cada pregunta ya se graba en forma independiente y hacer esta igualdad provocaba que el método de
		//grabado de los campos cat_#### así como el cálculo de las respuestas a partir de variables usado durante el grabado de acciones de eBavel,
		//utilizaran el valor de la última de las preguntas de catálogo (que es el que se iguala a las demás) en lugar del de la pregunta específica,
		//obviamente se asume que para esta versión, el array de respuestas ya viene correctamente asignado para cada pregunta, por lo que no hay
		//necesidad de hacer ningún ajuste adicional como este
		if (!$surveyInstance->DissociateCatDimens) {
			foreach ($questionCollection->Collection as $questionKey => $aQuestion)
			{
				//Ya no necesariamente tienen que ser del catálogo de la encuesta, cualquier pregunta de catálogo simple choice se debe validar
				if($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1 && $aQuestion->CatalogID > 0)
				{
					$intCatalogID = $aQuestion->CatalogID;
					if (!isset($arrayQuestionSameCatalog[$intCatalogID])) {
						$arrayQuestionSameCatalog[$intCatalogID] = array();
					}
					if (!isset($strQuestionCatalogValue[$intCatalogID])) {
						$strQuestionCatalogValue[$intCatalogID] = "";
					}
					$arrayQuestionSameCatalog[$intCatalogID][$aQuestion->QuestionID] = $aQuestion->QuestionID;
					
					$postKey = "qfield".$aQuestion->QuestionID;
					if(isset($arrayData[$postKey]))
					{
						$strValue = $arrayData[$postKey];
					}
					else 
					{
						$strValue = "";
					}
					
					if(strlen($strQuestionCatalogValue[$intCatalogID])<strlen($strValue))
					{
						$strQuestionCatalogValue[$intCatalogID] = $strValue;
					}
					
					foreach($arrayQuestionSameCatalog[$intCatalogID] as $valueID) 
					{
						$index = "qfield".$valueID;
						$arrayData[$index] = $strQuestionCatalogValue[$intCatalogID];
					}
				}
			}
		}
		/*
		$strQuestionCatalogValue = "";
		$strValue = "";
		
		if($surveyInstance->CatalogID>0)
		{
			foreach ($questionCollection->Collection as $questionKey => $aQuestion)
			{
				if($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1 && $aQuestion->CatalogID==$surveyInstance->CatalogID)
				{
					$arrayQuestionSameCatalog[$aQuestion->QuestionID] = $aQuestion->QuestionID;
					
					$postKey = "qfield".$aQuestion->QuestionID;
					
					if(isset($arrayData[$postKey]))
					{
						$strValue = $arrayData[$postKey];
					}
					else 
					{
						$strValue = "";
					}
					
					if(strlen($strQuestionCatalogValue)<strlen($strValue))
					{
						$strQuestionCatalogValue = $strValue;
					}
					
					foreach($arrayQuestionSameCatalog as $valueID) 
					{
						$index = "qfield".$valueID;
						$arrayData[$index] = $strQuestionCatalogValue;
					}
				}
			}
		}
		*/

		//Array de instancias de objetos dimension o indicator de los questions
		$arrayInstancesIndDim = array();
		$arrayInstancesIndicator = array();
		$arrayInstancesIndicatorSC = array();
		
		$arrayKeyValuesByQuestion = array();
		$arrayIndValuesByQuestion = array();
		$arrayIndValuesSCByQuestion = array();
		//@JAPR 2012-05-17: Agregadas las preguntas tipo Action
		$arrayValuesByActionQuestion = array();		//Contiene el Array completo de valores para todos los campos de las preguntas tipo Action, 
				//que es lo mismo que hacer un implode mediante el separador tal como está en $arrayData para esta pregunta, pero es
				//diferente al valor de $arrayKeyValuesByQuestion donde solo viene la llave subrrogada del valor de la Acción sin considerar
				//al resto de los elementos concatenados con separador (Responsible, Element y DaysDueDate)
		//@JAPR
		
		foreach ($questionCollection->Collection as $questionKey => $aQuestion)
		{
			$arrayValues = array();
			
			//@JAPR 2012-05-17: Agregados nuevos tipos de pregunta Foto y Firma
			//Las preguntas de Foto y Firma no graban una respuesta, sólo la imagen, por lo tanto se salta esta pregunta completamente
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($aQuestion->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSketch:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
					$blnValidQuestion = false;
					break;
			}
			
			if (!$blnValidQuestion)
			{
				continue;
			}
			//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
			elseif ($aQuestion->QTypeID == qtpGPS) {
				//Si la pregunta es tipo GPS, no debería sobreescribir su valor, ya que se debe mantener la ubicación de la captura original.
				//Se podría sin embargo grabar la posición de nuevos registros durante la edición, sin embargo esos no entrarían por esta función.
				//También sería posible actualizar la posición de aquellas preguntas (secciones) que originalmente habían sido saltadas, sin embargo
				//se tiene que autorizar primero dicho cambio, por lo pronto se leerá el valor anteriormente respondido
				if ($gbEnableIncrementalAggregations) {
					//Como el valor no va a cambiar, se llenan los arrays con el mismo valor leído
					$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
					chdir($strOriginalWD);
					$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
					$arrQuestionDimValues[] = $strDimDescValue;
					$arrQuestionDimValuesDesc[] = $strDimDescValue;
					$arrQuestionDimValuesOld[] = $strDimDescValue;
					$arrQuestionDimValuesDescOld[] = $strDimDescValue;
					
					//Primero desgloza el valor del GPS en sus componentes si es que contiene algo que asemeje a un dato de GPS
					if (stripos($strDimDescValue, ',') !== false || stripos($strDimDescValue, '(') !== false) {
						$arrGPSData = SplitGPSData($strDimDescValue);
						$strLatitude = $arrGPSData[0];
						$strLongitude = $arrGPSData[1];
						$strAccuracy = $arrGPSData[2];
						
						//Verifica cual de los atributos está asignado (deberían estar todos, si no está alguno, entonces hace falta editar la pregunta)
						if ($aQuestion->LatitudeAttribID > 0) {
							$arrQuestionDimValues[] = $strLatitude;
							$arrQuestionDimValuesDesc[] = $strLatitude;
							$arrQuestionDimValuesOld[] = $strLatitude;
							$arrQuestionDimValuesDescOld[] = $strLatitude;
						}
						if ($aQuestion->LongitudeAttribID > 0) {
							$arrQuestionDimValues[] = $strLongitude;
							$arrQuestionDimValuesDesc[] = $strLongitude;
							$arrQuestionDimValuesOld[] = $strLongitude;
							$arrQuestionDimValuesDescOld[] = $strLongitude;
						}
						if ($aQuestion->AccuracyAttribID > 0) {
							$arrQuestionDimValues[] = $strAccuracy;
							$arrQuestionDimValuesDesc[] = $strAccuracy;
							$arrQuestionDimValuesOld[] = $strAccuracy;
							$arrQuestionDimValuesDescOld[] = $strAccuracy;
						}
					}
				}
			}
			//@JAPR 2012-05-17: Agregadas las preguntas tipo Action
			//La pregunta tipo Action viene como un texto con separadores, por lo tanto obtenemos sólo la respuesta que es el Action para grabar
			//en la tabla paralela/hechos
			else if ($aQuestion->QTypeID == qtpAction)
			{
				//Obtener valor capturado de dicha question pero sólo del Action (índice 3)
				$postKey = "qfield".$aQuestion->QuestionID;
				$arrayActionValues = array();
				if(isset($arrayData[$postKey]))
				{
					if(trim($arrayData[$postKey])=="")
					{
						$arrayValues[] = $gblEFormsNA;
					}
					else 
					{
						$arrayActionValues = explode("_SVSep_", $arrayData[$postKey]);
						if ((string) @$arrayActionValues[3] != '')
						{
							$arrayValues[] = (string) @$arrayActionValues[3];
						}
						else 
						{
							$arrayActionValues = array();
							$arrayValues[] = $gblEFormsNA;
						}
					}
				}
				else 
				{
					$arrayValues[] = $gblEFormsNA;
				}
				
				//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					$arrQuestionDimValues[] = $arrayValues[0];
					$arrQuestionDimValuesDesc[] = $arrayValues[0];
				}
				//@JAPR
				
				//Almacena el resto de los componentes para generar acciones
				$arrayValuesByActionQuestion[$questionKey] = $arrayActionValues;
				
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
				$arrayInstancesIndDim[$aQuestion->QuestionID] = $anInstanceDimension;
				chdir($strOriginalWD);
				//@JAPR 2013-11-19: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
					$arrQuestionDimValuesOld[] = $strDimDescValue;
					$arrQuestionDimValuesDescOld[] = $strDimDescValue;
				}
				//@JAPR
				
				$arrayPassValues = getKeyValues($aRepository, $arrayInstancesIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
				
				if($arrayPassValues!==false)
				{
					$arrayKeyValuesByQuestion[$questionKey] = $arrayPassValues;
				}
				else 
				{
					return($gblEFormsErrorMessage);
				}
			}
			//@JAPR
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			else if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//Obtener valor capturado de dicha question
				$postKey = "qfield".$aQuestion->QuestionID;
				//@JAPR 2013-01-22: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				if(isset($arrayData[$postKey]) && trim($arrayData[$postKey]) !== '')
				{
					$arrayValues[0] = (float)$arrayData[$postKey];
				}
				else 
				{
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					$arrayValues[0] = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
				}
				
				//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					//@JAPR 2013-11-13: Validado que las dimensiones que son para preguntas numéricas graben NA en lugar de NULL
					if (is_null($arrayValues[0])) {
						$arrQuestionDimValues[] = ($surveyInstance->UseNULLForEmptyNumbers)?$gblEFormsNA:0;
						$arrQuestionDimValuesDesc[] = ($surveyInstance->UseNULLForEmptyNumbers)?$gblEFormsNA:0;
					}
					else {
						$arrQuestionDimValues[] = $arrayValues[0];
						$arrQuestionDimValuesDesc[] = $arrayValues[0];
					}
				}
				//@JAPR
				
				if($aQuestion->IsIndicator==1)
				{
					$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
					$arrayInstancesIndicator[$aQuestion->QuestionID] = $anInstanceIndicator;
					chdir($strOriginalWD);
					$arrayIndValuesByQuestion[$questionKey] = $arrayValues;
					
					//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
					if ($gbEnableIncrementalAggregations && !is_null($anInstanceIndicator)) {
						//@JAPR 2013-11-14: Agregado el grabado incremental de agregaciones
						//En agregaciones incrementales editadas, primero se tiene que cancelar el valor anterior de los indicadores usando la
						//combinación que anteriormente tenía la captura, para posteriormente insertar directamente el nuevo valor de los indicadores
						//pero ya en la nueva combinación, ya que cualquier pregunta pudo haber cambiado y por ender no se "reutiliza" realmente
						//ninguna combinación, sino que simplemente lo que antes había debe ser cancelado, ya que al haber una dimensión de ID
						//único asegura que cada captura tendrá su propia combinación que ninguna otra pueda reutilizar
						$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $anInstanceIndicator, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
						$arrRowIndicValuesOld[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
						$dblNewValue = (float) $arrayValues[0];
						//$dblDiff = $dblNewValue - $dblCurrValue;
						$arrRowIndicValues[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = $dblNewValue;
					}
					//@JAPR
				}
				
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
				$arrayInstancesIndDim[$aQuestion->QuestionID] = $anInstanceDimension;
				chdir($strOriginalWD);
				//@JAPR 2013-11-19: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
					$arrQuestionDimValuesOld[] = $strDimDescValue;
					$arrQuestionDimValuesDescOld[] = $strDimDescValue;
				}
				//@JAPR
				
				$arrayPassValues = getKeyValues($aRepository, $arrayInstancesIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
				
				if($arrayPassValues!==false)
				{
					$arrayKeyValuesByQuestion[$questionKey] = $arrayPassValues;
				}
				else 
				{
					return($gblEFormsErrorMessage);
				}
			}
			else 
			{
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
				$arrayInstancesIndDim[$aQuestion->QuestionID] = $anInstanceDimension;
				chdir($strOriginalWD);
				
				//@JAPR 2012-09-25: Agregado el tipo de pregunta Hora
				//@AAL 06/05/2015: Modificado para soportar preguntas tipo código de barras
				if($aQuestion->QTypeID==qtpOpenDate || $aQuestion->QTypeID==qtpOpenString || $aQuestion->QTypeID==qtpOpenAlpha || $aQuestion->QTypeID==qtpOpenTime || $aQuestion->QTypeID==qtpBarCode)
				{
					//Obtener valor capturado de dicha question Date o TextBox
					$postKey = "qfield".$aQuestion->QuestionID;
					
					if(isset($arrayData[$postKey]))
					{
						if(trim($arrayData[$postKey])=="")
						{
							$arrayValues[] = $gblEFormsNA;
						}
						else 
						{
							$arrayValues[] = $arrayData[$postKey];
						}
					}
					else
					{
						$arrayValues[] = $gblEFormsNA;
					}
				}
				else if($aQuestion->QTypeID==qtpSingle || $aQuestion->QTypeID==qtpCallList) //Conchita agregado tipo llamada
				{
					//Obtener valor capturado de dicha question Seleccion Simple
					$postKey = "qfield".$aQuestion->QuestionID;
					
					if(isset($arrayData[$postKey]))
					{
						if(trim($arrayData[$postKey])=="")
						{
							//Si es simple choice que no utiliza catalogo, entonces
							//que si se le indique valor de no aplica, en caso 
							//contrario se tiene q hacer vacio para que en la fn
							//de busqueda de valores-catalogo regrese llave de *NA
							if($aQuestion->UseCatalog==0)
							{
								$arrayValues[] = $gblEFormsNA;
							}
							else 
							{
								$arrayValues[] = '';
							}
						}
						else 
						{
							//$arrayValues[] = $arrayData[$postKey];
							$strCapturedAnswer = $arrayData[$postKey];
							
							//@JAPR 2012-04-04: Agregada la opción de "Otro" al grabar preguntas de selección sencilla sin catálogo
							$blnNewAnswer = false;
							if ($aQuestion->UseCatalog == 0)
							{
								$blnNewAnswer = true;
								foreach ($aQuestion->SelectOptions as $anAnswer)
								{
									if (strcasecmp($anAnswer, $strCapturedAnswer) == 0)
									{
										//Se encontró la respuesta entre la lista de 
										$blnNewAnswer = false;
										break;
									}
								}
							}
							
							if ($blnNewAnswer)
							{
								if ($aQuestion->AllowAdditionalAnswers)
								{
									//@JAPR 2013-04-19: Modificado el comportamiento para que ya no agregue las opciones de Otro a la lista de
									//opciones de respuesta de la pregunta
									/*
									//Hay que agregar la respuesta a la lista de posibles valores de esta pregunta
									$anInstanceQOption = BITAMQuestionOption::NewInstance($aRepository, $aQuestion->QuestionID);
									$anInstanceQOption->QuestionOptionName = $strCapturedAnswer;
									//Al hacer este grabado, automáticamente se agrega al catálogo de la dimensión por lo que mas adelante
									//cuando se pida obtener la llave Subrrogada (que es lo que realmente se graba como respuesta en la Fact) ya
									//se podrá localizar esta respuesta
									$anInstanceQOption->save();
									*/
									
									//Actualiza la instancia actual de la pregunta para agregar la opción recien creada
									$aQuestion->StrSelectOptions .= ((trim($aQuestion->StrSelectOptions) == "")?"":"\r\n").$strCapturedAnswer;
									$aQuestion->SelectOptions[] = $strCapturedAnswer;
								}
								else 
								{
									//En este caso se debe limpiar la respuesta porque la pregunta no acepta agregar nuevas respuestas
									if($aQuestion->UseCatalog==0)
									{
										$strCapturedAnswer = $gblEFormsNA;
									}
									else 
									{
										$strCapturedAnswer = '';
									}
								}
							}
							
							$arrayValues[] = $strCapturedAnswer;
							//@JAPR
						}
					}
					else 
					{
						//Si es simple choice que no utiliza catalogo, entonces
						//que si se le indique valor de no aplica, en caso 
						//contrario se tiene q hacer vacio para que en la fn
						//de busqueda de valores-catalogo regrese llave de *NA
						if($aQuestion->UseCatalog==0)
						{
							$arrayValues[] = $gblEFormsNA;
						}
						else 
						{
							$arrayValues[] = '';
						}
					}
					
					//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
					//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
					//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
					if(($aQuestion->QTypeID==qtpSingle) && $aQuestion->UseCatalog==0 && $aQuestion->IndicatorID>0)
					{
						$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
						$arrayInstancesIndicatorSC[$aQuestion->QuestionID] = $anInstanceIndicator;
						chdir($strOriginalWD);
						
						$bnlSaveScore = true;
						if ($surveyInstance->UseStdSectionSingleRec) {
							$bnlSaveScore = false;
							//En este caso las preguntas simple-choice con Score deben grabar la dimensión en todos los registros, pero sólo deben grabar
							//el indicador de Score en el registro que corresponde con la sección/página en cuestión según el contexto del registro
							if (isset($arrMasterDetSectionsIDs[$aQuestion->SectionID])) {
								//- Sección Maestro-detalle siempre
								$bnlSaveScore = true;
							}
							elseif ($hasDynamicSection && $dynamicSectionID == $aQuestion->SectionID) {
								//- Sección Dinámica sólo si es el registro de la página
								if ($anEntrySectionID == $aQuestion->SectionID && $aDynamicPageDSC != '') {
									$bnlSaveScore = true;
								}
							}
							elseif ($anEntryCatDimID > 0) {
								//- Cateogoría de dimensión nunca
							}
							elseif ($anEntrySectionID == 0 && $anEntryCatDimID == 0) {
								//- Sección Estándar sólo si es el registro único
								$bnlSaveScore = true;
							}
						}
						
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						if ($bnlSaveScore) {
							//Es un registro que si aplica para esta pregunta, por lo tanto graba el score
							$arrayPassValues = getScoreValues($aRepository, $aQuestion, $arrayValues, $surveyInstance->UseNULLForEmptyNumbers);
						}
						else {
							//No se debe grabar el score, por tanto se manda vacio
							if ($surveyInstance->UseNULLForEmptyNumbers) {
								$arrayPassValues = array(null);
							}
							else {
								$arrayPassValues = array(0);
							}
						}
						//@JAPR
						
						if($arrayPassValues!==false)
						{
							$arrayIndValuesSCByQuestion[$questionKey] = $arrayPassValues;
							//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
							if ($gbEnableIncrementalAggregations && !is_null($anInstanceIndicator)) {
								//@JAPR 2013-11-14: Agregado el grabado incremental de agregaciones
								//En agregaciones incrementales editadas, primero se tiene que cancelar el valor anterior de los indicadores usando la
								//combinación que anteriormente tenía la captura, para posteriormente insertar directamente el nuevo valor de los indicadores
								//pero ya en la nueva combinación, ya que cualquier pregunta pudo haber cambiado y por ender no se "reutiliza" realmente
								//ninguna combinación, sino que simplemente lo que antes había debe ser cancelado, ya que al haber una dimensión de ID
								//único asegura que cada captura tendrá su propia combinación que ninguna otra pueda reutilizar
								$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $anInstanceIndicator, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
								$arrRowIndicValuesOld[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
								$dblNewValue = (float) $arrayPassValues[0];
								//$dblDiff = $dblNewValue - $dblCurrValue;
								if ($bnlSaveScore) {
									$arrRowIndicValues[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = $dblNewValue;
								}
								else {
									$arrRowIndicValues[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = $arrayPassValues[0];
								}
							}
							//@JAPR
						}
						else 
						{
							return($gblEFormsErrorMessage);
						}
					}
				}
				
				//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					if ($surveyInstance->DissociateCatDimens && $aQuestion->QTypeID == qtpSingle && $aQuestion->UseCatalog == 1) {
						//En este caso esta pregunta no se grabará aquí
					}
					else {
						$arrQuestionDimValues[] = $arrayValues[0];
						$arrQuestionDimValuesDesc[] = $arrayValues[0];
						//@JAPR 2013-11-19: Agregado el grabado incremental de agregaciones
						if ($gbEnableIncrementalAggregations) {
							$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
							$arrQuestionDimValuesOld[] = $strDimDescValue;
							$arrQuestionDimValuesDescOld[] = $strDimDescValue;
						}
						//@JAPR
					}
				}
				//@JAPR
				
				if(!$hasDynamicSection)
				{
					//Si no hay secciones dinámicas, las respuestas vienen directamente como los textos o valores introducidos, por lo que se
					//debe consultar (y agregar si es necesario) dicha respuesta del catálogo de la dimensión asociada para obtener la llave 
					//subrrogada correspondiente pues eso es lo que se graba en la FactKey
					//@JAPR 2013-02-14: Agregada la edición de datos históricos
					//Si hubiera sido una combinación que en la captura original existía pero actualmente en el catálogo ya no, se regresaría
					//en este punto el valor de NA, sin embargo como para haber llegado a esta situación tuvo que haber sido a través de la
					//edición con soporte de catálogos desasociados, mas adelante se validará esa situacion para en el cubo grabar correctamente
					//la descripción de cada dimensión independiente, aunque en la tabla paralela definitivamente se va a perder el key subrrogado
					//puesto que ya ninguno aplica para esta combinación, lo que no se sabe es si deberíamos dejar un 1 o -1 para indicar
					//NA o que fué una edición de una combinación que ya no existe
					//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
					//En el caso de preguntas simple choice de catálogo cuando no hay sección dinámica, el propio método getKeyValues buscará el key
					//correspondiente a partir de la combinación recibida desde el App, así que este método no sufre cambios
					$arrayPassValues = getKeyValues($aRepository, $arrayInstancesIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
					
					if($arrayPassValues!==false)
					{
						//@JAPR 2013-02-14: Agregada la edición de datos históricos
						//Si se hubiera regresado el valor de NA pero además la descripción fuera la clave especial NOCHANGE, quiere decir
						//que es un valor que posiblemente ya no se encontraba en el catálogo así que no hay necesidad de cambiar nada pues
						//las dimensiones tal como estaban ya apuntan al valor correcto. Esto solo aplica con catálogos desasociados
						if ((int) @$arrayPassValues[0] == 1 && $surveyInstance->DissociateCatDimens && trim($arrayValues[0])=="NOCHANGE") {
							$arrayPassValues = array(-1);
						}
						//@JAPR
						$arrayKeyValuesByQuestion[$questionKey] = $arrayPassValues;
					}
					else 
					{
						return($gblEFormsErrorMessage);
					}
				}
				else 
				{
					//Verificamos en esta parte si se trata de alguna pregunta de seleccion simple con catalogo igual 
					//al catalogo del survey y que se haya generado seccion dinamica
					if($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1 && $aQuestion->CatalogID==$dynamicSectionCatID)
					{
						//@JAPR 2013-02-12: Agregada la edición de datos históricos
						//Si hubiera sido una combinación que en la captura original existía pero actualmente en el catálogo ya no, se regresaría
						//un sectionValue negativo que representa la posición de grabado de este registro en la captura editada, así que dicho
						//valor que en realidad es un key negativo no representará a un elemento del caálogo en la tabla paralela, 
						//sin embargo como para haber llegado a esta situación tuvo que haber sido a través de la
						//edición con soporte de catálogos desasociados, mas adelante se validará esa situacion para en el cubo grabar correctamente
						//la descripción de cada dimensión independiente
						//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
						//En el caso de preguntas simple choice del catálogo que se usa en la sección dinámica, para este punto ya se verificó que 
						//cada registro traiga una combinación que si exista en el catálogo o bien el valor de NA, ya que en el pre-procesamiento se 
						//basó en el valor recibido para ir a buscar al catálogo usando el método getKeyValues y se sobreescribieron los SectionValues
						//a keys que si existen en este momento, por lo que el resto del proceso no sufre cambios
						$arrayTmp = array();
						$arrayTmp[0] = $arrayData['sectionValue'];
						
						//Aqui asignamos el valor de sectionValue directamente ya que es el valor de la llave subrogada
						//por ejemplo Producto
						$arrayKeyValuesByQuestion[$questionKey] = $arrayTmp;
					}
					else 
					{
						//@JAPR 2013-02-12: Agregada la edición de datos históricos
						//Si hubiera sido una combinación que en la captura original existía pero actualmente en el catálogo ya no, se regresaría
						//en este punto el valor de NA, sin embargo como para haber llegado a esta situación tuvo que haber sido a través de la
						//edición con soporte de catálogos desasociados, mas adelante se validará esa situacion para en el cubo grabar correctamente
						//la descripción de cada dimensión independiente, aunque en la tabla paralela definitivamente se va a perder el key subrrogado
						//puesto que ya ninguno aplica para esta combinación, lo que no se sabe es si deberíamos dejar un 1 o -1 para indicar
						//NA o que fué una edición de una combinación que ya no existe
						//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
						//En este caso sucede igual que lo descrito en el If cuando no hay sección dinámica (ver arriba)
						$arrayPassValues = getKeyValues($aRepository, $arrayInstancesIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
					
						if($arrayPassValues!==false)
						{
							//@JAPR 2013-02-12: Agregada la edición de datos históricos
							//Si se hubiera regresado el valor de NA pero además la descripción fuera la clave especial NOCHANGE, quiere decir
							//que es un valor que posiblemente ya no se encontraba en el catálogo así que no hay necesidad de cambiar nada pues
							//las dimensiones tal como estaban ya apuntan al valor correcto. Esto solo aplica con catálogos desasociados
							if ((int) @$arrayPassValues[0] == 1 && $surveyInstance->DissociateCatDimens && trim($arrayValues[0])=="NOCHANGE") {
								$arrayPassValues = array(-1);
							}
							//@JAPR
							$arrayKeyValuesByQuestion[$questionKey] = $arrayPassValues;
						}
						else 
						{
							return($gblEFormsErrorMessage);
						}
					}
				}
			}
		}
		
		//@JAPR 2012-05-17: $arrayPassValues es el array con la (s) respuestas ya mapeadas a la tabla de dimensión, es decir, son las llaves subrrogadas
		//de lo que se contestó en esta pregunta, mientras que $arrayValues son las respuestas capturadas directamente por el usuario al contestar la
		//encuesta, por lo que según el tipo de pregunta pudieran ser textos, numeros o fechas o cualquier otra cosa
		
		/*************************************************************************************/
		//Generamos una coleccion de solo las preguntas que sean de selección múltiple 
		//y q pertenezcan a secciones dinamicas y esten dentro de ellas
		$uniqueMultiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceUniqueDim($aRepository, $surveyID);
		
		$countUniqueMChoiceQuestionCollection = count($uniqueMultiChoiceQuestionCollection->Collection);
	
		//Array de instancias de objetos dimension o indicator de los questions
		$arrayInstancesMCIndDim = array();
		$arrayInstancesMCIndicator = array();
		$arrayKeyMCValuesByQuestion = array();
		$arrayMCIndValuesByQuestion = array();
	
		//Recorremos las preguntas de seleccion multiple para obtener sus valores multiples
		foreach ($uniqueMultiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
		{
			$arrayValues = array();
			
			//Esto aplica para las preguntas multiple choice con numeros o textos
			//entonces lo procesamos como dimensiones unicas
			if($aQuestion->MCInputType>mpcCheckBox)
			{
				//Obtener valor capturado de dicha question
				$postKey = "qfield".$aQuestion->QuestionID;
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				if(isset($arrayData[$postKey]) && ($aQuestion->MCInputType!=mpcNumeric) || trim($arrayData[$postKey]) !== '')
				{
					if($aQuestion->MCInputType==mpcNumeric)
					{
						$arrayValues[0] = (int)$arrayData[$postKey];
					}
					else 
					{
						$arrayValues[0] = $arrayData[$postKey];
					}
				}
				else 
				{
					if($aQuestion->MCInputType==mpcNumeric)
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$arrayValues[0] = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					else 
					{
						$arrayValues[0] = $gblEFormsNA;
					}
				}
				
				//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					//@JAPR 2013-11-13: Validado que las dimensiones que son para preguntas numéricas graben NA en lugar de NULL
					if (is_null($arrayValues[0])) {
						//@JAPR 2014-01-10: Corregido un bug, al grabar en el cubo finalmente si no se selecciona entonces se graba un 0 incluso aunque
						//en realidad la pregunta hubiera sido saltada y debiera grabar *NA, por tanto se deja el 0 en caso de estar null (sólo aplica
						//para tipo CheckBox, las otras si grabarían el *NA o bien insertaría el elemento a la dimensión)
						if ($aQuestion->MCInputType == mpcCheckBox) {
							$arrQuestionDimValues[] = 0;		//($surveyInstance->UseNULLForEmptyNumbers)?$gblEFormsNA:0;
							$arrQuestionDimValuesDesc[] = 0;	//($surveyInstance->UseNULLForEmptyNumbers)?$gblEFormsNA:0;
						}
						else {
							$arrQuestionDimValues[] = $gblEFormsNA;
							$arrQuestionDimValuesDesc[] = $gblEFormsNA;
						}
						//@JAPR
					}
					else {
						$arrQuestionDimValues[] = $arrayValues[0];
						$arrQuestionDimValuesDesc[] = $arrayValues[0];
					}
				}
				//@JAPR
				
				if($aQuestion->MCInputType==mpcNumeric && $aQuestion->IsIndicatorMC==1)
				{
					$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
					$arrayInstancesMCIndicator[$aQuestion->QuestionID] = $anInstanceIndicator;
					chdir($strOriginalWD);
					$arrayMCIndValuesByQuestion[$questionKey] = $arrayValues;
					
					//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
					if ($gbEnableIncrementalAggregations && !is_null($anInstanceIndicator)) {
						//@JAPR 2013-11-14: Agregado el grabado incremental de agregaciones
						//En agregaciones incrementales editadas, primero se tiene que cancelar el valor anterior de los indicadores usando la
						//combinación que anteriormente tenía la captura, para posteriormente insertar directamente el nuevo valor de los indicadores
						//pero ya en la nueva combinación, ya que cualquier pregunta pudo haber cambiado y por ender no se "reutiliza" realmente
						//ninguna combinación, sino que simplemente lo que antes había debe ser cancelado, ya que al haber una dimensión de ID
						//único asegura que cada captura tendrá su propia combinación que ninguna otra pueda reutilizar
						$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $anInstanceIndicator, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
						$arrRowIndicValuesOld[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
						$dblNewValue = (float) $arrayValues[0];
						//$dblDiff = $dblNewValue - $dblCurrValue;
						$arrRowIndicValues[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = $dblNewValue;
					}
					//@JAPR
				}
				
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
				$arrayInstancesMCIndDim[$aQuestion->QuestionID] = $anInstanceDimension;
				chdir($strOriginalWD);
				//@JAPR 2013-11-19: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
					$arrQuestionDimValuesOld[] = $strDimDescValue;
					$arrQuestionDimValuesDescOld[] = $strDimDescValue;
				}
				//@JAPR
				
				$arrayPassValues = getKeyValuesForMC($aRepository, $arrayInstancesMCIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
				
				if($arrayPassValues!==false)
				{
					$arrayKeyMCValuesByQuestion[$questionKey] = $arrayPassValues;
				}
				else 
				{
					return($gblEFormsErrorMessage);
				}
			}
			else 
			{
				//Obtener valor capturado de dicha question
				$postKey = "qfield".$aQuestion->QuestionID;
				
				if(isset($arrayData[$postKey]))
				{
					$arrayValues[0] = (int)$arrayData[$postKey];
				}
				else 
				{
					$arrayValues[0] = 0;
				}
	
				$anInstanceDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
				$arrayInstancesMCIndDim[$aQuestion->QuestionID] = $anInstanceDimension;
				chdir($strOriginalWD);
				
				//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					$arrQuestionDimValues[] = $arrayValues[0];
					$arrQuestionDimValuesDesc[] = $arrayValues[0];
					//@JAPR 2013-11-19: Agregado el grabado incremental de agregaciones
					$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
					$arrQuestionDimValuesOld[] = $strDimDescValue;
					$arrQuestionDimValuesDescOld[] = $strDimDescValue;
				}
				//@JAPR
				
				$arrayPassValues = getKeyValuesForMC($aRepository, $arrayInstancesMCIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
				
				if($arrayPassValues!==false)
				{
					$arrayKeyMCValuesByQuestion[$questionKey] = $arrayPassValues;
				}
				else 
				{
					return($gblEFormsErrorMessage);
				}
			}
		}
		
		/************************************************************************************/
		//Generamos una coleccion de solo las preguntas que sean de selección múltiple
		//y los cuales siempre tengan activados que si son IsMultiDimension			//y q no pertenezcan a dimensiones dinamicas o bien esten fuera de ellas (porción extraída desde el SaveData, que se supone es equivalente a esta parte)
		$anArrayTypes = array();
		$anArrayTypes[0] = qtpMulti;
		//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
		$multiChoiceQuestionCollectionNoMultDim = BITAMQuestionCollection::NewInstanceMultiChoiceDims($aRepository, $surveyID, 0);
		//@JAPR
		$multiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceDims($aRepository, $surveyID);
		$arrayExistMultiChoiceIndicators = array();
		
		$arrayInstancesMultiDim = array();
		$arrayKeyMultiValuesByQuestion = array();
		
		$arrayInstancesMultiChoiceInds = array();
		$arrayMultiChoiceIndValuesByQuestion = array();

		//Verificar si existen opciones en preguntas de seleccion multiple
		$existMultiOptions = false;
	
		//Recorremos las preguntas de seleccion multiple para obtener sus valores multiples
		foreach ($multiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
		{
			$arrayValues = array();
			$postKey = "qfield".$aQuestion->QuestionID;
			
			if(isset($arrayData[$postKey]))
			{
				$strValues = $arrayData[$postKey];
				if(trim($strValues)!="")
				{
					$arrayValues = explode("_SVSep_", $arrayData[$postKey]);
				}
			}
			
			//Verificar si la pregunta tiene asignado indicadores por scores
			if($aQuestion->MCInputType == mpcCheckBox) 
			{
				//Verificamos si hay un score capturado para esta opcion de pregunta de seleccion multiple
				//que no sea matriz o bien revisamos si ya se crearon indicadores anteriormente
				$existIndicators = false;
				
				//Recorremos las opciones para revisar los indicadores
				foreach ($aQuestion->SelectOptions as $optionKey=>$optionValue)
				{
					$indicatorid = 0;
					
					if(isset($aQuestion->QIndicatorIds[$optionKey]))
					{
						$indicatorid = (int)$aQuestion->QIndicatorIds[$optionKey];
					}
					else 
					{
						$indicatorid = 0;
					}
		
					if(!is_null($indicatorid) && $indicatorid!=0)
					{
						$existIndicators = true;
						break;
					}
				}
				
				$arrayExistMultiChoiceIndicators[$aQuestion->QuestionID] = $existIndicators;
			}
			else 
			{
				$arrayExistMultiChoiceIndicators[$aQuestion->QuestionID] = false;
			}
			
			$arrayTempDims = array();
			$arrayTempInds = array();
			$QIndDimIDs = $aQuestion->QIndDimIds;
			
			//Obtenemos las instancias de las dimensiones de las opciones
			foreach ($QIndDimIDs as $qKey=>$qDimID) 
			{
				$existMultiOptions = true;
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $qDimID);
				$arrayTempDims[$qKey] = $anInstanceDimension;
				
				if($aQuestion->MCInputType == mpcCheckBox && $arrayExistMultiChoiceIndicators[$aQuestion->QuestionID]==true) 
				{
					$tmpInd = (int)$aQuestion->QIndicatorIds[$qKey];
					
					if($tmpInd!=0)
					{
						$arrayTempInds[$qKey] = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $tmpInd);
					}
					else 
					{
						$arrayTempInds[$qKey] = null;
					}
				}
				else 
				{
					$arrayTempInds[$qKey] = null;
				}
				
				//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					//@JAPR 2013-11-13: Validado que las dimensiones que son para preguntas numéricas graben NA en lugar de NULL
					if (is_null(@$arrayValues[$qKey])) {
						if ($aQuestion->MCInputType == mpcCheckBox) {
							//Si la pregunta es tipo CheckBox, el default es 0 en lugar de NA
							$arrQuestionDimValues[] = 0;
							$arrQuestionDimValuesDesc[] = 0;
						}
						else {
							$arrQuestionDimValues[] = $gblEFormsNA;
							$arrQuestionDimValuesDesc[] = $gblEFormsNA;
						}
					}
					else {
						//Si no viene asignado un valor, entonces se utilizará NA
						if ($arrayValues[$qKey] == '') {
							$arrQuestionDimValues[] = $gblEFormsNA;
							$arrQuestionDimValuesDesc[] = $gblEFormsNA;
						}
						else {
							$arrQuestionDimValues[] = @$arrayValues[$qKey];
							$arrQuestionDimValuesDesc[] = @$arrayValues[$qKey];
						}
					}
					
					//@JAPR 2013-11-19: Agregado el grabado incremental de agregaciones
					//@JAPR 2014-01-10: A diferencia de la corrección introducida en SaveData en esta fecha, la función getDimDescFromFactTable
					//(la cual también se usa desde report.inc.php para el borrado) ya va directamente al cubo por el valor de la dimensión, así
					//que en estos casos ya trae el 0 incluso si la pregunta no fué contestada realmente, ya que no graba nunca *NA en estos tipos
					$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
					$arrQuestionDimValuesOld[] = $strDimDescValue;
					$arrQuestionDimValuesDescOld[] = $strDimDescValue;
				}
				//@JAPR
				
				chdir($strOriginalWD);
			}
			
			$arrayInstancesMultiDim[$aQuestion->QuestionID] = $arrayTempDims;
			$arrayInstancesMultiChoiceInds[$aQuestion->QuestionID] = $arrayTempInds;
			
			if($aQuestion->MCInputType == mpcCheckBox) 
			{
				$arrayKeyMultiValuesByQuestion[$questionKey] = getKeyMultiValues($aRepository, $arrayInstancesMultiDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
				
				//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
				//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
				//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
				$bnlSaveScore = true;
				if ($surveyInstance->UseStdSectionSingleRec) {
					$bnlSaveScore = false;
					//En este caso las preguntas multiple-choice con Score deben grabar la dimensión en todos los registros, pero sólo deben grabar
					//el indicador de Score en el registro que corresponde con la sección/página en cuestión según el contexto del registro
					if (isset($arrMasterDetSectionsIDs[$aQuestion->SectionID])) {
						//- Sección Maestro-detalle siempre
						$bnlSaveScore = true;
					}
					elseif ($hasDynamicSection && $dynamicSectionID == $aQuestion->SectionID) {
						//- Sección Dinámica nunca, ya que toda multiple-choice en estas secciones se comporta como de catálogo automáticamente
					}
					elseif ($anEntryCatDimID > 0) {
						//- Cateogoría de dimensión nunca
					}
					elseif ($anEntrySectionID == 0 && $anEntryCatDimID == 0) {
						//- Sección Estándar sólo si es el registro único
						$bnlSaveScore = true;
					}
				}
				
				//Obtenemos los valores para los indicadores de los scores
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				if ($bnlSaveScore) {
					//Es un registro que si aplica para esta pregunta, por lo tanto graba el score
					$arrayMultiChoiceIndValuesByQuestion[$questionKey] = getScoreMultiValues($aRepository, $arrayInstancesMultiChoiceInds[$aQuestion->QuestionID], $aQuestion, $arrayExistMultiChoiceIndicators[$aQuestion->QuestionID], $arrayValues, $surveyInstance->UseNULLForEmptyNumbers);
				}
				else {
					//No se debe grabar el score, por tanto se manda vacio
					$arrayMultiChoiceIndValuesByQuestion[$questionKey] = getScoreMultiValues($aRepository, $arrayInstancesMultiChoiceInds[$aQuestion->QuestionID], $aQuestion, $arrayExistMultiChoiceIndicators[$aQuestion->QuestionID], array(), $surveyInstance->UseNULLForEmptyNumbers);
				}
				
				//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					foreach ($QIndDimIDs as $qKey => $qDimID) {
						$indicatorid = (int) @$aQuestion->QIndicatorIds[$qKey];
						if($indicatorid != 0) {
							$anInstanceIndicator = @$arrayTempInds[$qKey];
							//@JAPR 2013-11-14: Agregado el grabado incremental de agregaciones
							//En agregaciones incrementales editadas, primero se tiene que cancelar el valor anterior de los indicadores usando la
							//combinación que anteriormente tenía la captura, para posteriormente insertar directamente el nuevo valor de los indicadores
							//pero ya en la nueva combinación, ya que cualquier pregunta pudo haber cambiado y por ender no se "reutiliza" realmente
							//ninguna combinación, sino que simplemente lo que antes había debe ser cancelado, ya que al haber una dimensión de ID
							//único asegura que cada captura tendrá su propia combinación que ninguna otra pueda reutilizar
							$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $anInstanceIndicator, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
							$arrRowIndicValuesOld['IND_'.$indicatorid] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
							if ($bnlSaveScore) {
								$dblNewValue = (float) @$arrayMultiChoiceIndValuesByQuestion[$questionKey][$qKey];
								//$dblDiff = $dblNewValue - $dblCurrValue;
								$arrRowIndicValues['IND_'.$indicatorid] = $dblNewValue;
							}
							else {
								$arrRowIndicValues['IND_'.$indicatorid] = @$arrayMultiChoiceIndValuesByQuestion[$questionKey][$qKey];
							}
						}
					}
					
				}
				//@JAPR
			}
			else 
			{
				$arrayInstanceMultiDim = array();
				$arrayInstanceMultiDim = $arrayInstancesMultiDim[$aQuestion->QuestionID];
				$arrayKeys = array();
				$arrayZeroValues = array();
				$m=0;
				foreach ($arrayInstanceMultiDim as $anInstanceDim)
				{
					//@JAPR 2013-01-22: Corregido un bug, al introducir el registro único algunas preguntas ya no se mandan, pero se esperaba un valor
					$passValue = getInsertDimMultiValueKey($aRepository, $anInstanceDim, (string) @$arrayValues[$m]);
					
					if($passValue!==false)
					{
						$arrayKeys[$m] = $passValue;
					}
					else 
					{
						return($gblEFormsErrorMessage);
					}
					
				 	$arrayZeroValues[$m] = (($surveyInstance->UseNULLForEmptyNumbers)?null:(float)0);
				 	$m++;
				}
				$arrayKeyMultiValuesByQuestion[$questionKey] = $arrayKeys;
				
				//Almacenamos ceros por default para este arreglo para que contenga algo aunque para este tipo de Multiple Choice
				//con entradas numericas a alfanumericas no se manejen scores o indicadores
				$arrayMultiChoiceIndValuesByQuestion[$questionKey] = $arrayZeroValues;
			}
		}
		
		/*************************************************************************************/
		//Obtenemos las preguntas que pertenezcan a Category Dimension 
		$catDimQuestionsInfo = BITAMQuestion::getAllCategoryDimQuestionsInfo($aRepository, $surveyInstance->SurveyID);
		
		$catDimFieldNames = array();
		$catDimFieldValues = array();
	
		//Obtenemos campos y valores correspondientes de las preguntas CategoryDimension
		//Este código sólo debería grabar valores de no aplica, ya que sólo asume la existencia de un único elemento qCatDimValQID entre las respuestas
		//pero si pueden existir mas de una pregunta de tipo categoría de dimensión en la misma encuesta, además en cuanto se detectan respuestas
		//a este tipo de preguntas en el array se invoca a un método de grabado alternativo por lo que no hubiera llegado hasta aquí
		foreach ($catDimQuestionsInfo as $catDimQuestionKey=>$catDimQInfo)
		{
			$qid = -1;
			$postKey = "qCatDimValQID";
				
			if(isset($arrayData[$postKey]))
			{
				$qid = $arrayData[$postKey];
			}
			
			$categoryDimID = $catDimQInfo["CategoryDimID"];
			$elementDimID = $catDimQInfo["ElementDimID"];
			$elementIndID = $catDimQInfo["ElementIndID"];
			
			$categoryDimNA = $catDimQInfo["CategoryDimIDNA"];
			$elementDimNA = $catDimQInfo["ElementDimIDNA"];
	
			//Si resulta igual el questionID con el valor de qCatDimValQID
			//se procede a recolectar los valores
			if($catDimQuestionKey==$qid)
			{
				$qCatDimValue = $categoryDimNA;
				$postKey = "qCatDimVal";
					
				if(isset($arrayData[$postKey]))
				{
					$qCatDimValue = $arrayData[$postKey];
				}
				
				$qElemDimValue = $elementDimNA;
				$postKey = "qElemDimVal";
					
				if(isset($arrayData[$postKey]))
				{
					$qElemDimValue = $arrayData[$postKey];
				}
				
				$qElemDimValResponse = 0;
				$postKey = "qElemDimValResp";
					
				if(isset($arrayData[$postKey]))
				{
					$qElemDimValResponse = $arrayData[$postKey];
					//@JAPR 2012-08-09: Corregido un bug, no estaba validando correctamente por un dato vacio en este punto
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					if (trim($qElemDimValResponse) == '') {
						$qElemDimValResponse = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					//@JAPR
				}
	
				$catDimFieldNames[$categoryDimID] = "RIDIM_".$categoryDimID."KEY, RIDIM_".$elementDimID."KEY, IND_".$elementIndID;
				$catDimFieldValues[$categoryDimID] = $qCatDimValue.", ".$qElemDimValue.", ".StrIFNULL($qElemDimValResponse);
			}
			else 
			{
				if(!isset($catDimFieldNames[$categoryDimID]))
				{
					//En caso contrario se almacena el valor de No Aplica en los campos de CategoryDimension
					$catDimFieldNames[$categoryDimID] = "RIDIM_".$categoryDimID."KEY, RIDIM_".$elementDimID."KEY, IND_".$elementIndID;
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					$catDimFieldValues[$categoryDimID] = $categoryDimNA.", ".$elementDimNA.", ".StrIFNULL(($surveyInstance->UseNULLForEmptyNumbers)?null:(float)0);
				}
			}
		}
		
		//Obtenemos el numero de preguntas contestadas
		$answeredQuestions = getSurveyAnsweredQuestions($arrayData, $aRepository, $surveyInstance->SurveyID);
		$fieldAnsweredQuestionsInd = $answeredQuestionsIndicator->field_name.$answeredQuestionsIndicator->IndicatorID;
		
		$startDate = substr($arrayData["surveyDate"], 0, 10);
		$endDate = substr($thisDate, 0, 10);
		$currentDate = $arrayData["surveyDate"];
		$surveyHour = $arrayData["surveyHour"];
		$startTime = $arrayData["surveyHour"];
		$endTime = substr($thisDate, 11);
		$initialTime = strtotime((substr($currentDate, 0, 10)." ".$startTime));
		$finalTime = strtotime((substr($thisDate, 0, 10)." ".$endTime));
		$totalSeconds = $finalTime - $initialTime;
		$totalMinutes = $totalSeconds/60;
		//@JAPR 2014-10-27: Corregido un bug, el indicador duración sólo se debe grabar en el registro de la sección estándar
		//Durante la edición NO se está sobreescribiendo el valor del indicador
		$fieldDurationInd = $durationIndicator->field_name.$durationIndicator->IndicatorID;
		
		//Latitude y Longitude
		$fieldLatitudeInd = $latitudeIndicator->field_name.$latitudeIndicator->IndicatorID;
		$fieldLongitudeInd = $longitudeIndicator->field_name.$longitudeIndicator->IndicatorID;
		
		if(isset($arrayData['surveyLatitude'])) 
		{
			$latitude = (double)$arrayData['surveyLatitude'];
		} 
		else 
		{
			$latitude = 0;
		}
		
		if(isset($arrayData['surveyLongitude'])) 
		{
			$longitude = (double)$arrayData['surveyLongitude'];
		} 
		else 
		{
			$longitude = 0;
		}

		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues && !is_null($accuracyIndicator)) {
			$fieldAccuracyInd = $accuracyIndicator->field_name.$accuracyIndicator->IndicatorID;
			if(isset($arrayData['surveyAccuracy'])) 
			{
				$accuracy = (double)$arrayData['surveyAccuracy'];
			} 
			else 
			{
				$accuracy = 0;
			}
		}
		
		//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
		//Agrega los valores de las dimensiones default al array de recalculo incremental de agregaciones
		if ($gbEnableIncrementalAggregations) {
			if ($surveyInstance->SyncDateDimID > 0) {
				$arrRowDimValues[] = $syncDate;
				$arrRowDimDescValues[] = $syncDate;
				$aSyncDateInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SyncDateDimID);
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aSyncDateInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
			}
			if ($surveyInstance->SyncTimeDimID > 0) {
				$arrRowDimValues[] = $syncTime;
				$arrRowDimDescValues[] = $syncTime;
				$aSyncTimeInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SyncTimeDimID);
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aSyncTimeInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
			}

			//@JAPR 2014-11-19: Agregada la dimensión Agenda
			if (getMDVersion() >= esvAgendaDimID && $surveyInstance->AgendaDimID > 0) {
				$AgendaDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->AgendaDimID);
				if (!is_null($AgendaDimension)) {
					$arrRowDimValues[] = (string) @$arrDefDimValues['AgendaFilter'];
					$arrRowDimDescValues[] = (string) @$arrDefDimValues['AgendaFilter'];
					$arrRowDimValuesOld[] = (string) @$arrDefDimValues['AgendaFilter'];
					$arrRowDimDescValuesOld[] = (string) @$arrDefDimValues['AgendaFilter'];
				}
			}
			//@JAPR
			$arrRowDimValues[] = (string) @$arrDefDimValues['UserEMail'];
			$arrRowDimDescValues[] = (string) @$arrDefDimValues['UserEMail'];
			$arrRowDimValuesOld[] = (string) @$arrDefDimValues['UserEMail'];
			$arrRowDimDescValuesOld[] = (string) @$arrDefDimValues['UserEMail'];
			//@JAPR 2014-01-13: Corregido un bug al grabar el valor default de esta dimensión
			if (is_null($captureEmail) || trim($captureEmail) == '') {
				$arrRowDimValues[] = $gblEFormsNA;
				$arrRowDimDescValues[] = $gblEFormsNA;
				$arrRowDimValuesOld[] = $gblEFormsNA;
				$arrRowDimDescValuesOld[] = $gblEFormsNA;
			}
			else {
				$arrRowDimValues[] = $captureEmail;
				$arrRowDimDescValues[] = $captureEmail;
				$arrRowDimValuesOld[] = $captureEmail;
				$arrRowDimDescValuesOld[] = $captureEmail;
			}
			//@JAPR
			
			$statusDesc = '';
			$sql = "SELECT {$fieldDimStatusDesc} FROM {$tableDimStatus} WHERE {$fieldDimStatusKey} = {$statusKey}";
		    $aRS = $aRepository->DataADOConnection->Execute($sql);
		    if ($aRS || !$aRS->EOF) {
		    	$statusDesc = (string) @$aRS->fields[strtolower($fieldDimStatusDesc)];
		    }
			
			$arrRowDimValues[] = $statusDesc;
			$arrRowDimDescValues[] = $statusDesc;
			$arrRowDimValuesOld[] = $statusDesc;
			$arrRowDimDescValuesOld[] = $statusDesc;
			$arrRowDimValues[] = $schedulerID;
			$arrRowDimDescValues[] = $schedulerDesc;
			$arrRowDimValuesOld[] = $schedulerID;
			$arrRowDimDescValuesOld[] = $schedulerDesc;
			$arrRowDimValues[] = $startDate;
			$arrRowDimDescValues[] = $startDate;
			$arrRowDimValuesOld[] = $startDate;
			$arrRowDimDescValuesOld[] = $startDate;
			$arrRowDimValues[] = $endDate;
			$arrRowDimDescValues[] = $endDate;
			$arrRowDimValuesOld[] = $endDate;
			$arrRowDimDescValuesOld[] = $endDate;
			$arrRowDimValues[] = $startTime;
			$arrRowDimDescValues[] = $startTime;
			$arrRowDimValuesOld[] = $startTime;
			$arrRowDimDescValuesOld[] = $startTime;
			$arrRowDimValues[] = $endTime;
			$arrRowDimDescValues[] = $endTime;
			$arrRowDimValuesOld[] = $endTime;
			$arrRowDimDescValuesOld[] = $endTime;
			
			if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
					$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
				$serverStartDate = substr((string) @$arrayData["serverSurveyDate"], 0, 10);
				$serverEndDate = substr((string) @$arrayData["serverSurveyEndDate"], 0, 10);
				$serverStartTime = (string) @$arrayData["serverSurveyHour"];
				$serverEndTime = (string) @$arrayData["serverSurveyEndHour"];
				$arrRowDimValues[] = $serverStartDate;
				$arrRowDimDescValues[] = $serverStartDate;
				$aServerStartDateInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerStartDateDimID);
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aServerStartDateInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
				
				$arrRowDimValues[] = $serverEndDate;
				$arrRowDimDescValues[] = $serverEndDate;
				$aServerEndDateInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerEndDateDimID);
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aServerEndDateInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
				
				$arrRowDimValues[] = $serverStartTime;
				$arrRowDimDescValues[] = $serverStartTime;
				$aServerStartTimeInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerStartTimeDimID);
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aServerStartTimeInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
				
				$arrRowDimValues[] = $serverEndTime;
				$arrRowDimDescValues[] = $serverEndTime;
				$aServerEndTimeInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerEndTimeDimID);
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aServerEndTimeInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
			}
			
			while (count($arrQuestionDimValues) > 0) {
				array_push($arrRowDimValues, array_shift($arrQuestionDimValues));
			}
			while (count($arrQuestionDimValuesOld) > 0) {
				array_push($arrRowDimValuesOld, array_shift($arrQuestionDimValuesOld));
			}
			while (count($arrQuestionDimValuesDesc) > 0) {
				array_push($arrRowDimDescValues, array_shift($arrQuestionDimValuesDesc));
			}
			while (count($arrQuestionDimValuesDescOld) > 0) {
				array_push($arrRowDimDescValuesOld, array_shift($arrQuestionDimValuesDescOld));
			}
			
			$arrRowIndicValues[$fieldAnsweredQuestionsInd] = $answeredQuestions;
			$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $answeredQuestionsIndicator, null, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID, true);
			$arrRowIndicValuesOld[$fieldAnsweredQuestionsInd] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
			
			$arrRowIndicValues[$fieldDurationInd] = $totalMinutes;
			$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $durationIndicator, null, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID, true);
			$arrRowIndicValuesOld[$fieldDurationInd] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
			
			$arrRowIndicValues[$fieldLatitudeInd] = $latitude;
			$arrRowIndicValuesOld[$fieldLatitudeInd] = ($latitude == 0)?0:$latitude * -1;
			
			$arrRowIndicValues[$fieldLongitudeInd] = $longitude;
			$arrRowIndicValuesOld[$fieldLongitudeInd] = ($longitude == 0)?0:$longitude * -1;
			
			//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues && !is_null($accuracyIndicator)) {
				$arrRowIndicValues[$fieldAccuracyInd] = $accuracy;
				$arrRowIndicValuesOld[$fieldAccuracyInd] = ($accuracy == 0)?0:$accuracy * -1;
			}
			//@JAPR
		}
		
		//Esta variable nos permitira llevar un control de los campos que se encuentre repetidos
		//que no se vuelvan a considerar nuevamente en el UPDATE, esto aplica para las preguntas
		//de tipo seleccion simple con catalogo que usan el mismo catalogo
		$updateFields = array();
		
		//************************************************************************************************************************
		//************************************************************************************************************************
		//Actualiza los datos de la tabla de hechos
		//************************************************************************************************************************
		//************************************************************************************************************************
		$sql = "UPDATE ".$factTable." SET ";
		$sql.= $fieldDimStatusKey." = ".$statusKey.", ";
		$sql.= $fieldAnsweredQuestionsInd." = ".$answeredQuestions.", ";
		
		//@JAPR 2013-02-12: Agregada la edición de datos históricos
		$blnSkipDynAttribsWithNAVal = false;
		//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$arrProcessedAttribsByCatalog = array();
		$arrProcessedAttribsByCatalogForIncAggrs = array();
		$arrProcessedAttribsByCatalogForIncAggrsOld = array();
		//Procedemos a generar las combinaciones para insertarlas en la tabla de hechos
		foreach ($questionCollection->Collection as $questionKey => $aQuestion)
		{
			$fieldName = "";
			$fieldValues = "";
			$blnIncludeQuestion = true;
			
			//@JAPR 2012-05-17: Agregados nuevos tipos de pregunta Foto y Firma
			//Las preguntas de Foto y Firma no graban una respuesta, sólo la imagen, por lo tanto se salta esta pregunta completamente
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($aQuestion->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSketch:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
					$blnValidQuestion = false;
					break;
			}
			
			if (!$blnValidQuestion)
			{
				continue;
			}
			//@JAPR
			
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//Las preguntas numéricas pueden funcionar como indicador, por lo que opcionalmente se pudiera agregar un 2do campo para esto
				if($aQuestion->IsIndicator==1)
				{
					//$fieldName = $arrayInstancesIndicator[$aQuestion->QuestionID]->field_name;
					$fieldName = $arrayInstancesIndicator[$aQuestion->QuestionID]->field_name.$arrayInstancesIndicator[$aQuestion->QuestionID]->IndicatorID;
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					$fieldValues = StrIFNULL($arrayIndValuesByQuestion[$questionKey][0]);
					if(!isset($updateFields[$fieldName])) {
						$sql.=$fieldName." = ".$fieldValues.", ";
						$updateFields[$fieldName] = $fieldName;
					}
				}
				
				$fieldName = $arrayInstancesIndDim[$aQuestion->QuestionID]->Dimension->TableName."KEY";
				$fieldValues = $arrayKeyValuesByQuestion[$questionKey][0];
			}
			else 
			{
				//Las preguntas de selección sencilla que no son de catálogo pueden capturar un Score por respuesta y almacenarlo en un indicador,
				//por lo que opcionalmente se pudiera agregar un 2do campo para esto
				if(($aQuestion->QTypeID==qtpSingle) && $aQuestion->UseCatalog==0 && $aQuestion->IndicatorID>0)
				{
					//$fieldName = $arrayInstancesIndicatorSC[$aQuestion->QuestionID]->field_name;
					$fieldName = $arrayInstancesIndicatorSC[$aQuestion->QuestionID]->field_name.$arrayInstancesIndicatorSC[$aQuestion->QuestionID]->IndicatorID;
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					$fieldValues = StrIFNULL($arrayIndValuesSCByQuestion[$questionKey][0]);
					if(!isset($updateFields[$fieldName])) {
						$sql.=$fieldName." = ".$fieldValues.", ";
						$updateFields[$fieldName] = $fieldName;
					}
				}
				
				//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if($surveyInstance->DissociateCatDimens && $aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1) {
					//Si se usan catálogos desasociados, entonces no se graba sólo un campo de dimensión por pregunta de catálogo, sino que para cada
					//pregunta tiene un campo con su atributo correspondiente (ya que al día de implementación no podía haber 2 o mas preguntas con
					//el mismo atributo) pero además existen dimensiones para todos los atributos previos, así que la 1er pregunta de este catálogo que
					//se procese insertaría el valor para esos atributos previos, mientras que las preguntas subsecuentes sólo deberán insertar valores
					//para los atributos posteriores a la 1er pregunta que aun no se hubieran procesado
					if (!isset($arrProcessedAttribsByCatalog[$aQuestion->CatalogID])) {
						$arrProcessedAttribsByCatalog[$aQuestion->CatalogID] = array();
					}
					if (!isset($arrProcessedAttribsByCatalogForIncAggrs[$aQuestion->CatalogID])) {
						$arrProcessedAttribsByCatalogForIncAggrs[$aQuestion->CatalogID] = array();
					}
					if (!isset($arrProcessedAttribsByCatalogForIncAggrsOld[$aQuestion->CatalogID])) {
						$arrProcessedAttribsByCatalogForIncAggrsOld[$aQuestion->CatalogID] = array();
					}
					
					$intCatMemberID = $aQuestion->CatMemberID;
					//Si el registro que se está grabando es el correspondiente con la sección dinámica, entonces como ya todas las preguntas de
					//catálogo técnicamente se igualaron al mismo key subrrogado (que corresponde ya sea al valor de la página o checkbox dinámico), 
					//se utilizará hasta el atributo máximo usado por la sección dinámica, ya que en teoría no se deberían permitir preguntas de
					//catálogo del mismo de la dinámica después de dicha sección, por lo que nadie hubiera agregado los valores a las dimensiones de
					//sus atributos
					if ($hasDynamicSection && $anEntrySectionID == (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty)) {
						//Sólo en caso de que el atributo de la pregunta fuera posterior al de la sección es como utilizaría el de la pregunta
						if ((int)@$arrDynamicCatalogAttributesColl[$dynamicSectionCatMemberID] > (int)@$arrDynamicCatalogAttributesColl[$intCatMemberID]) {
							$intCatMemberID = (($dynamicSectionWithMultiChoice)?-1:$dynamicSectionCatMemberID);
						}
					}
					
					//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
					//Si el valor del key surrogado de catálogo para esta pregunta es el mismo de NA, entonces se hará el grabado a partir del valor
					//específico de esta pregunta porque significaría que actualmente la combinación recibida ya no existe en el catálogo, sin
					//embargo como se está usando el método para desasociar dimensiones, es seguro grabar en las dimensiones independientes cualquier
					//cosa que hubiera llegado
					//Se hace una validación adicional porque si además la pregunta fuera del mismo catálogo de la dinámica, se deberá usar el array
					//de valores completos de dicha sección según el número de registro que se está procesando
					$postKey = "qfield".$aQuestion->QuestionID;
					$strCatQuestionFullValue = trim((string) @$arrayData[$postKey]);
					if (stripos($strCatQuestionFullValue, '_SVSep_') === false) {
						//Si no tiene el formato de una respuesta de pregunta simple choice de catálogo, entonces no se envía para que funcione de la
						//manera original y vaya al catálogo a buscar los datos
						$strCatQuestionFullValue = '';
					}
					
					$intDynamicRecNum = -1;
					if ($anEntrySectionID == (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty)) {
						//Sólo se envía este parámetro si realmente es el grabado de un registro de la sección dinámica
						$intDynamicRecNum = $aDynamicRecNum;
					}
					
					$arrCatalogValues = getCatValueArrayForSurveyField($aRepository, $surveyInstance, $arrayKeyValuesByQuestion[$questionKey], $aQuestion->CatalogID, $intCatMemberID, $intDynamicRecNum, $strCatQuestionFullValue);
					$arrLastCatClaDescripFlipped = array_flip($arrLastCatClaDescrip);
					if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
						foreach ($arrCatalogValues as $aClaDescrip => $intCatValueKey) {
							if (isset($arrProcessedAttribsByCatalog[$aQuestion->CatalogID][$aClaDescrip])) {
								//Este atributo ya se había procesado para este registro, por lo tanto heredará el valor que la pregunta anterior del
								//mismo catálogo venía arrastrando pues todas las preguntas del mismo catálogo están ligadas entre si
								continue;
							}
							
							//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
							if ($gbEnableIncrementalAggregations) {
								if (!isset($arrCatalogDimValuesByCatalog[$aQuestion->CatalogID])) {
									$arrCatalogDimValuesByCatalog[$aQuestion->CatalogID] = array();
								}
								
								$intIndex = @$arrLastCatClaDescripFlipped[$aClaDescrip];
								$strDimValue = '';
								if (!is_null($intIndex)) {
									$strDimValue = (string) @$arrLastCatValues[$intIndex];
								}
								if (trim($strDimValue) == '') {
									$strDimValue = "*NA";
								}
								
								$arrCatalogDimValuesByCatalog[$aQuestion->CatalogID][] = $strDimValue;
								//$arrRowDimValues[] = $strDimValue;
								//$arrRowDimDescValues[] = $strDimValue;
							}
							//@JAPR
							
							$strDimFieldKey = "RIDIM_".$aClaDescrip."KEY";
							$fieldName = $strDimFieldKey;
							$intDimFieldValueKey = ((int)@$intCatValueKey < 0)?1:$intCatValueKey;
							$fieldValues = $intDimFieldValueKey;
							$arrProcessedAttribsByCatalog[$aQuestion->CatalogID][$aClaDescrip] = $intDimFieldValueKey;
							$arrProcessedAttribsByCatalogForIncAggrs[$aQuestion->CatalogID][$aClaDescrip] = $intDimFieldValueKey;
							$sql.=$fieldName." = ".$fieldValues.", ";
						}
						
						//Si se calcularán agregados incrementales, primero se debe cancelar el valor de la combinación anterior, así que carga
						//los datos tal como los tiene el registro para esta pregunta de catálogo
						if ($gbEnableIncrementalAggregations) {
							$arrCatalogValues = getCatQuestionValueArray($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID, (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty), $dynamicSectionCatID);
							if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
								foreach ($arrCatalogValues as $aClaDescrip => $strCatValueDesc) {
									if (isset($arrProcessedAttribsByCatalogForIncAggrsOld[$aQuestion->CatalogID][$aClaDescrip])) {
										//Este atributo ya se había procesado para este registro, por lo tanto ya no se puede agregar al array de datos
										//para agregaciones incrementales nuevamente
										continue;
									}
									
									if (!isset($arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID])) {
										$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID] = array();
									}
									
									$strDimValue = $strCatValueDesc;
									if (trim($strDimValue) == '') {
										$strDimValue = "*NA";
									}
									$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID][] = $strDimValue;
									//$arrRowDimValues[] = $strDimValue;
									//$arrRowDimDescValues[] = $strDimValue;
									$arrProcessedAttribsByCatalogForIncAggrsOld[$aQuestion->CatalogID][$aClaDescrip] = true;
								}
							}
						}
					}
					else {
						//@JAPR 2013-02-12: Agregada la edición de datos históricos
						//Este es el caso especial donde no se cambió el valor de la pregunta de catálogo, así que no se deben procesar los
						//atributos que restan y que estarían dentro de la sección dinámica si es que hubiera alguna
						$intSectionValue = (int) trim(@$arrayKeyValuesByQuestion[$questionKey][0]);
						if($intSectionValue < 0 && $surveyInstance->DissociateCatDimens) {
							//Esto solo es necesario si el registro a procesar NO es de la sección dinámica, si lo fuera entonces mas abajo donde 
							//se actualizan a NA los atributos de dicha sección no hubiera entrado de todas maneras, así que en un registro que no
							//es de la dinámica y que ya determinamos que no necesitamos cambiar, solo resta validar que efectivamente este caso
							//especial sea con el mismo catálogo de la dinámica, ya que de no ser así entonces no importaría pues abajo no se
							//va a procesar este catálogo
							if ($anEntrySectionID != (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty) && $aQuestion->CatalogID == $dynamicSectionCatID) {
								$blnSkipDynAttribsWithNAVal = true;
							}
						}
						
						//@JAPR 2013-11-15: Agregado el grabado incremental de agregaciones
						//En esta parte como se trata de una edición, se tienen que agregar al array de valores de las dimensiones los valores
						//actualmente grabados para esta combinación (aunque no vayan a cambiar), de lo contrario el agregado se recalculará
						//equivocadamente. Aquí la combinación actual y la anterior valía lo mismo para esta pregunta/catálogo, así que se hace
						//el proceso de llenado de arrays por duplicado
						if ($gbEnableIncrementalAggregations) {
							$arrCatalogValues = getCatQuestionValueArray($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID, (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty), $dynamicSectionCatID);
							if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
								foreach ($arrCatalogValues as $aClaDescrip => $strCatValueDesc) {
									if (isset($arrProcessedAttribsByCatalogForIncAggrs[$aQuestion->CatalogID][$aClaDescrip])) {
										//Este atributo ya se había procesado para este registro, por lo tanto ya no se puede agregar al array de datos
										//para agregaciones incrementales nuevamente
										continue;
									}
									
									if (!isset($arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID])) {
										$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID] = array();
									}
									if (!isset($arrCatalogDimValuesByCatalog[$aQuestion->CatalogID])) {
										$arrCatalogDimValuesByCatalog[$aQuestion->CatalogID] = array();
									}
									
									$strDimValue = $strCatValueDesc;
									if (trim($strDimValue) == '') {
										$strDimValue = "*NA";
									}
									$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID][] = $strDimValue;
									$arrCatalogDimValuesByCatalog[$aQuestion->CatalogID][] = $strDimValue;
									//$arrRowDimValues[] = $strDimValue;
									//$arrRowDimDescValues[] = $strDimValue;
									$arrProcessedAttribsByCatalogForIncAggrsOld[$aQuestion->CatalogID][$aClaDescrip] = true;
									$arrProcessedAttribsByCatalogForIncAggrs[$aQuestion->CatalogID][$aClaDescrip] = true;
								}
							}
							
							//Si es una pregunta del mismo catálogo de la sección dinámica, el proceso que para el grabado normal se hace mas abajo
							//para rellenar los atributos especiales de dicha sección no es necesario para el estatudo UPDATE pero si lo es para 
							//los arrays incrementales, porque la propia función getCatQuestionValueArray ya los incluirí para este caso especial 
							//sólo si el registro actual fuera de la sección dinámica, pero en este caso no lo es, así que sólo trae el dato hasta
							//el atributo usado por la pregunta que se está procesando en esta iteración, así que aunque se va a saltar el proceso
							//mas abajo que llena el estatuto UPDATE debido al cambio en la variable $blnSkipDynAttribsWithNAVal, se agregó un segundo
							//IF para permitir actualizar sólo el array de las agregaciones incrementales en este caso
						}
						//@JAPR
					}
					//@JAPR
					$blnIncludeQuestion = false;
				}
				else {
					$fieldName = $arrayInstancesIndDim[$aQuestion->QuestionID]->Dimension->TableName."KEY";
					$fieldValues = $arrayKeyValuesByQuestion[$questionKey][0];
				}
				//@JAPR
			}
			
			if(!isset($updateFields[$fieldName]))
			{
				if ($blnIncludeQuestion) {
					$sql.=$fieldName." = ".$fieldValues;
				}
			}
			
			if($questionKey!=($countCollection-1))
			{
				if(!isset($updateFields[$fieldName]))
				{
					//Validado para que no agregue 2 comas consecutivas sin campos
					if (substr($sql, -2, 2) != ", ") {
						$sql.=", ";
					}
				}
			}
			else 
			{
				//Si es la ultima posicion de esta coleccion
				//y dicho campo ya esta agregado previamente 
				//entonces se debe quitar la ultima coma
				//if(isset($insertFields[$fieldName]))
				if(substr($sql, -2, 2) == ", ")
				{
					$sql = substr($sql, 0, -2);
				}
			}
			
			$updateFields[$fieldName] = $fieldName;
		}
		
		//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//Justo después de procesar las preguntas simple choice se verifica si este registro no pertenece a la sección dinámica en caso de haber
		//alguna, si fuera el caso se tienen que procesar las dimensiones de los atributos que son parte de la sección dinámica pero asignando el
		//valor de NA ya que para este registro no se utilizan
		//@JAPR 2013-02-12: Agregada la edición de datos históricos
		//Si fuera el caso de edición donde no hubo un cambio de valores en la pregunta de catálogo, entonces NO se debe ejecutar este proceso
		//ya que como técnicamente no se ha procesado ningún atributo, terminaría sobreescribiendo todos con el valor de NA, de todas maneras
		//si fuera el caso entonces el registro ya contiene valores correctos
		if (!$blnSkipDynAttribsWithNAVal && $surveyInstance->DissociateCatDimens && ($dynamicSectionID + $dynamicSectionIDEmpty > 0)) {
			if ($anEntrySectionID != (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty)) {
				//En este caso el registro procesado (que puede ser 0 o el ID de la sección Maestro-detalle) no es el mismo que la sección dinámica,
				//se graban sus atributos con NA
				$arrCatalogValues = getCatValueArrayForSurveyField($aRepository, $surveyInstance, array(1), $dynamicSectionCatID, (($dynamicSectionWithMultiChoice)?-1:$dynamicSectionCatMemberID));
				$arrLastCatClaDescripFlipped = array_flip($arrLastCatClaDescrip);
				if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
					foreach ($arrCatalogValues as $aClaDescrip => $intCatValueKey) {
						if (isset($arrProcessedAttribsByCatalog[$dynamicSectionCatID][$aClaDescrip])) {
							//Este atributo ya se había procesado para este registro, por lo tanto heredará el valor que la pregunta anterior del
							//mismo catálogo venía arrastrando pues todas las preguntas del mismo catálogo están ligadas entre si
							continue;
						}
						
						//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
						if ($gbEnableIncrementalAggregations) {
							//En este caso complementa los atributos no capturables del registro con el valor de NA, como es la misma situación
							//tanto en el registro anterior como en el editado ya que la pregunta realmente no tiene por que usar estos atributos,
							//hace el proceso por duplicado en los arrays
							if (!isset($arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID])) {
								$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID] = array();
							}
							if (!isset($arrCatalogDimValuesByCatalog[$aQuestion->CatalogID])) {
								$arrCatalogDimValuesByCatalog[$aQuestion->CatalogID] = array();
							}
							
							$intIndex = @$arrLastCatClaDescripFlipped[$aClaDescrip];
							$strDimValue = '';
							if (!is_null($intIndex)) {
								$strDimValue = (string) @$arrLastCatValues[$intIndex];
							}
							if (trim($strDimValue) == '') {
								$strDimValue = "*NA";
							}
							$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID][] = $strDimValue;
							$arrCatalogDimValuesByCatalog[$aQuestion->CatalogID][] = $strDimValue;
							//$arrRowDimValues[] = $strDimValue;
							//$arrRowDimDescValues[] = $strDimValue;
						}
						//@JAPR
						
						$strDimFieldKey = "RIDIM_".$aClaDescrip."KEY";
						$fieldName = $strDimFieldKey;
						$intDimFieldValueKey = ((int)@$intCatValueKey < 0)?1:$intCatValueKey;
						$fieldValues = $intDimFieldValueKey;
						$arrProcessedAttribsByCatalog[$dynamicSectionCatID][$aClaDescrip] = $intDimFieldValueKey;
						$sql.=", ".$fieldName." = ".$fieldValues;
					}
				}
			}
		}
		
		//Es el mismo proceso que el IF de arriba, pero aquí entra cuando la pregunta no fué contestada durante la edición, por lo que se salta
		//todo el proceso de arriba, sin embargo en los arrays de agregados incrementales si se tienen que agregar los valores de las dimensiones,
		//por lo que realiza el proceso por duplicado ya que al no haber cambiado la respuesta, tanto el array actual como el anterior deben valer
		//lo mismo
		if ($gbEnableIncrementalAggregations && $surveyInstance->DissociateCatDimens && ($dynamicSectionID + $dynamicSectionIDEmpty > 0)) {
			if ($anEntrySectionID != (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty)) {
				//En este caso el registro procesado (que puede ser 0 o el ID de la sección Maestro-detalle) no es el mismo que la sección dinámica,
				//se graban sus atributos con NA
				$arrCatalogValues = getCatValueArrayForSurveyField($aRepository, $surveyInstance, array(1), $dynamicSectionCatID, (($dynamicSectionWithMultiChoice)?-1:$dynamicSectionCatMemberID));
				$arrLastCatClaDescripFlipped = array_flip($arrLastCatClaDescrip);
				if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
					foreach ($arrCatalogValues as $aClaDescrip => $intCatValueKey) {
						if (isset($arrProcessedAttribsByCatalogForIncAggrs[$dynamicSectionCatID][$aClaDescrip])) {
							//Este atributo ya se había procesado para este registro, por lo tanto heredará el valor que la pregunta anterior del
							//mismo catálogo venía arrastrando pues todas las preguntas del mismo catálogo están ligadas entre si
							continue;
						}
						
						//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
						if (!isset($arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID])) {
							$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID] = array();
						}
						if (!isset($arrCatalogDimValuesByCatalog[$aQuestion->CatalogID])) {
							$arrCatalogDimValuesByCatalog[$aQuestion->CatalogID] = array();
						}
						
						$intIndex = @$arrLastCatClaDescripFlipped[$aClaDescrip];
						$strDimValue = '';
						if (!is_null($intIndex)) {
							$strDimValue = (string) @$arrLastCatValues[$intIndex];
						}
						if (trim($strDimValue) == '') {
							$strDimValue = "*NA";
						}
						$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID][] = $strDimValue;
						$arrCatalogDimValuesByCatalog[$aQuestion->CatalogID][] = $strDimValue;
						//$arrRowDimValues[] = $strDimValue;
						//$arrRowDimDescValues[] = $strDimValue;
						//@JAPR
						$arrProcessedAttribsByCatalogForIncAggrsOld[$dynamicSectionCatID][$aClaDescrip] = true;
						$arrProcessedAttribsByCatalogForIncAggrs[$dynamicSectionCatID][$aClaDescrip] = true;
					}
				}
			}
		}
		//@JAPR
		
		//Si todavia existen preguntas de tipo multiple choice de unica dimension entonces si se debe concatenar la coma (,)
		//en la variable $sql
		//@JAPR 2012-05-17: Agregados nuevos tipos de pregunta Foto y Firma
		//Validado que ahora pregunte directamente por la coma al final, ya que si la última pregunta fué tipo Foto o Firma, no se hubiera eliminado
		//la coma dentro del ciclo anterior después de la última pregunta que no fuera Foto / Firma así que habría repetido coma en este punto
		if($countUniqueMChoiceQuestionCollection>0 && $countCollection>0 && substr(trim($sql), -1, 1) != ',')
		{
			$sql.=", ";
		}
	
		/*****************************************************************************************************/
		//Procedemos a generar las siguientes combinaciones para insertarlas en la tabla de hechos
		//estas preguntas pertenecen a secciones dinamicas son de tipo MChoice
		foreach ($uniqueMultiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
		{
			$fieldName = "";
			$fieldValues = "";
	
			if($aQuestion->MCInputType==mpcNumeric)
			{
				//Las preguntas numéricas de opción múltiple pueden funcionar como indicador, por lo que opcionalmente se pudiera agregar un 2do
				//campo para esto
				if($aQuestion->IsIndicatorMC==1)
				{
					//$fieldName = $arrayInstancesMCIndicator[$aQuestion->QuestionID]->field_name;
					$fieldName = $arrayInstancesMCIndicator[$aQuestion->QuestionID]->field_name.$arrayInstancesMCIndicator[$aQuestion->QuestionID]->IndicatorID;
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					$fieldValues = StrIFNULL($arrayMCIndValuesByQuestion[$questionKey][0]);
					
					$sql.=$fieldName." = ".$fieldValues.", ";
				}
				
				$fieldName = $arrayInstancesMCIndDim[$aQuestion->QuestionID]->Dimension->TableName."KEY";
				$fieldValues = $arrayKeyMCValuesByQuestion[$questionKey][0];
			}
			else 
			{
				$fieldName = $arrayInstancesMCIndDim[$aQuestion->QuestionID]->Dimension->TableName."KEY";
				$fieldValues = $arrayKeyMCValuesByQuestion[$questionKey][0];
			}
			
			$sql.=$fieldName." = ".$fieldValues;
				
			if($questionKey!=($countUniqueMChoiceQuestionCollection-1))
			{
				$sql.=", ";
			}
		}
		/****************************************************************************************************/
		
		//Si todavia existen preguntas de tipo multiple choice entonces si se debe concatenar la coma (,)
		//en las variables $sql 
		//@JAPRWarning: Esta condición se debería hacer contra el count($multiChoiceQuestionCollection) ya que la variable $countUniqueMChoiceQuestionCollection
		//es para preguntas múltiples de secciones dinámicas, sin embargo funcionaba por el Or del $countCollection que siempre sería true
		//Validado que ahora pregunte directamente por la coma al final, ya que si la última pregunta fué tipo Foto o Firma, no se hubiera eliminado
		//la coma dentro del ciclo anterior después de la última pregunta que no fuera Foto / Firma así que habría repetido coma en este punto
		if($existMultiOptions==true && ($countUniqueMChoiceQuestionCollection>0 || $countCollection>0) && substr(trim($sql), -1, 1) != ',')
		{
			$sql.=", ";
		}

		$countMultiChoiceQuestionCollection = count($multiChoiceQuestionCollection->Collection);
		//Todavia seguimos generando el UPDATE para las preguntas de seleccion multiple
		foreach ($multiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
		{
			//Obtenemos la instancia de multiples dimensiones de la pregunta que se esta procesando
			$arrayInstanceMultiDim = $arrayInstancesMultiDim[$aQuestion->QuestionID];
			
			//Obtenemos las respuestas para esa pregunta
			$arrayMultiValuesQuestion = $arrayKeyMultiValuesByQuestion[$questionKey];
			
			//Recorremos las instancias de dimensiones
			foreach ($arrayInstanceMultiDim as $numKey=>$anInstanceDim)
			{
				$fieldName = $anInstanceDim->Dimension->TableName."KEY";
				$fieldValues = $arrayMultiValuesQuestion[$numKey];
				
				$sql.=$fieldName." = ".$fieldValues;
				$sql.=", ";
			}
			
			//Verificamos si la pregunta es de tipo MCInputType = mpcCheckBox (Checkbox)
			if($aQuestion->MCInputType==mpcCheckBox && $arrayExistMultiChoiceIndicators[$aQuestion->QuestionID]==true)
			{
				//Obtenemos las instancias de multiples indicadores de la pregunta que se esta procesando
				$arrayMultiChoiceIndicators = $arrayInstancesMultiChoiceInds[$aQuestion->QuestionID];
				
				//Obtenemos las respuestas para esa pregunta
				$arrayMultiChoiceValuesQuestion = $arrayMultiChoiceIndValuesByQuestion[$questionKey];
				
				//Recorremos las instancias de indicadores
				foreach ($arrayMultiChoiceIndicators as $numKey=>$anInstanceInd)
				{
					//Verificamos q sea una instancia valida de Indicador
					if(!is_null($anInstanceInd))
					{
						//$fieldName = $anInstanceInd->field_name;
						$fieldName = $anInstanceInd->field_name.$anInstanceInd->IndicatorID;
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$fieldValues = StrIFNULL($arrayMultiChoiceValuesQuestion[$numKey]);
			
						$sql.=$fieldName." = ".$fieldValues;
						$sql.=", ";
					}
				}
			}
		}
		
		//Si existieron preguntas de tipo multiple choice entonces si se concateno una coma (,) extra
		//en las variables $sql y $sqlIndDimValues y por lo tanto se tiene q eliminar
		//@JAPR 2012-05-22: Agregada validación para que se remueva la última "," dejada posiblemente por preguntas que realmente no contienen 
		//respuesta cuando son la última de la encuesta (tipo foto o firma por ejemplo)
		//Se asumirá que si entró aquí es porque al final había una "," de más, y que los campos concatenados posteriores a este punto siempre
		//anteponen una "," en caso de necesitar agregar mas elementos, por lo que una "," al final en este punto ya no es esperada
		if($existMultiOptions==true || substr(trim($sql), -1, 1) == ',')
		{
			$sql = substr($sql, 0, -2);
		}
		//@JAPR
		
		//Metemos los campos de las dimensiones de las preguntas Matrix
		//Y tambien vamos a meter las llaves de las dims de la matrix
        $matrixCollection = BITAMQuestionCollection::NewInstanceWithSimpleChoiceMatrix($aRepository, $surveyInstance->SurveyID);
        $matrixDim = getMatrixDimFields($aRepository, $matrixCollection, $arrayData);

        if($matrixDim===false)
        {
        	return($gblEFormsErrorMessage);
        }

        $arrayMatrix = array();
        if(trim($matrixDim['fieldNames'])!="" && trim($matrixDim['fieldValues'])!="")
        {
        	$arrayMatrix = array_combine(explode(", ",$matrixDim['fieldNames']),explode(", ",$matrixDim['fieldValues']));
        }
        
        //Si todavia existen preguntas de tipo matriz entonces si se debe concatenar la coma (,)
		//en las variables $sql 
		if(count($arrayMatrix) > 0 && ($countMultiChoiceQuestionCollection>0 || $countUniqueMChoiceQuestionCollection>0 || $countCollection>0))
		{
			$sql.=", ";
		}

        foreach($arrayMatrix as $key => $value) 
        {
            $sql .= $key." = ".$value.", ";
        }
        
        if(count($arrayMatrix) > 0) 
        {
			$sql = substr($sql, 0, -2);
		}
		
		//Agregamos los campos de CategoryDimension
		if(count($catDimFieldNames)>0 && count($catDimFieldValues)>0 && (count($arrayMatrix)>0 || $countMultiChoiceQuestionCollection>0 || $countUniqueMChoiceQuestionCollection>0 || $countCollection>0)) 
		{
			$sql.=", ";
		}
		
		$allCatDimFieldNames = implode(", ", $catDimFieldNames);
		$allCatDimFieldValues = implode(", ", $catDimFieldValues);
		
	    $arrayCatDimFields = array();
	    if(trim($allCatDimFieldNames)!="" && trim($allCatDimFieldValues)!="")
	    {
	    	$arrayCatDimFields = array_combine(explode(", ", $allCatDimFieldNames), explode(", ", $allCatDimFieldValues));
	    }
	    
		foreach($arrayCatDimFields as $key => $value) 
		{
		    $sql.= $key." = ".$value.", ";
		}
		
		if(count($arrayCatDimFields)>0) 
		{
			$sql = substr($sql, 0, -2);
		}

		$sql.=" WHERE FactKey = ".$factKey." AND ".$fieldDimEncuestaID." = ".$factKeyDimVal;
		
		//@JAPR 2013-11-15: Agregado el grabado incremental de agregaciones
		//Antes de realizar el Insert, tiene que agregar los valores de las preguntas de catálogo en la misma secuencia en que encuentra los catálogos,
		//pero agregando todos los atributos a la vez para cada uno, ya que podrían venir preguntas de diferentes catálogos intercaladas pero dejarlo
		//así sólo complicaría las cosas
		if ($gbEnableIncrementalAggregations) {
			foreach ($arrCatalogDimValuesByCatalogOld as $intCatalogID => $arrCatalogValues) {
				foreach ($arrCatalogValues as $strDimValue) {
					$arrRowDimValuesOld[] = $strDimValue;
					$arrRowDimDescValuesOld[] = $strDimValue;
				}
			}
			foreach ($arrCatalogDimValuesByCatalog as $intCatalogID => $arrCatalogValues) {
				foreach ($arrCatalogValues as $strDimValue) {
					$arrRowDimValues[] = $strDimValue;
					$arrRowDimDescValues[] = $strDimValue;
				}
			}
		}
		
		//Verificamos si hay preguntas que fueron modificadas
		//si no es el caso entonces no se debera ejecutar ningun UPDATE
		if(count($arrayCatDimFields)>0 || count($arrayMatrix)>0 || $countMultiChoiceQuestionCollection>0 || $countUniqueMChoiceQuestionCollection>0 || $countCollection>0)
		{
			//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
			if ($gbEnableIncrementalAggregations) {
				$arrRowDimValues[] = $factKeyDimVal;
				$arrRowDimDescValues[] = $factKeyDimVal;
				$arrRowDimValuesOld[] = $factKeyDimVal;
				$arrRowDimDescValuesOld[] = $factKeyDimVal;
			}
			
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nupdateData UPDATE: ".$sql);
				
				if ($gbEnableIncrementalAggregations) {
					echo ("<br>\r\n<br>\r\nIncremental aggregations data arrays Old: Index == {$intComboID}, FactKey == {$factKey}");
					echo ("<br>\r\nnarrDateValues:");
					PrintMultiArray(@$arrDateValues);
					echo ("<br>\r\nnarrDimKeyValues: Index == {$intComboID}");
					PrintMultiArray(@$arrDimKeyValues[$intComboID]);
					echo ("<br>\r\nnarrDimDescValues: Index == {$intComboID}");
					PrintMultiArray(@$arrDimDescValues[$intComboID]);
					echo ("<br>\r\nnarrIndicValues: Index == {$intComboID}");
					PrintMultiArray(@$arrIndicValues[$intComboID]);
					
					echo ("<br>\r\n<br>\r\nIncremental aggregations data arrays: Index == ".($intComboID +1));
					//echo ("<br>\r\nnarrDateValues:");
					//PrintMultiArray(@$arrDateValues);
					echo ("<br>\r\nnarrDimKeyValues: Index == ".($intComboID +1));
					PrintMultiArray(@$arrDimKeyValues[$intComboID +1]);
					echo ("<br>\r\nnarrDimDescValues: Index == ".($intComboID +1));
					PrintMultiArray(@$arrDimDescValues[$intComboID +1]);
					echo ("<br>\r\nnarrIndicValues: Index == ".($intComboID +1));
					PrintMultiArray(@$arrIndicValues[$intComboID +1]);
				}
			}
			//@JAPR
			
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
					if ($gbEnableIncrementalAggregations) {
						if (isset($arrRowIndicValues)) {
							unset($arrIndicValues[$intComboID]);
						}
						unset($arrDimKeyValues[$intComboID]);
						unset($arrDimDescValues[$intComboID]);
						unset($arrDateValues[$intComboID]);
					}
					//@JAPR
					return("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
			if ($gbEnableIncrementalAggregations) {
				//Se debe incrementar 2 veces, porque cada actualización primero tiene que cancelar el valor previo con la combinación anterior, y 
				//posteriormente debe insertarse ya con la nueva combinación y los nuevos valores
				$intComboID += 2;
			}
			//@JAPR
		}
		
		//Si hay preguntas matrix, ya se guardaron en la tabla de hechos
        //ahora hay que guardar tambien las respuestas en su tabla de matrix
        if(count($arrayMatrix) > 0) 
        {
            $statusFn = updateDataInMatrixTable($aRepository, $surveyInstance, $matrixCollection, $arrayData, $factKey, $factKeyDimVal);
            
            if($statusFn===false)
            {
            	return($gblEFormsErrorMessage);
            }
        }
        
        //Se procede a actualizar la tabla SVSurveyCatDimVal_<XXXX> con los valores de Category Dimension si es
        //que los hay
        $statusFn = updateDataInCatDimValTable($aRepository, $surveyInstance, $arrayData, $factKey, $factKeyDimVal);
        
        if($statusFn===false)
        {
        	return($gblEFormsErrorMessage);
        }

		//************************************************************************************************************************
		//************************************************************************************************************************
		//Actualiza los datos de la tabla paralela
		//************************************************************************************************************************
		//************************************************************************************************************************
		$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
		
		//Ajustar el valor de $currentDate para que incluya hora, minuto y segundo
		$currentDate = substr($currentDate, 0, 10)." ".substr($surveyHour, 0, 8);
		
		//Antes de realizar el recorrido de las preguntas eliminamos todas las acciones 
		//de dicha encuesta donde SurveyDimVal = EntryId, ya que probablemente pudieron
		//haber seleccionado otras opciones
		$sql = "DELETE FROM SVSurveyActions WHERE SurveyID = ".$surveyInstance->SurveyID." AND SurveyDimVal = ".$entryID;
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			/*if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				return("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}*/
		}
		
		//LastDateID, LastHourID, LastStartTime, LastEndTime
		//@JAPR 2012-11-23: Corregidas las fechas de última edición, estaban tomando la misma del archivo pero esa se mantenía como la fecha
		//original de la captura, así que no reflejaba la fecha de última edición, para modo Web en realmente la fecha de sincronización
		$strLastDateID = $currentDate;
		$strLastHourID = $surveyHour;
		if ($blnWebMode) {
			//@JAPR 2012-11-23: Corregida la fecha de inicio de actualización
			if ($strLastUpdateDate != '' && $strLastUpdateTime != '') {
				$strLastDateID = $strLastUpdateDate;
				$strLastHourID = $strLastUpdateTime;
				$strLastDateID = substr($strLastDateID, 0, 10)." ".substr($strLastHourID, 0, 8);
			}
			//@JAPR 2012-11-24: Corregida la fecha de fin de actualización
			if ($strLastUpdateEndDate != '' && $strLastUpdateEndTime != '') {
				$endTime = $strLastUpdateEndTime;
			}
		}
		$sql = "UPDATE ".$tableName." SET ";
		$sql.="LastDateID = ".$aRepository->DataADOConnection->DBTimeStamp($strLastDateID).", ";
		$sql.="LastHourID = ".$aRepository->DataADOConnection->Quote($strLastHourID).", ";
		$sql.="LastStartTime = ".$aRepository->DataADOConnection->Quote($strLastHourID).", ";
		$sql.="LastEndTime = ".$aRepository->DataADOConnection->Quote($endTime).",";
		//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
		//Si existen los campos, se graba la referencia a la sección o pregunta especial que generan este registro para porder identificarlo
		if ($useSourceFields) {
			$sql .= "EntrySectionID = ".(int)$anEntrySectionID.", EntryCatDimID = ".(int)$anEntryCatDimID.",";
		}
		//@JAPR 2013-01-30: Corregido un bug, no estaba grabando este valor al actualizar los datos
		if ($useDynamicPageField) {
			$sql .= "DynamicPageDSC = ".$aRepository->DataADOConnection->Quote($aDynamicPageDSC).",";
		}
		//@JAPR

		//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$blnDynamicProcessed = false;
		foreach ($questionCollection->Collection as $questionKey => $aQuestion)
		{
			//@JAPR 2012-05-17: Agregados nuevos tipos de pregunta Foto y Firma
			//Las preguntas de Foto y Firma no graban una respuesta, sólo la imagen
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($aQuestion->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSketch:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
					$blnValidQuestion = false;
					break;
			}
			
			if (!$blnValidQuestion)
			{
				//No se debe hacer el Continue o se salta el grabado de la foto y comentarios
				$insertValue = "";
				//continue;
			}
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			else if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//Obtener valor capturado de dicha question
				$postKey = "qfield".$aQuestion->QuestionID;
				//@JAPR 2013-01-22: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				if(isset($arrayData[$postKey]) && trim($arrayData[$postKey]) !== '')
				{
					$insertValue = (float)$arrayData[$postKey];
				}
				else 
				{
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					$insertValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
				}
				
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$sql.=$aQuestion->SurveyField." = ".StrIFNULL($insertValue);
	
				if($questionKey != (count($questionCollection->Collection)-1))
				{
					$sql.=",";
				}
			}
			else 
			{
				//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
				//Las preguntas tipo Action contienen un String con separador _SVSep_ y se grabará así porque usar un separador mas sencillo podría
				//causar que en cambios futuros se confunda con parte de la propia acción, la cual es un Texto libre
				//@JAPR 2012-09-25: Agregado el tipo de pregunta Hora
				//@AAL 06/05/2015: Modificado para soportar preguntas tipo código de barras
				if($aQuestion->QTypeID==qtpOpenDate || $aQuestion->QTypeID==qtpOpenString || $aQuestion->QTypeID==qtpOpenAlpha || $aQuestion->QTypeID == qtpAction || $aQuestion->QTypeID==qtpOpenTime || $aQuestion->QTypeID==qtpBarCode)
				{
					//Obtener valor capturado de dicha question Date o TextBox
					$postKey = "qfield".$aQuestion->QuestionID;
					
					if(isset($arrayData[$postKey]))
					{
						//@JAPR 2012-11-27: Corregido un bug, no debe hacer un trim a los valores grabados
						$insertValue = $arrayData[$postKey];
					}
					else 
					{
						$insertValue = "";
					}
	
					$sql.=$aQuestion->SurveyField." = ".$aRepository->DataADOConnection->Quote($insertValue);
	
					if($questionKey != (count($questionCollection->Collection)-1))
					{
						$sql.=",";
					}
				}
				else 
				{
					if(($aQuestion->QTypeID==qtpSingle || $aQuestion->QTypeID==qtpCallList) && $aQuestion->UseCatalog==0)
					{
					//Conchita agregado callList
						//Obtener valor capturado de dicha question Seleccion Simple
						$postKey = "qfield".$aQuestion->QuestionID;
						
						if(isset($arrayData[$postKey]))
						{
							//@JAPR 2012-11-27: Corregido un bug, no debe hacer un trim a los valores grabados
							$insertValue = $arrayData[$postKey];
						}
						else 
						{
							$insertValue = "";
						}

						$sql.=$aQuestion->SurveyField." = ".$aRepository->DataADOConnection->Quote($insertValue);
			
						if($questionKey != (count($questionCollection->Collection)-1))
						{
							$sql.=",";
						}
						
						$arrayOptions = array();
						$arrayOptions[] = $insertValue;
						
						//Si dicha opcion trae una accion, entonces se procede a insertarla
						$statusFn = insertSurveyAction($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $arrayOptions);
						
						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
					else if($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1)
					{
						//Obtener valor capturado de dicha question Seleccion Simple
						$postKey = "qfield".$aQuestion->QuestionID;
						
						if(isset($arrayData[$postKey]))
						{
							//@JAPR 2012-11-27: Corregido un bug, no debe hacer un trim a los valores grabados
							$strValue = $arrayData[$postKey];
						}
						else 
						{
							$strValue = "";
						}
						
						//En el campo SurveyField original se sigue grabando el key subrrogado de la combinación de la tabla del catálogo que
						//hace match con los valores recibidos, si no se encuentra una combinación válida se graba NA
						$blnSkipCatDimen = false;
						if($hasDynamicSection==true && $aQuestion->CatalogID==$dynamicSectionCatID)
						{
							$arrayMapStrValues = array();
							$arrayMapStrValues[0] = $arrayData['sectionValue'];
							$insertValue = $arrayMapStrValues[0];
							//@JAPR 2013-02-13: Agregada la edición de datos históricos
							//Si el valor regresado por el sectionValue es negativo, quiere decir
							//que es un valor que posiblemente ya no se encontraba en el catálogo así que no hay necesidad de cambiar nada pues
							//las dimensiones tal como estaban ya apuntan al valor correcto. Esto solo aplica con catálogos desasociados
							if ((int) @$insertValue < 0 && $surveyInstance->DissociateCatDimens) {
								$blnSkipCatDimen = true;
								//Si además está procesandose un registro de la sección dinámica, como esta es una pregunta de catálogo del mismo
								//que dicha sección, se tiene que marcar la sección como ya procesada para que no se limpien los valores de los
								//campos que permiten identificar al registro dinámico, ya que no entrará al If que generalmente los hubiera
								//actualizado
								if ($anEntrySectionID == (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty)) {
									$blnDynamicProcessed = true;
								}
							}
							//@JAPR
						}
						else 
						{
							$arrayStrValues = array();
							$arrayStrValues[0] = $strValue;
							$arrayPassValues = getCatValueKeys($aRepository, $aQuestion, $arrayStrValues);
							//@JAPR 2013-02-12: Agregada la edición de datos históricos
							//Si se hubiera regresado el valor de NA pero además la descripción fuera la clave especial NOCHANGE, quiere decir
							//que es un valor que posiblemente ya no se encontraba en el catálogo así que no hay necesidad de cambiar nada pues
							//las dimensiones tal como estaban ya apuntan al valor correcto. Esto solo aplica con catálogos desasociados
							if ((int) @$arrayPassValues[0] == 1 && $surveyInstance->DissociateCatDimens && trim($strValue)=="NOCHANGE") {
								/*El valor en si del campo de la pregunta simple choice de catálogo lo cambiamos a -1 por 3 factores:
								1- Es una edición así que pudiera haber cambiado el catálogo (aunque no necesariamente es cierto, tal vez
									sigue existiendo el registro en el catálogo y se pudo haber regresado el subrrogado correspondiente)
								2- No importa ya este campo para hacer joins en ninguna consulta, ya que se están usando catálogos desasociados
									de las dimensiones
								3- El armado del UPDATE esperaba siempre actualizar algo y concatenaba "," al final de cada campo, así que se
									dejó un valor negativo para no tener que consultar el valor actual de esta pregunta en este registro (que según
									el punto No. 1 como sea ya no necesariamente sería correcto) y para no alterar el armado del UPDATE
								*/
								$arrayPassValues = array(-1);
								$blnSkipCatDimen = true;
							}
							//@JAPR
							
							if($arrayPassValues!==false)
							{
								$arrayMapStrValues = $arrayPassValues;
							}
							else 
							{
								return($gblEFormsErrorMessage);
							}
							
							$insertValue = $arrayMapStrValues[0];
						}
						
						$sql.=$aQuestion->SurveyField." = ".$aRepository->DataADOConnection->Quote($insertValue);
						
						//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
						//@JAPR 2013-02-12: Agregada la edición de datos históricos
						//Si no se recibió un valor diferente al anteriormente grabado, entonces no hay necesidad de sobreescribir estos campos
						//ya que ellos son lo único que permite reconstruir la edición cuando el catálogo ha cambiado
						if ($surveyInstance->DissociateCatDimens && !$blnSkipCatDimen) {
							//Si se graban catálogos desasociados entonces existe un campo SurveyCatField donde se graba el valor completo de las
							//combinaciónes recibidas tal como viene del App, además se debe verificar si se está o no grabando un registro para
							//la sección dinámica, ya que de ser así se tienen que grabar campos adicionales que incluirán además los valores
							//hasta el atributo ya sea de las páginas dinámicas o de los checkboxes que contienen, así como la descripción de
							//cada checkbox al que corresponde el registro
							if($aQuestion->CatalogID==$dynamicSectionCatID) {
								//La pregunta corresponde al mismo catálogo de la sección dinámica, así que hay que verificar si se está o no
								//grabando un registro para dicha sección si es así entonces se graban además los campos DynamicOptionDSC y 
								//DynamicValue con el valor del atributo que genera a las opciones múltiples para este registro específico (o vacio
								//si no hay múltiples) y con el valor completo de todos los atributos hasta el de la página/checkbox, el resto de los 
								//atributos existentes en el modelo contendrían el valor de NA.
								//Al día en que se desarrolló esta funcionalidad no se permitían preguntas de catálogo posteriores a la sección
								//dinámica (ni dentro de ella), ya que en ese caso se tendrían que filtrar por alguna de las páginas de la sección
								//y no se sabría por cual
								if (!$blnDynamicProcessed && $hasDynamicSection && ($anEntrySectionID == (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty))) {
									//Si es un registro de la dinámica así que se graban los campos según el atributo de su página/checkbox
									$blnDynamicProcessed = true;
									//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
									//Si se graba a partir de las descripciones, no puede ir a traer el String de la combinación ni la descripción
									//del checkbox al catálogo porque el valor probablemente pudo haber cambiado, y aunque para este punto si fuera un
									//key > 0 ya se habría remapeado, aquellos que fueron eliminados grabarían el valor de *NA así que mejor se usará
									//directamente el dato que llegó desde el App en esos casos
									$strDynamicOptionDSC = '';
									$strDynamicValue = '';
									$arrSectionDesc = @$arrSectionDescByID[$anEntrySectionID][$aDynamicRecNum];
									$strNewCatDimValAnswers = @$arrSectionDescStrByID[$anEntrySectionID][$aDynamicRecNum];
									if (!is_null($strNewCatDimValAnswers) && !is_null($arrSectionDesc) && is_array($arrSectionDesc) && count($arrSectionDesc) > 0) {
										//@JAPR 2013-05-09: Corregido un bug con la codificación
										//Este valor SI se debe decodificar, porque se usará para grabar en las tablas de datos
										//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
										$strDynamicOptionDSC = (string) @$arrSectionDesc[count($arrSectionDesc) -1];
										$strDynamicValue = BITAMCatalog::TranslateSCHValueToDissociatedDimensions($aRepository, $aQuestion->CatalogID, (string) $strNewCatDimValAnswers);
									}
									else {
										$arrDynamicRecValue = getCatValueDescForSurveyField($aRepository, $arrayMapStrValues, $dynamicSectionCatID);
										if (!is_array($arrDynamicRecValue) || count($arrDynamicRecValue) < 2) {
											$arrDynamicRecValue = array();
										}
										$strDynamicOptionDSC = (string) @$arrDynamicRecValue[1];
										$strDynamicValue = (string) @$arrDynamicRecValue[0];
									}
									
									$sql .= ", DynamicOptionDSC = ".$aRepository->DataADOConnection->Quote($strDynamicOptionDSC).
										", DynamicValue = ".$aRepository->DataADOConnection->Quote($strDynamicValue);
									//@JAPR
								}
							}
							
							//Obtiene el String tal como el que venía del App pero esta vez usando los atributos de las dimensiones independientes
							//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
							//Si se utilizan las descripciones para grabar, el valor de la pregunta simple choice de catálogo ya viene en el mismo
							//formato que se necesita pero con los ids de los atributos de la dimensión en lugar de los de las dimensiones
							//independientes, por lo tanto se ejecuta un proceso que remapea dichos ids pero respetando lo demás
							if (trim($strValue) != '') {
								$strCatalogValue = BITAMCatalog::TranslateSCHValueToDissociatedDimensions($aRepository, $aQuestion->CatalogID, $strValue);
							}
							else {
								$arrDynamicRecValue = getCatValueDescForSurveyField($aRepository, $arrayMapStrValues, $aQuestion->CatalogID, $aQuestion->CatMemberID);
								if (!is_array($arrDynamicRecValue) || count($arrDynamicRecValue) < 2) {
									$arrDynamicRecValue = array();
								}
								$strCatalogValue = (string) @$arrDynamicRecValue[0];
							}
							//@JAPR
							$sql.=",".$aQuestion->SurveyCatField." = ".$aRepository->DataADOConnection->Quote($strCatalogValue);
						}
						//@JAPR
						
						if($questionKey != (count($questionCollection->Collection)-1))
						{
							$sql.=",";
						}
					}
					else 
					{
						//Obtener valor capturado de dicha question Seleccion Multiple
						$postKey = "qfield".$aQuestion->QuestionID;
						
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						//@JAPR 2012-11-26: Corregido un bug, cuando no se grababa la sección dinámica pero esta de todas formas contenía una pregunta multiple-choice
						//tipo checkbox, el campo físicamente es un double, así que al no recibir un valor estaba intentando insertar un '' en lugar de un 0 y eso
						//generaba un error, así que se validará por la pregunta de sección dinámica incluso si no se recibió el sectionValue
						if(isset($arrayData[$postKey]) || ($surveyInstance->UseNULLForEmptyNumbers && is_null(@$arrayData[$postKey]) && ($dynamicSectionID==$aQuestion->SectionID || $dynamicSectionIDEmpty==$aQuestion->SectionID) && ($aQuestion->MCInputType == mpcCheckBox || $aQuestion->MCInputType == mpcNumeric)))
						{
							//Se usó @ para forzar a null si no estaba asignado el valor y cumple la condición indicada arriba, ya que en ese
							//caso intencionalmente se desea grabar NULL pues se trata de una valor numérico que no debe ser 0 para no afectar a
							//los cálculos que lo usen
							$strValues = @$arrayData[$postKey];
						}
						else 
						{
							$strValues = "";
						}

						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						if(trim($strValues)=="")
						{
							if ($surveyInstance->UseNULLForEmptyNumbers && ($dynamicSectionID==$aQuestion->SectionID || $dynamicSectionIDEmpty==$aQuestion->SectionID) && ($aQuestion->MCInputType == mpcCheckBox || $aQuestion->MCInputType == mpcNumeric)) {
								//En este caso se desea que se grabe el NULL, por lo que no sobreescribe
								if ($aQuestion->MCInputType == mpcNumeric) {
									$insertValue = null;
								}
								else {
									$insertValue = "0";
								}
							}
							else {
								$insertValue = "";
							}
						}
						else 
						{
							//Para las preguntas de Category Dimension no se queria grabar sobre la tabla SVSurvey_<ModelID>
							//Pero grabaremos la informacion para ver si esta nos sirve para efectos de despliegue del reporte
							if($dynamicSectionID!=$aQuestion->SectionID && $aQuestion->QDisplayMode==dspVertical && $aQuestion->MCInputType==mpcNumeric && $aQuestion->IsMultiDimension==0 && $aQuestion->UseCategoryDimChoice!=0)
							{
								//$insertValue = "";
								$arrayValues = explode("_SVSep_", $arrayData[$postKey]);
								$insertValue = implode(";", $arrayValues);
							}
							else 
							{
								$arrayValues = explode("_SVSep_", $arrayData[$postKey]);
								$insertValue = implode(";", $arrayValues);
							}
						}

						//Corregir caso de los multiple choice que capturan cantidades
						//dentro de las secciones dinamicas
						if(($dynamicSectionID==$aQuestion->SectionID || $dynamicSectionIDEmpty==$aQuestion->SectionID) && $aQuestion->MCInputType==mpcNumeric && $aQuestion->IsIndicatorMC==1)
						{
							//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
							//$insertValue = (float)$insertValue;
							$insertValue = StrIFNULL($insertValue);
							$sql.=$aQuestion->SurveyField." = ".$insertValue;
						}
						else 
						{
							//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
							$sql.=$aQuestion->SurveyField." = ". (is_null($insertValue)?'NULL':$aRepository->DataADOConnection->Quote($insertValue));
						}

						if($questionKey != (count($questionCollection->Collection)-1))
						{
							$sql.=",";
						}
						
						//Verificamos si la pregunta de seleccion multiple no pertenece a seccion dinamica y que sea de tipo checkbox
						if(($dynamicSectionID!=$aQuestion->SectionID && $dynamicSectionIDEmpty!=$aQuestion->SectionID) && $aQuestion->UseCatToFillMC==0 && $aQuestion->MCInputType==mpcCheckBox)
						{
							//Si dicha opcion trae una accion, entonces se procede a insertarla
							$statusFn = insertSurveyAction($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $arrayValues);
							
							if($statusFn===false)
							{
								return($gblEFormsErrorMessage);
							}
						}
					}
				}
			}
			
			//Grabar la imagen o foto
			if($aQuestion->HasReqPhoto==1 || $aQuestion->HasReqPhoto==2)
			{
				$postKey = "qimage".$aQuestion->QuestionID;
				
				if(isset($arrayData[$postKey]))
				{
					$insertImage = $arrayData[$postKey];
					
					if(!is_null($insertImage) && $insertImage!="")
					{
						$statusFn = savePhoto($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertImage);
						
						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
					else 
					{
						//Inserta el valor de No Aplica
						$statusFn  = savePhotoNA($aRepository, $surveyInstance, $aQuestion, $factKey);
						
						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
				}
				else 
				{
					$postKey = "qimageencode".$aQuestion->QuestionID;
					
					if(isset($arrayData[$postKey]))
					{
						if($arrayData[$postKey] != "qimageencodedyn") 
						{
							//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
							if ($usePhotoPaths) {
							//Conchita 2015-01-14 faltaba validar en web para cuando viene como ruta (nuevo) o cuando viene base 64 (versiones antiguas)
								//En este caso es un nombre de archivo, así que no decodifica nada
								//
								$insertImage = $arrayData[$postKey];
								$blnIsPhotoName = true;
								//si es web mode
								if($blnWebMode){
									if(strpos($insertImage, "tmpsurveyimages")>=0){ //si contiene tmpsurveyimages
										//es modo web y usa la ruta de la imagen
										 if ($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch) {
                                               $blnIsPhotoName = false;
                                               if (strpos($insertImage, "photo") || strpos($insertImage, "canva")) {
											   //si es canvas o photo se pone en true, si no, ya estaba en false
                                                     $blnIsPhotoName = true;
                                               }
										}
									}
									else {
										//si no contiene la ruta debe de ser un base 64
										$blnIsPhotoName = false;
									}
								}
								else { //es movil (se deja como estaba)
								if ($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch) {
									$blnIsPhotoName = false;
									if (substr($insertImage, 0, 5) == "photo" || substr($insertImage, 0, 5) == "canva" ) {
										$blnIsPhotoName = true;
									}
								}
								}
								if (!$blnIsPhotoName) {
									$insertImage = base64_decode($arrayData[$postKey]);
								}
								//
							}
							else {
								$insertImage = base64_decode($arrayData[$postKey]);
							}
							//@JAPR
						} 
						else 
						{
							$insertImage = $arrayData[$postKey];
						}
					
						if(!is_null($insertImage) && $insertImage!="")
						{
							$statusFn = savePhoto($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertImage);
	
							if($statusFn===false)
							{
								return($gblEFormsErrorMessage);
							}
						}
						else 
						{
							//Inserta el valor de No Aplica
							$statusFn = savePhotoNA($aRepository, $surveyInstance, $aQuestion, $factKey);
	
							if($statusFn===false)
							{
								return($gblEFormsErrorMessage);
							}
						}
					}
					else 
					{
						//Inserta el valor de No Aplica
						$statusFn = savePhotoNA($aRepository, $surveyInstance, $aQuestion, $factKey);
						
						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
				}
	            
	            //@MABH 2012-05-24: Para el soporte de fotos (en web), se van a enviar
	            //las imagenes en input type="file" con id que inicien en photoimage
	            //no como qimageencode ya que desde web no se envia un base64
	            $postKey = "photoimage".$aQuestion->QuestionID;
	            if(isset($_FILES[$postKey])) {
	                $insertImage = "photoimage";
	                $statusFn = savePhoto($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertImage);
	                if($statusFn===false) {
	                    return($gblEFormsErrorMessage);
	                }
	            }
	            
			}
			else 
			{
				//Verificamos si de pura casualidad tiene dicha dimension entonces
				//se trata de insertar el valor de No Aplica
				//$statusFn = savePhotoNA($aRepository, $surveyInstance, $aQuestion, $factKey);
				/*
				if($statusFn===false)
				{
					return($gblEFormsErrorMessage);
				}
				*/
			}
			
			//Grabar el documento
			if($aQuestion->HasReqDocument==1 || $aQuestion->HasReqDocument==2)
			{
				$postKey = "qdocument".$aQuestion->QuestionID;
				
				if(isset($arrayData[$postKey]))
				{
					$insertDocument = $arrayData[$postKey];
					
					if(!is_null($insertDocument) && $insertDocument!="")
					{
						$statusFn = saveDocument($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertDocument);
						
						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
					else 
					{
						//Inserta el valor de No Aplica
						$statusFn  = saveDocumentNA($aRepository, $surveyInstance, $aQuestion, $factKey);
						
						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
				}	            
			}
			
			//Grabar comentario
			if($aQuestion->HasReqComment==1 || $aQuestion->HasReqComment==2)
			{
				$postKey = "qcomment".$aQuestion->QuestionID;
				
				if(isset($arrayData[$postKey]))
				{
					$insertComment = $arrayData[$postKey];
					
					if(!is_null($insertComment))
					{
						$statusFn = updateComment($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertComment);
						
						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
				}
			}
		}
		
		//@JAPR 2012
		
		if (substr(trim($sql), -1, 1) == ',') {
			$sql = substr(trim($sql), 0, -1);
		}
		$sql.=" WHERE FactKey = ".$factKey." AND FactKeyDimVal = ".$factKeyDimVal;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nupdateDataInSurveyTable UPDATE: ".$sql);
		}
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				return("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		else {
			//Graba los campos de secciones dinámicas que no fueron procesados si este registro no pertenecía a ella
			if (!$blnDynamicProcessed && $surveyInstance->DissociateCatDimens) {
				$sql = "UPDATE ".$tableName." SET DynamicOptionDSC = ".$aRepository->DataADOConnection->Quote('').
					", DynamicValue = ".$aRepository->DataADOConnection->Quote('').
					" WHERE FactKeyDimVal = ".$factKeyDimVal." AND FactKey = ".$factKey;
				$aRepository->DataADOConnection->Execute($sql);
			}
		}
		//@JAPR
		
		//En esta parte validamos si se trata del repositorio de Bellisima para realizar unos UPDATES que se requieren
		//para cierta dimension de la encuesta que estan manejando
		if(trim($_SESSION["PABITAM_RepositoryName"])=="fbm_bmd_0211" && $surveyInstance->SurveyID==1) 
		{
			//Actualizar tabla de hechos
			$sqlFactTable = "UPDATE RIFACT_29006 A, RIDIM_29024 B, RIDIM_29037 C 
							SET A.RIDIM_29037KEY = C.RIDIM_29037KEY 
							WHERE A.RIDIM_29024KEY = B.RIDIM_29024KEY AND B.DSC_29024 = C.DSC_29043";
			
			if($aRepository->DataADOConnection->Execute($sqlFactTable) === false)
			{
				if($gblShowErrorSurvey)
				{
					//die("(".__METHOD__.") ".translate("Error accessing")." RIFACT_29006 ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlFactTable);
				}
				else 
				{
					//return ("(".__METHOD__.") ".translate("Error accessing")." RIFACT_29006 ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlFactTable);
				}
			}
			
			//Actualizar tabla paralela
			$sqlSVTable = "UPDATE RIFACT_29006 A, RIDIM_29024 B, RIDIM_29037 C, SVSurvey_29006 D 
							SET D.dim_52 = C.RIDIM_29037KEY 
							WHERE A.RIDIM_29024KEY = B.RIDIM_29024KEY AND B.DSC_29024 = C.DSC_29043 AND A.FactKey = D.FactKey";
			
			if($aRepository->DataADOConnection->Execute($sqlSVTable) === false)
			{
				if($gblShowErrorSurvey)
				{
					//die("(".__METHOD__.") ".translate("Error accessing")." SVSurvey_29006 ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlSVTable);
				}
				else 
				{
					//return ("(".__METHOD__.") ".translate("Error accessing")." SVSurvey_29006 ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlSVTable);
				}
			}
		}
		
		//termina de actualizarse la tabla paralela
		//ahora actualizamos la tabla global
		$strResult = updateDataInGlobalSurveyModel($aRepository, $surveyInstance, $arrayData, $thisDate, $factKey, $factKeyDimVal);
		
		//Procedemos a grabar la firma si es que existe
		if($surveyInstance->HasSignature==1)
		{
			$postKey = "qsignature";
			
			if(isset($arrayData[$postKey]))
			{
				$insertSignature = $arrayData[$postKey];
				if ($usePhotoPaths ) {
					$blnIsPhotoName = false;
					if (substr($insertSignature, 0, 9) == "signature") {
						$blnIsPhotoName = true;
					}
					if (!$blnIsPhotoName) {
						$insertSignature = base64_decode($arrayData[$postKey]);
					}
				}
				else {
					$insertSignature = base64_decode($arrayData[$postKey]);
				}
				
				if(!is_null($insertSignature) && $insertSignature!="")
				{
					$statusFn = saveSignature($aRepository, $surveyInstance, $factKey, $insertSignature);
					
					if($statusFn===false)
					{
						//return($gblEFormsErrorMessage);
					}
				}
			}
		}
		
		return($strResult);
	}
	else 
	{
		//@JAPR 2012-01-24: Corregido un bug, se había enviado al SaveData en pruebas pero no se había habilitado realmente, sólo si se enviaba
		//el parámetro de depuración
		//return "Error cannot be updated the survey because it doesn't exist";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nError: The survey cannot be updated because it doesn't exist, adding a new record instead");
			//$strResult = saveData($aRepository, $surveyID, $arrayData, $thisDate, $srcFactKeyDimVal, false, 0, null, $dynamicSectionID);
			//return($strResult);
		}
		
		//@JAPR 2014-10-29: Agregado el mapeo de Scores
		//Agregado el parámetro $aMultiRecordNum para indicar el registro de cada sección que se esté procesando
		$strResult = saveData($aRepository, $surveyID, $arrayData, $thisDate, $srcFactKeyDimVal, false, 0, null, $anEntrySectionID, $anEntryCatDimID, $aDynamicPageDSC, $bSaveSignature, $aDynamicRecNum, $aDynamicRecNum);
		return($strResult);
	}
}

//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
//@JAPR 2013-01-22: Agregado el parámetro $bSaveSignature para optimizar en que momento se graba la firma
//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
//Agregado el parámetro $aDynamicRecNum para indicar el número de registro de la sección dinámica del que se trata. Originalmente fué usado solo
//para secciones dinámicas, pero se pudiera aplicar en conjunto con $anEntrySectionID para usarlo también en secciones Maestro-detalle. Si se
//envía un -1 quiere decir que no es un registro de una sección múltiple (es el índice empezando en 0 tal como lo maneja el App de v4)
/*
//@JAPR 2014-10-29: Agregado el mapeo de Scores
//Agregado el parámetro $aMultiRecordNum para indicar el registro de cada sección que se esté procesando. Va de la mano con $anEntrySectionID y
$anEntryCarDimID y no aplica para secciones estándar en las cuales debería llegar el valor 0. Se decidió agregar a pesar de ya contar con el
parámetro $aDynamicRecNum que es lo mismo en secciones Inline o dinámicas, porque precisamente para dichas secciones ya se usaba $aDynamicRecNum
para algunos procesos que específicamente aplican para ellas, mientras que $aMultiRecordNum se usará para procesos de reemplazos de variables o otros
que requieran entre otras cosas acceso al objeto JSon original de la captura
*/
function saveData($aRepository, $surveyID, $arrayData, $thisDate=null, &$factKeyDimVal=0, $sendEmailDashboards=true, $countSurveys=1, $folioID=null, $anEntrySectionID = 0, $anEntryCatDimID = 0, $aDynamicPageDSC = '', $bSaveSignature = true, $aDynamicRecNum = -1, $aMultiRecordNum = 0)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	global $arrProcessedActionQuestions;
	global $arrQuestionOptions;
	global $queriesLogFile;
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	global $syncDate;
	global $syncTime;
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	global $useSourceFields;
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	global $useRecVersionField;
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	global $useDynamicPageField;
	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	global $arrMasterDetSections;
	global $arrMasterDetSectionsIDs;
	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	global $arrInlineSectionsIDs;
	//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
	global $eBavelAppCodeName;
	global $arrActionItems;
	global $arrayPDF;
	//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
	global $arrEditedFactKeys;
	//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	global $dynamicSectionWithMultiChoice;
	global $arrDynamicCatalogAttributes;
	//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
	global $blnEBavelSupLoaded;
	global $arrEBavelSupervisors;
	//@JAPR 2013-05-06: Corregido un bug, la funcionalidad de Supervisores adicionales no funcionaba con Apps antes de esveBavelMapping, así que se
	//validará para que en esos casos se force a enviar IDs positivos que permitan usarla, y solo en las mas recientes envíe IDs negativos como debe ser
	global $blnUseMaxLongDummies;
	//@JAPR 2013-07-24: Agregado el mapeo de fotos durante la generación de acciones
	global $usePhotoPaths;
	//@JAPR
	/*@MABH20121205*/
	global $appVersion;
	//@MABH20121205
	//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
	global $intComboID;
	global $strIndicFieldsArrayCode;
	global $gbEnableIncrementalAggregations;
	global $gbTestIncrementalAggregationsData;
	global $arrDateValues;
	global $arrDimKeyValues;
	global $arrDimDescValues;
	global $arrIndicValues;
	global $arrDefDimValues;
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	global $arrAdditionalEMails;
	//@JAPR 2014-10-29: Agregado el mapeo de Scores
	global $aSectionEvalRecord;
	//@JAPR 2014-11-24: Agregadas variables para las secciones
	global $aSectionEvalPage;
	
	$aSectionEvalRecord = array();
	//Este array sólo se llena cuando se trata de una sección múlti-registro
	if ($anEntrySectionID > 0) {
		$aSectionEvalRecord[$anEntrySectionID] = $aMultiRecordNum;
	}
	//@JAPR 2014-11-24: Agregadas variables para las secciones
	$aSectionEvalPage = array();
	if ($anEntrySectionID > 0) {
		$aSectionEvalPage[$anEntrySectionID] = $aDynamicPageDSC;
	}
	//@JAPR
	
	$accuracy = 0;
	$arrRowDimValues = array();
	$arrRowDimDescValues = array();
	if ($gbEnableIncrementalAggregations) {
		if ($strIndicFieldsArrayCode != '') {
			@eval($strIndicFieldsArrayCode);
			if (isset($arrRowIndicValues)) {
				$arrIndicValues[$intComboID] =& $arrRowIndicValues;
			}
		}
		
		$arrDimKeyValues[$intComboID] =& $arrRowDimValues;
		$arrDimDescValues[$intComboID] =& $arrRowDimDescValues;
		$arrDateValues[$intComboID] = substr($arrayData["surveyDate"], 0, 10);
	}
	
	$arrQuestionDimValues = array();
	$arrQuestionDimValuesDesc = array();
	global $arrLastCatClaDescrip;	
	global $arrLastCatValues;
	$arrLastCatClaDescrip = array();
	$arrLastCatValues = array();
	$arrCatalogDimValuesByCatalog = array();
	//@JAPR
	
	//@JAPR 2013-08-14: Corregido un bug, si se reconstruía la sección dinámica durante una edición, no se estaba guardando como
	//editado el FactKey recien agregado por lo que se terminaban perdiendo todos los datos dinámicos nuevos, así que ahora se
	//agregan al array para conservarlos
	$blnEdit = (bool) getParamValue('edit', 'both', "(int)");
	$entryID = getParamValue('entryID', 'both', "(int)");
	//@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br><br>\r\n\r\nsaveData anEntrySectionID == {$anEntrySectionID}, anEntryCatDimID == {$anEntryCatDimID}, aDynamicPageDSC == {$aDynamicPageDSC}, aDynamicRecNum == {$aDynamicRecNum}");
		PrintMultiArray($arrayData);
	}
	
	if (is_null($syncDate) || trim((string) $syncDate) == '') {
		$syncDate = date("Y-m-d");
	}
	if (is_null($syncTime) || trim((string) $syncTime) == '') {
		$syncTime = date("H:i:s");
	}
	//@JAPR
	
	if(is_null($thisDate))
	{
		$thisDate = date("Y-m-d H:i:s");
	}
	
	$strOriginalWD = getcwd();

	require_once("survey.inc.php");
	require_once("question.inc.php");
	require_once("cubeClasses.php");
	require_once("dimension.inc.php");
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	require_once("../model_manager/indicatorkpi.inc.php");
	
	//Obtenemos los valores de CaptureVia, CaptureEmail y SchedulerID
	//para verificar si dichos valores los estan dando de alta un correo electronico
	$captureVia = 0;
	if(isset($arrayData["CaptureVia"]))
	{
		$captureVia = (int)$arrayData["CaptureVia"];
	}
	
	$captureEmail = "";
	if(isset($arrayData["CaptureEmail"]))
	{
		$captureEmail = $arrayData["CaptureEmail"];
	}
	
	$schedulerID = "";
	if(isset($arrayData["SchedulerID"]))
	{
		$schedulerID = (int)$arrayData["SchedulerID"];
	}
	
	//Se genera instancia de encuesta dado un SurveyID
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->ModelID);
	chdir($strOriginalWD);
	
	$statusKey = svstFinished;
	$tableDimStatus = "RIDIM_".$surveyInstance->StatusDimID;
	$fieldDimStatusKey = $tableDimStatus."KEY";
	$fieldDimStatusDesc = "DSC_".$surveyInstance->StatusDimID;
	
	//@JAPR 2014-11-19: Agregada la dimensión Agenda
	//Obtiene el key de la agenda que se capturó (o de la NA si no se capturó a partir de agenda)
	$agendaKey = null;
	$fieldDimAgendaKey = '';
	if (getMDVersion() >= esvAgendaDimID && $surveyInstance->AgendaDimID > 0) {
		$agendaKey = 1;
		$tableDimAgenda = "RIDIM_".$surveyInstance->AgendaDimID;
		$fieldDimAgendaKey = $tableDimAgenda."KEY";
		$anAgendaInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->AgendaDimID);
		if (!is_null($anAgendaInstanceDim)) {
			$strAgendaFilter = (string) @$arrDefDimValues['AgendaFilter'];
			if ($strAgendaFilter == '') {
				$strAgendaFilter = $gblEFormsNA;
			}
			
			$arrayValues = array();
			$arrayValues[] = $strAgendaFilter;
			$arrayPassValues = getInsertDimValueKey($aRepository, $anAgendaInstanceDim, $arrayValues);
			if ($arrayPassValues !== false && is_array($arrayPassValues) && isset($arrayPassValues[0])) {
				$agendaKey = (int) @$arrayPassValues[0];
				if ($agendaKey == 0) {
					$agendaKey = 1;
				}
			}
			else {
				$agendaKey = 1;
			}
		}
		else {
			//Asignando null se impedirá que genere un error de armado del INSERT mas adelante
			$agendaKey = null;
		}
	}
	//@JAPR
	
	$emailKey = 1;
	$tableDimEmail = "RIDIM_".$surveyInstance->EmailDimID;
	$fieldDimEMailKey = $tableDimEmail."KEY";
	
	$schedulerKey = 1;
	$schedulerDesc = '';
	$tableDimScheduler= "RIDIM_".$surveyInstance->SchedulerDimID;
	$fieldDimSchedulerKey = $tableDimScheduler."KEY";
	//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
	$fieldDimSchedulerDsc = "DSC_".$surveyInstance->SchedulerDimID;;
	
	/*Se agregaron las 4 dimensiones al cubo de la encuesta*/
	//Obtenemos los valores para las dimensiones de Start Date, End Date, Start Hour, End Hour

	$startDate = substr($arrayData["surveyDate"], 0, 10);
	$endDate = substr($thisDate, 0, 10);
	$startTime = $arrayData["surveyHour"];
	$endTime = substr($thisDate, 11);
	
	//Obtenemos el StartDateKey
	$aStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->StartDateDimID);
	$startDateKey = getDateTimeDimKey($aRepository, $aStartDateInstanceDim, $startDate);
	
	if($startDateKey===false)
	{
		return($gblEFormsErrorMessage);
	}
	
	//Obtenemos el EndDateKey
	$anEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->EndDateDimID);
	$endDateKey = getDateTimeDimKey($aRepository, $anEndDateInstanceDim, $endDate);
	
	if($endDateKey===false)
	{
		return($gblEFormsErrorMessage);
	}

	//Obtenemos el StartTimeKey
	$aStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->StartTimeDimID);
	$startTimeKey = getDateTimeDimKey($aRepository, $aStartTimeInstanceDim, $startTime);
	
	if($startTimeKey===false)
	{
		return($gblEFormsErrorMessage);
	}

	//Obtenemos el EndTimeKey
	$anEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->EndTimeDimID);
	$endTimeKey = getDateTimeDimKey($aRepository, $anEndTimeInstanceDim, $endTime);
	
	if($endTimeKey===false)
	{
		return($gblEFormsErrorMessage);
	}
	
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	//Obtenemos el SyncDateKey
	$syncDateKey = '';
	$syncDateDimField = '';
	if ($surveyInstance->SyncDateDimID > 0) {
		$aSyncDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SyncDateDimID);
		$syncDateKey = ', '.getDateTimeDimKey($aRepository, $aSyncDateInstanceDim, $syncDate);
		$syncDateDimField = ', '.$aSyncDateInstanceDim->Dimension->TableName."KEY";
	}
	//No cancela el grabado por faltar esta dimensión ya que fué un cambio en las versiones finales de la v3, simplemente continua con el
	//grabado original
	/*
	if($syncDateKey===false)
	{
		return($gblEFormsErrorMessage);
	}
	*/
	
	//Obtenemos el SyncTimeKey
	$syncTimeKey = '';
	$syncTimeDimField = '';
	if ($surveyInstance->SyncTimeDimID > 0) {
		$aSyncTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SyncTimeDimID);
		$syncTimeKey = ', '.getDateTimeDimKey($aRepository, $aSyncTimeInstanceDim, $syncTime);
		$syncTimeDimField = ', '.$aSyncTimeInstanceDim->Dimension->TableName."KEY";
	}
	chdir($strOriginalWD);
	//No cancela el grabado por faltar esta dimensión ya que fué un cambio en las versiones finales de la v3, simplemente continua con el
	//grabado original
	/*
	if($syncTimeKey===false)
	{
		return($gblEFormsErrorMessage);
	}
	*/
	//@JAPR
	
	//Nombre de los campos en la tabla de hechos de las dimensiones StartDate, EndDate, StartTime, EndTime
	$startDateDimField = $aStartDateInstanceDim->Dimension->TableName."KEY";
	$endDateDimField = $anEndDateInstanceDim->Dimension->TableName."KEY";
	$startTimeDimField = $aStartTimeInstanceDim->Dimension->TableName."KEY";
	$endTimeDimField = $anEndTimeInstanceDim->Dimension->TableName."KEY";
	
	/*@MABH20121205*/
	/*Se agregaron las 4 dimensiones al cubo de la encuesta*/
	//Obtenemos los valores para las dimensiones de Start Date, End Date, Start Hour, End Hour
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$serverStartDate = substr((string) @$arrayData["serverSurveyDate"], 0, 10);
		$serverEndDate = substr((string) @$arrayData["serverSurveyEndDate"], 0, 10);
		$serverStartTime = (string) @$arrayData["serverSurveyHour"];
		$serverEndTime = (string) @$arrayData["serverSurveyEndHour"];
		
		//Obtenemos el StartDateKey
		$aServerStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerStartDateDimID);
		$serverStartDateKey = getDateTimeDimKey($aRepository, $aServerStartDateInstanceDim, $serverStartDate);
		
		if($serverStartDateKey===false)
		{
			return($gblEFormsErrorMessage);
		}
		
		//Obtenemos el EndDateKey
		$aServerEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerEndDateDimID);
		$serverEndDateKey = getDateTimeDimKey($aRepository, $aServerEndDateInstanceDim, $serverEndDate);
		
		if($serverEndDateKey===false)
		{
			return($gblEFormsErrorMessage);
		}

		//Obtenemos el StartTimeKey
		$aServerStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerStartTimeDimID);
		$serverStartTimeKey = getDateTimeDimKey($aRepository, $aServerStartTimeInstanceDim, $serverStartTime);
		
		if($serverStartTimeKey===false)
		{
			return($gblEFormsErrorMessage);
		}

		//Obtenemos el EndTimeKey
		$aServerEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerEndTimeDimID);
		$serverEndTimeKey = getDateTimeDimKey($aRepository, $aServerEndTimeInstanceDim, $serverEndTime);
		
		if($serverEndTimeKey===false)
		{
			return($gblEFormsErrorMessage);
		}
		
		//Nombre de los campos en la tabla de hechos de las dimensiones StartDate, EndDate, StartTime, EndTime
		$serverStartDateDimField = $aServerStartDateInstanceDim->Dimension->TableName."KEY";
		$serverEndDateDimField = $aServerEndDateInstanceDim->Dimension->TableName."KEY";
		$serverStartTimeDimField = $aServerStartTimeInstanceDim->Dimension->TableName."KEY";
		$serverEndTimeDimField = $aServerEndTimeInstanceDim->Dimension->TableName."KEY";
	}
	//@MABH20121205

	if($captureVia==1 && trim($captureEmail)!="" && $schedulerID>0)
	{
		//Obtiene la llave subrrogada del EMail en el catálogo correspondiente. Este tipo de capturas no tienen asociado un Usuario pero si
		//un Scheduler
		$fieldDimEMailDsc = "DSC_".$surveyInstance->EmailDimID;
		$sqlEmail = "SELECT ".$fieldDimEMailKey." FROM ".$tableDimEmail." WHERE ".$fieldDimEMailDsc." = ".$aRepository->DataADOConnection->Quote($captureEmail);
		$aRS = $aRepository->DataADOConnection->Execute($sqlEmail);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimEmail." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlEmail);
			}
			else 
			{
				return("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimEmail." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlEmail);
			}
		}
		
		if(!$aRS->EOF)
		{
			$emailKey = (int)$aRS->fields[strtolower($fieldDimEMailKey)];
		}
		
		$fieldDimSchedulerClave = "KEY_".$surveyInstance->SchedulerDimID;
		//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
		$sqlScheduler = "SELECT ".$fieldDimSchedulerKey.', '.$fieldDimSchedulerDsc." FROM ".$tableDimScheduler." WHERE ".$fieldDimSchedulerClave." = ".$schedulerID;
		$aRS = $aRepository->DataADOConnection->Execute($sqlScheduler);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimScheduler." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlScheduler);
			}
			else 
			{
				return("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimScheduler." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlScheduler);
			}
		}
		
		if(!$aRS->EOF)
		{
			$schedulerKey = (int)$aRS->fields[strtolower($fieldDimSchedulerKey)];
			//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
			$schedulerDesc = (string)@$aRS->fields[strtolower($fieldDimSchedulerDsc)];
		}
		
		$userID = -1;
	}
	else 
	{
		$userID = $arrayData["userID"];
		
		//@JAPR 2012-08-27: Agregado el grabado automático del Scheduler para las capturas desde móviles. Esto es un parche temporal ya que los
		//móviles realmente no están ligados a Schedulers, así que por ahora se obtendrá el MAX del SchedulerID y eso es lo que se grabará. Se
		//atrapará cualquier error para no detener el proceso de grabado
		
		//Primero obtiene el SchedulerID asociado con esta encuesta, se le dará prioridad a los Schedulers que NO son de captura vía EMail ya
		//que esos actualmente ya graban el Scheduler asociado, así que si se usara el mismo aquí no habría forma de diferenciar entre una
		//captura vía EMail o móvil. Se tomará el último Scheduler creado para esta encuesta
		$sqlScheduler = "SELECT A.CaptureVia, MAX(A.SchedulerID) AS MaxSchedulerID 
			FROM SI_SV_SurveyScheduler A 
			WHERE A.SurveyID = $surveyID 
			GROUP BY A.SurveyID, A.CaptureVia
			ORDER BY A.CaptureVia";
		$aRS = $aRepository->DataADOConnection->Execute($sqlScheduler);
		if ($aRS) {
			if(!$aRS->EOF)
			{
				//Como viene ordenado por CaptureVia, primero se usa el Scheduler normal (si lo hay) y al final el de EMail
				$schedulerID = (int) @$aRS->fields["maxschedulerid"];
			}
		}
		
		if ((int) $schedulerID > 0) {
			$fieldDimSchedulerClave = "KEY_".$surveyInstance->SchedulerDimID;
			//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
			$sqlScheduler = "SELECT ".$fieldDimSchedulerKey.', '.$fieldDimSchedulerDsc." FROM ".$tableDimScheduler." WHERE ".$fieldDimSchedulerClave." = ".$schedulerID;
			$aRS = $aRepository->DataADOConnection->Execute($sqlScheduler);
			if ($aRS)
			{
				if(!$aRS->EOF)
				{
					$schedulerKey = (int) @$aRS->fields[strtolower($fieldDimSchedulerKey)];
					//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
					$schedulerDesc = (string)@$aRS->fields[strtolower($fieldDimSchedulerDsc)];
				}
			}
		}
		//@JAPR
	}
	
	if(isset($arrayData['surveyFinished'])) 
	{
		if($arrayData['surveyFinished']=='' || $arrayData['surveyFinished']<=1)
		{
			$statusKey = svstFinished;
		}
		else 
		{
			$statusKey = $arrayData['surveyFinished'];
		}
	}
	else 
	{
		$statusKey = svstFinished;
	}

	//Determinamos si la encuesta tiene seccion dinamica
	//la primera prueba es verificar si en el arreglo $arrayData contiene la llave "sectionValue"
	//@JAPR 2014-05-29: Agregado el tipo de sección Inline
	//Las secciones Inline se comportan muy parecido a las dinámicas en el sentido que son pregeneradas a partir de un catálogo (si es que están
	//configuradas de esa forma en lugar de usar opciones fijas) así que deben grabar todos los atritbutos previos de dicho catálogo de la misma
	//manera que las dinámicas, sin embargo en estas secciones no hay diferencia entre las preguntas múltiple choice ni las simple choice de
	//catálogo (que en las dinámicas respectivamente, las primeras siempre se ligan al catálogo y graban un valor por registro, mientras que las
	//segundas simplemente no existen) sino que en esos casos específicos se comportan como secciones Maestro-detalle, por lo anterior, se van a
	//asignar las variables de esta función equivalentes a las de una sección dinámica para realizar el grabado de los atributos del catálogo, pero
	//se van a agregar dos variables exclusivas para indicar si realmente es una sección Inline y si realmente usa catálogo para generarse
	$hasDynamicSection = false;
	$dynamicSectionID = 0;
	$dynamicSectionCatID = 0;
	//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	$dynamicSectionCatMemberID = 0;
	$dynamicSectionChildCatMemberID = 0;
	$dynamicSectionIDEmpty = 0;
	if(isset($arrayData['sectionValue']))
	{
		//Si entro a este if quiere decir que al menos si trae valor para las secciones dinamicas
		//ahora se corrobora realmente q si exista
		$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $surveyID);
		
		if($dynamicSectionID>0)
		{
			$hasDynamicSection = true;
			$dynamicSection = BITAMSection::NewInstanceWithID($aRepository, $dynamicSectionID);
			$dynamicSectionCatID = $dynamicSection->CatalogID;
			$dynamicSectionCatMemberID = $dynamicSection->CatMemberID;
			$dynamicSectionChildCatMemberID = $dynamicSection->ChildCatMemberID;
		}
	}
	else {
		$dynamicSectionIDEmpty = BITAMSection::existDynamicSection($aRepository, $surveyID);
		if ($dynamicSectionIDEmpty > 0) {
			$dynamicSection = BITAMSection::NewInstanceWithID($aRepository, $dynamicSectionIDEmpty);
			if (!is_null($dynamicSection)) {
				$dynamicSectionCatID = $dynamicSection->CatalogID;
				$dynamicSectionCatMemberID = $dynamicSection->CatMemberID;
				$dynamicSectionChildCatMemberID = $dynamicSection->ChildCatMemberID;
			}
		}
	}
	
	//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Genera un array indexado por el MemberID y cuyo valor es la posición del atributo, esto para el catálogo de la sección dinámica
	$arrDynamicCatalogAttributesColl = array();
	if ($dynamicSectionCatID > 0) {
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $dynamicSectionCatID);
		if (!is_null($objCatalogMembersColl)) {
			foreach ($objCatalogMembersColl as $objCatalogMember) {
				$arrDynamicCatalogAttributesColl[$objCatalogMember->MemberID] = $objCatalogMember->MemberOrder;
			}
		}
	}
	//@JAPR
	
	//@JAPR 2014-05-29: Agregado el tipo de sección Inline
	//Para este tipo de secciones, podría haber alguna que se esté grabando en este punto, lo cual se representará con las variables de abajo,
	//pero potencialmente podría haber muchas mas secciones de diferentes catálogos, lo cual se representará con los arrays de abajo, en el caso
	//de repetir el mismo catálogo, sólo se debería grabar hasta el máximo atributo usado en cualquiera de las secciones, así que se valida desde
	//este punto antes de agregarlo a los Arrays (los cuales se indexan por el Id de la sección)
	//Si se está grabando una sección Inline en este paso, entonces NO aparecerá en los arrays, ya que esos son para grabar los datos de las
	//secciones Inline que no fueron capturadas
	$hasInlineSection = false;
	$inlineSection = null;
	$inlineSectionID = 0;
	$inlineSectionCatID = 0;
	$inlineSectionCatMemberID = 0;
	$inlineSectionChildCatMemberID = 0;
	$inlineSectionIDEmpty = 0;
	$arrInlineSectionIDs = array();
	$arrInlineSectionCatID = array();
	$arrInlineSectionCatMemberID = array();
	$arrInlineSectionChildCatMemberID = array();
	$arrInlineSectionIDs = BITAMSection::existInlineSections($aRepository, $surveyID, true);
	if (is_array($arrInlineSectionIDs)) {
		$arrInlineSectionIDs = array_flip($arrInlineSectionIDs);
	}
	else {
		$arrInlineSectionIDs = array();
	}
	
	if (isset($arrayData['inlineValue'])) {
		//Si entro a este if quiere decir que al menos si trae valor para las secciones Inline
		//ahora se corrobora realmente q si exista (el ID de la sección es el mismo que está grabando este registro, ya que puede haber varias
		//secciones)
		$inlineSectionID = $anEntrySectionID;
		if(isset($arrInlineSectionIDs[$inlineSectionID]))
		{
			$hasInlineSection = true;
			$inlineSection = BITAMSection::NewInstanceWithID($aRepository, $inlineSectionID);
			$inlineSectionCatID = $inlineSection->CatalogID;
			$inlineSectionCatMemberID = $inlineSection->CatMemberID;
			$inlineSectionChildCatMemberID = $inlineSection->ChildCatMemberID;
			//Se elimina del Array para que no se repita su procesamiento
			//unset($arrInlineSectionIDs[$inlineSectionID]);
		}
	}
	
	//@JAPR 2014-09-23: Agregada la dimensión de la sección para las que están definidas como Inline y que no requieren de catálogo
	//Obtiene la dimensión de la sección Inline que está haciendo el grabado de este registro si es que es precisamente de dicha sección, para
	//agregar el valor específico del registro en dicha dimensión (También aplica si la sección Inline está ligada a un catálogo, aunque en ese caso
	//realmente ya existen dimensiones por atributos así que este valor es innecesario en otra dimensión, pero se hará por consistencia)
	$strInlineSectionsDimFields = '';
	$strInlineSectionDimKeys = '';
	if ($hasInlineSection && !is_null($inlineSection) && $inlineSection->SectionDimValID > 0) {
		$anInlineSectionInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $inlineSection->SectionDimValID);
		if (!is_null($anInlineSectionInstanceDim)) {
			$strInlineSectionsDimFields .= ', '.((string) @$anInlineSectionInstanceDim->Dimension->TableName."KEY");
			//Obtiene el key del valor recibido como registro para la sección Inline
			$arrayValues = array();
			$arrayValues[] = $aDynamicPageDSC;
			$arrayPassValues = getInsertDimValueKey($aRepository, $anInlineSectionInstanceDim, $arrayValues);
			if ($arrayPassValues !== false && is_array($arrayPassValues) && isset($arrayPassValues[0])) {
				$intDimFieldValueKey = (int) $arrayPassValues[0];
				if ($intDimFieldValueKey == 0) {
					$intDimFieldValueKey = 1;
				}
			}
			else {
				$intDimFieldValueKey = 1;
			}
			$strInlineSectionDimKeys .= ', '.$intDimFieldValueKey;
		}
		chdir($strOriginalWD);
	}
	//@JAPR
	
	//Todas las secciones Inline necesitan llenar el array con sus datos, ya que basado en eso se llenarán sus atributos al grabar
	foreach ($arrInlineSectionIDs as $intSectionID => $strEmpty) {
		$anInlineSection = BITAMSection::NewInstanceWithID($aRepository, $intSectionID);
		if (!is_null($anInlineSection)) {
			//Sólo se consideran las secciones Inline con catálogo, ya que las que son de opción fija no son reelevantes pues no graban
			//nada adicional
			if ($anInlineSection->CatalogID > 0) {
				$arrInlineSectionCatID[$intSectionID] = $anInlineSection->CatalogID;
				$arrInlineSectionCatMemberID[$intSectionID] = $anInlineSection->CatMemberID;
				//Debe almacenar el valor mas grande de atributo usado por el mismo catálogo entre las secciones Inline que lo compartan,
				//por tanto la segunda que se procese en este punto comparará contra las anteriores
				$intChildCatMemberOrder = (int) @$arrInlineSectionChildCatMemberID[$intSectionID];
				if ($anInlineSection->ChildCatMemberID > $intChildCatMemberOrder) {
					$arrInlineSectionChildCatMemberID[$intSectionID] = $anInlineSection->ChildCatMemberID;
				}
			}
			
			//@JAPR 2014-09-23: Agregada la dimensión de la sección para las que están definidas como Inline y que no requieren de catálogo
			//Ahora ya se grabará una dimensión con el valor fijo de la sección Inline que corresponde con el registro capturado, aunque en
			//este punto es muy probable que la sección no sea la que está grabando así que básicamente insertará un dato vacio (*NA) en esta
			//sección, porque para la sección que está capturando este registro ya lo hubiera hecho antes de este ciclo
			if ($intSectionID != $inlineSectionID && $anInlineSection->SectionDimValID > 0) {
				$anInlineSectionInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $anInlineSection->SectionDimValID);
				if (!is_null($anInlineSectionInstanceDim)) {
					$strInlineSectionsDimFields .= ', '.((string) @$anInlineSectionInstanceDim->Dimension->TableName."KEY");
					//Obtiene el key del valor de NA ya que este ciclo es para las secciones que no son la que está grabando este registro
					$arrayValues = array();
					$arrayValues[] = $gblEFormsNA;
					$arrayPassValues = getDimValueKeys($aRepository, $anInlineSectionInstanceDim, $arrayValues);
					if ($arrayPassValues !== false && is_array($arrayPassValues) && isset($arrayPassValues[0])) {
						$intDimFieldValueKey = (int) $arrayPassValues[0];
						if ($intDimFieldValueKey == 0) {
							$intDimFieldValueKey = 1;
						}
					}
					else {
						$intDimFieldValueKey = 1;
					}
					$strInlineSectionDimKeys .= ', '.$intDimFieldValueKey;
				}
				chdir($strOriginalWD);
			}
			//@JAPR
		}
	}
	
	//Genera un array indexado por el MemberID y cuyo valor es la posición del atributo, esto para el catálogo de las secciones Inline
	$arrInlineCatalogAttributesColl = array();
	if ($inlineSectionCatID > 0 || count($arrInlineSectionCatID) > 0) {
		$arrAllInlineCatalogIDs = array_unique(array_merge(array($inlineSectionCatID), $arrInlineSectionCatID));
		foreach ($arrAllInlineCatalogIDs as $intCatalogID) {
			$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $intCatalogID);
			if (!is_null($objCatalogMembersColl)) {
				foreach ($objCatalogMembersColl as $objCatalogMember) {
					$arrInlineCatalogAttributesColl[$objCatalogMember->MemberID] = $objCatalogMember->MemberOrder;
				}
			}
		}
	}
	//@JAPR
	
	//Se genera instancia de coleccion de preguntas dado un SurveyID pero se ignoraran los datos de tipo 3 Seleccion Multiple
	$anArrayTypes = array();
	$anArrayTypes[0] = qtpMulti;
	$dspMatrix = dspMatrix;
	//$questionCollection = BITAMQuestionCollection::NewInstanceWithExceptType($aRepository, $surveyID, $anArrayTypes);
	$questionCollection = BITAMQuestionCollection::NewInstanceWithExceptTypeQDisplayMode($aRepository, $surveyID, $anArrayTypes, $dspMatrix);
	
	//Se van a obtener todas las preguntas de catalogo de la encuesta en las cuales dicho catalogo
	//sea el mismo que el catalogo de la encuesta ya que estas preguntas pueden venir repetidas
	//en este caso vamos igualarlas entre si para obtener el mismo valor ya que hacen referencia
	//al mismo campo de dimension de la tabla de hechos y tambien cada campo de la tabla paralela
	//de estas preguntas deben de tener el mismo valor
	$arrayQuestionSameCatalog = array();
	//@JAPR 2013-01-11: Corregido un bug, con la introducción de múltiples catálogos, cuando había mas de un par de preguntas del mismo catálogo
	//pero de dos o mas catálogos, sólo aquellas del catálogo de la encuesta estaban igualando su valor, siendo que lo correcto es que todos
	//los pares lo hubieran igualado (ver IF debajo del siguiente para referencia)
	$strQuestionCatalogValue = array();
	$strValue = "";
	//@JAPR 2013-07-18: Corregido un bug, a partir de la versión con catálogos independientes, ya no es necesario que se igualen las respuestas
	//de las preguntas de catálogo, porque cada pregunta ya se graba en forma independiente y hacer esta igualdad provocaba que el método de
	//grabado de los campos cat_#### así como el cálculo de las respuestas a partir de variables usado durante el grabado de acciones de eBavel,
	//utilizaran el valor de la última de las preguntas de catálogo (que es el que se iguala a las demás) en lugar del de la pregunta específica,
	//obviamente se asume que para esta versión, el array de respuestas ya viene correctamente asignado para cada pregunta, por lo que no hay
	//necesidad de hacer ningún ajuste adicional como este
	if (!$surveyInstance->DissociateCatDimens) {
		foreach ($questionCollection->Collection as $questionKey => $aQuestion)
		{
			//Ya no necesariamente tienen que ser del catálogo de la encuesta, cualquier pregunta de catálogo simple choice se debe validar
			if($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1 && $aQuestion->CatalogID > 0)
			{
				$intCatalogID = $aQuestion->CatalogID;
				if (!isset($arrayQuestionSameCatalog[$intCatalogID])) {
					$arrayQuestionSameCatalog[$intCatalogID] = array();
				}
				if (!isset($strQuestionCatalogValue[$intCatalogID])) {
					$strQuestionCatalogValue[$intCatalogID] = "";
				}
				$arrayQuestionSameCatalog[$intCatalogID][$aQuestion->QuestionID] = $aQuestion->QuestionID;
				
				$postKey = "qfield".$aQuestion->QuestionID;
				if(isset($arrayData[$postKey]))
				{
					$strValue = $arrayData[$postKey];
				}
				else 
				{
					$strValue = "";
				}
				
				if(strlen($strQuestionCatalogValue[$intCatalogID])<strlen($strValue))
				{
					$strQuestionCatalogValue[$intCatalogID] = $strValue;
				}
				
				foreach($arrayQuestionSameCatalog[$intCatalogID] as $valueID) 
				{
					$index = "qfield".$valueID;
					$arrayData[$index] = $strQuestionCatalogValue[$intCatalogID];
				}
			}
		}
	}
	
	$descriptKeysCollection = BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository, $anInstanceModel->ModelID);
	
	$currentDate = $arrayData["surveyDate"];
	$dateKey = getDateKey($aRepository, $anInstanceModel, $descriptKeysCollection, $currentDate);
	
	if($dateKey===false)
	{
		return($gblEFormsErrorMessage);
	}
	
	//Obtenemos instancia del indicador AnsweredQuestions
	$answeredQuestionsIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->AnsweredQuestionsIndID);
	chdir($strOriginalWD);
	
	//Obtenemos instancia del indicador Duration
	$durationIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->DurationIndID);
	chdir($strOriginalWD);
	
	//Obtenemos instancia del indicador Latitude y Longitude
	$latitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->LatitudeIndID);
	$longitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->LongitudeIndID);
	chdir($strOriginalWD);
	
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	$accuracyIndicator = null;
	if (getMDVersion() >= esvAccuracyValues && $surveyInstance->AccuracyIndID) {
		//Obtenemos instancia del indicador Accuracy si es que existe
		$accuracyIndicator = @BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->AccuracyIndID);
		chdir($strOriginalWD);
	}
	//@JAPR
	
	//Obtenemos instancia de la dimension User
	$userDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->UserDimID);
	chdir($strOriginalWD);
	
	//Obtenemos instancia de la dimension Section
	//$sectionDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SectionDimID);
	//chdir($strOriginalWD);
	
	//Array de instancias de objetos dimension o indicator de los questions
	$arrayInstancesIndDim = array();
	$arrayInstancesIndicator = array();
	$arrayInstancesIndicatorSC = array();
	
	$arrayKeyValuesByQuestion = array();
	$arrayIndValuesByQuestion = array();
	$arrayIndValuesSCByQuestion = array();
	//@JAPR 2012-05-17: Agregadas las preguntas tipo Action
	$arrayValuesByActionQuestion = array();		//Contiene el Array completo de valores para todos los campos de las preguntas tipo Action, 
			//que es lo mismo que hacer un implode mediante el separador tal como está en $arrayData para esta pregunta, pero es
			//diferente al valor de $arrayKeyValuesByQuestion donde solo viene la llave subrrogada del valor de la Acción sin considerar
			//al resto de los elementos concatenados con separador (Responsible, Element y DaysDueDate)
	//@JAPR
	
	foreach ($questionCollection->Collection as $questionKey => $aQuestion) 
	{
		$arrayValues = array();
		
		//@JAPR 2012-05-17: Agregados nuevos tipos de pregunta Foto y Firma
		//Las preguntas de Foto y Firma no graban una respuesta, sólo la imagen, por lo tanto se salta esta pregunta completamente
		//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
		//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
		//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
		//Cambiado a un switch para mayor facilidad
		$blnValidQuestion = true;
		switch ($aQuestion->QTypeID) {
			case qtpPhoto:
			case qtpDocument:
			case qtpSignature:
			case qtpSketch:
			case qtpSkipSection:
			case qtpMessage:
			case qtpSync:
			case qtpPassword:
			case qtpAudio:
			case qtpVideo:
			case qtpSection:
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			case qtpExit:
				$blnValidQuestion = false;
				break;
		}
		
		if (!$blnValidQuestion)
		{
			continue;
		}
		//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		elseif ($aQuestion->QTypeID == qtpGPS) {
			//Estas preguntas graban en 4 dimensiones distintas además de en la tabla paralela
			$postKey = "qfield".$aQuestion->QuestionID;
			$postKeyLat = "qlat".$aQuestion->QuestionID;
			$postKeyLong = "qlong".$aQuestion->QuestionID;
			$postKeyAcc = "qacc".$aQuestion->QuestionID;
			if(isset($arrayData[$postKey]))
			{
				if(trim($arrayData[$postKey])=="")
				{
					$arrayValues[] = $gblEFormsNA;
				}
				else 
				{
					//En este caso si hay un valor como respuesta, así que concatena el Accuracy al final en la forma "Latitude, Longitude (Accuracy)"
					$strData = (string) @$arrayData[$postKeyAcc];
					if (trim($strData) == "") {
						$strData = $gblEFormsNA;
					}
					$arrayValues[] = $arrayData[$postKey]." ({$strData})";
				}
			}
			else 
			{
				$arrayValues[] = $gblEFormsNA;
			}
			
			//Si se graban agregaciones, debe agregar los valores de las 4 dimensiones
			if ($gbEnableIncrementalAggregations) {
				//Dimensión base con la combinación "Latitude, Longitude (Accuracy)"
				$strData = (string) @$arrayData[$postKey];
				if (trim($strData) == "") {
					$strData = $gblEFormsNA;
				}
				$arrQuestionDimValues[] = $strData;
				$arrQuestionDimValuesDesc[] = $strData;
				
				//Dimensión Latitude
				$strData = (string) @$arrayData[$postKeyLat];
				if (trim($strData) == "") {
					$strData = $gblEFormsNA;
				}
				$arrQuestionDimValues[] = $strData;
				$arrQuestionDimValuesDesc[] = $strData;
				
				//Dimensión Longitude
				$strData = (string) @$arrayData[$postKeyLong];
				if (trim($strData) == "") {
					$strData = $gblEFormsNA;
				}
				$arrQuestionDimValues[] = $strData;
				$arrQuestionDimValuesDesc[] = $strData;
				
				//Dimensión Accuracy
				$strData = (string) @$arrayData[$postKeyAcc];
				if (trim($strData) == "") {
					$strData = $gblEFormsNA;
				}
				$arrQuestionDimValues[] = $strData;
				$arrQuestionDimValuesDesc[] = $strData;
			}
			
			$arrayInstancesIndDim[$aQuestion->QuestionID] = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
			chdir($strOriginalWD);
			
			$arrayPassValues = getKeyValues($aRepository, $arrayInstancesIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
			
			if($arrayPassValues!==false)
			{				
				$arrayKeyValuesByQuestion[$questionKey] = $arrayPassValues;
			}
			else 
			{
				return($gblEFormsErrorMessage);
			}
		}
		//@JAPR 2012-05-17: Agregadas las preguntas tipo Action
		//La pregunta tipo Action viene como un texto con separadores, por lo tanto obtenemos sólo la respuesta que es el Action para grabar
		//en la tabla paralela/hechos
		else if ($aQuestion->QTypeID == qtpAction)
		{
			//Obtener valor capturado de dicha question pero sólo del Action (índice 3)
			$postKey = "qfield".$aQuestion->QuestionID;
			$arrayActionValues = array();
			if(isset($arrayData[$postKey]))
			{
				if(trim($arrayData[$postKey])=="")
				{
					$arrayValues[] = $gblEFormsNA;
				}
				else 
				{
					$arrayActionValues = explode("_SVSep_", $arrayData[$postKey]);
					if ((string) @$arrayActionValues[3] != '')
					{
						$arrayValues[] = (string) @$arrayActionValues[3];
					}
					else 
					{
						$arrayActionValues = array();
						$arrayValues[] = $gblEFormsNA;
					}
				}
			}
			else 
			{
				$arrayValues[] = $gblEFormsNA;
			}
			
			//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
			if ($gbEnableIncrementalAggregations) {
				$arrQuestionDimValues[] = $arrayValues[0];
				$arrQuestionDimValuesDesc[] = $arrayValues[0];
			}
			//@JAPR
			
			//Almacena el resto de los componentes para generar acciones
			$arrayValuesByActionQuestion[$questionKey] = $arrayActionValues;
			
			$arrayInstancesIndDim[$aQuestion->QuestionID] = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
			chdir($strOriginalWD);
			
			$arrayPassValues = getKeyValues($aRepository, $arrayInstancesIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
			
			if($arrayPassValues!==false)
			{
				$arrayKeyValuesByQuestion[$questionKey] = $arrayPassValues;
			}
			else 
			{
				return($gblEFormsErrorMessage);
			}
		}
		//@JAPR
		//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
		else if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
		{
			//@JAPRWarning: Validar las preguntas tipo Photo porque esas no tienen un $aQuestion->IndDimID
			//Obtener valor capturado de dicha question
			$postKey = "qfield".$aQuestion->QuestionID;
			//@JAPR 2013-01-22: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
			if(isset($arrayData[$postKey]) && trim($arrayData[$postKey]) !== '')
			{
				$arrayValues[0] = (float)$arrayData[$postKey];
			}
			else 
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$arrayValues[0] = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}

			//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
			if ($gbEnableIncrementalAggregations) {
				//@JAPR 2013-11-13: Validado que las dimensiones que son para preguntas numéricas graben NA en lugar de NULL
				if (is_null($arrayValues[0])) {
					$arrQuestionDimValues[] = ($surveyInstance->UseNULLForEmptyNumbers)?$gblEFormsNA:0;
					$arrQuestionDimValuesDesc[] = ($surveyInstance->UseNULLForEmptyNumbers)?$gblEFormsNA:0;
				}
				else {
					$arrQuestionDimValues[] = $arrayValues[0];
					$arrQuestionDimValuesDesc[] = $arrayValues[0];
				}
			}
			//@JAPR
			
			if($aQuestion->IsIndicator==1)
			{
				$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
				$arrayInstancesIndicator[$aQuestion->QuestionID] = $anInstanceIndicator;
				chdir($strOriginalWD);
				$arrayIndValuesByQuestion[$questionKey] = $arrayValues;
				
				//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations && !is_null($anInstanceIndicator)) {
					$arrRowIndicValues[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = $arrayValues[0];
				}
				//@JAPR
			}
			
			$arrayInstancesIndDim[$aQuestion->QuestionID] = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
			chdir($strOriginalWD);
			
			$arrayPassValues = getKeyValues($aRepository, $arrayInstancesIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
			
			if($arrayPassValues!==false)
			{
				$arrayKeyValuesByQuestion[$questionKey] = $arrayPassValues;
			}
			else 
			{
				return($gblEFormsErrorMessage);
			}
		}
		else 
		{
			$arrayInstancesIndDim[$aQuestion->QuestionID] = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
			chdir($strOriginalWD);
			
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Hora
			//@AAL 06/05/2015: Modificado para soportar preguntas tipo código de barras
			if($aQuestion->QTypeID==qtpOpenDate || $aQuestion->QTypeID==qtpOpenString || $aQuestion->QTypeID==qtpOpenAlpha || $aQuestion->QTypeID==qtpOpenTime || $aQuestion->QTypeID==qtpBarCode)
			{
				//Obtener valor capturado de dicha question Date o TextBox
				$postKey = "qfield".$aQuestion->QuestionID;
				
				if(isset($arrayData[$postKey]))
				{
					if(trim($arrayData[$postKey])=="")
					{
						$arrayValues[] = $gblEFormsNA;
					}
					else 
					{
						$arrayValues[] = $arrayData[$postKey];
					}
				}
				else 
				{
					$arrayValues[] = $gblEFormsNA;
				}
			}
			else if($aQuestion->QTypeID==qtpSingle || $aQuestion->QTypeID==qtpCallList) //agregado tipo callList
			{
				//Obtener valor capturado de dicha question Seleccion Simple
				$postKey = "qfield".$aQuestion->QuestionID;
				
				if(isset($arrayData[$postKey]))
				{
					if(trim($arrayData[$postKey])=="")
					{
						//Si es simple choice que no utiliza catalogo, entonces
						//que si se le indique valor de no aplica, en caso 
						//contrario se tiene q hacer vacio para que en la fn
						//de busqueda de valores-catalogo regrese llave de *NA
						if($aQuestion->UseCatalog==0)
						{
							$arrayValues[] = $gblEFormsNA;
						}
						else 
						{
							$arrayValues[] = '';
						}
					}
					else 
					{
						//$arrayValues[] = $arrayData[$postKey];
						$strCapturedAnswer = $arrayData[$postKey];
						
						//@JAPR 2012-04-04: Agregada la opción de "Otro" al grabar preguntas de selección sencilla sin catálogo
						$blnNewAnswer = false;
						if ($aQuestion->UseCatalog == 0)
						{
							$blnNewAnswer = true;
							foreach ($aQuestion->SelectOptions as $anAnswer)
							{
								if (strcasecmp($anAnswer, $strCapturedAnswer) == 0)
								{
									//Se encontró la respuesta entre la lista de 
									$blnNewAnswer = false;
									break;
								}
							}
						}
						
						if ($blnNewAnswer)
						{
							if ($aQuestion->AllowAdditionalAnswers)
							{
								//@JAPR 2013-04-19: Modificado el comportamiento para que ya no agregue las opciones de Otro a la lista de
								//opciones de respuesta de la pregunta
								/*
								//Hay que agregar la respuesta a la lista de posibles valores de esta pregunta
								$anInstanceQOption = BITAMQuestionOption::NewInstance($aRepository, $aQuestion->QuestionID);
								$anInstanceQOption->QuestionOptionName = $strCapturedAnswer;
								//Al hacer este grabado, automáticamente se agrega al catálogo de la dimensión por lo que mas adelante
								//cuando se pida obtener la llave Subrrogada (que es lo que realmente se graba como respuesta en la Fact) ya
								//se podrá localizar esta respuesta
								$anInstanceQOption->save();
								*/
								
								//Actualiza la instancia actual de la pregunta para agregar la opción recien creada
								$aQuestion->StrSelectOptions .= ((trim($aQuestion->StrSelectOptions) == "")?"":"\r\n").$strCapturedAnswer;
								$aQuestion->SelectOptions[] = $strCapturedAnswer;
							}
							else 
							{
								//En este caso se debe limpiar la respuesta porque la pregunta no acepta agregar nuevas respuestas
								if($aQuestion->UseCatalog==0)
								{
									$strCapturedAnswer = $gblEFormsNA;
								}
								else 
								{
									$strCapturedAnswer = '';
								}
							}
						}
						
						$arrayValues[] = $strCapturedAnswer;
						//@JAPR
					}
				}
				else 
				{
					//Si es simple choice que no utiliza catalogo, entonces
					//que si se le indique valor de no aplica, en caso 
					//contrario se tiene q hacer vacio para que en la fn
					//de busqueda de valores-catalogo regrese llave de *NA
					if($aQuestion->UseCatalog==0)
					{
						$arrayValues[] = $gblEFormsNA;
					}
					else 
					{
						$arrayValues[] = '';
					}
				}
				
				//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
				//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
				//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
				if(($aQuestion->QTypeID==qtpSingle) && $aQuestion->UseCatalog==0 && $aQuestion->IndicatorID>0)
				{
					$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
					$arrayInstancesIndicatorSC[$aQuestion->QuestionID] = $anInstanceIndicator;
					chdir($strOriginalWD);
					
					$bnlSaveScore = true;
					if ($surveyInstance->UseStdSectionSingleRec) {
						$bnlSaveScore = false;
						//En este caso las preguntas simple-choice con Score deben grabar la dimensión en todos los registros, pero sólo deben grabar
						//el indicador de Score en el registro que corresponde con la sección/página en cuestión según el contexto del registro
						if (isset($arrMasterDetSectionsIDs[$aQuestion->SectionID])) {
							//- Sección Maestro-detalle siempre
							$bnlSaveScore = true;
						}
						elseif ($hasDynamicSection && $dynamicSectionID == $aQuestion->SectionID) {
							//- Sección Dinámica sólo si es el registro de la página
							if ($anEntrySectionID == $aQuestion->SectionID && $aDynamicPageDSC != '') {
								$bnlSaveScore = true;
							}
						}
						elseif ($anEntryCatDimID > 0) {
							//- Cateogoría de dimensión nunca
						}
						elseif ($anEntrySectionID == 0 && $anEntryCatDimID == 0) {
							//- Sección Estándar sólo si es el registro único
							$bnlSaveScore = true;
						}
					}
					
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					if ($bnlSaveScore) {
						//Es un registro que si aplica para esta pregunta, por lo tanto graba el score
						$arrayPassValues = getScoreValues($aRepository, $aQuestion, $arrayValues, $surveyInstance->UseNULLForEmptyNumbers);
					}
					else {
						//No se debe grabar el score, por tanto se manda vacio
						if ($surveyInstance->UseNULLForEmptyNumbers) {
							$arrayPassValues = array(null);
						}
						else {
							$arrayPassValues = array(0);
						}
					}
					//@JAPR
					
					if($arrayPassValues!==false)
					{
						$arrayIndValuesSCByQuestion[$questionKey] = $arrayPassValues;
						//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
						if ($gbEnableIncrementalAggregations && !is_null($anInstanceIndicator)) {
							$arrRowIndicValues[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = $arrayPassValues[0];
						}
						//@JAPR
					}
					else 
					{
						return($gblEFormsErrorMessage);
					}
				}
			}
			
			//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
			if ($gbEnableIncrementalAggregations) {
				if ($surveyInstance->DissociateCatDimens && $aQuestion->QTypeID == qtpSingle && $aQuestion->UseCatalog == 1) {
					//En este caso esta pregunta no se grabará aquí
				}
				else {
					$arrQuestionDimValues[] = $arrayValues[0];
					$arrQuestionDimValuesDesc[] = $arrayValues[0];
				}
			}
			//@JAPR
			
			if(!$hasDynamicSection)
			{
				//Si no hay secciones dinámicas, las respuestas vienen directamente como los textos o valores introducidos, por lo que se
				//debe consultar (y agregar si es necesario) dicha respuesta del catálogo de la dimensión asociada para obtener la llave 
				//subrrogada correspondiente pues eso es lo que se graba en la FactKey
				//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				//En el caso de preguntas simple choice de catálogo cuando no hay sección dinámica, el propio método getKeyValues buscará el key
				//correspondiente a partir de la combinación recibida desde el App, así que este método no sufre cambios
				$arrayPassValues = getKeyValues($aRepository, $arrayInstancesIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
				
				if($arrayPassValues!==false)
				{				
					$arrayKeyValuesByQuestion[$questionKey] = $arrayPassValues;
				}
				else 
				{
					return($gblEFormsErrorMessage);
				}
			}
			else 
			{
				//Verificamos en esta parte si se trata de alguna pregunta de seleccion simple con catalogo igual 
				//al catalogo del survey y que se haya generado seccion dinamica
				if($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1 && $aQuestion->CatalogID==$dynamicSectionCatID)
				{
					//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
					//En el caso de preguntas simple choice del catálogo que se usa en la sección dinámica, para este punto ya se verificó que 
					//cada registro traiga una combinación que si exista en el catálogo o bien el valor de NA, ya que en el pre-procesamiento se 
					//basó en el valor recibido para ir a buscar al catálogo usando el método getKeyValues y se sobreescribieron los SectionValues
					//a keys que si existen en este momento, por lo que el resto del proceso no sufre cambios
					$arrayTmp = array();
					$arrayTmp[0] = $arrayData['sectionValue'];
					
					//Aqui asignamos el valor de sectionValue directamente ya que es el valor de la llave subrogada
					//por ejemplo Producto
					$arrayKeyValuesByQuestion[$questionKey] = $arrayTmp;
				}
				else 
				{
					//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
					//En este caso sucede igual que lo descrito en el If cuando no hay sección dinámica (ver arriba)
					$arrayPassValues = getKeyValues($aRepository, $arrayInstancesIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
					
					if($arrayPassValues!==false)
					{
						$arrayKeyValuesByQuestion[$questionKey] = $arrayPassValues;
					}
					else 
					{
						return($gblEFormsErrorMessage);
					}
				}
			}
		}
	}
	
	//@JAPR 2012-05-17: $arrayPassValues es el array con la (s) respuestas ya mapeadas a la tabla de dimensión, es decir, son las llaves subrrogadas
	//de lo que se contestó en esta pregunta, mientras que $arrayValues son las respuestas capturadas directamente por el usuario al contestar la
	//encuesta, por lo que según el tipo de pregunta pudieran ser textos, numeros o fechas o cualquier otra cosa
	
	/*************************************************************************************/
	//Generamos una coleccion de solo las preguntas que sean de selección múltiple 
	//y q pertenezcan a secciones dinamicas y esten dentro de ellas
	$uniqueMultiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceUniqueDim($aRepository, $surveyID);
	
	$countUniqueMChoiceQuestionCollection = count($uniqueMultiChoiceQuestionCollection->Collection);

	//Array de instancias de objetos dimension o indicator de los questions
	$arrayInstancesMCIndDim = array();
	$arrayInstancesMCIndicator = array();
	$arrayKeyMCValuesByQuestion = array();
	$arrayMCIndValuesByQuestion = array();

	//Recorremos las preguntas de seleccion multiple para obtener sus valores multiples
	foreach ($uniqueMultiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
	{
		$arrayValues = array();
		
		//Esto aplica para las preguntas multiple choice con numeros o textos
		//entonces lo procesamos como dimensiones unicas
		if($aQuestion->MCInputType>mpcCheckBox)
		{
			//Obtener valor capturado de dicha question
			$postKey = "qfield".$aQuestion->QuestionID;
			//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
			if(isset($arrayData[$postKey]) && ($aQuestion->MCInputType!=mpcNumeric) || trim($arrayData[$postKey]) !== '')
			{
				if($aQuestion->MCInputType==mpcNumeric)
				{
					$arrayValues[0] = (int)$arrayData[$postKey];
				}
				else 
				{
					$arrayValues[0] = $arrayData[$postKey];
				}
			}
			else 
			{
				if($aQuestion->MCInputType==mpcNumeric)
				{
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					$arrayValues[0] = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
				}
				else 
				{
					$arrayValues[0] = $gblEFormsNA;
				}
			}
			
			//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
			if ($gbEnableIncrementalAggregations) {
				//@JAPR 2013-11-13: Validado que las dimensiones que son para preguntas numéricas graben NA en lugar de NULL
				//@JAPR 2014-01-10: Corregido un bug, al grabar en el cubo finalmente si no se selecciona entonces se graba un 0 incluso aunque
				//en realidad la pregunta hubiera sido saltada y debiera grabar *NA, por tanto se deja el 0 en caso de estar null (sólo aplica
				//para tipo CheckBox, las otras si grabarían el *NA o bien insertaría el elemento a la dimensión)
				if (is_null($arrayValues[0])) {
					if ($aQuestion->MCInputType == mpcCheckBox) {
						$arrQuestionDimValues[] = 0;		//($surveyInstance->UseNULLForEmptyNumbers)?$gblEFormsNA:0;
						$arrQuestionDimValuesDesc[] = 0;	//($surveyInstance->UseNULLForEmptyNumbers)?$gblEFormsNA:0;
					}
					else {
						$arrQuestionDimValues[] = $gblEFormsNA;
						$arrQuestionDimValuesDesc[] = $gblEFormsNA;
					}
					//@JAPR
				}
				else {
					$arrQuestionDimValues[] = $arrayValues[0];
					$arrQuestionDimValuesDesc[] = $arrayValues[0];
				}
			}
			//@JAPR
			
			if($aQuestion->MCInputType==mpcNumeric && $aQuestion->IsIndicatorMC==1)
			{
				$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
				$arrayInstancesMCIndicator[$aQuestion->QuestionID] = $anInstanceIndicator;
				chdir($strOriginalWD);
				$arrayMCIndValuesByQuestion[$questionKey] = $arrayValues;
				
				//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations && !is_null($anInstanceIndicator)) {
					$arrRowIndicValues[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = $arrayValues[0];
				}
				//@JAPR
			}
			
			$arrayInstancesMCIndDim[$aQuestion->QuestionID] = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
			chdir($strOriginalWD);

			$arrayPassValues = getKeyValuesForMC($aRepository, $arrayInstancesMCIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
			
			if($arrayPassValues!==false)
			{
				$arrayKeyMCValuesByQuestion[$questionKey] = $arrayPassValues;
			}
			else 
			{
				return($gblEFormsErrorMessage);
			}
		}
		else 
		{
			//Obtener valor capturado de dicha question
			$postKey = "qfield".$aQuestion->QuestionID;
			
			if(isset($arrayData[$postKey]))
			{
				$arrayValues[0] = (int)$arrayData[$postKey];
			}
			else 
			{
				$arrayValues[0] = 0;
			}

			$anInstanceDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
			$arrayInstancesMCIndDim[$aQuestion->QuestionID] = $anInstanceDimension;
			chdir($strOriginalWD);
			
			//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
			if ($gbEnableIncrementalAggregations) {
				$arrQuestionDimValues[] = $arrayValues[0];
				$arrQuestionDimValuesDesc[] = $arrayValues[0];
			}
			//@JAPR
			
			$arrayPassValues = getKeyValuesForMC($aRepository, $arrayInstancesMCIndDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
			
			if($arrayPassValues!==false)
			{
				$arrayKeyMCValuesByQuestion[$questionKey] = $arrayPassValues;
			}
			else 
			{
				return($gblEFormsErrorMessage);
			}
		}
	}
	
	/************************************************************************************/
	//Generamos una coleccion de solo las preguntas que sean de selección múltiple 
	//y q no pertenezcan a secciones dinamicas o bien esten fuera de ellas
	$anArrayTypes = array();
	$anArrayTypes[0] = qtpMulti;
	//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
	$multiChoiceQuestionCollectionNoMultDim = BITAMQuestionCollection::NewInstanceMultiChoiceDims($aRepository, $surveyID, 0);
	//@JAPR
	$multiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceDims($aRepository, $surveyID);
	$arrayExistMultiChoiceIndicators = array();
	
	$arrayInstancesMultiDim = array();
	$arrayKeyMultiValuesByQuestion = array();
	
	$arrayInstancesMultiChoiceInds = array();
	$arrayMultiChoiceIndValuesByQuestion = array();

	//Verificar si existen opciones en preguntas de seleccion multiple
	$existMultiOptions = false;

	//Recorremos las preguntas de seleccion multiple para obtener sus valores multiples
	foreach ($multiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
	{
		$arrayValues = array();
		$postKey = "qfield".$aQuestion->QuestionID;
		
		if(isset($arrayData[$postKey]))
		{
			$strValues = $arrayData[$postKey];
			if(trim($strValues)!="")
			{
				$arrayValues = explode("_SVSep_", $arrayData[$postKey]);
			}
		}
		
		//Verificar si la pregunta tiene asignado indicadores por scores
		if($aQuestion->MCInputType == mpcCheckBox) 
		{
			//Verificamos si hay un score capturado para esta opcion de pregunta de seleccion multiple
			//que no sea matriz o bien revisamos si ya se crearon indicadores anteriormente
			$existIndicators = false;
			
			//Recorremos las opciones para revisar los indicadores
			foreach ($aQuestion->SelectOptions as $optionKey=>$optionValue)
			{
				$indicatorid = 0;
				
				if(isset($aQuestion->QIndicatorIds[$optionKey]))
				{
					$indicatorid = (int)$aQuestion->QIndicatorIds[$optionKey];
				}
				else 
				{
					$indicatorid = 0;
				}
	
				if(!is_null($indicatorid) && $indicatorid!=0)
				{
					$existIndicators = true;
					break;
				}
			}
			
			$arrayExistMultiChoiceIndicators[$aQuestion->QuestionID] = $existIndicators;
		}
		else 
		{
			$arrayExistMultiChoiceIndicators[$aQuestion->QuestionID] = false;
		}
		
		
		$arrayTempDims = array();
		$arrayTempInds = array();
		$QIndDimIDs = $aQuestion->QIndDimIds;
		
		//Obtenemos las instancias de las dimensiones de las opciones
		foreach ($QIndDimIDs as $qKey=>$qDimID) 
		{
			$existMultiOptions = true;
			$arrayTempDims[$qKey] = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $qDimID);

			if($aQuestion->MCInputType == mpcCheckBox && $arrayExistMultiChoiceIndicators[$aQuestion->QuestionID]==true) 
			{
				$tmpInd = (int)$aQuestion->QIndicatorIds[$qKey];
				
				if($tmpInd!=0)
				{
					$arrayTempInds[$qKey] = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $tmpInd);
				}
				else 
				{
					$arrayTempInds[$qKey] = null;
				}
			}
			else 
			{
				$arrayTempInds[$qKey] = null;
			}
			
			//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
			if ($gbEnableIncrementalAggregations) {
				//@JAPR 2013-11-13: Validado que las dimensiones que son para preguntas numéricas graben NA en lugar de NULL
				if (is_null(@$arrayValues[$qKey])) {
					//@JAPR 2014-01-10: Corregido un bug, al grabar en el cubo finalmente si no se selecciona entonces se graba un 0 incluso aunque
					//en realidad la pregunta hubiera sido saltada y debiera grabar *NA, por tanto se deja el 0 en caso de estar null (sólo aplica
					//para tipo CheckBox, las otras si grabarían el *NA o bien insertaría el elemento a la dimensión)
					if ($aQuestion->MCInputType == mpcCheckBox) {
						$arrQuestionDimValues[] = 0;		// ($surveyInstance->UseNULLForEmptyNumbers)?$gblEFormsNA:0;
						$arrQuestionDimValuesDesc[] = 0;	// ($surveyInstance->UseNULLForEmptyNumbers)?$gblEFormsNA:0;
					}
					else {
						$arrQuestionDimValues[] = $gblEFormsNA;
						$arrQuestionDimValuesDesc[] = $gblEFormsNA;
					}
					//@JAPR
				}
				else {
					$arrQuestionDimValues[] = @$arrayValues[$qKey];
					$arrQuestionDimValuesDesc[] = @$arrayValues[$qKey];
				}
			}
			//@JAPR
			
			chdir($strOriginalWD);
		}
		
		$arrayInstancesMultiDim[$aQuestion->QuestionID] = $arrayTempDims;
		$arrayInstancesMultiChoiceInds[$aQuestion->QuestionID] = $arrayTempInds;
		
		if($aQuestion->MCInputType == mpcCheckBox) 
		{
			$arrayKeyMultiValuesByQuestion[$questionKey] = getKeyMultiValues($aRepository, $arrayInstancesMultiDim[$aQuestion->QuestionID], $aQuestion, $arrayValues);
			
			//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
			//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
			//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
			$bnlSaveScore = true;
			if ($surveyInstance->UseStdSectionSingleRec) {
				$bnlSaveScore = false;
				//En este caso las preguntas multiple-choice con Score deben grabar la dimensión en todos los registros, pero sólo deben grabar
				//el indicador de Score en el registro que corresponde con la sección/página en cuestión según el contexto del registro
				if (isset($arrMasterDetSectionsIDs[$aQuestion->SectionID])) {
					//- Sección Maestro-detalle siempre
					$bnlSaveScore = true;
				}
				elseif ($hasDynamicSection && $dynamicSectionID == $aQuestion->SectionID) {
					//- Sección Dinámica nunca, ya que toda multiple-choice en estas secciones se comporta como de catálogo automáticamente
				}
				elseif ($anEntryCatDimID > 0) {
					//- Cateogoría de dimensión nunca
				}
				elseif ($anEntrySectionID == 0 && $anEntryCatDimID == 0) {
					//- Sección Estándar sólo si es el registro único
					$bnlSaveScore = true;
				}
			}
			
			//Obtenemos los valores para los indicadores de los scores
			//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
			if ($bnlSaveScore) {
				//Es un registro que si aplica para esta pregunta, por lo tanto graba el score
				$arrayMultiChoiceIndValuesByQuestion[$questionKey] = getScoreMultiValues($aRepository, $arrayInstancesMultiChoiceInds[$aQuestion->QuestionID], $aQuestion, $arrayExistMultiChoiceIndicators[$aQuestion->QuestionID], $arrayValues, $surveyInstance->UseNULLForEmptyNumbers);
			}
			else {
				//No se debe grabar el score, por tanto se manda vacio
				$arrayMultiChoiceIndValuesByQuestion[$questionKey] = getScoreMultiValues($aRepository, $arrayInstancesMultiChoiceInds[$aQuestion->QuestionID], $aQuestion, $arrayExistMultiChoiceIndicators[$aQuestion->QuestionID], array(), $surveyInstance->UseNULLForEmptyNumbers);
			}
			
			//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
			if ($gbEnableIncrementalAggregations) {
				foreach ($QIndDimIDs as $qKey => $qDimID) {
					$indicatorid = (int) @$aQuestion->QIndicatorIds[$qKey];
					if($indicatorid != 0) {
						$arrRowIndicValues['IND_'.$indicatorid] = @$arrayMultiChoiceIndValuesByQuestion[$questionKey][$qKey];
					}
				}
				
			}
			//@JAPR
		}
		else 
		{
			$arrayInstanceMultiDim = array();
			$arrayInstanceMultiDim = $arrayInstancesMultiDim[$aQuestion->QuestionID];
			$arrayKeys = array();
			$arrayZeroValues = array();
			$m=0;
			foreach ($arrayInstanceMultiDim as $anInstanceDim)
			{
				//@JAPR 2013-01-22: Corregido un bug, al introducir el registro único algunas preguntas ya no se mandan, pero se esperaba un valor
				$passValue = getInsertDimMultiValueKey($aRepository, $anInstanceDim, (string) @$arrayValues[$m]);
				
				if($passValue!==false)
				{
					$arrayKeys[$m] = $passValue;
				}
				else 
				{
					return($gblEFormsErrorMessage);
				}

				$arrayZeroValues[$m] = (($surveyInstance->UseNULLForEmptyNumbers)?null:(float)0);
				$m++;
			}
			$arrayKeyMultiValuesByQuestion[$questionKey] = $arrayKeys;
			
			//Almacenamos ceros por default para este arreglo para que contenga algo aunque para este tipo de Multiple Choice
			//con entradas numericas a alfanumericas no se manejen scores o indicadores
			$arrayMultiChoiceIndValuesByQuestion[$questionKey] = $arrayZeroValues;
		}
	}
	
	/*************************************************************************************/
	//Obtenemos las preguntas que pertenezcan a Category Dimension 
	$catDimQuestionsInfo = BITAMQuestion::getAllCategoryDimQuestionsInfo($aRepository, $surveyInstance->SurveyID);

	$catDimFieldNames = array();
	$catDimFieldValues = array();

	//Obtenemos campos y valores correspondientes de las preguntas CategoryDimension
	//Este código sólo debería grabar valores de no aplica, ya que sólo asume la existencia de un único elemento qCatDimValQID entre las respuestas
	//pero si pueden existir mas de una pregunta de tipo categoría de dimensión en la misma encuesta, además en cuanto se detectan respuestas
	//a este tipo de preguntas en el array se invoca a un método de grabado alternativo por lo que no hubiera llegado hasta aquí
	foreach ($catDimQuestionsInfo as $catDimQuestionKey=>$catDimQInfo)
	{
		$qid = -1;
		$postKey = "qCatDimValQID";
			
		if(isset($arrayData[$postKey]))
		{
			$qid = $arrayData[$postKey];
		}
		
		$categoryDimID = $catDimQInfo["CategoryDimID"];
		$elementDimID = $catDimQInfo["ElementDimID"];
		$elementIndID = $catDimQInfo["ElementIndID"];
		
		$categoryDimNA = $catDimQInfo["CategoryDimIDNA"];
		$elementDimNA = $catDimQInfo["ElementDimIDNA"];

		//Si resulta igual el questionID con el valor de qCatDimValQID
		//se procede a recolectar los valores
		if($catDimQuestionKey==$qid)
		{
			$qCatDimValue = $categoryDimNA;
			$postKey = "qCatDimVal";
				
			if(isset($arrayData[$postKey]))
			{
				$qCatDimValue = $arrayData[$postKey];
			}
			
			$qElemDimValue = $elementDimNA;
			$postKey = "qElemDimVal";
				
			if(isset($arrayData[$postKey]))
			{
				$qElemDimValue = $arrayData[$postKey];
			}
			
			$qElemDimValResponse = 0;
			$postKey = "qElemDimValResp";
				
			if(isset($arrayData[$postKey]))
			{
				$qElemDimValResponse = $arrayData[$postKey];
				//@JAPR 2012-08-09: Corregido un bug, no estaba validando correctamente por un dato vacio en este punto
				//@JAPR 2012-08-09: Corregido un bug, no estaba validando correctamente por un dato vacio en este punto
				if (trim($qElemDimValResponse) == '') {
					$qElemDimValResponse = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
				}
				//@JAPR
			}

			$catDimFieldNames[$categoryDimID] = "RIDIM_".$categoryDimID."KEY, RIDIM_".$elementDimID."KEY, IND_".$elementIndID;
			$catDimFieldValues[$categoryDimID] = $qCatDimValue.", ".$qElemDimValue.", ".StrIFNULL($qElemDimValResponse);
		}
		else 
		{
			if(!isset($catDimFieldNames[$categoryDimID]))
			{
				//En caso contrario se almacena el valor de No Aplica en los campos de CategoryDimension
				$catDimFieldNames[$categoryDimID] = "RIDIM_".$categoryDimID."KEY, RIDIM_".$elementDimID."KEY, IND_".$elementIndID;
				$catDimFieldValues[$categoryDimID] = $categoryDimNA.", ".$elementDimNA.", ".StrIFNULL(($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
		}
	}
	
	/*************************************************************************************/
	//Obtenemos el numero de preguntas contestadas
	$answeredQuestions = getSurveyAnsweredQuestions($arrayData, $aRepository, $surveyInstance->SurveyID);
	$fieldAnsweredQuestionsInd = $answeredQuestionsIndicator->field_name.$answeredQuestionsIndicator->IndicatorID;
	
	$initialTime = strtotime((substr($currentDate, 0, 10)." ".$startTime));
	$finalTime = strtotime((substr($thisDate, 0, 10)." ".$endTime));
	$totalSeconds = $finalTime - $initialTime;
	$totalMinutes = $totalSeconds/60;
	//@JAPR 2014-10-27: Corregido un bug, el indicador duración sólo se debe grabar en el registro de la sección estándar
	if ($anEntrySectionID != 0) {
		$totalMinutes = "NULL";
	}
	//@JAPR
	$fieldDurationInd = $durationIndicator->field_name.$durationIndicator->IndicatorID;
	
	//Latitude y Longitude
	$fieldLatitudeInd = $latitudeIndicator->field_name.$latitudeIndicator->IndicatorID;
	$fieldLongitudeInd = $longitudeIndicator->field_name.$longitudeIndicator->IndicatorID;
	
	if(isset($arrayData['surveyLatitude'])) 
	{
		$latitude = (double)$arrayData['surveyLatitude'];
	}
	else 
	{
		$latitude = 0;
	}
	
	if(isset($arrayData['surveyLongitude'])) 
	{
		$longitude = (double)$arrayData['surveyLongitude'];
	} 
	else 
	{
		$longitude = 0;
	}
	
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	if (getMDVersion() >= esvAccuracyValues && !is_null($accuracyIndicator)) {
		$fieldAccuracyInd = $accuracyIndicator->field_name.$accuracyIndicator->IndicatorID;
		if(isset($arrayData['surveyAccuracy'])) 
		{
			$accuracy = (double)$arrayData['surveyAccuracy'];
		}
		else 
		{
			$accuracy = 0;
		}
	}
	//@JAPR
	
	$fieldUserDim = $userDimension->Dimension->TableName."KEY";
	
	$countCollection = count($questionCollection->Collection);
	
	$factTable = $anInstanceModel->nom_tabla;
	$dimPeriodo = $anInstanceModel->dim_periodo;
	$dateKeyField = getJoinField($dimPeriodo, "", $descriptKeysCollection, true);
	
	//Esta variable nos permitira llevar un control de los campos que se encuentre repetidos
	//que no se vuelvan a considerar nuevamente en el INSERT, esto aplica para las preguntas
	//de tipo seleccion simple con catalogo que usan el mismo catalogo
	$insertFields = array();
	
	$sqlIndDimValues = "";

	//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
	//Agrega los valores de las dimensiones default al array de recalculo incremental de agregaciones
	if ($gbEnableIncrementalAggregations) {
		if ($surveyInstance->SyncDateDimID > 0) {
			$arrRowDimValues[] = $syncDate;
			$arrRowDimDescValues[] = $syncDate;
		}
		if ($surveyInstance->SyncTimeDimID > 0) {
			$arrRowDimValues[] = $syncTime;
			$arrRowDimDescValues[] = $syncTime;
		}
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n SaveData captureEmail: {$captureEmail}");
			echo("<br>\r\n SaveData arrDefDimValues: ");
			PrintMultiArray($arrDefDimValues);
		}
		
		//@JAPR 2014-11-19: Agregada la dimensión Agenda
		if (getMDVersion() >= esvAgendaDimID && $surveyInstance->AgendaDimID > 0) {
			if (!is_null($agendaKey)) {
				$arrRowDimValues[] = (string) @$arrDefDimValues['AgendaFilter'];
				$arrRowDimDescValues[] = (string) @$arrDefDimValues['AgendaFilter'];
			}
		}
		//@JAPR
		$arrRowDimValues[] = (string) @$arrDefDimValues['UserEMail'];
		$arrRowDimDescValues[] = (string) @$arrDefDimValues['UserEMail'];
		if (is_null($captureEmail) || trim($captureEmail) == '') {
			$arrRowDimValues[] = $gblEFormsNA;
			$arrRowDimDescValues[] = $gblEFormsNA;
		}
		else {
			$arrRowDimValues[] = $captureEmail;
			$arrRowDimDescValues[] = $captureEmail;
		}
		
		$statusDesc = '';
		$sql = "SELECT {$fieldDimStatusDesc} FROM {$tableDimStatus} WHERE {$fieldDimStatusKey} = {$statusKey}";
	    $aRS = $aRepository->DataADOConnection->Execute($sql);
	    if ($aRS || !$aRS->EOF) {
	    	$statusDesc = (string) @$aRS->fields[strtolower($fieldDimStatusDesc)];
	    }
		
		$arrRowDimValues[] = $statusDesc;
		$arrRowDimDescValues[] = $statusDesc;
		$arrRowDimValues[] = $schedulerID;
		$arrRowDimDescValues[] = $schedulerDesc;
		$arrRowDimValues[] = $startDate;
		$arrRowDimDescValues[] = $startDate;
		$arrRowDimValues[] = $endDate;
		$arrRowDimDescValues[] = $endDate;
		$arrRowDimValues[] = $startTime;
		$arrRowDimDescValues[] = $startTime;
		$arrRowDimValues[] = $endTime;
		$arrRowDimDescValues[] = $endTime;

		if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
				$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
			$arrRowDimValues[] = $serverStartDate;
			$arrRowDimDescValues[] = $serverStartDate;
			$arrRowDimValues[] = $serverEndDate;
			$arrRowDimDescValues[] = $serverEndDate;
			$arrRowDimValues[] = $serverStartTime;
			$arrRowDimDescValues[] = $serverStartTime;
			$arrRowDimValues[] = $serverEndTime;
			$arrRowDimDescValues[] = $serverEndTime;
		}
		
		while (count($arrQuestionDimValues) > 0) {
			array_push($arrRowDimValues, array_shift($arrQuestionDimValues));
		}
		while (count($arrQuestionDimValuesDesc) > 0) {
			array_push($arrRowDimDescValues, array_shift($arrQuestionDimValuesDesc));
		}
		
		$arrRowIndicValues[$fieldAnsweredQuestionsInd] = $answeredQuestions;
		$arrRowIndicValues[$fieldDurationInd] = $totalMinutes;
		$arrRowIndicValues[$fieldLatitudeInd] = $latitude;
		$arrRowIndicValues[$fieldLongitudeInd] = $longitude;
		
		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues && !is_null($accuracyIndicator)) {
			$arrRowIndicValues[$fieldAccuracyInd] = $accuracy;
		}
		//@JAPR
	}
	
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	//Se insertarán casi al inicio los nuevos campos, lo cuales ya deben venir con las "," necesarias para funcionar o vacios para no dar problemas
	//@JAPR 2013-03-25: Corregido un bug, el FactKey se debe obtener del auto-incremental en lugar de generarlo antes de insertar
	//$sql = "INSERT INTO ".$factTable." ( FactKey, ".$dateKeyField.$syncDateDimField.$syncTimeDimField.", ".$fieldUserDim.", ".$fieldDimEMailKey.", ".$fieldDimStatusKey.", ".$fieldDimSchedulerKey.", ".$startDateDimField.", ".$endDateDimField.", ".$startTimeDimField.", ".$endTimeDimField.", ".$fieldAnsweredQuestionsInd.", ".$fieldDurationInd.", ".$fieldLatitudeInd.", ".$fieldLongitudeInd.", ";
	$sql = "INSERT INTO ".$factTable." ( ".$dateKeyField.$syncDateDimField.$syncTimeDimField.", ".$fieldUserDim.", ".$fieldDimEMailKey.", ".$fieldDimStatusKey.", ".$fieldDimSchedulerKey.", ".$startDateDimField.", ".$endDateDimField.", ".$startTimeDimField.", ".$endTimeDimField.", ".$fieldAnsweredQuestionsInd.", ".$fieldDurationInd.", ".$fieldLatitudeInd.", ".$fieldLongitudeInd.", ";
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	if (getMDVersion() >= esvAccuracyValues && !is_null($accuracyIndicator)) {
		$sql .= $fieldAccuracyInd.", ";
	}
	//@JAPR 2014-11-20: Agregada la dimensión Agenda
	if (getMDVersion() >= esvAgendaDimID) {
		if (!is_null($agendaKey)) {
			$sql .= $fieldDimAgendaKey.", ";
		}
	}
	//@JAPR

	/*@MABH20121205*/
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$sql .= $serverStartDateDimField.", ".$serverEndDateDimField.", ".$serverStartTimeDimField.", ".$serverEndTimeDimField.", ";
	}
	//@MABH20121205

	//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	$arrProcessedAttribsByCatalog = array();
	//@JAPR 2015-01-08: Corregido un bug en el grabado de múltiples secciones que comparten la misma dimensión (2 o mas Inlines de catálogo), se
	//debe identificar que atributos estaban vacios para que en caso de ser compartidos por otra sección que si grabará datos, se asignen los
	//valores de dicha sección aunque ya estuvieran como NA (#N8X5F2)
	$arrProcessedAttribsByCatalogEmpty = array();
	//@JAPR
	//Procedemos a generar las combinaciones para insertarlas en la tabla de hechos
	//estas preguntas pertenecen a secciones estaticas o preguntas fuera de la seccion dinamica
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		$fieldName = "";
		$fieldValues = "";
		$blnIncludeQuestion = true;
		
		//@JAPR 2012-05-17: Agregados nuevos tipos de pregunta Foto y Firma
		//Las preguntas de Foto y Firma no graban una respuesta, sólo la imagen, por lo tanto se salta esta pregunta completamente
		//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
		//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
		//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
		//Cambiado a un switch para mayor facilidad
		$blnValidQuestion = true;
		switch ($aQuestion->QTypeID) {
			case qtpPhoto:
			case qtpDocument:
			case qtpSignature:
			case qtpSketch:
			case qtpSkipSection:
			case qtpMessage:
			case qtpSync:
			case qtpPassword:
			case qtpAudio:
			case qtpVideo:
			case qtpSection:
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			case qtpExit:
				$blnValidQuestion = false;
				break;
		}
		
		if (!$blnValidQuestion)
		{
			continue;
		}
		//@JAPR
		
		//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
		if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
		{
			//Las preguntas numéricas pueden funcionar como indicador, por lo que opcionalmente se pudiera agregar un 2do campo para esto
			if($aQuestion->IsIndicator==1)
			{
				//$fieldName = $arrayInstancesIndicator[$aQuestion->QuestionID]->field_name;
				$fieldName = $arrayInstancesIndicator[$aQuestion->QuestionID]->field_name.$arrayInstancesIndicator[$aQuestion->QuestionID]->IndicatorID.", ";
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$fieldValues = StrIFNULL($arrayIndValuesByQuestion[$questionKey][0]).", ";
			}
			
			$fieldName.=$arrayInstancesIndDim[$aQuestion->QuestionID]->Dimension->TableName."KEY";
			$fieldValues.=$arrayKeyValuesByQuestion[$questionKey][0];
		}
		else 
		{
			//Las preguntas de selección sencilla que no son de catálogo pueden capturar un Score por respuesta y almacenarlo en un indicador,
			//por lo que opcionalmente se pudiera agregar un 2do campo para esto
			if($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==0 && $aQuestion->IndicatorID>0)
			{
				//$fieldName = $arrayInstancesIndicatorSC[$aQuestion->QuestionID]->field_name;
				$fieldName = $arrayInstancesIndicatorSC[$aQuestion->QuestionID]->field_name.$arrayInstancesIndicatorSC[$aQuestion->QuestionID]->IndicatorID.", ";
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$fieldValues = StrIFNULL($arrayIndValuesSCByQuestion[$questionKey][0]).", ";
			}
			
			//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if($surveyInstance->DissociateCatDimens && $aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1) {
				//Si se usan catálogos desasociados, entonces no se graba sólo un campo de dimensión por pregunta de catálogo, sino que para cada
				//pregunta tiene un campo con su atributo correspondiente (ya que al día de implementación no podía haber 2 o mas preguntas con
				//el mismo atributo) pero además existen dimensiones para todos los atributos previos, así que la 1er pregunta de este catálogo que
				//se procese insertaría el valor para esos atributos previos, mientras que las preguntas subsecuentes sólo deberán insertar valores
				//para los atributos posteriores a la 1er pregunta que aun no se hubieran procesado
				if (!isset($arrProcessedAttribsByCatalog[$aQuestion->CatalogID])) {
					$arrProcessedAttribsByCatalog[$aQuestion->CatalogID] = array();
				}
				
				$intCatMemberID = $aQuestion->CatMemberID;
				//Si el registro que se está grabando es el correspondiente con la sección dinámica, entonces como ya todas las preguntas de
				//catálogo técnicamente se igualaron al mismo key subrrogado (que corresponde ya sea al valor de la página o checkbox dinámico), 
				//se utilizará hasta el atributo máximo usado por la sección dinámica, ya que en teoría no se deberían permitir preguntas de
				//catálogo del mismo de la dinámica después de dicha sección, por lo que nadie hubiera agregado los valores a las dimensiones de
				//sus atributos
				//@JAPR 2013-08-22: Corregido un bug, esta validación está bien pero sólo aplica a preguntas de catálogo del mismo de la dinámica
				if ($hasDynamicSection && $anEntrySectionID == (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty) && $aQuestion->CatalogID == $dynamicSectionCatID) {
					//Sólo en caso de que el atributo de la pregunta fuera posterior al de la sección es como utilizaría el de la pregunta
					if ((int)@$arrDynamicCatalogAttributesColl[$dynamicSectionCatMemberID] > (int)@$arrDynamicCatalogAttributesColl[$intCatMemberID]) {
						$intCatMemberID = (($dynamicSectionWithMultiChoice)?-1:$dynamicSectionCatMemberID);
					}
				}
				
				//@JAPR 2014-05-29: Agregado el tipo de sección Inline
				//Tal como se verifica para las secciones dinámicas, se verifica si este catálogo es usado por alguna sección Inline y si se está
				//grabando en esta iteración precisamente a dicha sección Inline, si es así, entonces se deberá usar hasta el atributo de la
				//sección Inline en lugar del atributo de la pregunta, ya que se asume que están prefiltradas las secciones Inline
				if ($hasInlineSection && $aQuestion->CatalogID == $inlineSectionCatID) {
					//Sólo en caso de que el atributo de la pregunta fuera posterior al de la sección es como utilizaría el de la pregunta
					if ((int)@$arrInlineCatalogAttributesColl[$inlineSectionCatMemberID] > (int)@$arrInlineCatalogAttributesColl[$intCatMemberID]) {
						$intCatMemberID = $inlineSectionCatMemberID;
					}
				}
				
				//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				//Si el valor del key surrogado de catálogo para esta pregunta es el mismo de NA, entonces se hará el grabado a partir del valor
				//específico de esta pregunta porque significaría que actualmente la combinación recibida ya no existe en el catálogo, sin
				//embargo como se está usando el método para desasociar dimensiones, es seguro grabar en las dimensiones independientes cualquier
				//cosa que hubiera llegado
				//Se hace una validación adicional porque si además la pregunta fuera del mismo catálogo de la dinámica, se deberá usar el array
				//de valores completos de dicha sección según el número de registro que se está procesando
				$postKey = "qfield".$aQuestion->QuestionID;
				$strCatQuestionFullValue = trim((string) @$arrayData[$postKey]);
				if (stripos($strCatQuestionFullValue, '_SVSep_') === false) {
					//Si no tiene el formato de una respuesta de pregunta simple choice de catálogo, entonces no se envía para que funcione de la
					//manera original y vaya al catálogo a buscar los datos
					//@JAPR 2013-08-21: Corregido un bug, durante la edición cuando se agregan nuevos registros, se debe
					//validar para no usar NOCHANGE sino el valor de catálogo correcto
					//En caso de venir el valor NOCHANGE y tratarse de una edición, se deberá leer el valor de la combinación original para que
					//continue con ese valor el resto del proceso, de lo contrario grabaría el valor de NA como respuesta
					if ($blnEdit && $entryID > 0 && $strCatQuestionFullValue == "NOCHANGE" && isset($arrayData[$postKey.'Cat'])) {
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							echo("<br>\r\nUsing catalog combination for question {$aQuestion->QuestionID} while saving a new record");
							echo("<br>\r\nOriginal value == {$strCatQuestionFullValue}");
							echo("<br>\r\nNew value == ".trim((string) @$arrayData[$postKey.'Cat']));
						}
						$strCatQuestionFullValue = trim((string) @$arrayData[$postKey.'Cat']);
					}
					else {
						$strCatQuestionFullValue = '';
					}
					//@JAPR
				}
				
				$intDynamicRecNum = -1;
				if ($anEntrySectionID == (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty)) {
					//Sólo se envía este parámetro si realmente es el grabado de un registro de la sección dinámica
					$intDynamicRecNum = $aDynamicRecNum;
				}
				
				//@JAPR 2014-05-29: Agregado el tipo de sección Inline
				//En las secciones Inline se hace el mismo proceso que las dinámicas, sólo que aquí si existe la posibilidad de agregar preguntas
				//del mismo catálogo posteriores a la sección, así que sobreescribe el valor de la pregunta sólo si el atributo de esta es menor
				//o igual (generalmente igual, porque arriba se validó) al de la sección, no si es posterior
				$intDataSectionID = -1;
				if ($hasInlineSection) {
					//Sólo en caso de que el atributo de la pregunta fuera posterior al de la sección es como utilizaría el de la pregunta
					if ((int)@$arrInlineCatalogAttributesColl[$inlineSectionCatMemberID] >= (int)@$arrInlineCatalogAttributesColl[$intCatMemberID]) {
						//Sólo se envía este parámetro si realmente es el grabado de un registro de la sección inline
						$intDynamicRecNum = $aDynamicRecNum;
						$intDataSectionID = $inlineSectionID;
					}
				}
				//@JAPR
				
				//@JAPR 2014-05-29: Agregado el tipo de sección Inline
				//Agregado el parámetro $aSectionID
				$arrCatalogValues = getCatValueArrayForSurveyField($aRepository, $surveyInstance, $arrayKeyValuesByQuestion[$questionKey], $aQuestion->CatalogID, $intCatMemberID, $intDynamicRecNum, $strCatQuestionFullValue, 0, 0, $intDataSectionID);
				$arrLastCatClaDescripFlipped = array_flip($arrLastCatClaDescrip);
				if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
					foreach ($arrCatalogValues as $aClaDescrip => $intCatValueKey) {
						if (isset($arrProcessedAttribsByCatalog[$aQuestion->CatalogID][$aClaDescrip])) {
							//Este atributo ya se había procesado para este registro, por lo tanto heredará el valor que la pregunta anterior del
							//mismo catálogo venía arrastrando pues todas las preguntas del mismo catálogo están ligadas entre si
							continue;
						}
						
						//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
						if ($gbEnableIncrementalAggregations) {
							if (!isset($arrCatalogDimValuesByCatalog[$aQuestion->CatalogID])) {
								$arrCatalogDimValuesByCatalog[$aQuestion->CatalogID] = array();
							}
							
							$intIndex = @$arrLastCatClaDescripFlipped[$aClaDescrip];
							$strDimValue = '';
							if (!is_null($intIndex)) {
								$strDimValue = (string) @$arrLastCatValues[$intIndex];
							}
							if (trim($strDimValue) == '') {
								$strDimValue = "*NA";
							}
							$arrCatalogDimValuesByCatalog[$aQuestion->CatalogID][] = $strDimValue;
							//$arrRowDimValues[] = $strDimValue;
							//$arrRowDimDescValues[] = $strDimValue;
						}
						//@JAPR
						
						$strDimFieldKey = "RIDIM_".$aClaDescrip."KEY";
						$fieldName = $strDimFieldKey;
						$intDimFieldValueKey = ((int)@$intCatValueKey < 0)?1:$intCatValueKey;
						$fieldValues = $intDimFieldValueKey;
						$arrProcessedAttribsByCatalog[$aQuestion->CatalogID][$aClaDescrip] = $intDimFieldValueKey;
						$sql.=$fieldName.", ";
						$sqlIndDimValues.=$fieldValues.", ";
					}
				}
				$blnIncludeQuestion = false;
			}
			else {
				$fieldName.=$arrayInstancesIndDim[$aQuestion->QuestionID]->Dimension->TableName."KEY";
				$fieldValues.=$arrayKeyValuesByQuestion[$questionKey][0];
			}
		}
		
		if(!isset($insertFields[$fieldName]))
		{
			if ($blnIncludeQuestion) {
				$sql.=$fieldName;
				$sqlIndDimValues.=$fieldValues;
			}
		}
		
		if($questionKey!=($countCollection-1))
		{
			if(!isset($insertFields[$fieldName]))
			{
				//Validado para que no agregue 2 comas consecutivas sin campos
				if (substr($sql, -2, 2) != ", ") {
					$sql.=", ";
					$sqlIndDimValues.=", ";
				}
			}
		}
		else 
		{
			//Si es la ultima posicion de esta coleccion
			//y dicho campo ya esta agregado previamente 
			//entonces se debe quitar la ultima coma
			//if(isset($insertFields[$fieldName]))
			if(substr($sql, -2, 2) == ", ")
			{
				$sql = substr($sql, 0, -2);
				$sqlIndDimValues = substr($sqlIndDimValues, 0, -2);
			}
		}
		
		$insertFields[$fieldName] = $fieldName;
	}
	
	//@JAPR 2015-01-08: Corregido un bug en el grabado de múltiples secciones que comparten la misma dimensión (2 o mas Inlines de catálogo), se
	//debe identificar que atributos estaban vacios para que en caso de ser compartidos por otra sección que si grabará datos, se asignen los
	//valores de dicha sección aunque ya estuvieran como NA (#N8X5F2)
	//Como ahora se pueden sobreescribir atributos (dimensiones) de catálogos que son compartidos en diferentes secciones, ya no se puede
	//modificar directamente el SQL al momento de procesarlo, en lugar de eso se va a generar un array que al final después de procesar todas
	//las secciones será usado para sobreescribir el SQL, de esta manera secciones posteriores que si usaron el atributo con un valor no vacio
	//podrán sobreescribir el valor del atributo en cuestión
	$arrSQLAttribFields = array();
	$arrSQLAttribVals = array();
	$arrCatalogDimValuesByCatalogMemberID = array();
	
	//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Justo después de procesar las preguntas simple choice se verifica si este registro no pertenece a la sección dinámica en caso de haber
	//alguna, si fuera el caso se tienen que procesar las dimensiones de los atributos que son parte de la sección dinámica pero asignando el
	//valor de NA ya que para este registro no se utilizan
	if ($surveyInstance->DissociateCatDimens && ($dynamicSectionID + $dynamicSectionIDEmpty > 0)) {
		if ($anEntrySectionID != (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty)) {
			//En este caso el registro procesado (que puede ser 0 o el ID de la sección Maestro-detalle) no es el mismo que la sección dinámica,
			//se graban sus atributos con NA
			$arrCatalogValues = getCatValueArrayForSurveyField($aRepository, $surveyInstance, array(1), $dynamicSectionCatID, (($dynamicSectionWithMultiChoice)?-1:$dynamicSectionCatMemberID));
			$arrLastCatClaDescripFlipped = array_flip($arrLastCatClaDescrip);
			if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
				foreach ($arrCatalogValues as $aClaDescrip => $intCatValueKey) {
					if (isset($arrProcessedAttribsByCatalog[$dynamicSectionCatID][$aClaDescrip])) {
						//Este atributo ya se había procesado para este registro, por lo tanto heredará el valor que la pregunta anterior del
						//mismo catálogo venía arrastrando pues todas las preguntas del mismo catálogo están ligadas entre si
						continue;
					}
					
					//@JAPR 2015-01-08: Corregido un bug en el grabado de múltiples secciones que comparten la misma dimensión (2 o mas Inlines de catálogo), se
					//debe identificar que atributos estaban vacios para que en caso de ser compartidos por otra sección que si grabará datos, se asignen los
					//valores de dicha sección aunque ya estuvieran como NA (#N8X5F2)
					//Como ahora se pueden sobreescribir atributos (dimensiones) de catálogos que son compartidos en diferentes secciones, ya no se puede
					//agregar la descripción al array de agregaciones incrementales directamente, en lugar de eso se va a generar un array que al final después 
					//de procesar todas las secciones será usado para agregar en el array de agregaciones incrementales, de esta manera secciones posteriores que
					//si usaron el atributo con un valor no vacio podrán sobreescribir el valor del atributo en cuestión. Para las agregacioens incrementales
					//el orden es indispensable, así que se asume que al momento de llegar a este punto ya se procesaron todas las preguntas
					//simple choice, por tanto lo único que puede restar es agregar los atributos posteriores a dichas preguntas, así que es válido
					//primero procesarlos indexados por el ID de atributos en el array temporal, y al final recorrer dicho array para agregarlos en orden en
					//el array de las agregaciones incrementales
					//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
					if ($gbEnableIncrementalAggregations) {
						//@JAPR 2014-05-31: Corregido un bug, estaba usando una instancia de pregunta en lugar de directamente el catálogo de la sección
						if (!isset($arrCatalogDimValuesByCatalog[$dynamicSectionCatID])) {
							$arrCatalogDimValuesByCatalog[$dynamicSectionCatID] = array();
						}
						if (!isset($arrCatalogDimValuesByCatalogMemberID[$dynamicSectionCatID])) {
							$arrCatalogDimValuesByCatalogMemberID[$dynamicSectionCatID] = array();
						}
						
						$intIndex = @$arrLastCatClaDescripFlipped[$aClaDescrip];
						$strDimValue = '';
						if (!is_null($intIndex)) {
							$strDimValue = (string) @$arrLastCatValues[$intIndex];
						}
						if (trim($strDimValue) == '') {
							$strDimValue = "*NA";
						}
						//$arrCatalogDimValuesByCatalog[$dynamicSectionCatID][] = $strDimValue;
						$arrCatalogDimValuesByCatalogMemberID[$dynamicSectionCatID][$aClaDescrip] = $strDimValue;
						//$arrRowDimValues[] = $strDimValue;
						//$arrRowDimDescValues[] = $strDimValue;
					}
					
					//@JAPR 2015-01-08: Corregido un bug en el grabado de múltiples secciones que comparten la misma dimensión (2 o mas Inlines de catálogo), se
					//debe identificar que atributos estaban vacios para que en caso de ser compartidos por otra sección que si grabará datos, se asignen los
					//valores de dicha sección aunque ya estuvieran como NA (#N8X5F2)
					//Como ahora se pueden sobreescribir atributos (dimensiones) de catálogos que son compartidos en diferentes secciones, ya no se puede
					//modificar directamente el SQL al momento de procesarlo, en lugar de eso se va a generar un array que al final después de procesar todas
					//las secciones será usado para sobreescribir el SQL, de esta manera secciones posteriores que si usaron el atributo con un valor no vacio
					//podrán sobreescribir el valor del atributo en cuestión
					//@JAPR 2013-08-29: Corregido un bug, si la última pregunta no multiple-choice, matriz o categoría de dimensión del ciclo previo
					//NO era una pregunta que grabara dimensión (por ejemplo una Foto o Firma) y se encontraba en una encuesta con sección dinámica,
					//entrar a este código para grabar en el registro de las estándars provocana una doble "," porque esa última pregunta hubiera 
					//saltado el final del for previo por lo que no se hubiera quitado la "," arrastrada
					//$strAnd = '';
					//if (substr($sql, -2, 2) != ", ") {
					//	$strAnd = ', ';
					///}
					$strDimFieldKey = "RIDIM_".$aClaDescrip."KEY";
					$fieldName = $strDimFieldKey;
					$intDimFieldValueKey = ((int)@$intCatValueKey < 0)?1:$intCatValueKey;
					$fieldValues = $intDimFieldValueKey;
					$arrProcessedAttribsByCatalog[$dynamicSectionCatID][$aClaDescrip] = $intDimFieldValueKey;
					//$sql .= $strAnd.$fieldName;
					//$sqlIndDimValues .= $strAnd.$fieldValues;
					if (!isset($arrSQLAttribFields[$dynamicSectionCatID])) {
						$arrSQLAttribFields[$dynamicSectionCatID] = array();
					}
					$arrSQLAttribFields[$dynamicSectionCatID][$aClaDescrip] = $fieldName;
					if (!isset($arrSQLAttribVals[$dynamicSectionCatID])) {
						$arrSQLAttribVals[$dynamicSectionCatID] = array();
					}
					$arrSQLAttribVals[$dynamicSectionCatID][$aClaDescrip] = $fieldValues;
					//Marca este atributo como procesado pero con un valor de NA
					if (!isset($arrProcessedAttribsByCatalogEmpty[$dynamicSectionCatID])) {
						$arrProcessedAttribsByCatalogEmpty[$dynamicSectionCatID] = array();
					}
					$arrProcessedAttribsByCatalogEmpty[$dynamicSectionCatID][$aClaDescrip] = true;
					//@JAPR
				}
			}
		}
	}
	
	//@JAPR 2014-05-29: Agregado el tipo de sección Inline
	//Si existen secciones Inline, hay que hacer un proceso parecido al de arriba de la sección dinámica, es decir, recorrer todos los casos de
	//secciones Inline y completar sus atributos con el valor de NA, sólo que a diferencia de las secciones dinámicas, aquí incluso si el registro
	//actual pertenece a una sección Inline, se tiene que hacer este proceso porque podría haber mas de una sección Inline que utilice el mismo
	//catálogo con diferente atributo (aunque sería raro), si es el caso, entonces algunas de esas secciones requerirían completar sus atributos
	//con valor de NA porque la que se estuviera capturando solo llegaría hasta uno específico, mientras que aquellas que simplemente pertenecen a
	//otro catálogo hay que grabarlas con todos usando NA. Para este punto, inclusive si cada sección tuviera alguna pregunta simple choice del
	//mismo catálogo de la sección Inline, dicha pregunta ya habría grabado en todos sus atributos arriba, por lo que aquí no se reprocesarían ya
	//que se dejaron marcados en el ciclo de preguntas previo
	if ($surveyInstance->DissociateCatDimens && count($arrInlineSectionIDs) > 0) {
		//En este caso, si el registro que se graba es parte de una sección Inline, se intentará grabar con los datos del propio registro Inline
		//que generó la página, aquí sucederán una de dos cosas:
		//1- Si no hay mas preguntas simple choice dentro de la sección, entonces se grabarán todos los atributos con la página exacta de este
		//registro, pero faltarían todos los atributos posteriores de otras posibles secciones Inline, así que de todas formas se tiene que reprocesar
		//este mismo catálogo pero con valor de NA
		//2- Si hubiera al menos alguna pregunta del mismo catálogo de esta sección dentro de la misma sección, el proceso de arriba que graba las
		//respuestas de las preguntas ya la habría procesado grabando todos los atributos correctamente, así que sólo restaría procesar con NA los
		//posibles atributos que aun estuvieran pendientes
		
		//Sólo se están procesando secciones con catálogo, las demas no son necesarias
		foreach ($arrInlineSectionIDs as $intSectionID => $strEmpty) {
			//@JAPR 2014-06-24: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
			//Si no hay un catálogo para esta sección Inline entonces la salta completamente pues no tiene dimensiones asociadas
			$intSectionCatalogID = (int) @$arrInlineSectionCatID[$intSectionID];
			if ($intSectionCatalogID <= 0) {
				continue;
			}
			//@JAPR
			
			if ($anEntrySectionID == $intSectionID) {
				//Se trata de la sección que está grabando, así que aplica lo mencionado en los dos puntos de arriba, enviando vacio como valor
				//directo y null como array de respuesta, para forzar a utilizar directamente el valor de la sección mediante $aDynamicRecNum
				$arrCatalogValues = getCatValueArrayForSurveyField($aRepository, $surveyInstance, null, $intSectionCatalogID, $arrInlineSectionCatMemberID[$intSectionID], $aDynamicRecNum, '', 0, 0, $intSectionID);
				$arrLastCatClaDescripFlipped = array_flip($arrLastCatClaDescrip);
				if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
					foreach ($arrCatalogValues as $aClaDescrip => $intCatValueKey) {
						//@JAPR 2014-05-31: Corregido un bug, estaba usando una instancia de pregunta en lugar de directamente el catálogo de la sección
						//@JAPR 2015-01-08: Corregido un bug en el grabado de múltiples secciones que comparten la misma dimensión (2 o mas Inlines de catálogo), se
						//debe identificar que atributos estaban vacios para que en caso de ser compartidos por otra sección que si grabará datos, se asignen los
						//valores de dicha sección aunque ya estuvieran como NA (#N8X5F2)
						//En este caso, como se trata de una sección que si grabará datos por ser la procesada, no sólo se verifica que no
						//se hubiera utilizado previamente el atributo en cuestión, sino que no se hubiera utilizado para grabara sólo NA, ya que
						//esta sección si grabará datos y se tiene como premisa que dos secciones que graban datos no pueden co-existir en el
						//mismo registro al mismo nivel de atributo, así que si está entrando a esta sección y ya había algo grabado, se asume
						//que tiene que ser algo que asignó exclusivamente NA (ejemplo, con 2 secciones Inline que usan el mismo catálogo, si
						//este registro corresponde a la segunda sección en el orden, ya habría pasado por este mismo ciclo para la primera
						//sección pero no habría entrado a este "if ($anEntrySectionID == $intSectionID) {" sino continuando así que habría grabado
						//sólo NA para estos mismos atributos, por tanto si es válido que al procesar la segunda sección aunque ya hubieran sido
						//utilizados los atributos, los puede sobreescribir porque ella si grabará un valor en este registro)
						$blnIsProcessedAttribEmpty = (bool) @$arrProcessedAttribsByCatalogEmpty[$intSectionCatalogID][$aClaDescrip];
						if (isset($arrProcessedAttribsByCatalog[$intSectionCatalogID][$aClaDescrip]) && !$blnIsProcessedAttribEmpty) {
							//Este atributo ya se había procesado para este registro, por lo tanto heredará el valor que la pregunta anterior del
							//mismo catálogo venía arrastrando pues todas las preguntas del mismo catálogo están ligadas entre si
							continue;
						}
						
						//@JAPR 2015-01-08: Corregido un bug en el grabado de múltiples secciones que comparten la misma dimensión (2 o mas Inlines de catálogo), se
						//debe identificar que atributos estaban vacios para que en caso de ser compartidos por otra sección que si grabará datos, se asignen los
						//valores de dicha sección aunque ya estuvieran como NA (#N8X5F2)
						//Como ahora se pueden sobreescribir atributos (dimensiones) de catálogos que son compartidos en diferentes secciones, ya no se puede
						//agregar la descripción al array de agregaciones incrementales directamente, en lugar de eso se va a generar un array que al final después 
						//de procesar todas las secciones será usado para agregar en el array de agregaciones incrementales, de esta manera secciones posteriores que
						//si usaron el atributo con un valor no vacio podrán sobreescribir el valor del atributo en cuestión. Para las agregacioens incrementales
						//el orden es indispensable, así que se asume que al momento de llegar a este punto ya se procesaron todas las preguntas
						//simple choice, por tanto lo único que puede restar es agregar los atributos posteriores a dichas preguntas, así que es válido
						//primero procesarlos indexados por el ID de atributos en el array temporal, y al final recorrer dicho array para agregarlos en orden en
						//el array de las agregaciones incrementales
						//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
						if ($gbEnableIncrementalAggregations) {
							//@JAPR 2014-05-31: Corregido un bug, estaba usando una instancia de pregunta en lugar de directamente el catálogo de la sección
							if (!isset($arrCatalogDimValuesByCatalog[$intSectionCatalogID])) {
								$arrCatalogDimValuesByCatalog[$intSectionCatalogID] = array();
							}
							if (!isset($arrCatalogDimValuesByCatalogMemberID[$intSectionCatalogID])) {
								$arrCatalogDimValuesByCatalogMemberID[$intSectionCatalogID] = array();
							}
							
							$intIndex = @$arrLastCatClaDescripFlipped[$aClaDescrip];
							$strDimValue = '';
							if (!is_null($intIndex)) {
								$strDimValue = (string) @$arrLastCatValues[$intIndex];
							}
							if (trim($strDimValue) == '') {
								$strDimValue = "*NA";
							}
							//$arrCatalogDimValuesByCatalog[$intSectionCatalogID][] = $strDimValue;
							$arrCatalogDimValuesByCatalogMemberID[$intSectionCatalogID][$aClaDescrip] = $strDimValue;
							//$arrRowDimValues[] = $strDimValue;
							//$arrRowDimDescValues[] = $strDimValue;
						}
						
						//@JAPR 2015-01-08: Corregido un bug en el grabado de múltiples secciones que comparten la misma dimensión (2 o mas Inlines de catálogo), se
						//debe identificar que atributos estaban vacios para que en caso de ser compartidos por otra sección que si grabará datos, se asignen los
						//valores de dicha sección aunque ya estuvieran como NA (#N8X5F2)
						//Como ahora se pueden sobreescribir atributos (dimensiones) de catálogos que son compartidos en diferentes secciones, ya no se puede
						//modificar directamente el SQL al momento de procesarlo, en lugar de eso se va a generar un array que al final después de procesar todas
						//las secciones será usado para sobreescribir el SQL, de esta manera secciones posteriores que si usaron el atributo con un valor no vacio
						//podrán sobreescribir el valor del atributo en cuestión
						//$strAnd = '';
						//if (substr($sql, -2, 2) != ", ") {
						//	$strAnd = ', ';
						//}
						$strDimFieldKey = "RIDIM_".$aClaDescrip."KEY";
						$fieldName = $strDimFieldKey;
						$intDimFieldValueKey = ((int)@$intCatValueKey < 0)?1:$intCatValueKey;
						$fieldValues = $intDimFieldValueKey;
						$arrProcessedAttribsByCatalog[$intSectionCatalogID][$aClaDescrip] = $intDimFieldValueKey;
						//$sql .= $strAnd.$fieldName;
						//$sqlIndDimValues .= $strAnd.$fieldValues;
						if (!isset($arrSQLAttribFields[$intSectionCatalogID])) {
							$arrSQLAttribFields[$intSectionCatalogID] = array();
						}
						$arrSQLAttribFields[$intSectionCatalogID][$aClaDescrip] = $fieldName;
						if (!isset($arrSQLAttribVals[$intSectionCatalogID])) {
							$arrSQLAttribVals[$intSectionCatalogID] = array();
						}
						$arrSQLAttribVals[$intSectionCatalogID][$aClaDescrip] = $fieldValues;
						//Desmarca este atributo como procesado con un valor de NA, ya que acaba de asignarsele una respuesta para esta sección
						if (!isset($arrProcessedAttribsByCatalogEmpty[$intSectionCatalogID])) {
							$arrProcessedAttribsByCatalogEmpty[$intSectionCatalogID] = array();
						}
						if (isset($arrProcessedAttribsByCatalogEmpty[$intSectionCatalogID][$aClaDescrip])) {
							unset($arrProcessedAttribsByCatalogEmpty[$intSectionCatalogID][$aClaDescrip]);
						}
						//@JAPR
					}
				}
			}
			
			//En este punto simplemente almacena el valor de NA para cualquier atributo que pudiera restar en este catálogo
			$arrCatalogValues = getCatValueArrayForSurveyField($aRepository, $surveyInstance, array(1), $arrInlineSectionCatID[$intSectionID], $arrInlineSectionCatMemberID[$intSectionID], -1, '', 0, 0, $intSectionID);
			$arrLastCatClaDescripFlipped = array_flip($arrLastCatClaDescrip);
			if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
				foreach ($arrCatalogValues as $aClaDescrip => $intCatValueKey) {
					//@JAPR 2014-05-31: Corregido un bug, estaba usando el catálogo dinámico en lugar de directamente el catálogo de la sección
					if (isset($arrProcessedAttribsByCatalog[$intSectionCatalogID][$aClaDescrip])) {
						//Este atributo ya se había procesado para este registro, por lo tanto heredará el valor que la pregunta anterior del
						//mismo catálogo venía arrastrando pues todas las preguntas del mismo catálogo están ligadas entre si
						continue;
					}
					
					//@JAPR 2015-01-08: Corregido un bug en el grabado de múltiples secciones que comparten la misma dimensión (2 o mas Inlines de catálogo), se
					//debe identificar que atributos estaban vacios para que en caso de ser compartidos por otra sección que si grabará datos, se asignen los
					//valores de dicha sección aunque ya estuvieran como NA (#N8X5F2)
					//Como ahora se pueden sobreescribir atributos (dimensiones) de catálogos que son compartidos en diferentes secciones, ya no se puede
					//agregar la descripción al array de agregaciones incrementales directamente, en lugar de eso se va a generar un array que al final después 
					//de procesar todas las secciones será usado para agregar en el array de agregaciones incrementales, de esta manera secciones posteriores que
					//si usaron el atributo con un valor no vacio podrán sobreescribir el valor del atributo en cuestión. Para las agregacioens incrementales
					//el orden es indispensable, así que se asume que al momento de llegar a este punto ya se procesaron todas las preguntas
					//simple choice, por tanto lo único que puede restar es agregar los atributos posteriores a dichas preguntas, así que es válido
					//primero procesarlos indexados por el ID de atributos en el array temporal, y al final recorrer dicho array para agregarlos en orden en
					//el array de las agregaciones incrementales
					//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
					if ($gbEnableIncrementalAggregations) {
						//@JAPR 2014-05-31: Corregido un bug, estaba usando una instancia de pregunta en lugar de directamente el catálogo de la sección
						if (!isset($arrCatalogDimValuesByCatalog[$intSectionCatalogID])) {
							$arrCatalogDimValuesByCatalog[$intSectionCatalogID] = array();
						}
						if (!isset($arrCatalogDimValuesByCatalogMemberID[$intSectionCatalogID])) {
							$arrCatalogDimValuesByCatalogMemberID[$intSectionCatalogID] = array();
						}
						
						$intIndex = @$arrLastCatClaDescripFlipped[$aClaDescrip];
						$strDimValue = '';
						if (!is_null($intIndex)) {
							$strDimValue = (string) @$arrLastCatValues[$intIndex];
						}
						if (trim($strDimValue) == '') {
							$strDimValue = "*NA";
						}
						//$arrCatalogDimValuesByCatalog[$intSectionCatalogID][] = $strDimValue;
						$arrCatalogDimValuesByCatalogMemberID[$intSectionCatalogID][$aClaDescrip] = $strDimValue;
						//$arrRowDimValues[] = $strDimValue;
						//$arrRowDimDescValues[] = $strDimValue;
					}
					
					//@JAPR 2015-01-08: Corregido un bug en el grabado de múltiples secciones que comparten la misma dimensión (2 o mas Inlines de catálogo), se
					//debe identificar que atributos estaban vacios para que en caso de ser compartidos por otra sección que si grabará datos, se asignen los
					//valores de dicha sección aunque ya estuvieran como NA (#N8X5F2)
					//Como ahora se pueden sobreescribir atributos (dimensiones) de catálogos que son compartidos en diferentes secciones, ya no se puede
					//modificar directamente el SQL al momento de procesarlo, en lugar de eso se va a generar un array que al final después de procesar todas
					//las secciones será usado para sobreescribir el SQL, de esta manera secciones posteriores que si usaron el atributo con un valor no vacio
					//podrán sobreescribir el valor del atributo en cuestión
					//$strAnd = '';
					//if (substr($sql, -2, 2) != ", ") {
					//	$strAnd = ', ';
					//}
					$strDimFieldKey = "RIDIM_".$aClaDescrip."KEY";
					$fieldName = $strDimFieldKey;
					$intDimFieldValueKey = ((int)@$intCatValueKey < 0)?1:$intCatValueKey;
					$fieldValues = $intDimFieldValueKey;
					$arrProcessedAttribsByCatalog[$intSectionCatalogID][$aClaDescrip] = $intDimFieldValueKey;
					//$sql .= $strAnd.$fieldName;
					//$sqlIndDimValues .= $strAnd.$fieldValues;
					if (!isset($arrSQLAttribFields[$intSectionCatalogID])) {
						$arrSQLAttribFields[$intSectionCatalogID] = array();
					}
					$arrSQLAttribFields[$intSectionCatalogID][$aClaDescrip] = $fieldName;
					if (!isset($arrSQLAttribVals[$intSectionCatalogID])) {
						$arrSQLAttribVals[$intSectionCatalogID] = array();
					}
					$arrSQLAttribVals[$intSectionCatalogID][$aClaDescrip] = $fieldValues;
					//Marca este atributo como procesado pero con un valor de NA
					if (!isset($arrProcessedAttribsByCatalogEmpty[$intSectionCatalogID])) {
						$arrProcessedAttribsByCatalogEmpty[$intSectionCatalogID] = array();
					}
					$arrProcessedAttribsByCatalogEmpty[$intSectionCatalogID][$aClaDescrip] = true;
					//@JAPR
				}
			}
		}
	}
	
	//@JAPR 2015-01-08: Corregido un bug en el grabado de múltiples secciones que comparten la misma dimensión (2 o mas Inlines de catálogo), se
	//debe identificar que atributos estaban vacios para que en caso de ser compartidos por otra sección que si grabará datos, se asignen los
	//valores de dicha sección aunque ya estuvieran como NA (#N8X5F2)
	
	//Como ahora se pueden sobreescribir atributos (dimensiones) de catálogos que son compartidos en diferentes secciones, ya no se puede
	//agregar la descripción al array de agregaciones incrementales directamente, en lugar de eso se va a generar un array que al final después 
	//de procesar todas las secciones será usado para agregar en el array de agregaciones incrementales, de esta manera secciones posteriores que
	//si usaron el atributo con un valor no vacio podrán sobreescribir el valor del atributo en cuestión. Para las agregacioens incrementales
	//el orden es indispensable, así que se asume que al momento de llegar a este punto ya se procesaron todas las preguntas
	//simple choice, por tanto lo único que puede restar es agregar los atributos posteriores a dichas preguntas, así que es válido
	//primero procesarlos indexados por el ID de atributos en el array temporal, y al final recorrer dicho array para agregarlos en orden en
	//el array de las agregaciones incrementales
	foreach ($arrCatalogDimValuesByCatalogMemberID as $intCatalogID => $arrCatalogDimValues) {
		foreach ($arrCatalogDimValues as $aClaDescrip => $strDimValue) {
			if (!isset($arrCatalogDimValuesByCatalog[$intCatalogID])) {
				$arrCatalogDimValuesByCatalog[$intCatalogID] = array();
			}
			$arrCatalogDimValuesByCatalog[$intCatalogID][] = $strDimValue;
		}
	}
	
	//Como ahora se pueden sobreescribir atributos (dimensiones) de catálogos que son compartidos en diferentes secciones, ya no se puede
	//modificar directamente el SQL al momento de procesarlo, en lugar de eso se va a generar un array que al final después de procesar todas
	//las secciones será usado para sobreescribir el SQL, de esta manera secciones posteriores que si usaron el atributo con un valor no vacio
	//podrán sobreescribir el valor del atributo en cuestión
	foreach ($arrProcessedAttribsByCatalog as $intCatalogID => $arrFieldValues) {
		foreach ($arrFieldValues as $aClaDescrip => $intDimFieldValueKeyLastSChoice) {
			//@JAPR 2015-01-13: Corregido un bug, al agregar esta validación se integró mal, estaba usando el array de atributos procesados por
			//catálogo, pero esos pudieran pertenecer a las preguntas simple choice y no a las secciones, así que no estaban dentro de los
			//arrays que finalmente se usan, por tanto ahora se verificará que si se encuentren en ellos
			if (!isset($arrSQLAttribFields[$intCatalogID][$aClaDescrip])) {
				continue;
			}
			
			$intCatValueKey = (int)@ $arrSQLAttribVals[$intCatalogID][$aClaDescrip];
			$intDimFieldValueKey = ($intCatValueKey < 0)?1:$intCatValueKey;
			//@JAPR
			
			$strAnd = '';
			if (substr($sql, -2, 2) != ", ") {
				$strAnd = ', ';
			}
			
			$fieldName = (string) @$arrSQLAttribFields[$intCatalogID][$aClaDescrip];
			$sql .= $strAnd.$fieldName;
			$fieldValues = $intDimFieldValueKey;
			$sqlIndDimValues .= $strAnd.$fieldValues;
		}
	}
	//@JAPR
	
	//Si todavia existen preguntas de tipo multiple choice de unica dimension entonces si se debe concatenar la coma (,)
	//en las variables $sql y $sqlIndDimValues
	//@JAPR 2012-05-17: Agregados nuevos tipos de pregunta Foto y Firma
	//Validado que ahora pregunte directamente por la coma al final, ya que si la última pregunta fué tipo Foto o Firma, no se hubiera eliminado
	//la coma dentro del ciclo anterior después de la última pregunta que no fuera Foto / Firma así que habría repetido coma en este punto
	if($countUniqueMChoiceQuestionCollection>0 && $countCollection>0 && substr(trim($sql), -1, 1) != ',')
	{
		$sql.=", ";
		$sqlIndDimValues.=", ";
	}

	/*****************************************************************************************************/
	//Procedemos a generar las siguientes combinaciones para insertarlas en la tabla de hechos
	//estas preguntas pertenecen a secciones dinamicas son de tipo MChoice
	foreach ($uniqueMultiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
	{
		$fieldName = "";
		$fieldValues = "";

		if($aQuestion->MCInputType==mpcNumeric)
		{
			//Las preguntas numéricas de opción múltiple pueden funcionar como indicador, por lo que opcionalmente se pudiera agregar un 2do
			//campo para esto
			if($aQuestion->IsIndicatorMC==1)
			{
				//$fieldName = $arrayInstancesMCIndicator[$aQuestion->QuestionID]->field_name;
				$fieldName = $arrayInstancesMCIndicator[$aQuestion->QuestionID]->field_name.$arrayInstancesMCIndicator[$aQuestion->QuestionID]->IndicatorID.", ";
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$fieldValues = StrIFNULL($arrayMCIndValuesByQuestion[$questionKey][0]).", ";
			}
			
			$fieldName.=$arrayInstancesMCIndDim[$aQuestion->QuestionID]->Dimension->TableName."KEY";
			$fieldValues.=$arrayKeyMCValuesByQuestion[$questionKey][0];
		}
		else 
		{
			$fieldName = $arrayInstancesMCIndDim[$aQuestion->QuestionID]->Dimension->TableName."KEY";
			$fieldValues = $arrayKeyMCValuesByQuestion[$questionKey][0];
		}
		
		$sql.=$fieldName;
		$sqlIndDimValues.=$fieldValues;

		if($questionKey!=($countUniqueMChoiceQuestionCollection-1))
		{
			$sql.=", ";
			$sqlIndDimValues.=", ";
		}
	}
	/****************************************************************************************************/
	
	//Si todavia existen preguntas de tipo multiple choice entonces si se debe concatenar la coma (,)
	//en las variables $sql y $sqlIndDimValues
	//@JAPRWarning: Esta condición se debería hacer contra el count($multiChoiceQuestionCollection) ya que la variable $countUniqueMChoiceQuestionCollection
	//es para preguntas múltiples de secciones dinámicas, sin embargo funcionaba por el Or del $countCollection que siempre sería true
	//Validado que ahora pregunte directamente por la coma al final, ya que si la última pregunta fué tipo Foto o Firma, no se hubiera eliminado
	//la coma dentro del ciclo anterior después de la última pregunta que no fuera Foto / Firma así que habría repetido coma en este punto
	if($existMultiOptions==true && ($countUniqueMChoiceQuestionCollection>0 || $countCollection>0) && substr(trim($sql), -1, 1) != ',')
	{
		$sql.=", ";
		$sqlIndDimValues.=", ";
	}
	
	//Todavia seguimos generando las combinaciones para las preguntas de seleccion multiple
	foreach ($multiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
	{
		//Obtenemos la instancia de multiples dimensiones de la pregunta que se esta procesando
		$arrayInstanceMultiDim = $arrayInstancesMultiDim[$aQuestion->QuestionID];
		
		//Obtenemos las respuestas para esa pregunta
		$arrayMultiValuesQuestion = $arrayKeyMultiValuesByQuestion[$questionKey];
		
		//Recorremos las instancias de dimensiones
		foreach ($arrayInstanceMultiDim as $numKey=>$anInstanceDim)
		{
			$fieldName = $anInstanceDim->Dimension->TableName."KEY";
			$fieldValues = $arrayMultiValuesQuestion[$numKey];

			$sql.=$fieldName;
			$sqlIndDimValues.=$fieldValues;
		
			$sql.=", ";
			$sqlIndDimValues.=", ";
		}
		
		//Verificamos si la pregunta es de tipo MCInputType = mpcCheckBox (Checkbox)
		if($aQuestion->MCInputType==mpcCheckBox && $arrayExistMultiChoiceIndicators[$aQuestion->QuestionID]==true)
		{
			//Obtenemos las instancias de multiples indicadores de la pregunta que se esta procesando
			$arrayMultiChoiceIndicators = $arrayInstancesMultiChoiceInds[$aQuestion->QuestionID];
			
			//Obtenemos las respuestas para esa pregunta
			$arrayMultiChoiceValuesQuestion = $arrayMultiChoiceIndValuesByQuestion[$questionKey];
			
			//Recorremos las instancias de indicadores
			foreach ($arrayMultiChoiceIndicators as $numKey=>$anInstanceInd)
			{
				//Verificamos q sea una instancia valida de Indicador
				if(!is_null($anInstanceInd))
				{
					//$fieldName = $anInstanceInd->field_name;
					$fieldName = $anInstanceInd->field_name.$anInstanceInd->IndicatorID;
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					$fieldValues = StrIFNULL($arrayMultiChoiceValuesQuestion[$numKey]);
					
					$sql.=$fieldName;
					$sqlIndDimValues.=$fieldValues;
				
					$sql.=", ";
					$sqlIndDimValues.=", ";
				}
			}
		}
	}
	
	//Si existieron preguntas de tipo multiple choice entonces si se concateno una coma (,) extra
	//en las variables $sql y $sqlIndDimValues y por lo tanto se tiene q eliminar
	//@JAPR 2012-05-22: Agregada validación para que se remueva la última "," dejada posiblemente por preguntas que realmente no contienen 
	//respuesta cuando son la última de la encuesta (tipo foto o firma por ejemplo)
	//Se asumirá que si entró aquí es porque al final había una "," de más, y que los campos concatenados posteriores a este punto siempre
	//anteponen una "," en caso de necesitar agregar mas elementos, por lo que una "," al final en este punto ya no es esperada
	if($existMultiOptions==true || substr(trim($sql), -1, 1) == ',')
	{
		$sql = substr($sql, 0, -2);
		$sqlIndDimValues = substr($sqlIndDimValues, 0, -2);
	}
	//@JAPR
	
	//Metemos los campos de las dimensiones de las preguntas Matrix
	//Y tambien vamos a meter las llaves de las dims de la matrix
	$matrixCollection = BITAMQuestionCollection::NewInstanceWithSimpleChoiceMatrix($aRepository, $surveyInstance->SurveyID);
	$matrixDim = getMatrixDimFields($aRepository, $matrixCollection, $arrayData);
	
	if($matrixDim===false)
	{
		return($gblEFormsErrorMessage);
	}
	
	if($matrixDim['fieldNames'] != "") 
	{
	    $sql .= ", ";
	    $sqlIndDimValues .= ", ";
	}
	$sql .= $matrixDim['fieldNames'];
	$sqlIndDimValues .= $matrixDim['fieldValues'];

	//Agregamos los campos de CategoryDimension
	if(count($catDimFieldNames)>0 && count($catDimFieldValues)>0) 
	{
	    $sql .= ", ";
		//@JAPR 2012-09-10: Validado el caso donde sólo existen preguntas tipo categoría de dimensión
		//Estaba agregando siempre una "," pero si sólo hubiera este tipo de preguntas entonces no era necesario porque en el armado del $sql
		//ya se dejó al final una "," así que sólo se agrega si por lo menos hay algún otro valor
		if (trim($sqlIndDimValues) != '') {
		    $sqlIndDimValues .= ", ";
		}
		//@JAPR
	}
	
	$sql .= implode(", ", $catDimFieldNames);
	$sqlIndDimValues .= implode(", ", $catDimFieldValues);
	
	//@JAPR 2013-11-15: Agregado el grabado incremental de agregaciones
	//Antes de realizar el Insert, tiene que agregar los valores de las preguntas de catálogo en la misma secuencia en que encuentra los catálogos,
	//pero agregando todos los atributos a la vez para cada uno, ya que podrían venir preguntas de diferentes catálogos intercaladas pero dejarlo
	//así sólo complicaría las cosas
	foreach ($arrCatalogDimValuesByCatalog as $intCatalogID => $arrCatalogValues) {
		foreach ($arrCatalogValues as $strDimValue) {
			$arrRowDimValues[] = $strDimValue;
			$arrRowDimDescValues[] = $strDimValue;
		}
	}
	
	//@JAPR 2013-03-25: Corregido un bug, el FactKey se debe obtener del auto-incremental en lugar de generarlo antes de insertar
	$factKey = 0;
	//@JAPR
	if ($handle = @fopen("./log/file_getfactkey_".$aRepository->RepositoryName."_".$surveyInstance->SurveyID.".lck", "w"))
	{
		/* flock regreso con false, por lo tanto ya esta corriendo este proceso. Vamos a esperar a que termine, no tiene caso volverlo a ejecutar */
		if (@flock($handle, LOCK_EX))
		{
			//Verificamos si traemos valor maximo de dimension FactKeyDim, en el dado caso que sea cero el valor que se trae
			//entonces se inserta un nuevo valor a la tabla de dimension y ese valor regresado es el que se utiliza
			//para insertarlo a la tabla de hechos de la encuesta
			if($factKeyDimVal==0)
			{
				//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
				$passValue = getMaxFactKeyDimValue($aRepository, $surveyInstance, $latitude, $longitude, $accuracy);
				
				if($passValue!==false)
				{
					$factKeyDimVal = $passValue;
				}
				else 
				{
					@flock($handle, LOCK_UN);
					return($gblEFormsErrorMessage);
				}
			}
			
			//Obtenemos los campos de la dimension FactKey de la encuesta
			$tableDimFactKey= "RIDIM_".$surveyInstance->FactKeyDimID;
			$fieldDimFactKeyKey = $tableDimFactKey."KEY";
			
			//Insertaremos al ultimo el factkey en la dimension factkey
			//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
			//Se insertarán casi al inicio los nuevos campos, lo cuales ya deben venir con las "," necesarias para funcionar o vacios para no dar problemas
			//@JAPR 2013-03-25: Corregido un bug, el FactKey se debe obtener del auto-incremental en lugar de generarlo antes de insertar
			//$sql.=",".$fieldDimFactKeyKey." ) VALUES ( ".$factKey.", ".$dateKey.$syncDateKey.$syncTimeKey.", ".$userID.", ".$emailKey.", ".$statusKey.", ".$schedulerKey.", ".$startDateKey.", ".$endDateKey.", ".$startTimeKey.", ".$endTimeKey.", ".$answeredQuestions.", ".$totalMinutes.", ".$latitude.", ".$longitude.", ";
			//@JAPR 2014-09-24: Agregada la dimensión de la sección para las que están definidas como Inline y que no requieren de catálogo
			$sql.=",".$fieldDimFactKeyKey." ".$strInlineSectionsDimFields.") VALUES ( ".$dateKey.$syncDateKey.$syncTimeKey.", ".$userID.", ".$emailKey.", ".$statusKey.", ".$schedulerKey.", ".$startDateKey.", ".$endDateKey.", ".$startTimeKey.", ".$endTimeKey.", ".$answeredQuestions.", ".$totalMinutes.", ".$latitude.", ".$longitude.", ";
			//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues && !is_null($accuracyIndicator)) {
				$sql .= $accuracy.",";
			}
			//@JAPR 2014-11-20: Agregada la dimensión Agenda
			if (getMDVersion() >= esvAgendaDimID) {
				if (!is_null($agendaKey)) {
					$sql .= $agendaKey.",";
				}
			}
			//@JAPR
			
			/*@MABH20121205*/
			if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
					$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
				$sql .= $serverStartDateKey.", ".$serverEndDateKey.", ".$serverStartTimeKey.", ".$serverEndTimeKey.", ";
			}
			//@MABH20121205
			
			//@JAPR 2014-03-21: Corregido un bug, si no había ni una sola dimensión de pregunta, se estaba agregando una "," de mas
			if (trim($sqlIndDimValues) != '') {
				$sqlIndDimValues .= ", ";
			}
			//@JAPR 2014-09-24: Agregada la dimensión de la sección para las que están definidas como Inline y que no requieren de catálogo
			$sqlToInsert = $sql.$sqlIndDimValues.$factKeyDimVal." ".$strInlineSectionDimKeys.")";

			//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
			if ($gbEnableIncrementalAggregations) {
				$arrRowDimValues[] = $factKeyDimVal;
				$arrRowDimDescValues[] = $factKeyDimVal;
			}
			
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nsaveData INSERT: ".$sqlToInsert);
				
				if ($gbEnableIncrementalAggregations) {
					echo ("<br>\r\n<br>\r\n Incremental aggregations data arrays: Index == $intComboID");
					echo ("<br>\r\n arrDateValues:");
					PrintMultiArray(@$arrDateValues);
					echo ("<br>\r\n arrDimKeyValues:");
					PrintMultiArray(@$arrDimKeyValues[$intComboID]);
					echo ("<br>\r\n arrDimDescValues:");
					PrintMultiArray(@$arrDimDescValues[$intComboID]);
					echo ("<br>\r\n arrIndicValues:");
					PrintMultiArray(@$arrIndicValues[$intComboID]);
				}
			}
			//@JAPR
			if($aRepository->DataADOConnection->Execute($sqlToInsert) === false)
			{
				if($gblShowErrorSurvey)
				{
					@flock($handle, LOCK_UN);
					die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlToInsert);
				}
				else 
				{
					@flock($handle, LOCK_UN);
					//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
					if ($gbEnableIncrementalAggregations) {
						if (isset($arrRowIndicValues)) {
							unset($arrIndicValues[$intComboID]);
						}
						unset($arrDimKeyValues[$intComboID]);
						unset($arrDimDescValues[$intComboID]);
						unset($arrDateValues[$intComboID]);
					}
					
					$strMsg = "(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlToInsert;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\nsaveData INSERT Error: ".$strMsg);
					}
					//@JAPR
					return ($strMsg);
				}
			}
			//@JAPR 2013-03-25: Corregido un bug, el FactKey se debe obtener del auto-incremental en lugar de generarlo antes de insertar
			else {
				$factKey = (int) @$aRepository->DataADOConnection->_insertid();
				if ($factKey <= 0) {
					if($gblShowErrorSurvey)
					{
						@flock($handle, LOCK_UN);
						die("(".__METHOD__.") ".translate("Error calculating the auto-incremental fact key")." (".$factTable."). ".translate("Executing").": ".$sqlToInsert);	
					}
					else 
					{
						@flock($handle, LOCK_UN);
						return ("(".__METHOD__.") ".translate("Error calculating the auto-incremental fact key")." (".$factTable.").".translate("Executing").": ".$sqlToInsert);
					}
				}
				
				//@JAPR 2013-08-14: Corregido un bug, si se reconstruía la sección dinámica durante una edición, no se estaba guardando como
				//editado el FactKey recien agregado por lo que se terminaban perdiendo todos los datos dinámicos nuevos, así que ahora se
				//agregan al array para conservarlos
				if ($blnEdit && $entryID > 0 && isset($arrEditedFactKeys) && !is_null($arrEditedFactKeys)) {
					$arrEditedFactKeys[$factKey] = $factKey;
				}
				
				//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
				if ($gbEnableIncrementalAggregations) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo ("<br>\r\n<br>\r\n FactKey == {$factKey}");
					}
					
					$intComboID++;
				}
				//@JAPR
			}
			//@JAPR
			
			@flock($handle, LOCK_UN);
		}
		
		@fclose($handle);
	}
	else
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error saving answers.")." ".translate("The writing permissions must be assigned in the log directory for the IIS user.")." ".translate("Please contact your System Administrator"));
		}
		else 
		{
			return("(".__METHOD__.") ".translate("Error saving answers.")." ".translate("The writing permissions must be assigned in the log directory for the IIS user.")." ".translate("Please contact your System Administrator"));
		}
	}

	//Almacenar datos en la tabla paralela
	//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
	$arrFieldValues = array();
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	//@JAPR 2013-01-22: Agregado el parámetro $bSaveSignature para optimizar en que momento se graba la firma
	//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	$intDynamicRecNum = -1;
	if ($anEntrySectionID == (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty)) {
		//Sólo se envía este parámetro si realmente es el grabado de un registro de la sección dinámica
		$intDynamicRecNum = $aDynamicRecNum;
	}
	//@JAPR 2014-05-30: Agregado el tipo de sección Inline
	elseif ($hasInlineSection) {
		//Si se estuviera grabando una sección Inline entonces también se envía este parámetro, ya que esas secciones reutilizan los mismos campos
		//dinámicos de la tabla paralela que usan las secciones dinámicas
		$intDynamicRecNum = $aDynamicRecNum;
	}
	
	$strResult = saveDataInSurveyTable($aRepository, $surveyInstance, $arrayData, $thisDate, $factKey, $factKeyDimVal, $folioID, $arrFieldValues, $anEntrySectionID, $anEntryCatDimID, $aDynamicPageDSC, $bSaveSignature, $intDynamicRecNum);
	//@JAPR
	
	//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
	//Almacena las acciones en el eBavel
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\nResult before action generation?: {$strResult}");
	}
	@logeBavelString("Result before action generation?: {$strResult}");
	
	if($strResult=="" || $strResult=="OK")
	{
		//Valida que se tengan las configuraciones necesarias para poder interactuar con el eBavel
		$blnCanSaveActions = true;
		
		require_once('settingsvariable.inc.php');
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'EBAVELURL');
		if (is_null($objSetting) || trim($objSetting->SettingValue == ''))
		{
			$blnCanSaveActions = false;
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel URL");
			}
			@logeBavelString("No eBavel URL");
		}
		$eBavelAppCodeName = '';
		$sql = "SELECT codeName FROM SI_FORMS_APPS WHERE id_app = ".$surveyInstance->eBavelAppID;
	    $aRS = $aRepository->ADOConnection->Execute($sql);
	    if ($aRS === false || $aRS->EOF)
	    {
			$blnCanSaveActions = false;
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel App");
			}
			@logeBavelString("No eBavel App");
	    }
	    else 
	    {
			//@JAPR 2013-07-05: Agregado el mapeo de una forma de eBavel para acciones
			$blnUseFormForActions = false;
			if (getMDVersion() >= esveBavelSuppActions) {
				$blnUseFormForActions = true;
			}
	    	
	    	//@JAPR 2013-12-13: Corregido un bug, de cierta fecha en adelante, ahora eBavel ya no graba como utf-8 sus strings
			//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	    	$eBavelAppCodeName = (string) $aRS->fields["codename"];
	    	if (trim($eBavelAppCodeName) == '' || ($surveyInstance->eBavelFormID <= 0 && !$blnUseFormForActions))
	    	{
	    		$blnCanSaveActions = false;
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\nNo eBavel Form");
				}
				@logeBavelString("No eBavel Form");
	    	}
	    	//@JAPR
	    }

		if ($blnCanSaveActions)
		{
			$arrURL = parse_url($objSetting->SettingValue);
			$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
			$strPath = str_replace('/', '\\', $strPath);
			$strPath = str_replace('\\\\', '\\', $strPath);
			//@JAPR 2012-07-10: Corregido un bug, si la ruta al eBavel configurada  no existe, a pesar de ignorar el error mediante el uso de @
			//estaba generando un error de compilación interrumpiendo con ello el grabado al primer registro de una captura que debió haber
			//grabado múltiples registros
			if (file_exists($strPath))
			{
				@require_once($strPath);
			}
			//@JAPR
			
			if (!class_exists('Tables'))
			{
				$blnCanSaveActions = false;
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\nNo eBavel class Tables");
				}
				@logeBavelString("No eBavel class Tables");
			}
		}
		
		if ($blnCanSaveActions)
		{
			//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
			//Si aun no había cargado los datos de los supervisores, lo hace en este momento para tenerlos disponibles en caso de usarse
			if (!$blnEBavelSupLoaded) {
				//@JAPR 2013-05-07: Pospuesto este cambio porque eBavel dejó de manejar las acciones como antes así que ya no funcionaba lo que hacía eForms
				//@JAPR 2013-07-15: Habilitado nuevamente este cambio porque ya se soportan acciones de eBavel otra vez
				if ($appVersion >= esvMultiDynamicAndStandarization && $appVersion < esveBavelSuppActions123) {
					//En este caso el App no soportaba valores negativos, por tanto se forzará a usar los IDs cercanos al Long como Supervisores y MySelf,
					//sin embargo primero se valida si la BD tiene o no algún usuario registrado con dicho ID, ya que si lo tiene, no puede basarse en este
					//rango para valores dummy
					$intMaxUserID = 0;
					$sql = "SELECT MAX(CLA_USUARIO) AS CLA_USUARIO FROM SI_USUARIO";
				    $aRS = $aRepository->ADOConnection->Execute($sql);
				    if ($aRS && !$aRS->EOF) {
			        	$intMaxUserID = (int) @$aRS->fields["cla_usuario"];
					}
					
					$blnUseMaxLongDummies = ($intMaxUserID < MAX_LONG -3);
				}
				else {
					//En este caso la nueva versión del App entiende perfectamente los IDs <= 0 como dummies y los respeta
				}
				
				$arrEBavelSupervisors = array();
				$sql = "SELECT EBAVEL_SUP1, EBAVEL_SUP2, EBAVEL_SUP3 
					FROM SI_USUARIO 
					WHERE CLA_USUARIO = ".(int) @$_SESSION["PABITAM_UserID"];
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					$arrEBavelSupervisors[-1] = (int) @$aRS->fields["ebavel_sup1"];
					$arrEBavelSupervisors[-2] = (int) @$aRS->fields["ebavel_sup2"];
					$arrEBavelSupervisors[-3] = (int) @$aRS->fields["ebavel_sup3"];
				}
				$blnEBavelSupLoaded = true;
			}
			//@JAPR
			
			//@JAPR 2012-05-21: Agregadas las acciones en preguntas de selección sencilla/múltiple
			//Genera una colección con todas las preguntas que pudiesen generar acciones
			$allActionQuestions = BITAMQuestionCollection::NewInstanceEmpty($aRepository, $surveyID);
			foreach ($questionCollection->Collection as $questionKey => $aQuestion)
			{
				$allActionQuestions->Collection[] = $aQuestion;
			}
			foreach ($multiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
			{
				$allActionQuestions->Collection[] = $aQuestion;
			}
			//@JAPR 2012-10-29: No se estaban incluyendo las preguntas multiple choice que no fueran multi-dimensión
			foreach ($multiChoiceQuestionCollectionNoMultDim->Collection as $questionKey => $aQuestion)
			{
				if ($aQuestion->UseCategoryDim == 0) {
					$allActionQuestions->Collection[] = $aQuestion;
				}
			}
			//@JAPR
			
			require_once('catalog.inc.php');
			$arrCategories = BITAMCatalog::GetCategories($aRepository, false, false);
			if (is_null($arrCategories))
			{
				$arrCategories = array();
			}
			//@JAPR 2012-05-18: Validado que si no viene el Element, le asigne por lo menos una descripción vacía
			$strElement = (string) @$arrayData['element'];
			if (trim($strElement) == '')
			{
				$strElement = '('.translate('None').')';
			}
			
			//@JAPR 2012-05-21: Validado que las acciones sólo se generen una vez cuando existen secciones Maestro - Detalle o dinámicas (las preguntas que
			//se encuentra dentro de ellas pueden grabar múltiples acciones, pero las que están fuera de ellas sólo pueden grabar una acción)
			//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
			//Ahora estas variables son globales
			/*
	        $arrMasterDetSections = BITAMSection::existMasterDetSections($aRepository, $surveyID, true);
	        if (!is_array($arrMasterDetSections))
	        {
	        	$arrMasterDetSections = array();
	        }
	        $arrMasterDetSectionsIDs = array_flip($arrMasterDetSections);
	        */
	        //@JAPR
			
			//@JAPR 2013-07-19: Corregido un bug, si las preguntas son numéricas y están en secciones estándar,
			//no se puede obtener su valor durante el procesamiento de registros que no sean el único de secciones estándar,
			//Se agregará un array con las respuestas a todas estas preguntas justo cuando se proceso este registro único para reasignarlo mas
			//adelante a las acciones generadas antes de este punto
			//Sólo es necesario este proceso si hay preguntas con acciones y si esta procesando el registro único (para la fecha de implementación
			//no se sabía con certeza en este punto si había preguntas con acciones, ya que se resuelve mas abajo según el tipo de pregunta)
			if ($anEntrySectionID == 0 && $anEntryCatDimID == 0) {	//&& count($allActionQuestions->Collection) > 0
				global $arrNumericQAnswersColl;
				$arrNumericQAnswersColl = array();
				
				//Recorre todas las preguntas numéricas que sólo se graban en el registro único
				foreach ($questionCollection->Collection as $questionKey => $aQuestion) {
					if ($aQuestion->QTypeID == qtpOpenNumeric || $aQuestion->QTypeID == qtpCalc) {
						$strQuestionField = 'qfield'.$aQuestion->QuestionID;
						//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
						$strQuestionValue = @replaceQuestionVariables("@Q$aQuestion->QuestionID", $arrFieldValues, false, $aRepository, $surveyID, false);
						if (!is_null($strQuestionValue) && trim($strQuestionValue) !== '') {
							$arrNumericQAnswersColl[$strQuestionField] = $strQuestionValue;
						}
					}
				}
			}
			//@JAPR
	        
			//No intenta crear las tablas del eBavel pues para poder haber llegado a este punto deberían de existir, y se debió haber actualizado
			//a la versión de eBavel 1.5 por lo menos antes de enlazarlo con eForms
			$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
			//@JAPR 2012-05-21: Agregadas las acciones en preguntas de selección sencilla/múltiple
			foreach ($allActionQuestions->Collection as $questionKey => $aQuestion)
			{
				$insertData = array();
				
				//@JAPR 2012-05-21: Validado que las acciones sólo se generen una vez cuando existen secciones Maestro - Detalle o dinámicas (las preguntas que
				//se encuentra dentro de ellas pueden grabar múltiples acciones, pero las que están fuera de ellas sólo pueden grabar una acción)
				$blnCanAddQuestionAction = false;
				if ($hasDynamicSection && $dynamicSectionID == $aQuestion->SectionID)
				{
					//Si es una pregunta de sección dinámica o sección Maestro - Detalle, siempre entra a grabar pues cada llamada a SaveData 
					//equivale a una respuesta diferente y una posible acción distinta también
					//Sólo entrará a grabar en este caso si la pregunta es tipo Action, ya que Single y Multiple choice dentro de secciones
					//dinámicas se basan en los catálogos y para ellas no aplican las acciones
					//@JAPR 2013-07-15: Corregido un bug, las preguntas simple choice si pueden tener opciones fijas sin depender del catálogo,
					//por lo tanto esas si son válidas para generar acciones, sólo las múltiple choice efectivamente no son válidas
					//$blnCanAddQuestionAction = ($aQuestion->QTypeID != qtpSingle && $aQuestion->QTypeID != qtpMulti);
					$blnCanAddQuestionAction = ($aQuestion->QTypeID != qtpMulti);
					//@JAPR
				}
				//@JAPR 2014-05-29: Agregado el tipo de sección Inline
				elseif (isset($arrMasterDetSectionsIDs[$aQuestion->SectionID]) || isset($arrInlineSectionsIDs[$aQuestion->SectionID]))
				{
					//Si es una pregunta de sección Maestro - Detalle, siempre entra a grabar pues cada llamada a SaveData 
					//equivale a una respuesta diferente y una posible acción distinta también (lo mismo aplica en secciones Inline)
					$blnCanAddQuestionAction = true;
				}
				else 
				{
					//Si se trata de una sección estática, sólo entraría a grabar si fuera la primera vez que procesa esta pregunta en esta 
					//llamada al servicio, ya que las iteraciones posteriores simplemente repiten los mismos valores para estas preguntas/acciones
					if (!isset($arrProcessedActionQuestions[$aQuestion->QuestionID]))
					{
						$blnCanAddQuestionAction = true;
						$arrProcessedActionQuestions[$aQuestion->QuestionID] = $aQuestion->QuestionID;
					}
				}
				
				//En ningún caso las preguntas de catálogo, o bien las Multiple-Choice que no son de Checkbox ni las Single-Choice matriz permiten
				//generar acciones, así que se omiten
				if ($aQuestion->CatalogID > 0 || ($aQuestion->QTypeID == qtpSingle && $aQuestion->QDisplayMode == dspMatrix) ||
						($aQuestion->QTypeID == qtpMulti && $aQuestion->MCInputType != mpcCheckBox))
				{
					$blnCanAddQuestionAction = false;
				}
				//@JAPR
				
				try
				{
					//La pregunta tipo Action viene como un texto con separadores, por lo tanto obtenemos sólo la respuesta que es el Action para grabar
					//en la tabla paralela/hechos
					//@JAPR 2012-05-21: Validado que las acciones sólo se generen una vez cuando existen secciones Maestro - Detalle o dinámicas (las preguntas que
					//se encuentra dentro de ellas pueden grabar múltiples acciones, pero las que están fuera de ellas sólo pueden grabar una acción)
					if ($blnCanAddQuestionAction)
					{
						if ($aQuestion->QTypeID == qtpAction)
						{
							$arrayActionValues = @$arrayValuesByActionQuestion[$questionKey];
							$blnCanSaveActions = (!is_null($arrayActionValues) && is_array($arrayActionValues) && count($arrayActionValues) >= 3);
							
							if ($blnCanSaveActions)
							{
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									echo("<br>\r\nAdding action for question: {$aQuestion->QuestionID}");
								}
								@logeBavelString("Adding action for question: {$aQuestion->QuestionID}");
								
								//@JAPR 2013-07-15: Agregado el mapeo de una forma de eBavel para acciones
								if (getMDVersion() >= esveBavelSuppActions123) {
									$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
									$aQuestion->readeBavelActionFieldsMapping();
									$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
								}
								//@JAPR
								$intResponsibleID = (int) @$arrayActionValues[0];
								
								//@JAPR 2013-05-07: Pospuesto este cambio porque eBavel dejó de manejar las acciones como antes así que ya no funcionaba lo que hacía eForms
								//@JAPR 2013-07-15: Habilitado nuevamente este cambio porque ya se soportan acciones de eBavel otra vez
								//@JAPR 2013-05-06: Corregido un bug, la funcionalidad de Supervisores adicionales no funcionaba con Apps antes de esveBavelMapping, así que se
								//validará para que en esos casos se force a enviar IDs positivos que permitan usarla, y solo en las mas recientes envíe IDs negativos como debe ser
								if ($blnUseMaxLongDummies && ($intResponsibleID >= MAX_LONG -3) && $appVersion >= esvMultiDynamicAndStandarization && $appVersion < esveBavelSuppActions123) {
									//En este caso el App no soportaba valores negativos, por tanto se forzará a usar los IDs cercanos al Long como Supervisores y MySelf,
									//sin embargo primero se valida si la BD tiene o no algún usuario registrado con dicho ID, ya que si lo tiene, no puede basarse en este
									//rango para valores dummy
									$intResponsibleID = $intResponsibleID - MAX_LONG;
								}
								else {
									if ($appVersion >= esveBavelSuppActions123) {
										//@JAPR 2013-07-15: Agregado el mapeo de responsables supervisores 1 al 3 y MySelf desde el servicio, ahora los valores negativos mayores
										//del -255 son válidos, por lo que se cambió el valor default de no asignado a -255, sin embargo para la pregunta anteriormente las
										//definiciones grababan un -1 como no asignado para el responsable, pero como ese valor corresponde al SUPERVISOR 1, se tendrá que
										//sumar uno al valor real grabado en la definición para representar al elemento correcto si los valores grabados son negativos, en
										//otras palabras, mientras el server usará -1, -2 y -3 para los SUPERVISOR 1, 2 y 3 respectivamente, en la definición del App se
										//grabarán -2, -3 y -4 respectivamente porque -1 ya era usado como el anterior valor de no asignado del QuestionCls.ResponsibleID y
										//por tanto todas las definiciones actuales ya traerían dicho valor
										if ($intResponsibleID < 0) {
											$intResponsibleID++;
										}
									}
								}
								//@JAPR
								
								if ($intResponsibleID <= 0)
								{
									//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
									switch ($intResponsibleID) {
										case 0:			//Significa que se desea enviar al usuario que hace la captura
											break;
										case -1:
										case -2:
										case -3:
											//Se usa el índice del SUPERVISOR correspondiente, si no estuviera asignado entonces la validación de 
											//abajo lo forzará a usar al mismo usuario que hace la captura
											$intResponsibleID = (int) @$arrEBavelSupervisors[$intResponsibleID];
											break;
										default:		//Casos no soportados, la validación de abajo lo forzará a usar al mismo usuario que hace la captura
											break;
									}
									
									if ($intResponsibleID <= 0) {
										$intResponsibleID = $_SESSION["PABITAM_UserID"];
									}
									//@JAPR
								}
								$strAction = (string) @$arrayActionValues[3];
								//Este reemplazo se hace mediante los números de las preguntas porque es texto capturado por el usuario en el App
								$strAction = replaceQuestionVariables($strAction, $arrFieldValues);
								$strCategory = (string) @$arrCategories[(int) @$arrayActionValues[2]];
								//@JAPR 2012-05-21: Modificado para poder recibir una fecha directamente
								$intDaysDueDate = @$arrayActionValues[1];
								if (is_null($intDaysDueDate) || trim((string) $intDaysDueDate) == '' || ((int) $intDaysDueDate) == 0)
								{
									//Si es null, se usa directamente la fecha actual
									$intDaysDueDate = $thisDate;
								}
								else if (is_numeric($intDaysDueDate))
								{
									//Si es un número, entonces se aplica ese número como desplazamiento de la fecha actual
									$intDaysDueDate = days_add($thisDate, abs((int) $intDaysDueDate));
								}
								else
								{
									//Si no es un número se asume que ya es una fecha así que se usa directamente
									//$intDaysDueDate = abs((int) @$arrayActionValues[1]);
								}
								//@JAPR
								if (trim($strCategory) == '')
								{
									$strCategory = '('.translate('None').')';
								}
								
								//Se obtiene todas las partes de la pregunta tipo Acción para agregarlas a la acción del eBavel
								$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
								//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
								/*
							    $manager = new Tables();
							    $manager->applicationCodeName = $eBavelAppCodeName;
							    $manager->init($aRepository->ADOConnection);
							    */
								$insertData = array();
								$insertData["id_actionitem"] = -1;
								$insertData["createdDate"] = date('Y-m-d H:i:s');			//Fijo
								$insertData["modifiedDate"] = date('Y-m-d H:i:s');			//Fijo
								$insertData["section_id"] = $surveyInstance->eBavelFormID;	//Ya no
								$insertData["createdUser"] = $_SESSION["PABITAM_UserID"];	//Fijo
								$insertData["user"] = $intResponsibleID;					//Igual //Pedir - mapeable
								$strFullActionDesc = (string) @$aQuestion->ActionText;
								$intActionDescPos = stripos($strFullActionDesc, "{Mobile_Action_Desc}");
								if ($intActionDescPos !== false) {
									$strFullActionDesc = str_ireplace("{Mobile_Action_Desc}", $strAction, $strFullActionDesc);
								}
								else {
									$strFullActionDesc = $strAction.$strFullActionDesc;
								}
								//Este reemplazo se hace mediante los IDs de las preguntas porque sólo queda texto que se debió definir en
								//el Administrador, no capturado por el usuario en el App
								//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
								$insertData["description"] = replaceQuestionVariables($strFullActionDesc, $arrFieldValues, false, $aRepository, $surveyID);	//Igual //Pedir - mapeable
								$strActionTitle = (string) @$aQuestion->ActionTitle;
								if (trim($strActionTitle) != '') {
									//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
									$insertData["title"] = replaceQuestionVariables($strActionTitle, $arrFieldValues, false, $aRepository, $surveyID);		//Ya no
								}
								$insertData["email"] = 0;
								//@JAPR 2012-10-10: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
								//Se cambió el valor de estos campos de 0 a NULL para permitir enviar los EMails
								$insertData["row_key"] = null;
								$insertData["process_id"] = null;
								if ($intResponsibleID > 0) {
									$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
									require_once("appuser.inc.php");
									require_once("bpaemailconfiguration.inc.php");
									$objResource = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $intResponsibleID);
									$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
									if (!is_null($objResource)) {
										$tempEmailCC = @$objResource->AltEmail;
										if (trim($tempEmailCC) != '') {
											$blnCheckMail = BPAEmailConfiguration::check_email_address($tempEmailCC);
											if ($blnCheckMail)
											{
												$insertData["email_cc"] = $tempEmailCC;		//Campo eMail (catalogo)
											}
										}
									}
								}
								//@JAPR
								
								//0 = 'In Progress (Open)';
							   	//1 = 'Stand By';
							   	//2 = 'Finished';
								//@JAPR 2012-10-10: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
								//Agregado el tipo de Status a Request para permitir que se notifique sobre la acción
								//3 = 'Request';
								$insertData["status"] = 3; 			//Pedir -Settings fijo para todos
								//@JAPR
								$insertData["source"] = "eForms";						//Pedir - Settings fijo para todos
								$insertData["sourceID"] = $surveyInstance->SurveyID;	//Ya no
								$insertData["sourceRow_key"] = $factKey;				//Ya no
								//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
								$insertData["category"] = $strCategory;	//Categoria Mapeada (capturado en el App) //Pedir - mapeable
								$insertData["element"] = $strElement;		//Ya no
								$insertData["deadline"] = $intDaysDueDate;	//Igual	//days_add($thisDate, $intDaysDueDate); //Pedir - mapeable
								//@JAPR 2013-07-09: Validado que por ahora no se permitan grabar acciones de preguntas tipo acción cuando se
								//mapea una forma de eBavel, sólo si se usa el viejo formato de eMeeting
								if (getMDVersion() >= esveBavelSuppActions && getMDVersion() < esveBavelSuppActions123) {
									//Por ahora estas acciones no son compatibles con el nuevo formato
									//A partir de la versión esveBavelSuppActions123 ya se soportaron nuevamente las preguntas tipo acción
								}
								else {
									//@JAPR 2013-07-05: Agregado el mapeo de una forma de eBavel para acciones
									//Verifica si se tiene mapeo de preguntas a campos de eBavel, si es así, entonces agrega como un
									//nuevo valor del array la respuesta de dicha pregunta en el registro actualmente procesado
									$objQuestionFieldMap = @$aQuestion->QuestionFieldMap;
									if (getMDVersion() >= esveBavelSuppActions123 && !is_null($objQuestionFieldMap)) {
										foreach($objQuestionFieldMap->ArrayVariables as $inteBavelFieldID => $strVariableName) {
											//Los valores > 0 indican mapeos hacia preguntas, por lo tanto agrega el valor al array
											$intQuestionID = ((int) @$objQuestionFieldMap->ArrayVariableValues[$inteBavelFieldID]);
											//@JAPR 2014-10-29: Agregado el mapeo de Scores
											$intDataID = ((int) @$objQuestionFieldMap->ArrayVariablesDataID[$inteBavelFieldID]);
											if ($intQuestionID > 0) {
												//@JAPR 2013-07-19: Corregido un bug, si las preguntas son numéricas y están en secciones estándar,
												//no se puede obtener el valor en este punto por lo que se validará el tipo y sección antes de 
												//intentar obtenerlo. Se agregará un array dummy que indicará que se obtenga justo antes de grabar
												//la acción, momento para el cual ya se tiene identificado correctamente
												$strQuestionField = 'qfield'.$intQuestionID;
												$blnAddThisAnswer = true;
												//Si se trata del registro único de las secciones estándar entonces no es necesario este proceso porque aquí si debe venir
												//asignado el valor de esta pregunta
												if (($anEntrySectionID != 0 || $anEntryCatDimID != 0) && 
														class_exists('BITAMQuestionActionMap') && isset(BITAMQuestionActionMap::$ArrayQuestionData) && 
														isset(BITAMQuestionActionMap::$ArrayQuestionData[$strQuestionField])) {
													//Si se encuentra cargada la instancia, entonces se agregará un dummy para que el valor se
													//obtenga mas adelante si es una pregunta numérica de sección estándar
													$objQuestion = @BITAMQuestionActionMap::$ArrayQuestionData[$strQuestionField]['Obj'];
													if (!is_null($objQuestion)) {
														if (($objQuestion->QTypeID == qtpOpenNumeric || $objQuestion->QTypeID == qtpCalc) && 
																$objQuestion->SectionType == sectNormal) {
															//Este valor no se puede obtener en este punto, se agrega el dummy para que se cargue mas adelante
															$blnAddThisAnswer = false;
															//@JAPR 2014-10-29: Agregado el mapeo de Scores
															//Si se está pidiendo un score no aplica para esta pregunta
															if ($intDataID == sdidValue) {
																$insertData['qfield'.$intQuestionID] = array('GetFromStRec');
															}
															//@JAPR
														}
													}
												}
												
												if ($blnAddThisAnswer) {
													//@JAPR 2013-07-24: Agregado el mapeo de fotos durante la generación de acciones
													//Verifica si es una pregunta tipo foto o algún tipo especial no soportado, si lo es entonces
													//recibirá trato especial para grabarse o no se grabará en absoluto
													if (class_exists('BITAMQuestionActionMap') && isset(BITAMQuestionActionMap::$ArrayQuestionData) && 
															isset(BITAMQuestionActionMap::$ArrayQuestionData[$strQuestionField])) {
														$objQuestion = @BITAMQuestionActionMap::$ArrayQuestionData[$strQuestionField]['Obj'];
														if (!is_null($objQuestion)) {
															switch ($objQuestion->QTypeID) {
																case qtpPhoto:
																	//Obtiene la ruta de la imagen para que mas adelante sea procesada y grabada
																	//como parte de eBavel también
																	if ($usePhotoPaths) {
																		//@JAPR 2014-10-29: Agregado el mapeo de Scores
																		//Si se está pidiendo un score no aplica para esta pregunta
																		if ($intDataID == sdidValue) {
																			//En este caso es un nombre de archivo, así que no decodifica nada
																			$otherPostKey = "qimageencode".$intQuestionID;
																			$strQuestionValue = (string) @$arrayData[$otherPostKey];
																			if (!is_null($strQuestionValue) && trim($strQuestionValue) !== '') {
																				$insertData['qfield'.$intQuestionID] = $strQuestionValue;
																			}
																		}
																		//@JAPR
																		
																		//Se marca para que ya no intente obtener la respuesta mas adelante
																		$blnAddThisAnswer = false;
																	}
																	break;
																
																case qtpSketch:
																case qtpSignature:
																case qtpDocument:
																case qtpAudio:
																case qtpVideo:
																	//No soportado por ahora
																	break;
															}
														}
													}
													
													if ($blnAddThisAnswer) {
														//@JAPR 2014-10-29: Agregado el mapeo de Scores
														//Ajusta la variable dependiendo de si se está o no pidiendo el score (el default es el valor)
														$strQuestionVariable = "@Q$intQuestionID";
														$strQuestionPostKey = 'qfield'.$intQuestionID;
														if ($intDataID == sdidScore) {
															$strQuestionVariable .= ".score()";
															$strQuestionPostKey .= "Score";
														}
														//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
														$strQuestionValue = replaceQuestionVariables($strQuestionVariable, $arrFieldValues, false, $aRepository, $surveyID, false);
														if (!is_null($strQuestionValue) && trim($strQuestionValue) !== '') {
															$insertData[$strQuestionPostKey] = $strQuestionValue;
														}
														//@JAPR
													}
													//@JAPR
												}
												//@JAPR
											}
											//@JAPR 2014-10-29: Agregado el mapeo de Constantes fijas
											elseif ($intQuestionID < 0) {
												if (getParamValue('DebugBreak', 'both', '(int)', true)) {
													echo("<br>\r\nChecking for Expressions #1: {$intQuestionID}");
												}
												switch ($intQuestionID * -1) {
													case sfidFixedStrExpression:
													case sfidFixedNumExpression:
														//En este caso se trata de una expresión que se puede resolver a un texto o a un número, pero
														//en ambos casos puede contener variables, así que se tiene que hacer un replace de los valores
														//de dichas variables y al final aplicar la conversión a número si es necesario. Adicionalmente
														//si la expresión inicia con el caracter "=" se consideraría una fórmula, por lo que hay
														//que aplicar un eval antes de continuar
														$strFormula = ((string) @$objQuestionFieldMap->ArrayVariablesFormula[$inteBavelFieldID]);
														//@JAPR 2014-11-25: Corregido un bug, no delimitará las variables de preguntas/secciones tipo string para permitir mapear directamente
														//los valores sin comillas
														$strFormulaValue = replaceQuestionVariables($strFormula, $arrFieldValues, false, $aRepository, $surveyID, false, true);
														
														//En este punto la fórmula podría necesitar evaluarse antes de ser válida
														//@JAPR 2014-10-30: Finalmente después de analizarlo, LRoux determinó no permitir evaluar
														//fórmulas en PhP para evitar la posible inyección de código por parte del usuario
														if (false && substr($strFormulaValue, 0, 1) == '=') {
															try {
																if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																	echo("<br>\r\nEvaluating mapped eBavel formula: ".'$strFormulaValueResult = "'.substr($strFormulaValue, 1).'"');
																}
																@eval('$strFormulaValueResult = '.substr($strFormulaValue, 1));
															}
															catch (Exception $e) {
																if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																	echo("<br>\r\nEvaluating mapped eBavel formula error: ".$e->getMessage());
																}
																$strFormulaValueResult = null;
															}
															
															$strFormulaValue = $strFormulaValueResult;
															if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																echo("<br>\r\nEvaluating mapped eBavel formula result: {$strFormulaValue}");
															}
														}
														
														//Finalmente aplica la conversión si es un tipo de expresión que debería ser numérica
														if ($intQuestionID * -1 == sfidFixedNumExpression) {
															$strFormulaValue = (float) @$strFormulaValue;
														}
														else {
															//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
															$strFormulaValue = (string) @$strFormulaValue;
														}
														
														//Agrega el resultado a la acción utilizando el ID del campo de eBavel correspondiente
														$insertData["value".$inteBavelFieldID] = $strFormulaValue;
														break;
													
													//@JAPR 2014-10-30: Agregado el mapeo de atributos de catálogos
													case sfidCatalogAttribute:
														$intAttributeID = ((int) @$objQuestionFieldMap->ArrayVariablesMemberID[$inteBavelFieldID]);
														$strAttribValue = getEntryAttributeValueForAction($aRepository, $surveyID, $intAttributeID, $arrFieldValues, $anEntrySectionID, $anEntryCatDimID, $aDynamicPageDSC, $aMultiRecordNum);
														//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
														$insertData["attribValue".$intAttributeID] = $strAttribValue;
														break;
												}
											}
											//@JAPR
										}
										
										//Debe agregar además una referencia a la pregunta y opción de respuesta que produce
										//la acción, esto para mas adelante poder recuperar el mapeo completo
										$insertData["QuestionID"] = $aQuestion->QuestionID;
										//$insertData["QuestionAnswer"] = $anOption->QuestionOptionName;
										$insertData["eBavelActionSrcType"] = "question";
										$insertData["eBavelActionFormID"] = $aQuestion->eBavelActionFormID;
										$insertData["eBavelFieldMap"] = $objQuestionFieldMap;
									}
									//@JAPR
									$arrActionItems[] = $insertData;
								}
							   	//$manager->insertData('ACTIONITEMS', $insertData, array(), $aRepository->ADOConnection);
							   	//@JAPR
							}
						}
						else
						{
							if ($aQuestion->QTypeID == qtpSingle || $aQuestion->QTypeID == qtpMulti)
							{
								$arrOptionsColl = @$arrQuestionOptions[$aQuestion->QuestionID];
								if (is_null($arrOptionsColl))
								{
									$arrOptionsColl = array();
									//@JAPR 2013-07-05: Agregado el mapeo de una forma de eBavel para acciones
									//Para las preguntas simple y multiple choice, se obtiene el mapeo directo de columnas de eBavel así que se
									//envía el parámetro $aGetValues == true para este fin, mas adelante se usará para agregar posibles
									//respuestas de preguntas que se encuentren mapeadas
									
									//@JAPR 2013-07-08: Corregido un bug, se debe tener FETCH_ASSOC o de lo contrario no cargará la instancia
									$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
									$arrSelectOptions = BITAMQuestionOptionCollection::NewInstance($aRepository, $aQuestion->QuestionID, null, true);
									$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
									//@JAPR
									if (is_null($arrSelectOptions))
									{
										$arrSelectOptions = array();
									}
									
									$blnHasAction = false;
									foreach ($arrSelectOptions as $anOption)
									{
										if (trim($anOption->QuestionOptionName) != '' && trim($anOption->ActionText) != '')
										{
											$arrOptionsColl[$anOption->QuestionOptionName] = $anOption;
										}
									}
									
									$arrQuestionOptions[$aQuestion->QuestionID] = $arrOptionsColl;
								}
								
								$arrAnswers = array();
								$postKey = "qfield".$aQuestion->QuestionID;
								if(isset($arrayData[$postKey]))
								{
									$strValues = $arrayData[$postKey];
									if(trim($strValues)!="")
									{
										if ($aQuestion->QTypeID == qtpSingle)
										{
											$arrAnswers = array($strValues);
										}
										else 
										{
											$arrAnswers = explode("_SVSep_", $strValues);
										}
									}
								}
								
								if ($aQuestion->QTypeID == qtpSingle)
								{
									//En este caso sólo la respuesta seleccionada grabará la acción que tenga asociada
									if (count($arrAnswers) > 0)
									{
										//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
										//Ahora verificará si existe una condición para generar la acción, si la hay y se cumple entonces si
										//se mandarán los datos, de lo contrario simplemente registra la condición no cumplica en el log
										$blnAddAction = isset($arrOptionsColl[$arrAnswers[0]]);
										if ($blnAddAction) {
											$anOption = @$arrOptionsColl[$arrAnswers[0]];
											if (!is_null($anOption) && $anOption->ActionCondition) {
												//Hay una condición, así que la evalúa para decidir si se manda o no la acción
												$strActionCondition = replaceQuestionVariables($anOption->ActionCondition, $arrFieldValues, false, $aRepository, $surveyID);
												try {
													//Para tratar de reducir riesgo
													$strActionConditionVal = eval("return ({$strActionCondition});");
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														echo("<br>\r\n Action condition for single answer: {$aQuestion->QuestionID} => ".@$anOption->QuestionOptionName." == {$strActionConditionVal}");
													}
												} catch (Exception $e) {
													//No importa el error, simplemente no habrá valor asignado
													$strActionConditionVal = 0;
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														echo("<br>\r\n Error evaluating action condition for single answer: {$aQuestion->QuestionID} => ".@$anOption->QuestionOptionName." == ".$e->getMessage());
													}
												}
												
												$blnAddAction = $strActionConditionVal;
											}
										}
										
										if ($blnAddAction)
										{
											$anOption = $arrOptionsColl[$arrAnswers[0]];
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												echo("<br>\r\nAdding action for single answer: {$aQuestion->QuestionID} => ".@$anOption->QuestionOptionName);
											}
											@logeBavelString("Adding action for single answer: {$aQuestion->QuestionID} => ".@$anOption->QuestionOptionName);
											//Existe la acción asociada a la opción seleccionada, así que la crea
											$intResponsibleID = (int) $anOption->ResponsibleID;
											
											//@JAPR 2013-05-07: Pospuesto este cambio porque eBavel dejó de manejar las acciones como antes así que ya no funcionaba lo que hacía eForms
											//@JAPR 2013-05-06: Corregido un bug, la funcionalidad de Supervisores adicionales no funcionaba con Apps antes de esveBavelMapping, así que se
											//validará para que en esos casos se force a enviar IDs positivos que permitan usarla, y solo en las mas recientes envíe IDs negativos como debe ser
											//@JAPR 2013-07-15: Habilitado nuevamente este cambio porque ya se soportan acciones de eBavel otra vez
											if ($blnUseMaxLongDummies && ($intResponsibleID >= MAX_LONG -3) && $appVersion >= esvMultiDynamicAndStandarization && $appVersion < esveBavelSuppActions123) {
												//En este caso el App no soportaba valores negativos, por tanto se forzará a usar los IDs cercanos al Long como Supervisores y MySelf,
												//sin embargo primero se valida si la BD tiene o no algún usuario registrado con dicho ID, ya que si lo tiene, no puede basarse en este
												//rango para valores dummy
												$intResponsibleID = $intResponsibleID - MAX_LONG;
											}
											else {
												if ($appVersion >= esveBavelSuppActions123) {
													//@JAPR 2013-07-15: Agregado el mapeo de responsables supervisores 1 al 3 y MySelf desde el servicio, ahora los valores negativos mayores
													//del -255 son válidos, por lo que se cambió el valor default de no asignado a -255, sin embargo para la pregunta anteriormente las
													//definiciones grababan un -1 como no asignado para el responsable, pero como ese valor corresponde al SUPERVISOR 1, se tendrá que
													//sumar uno al valor real grabado en la definición para representar al elemento correcto si los valores grabados son negativos, en
													//otras palabras, mientras el server usará -1, -2 y -3 para los SUPERVISOR 1, 2 y 3 respectivamente, en la definición del App se
													//grabarán -2, -3 y -4 respectivamente porque -1 ya era usado como el anterior valor de no asignado del QuestionCls.ResponsibleID y
													//por tanto todas las definiciones actuales ya traerían dicho valor
													if ($intResponsibleID < 0) {
														$intResponsibleID++;
													}
												}
											}
											//@JAPR
											
											if ($intResponsibleID <= 0)
											{
												//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
												switch ($intResponsibleID) {
													case 0:			//Significa que se desea enviar al usuario que hace la captura
														break;
													case -1:
													case -2:
													case -3:
														//Se usa el índice del SUPERVISOR correspondiente, si no estuviera asignado entonces la validación de 
														//abajo lo forzará a usar al mismo usuario que hace la captura
														$intResponsibleID = (int) @$arrEBavelSupervisors[$intResponsibleID];
														break;
													default:		//Casos no soportados, la validación de abajo lo forzará a usar al mismo usuario que hace la captura
														break;
												}
												
												if ($intResponsibleID <= 0) {
													$intResponsibleID = $_SESSION["PABITAM_UserID"];
												}
												//@JAPR
											}
											$strAction = (string) $anOption->ActionText;
											$strCategory = (string) @$arrCategories[$anOption->CategoryID];
											$intDaysDueDate = (int) $anOption->DaysDueDate;
											if (is_null($intDaysDueDate) || trim((string) $intDaysDueDate) == '' || ((int) $intDaysDueDate) == 0)
											{
												//Si es null, se usa directamente la fecha actual
												$intDaysDueDate = $thisDate;
											}
											else if (is_numeric($intDaysDueDate))
											{
												//Si es un número, entonces se aplica ese número como desplazamiento de la fecha actual
												$intDaysDueDate = days_add($thisDate, abs((int) $intDaysDueDate));
											}
											else
											{
												//Si no es un número se asume que ya es una fecha así que se usa directamente
												//$intDaysDueDate = abs((int) @$arrayActionValues[1]);
											}
											//@JAPR
											if (trim($strCategory) == '')
											{
												$strCategory = '('.translate('None').')';
											}
											
											//Se obtiene todas las partes de la pregunta tipo Acción para agregarlas a la acción del eBavel
											$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
											//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
											/*
										    $manager = new Tables();
										    $manager->applicationCodeName = $eBavelAppCodeName;
										    $manager->init($aRepository->ADOConnection);
										    */
											$insertData = array();
											$insertData["id_actionitem"] = -1;
											$insertData["createdDate"] = date('Y-m-d H:i:s');
											$insertData["modifiedDate"] = date('Y-m-d H:i:s');
											$insertData["section_id"] = $surveyInstance->eBavelFormID;
											$insertData["createdUser"] = $_SESSION["PABITAM_UserID"];
											$insertData["user"] = $intResponsibleID;
											//Este reemplazo se hace mediante los IDs de las preguntas porque sólo queda texto que se debió definir en
											//el Administrador, no capturado por el usuario en el App
											//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
											$insertData["description"] = replaceQuestionVariables($strAction, $arrFieldValues, false, $aRepository, $surveyID);
											$strActionTitle = (string) @$anOption->ActionTitle;
											if (trim($strActionTitle) != '') {
												//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
												$insertData["title"] = replaceQuestionVariables($strActionTitle, $arrFieldValues, false, $aRepository, $surveyID);
											}
											$insertData["email"] = 0;
											//@JAPR 2012-10-10: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
											//Se cambió el valor de estos campos de 0 a NULL para permitir enviar los EMails
											$insertData["row_key"] = null;
											$insertData["process_id"] = null;
											if ($intResponsibleID > 0) {
												$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
												require_once("appuser.inc.php");
												require_once("bpaemailconfiguration.inc.php");
												$objResource = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $intResponsibleID);
												$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
												if (!is_null($objResource)) {
													$tempEmailCC = @$objResource->AltEmail;
													if (trim($tempEmailCC) != '') {
														$blnCheckMail = BPAEmailConfiguration::check_email_address($tempEmailCC);
														if ($blnCheckMail)
														{
															$insertData["email_cc"] = $tempEmailCC;
														}
													}
												}
											}
											//@JAPR
											
											//0 = 'In Progress (Open)';
										   	//1 = 'Stand By';
										   	//2 = 'Finished';
											//@JAPR 2012-10-10: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
											//Agregado el tipo de Status a Request para permitir que se notifique sobre la acción
											//3 = 'Request';
											$insertData["status"] = 3;
											//@JAPR
											$insertData["source"] = "eForms";
											$insertData["sourceID"] = $surveyInstance->SurveyID;
											$insertData["sourceRow_key"] = $factKey;
											//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
											$insertData["category"] = $strCategory;
											$insertData["element"] = $strElement;
											$insertData["deadline"] = $intDaysDueDate;	//days_add($thisDate, $intDaysDueDate);
											//@JAPR 2013-07-05: Agregado el mapeo de una forma de eBavel para acciones
											//Verifica si se tiene mapeo de preguntas a campos de eBavel, si es así, entonces agrega como un
											//nuevo valor del array la respuesta de dicha pregunta en el registro actualmente procesado
											$objQuestionOptionFieldMap = @$anOption->QuestionOptionFieldMap;
											if (getMDVersion() >= esveBavelSuppActions && !is_null($objQuestionOptionFieldMap)) {
												foreach($objQuestionOptionFieldMap->ArrayVariables as $inteBavelFieldID => $strVariableName) {
													//Los valores > 0 indican mapeos hacia preguntas, por lo tanto agrega el valor al array
													$intQuestionID = ((int) @$objQuestionOptionFieldMap->ArrayVariableValues[$inteBavelFieldID]);
													//@JAPR 2014-10-29: Agregado el mapeo de Scores
													$intDataID = ((int) @$objQuestionOptionFieldMap->ArrayVariablesDataID[$inteBavelFieldID]);
													if ($intQuestionID > 0) {
														
														//@JAPR 2013-07-19: Corregido un bug, si las preguntas son numéricas y están en secciones estándar,
														//no se puede obtener el valor en este punto por lo que se validará el tipo y sección antes de 
														//intentar obtenerlo. Se agregará un array dummy que indicará que se obtenga justo antes de grabar
														//la acción, momento para el cual ya se tiene identificado correctamente
														$strQuestionField = 'qfield'.$intQuestionID;
														$blnAddThisAnswer = true;
														//Si se trata del registro único de las secciones estándar entonces no es necesario este proceso porque aquí si debe venir
														//asignado el valor de esta pregunta
														if (($anEntrySectionID != 0 || $anEntryCatDimID != 0) && 
																class_exists('BITAMQuestionOptionActionMap') && isset(BITAMQuestionOptionActionMap::$ArrayQuestionData) && 
																isset(BITAMQuestionOptionActionMap::$ArrayQuestionData[$strQuestionField])) {
															//Si se encuentra cargada la instancia, entonces se agregará un dummy para que el valor se
															//obtenga mas adelante si es una pregunta numérica de sección estándar
															$objQuestion = @BITAMQuestionOptionActionMap::$ArrayQuestionData[$strQuestionField]['Obj'];
															if (!is_null($objQuestion)) {
																if (($objQuestion->QTypeID == qtpOpenNumeric || $objQuestion->QTypeID == qtpCalc) && 
																		$objQuestion->SectionType == sectNormal) {
																	//Este valor no se puede obtener en este punto, se agrega el dummy para que se cargue mas adelante
																	$blnAddThisAnswer = false;
																	//@JAPR 2014-10-29: Agregado el mapeo de Scores
																	//Si se está pidiendo un score no aplica para esta pregunta
																	if ($intDataID == sdidValue) {
																		$insertData['qfield'.$intQuestionID] = array('GetFromStRec');
																	}
																	//@JAPR
																}
															}
														}
														
														if ($blnAddThisAnswer) {
															//@JAPR 2013-07-24: Agregado el mapeo de fotos durante la generación de acciones
															//Verifica si es una pregunta tipo foto o algún tipo especial no soportado, si lo es entonces
															//recibirá trato especial para grabarse o no se grabará en absoluto
															if (class_exists('BITAMQuestionOptionActionMap') && isset(BITAMQuestionOptionActionMap::$ArrayQuestionData) && 
																	isset(BITAMQuestionOptionActionMap::$ArrayQuestionData[$strQuestionField])) {
																$objQuestion = @BITAMQuestionOptionActionMap::$ArrayQuestionData[$strQuestionField]['Obj'];
																if (!is_null($objQuestion)) {
																	switch ($objQuestion->QTypeID) {
																		case qtpPhoto:
																			//Obtiene la ruta de la imagen para que mas adelante sea procesada y grabada
																			//como parte de eBavel también
																			if ($usePhotoPaths) {
																				//@JAPR 2014-10-29: Agregado el mapeo de Scores
																				//Si se está pidiendo un score no aplica para esta pregunta
																				if ($intDataID == sdidValue) {
																					//En este caso es un nombre de archivo, así que no decodifica nada
																					$otherPostKey = "qimageencode".$intQuestionID;
																					$strQuestionValue = (string) @$arrayData[$otherPostKey];
																					if (!is_null($strQuestionValue) && trim($strQuestionValue) !== '') {
																						$insertData['qfield'.$intQuestionID] = $strQuestionValue;
																					}
																				}
																				//@JAPR
																				
																				//Se marca para que ya no intente obtener la respuesta mas adelante
																				$blnAddThisAnswer = false;
																			}
																			break;
																		
																		case qtpSketch:
																		case qtpSignature:
																		case qtpDocument:
																		case qtpAudio:
																		case qtpVideo:
																			//No soportado por ahora
																			break;
																	}
																}
																else {
																}
															}
															
															if ($blnAddThisAnswer) {
																//@JAPR 2014-10-29: Agregado el mapeo de Scores
																//Ajusta la variable dependiendo de si se está o no pidiendo el score (el default es el valor)
																$strQuestionVariable = "@Q$intQuestionID";
																$strQuestionPostKey = 'qfield'.$intQuestionID;
																if ($intDataID == sdidScore) {
																	$strQuestionVariable .= ".score()";
																	$strQuestionPostKey .= "Score";
																}
																//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
																$strQuestionValue = replaceQuestionVariables($strQuestionVariable, $arrFieldValues, false, $aRepository, $surveyID, false);
																if (!is_null($strQuestionValue) && trim($strQuestionValue) !== '') {
																	$insertData[$strQuestionPostKey] = $strQuestionValue;
																}
																//@JAPR
															}
															//@JAPR
														}
														//@JAPR
													}
													//@JAPR 2014-10-29: Agregado el mapeo de Constantes fijas
													elseif ($intQuestionID < 0) {
														if (getParamValue('DebugBreak', 'both', '(int)', true)) {
															echo("<br>\r\nChecking for Expressions #2: {$intQuestionID}");
														}
														switch ($intQuestionID * -1) {
															case sfidFixedStrExpression:
															case sfidFixedNumExpression:
																//En este caso se trata de una expresión que se puede resolver a un texto o a un número, pero
																//en ambos casos puede contener variables, así que se tiene que hacer un replace de los valores
																//de dichas variables y al final aplicar la conversión a número si es necesario. Adicionalmente
																//si la expresión inicia con el caracter "=" se consideraría una fórmula, por lo que hay
																//que aplicar un eval antes de continuar
																$strFormula = ((string) @$objQuestionOptionFieldMap->ArrayVariablesFormula[$inteBavelFieldID]);
																//@JAPR 2014-11-25: Corregido un bug, no delimitará las variables de preguntas/secciones tipo string para permitir mapear directamente
																//los valores sin comillas
																$strFormulaValue = replaceQuestionVariables($strFormula, $arrFieldValues, false, $aRepository, $surveyID, false, true);
																
																//En este punto la fórmula podría necesitar evaluarse antes de ser válida
																//@JAPR 2014-10-30: Finalmente después de analizarlo, LRoux determinó no permitir evaluar
																//fórmulas en PhP para evitar la posible inyección de código por parte del usuario
																if (false && substr($strFormulaValue, 0, 1) == '=') {
																	try {
																		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																			echo("<br>\r\nEvaluating mapped eBavel formula: ".'$strFormulaValueResult = "'.substr($strFormulaValue, 1).'"');
																		}
																		@eval('$strFormulaValueResult = '.substr($strFormulaValue, 1));
																	}
																	catch (Exception $e) {
																		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																			echo("<br>\r\nEvaluating mapped eBavel formula error: ".$e->getMessage());
																		}
																		$strFormulaValueResult = null;
																	}
																	
																	$strFormulaValue = $strFormulaValueResult;
																	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																		echo("<br>\r\nEvaluating mapped eBavel formula result: {$strFormulaValue}");
																	}
																}
																
																//Finalmente aplica la conversión si es un tipo de expresión que debería ser numérica
																if ($intQuestionID * -1 == sfidFixedNumExpression) {
																	$strFormulaValue = (float) @$strFormulaValue;
																}
																else {
																	//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
																	$strFormulaValue = (string) @$strFormulaValue;
																}
																
																//Agrega el resultado a la acción utilizando el ID del campo de eBavel correspondiente
																$insertData["value".$inteBavelFieldID] = $strFormulaValue;
																break;
															
															//@JAPR 2014-10-30: Agregado el mapeo de atributos de catálogos
															case sfidCatalogAttribute:
																$intAttributeID = ((int) @$objQuestionOptionFieldMap->ArrayVariablesMemberID[$inteBavelFieldID]);
																$strAttribValue = getEntryAttributeValueForAction($aRepository, $surveyID, $intAttributeID, $arrFieldValues, $anEntrySectionID, $anEntryCatDimID, $aDynamicPageDSC, $aMultiRecordNum);
																//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
																$insertData["attribValue".$intAttributeID] = $strAttribValue;
																break;
														}
													}
													//@JAPR
												}
												
												//Debe agregar además una referencia a la pregunta y opción de respuesta que produce
												//la acción, esto para mas adelante poder recuperar el mapeo completo
												$insertData["QuestionID"] = $aQuestion->QuestionID;
												$insertData["QuestionAnswer"] = $anOption->QuestionOptionName;
												$insertData["eBavelActionSrcType"] = "answer";
												$insertData["eBavelActionFormID"] = $anOption->eBavelActionFormID;
												$insertData["eBavelFieldMap"] = $objQuestionOptionFieldMap;
											}
											//@JAPR
											
											$arrActionItems[] = $insertData;
										   	//$manager->insertData('ACTIONITEMS', $insertData, array(), $aRepository->ADOConnection);
										   	//@JAPR
										}
									}
								}
								else 
								{
									//En este caso, por cada posible respuesta definida en la pregunta que tenga una acción seleccionada, se 
									//verifica si fué o no elegida y toma la acción correspondiente
									$arrAnswers = array_flip($arrAnswers);
									foreach ($arrOptionsColl as $strOptionText => $anOption)
									{
										$blnSelected = ($anOption->InsertWhen == ioaChecked);
										//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
										//Ahora verificará si existe una condición para generar la acción, si la hay y se cumple entonces si
										//se mandarán los datos, de lo contrario simplemente registra la condición no cumplica en el log
										$blnAddAction = true;
										if (!is_null($anOption) && $anOption->ActionCondition) {
											//Hay una condición, así que la evalúa para decidir si se manda o no la acción
											$strActionCondition = replaceQuestionVariables($anOption->ActionCondition, $arrFieldValues, false, $aRepository, $surveyID);
											try {
												//Para tratar de reducir riesgo
												$strActionConditionVal = eval("return ({$strActionCondition});");
												if (getParamValue('DebugBreak', 'both', '(int)', true)) {
													echo("<br>\r\n Action condition for multi answer: {$aQuestion->QuestionID} => ".@$strOptionText." == {$strActionConditionVal}");
												}
											} catch (Exception $e) {
												//No importa el error, simplemente no habrá valor asignado
												$strActionConditionVal = 0;
												if (getParamValue('DebugBreak', 'both', '(int)', true)) {
													echo("<br>\r\n Error evaluating action condition for multi answer: {$aQuestion->QuestionID} => ".@$strOptionText." == ".$e->getMessage());
												}
											}
											
											$blnAddAction = $strActionConditionVal;
										}
										
										//La condición para que se genere la acción es que la múltiple choice está configurada ya se para generar
										//cuando la opción se marca o bien se desmarca, además que la opción precisamente viniera en el estado
										//marcado o desmarcado según el caso, por eso xor sólo nos dejará pasar si ambas están marcadas o 
										//desmarcadas, además ahora se agregó que se cumpla la condición si hay alguna configurada
										if (!($blnSelected xor isset($arrAnswers[$strOptionText])) && $blnAddAction)
										{
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												echo("<br>\r\nAdding action for multi answer: {$aQuestion->QuestionID} => ".@$strOptionText);
											}
											@logeBavelString("Adding action for multi answer: {$aQuestion->QuestionID} => ".@$strOptionText);
											//Sólo entra aquí si la Acción se debe insertar cuando se seleccione la opción y esta si fué seleccionada
											//o bien se inserta cuando no se selecciona y esta no fué seleccionada
											$intResponsibleID = (int) $anOption->ResponsibleID;
											
											//@JAPR 2013-05-07: Pospuesto este cambio porque eBavel dejó de manejar las acciones como antes así que ya no funcionaba lo que hacía eForms
											//@JAPR 2013-05-06: Corregido un bug, la funcionalidad de Supervisores adicionales no funcionaba con Apps antes de esveBavelMapping, así que se
											//validará para que en esos casos se force a enviar IDs positivos que permitan usarla, y solo en las mas recientes envíe IDs negativos como debe ser
											//@JAPR 2013-07-15: Habilitado nuevamente este cambio porque ya se soportan acciones de eBavel otra vez
											if ($blnUseMaxLongDummies && ($intResponsibleID >= MAX_LONG -3) && $appVersion >= esvMultiDynamicAndStandarization && $appVersion < esveBavelSuppActions123) {
												//En este caso el App no soportaba valores negativos, por tanto se forzará a usar los IDs cercanos al Long como Supervisores y MySelf,
												//sin embargo primero se valida si la BD tiene o no algún usuario registrado con dicho ID, ya que si lo tiene, no puede basarse en este
												//rango para valores dummy
												$intResponsibleID = $intResponsibleID - MAX_LONG;
											}
											else {
												if ($appVersion >= esveBavelSuppActions123) {
													//@JAPR 2013-07-15: Agregado el mapeo de responsables supervisores 1 al 3 y MySelf desde el servicio, ahora los valores negativos mayores
													//del -255 son válidos, por lo que se cambió el valor default de no asignado a -255, sin embargo para la pregunta anteriormente las
													//definiciones grababan un -1 como no asignado para el responsable, pero como ese valor corresponde al SUPERVISOR 1, se tendrá que
													//sumar uno al valor real grabado en la definición para representar al elemento correcto si los valores grabados son negativos, en
													//otras palabras, mientras el server usará -1, -2 y -3 para los SUPERVISOR 1, 2 y 3 respectivamente, en la definición del App se
													//grabarán -2, -3 y -4 respectivamente porque -1 ya era usado como el anterior valor de no asignado del QuestionCls.ResponsibleID y
													//por tanto todas las definiciones actuales ya traerían dicho valor
													if ($intResponsibleID < 0) {
														$intResponsibleID++;
													}
												}
											}
											//@JAPR
											
											if ($intResponsibleID <= 0)
											{
												//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
												switch ($intResponsibleID) {
													case 0:			//Significa que se desea enviar al usuario que hace la captura
														break;
													case -1:
													case -2:
													case -3:
														//Se usa el índice del SUPERVISOR correspondiente, si no estuviera asignado entonces la validación de 
														//abajo lo forzará a usar al mismo usuario que hace la captura
														$intResponsibleID = (int) @$arrEBavelSupervisors[$intResponsibleID];
														break;
													default:		//Casos no soportados, la validación de abajo lo forzará a usar al mismo usuario que hace la captura
														break;
												}
												
												if ($intResponsibleID <= 0) {
													$intResponsibleID = $_SESSION["PABITAM_UserID"];
												}
												//@JAPR
											}
											$strAction = (string) $anOption->ActionText;
											$strCategory = (string) @$arrCategories[$anOption->CategoryID];
											$intDaysDueDate = (int) $anOption->DaysDueDate;
											if (is_null($intDaysDueDate) || trim((string) $intDaysDueDate) == '' || ((int) $intDaysDueDate) == 0)
											{
												//Si es null, se usa directamente la fecha actual
												$intDaysDueDate = $thisDate;
											}
											else if (is_numeric($intDaysDueDate))
											{
												//Si es un número, entonces se aplica ese número como desplazamiento de la fecha actual
												$intDaysDueDate = days_add($thisDate, abs((int) $intDaysDueDate));
											}
											else
											{
												//Si no es un número se asume que ya es una fecha así que se usa directamente
												//$intDaysDueDate = abs((int) @$arrayActionValues[1]);
											}
											//@JAPR
											if (trim($strCategory) == '')
											{
												$strCategory = '('.translate('None').')';
											}
											
											//Se obtiene todas las partes de la pregunta tipo Acción para agregarlas a la acción del eBavel
											$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
											//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
											/*
										    $manager = new Tables();
										    $manager->applicationCodeName = $eBavelAppCodeName;
										    $manager->init($aRepository->ADOConnection);
										    */
											$insertData = array();
											$insertData["id_actionitem"] = -1;
											$insertData["createdDate"] = date('Y-m-d H:i:s');
											$insertData["modifiedDate"] = date('Y-m-d H:i:s');
											$insertData["section_id"] = $surveyInstance->eBavelFormID;
											$insertData["createdUser"] = $_SESSION["PABITAM_UserID"];
											$insertData["user"] = $intResponsibleID;
											//Este reemplazo se hace mediante los IDs de las preguntas porque sólo queda texto que se debió definir en
											//el Administrador, no capturado por el usuario en el App
											//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
											$insertData["description"] = replaceQuestionVariables($strAction, $arrFieldValues, false, $aRepository, $surveyID);
											$strActionTitle = (string) @$anOption->ActionTitle;
											if (trim($strActionTitle) != '') {
												//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
												$insertData["title"] = replaceQuestionVariables($strActionTitle, $arrFieldValues, false, $aRepository, $surveyID);
											}
											$insertData["email"] = 0;
											//@JAPR 2012-10-10: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
											//Se cambió el valor de estos campos de 0 a NULL para permitir enviar los EMails
											$insertData["row_key"] = null;
											$insertData["process_id"] = null;
											if ($intResponsibleID > 0) {
												$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
												require_once("appuser.inc.php");
												require_once("bpaemailconfiguration.inc.php");
												$objResource = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $intResponsibleID);
												$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
												if (!is_null($objResource)) {
													$tempEmailCC = @$objResource->AltEmail;
													if (trim($tempEmailCC) != '') {
														$blnCheckMail = BPAEmailConfiguration::check_email_address($tempEmailCC);
														if ($blnCheckMail)
														{
															$insertData["email_cc"] = $tempEmailCC;
														}
													}
												}
											}
											//@JAPR
											
											//0 = 'In Progress (Open)';
										   	//1 = 'Stand By';
										   	//2 = 'Finished';
											//@JAPR 2012-10-10: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
											//Agregado el tipo de Status a Request para permitir que se notifique sobre la acción
											//3 = 'Request';
											$insertData["status"] = 3;
											//@JAPR
											$insertData["source"] = "eForms";
											$insertData["sourceID"] = $surveyInstance->SurveyID;
											$insertData["sourceRow_key"] = $factKey;
											//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
											$insertData["category"] = $strCategory;
											$insertData["element"] = $strElement;
											$insertData["deadline"] = $intDaysDueDate;	//days_add($thisDate, $intDaysDueDate);
											//@JAPR 2013-07-05: Agregado el mapeo de una forma de eBavel para acciones
											//Verifica si se tiene mapeo de preguntas a campos de eBavel, si es así, entonces agrega como un
											//nuevo valor del array la respuesta de dicha pregunta en el registro actualmente procesado
											$objQuestionOptionFieldMap = @$anOption->QuestionOptionFieldMap;
											if (getMDVersion() >= esveBavelSuppActions && !is_null($objQuestionOptionFieldMap)) {
												foreach($objQuestionOptionFieldMap->ArrayVariables as $inteBavelFieldID => $strVariableName) {
													//Los valores > 0 indican mapeos hacia preguntas, por lo tanto agrega el valor al array
													$intQuestionID = ((int) @$objQuestionOptionFieldMap->ArrayVariableValues[$inteBavelFieldID]);
													//@JAPR 2014-10-29: Agregado el mapeo de Scores
													$intDataID = ((int) @$objQuestionOptionFieldMap->ArrayVariablesDataID[$inteBavelFieldID]);
													if ($intQuestionID > 0) {
														//@JAPR 2013-07-19: Corregido un bug, si las preguntas son numéricas y están en secciones estándar,
														//no se puede obtener el valor en este punto por lo que se validará el tipo y sección antes de 
														//intentar obtenerlo. Se agregará un array dummy que indicará que se obtenga justo antes de grabar
														//la acción, momento para el cual ya se tiene identificado correctamente
														$strQuestionField = 'qfield'.$intQuestionID;
														$blnAddThisAnswer = true;
														//Si se trata del registro único de las secciones estándar entonces no es necesario este proceso porque aquí si debe venir
														//asignado el valor de esta pregunta
														if (($anEntrySectionID != 0 || $anEntryCatDimID != 0) && 
																class_exists('BITAMQuestionOptionActionMap') && isset(BITAMQuestionOptionActionMap::$ArrayQuestionData) && 
																isset(BITAMQuestionOptionActionMap::$ArrayQuestionData[$strQuestionField])) {
															//Si se encuentra cargada la instancia, entonces se agregará un dummy para que el valor se
															//obtenga mas adelante si es una pregunta numérica de sección estándar
															$objQuestion = @BITAMQuestionOptionActionMap::$ArrayQuestionData[$strQuestionField]['Obj'];
															if (!is_null($objQuestion)) {
																if (($objQuestion->QTypeID == qtpOpenNumeric || $objQuestion->QTypeID == qtpCalc) && 
																		$objQuestion->SectionType == sectNormal) {
																	//Este valor no se puede obtener en este punto, se agrega el dummy para que se cargue mas adelante
																	$blnAddThisAnswer = false;
																	//@JAPR 2014-10-29: Agregado el mapeo de Scores
																	//Si se está pidiendo un score no aplica para esta pregunta
																	if ($intDataID == sdidValue) {
																		$insertData['qfield'.$intQuestionID] = array('GetFromStRec');
																	}
																	//@JAPR
																}
															}
														}
														
														if ($blnAddThisAnswer) {
															//@JAPR 2013-07-24: Agregado el mapeo de fotos durante la generación de acciones
															//Verifica si es una pregunta tipo foto o algún tipo especial no soportado, si lo es entonces
															//recibirá trato especial para grabarse o no se grabará en absoluto
															if (class_exists('BITAMQuestionOptionActionMap') && isset(BITAMQuestionOptionActionMap::$ArrayQuestionData) && 
																	isset(BITAMQuestionOptionActionMap::$ArrayQuestionData[$strQuestionField])) {
																$objQuestion = @BITAMQuestionOptionActionMap::$ArrayQuestionData[$strQuestionField]['Obj'];
																if (!is_null($objQuestion)) {
																	switch ($objQuestion->QTypeID) {
																		case qtpPhoto:
																			//Obtiene la ruta de la imagen para que mas adelante sea procesada y grabada
																			//como parte de eBavel también
																			if ($usePhotoPaths) {
																				//@JAPR 2014-10-29: Agregado el mapeo de Scores
																				//Si se está pidiendo un score no aplica para esta pregunta
																				if ($intDataID == sdidValue) {
																					//En este caso es un nombre de archivo, así que no decodifica nada
																					$otherPostKey = "qimageencode".$intQuestionID;
																					$strQuestionValue = (string) @$arrayData[$otherPostKey];
																					if (!is_null($strQuestionValue) && trim($strQuestionValue) !== '') {
																						$insertData['qfield'.$intQuestionID] = $strQuestionValue;
																					}
																				}
																				//@JAPR
																				
																				//Se marca para que ya no intente obtener la respuesta mas adelante
																				$blnAddThisAnswer = false;
																			}
																			break;
																		
																		case qtpSketch:
																		case qtpSignature:
																		case qtpDocument:
																		case qtpAudio:
																		case qtpVideo:
																			//No soportado por ahora
																			break;
																	}
																}
															}
															
															if ($blnAddThisAnswer) {
																//@JAPR 2014-10-29: Agregado el mapeo de Scores
																//Ajusta la variable dependiendo de si se está o no pidiendo el score (el default es el valor)
																$strQuestionVariable = "@Q$intQuestionID";
																$strQuestionPostKey = 'qfield'.$intQuestionID;
																if ($intDataID == sdidScore) {
																	$strQuestionVariable .= ".score()";
																	$strQuestionPostKey .= "Score";
																}
																//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
																$strQuestionValue = replaceQuestionVariables($strQuestionVariable, $arrFieldValues, false, $aRepository, $surveyID, false);
																//@JAPR
																if (!is_null($strQuestionValue) && trim($strQuestionValue) !== '') {
																	$insertData[$strQuestionPostKey] = $strQuestionValue;
																}
															}
															//@JAPR
														}
														//@JAPR
													}
													//@JAPR 2014-10-29: Agregado el mapeo de Constantes fijas
													elseif ($intQuestionID < 0) {
														if (getParamValue('DebugBreak', 'both', '(int)', true)) {
															echo("<br>\r\nChecking for Expressions #3: {$intQuestionID}");
														}
														switch ($intQuestionID * -1) {
															case sfidFixedStrExpression:
															case sfidFixedNumExpression:
																//En este caso se trata de una expresión que se puede resolver a un texto o a un número, pero
																//en ambos casos puede contener variables, así que se tiene que hacer un replace de los valores
																//de dichas variables y al final aplicar la conversión a número si es necesario. Adicionalmente
																//si la expresión inicia con el caracter "=" se consideraría una fórmula, por lo que hay
																//que aplicar un eval antes de continuar
																$strFormula = ((string) @$objQuestionOptionFieldMap->ArrayVariablesFormula[$inteBavelFieldID]);
																//@JAPR 2014-11-25: Corregido un bug, no delimitará las variables de preguntas/secciones tipo string para permitir mapear directamente
																//los valores sin comillas
																$strFormulaValue = replaceQuestionVariables($strFormula, $arrFieldValues, false, $aRepository, $surveyID, false, true);
																
																//En este punto la fórmula podría necesitar evaluarse antes de ser válida
																//@JAPR 2014-10-30: Finalmente después de analizarlo, LRoux determinó no permitir evaluar
																//fórmulas en PhP para evitar la posible inyección de código por parte del usuario
																if (false && substr($strFormulaValue, 0, 1) == '=') {
																	try {
																		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																			echo("<br>\r\nEvaluating mapped eBavel formula: ".'$strFormulaValueResult = "'.substr($strFormulaValue, 1).'"');
																		}
																		@eval('$strFormulaValueResult = '.substr($strFormulaValue, 1));
																	}
																	catch (Exception $e) {
																		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																			echo("<br>\r\nEvaluating mapped eBavel formula error: ".$e->getMessage());
																		}
																		$strFormulaValueResult = null;
																	}
																	
																	$strFormulaValue = $strFormulaValueResult;
																	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																		echo("<br>\r\nEvaluating mapped eBavel formula result: {$strFormulaValue}");
																	}
																}
																
																//Finalmente aplica la conversión si es un tipo de expresión que debería ser numérica
																if ($intQuestionID * -1 == sfidFixedNumExpression) {
																	$strFormulaValue = (float) @$strFormulaValue;
																}
																else {
																	//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
																	$strFormulaValue = (string) @$strFormulaValue;
																}
																
																//Agrega el resultado a la acción utilizando el ID del campo de eBavel correspondiente
																$insertData["value".$inteBavelFieldID] = $strFormulaValue;
																break;
															
															//@JAPR 2014-10-30: Agregado el mapeo de atributos de catálogos
															case sfidCatalogAttribute:
																$intAttributeID = ((int) @$objQuestionOptionFieldMap->ArrayVariablesMemberID[$inteBavelFieldID]);
																$strAttribValue = getEntryAttributeValueForAction($aRepository, $surveyID, $intAttributeID, $arrFieldValues, $anEntrySectionID, $anEntryCatDimID, $aDynamicPageDSC, $aMultiRecordNum);
																//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
																$insertData["attribValue".$intAttributeID] = $strAttribValue;
																break;
														}
													}
													//@JAPR
												}
												
												//Debe agregar además una referencia a la pregunta y opción de respuesta que produce
												//la acción, esto para mas adelante poder recuperar el mapeo completo
												$insertData["QuestionID"] = $aQuestion->QuestionID;
												$insertData["QuestionAnswer"] = $strOptionText;
												$insertData["eBavelActionSrcType"] = "answer";
												$insertData["eBavelActionFormID"] = $anOption->eBavelActionFormID;
												$insertData["eBavelFieldMap"] = $objQuestionOptionFieldMap;
											}
											//@JAPR
											
											$arrActionItems[] = $insertData;
										   	//$manager->insertData('ACTIONITEMS', $insertData, array(), $aRepository->ADOConnection);
										   	//@JAPR
										}
									}
								}
							}
						}
					}
				}
				catch (Exception $e)
				{
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error inserting eBavel action")." (".(!is_null(@$questionKey)?translate('Question Id').': '.$questionKey:'').") ".": ".$e->getMessage();
					global $queriesLogFile;
			        $aLogString = $gblEFormsErrorMessage."\r\neBavelAppCodeName: ".$eBavelAppCodeName.', data: '.serialize($insertData);
			        @error_log($aLogString, 3, $queriesLogFile);
				}
			}
		   	$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		}
		
		//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
		//Si la encuesta está configurada para enviar EMails adicionales, en este punto identifica basado en las respuestas del registro si
		//hay nuevos EMails a agregar en el array
		if (getMDVersion() >= esvReportEMailsByCatalog && trim($surveyInstance->AdditionalEMails) != '') {
			$strAdditionalEMails = $surveyInstance->AdditionalEMails;
			if (stripos($surveyInstance->AdditionalEMails, '@Q') !== false) {
				//En este caso se debe traducir el texto para reemplazar las variables que utiliza
				$strAdditionalEMails = (string) @replaceQuestionVariables($strAdditionalEMails, $arrFieldValues, true, $aRepository, $surveyID, false, true);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n Additional EMails: {$strAdditionalEMails}");
				}
			}
			
			//@JAPR 2014-01-27: Corregido un bug, debe hacer este proceso no sólo si hay atributos, por si vienen EMails fijos
			if (trim($strAdditionalEMails) != '') {
				$arrNewEMails = explode(';', $strAdditionalEMails);
				foreach ($arrNewEMails as $strEMail) {
					//Verifica que el EMail sea válido antes de intentar utilizarlo
					if (trim($strEMail) != '' && strpos($strEMail, '@') !== false && strlen($strEMail) >= 3) {
						if (!isset($arrAdditionalEMails[$strEMail])) {
							$arrAdditionalEMails[$strEMail] = $strEMail;
						}
					}
				}
			}
		}
		//@JAPR
	}
	//@JAPR

	//En esta parte validamos si se trata del repositorio de Bellisima para realizar unos UPDATES que se requieren
	//para cierta dimension de la encuesta que estan manejando
	if(trim($_SESSION["PABITAM_RepositoryName"])=="fbm_bmd_0211" && $surveyInstance->SurveyID==1) 
	{
		//Actualizar tabla de hechos
		$sqlFactTable = "UPDATE RIFACT_29006 A, RIDIM_29024 B, RIDIM_29037 C 
						SET A.RIDIM_29037KEY = C.RIDIM_29037KEY 
						WHERE A.RIDIM_29024KEY = B.RIDIM_29024KEY AND B.DSC_29024 = C.DSC_29043";
		
		if($aRepository->DataADOConnection->Execute($sqlFactTable) === false)
		{
			if($gblShowErrorSurvey)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." RIFACT_29006 ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlFactTable);
			}
			else 
			{
				//return ("(".__METHOD__.") ".translate("Error accessing")." RIFACT_29006 ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlFactTable);
			}
		}
		
		//Actualizar tabla paralela
		$sqlSVTable = "UPDATE RIFACT_29006 A, RIDIM_29024 B, RIDIM_29037 C, SVSurvey_29006 D 
						SET D.dim_52 = C.RIDIM_29037KEY 
						WHERE A.RIDIM_29024KEY = B.RIDIM_29024KEY AND B.DSC_29024 = C.DSC_29043 AND A.FactKey = D.FactKey";
		
		if($aRepository->DataADOConnection->Execute($sqlSVTable) === false)
		{
			if($gblShowErrorSurvey)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SVSurvey_29006 ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlSVTable);
			}
			else 
			{
				//return ("(".__METHOD__.") ".translate("Error accessing")." SVSurvey_29006 ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlSVTable);
			}
		}
	}
	
	if($strResult=="" || $strResult=="OK")
	{
		//@JAPR 2014-10-27: Corregido un bug, el indicador duración sólo se debe grabar en el registro de la sección estándar
		//Agregados los parámetros $anEntrySectionID = 0, $anEntryCatDimID = 0
		//@JAPR 2014-11-20: Agregada la dimensión Agenda
		//Agregado el parámetro $anAgendaKey para especificar si se grabará o no la dimensión agenda correspondiente a la captura
		//Un valor de null provoca que no se grabe, así que las capturas que no son de agenda deben enviar el key del valor de NA
		$strResult = saveDataInGlobalSurveyModel($aRepository, $surveyInstance, $arrayData, $thisDate, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID, $agendaKey);
	}
	else 
	{
		return $strResult;	
	}
	
	//@JAPR 2013-05-014: Corregido un bug, se estaba enviando doble el escenario cuando existían secciones dinámicas o Maestro-Detalle en la 
	//encuesta debido al grabado del registro único al final del proceso, así que se validará que en esos casos no intente el envío a menos que
	//se esté grabando precisamente el registro único
	$blnSendEMailDashboards = $sendEmailDashboards;
	if ($surveyInstance->UseStdSectionSingleRec) {
		//Si hay registro único, sólo debe hacer el envío si se trata precisamente del registro único, de lo contrario se respeta el valor
		//que ya traiga la variable para forzar el envío del EMail
		if ($anEntrySectionID == 0 && $anEntryCatDimID == 0) {
			$blnSendEMailDashboards = true;
		}
		else {
			$blnSendEMailDashboards = false;
		}
	}
	
	if($captureVia==0 && $userID!=-1)
	{
		//Antes de enviar el PDF y despues de haber almacenados los datos
		//en la encuesta DSD Supervision, procedemos a ejecutar un SP de DHuerta
		//@JAPR 2013-05-014: Corregido un bug, se estaba enviando doble el escenario cuando existían secciones dinámicas o Maestro-Detalle en la 
		//encuesta debido al grabado del registro único al final del proceso, así que se validará que en esos casos no intente el envío a menos que
		//se esté grabando precisamente el registro único
		if($blnSendEMailDashboards==true)
		{
			//Ejecutamos SP requerido por Diego Huerta para base de datos fbm_bmd_0030 y para la
			//encuesta DSD Supervision
			if(trim($_SESSION["PABITAM_RepositoryName"])=="fbm_bmd_0030" && $surveyInstance->SurveyID==5) 
			{
				$configSPName = "SP_ETL_BUSINESS_OPPTY";
				$strParameters = $factKeyDimVal;
				
				$strStmtSP = "CALL ".$configSPName." (".$strParameters.");";
				
				if($aRepository->DataADOConnection->Execute($strStmtSP) === false)
				{
					if($gblShowErrorSurvey)
					{
						//die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$strStmtSP);
					}
					else 
					{
						//return ("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$strStmtSP);
					}
				}
			}
			else if(trim($_SESSION["PABITAM_RepositoryName"])=="fbm_bmd_0030" && $surveyInstance->SurveyID==6) 
			{
				$configSPName = "SP_ETL_SURVEY_COUNT";
				$strParameters = $factKeyDimVal;
				
				$strStmtSP = "CALL ".$configSPName." (".$strParameters.");";
				
				if($aRepository->DataADOConnection->Execute($strStmtSP) === false)
				{
					if($gblShowErrorSurvey)
					{
						//die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$strStmtSP);
					}
					else 
					{
						//return ("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$strStmtSP);
					}
				}
			}
		}
		
		//Enviamos los PDFs de los escenarios asociados a los schedulers que estan asociados a dicho usuario
		//logueado, si la funcion de envio de PDFs regresa false eso quiere decir que probablemente no 
		//haya escenarios asociados y por lo tanto se procede a realizar el envio de la encuesta en PDF
		//como se ha realizado hasta ahora
		//@JAPR 2013-05-014: Corregido un bug, se estaba enviando doble el escenario cuando existían secciones dinámicas o Maestro-Detalle en la 
		//encuesta debido al grabado del registro único al final del proceso, así que se validará que en esos casos no intente el envío a menos que
		//se esté grabando precisamente el registro único
		$statusSendDashboards = sendDashboards($aRepository, $surveyInstance, $arrayData["userID"], $factKeyDimVal, $blnSendEMailDashboards);
		
		if(!$statusSendDashboards)
		{
			//Solo si esta activado el centinela entonces se enviara el correo electronico del PDF
			//@JAPR 2013-05-014: Corregido un bug, se estaba enviando doble el escenario cuando existían secciones dinámicas o Maestro-Detalle en la 
			//encuesta debido al grabado del registro único al final del proceso, así que se validará que en esos casos no intente el envío a menos que
			//se esté grabando precisamente el registro único
			if($blnSendEMailDashboards==true)
			{
				//Realizamos instancia del usuario
				$appUserInstance = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $arrayData["userID"]);
				
				//Verificar que el usuario tenga activado el envio de PDF por correo
				if($appUserInstance->SendPDFByEmail==1)
				{
					//Genero el documento PDF de prueba
					set_time_limit(0);
					//@JAPR 2013-05-14 Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
					require_once("exportsurvey.inc.php");
					/*
					$docPDF = BITAMExportSurvey::NewInstanceWithID($aRepository, $surveyInstance->SurveyID, $factKey);
					$docPDF->exportSurveyToPDF();
					*/
					//require_once("report.inc.php");		//report.inc.php ya se incluye como parte de exportSurvey.inc.php
					$docPDF = BITAMReport::NewInstanceWithID($aRepository, $factKey, $surveyInstance->SurveyID, true, true);
					$docPDF->exportToPDF();
					$docPDF->sendNotification();
					//@JAPR
					$arrayPDF = array();
					$arrayPDF[] = (string) @$docPDF->filePath;
				}
			}
		}
	}
	
	//Insertamos el registro en las estadisticas
	//@JAPR 2013-05-014: Corregido un bug, se estaba enviando doble el escenario cuando existían secciones dinámicas o Maestro-Detalle en la 
	//encuesta debido al grabado del registro único al final del proceso, así que se validará que en esos casos no intente el envío a menos que
	//se esté grabando precisamente el registro único
	if($blnSendEMailDashboards) {
		if(isset($arrayData["captureType"])) {
			if($captureVia == 1) {
			$captureType = "Saved by email";
			} else {
			$captureType = "Saved by web";
			}
		} else {
			$captureType = "Saved by mobile";
		}
		insertStatistics($aRepository, $surveyID, $thisDate,$captureType,$countSurveys);
	}

	return $strResult;
}

//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
//Graba un sólo registro que contiene a todas las preguntas de secciones estándar de la encuesta (incluso si estas vienen vacias), sólo no 
//grabaría el registros si no existíeran preguntas de este tipo (por ejemplo una encuesta con puras secciones formateadas o maestro-detalle, o
//preguntas categoría de dimensión).
//Este registro sólo se agrega si la encuesta está configurada como UseStdSectionSingleRec, ya que de lo contrario los valores de estas preguntas
//se grababan como parte de los registros de otras secciones o de Categorías de dimensión
function singleSaveStandardData($aRepository, $surveyID, $arrayData, $thisDate, &$factKeyDimVal=0)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	/*@MABH20121205*/
	global $appVersion;
	//@MABH20121205
	
	//Ya no es necesario asignar el 0 explícitamente, si el parámetro no se envía entonces tomará el 0 automáticamente, por el contrario reutilizará
	//el valor recibido por ejemplo del grabado de la sección Maestro - Detalle
	//$factKeyDimVal = 0;
	//@JAPRWarning: Hay que validar que el envío del EMail ocurra sólo hasta este punto, ya que originalmente ocurría en las funciones que generaban
	//las dinámias o las maestro-detalle, pero ahora por encuesta pudiera ser ahi o nada mas en este punto que es el último registro grabado
	$sendEmailDashboards = false;
	
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	
	$newArrayData = array();
	$newArrayData["surveyDate"] = $arrayData["surveyDate"];
	$newArrayData["surveyHour"] = $arrayData["surveyHour"];
	$newArrayData["userID"] = $arrayData["userID"];
	$newArrayData["appUserID"] = $arrayData["appUserID"];
	/*@MABH20121205*/
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$newArrayData["serverSurveyDate"] = (string) @$arrayData["serverSurveyDate"];
		$newArrayData["serverSurveyHour"] = (string) @$arrayData["serverSurveyHour"];
		$newArrayData["serverSurveyEndDate"] = (string) @$arrayData["serverSurveyEndDate"];
		$newArrayData["serverSurveyEndHour"] = (string) @$arrayData["serverSurveyEndHour"];
	}
	//@MABH20121205
	$newArrayData["surveyFinished"] = $arrayData["surveyFinished"];
	//@JAPR 2013-04-10: A este día, cuando se hacía un Reupload no venían estos datos porque llegaban como parámetro $_POST y no como parte del
	//archivo, por lo tanto generaban Warnings así que se validará en lo que se integran al archivo
	$newArrayData["surveyLatitude"] = @$arrayData["surveyLatitude"];
	$newArrayData["surveyLongitude"] = @$arrayData["surveyLongitude"];
	//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
	$newArrayData["surveyAccuracy"] = @$arrayData["surveyAccuracy"];
	//@JAPR
	
	if(isset($arrayData["CaptureVia"]))
	{
		$newArrayData["CaptureVia"] = $arrayData["CaptureVia"];
	}
	
	if(isset($arrayData["CaptureEmail"]))
	{
		$newArrayData["CaptureEmail"] = $arrayData["CaptureEmail"];
	}
	
	if(isset($arrayData["SchedulerID"]))
	{
		$newArrayData["SchedulerID"] = $arrayData["SchedulerID"];
	}
	
	if(isset($arrayData["qsignature"]))
	{
		$newArrayData["qsignature"] = $arrayData["qsignature"];
	}
	
	if(isset($arrayData["captureType"]))
	{
		$newArrayData["captureType"] = $arrayData["captureType"];
	}
	
	//Obtiene la referencia de las secciones que no se deben procesar
    $dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $surveyID);
    $arrMasterDetSections = BITAMSection::existMasterDetSections($aRepository, $surveyID, true);
    if (!is_array($arrMasterDetSections) || count($arrMasterDetSections) == 0) {
    	$arrMasterDetSections = array();
    }
    $arrMasterDetSectionsIDs = array_flip($arrMasterDetSections);
	//@JAPR 2014-05-30: Agregado el tipo de sección Inline
    $arrInlineSections = BITAMSection::existInlineSections($aRepository, $surveyID, true);
    if (!is_array($arrInlineSections) || count($arrInlineSections) == 0) {
    	$arrInlineSections = array();
    }
    $arrInlineSectionsIDs = array_flip($arrInlineSections);
    //@JAPR
	
	//Recorre el array de preguntas y sólo procesa aquellas que son de secciones estándar, ya que al llegar a esta función se asume que cualquier
	//otra sección que requiera grabar multiples registros ya debió ser invocada y grabada previamente
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		//@JAPR 2013-01-24: Corregido un bug, cuando se combinan Maestro - Detalle con dinámicas, como se estaba dejando sin asignar el 
		//qfield las preguntas que son numéricas marcaban un error de truncado porque les intentaban asignar ''
		//Se agregó la validación para que en caso de ser una pregunta que se graba en campo numérico asigne un 0 (multiples-> numéricas o checkbox)
		if($dynamicSectionID > 0 && $aQuestion->SectionID == $dynamicSectionID)
		{
			if ($aQuestion->QTypeID == qtpMulti && ($aQuestion->MCInputType == mpcCheckBox || $aQuestion->MCInputType == mpcNumeric))
			{
				$postKey = "qfield".$aQuestion->QuestionID;
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$newArrayData[$postKey] = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
				continue;
			}
		}
		//@JAPR
		
		//Si las preguntas pertenecen a secciones que no son estándar entonces son omitidas
		//@JAPR 2014-05-30: Agregado el tipo de sección Inline
		if($aQuestion->SectionID == $dynamicSectionID || isset($arrMasterDetSectionsIDs[$aQuestion->SectionID]) || isset($arrInlineSectionsIDs[$aQuestion->SectionID])) {
			continue;
		}
		//@JAPR
		
		//Si las preguntas son tipo categoría de dimensión, incluso si son de secciones estándar también son omitidas pues generan múltiples
		//registros y en ese caso se hubieran procesado en su propia función previamente
		if ($aQuestion->QDisplayMode==dspVertical && $aQuestion->MCInputType==mpcNumeric && $aQuestion->IsMultiDimension==0 && $aQuestion->UseCategoryDimChoice!=0) {
			continue;
		}
		
		//Procesamiento del dato de qfield
		$postKey = "qfield".$aQuestion->QuestionID;
		if(isset($arrayData[$postKey]))
		{
			$strValue = $arrayData[$postKey];
		}
		else 
		{
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			else 
			{
				$strValue = "";
			}
		}

		$newArrayData[$postKey] = $strValue;
		
		//Procesamiento del dato de qimageencode
		$otherPostKey = "qimageencode".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = "";
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Procesamiento del dato de comment
		$otherPostKey = "qcomment".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
		//Procesamiento del dato de document
		$otherPostKey = "qdocument".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		//@JAPR
		
		//Procesamiento del dato de action
		$otherPostKey = "qactions".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
	}
	
	$countSurveys = 1;
	$i=0;
	$strStatus = array ();
	$totalOK = 0;
	$sendEmailDashboards = true;
	$strStatus[$i] = saveData($aRepository, $surveyID, $newArrayData, $thisDate, $factKeyDimVal, $sendEmailDashboards, $countSurveys);
	
	if ($strStatus[$i] == "OK") 
	{
		$totalOK ++;
	}
	
	if ($totalOK == $countSurveys) 
	{
		$strStatusDyn = "OK";
	}
	else
	{
		$strStatusDyn = implode(";\r\n", $strStatus);
	}
	
	return $strStatusDyn;
}

function singleUpdateStandardData($aRepository, $surveyID, $arrayData, $dateID, $hourID, $userID, $thisDate, &$srcFactKeyDimVal=0)
{
	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	global $arrMasterDetSections;
	global $arrMasterDetSectionsIDs;
	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	global $arrInlineSections;
	global $arrInlineSectionsIDs;
	/*@MABH20121205*/
	global $appVersion;
	//@MABH20121205
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\nsingleUpdateStandardData");
	}
    
	$sendEmailDashboards = false;
	
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	
	$newArrayData = array();
	$newArrayData["surveyDate"] = $arrayData["surveyDate"];
	$newArrayData["surveyHour"] = $arrayData["surveyHour"];
	$newArrayData["userID"] = $arrayData["userID"];
	$newArrayData["appUserID"] = $arrayData["appUserID"];
	
	if(isset($arrayData['entryID']))
	{
		$newArrayData["entryID"] = $arrayData['entryID'];
	}
	$entryID = 0;
	if(isset($arrayData['entryID'])) 
	{
		$entryID = (int)$arrayData['entryID'];
	}
	
	/*@MABH20121205*/
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$newArrayData["serverSurveyDate"] = (string) @$arrayData["serverSurveyDate"];
		$newArrayData["serverSurveyHour"] = (string) @$arrayData["serverSurveyHour"];
		$newArrayData["serverSurveyEndDate"] = (string) @$arrayData["serverSurveyEndDate"];
		$newArrayData["serverSurveyEndHour"] = (string) @$arrayData["serverSurveyEndHour"];
	}
	//@MABH20121205	
	$newArrayData["surveyFinished"] = $arrayData["surveyFinished"];
	//@JAPR 2013-04-10: A este día, cuando se hacía un Reupload no venían estos datos porque llegaban como parámetro $_POST y no como parte del
	//archivo, por lo tanto generaban Warnings así que se validará en lo que se integran al archivo
	$newArrayData["surveyLatitude"] = @$arrayData["surveyLatitude"];
	$newArrayData["surveyLongitude"] = @$arrayData["surveyLongitude"];
	//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
	$newArrayData["surveyAccuracy"] = @$arrayData["surveyAccuracy"];
	//@JAPR
	
	if(isset($arrayData["CaptureVia"]))
	{
		$newArrayData["CaptureVia"] = $arrayData["CaptureVia"];
	}
	
	if(isset($arrayData["CaptureEmail"]))
	{
		$newArrayData["CaptureEmail"] = $arrayData["CaptureEmail"];
	}
	
	if(isset($arrayData["SchedulerID"]))
	{
		$newArrayData["SchedulerID"] = $arrayData["SchedulerID"];
	}
	
	if(isset($arrayData["qsignature"]))
	{
		$newArrayData["qsignature"] = $arrayData["qsignature"];
	}
	
	if(isset($arrayData["captureType"]))
	{
		$newArrayData["captureType"] = $arrayData["captureType"];
	}

	//Obtiene la referencia de las seccioens que no se deben procesar
    $dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $surveyID);
    $arrMasterDetSections = BITAMSection::existMasterDetSections($aRepository, $surveyID, true);
    if (!is_array($arrMasterDetSections) || count($arrMasterDetSections) == 0) {
    	$arrMasterDetSections = array();
    }
    $arrMasterDetSectionsIDs = array_flip($arrMasterDetSections);
	
	//Recorre el array de preguntas y sólo procesa aquellas que son de secciones estándar, ya que al llegar a esta función se asume que cualquier
	//otra sección que requiera grabar multiples registros ya debió ser invocada y grabada previamente
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		//@JAPR 2013-01-24: Corregido un bug, cuando se combinan Maestro - Detalle con dinámicas, como se estaba dejando sin asignar el 
		//qfield las preguntas que son numéricas marcaban un error de truncado porque les intentaban asignar ''
		//Se agregó la validación para que en caso de ser una pregunta que se graba en campo numérico asigne un 0 (multiples-> numéricas o checkbox)
		if($dynamicSectionID > 0 && $aQuestion->SectionID == $dynamicSectionID)
		{
			if ($aQuestion->QTypeID == qtpMulti && ($aQuestion->MCInputType == mpcCheckBox || $aQuestion->MCInputType == mpcNumeric))
			{
				$postKey = "qfield".$aQuestion->QuestionID;
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$newArrayData[$postKey] = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
				continue;
			}
		}
		//@JAPR

		//Si las preguntas pertenecen a secciones que no son estándar entonces son omitidas
		if($aQuestion->SectionID == $dynamicSectionID || isset($arrMasterDetSectionsIDs[$aQuestion->SectionID])) {
			continue;
		}
		//Si las preguntas son tipo categoría de dimensión, incluso si son de secciones estándar también son omitidas pues generan múltiples
		//registros y en ese caso se hubieran procesado en su propia función previamente
		if ($aQuestion->QDisplayMode==dspVertical && $aQuestion->MCInputType==mpcNumeric && $aQuestion->IsMultiDimension==0 && $aQuestion->UseCategoryDimChoice!=0) {
			continue;
		}
		
		//Procesamiento del dato de qfield
		$postKey = "qfield".$aQuestion->QuestionID;
		if(isset($arrayData[$postKey]))
		{
			$strValue = $arrayData[$postKey];
		}
		else 
		{
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			else 
			{
				$strValue = "";
			}
		}

		$newArrayData[$postKey] = $strValue;
		
		//Procesamiento del dato de qimageencode
		$otherPostKey = "qimageencode".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = "";
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Procesamiento del dato de comment
		$otherPostKey = "qcomment".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
		//Procesamiento del dato de document
		$otherPostKey = "qdocument".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		//@JAPR
		
		//Procesamiento del dato de action
		$otherPostKey = "qactions".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
	}
	
	$countSurveys = 1;
	$i=0;
	$strStatus = array ();
	$totalOK = 0;
	//$sendEmailDashboards = true;
	
	//Se debe obtener el FactKey del registro único de secciones estándar
	$tableName = $surveyInstance->SurveyTable;
	$registerID = null;
	$sql = "SELECT FactKey, FactKeyDimVal FROM ".$tableName." WHERE FactKeyDimVal = ".$entryID." AND EntrySectionID = 0 AND EntryCatDimID = 0";
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			return("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	if(!$aRS->EOF)
	{
		$registerID = (int) @$aRS->fields['factkey'];
		if ($registerID <= 0) {
			$registerID = null;
		}
	}
	
	if (is_null($registerID)) {
		$strStatus[$i] = saveData($aRepository, $surveyID, $newArrayData, $thisDate, $srcFactKeyDimVal, $sendEmailDashboards, $countSurveys);
	}
	else {
		$strStatus[$i] = updateData($aRepository, $surveyID, $newArrayData, $dateID, $hourID, $userID, $thisDate, $registerID, 0, 0, $srcFactKeyDimVal);
	}
	
	if ($strStatus[$i] == "OK") 
	{
		$totalOK ++;
	}
	
	if ($totalOK == $countSurveys) 
	{
		$strStatusMasterDet = "OK";
	}
	else
	{
		$strStatusMasterDet = implode(";\r\n", $strStatus);
	}
	
	return $strStatusMasterDet;
}
//@JAPR

function getMatrixDimFields($aRepository, $matrixCollection, $arrayData) 
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	require_once("../model_manager/indicatorkpi.inc.php");

    global $gblEFormsNA;
    $m = 0;
    $fieldNames = array();
    $fieldValues = array();

    if(count($matrixCollection->Collection) > 0) 
    {
        foreach ($matrixCollection->Collection as $aQuestion) 
        {
            $arrayConsecutiveIDs = array();
            $theQIndDimIDs = array();

            if ($aQuestion->OneChoicePer == 0) 
            {
                //PerRow
                $theQIndDimIDs = $aQuestion->QIndDimIds;
                $arrayConsecutiveIDs = $aQuestion->QConsecutiveIDs;
                $theQindicatorIds = $aQuestion->QIndicatorIds;
                $theScores = $aQuestion->Scores;
            } 
            else 
            {
                //PerColumn
                $theQIndDimIDs = $aQuestion->QIndDimIdsCol;
                $arrayConsecutiveIDs = $aQuestion->QConsecutiveIDsCol;
                $theQindicatorIds = $aQuestion->QIndicatorIdsCol;
                $theScores = $aQuestion->ScoresCol;
            }

            foreach ($arrayConsecutiveIDs as $key => $consecutiveID) 
            {
                //Obtenemos la instancia
                $instanceIndDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $theQIndDimIDs[$key]);

                //Obtenemos las respuestas
                $postKey = "qfield".$aQuestion->QuestionID."_".$consecutiveID;
                if (isset($arrayData[$postKey])) 
                {
                    if (trim($arrayData[$postKey]) == "") 
                    {
                        $value = $gblEFormsNA;
                    } 
                    else 
                    {
                        $value = $arrayData[$postKey];
                    }
                } 
                else 
                {
                    $value = $gblEFormsNA;
                }

                $fieldNames[$m] = $instanceIndDim->Dimension->TableName."KEY";
                
                $passValue = getMatrixDimKeyValue($aRepository, $instanceIndDim, $value);
                
                if($passValue!==false)
                {
                	$fieldValues[$m] = $passValue;
                }
                else 
                {
                	return false;
                }
                $m++;
                
                //Obtenemos el dato del indicador para revisar si tiene score
                $indicatorID = $theQindicatorIds[$key];
                if(!is_null($indicatorID) && $indicatorID>0)
                {
                	$firstScore = (float)$theScores[$key];
                	$instanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $indicatorID);
                	
                	$operator = null;
                	
                	if($aQuestion->OneChoicePer == 1)
                	{
                		if(isset($aQuestion->ScoreOperatorsCol[$key]))
                		{
                			$operator = (int)$aQuestion->ScoreOperatorsCol[$key];
                		}
                	}
                	
	                $passValue = getScoreValuesFromMatrix($aRepository, $aQuestion, $value, $firstScore, $operator);
	                
	                if($passValue!==false)
	                {
	                	$indValue = $passValue;
	                }
	                else 
	                {
	                	return false;
	                }

					$fieldNames[$m] = $instanceIndicator->field_name.$instanceIndicator->IndicatorID;
					$fieldValues[$m] = $indValue;
                	$m++;
                }
            }
        }
    }
    
    $return = array();
    $return['fieldNames'] = implode(", ", $fieldNames);
    $return['fieldValues'] = implode(", ", $fieldValues);
    return $return;
}

function getMatrixDimKeyValue($aRepository, $anInstanceIndDim, $value) 
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	
    $theValue = putQuotes($aRepository, $value);
    $tableName = $anInstanceIndDim->Dimension->TableName;
    $fieldKey = $tableName . "KEY";
    $fieldDesc = $anInstanceIndDim->Dimension->FieldDescription;
    
	$sql = "SELECT " . $fieldKey . " FROM " . $tableName . " WHERE " . $fieldDesc . " = " . $theValue . " ORDER BY " . $fieldKey;
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS === false) 
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing") . " " . $tableName . " " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing") . " " . $tableName . " " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql;
			return false;
		}
	}

	if (!$aRS->EOF) 
	{
		$valueKey = $aRS->fields[strtolower($fieldKey)];
	}
	else 
	{
		//Como no encontró un valor valido Entonces se retorna el valor de No Aplica
		$sql = "SELECT ".$fieldKey." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA);
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		$valueKey =  $aRS->fields[strtolower($fieldKey)];
	}
	return $valueKey;
}

function saveDataInMatrixTable($aRepository, $surveyInstance, $matrixCollection, $arrayData, $factKey, $factKeyDimVal) 
{
    global $gblEFormsNA;
    global $gblShowErrorSurvey;

    foreach ($matrixCollection->Collection as $questionKey => $aQuestion) {
        $arrayConsecutiveIDs = array();
        if ($aQuestion->OneChoicePer == 0) {
            //PerRow
            //obtenemos los consecutiveids
            $arrayConsecutiveIDs = $aQuestion->QConsecutiveIDs;
        } else {
            //PerColumn
            //obtenemos los consecutiveids
            $arrayConsecutiveIDs = $aQuestion->QConsecutiveIDsCol;
        }

        //Por cada consecutiveid vamos a generar el qfield_consecutiveid que tiene
        //cada fila o columna de la matrix y asi sacar la respuesta
        foreach ($arrayConsecutiveIDs as $consecutiveID) {
            //Obtener valor capturado de dicha question
            $postKey = "qfield" . $aQuestion->QuestionID . "_" . $consecutiveID;
            if (isset($arrayData[$postKey])) {
                if (trim($arrayData[$postKey]) == "") {
                    $value = "";
                } else {
                    $value = $arrayData[$postKey];
                }
            } else {
                $value = "";
            }
            $theValue = putQuotes($aRepository, $value);

            //Por cada valor de respuesta que tiene el arrayvalues
            //debe guardarse un registro en la tabla de matrix
            $sql = "INSERT INTO SVSurveyMatrixData_" . $surveyInstance->ModelID . "("
                    . "FactKey, FactKeyDimVal,QuestionID,ConsecutiveID,dim_value)"
                    . " VALUES ("
                    . $factKey . ", "
                    . $factKeyDimVal . ", "
                    . $aQuestion->QuestionID . ", "
                    . $consecutiveID . ", "
                    . $theValue .
                    ")";

			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyMatrixData_".$surveyInstance->ModelID." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					return ("(".__METHOD__.") ".translate("Error accessing")." "." SVSurveyMatrixData_".$surveyInstance->ModelID." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
        }
    }
    
    return "OK";
}

function updateDataInMatrixTable($aRepository, $surveyInstance, $matrixCollection, $arrayData, $factKey, $factKeyDimVal) 
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
    
    foreach ($matrixCollection->Collection as $questionKey => $aQuestion) {
        $arrayConsecutiveIDs = array();
        if ($aQuestion->OneChoicePer == 0) {
            //PerRow
            //obtenemos los consecutiveids
            $arrayConsecutiveIDs = $aQuestion->QConsecutiveIDs;
        } else {
            //PerColumn
            //obtenemos los consecutiveids
            $arrayConsecutiveIDs = $aQuestion->QConsecutiveIDsCol;
        }

        //Por cada consecutiveid vamos a generar el qfield_consecutiveid que tiene
        //cada fila o columna de la matrix y asi sacar la respuesta
        foreach ($arrayConsecutiveIDs as $consecutiveID) {
            //Obtener valor capturado de dicha question
            $postKey = "qfield" . $aQuestion->QuestionID . "_" . $consecutiveID;
            if (isset($arrayData[$postKey])) {
                if (trim($arrayData[$postKey]) == "") {
                    $value = "";
                } else {
                    $value = $arrayData[$postKey];
                }
            } else {
                $value = "";
            }
            $theValue = putQuotes($aRepository, $value);

            //Por cada valor de respuesta que tiene el arrayvalues
            //debe guardarse un registro en la tabla de matrix
            $sql = "UPDATE SVSurveyMatrixData_" . $surveyInstance->ModelID 
                    . " SET dim_value=".$theValue. " WHERE"
                    . " FactKey = ".$factKey
                    . " AND FactKeyDimVal = ".$factKeyDimVal
                    . " AND QuestionID = ".$aQuestion->QuestionID
                    . " AND ConsecutiveID = ".$consecutiveID;

            $aRS = $aRepository->DataADOConnection->Execute($sql);
            if ($aRS === false) 
            {
            	if($gblShowErrorSurvey)
            	{
                	die("(".__METHOD__.") ".translate("Error accessing") . " SVSurveyMatrixData_" . $surveyInstance->ModelID . " " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
            	}
            	else 
            	{
            		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing") . " SVSurveyMatrixData_" . $surveyInstance->ModelID . " " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql;
            		return false;
            	}
            }
        }
    }
    
    return true;
}

function saveDataInCatDimValTable($aRepository, $surveyInstance, $arrayData, $factKey, $factKeyDimVal) 
{
    global $gblShowErrorSurvey;
		
	if(isset($arrayData["qCatDimValQID"]) && isset($arrayData["qCatDimVal"]) && isset($arrayData["qElemDimVal"])&& isset($arrayData["qElemDimValResp"]))
	{
		$qid = $arrayData["qCatDimValQID"];
		
		if($qid!=-1)
		{
			$qCatDimValue = $arrayData["qCatDimVal"];
			$qElemDimValue = $arrayData["qElemDimVal"];
			$qElemDimValResponse = $arrayData["qElemDimValResp"];
			
			//Por cada valor de respuesta que tiene el arrayvalues
			//debe guardarse un registro en la tabla de matrix
			$sql = "INSERT INTO SVSurveyCatDimVal_".$surveyInstance->ModelID."
					(
						FactKey, 
						FactKeyDimVal, 
						QuestionID, 
						CatDimVal, 
						ElemDimVal, 
						ResponseValue
					) VALUES ( "
						.$factKey.", "
						.$factKeyDimVal.", "
						.$qid.", "
						.$qCatDimValue.", "
						.$qElemDimValue.", "
						.$aRepository->DataADOConnection->Quote($qElemDimValResponse).
					")";
			
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyCatDimVal_".$surveyInstance->ModelID." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					return ("(".__METHOD__.") ".translate("Error accessing")." "." SVSurveyCatDimVal_".$surveyInstance->ModelID." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}

	return "OK";
}

function updateDataInCatDimValTable($aRepository, $surveyInstance, $arrayData, $factKey, $factKeyDimVal) 
{
    global $gblShowErrorSurvey;
		
	if(isset($arrayData["qCatDimValQID"]) && isset($arrayData["qCatDimVal"]) && isset($arrayData["qElemDimVal"])&& isset($arrayData["qElemDimValResp"]))
	{
		$qid = $arrayData["qCatDimValQID"];
		
		if($qid!=-1)
		{
			$qCatDimValue = $arrayData["qCatDimVal"];
			$qElemDimValue = $arrayData["qElemDimVal"];
			$qElemDimValResponse = $arrayData["qElemDimValResp"];
			
			//Por cada valor de respuesta que tiene el arrayvalues
			//debe guardarse un registro en la tabla de matrix
			$sql = "UPDATE SVSurveyCatDimVal_".$surveyInstance->ModelID." 
					SET ResponseValue = ".$aRepository->DataADOConnection->Quote($qElemDimValResponse)." 
					WHERE FactKey = ".$factKey." 
					AND FactKeyDimVal = ".$factKeyDimVal." 
					AND QuestionID = ".$qid." 
					AND CatDimVal = ".$qCatDimValue." 
					AND ElemDimVal = ".$qElemDimValue;
			
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyCatDimVal_".$surveyInstance->ModelID." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					return ("(".__METHOD__.") ".translate("Error accessing")." "." SVSurveyCatDimVal_".$surveyInstance->ModelID." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}

	return "OK";
}

function putQuotes($aRepository, $str) {
    return $aRepository->DataADOConnection->Quote($str);
}

function doTimeStamp($aRepository, $date) {
    return $aRepository->DataADOConnection->DBTimeStamp($date);
}

function insertStatistics($aRepository, $surveyID, $thisDate, $captureType, $countSurveys=1) {
    
    //obtenemos el email del usuario
    $sql = "SELECT CUENTA_CORREO FROM SI_USUARIO WHERE CLA_USUARIO = ".$_SESSION["PABITAM_UserID"];
    
    $aRS = $aRepository->ADOConnection->Execute($sql);
    if ($aRS === false)
    {
        die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
    }
    if (!$aRS->EOF)
    {
        $userEmail = $aRS->fields["CUENTA_CORREO"];
    }
    
    //Para conectarse a fbm000
    require('../../../fbm/conns.inc.php');
    
    $mysql_Connection = mysql_connect($server, $server_user, $server_pwd);

    if (!$mysql_Connection)
    {
            //return 'Error, connection error! '.mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
            return 'Connection error! ';
    }

    $db_selected = mysql_select_db($masterdbname, $mysql_Connection);

    if (!$db_selected)
    {
            //conchita 1-nov-2011 ocultar mensajes sql
            //return 'Error, could not select '.$masterdbname.'!'."\n".mysql_error();
            return 'Error, could not select '.$masterdbname;
    }
    
    //Obtenemos los valores del userID en fbm000 de la tabla de saas_users
    $sql = "SELECT UserID FROM SAAS_USERS WHERE Email = '".$userEmail."'";

    $result = mysql_query($sql, $mysql_Connection);

    if (!$result || mysql_num_rows($result) == 0)
    {
            //return 'Error, data not found: '.$aSQL."\n".mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
            return 'Error, email or password incorrect';
    }
    
    $row = mysql_fetch_assoc($result);
    $intUserID = $row["UserID"];  
    
    //sacamos el nombre del modelo
    $surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
    $modelName = $surveyInstance->SurveyName;
    
    //Vamos a setear cada uno de los valores para tabla de estadisticas
    
    //server
    $serverField = putQuotes($aRepository, "kpionline"); 
    
    //date_time
    $date_time = doTimeStamp($aRepository, $thisDate);   
    
    //account_email
    $account_email = putQuotes($aRepository, $userEmail); 
    
    //model
    $model = putQuotes($aRepository, $modelName);  
    
    //company
    $company = "null";  
    
    //id
    $id = $intUserID;      
    
    //account_database
    $account_database = putQuotes($aRepository, $_SESSION["BITAM_RepositoryName"]);  
    
    //ip
    $ip = putQuotes($aRepository, $_SERVER['REMOTE_ADDR']);    
    
    //records_deleted
    $records_deleted = "0";
    
    //records_updated
    $records_updated = "0";  
    
    //records_uploaded
    $records_uploaded = $countSurveys; 
    
    //source
    $source = putQuotes($aRepository, "EForms");      
    
    //load_type
    $load_type = putQuotes($aRepository, $captureType);   
    
    //saas_etl_version
    $saas_etl_version = putQuotes($aRepository,"");
    
    //erp
    $erp = putQuotes($aRepository,"");  
    
    //connector_version
    $connector_version = putQuotes($aRepository,"");
    
    //file_generation_time
    $file_generation_time = "0"; 
    
    //data_transmision_time
    $data_transmision_time = "0";
    
    //loading_etl_script
    $loading_etl_script = "0";  
    
    //total_time
    $total_time  = "0";  
    
    //upload_status
    $upload_status = putQuotes($aRepository,"success"); 
    
    //task_id
    $task_id = "0";    
    
    //task_schedule
    $task_schedule = "0";        
    
    //Insertamos el registro a la tabla de estadisticas
    $sql = "INSERT INTO saas_data_audit (server, date_time, account_email, model, company, id, account_database, ip, records_deleted, records_updated, records_uploaded, source, load_type, 
            saas_etl_version, erp, connector_version, file_generation_time, data_transmision_time, loading_etl_script, total_time, upload_status, task_id, task_schedule) VALUES (".
            $serverField.", ".          //server
            $date_time.", ".            //date_time
            $account_email.", ".        //account_email
            $model.", ".                //model
            $company.", ".              //company
            $id.", ".                   //id
            $account_database.", ".     //account_database
            $ip.", ".                   //ip
            $records_deleted.", ".      //records_deleted
            $records_updated.", ".      //records_updated
            $records_uploaded.", ".     //records_uploaded
            $source.", ".               //source
            $load_type.", ".            //load_type
            $saas_etl_version.", ".     //saas_etl_version
            $erp.", ".                  //erp
            $connector_version.", ".    //connector_version
            $file_generation_time.", ". //file_generation_time
            $data_transmision_time.", ".//data_transmision_time
            $loading_etl_script.", ".   //loading_etl_script
            $total_time.", ".           //total_time
            $upload_status.", ".        //upload_status
            $task_id.", ".              //task_id
            $task_schedule.             //task_schedule
            ");"
            ;

    $result = mysql_query($sql, $mysql_Connection);

    if (!$result || mysql_affected_rows() == 0)
    {
		global $queriesLogFile;
        $aLogString = 'Error executing query: '.$sql."\n".mysql_error();
        error_log($aLogString, 3, $queriesLogFile);
    }
}

//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
function getMaxFactKeyDimValue($aRepository, $surveyInstance, $latitude, $longitude, $accuracy = 0)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	$tableDimFactKey = "RIDIM_".$surveyInstance->FactKeyDimID;
	$fieldDimFactKeyKey = $tableDimFactKey."KEY";
	$fieldDimFactKeyDsc = "DSC_".$surveyInstance->FactKeyDimID;
	$fieldAttribLatitude = "DSC_".$surveyInstance->LatitudeAttribID;
	$fieldAttribLongitude = "DSC_".$surveyInstance->LongitudeAttribID;
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	$strAdditionalFields = '';
	$strAdditionalValues = '';
	if (getMDVersion() >= esvAccuracyValues && $surveyInstance->AccuracyAttribID > 0) {
		$fieldAttribAccuracy = "DSC_".$surveyInstance->AccuracyAttribID;
		$strAdditionalFields .= ', '.$fieldAttribAccuracy;
		$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote($accuracy);
	}
	//@JAPR
	
	$sql =  "SELECT ".$aRepository->DataADOConnection->IfNull("MAX(".$fieldDimFactKeyKey.")", "0")." + 1 AS ValueID FROM ".$tableDimFactKey;
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if (!$aRS || $aRS->EOF)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimFactKey." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableDimFactKey." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Obtenemos el valor maximo de FactKeyDim
	$factkeydimvalue = (int)$aRS->fields["valueid"];
	
	//Procedemos siempre insertar desde el valor 1, si llega un cero por el caso del No Aplica (-1)
	//entonces lo pasamos a 1
	if($factkeydimvalue==0)
	{
		$factkeydimvalue = 1;
	}

	//Despues insertamos dicho FactKey Value en la dimension FactKey de la encuesta
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	$sqlDimFactKey = "INSERT INTO ".$tableDimFactKey." (".$fieldDimFactKeyKey.", ".$fieldDimFactKeyDsc.", ".$fieldAttribLatitude.", ".$fieldAttribLongitude.$strAdditionalFields.") 
			VALUES (".$factkeydimvalue.", ".$aRepository->DataADOConnection->Quote($factkeydimvalue).", ".$aRepository->DataADOConnection->Quote($latitude).", ".$aRepository->DataADOConnection->Quote($longitude).$strAdditionalValues.")";
	if($aRepository->DataADOConnection->Execute($sqlDimFactKey) === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimFactKey." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimFactKey);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableDimFactKey." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimFactKey;
			return false;
		}
	}
	
	return $factkeydimvalue;
}

function sendDashboards($aRepository, $surveyInstance, $userID, $factKeyDimVal, $sendEmailDashboards=true)
{
	global $arrayPDF;
	
	//require_once("getArtusReporter.php");
	//En esta funcion vamos a enviar todos los pdfs de los escenarios asociados
	//a los schedulers que estan asociados al usuario logueado
	$surveySchedulerCollection = BITAMSurveySchedulerCollection::NewInstanceBySurveyAndUser($aRepository, $surveyInstance->SurveyID, $userID);
	
	//arreglo de rutas de PDF si es que existen
	$arrayPDF = array();
	$countSchedulers = count($surveySchedulerCollection->Collection);
	
	if($sendEmailDashboards==true)
	{
		foreach ($surveySchedulerCollection->Collection as $surveySchedulerInstance)
		{
			$res = getArtusReporter($surveySchedulerInstance->SchedulerID, $factKeyDimVal, $aRepository);
			
			if($res!==false)
			{
				$arrayPDF[] = $res;
			}
		}
		
		//Si hubo PDFs generados entonces si se envian los escenarios por correo 
		if(count($arrayPDF)>0)
		{
			//Verificamos si la variable esta activada para que se envien los dashboards
			//en caso de que enga en false eso quiere decir que probablemente
			//sea una encuesta de multiples dimensiones dinamicas y por ello se esta
			//enviando a llamar esta funcion varias veces por lo tanto se le envia
			//un centinela para indicarle cuando enviar el correo de los escenarios en PDF
			NotifySurveyDashboardPDFByMail($aRepository, $surveyInstance, $userID, $arrayPDF);
			
			return true;
		}
		else 
		{
			if($countSchedulers>0)
			{
				$logDate = date('Y-m-d');
				$logfile = "./log/monitoringSendDash_".$logDate.".log";
				$friendlyDate = date('Y-m-d H:i:s');
				$lastDate = substr($_POST["surveyDate"], 0, 10)." ".substr($_POST["surveyHour"], 0, 8);
				$straction = "[".$friendlyDate."] [Warning: No Sending Dashboards, getArtusReporter process returns false] [UserID: ".$_SESSION["PAtheUserID"]."] [UserName: ".$_SESSION["PABITAM_UserName"]."] [Repository: ".$aRepository->RepositoryName."] [SurveyID: ".$_POST["surveyID"]."] [SurveyDate: ".$lastDate."]\r\n";
				@error_log($straction, 3, $logfile);
			}
			else 
			{
				$logDate = date('Y-m-d');
				$logfile = "./log/monitoringSendDash_".$logDate.".log";
				$friendlyDate = date('Y-m-d H:i:s');
				$lastDate = substr($_POST["surveyDate"], 0, 10)." ".substr($_POST["surveyHour"], 0, 8);
				$straction = "[".$friendlyDate."] [Warning: No Sending Dashboards, no schedulers for this user] [UserID: ".$_SESSION["PAtheUserID"]."] [UserName: ".$_SESSION["PABITAM_UserName"]."] [Repository: ".$aRepository->RepositoryName."] [SurveyID: ".$_POST["surveyID"]."] [SurveyDate: ".$lastDate."]\r\n";
				@error_log($straction, 3, $logfile);
			}

			return false;
		}
	}
	else 
	{
		$logDate = date('Y-m-d');
		$logfile = "./log/monitoringSendDash_".$logDate.".log";
		$friendlyDate = date('Y-m-d H:i:s');
		$lastDate = substr($_POST["surveyDate"], 0, 10)." ".substr($_POST["surveyHour"], 0, 8);
		$straction = "[".$friendlyDate."] [Warning: No Sending Dashboards,  it is not the end of the survey] [UserID: ".$_SESSION["PAtheUserID"]."] [UserName: ".$_SESSION["PABITAM_UserName"]."] [Repository: ".$aRepository->RepositoryName."] [SurveyID: ".$_POST["surveyID"]."] [SurveyDate: ".$lastDate."]\r\n";
		@error_log($straction, 3, $logfile);

		return false;
	}
}

function NotifySurveyDashboardPDFByMail($aRepository, $surveyInstance, $userID, $arrayPDF)
{
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	global $arrAdditionalEMails;
	//@JAPR 2014-04-09: Agregada la variable para identificar el envío de reportes desde la captura o desde la opción para forzarlo en la lista de reportes
	global $blnSendPDFEntries;
	//@JAPR
	
	require_once("appuser.inc.php");

	$objResource = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $userID);
	
	$strCreatorStatusEmail = $objResource->Email;
	$strCreatorStatusUserName = $objResource->Email;
	
	require_once("bpaemailconfiguration.inc.php");
	
	$anInstanceEmailConf = BPAEmailConfiguration::readConfiguration();
	
	if(is_null($anInstanceEmailConf))
	{
		return false;
	}
  
	$SMTPServer = $anInstanceEmailConf->fbm_email_server;
	
	if (strlen($SMTPServer) == 0) 
	{
		return false;
	}

	$SMTPPort = $anInstanceEmailConf->fbm_email_port;
	
	if (strlen($SMTPPort) == 0) 
	{
		return false;
	}
	
	$SMTPUserName = $anInstanceEmailConf->fbm_email_username;
	
	if (strlen($SMTPUserName) == 0)
	{
		return false;
	}

	$SMTPPassword = $anInstanceEmailConf->fbm_email_pwd;
	
	//@JAPR 2012-10-10: Validada la integración con eBavel, como eBavel mandará eMails, incluye primero la clase PhPMailer así que se debe validar
	//que sólo incluya eForms su versión si no lo hizo ya eBavel
	if(!class_exists("PHPMailer")){
		require_once("mail/class.phpmailer.php");
	}	
	//@JAPR
	
	$mail = new PHPMailer();
	$mail->Mailer = "smtp";
	$mail->Host = $SMTPServer;
	$mail->Port = $SMTPPort;
	$mail->SMTPAuth = TRUE;
	//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
	//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
	$mail->SMTPAutoTLS = false;
	//$mail->SMTPDebug = false;
	//$mail->Debugoutput = 'echo';
	//$mail->SMTPSecure = '';
	//$mail->CharSet = 'UTF-8';
	//@JAPR
	$mail->Username = $SMTPUserName;
	$mail->Password = $SMTPPassword;
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br><br>\r\n\r\n NotifySurveyDashboardPDFByMail Host: {$SMTPServer}");
		echo("<br>\r\n Host: {$SMTPServer}");
		echo("<br>\r\n Port: {$SMTPPort}");
		echo("<br>\r\n Username: {$SMTPUserName}");
		echo("<br>\r\n Password: {$SMTPPassword}");
	}

	//Se modifico el envio de correos para que el usuario que se loguea al servidor de correos
	//sea el mismo que vaya en el FROM, pero en el momento en que se contesta el correo entonces
	//el Reply To va dirigido a la persona q realmente si creo el correo
	//$mail->From = $strCreatorStatusEmail;
	//$mail->FromName = $strCreatorStatusUserName;
	$mail->From = $anInstanceEmailConf->fbm_email_source;
	$mail->FromName = $strCreatorStatusUserName;
	$mail->AddReplyTo($strCreatorStatusEmail, $strCreatorStatusUserName);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n From: {$anInstanceEmailConf->fbm_email_source}");
		echo("<br>\r\n FromName: {$strCreatorStatusUserName}");
		echo("<br>\r\n AddReplyTo: {$strCreatorStatusEmail} {$strCreatorStatusUserName}");
	}

	$mail->IsHTML(TRUE);
	
	$maillanguage = $_SESSION["PAuserLanguage"];
	$mail->SetLanguage(strtolower($maillanguage),"mail/language/"); //set the language
	
	$mail->Subject = $surveyInstance->SurveyName." - "."Dashboards";
	$existRecipients = false;

	$tempEmail = $strCreatorStatusEmail;
	//$tempEmail = "iris.suarez@gmail.com";
	$tempUserName = $strCreatorStatusUserName;
	
	$blnCheckMail = BPAEmailConfiguration::check_email_address($tempEmail);
	if ($blnCheckMail)
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n Address: {$tempEmail} {$tempUserName}");
		}
		$mail->AddAddress($tempEmail, $tempUserName);
		$existRecipients = true;
	}
	
	//@JAPR 2012-10-09: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
	$tempEmailCC = @$objResource->AltEmail;
	if (trim($tempEmailCC) != '') {
		$blnCheckMail = BPAEmailConfiguration::check_email_address($tempEmailCC);
		if ($blnCheckMail)
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n CC: {$tempEmailCC}");
			}
			$mail->AddCC($tempEmailCC, "");
			$existRecipients = true;
		}
	}
	
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	//Agrega las cuentas de correo adicionales configuradas para la encuesta, en este punto se asume que ya están validadas por lo menos para que
	//se vean como un EMail, si están bien armadas o no son reales, ya se sale del alcance y se dejará al server de correo que lo resuelva
	if (isset($arrAdditionalEMails) && is_array($arrAdditionalEMails) && count($arrAdditionalEMails) > 0) {
		foreach ($arrAdditionalEMails as $strEMail) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n CC: {$strEMail}");
			}
			@$mail->AddCC($strEMail, "");
			$existRecipients = true;
		}
	}
	//@JAPR
	
	//Enviaremos copias ocultas a los correos que se encuentran en la variable global
	//$KPAMailTesting definida en el config con la finalidad de monitorear el envio de correos
	global $KPAMailTesting;
	
	foreach ($KPAMailTesting as $emailTest)
	{
		$blnCheckMail = BPAEmailConfiguration::check_email_address($emailTest);
		if ($blnCheckMail)
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n BCC: {$emailTest}");
			}
			$mail->AddBCC($emailTest, "");
			$existRecipients = true;
		}
	}
	
	$mail->Body = CreateSurveyDashboardPDFMailBody($aRepository, $surveyInstance, $userID);
	
	//Realizamos un recorrido por los archivos PDFs para adjuntarlos al correo
	foreach ($arrayPDF as $docPDF)
	{
		$mail->AddAttachment($docPDF, $docPDF);
	}
	
	if($existRecipients)
	{
		AddCuentaCorreoAltToMail($objResource, $mail, $appVersion);
		if (!@$mail->Send())
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n Email Failed: {$mail->ErrorInfo}");
			}
			//Enviamos al log el error generado
			global $SendMailLogFile;
			
			$aLogString = "\r\n\r\n".$surveyInstance->SurveyName." - "."Dashboards: ".date('Y-m-d H:i:s');
			$aLogString.="\r\n";
			$aLogString.="\r\n\r\n".$mail->ErrorInfo;
			$aLogString.="\r\n";

			//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log($aLogString, 3, GeteFormsLogPath().$SendMailLogFile);
			//@JAPR
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n Email Sent");
			}
		}
	}

	unset($mail);
	return true;
}

function CreateSurveyDashboardPDFMailBody($aRepository, $surveyInstance, $userID)
{
	$strMailTitle = translate("Dashboards of ".$surveyInstance->SurveyName);
	
	$strAdditionalNotes = "Review attachment....";
	
	$strtemp = "<html>"."\n"
	."<style type=text/css>"."\n"
	."  A:active { color: #00008b }"."\n"
	."  A:visited { color: #708090 }"."\n"
	."  A:link { color: #00008b }"."\n"
	."  A:hover { color: #4169e1 }"."\n"
	."  A { font-family: verdana; font-size: 8pt }"."\n"
	."</style>"."\n"
	."<body>"."\n"
	."<div style=\"font-family:verdana;color:#336699;font-size:13pt\"><b>".$strMailTitle."</b></div>"."\n"
	."<table border=0 cellPadding=5 cellSpacing=1 style=\"font-family:verdana;font-weight:normal;font-size:10pt\">"."\n"
	."  <tr>"."\n"
	."    <td colspan=2><hr noshade width=100% size=1></td>"."\n"
	."  </tr>"."\n"
	."  <tr>"."\n"
	."    <td>&nbsp;</td>"."\n"
	."  </tr>"."\n"	
	."  <tr>"."\n"
	."    <td>".$strAdditionalNotes."</td>"."\n"
	."  </tr>"."\n"
	
	."</table>"."\n"
	."<br>"."\n";

	$strtemp .= "</body>"."\n"
	."</html>"."\n";

	return $strtemp;
}

//@JAPR 2014-10-27: Corregido un bug, el indicador duración sólo se debe grabar en el registro de la sección estándar
//Agregados los parámetros $anEntrySectionID = 0, $anEntryCatDimID = 0
//@JAPR 2014-11-20: Agregada la dimensión Agenda
//Agregado el parámetro $anAgendaKey para especificar si se grabará o no la dimensión agenda correspondiente a la captura
//Un valor de null provoca que no se grabe, así que las capturas que no son de agenda deben enviar el key del valor de NA
function saveDataInGlobalSurveyModel($aRepository, $surveyInstance, $arrayData, $thisDate, $factKey, $factKeyDimVal, $anEntrySectionID = 0, $anEntryCatDimID = 0, $anAgendaKey = null)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	global $syncDate;
	global $syncTime;
	/*@MABH20121205*/
	global $appVersion;
	//@MABH20121205
	
	if (is_null($syncDate) || trim((string) $syncDate) == '') {
		$syncDate = date("Y-m-d");
	}
	if (is_null($syncTime) || trim((string) $syncTime) == '') {
		$syncTime = date("H:i:s");
	}
	//@JAPR
	
	//Obtenemos los valores para las dimensiones de Start Date, End Date, Start Hour, End Hour
	$startDate = substr($arrayData["surveyDate"], 0, 10);
	$endDate = substr($thisDate, 0, 10);
	$startTime = $arrayData["surveyHour"];
	$endTime = substr($thisDate, 11);
	$userID = $arrayData["userID"];
	
	/*@MABH20121205*/
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->GblServerStartDateDimID > 0 && $surveyInstance->GblServerEndDateDimID > 0 && $surveyInstance->GblServerStartTimeDimID > 0 && $surveyInstance->GblServerEndTimeDimID > 0) {
		$serverStartDate = substr((string) @$arrayData["serverSurveyDate"], 0, 10);
		$serverEndDate = substr((string) @$arrayData["serverSurveyEndDate"], 0, 10);
		$serverStartTime = (string) @$arrayData["serverSurveyHour"];
		$serverEndTime = (string) @$arrayData["serverSurveyEndHour"];
	}
	//@MABH20121205
	
	//Obtenemos el answeredquestions
	$answeredQuestions = getSurveyAnsweredQuestions($arrayData, $aRepository, $surveyInstance->SurveyID);
	
	//Obtenemos los valores para los indicadores Duration, Latitude y Longitude
	$currentDate = $arrayData["surveyDate"];
	$initialTime = strtotime((substr($currentDate, 0, 10)." ".$startTime));
	$finalTime = strtotime((substr($thisDate, 0, 10)." ".$endTime));
	$totalSeconds = $finalTime - $initialTime;
	$totalMinutes = $totalSeconds/60;
	//@JAPR 2014-10-27: Corregido un bug, el indicador duración sólo se debe grabar en el registro de la sección estándar
	if ($anEntrySectionID != 0) {
		$totalMinutes = "NULL";
	}
	//@JAPR
	
	$latitude = 0;
	if(isset($arrayData['surveyLatitude'])) 
	{
		$latitude = (double)$arrayData['surveyLatitude'];
	}

	$longitude = 0;
	if(isset($arrayData['surveyLongitude'])) 
	{
		$longitude = (double)$arrayData['surveyLongitude'];
	}
	
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	$accuracy = 0;
	if(isset($arrayData['surveyAccuracy'])) 
	{
		$accuracy = (double)$arrayData['surveyAccuracy'];
	}
	//@JAPR
	
	$strOriginalWD = getcwd();
	
	//Obtener el DateKey
	require_once("cubeClasses.php");
	require_once("dimension.inc.php");
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	require_once("../model_manager/indicatorkpi.inc.php");

	$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->GblSurveyModelID);
	chdir($strOriginalWD);

	$descriptKeysCollection = BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository, $anInstanceModel->ModelID);
	$dateKey = getDateKey($aRepository, $anInstanceModel, $descriptKeysCollection, $currentDate);
	
	if($dateKey===false)
	{
		return($gblEFormsErrorMessage);
	}
	
	//Obtenemos el SurveyKey
	$aSurveyInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyDimID);
	$surveyKey = getSurveyDimKey($aRepository, $aSurveyInstanceDim, $surveyInstance);
	
	if($surveyKey===false)
	{
		return($gblEFormsErrorMessage);
	}
        
    //Obtenemos las dims SurveyGlobalDimID y SurveySingleRecordDimID
    $aSurveyGlobalInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyGlobalDimID);
	$surveyGlobalKey = getSurveyGlobalDimKey($aRepository, $aSurveyGlobalInstanceDim, $surveyInstance, $surveyKey, $factKeyDimVal, $latitude, $longitude, $accuracy);
	
	if($surveyGlobalKey===false)
	{
		return($gblEFormsErrorMessage);
	}

    $aSurveySingleRecordInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveySingleRecordDimID);
	$surveySingleRecordKey = getSurveySingleRecordDimKey($aRepository, $aSurveySingleRecordInstanceDim, $surveyInstance, $surveyKey, $factKeyDimVal, $factKey);
	
	if($surveySingleRecordKey===false)
	{
		return($gblEFormsErrorMessage);
	}

	//Obtenemos el StartDateKey
	$aStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStartDateDimID);
	$startDateKey = getDateTimeDimKey($aRepository, $aStartDateInstanceDim, $startDate);
	
	if($startDateKey===false)
	{
		return($gblEFormsErrorMessage);
	}
	
	//Obtenemos el EndDateKey
	$anEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblEndDateDimID);
	$endDateKey = getDateTimeDimKey($aRepository, $anEndDateInstanceDim, $endDate);
	
	if($endDateKey===false)
	{
		return($gblEFormsErrorMessage);
	}

	//Obtenemos el StartTimeKey
	$aStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStartTimeDimID);
	$startTimeKey = getDateTimeDimKey($aRepository, $aStartTimeInstanceDim, $startTime);
	
	if($startTimeKey===false)
	{
		return($gblEFormsErrorMessage);
	}

	//Obtenemos el EndTimeKey
	$anEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblEndTimeDimID);
	$endTimeKey = getDateTimeDimKey($aRepository, $anEndTimeInstanceDim, $endTime);
	
	if($endTimeKey===false)
	{
		return($gblEFormsErrorMessage);
	}
	
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	//Obtenemos el SyncDateKey
	$syncDateKey = '';
	$syncDateDimField = '';
	if ($surveyInstance->SyncDateDimID > 0) {
		$aSyncDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSyncDateDimID);
		$syncDateKey = ', '.getDateTimeDimKey($aRepository, $aSyncDateInstanceDim, $syncDate);
		$syncDateDimField = ', '.$aSyncDateInstanceDim->Dimension->TableName."KEY";
	}
	
	//Obtenemos el SyncTimeKey
	$syncTimeKey = '';
	$syncTimeDimField = '';
	if ($surveyInstance->SyncTimeDimID > 0) {
		$aSyncTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSyncTimeDimID);
		$syncTimeKey = ', '.getDateTimeDimKey($aRepository, $aSyncTimeInstanceDim, $syncTime);
		$syncTimeDimField = ', '.$aSyncTimeInstanceDim->Dimension->TableName."KEY";
	}
	
	//@JAPR 2014-11-20: Agregada la dimensión Agenda
	$agendaKey = '';
	$fieldDimAgendaKey = '';
	if (getMDVersion() >= esvAgendaDimID && $surveyInstance->AgendaDimID > 0) {
		//Agregado el parámetro $anAgendaKey para especificar si se grabará o no la dimensión agenda correspondiente a la captura
		//Un valor de null provoca que no se grabe, así que las capturas que no son de agenda deben enviar el key del valor de NA
		if (!is_null($anAgendaKey)) {
			$anAgendaInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->AgendaDimID);
			if (!is_null($anAgendaInstanceDim)) {
				$agendaKey = ', '.$anAgendaKey;
				$fieldDimAgendaKey = ', '.$anAgendaInstanceDim->Dimension->TableName."KEY";
			}
		}
	}
	//@JAPR
	
	//@JAPRWarning: ¿ Por qué no es necesario hacer esto siempre ? no lo puse porque este código ha estado
	//funcionando sin esta llamada, pero en teoría luego de accesar al Model Manager se debería invocar este chdir
	//chdir($strOriginalWD);
	
	/*@MABH20121205*/
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->GblServerStartDateDimID > 0 && $surveyInstance->GblServerEndDateDimID > 0 && $surveyInstance->GblServerStartTimeDimID > 0 && $surveyInstance->GblServerEndTimeDimID > 0) {
		//Obtenemos el StartDateKey
		$aServerStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerStartDateDimID);
		$serverStartDateKey = getDateTimeDimKey($aRepository, $aServerStartDateInstanceDim, $serverStartDate);
		
		if($serverStartDateKey===false)
		{
			return($gblEFormsErrorMessage);
		}
		
		//Obtenemos el EndDateKey
		$aServerEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerEndDateDimID);
		$serverEndDateKey = getDateTimeDimKey($aRepository, $aServerEndDateInstanceDim, $serverEndDate);
		
		if($serverEndDateKey===false)
		{
			return($gblEFormsErrorMessage);
		}
	
		//Obtenemos el StartTimeKey
		$aServerStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerStartTimeDimID);
		$serverStartTimeKey = getDateTimeDimKey($aRepository, $aServerStartTimeInstanceDim, $serverStartTime);
		
		if($serverStartTimeKey===false)
		{
			return($gblEFormsErrorMessage);
		}
	
		//Obtenemos el EndTimeKey
		$aServerEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerEndTimeDimID);
		$serverEndTimeKey = getDateTimeDimKey($aRepository, $aServerEndTimeInstanceDim, $serverEndTime);
		
		if($serverEndTimeKey===false)
		{
			return($gblEFormsErrorMessage);
		}
		
		$serverStartDateDimField = $aServerStartDateInstanceDim->Dimension->TableName."KEY";
		$serverEndDateDimField = $aServerEndDateInstanceDim->Dimension->TableName."KEY";
		$serverStartTimeDimField = $aServerStartTimeInstanceDim->Dimension->TableName."KEY";
		$serverEndTimeDimField = $aServerEndTimeInstanceDim->Dimension->TableName."KEY";
	}
	//@MABH20121205
	
	//Obtenemos la instancia de la dimension User
	$userDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblUserDimID);
	
	//Obtenemos las instancias de los indicadores Duration, Latitud y Longitud
	$answeredQuestionsIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblAnsweredQuestionsIndID);
	$durationIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblDurationIndID);
	$latitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblLatitudeIndID);
	$longitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblLongitudeIndID);
	
	$answeredQuestionsIndField = $answeredQuestionsIndicator->field_name.$answeredQuestionsIndicator->IndicatorID;
	$durationIndField = $durationIndicator->field_name.$durationIndicator->IndicatorID;
	$latitudeIndField = $latitudeIndicator->field_name.$latitudeIndicator->IndicatorID;
	$longitudeIndField = $longitudeIndicator->field_name.$longitudeIndicator->IndicatorID;

	//Generamos el INSERT a la tabla de hechos de encuestas globales
	$factTable = $anInstanceModel->nom_tabla;
	$dimPeriodo = $anInstanceModel->dim_periodo;
	$dateKeyField = getJoinField($dimPeriodo, "", $descriptKeysCollection, true);
	$surveyDimField = $aSurveyInstanceDim->Dimension->TableName."KEY";
	//campos surveyglobalid y surveysinglerecord
    $surveyGlobalDimField = $aSurveyGlobalInstanceDim->Dimension->TableName."KEY";
    $surveySingleRecordDimField = $aSurveySingleRecordInstanceDim->Dimension->TableName."KEY";
	$userDimField = $userDimension->Dimension->TableName."KEY";
	$startDateDimField = $aStartDateInstanceDim->Dimension->TableName."KEY";
	$endDateDimField = $anEndDateInstanceDim->Dimension->TableName."KEY";
	$startTimeDimField = $aStartTimeInstanceDim->Dimension->TableName."KEY";
	$endTimeDimField = $anEndTimeInstanceDim->Dimension->TableName."KEY";
	
	/*@MABH20121205*/
	$strAdditionalFields = "";
	$strAdditionalValues = "";
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->GblServerStartDateDimID > 0 && $surveyInstance->GblServerEndDateDimID > 0 && $surveyInstance->GblServerStartTimeDimID > 0 && $surveyInstance->GblServerEndTimeDimID > 0) {
		$strAdditionalFields .= ", ".$serverStartDateDimField.", ".$serverEndDateDimField.", ".$serverStartTimeDimField.", ".$serverEndTimeDimField;
		$strAdditionalValues .= ", ".$serverStartDateKey.", ".$serverEndDateKey.", ".$serverStartTimeKey.", ".$serverEndTimeKey;
	}
	//@MABH20121205
	
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	if (getMDVersion() >= esvAccuracyValues) {
		/*
		//Dimensión de Latitude
		if ($surveyInstance->GblLatitudeDimID > 0) {
			$aLatitudeInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblLatitudeDimID);
			if (!is_null($aLatitudeInstanceDim)) {
				$latitudeKey = ', '.getDateTimeDimKey($aRepository, $aLatitudeInstanceDim, $latitude);
				$latitudeDimField = ', '.$aLatitudeInstanceDim->Dimension->TableName."KEY";
				$strAdditionalFields .= ", ".$latitudeDimField;
				$strAdditionalValues .= ", ".$latitudeKey;
			}
		}
		
		//Dimensión de Longitude
		if ($surveyInstance->GblLongitudeDimID > 0) {
			$aLongitudeInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblLongitudeDimID);
			if (!is_null($aLongitudeInstanceDim)) {
				$longitudeKey = ', '.getDateTimeDimKey($aRepository, $aLongitudeInstanceDim, $longitude);
				$longitudeDimField = ', '.$aLongitudeInstanceDim->Dimension->TableName."KEY";
				$strAdditionalFields .= ", ".$longitudeDimField;
				$strAdditionalValues .= ", ".$longitudeKey;
			}
		}
		
		//Dimensión de Accuracy
		if ($surveyInstance->GblAccuracyDimID > 0) {
			$anAccuracyInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblAccuracyDimID);
			if (!is_null($anAccuracyInstanceDim)) {
				$accuracyKey = ', '.getDateTimeDimKey($aRepository, $anAccuracyInstanceDim, $accuracy);
				$accuracyDimField = ', '.$anAccuracyInstanceDim->Dimension->TableName."KEY";
				$strAdditionalFields .= ", ".$accuracyDimField;
				$strAdditionalValues .= ", ".$accuracyKey;
			}
		}
		*/
		
		//Indicador de Accuracy
		if ($surveyInstance->GblAccuracyIndID > 0) {
			$accuracyIndicator = @BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblAccuracyIndID);
			if (!is_null($accuracyIndicator)) {
				$accuracyIndField = $accuracyIndicator->field_name.$accuracyIndicator->IndicatorID;
				$strAdditionalFields .= ", ".$accuracyIndField;
				$strAdditionalValues .= ", ".$accuracy;
			}
		}
	}
	
	//@JAPR 2014-11-20: Agregada la dimensión Agenda
	if (getMDVersion() >= esvAgendaDimID) {
		$strAdditionalFields .= $fieldDimAgendaKey;
		$strAdditionalValues .= $agendaKey;
	}
	//@JAPR

	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	//Se insertarán casi al inicio los nuevos campos, lo cuales ya deben venir con las "," necesarias para funcionar o vacios para no dar problemas
	$sql = "INSERT INTO ".$factTable." ( ".
		$dateKeyField.
		$syncDateDimField.
		$syncTimeDimField.
		", ".$surveyDimField.
		", ".$surveyGlobalDimField.      
	    ", ".$surveySingleRecordDimField.
		", ".$userDimField.
		", ".$startDateDimField.
		", ".$endDateDimField.
		", ".$startTimeDimField.
		", ".$endTimeDimField.
		", ".$answeredQuestionsIndField.
		", ".$durationIndField.
		", ".$latitudeIndField.
		", ".$longitudeIndField.
		$strAdditionalFields.
	" ) VALUES ( ".
		$dateKey.
		$syncDateKey.
		$syncTimeKey.
		",".$surveyKey.
		",".$surveyGlobalKey.        
	    ",".$surveySingleRecordKey.
		",".$userID.
		",".$startDateKey.
		",".$endDateKey.
		",".$startTimeKey.
		",".$endTimeKey.
		",".$answeredQuestions.
		",".$totalMinutes.
		",".$latitude.
		",".$longitude.
		$strAdditionalValues.
	" )";
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			return("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}

	return "OK";
}

function updateDataInGlobalSurveyModel($aRepository, $surveyInstance, $arrayData, $thisDate, $factKey, $factKeyDimVal)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	
	//Obtenemos los valores para las dimensiones de Start Date, End Date, Start Hour, End Hour
	$startDate = substr($arrayData["surveyDate"], 0, 10);
	$endDate = substr($thisDate, 0, 10);
	$startTime = $arrayData["surveyHour"];
	$endTime = substr($thisDate, 11);
	$userID = $arrayData["userID"];
	
	//Obtenemos el answeredquestions
	$answeredQuestions = getSurveyAnsweredQuestions($arrayData, $aRepository, $surveyInstance->SurveyID);
	
	//Obtenemos los valores para los indicadores Duration, Latitude y Longitude
	$currentDate = $arrayData["surveyDate"];
	$initialTime = strtotime((substr($currentDate, 0, 10)." ".$startTime));
	$finalTime = strtotime((substr($currentDate, 0, 10)." ".$endTime));
	$totalSeconds = $finalTime - $initialTime;
	//@JAPR 2014-10-27: Corregido un bug, el indicador duración sólo se debe grabar en el registro de la sección estándar
	//Durante la edición NO se está sobreescribiendo el valor del indicador (verificar si el agregado incremental lo está dejando bien asignado o no)
	$totalMinutes = $totalSeconds/60;

	$latitude = 0;
	if(isset($arrayData['surveyLatitude'])) 
	{
		$latitude = (double)$arrayData['surveyLatitude'];
	}

	$longitude = 0;
	if(isset($arrayData['surveyLongitude'])) 
	{
		$longitude = (double)$arrayData['surveyLongitude'];
	}

	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	$accuracy = 0;
	if(isset($arrayData['surveyAccuracy'])) 
	{
		$accuracy = (double)$arrayData['surveyAccuracy'];
	}
	//@JAPR
	
	$strOriginalWD = getcwd();
	
	//Obtener el DateKey
	require_once("cubeClasses.php");
	require_once("dimension.inc.php");
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	require_once("../model_manager/indicatorkpi.inc.php");

	$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->GblSurveyModelID);
	chdir($strOriginalWD);
	
	//Obtenemos el SurveyKey
	$aSurveyInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyDimID);
	$surveyKey = getSurveyDimKey($aRepository, $aSurveyInstanceDim, $surveyInstance);
	
	if($surveyKey===false)
	{
		return($gblEFormsErrorMessage);
	}
        
    //Obtenemos las dims SurveyGlobalDimID y SurveySingleRecordDimID
	$aSurveyGlobalInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyGlobalDimID);
	$surveyGlobalKey = getSurveyGlobalDimKey($aRepository, $aSurveyGlobalInstanceDim, $surveyInstance, $surveyKey, $factKeyDimVal, $latitude, $longitude, $accuracy);
	
	if($surveyGlobalKey===false)
	{
		return($gblEFormsErrorMessage);
	}

	$aSurveySingleRecordInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveySingleRecordDimID);
	$surveySingleRecordKey = getSurveySingleRecordDimKey($aRepository, $aSurveySingleRecordInstanceDim, $surveyInstance, $surveyKey, $factKeyDimVal, $factKey);
	
	if($surveySingleRecordKey===false)
	{
		return($gblEFormsErrorMessage);
	}
	
	//Obtenemos las instancias de los indicadores Duration, Latitud y Longitud
	$answeredQuestionsIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblAnsweredQuestionsIndID);
	
	$answeredQuestionsIndField = $answeredQuestionsIndicator->field_name.$answeredQuestionsIndicator->IndicatorID;

	//Generamos el INSERT a la tabla de hechos de encuestas globales
	$factTable = $anInstanceModel->nom_tabla;
        
	//campos surveyglobalid y surveysinglerecord
	$surveyGlobalDimField = $aSurveyGlobalInstanceDim->Dimension->TableName."KEY";
	$surveySingleRecordDimField = $aSurveySingleRecordInstanceDim->Dimension->TableName."KEY";
        
    $sql = "UPDATE ".$factTable." SET ".
            $answeredQuestionsIndField." = ".$answeredQuestions." ".
            " WHERE ".$surveySingleRecordDimField." = ".$surveySingleRecordKey;
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			return("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}

	return "OK";
}

function getSurveyAnsweredQuestions($arrayData, $aRepository, $surveyID) {
	//conteo de preguntas contestadas
	$answeredQuestionsCount = 0;
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	//@JAPRWarning: Falta considerar las preguntas de tipo Foto y firma, esas no contienen un qField pero se debe validar la foto que pudieran haber grabado
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		$postKey = "qfield".$aQuestion->QuestionID;
		if(isset($arrayData[$postKey]))
		{
			if($arrayData[$postKey] != '') {
				$answeredQuestionsCount++;
			}
		}
	}
	return $answeredQuestionsCount;
}

function getSurveyDimKey($aRepository, $aSurveyInstanceDim, $surveyInstance)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	$tableName = $aSurveyInstanceDim->Dimension->TableName;
	$fieldSurrogateKey = $tableName."KEY";
	$fieldKey = $aSurveyInstanceDim->Dimension->FieldKey;
	$fieldDesc = $aSurveyInstanceDim->Dimension->FieldDescription;
	
	$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldKey." = ".$surveyInstance->SurveyID;
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Considerando que es un solo valor el que se esta procesando
	if (!$aRS->EOF)
	{	
		//Ya existe ese valor en la tabla de la dimension de Surveys, se retorna su clave
		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	else 
	{
		//Se debe insertar dicho valor en la tabla de dimension de Surveys
		$sql = "INSERT INTO ".$tableName." ( ".$fieldKey.", ".$fieldDesc." ) VALUES ( ".$surveyInstance->SurveyID.", ".$aRepository->DataADOConnection->Quote($surveyInstance->SurveyName).")";
		
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldKey." = ".$surveyInstance->SurveyID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}

		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	
	return $valueKey;
}

//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
//Agregados los parámetros $latitude, $longitude y $accuracy para grabar estos datos cuando se inserte por primera vez en el cubo global de encuestas,
//tal como lo hace la función getMaxFactKeyDimValue. Si no se proporcional (NULL) o si la encuesta no contiene los atributos correspondientes, y
//si no es por lo menos la versión donde se supone que deben existir dichos atributos, entonces omite ese paso simplemente
function getSurveyGlobalDimKey($aRepository, $aSurveyGlobalInstanceDim, $surveyInstance, $surveyKey, $factKeyDimVal, $latitude = null, $longitude = null, $accuracy = null)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	$tableName = $aSurveyGlobalInstanceDim->Dimension->TableName;
	$fieldSurrogateKey = $tableName."KEY";
	//$fieldKey = $aSurveyGlobalInstanceDim->Dimension->FieldKey;
	$fieldDesc = $aSurveyGlobalInstanceDim->Dimension->FieldDescription;
    $theSurveyGlobalValue = $aRepository->DataADOConnection->Quote($surveyKey."-".$factKeyDimVal);
	
	//$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldKey." = ".$surveyInstance->SurveyID;
    $sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldDesc." = ".$theSurveyGlobalValue;
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Considerando que es un solo valor el que se esta procesando
	if (!$aRS->EOF)
	{	
		//Ya existe ese valor en la tabla de la dimension de Surveys, se retorna su clave
		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	else 
	{
		//Se debe insertar dicho valor en la tabla de dimension de Surveys
		$strAdditionalFields = "";
		$strAdditionalValues = "";
		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues) {
			$fieldAttribLatitude = "DSC_".$surveyInstance->GblLatitudeDimID;
			$fieldAttribLongitude = "DSC_".$surveyInstance->GblLongitudeDimID;
			$fieldAttribAccuracy = "DSC_".$surveyInstance->GblAccuracyDimID;
			if ($surveyInstance->GblLatitudeDimID > 0 && !is_null($latitude)) {
				$strAdditionalFields .= ", {$fieldAttribLatitude}";
				$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($latitude);
			}
			if ($surveyInstance->GblLongitudeDimID > 0 && !is_null($longitude)) {
				$strAdditionalFields .= ", {$fieldAttribLongitude}";
				$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($longitude);
			}
			if ($surveyInstance->GblAccuracyDimID > 0 && !is_null($accuracy)) {
				$strAdditionalFields .= ", {$fieldAttribAccuracy}";
				$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($accuracy);
			}
		}
		
		//$sql = "INSERT INTO ".$tableName." ( ".$fieldKey.", ".$fieldDesc." ) VALUES ( ".$surveyInstance->SurveyID.", ".$aRepository->DataADOConnection->Quote($surveyInstance->SurveyName).")";
        $sql = "INSERT INTO ".$tableName." ( ".$fieldDesc.$strAdditionalFields." ) VALUES ( ".$theSurveyGlobalValue.$strAdditionalValues.")";
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldDesc." = ".$theSurveyGlobalValue;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	
	return $valueKey;
}

function getSurveySingleRecordDimKey($aRepository, $aSurveySingleRecordInstanceDim, $surveyInstance, $surveyKey, $factKeyDimVal, $factKey)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	$tableName = $aSurveySingleRecordInstanceDim->Dimension->TableName;
	$fieldSurrogateKey = $tableName."KEY";
	//$fieldKey = $aSurveySingleRecordInstanceDim->Dimension->FieldKey;
	$fieldDesc = $aSurveySingleRecordInstanceDim->Dimension->FieldDescription;
        
        $theSurveySingleRecordValue = $aRepository->DataADOConnection->Quote($surveyKey."-".$factKeyDimVal."-".$factKey);
	
	$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldDesc." = ".$theSurveySingleRecordValue;
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Considerando que es un solo valor el que se esta procesando
	if (!$aRS->EOF)
	{	
		//Ya existe ese valor en la tabla de la dimension de Surveys, se retorna su clave
		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	else 
	{
		//Se debe insertar dicho valor en la tabla de dimension de Surveys
		$sql = "INSERT INTO ".$tableName." ( ".$fieldDesc." ) VALUES ( ".$theSurveySingleRecordValue.")";
		
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldDesc." = ".$theSurveySingleRecordValue;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}

		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	
	return $valueKey;
}

//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
//Esta función se usaba originalmente sólo para fechas, pero analizando el código, no hay diferencia entre esto y una consulta idéntica para
//dimensiones tipo String ya que al final las dimensiones de Fecha se graban como String y no como campos Date. En caso de que eventualmente se
//quisieran validar las fechas como tal, se puede parametrizar o bien generar una función independiente para dimensiones String, pero habría que
//buscar todas las llamadas a este método para cambiar por el correcto cuando se utilizó para dimensiones String
function getDateTimeDimKey($aRepository, $anInstanceDim, $value)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	$tableName = $anInstanceDim->Dimension->TableName;
	$fieldSurrogateKey = $tableName."KEY";
	$fieldDesc = $anInstanceDim->Dimension->FieldDescription;
	
	$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($value);
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Considerando que es un solo valor el que se esta procesando
	if (!$aRS->EOF)
	{	
		//Ya existe ese valor en la tabla de la dimension de Surveys, se retorna su clave
		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	else 
	{
		//Se debe insertar dicho valor en la tabla de dimension de Surveys
		$sql = "INSERT INTO ".$tableName." ( ".$fieldDesc." ) VALUES ( ".$aRepository->DataADOConnection->Quote($value).")";
		
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($value);
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}

		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	
	return $valueKey;
}

//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
//Agregado el parámetro &$arrFieldValues para regresar las descripciones de los elementos que fueron recibidos, ya que este método obtiene el 
//campo Key que es lo que finalmente graba
//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
//@JAPR 2013-01-22: Agregado el parámetro $bSaveSignature para optimizar en que momento se graba la firma
//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
//Agregado el parámetro $aDynamicRecNum para indicar el número de registro de la sección dinámica del que se trata. Originalmente fué usado solo
//para secciones dinámicas, pero se pudiera aplicar en conjunto con $anEntrySectionID para usarlo también en secciones Maestro-detalle. Si se
//envía un -1 quiere decir que no es un registro de una sección múltiple (es el índice empezando en 0 tal como lo maneja el App de v4)
function saveDataInSurveyTable($aRepository, $surveyInstance, $arrayData, $thisDate, $factKey, $factKeyDimVal, $folioID=null, &$arrFieldValues = null, $anEntrySectionID = 0, $anEntryCatDimID = 0, $aDynamicPageDSC = '', $bSaveSignature = true, $aDynamicRecNum = -1)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	global $syncDate;
	global $syncTime;
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	global $useSourceFields;
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	global $useRecVersionField;
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	global $useDynamicPageField;
	//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
	global $usePhotoPaths;
	//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
	global $blnWebMode;
	//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	global $dynamicSectionWithMultiChoice;
	global $arrDynamicCatalogAttributes;
	//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	global $arrSectionDescByID;
	global $arrSectionDescStrByID;
	//@JAPR
	/*@MABH20121205*/
	global $appVersion;
	global $gblEFormsNA;
	//@MABH20121205
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br><br>\r\n\r\nsaveDataInSurveyTable factKey: $factKey");
	}
	
	if (is_null($syncDate) || trim((string) $syncDate) == '') {
		$syncDate = date("Y-m-d");
	}
	if (is_null($syncTime) || trim((string) $syncTime) == '') {
		$syncTime = date("H:i:s");
	}
	
	//Verifica si esta encuesta tiene o no las nuevas dimensiones para determinar si debe o no intentar grabar en los nuevos campos
	$syncDateField = '';
	$syncDateValue = '';
	if ($surveyInstance->SyncDateDimID > 0) {
		$syncDateField = 'SyncDate, ';
		$syncDateValue = $aRepository->DataADOConnection->Quote($syncDate).', ';
	}
	$syncTimeField = '';
	$syncTimeValue = '';
	if ($surveyInstance->SyncTimeDimID > 0) {
		$syncTimeField = 'SyncTime, ';
		$syncTimeValue = $aRepository->DataADOConnection->Quote($syncTime).', ';
	}
	//@JAPR

	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyInstance->SurveyID);

	$currentDate = $arrayData["surveyDate"];
	$surveyHour = $arrayData["surveyHour"];
	$startTime = $arrayData["surveyHour"];
	$endTime = substr($thisDate, 11);
	/*@MABH20121205*/
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$serverSurveyDate = (string) @$arrayData["serverSurveyDate"];
		$serverSurveyHour = (string) @$arrayData["serverSurveyHour"];
		$serverSurveyEndDate = (string) @$arrayData["serverSurveyEndDate"];
		$serverSurveyEndHour = (string) @$arrayData["serverSurveyEndHour"];
	}
	//@MABH20121205
	
	$appUserID = $arrayData["appUserID"];
	
	//Obtenemos los valores de CaptureVia, CaptureEmail y SchedulerID
	//para verificar si dichos valores los estan dando de alta un correo electronico
	$captureVia = 0;
	if(isset($arrayData["CaptureVia"]))
	{
		$captureVia = (int)$arrayData["CaptureVia"];
	}
	
	$captureEmail = "";
	if(isset($arrayData["CaptureEmail"]))
	{
		$captureEmail = $arrayData["CaptureEmail"];
	}
	
	$schedulerID = "";
	if(isset($arrayData["SchedulerID"]))
	{
		$schedulerID = (int)$arrayData["SchedulerID"];
	}
	
	if($captureVia==1 && trim($captureEmail)!="" && $schedulerID>0)
	{
		$userID = -1;
	}
	else 
	{
		$userID = $arrayData["userID"];
	}
	
	//Determinamos si la encuesta tiene seccion dinamica
	//la primera prueba es verificar si en el arreglo $arrayData contiene la llave "sectionValue"
	$hasDynamicSection = false;
	$dynamicSectionID = 0;
	$dynamicSectionCatID = 0;
	$dynamicSectionCatMemberID = 0;
	$dynamicSectionChildCatMemberID = 0;
	//@JAPR 2012-11-26: Corregido un bug, cuando no se grababa la sección dinámica pero esta de todas formas contenía una pregunta multiple-choice
	//tipo checkbox, el campo físicamente es un double, así que al no recibir un valor estaba intentando insertar un '' en lugar de un 0 y eso
	//generaba un error, así que se validará por la pregunta de sección dinámica incluso si no se recibió el sectionValue
	$dynamicSectionIDEmpty = 0;
	if(isset($arrayData['sectionValue']))
	{
		//Si entro a este if quiere decir que al menos si trae valor para las secciones dinamicas
		//ahora se corrobora realmente q si exista
		$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $surveyInstance->SurveyID);
		
		if($dynamicSectionID>0)
		{
			$hasDynamicSection = true;
			$dynamicSection = BITAMSection::NewInstanceWithID($aRepository, $dynamicSectionID);
			$dynamicSectionCatID = $dynamicSection->CatalogID;
			$dynamicSectionCatMemberID = $dynamicSection->CatMemberID;
			$dynamicSectionChildCatMemberID = $dynamicSection->ChildCatMemberID;
		}
	}
	else {
		$dynamicSectionIDEmpty = BITAMSection::existDynamicSection($aRepository, $surveyInstance->SurveyID);
		if ($dynamicSectionIDEmpty > 0) {
			$dynamicSection = BITAMSection::NewInstanceWithID($aRepository, $dynamicSectionIDEmpty);
			if (!is_null($dynamicSection)) {
				$dynamicSectionCatID = $dynamicSection->CatalogID;
				$dynamicSectionCatMemberID = $dynamicSection->CatMemberID;
				$dynamicSectionChildCatMemberID = $dynamicSection->ChildCatMemberID;
			}
		}
	}
	
	//@JAPR 2014-05-29: Agregado el tipo de sección Inline
	//Verifica si existen secciones Inline y si este registro que se está grabando pertenece a alguna de ellas
	$hasInlineSection = false;
	$inlineSectionID = 0;
	$inlineSectionCatID = 0;
	$arrInlineSectionIDs = BITAMSection::existInlineSections($aRepository, $surveyInstance->SurveyID, true);
	if (is_array($arrInlineSectionIDs)) {
		$arrInlineSectionIDs = array_flip($arrInlineSectionIDs);
	}
	else {
		$arrInlineSectionIDs = array();
	}
	
	if (isset($arrayData['inlineValue'])) {
		//Si entro a este if quiere decir que al menos si trae valor para las secciones Inline
		//ahora se corrobora realmente q si exista (el ID de la sección es el mismo que está grabando este registro, ya que puede haber varias
		//secciones)
		$inlineSectionID = $anEntrySectionID;
		if(isset($arrInlineSectionIDs[$inlineSectionID]))
		{
			$hasInlineSection = true;
			$inlineSection = BITAMSection::NewInstanceWithID($aRepository, $inlineSectionID);
			$inlineSectionCatID = $inlineSection->CatalogID;
		}
	}
	//@JAPR
	
	$tableName = $surveyInstance->SurveyTable;
	
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	//Se insertarán los nuevos campos antes de las respuestas, lo cuales ya deben venir con las "," necesarias para funcionar o vacios para no dar problemas
	$sql = "INSERT INTO ".$tableName." ( DateID, HourID, UserID, StartTime, EndTime, LastDateID, LastHourID, LastStartTime, LastEndTime, FactKey, FactKeyDimVal, ".$syncDateField.$syncTimeField;
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	//Si existen los campos, se graba la referencia a la sección o pregunta especial que generan este registro para porder identificarlo
	if ($useSourceFields) {
		$sql .= "EntrySectionID, EntryCatDimID, ";
	}
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	if ($useRecVersionField) {
		$sql .= "eFormsVersionNum, ";
	}
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	if ($useDynamicPageField) {
		$sql .= "DynamicPageDSC, ";
	}
	//@JAPR
	
	/*@MABH20121205*/
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$sql .= "ServerStartDate, ServerStartTime, ServerEndDate, ServerEndTime, ";
	}
	//@MABH20121205

	if(!is_null($folioID))
	{
		$sql.="FolioID, ";
	}
	
	//Modificamos el valor de $currentDate para que incluya la hora, minutos y segundos
	$currentDate = substr($currentDate, 0, 10)." ".substr($surveyHour, 0, 8);
	
	$sqlValues = " VALUES ( ".$aRepository->DataADOConnection->DBTimeStamp($currentDate).", ";
	$sqlValues.=$aRepository->DataADOConnection->Quote($surveyHour).", ";
	$sqlValues.=$userID.", ";
	$sqlValues.=$aRepository->DataADOConnection->Quote($startTime).", ";
	$sqlValues.=$aRepository->DataADOConnection->Quote($endTime).", ";
	//@JAPR 2012-11-23: Corregidas las fechas de última edición, estaban tomando la misma del archivo pero esa se mantenía como la fecha
	//original de la captura, así que no reflejaba la fecha de última edición, para modo Web en realmente la fecha de sincronización
	if ($blnWebMode && $syncDate != '' && $syncTime != '') {
		$sqlValues.=$aRepository->DataADOConnection->DBTimeStamp(substr($syncDate, 0, 10)." ".substr($syncTime, 0, 8)).", ";
		$sqlValues.=$aRepository->DataADOConnection->Quote($syncTime).", ";
	}
	else {
		$sqlValues.=$aRepository->DataADOConnection->DBTimeStamp($currentDate).", ";
		$sqlValues.=$aRepository->DataADOConnection->Quote($surveyHour).", ";
	}
	//@JAPR
	$sqlValues.=$aRepository->DataADOConnection->Quote($startTime).", ";
	$sqlValues.=$aRepository->DataADOConnection->Quote($endTime).", ";
	$sqlValues.=$factKey.", ";
	$sqlValues.=$factKeyDimVal.", ";
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	//Se insertarán los nuevos campos antes de las respuestas, lo cuales ya deben venir con las "," necesarias para funcionar o vacios para no dar problemas
	$sqlValues.=$syncDateValue.$syncTimeValue;
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	//Si existen los campos, se graba la referencia a la sección o pregunta especial que generan este registro para porder identificarlo
	if ($useSourceFields) {
		$sqlValues .= (int)$anEntrySectionID.", ".(int)$anEntryCatDimID.", ";
	}
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	if ($useRecVersionField) {
		$sqlValues .= $aRepository->DataADOConnection->Quote(ESURVEY_SERVICE_VERSION).", ";
	}
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	if ($useDynamicPageField) {
		$sqlValues .= $aRepository->DataADOConnection->Quote($aDynamicPageDSC).", ";
	}
	//@JAPR
	/*@MABH20121205*/
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$sqlValues.=$aRepository->DataADOConnection->Quote($serverSurveyDate).", ";
		$sqlValues.=$aRepository->DataADOConnection->Quote($serverSurveyHour).", ";
		$sqlValues.=$aRepository->DataADOConnection->Quote($serverSurveyEndDate).", ";
		$sqlValues.=$aRepository->DataADOConnection->Quote($serverSurveyEndHour).", ";
	}
	//@MABH20121205
	if(!is_null($folioID))
	{
		$sqlValues.=$folioID.", ";
	}
	
	//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	$blnDynamicProcessed = false;
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		$blnOverWriteValue = true;
		//@JAPR 2012-05-17: Agregados nuevos tipos de pregunta Foto y Firma
		//Las preguntas de Foto y Firma no graban una respuesta, sólo la imagen
		//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
		//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
		//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
		//Cambiado a un switch para mayor facilidad
		$blnValidQuestion = true;
		switch ($aQuestion->QTypeID) {
			case qtpPhoto:
			case qtpDocument:
			case qtpSignature:
			case qtpSketch:
			case qtpSkipSection:
			case qtpMessage:
			case qtpSync:
			case qtpPassword:
			case qtpAudio:
			case qtpVideo:
			case qtpSection:
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			case qtpExit:
				$blnValidQuestion = false;
				break;
		}
		
		if (!$blnValidQuestion)
		{
			//No se debe hacer el Continue o se salta el grabado de la foto y comentarios
			$insertValue = "";
			//continue;
		}
		//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
		else if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
		{
			$sql.=$aQuestion->SurveyField;
			
			//Obtener valor capturado de dicha question
			$postKey = "qfield".$aQuestion->QuestionID;
			//@JAPR 2013-01-22: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
			if(isset($arrayData[$postKey]) && trim($arrayData[$postKey]) !== '')
			{
				$insertValue = (float)$arrayData[$postKey];
			}
			else 
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$insertValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			
			$sqlValues.= StrIFNULL($insertValue);
			
			if($questionKey != (count($questionCollection->Collection)-1))
			{
				$sql.=", ";
				$sqlValues.=", ";
			}
		}
		else 
		{
			//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
			//Las preguntas tipo Action contienen un String con separador _SVSep_ y se grabará así porque usar un separador mas sencillo podría
			//causar que en cambios futuros se confunda con parte de la propia acción, la cual es un Texto libre
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Hora
			//@AAL 06/05/2015: Modificado para soportar preguntas tipo código de barras
			if($aQuestion->QTypeID==qtpOpenDate || $aQuestion->QTypeID==qtpOpenString || $aQuestion->QTypeID==qtpOpenAlpha || $aQuestion->QTypeID == qtpAction || $aQuestion->QTypeID==qtpOpenTime || $aQuestion->QTypeID==qtpBarCode)
			{
				$sql.=$aQuestion->SurveyField;
	
				//Obtener valor capturado de dicha question Date o TextBox
				$postKey = "qfield".$aQuestion->QuestionID;
				
				if(isset($arrayData[$postKey]))
				{
					//@JAPR 2012-11-27: Corregido un bug, no debe hacer un trim a los valores grabados
					$insertValue = $arrayData[$postKey];
				}
				else 
				{
					$insertValue = "";
				}
				
				$sqlValues.=$aRepository->DataADOConnection->Quote($insertValue);

				if($questionKey != (count($questionCollection->Collection)-1))
				{
					$sql.=", ";
					$sqlValues.=", ";
				}
			}
			//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
			elseif ($aQuestion->QTypeID == qtpGPS) {
				$sql.=$aQuestion->SurveyField;
				
				//Obtener valor capturado de dicha question Date o TextBox
				$postKey = "qfield".$aQuestion->QuestionID;
				
				if(isset($arrayData[$postKey]))
				{
					//En este caso hay que agregar el Accuracy si es que no lo traía ya (originalmente no lo hacía)
					$insertValue = $arrayData[$postKey];
					if (stripos($insertValue, '(') === false) {
						$postKeyAcc = "qacc".$aQuestion->QuestionID;
						$strData = (string) @$arrayData[$postKeyAcc];
						if (trim($strData) == '') {
							$strData = $gblEFormsNA;
						}
						$insertValue .= ' ('.$strData.')';
					}
				}
				else 
				{
					$insertValue = "";
				}
				
				$sqlValues.=$aRepository->DataADOConnection->Quote($insertValue);

				if($questionKey != (count($questionCollection->Collection)-1))
				{
					$sql.=", ";
					$sqlValues.=", ";
				}
			}
			//@JAPR
			else 
			{
				if(($aQuestion->QTypeID==qtpSingle || $aQuestion->QTypeID==qtpCallList) && $aQuestion->UseCatalog==0)
				{
					$sql.=$aQuestion->SurveyField;
		
					//Obtener valor capturado de dicha question Seleccion Simple
					$postKey = "qfield".$aQuestion->QuestionID;
					
					if(isset($arrayData[$postKey]))
					{
						//@JAPR 2012-11-27: Corregido un bug, no debe hacer un trim a los valores grabados
						$insertValue = $arrayData[$postKey];
					}
					else 
					{
						$insertValue = "";
					}
					
					$sqlValues.=$aRepository->DataADOConnection->Quote($insertValue);
	
					if($questionKey != (count($questionCollection->Collection)-1))
					{
						$sql.=", ";
						$sqlValues.=", ";
					}
					
					$arrayOptions = array();
					$arrayOptions[] = $insertValue;
					
					//Si dicha opcion trae una accion, entonces se procede a insertarla
					$statusFn = insertSurveyAction($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $arrayOptions);
					
					if($statusFn===false)
					{
						return($gblEFormsErrorMessage);
					}
				}
				else if($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1)
				{
					$sql.=$aQuestion->SurveyField;
		
					//Obtener valor capturado de dicha question Seleccion Simple
					$postKey = "qfield".$aQuestion->QuestionID;
					
					if(isset($arrayData[$postKey]))
					{
						//@JAPR 2012-11-27: Corregido un bug, no debe hacer un trim a los valores grabados
						$strValue = $arrayData[$postKey];
						//@JAPR 2013-08-21: Corregido un bug, durante la edición cuando se agregan nuevos registros, se debe
						//validar para no usar NOCHANGE sino el valor de catálogo correcto
						//Se pasa al array a utilizar el valor de la combinación de catálogo para usarlo en caso de requerir grabar un nuevo registro
						if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID != $dynamicSectionCatID && (string) $strValue == "NOCHANGE" && isset($arrayData[$postKey.'Cat'])) {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								echo("<br>\r\nUsing catalog combination for question {$aQuestion->QuestionID} while saving a new record in survey table");
								echo("<br>\r\nOriginal value == {$strValue}");
								echo("<br>\r\nNew value == ".trim((string) @$arrayData[$postKey.'Cat']));
							}
							$strValue = $arrayData[$postKey.'Cat'];
						}
						//@JAPR
					}
					else 
					{
						$strValue = "";
					}
					
					//En el campo SurveyField original se sigue grabando el key subrrogado de la combinación de la tabla del catálogo que
					//hace match con los valores recibidos, si no se encuentra una combinación válida se graba NA
					if($hasDynamicSection==true && $aQuestion->CatalogID==$dynamicSectionCatID)
					{
						$arrayMapStrValues = array();
						$arrayMapStrValues[0] = $arrayData['sectionValue'];
						$insertValue = $arrayMapStrValues[0];
						//@JAPR 2013-12-12: Corregido un bug, cuando la pregunta era del catálogo mismo catálogo que la de la dinámica, se estaba
						//grabando como valor de las variables simplemente el Key del valor del catálogo correspondiente con la iteración dinámica,
						//lo cual estaba incorrecto porque se pudiera haber estado pidiendo simplemente el valor seleccionado para generar a la
						//sección dinámica en lugar del valor dinámico específico
						//Ahora se forzará a usar la misma función para obtener el Array de descripciones de cada atributo usado en la pregunta
						$arrayStrValues = array();
						$arrayStrValues[0] = $strValue;
						$blnOverWriteValue = false;
						$arrayPassValues = getCatValueKeys($aRepository, $aQuestion, $arrayStrValues, $arrFieldValues);
						//@JAPR
					}
					else 
					{
						$arrayStrValues = array();
						$arrayStrValues[0] = $strValue;

						//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
						$blnOverWriteValue = false;
						$arrayPassValues = getCatValueKeys($aRepository, $aQuestion, $arrayStrValues, $arrFieldValues);
						//@JAPR
						
						if($arrayPassValues!==false)
						{
							$arrayMapStrValues = $arrayPassValues;
						}
						else 
						{
							return($gblEFormsErrorMessage);
						}

						$insertValue = $arrayMapStrValues[0];
					}
					
					$sqlValues.=$aRepository->DataADOConnection->Quote($insertValue);
					
					//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($surveyInstance->DissociateCatDimens) {
						//Si se graban catálogos desasociados entonces existe un campo SurveyCatField donde se graba el valor completo de las
						//combinaciónes recibidas tal como viene del App, además se debe verificar si se está o no grabando un registro para
						//la sección dinámica, ya que de ser así se tienen que grabar campos adicionales que incluirán además los valores
						//hasta el atributo ya sea de las páginas dinámicas o de los checkboxes que contienen, así como la descripción de
						//cada checkbox al que corresponde el registro
						if($aQuestion->CatalogID==$dynamicSectionCatID) {
							//La pregunta corresponde al mismo catálogo de la sección dinámica, así que hay que verificar si se está o no
							//grabando un registro para dicha sección si es así entonces se graban además los campos DynamicOptionDSC y 
							//DynamicValue con el valor del atributo que genera a las opciones múltiples para este registro específico (o vacio
							//si no hay múltiples) y con el valor completo de todos los atributos hasta el de la página/checkbox, el resto de los 
							//atributos existentes en el modelo contendrían el valor de NA.
							//Al día en que se desarrolló esta funcionalidad no se permitían preguntas de catálogo posteriores a la sección
							//dinámica (ni dentro de ella), ya que en ese caso se tendrían que filtrar por alguna de las páginas de la sección
							//y no se sabría por cual
							if (!$blnDynamicProcessed && $hasDynamicSection && ($anEntrySectionID == (($dynamicSectionID > 0)?$dynamicSectionID:$dynamicSectionIDEmpty))) {
								//Si es un registro de la dinámica así que se graban los campos según el atributo de su página/checkbox
								$blnDynamicProcessed = true;
								//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
								//Si se graba a partir de las descripciones, no puede ir a traer el String de la combinación ni la descripción
								//del checkbox al catálogo porque el valor probablemente pudo haber cambiado, y aunque para este punto si fuera un
								//key > 0 ya se habría remapeado, aquellos que fueron eliminados grabarían el valor de *NA así que mejor se usará
								//directamente el dato que llegó desde el App en esos casos
								$strDynamicOptionDSC = '';
								$strDynamicValue = '';
								$arrSectionDesc = @$arrSectionDescByID[$anEntrySectionID][$aDynamicRecNum];
								$strNewCatDimValAnswers = @$arrSectionDescStrByID[$anEntrySectionID][$aDynamicRecNum];
								if (!is_null($strNewCatDimValAnswers) && !is_null($arrSectionDesc) && is_array($arrSectionDesc) && count($arrSectionDesc) > 0) {
									//@JAPR 2013-05-09: Corregido un bug con la codificación
									//Este valor SI se debe decodificar, porque se usará para grabar en las tablas de datos
									//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
									$strDynamicOptionDSC = (string) @$arrSectionDesc[count($arrSectionDesc) -1];
									$strDynamicValue = BITAMCatalog::TranslateSCHValueToDissociatedDimensions($aRepository, $aQuestion->CatalogID, (string) $strNewCatDimValAnswers);
								}
								else {
									$arrDynamicRecValue = getCatValueDescForSurveyField($aRepository, $arrayMapStrValues, $dynamicSectionCatID);
									if (!is_array($arrDynamicRecValue) || count($arrDynamicRecValue) < 2) {
										$arrDynamicRecValue = array();
									}
									$strDynamicOptionDSC = (string) @$arrDynamicRecValue[1];
									$strDynamicValue = (string) @$arrDynamicRecValue[0];
								}
								$sql.=",DynamicOptionDSC, DynamicValue";
								$sqlValues.=",".$aRepository->DataADOConnection->Quote($strDynamicOptionDSC).
									",".$aRepository->DataADOConnection->Quote($strDynamicValue);
								//@JAPR
							}
						}
						
						//Obtiene el String tal como el que venía del App pero esta vez usando los atributos de las dimensiones independientes
						//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
						//Si se utilizan las descripciones para grabar, el valor de la pregunta simple choice de catálogo ya viene en el mismo
						//formato que se necesita pero con los ids de los atributos de la dimensión en lugar de los de las dimensiones
						//independientes, por lo tanto se ejecuta un proceso que remapea dichos ids pero respetando lo demás
						if (trim($strValue) != '') {
							$strCatalogValue = BITAMCatalog::TranslateSCHValueToDissociatedDimensions($aRepository, $aQuestion->CatalogID, $strValue);
						}
						else {
							$arrDynamicRecValue = getCatValueDescForSurveyField($aRepository, $arrayMapStrValues, $aQuestion->CatalogID, $aQuestion->CatMemberID);
							if (!is_array($arrDynamicRecValue) || count($arrDynamicRecValue) < 2) {
								$arrDynamicRecValue = array();
							}
							$strCatalogValue = (string) @$arrDynamicRecValue[0];
						}
						//@JAPR
						$sql.=", ".$aQuestion->SurveyCatField;
						$sqlValues.=", ".$aRepository->DataADOConnection->Quote($strCatalogValue);
					}
					//@JAPR
					
					if($questionKey != (count($questionCollection->Collection)-1))
					{
						$sql.=", ";
						$sqlValues.=", ";
					}
				}
				else 
				{
					$arrayValues = array();
					$sql.=$aQuestion->SurveyField;
		
					//Obtener valor capturado de dicha question Seleccion Multiple
					$postKey = "qfield".$aQuestion->QuestionID;
					
					//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
					if(isset($arrayData[$postKey]) || ($surveyInstance->UseNULLForEmptyNumbers && is_null(@$arrayData[$postKey]) && ($dynamicSectionID==$aQuestion->SectionID || $dynamicSectionIDEmpty==$aQuestion->SectionID) && ($aQuestion->MCInputType == mpcCheckBox || $aQuestion->MCInputType == mpcNumeric)))
					{
						//Se usó @ para forzar a null si no estaba asignado el valor y cumple la condición indicada arriba, ya que en ese
						//caso intencionalmente se desea grabar NULL pues se trata de una valor numérico que no debe ser 0 para no afectar a
						//los cálculos que lo usen
						$strValues = @$arrayData[$postKey];
					}
					else 
					{
						$strValues = "";
					}
					
					if(trim($strValues)=="")
					{
						if ($surveyInstance->UseNULLForEmptyNumbers && ($dynamicSectionID==$aQuestion->SectionID || $dynamicSectionIDEmpty==$aQuestion->SectionID) && ($aQuestion->MCInputType == mpcCheckBox || $aQuestion->MCInputType == mpcNumeric)) {
							//En este caso se desea que se grabe el NULL, por lo que no sobreescribe
							if ($aQuestion->MCInputType == mpcNumeric) {
								$insertValue = null;
							}
							else {
								$insertValue = '0';
							}
						}
						else {
							$insertValue = "";
						}
					}
					else 
					{
						//Para las preguntas de Category Dimension no se queria grabar sobre la tabla SVSurvey_<ModelID>
						//Pero grabaremos la informacion para ver si esta nos sirve para efectos de despliegue del reporte
						if($dynamicSectionID!=$aQuestion->SectionID && $aQuestion->QDisplayMode==dspVertical && $aQuestion->MCInputType==mpcNumeric && $aQuestion->IsMultiDimension==0 && $aQuestion->UseCategoryDimChoice!=0)
						{
							//$insertValue = "";
							$arrayValues = explode("_SVSep_", $arrayData[$postKey]);
							$insertValue = implode(";", $arrayValues);
						}
						else 
						{
							$arrayValues = explode("_SVSep_", $arrayData[$postKey]);
							$insertValue = implode(";", $arrayValues);
						}
					}
					
					//Corregir caso de los multiple choice que capturan cantidades
					//dentro de las secciones dinamicas
					//@JAPR 2012-11-26: Corregido un bug, cuando no se grababa la sección dinámica pero esta de todas formas contenía una pregunta multiple-choice
					//tipo checkbox, el campo físicamente es un double, así que al no recibir un valor estaba intentando insertar un '' en lugar de un 0 y eso
					//generaba un error, así que se validará por la pregunta de sección dinámica incluso si no se recibió el sectionValue
					if(($dynamicSectionID==$aQuestion->SectionID || $dynamicSectionIDEmpty==$aQuestion->SectionID) && $aQuestion->MCInputType==mpcNumeric && $aQuestion->IsIndicatorMC==1)
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						//$insertValue = (float)$insertValue;
						$insertValue = StrIFNULL($insertValue);
						$sqlValues.=$insertValue;
					}
					else 
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$sqlValues.= (is_null($insertValue)?'NULL':$aRepository->DataADOConnection->Quote($insertValue));
					}
	
					if($questionKey != (count($questionCollection->Collection)-1))
					{
						$sql.=", ";
						$sqlValues.=", ";
					}
					
					//Verificamos si la pregunta de seleccion multiple no pertenece a seccion dinamica y que sea de tipo checkbox
					if($dynamicSectionID!=$aQuestion->SectionID && $aQuestion->UseCatToFillMC==0 && $aQuestion->MCInputType==mpcCheckBox)
					{
						//Si dicha opcion trae una accion, entonces se procede a insertarla
						$statusFn = insertSurveyAction($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $arrayValues);
						
						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
				}
			}
		}

		//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		if ($blnOverWriteValue)
		{
			if (!is_null($arrFieldValues) && is_array($arrFieldValues))
			{
				//@JAPR 2012-06-27: Corregido un bug, este array debe indexarse por QuestionNumber, no por QuestionID
				$arrFieldValues["qfield".$aQuestion->QuestionNumber] = $insertValue;
			}
		}
		//@JAPR
		
		//Grabar la imagen o foto
		if($aQuestion->HasReqPhoto==1 || $aQuestion->HasReqPhoto==2)
		{
			$postKey = "qimage".$aQuestion->QuestionID;
			
			if(isset($arrayData[$postKey]))
			{
				$insertImage = $arrayData[$postKey];
				
				if(!is_null($insertImage) && $insertImage!="")
				{
					$statusFn = savePhoto($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertImage);
					
					if($statusFn===false)
					{
						return($gblEFormsErrorMessage);
					}
				}
				else 
				{
					//Inserta el valor de No Aplica
					$statusFn  = savePhotoNA($aRepository, $surveyInstance, $aQuestion, $factKey);
					
					if($statusFn===false)
					{
						return($gblEFormsErrorMessage);
					}
				}
			}
			else 
			{
				$postKey = "qimageencode".$aQuestion->QuestionID;
				
				if(isset($arrayData[$postKey]))
				{
					
					if($arrayData[$postKey] != "qimageencodedyn") 
					{
						//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
						if ($usePhotoPaths) {
							//Conchita 2015-01-14 faltaba validar en web para cuando viene como ruta (nuevo) o cuando viene base 64 (versiones antiguas)
							//En este caso es un nombre de archivo, así que no decodifica nada
								//
							$insertImage = $arrayData[$postKey];
							$blnIsPhotoName = true;
								//si es web mode
								if($blnWebMode){
											
									if(strpos($insertImage, "tmpsurveyimages")>=0){ //si contiene tmpsurveyimages
										//es modo web y usa la ruta de la imagen
														
										 if ($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch) {
                                               $blnIsPhotoName = false;
                                               if (strpos($insertImage, "photo") || strpos($insertImage, "canva")) {
											   //si es canvas o photo se pone en true, si no, ya estaba en false
                                                     $blnIsPhotoName = true;
                                               }
										}
									}
									else {
										//si no contiene la ruta debe de ser un base 64
										$blnIsPhotoName = false;
									}
								}
								else { //es movil (se deja como estaba)
							if ($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch) {
								$blnIsPhotoName = false;
								if (substr($insertImage, 0, 5) == "photo" || substr($insertImage, 0, 5) == "canva") {
									$blnIsPhotoName = true;
								}
							}
								}
							if (!$blnIsPhotoName) {
								$insertImage = base64_decode($arrayData[$postKey]);
							}
								//
						}
						else {
							$insertImage = base64_decode($arrayData[$postKey]);
						}
						//@JAPR
					}
					else 
					{
						$insertImage = $arrayData[$postKey];
					}
				
					if(!is_null($insertImage) && $insertImage!="")
					{
						$statusFn = savePhoto($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertImage);

						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
					else 
					{
						//Inserta el valor de No Aplica
						$statusFn = savePhotoNA($aRepository, $surveyInstance, $aQuestion, $factKey);

						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
				}
				else 
				{
					//Inserta el valor de No Aplica
					$statusFn = savePhotoNA($aRepository, $surveyInstance, $aQuestion, $factKey);
					
					if($statusFn===false)
					{
						return($gblEFormsErrorMessage);
					}
				}
			}
            
            //@MABH 2012-05-24: Para el soporte de fotos (en web), se van a enviar
            //las imagenes en input type="file" con id que inicien en photoimage
            //no como qimageencode ya que desde web no se envia un base64
            $postKey = "photoimage".$aQuestion->QuestionID;
            if(isset($_FILES[$postKey])) {
                $insertImage = "photoimage";
                $statusFn = savePhoto($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertImage);
                if($statusFn===false) {
                    return($gblEFormsErrorMessage);
                }
            }
            
		}
		else 
		{
			//Verificamos si de pura casualidad tiene dicha dimension entonces
			//se trata de insertar el valor de No Aplica
			//$statusFn = savePhotoNA($aRepository, $surveyInstance, $aQuestion, $factKey);
			/*
			if($statusFn===false)
			{
				return($gblEFormsErrorMessage);
			}
			*/
		}
		
		//Grabar el documento
		if($aQuestion->HasReqDocument==1 || $aQuestion->HasReqDocument==2)
		{
			$postKey = "qdocument".$aQuestion->QuestionID;
			
			if(isset($arrayData[$postKey]))
			{
				$insertDocument = $arrayData[$postKey];
				
				if(!is_null($insertDocument) && $insertDocument!="")
				{
					$statusFn = saveDocument($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertDocument);
					
					if($statusFn===false)
					{
						return($gblEFormsErrorMessage);
					}
				}
				else 
				{
					//Inserta el valor de No Aplica
					$statusFn  = saveDocumentNA($aRepository, $surveyInstance, $aQuestion, $factKey);
					
					if($statusFn===false)
					{
						return($gblEFormsErrorMessage);
					}
				}
			}
		}
		
		//Grabar comentario
		if($aQuestion->HasReqComment==1 || $aQuestion->HasReqComment==2)
		{
			$postKey = "qcomment".$aQuestion->QuestionID;
			
			if(isset($arrayData[$postKey]))
			{
				$insertComment = $arrayData[$postKey];
				
				if(!is_null($insertComment))
				{
					$statusFn = saveComment($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertComment);
					
					if($statusFn===false)
					{
						return($gblEFormsErrorMessage);
					}
				}
			}
		}
	}
	
	//@JAPR 2012-05-18: Validados casos donde la última pregunta NO debe incluir campo, si la pregunta previa a esta si los incluye estaba
	//dejando una coma de mas
	if (substr(trim($sql), -1, 1) == ',')
	{
		$sql = substr(trim($sql), 0, -1);
		$sqlValues = substr(trim($sqlValues), 0, -1);
	}
	//@JAPR
	
	$sqlToInsert = $sql.") ".$sqlValues." )";
	
	//Obtener coleccion de las preguntas de simple choice con matriz
	$matrixCollection = BITAMQuestionCollection::NewInstanceWithSimpleChoiceMatrix($aRepository, $surveyInstance->SurveyID);
	
	//Se guardan en la tabla de SVSurveyMatrixData las preguntas de tipo
	//simple choice y se muestran como matrices, si es que hay
	$strResult = saveDataInMatrixTable($aRepository, $surveyInstance, $matrixCollection, $arrayData, $factKey, $factKeyDimVal);
	
	//Se guardan en la tabla de SVSurveyCatDimVal las preguntas de tipo
	//category dimension, si es que no hubo problemas durante el grabado de las matrices
	if($strResult=="" || $strResult=="OK")
	{
		$strResult = saveDataInCatDimValTable($aRepository, $surveyInstance, $arrayData, $factKey, $factKeyDimVal);
	}
	
	if(($strResult=="" || $strResult=="OK"))
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nsaveDataInSurveyTable INSERT: ".$sqlToInsert);
		}
		if($aRepository->DataADOConnection->Execute($sqlToInsert) === false)
		{
			//Si hubo un error en la insercion de los datos en esta tabla paralela, tambien 
			//borramos los datos de la tabla paralela matriz y de la tabla de comentarios, imagenes y acciones
			$strSQLErrMsg = (string) @$aRepository->DataADOConnection->ErrorMsg();
			$statusFn = rollBackSupportTables($aRepository, $surveyInstance, $factKey, $factKeyDimVal);
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$strSQLErrMsg.". ".translate("Executing").": ".$sqlToInsert);
			}
			else 
			{
				return("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$strSQLErrMsg.". ".translate("Executing").": ".$sqlToInsert);
			}
		}
		else 
		{
			//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			//Graba los campos de secciones dinámicas que no fueron procesados si este registro no pertenecía a ella
			if (!$blnDynamicProcessed && $surveyInstance->DissociateCatDimens) {
				//@JAPR 2014-05-30: Agregado el tipo de sección Inline
				//Ahora no símplemente se va a graba vacio si no se estuviese grabando un registro de seccion dinamica en este punto, ya que los
				//campos que aquí se limpiaban pudieran pertenecer a una sección Inline, si fuera así entonces se tienen que grabar los datos
				//equivalentes que en el proceso de arriba se graban en estos campos para las dinámicas, porque gracias al campo EntrySectionID se
				//sabe perfectamente a que registro pertenece cada sección, así que no habría inconveniente en grabar esta información para los
				//registros de Inline
				$strDynamicOptionDSC = '';
				$strDynamicValue = '';
				if ($hasInlineSection) {
					//Hay que sobreescribir los datos con la información del registro Inline
					$arrSectionDesc = @$arrSectionDescByID[$anEntrySectionID][$aDynamicRecNum];
					$strNewCatDimValAnswers = @$arrSectionDescStrByID[$anEntrySectionID][$aDynamicRecNum];
					if (!is_null($strNewCatDimValAnswers) && !is_null($arrSectionDesc) && is_array($arrSectionDesc) && count($arrSectionDesc) > 0) {
						//@JAPR 2013-05-09: Corregido un bug con la codificación
						//Este valor SI se debe decodificar, porque se usará para grabar en las tablas de datos
						//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
						$strDynamicOptionDSC = (string) @$arrSectionDesc[count($arrSectionDesc) -1];
						$strDynamicValue = BITAMCatalog::TranslateSCHValueToDissociatedDimensions($aRepository, $inlineSectionCatID, (string) $strNewCatDimValAnswers);
					}
				}
				
				$sql = "UPDATE ".$tableName." SET DynamicOptionDSC = ".$aRepository->DataADOConnection->Quote($strDynamicOptionDSC).
					", DynamicValue = ".$aRepository->DataADOConnection->Quote($strDynamicValue).
					" WHERE FactKeyDimVal = ".$factKeyDimVal." AND FactKey = ".$factKey;
				$aRepository->DataADOConnection->Execute($sql);
			}
			//@JAPR
			
			//Procedemos a grabar la firma si es que existe
			//@JAPR 2013-01-22: Agregado el parámetro $bSaveSignature para optimizar en que momento se graba la firma
			if($surveyInstance->HasSignature==1 && $bSaveSignature)
			{
				$postKey = "qsignature";
				
				if(isset($arrayData[$postKey]))
				{
					$insertSignature = $arrayData[$postKey];
					if ($usePhotoPaths ) {
						$blnIsPhotoName = false;
						if (substr($insertSignature, 0, 9) == "signature") {
							$blnIsPhotoName = true;
						}
						if (!$blnIsPhotoName) {
							$insertSignature = base64_decode($arrayData[$postKey]);
						}
					}
					else {
						$insertSignature = base64_decode($arrayData[$postKey]);
					}
					
					if(!is_null($insertSignature) && $insertSignature!="")
					{
						$statusFn = saveSignature($aRepository, $surveyInstance, $factKey, $insertSignature);
						
						if($statusFn===false)
						{
							return($gblEFormsErrorMessage);
						}
					}
				}
			}
			
			//@JAPR 2014-11-07: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda
			$agendaID = 0;
			if (array_key_exists("agendaID", $_POST))
			{
				$agendaID = (int) @$_POST["agendaID"];
			}
			
			if ($agendaID > 0) {
				$objAgenda = @BITAMSurveyAgenda::NewInstanceWithID($aRepository, $agendaID);
				if (!is_null($objAgenda)) {
					$intCheckInSurvey = (int) @$objAgenda->CheckinSurvey;
					if ($intCheckInSurvey == $surveyInstance->SurveyID) {
						//Se debe actualizar la fecha de captura de la encuesta de CheckIn de esta captura que es de agenda
						$sql = "UPDATE SI_SV_SurveyAgenda SET 
								EntryCheckInStartDate = ".$aRepository->DataADOConnection->DBTimeStamp($currentDate)." 
							WHERE AgendaID = {$agendaID}";
						$aRepository->DataADOConnection->Execute($sql);
					}
				}
			}
			//@JAPR
		}
	}
	else 
	{
		//Realizamos RollBack de los comentarios, imagenes y acciones insertadas, tabla matriz
		$statusFn = rollBackSupportTables($aRepository, $surveyInstance, $factKey, $factKeyDimVal);

		return $strResult;	
	}
	
	return "OK";
}

function rollBackSupportTables($aRepository, $surveyInstance, $factKey, $factKeyDimVal)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	
	//Borrar las acciones insertadas
	$sql = "DELETE FROM SVSurveyActions WHERE SurveyID = ".$surveyInstance->SurveyID." AND SurveyDimVal = ".$factKeyDimVal." AND FactKey = ".$factKey;
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Borrar las imagenes insertadas
	//Solo faltarian borrar las imagenes fisicamente, lo cual se realizara en otra ocasion
	$sql = "DELETE FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$surveyInstance->SurveyID." AND FactKey = ".$factKeyDimVal." AND MainFactKey = ".$factKey;
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Borrar los comentarios insertados
	$sql = "DELETE FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$surveyInstance->SurveyID." AND FactKey = ".$factKeyDimVal." AND MainFactKey = ".$factKey;
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Eliminamos los elementos de la tabla paralela matrix
	$tableMatrix = "SVSurveyMatrixData_".$surveyInstance->ModelID;
	
	$sql = "DELETE FROM ".$tableMatrix." WHERE FactKeyDimVal = ".$factKeyDimVal." AND FactKey = ".$factKey;
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableMatrix." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableMatrix." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Eliminamos los elementos de la tabla paralela category dimension
	$tableCatDimVal = "SVSurveyCatDimVal_".$surveyInstance->ModelID;
	
	$sql = "DELETE FROM ".$tableCatDimVal." WHERE FactKeyDimVal = ".$factKeyDimVal." AND FactKey = ".$factKey;
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableCatDimVal." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableCatDimVal." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Eliminamos los elementos de la tabla paralela
	$sql = "DELETE FROM ".$surveyInstance->SurveyTable." WHERE FactKeyDimVal = ".$factKeyDimVal." AND FactKey = ".$factKey;
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	return true;
}

function rollBackCmtImgAct($aRepository, $surveyInstance, $factKey, $factKeyDimVal)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	
	//Borrar las acciones insertadas
	$sql = "DELETE FROM SVSurveyActions WHERE SurveyID = ".$surveyInstance->SurveyID." AND SurveyDimVal = ".$factKeyDimVal." AND FactKey = ".$factKey;
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	//Borrar las imagenes insertadas
	//Solo faltarian borrar las imagenes fisicamente, lo cual se realizara en otra ocasion
	$sql = "DELETE FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$surveyInstance->SurveyID." AND FactKey = ".$factKeyDimVal." AND MainFactKey = ".$factKey;
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	//Borrar los comentarios insertados
	$sql = "DELETE FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$surveyInstance->SurveyID." AND FactKey = ".$factKeyDimVal." AND MainFactKey = ".$factKey;
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
}

function insertSurveyAction($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $arrayOptions)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	
	$surveyID = $surveyInstance->SurveyID;
	$questionID = $aQuestion->QuestionID;
	
	foreach($arrayOptions as $optionDesc)
	{
		$sql = "SELECT ActionText FROM SI_SV_QOptionActions 
				WHERE QuestionID = ".$questionID." AND OptionText = ".$aRepository->DataADOConnection->Quote($optionDesc)." 
				ORDER BY SortOrder";
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QOptionActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_QOptionActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		while(!$aRS->EOF)
		{
			$actionText = $aRS->fields["actiontext"];
			
			//Verificamos si la accion ya se encuentra dada de alta en la tabla SVSurveyActions, si es asi, entonces no la insertamos nuevamente
			$sql = "SELECT Action FROM SVSurveyActions WHERE SurveyDimVal = ".$factKeyDimVal." AND SurveyID = ".$surveyID." AND QuestionID = ".$questionID." 
					AND OptionText = ".$aRepository->DataADOConnection->Quote($optionDesc)." AND Action = ".$aRepository->DataADOConnection->Quote($actionText);
			
			$aRSTmp = $aRepository->DataADOConnection->Execute($sql);
			
			if ($aRSTmp === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					return false;
				}
			}
			
			//Si no regresa algun elemento entonces si se inserta
			if ($aRSTmp->EOF)
			{	
				//Insertamos la accion dentro de la tabla SVSurveyActions
				$sql = "INSERT INTO SVSurveyActions (
						SurveyDimVal
						, SurveyID
						, FactKey
						, QuestionID
						, OptionText
						, Action
						) VALUES ( 
						".$factKeyDimVal." 
						, ".$surveyID." 
						, ".$factKey." 
						, ".$questionID." 
						, ".$aRepository->DataADOConnection->Quote($optionDesc)." 
						, ".$aRepository->DataADOConnection->Quote($actionText)." 
						)";
				
				if($aRepository->DataADOConnection->Execute($sql) === false)
				{
					if($gblShowErrorSurvey)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					else 
					{
						$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
						return false;
					}
				}
			}
			
			$aRS->MoveNext();
		}
	}
	
	return true;
}

function savePhotoNA($aRepository, $surveyInstance, $aQuestion, $factKey)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	
	//Verificamos si hay dimension asignada para la imagen requerida/opcional
	if(!is_null($aQuestion->ImgDimID) && $aQuestion->ImgDimID>0)
	{
		$tableDimImage = "RIDIM_".$aQuestion->ImgDimID;
		$fieldDimImageKey = $tableDimImage."KEY";
		$fieldDimImageDsc = "DSC_".$aQuestion->ImgDimID;
		$tablaHechos = "RIFACT_".$surveyInstance->ModelID;
		
		//Y realizamos el UPDATE para indicar con que valor se guardo la imagen en la dimension
		//que en este caso es valor de No Aplica (1)
		$sqlDimImage = "UPDATE ".$tablaHechos." SET ".$fieldDimImageKey." = 1 WHERE FactKey = ".$factKey;
		
		if($aRepository->DataADOConnection->Execute($sqlDimImage) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimImage);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimImage;
				return false;
			}
		}
	}
	
	return true;
}

function savePhoto($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertImage)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
	global $usePhotoPaths;
	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	global $appVersion;
	//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
	global $blnWebMode;
	//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
	global $useRecoveryPaths;
	//@JAPR 2013-05-09: Corregido un bug, faltaba hacer referencia al array de secciones maestro-detalle
	global $arrMasterDetSectionsIDs;
	//@JAPR
	
	//Obtenemos el prefijo de la ruta requerida en la dimension imagen
	$script = $_SERVER["REQUEST_URI"];
	//Eliminamos la primera diagonal que separa el archivo php de la carpeta
	$position = strrpos($script, "/");
	$dir = substr($script, 0, $position);
		
	//Obtenemos el siguiente diagonal para de ahi obtener la carpeta principal de trabajo
	$position = strrpos($dir, "/");
	$mainDir = substr($dir, ($position+1));
	
	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	$dynamicSectionID = BITAMSection::existDynamicSection($surveyInstance->Repository, $surveyInstance->SurveyID);
	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	$blnMasterDetPhoto = false;
	if ($appVersion >= esveFormsV41Func) {
		if (isset($arrMasterDetSectionsIDs[$aQuestion->SectionID])) {
			$blnMasterDetPhoto = true;
		}
	}
	//@JAPR
	
	if($insertImage != "qimageencodedyn" || $insertImage == "photoimage") {

		$DocsDir = getcwd()."\\"."surveyimages";
		$SurveyImagesPath = "\\"."surveyimages";
		if (!file_exists($DocsDir))
		{
			return true; //dir not found
		}
		
		$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
		$SurveyImagesPath .= "\\".trim($_SESSION["PABITAM_RepositoryName"]);
		if (!file_exists($RepositoryDir))
		{
			if (!mkdir($RepositoryDir))
			{
				return true; //the repository dir could not be created
			}
		}
	  
		$ProjectDir = $RepositoryDir."\\"."survey_".$surveyInstance->SurveyID;
		$SurveyImagesPath .= "\\"."survey_".$surveyInstance->SurveyID;
		if (!file_exists($ProjectDir))
		{
			if (!mkdir($ProjectDir))
			{
				return true; //the project dir could not be created
			}
		}
		
		//agregar el factkey si se trata de una seccion dinamica
		//$dynamicSectionID = BITAMSection::existDynamicSection($surveyInstance->Repository, $surveyInstance->SurveyID);
		//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
		//Agrega el factkey si se trata de una sección maestro-detalle
		$mainFactKey = "";
		if($dynamicSectionID == $aQuestion->SectionID || $blnMasterDetPhoto) {
			$mainFactKey = "_".$factKey;
		}
		//@JAPR
		
		//survey_10/photo_survey_question_factkey.jpg
		//$strDocumentName = "photo_".$surveyInstance->SurveyID."_".$aQuestion->QuestionID."_".$factKey.".jpg";
		$strDocumentName = "photo_".$surveyInstance->SurveyID."_".$aQuestion->QuestionID."_".$factKeyDimVal.$mainFactKey.".jpg";
		
		$strFieldDocumentName = "survey_".$surveyInstance->SurveyID."/".$strDocumentName;
		$strFieldDocumentName2 = $mainDir."/"."surveyimages"."/".trim($_SESSION["PABITAM_RepositoryName"])."/"."survey_".$surveyInstance->SurveyID."/".$strDocumentName;
		//$SurveyImagesPath .= $strDocumentName;
		$strCompletePath = $ProjectDir."\\".$strDocumentName;
		
		//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
		$blnIsPhotoName = true;
		if ($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch) {
			if ($usePhotoPaths) {
				$blnIsPhotoName = false;
				if (substr($insertImage, 0, 5) == "photo" || substr($insertImage, 0, 5) == "canva" ) {
					$blnIsPhotoName = true;
				}
			}
		}
		//Conchita 2015-01-14
		//Si es webmode validamos si trae la palabra canvas o photo
		if($blnWebMode){
			if ($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch) {
				if (strpos($insertImage, "photo")   || strpos($insertImage, "canvas")) {
					$blnIsPhotoName = true;
				}
			}
		
		}
		//Conchita
		
		
		if ($usePhotoPaths && $blnIsPhotoName) {
			//Realiza la copia del archivo de imagen
			//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
			if ($blnWebMode) {
				$path = getcwd()."\\";
			}
			else {
				//@JAPR 2013-02-27: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
				$path = getcwd()."\\".(($useRecoveryPaths)?"syncerrors":"syncdata")."\\".trim($_SESSION["PABITAM_RepositoryName"]);
			}
			$strCompleteSourcePath = str_replace("/", "\\", $path."\\".$insertImage);
			$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
			if (strpos(strtolower($strCompleteSourcePath), '.jpg') === false) {
				$strCompleteSourcePath .= '.jpg';
			}
			$status = @copy($strCompleteSourcePath, $strCompletePath);
		}
		else {
	        if($insertImage == "photoimage") {
	            $tmpfilename = $_FILES['photoimage'.$aQuestion->QuestionID]['tmp_name'];
	            $status = move_uploaded_file($tmpfilename, $strCompletePath);
	        } else {
				$status = file_put_contents ($strCompletePath, $insertImage);
	        }
		}
		//@JAPR
        
		//Conchita 2-nov-2011 si es android la imagen no se debe de rotar
		
		//@JAPR 2013-02-25: Modificada la forma de validar para usar una función genérica que ya se tenía (caso de curl donde HTTP_USER_AGENT no existe)
		$nav=(isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
		//@JAPR 2012-05-22: Agregadas las preguntas tipo Firma
		if ($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch)
		{
			//Si la pregunta es tipo firma, realmente lo que llega es un .png, pero se disfraza como .jpg
			//En este caso NO se va a renombrar el archivo a Signature_####_###, se dejará como si fuera una Foto de cualquier pregunta
			$status = true;
		}
		//@JAPR
		else
		{
			//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
			if ($usePhotoPaths) {
				//En este caso la imagen ya estaba rotada, así que no hay que hacer procesamiento especial
			}
			else {
				//Conchita corregido para la 3.00018 no debe de rotar
				if(!preg_match('/android/i',$nav) && $insertImage != "photoimage" && $appVersion < 3.00018) {
					//la imagen se guarda rotada, se debe volver a rotar en el servidor
					$origen = imagecreatefromjpeg($strCompletePath);
					$rotar = imagerotate($origen, -90, 0);
					$status = imagejpeg($rotar, $strCompletePath);
				}
			}
		}
		
		//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar imagenes a otra ruta
		$blnCopyToAltPath = false;
		$strDestPath = '';
		require_once('settingsvariable.inc.php');
		$objSettingReqAltVersion = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'REQUIREDVERSIONFORALTSERVER');
		if (!is_null($objSettingReqAltVersion) && trim($objSettingReqAltVersion->SettingValue != ''))
		{
			$objSettingAltVersion = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ALTEFORMSSERVICEVERSION');
			//Si la app que se conecta es menor que el servicio alterno (si existe) entonces se usa este
			//sino entonces debe ocupar la config de mirror
			if($appVersion < $objSettingAltVersion->SettingValue) {
				$objSettingAltService = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ALTEFORMSSERVICE');
				if(!is_null($objSettingAltService) && trim($objSettingAltService->SettingValue != '')) {
					$blnCopyToAltPath = true;
					$tmpAltService = substr($objSettingAltService->SettingValue, strpos($objSettingAltService->SettingValue, "/"));
					$strDestPath = $tmpAltService;
				}
			} else {
				$objSettingMirror = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'SURVEYIMAGESMIRRORFOLDER');
				if(!is_null($objSettingMirror) && trim($objSettingMirror->SettingValue != '')) {
					$blnCopyToAltPath = true;
					$strDestPath = $objSettingMirror->SettingValue;
				}
			}
		}
		
		if ($blnCopyToAltPath && trim($strDestPath) != '') {
			//Si el path configurado es vía http, se debe hacer una transferencia de archivo a otro server
			if (strtolower(substr($strDestPath, 0, 4)) == 'http') {
				
			}
			else {
				//En este caso es una ruta relativa, se asume que está en el mismo server por lo que
				//simplemente de la ruta de $strCompletePath elimina la referencia al servicio actual
				//y pone la ruta especificada, pero en el mismo directorio base
				$strBasePath = getcwd();	//Ruta completa de este script desde la unidad C:\, incluyendo el path del servicio
				$strCurrServicePath = substr($_SERVER["REQUEST_URI"], 0, strrpos($_SERVER["REQUEST_URI"], "/"));	//Solo el path del servicio empezando por "/" sin "/" al final
				$strCurrServicePath = str_ireplace("/", "\\", $strCurrServicePath);
				$strBasePath = str_ireplace($strCurrServicePath, "", $strBasePath);	//Sólo la ruta de la raíz sin el path del servicio
				//Concatena a la ruta raiz base, la ruta alternativa de servicio configurada
				$strDestPath = $strBasePath.$strDestPath;
			}
			
			//Finalmente a la ruta obtenida le concatena el nombre de archivo, el cual empieza desde el path SurveyImages
			$strDestPath .= "\\".$SurveyImagesPath;
			$strDestPath = str_ireplace('/', "\\", $strDestPath);
			$strDestPath = str_ireplace("\\\\", "\\", $strDestPath);
			//Primero intenta crear la ruta generada
			@mkdir($strDestPath, 0777, true);
			//Luego copia el archivo a la nueva ruta
			@copy($strCompletePath, $strDestPath."\\".$strDocumentName);
		}
		//@JAPR
	} else {
		$status = true;
	}
	
	if($status!=false)
	{
		//$dynamicSectionID = BITAMSection::existDynamicSection($surveyInstance->Repository, $surveyInstance->SurveyID);
		$checkMainFactKey = "";
		//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
		if($dynamicSectionID == $aQuestion->SectionID || $blnMasterDetPhoto) {
			$checkMainFactKey = " AND MainFactKey = ".$factKey;
		}
	
		//Verificamos si la imagen ya se encuentra dada de alta en la tabla SI_SV_SurveyAnswerImage, si es asi, entonces no la insertamos nuevamente
		if($insertImage == "qimageencodedyn") {
			$sql = "SELECT MainFactKey, PathImage FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$surveyInstance->SurveyID." AND QuestionID = ".$aQuestion->QuestionID." AND FactKey = ".$factKeyDimVal." ORDER BY MainFactKey DESC";
		} else {
			$sql = "SELECT PathImage FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$surveyInstance->SurveyID." AND QuestionID = ".$aQuestion->QuestionID." AND FactKey = ".$factKeyDimVal.$checkMainFactKey;
		}
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		} 

		if($insertImage == "qimageencodedyn") {
			$pathImage = $aRS->fields["pathimage"];
		}
		
		//Si no regresa algun elemento entonces si se inserta
		if (($aRS->EOF && $insertImage != "qimageencodedyn") || (!$aRS->EOF && $insertImage == "qimageencodedyn"))
		{
			
			if($insertImage == "qimageencodedyn") {
				$strFieldDocumentName = $pathImage;
				$strFieldDocumentName2 = $mainDir."/"."surveyimages"."/".trim($_SESSION["PABITAM_RepositoryName"])."/".$strFieldDocumentName;
			}
			
			//Procedemos a insertar la imagen
			$sql = "INSERT INTO SI_SV_SurveyAnswerImage (SurveyID, QuestionID, MainFactKey, FactKey, PathImage) VALUES 
					(
					".$surveyInstance->SurveyID.
					", ".$aQuestion->QuestionID.
					", ".$factKey.
					", ".$factKeyDimVal.
					", ".$aRepository->DataADOConnection->Quote($strFieldDocumentName).
					")";
					
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					return false;
				}
			}
		}
		
		if(!is_null($aQuestion->ImgDimID) && $aQuestion->ImgDimID>0)
		{
			
			$tableDimImage = "RIDIM_".$aQuestion->ImgDimID;
			$fieldDimImageKey = $tableDimImage."KEY";
			$fieldDimImageDsc = "DSC_".$aQuestion->ImgDimID;
			$tablaHechos = "RIFACT_".$surveyInstance->ModelID;
			
			//Primero consultamos la imagen para verificar si ya se encuentra dada de alta en la dimension
			$sql = "SELECT ".$fieldDimImageKey." FROM ".$tableDimImage." WHERE ".$fieldDimImageDsc." = ".$aRepository->DataADOConnection->Quote($strFieldDocumentName2);
			
			$aRS = $aRepository->DataADOConnection->Execute($sql);
	
			if ($aRS === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimImage." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableDimImage." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					return false;
				}
			}
	
			//Si se encuentra dada de alta solo consultamos su valor de llave subrogada
			if (!$aRS->EOF)
			{
				$valueKey = $aRS->fields[strtolower($fieldDimImageKey)];
				
				//Y realizamos el UPDATE para indicar con que valor se guardo la imagen en la dimension
				$sqlDimImage = "UPDATE ".$tablaHechos." SET ".$fieldDimImageKey." = ".$valueKey." WHERE FactKey = ".$factKey;
				
				if($aRepository->DataADOConnection->Execute($sqlDimImage) === false)
				{
					if($gblShowErrorSurvey)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimImage);
					}
					else 
					{
						$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimImage;
						return false;
					}
				}
			}
			else 
			{
				//En caso contrario entonces insertamos dicho Image en la dimension Image de la encuesta
				$sqlDimImage = "INSERT INTO ".$tableDimImage." (".$fieldDimImageDsc.") VALUES (".$aRepository->DataADOConnection->Quote($strFieldDocumentName2).")";
				
				if($aRepository->DataADOConnection->Execute($sqlDimImage) === false)
				{
					if($gblShowErrorSurvey)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimImage." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimImage);
					}
					else 
					{
						$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableDimImage." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimImage;
						return false;
					}
				}
				else 
				{
					//Realizamos el UPDATE en la tabla de hechos pero primero obtenemos el valor con el que se inserto
					$sql = "SELECT ".$fieldDimImageKey." FROM ".$tableDimImage." WHERE ".$fieldDimImageDsc." = ".$aRepository->DataADOConnection->Quote($strFieldDocumentName2);
					
					$aRS = $aRepository->DataADOConnection->Execute($sql);
			
					if ($aRS === false)
					{
						if($gblShowErrorSurvey)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimImage." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						else 
						{
							$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableDimImage." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
							return false;
						}
					}
			
					if (!$aRS->EOF)
					{
						$valueKey = $aRS->fields[strtolower($fieldDimImageKey)];
						
						//Y realizamos el UPDATE para indicar con que valor se guardo la imagen en la dimension
						$sqlDimImage = "UPDATE ".$tablaHechos." SET ".$fieldDimImageKey." = ".$valueKey." WHERE FactKey = ".$factKey;
						
						if($aRepository->DataADOConnection->Execute($sqlDimImage) === false)
						{
							if($gblShowErrorSurvey)
							{
								die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimImage);
							}
							else 
							{
								$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimImage;
								return false;
							}
						}
					}
				}
			}
		}
	}
	else 
	{
		if(!is_null($aQuestion->ImgDimID) && $aQuestion->ImgDimID>0)
		{
			//En este caso si falla el guardado de la imagen entonces no debe haber 
			//valor para la dimension imagen mas que el No Aplica
			//Inserta el valor de No Aplica
			$statusFn = savePhotoNA($aRepository, $surveyInstance, $aQuestion, $factKey);
			
			return($statusFn);
		}
	}
	
	return true;
}

function saveDocumentNA($aRepository, $surveyInstance, $aQuestion, $factKey)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	//Verificamos si hay dimension asignada para la imagen requerida/opcional
	if(!is_null($aQuestion->DocDimID) && $aQuestion->DocDimID>0)
	{
		$tableDimImage = "RIDIM_".$aQuestion->DocDimID;
		$fieldDimImageKey = $tableDimImage."KEY";
		$fieldDimImageDsc = "DSC_".$aQuestion->DocDimID;
		$tablaHechos = "RIFACT_".$surveyInstance->ModelID;
		
		//Y realizamos el UPDATE para indicar con que valor se guardo la imagen en la dimension
		//que en este caso es valor de No Aplica (1)
		$sqlDimImage = "UPDATE ".$tablaHechos." SET ".$fieldDimImageKey." = 1 WHERE FactKey = ".$factKey;
		
		if($aRepository->DataADOConnection->Execute($sqlDimImage) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimImage);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimImage;
				return false;
			}
		}
	}
	
	return true;
}

function saveDocument($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertDocument)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	//@JAPR 2012-10-22: Agregado el grabado de documentos a partir de la ruta donde se subieron
	global $useDocumentPaths;
	//@JAPR 2012-10-29: Corregido un bug, los documentos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	global $appVersion;
	//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
	global $blnWebMode;
	//@JAPR
	
	//si es movil
	//buscamos el nombre del archivo ya que en movil viene la ruta del dispositivo
	//movil
	if($blnWebMode != 1){

		//obtenemos el nombre del archivo de audio y video (en android y en el caso de video de ios viene con / y una ruta del dispositivo)
		$x = strrpos($insertDocument,'/');
		if ($x !== false) {
			$insertDocument = substr($insertDocument,$x+1);	
		}
	}
	//continua normal
	
	//Obtenemos el prefijo de la ruta requerida en la dimension documento
	$script = $_SERVER["REQUEST_URI"];
	//Eliminamos la primera diagonal que separa el archivo php de la carpeta
	$position = strrpos($script, "/");
	$dir = substr($script, 0, $position);
		
	//Obtenemos el siguiente diagonal para de ahi obtener la carpeta principal de trabajo
	$position = strrpos($dir, "/");
	$mainDir = substr($dir, ($position+1));
	
	//@JAPR 2012-10-29: Corregido un bug, los documentos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	$dynamicSectionID = BITAMSection::existDynamicSection($surveyInstance->Repository, $surveyInstance->SurveyID);
	//@JAPR 2012-10-29: Corregido un bug, los documentos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	$blnMasterDetDocument = false;
	if ($appVersion >= esveFormsV41Func) {
		if (isset($arrMasterDetSectionsIDs[$aQuestion->SectionID])) {
			$blnMasterDetDocument = true;
		}
	}
	//@JAPR
	
	if($insertDocument != "qdocumentdyn" || $insertDocument == "qdocument") {
		//validar si es movil debe de buscar en syncdata no en surveydocuments
		if($blnWebMode != 1){
			$DocsDir = getcwd()."\\"."syncdata";

		}else {
			$DocsDir = getcwd()."\\"."surveydocuments";
		}
		
		if (!file_exists($DocsDir))
		{
			return true; //dir not found
		}
		
		$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
		
		if (!file_exists($RepositoryDir))
		{
			if (!mkdir($RepositoryDir))
			{
				return true; //the repository dir could not be created
			}
		}
	  
		$ProjectDir = $RepositoryDir."\\"."survey_".$surveyInstance->SurveyID;
	  
		if (!file_exists($ProjectDir))
		{
			if (!mkdir($ProjectDir))
			{
				return true; //the project dir could not be created
			}
		}
		
		//agregar el factkey si se trata de una seccion dinamica
		//$dynamicSectionID = BITAMSection::existDynamicSection($surveyInstance->Repository, $surveyInstance->SurveyID);
		//@JAPR 2012-10-29: Corregido un bug, los documentos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
		//Agrega el factkey si se trata de una sección maestro-detalle
		$mainFactKey = "";
		if($dynamicSectionID == $aQuestion->SectionID || $blnMasterDetDocument) {
			$mainFactKey = "_".$factKey;
		}
		//@JAPR
		
		//survey_10/document_survey_question_factkey.jpg
		//$strDocumentName = "photo_".$surveyInstance->SurveyID."_".$aQuestion->QuestionID."_".$factKey.".jpg";
		
		//substr($insertDocument,42) se usa el 42 porque del nombre del archivo temporal queremos sacar el nombre original
		//los archivos temporales se guardan como tmpsurveydocuments/yyyyMMddhhmmss/bftxxxx.FILE.ext, ej.
		//tmpsurveydocuments/20121127090437/bft43dd.FormsV4.xlsx 
		if($blnWebMode != 1){
			//si es movil, entonces el nombre es el insertdocument previamente limpiado
			$documentOriginalName = $insertDocument;
		}else {
			//es web entonces se hace como antes
			$documentOriginalName = substr($insertDocument,42);
		}
		
		if($blnWebMode != 1){
			$strDocumentName = $documentOriginalName;
			$strFieldDocumentName = $strDocumentName;
			$strFieldDocumentName2 = $mainDir."/"."syncdata"."/".trim($_SESSION["PABITAM_RepositoryName"])."/"."survey_".$surveyInstance->SurveyID."/".$strDocumentName;
				
		}
		else {
			$strDocumentName = "document_".$surveyInstance->SurveyID."_".$aQuestion->QuestionID."_".$factKeyDimVal.$mainFactKey."_".$documentOriginalName;
			$strFieldDocumentName = "survey_".$surveyInstance->SurveyID."/".$strDocumentName;
			$strFieldDocumentName2 = $mainDir."/"."surveydocuments"."/".trim($_SESSION["PABITAM_RepositoryName"])."/"."survey_".$surveyInstance->SurveyID."/".$strDocumentName;
		}
		$strCompletePath = $ProjectDir."\\".$strDocumentName;
		
		//@JAPR 2012-10-22: Agregado el grabado de documentos a partir de la ruta donde se subieron
		$path = getcwd()."\\";
		if($blnWebMode != 1){
			//si es movil se graba en syncdata
			$strCompleteSourcePath = $path."syncdata"."/".trim($_SESSION["PABITAM_RepositoryName"])."/".$strDocumentName;
			$strCompleteSourcePath = str_replace("/", "\\", $strCompleteSourcePath);
			$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
		} else {
			$strCompleteSourcePath = str_replace("/", "\\", $path."\\".$insertDocument);
			$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
		}
		
		$status = @copy($strCompleteSourcePath, $strCompletePath);
		//Conchita 2014-11-27 Si es movil, borramos el archivo anterior
		//ya que se estaba guardando dos veces
		if($blnWebMode != 1 && $status){
			@unlink($strCompleteSourcePath);
		}
	} else {
		$status = true;
	}
	
	if($status!=false)
	{
		//$dynamicSectionID = BITAMSection::existDynamicSection($surveyInstance->Repository, $surveyInstance->SurveyID);
		$checkMainFactKey = "";
		//@JAPR 2012-10-29: Corregido un bug, los documentos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
		if($dynamicSectionID == $aQuestion->SectionID || $blnMasterDetDocument) {
			$checkMainFactKey = " AND MainFactKey = ".$factKey;
		}
	
		//Verificamos si el documento ya se encuentra dada de alta en la tabla SI_SV_SurveyAnswerDocument, si es asi, entonces no la insertamos nuevamente
		if($insertDocument == "qdocumentdyn") {
			$sql = "SELECT MainFactKey, PathDocument FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$surveyInstance->SurveyID." AND QuestionID = ".$aQuestion->QuestionID." AND FactKey = ".$factKeyDimVal." ORDER BY MainFactKey DESC";
		} else {
			$sql = "SELECT PathDocument FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$surveyInstance->SurveyID." AND QuestionID = ".$aQuestion->QuestionID." AND FactKey = ".$factKeyDimVal.$checkMainFactKey;
		}
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		} 

		if($insertDocument == "qdocumentdyn") {
			$pathDocument = $aRS->fields["pathdocument"];
		}
		
		//Si no regresa algun elemento entonces si se inserta
		if (($aRS->EOF && $insertDocument != "qdocumentdyn") || (!$aRS->EOF && $insertDocument == "qdocumentdyn"))
		{
			
			if($insertDocument == "qdocumentdyn") {
				$strFieldDocumentName = $pathDocument;
				$strFieldDocumentName2 = $mainDir."/"."surveydocuments"."/".trim($_SESSION["PABITAM_RepositoryName"])."/".$strFieldDocumentName;
			}
			
			//Procedemos a insertar el documento
			$sql = "INSERT INTO SI_SV_SurveyAnswerDocument (SurveyID, QuestionID, MainFactKey, FactKey, PathDocument) VALUES 
					(
					".$surveyInstance->SurveyID.
					", ".$aQuestion->QuestionID.
					", ".$factKey.
					", ".$factKeyDimVal.
					", ".$aRepository->DataADOConnection->Quote($strFieldDocumentName).
					")";
					
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					return false;
				}
			}
		}
		
		if(!is_null($aQuestion->DocDimID) && $aQuestion->DocDimID>0)
		{
			
			$tableDimDocument = "RIDIM_".$aQuestion->DocDimID;
			$fieldDimDocumentKey = $tableDimDocument."KEY";
			$fieldDimDocumentDsc = "DSC_".$aQuestion->DocDimID;
			$tablaHechos = "RIFACT_".$surveyInstance->ModelID;
			
			//Primero consultamos el documento para verificar si ya se encuentra dada de alta en la dimension
			$sql = "SELECT ".$fieldDimDocumentKey." FROM ".$tableDimDocument." WHERE ".$fieldDimDocumentDsc." = ".$aRepository->DataADOConnection->Quote($strFieldDocumentName2);
			
			$aRS = $aRepository->DataADOConnection->Execute($sql);
	
			if ($aRS === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimDocument." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableDimDocument." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					return false;
				}
			}
	
			//Si se encuentra dada de alta solo consultamos su valor de llave subrogada
			if (!$aRS->EOF)
			{
				$valueKey = $aRS->fields[strtolower($fieldDimDocumentKey)];
				
				//Y realizamos el UPDATE para indicar con que valor se guardo el documento en la dimension
				$sqlDimDocument = "UPDATE ".$tablaHechos." SET ".$fieldDimDocumentKey." = ".$valueKey." WHERE FactKey = ".$factKey;
				
				if($aRepository->DataADOConnection->Execute($sqlDimDocument) === false)
				{
					if($gblShowErrorSurvey)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimDocument);
					}
					else 
					{
						$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimDocument;
						return false;
					}
				}
			}
			else 
			{
				//En caso contrario entonces insertamos dicho Document en la dimension Document de la encuesta
				$sqlDimDocument = "INSERT INTO ".$tableDimDocument." (".$fieldDimDocumentDsc.") VALUES (".$aRepository->DataADOConnection->Quote($strFieldDocumentName2).")";
				
				if($aRepository->DataADOConnection->Execute($sqlDimDocument) === false)
				{
					if($gblShowErrorSurvey)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimDocument." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimDocument);
					}
					else 
					{
						$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableDimDocument." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimDocument;
						return false;
					}
				}
				else 
				{
					//Realizamos el UPDATE en la tabla de hechos pero primero obtenemos el valor con el que se inserto
					$sql = "SELECT ".$fieldDimDocumentKey." FROM ".$tableDimDocument." WHERE ".$fieldDimDocumentDsc." = ".$aRepository->DataADOConnection->Quote($strFieldDocumentName2);
					
					$aRS = $aRepository->DataADOConnection->Execute($sql);
			
					if ($aRS === false)
					{
						if($gblShowErrorSurvey)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimDocument." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						else 
						{
							$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableDimDocument." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
							return false;
						}
					}
			
					if (!$aRS->EOF)
					{
						$valueKey = $aRS->fields[strtolower($fieldDimDocumentKey)];
						
						//Y realizamos el UPDATE para indicar con que valor se guardo el document en la dimension
						$sqlDimDocument = "UPDATE ".$tablaHechos." SET ".$fieldDimDocumentKey." = ".$valueKey." WHERE FactKey = ".$factKey;
						
						if($aRepository->DataADOConnection->Execute($sqlDimDocument) === false)
						{
							if($gblShowErrorSurvey)
							{
								die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimDocument);
							}
							else 
							{
								$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDimDocument;
								return false;
							}
						}
					}
				}
			}
		}
	}
	else 
	{
		if(!is_null($aQuestion->DocDimID) && $aQuestion->DocDimID>0)
		{
			//En este caso si falla el guardado de la documento entonces no debe haber 
			//valor para la dimension documento mas que el No Aplica
			//Inserta el valor de No Aplica
			$statusFn = saveDocumentNA($aRepository, $surveyInstance, $aQuestion, $factKey);
			
			return($statusFn);
		}
	}
	
	return true;
}

function saveSignature($aRepository, $surveyInstance, $factKey, $insertSignature)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	global $usePhotoPaths;
	//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
	global $useRecoveryPaths;
	//@JAPR
	
	$DocsDir = getcwd()."\\"."surveysignature";
	
	if (!file_exists($DocsDir))
	{
		return true; //dir not found
	} 
	
	$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
	
	if (!file_exists($RepositoryDir))
	{
		if (!mkdir($RepositoryDir))
		{
			return true; //the repository dir could not be created
		}
	}
  
	$ProjectDir = $RepositoryDir."\\"."survey_".$surveyInstance->SurveyID;
  
	if (!file_exists($ProjectDir))
	{
		if (!mkdir($ProjectDir))
		{
			return true; //the project dir could not be created
		}
	}
	
	//survey_10/signature_survey_factkey.jpg
	$strDocumentName = "signature_".$surveyInstance->SurveyID."_".$factKey.".jpg";
	$strFieldDocumentName = "survey_".$surveyInstance->SurveyID."/".$strDocumentName;
	
	$strCompletePath = $ProjectDir."\\".$strDocumentName;
	//si usephotopaths entonces copia la imagen
	$blnIsPhotoName = true;
	if ($usePhotoPaths) {
		$blnIsPhotoName = false;
		if (substr($insertSignature, 0, 9) == "signature") {
			$blnIsPhotoName = true;
		}
	}
	if ($usePhotoPaths && $blnIsPhotoName) {
		//Realiza la copia del archivo de imagen
		//@JAPR 2013-02-27: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
		$path = getcwd()."\\".(($useRecoveryPaths)?"syncerrors":"syncdata")."\\".trim($_SESSION["PABITAM_RepositoryName"]);
		$strCompleteSourcePath = $path."\\".$insertSignature;
		if (strpos(strtolower($strCompleteSourcePath), '.jpg') === false) {
			$strCompleteSourcePath .= '.jpg';
		}
		$status = @copy($strCompleteSourcePath, $strCompletePath);
	}
	else {
		$status = file_put_contents ($strCompletePath, $insertSignature);
	}
	
	if($status!=false)
	{
		//Procedemos a insertar la imagen de signature
		$sql = "INSERT INTO SI_SV_SurveySignatureImg (SurveyID, FactKey, PathImage) VALUES 
				(
				".$surveyInstance->SurveyID.
				", ".$factKey.
				", ".$aRepository->DataADOConnection->Quote($strFieldDocumentName).
				")";
				
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySignatureImg ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySignatureImg ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
	}
	
	return true;
}

function saveComment($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertComment)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	//Procedemos a insertar el comentario
	$sql = "INSERT INTO SI_SV_SurveyAnswerComment (SurveyID, QuestionID, MainFactKey, FactKey, StrComment) VALUES 
			(
			".$surveyInstance->SurveyID.
			", ".$aQuestion->QuestionID.
			", ".$factKey.
			", ".$factKeyDimVal.
			", ".$aRepository->DataADOConnection->Quote($insertComment).
			")";
			
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	return true;
}

function updateComment($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $insertComment)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	$sql = "SELECT StrComment FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$surveyInstance->SurveyID." 
			AND QuestionID = ".$aQuestion->QuestionID." AND FactKey = ".$factKeyDimVal." AND MainFactKey = ".$factKey;
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);

	if(!$aRS)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}

	//Si es fin de recordset entonces se inserta el comentario
	if($aRS->EOF)
	{
		//Procedemos a insertar el comentario
		$sql = "INSERT INTO SI_SV_SurveyAnswerComment (SurveyID, QuestionID, MainFactKey, FactKey, StrComment) VALUES 
				(
				".$surveyInstance->SurveyID.
				", ".$aQuestion->QuestionID.
				", ".$factKey.
				", ".$factKeyDimVal.
				", ".$aRepository->DataADOConnection->Quote($insertComment).
				")";
				
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
	}
	else 
	{
		$sql = "UPDATE SI_SV_SurveyAnswerComment SET StrComment = ".$aRepository->DataADOConnection->Quote($insertComment)." WHERE SurveyID = ".$surveyInstance->SurveyID." 
				AND QuestionID = ".$aQuestion->QuestionID." AND FactKey = ".$factKeyDimVal." AND MainFactKey = ".$factKey;
		
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
	}
	
	return true;
}

function saveActions($aRepository, $surveyInstance, $aQuestion, $factKey, $insertActions)
{
	//Procedemos a insertar el comentario
	$sql = "INSERT INTO SI_SV_SurveyQuestionActions (SurveyID, QuestionID,  FactKey, StrActions) VALUES 
			(
			".$surveyInstance->SurveyID.
			", ".$aQuestion->QuestionID.
			", ".$factKey.
			", ".$aRepository->DataADOConnection->Quote($insertActions).
			")";
			
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
}

/* Dado una encuesta, un indicador del cual se desea obtener el valor y una instancia de pregunta si es que el indicador proporcionado está ligado
a ella (de lo contrario, se considerará que debe obtener el valor del registro indicado sin mas validaciones), consulta el valor actual de este
indicador en la tabla de hechos directamente para la encuesta indicada, aplicando validaciones según si el registro definido por el $factKey, 
$factKeyDimVal, $anEntrySectionID y $anEntryCatDimID pertenece o no a un registro que aplique para la sección a la que pertenece la pregunta
especificada
En caso de error o de no encontrarse un registro, regresará NULL
*/
function getIndicatorValue($aRepository, $aSurveyInstance, $anInstanceInd, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID = 0, $anEntryCatDimID = 0) {
	$dblValue = NULL;
	
	if (is_null($factKey) || ((int)$factKey) <= 0) {
		return NULL;
	}
	
	//Determina si es o no válido el registro según la información de la pregunta
	if (!is_null($aQuestion) && is_object($aQuestion)) {
		switch ($aQuestion->SectionType) {
			case sectNormal:
				//Sólo es válida si se trata del registro único
				if ($anEntrySectionID != 0 || $anEntryCatDimID != 0) {
					return NULL;
				}
				break;
			
			case sectDynamic:
			case sectFormatted:
				//Sólo es válida si se trata de la propia sección (no se soportan preguntas tipo categoría de dimensión con otras secciones 
				//múltiples, así que basta verificar que si sea la misma sección de la pregunta)
				if ($aQuestion->SectionID != $anEntrySectionID) {
					return NULL;
				}
				break;
				
			case sectFormatted:
			default:
				//No es válida una pregunta en este tipo de secciones
				return NULL;
				break;
		}
	}
	
	$factTable = $aSurveyInstance->FactTable;
	$fieldName = $anInstanceInd->field_name.$anInstanceInd->IndicatorID;
	$sql = "SELECT {$fieldName} 
		FROM {$factTable} 
		WHERE FactKey = {$factKey}";
    $aRS = $aRepository->DataADOConnection->Execute($sql);
    if (is_null($aRS) || $aRS->EOF) {
    	return NULL;
    }
    
    $dblValue = (float) @$aRS->fields[strtolower($fieldName)];
	return $dblValue;
}

function getInsertDimMultiValueKey($aRepository, $anInstanceIndDim, $value)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;

	if($value == '') {
		$value = $gblEFormsNA;
	}

	$valueToProcess = substr($value, 0, 255);

	$tableName = $anInstanceIndDim->Dimension->TableName;
	$fieldKey = $tableName."KEY";
	$fieldDesc = $anInstanceIndDim->Dimension->FieldDescription;
	
	$sql = "SELECT ".$fieldKey.", ".$fieldDesc." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($valueToProcess);
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Considerando que es un solo valor el que se esta procesando cuando se trata de question Date y TextBox
	if (!$aRS->EOF)
	{	
		//Ya existe ese valor en la tabla de la dimension, se retorna su clave
		$valueKey =  $aRS->fields[strtolower($fieldKey)];
	}
	else 
	{
		//Se debe insertar dicho valor en la tabla de dimension
		$sql = "INSERT INTO ".$tableName." ( ".$fieldDesc." ) VALUES ( ".$aRepository->DataADOConnection->Quote($valueToProcess).")";
			
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT ".$fieldKey.", ".$fieldDesc." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($valueToProcess);
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}

		$valueKey =  $aRS->fields[strtolower($fieldKey)];
	}
	
	return $valueKey;
}

/* Dada la dimensión y un valor para buscar dentro de su tabla, regresa el key subrrogado que corresponde con dicho valor, pero en caso de no 
encontrarlo entonces lo agrega y regresa el key subrrogado que se le asignó
//@JAPR 2015-05-19: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
//estaban limitadas a 255 caracteres (#BA7866)
Agregado el parámetro $aQuestion para permitir variar la consulta a partir del tipo de pregunta para el que aplique. Si no se especifica entonces
se usarán valores default tal como se había hecho anteriormente
//@JAPR 2015-06-16: Corregido un bug, el parámetro $aQuestion no se estaba asignando con null por default, así que generaba un Warning
*/
function getInsertDimValueKey($aRepository, $anInstanceIndDim, $arrayValues, $aQuestion = null)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	
	$arrayKeyValues = array();
	//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
	if (is_null($arrayValues[0]) || trim($arrayValues[0]) == '') {
		$arrayKeyValues[0] = 1;
		return $arrayKeyValues;
	}
	//@JAPR
	$tableName = $anInstanceIndDim->Dimension->TableName;
	$fieldKey = $tableName."KEY";
	$fieldDesc = $anInstanceIndDim->Dimension->FieldDescription;
	//@JAPR 2015-05-19: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
	//estaban limitadas a 255 caracteres (#BA7866)
	$intLength = 255;
	if (!is_null($aQuestion) && is_object($aQuestion)) {
		try {
			$blnValidLength = false;
			$intMaxLength = 0;
			if (property_exists($anInstanceIndDim->Dimension, "LongDescription")) {
				$blnValidLength = true;
				$intMaxLength = $anInstanceIndDim->Dimension->LongDescription;
			}
			
			if ($blnValidLength) {
				switch ($aQuestion->QTypeID) {
					case qtpOpenString:
						$intLength = DEFAULT_TEXT_LENGTH;
						break;
					case qtpOpenAlpha:
						$intLength = $aQuestion->QLength;
						if (is_null($intLength) || !is_numeric($intLength) || (int) $intLength <= 0 || (int) $intLength > MAX_ALPHA_LENGTH) {
							if ((int) $intLength > MAX_ALPHA_LENGTH) {
								$intLength = MAX_ALPHA_LENGTH;
							}
							else {
								$intLength = DEFAULT_ALPHA_LENGTH;
							}
						}
						break;
				}
				
				//Verifica si la longitud de la pregunta no es mayor que la de la dimensión, ya que son entidades diferentes
				if ($intLength > $intMaxLength) {
					if ($intMaxLength > 0) {
						$intLength = $intMaxLength;
					}
					else {
						//En el peor de los casos, se deja la longitud original que siempre había funcionado por compatibilidad hacia atrás
						$intLength = 255;
					}
				}
			}
		} catch (Exception $e) {
			//No se reporta este error, simplemente usará el default original
		}
	}
	
	$valueToProcess = substr($arrayValues[0], 0, $intLength);
	//@JAPR
	
	$sql = "SELECT ".$fieldKey.", ".$fieldDesc." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($valueToProcess);
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Considerando que es un solo valor el que se esta procesando cuando se trata de question Date y TextBox
	if (!$aRS->EOF)
	{	
		//Ya existe ese valor en la tabla de la dimension, se retorna su clave
		$valueKey =  $aRS->fields[strtolower($fieldKey)];
	}
	else 
	{
		//Se debe insertar dicho valor en la tabla de dimension
		$sql = "INSERT INTO ".$tableName." ( ".$fieldDesc." ) VALUES ( ".$aRepository->DataADOConnection->Quote($valueToProcess).")";
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT ".$fieldKey.", ".$fieldDesc." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($valueToProcess);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		$valueKey =  $aRS->fields[strtolower($fieldKey)];
	}
	
	$arrayKeyValues[0] = $valueKey;
	
	return $arrayKeyValues;
}

//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
//A diferencia de getInsertDimValueKey, esta función recibe el $aQuestion para permitirle obtener los IDs del resto de los atributos ligados a
//la dimensión especificada, de tal forma que pueda insertar correctamente los valores en la misma en caso de no existir ya como miembro de la
//dimensión
function getInsertDimGPSValueKey($aRepository, $anInstanceIndDim, $aQuestion, $arrayValues)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	
	$arrayKeyValues = array();
	//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
	if (is_null($arrayValues[0]) || trim($arrayValues[0]) == '') {
		$arrayKeyValues[0] = 1;
		return $arrayKeyValues;
	}
	//@JAPR
	$tableName = $anInstanceIndDim->Dimension->TableName;
	$fieldKey = $tableName."KEY";
	$fieldDesc = $anInstanceIndDim->Dimension->FieldDescription;
	$valueToProcess = substr($arrayValues[0], 0, 255);
	
	$sql = "SELECT ".$fieldKey.", ".$fieldDesc." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($valueToProcess);
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Considerando que es un solo valor el que se esta procesando cuando se trata de question Date y TextBox
	if (!$aRS->EOF)
	{	
		//Ya existe ese valor en la tabla de la dimension, se retorna su clave
		$valueKey =  $aRS->fields[strtolower($fieldKey)];
	}
	else 
	{
		//Se debe insertar dicho valor en la tabla de dimension, pero adicionalmente insertará los atributos de GPS ligados
		$strAdditionalFields = '';
		$strAdditionalValues = '';
		if (!is_null($aQuestion)) {
			//Primero desgloza el valor del GPS en sus componentes
			$arrGPSData = SplitGPSData($valueToProcess);
			$strLatitude = $aRepository->DataADOConnection->Quote($arrGPSData[0]);
			$strLongitude = $aRepository->DataADOConnection->Quote($arrGPSData[1]);
			$strAccuracy = $aRepository->DataADOConnection->Quote($arrGPSData[2]);
			
			//Verifica cual de los atributos está asignado (deberían estar todos, si no está alguno, entonces hace falta editar la pregunta)
			if ($aQuestion->LatitudeAttribID > 0) {
				$strAdditionalFields .= ', DSC_'.$aQuestion->LatitudeAttribID;
				$strAdditionalValues .= ', '.$strLatitude;
			}
			if ($aQuestion->LongitudeAttribID > 0) {
				$strAdditionalFields .= ', DSC_'.$aQuestion->LongitudeAttribID;
				$strAdditionalValues .= ', '.$strLongitude;
			}
			if ($aQuestion->AccuracyAttribID > 0) {
				$strAdditionalFields .= ', DSC_'.$aQuestion->AccuracyAttribID;
				$strAdditionalValues .= ', '.$strAccuracy;
			}
		}
		
		$sql = "INSERT INTO ".$tableName." ( ".$fieldDesc." {$strAdditionalFields} ) VALUES ( ".$aRepository->DataADOConnection->Quote($valueToProcess)." {$strAdditionalValues})";
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT ".$fieldKey.", ".$fieldDesc." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($valueToProcess);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		$valueKey =  $aRS->fields[strtolower($fieldKey)];
	}
	
	$arrayKeyValues[0] = $valueKey;
	
	return $arrayKeyValues;
}
//@JAPR

//@JAPR 2015-01-26: Corregido un bug, las preguntas tipo Ranking dentro de secciones dinámicas no estaban grabando la posición de selección
//sino sólo el estado, así que el método usado para obtener el key subrrogado asumía sólo dos estados posibles y fallaba al insertar nuevos.
//Agregado el parámetro $aQuestion (#4ZQ66A)
/* Obtiene el key subrrogado de una pregunta múltiple choice cuando se encuentra dentro de una sección dinámica y es tipo CheckBox, en este caso
sólo debería haber uno de dos posibles valores (Marcada/Desmarcada) excepto si está como despliegue Ranking, en ese caso se habilitará el grabado
de mas valores que corresponden con la posición de la selección
*/
function getDimValueForMChoiceCheck($aRepository, $anInstanceIndDim, $arrayValues, $aQuestion)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	$arrayKeyValues = array();

	$tableName = $anInstanceIndDim->Dimension->TableName;
	$fieldKey = $tableName."KEY";
	$fieldDesc = $anInstanceIndDim->Dimension->FieldDescription;
	$checkValue = (int)$arrayValues[0];
	
	$sql = "SELECT ".$fieldKey.", ".$fieldDesc." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($checkValue);
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Considerando que es un solo valor el que se esta procesando cuando se trata de question Date y TextBox
	if (!$aRS->EOF)
	{	
		//Ya existe ese valor en la tabla de la dimension, se retorna su clave
		$valueKey =  $aRS->fields[strtolower($fieldKey)];
	}
	else 
	{
		//@JAPR 2015-01-26: Corregido un bug, las preguntas tipo Ranking dentro de secciones dinámicas no estaban grabando la posición de selección
		//sino sólo el estado, así que el método usado para obtener el key subrrogado asumía sólo dos estados posibles y fallaba al insertar nuevos.
		//Agregado el parámetro $aQuestion (#4ZQ66A)
		//Si la pregunta está configurada como Ranking, no debe forzar a utilizar uno de los estados ya que el valor recibido corresponde con
		//la posición de la selección dentro del total, así que se utiliza el siguiente key subrrogado existente
		$blnIsRanking = false;
		if (!is_null($aQuestion)) {
			if ($aQuestion->QTypeID == qtpMulti && $aQuestion->QDisplayMode == dspLabelnum) {
				$blnIsRanking = true;
			}
		}
		
		if ($blnIsRanking) {
			$sql = "INSERT INTO ".$tableName." ( ".$fieldDesc." ) VALUES ( ".$aRepository->DataADOConnection->Quote($checkValue).")";
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					return false;
				}
			}
			
			$sql = "SELECT ".$fieldKey.", ".$fieldDesc." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($checkValue);
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					return false;
				}
			}
			
			$valueKey =  (int) @$aRS->fields[strtolower($fieldKey)];
			//Validación final, aunque no se debería dar el caso, pero si no pudo insertarse entonces regresa 0 (no marcado)
			if ($valueKey <= 0) {
				$valueKey = 3;
			}
		}
		else {
			if($checkValue==0)
			{
				$valueKey = 3;
			}
			else 
			{
				$valueKey = 2;
			}
			
			//Se debe insertar dicho valor en la tabla de dimension
			$sql = "INSERT INTO ".$tableName." ( ".$fieldKey.", ".$fieldDesc." ) VALUES ( ".$valueKey.", ".$aRepository->DataADOConnection->Quote($checkValue).")";
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					return false;
				}
			}
		}
		//@JAPR
	}
	
	$arrayKeyValues[0] = $valueKey;
	
	return $arrayKeyValues;
}

function getKeyMultiValues($aRepository, $anInstanceMultiDim, $aQuestion, $arrayValues)
{
	$arrayKeyValues = array();
	$arrayValuesFlip = array_flip($arrayValues);
	
	//$notSelectValue = 1;
	$notSelectValue = 3;
	//Determinar que valor asignar cuando no vienen seleccionados algunos valores
	//o bien no se seleccion ninguno
	if(count($arrayValues)>0)
	{
		$notSelectValue = 3;
	}

	//Obtenemos los valores de las opciones
	$selectOptions = $aQuestion->SelectOptions;
	
	foreach ($selectOptions as $optionKey=>$optionValue)
	{
		if(isset($arrayValuesFlip[$optionValue]))
		{
			$arrayKeyValues[$optionKey] = 2;
		}
		else 
		{
			$arrayKeyValues[$optionKey] = $notSelectValue;
		}
	}

	return $arrayKeyValues;
}

//@JAPR 2014-10-29: Agregado el mapeo de Scores
/* Regresa el array con los valores de score para cada opción de respuesta de las preguntas múltiple choice para permitir procesar durante el grabado
el valor total del score según la selección realizada durante la captura
//Probablemente esta función debería cambiar para no utilizar los scores de la definición actual de cada opción de respuesta, sino que deba usar
la lista de Scores recibida desde el App, ya que esos son los scores usados durante la captura
*/
function getScoreMultiValues($aRepository, $anInstanceMChoiceIndicator, $aQuestion, $existMChoiceIndicators, $arrayValues, $bUseNullDefault = false)
{
	$arrayValuesToReturn = array();
	$arrayValuesFlip = array_flip($arrayValues);

	//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
	$valueByDefault = (($bUseNullDefault)?null:(float)0);
	//@JAPR

	//Obtenemos los valores de las opciones y de los scores
	$selectOptions = $aQuestion->SelectOptions;
	$arrayScores = $aQuestion->Scores;
	
	foreach ($selectOptions as $optionKey=>$optionValue)
	{
		if($existMChoiceIndicators==true)
		{
			//Si el valor se encuentra entre los seleccionados entonces se envia el score correspondiente
			if(isset($arrayValuesFlip[$optionValue]))
			{
				$scoreValue = (float)$arrayScores[$optionKey];
				$arrayValuesToReturn[$optionKey] = $scoreValue;
			}
			else 
			{
				$arrayValuesToReturn[$optionKey] = $valueByDefault;
			}
		}
		else 
		{
			$arrayValuesToReturn[$optionKey] = $valueByDefault;
		}
	}

	return $arrayValuesToReturn;
}

/* Dada la dimensión y un valor para buscar dentro de su tabla, regresa el key subrrogado que corresponde con dicho valor, pero en caso de no 
encontrarlo entonces simplemente regresa el key subrrogado del valor de *NA. El parámetro del valor puede ser un array de hecho, pero la función
no está preparada para consultar individualmente cada valor sino que realiza un IN, así que con algún valor que si encuentre ya regresaría el key
subrrogado para él, incluso si los demás no existieran en la tabla
*/
function getDimValueKeys($aRepository, $anInstanceIndDim, $arrayValues)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	$arrayKeyValues = array();

	$tableName = $anInstanceIndDim->Dimension->TableName;
	$fieldKey = $tableName."KEY";
	$fieldDesc = $anInstanceIndDim->Dimension->FieldDescription;
	
	$strValues = "";
	$count = 0;
	foreach ($arrayValues as $value)
	{
		if($count!=0)
		{
			$strValues.=", ";
		}
		
		$strValues.=$aRepository->DataADOConnection->Quote($value);
		$count++;
	}

	$sql = "SELECT ".$fieldKey.", ".$fieldDesc." FROM ".$tableName." WHERE ".$fieldDesc." IN (".$strValues.") ORDER BY ".$fieldKey;
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	if(!$aRS->EOF)
	{
		$m=0;
		while(!$aRS->EOF)
		{	
			//Ya existe esa fecha en la tabla especificada en dim_periodo, se retorna su clave
			$valueKey =  $aRS->fields[strtolower($fieldKey)];
			$arrayKeyValues[$m] = $valueKey;
			
			$m++;
			$aRS->MoveNext();
		}
	}
	else 
	{
		//Como no encontró un valor valido en los campos de seleccion simple o multiple
		//Entonces se retorna el valor de No Aplica
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT ".$fieldKey.", ".$fieldDesc." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}

		$valueKey =  $aRS->fields[strtolower($fieldKey)];
		$arrayKeyValues[0] = $valueKey;
	}
	
	return $arrayKeyValues;
}

//Según el tipo de pregunta (agrupado en forma diferente para las que usan catálogos, las que son selección sencilla o múltiple con respuestas
//manuales, o bien las preguntas abiertas) obtiene la llave subrrogada de su tabla de dimensión para el valor indicado en $arrayValues, y en
//caso de no existir lo agrega a la dimensión correspondiente como un nuevo miembro (sólo en el caso de las abiertas, en los otros regrea
//la llave subrrogada del valor de No Aplica)
//Aunque $arrayValues es un array, realmente la función sólo utiliza el primer valor del mismo
function getKeyValues($aRepository, $anInstanceIndDim, $aQuestion, $arrayValues)
{
	$arrayKeyValues = array();
	
	//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
	if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
	{
		$arrayKeyValues = getInsertDimValueKey($aRepository, $anInstanceIndDim, $arrayValues);
	}
	//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
	elseif ($aQuestion->QTypeID == qtpGPS) {
		//En este caso la dimensión que se recibe es la propia de la pregunta, la cual debe grabar en formato "Latitude, Longitude (Accuracy)", 
		//sin embargo hay otras 3 dimensiones-atributo para cada uno de los elementos posibles así que se invoca a un método diferente que sólo 
		//regresará el Key subrroagdo de la combinación mencionada arriba, pero grabará los demás atributos
		$arrayKeyValues = getInsertDimGPSValueKey($aRepository, $anInstanceIndDim, $aQuestion, $arrayValues);
	}
	//@JAPR
	else 
	{
		//@JAPR 2012-05-17: Agregadas las preguntas tipo Action
		//@JAPR 2012-09-25: Agregado el tipo de pregunta Hora
		//@JAPR 2014-03-28: Corregido un bug, si la pregunta es simple choice con opción de otro, se debe agregar el valor a la dimensión en caso
		//de no encontrarlo
		//@AAL 06/05/2015: modificado para soportar preguntas tipo código de barras
		if($aQuestion->QTypeID==qtpOpenDate || $aQuestion->QTypeID==qtpOpenString || $aQuestion->QTypeID==qtpOpenAlpha || $aQuestion->QTypeID == qtpAction || $aQuestion->QTypeID==qtpOpenTime || $aQuestion->QTypeID==qtpBarCode || ($aQuestion->QTypeID==qtpSingle && $aQuestion->AllowAdditionalAnswers))
		{
			//@JAPR 2015-05-19: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
			//estaban limitadas a 255 caracteres (#BA7866)
			$objQuestion = null;
			if ($aQuestion->QTypeID==qtpOpenString || $aQuestion->QTypeID==qtpOpenAlpha) {
				$objQuestion = $aQuestion;
			}
			$arrayKeyValues = getInsertDimValueKey($aRepository, $anInstanceIndDim, $arrayValues, $objQuestion);
			//@JAPR
		}
		else if(($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==0) || $aQuestion->QTypeID==qtpMulti || $aQuestion->QTypeID==qtpCallList)
		{ //conchita agregado calllist
			$arrayKeyValues = getDimValueKeys($aRepository, $anInstanceIndDim, $arrayValues);
		}
		else if ($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1)
		{
			$arrayKeyValues = getCatValueKeys($aRepository, $aQuestion, $arrayValues);
		}
	}
	
	return $arrayKeyValues;
}

//@JAPR 2014-10-29: Agregado el mapeo de Scores
/* Regresa el valore de score para la opción de respuesta de la pregunta simple choice que fue seleccionada
//Probablemente esta función debería cambiar para no utilizar los scores de la definición actual de cada opción de respuesta, sino que deba usar
la lista de Scores recibida desde el App, ya que esos son los scores usados durante la captura
*/
function getScoreValues($aRepository, $aQuestion, $arrayValues, $bUseNullDefault = false)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	
	$arrayScoreValues = array();
	//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
	if (is_null($arrayValues[0]) || trim($arrayValues[0]) == '') {
		$arrayScoreValues[0] = null;
		return $arrayScoreValues;
	}
	$scoreValue = (($bUseNullDefault)?null:(float)0);
	//@JAPR
	
	if($aQuestion->QTypeID==qtpSingle)
	{
		$sql = "SELECT Score FROM SI_SV_QAnswers WHERE QuestionID = ".$aQuestion->QuestionID." AND DisplayText = ".$aRepository->DataADOConnection->Quote($arrayValues[0]);
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}

		if (!$aRS->EOF)
		{	
			$score = $aRS->fields["score"];
			
			if($score!="")
			{
				$scoreValue =  (float)$score;
			}
			else 
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$scoreValue = (($bUseNullDefault)?null:(float)0);
				//@JAPR
			}
		}
	}
	
	$arrayScoreValues[0] = $scoreValue;
	
	return $arrayScoreValues;
}

function getScoreValuesFromMatrix($aRepository, $aQuestion, $value, $firstScore, $operator)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	$scoreValue = (float)0;
	
	if($aQuestion->QTypeID==qtpSingle && $aQuestion->QDisplayMode==dspMatrix)
	{
		if($aQuestion->OneChoicePer==0)
		{
			$tableName = "SI_SV_QAnswersCol";
			$strFields = "Score, ScoreOperation";
		}
		else 
		{
			$tableName = "SI_SV_QAnswers";
			$strFields = "Score";
		}
		
		$sql = "SELECT ".$strFields." FROM ".$tableName." WHERE QuestionID = ".$aQuestion->QuestionID." AND DisplayText = ".$aRepository->DataADOConnection->Quote($value);
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}

		if (!$aRS->EOF)
		{	
			if($aQuestion->OneChoicePer==0)
			{
				$operator = (int)$aRS->fields["scoreoperation"];
			}
			
			if(!is_null($operator))
			{
				$score = $aRS->fields["score"];
				
				if($score!="")
				{
					$secondScore =  (float)$score;
				}
				else 
				{
					$secondScore = (float)0;
				}
				
				if($operator==0)
				{
					$scoreValue = ((float)$firstScore) + ((float)$secondScore);
				}
				else 
				{
					$scoreValue = ((float)$firstScore) * ((float)$secondScore);
				}
			}
			else 
			{
				$scoreValue = (float)0;
			}
		}
	}
	
	return $scoreValue;
}

//@JAPR 2015-01-26: Corregido un bug, las preguntas tipo Ranking dentro de secciones dinámicas no estaban grabando la posición de selección
//sino sólo el estado, así que el método usado para obtener el key subrrogado asumía sólo dos estados posibles y fallaba al insertar nuevos.
//Agregado el parámetro $aQuestion (#4ZQ66A)
/* Obtiene el key subrrogado de una pregunta múltiple choice cuando se encuentra dentro de una sección dinámica, ya que en este caso si
existe una única dimensión para la pregunta. Varía según si la pregunta es o no tipo CheckBox
*/
function getKeyValuesForMC($aRepository, $anInstanceIndDim, $aQuestion, $arrayValues)
{
	$arrayKeyValues = array();
	
	if($aQuestion->MCInputType>mpcCheckBox)
	{
		//Los valores de múltiple choice con capturas de Texto deben grabar el Texto capturado
		$arrayKeyValues = getInsertDimValueKey($aRepository, $anInstanceIndDim, $arrayValues);
	}
	else 
	{
		//Los valores de múltiple choice sin capturas deben grabar el estado del CheckBox correspondiente
		//@JAPR 2015-01-26: Corregido un bug, las preguntas tipo Ranking dentro de secciones dinámicas no estaban grabando la posición de selección
		//sino sólo el estado, así que el método usado para obtener el key subrrogado asumía sólo dos estados posibles y fallaba al insertar nuevos.
		//Agregado el parámetro $aQuestion (#4ZQ66A)
		$arrayKeyValues = getDimValueForMChoiceCheck($aRepository, $anInstanceIndDim, $arrayValues, $aQuestion);
	}
	
	return $arrayKeyValues;
}

//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
//Agregado el parámetro &$arrFieldValues para regresar las descripciones de los elementos que fueron recibidos, ya que este método obtiene el 
//campo Key que es lo que finalmente graba
/*Este método recibe un valor de pregunta SimpleChoice de catálogo del tipo:
		key_10001_SVSep_HOU R1_SVElem_key_10002_SVSep_El Ahorro_SVElem_key_10003_SVSep_El Ahorro #23_SVElem_key_10004_SVSep_2211 Southmore Avenue_SVElem_key_10005_SVSep_HOUSTON_SVElem_key_10006_SVSep_TX_SVElem_key_10007_SVSep_77502_SVElem_key_10068_SVSep__SVElem_key_10069_SVSep_
y busca en la tabla del catálogo correspondiente la combinación que contenga a todos los atributos involucrados, regresando el Key subrrogado. Si
no se encuentra el valor entonces se regresa NA
*/
function getCatValueKeys($aRepository, $aQuestion, $arrayValues, &$arrFieldValues = null)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	$arrayKeyValues = array();
	
	//Sino trae algun contenido el valor del catalogo, eso quiere decir,
	//que es una pregunta omitida por una regla o que hay un detalle 
	//en el html de ese catalogo que no permite regresar valores
	//por lo tanto se regresa el valor de no aplica
	if(trim($arrayValues[0])=="")
	{
		$arrayKeyValues[0] = 1;
		
		//Vamos a almacenar en un log los filtros que no regresen valor valido incluso los que estan vacios
		$catlogfile = "./log/InvalidCatValues.log";
		$friendlyDate = date('Y-m-d H:i:s');
		$postFieldName = "qfield".$aQuestion->QuestionID;
		$lastDate = substr($_POST["surveyDate"], 0, 10)." ".substr($_POST["surveyHour"], 0, 8);
		$straction = "[".$friendlyDate."] [Error: Get Valid Cat Values ] [UserID: ".$_SESSION["PAtheUserID"]."] [UserName: ".$_SESSION["PABITAM_UserName"]."] [Repository: ".$aRepository->RepositoryName."] [SurveyID: ".(int) @$_POST["surveyID"]."] [SurveyDate: ".$lastDate."] [QuestionID: ".$aQuestion->QuestionID."] [Filtro: ".(string) @$arrayValues[0]."] [Valor filtro POST: ".(string) @$_POST[$postFieldName]."]\r\n";
		@error_log($straction, 3, $catlogfile);
		//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		if (!is_null($arrFieldValues) && is_array($arrFieldValues))
		{
			//@JAPR 2012-06-27: Corregido un bug, este array debe indexarse por QuestionNumber, no por QuestionID
			$arrFieldValues["qfield".$aQuestion->QuestionNumber] = "NA";
		}
		//@JAPR

		return $arrayKeyValues;
	}

	//@JAPR 2013-02-12: Agregada la edición de datos históricos
	//Si el valor del catálogo fuera el texto NOCHANGE, querría decir que se debe dejar el mismo valor que se estaba editando ya que no fué
	//cambiado. Esto sólo aplica si el catálogo usa dimensiones independientes, ya que de otra manera tiene que haber estado ligado a un key
	//subrrogado, además sólo podría presentarse este caso durante la edición, en cualquier otra situación algo como esto sería un error así
	//que se regresa el valor de NA de todas maneras
	if(trim($arrayValues[0])=="NOCHANGE") {
		$arrayKeyValues[0] = 1;
		return $arrayKeyValues;
	}
	//@JAPR
	
	$arrayCatPairValues = explode('_SVElem_', $arrayValues[0]);
	$arrayCatClaDescrip = array();
	$arrayCatValues = array();
	$arrayTemp = array();
	$arrayInfo = array();
	
	foreach($arrayCatPairValues as $element)
	{
		$arrayInfo = explode('_SVSep_', $element);
		
		//Obtener el cla_descrip y almacenar (key_1178)
		$arrayTemp = explode('_', $arrayInfo[0]);
		$arrayCatClaDescrip[] = $arrayTemp[1];
		
		//Obtener el valor del catalogo
		$arrayCatValues[] = $arrayInfo[1];
	}
	
	//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
	$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aQuestion->CatalogID);
	if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
		//Por default prepara el valor de *NA por si no se pueden extraer los datos
		$arrayKeyValues[0] = 1;
		if (!is_null($arrFieldValues) && is_array($arrFieldValues))
		{
			//@JAPR 2012-06-27: Corregido un bug, este array debe indexarse por QuestionNumber, no por QuestionID
			$arrFieldValues["qfield".$aQuestion->QuestionNumber] = "NA";
		}
		
		//En este caso se trata de un catálogo mapeado desde eBavel, en lugar de reutilizar BITAMReportCollection como se hace en catálogos propios, se
		//utilizará la funcionalidad de catalogFilter, donde ya se había implementado el método casi idéntico a lo requerido aqui, así de esa forma no
		//se duplicará código como sucedió con BITAMReportCollection, ya que el mismo tipo de consulta en catálogos normales también estaba en catalogFilter
		$intMemberParentID = 0;
		$aCatMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $aQuestion->CatMemberID);
		if (!is_null($aCatMember)) {
			$intMemberParentID = $aCatMember->ClaDescrip;
			//En este caso si hay un atributo para la pregunta, así que obtenemos los valores aplicando filtros hasta su posición en el catálogo
			$catMemberOrder = $aCatMember->MemberOrder;
			$aCatalogFilter = BITAMCatalogFilter::NewInstance($aRepository, $aQuestion->CatalogID);
			if (!is_null($aCatalogFilter) && is_object($aCatalogFilter)) {
				foreach($arrayCatClaDescrip as $index=>$element) {
					$strAttributeFieldName = "DSC_".$element;
					$aCatalogFilter->$strAttributeFieldName = $arrayCatValues[$index];
				}
				
				$aRS = $aCatalogFilter->getCatalogValues($catMemberOrder-1, true, true, true);
				//@JAPR 2014-05-20: Agregada validación para controlar cuando eBavel no pueda regresar el RecordSet
				if ($aRS && is_object($aRS) && property_exists($aRS, 'EOF') && !$aRS->EOF) {
					$fieldKey = "id_".$aCatalogFilter->FormID;
					$valueKey = (int)@$aRS->fields[strtolower($fieldKey)];
					if ($valueKey <= 0) {
						$valueKey = 1;
					}
					$arrayKeyValues[0] = $valueKey;
					//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
					if (!is_null($arrFieldValues) && is_array($arrFieldValues) && $intMemberParentID > 0)
					{
						//@JAPR 2012-06-20: Modificado para que los parámetros para reemplazar las respuestas dentro de las Acciones de eBavel especifiquen el numero
						//de atributo del catálogo para el caso de las Simple Choice de catálogo
						//$arrFieldValues[$postFieldName] = (string) @$aRS->fields["questionvalue"];
						//@JAPR 2012-06-27: Corregido un bug, este array debe indexarse por QuestionNumber, no por QuestionID
						$arrFieldValues["qfield".$aQuestion->QuestionNumber] = $arrayCatValues;
						//@JAPR
					}
					//@JAPR
				}
			}
		}
		
		return $arrayKeyValues;
	}
	//@JAPR
	
	//Obtenemos cual es el cla_descrip de la dimension principal dummy
	$sql = "SELECT ParentID FROM SI_SV_Catalog WHERE CatalogID = ".$aQuestion->CatalogID;
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if (!$aRS || $aRS->EOF)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}

	$parentCladescrip = (int)$aRS->fields["parentid"];
	//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
	$intMemberParentID = 0;
	$aCatMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $aQuestion->CatMemberID);
	if (!is_null($aCatMember))
	{
		$intMemberParentID = $aCatMember->ClaDescrip;
	}
	$sql = "SELECT RIDIM_".$parentCladescrip."KEY ".(($intMemberParentID > 0)?(", DSC_".$intMemberParentID." AS QuestionValue "):"")." FROM RIDIM_".$parentCladescrip;
	//@JAPR

	if(count($arrayCatClaDescrip)>0)
	{
		$sql.=" WHERE ";
	}
	
	$whereElements = "";
	foreach($arrayCatClaDescrip as $index=>$element)
	{
		if($whereElements!="")
		{
			$whereElements.=" AND ";
		}
		$whereElements.="DSC_".$element." = ".$aRepository->DataADOConnection->Quote($arrayCatValues[$index]);
	}
	
	$sql.=$whereElements." ORDER BY RIDIM_".$parentCladescrip."KEY";
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if (!$aRS)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." RIDIM_".$parentCladescrip." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." RIDIM_".$parentCladescrip." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	if(!$aRS->EOF)
	{
		$fieldKey = "RIDIM_".$parentCladescrip."KEY";
		
		$valueKey = (int)$aRS->fields[strtolower($fieldKey)];
		$arrayKeyValues[0] = $valueKey;
		//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		if (!is_null($arrFieldValues) && is_array($arrFieldValues) && $intMemberParentID > 0)
		{
			//@JAPR 2012-06-20: Modificado para que los parámetros para reemplazar las respuestas dentro de las Acciones de eBavel especifiquen el numero
			//de atributo del catálogo para el caso de las Simple Choice de catálogo
			//$arrFieldValues[$postFieldName] = (string) @$aRS->fields["questionvalue"];
			//@JAPR 2012-06-27: Corregido un bug, este array debe indexarse por QuestionNumber, no por QuestionID
			$arrFieldValues["qfield".$aQuestion->QuestionNumber] = $arrayCatValues;
			//@JAPR
		}
		//@JAPR
	}
	else 
	{
		//Como no encontró un valor valido en los campos de seleccion simple CON CATALOGO
		//Entonces se retorna el valor de No Aplica el cual tiene el VALOR DE 1
		$arrayKeyValues[0] = 1;
		
		//Vamos a almacenar en un log los filtros que no regresen valor valido incluso los que estan vacios
		$catlogfile = "./log/InvalidCatValues.log";
		$friendlyDate = date('Y-m-d H:i:s');
		$postFieldName = "qfield".$aQuestion->QuestionID;
		$lastDate = substr($_POST["surveyDate"], 0, 10)." ".substr($_POST["surveyHour"], 0, 8);
		$straction = "[".$friendlyDate."] [Error: Get Valid Cat Values ] [UserID: ".$_SESSION["PAtheUserID"]."] [UserName: ".$_SESSION["PABITAM_UserName"]."] [Repository: ".$aRepository->RepositoryName."] [SurveyID: ".$_POST["surveyID"]."] [SurveyDate: ".$lastDate."] [QuestionID: ".$aQuestion->QuestionID."] [Filtro: ".$arrayValues[0]."] [SQL: ".$sql."] [Valor filtro POST: ".(string) @$_POST[$postFieldName]."]\r\n";
		@error_log($straction, 3, $catlogfile);
		
		//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		if (!is_null($arrFieldValues) && is_array($arrFieldValues))
		{
			//@JAPR 2012-06-27: Corregido un bug, este array debe indexarse por QuestionNumber, no por QuestionID
			$arrFieldValues["qfield".$aQuestion->QuestionNumber] = "NA";
		}
		//@JAPR
	}
	
	return $arrayKeyValues;
}

//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
//Dado un key subrrogado del catálogo (que debería ser válido, de lo contrario regresará NA) se regresa un valor que corresponde al mismo formato 
//que graban las preguntas simple choice de catálogo desde el App, el cual es usado para los campos DynamicOptionDSC y DynamicValue que corresponden
//a los registros de las secciones dinámicas cuando se usan catálogos desasociados de sus dimensiones
//Para generar el valor se usará el array global pregenerado que contiene a todos los atributos a utilizar hasta el nivel de la sección dinámica,
//a menos que se especifique el parámetro $aMaxMemberID > 0, en cuyo caso se regresaría el valor con todos los atributos del
//catálogo indicado pero hasta el nivel del atributo especificado
//El array resultante contiene 2 valores, el índice 0 es el string concatenado de todos los atributos, el índice 1 es el valor específico del
//último atributo, el cual corresponde ya sea a la página o al checkbox dinámico dependiendo de si hay o no multiple-choice en la dinámica
//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
//Agregado el parámetro $aDynamicRecNum para indicar el número de registro de la sección dinámica del que se trata. Si se manda un valor < 0
//significa que no es un registro de la sección dinámica, por lo tanto funcionará como antes. Cuando si es un registro de la dinámica, en dado
//caso de recibir el valor de NA, lo que se hace es ir en busca del valor pero al array de descripciones de la dinámica que se tuvo que haber
//recibido y se usan tales descripciones para grabar los valores, en lugar de usar el key subrrogado para asignar solo valores de NA
//Agregado el parámetro $aCatalogValue con el valor seleccionado de una combinación de catálogo en el formato que se recibe para las preguntas
//simple choice de catálogo, el cual en caso de recibirse será el que se utilice para generar el array de resultado de la función, siempre
//y cuando no se reciba también el parámetro $aDynamicRecNum pues en ese caso se da preferencia a obtener el valor de la sección dinámica (tal
//como este otro parámetro, sólo aplica si el key subrrogado del parámetro $arrayValues es el de NA)
//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
//Agregado el parámetro $bReturnClaDescrip para indicar si el resultado deberá usar el ID de dimensión CLA_DESCRIP en lugar del IndDimID
//Agregado el parámetro $bReturnEmpty para indicar si en caso de no encontrar el valor se debe regresar vacio (true) o *NA (false)
//@JAPR 2014-05-29: Agregado el tipo de sección Inline
//Agregado el parámetro $aSectionID, si se especifica se asumirá que NO se trata de una sección dinámica (ya que sólo hay una) sino que se trata
//de una sección Inline (a la fecha de implementación son las únicas que usarían este parámetro) y se recibe este ID para saber a cual sección
//específicamente se está haciendo referencia, para permitir accesar a sus Arrays cuando $aDynamicRecNum != -1 (en ese caso se usan arrays diferentes)
function getCatValueDescForSurveyField($aRepository, $arrayValues, $aCatalogID, $aMaxMemberID = -1, $aDynamicRecNum = -1, $aCatalogValue = '', $bReturnClaDescrip = 0, $bReturnEmpty = 0, $aSectionID = -1) {
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	global $arrDynamicCatalogAttributes;
	//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	//global $arrSectionDescByID;
	global $arrSectionDescStrByID;
	//@JAPR 2014-05-29: Agregado el tipo de sección Inline
	global $arrInlineCatalogAttributes;
	//@JAPR
	
	$arrayKeyValues = array(0 => '', 1 => '');
	if ($aCatalogID <= 0) {
		return $arrayKeyValues;
	}
	
	if ($aMaxMemberID > 0) {
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
		if (is_null($objCatalogMembersColl)) {
			return $arrayKeyValues;
		}
		
		$arrAttributesColl = array();
		foreach ($objCatalogMembersColl as $objCatalogMember) {
			$arrAttributesColl[$objCatalogMember->MemberID] = array($objCatalogMember->ClaDescrip, $objCatalogMember->IndDimID);
			if ($objCatalogMember->MemberID == $aMaxMemberID) {
				break;
			}
		}
	}
	else {
		//@JAPR 2014-05-29: Agregado el tipo de sección Inline
		//Agregado el parámetro $aSectionID, si se especifica se asumirá que NO se trata de una sección dinámica (ya que sólo hay una) sino que se trata
		//de una sección Inline (a la fecha de implementación son las únicas que usarían este parámetro) y se recibe este ID para saber a cual sección
		//específicamente se está haciendo referencia, para permitir accesar a sus Arrays cuando $aDynamicRecNum != -1 (en ese caso se usan arrays diferentes)
		if ($aSectionID > 0) {
			$arrAttributesColl = $arrInlineCatalogAttributes[$aSectionID];
		}
		else {
			$aSectionID = -1;
			$arrAttributesColl = $arrDynamicCatalogAttributes[$aSectionID];
		}
		//@JAPR
	}
	
	//No se supone que se deba invocar a este método si no hay un valor (que sería el SectionValue), así que no habría por qué validar un caso
	//donde no se pida un valor, sin embargo para consistencia se asumirá que si se presenta ese caso entonces se regresará el valor de NA
	$intSectionValue = (int) trim(@$arrayValues[0]);
	if($intSectionValue <= 0) {
		$intSectionValue = 1;
	}
	
	//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	//Si es el registro de NA, se verifica si se especificó un valor ya sea dinámico o directo de respuesta y en ese caso se utiliza dicho valor
	//para generar el array, sólo si no hubiera tal valor entonces carga el NA directo del catálogo
	if ($intSectionValue == 1 && (trim($aCatalogValue) != '' || $aDynamicRecNum >= 0)) {
		$blnUseValue = false;
		$strQuestionCatalogValue = '';
		if ($aDynamicRecNum >= 0) {
			//Si se especificó un registro dinámico, verifica si se tiene el array de descripciones para esta combinación
			if (isset($arrSectionDescStrByID) && is_array($arrSectionDescStrByID)) {
				//A la fecha de esta implementación solo podía haber una sección dinámica, por lo tanto se usará el primer índice que se
				//encuentre en este array, si eventualmente se pudieran agregar varias de estas secciones entonces se tendría que recibir
				//como parámetro la sección específica a usar
				$dynamicSectionID = -1;
				foreach ($arrSectionDescStrByID as $intID => $arrSectionDesc) {
					//@JAPR 2014-05-29: Agregado el tipo de sección Inline
					//Agregado el parámetro $aSectionID, si se especifica se asumirá que NO se trata de una sección dinámica (ya que sólo hay una) sino que se trata
					//de una sección Inline (a la fecha de implementación son las únicas que usarían este parámetro) y se recibe este ID para saber a cual sección
					//específicamente se está haciendo referencia, para permitir accesar a sus Arrays cuando $aDynamicRecNum != -1 (en ese caso se usan arrays diferentes)
					//A partir de esta modificación, este array contendrá índices por cada sección Inline o Dinámica, sin embargo en esta función
					//no conocemos el ID de la sección dinámica, así que se pedirá específicamente el índice default -1 como el de la dinámica, ya
					//que así se va a generar el array con dicho elemento adicional
					if ($aSectionID == $intID) {
						$dynamicSectionID = $intID;
						break;
					}
				}
				
				if (isset($arrSectionDescStrByID[$dynamicSectionID])) {
					$arrSectionDesc = $arrSectionDescStrByID[$dynamicSectionID];
					if (isset($arrSectionDesc[$aDynamicRecNum])) {
						$strQuestionCatalogValue = (string) @$arrSectionDesc[$aDynamicRecNum];
						if (trim($strQuestionCatalogValue) != '') {
							$blnUseValue = true;
						}
					}
				}
			}
		}
		else {
			$strQuestionCatalogValue = $aCatalogValue;
			if (trim($strQuestionCatalogValue) != '') {
				$blnUseValue = true;
			}
		}
		
		if ($blnUseValue) {
			//Utiliza el string con el valor recibido para llenar los atributos correspondientes del array de resultado. Aunque el array
			//resultante tendrá un string con el mismo formato que este recibido, se tiene que hacer la conversión porque el String recibido
			//utiliza los CLA_DESCRIP de las dimensiones originales del catálogo mientras que el string resultante usa los de las dimensiones
			//independientes del catálogo
			$arrayCatPairValues = explode('_SVElem_', $strQuestionCatalogValue);
			$arrayCatClaDescrip = array();
			$arrayCatValues = array();
			$arrayTemp = array();
			$arrayInfo = array();
			
			foreach($arrayCatPairValues as $element)
			{
				$arrayInfo = @explode('_SVSep_', $element);
				
				//Obtener el cla_descrip y almacenar (key_1178)
				$arrayTemp = @explode('_', $arrayInfo[0]);
				$intClaDescrip = (int) @$arrayTemp[1];
				$arrayCatClaDescrip[] = $intClaDescrip;
				
				//Obtener el valor del catalogo
				$arrayCatValues[$intClaDescrip] = @$arrayInfo[1];
			}
			
			//El String que viene como respuesta pudiera tener menos atributos de los que el catálogo tiene actualmente, si fuera el caso entonces
			//los atributos que no trae se graban con el valor de NA. No se debería dar nunca la situación en la que el atributo que no hace
			//match sea precisamente el pedido a esta función, pero aun en ese caso se regresa también NA
			$strValue = '';
			$arrCatMemberValues = array();
			foreach ($arrAttributesColl as $intMemberID => $arrDescrips) {
				$intMemberClaDescrip = $arrDescrips[0];
				$strValue = @$arrayCatValues[$intMemberClaDescrip];
				if (is_null($strValue)) {
					$strValue = "*NA";
				}
				else {
					$strValue = (string) $strValue;
				}
				
				//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				$intDimensionKey = $arrDescrips[1];
				if ($bReturnClaDescrip) {
					$intDimensionKey = $intMemberClaDescrip;
				}
				
				$arrCatMemberValues[] = "key_".$intDimensionKey."_SVSep_".$strValue;
				//@JAPR
			}
			
			//Devuelve el resultado a partir del String en lugar de ir al catálogo por él
			$strCatalogValue = implode("_SVElem_", $arrCatMemberValues);
			$arrayKeyValues[0] = $strCatalogValue;
			$arrayKeyValues[1] = $strValue;
			return $arrayKeyValues;
		}
	}
	
	//Obtenemos cual es el cla_descrip de la dimension principal dummy
	//@JAPR 2014-05-30: Agregado el tipo de sección Inline
	$strAdditionalFields = '';
	if (getMDVersion() >= esveBavelCatalogs) {
		$strAdditionalFields .= ', eBavelAppID, eBavelFormID';
	}
	//@JAPR
	$sql = "SELECT ParentID $strAdditionalFields FROM SI_SV_Catalog WHERE CatalogID = ".$aCatalogID;
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if (!$aRS || $aRS->EOF)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return $arrayKeyValues;
		}
	}
	
	$parentCladescrip = (int)$aRS->fields["parentid"];
	//@JAPR 2014-05-30: Agregado el tipo de sección Inline
	//@JAPR 2014-05-30: Agregados los catálogos tipo eBavel
	$blnEmptyRS = true;
	$inteBavelAppID = (int) @$aRS->fields["ebavelappid"];
	$inteBavelFormID = (int) @$aRS->fields["ebavelformid"];
	$strValue = '';
	$arrCatMemberValues = array();
	if ($inteBavelAppID > 0 && $inteBavelFormID > 0) {
		//Es un catálogo de eBavel, así que se deben extraer los datos desde dicho producto
		$aCatalogFilter = BITAMCatalogFilter::NewInstance($aRepository, $aCatalogID, true);
		
		$arrCatalogMembersByID = array();
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
		if (!is_null($objCatalogMembersColl)) {
			foreach ($objCatalogMembersColl as $objCatalogMember) {
				$arrCatalogMembersByID[$objCatalogMember->MemberID] = $objCatalogMember;
			}
		}
		
		$intCatMemberID = 0;
		$arreBavelFieldIDs = array();
		//Agrega todos los IDs de campos de eBavel de los atributos que se van a pedir
		foreach ($arrAttributesColl as $intMemberID => $arrDescrips) {
			$intCatMemberID = $intMemberID;
			$objCatalogMember = @$arrCatalogMembersByID[$intCatMemberID];
			if (!is_null($objCatalogMember) && $objCatalogMember->eBavelFieldID > 0) {
				$arreBavelFieldIDs[] = $objCatalogMember->eBavelFieldID;
			}
		}
		
		//Usando el último atributo solicitado, se carga su instancia para obtener su posición
		$intMemberOrder = 0;
		if (!is_null($objCatalogMember)) {
			$intMemberOrder = $objCatalogMember->MemberOrder -1;
		}
		
		$strWhere = 'id_'.$aCatalogFilter->FormID.' = '.$intSectionValue;
		$aRS = $aCatalogFilter->getCatalogValues($intMemberOrder, true, true, false, $arreBavelFieldIDs, $strWhere);
		if ($aRS && is_object($aRS) && property_exists($aRS, 'EOF') && !$aRS->EOF) {
			//@JAPR 2014-09-12: Corregido un bug, nunca se estaba asignando esta variable al recorrer el recordset, así que siempre regresaba adicional a
			//los datos correctos, valores de *NA para los mismos atributos por lo que terminaba grabando *NA por ser los últimos del string devuelto
			$blnEmptyRS = false;
			//@JAPR
			foreach ($arrAttributesColl as $intMemberID => $arrDescrips) {
				$intMemberClaDescrip = $arrDescrips[0];
				$strAttrField = "DSC_".$intMemberClaDescrip;
				$streBavelFieldID = (string) @$aCatalogFilter->RSFields[$strAttrField];
				$strValue = (string) @$aRS->fields[$streBavelFieldID];
				$intDimensionKey = $arrDescrips[1];
				if ($bReturnClaDescrip) {
					$intDimensionKey = $intMemberClaDescrip;
				}
				
				$arrCatMemberValues[] = "key_".$intDimensionKey."_SVSep_".$strValue;
			}
		}
	}
	else {
		//Es un catálogo normal, así que consulta directamente la tabla de dimensión del mismo
		$sql = "SELECT RIDIM_".$parentCladescrip."KEY ";
		foreach ($arrAttributesColl as $intMemberID => $arrDescrips) {
			$intMemberClaDescrip = $arrDescrips[0];
			$sql .= ", DSC_".$intMemberClaDescrip;
		}
		$sql .= " FROM RIDIM_".$parentCladescrip." 
			WHERE RIDIM_".$parentCladescrip."KEY = ".$intSectionValue;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." RIDIM_".$parentCladescrip." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." RIDIM_".$parentCladescrip." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
		
		if(!$aRS->EOF)
		{
			//@JAPR 2014-09-12: Corregido un bug, nunca se estaba asignando esta variable al recorrer el recordset, así que siempre regresaba adicional a
			//los datos correctos, valores de *NA para los mismos atributos por lo que terminaba grabando *NA por ser los últimos del string devuelto
			$blnEmptyRS = false;
			//@JAPR
			foreach ($arrAttributesColl as $intMemberID => $arrDescrips) {
				$intMemberClaDescrip = $arrDescrips[0];
				$strAttrField = "dsc_".$intMemberClaDescrip;
				$strValue = (string) @$aRS->fields[$strAttrField];
				//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				$intDimensionKey = $arrDescrips[1];
				if ($bReturnClaDescrip) {
					$intDimensionKey = $intMemberClaDescrip;
				}
				
				$arrCatMemberValues[] = "key_".$intDimensionKey."_SVSep_".$strValue;
				//@JAPR
			}
		}
	}
	//@JAPR
	
	if ($blnEmptyRS) {
		//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
		if (!$bReturnEmpty) {
			//Como no encontró un valor valido en los campos de seleccion simple CON CATALOGO
			//Entonces se retorna el valor de No Aplica el cual tiene el VALOR DE 1
			foreach ($arrAttributesColl as $intMemberID => $arrDescrips) {
				$intMemberClaDescrip = $arrDescrips[0];
				$strAttrField = "dsc_".$intMemberClaDescrip;
				//@JAPR 2013-03-27: Se debe regresar el valor de NA en este caso
				$strValue = "*NA";	// (string) @$aRS->fields[$strAttrField];
				//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				$intDimensionKey = $arrDescrips[1];
				if ($bReturnClaDescrip) {
					$intDimensionKey = $intMemberClaDescrip;
				}
				
				$arrCatMemberValues[] = "key_".$intDimensionKey."_SVSep_".$strValue;
				//@JAPR
			}
		}
	}
	
	$strCatalogValue = implode("_SVElem_", $arrCatMemberValues);
	$arrayKeyValues[0] = $strCatalogValue;
	$arrayKeyValues[1] = $strValue;
	
	return $arrayKeyValues;
}

/* Dado una encuesta y una instancia de pregunta de catálogo de la cual se desea obtener el valor, consulta el valor actual de esta
pregunta en la tabla paralela directamente para la encuesta indicada, aplicando validaciones según si el registro definido por el $factKey, 
$factKeyDimVal, $anEntrySectionID y $anEntryCatDimID pertenece o no a un registro de sección dinámica, en cuyo caso regresa el valor completo
asociado a dicha sección
El resultado es el Array indexado por el CLA_DESCRIP de la dimensión Desasociada del catálogo correspondiente con cada atributo, y cada valor
es la descripción grabada para dicho atributo
En caso de error o de no encontrarse un registro, regresará un array vacio
*/
function getCatQuestionValueArray($aRepository, $aSurveyInstance, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID = 0, $anEntryCatDimID = 0, $dynamicSectionID = 0, $dynamicSectionCatID = 0) {
	if (is_null($factKey) || ((int)$factKey) <= 0) {
		return array();
	}
	
	if (is_null($aQuestion) || $aQuestion->QTypeID != qtpSingle || $aQuestion->CatalogID <=0) {
		//No es una pregunta de catálogo
		return array();
	}
	
	//Determina si debe o no utilizar el campo de la pregunta o bien el campo de la sección dinámica
	$fieldName = $aQuestion->SurveyCatField;
	if ($aQuestion->CatalogID == $dynamicSectionCatID) {
		//Sólo usará el valor de la sección dinámica si el registro procesado es precisamente de esa sección
		if ($anEntrySectionID == $dynamicSectionID) {
			$fieldName = "DynamicValue";
		}
	}
	
	$tableName = $aSurveyInstance->SurveyTable;
	$sql = "SELECT {$fieldName} 
		FROM {$tableName} 
		WHERE FactKey = {$factKey}";
    $aRS = $aRepository->DataADOConnection->Execute($sql);
    if (is_null($aRS) || $aRS->EOF) {
    	return array();
    }
    
    $strCatValue = (string) @$aRS->fields[strtolower($fieldName)];
    if ($strCatValue == '') {
    	return array();
    }
    
	$arrayCatPairValues = explode('_SVElem_', $strCatValue);
	$arrayCatClaDescrip = array();
	$arrayCatValues = array();
	$arrayTemp = array();
	$arrayInfo = array();
	$arrCatDesc = array();
	foreach($arrayCatPairValues as $element)
	{
		$arrayInfo = explode('_SVSep_', $element);
		
		//Obtener el cla_descrip y almacenar (key_1178)
		$arrayTemp = explode('_', $arrayInfo[0]);
		$intClaDescrip = $arrayTemp[1];
		
		//Obtener el valor del catalogo
		$strDesc = $arrayInfo[1];
		
		$arrCatDesc[$intClaDescrip] = $strDesc;
	}
    
	return $arrCatDesc;
}

/* Dado una encuesta y una instancia de dimensión de la cual se desea obtener el valor, consulta la descripción actual de esta
pregunta en la tabla del cubo directamente para la encuesta indicada. Si no se encuentra el valor, regresará el default de eForms para dimensiones
Si no se envía la instancia de pregunta, se asume que es una dimensión fija de eForms en lugar de una dimensión de pregunta
*/
function getDimDescFromFactTable($aRepository, $aSurveyInstance, $anInstanceDim, $aQuestion, $factKey, $factKeyDimVal, $bDieOnSQL = false) {
	global $gblEFormsNA;
	
	$strDimDesc = '';
	if (is_null($factKey) || ((int)$factKey) <= 0) {
		return $gblEFormsNA;
	}
	
	if (is_null($aSurveyInstance) || is_null($anInstanceDim)) {
		return $gblEFormsNA;
	}
	
	$fieldName = $anInstanceDim->Dimension->FieldDescription;
	$dimTableName = $anInstanceDim->Dimension->TableName;
	$strDimFieldKey = $anInstanceDim->Dimension->TableName."KEY";
	$tableName = $aSurveyInstance->FactTable;
	$sql = "SELECT B.{$fieldName} 
		FROM {$tableName} A, {$dimTableName} B 
		WHERE A.{$strDimFieldKey} = B.{$strDimFieldKey} AND A.FactKey = {$factKey}";
	if ($bDieOnSQL) {
		die();
	}
    $aRS = $aRepository->DataADOConnection->Execute($sql);
    if (is_null($aRS) || $aRS->EOF) {
    	return $gblEFormsNA;
    }
    
    $strDimDesc = @$aRS->fields[strtolower($fieldName)];
    if (is_null($strDimDesc)) {
    	return $gblEFormsNA;
    }
    
	return $strDimDesc;
}

/* Dado una encuesta y un nombre de campo del cual se desea obtener el valor, consulta la descripción actual de dicho campo en la tabla paralela
para la encuesta indicada. Si no se encuentra el valor, regresará vacio
*/
function getFieldDescFromFactTable($aRepository, $aSurveyInstance, $aFieldName, $factKey, $factKeyDimVal) {
	global $gblEFormsNA;
	
	$strDimDesc = '';
	if (is_null($factKey) || ((int)$factKey) <= 0) {
		return '';
	}
	
	if (is_null($aSurveyInstance) || trim($aFieldName) == '') {
		return '';
	}
	
	$fieldName = $aFieldName;
	$tableName = $aSurveyInstance->SurveyTable;
	$sql = "SELECT A.{$fieldName} 
		FROM {$tableName} A 
		WHERE A.FactKey = {$factKey}";
    $aRS = $aRepository->DataADOConnection->Execute($sql);
    if (is_null($aRS) || $aRS->EOF) {
    	return '';
    }
    
    $strDimDesc = @$aRS->fields[strtolower($fieldName)];
    if (is_null($strDimDesc)) {
    	return '';
    }
    
	return $strDimDesc;
}

//Igual que getCatValueDescForSurveyField pero en lugar de regresar un array con el string de todos los atributos concatenados + el valor específico
//del último atributo, regresa un array indexado por el CLA_DESCRIP de cada atributo con el key subrrogado que le tocó a cada uno en su dimensión
//según la descripción del valor de su atributo en el key subrrogado del catálogo proporcionado en los parámetros, por lo cual si no existía ese
//miembro en su dimensión entonces lo inserta en este momento
//Esta función sólo se debe usar si la encuesta correspondiente utiliza catálogos desasociados de sus dimensiones
//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
//Agregado el parámetro $aDynamicRecNum para indicar el número de registro de la sección dinámica del que se trata. Si se manda un valor < 0
//significa que no es un registro de la sección dinámica, por lo tanto funcionará como antes. Cuando si es un registro de la dinámica, en dado
//caso de recibir el valor de NA, lo que se hace es ir en busca del valor pero al array de descripciones de la dinámica que se tuvo que haber
//recibido y se usan tales descripciones para grabar los valores, en lugar de usar el key subrrogado para asignar solo valores de NA
//Agregado el parámetro $aCatalogValue con el valor seleccionado de una combinación de catálogo en el formato que se recibe para las preguntas
//simple choice de catálogo, el cual en caso de recibirse será el que se utilice para generar el array de resultado de la función, siempre
//y cuando no se reciba también el parámetro $aDynamicRecNum pues en ese caso se da preferencia a obtener el valor de la sección dinámica (tal
//como este otro parámetro, sólo aplica si el key subrrogado del parámetro $arrayValues es el de NA)
//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
//Agregado el parámetro $bReturnClaDescrip para indicar si el resultado deberá usar el ID de dimensión CLA_DESCRIP en lugar del IndDimID
//Agregado el parámetro $bReturnEmpty para indicar si en caso de no encontrar el valor se debe regresar vacio (true) o *NA (false)
//@JAPR 2014-05-29: Agregado el tipo de sección Inline
//Agregado el parámetro $aSectionID, si se especifica se asumirá que NO se trata de una sección dinámica (ya que sólo hay una) sino que se trata
//de una sección Inline (a la fecha de implementación son las únicas que usarían este parámetro) y se recibe este ID para saber a cual sección
//específicamente se está haciendo referencia, para permitir accesar a sus Arrays cuando $aDynamicRecNum != -1 (en ese caso se usan arrays diferentes)
function getCatValueArrayForSurveyField($aRepository, $aSurveyInstance, $arrayValues, $aCatalogID, $aMaxMemberID = -1, $aDynamicRecNum = -1, $aCatalogValue = '', $bReturnClaDescrip = 0, $bReturnEmpty = 0, $aSectionID = -1) {
	global $arrDynamicCatalogIDs;
	global $arrDynamicCatalogAttributes;
	//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
	//Estas variables contendrán los valores que fueron procesados en la última llamada a esta función, para poder usarlos desde quien la invoca
	global $arrLastCatClaDescrip;
	global $arrLastCatValues;
	
	//@JAPR 2013-02-12: Agregada la edición de datos históricos
	$intSectionValue = (int) trim(@$arrayValues[0]);
	if($intSectionValue < 0 && $aSurveyInstance->DissociateCatDimens) {
		//Si se hubiera especificado un key negativo y la encuesta usara catálogos desasociados, significaría que esta petición proviene de una
		//edición de captura y que el valor del catálogo no cambió para esta pregunta durante la misma, así que como existe la posibilidad de que
		//el nuevo catálogo pudiera estar diferente, se optará por no modificar estas dimensiones independientes pues ya apuntan a los valores
		//correctos, al regresar un array vacio estas dimensiones ya no son procesadas. Esto no aplica para el grabado de una nueva captura, ya
		//que en esos casos el key subrrogado siempre viene como un valor del catálogo actual, o bien, si durante la edición hubieran decidido
		//seleccionar un valor distinto del catálogo también se hubiera recibido un key positivo por lo que seguiría el proceso normal de grabado
		return array();
	}
	
	//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	//@JAPR 2014-05-29: Agregado el tipo de sección Inline
	//Agregado el parámetro $aSectionID
	$arrCatalogValues = getCatValueDescForSurveyField($aRepository, $arrayValues, $aCatalogID, $aMaxMemberID, $aDynamicRecNum, $aCatalogValue, $bReturnClaDescrip, $bReturnEmpty, $aSectionID);
	
	//Si hay errores, regresa un array con sólo *NA o vacio si no puede cargar los atributos
	if (!is_array($arrCatalogValues) || trim((string) @$arrCatalogValues[0]) == '') {
		//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
		if ($bReturnEmpty) {
			return array();
		}
		//@JAPR
		
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
		if (is_null($objCatalogMembersColl)) {
			return array();
		}
		
		$arrCatMemberValues = array();
		foreach ($objCatalogMembersColl as $objCatalogMember) {
			$intMemberClaDescrip = $objCatalogMember->IndDimID;	//ClaDescrip
			//@JAPR 2013-03-27: Se debe regresar el valor de NA en este caso
			$arrCatMemberValues[] = "key_".$intMemberClaDescrip."_SVSep_"."*NA";
			if ($objCatalogMember->MemberID == $aMaxMemberID) {
				break;
			}
		}
		$arrCatalogValues = array();
		$arrCatalogValues[0] = $strCatalogValue = implode("_SVElem_", $arrCatMemberValues);
	}
	
	//Hasta este punto se tienen todos los atributos pedidos en un string, ahora se separa el String en sus componentes para identificar por
	//dimensión si existe o no el valor y regresar su key subrrogado existente o recien agregado según el caso
	$arrayCatPairValues = explode('_SVElem_', $arrCatalogValues[0]);
	$arrayCatClaDescrip = array();
	$arrayCatValues = array();
	$arrayTemp = array();
	$arrayInfo = array();
	
	foreach($arrayCatPairValues as $element)
	{
		$arrayInfo = explode('_SVSep_', $element);
		
		//Obtener el cla_descrip y almacenar (key_1178)
		$arrayTemp = explode('_', $arrayInfo[0]);
		$arrayCatClaDescrip[] = $arrayTemp[1];
		
		//Obtener el valor del catalogo
		$arrayCatValues[] = $arrayInfo[1];
	}
	
	//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
	$arrLastCatClaDescrip = $arrayCatClaDescrip;
	$arrLastCatValues = $arrayCatValues;
	
	//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	require_once("../model_manager/indicatorkpi.inc.php");
	//@JAPR
	
	$arrCatMemberValues = array();
	$intCont = 0;
	foreach ($arrayCatClaDescrip as $aClaDescrip) {
		$anInstanceIndDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aSurveyInstance->ModelID, -1, $aClaDescrip);
		if (is_null($anInstanceIndDim)) {
			$arrayKeyValues = array(1);
		}
		else {
			$arrayKeyValues = getInsertDimValueKey($aRepository, $anInstanceIndDim, array((string) @$arrayCatValues[$intCont]));
			if (!is_array($arrayKeyValues)) {
				$arrayKeyValues = array(1);
			}
		}
		
		$arrCatMemberValues[$aClaDescrip] = $arrayKeyValues[0];
		$intCont++;
	}
	
	return $arrCatMemberValues;
}
//@JAPR

//Obtiene la llave subrrogada en la tabla de tiempo del modelo especificado para la fecha indicada, en caso de no existir, la agrega 
//formateando la fecha según cada dimensión tiempo definida en el modelo
function getDateKey($aRepository, $anInstanceModel, $descriptKeysCollection, $currentDate)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;

	$periodDims = BITAMDimensionCollection::NewInstancePeriodDims($aRepository, $anInstanceModel->ModelID, false);
	$numPeriodDims=count($periodDims->Collection);
	
	$formatedDate = $currentDate." 00:00:00";
	$dimPeriodo = $anInstanceModel->dim_periodo;
	$fechaField = $anInstanceModel->fecha;
	$dateKeyField = getJoinField($dimPeriodo, "", $descriptKeysCollection, false);
	
	$sql = "SELECT ".$dateKeyField." FROM ".$dimPeriodo." WHERE ".$fechaField." = ".$aRepository->DataADOConnection->DBTimeStamp($currentDate);
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$dimPeriodo." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$dimPeriodo." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	if (!$aRS->EOF)
	{	
		//Ya existe esa fecha en la tabla especificada en dim_periodo, se retorna su clave
		$dateKey =  $aRS->fields[strtolower($dateKeyField)];
	}
	else 
	{
		$dateKey = substr($currentDate, 0, 10);
		$dateKey = str_replace("-", "", $dateKey);
		
		$sql = "INSERT INTO ".$dimPeriodo." ( ".$dateKeyField.", ".$fechaField;
					
		for($i=0; $i<$numPeriodDims; $i++)
		{
			$sql.= ", ".$periodDims->Collection[$i]->nom_fisicok_bd;
		}
		
		$sql.= " ) VALUES ( ".$dateKey.", ".$aRepository->DataADOConnection->DBTimeStamp($currentDate);
		
		for($i=0; $i<$numPeriodDims; $i++)
		{	
			$formatedDateField = formatADateTime($formatedDate, $periodDims->Collection[$i]->fmt_periodo);
			$formatedDateField = $aRepository->DataADOConnection->Quote($formatedDateField);
			$sql.= ", ".$formatedDateField;
		}
		
		$sql.=" )";
		
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$dimPeriodo." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$dimPeriodo." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
		}
	}
	
	return $dateKey;
}

function isMultiSurvey($aRepository, $surveyID, $arrayData)
{
	$countSurveys = 0;
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);

	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		$postKey = "qfield".$aQuestion->QuestionID;
		
		if(isset($arrayData[$postKey]))
		{
			if(is_array($arrayData[$postKey]))
			{
				$countSurveys = count($arrayData[$postKey]);
				break;
			}

		}
	}
	
	return $countSurveys;
}

function multipleUpdateCategoryDimData($aRepository, $surveyID, $arrayData, $dateID, $hourID, $userID, $thisDate, &$factKeyDimVal=0)
{
	global $gblEFormsErrorMessage;
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	global $syncDate;
	global $syncTime;
	//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
	global $blnWebMode;
	//@JAPR 2012-11-23: Corregida la fecha de inicio de actualización
	global $strLastUpdateDate;
	global $strLastUpdateTime;

	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br><br>\r\n\r\nmultipleUpdateCategoryDimData");
	}
	
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	
	//antes de enviar a grabar este elemento tenemos que ajustar los valores de las fechas en $thisDate y en las llaves surveyDate, surveyHour,
	//surveyLatitude, surveyLongitude para que tengan los mismos valores que los demas entries que se insertaron cuando se trata de registro nuevo
	//Obtenemos el SurveyKey
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	
	$aSurveyInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyDimID);
	$surveyKey = getSurveyDimKey($aRepository, $aSurveyInstanceDim, $surveyInstance);
	
	if($surveyKey===false)
	{
		return($gblEFormsErrorMessage);
	}
	
	$groupKey = $surveyKey."-".$arrayData['entryID'];

	//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
	$strAccuracyIndStr = '';
	if ($surveyInstance->GblAccuracyIndID > 0) {
		$strAccuracyIndStr = ", A.IND_".$surveyInstance->GblAccuracyIndID." AS Accuracy";
	}
	$sql = "SELECT C.DSC_".$surveyInstance->GblStartDateDimID." AS StartDate, D.DSC_".$surveyInstance->GblStartTimeDimID." AS StartTime, 
			E.DSC_".$surveyInstance->GblEndDateDimID." AS EndDate, F.DSC_".$surveyInstance->GblEndTimeDimID." AS EndTime, 
			A.IND_".$surveyInstance->GblLatitudeIndID." AS Latitude, A.IND_".$surveyInstance->GblLongitudeIndID." AS Longitude {$strAccuracyIndStr} 
		FROM RIFACT_".$surveyInstance->GblSurveyModelID." A, RIDIM_".$surveyInstance->GblSurveyGlobalDimID." B, 
			RIDIM_".$surveyInstance->GblStartDateDimID." C, RIDIM_".$surveyInstance->GblStartTimeDimID." D, 
			RIDIM_".$surveyInstance->GblEndDateDimID." E, RIDIM_".$surveyInstance->GblEndTimeDimID." F 
		WHERE B.DSC_".$surveyInstance->GblSurveyGlobalDimID." = ".$aRepository->DataADOConnection->Quote($groupKey)." 
			AND A.RIDIM_".$surveyInstance->GblSurveyGlobalDimID."KEY = B.RIDIM_".$surveyInstance->GblSurveyGlobalDimID."KEY 
			AND A.RIDIM_".$surveyInstance->GblStartDateDimID."KEY = C.RIDIM_".$surveyInstance->GblStartDateDimID."KEY
			AND A.RIDIM_".$surveyInstance->GblStartTimeDimID."KEY = D.RIDIM_".$surveyInstance->GblStartTimeDimID."KEY
			AND A.RIDIM_".$surveyInstance->GblEndDateDimID."KEY = E.RIDIM_".$surveyInstance->GblEndDateDimID."KEY
			AND A.RIDIM_".$surveyInstance->GblEndTimeDimID."KEY = F.RIDIM_".$surveyInstance->GblEndTimeDimID."KEY 
		ORDER BY A.FactKey";
	
    $aRS = $aRepository->DataADOConnection->Execute($sql);

    if (!is_null($aRS) && !$aRS->EOF) 
    {
    	$startDateVal = $aRS->fields["startdate"];
    	$startTimeVal = $aRS->fields["starttime"];
    	
    	$endDateVal = $aRS->fields["enddate"];
    	$endTimeVal = $aRS->fields["endtime"];
    	
    	$latitudeVal = (float) $aRS->fields["latitude"];
    	$longitudeVal = (float) $aRS->fields["longitude"];
    	
    	$previewSurveyDate = $startDateVal;
    	$previewSurveyHour = $startTimeVal;
    	$previewThisDate = $endDateVal." ".$endTimeVal;

    	$previewLatitude = $latitudeVal;
    	$previewLongitude = $longitudeVal;
		//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
    	$previewAccuracy = (int) @$aRS->fields["accuracy"];
    }
    else 
    {
    	$previewSurveyDate = $arrayData["surveyDate"];
    	$previewSurveyHour = $arrayData["surveyHour"];
    	$previewThisDate = $thisDate;

    	$previewLatitude = $arrayData["surveyLatitude"];
    	$previewLongitude = $arrayData["surveyLongitude"];
		//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
		$previewAccuracy = $arrayData["surveyAccuracy"];
    }

	$sendEmailDashboards = false;
	
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	
	$newArrayData = array();
	$newArrayData["surveyDate"] = $arrayData["surveyDate"];
	$newArrayData["surveyHour"] = $arrayData["surveyHour"];
	$newArrayData["userID"] = $arrayData["userID"];
	$newArrayData["appUserID"] = $arrayData["appUserID"];
	$newArrayData["entryID"] = $arrayData['entryID'];
	$entryID = $newArrayData["entryID"];

	$newArrayData["surveyFinished"] = $arrayData["surveyFinished"];
	//@JAPR 2013-04-10: A este día, cuando se hacía un Reupload no venían estos datos porque llegaban como parámetro $_POST y no como parte del
	//archivo, por lo tanto generaban Warnings así que se validará en lo que se integran al archivo
	$newArrayData["surveyLatitude"] = @$arrayData["surveyLatitude"];
	$newArrayData["surveyLongitude"] = @$arrayData["surveyLongitude"];
	//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
	$newArrayData["surveyAccuracy"] = @$arrayData["surveyAccuracy"];
	//@JAPR
	
	if(isset($arrayData["CaptureVia"]))
	{
		$newArrayData["CaptureVia"] = $arrayData["CaptureVia"];
	}
	
	if(isset($arrayData["CaptureEmail"]))
	{
		$newArrayData["CaptureEmail"] = $arrayData["CaptureEmail"];
	}
	
	if(isset($arrayData["SchedulerID"]))
	{
		$newArrayData["SchedulerID"] = $arrayData["SchedulerID"];
	}
	
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		//si las preguntas pertenecen a las de tipo Category Dimension
		//entonces las excluimos de este ciclo ya que aqui se procesan
		//las preguntas que se repetiran los datos en todos los registros
		if($aQuestion->QTypeID==qtpMulti && $aQuestion->QDisplayMode==dspVertical && $aQuestion->MCInputType==mpcNumeric && $aQuestion->IsMultiDimension==0 && $aQuestion->UseCategoryDimChoice!=0)
		{
			//Procesamiento del dato de qfield para que sea considerado en el indicador answeredquestions como pregunta contestada
			$postKey = "qfield".$aQuestion->QuestionID;
			if(isset($arrayData[$postKey]))
			{
				$strValue = $arrayData[$postKey];
			}
			else 
			{
				$strValue = "";
			}
			$newArrayData[$postKey] = $strValue;

			continue;
		}
		
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		//Si la encuesta está configurada para grabar un registro único para secciones estándar, ya no procesa ningún otro tipo de pregunta
		//para no provocar que se repitan sus valores
		//@JAPR 2013-01-17: Corregido un bug, se habían excluido de los registros de maestro-detalle las preguntas de catálogo de las secciones
		//estándar, por lo tanto no se podía agrupar por ellas al consultar desde el EAS. En este caso se dejará el mismo valor seleccionado
		//para la pregunta estándar
		//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
		//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
		//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
		//if ($surveyInstance->UseStdSectionSingleRec && !($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1 && $aQuestion->CatalogID>0)) {
		if ($surveyInstance->UseStdSectionSingleRec && ($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)) {
			continue;
		}
		//@JAPR
		
		//Procesamiento del dato de qfield
		$postKey = "qfield".$aQuestion->QuestionID;
		if(isset($arrayData[$postKey]))
		{
			$strValue = $arrayData[$postKey];
		}
		else 
		{
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			else 
			{
				$strValue = "";
			}
		}

		$newArrayData[$postKey] = $strValue;
		
		//Procesamiento del dato de comment
		$otherPostKey = "qcomment".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
		//Procesamiento del dato de document
		$otherPostKey = "qdocument".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		//@JAPR
	}
	
	//Buscamos las respuestas de CategoryDimension
	$postKey = "qCatDimVal";
	$countSurveys = count($arrayData[$postKey]);

	$i=0;
	$strStatus = array ();
	$totalOK = 0;
	for($i=0; $i<$countSurveys; $i++)
	{
		$newArrayData["surveyDate"] = $arrayData["surveyDate"];
		$newArrayData["surveyHour"] = $arrayData["surveyHour"];
		$newArrayData["surveyLatitude"] = $arrayData["surveyLatitude"];
		$newArrayData["surveyLongitude"] = $arrayData["surveyLongitude"];
		//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
		$newArrayData["surveyAccuracy"] = $arrayData["surveyAccuracy"];

		//Obtenemos el valor en su indice $i del campo oculto qCatDimVal
		//que es quien contiene el valor subrogado de la dimension categoria
		$otherPostKey = "qCatDimVal";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
			}
			else 
			{
				$strValue = 1;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Obtenemos el valor en su indice $i del campo oculto qElemDimVal
		//que es quien contiene el valor subrogado de la dimension element
		$otherPostKey = "qElemDimVal";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
			}
			else 
			{
				$strValue = 1;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//Obtenemos el valor en su indice $i del campo oculto qCatDimValQID
		//que es quien contiene el valor del questionid al que esta asociada la respuesta
		$otherPostKey = "qCatDimValQID";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
			}
			else 
			{
				$strValue = -1;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		$intEntryCatDimID = (int) $newArrayData[$otherPostKey];
		
		//Obtenemos el valor en su indice $i del campo oculto qElemDimValResp
		//que es quien contiene el valor del questionid al que esta asociada la respuesta
		$otherPostKey = "qElemDimValResp";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
			}
			else 
			{
				$strValue = 0;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//Procesamiento del dato de registerID
		//Obtenemos el valor en su indice $i del campo oculto registerID
		//que es el FactKey con el q se grabo esa respuesta de qElemDimValResp
		$strValue = null;
		$registerID = null;
		$otherPostKey = "registerID";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
				$registerID = $strValue;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Si hay una valor valido para registerID entonces si se esta editando
		//un registro ya existente, en caso contrario se deberia insertar el nuevo registro
		if(!is_null($registerID) && $registerID>0)
		{
			//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
			//En este caso se debe identificar a que pregunta de categoría de dimensión pertenece este registro. Hasta la fecha no se podían
			//combinar este tipo de preguntas con otras secciones de varios registros, así que sólo enviamos el EntryCatDimID
			$strStatus[$i] = updateData($aRepository, $surveyID, $newArrayData, $dateID, $hourID, $userID, $thisDate, $registerID, 0, $intEntryCatDimID, $factKeyDimVal);
		}
		else
		{
			//Mandamos la fecha, hora, latitud y longitud a como se grabaron los registros previamente almacenados
			//para que no hayan discrepancia en los datos
	    	$newArrayData["surveyDate"] = $previewSurveyDate;
	    	$newArrayData["surveyHour"] = $previewSurveyHour;
	    	$newArrayData["surveyLatitude"] = $previewLatitude;
	    	$newArrayData["surveyLongitude"] = $previewLongitude;
			//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
			$newArrayData["surveyAccuracy"] = $previewAccuracy;
    		
			//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
			//En este caso se debe identificar a que pregunta de categoría de dimensión pertenece este registro. Hasta la fecha no se podían
			//combinar este tipo de preguntas con otras secciones de varios registros, así que sólo enviamos el EntryCatDimID
			$strStatus[$i] = saveData($aRepository, $surveyID, $newArrayData, $previewThisDate, $factKeyDimVal, $sendEmailDashboards, $countSurveys, null, 0, $intEntryCatDimID);
			
			//Despues de ello debemos actualizar LastDateID, LastHourID, LastStartTime, LastEndTime
			//@JAPR 2012-11-23: Corregidas las fechas de última edición, estaban tomando la misma del archivo pero esa se mantenía como la fecha
			//original de la captura, así que no reflejaba la fecha de última edición, para modo Web en realmente la fecha de sincronización
			$strLastDateID = $arrayData["surveyDate"];
			$strLastHourID = $arrayData["surveyHour"];
			$endTime = $thisDate;
			if ($blnWebMode) {
				//@JAPR 2012-11-23: Corregida la fecha de inicio de actualización
				 if ($strLastUpdateDate != '' && $strLastUpdateTime != '') {
					$strLastDateID = $strLastUpdateDate;
					$strLastHourID = $strLastUpdateTime;
					$strLastDateID = substr($strLastDateID, 0, 10)." ".substr($strLastHourID, 0, 8);
				 }
				//@JAPR 2012-11-24: Corregida la fecha de fin de actualización
				if ($strLastUpdateEndDate != '' && $strLastUpdateEndTime != '') {
					$endTime = strtotime((substr($strLastUpdateEndDate, 0, 10)." ".$strLastUpdateEndTime));
				}
			}
			$sql = "UPDATE ".$surveyInstance->SurveyTable." SET 
					LastDateID = ".$aRepository->DataADOConnection->DBTimeStamp($strLastDateID).",  
					LastHourID = ".$aRepository->DataADOConnection->DBTimeStamp($strLastHourID).",  
					LastStartTime = ".$aRepository->DataADOConnection->DBTimeStamp($strLastHourID).",  
					LastEndTime = ".$aRepository->DataADOConnection->DBTimeStamp(substr($endTime, 11))." 
				WHERE FactKeyDimVal = ".$entryID;
			//@JAPR
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					return("(".__METHOD__.") ".translate("Error accessing")." ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}

		if ($strStatus[$i] == "OK") 
		{
			$totalOK ++;
		}
	}
	
	if ($totalOK == $countSurveys) 
	{
		$strStatusDyn = "OK";
	}
	else
	{
		$strStatusDyn = implode(";\r\n", $strStatus);
	}
	
	return $strStatusDyn;
}

function multipleUpdateDynamicData($aRepository, $surveyID, $arrayData, $dateID, $hourID, $userID, $thisDate, $dynamicSectionID, &$factKeyDimVal=0)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	global $arrSectionValuesByMultiChoiceAnswerFromApp;
	global $arrSectionPagesByDesc;
	//$factKeyDimVal = 0;
	//@JAPR 2013-12-10: Corregido un bug en el grabado de la página durante la edición cuando no había preguntas múltiple choice
	global $arrSectionDescByID;
	global $arrSectionDescStrByID;
	//@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br><br>\r\n\r\nmultipleUpdateDynamicData");
	}

	$sendEmailDashboards = false;
	
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	
	$newArrayData = array();
	$newArrayData["surveyDate"] = $arrayData["surveyDate"];
	$newArrayData["surveyHour"] = $arrayData["surveyHour"];
	$newArrayData["userID"] = $arrayData["userID"];
	$newArrayData["appUserID"] = $arrayData["appUserID"];
	
	if(isset($arrayData['entryID']))
	{
		$newArrayData["entryID"] = $arrayData['entryID'];
	}

	$newArrayData["surveyFinished"] = $arrayData["surveyFinished"];
	//@JAPR 2013-04-10: A este día, cuando se hacía un Reupload no venían estos datos porque llegaban como parámetro $_POST y no como parte del
	//archivo, por lo tanto generaban Warnings así que se validará en lo que se integran al archivo
	$newArrayData["surveyLatitude"] = @$arrayData["surveyLatitude"];
	$newArrayData["surveyLongitude"] = @$arrayData["surveyLongitude"];
	//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
	$newArrayData["surveyAccuracy"] = @$arrayData["surveyAccuracy"];
	//@JAPR
	
	if(isset($arrayData["CaptureVia"]))
	{
		$newArrayData["CaptureVia"] = $arrayData["CaptureVia"];
	}
	
	if(isset($arrayData["CaptureEmail"]))
	{
		$newArrayData["CaptureEmail"] = $arrayData["CaptureEmail"];
	}
	
	if(isset($arrayData["SchedulerID"]))
	{
		$newArrayData["SchedulerID"] = $arrayData["SchedulerID"];
	}
	
	if(isset($arrayData["qsignature"]))
	{
		$newArrayData["qsignature"] = $arrayData["qsignature"];
	}
	
	//@JAPR 2012-06-08: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas cuando existe por lo
	//menos una pregunta múltiple en ella, ya que en esos casos sectionValue sólo es un array con una respuesta por subsección dinámica, en lugar
	//de repetir la misma respuesta por cada opción de la múltiple dentro de las subsecciones como sucede con las preguntas Single Choice sin
	//catálogo
	
	//Identifica si es necesario obtener el Array de padres usados para generar las secciones, ya que si no hay pregunta múltiple no es necesario
	$blnGenerateParentValuesArr = false;
	//Contiene la instancia de la pregunta de Single Choice que filtra para generar las subsecciones dinámicas
	$aSCDynQuestionInstance = null;
	$dynamicSection = BITAMSection::NewInstanceWithID($aRepository, $dynamicSectionID);
	$dynamicSectionCatID = $dynamicSection->CatalogID;
	$blnDynamicSectionFound = false;
	//@JAPR 2013-01-15: Agregada la opción para que las preguntas estándar se graben en un registro único
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	//@JAPR
	
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		//@JAPR 2012-06-08: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
		if (!$blnDynamicSectionFound)
		{
			if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID == $dynamicSectionCatID)
			{
				//Como pudiera haber varias preguntas de catálogo sincronizadas, debemos tomar la última para saber exactamente por qué atributo 
				//se debe filtrar
				$aSCDynQuestionInstance = $aQuestion;
			}
		}
		
		//si las preguntas pertenecen a la seccion dinamica y que estan dentro de la seccion
		//entonces si son repetibles y se ignoran dichas preguntas
		if($aQuestion->SectionID == $dynamicSectionID)
		{
			$blnDynamicSectionFound = true;
			if ($aQuestion->QTypeID == qtpMulti)
			{
		        //@JAPR 2012-06-29: Corregido un bug, no había por qué hacer este Break, eso provocaba que ya no se grabaran las preguntas posteriores a las dinámicas
				$blnGenerateParentValuesArr = true;
				//break;
				//@JAPR
			}
			continue;
		}
		
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		//Si la encuesta está configurada para grabar un registro único para secciones estándar, ya no procesa ningún otro tipo de pregunta
		//para no provocar que se repitan sus valores
		//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
		//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
		//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
		if ($surveyInstance->UseStdSectionSingleRec && ($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)) {
			continue;
		}
		//@JAPR
		
		//Procesamiento del dato de qfield
		$postKey = "qfield".$aQuestion->QuestionID;
		if(isset($arrayData[$postKey]))
		{
			$strValue = $arrayData[$postKey];
		}
		else 
		{
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			else 
			{
				$strValue = "";
			}
		}

		$newArrayData[$postKey] = $strValue;
		//@JAPR 2013-08-21: Corregido un bug, durante la edición cuando se agregan nuevos registros, se debe
		//validar para no usar NOCHANGE sino el valor de catálogo correcto
		//Se pasa al array a utilizar el valor de la combinación de catálogo para usarlo en caso de requerir grabar un nuevo registro
		if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID > 0 && (string) $strValue == "NOCHANGE" && isset($arrayData[$postKey.'Cat'])) {
			$newArrayData[$postKey.'Cat'] = $arrayData[$postKey.'Cat'];
		}
		//@JAPR
		
		//Procesamiento del dato de qimageencode
		$otherPostKey = "qimageencode".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = "";
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Procesamiento del dato de comment
		$otherPostKey = "qcomment".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
		//Procesamiento del dato de document
		$otherPostKey = "qdocument".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		//@JAPR
	}
	
	//Realizamos coleccion de preguntas de la seccion dinamica
	$otherQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($aRepository, $dynamicSectionID);

	//@JAPR 2012-06-08: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas cuando existe por lo
	//menos una pregunta múltiple en ella, ya que en esos casos sectionValue sólo es un array con una respuesta por subsección dinámica, en lugar
	//de repetir la misma respuesta por cada opción de la múltiple dentro de las subsecciones como sucede con las preguntas Single Choice sin
	//catálogo
	$arrSectionValuesByMultiChoiceAnswer = array();		//Array indexado por Key de las respuestas múltiples conteniendo el número de página
	$arrPagesByAttribDesc = array();					//Array indexado por Descripción conteniendo el número de página
	$arrDynSectionValues = array();
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\nblnGenerateParentValuesArr: $blnGenerateParentValuesArr");
		echo("<br>\r\nIsNull(aSCDynQuestionInstance): ".(is_null($aSCDynQuestionInstance)));
	}
	
	if ($blnGenerateParentValuesArr && !is_null($aSCDynQuestionInstance))
	{
		//Obtiene el atributo de la última pregunta previa a la sección dinámica, ya que de esa pregunta se obtendrá el valor a filtrar
		//El valor en este punto viene como una lista de los pares "key_AttributeID_SVSep_Value" separada por "_SVElem_", así que obtiene el
		//último pues ese es el filtro sobre el cual se deben obtener los valores del catálogo para el atributo de la sección dinámica y el de
		//las preguntas multiple choice
		$postKey = "qfield".$aSCDynQuestionInstance->QuestionID;
		$strFilterValue = (string) @$arrayData[$postKey];
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nstrFilterValue: $strFilterValue");
		}
		$intDynamicSectionAttribParentID = 0;
		$intMultiAttribParentID = 0;
		$aCatMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $dynamicSection->CatMemberID);
		if (!is_null($aCatMember))
		{
			$intDynamicSectionAttribParentID = $aCatMember->ClaDescrip;
			//Obtiene el siguiente atributo del catálogo, ya que ese será el usado por las preguntas múltiples de la sección dinámica 
			$sql = "SELECT ParentID FROM SI_SV_CatalogMember 
				WHERE CatalogID = ".$aCatMember->CatalogID." AND MemberOrder > ".$aCatMember->MemberOrder." ORDER BY MemberOrder";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF)
			{
				$intMultiAttribParentID = (int) @$aRS->fields["parentid"];
			}
		}
		
		if (trim($strFilterValue) != '' && $intDynamicSectionAttribParentID > 0 && $intMultiAttribParentID > 0)
		{
			//Hay una captura de la pregunta que genera las dinámicas, así que obtenemos los valores basados en el seleccionado en esta pregunta
			$arrDynSectionValues = getAllDynPagesCatValues($aRepository, $surveyID, $dynamicSectionID, $intDynamicSectionAttribParentID, $intMultiAttribParentID, $strFilterValue);
			if (is_null($arrDynSectionValues))
			{
				return($gblEFormsErrorMessage);
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\narrDynSectionValues:");
				PrintMultiArray($arrDynSectionValues);
			}
			
			//Recorre el array indexado por los Keys de las opciones de respuesta del atributo indicados en sectionValues, con el valor obtenido
			//se va llenando otro array que representa a las páginas creadas, como los valores en sectionValue vienen ordenados por páginas,
			//se generan nuevos índices del array de páginas hasta que se cambia la descripción obtenida
			$intDynamicPageNumber = -1;
			//$arrPagesByAttribDesc = array();	//Array indexado por Descripción conteniendo el número de página
			//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
			//Se reutilizará el array global que ya contiene las descripciones de páginas indexadas por su posición
			if ($surveyInstance->DissociateCatDimens && isset($arrSectionValuesByMultiChoiceAnswerFromApp[$dynamicSectionID]) && isset($arrSectionPagesByDesc)) {
				if (isset($arrSectionPagesByDesc[$dynamicSectionID])) {
					$arrPagesByAttribDesc = array_flip($arrSectionPagesByDesc[$dynamicSectionID]);
				}
			}
			//@JAPR
			
			$postKey = "sectionValue";
			$countSurveys = count($arrayData[$postKey]);
			$otherPostKey = "sectionValue";
			for($i=0; $i<$countSurveys; $i++)
			{
				$intSectionValue = 0;
				if(isset($arrayData[$otherPostKey]))
				{
					if(isset($arrayData[$otherPostKey][$i]))
					{
						$intSectionValue = $arrayData[$otherPostKey][$i];
					}
					else 
					{
						//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
						//En v4 no se supone que jamás llegue intencionalmente un key subrrogado de catálogo == 1 (Registro de No Aplica), ya que
						//si la sección dinámica se hubiera saltado, entonces simplemente no hubieran llegado datos de la sección y por tanto no
						//hubiera entrado a este método. Esta validación tal vez aplicaba para v3 o anteriores así que en v4 se usará solo para
						//casos donde dentro de StoreSurveyCapture se determine que los keys recibidos como parte de la sección ya no son válidos
						//en el catálogo (incluso después de intentar remapearlos a partir de las descripciones) por lo que se forzará a usar NA
						//para sus valores, en ese caso excepción la página se determinará por la posición de este SectionValue en lugar de por 
						//el propio key como con los demás valores
						$intSectionValue = 1;
					}
				}
				
				//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				//Cuando se graba a partir de las descripciones recibidas, no se debe usar el array con los datos actuales en el catálogo porque
				//podrían tener descripciones alteradas los nuevos elementos lo cual reordenaría las páginas dinámicas, así que no hay seguridad
				//que aunque algunos keys fueron reutilizados (ya que otros pudieron haberse remapeado o cambiado a NA) las páginas serán tal
				//como se capturaron, por tanto mejor se usará el mismo array recibido en los datos. Esto solo se puede hacer si se utilizan
				//dimensiones independientes del catálogo, de lo contrario se tiene que respetar todo como estén los keys del catálogo
				if ($intSectionValue == 1 || ($surveyInstance->DissociateCatDimens && isset($arrSectionValuesByMultiChoiceAnswerFromApp[$dynamicSectionID][$i]))) {
					//En este caso el key subrrogado no pudo ser remapeado al catálogo actual, así que se procesará el valor para respetar la
					//captura pero ya no se quedará asociado a ningún elemento. Las páginas son determinadas por la posición en lugar de por el
					//propio key, por lo tanto se genera un array en lugar de asignar un valor directo
					if ($intSectionValue == 1) {
						if (!isset($arrSectionValuesByMultiChoiceAnswer[$intSectionValue])) {
							$arrSectionValuesByMultiChoiceAnswer[$intSectionValue] = array();
						}
						
						//Para este proceso se reutilizará el array global de páginas por índice de la sección dinámica, el cual se obtiene a partir
						//de las descripciones recibidas por el App
						//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
						$arrSectionValuesByMultiChoiceAnswer[$intSectionValue][$i] = (int) @$arrSectionValuesByMultiChoiceAnswerFromApp[$dynamicSectionID][$i];
					}
					else {
						$arrSectionValuesByMultiChoiceAnswer[$intSectionValue] = (int) @$arrSectionValuesByMultiChoiceAnswerFromApp[$dynamicSectionID][$i];
					}
				}
				//@JAPR 2013-02-12: Agregada la edición de datos históricos
				//Si es un key subrrogado positivo quiere decir que se trata de una captura donde usaron el catálogo actual, de lo contrario si
				//es un key subrrogado negativo y exclusivamente si se usan catálogos desasociados de sus dimensiones significaría que es una
				//edición en la cual no se cambió la combinación que generaba la página dinámica respecto a lo grabado previamente, así que el
				//array $arrDynSectionValues realmente contiene keys negativos basados en el orden grabado de la sección dinámica. Un key
				//subrrogado de 0 significaría que no hay respuesta así que ese caso no se procesa
				elseif ($intSectionValue > 0 || ($intSectionValue < 0 && $surveyInstance->DissociateCatDimens))
				{
					//Con el Key del atributo de las múltiples se obtiene la descripción de la subpágina dinámica, y con ella se obtiene el número
					//de página, ya que si aun no estaba registrada entonces se agrega
					$strPageValueDesc = @$arrDynSectionValues[$intSectionValue];
					if (is_null($strPageValueDesc))
					{
						//Es una descripción que no existe, por lo tanto no se asigna un número de página para que genere un valor vacio al grabar
					}
					else 
					{
						//Se genera la página o se reutiliza el número de la previamente generada
						if (!isset($arrPagesByAttribDesc[$strPageValueDesc]))
						{
							//Genera una nueva página correspondiente a esta descripción de subsección dinámica
							$intDynamicPageNumber++;
							$arrPagesByAttribDesc[$strPageValueDesc] = $intDynamicPageNumber;
						}
						$arrSectionValuesByMultiChoiceAnswer[$intSectionValue] = $arrPagesByAttribDesc[$strPageValueDesc];
					}
				}
			}
		}
	}
	//@JAPR
	
	//Buscamos las preguntas de la seccion dinamica y nos basamos en el index de la variable $i
	$postKey = "sectionValue";
	$countSurveys = count($arrayData[$postKey]);

	$i=0;
	$strStatus = array ();
	$totalOK = 0;
	//@JAPR 2013-01-12: Modificado para que ahora sólo un registro (en caso de haber multiple-choice) contenga datos de las preguntas Open o que
	//no corresponden con las multiple-choice, ya que si se repiten no se pueden estimar correctamente
	$arrDynamicPagesProcessed = array();
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\narrSectionValuesByMultiChoiceAnswer");
		PrintMultiArray($arrSectionValuesByMultiChoiceAnswer);
		echo("<br>\r\narrPagesByAttribDesc");
		PrintMultiArray($arrPagesByAttribDesc);
	}
	
	//@JAPR 2013-01-12: Modificado para que ahora sólo un registro (en caso de haber multiple-choice) contenga datos de las preguntas Open o que
	//no corresponden con las multiple-choice, ya que si se repiten no se pueden estimar correctamente
	$arrPagesByNum = array_flip($arrPagesByAttribDesc);
	for($i=0; $i<$countSurveys; $i++)
	{
		//@JAPR 2012-06-11: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
		//Movido este código al inicio del ciclo ya que se usará para obtener los valores de las preguntas Open
		$intSectionValue = false;
		//Procesamiento del dato de sectionValue
		$otherPostKey = "sectionValue";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
				//Asigna el valor del elemento múltiple choice (o single choice si no había preguntas múltiple choice) que se está grabando en 
				//este registro para mas adelante usarlo al obtener el índice de la respuesta Open (o cualquiera que no se manda repetida desde 
				//la App) que corresponde a esta página. Sólo aplica si existen preguntas múltiple choice
				$intSectionValue = (int) $strValue;
			}
			else 
			{
				$strValue = 1;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//@JAPR 2013-01-12: Modificado para que ahora sólo un registro (en caso de haber multiple-choice) contenga datos de las preguntas Open o que
		//no corresponden con las multiple-choice, ya que si se repiten no se pueden estimar correctamente
		//Si la pregunta no corresponde con una multiple-choice, como está dentro de la sección dinámica, usando la nueva opción de Single record
		//para estándar, NO grabará repetidas estas preguntas pues alterarían los cálculos, la grabará una sola vez por página
		$strDynamicPageDSC = '';
		$blnCleanNonMCFields = false;
		
		if ($surveyInstance->UseStdSectionSingleRec) {
			//Solo podemos hacer este ajuste si hay preguntas multiple-choice y se recibió información suficiente para poder identificar las
			//páginas, de lo contrario se considera que es un grabado donde no hay multiple-choice y se graba el mismo sectionValue en el
			//nuevo campo, sólo para que se considere que se deben usar estos valores en preguntas que no son multi-choice al ver el reporte
			if ($blnGenerateParentValuesArr && $intSectionValue !== false && isset($arrSectionValuesByMultiChoiceAnswer[$intSectionValue])) {
				//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				$intPageNum = null;
				if ($intSectionValue == 1) {
					//Si el SectionValue es el registro de no aplica, entonces significa que no se pudo mapear el elemento recibido a como está
					//el catálogo actualmente, pero como pudieran haber varios casos, se usará la posición específica del SectionValue para 
					//determinar en qué página se encuentra
					$arrPagesForNA = $arrSectionValuesByMultiChoiceAnswer[$intSectionValue];
					if (is_array($arrPagesForNA) && isset($arrPagesForNA[$i])) {
						//Si se encontró la página por lo que la reutiliza
						$intPageNum = (int) $arrPagesForNA[$i];
					}
					else {
						//En este caso no hay por qué o no hay como procesar las páginas, así que se graba un valor genérico
						$strDynamicPageDSC = 'sectionValue-'.$intSectionValue;
					}
				}
				else {
					$intPageNum = $arrSectionValuesByMultiChoiceAnswer[$intSectionValue];
				}
				
				if (!is_null($intPageNum)) {
					//Cualquier otra pregunta en sección dinámica sólo debe grabar un valor por página dinámica, así que verificamos si para
					//la página actualmente procesada ya se grabó un valor, de no ser así entonces si se incluyen los datos en el registro
					if (!isset($arrDynamicPagesProcessed[$intPageNum])) {
						//El valor del nuevo campo sólo se debe asignar en el registro que representa a la página dinámica, en el resto debe ir vacio
						$strDynamicPageDSC = (string) @$arrPagesByNum[$intPageNum];
						$arrDynamicPagesProcessed[$intPageNum] = $strDynamicPageDSC;
					}
					else {
						$blnCleanNonMCFields = true;
					}
				}
			}
			else {
				//En este caso no hay por qué o no hay como procesar las páginas, así que se graba un valor genérico
				//@JAPR 2013-12-10: Corregido un bug en el grabado de la página durante la edición cuando no había preguntas múltiple choice
				//Había faltado copiar esta parte para la edición
				//Entrar aquí signifca una de 3 cosas:
				//1- Que el valor del key de catálogo no viene asignado, lo cual no es posible tecnicamente y si lo hiciera, simplemente va a
				//ocurrir otro error mas adelante, así que da lo mismo
				//2- Que no estuviera asignado $arrSectionValuesByMultiChoiceAnswer para este key, lo cual tampoco sería posible porque se procesan
				//todos los keys recibidos en el Post
				//3- Que simplemente no hay preguntas múltiple choice en la dinámica, por lo que $blnGenerateParentValuesArr == false y por tanto
				//$arrSectionValuesByMultiChoiceAnswer está vacio, si ese es el caso, entonces se debe usar como página el mismo valor que se
				//usará como opción (es decir, el que corresponde en el catálogo al key subrrogado procesado) por lo que no se debe grabar
				//sectionValue- en esos casos
				if (!$blnGenerateParentValuesArr && $surveyInstance->DissociateCatDimens && isset($arrSectionDescByID) && isset($arrSectionDescStrByID)) {
					//@JAPR 2013-08-22: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
					//Si se graba a partir de las descripciones, no puede ir a traer el String de la combinación ni la descripción
					//del checkbox al catálogo porque el valor probablemente pudo haber cambiado, y aunque para este punto si fuera un
					//key > 0 ya se habría remapeado, aquellos que fueron eliminados grabarían el valor de *NA así que mejor se usará
					//directamente el dato que llegó desde el App en esos casos
					$strDynamicOptionDSC = '';
					$arrSectionDesc = @$arrSectionDescByID[$dynamicSectionID][$i];
					//Durante la edición a diferencia del grabado, no necesariamente viene asignado el valor $strNewCatDimValAnswers, ya que dentro
					//de StoreSurveyCapture se salta esa asignación si vienen keys de catálogo negativos lo cual indicaría un valor editado que
					//no debe cambiar, sin embargo si viene asignado el array de descripciones así que lo reutiliza directamente, que al final
					//es lo que haría si viniera el otro valor asignado
					//$strNewCatDimValAnswers = @$arrSectionDescStrByID[$dynamicSectionID][$i];
					if (/*!is_null($strNewCatDimValAnswers) &&*/ !is_null($arrSectionDesc) && is_array($arrSectionDesc) && count($arrSectionDesc) > 0) {
						//@JAPR 2013-05-09: Corregido un bug con la codificación
						//Este valor SI se debe decodificar, porque se usará para grabar en las tablas de datos
						//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
						$strDynamicOptionDSC = (string) @$arrSectionDesc[count($arrSectionDesc) -1];
					}
					else {
						$arrayMapStrValues = array();
						//A diferencia del grabado inicial, durante la edición el $intSectionValue podría venir negativo si no hubo un cambio
						//en el catálogo, por tanto no sería correcto usarlo para obtener la descripción, sin embargo esta funcionalidad sólo
						//debería aplicar para versiones mayores del App a la que ya enviaba todas las descripciones, por tanto teóricamente
						//nunca debería llegar a este punto para grabar algo equivocadamente... se dejará así como está bajo esa premisa
						$arrayMapStrValues[0] = $intSectionValue;
						$arrDynamicRecValue = getCatValueDescForSurveyField($aRepository, $arrayMapStrValues, $dynamicSectionCatID);
						if (!is_array($arrDynamicRecValue) || count($arrDynamicRecValue) < 2) {
							$arrDynamicRecValue = array();
						}
						$strDynamicOptionDSC = (string) @$arrDynamicRecValue[1];
					}
					if (trim($strDynamicOptionDSC) == '') {
						$strDynamicPageDSC = 'sectionValue-'.$intSectionValue;
					}
					else {
						$strDynamicPageDSC = $strDynamicOptionDSC;
					}
				}
				else {
					$strDynamicPageDSC = 'sectionValue-'.$intSectionValue;
				}
			}
		}
		//@JAPR
		
		foreach ($otherQuestionCollection->Collection as $questionKey => $aQuestion)
		{
			//Procesamiento del dato de qfield
			//@JAPR 2012-06-11: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
			if ($blnGenerateParentValuesArr && $intSectionValue !== false && isset($arrSectionValuesByMultiChoiceAnswer[$intSectionValue]) && ($aQuestion->GQTypeID == qtpOpen || $aQuestion->QTypeID == qtpCalc))
			{
				//Si entra aquí quiere decir que es un tipo de pregunta que en la sección dinámica NO se envía repetida desde la App, sino que
				//se manda sólo una respuesta por página, así que basandose en el sectionValue se determina a que página corresponde este registro
				//y se pide la respuesta de la pregunta con ese índice de página
				$postKey = "qfield".$aQuestion->QuestionID;
				if(isset($arrayData[$postKey]))
				{
					//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
					$intPageNum = null;
					if ($intSectionValue == 1) {
						//Si el SectionValue es el registro de no aplica, entonces significa que no se pudo mapear el elemento recibido a como está
						//el catálogo actualmente, pero como pudieran haber varios casos, se usará la posición específica del SectionValue para 
						//determinar en qué página se encuentra
						$arrPagesForNA = $arrSectionValuesByMultiChoiceAnswer[$intSectionValue];
						if (is_array($arrPagesForNA) && isset($arrPagesForNA[$i])) {
							//Si se encontró la página por lo que la reutiliza
							$intPageNum = (int) $arrPagesForNA[$i];
						}
					}
					else {
						$intPageNum = $arrSectionValuesByMultiChoiceAnswer[$intSectionValue];
					}
					
					//Ahora se deberá validar por null antes de intentar usar el número de página por si este valor ya no existiera en el catálogo
					if(!is_null($intPageNum) && isset($arrayData[$postKey][$intPageNum]))
					{
						$strValue = $arrayData[$postKey][$intPageNum];
					}
					else 
					{
						//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
						if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
						{
							//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
							$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
						}
						else 
						{
							$strValue = "";
						}
					}
					//@JAPR
				}
				else 
				{
					//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
					if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					else 
					{
						$strValue = "";
					}
				}
			}
			//@JAPR
			else
			{
				$postKey = "qfield".$aQuestion->QuestionID;
				if(isset($arrayData[$postKey]))
				{
					if(isset($arrayData[$postKey][$i]))
					{
						$strValue = $arrayData[$postKey][$i];
					}
					else 
					{
						//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
						if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
						{
							//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
							$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
						}
						else 
						{
							$strValue = "";
						}
					}
				}
				else 
				{
					//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
					if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					else 
					{
						$strValue = "";
					}
				}
			}

			//@JAPR 2013-01-12: Modificado para que ahora sólo un registro (en caso de haber multiple-choice) contenga datos de las preguntas Open o que
			//no corresponden con las multiple-choice, ya que si se repiten no se pueden estimar correctamente
			//Si la pregunta no corresponde con una multiple-choice, como está dentro de la sección dinámica, usando la nueva opción de Single record
			//para estándar, NO grabará repetidas estas preguntas pues alterarían los cálculos, la grabará una sola vez por página
			//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
			//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
			//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
			if ($surveyInstance->UseStdSectionSingleRec && $blnCleanNonMCFields && ($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)) {
				//Solo podemos hacer este ajuste si hay preguntas multiple-choice y se recibió información suficiente para poder identificar las
				//páginas, de lo contrario se considera que es un grabado donde no hay multiple-choice y se graba el mismo sectionValue en el
				//nuevo campo, sólo para que se considere que se deben usar estos valores en preguntas que no son multi-choice al ver el reporte
				if ($aQuestion->QTypeID == qtpMulti) {
					//Las preguntas múltiple choice deben variar su valor por registro, así que con ellas no se hace ajuste
				}
				else {
					//Cualquier otra pregunta en sección dinámica sólo debe grabar un valor por página dinámica, así que verificamos si para
					//la página actualmente procesada ya se grabó un valor, de no ser así entonces si se incluyen los datos en el registro
					//Esta página ya había grabado un registro, por lo tanto ya no incluye de nuevo las respuestas de estas preguntas
					//y reemplaza su valor por vacio según el tipo
					if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					else 
					{
						$strValue = "";
					}
				}
			}
			//@JAPR
			$newArrayData[$postKey] = $strValue;
			//@JAPR 2013-08-21: Corregido un bug, durante la edición cuando se agregan nuevos registros, se debe
			//validar para no usar NOCHANGE sino el valor de catálogo correcto
			//Se pasa al array a utilizar el valor de la combinación de catálogo para usarlo en caso de requerir grabar un nuevo registro
			if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID > 0 && (string) $strValue == "NOCHANGE" && isset($arrayData[$postKey.'Cat'][$i])) {
				//En este caso, no podría ser una pregunta simple choice de catálogo del mismo catálogo de la dinámica, así que es seguro hacer
				//esta asignación
				$newArrayData[$postKey.'Cat'] = $arrayData[$postKey.'Cat'][$i];
			}
			//@JAPR
			
			//Procesamiento del dato de qimageencode
			$otherPostKey = "qimageencode".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = "";
				}
			}
			else 
			{
				$strValue = "";
			}
			$newArrayData[$otherPostKey] = $strValue;
			
			//Procesamiento del dato de comment
			$otherPostKey = "qcomment".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = null;
				}
			}
			else 
			{
				$strValue = null;
			}
			$newArrayData[$otherPostKey] = $strValue;
			
			//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
			//Procesamiento del dato de document
			$otherPostKey = "qdocument".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = null;
				}
			}
			else 
			{
				$strValue = null;
			}
			$newArrayData[$otherPostKey] = $strValue;
			//@JAPR
		}
		
		//@JAPR 2012-06-11: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
		//Movido este código al inicio del ciclo ya que se usará para obtener los valores de las preguntas Open
		//Procesamiento del dato de sectionValue
		/*
		$strValue = null;
		$otherPostKey = "sectionValue";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		*/
		
		//Procesamiento del dato de registerID
		$strValue = null;
		$registerID = null;
		$otherPostKey = "registerID";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
				$registerID = $strValue;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//Si no viene asignado $registerID, sólo se puede asumir que por alguna razón se agregaron nuevos elementos al catálogo por lo que
		//durante esta edición hay mas registros que grabar de los que había originalmente, por tanto se debe hacer una inserción en lugar
		//de actualización
		//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
		//En este caso se recibe como parámetro el ID de la sección dinámica que se está procesando, así que ese se usa directamente
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nis_null(registerID): ".(is_null($registerID)));
			echo("<br>\r\ntrim(registerID): '".(trim($registerID))."'");
			echo("<br>\r\nregisterID === '': ".($registerID === ''));
			echo("<br>\r\nregisterID === 0: ".($registerID === 0)."<br>\r\n");
		}
		
		if (is_null($registerID) || (int) trim($registerID) == 0) {
			//@JAPR 2013-01-22: Agregado el parámetro $bSaveSignature para optimizar en que momento se graba la firma
			//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
			//@JAPR 2014-10-29: Agregado el mapeo de Scores
			//Agregado el parámetro $aMultiRecordNum para indicar el registro de cada sección que se esté procesando
			$strStatus[$i] = saveData($aRepository, $surveyID, $newArrayData, $thisDate, $factKeyDimVal, $sendEmailDashboards, $countSurveys, null, $dynamicSectionID, 0, $strDynamicPageDSC, ($i == 0), $i, $i);
		}
		else {
			//@JAPR 2013-01-22: Agregado el parámetro $bSaveSignature para optimizar en que momento se graba la firma
			//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
			//@JAPR 2014-10-29: Agregado el mapeo de Scores
			//Agregado el parámetro $aMultiRecordNum para indicar el registro de cada sección que se esté procesando
			$strStatus[$i] = updateData($aRepository, $surveyID, $newArrayData, $dateID, $hourID, $userID, $thisDate, $registerID, $dynamicSectionID, 0, $factKeyDimVal, $strDynamicPageDSC, ($i == 0), $i, $i);
		}
		
		if ($strStatus[$i] == "OK") 
		{
			$totalOK ++;
		}
	}
	
	if ($totalOK == $countSurveys) 
	{
		$strStatusDyn = "OK";
	}
	else
	{
		$strStatusDyn = implode(";\r\n", $strStatus);
	}
	return $strStatusDyn;
}

/*Grabado de secciones dinámicas, en este caso las respuestas vienen con los valores directos en los campos QField que les corresponden, excepto 
	para las preguntas de la sección dinámica, las cuales vienen como un Array con "n" elementos que corresponden al total de miembros del atributo
	hijo del que generó las "x" páginas dinámicas, cada elemento corresponde en índice al contenido del parámetro "sectionValue" donde se graba
	la llave subrrogada que corresponde al elemento del atributo, por ejemplo:
	Sección estática:
		QField1 - Pregunta de catálogo que genera las secciones dinámicas (el Atributo genera 3 secciones dinámicas)
		QField2 - Pregunta texto
	Sección dinámica:
		QField3 - Pregunta de Selección múltiple (se guarda 1 | 0 según el estado del checkbox)
		QField4 - Pregunta texto
		Qfield5 - Pregunta de Selección sencilla sin catálogo
	asumiendo que el Atributo hijo del usado en QField1 contiene entre sus 3 valores de los padres 10 posibles valores con la siguiente correspondencia
	Valor 1 del atributo padre: Valores del atributo hijo del 0 al 5
	Valor 2 del atributo padre: Valores del atributo hijo del 6 al 7
	Valor 3 del atributo padre: Valores del atributo hijo del 8 al 9
las respuestas vendrían como:

QField1: key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_ValorAtributoUsadoEnQField1
QField2: Texto capturado como respuesta
QField3: array(0 => 1, 1 => 1, 2 => 0, 3 => 1, 4 => 0, 5 => 1, 6 => 0, 7 => 0, 8 => 0, 9 => 0) (los estados de cada check box para el total de
	elementos del atributo hijo del que se usó para generar las secciones dinámicas, en este ejemplo son 10 valores)
	
QField4: array(0 => "Texto de la 1er subsección dinámica", 1 => "Texto de la 1er subsección dinámica", 2 => "Texto de la 1er subsección dinámica", 
				3 => "Texto de la 1er subsección dinámica", 4 => "Texto de la 1er subsección dinámica", 5 => "Texto de la 1er subsección dinámica", 
	porque la primer subsección dinámica que es el Valor 1 del atributo padre, contiene los valores del atributo hijo del 0 al 5, así que para 
	todos ellos se repite la respuesta capturada en esta pregunta, NO es como en la pregunta múltiple que hay un CheckBox para cada valor				
				6 => "Texto de la 2da subsección dinámica", 7 => "Texto de la 2da subsección dinámica", 
				8 => "Texto de la 3ra subsección dinámica", 9 => "Texto de la 3ra subsección dinámica") (es decir, cada grupo de valores del atributo
	hijo que corresponden al mismo valor del atributo padre repiten la misma respuesta capturada)

QField5:  array(0 => "Opción de la 1er subseccion dinámica", 1 => "Opción de la 1er subseccion dinámica", 2 => "Opción de la 1er subseccion dinámica", 
				3 => "Opción de la 1er subseccion dinámica", 4 => "Opción de la 1er subseccion dinámica", 5 => "Opción de la 1er subseccion dinámica", 
				6 => "Opción de la 2da subseccion dinámica", 7 => "Opción de la 2da subsección dinámica", 
				8 => "Opción de la 3ra subsección dinámica", 9 => "Opción de la 3ra subsección dinámica") (igual que la pregunta anterior sólo que se
	graba el texto de la opción seleccionada ya que en este caso no se captura nada por ser selección sencilla)
	
En la tabla de hechos se grabarán 10 registros, uno por cada posible valor de los atributos hijos con las respuestas de selección múltiple de
cada uno que se encontraban en la sección dinámica, mientras que las respuestas que NO son selección múltiple pero si están en la sección dinámica
repetirán su respuesta agrupando a todos los valores del atributo hijo del mismo padre (en este ejemplo son 3 agrupaciones que corresponden con las
3 subsecciones dinámicas), y cualquier pregunta que NO esté en la sección dinámica repetirá su respuesta para los 10 registros a grabar
*/
//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
//Agregado el parámetro $factKeyDimVal, si este viene asignado entonces quiere decir que proviene de otro grabado previo de la misma captura
//de encuesta sólo que se está particionando el grabado por tipo de sección. Originalmente usado para grabar secciones dinámicas con este método
//posteriormente a haber invocado el grabado de secciones Maestro - Detalle, para permitir que todos los registros de ambos métodos se graben
//con la misma referencia al FactKeyDimVal
function multipleSaveDynamicData($aRepository, $surveyID, $arrayData, $thisDate, $dynamicSectionID, &$factKeyDimVal=0)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	global $arrSectionValuesByMultiChoiceAnswerFromApp;
	global $arrSectionPagesByDesc;
	/*@MABH20121205*/
	global $appVersion;
	//@MABH20121205
	//@JAPR 2013-08-22: Corregido un bug en el grabado de la página cuando no había preguntas múltiple choice
	global $arrSectionDescByID;
	global $arrSectionDescStrByID;
	//@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br><br>\r\n\r\nmultipleSaveDynamicData {$dynamicSectionID}");
	}
	
	//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
	//Ya no es necesario asignar el 0 explícitamente, si el parámetro no se envía entonces tomará el 0 automáticamente, por el contrario reutilizará
	//el valor recibido por ejemplo del grabado de la sección Maestro - Detalle
	//$factKeyDimVal = 0;
	//@JAPR
	$sendEmailDashboards = false;
	
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	
	$newArrayData = array();
	$newArrayData["surveyDate"] = $arrayData["surveyDate"];
	$newArrayData["surveyHour"] = $arrayData["surveyHour"];
	$newArrayData["userID"] = $arrayData["userID"];
	$newArrayData["appUserID"] = $arrayData["appUserID"];
	/*@MABH20121205*/
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$newArrayData["serverSurveyDate"] = (string) @$arrayData["serverSurveyDate"];
		$newArrayData["serverSurveyHour"] = (string) @$arrayData["serverSurveyHour"];
		$newArrayData["serverSurveyEndDate"] = (string) @$arrayData["serverSurveyEndDate"];
		$newArrayData["serverSurveyEndHour"] = (string) @$arrayData["serverSurveyEndHour"];
	}
	//@MABH20121205
	$newArrayData["surveyFinished"] = $arrayData["surveyFinished"];
	//@JAPR 2013-04-10: A este día, cuando se hacía un Reupload no venían estos datos porque llegaban como parámetro $_POST y no como parte del
	//archivo, por lo tanto generaban Warnings así que se validará en lo que se integran al archivo
	$newArrayData["surveyLatitude"] = @$arrayData["surveyLatitude"];
	$newArrayData["surveyLongitude"] = @$arrayData["surveyLongitude"];
	//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
	$newArrayData["surveyAccuracy"] = @$arrayData["surveyAccuracy"];
	//@JAPR
	
	if(isset($arrayData["CaptureVia"]))
	{
		$newArrayData["CaptureVia"] = $arrayData["CaptureVia"];
	}
	
	if(isset($arrayData["CaptureEmail"]))
	{
		$newArrayData["CaptureEmail"] = $arrayData["CaptureEmail"];
	}
	
	if(isset($arrayData["SchedulerID"]))
	{
		$newArrayData["SchedulerID"] = $arrayData["SchedulerID"];
	}
	
	if(isset($arrayData["qsignature"]))
	{
		$newArrayData["qsignature"] = $arrayData["qsignature"];
	}
	
	if(isset($arrayData["captureType"]))
	{
		$newArrayData["captureType"] = $arrayData["captureType"];
	}

	//@JAPR 2012-06-08: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas cuando existe por lo
	//menos una pregunta múltiple en ella, ya que en esos casos sectionValue sólo es un array con una respuesta por subsección dinámica, en lugar
	//de repetir la misma respuesta por cada opción de la múltiple dentro de las subsecciones como sucede con las preguntas Single Choice sin
	//catálogo
	
	//Identifica si es necesario obtener el Array de padres usados para generar las secciones, ya que si no hay pregunta múltiple no es necesario
	$blnGenerateParentValuesArr = false;
	//Contiene la instancia de la pregunta de Single Choice que filtra para generar las subsecciones dinámicas
	$aSCDynQuestionInstance = null;
	$dynamicSection = BITAMSection::NewInstanceWithID($aRepository, $dynamicSectionID);
	$dynamicSectionCatID = $dynamicSection->CatalogID;
	$blnDynamicSectionFound = false;
	//@JAPR
	
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		//@JAPR 2012-06-08: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
		if (!$blnDynamicSectionFound)
		{
			if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID == $dynamicSectionCatID)
			{
				//Como pudiera haber varias preguntas de catálogo sincronizadas, debemos tomar la última para saber exactamente por qué atributo 
				//se debe filtrar
				$aSCDynQuestionInstance = $aQuestion;
			}
		}
		
		//si las preguntas pertenecen a la seccion dinamica y que estan dentro de la seccion
		//entonces si son repetibles y se ignoran dichas preguntas
		if($aQuestion->SectionID == $dynamicSectionID)
		{
			$blnDynamicSectionFound = true;
			if ($aQuestion->QTypeID == qtpMulti)
			{
		        //@JAPR 2012-06-29: Corregido un bug, no había por qué hacer este Break, eso provocaba que ya no se grabaran las preguntas posteriores a las dinámicas
				$blnGenerateParentValuesArr = true;
				//break;
				//@JAPR
			}
			continue;
		}
		
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		//Si la encuesta está configurada para grabar un registro único para secciones estándar, ya no procesa ningún otro tipo de pregunta
		//para no provocar que se repitan sus valores
		//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
		//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
		//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
		if ($surveyInstance->UseStdSectionSingleRec && ($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)) {
			continue;
		}
		//@JAPR
		
		//Procesamiento del dato de qfield
		$postKey = "qfield".$aQuestion->QuestionID;
		if(isset($arrayData[$postKey]))
		{
			$strValue = $arrayData[$postKey];
		}
		else 
		{
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:(float)0);
			}
			else 
			{
				$strValue = "";
			}
		}

		$newArrayData[$postKey] = $strValue;
		
		//Procesamiento del dato de qimageencode
		$otherPostKey = "qimageencode".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = "";
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Procesamiento del dato de comment
		$otherPostKey = "qcomment".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
		//Procesamiento del dato de document
		$otherPostKey = "qdocument".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		//@JAPR
		
		//Procesamiento del dato de action
		$otherPostKey = "qactions".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
	}
	
	//Realizamos coleccion de preguntas de la seccion dinamica
	$otherQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($aRepository, $dynamicSectionID);

	//@JAPR 2012-06-08: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas cuando existe por lo
	//menos una pregunta múltiple en ella, ya que en esos casos sectionValue sólo es un array con una respuesta por subsección dinámica, en lugar
	//de repetir la misma respuesta por cada opción de la múltiple dentro de las subsecciones como sucede con las preguntas Single Choice sin
	//catálogo
	$arrSectionValuesByMultiChoiceAnswer = array();		//Array indexado por Key de las respuestas múltiples conteniendo el número de página
	$arrPagesByAttribDesc = array();					//Array indexado por Descripción conteniendo el número de página
	$arrDynSectionValues = array();
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\nblnGenerateParentValuesArr: $blnGenerateParentValuesArr");
		echo("<br>\r\nIsNull(aSCDynQuestionInstance): ".(is_null($aSCDynQuestionInstance)));
	}
	if ($blnGenerateParentValuesArr && !is_null($aSCDynQuestionInstance))
	{
		//Obtiene el atributo de la última pregunta previa a la sección dinámica, ya que de esa pregunta se obtendrá el valor a filtrar
		//El valor en este punto viene como una lista de los pares "key_AttributeID_SVSep_Value" separada por "_SVElem_", así que obtiene el
		//último pues ese es el filtro sobre el cual se deben obtener los valores del catálogo para el atributo de la sección dinámica y el de
		//las preguntas multiple choice
		$postKey = "qfield".$aSCDynQuestionInstance->QuestionID;
		$strFilterValue = (string) @$arrayData[$postKey];
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nstrFilterValue $postKey: $strFilterValue");
		}
		$intDynamicSectionAttribParentID = 0;
		$intMultiAttribParentID = 0;
		$aCatMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $dynamicSection->CatMemberID);
		if (!is_null($aCatMember))
		{
			$intDynamicSectionAttribParentID = $aCatMember->ClaDescrip;
			//Obtiene el siguiente atributo del catálogo, ya que ese será el usado por las preguntas múltiples de la sección dinámica 
			$sql = "SELECT ParentID FROM SI_SV_CatalogMember 
				WHERE CatalogID = ".$aCatMember->CatalogID." AND MemberOrder > ".$aCatMember->MemberOrder." ORDER BY MemberOrder";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF)
			{
				$intMultiAttribParentID = (int) @$aRS->fields["parentid"];
			}
		}
		
		if (trim($strFilterValue) != '' && $intDynamicSectionAttribParentID > 0 && $intMultiAttribParentID > 0)
		{
			//Hay una captura de la pregunta que genera las dinámicas, así que obtenemos los valores basados en el seleccionado en esta pregunta
			$arrDynSectionValues = getAllDynPagesCatValues($aRepository, $surveyID, $dynamicSectionID, $intDynamicSectionAttribParentID, $intMultiAttribParentID, $strFilterValue);
			if (is_null($arrDynSectionValues))
			{
				return($gblEFormsErrorMessage);
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\narrDynSectionValues:");
				PrintMultiArray($arrDynSectionValues);
			}
			
			//Recorre el array indexado por los Keys de las opciones de respuesta del atributo indicados en sectionValues, con el valor obtenido
			//se va llenando otro array que representa a las páginas creadas, como los valores en sectionValue vienen ordenados por páginas,
			//se generan nuevos índices del array de páginas hasta que se cambia la descripción obtenida
			$intDynamicPageNumber = -1;
			//$arrPagesByAttribDesc = array();	//Array indexado por Descripción conteniendo el número de página
			//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
			//Se reutilizará el array global que ya contiene las descripciones de páginas indexadas por su posición
			if ($surveyInstance->DissociateCatDimens && isset($arrSectionValuesByMultiChoiceAnswerFromApp[$dynamicSectionID]) && isset($arrSectionPagesByDesc)) {
				if (isset($arrSectionPagesByDesc[$dynamicSectionID])) {
					$arrPagesByAttribDesc = array_flip($arrSectionPagesByDesc[$dynamicSectionID]);
				}
			}
			//@JAPR
			
			$postKey = "sectionValue";
			$countSurveys = count($arrayData[$postKey]);
			$otherPostKey = "sectionValue";
			for($i=0; $i<$countSurveys; $i++)
			{
				$intSectionValue = 0;
				if(isset($arrayData[$otherPostKey]))
				{
					if(isset($arrayData[$otherPostKey][$i]))
					{
						$intSectionValue = $arrayData[$otherPostKey][$i];
					}
					else 
					{
						//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
						//En v4 no se supone que jamás llegue intencionalmente un key subrrogado de catálogo == 1 (Registro de No Aplica), ya que
						//si la sección dinámica se hubiera saltado, entonces simplemente no hubieran llegado datos de la sección y por tanto no
						//hubiera entrado a este método. Esta validación tal vez aplicaba para v3 o anteriores así que en v4 se usará solo para
						//casos donde dentro de StoreSurveyCapture se determine que los keys recibidos como parte de la sección ya no son válidos
						//en el catálogo (incluso después de intentar remapearlos a partir de las descripciones) por lo que se forzará a usar NA
						//para sus valores, en ese caso excepción la página se determinará por la posición de este SectionValue en lugar de por 
						//el propio key como con los demás valores
						$intSectionValue = 1;
					}
				}
				
				//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				//Cuando se graba a partir de las descripciones recibidas, no se debe usar el array con los datos actuales en el catálogo porque
				//podrían tener descripciones alteradas los nuevos elementos lo cual reordenaría las páginas dinámicas, así que no hay seguridad
				//que aunque algunos keys fueron reutilizados (ya que otros pudieron haberse remapeado o cambiado a NA) las páginas serán tal
				//como se capturaron, por tanto mejor se usará el mismo array recibido en los datos. Esto solo se puede hacer si se utilizan
				//dimensiones independientes del catálogo, de lo contrario se tiene que respetar todo como estén los keys del catálogo
				if ($intSectionValue == 1 || ($surveyInstance->DissociateCatDimens && isset($arrSectionValuesByMultiChoiceAnswerFromApp[$dynamicSectionID][$i]))) {
					//En este caso el key subrrogado no pudo ser remapeado al catálogo actual, así que se procesará el valor para respetar la
					//captura pero ya no se quedará asociado a ningún elemento. Las páginas son determinadas por la posición en lugar de por el
					//propio key, por lo tanto se genera un array en lugar de asignar un valor directo
					if ($intSectionValue == 1) {
						if (!isset($arrSectionValuesByMultiChoiceAnswer[$intSectionValue])) {
							$arrSectionValuesByMultiChoiceAnswer[$intSectionValue] = array();
						}
						
						//Para este proceso se reutilizará el array global de páginas por índice de la sección dinámica, el cual se obtiene a partir
						//de las descripciones recibidas por el App
						//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
						$arrSectionValuesByMultiChoiceAnswer[$intSectionValue][$i] = (int) @$arrSectionValuesByMultiChoiceAnswerFromApp[$dynamicSectionID][$i];
					}
					else {
						$arrSectionValuesByMultiChoiceAnswer[$intSectionValue] = (int) @$arrSectionValuesByMultiChoiceAnswerFromApp[$dynamicSectionID][$i];
					}
				}
				//@JAPR 2013-02-12: Agregada la edición de datos históricos
				//Si es un key subrrogado positivo quiere decir que se trata de una captura donde usaron el catálogo actual, de lo contrario si
				//es un key subrrogado negativo y exclusivamente si se usan catálogos desasociados de sus dimensiones significaría que es una
				//edición en la cual no se cambió la combinación que generaba la página dinámica respecto a lo grabado previamente, así que el
				//array $arrDynSectionValues realmente contiene keys negativos basados en el orden grabado de la sección dinámica. Un key
				//subrrogado de 0 significaría que no hay respuesta así que ese caso no se procesa
				elseif ($intSectionValue > 0)
				{
					//Con el Key del atributo de las múltiples se obtiene la descripción de la subpágina dinámica, y con ella se obtiene el número
					//de página, ya que si aun no estaba registrada entonces se agrega
					$strPageValueDesc = @$arrDynSectionValues[$intSectionValue];
					if (is_null($strPageValueDesc))
					{
						//Es una descripción que no existe, por lo tanto no se asigna un número de página para que genere un valor vacio al grabar
					}
					else 
					{
						//Se genera la página o se reutiliza el número de la previamente generada
						if (!isset($arrPagesByAttribDesc[$strPageValueDesc]))
						{
							//Genera una nueva página correspondiente a esta descripción de subsección dinámica
							$intDynamicPageNumber++;
							$arrPagesByAttribDesc[$strPageValueDesc] = $intDynamicPageNumber;
						}
						$arrSectionValuesByMultiChoiceAnswer[$intSectionValue] = $arrPagesByAttribDesc[$strPageValueDesc];
					}
				}
			}
		}
	}
	//@JAPR
	
	//Buscamos las preguntas de la seccion dinamica y nos basamos en el index de la variable $i
	$postKey = "sectionValue";
	$countSurveys = count($arrayData[$postKey]);

	$i=0;
	$strStatus = array ();
	$totalOK = 0;
	//@JAPR 2013-01-12: Modificado para que ahora sólo un registro (en caso de haber multiple-choice) contenga datos de las preguntas Open o que
	//no corresponden con las multiple-choice, ya que si se repiten no se pueden estimar correctamente
	$arrDynamicPagesProcessed = array();
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\narrSectionValuesByMultiChoiceAnswer");
		PrintMultiArray($arrSectionValuesByMultiChoiceAnswer);
		echo("<br>\r\narrPagesByAttribDesc");
		PrintMultiArray($arrPagesByAttribDesc);
	}
	
	//@JAPR 2013-01-12: Modificado para que ahora sólo un registro (en caso de haber multiple-choice) contenga datos de las preguntas Open o que
	//no corresponden con las multiple-choice, ya que si se repiten no se pueden estimar correctamente
	$arrPagesByNum = array_flip($arrPagesByAttribDesc);
	for($i=0; $i<$countSurveys; $i++)
	{
		//@JAPR 2012-06-11: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
		//Movido este código al inicio del ciclo ya que se usará para obtener los valores de las preguntas Open
		$intSectionValue = false;
		//Procesamiento del dato de sectionValue
		$otherPostKey = "sectionValue";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
				//Asigna el valor del elemento múltiple choice (o single choice si no había preguntas múltiple choice) que se está grabando en 
				//este registro para mas adelante usarlo al obtener el índice de la respuesta Open (o cualquiera que no se manda repetida desde 
				//la App) que corresponde a esta página. Sólo aplica si existen preguntas múltiple choice
				$intSectionValue = (int) $strValue;
			}
			else 
			{
				$strValue = 1;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//@JAPR 2013-01-12: Modificado para que ahora sólo un registro (en caso de haber multiple-choice) contenga datos de las preguntas Open o que
		//no corresponden con las multiple-choice, ya que si se repiten no se pueden estimar correctamente
		//Si la pregunta no corresponde con una multiple-choice, como está dentro de la sección dinámica, usando la nueva opción de Single record
		//para estándar, NO grabará repetidas estas preguntas pues alterarían los cálculos, la grabará una sola vez por página
		$strDynamicPageDSC = '';
		$blnCleanNonMCFields = false;
		if ($surveyInstance->UseStdSectionSingleRec) {
			//Solo podemos hacer este ajuste si hay preguntas multiple-choice y se recibió información suficiente para poder identificar las
			//páginas, de lo contrario se considera que es un grabado donde no hay multiple-choice y se graba el mismo sectionValue en el
			//nuevo campo, sólo para que se considere que se deben usar estos valores en preguntas que no son multi-choice al ver el reporte
			if ($blnGenerateParentValuesArr && $intSectionValue !== false && isset($arrSectionValuesByMultiChoiceAnswer[$intSectionValue])) {
				//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				$intPageNum = null;
				if ($intSectionValue == 1) {
					//Si el SectionValue es el registro de no aplica, entonces significa que no se pudo mapear el elemento recibido a como está
					//el catálogo actualmente, pero como pudieran haber varios casos, se usará la posición específica del SectionValue para 
					//determinar en qué página se encuentra
					$arrPagesForNA = $arrSectionValuesByMultiChoiceAnswer[$intSectionValue];
					if (is_array($arrPagesForNA) && isset($arrPagesForNA[$i])) {
						//Si se encontró la página por lo que la reutiliza
						$intPageNum = (int) $arrPagesForNA[$i];
					}
					else {
						//En este caso no hay por qué o no hay como procesar las páginas, así que se graba un valor genérico
						$strDynamicPageDSC = 'sectionValue-'.$intSectionValue;
					}
				}
				else {
					$intPageNum = $arrSectionValuesByMultiChoiceAnswer[$intSectionValue];
				}
				
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\nPage number identified: $intPageNum");
				}
				
				if (!is_null($intPageNum)) {
					//Cualquier otra pregunta en sección dinámica sólo debe grabar un valor por página dinámica, así que verificamos si para
					//la página actualmente procesada ya se grabó un valor, de no ser así entonces si se incluyen los datos en el registro
					if (!isset($arrDynamicPagesProcessed[$intPageNum])) {
						//El valor del nuevo campo sólo se debe asignar en el registro que representa a la página dinámica, en el resto debe ir vacio
						$strDynamicPageDSC = (string) @$arrPagesByNum[$intPageNum];
						$arrDynamicPagesProcessed[$intPageNum] = $strDynamicPageDSC;
					}
					else {
						$blnCleanNonMCFields = true;
					}
				}
			}
			else {
				//En este caso no hay por qué o no hay como procesar las páginas, así que se graba un valor genérico
				//@JAPR 2013-08-22: Corregido un bug en el grabado de la página cuando no había preguntas múltiple choice
				//Entrar aquí signifca una de 3 cosas:
				//1- Que el valor del key de catálogo no viene asignado, lo cual no es posible tecnicamente y si lo hiciera, simplemente va a
				//ocurrir otro error mas adelante, así que da lo mismo
				//2- Que no estuviera asignado $arrSectionValuesByMultiChoiceAnswer para este key, lo cual tampoco sería posible porque se procesan
				//todos los keys recibidos en el Post
				//3- Que simplemente no hay preguntas múltiple choice en la dinámica, por lo que $blnGenerateParentValuesArr == false y por tanto
				//$arrSectionValuesByMultiChoiceAnswer está vacio, si ese es el caso, entonces se debe usar como página el mismo valor que se
				//usará como opción (es decir, el que corresponde en el catálogo al key subrrogado procesado) por lo que no se debe grabar
				//sectionValue- en esos casos
				if (!$blnGenerateParentValuesArr && $surveyInstance->DissociateCatDimens && isset($arrSectionDescByID) && isset($arrSectionDescStrByID)) {
					//@JAPR 2013-08-22: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
					//Si se graba a partir de las descripciones, no puede ir a traer el String de la combinación ni la descripción
					//del checkbox al catálogo porque el valor probablemente pudo haber cambiado, y aunque para este punto si fuera un
					//key > 0 ya se habría remapeado, aquellos que fueron eliminados grabarían el valor de *NA así que mejor se usará
					//directamente el dato que llegó desde el App en esos casos
					$strDynamicOptionDSC = '';
					$arrSectionDesc = @$arrSectionDescByID[$dynamicSectionID][$i];
					$strNewCatDimValAnswers = @$arrSectionDescStrByID[$dynamicSectionID][$i];
					if (!is_null($strNewCatDimValAnswers) && !is_null($arrSectionDesc) && is_array($arrSectionDesc) && count($arrSectionDesc) > 0) {
						//@JAPR 2013-05-09: Corregido un bug con la codificación
						//Este valor SI se debe decodificar, porque se usará para grabar en las tablas de datos
						//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
						$strDynamicOptionDSC = (string) @$arrSectionDesc[count($arrSectionDesc) -1];
					}
					else {
						$arrayMapStrValues = array();
						$arrayMapStrValues[0] = $intSectionValue;
						$arrDynamicRecValue = getCatValueDescForSurveyField($aRepository, $arrayMapStrValues, $dynamicSectionCatID);
						if (!is_array($arrDynamicRecValue) || count($arrDynamicRecValue) < 2) {
							$arrDynamicRecValue = array();
						}
						$strDynamicOptionDSC = (string) @$arrDynamicRecValue[1];
					}
					if (trim($strDynamicOptionDSC) == '') {
						$strDynamicPageDSC = 'sectionValue-'.$intSectionValue;
					}
					else {
						$strDynamicPageDSC = $strDynamicOptionDSC;
					}
				}
				else {
					$strDynamicPageDSC = 'sectionValue-'.$intSectionValue;
				}
			}
		}
		//@JAPR
		
		foreach ($otherQuestionCollection->Collection as $questionKey => $aQuestion)
		{
			//Procesamiento del dato de qfield
			//@JAPR 2012-06-11: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
			if ($blnGenerateParentValuesArr && $intSectionValue !== false && isset($arrSectionValuesByMultiChoiceAnswer[$intSectionValue]) && ($aQuestion->GQTypeID == qtpOpen || $aQuestion->QTypeID == qtpCalc))
			{
				//Si entra aquí quiere decir que es un tipo de pregunta que en la sección dinámica NO se envía repetida desde la App, sino que
				//se manda sólo una respuesta por página, así que basandose en el sectionValue se determina a que página corresponde este registro
				//y se pide la respuesta de la pregunta con ese índice de página
				$postKey = "qfield".$aQuestion->QuestionID;
				if(isset($arrayData[$postKey]))
				{
					//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
					$intPageNum = null;
					if ($intSectionValue == 1) {
						//Si el SectionValue es el registro de no aplica, entonces significa que no se pudo mapear el elemento recibido a como está
						//el catálogo actualmente, pero como pudieran haber varios casos, se usará la posición específica del SectionValue para 
						//determinar en qué página se encuentra
						$arrPagesForNA = $arrSectionValuesByMultiChoiceAnswer[$intSectionValue];
						if (is_array($arrPagesForNA) && isset($arrPagesForNA[$i])) {
							//Si se encontró la página por lo que la reutiliza
							$intPageNum = (int) $arrPagesForNA[$i];
						}
					}
					else {
						$intPageNum = $arrSectionValuesByMultiChoiceAnswer[$intSectionValue];
					}
					
					//Ahora se deberá validar por null antes de intentar usar el número de página por si este valor ya no existiera en el catálogo
					if(!is_null($intPageNum) && isset($arrayData[$postKey][$intPageNum]))
					{
						$strValue = $arrayData[$postKey][$intPageNum];
					}
					else 
					{
						//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
						if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
						{
							//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
							$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
						}
						else 
						{
							$strValue = "";
						}
					}
					//@JAPR
				}
				else 
				{
					//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
					if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					else 
					{
						$strValue = "";
					}
				}
			}
			//@JAPR
			else
			{
				$postKey = "qfield".$aQuestion->QuestionID;
				if(isset($arrayData[$postKey]))
				{
					if(isset($arrayData[$postKey][$i]))
					{
						$strValue = $arrayData[$postKey][$i];
					}
					else 
					{
						//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
						if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
						{
							//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
							$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
						}
						else 
						{
							$strValue = "";
						}
					}
				}
				else 
				{
					//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
					if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					else 
					{
						$strValue = "";
					}
				}
			}

			//@JAPR 2013-01-12: Modificado para que ahora sólo un registro (en caso de haber multiple-choice) contenga datos de las preguntas Open o que
			//no corresponden con las multiple-choice, ya que si se repiten no se pueden estimar correctamente
			//Si la pregunta no corresponde con una multiple-choice, como está dentro de la sección dinámica, usando la nueva opción de Single record
			//para estándar, NO grabará repetidas estas preguntas pues alterarían los cálculos, la grabará una sola vez por página
			//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
			//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
			//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
			if ($surveyInstance->UseStdSectionSingleRec && $blnCleanNonMCFields && ($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)) {
				//Solo podemos hacer este ajuste si hay preguntas multiple-choice y se recibió información suficiente para poder identificar las
				//páginas, de lo contrario se considera que es un grabado donde no hay multiple-choice y se graba el mismo sectionValue en el
				//nuevo campo, sólo para que se considere que se deben usar estos valores en preguntas que no son multi-choice al ver el reporte
				if ($aQuestion->QTypeID == qtpMulti) {
					//Las preguntas múltiple choice deben variar su valor por registro, así que con ellas no se hace ajuste
				}
				else {
					//Cualquier otra pregunta en sección dinámica sólo debe grabar un valor por página dinámica, así que verificamos si para
					//la página actualmente procesada ya se grabó un valor, de no ser así entonces si se incluyen los datos en el registro
					//Esta página ya había grabado un registro, por lo tanto ya no incluye de nuevo las respuestas de estas preguntas
					//y reemplaza su valor por vacio según el tipo
					if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					else 
					{
						$strValue = "";
					}
				}
			}
			//@JAPR
			$newArrayData[$postKey] = $strValue;
			
			//Procesamiento del dato de qimageencode
			$otherPostKey = "qimageencode".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = "";
				}
			}
			else 
			{
				$strValue = "";
			}
			$newArrayData[$otherPostKey] = $strValue;
			
			//Procesamiento del dato de comment
			$otherPostKey = "qcomment".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = null;
				}
			}
			else 
			{
				$strValue = null;
			}
			$newArrayData[$otherPostKey] = $strValue;

			//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
			//Procesamiento del dato de document
			$otherPostKey = "qdocument".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = null;
				}
			}
			else 
			{
				$strValue = null;
			}
			$newArrayData[$otherPostKey] = $strValue;
			//@JAPR
			
			//Procesamiento del dato de action
			$otherPostKey = "qactions".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = null;
				}
			}
			else 
			{
				$strValue = null;
			}
			$newArrayData[$otherPostKey] = $strValue;
		}
		
		//@JAPR 2012-06-11: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
		//Movido este código al inicio del ciclo ya que se usará para obtener los valores de las preguntas Open
		//Procesamiento del dato de sectionValue
		/*
		$otherPostKey = "sectionValue";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
			}
			else 
			{
				$strValue = 1;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		*/
		
		//@JAPRWarning: Validar que al integrar dinámicas con maestro - detalle, sólo se envíe el EMail una vez, ya que al llegar
		//a este método si proviene del que graba Maestro - Detalle entonces ya se debería haber enviado este EMail
		if($i==($countSurveys-1))
		{
			$sendEmailDashboards = true;
		}
		else 
		{
			$sendEmailDashboards = false;
		}
		
		//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
		//En este caso se recibe como parámetro el ID de la sección dinámica que se está procesando, así que ese se usa directamente
		//@JAPR 2013-01-22: Agregado el parámetro $bSaveSignature para optimizar en que momento se graba la firma
		//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
		//@JAPR 2014-10-29: Agregado el mapeo de Scores
		//Agregado el parámetro $aMultiRecordNum para indicar el registro de cada sección que se esté procesando
		$strStatus[$i] = saveData($aRepository, $surveyID, $newArrayData, $thisDate, $factKeyDimVal, $sendEmailDashboards, $countSurveys, null, $dynamicSectionID, 0, $strDynamicPageDSC, ($i == 0), $i, $i);

		if ($strStatus[$i] == "OK") 
		{
			$totalOK ++;
		}
	}
	
	if ($totalOK == $countSurveys) 
	{
		$strStatusDyn = "OK";
	}
	else
	{
		$strStatusDyn = implode(";\r\n", $strStatus);
	}
	
	return $strStatusDyn;
}

//@JAPR 2012-04-12: Agregadas las secciones Maestro - Detalle
/*Grabado de secciones Maestro - Detalle, en este caso las respuestas vienen con los valores directos en los campos QField que les corresponden, 
	excepto para las preguntas de las secciones Maestro - Detalle, las cuales vienen como un Array con "n" elementos cuya cantidad es distinta
	para cada una de las secciones Maestro - Detalle de la encuesta (puede haber varias), y cada elemento si contiene la respuesta directa de esa
	pregunta para esa instancia de "Detalle" tal como se grabaría en una pregunta normal, ya que todas las preguntas de la misma sección Maestro - 
	Detalle contendrán en sus arrays la misma cantidad de elementos y el total de respuestas de un índice en particular en conjunto forman un
	registro de la Fact agregandoles a las respuestas de otras secciones Maestro - Detalle el valor de No Aplica, y a las preguntas de secciones
	que NO son Maestro - Detalle el valor repetido para todos los detalles a capturar, por ejemplo lo que se recibiría es lo siguiente:
	Sección estática:
		QField1- Pregunta de catálogo selección sencilla
		QField2- Pregunta texto
	Sección Maestro - Detalle 1:
		QField3- Pregunta numérica
		QFueld4- Pregunta de catálogo selección sencilla
	Sección Maestro - Detalle 2:
		QField5 - Pregunta texto
		QField6 - Pregunta selección sencilla
asumiendo que la primer sección Maestro - Detalle capturó 3 registros y la segunda sólo capturó 2 las respuestas vendrían como:

QField1: key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_ValorAtributoUsadoEnQField1
QField2: Texto capturado como respuesta
QField3: array(0 => 34, 1 => 56, 2 => 90) (es decir, los números para cada uno de los 3 registros de la primer sección Maestro - Detalle)
QField4: array(0 => key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_Valor2doAtributoCatalogo_SVElem_key_10109_SVSep_ValorAtributoUsadoEnQField4, 
			   1 => key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_Valor2doAtributoCatalogo_SVElem_key_10109_SVSep_ValorAtributoUsadoEnQField4, 
			   2 => key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_Valor2doAtributoCatalogo_SVElem_key_10109_SVSep_ValorAtributoUsadoEnQField4) 
			   (es decir, los valores seleccionados del catálogo para cada uno de los 3 registros de la primer sección Maestro - Detalle)
QField5: array(0 => "Texto del primer registro", 1 => "Texto del segundo registro") (es decir, los textos para cada uno de los 2 registros de la segunda sección Maestro - Detalle)
QField5: array(0 => "Opción del primer registro", 1 => "Opción del segundo registro") (es decir, los textos para cada uno de los 2 registros de la segunda sección Maestro - Detalle)

dónde los índices 0 y 1 de ambas secciones Maestro - Detalle NO corresponden para nada entre si, en la Fact los índices 0, 1 y 2 de la primer
sección Maestro - Detalle tendrán como valor de las preguntas de la 2da sección Maestro - Detalle el valor de No Aplica, mientras que en 2 registros
independientes los índices 0 y 1 de la segunda sección Maestro - Detalle harán lo propio. En los 5 índices combinados las preguntas que NO son
de secciones Maestro - Detalle contendrán sus valores correspondientes repetidos
*/
function multipleUpdateMasterDetData($aRepository, $surveyID, $arrayData, $dateID, $hourID, $userID, $thisDate, $aMasterDetSectionsColl, &$factKeyDimVal=0)
{
	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	global $arrMasterDetSections;
	global $arrMasterDetSectionsIDs;
	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	global $arrInlineSections;
	global $arrInlineSectionsIDs;
	/*@MABH20121205*/
	global $appVersion;
	//@MABH20121205
	
	//@JAPR 2012-06-05: Integradas las secciones dinámicas con las Maestro - Detalle
    $dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $surveyID);
    $hasDynamicSection = false;
	
    if ($dynamicSectionID > 0) {
        //Aqui verificamos con el sectionValue que realmente si se crearon arreglos
        //de preguntas en las encuestas para poder capturar respuestas para mas de una
        //encuesta
        $postKey = "sectionValue";
        if (isset($arrayData[$postKey])) {
            $hasDynamicSection = true;
        }
    }
    //@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br><br>\r\n\r\nmultipleUpdateMasterDetData");
	}
    
	//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
	//Ya no es necesario asignar el 0 explícitamente, si el parámetro no se envía entonces tomará el 0 automáticamente, por el contrario reutilizará
	//el valor recibido desde otro grabado (al momento de programar esto, sólo el grabado de Maestro - Detalle invocaba a otros métodos)
	//$factKeyDimVal = 0;
	//@JAPR
	$sendEmailDashboards = false;
	
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	
	$newArrayData = array();
	$newArrayData["surveyDate"] = $arrayData["surveyDate"];
	$newArrayData["surveyHour"] = $arrayData["surveyHour"];
	$newArrayData["userID"] = $arrayData["userID"];
	$newArrayData["appUserID"] = $arrayData["appUserID"];
	
	if(isset($arrayData['entryID']))
	{
		$newArrayData["entryID"] = $arrayData['entryID'];
	}
	
	/*@MABH20121205*/
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$newArrayData["serverSurveyDate"] = (string) @$arrayData["serverSurveyDate"];
		$newArrayData["serverSurveyHour"] = (string) @$arrayData["serverSurveyHour"];
		$newArrayData["serverSurveyEndDate"] = (string) @$arrayData["serverSurveyEndDate"];
		$newArrayData["serverSurveyEndHour"] = (string) @$arrayData["serverSurveyEndHour"];
	}
	//@MABH20121205	
	$newArrayData["surveyFinished"] = $arrayData["surveyFinished"];
	//@JAPR 2013-04-10: A este día, cuando se hacía un Reupload no venían estos datos porque llegaban como parámetro $_POST y no como parte del
	//archivo, por lo tanto generaban Warnings así que se validará en lo que se integran al archivo
	$newArrayData["surveyLatitude"] = @$arrayData["surveyLatitude"];
	$newArrayData["surveyLongitude"] = @$arrayData["surveyLongitude"];
	//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
	$newArrayData["surveyAccuracy"] = @$arrayData["surveyAccuracy"];
	//@JAPR
	
	if(isset($arrayData["CaptureVia"]))
	{
		$newArrayData["CaptureVia"] = $arrayData["CaptureVia"];
	}
	
	if(isset($arrayData["CaptureEmail"]))
	{
		$newArrayData["CaptureEmail"] = $arrayData["CaptureEmail"];
	}
	
	if(isset($arrayData["SchedulerID"]))
	{
		$newArrayData["SchedulerID"] = $arrayData["SchedulerID"];
	}
	
	if(isset($arrayData["qsignature"]))
	{
		$newArrayData["qsignature"] = $arrayData["qsignature"];
	}
	
	if(isset($arrayData["captureType"]))
	{
		$newArrayData["captureType"] = $arrayData["captureType"];
	}

	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	//Ahora estas variables son globales
	//$arrMasterDetSectionsIDs = array_flip($arrMasterDetSections);
	//@JAPR
	//Array de preguntas que corresponden con las secciones Maestro - Detalle indexadas por el Id de la sección
	$arrQuestionsByMasterDetSection = array();
	$arrRecNumberByMasterDetSection = array();
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		$postKey = "qfield".$aQuestion->QuestionID;

		//Si las preguntas pertenecen a la seccion Maestro - Detalle y que estan dentro de la seccion
		//entonces si son repetibles y se ignoran dichas preguntas
		if(isset($arrMasterDetSectionsIDs[$aQuestion->SectionID]))
		{
			//Se agregan las preguntas al array indexado por sección para ser procesadas múltiples veces mas adelante
			$arrMasterDetQuestionsColl = @$arrQuestionsByMasterDetSection[$aQuestion->SectionID];
			if (is_null($arrMasterDetQuestionsColl))
			{
				$arrMasterDetQuestionsColl = BITAMQuestionCollection::NewInstanceEmpty($aRepository, $aQuestion->SurveyID);
				$arrQuestionsByMasterDetSection[$aQuestion->SectionID] = $arrMasterDetQuestionsColl;
				//@JAPR 2012-10-24: Modificada la forma de identificar si hay o no registros para la sección Maestro-Detalle
				$intCurrRecNumber = (int) @$arrRecNumberByMasterDetSection[$aQuestion->SectionID];
				$masterDetKey = "masterSectionRecs".$aQuestion->SectionID;
				$intNewRecNumber = 0;
				if (isset($arrayData[$masterDetKey])) {
					if (!is_array($arrayData[$masterDetKey])) {
						$intNewRecNumber = (int) $arrayData[$masterDetKey];
					}
					else {
						$intNewRecNumber = (int) count($arrayData[$masterDetKey]);
					}
				}
				/*
				if (isset($arrayData[$postKey]))
				{
					//$arrRecNumberByMasterDetSection[$aQuestion->SectionID] = count($arrayData[$postKey]);
					$intNewRecNumber = count($arrayData[$postKey]);
				}
				else 
				{
					//$arrRecNumberByMasterDetSection[$aQuestion->SectionID] = 0;
					$intNewRecNumber = 0;
				}
				*/
				
				if ($intNewRecNumber >= $intCurrRecNumber) {
					$arrRecNumberByMasterDetSection[$aQuestion->SectionID] = $intNewRecNumber;
				}
				//@JAPR
			}
			$arrMasterDetQuestionsColl->Collection[] = $aQuestion;
			
			continue;
		}
		
		//@JAPR 2012-06-05: Integradas las secciones dinámicas con las Maestro - Detalle
		//Si la pregunta corresponde a una sección Dinámica, como se encuentra en esta sección quiere decir que por lo menos una sección
		//Maestro - Detalle si traía valores, así que en este caso simplemente asigna NA a todas las preguntas que sean de secciones dinámicas
		//puesto que esas se grabarán cuando al salir de este método se invoque a multipleSaveDynamicData
		
		//si las preguntas pertenecen a la seccion dinamica entonces se ignoran dichas preguntas porque se grabarán al terminar este método y 
		//en los registros de las secciones Maestro - Detalle deben ir como NA
		if($hasDynamicSection && $aQuestion->SectionID == $dynamicSectionID)
		{
			//@JAPR 2012-06-07: Corregido un bug, cuando se combinan Maestro - Detalle con dinámicas, como se estaba dejando sin asignar el 
			//qfield las preguntas que son numéricas marcaban un error de truncado porque les intentaban asignar ''
			//Se agregó la validación para que en caso de ser una pregunta que se graba en campo numérico asigne un 0 (multiples-> numéricas o checkbox)
			if ($aQuestion->QTypeID == qtpMulti && ($aQuestion->MCInputType == mpcCheckBox || $aQuestion->MCInputType == mpcNumeric))
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$newArrayData[$postKey] = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			//@JAPR
			continue;
		}
		//@JAPR
		
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		//Si la encuesta está configurada para grabar un registro único para secciones estándar, ya no procesa ningún otro tipo de pregunta
		//para no provocar que se repitan sus valores
		//@JAPR 2013-01-17: Corregido un bug, se habían excluido de los registros de maestro-detalle las preguntas de catálogo de las secciones
		//estándar, por lo tanto no se podía agrupar por ellas al consultar desde el EAS. En este caso se dejará el mismo valor seleccionado
		//para la pregunta estándar
		//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
		//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
		//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
		//if ($surveyInstance->UseStdSectionSingleRec && !($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1 && $aQuestion->CatalogID>0)) {
		if ($surveyInstance->UseStdSectionSingleRec && ($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)) {
			continue;
		}
		//@JAPR
		
		//Procesamiento del dato de qfield
		if(isset($arrayData[$postKey]))
		{
			$strValue = $arrayData[$postKey];
		}
		else 
		{
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			else 
			{
				$strValue = "";
			}
		}

		$newArrayData[$postKey] = $strValue;
		//@JAPR 2013-08-21: Corregido un bug, durante la edición cuando se agregan nuevos registros, se debe
		//validar para no usar NOCHANGE sino el valor de catálogo correcto
		//Se pasa al array a utilizar el valor de la combinación de catálogo para usarlo en caso de requerir grabar un nuevo registro
		if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID > 0 && (string) $strValue == "NOCHANGE" && isset($arrayData[$postKey.'Cat'])) {
			$newArrayData[$postKey.'Cat'] = $arrayData[$postKey.'Cat'];
		}
		//@JAPR
		
		//Procesamiento del dato de qimageencode
		$otherPostKey = "qimageencode".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = "";
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Procesamiento del dato de comment
		$otherPostKey = "qcomment".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;

		//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
		//Procesamiento del dato de comment
		$otherPostKey = "qdocument".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		//@JAPR
		
		//Procesamiento del dato de action
		$otherPostKey = "qactions".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
	}
	
	//Realizamos coleccion de preguntas de la seccion Maestro - Detalle
	//Por cada sección Maestro - Detalle se agregarán tantos registros como respuestas se hubieran capturado para sus preguntas, sin embargo
	//para otras secciones Maestro - Detalle se deberán de asignar valores de No Aplica para sus preguntas, por lo que el total de registros a
	//agregar es la suma de los totales de registros de cada sección Maestro - Detalle, o bien sólo uno en caso de que ninguna tuviera registros
	//(que eso es lo mismo a no haber contestado ninguna sección de este tipo sino el resto de las secciones de la encuesta que no son Maestro - Detalle)
	$intMasterDetCount = count($arrQuestionsByMasterDetSection);
	$strStatus = array ();
	$totalOK = 0;
	$intNumsurveys = 0;
	$countSurveys = 0;
	//Obtiene el número total de registros que deberá grabar
	foreach ($arrQuestionsByMasterDetSection as $intSectionID => $arrMasterDetQuestionsColl)
	{
		$intRecNumber = $arrRecNumberByMasterDetSection[$intSectionID];
		$countSurveys += $intRecNumber;
	}
	if ($countSurveys == 0)
	{
		$countSurveys = 1;
	}
	
	for ($intMasterDetIdx = 0; $intMasterDetIdx < $intMasterDetCount; $intMasterDetIdx++)
	{
		//Procesa la sección Maestro - Detalle del índice actual y sólo recorre sus registros, para el resto de las secciones agrega el valor 
		//de No Aplica a sus preguntas. Si esta sección NO tiene registros, salta a la siguiente sección sin agregar datos pues, sólo en caso de
		//que fuera la última sección Maestro - Detalle y que no tuviera registros es como se forzaría a asignar a todas las preguntas el valor de 
		//No Aplica porque de lo contrario la encuesta no grabaría ningún registro
		
		$intSectionIdx = 0;
    	$newArrayDataDet = $newArrayData;
		//Asigna el valor de No Aplica a las secciones que no se están procesando
		foreach ($arrQuestionsByMasterDetSection as $intSectionID => $arrMasterDetQuestionsColl)
		{
			$intRecNumber = $arrRecNumberByMasterDetSection[$intSectionID];
			if ($intSectionIdx != $intMasterDetIdx || ($intSectionIdx == $intMasterDetIdx && $intMasterDetIdx == $intMasterDetCount -1 && $intRecNumber == 0 && $intNumsurveys == 0))
			{
				//No se trata de la sección que se está procesando, por lo que cada registro se agrega con el valor de No Aplica
				//La otra opción es que es la última sección y tampoco tiene registros, en ese caso también se asigna el de No Aplica
				foreach ($arrMasterDetQuestionsColl->Collection as $questionKey => $aQuestion)
				{
					//Procesamiento del dato de qfield
					$postKey = "qfield".$aQuestion->QuestionID;
					//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
					if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					else 
					{
						$strValue = "";
					}
					$newArrayDataDet[$postKey] = $strValue;
					
					//Procesamiento del dato de qimageencode
					$otherPostKey = "qimageencode".$aQuestion->QuestionID;
					$strValue = "";
					$newArrayDataDet[$otherPostKey] = $strValue;
					
					//Procesamiento del dato de comment
					$otherPostKey = "qcomment".$aQuestion->QuestionID;
					$strValue = null;
					$newArrayDataDet[$otherPostKey] = $strValue;

					//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
					//Procesamiento del dato de document
					$otherPostKey = "qdocument".$aQuestion->QuestionID;
					$strValue = null;
					$newArrayDataDet[$otherPostKey] = $strValue;
					//@JAPR
					
					//Procesamiento del dato de action
					$otherPostKey = "qactions".$aQuestion->QuestionID;
					$strValue = null;
					$newArrayDataDet[$otherPostKey] = $strValue;
				}
			}
			
			$intSectionIdx++;
		}
		
		//Graba los de la sección que se está procesando, se procesa al final porque primero hay que asignar los valores de No Aplica al resto
		//de las secciones Maestro - Detalle que no se están procesando en esta iteración del ciclo principal
		$intSectionIdx = 0;
		foreach ($arrQuestionsByMasterDetSection as $intSectionID => $arrMasterDetQuestionsColl)
		{
			$intRegisterNumber = 0;		//Número de registro procesado en esta sección
			if ($intSectionIdx == $intMasterDetIdx)
			{
				//Se trata de la sección que se está procesando, por lo que cada registro generará la grabación en la tabla de hechos
				$intRecNumber = $arrRecNumberByMasterDetSection[$intSectionID];
				
				//Si la cantidad de registros es 0 entonces NO se debe invocar a la grabación, a menos que ya sea la última sección y que no se 
				//hubiera grabado ningún registro hasta ahora, en ese caso se debe forzar a la grabación porque de lo contrario no se grabaría
				//ninguna respuesta de las secciones normales
				if ($intMasterDetIdx == $intMasterDetCount -1 && $intRecNumber == 0 && $intNumsurveys == 0)
				{
					//En el proceso de actualización tentativamente no se mandaría un reporte por ahora
					//$sendEmailDashboards = true;
					
					//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
					//Se graba el ID de la sección Maestro-Detalle que está generando el registro
					$intStatusIdx = count($strStatus);
					//$strStatus[$intStatusIdx] = saveData($aRepository, $surveyID, $newArrayDataDet, $thisDate, $factKeyDimVal, $sendEmailDashboards, $countSurveys, null, $intSectionID);
					$strStatus[$intStatusIdx] = updateData($aRepository, $surveyID, $newArrayDataDe, $dateID, $hourID, $userID, $thisDate, null, 0, 0, $factKeyDimVal);
			        //$strStatus[$intStatusIdx] = "OK";
					$intNumsurveys++;
					$intRegisterNumber++;
					if ($strStatus[$intStatusIdx] == "OK") 
					{
						$totalOK ++;
					}
				}
				else 
				{
					//Recorre el total de registros enviados para esta sección e invoca a la grabación por cada uno de ellos
					for($i=0; $i<$intRecNumber; $i++)
					{
						foreach ($arrMasterDetQuestionsColl->Collection as $questionKey => $aQuestion)
						{
							//Procesamiento del dato de qfield
							$postKey = "qfield".$aQuestion->QuestionID;
							if(isset($arrayData[$postKey]))
							{
								if(isset($arrayData[$postKey][$i]))
								{
									$strValue = $arrayData[$postKey][$i];
								}
								else 
								{
									//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
									if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
									{
										//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
										$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
									}
									else 
									{
										$strValue = "";
									}
								}
							}
							else 
							{
								//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
								if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
								{
									//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
									$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
								}
								else 
								{
									$strValue = "";
								}
							}
							$newArrayDataDet[$postKey] = $strValue;
							//@JAPR 2013-08-21: Corregido un bug, durante la edición cuando se agregan nuevos registros, se debe
							//validar para no usar NOCHANGE sino el valor de catálogo correcto
							//Se pasa al array a utilizar el valor de la combinación de catálogo para usarlo en caso de requerir grabar un nuevo registro
							if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID > 0 && (string) $strValue == "NOCHANGE" && isset($arrayData[$postKey.'Cat'][$i])) {
								$newArrayDataDet[$postKey.'Cat'] = $arrayData[$postKey.'Cat'][$i];
							}
							//@JAPR
							
							//Procesamiento del dato de qimageencode
							$otherPostKey = "qimageencode".$aQuestion->QuestionID;
							if(isset($arrayData[$otherPostKey]))
							{
								if(isset($arrayData[$otherPostKey][$i]))
								{
									$strValue = $arrayData[$otherPostKey][$i];
								}
								else 
								{
									$strValue = "";
								}
							}
							else 
							{
								$strValue = "";
							}
							$newArrayDataDet[$otherPostKey] = $strValue;
							
							//Procesamiento del dato de comment
							$otherPostKey = "qcomment".$aQuestion->QuestionID;
							if(isset($arrayData[$otherPostKey]))
							{
								if(isset($arrayData[$otherPostKey][$i]))
								{
									$strValue = $arrayData[$otherPostKey][$i];
								}
								else 
								{
									$strValue = null;
								}
							}
							else 
							{
								$strValue = null;
							}
							$newArrayDataDet[$otherPostKey] = $strValue;
							
							//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
							//Procesamiento del dato de document
							$otherPostKey = "qdocument".$aQuestion->QuestionID;
							if(isset($arrayData[$otherPostKey]))
							{
								if(isset($arrayData[$otherPostKey][$i]))
								{
									$strValue = $arrayData[$otherPostKey][$i];
								}
								else 
								{
									$strValue = null;
								}
							}
							else 
							{
								$strValue = null;
							}
							$newArrayDataDet[$otherPostKey] = $strValue;
							//@JAPR
							
							//Procesamiento del dato de action
							$otherPostKey = "qactions".$aQuestion->QuestionID;
							if(isset($arrayData[$otherPostKey]))
							{
								if(isset($arrayData[$otherPostKey][$i]))
								{
									$strValue = $arrayData[$otherPostKey][$i];
								}
								else 
								{
									$strValue = null;
								}
							}
							else 
							{
								$strValue = null;
							}
							$newArrayDataDet[$otherPostKey] = $strValue;
						}
						
						//@JAPRWarning: Validar que al integrar dinámicas con maestro - detalle, sólo se envíe el EMail una vez, ya que al salir
						//de este método se invocará al de secciones dinámicas donde también se va a hacer un envío de EMail al terminar
						if($i==($countSurveys-1))
						{
							//En el proceso de actualización tentativamente no se mandaría un reporte por ahora
							//$sendEmailDashboards = true;
						}
						else 
						{
							$sendEmailDashboards = false;
						}
						
						//Procesamiento del dato de registerID
						$strValue = null;
						$registerID = null;
						$otherPostKey = "masterSectionFKs".$intSectionID;
						if(isset($arrayData[$otherPostKey]) && is_array($arrayData[$otherPostKey]))
						{
							if(isset($arrayData[$otherPostKey][$i]))
							{
								$strValue = $arrayData[$otherPostKey][$i];
								$registerID = $strValue;
							}
						}
						$newArrayData[$otherPostKey] = $strValue;
						
						//Si no viene asignado $registerID, sólo se puede asumir que por alguna razón se agregaron nuevos registros por lo que
						//durante esta edición hay mas registros que grabar de los que había originalmente, por tanto se debe hacer una inserción en lugar
						//de actualización
						//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
						//Se graba el ID de la sección Maestro-Detalle que está generando el registro
						$intStatusIdx = count($strStatus);
						if (is_null($registerID)) {
							//@JAPR 2014-10-29: Agregado el mapeo de Scores
							//Agregado el parámetro $aMultiRecordNum para indicar el registro de cada sección que se esté procesando
							$strStatus[$intStatusIdx] = saveData($aRepository, $surveyID, $newArrayDataDet, $thisDate, $factKeyDimVal, $sendEmailDashboards, $countSurveys, null, $intSectionID, 0, '', true, -1, $i);
						}
						else {
							//@JAPR 2014-10-29: Agregado el mapeo de Scores
							//Agregado el parámetro $aMultiRecordNum para indicar el registro de cada sección que se esté procesando
							$strStatus[$intStatusIdx] = updateData($aRepository, $surveyID, $newArrayDataDet, $dateID, $hourID, $userID, $thisDate, $registerID, $intSectionID, 0, $factKeyDimVal, '', true, -1, $i);
						}
			            //$strStatus[$intStatusIdx] = "OK";
						$intNumsurveys++;
						$intRegisterNumber++;
						if ($strStatus[$intStatusIdx] == "OK") 
						{
							$totalOK ++;
						}
					}
				}
			}
			
			$intSectionIdx++;
		}
	}
	
	if ($totalOK == $countSurveys) 
	{
		$strStatusMasterDet = "OK";
	}
	else
	{
		$strStatusMasterDet = implode(";\r\n", $strStatus);
	}
	
	return $strStatusMasterDet;
}

//@JAPR 2012-04-12: Agregadas las secciones Maestro - Detalle
/*Grabado de secciones Maestro - Detalle, en este caso las respuestas vienen con los valores directos en los campos QField que les corresponden, 
	excepto para las preguntas de las secciones Maestro - Detalle, las cuales vienen como un Array con "n" elementos cuya cantidad es distinta
	para cada una de las secciones Maestro - Detalle de la encuesta (puede haber varias), y cada elemento si contiene la respuesta directa de esa
	pregunta para esa instancia de "Detalle" tal como se grabaría en una pregunta normal, ya que todas las preguntas de la misma sección Maestro - 
	Detalle contendrán en sus arrays la misma cantidad de elementos y el total de respuestas de un índice en particular en conjunto forman un
	registro de la Fact agregandoles a las respuestas de otras secciones Maestro - Detalle el valor de No Aplica, y a las preguntas de secciones
	que NO son Maestro - Detalle el valor repetido para todos los detalles a capturar, por ejemplo lo que se recibiría es lo siguiente:
	Sección estática:
		QField1- Pregunta de catálogo selección sencilla
		QField2- Pregunta texto
	Sección Maestro - Detalle 1:
		QField3- Pregunta numérica
		QFueld4- Pregunta de catálogo selección sencilla
	Sección Maestro - Detalle 2:
		QField5 - Pregunta texto
		QField6 - Pregunta selección sencilla
asumiendo que la primer sección Maestro - Detalle capturó 3 registros y la segunda sólo capturó 2 las respuestas vendrían como:

QField1: key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_ValorAtributoUsadoEnQField1
QField2: Texto capturado como respuesta
QField3: array(0 => 34, 1 => 56, 2 => 90) (es decir, los números para cada uno de los 3 registros de la primer sección Maestro - Detalle)
QField4: array(0 => key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_Valor2doAtributoCatalogo_SVElem_key_10109_SVSep_ValorAtributoUsadoEnQField4, 
			   1 => key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_Valor2doAtributoCatalogo_SVElem_key_10109_SVSep_ValorAtributoUsadoEnQField4, 
			   2 => key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_Valor2doAtributoCatalogo_SVElem_key_10109_SVSep_ValorAtributoUsadoEnQField4) 
			   (es decir, los valores seleccionados del catálogo para cada uno de los 3 registros de la primer sección Maestro - Detalle)
QField5: array(0 => "Texto del primer registro", 1 => "Texto del segundo registro") (es decir, los textos para cada uno de los 2 registros de la segunda sección Maestro - Detalle)
QField5: array(0 => "Opción del primer registro", 1 => "Opción del segundo registro") (es decir, los textos para cada uno de los 2 registros de la segunda sección Maestro - Detalle)

dónde los índices 0 y 1 de ambas secciones Maestro - Detalle NO corresponden para nada entre si, en la Fact los índices 0, 1 y 2 de la primer
sección Maestro - Detalle tendrán como valor de las preguntas de la 2da sección Maestro - Detalle el valor de No Aplica, mientras que en 2 registros
independientes los índices 0 y 1 de la segunda sección Maestro - Detalle harán lo propio. En los 5 índices combinados las preguntas que NO son
de secciones Maestro - Detalle contendrán sus valores correspondientes repetidos
*/
function multipleSaveMasterDetData($aRepository, $surveyID, $arrayData, $thisDate, $aMasterDetSectionsColl, &$factKeyDimVal=0)
{
	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	global $arrMasterDetSections;
	global $arrMasterDetSectionsIDs;
	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	global $arrInlineSections;
	global $arrInlineSectionsIDs;
	/*@MABH20121205*/
	global $appVersion;
	//@MABH20121205
	
	//@JAPR 2012-06-05: Integradas las secciones dinámicas con las Maestro - Detalle
    $dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $surveyID);
    $hasDynamicSection = false;
	
    if ($dynamicSectionID > 0) {
        //Aqui verificamos con el sectionValue que realmente si se crearon arreglos
        //de preguntas en las encuestas para poder capturar respuestas para mas de una
        //encuesta
        $postKey = "sectionValue";
        if (isset($arrayData[$postKey])) {
            $hasDynamicSection = true;
        }
    }
    //@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br><br>\r\n\r\nmultipleSaveMasterDetData");
	}
    
	//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
	//Ya no es necesario asignar el 0 explícitamente, si el parámetro no se envía entonces tomará el 0 automáticamente, por el contrario reutilizará
	//el valor recibido desde otro grabado (al momento de programar esto, sólo el grabado de Maestro - Detalle invocaba a otros métodos)
	//$factKeyDimVal = 0;
	//@JAPR
	$sendEmailDashboards = false;
	
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	
	$newArrayData = array();
	$newArrayData["surveyDate"] = $arrayData["surveyDate"];
	$newArrayData["surveyHour"] = $arrayData["surveyHour"];
	$newArrayData["userID"] = $arrayData["userID"];
	$newArrayData["appUserID"] = $arrayData["appUserID"];
	/*@MABH20121205*/
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$newArrayData["serverSurveyDate"] = (string) @$arrayData["serverSurveyDate"];
		$newArrayData["serverSurveyHour"] = (string) @$arrayData["serverSurveyHour"];
		$newArrayData["serverSurveyEndDate"] = (string) @$arrayData["serverSurveyEndDate"];
		$newArrayData["serverSurveyEndHour"] = (string) @$arrayData["serverSurveyEndHour"];
	}
	//@MABH20121205
	
	$newArrayData["surveyFinished"] = $arrayData["surveyFinished"];
	//@JAPR 2013-04-10: A este día, cuando se hacía un Reupload no venían estos datos porque llegaban como parámetro $_POST y no como parte del
	//archivo, por lo tanto generaban Warnings así que se validará en lo que se integran al archivo
	$newArrayData["surveyLatitude"] = @$arrayData["surveyLatitude"];
	$newArrayData["surveyLongitude"] = @$arrayData["surveyLongitude"];
	//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
	$newArrayData["surveyAccuracy"] = @$arrayData["surveyAccuracy"];
	//@JAPR
	
	if(isset($arrayData["CaptureVia"]))
	{
		$newArrayData["CaptureVia"] = $arrayData["CaptureVia"];
	}
	
	if(isset($arrayData["CaptureEmail"]))
	{
		$newArrayData["CaptureEmail"] = $arrayData["CaptureEmail"];
	}
	
	if(isset($arrayData["SchedulerID"]))
	{
		$newArrayData["SchedulerID"] = $arrayData["SchedulerID"];
	}
	
	if(isset($arrayData["qsignature"]))
	{
		$newArrayData["qsignature"] = $arrayData["qsignature"];
	}
	
	if(isset($arrayData["captureType"]))
	{
		$newArrayData["captureType"] = $arrayData["captureType"];
	}

	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	//Ahora estas variables son globales
	//$arrMasterDetSectionsIDs = array_flip($arrMasterDetSections);
	//@JAPR
	//Array de preguntas que corresponden con las secciones Maestro - Detalle indexadas por el Id de la sección
	$arrQuestionsByMasterDetSection = array();
	$arrRecNumberByMasterDetSection = array();
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		$postKey = "qfield".$aQuestion->QuestionID;

		//Si las preguntas pertenecen a la seccion Maestro - Detalle y que estan dentro de la seccion
		//entonces si son repetibles y se ignoran dichas preguntas
		if(isset($arrMasterDetSectionsIDs[$aQuestion->SectionID]))
		{
			//Se agregan las preguntas al array indexado por sección para ser procesadas múltiples veces mas adelante
			$arrMasterDetQuestionsColl = @$arrQuestionsByMasterDetSection[$aQuestion->SectionID];
			if (is_null($arrMasterDetQuestionsColl))
			{
				$arrMasterDetQuestionsColl = BITAMQuestionCollection::NewInstanceEmpty($aRepository, $aQuestion->SurveyID);
				$arrQuestionsByMasterDetSection[$aQuestion->SectionID] = $arrMasterDetQuestionsColl;
				//@JAPR 2012-10-24: Modificada la forma de identificar si hay o no registros para la sección Maestro-Detalle
				$intCurrRecNumber = (int) @$arrRecNumberByMasterDetSection[$aQuestion->SectionID];
				$masterDetKey = "masterSectionRecs".$aQuestion->SectionID;
				$intNewRecNumber = 0;
				if (isset($arrayData[$masterDetKey])) {
					if (!is_array($arrayData[$masterDetKey])) {
						$intNewRecNumber = (int) $arrayData[$masterDetKey];
					}
					else {
						$intNewRecNumber = (int) count($arrayData[$masterDetKey]);
					}
				}
				/*
				if (isset($arrayData[$postKey]))
				{
					//$arrRecNumberByMasterDetSection[$aQuestion->SectionID] = count($arrayData[$postKey]);
					$intNewRecNumber = count($arrayData[$postKey]);
				}
				else 
				{
					//$arrRecNumberByMasterDetSection[$aQuestion->SectionID] = 0;
					$intNewRecNumber = 0;
				}
				*/
				
				if ($intNewRecNumber >= $intCurrRecNumber) {
					$arrRecNumberByMasterDetSection[$aQuestion->SectionID] = $intNewRecNumber;
				}
				//@JAPR
			}
			$arrMasterDetQuestionsColl->Collection[] = $aQuestion;
			
			continue;
		}
		
		//@JAPR 2012-06-05: Integradas las secciones dinámicas con las Maestro - Detalle
		//Si la pregunta corresponde a una sección Dinámica, como se encuentra en esta sección quiere decir que por lo menos una sección
		//Maestro - Detalle si traía valores, así que en este caso simplemente asigna NA a todas las preguntas que sean de secciones dinámicas
		//puesto que esas se grabarán cuando al salir de este método se invoque a multipleSaveDynamicData
		
		//si las preguntas pertenecen a la seccion dinamica entonces se ignoran dichas preguntas porque se grabarán al terminar este método y 
		//en los registros de las secciones Maestro - Detalle deben ir como NA
		if($hasDynamicSection && $aQuestion->SectionID == $dynamicSectionID)
		{
			//@JAPR 2012-06-07: Corregido un bug, cuando se combinan Maestro - Detalle con dinámicas, como se estaba dejando sin asignar el 
			//qfield las preguntas que son numéricas marcaban un error de truncado porque les intentaban asignar ''
			//Se agregó la validación para que en caso de ser una pregunta que se graba en campo numérico asigne un 0 (multiples-> numéricas o checkbox)
			if ($aQuestion->QTypeID == qtpMulti && ($aQuestion->MCInputType == mpcCheckBox || $aQuestion->MCInputType == mpcNumeric))
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$newArrayData[$postKey] = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			//@JAPR
			continue;
		}
		
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		//Si la encuesta está configurada para grabar un registro único para secciones estándar, ya no procesa ningún otro tipo de pregunta
		//para no provocar que se repitan sus valores
		//@JAPR 2013-01-17: Corregido un bug, se habían excluido de los registros de maestro-detalle las preguntas de catálogo de las secciones
		//estándar, por lo tanto no se podía agrupar por ellas al consultar desde el EAS. En este caso se dejará el mismo valor seleccionado
		//para la pregunta estándar
		//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
		//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
		//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
		//if ($surveyInstance->UseStdSectionSingleRec && !($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1 && $aQuestion->CatalogID>0)) {
		if ($surveyInstance->UseStdSectionSingleRec && ($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)) {
			continue;
		}
		//@JAPR
		
		//Procesamiento del dato de qfield
		if(isset($arrayData[$postKey]))
		{
			$strValue = $arrayData[$postKey];
		}
		else 
		{
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			else 
			{
				$strValue = "";
			}
		}

		$newArrayData[$postKey] = $strValue;
		
		//Procesamiento del dato de qimageencode
		$otherPostKey = "qimageencode".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = "";
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Procesamiento del dato de comment
		$otherPostKey = "qcomment".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;

		//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
		//Procesamiento del dato de document
		$otherPostKey = "qdocument".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		//@JAPR
		
		//Procesamiento del dato de action
		$otherPostKey = "qactions".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
	}
	
	//Realizamos coleccion de preguntas de la seccion Maestro - Detalle
	//Por cada sección Maestro - Detalle se agregarán tantos registros como respuestas se hubieran capturado para sus preguntas, sin embargo
	//para otras secciones Maestro - Detalle se deberán de asignar valores de No Aplica para sus preguntas, por lo que el total de registros a
	//agregar es la suma de los totales de registros de cada sección Maestro - Detalle, o bien sólo uno en caso de que ninguna tuviera registros
	//(que eso es lo mismo a no haber contestado ninguna sección de este tipo sino el resto de las secciones de la encuesta que no son Maestro - Detalle)
	$intMasterDetCount = count($arrQuestionsByMasterDetSection);
	$strStatus = array ();
	$totalOK = 0;
	$intNumsurveys = 0;
	$countSurveys = 0;
	//Obtiene el número total de registros que deberá grabar
	foreach ($arrQuestionsByMasterDetSection as $intSectionID => $arrMasterDetQuestionsColl)
	{
		$intRecNumber = $arrRecNumberByMasterDetSection[$intSectionID];
		$countSurveys += $intRecNumber;
	}
	if ($countSurveys == 0)
	{
		$countSurveys = 1;
	}
	
	for ($intMasterDetIdx = 0; $intMasterDetIdx < $intMasterDetCount; $intMasterDetIdx++)
	{
		//Procesa la sección Maestro - Detalle del índice actual y sólo recorre sus registros, para el resto de las secciones agrega el valor 
		//de No Aplica a sus preguntas. Si esta sección NO tiene registros, salta a la siguiente sección sin agregar datos pues, sólo en caso de
		//que fuera la última sección Maestro - Detalle y que no tuviera registros es como se forzaría a asignar a todas las preguntas el valor de 
		//No Aplica porque de lo contrario la encuesta no grabaría ningún registro
		
		$intSectionIdx = 0;
    	$newArrayDataDet = $newArrayData;
		//Asigna el valor de No Aplica a las secciones que no se están procesando
		foreach ($arrQuestionsByMasterDetSection as $intSectionID => $arrMasterDetQuestionsColl)
		{
			$intRecNumber = $arrRecNumberByMasterDetSection[$intSectionID];
			if ($intSectionIdx != $intMasterDetIdx || ($intSectionIdx == $intMasterDetIdx && $intMasterDetIdx == $intMasterDetCount -1 && $intRecNumber == 0 && $intNumsurveys == 0))
			{
				//No se trata de la sección que se está procesando, por lo que cada registro se agrega con el valor de No Aplica
				//La otra opción es que es la última sección y tampoco tiene registros, en ese caso también se asigna el de No Aplica
				foreach ($arrMasterDetQuestionsColl->Collection as $questionKey => $aQuestion)
				{
					//Procesamiento del dato de qfield
					$postKey = "qfield".$aQuestion->QuestionID;
					//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
					if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
					{
						//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
						$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					else 
					{
						$strValue = "";
					}
					$newArrayDataDet[$postKey] = $strValue;
					
					//Procesamiento del dato de qimageencode
					$otherPostKey = "qimageencode".$aQuestion->QuestionID;
					$strValue = "";
					$newArrayDataDet[$otherPostKey] = $strValue;
					
					//Procesamiento del dato de comment
					$otherPostKey = "qcomment".$aQuestion->QuestionID;
					$strValue = null;
					$newArrayDataDet[$otherPostKey] = $strValue;

					//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
					//Procesamiento del dato de document
					$otherPostKey = "qdocument".$aQuestion->QuestionID;
					$strValue = null;
					$newArrayDataDet[$otherPostKey] = $strValue;
					//@JAPR
					
					//Procesamiento del dato de action
					$otherPostKey = "qactions".$aQuestion->QuestionID;
					$strValue = null;
					$newArrayDataDet[$otherPostKey] = $strValue;
				}
			}
			
			$intSectionIdx++;
		}
		
		//Graba los de la sección que se está procesando, se procesa al final porque primero hay que asignar los valores de No Aplica al resto
		//de las secciones Maestro - Detalle que no se están procesando en esta iteración del ciclo principal
		$intSectionIdx = 0;
		foreach ($arrQuestionsByMasterDetSection as $intSectionID => $arrMasterDetQuestionsColl)
		{
			if ($intSectionIdx == $intMasterDetIdx)
			{
				//Se trata de la sección que se está procesando, por lo que cada registro generará la grabación en la tabla de hechos
				$intRecNumber = $arrRecNumberByMasterDetSection[$intSectionID];
				
				//Si la cantidad de registros es 0 entonces NO se debe invocar a la grabación, a menos que ya sea la última sección y que no se 
				//hubiera grabado ningún registro hasta ahora, en ese caso se debe forzar a la grabación porque de lo contrario no se grabaría
				//ninguna respuesta de las secciones normales
				if ($intMasterDetIdx == $intMasterDetCount -1 && $intRecNumber == 0 && $intNumsurveys == 0)
				{
					$sendEmailDashboards = true;
					
			        //echo("@JAPR: Grabado\r\n");
			        /*
			        echo('$strStatus['.$i.'] = ');
			        var_export($newArrayDataDet);
			        echo(";\r\n");
			        */
					//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
					//Se graba el ID de la sección Maestro-Detalle que está generando el registro
					$intStatusIdx = count($strStatus);
					//@JAPR 2013-01-22: Agregado el parámetro $bSaveSignature para optimizar en que momento se graba la firma
					$strStatus[$intStatusIdx] = saveData($aRepository, $surveyID, $newArrayDataDet, $thisDate, $factKeyDimVal, $sendEmailDashboards, $countSurveys, null, $intSectionID, 0, '', ($intStatusIdx == 0));
			        //$strStatus[$intStatusIdx] = "OK";
					$intNumsurveys++;
					if ($strStatus[$intStatusIdx] == "OK") 
					{
						$totalOK ++;
					}
				}
				else 
				{
					//Recorre el total de registros enviados para esta sección e invoca a la grabación por cada uno de ellos
					for($i=0; $i<$intRecNumber; $i++)
					{
						foreach ($arrMasterDetQuestionsColl->Collection as $questionKey => $aQuestion)
						{
							//Procesamiento del dato de qfield
							$postKey = "qfield".$aQuestion->QuestionID;
							if(isset($arrayData[$postKey]))
							{
								if(isset($arrayData[$postKey][$i]))
								{
									$strValue = $arrayData[$postKey][$i];
								}
								else 
								{
									//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
									if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
									{
										//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
										$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
									}
									else 
									{
										$strValue = "";
									}
								}
							}
							else 
							{
								//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
								if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
								{
									//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
									$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
								}
								else 
								{
									$strValue = "";
								}
							}
							$newArrayDataDet[$postKey] = $strValue;
							
							//Procesamiento del dato de qimageencode
							$otherPostKey = "qimageencode".$aQuestion->QuestionID;
							if(isset($arrayData[$otherPostKey]))
							{
								if(isset($arrayData[$otherPostKey][$i]))
								{
									$strValue = $arrayData[$otherPostKey][$i];
								}
								else 
								{
									$strValue = "";
								}
							}
							else 
							{
								$strValue = "";
							}
							$newArrayDataDet[$otherPostKey] = $strValue;
							
							//Procesamiento del dato de comment
							$otherPostKey = "qcomment".$aQuestion->QuestionID;
							if(isset($arrayData[$otherPostKey]))
							{
								if(isset($arrayData[$otherPostKey][$i]))
								{
									$strValue = $arrayData[$otherPostKey][$i];
								}
								else 
								{
									$strValue = null;
								}
							}
							else 
							{
								$strValue = null;
							}
							$newArrayDataDet[$otherPostKey] = $strValue;

							//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
							//Procesamiento del dato de document
							$otherPostKey = "qdocument".$aQuestion->QuestionID;
							if(isset($arrayData[$otherPostKey]))
							{
								if(isset($arrayData[$otherPostKey][$i]))
								{
									$strValue = $arrayData[$otherPostKey][$i];
								}
								else 
								{
									$strValue = null;
								}
							}
							else 
							{
								$strValue = null;
							}
							$newArrayDataDet[$otherPostKey] = $strValue;
							//@JAPR
							
							//Procesamiento del dato de action
							$otherPostKey = "qactions".$aQuestion->QuestionID;
							if(isset($arrayData[$otherPostKey]))
							{
								if(isset($arrayData[$otherPostKey][$i]))
								{
									$strValue = $arrayData[$otherPostKey][$i];
								}
								else 
								{
									$strValue = null;
								}
							}
							else 
							{
								$strValue = null;
							}
							$newArrayDataDet[$otherPostKey] = $strValue;
						}
						
						//@JAPRWarning: Validar que al integrar dinámicas con maestro - detalle, sólo se envíe el EMail una vez, ya que al salir
						//de este método se invocará al de secciones dinámicas donde también se va a hacer un envío de EMail al terminar
						if($i==($countSurveys-1))
						{
							$sendEmailDashboards = true;
						}
						else 
						{
							$sendEmailDashboards = false;
						}
						
			            //echo("@JAPR: Grabado\r\n");
			            /*
			            echo('$strStatus['.$i.'] = ');
			            var_export($newArrayDataDet);
			        		echo(";\r\n");
			            */
						//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
						//Se graba el ID de la sección Maestro-Detalle que está generando el registro
						$intStatusIdx = count($strStatus);
						//@JAPR 2013-01-22: Agregado el parámetro $bSaveSignature para optimizar en que momento se graba la firma
						//@JAPR 2014-10-29: Agregado el mapeo de Scores
						//Agregado el parámetro $aMultiRecordNum para indicar el registro de cada sección que se esté procesando
						$strStatus[$intStatusIdx] = saveData($aRepository, $surveyID, $newArrayDataDet, $thisDate, $factKeyDimVal, $sendEmailDashboards, $countSurveys, null, $intSectionID, 0, '', ($intStatusIdx == 0), -1, $i);
			            //$strStatus[$intStatusIdx] = "OK";
						$intNumsurveys++;
						if ($strStatus[$intStatusIdx] == "OK") 
						{
							$totalOK ++;
						}
					}
				}
			}
			
			$intSectionIdx++;
		}
	}
	
	if ($totalOK == $countSurveys) 
	{
		$strStatusMasterDet = "OK";
	}
	else
	{
		$strStatusMasterDet = implode(";\r\n", $strStatus);
	}
	
	return $strStatusMasterDet;
}

/*Grabado de secciones Inline, en este caso las respuestas vienen con los valores directos en los campos QField que les corresponden, excepto 
	para las preguntas de la sección dinámica, las cuales vienen como un Array con "n" elementos que corresponden al total de miembros del atributo
	que generó los "x" registros Inline, cada elemento corresponde en índice al contenido del parámetro "sectionValue####" donde se graba
	la llave subrrogada que corresponde al elemento del atributo, por ejemplo:
	Sección estática:
		QField1 - Pregunta de catálogo que genera las secciones Inline (el Atributo genera 3 secciones dinámicas, NO es requisito que exista, ya que
			las secciones Inline se pueden generar directamente o incluso no depender de un catálogo sino con opciones fijas de la sección)
		QField2 - Pregunta texto
	Sección Inline:
		QField3- Pregunta numérica
		QFueld4- Pregunta de catálogo selección sencilla
	asumiendo que el Atributo hijo del usado en QField1 (o mejor dicho, el atributo configurado en la sección Inline) contiene entre sus 3 valores 
de los padres 10 posibles valores con la siguiente correspondencia
	Valor 1 del atributo padre: Valores del atributo hijo del 0 al 5
	Valor 2 del atributo padre: Valores del atributo hijo del 6 al 7
	Valor 3 del atributo padre: Valores del atributo hijo del 8 al 9
las respuestas vendrían como:

QField1: key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_ValorAtributoUsadoEnQField1
QField2: Texto capturado como respuesta
QField3: array(0 => 34, 1 => 56, 2 => 90, .., 9 => 100) (es decir, los números para cada uno de los 10 registros de la sección Inline)
QField4: array(0 => key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_Valor2doAtributoCatalogo_SVElem_key_10109_SVSep_ValorAtributoUsadoEnQField4, 
			   1 => key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_Valor2doAtributoCatalogo_SVElem_key_10109_SVSep_ValorAtributoUsadoEnQField4, 
			   2 => key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_Valor2doAtributoCatalogo_SVElem_key_10109_SVSep_ValorAtributoUsadoEnQField4,
			   ...,
			   9 => key_10107_SVSep_Valor1erAtributoCatalogo_SVElem_key_10108_SVSep_Valor2doAtributoCatalogo_SVElem_key_10109_SVSep_ValorAtributoUsadoEnQField9) 
			   (es decir, los valores seleccionados del catálogo para cada uno de los 10 registros de la sección Inline)

En la tabla de hechos se grabarán 10 registros, uno por cada posible valor de los atributos hijos (el asignado a la sección) con las respuestas de 
cada pregunta que se encontraban en la sección Inline, cualquier pregunta que NO esté en la sección Inline repetirá su respuesta para los 10 registros
a grabar, excepto si pertenece a otra sección múltiple de algún tipo
*/
//Agregado el parámetro $factKeyDimVal, si este viene asignado entonces quiere decir que proviene de otro grabado previo de la misma captura
//de encuesta sólo que se está particionando el grabado por tipo de sección. Originalmente usado para grabar secciones dinámicas con este método
//posteriormente a haber invocado el grabado de secciones Maestro - Detalle, para permitir que todos los registros de ambos métodos se graben
//con la misma referencia al FactKeyDimVal
function multipleSaveInlineData($aRepository, $surveyID, $arrayData, $thisDate, $inlineSectionID, &$factKeyDimVal=0)
{
	global $appVersion;
	global $arrSectionDescByID;
	global $arrSectionDescStrByID;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br><br>\r\n\r\nmultipleSaveInlineData {$inlineSectionID}");
	}
	
    $dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $surveyID);
    $hasDynamicSection = false;
	
    if ($dynamicSectionID > 0) {
        //Aqui verificamos con el sectionValue que realmente si se crearon arreglos
        //de preguntas en las encuestas para poder capturar respuestas para mas de una
        //encuesta
        $postKey = "sectionValue";
        if (isset($arrayData[$postKey])) {
            $hasDynamicSection = true;
        }
    }
	
	$sendEmailDashboards = false;
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	
	$newArrayData = array();
	$newArrayData["surveyDate"] = $arrayData["surveyDate"];
	$newArrayData["surveyHour"] = $arrayData["surveyHour"];
	$newArrayData["userID"] = $arrayData["userID"];
	$newArrayData["appUserID"] = $arrayData["appUserID"];
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$newArrayData["serverSurveyDate"] = (string) @$arrayData["serverSurveyDate"];
		$newArrayData["serverSurveyHour"] = (string) @$arrayData["serverSurveyHour"];
		$newArrayData["serverSurveyEndDate"] = (string) @$arrayData["serverSurveyEndDate"];
		$newArrayData["serverSurveyEndHour"] = (string) @$arrayData["serverSurveyEndHour"];
	}
	
	$newArrayData["surveyFinished"] = $arrayData["surveyFinished"];
	$newArrayData["surveyLatitude"] = @$arrayData["surveyLatitude"];
	$newArrayData["surveyLongitude"] = @$arrayData["surveyLongitude"];
	$newArrayData["surveyAccuracy"] = @$arrayData["surveyAccuracy"];
	
	if(isset($arrayData["CaptureVia"]))
	{
		$newArrayData["CaptureVia"] = $arrayData["CaptureVia"];
	}
	
	if(isset($arrayData["CaptureEmail"]))
	{
		$newArrayData["CaptureEmail"] = $arrayData["CaptureEmail"];
	}
	
	if(isset($arrayData["SchedulerID"]))
	{
		$newArrayData["SchedulerID"] = $arrayData["SchedulerID"];
	}
	
	if(isset($arrayData["qsignature"]))
	{
		$newArrayData["qsignature"] = $arrayData["qsignature"];
	}
	
	if(isset($arrayData["captureType"]))
	{
		$newArrayData["captureType"] = $arrayData["captureType"];
	}
	
    $arrMasterDetSections = BITAMSection::existMasterDetSections($aRepository, $surveyID, true);
    if (!is_array($arrMasterDetSections) || count($arrMasterDetSections) == 0) {
    	$arrMasterDetSections = array();
    }
    $arrMasterDetSectionsIDs = array_flip($arrMasterDetSections);
	//@JAPR 2014-05-30: Agregado el tipo de sección Inline
    $arrInlineSections = BITAMSection::existInlineSections($aRepository, $surveyID, true);
    if (!is_array($arrInlineSections) || count($arrInlineSections) == 0) {
    	$arrInlineSections = array();
    }
    $arrInlineSectionsIDs = array_flip($arrInlineSections);
	
	//Contiene la instancia de la pregunta de Single Choice que filtra para generar los registros Inline
	$inlineSection = BITAMSection::NewInstanceWithID($aRepository, $inlineSectionID);
	$inlineSectionCatID = $inlineSection->CatalogID;
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		
		//si las preguntas pertenecen a la seccion dinamica entonces se ignoran dichas preguntas porque se grabarán al terminar este método y 
		//en los registros de las secciones Maestro - Detalle deben ir como NA
		if($hasDynamicSection && $aQuestion->SectionID == $dynamicSectionID)
		{
			//@JAPR 2012-06-07: Corregido un bug, cuando se combinan Maestro - Detalle con dinámicas, como se estaba dejando sin asignar el 
			//qfield las preguntas que son numéricas marcaban un error de truncado porque les intentaban asignar ''
			//Se agregó la validación para que en caso de ser una pregunta que se graba en campo numérico asigne un 0 (multiples-> numéricas o checkbox)
			if ($aQuestion->QTypeID == qtpMulti && ($aQuestion->MCInputType == mpcCheckBox || $aQuestion->MCInputType == mpcNumeric))
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$newArrayData[$postKey] = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			//@JAPR
			continue;
		}
		
		//Si la pregunta pertenece a alguna sección Maestro-detalle o a alguna Inline, entonces la ignora, ya que esas se grabarán en su propio
		//método y especificamente las de estas sección se agregarán en un ciclo mas abajo
		if($aQuestion->SectionID == $dynamicSectionID || isset($arrMasterDetSectionsIDs[$aQuestion->SectionID]) || isset($arrInlineSectionsIDs[$aQuestion->SectionID])) {
			continue;
		}
		
		//Si la encuesta está configurada para grabar un registro único para secciones estándar, ya no procesa ningún otro tipo de pregunta
		//para no provocar que se repitan sus valores
		//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
		//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
		//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
		if ($surveyInstance->UseStdSectionSingleRec && ($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)) {
			continue;
		}
		//@JAPR
		
		//Procesamiento del dato de qfield
		$postKey = "qfield".$aQuestion->QuestionID;
		if(isset($arrayData[$postKey]))
		{
			$strValue = $arrayData[$postKey];
		}
		else 
		{
			if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:(float)0);
			}
			else 
			{
				$strValue = "";
			}
		}
		
		$newArrayData[$postKey] = $strValue;
		
		//Procesamiento del dato de qimageencode
		$otherPostKey = "qimageencode".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = "";
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Procesamiento del dato de comment
		$otherPostKey = "qcomment".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;

		//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
		//Procesamiento del dato de document
		$otherPostKey = "qdocument".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		//@JAPR
		
		//Procesamiento del dato de action
		$otherPostKey = "qactions".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
	}
	
	//Realizamos coleccion de preguntas de la sección Inline
	$otherQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($aRepository, $inlineSectionID);
	
	//Buscamos las preguntas de la sección Inline y nos basamos en el index de la variable $i
	$postKey = "inlineValue".$inlineSectionID;
	$countSurveys = 0;
	if (isset($arrayData[$postKey])) {
		$countSurveys = count($arrayData[$postKey]);
	}

	$i=0;
	$strStatus = array ();
	$totalOK = 0;
	
	//@JAPR 2013-01-12: Modificado para que ahora sólo un registro (en caso de haber multiple-choice) contenga datos de las preguntas Open o que
	//no corresponden con las multiple-choice, ya que si se repiten no se pueden estimar correctamente
	for($i=0; $i<$countSurveys; $i++)
	{
		//@JAPR 2012-06-11: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
		//Movido este código al inicio del ciclo ya que se usará para obtener los valores de las preguntas Open
		$intSectionValue = false;
		//Procesamiento del dato de sectionValue
		$otherPostKey = "inlineValue".$inlineSectionID;
		$inlinePostKey = "inlineValue";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
				//Asigna el valor del elemento múltiple choice (o single choice si no había preguntas múltiple choice) que se está grabando en 
				//este registro para mas adelante usarlo al obtener el índice de la respuesta Open (o cualquiera que no se manda repetida desde 
				//la App) que corresponde a esta página. Sólo aplica si existen preguntas múltiple choice
				$intSectionValue = (int) $strValue;
			}
			else 
			{
				$strValue = 1;
			}
		}
		$newArrayData[$inlinePostKey] = $strValue;
		
		//@JAPR 2013-01-12: Modificado para que ahora sólo un registro (en caso de haber multiple-choice) contenga datos de las preguntas Open o que
		//no corresponden con las multiple-choice, ya que si se repiten no se pueden estimar correctamente
		//Si la pregunta no corresponde con una multiple-choice, como está dentro de la sección dinámica, usando la nueva opción de Single record
		//para estándar, NO grabará repetidas estas preguntas pues alterarían los cálculos, la grabará una sola vez por página
		$strDynamicPageDSC = '';
		if ($surveyInstance->UseStdSectionSingleRec) {
			if ($surveyInstance->DissociateCatDimens && isset($arrSectionDescByID) && isset($arrSectionDescStrByID)) {
				//@JAPR 2013-08-22: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				//Si se graba a partir de las descripciones, no puede ir a traer el String de la combinación ni la descripción
				//del checkbox al catálogo porque el valor probablemente pudo haber cambiado, y aunque para este punto si fuera un
				//key > 0 ya se habría remapeado, aquellos que fueron eliminados grabarían el valor de *NA así que mejor se usará
				//directamente el dato que llegó desde el App en esos casos
				$strDynamicOptionDSC = '';
				$arrSectionDesc = @$arrSectionDescByID[$inlineSectionID][$i];
				$strNewCatDimValAnswers = @$arrSectionDescStrByID[$inlineSectionID][$i];
				if (!is_null($strNewCatDimValAnswers) && !is_null($arrSectionDesc) && is_array($arrSectionDesc) && count($arrSectionDesc) > 0) {
					//@JAPR 2013-05-09: Corregido un bug con la codificación
					//Este valor SI se debe decodificar, porque se usará para grabar en las tablas de datos
					//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					$strDynamicOptionDSC = (string) @$arrSectionDesc[count($arrSectionDesc) -1];
				}
				else {
					$arrayMapStrValues = array();
					$arrayMapStrValues[0] = $intSectionValue;
					//@JAPR 2014-05-30: Agregado el tipo de sección Inline
					//Agregado el parámetro $aSectionID, si se especifica se asumirá que NO se trata de una sección dinámica sino de una Inline
					$arrDynamicRecValue = getCatValueDescForSurveyField($aRepository, $arrayMapStrValues, $inlineSectionCatID, -1, -1, '', 0, 0, $inlineSectionID);
					//@JAPR
					if (!is_array($arrDynamicRecValue) || count($arrDynamicRecValue) < 2) {
						$arrDynamicRecValue = array();
					}
					$strDynamicOptionDSC = (string) @$arrDynamicRecValue[1];
				}
				if (trim($strDynamicOptionDSC) == '') {
					$strDynamicPageDSC = 'sectionValue-'.$intSectionValue;
				}
				else {
					$strDynamicPageDSC = $strDynamicOptionDSC;
				}
			}
			else {
				$strDynamicPageDSC = 'sectionValue-'.$intSectionValue;
			}
		}
		//@JAPR
		
		foreach ($otherQuestionCollection->Collection as $questionKey => $aQuestion)
		{
			//Procesamiento del dato de qfield
			$postKey = "qfield".$aQuestion->QuestionID;
			if(isset($arrayData[$postKey]))
			{
				if(isset($arrayData[$postKey][$i]))
				{
					$strValue = $arrayData[$postKey][$i];
				}
				else 
				{
					if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
					{
						$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
					}
					else 
					{
						$strValue = "";
					}
				}
			}
			else 
			{
				if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
				{
					$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
				}
				else 
				{
					$strValue = "";
				}
			}
			
			$newArrayData[$postKey] = $strValue;
			
			//Procesamiento del dato de qimageencode
			$otherPostKey = "qimageencode".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = "";
				}
			}
			else 
			{
				$strValue = "";
			}
			$newArrayData[$otherPostKey] = $strValue;
			
			//Procesamiento del dato de comment
			$otherPostKey = "qcomment".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = null;
				}
			}
			else 
			{
				$strValue = null;
			}
			$newArrayData[$otherPostKey] = $strValue;

			//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
			//Procesamiento del dato de document
			$otherPostKey = "qdocument".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = null;
				}
			}
			else 
			{
				$strValue = null;
			}
			$newArrayData[$otherPostKey] = $strValue;
			//@JAPR
			
			//Procesamiento del dato de action
			$otherPostKey = "qactions".$aQuestion->QuestionID;
			if(isset($arrayData[$otherPostKey]))
			{
				if(isset($arrayData[$otherPostKey][$i]))
				{
					$strValue = $arrayData[$otherPostKey][$i];
				}
				else 
				{
					$strValue = null;
				}
			}
			else 
			{
				$strValue = null;
			}
			$newArrayData[$otherPostKey] = $strValue;
		}
		
		//Con las versiones 4 o posteriores, siempre existe un grabado de un registro único, así que ninguna sección multi registro debería intentar
		//enviar el reporte por correo porque eso está delegado exclusivamente para el grabado del registro único
		$sendEmailDashboards = false;
		
		//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
		//En este caso se recibe como parámetro el ID de la sección dinámica que se está procesando, así que ese se usa directamente
		//@JAPR 2013-01-22: Agregado el parámetro $bSaveSignature para optimizar en que momento se graba la firma
		//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
		//@JAPR 2014-10-29: Agregado el mapeo de Scores
		//Agregado el parámetro $aMultiRecordNum para indicar el registro de cada sección que se esté procesando
		$strStatus[$i] = saveData($aRepository, $surveyID, $newArrayData, $thisDate, $factKeyDimVal, $sendEmailDashboards, $countSurveys, null, $inlineSectionID, 0, $strDynamicPageDSC, ($i == 0), $i, $i);
		
		if ($strStatus[$i] == "OK") 
		{
			$totalOK ++;
		}
	}
	
	if ($totalOK == $countSurveys) 
	{
		$strStatusInline = "OK";
	}
	else
	{
		$strStatusInline = implode(";\r\n", $strStatus);
	}
	
	return $strStatusInline;
}
//@JAPR

//@JAPR 2012-09-05: Agregado el cubo global de Scores (parámetro $factKeyDimVal)
function multipleSaveCategoryDimData($aRepository, $surveyID, $arrayData, $thisDate, &$factKeyDimVal=0)
{
	/*@MABH20121205*/
	global $appVersion;
	//@MABH20121205

	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br><br>\r\n\r\nmultipleSaveCategoryDimData");
	}
	
	//@JAPR 2012-09-05: Agregado el cubo global de Scores
	//Ya no es necesario asignar el 0 explícitamente, si el parámetro no se envía entonces tomará el 0 automáticamente, por el contrario reutilizará
	//el valor recibido desde otro grabado (al momento de programar esto, sólo el grabado de Maestro - Detalle invocaba a otros métodos)
	//$factKeyDimVal = 0;
	//@JAPR
	$sendEmailDashboards = false;
	
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	
	$newArrayData = array();
	$newArrayData["surveyDate"] = $arrayData["surveyDate"];
	$newArrayData["surveyHour"] = $arrayData["surveyHour"];
	$newArrayData["userID"] = $arrayData["userID"];
	$newArrayData["appUserID"] = $arrayData["appUserID"];
	/*@MABH20121205*/
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if(!is_null($appVersion) && $appVersion >= esvServerDateTime  && !is_null($surveyInstance) && 
			$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
		$newArrayData["serverSurveyDate"] = (string) @$arrayData["serverSurveyDate"];
		$newArrayData["serverSurveyHour"] = (string) @$arrayData["serverSurveyHour"];
		$newArrayData["serverSurveyEndDate"] = (string) @$arrayData["serverSurveyEndDate"];
		$newArrayData["serverSurveyEndHour"] = (string) @$arrayData["serverSurveyEndHour"];
	}
	//@MABH20121205
	
	$newArrayData["surveyFinished"] = $arrayData["surveyFinished"];
	//@JAPR 2013-04-10: A este día, cuando se hacía un Reupload no venían estos datos porque llegaban como parámetro $_POST y no como parte del
	//archivo, por lo tanto generaban Warnings así que se validará en lo que se integran al archivo
	$newArrayData["surveyLatitude"] = @$arrayData["surveyLatitude"];
	$newArrayData["surveyLongitude"] = @$arrayData["surveyLongitude"];
	//@JAPR 2013-11-29: Agregado el Atributo de Accuracy
	$newArrayData["surveyAccuracy"] = @$arrayData["surveyAccuracy"];
	//@JAPR
	
	if(isset($arrayData["CaptureVia"]))
	{
		$newArrayData["CaptureVia"] = $arrayData["CaptureVia"];
	}
	
	if(isset($arrayData["CaptureEmail"]))
	{
		$newArrayData["CaptureEmail"] = $arrayData["CaptureEmail"];
	}
	
	if(isset($arrayData["SchedulerID"]))
	{
		$newArrayData["SchedulerID"] = $arrayData["SchedulerID"];
	}
	
	if(isset($arrayData["qsignature"]))
	{
		$newArrayData["qsignature"] = $arrayData["qsignature"];
	}
	
	if(isset($arrayData["captureType"]))
	{
		$newArrayData["captureType"] = $arrayData["captureType"];
	}

	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		//si las preguntas pertenecen a las de tipo Category Dimension
		//entonces las excluimos de este ciclo ya que aqui se procesan
		//las preguntas que se repetiran los datos en todos los registros
		if($aQuestion->QTypeID==qtpMulti && $aQuestion->QDisplayMode==dspVertical && $aQuestion->MCInputType==mpcNumeric && $aQuestion->IsMultiDimension==0 && $aQuestion->UseCategoryDimChoice!=0)
		{
			//Procesamiento del dato de qfield para que sea considerado en el indicador answeredquestions como pregunta contestada
			$postKey = "qfield".$aQuestion->QuestionID;
			if(isset($arrayData[$postKey]))
			{
				$strValue = $arrayData[$postKey];
			}
			else 
			{
					$strValue = "";
			}
			$newArrayData[$postKey] = $strValue;

			continue;
		}
		
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		//Si la encuesta está configurada para grabar un registro único para secciones estándar, ya no procesa ningún otro tipo de pregunta
		//para no provocar que se repitan sus valores
		//@JAPR 2013-01-17: Corregido un bug, se habían excluido de los registros de maestro-detalle las preguntas de catálogo de las secciones
		//estándar, por lo tanto no se podía agrupar por ellas al consultar desde el EAS. En este caso se dejará el mismo valor seleccionado
		//para la pregunta estándar
		//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
		//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
		//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
		//if ($surveyInstance->UseStdSectionSingleRec && !($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==1 && $aQuestion->CatalogID>0)) {
		if ($surveyInstance->UseStdSectionSingleRec && ($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)) {
			continue;
		}
		//@JAPR
		
		//Procesamiento del dato de qfield
		$postKey = "qfield".$aQuestion->QuestionID;
		if(isset($arrayData[$postKey]))
		{
			$strValue = $arrayData[$postKey];
		}
		else 
		{
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
			{
				//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
				$strValue = (($surveyInstance->UseNULLForEmptyNumbers)?null:0);
			}
			else 
			{
				$strValue = "";
			}
		}

		$newArrayData[$postKey] = $strValue;
		
		//Procesamiento del dato de qimageencode
		$otherPostKey = "qimageencode".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = "";
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Procesamiento del dato de comment
		$otherPostKey = "qcomment".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;

		//@JAPR 2014-09-24: Corregido un bug, no estaba heredando el valor del documento recibido así que no lo procesaba
		//Procesamiento del dato de document
		$otherPostKey = "qdocument".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
		//@JAPR
		
		//Procesamiento del dato de action
		$otherPostKey = "qactions".$aQuestion->QuestionID;
		if(isset($arrayData[$otherPostKey]))
		{
			$strValue = $arrayData[$otherPostKey];
		}
		else 
		{
			$strValue = null;
		}
		$newArrayData[$otherPostKey] = $strValue;
	}
	
	//Buscamos las respuestas de CategoryDimension
	$postKey = "qCatDimVal";
	$countSurveys = count($arrayData[$postKey]);

	$i=0;
	$strStatus = array ();
	$totalOK = 0;
	for($i=0; $i<$countSurveys; $i++)
	{
		//Obtenemos el valor en su indice $i del campo oculto qCatDimVal
		//que es quien contiene el valor subrogado de la dimension categoria
		$otherPostKey = "qCatDimVal";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
			}
			else 
			{
				$strValue = 1;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;

		//Obtenemos el valor en su indice $i del campo oculto qElemDimVal
		//que es quien contiene el valor subrogado de la dimension element
		$otherPostKey = "qElemDimVal";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
			}
			else 
			{
				$strValue = 1;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		//Obtenemos el valor en su indice $i del campo oculto qCatDimValQID
		//que es quien contiene el valor del questionid al que esta asociada la respuesta
		$otherPostKey = "qCatDimValQID";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
			}
			else 
			{
				$strValue = -1;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		$intEntryCatDimID = (int) $newArrayData[$otherPostKey];
		
		//Obtenemos el valor en su indice $i del campo oculto qElemDimValResp
		//que es quien contiene el valor del questionid al que esta asociada la respuesta
		$otherPostKey = "qElemDimValResp";
		if(isset($arrayData[$otherPostKey]))
		{
			if(isset($arrayData[$otherPostKey][$i]))
			{
				$strValue = $arrayData[$otherPostKey][$i];
			}
			else 
			{
				$strValue = 0;
			}
		}
		$newArrayData[$otherPostKey] = $strValue;
		
		if($i==($countSurveys-1))
		{
			$sendEmailDashboards = true;
		}
		else 
		{
			$sendEmailDashboards = false;
		}

		//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
		//En este caso se debe identificar a que pregunta de categoría de dimensión pertenece este registro. Hasta la fecha no se podían
		//combinar este tipo de preguntas con otras secciones de varios registros, así que sólo enviamos el EntryCatDimID
		$strStatus[$i] = saveData($aRepository, $surveyID, $newArrayData, $thisDate, $factKeyDimVal, $sendEmailDashboards, $countSurveys, null, 0, $intEntryCatDimID);

		if ($strStatus[$i] == "OK") 
		{
			$totalOK ++;
		}
	}
	
	if ($totalOK == $countSurveys) 
	{
		$strStatusDyn = "OK";
	}
	else
	{
		$strStatusDyn = implode(";\r\n", $strStatus);
	}
	
	return $strStatusDyn;
}

//@JAPRDescontinuada: Al día 2014-06-05 no se encontró ningún proceso que la involucrara
function getAllCatValues($aRepository, $surveyID, $sectionID, $attribID, $filter)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	
	$arrayCatPairValues = explode('_SVElem_', $filter);
	$arrayCatClaDescrip = array();
	$arrayCatValues = array();
	$arrayTemp = array();
	$arrayInfo = array();
	
	foreach($arrayCatPairValues as $element)
	{
		$arrayInfo = explode('_SVSep_', $element);
		
		//Obtener el cla_descrip y almacenar (viene asi key_1178)
		$arrayTemp = explode('_', $arrayInfo[0]);
		$arrayCatClaDescrip[] = $arrayTemp[1];
		
		//Obtener el valor del catalogo
		$arrayCatValues[] = $arrayInfo[1];
	}

	$aSection = BITAMSection::NewInstanceWithID($aRepository, $sectionID);
	
	//De este objeto $aSection obtenemos el $resultFilter resultante de todos
	//los filtros dados de alta en la tabla si_sv_catfilter si es que aplica
	$resultFilter = $aSection->generateResultFilter();
	
	$catalogID = $aSection->CatalogID;
	
	//Obtener cla_descrip del catalogo
	$sql = "SELECT ParentID FROM SI_SV_Catalog WHERE CatalogID = ".$catalogID;
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if (!$aRS || $aRS->EOF)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return null;
		}
	}

	$parentCladescrip = (int)$aRS->fields["parentid"];
	$tableName = "RIDIM_".$parentCladescrip;
	$selectField = "DSC_".$attribID;
	$selectFieldKey = "RIDIM_".$parentCladescrip."KEY";
	
	$sql = "SELECT ".$selectFieldKey.", ".$selectField." FROM ".$tableName;
		
	$whereElements = "";
	foreach($arrayCatClaDescrip as $index=>$element)
	{
		if($whereElements!="")
		{
			$whereElements.=" AND ";
		}
		$whereElements.="DSC_".$element." = ".$aRepository->DataADOConnection->Quote($arrayCatValues[$index]);
	}
	
	if($whereElements!="")
	{
		$whereElements.=" AND ";
	}
	
	$whereElements.="RIDIM_".$parentCladescrip."KEY > 1";
	
	$sql.=" WHERE ".$whereElements;
	
	if($resultFilter!="")
	{
		$sql.=" AND ";
	}
	
	$sql.=$resultFilter;

	$sql.=" ORDER BY ".$selectField." ASC, ".$selectFieldKey." ASC";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if (!$aRS)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return null;
		}
	}
	
	$existElements = array();
	$catValuesList = array();
	while(!$aRS->EOF)
	{
		$catValueID = $aRS->fields[strtolower($selectFieldKey)];
		$catValue = $aRS->fields[strtolower($selectField)];
		
		if(!isset($existElements[$catValue]))
		{
			$catValuesList[$catValueID] = $catValue;
			$existElements[$catValue] = $catValue;
		}

		$aRS->MoveNext();
	}

	return $catValuesList;
}

//@JAPR 2012-06-08: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
//Obtiene el Array con los valores de los atributos de la subsección dinámica + el de las preguntas múltiples, indexado por los IDs del atributo
//de las múltiples para poder obtener a que padre (subsección dinámica) se está haciendo referencia, y así poder identificar cual fué el valor
//del atributo que generó la página, ya que en el sectionValues llegan los IDs del atributo de las múltiples en el mismo orden en que se generaron
//la subsecciones dinámicas.
function getAllDynPagesCatValues($aRepository, $surveyID, $sectionID, $sectionAttribID, $multiAttribID, $filter)
{
	global $gblShowErrorSurvey;
	global $gblEFormsErrorMessage;
	//@JAPR 2013-02-13: Agregada la edición de datos históricos
	global $arrSectionValuesByDesc;
	global $arrSectionPagesByDesc;
	//@JAPR

	//@JAPR 2013-02-13: Agregada la edición de datos históricos
	//Si se recibe el filtro especial NOCHANGE, quiere decir que se trata de una edición y que no hubo ningún cambio en la pregunta que genera 
	//a la sección dinámica, por lo tanto se puede reconstruir a partir de los datos que hay en el reporte. Para que esto fuera válido tiene que
	//tratarse de una edición hecha con la opción de catálogos desasociados, así que también tendría que existir por tanto el array global del
	//proceso de generación del POST que ya contiene una estructura leída del reporte casi con los mismos datos que se necesitan, así que se
	//reutiliza dicha estructura. A este método sólo pudo haber llegado si existiera una sección dinámica, así que es seguro usar el array mencionado
	if (trim($filter) == "NOCHANGE") {
		if (!isset($arrSectionValuesByDesc) || !is_array($arrSectionValuesByDesc) || !isset($arrSectionValuesByDesc[$sectionID])) {
			if($gblShowErrorSurvey)
			{
				die("(".__METHOD__.") ".translate("Unable to find the dynamic section data in the specified report"));
			}
			else 
			{
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Unable to find the dynamic section data in the specified report");
				return null;
			}
		}
		
		//Genera el array en el formato esperado a partir de los valores leídos en el reporte. Si por alguna razón no hubiera habido una sección
		//dinámica, el array que regresará estará vacío lo que no debería afectar porque el proceso que lo usará debería validar en ese caso
		//la no existencia de registros dinámicos
		$catValuesList = array();
		$arrayCatValuesByPage = $arrSectionValuesByDesc[$sectionID];
		if (is_array($arrayCatValuesByPage)) {
			foreach ($arrayCatValuesByPage as $intPageKey => $arrayCatValues) {
				$catValue = (string) @$arrSectionPagesByDesc[$sectionID][$intPageKey];
				
				foreach ($arrayCatValues as $catValueID) {
					$catValuesList[$catValueID] = $catValue;
				}
			}
		}
		return $catValuesList;
	}
	//@JAPR
	
	$arrayCatPairValues = explode('_SVElem_', $filter);
	$arrayCatClaDescrip = array();
	$arrayCatValues = array();
	$arrayTemp = array();
	$arrayInfo = array();
	
	foreach($arrayCatPairValues as $element)
	{
		$arrayInfo = explode('_SVSep_', $element);
		
		//Obtener el cla_descrip y almacenar (viene asi key_1178)
		$arrayTemp = explode('_', $arrayInfo[0]);
		$arrayCatClaDescrip[] = $arrayTemp[1];
		
		//Obtener el valor del catalogo
		$arrayCatValues[] = $arrayInfo[1];
	}

	$aSection = BITAMSection::NewInstanceWithID($aRepository, $sectionID);
	
	//De este objeto $aSection obtenemos el $resultFilter resultante de todos
	//los filtros dados de alta en la tabla si_sv_catfilter si es que aplica
	$resultFilter = $aSection->generateResultFilter();
	
	$catalogID = $aSection->CatalogID;
	
	//Obtener cla_descrip del catalogo
	$sql = "SELECT ParentID FROM SI_SV_Catalog WHERE CatalogID = ".$catalogID;
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if (!$aRS || $aRS->EOF)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return null;
		}
	}

	$parentCladescrip = (int)$aRS->fields["parentid"];
	$tableName = "RIDIM_".$parentCladescrip;
	$sectionSelectField = "DSC_".$sectionAttribID;
	$multiSelectField = "DSC_".$multiAttribID;
	$selectFieldKey = "RIDIM_".$parentCladescrip."KEY";
	
	$sql = "SELECT ".$selectFieldKey.", ".$sectionSelectField.", ".$multiSelectField." FROM ".$tableName;
		
	$whereElements = "";
	foreach($arrayCatClaDescrip as $index=>$element)
	{
		if($whereElements!="")
		{
			$whereElements.=" AND ";
		}
		$whereElements.="DSC_".$element." = ".$aRepository->DataADOConnection->Quote($arrayCatValues[$index]);
	}
	
	if($whereElements!="")
	{
		$whereElements.=" AND ";
	}
	
	$whereElements.="RIDIM_".$parentCladescrip."KEY > 1";
	
	$sql.=" WHERE ".$whereElements;
	
	if($resultFilter!="")
	{
		$sql.=" AND ";
	}
	
	$sql.=$resultFilter;

	$sql.=" ORDER BY ".$sectionSelectField." ASC, ".$multiSelectField." ASC, ".$selectFieldKey." ASC";
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if (!$aRS)
	{
		if($gblShowErrorSurvey)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return null;
		}
	}
	//$existElements = array();

	$selectFieldKey = strtolower($selectFieldKey);
	$sectionSelectField = strtolower($sectionSelectField);
	$multiSelectField = strtolower($multiSelectField);
	$catValuesList = array();
	while(!$aRS->EOF)
	{
		$catValueID = $aRS->fields[$selectFieldKey];
		$catValue = $aRS->fields[$sectionSelectField];
		$multiValue = $aRS->fields[$multiSelectField];
		
		//if(!isset($existElements[$catValue]))
		//{
			$catValuesList[$catValueID] = $catValue;
			//$existElements[$catValue] = $catValue;
		//}

		$aRS->MoveNext();
	}

	return $catValuesList;
}
//@JAPR

//@JAPR 2012-04-12: Agregadas las secciones Maestro-Detalle
function hasCategoryDimensions($aRepository, $surveyID)
{
	$hasCategoryDimensions = false;
	
	$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$surveyID." AND QTypeID = ".qtpMulti." AND QDisplayMode = 0 AND MCInputType = ".mpcNumeric." AND IsMultiDimension = 0 AND UseCategoryDim = 1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if (!$aRS)
	{
		die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	if(!$aRS->EOF)
	{
		$hasCategoryDimensions = true;
	}

	return $hasCategoryDimensions;
}

function getArrayDataWithCategoryDimension($aRepository, $surveyID, $arrayData)
{
	require_once("question.inc.php");
	
	$sql = "SELECT QuestionID FROM SI_SV_Question 
			WHERE SurveyID = ".$surveyID." AND QTypeID = ".qtpMulti." AND QDisplayMode = 0 
			AND MCInputType = ".mpcNumeric." AND IsMultiDimension = 0 AND UseCategoryDim = 1 ORDER BY QuestionNumber";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if (!$aRS)
	{
		die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	$arrayData["qCatDimVal"] = array();
	$arrayData["qElemDimVal"] = array();
	$arrayData["qCatDimValQID"] = array();
	$arrayData["qElemDimValResp"] = array();

	while(!$aRS->EOF)
	{
		$questionID = (int)$aRS->fields["questionid"];
		$aQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $questionID);
		
		//Obtener valor capturado de dicha question Seleccion Multiple
		$postKey = "qfield".$aQuestion->QuestionID;
		
		if(isset($arrayData[$postKey]))
		{
			$strValues = $arrayData[$postKey];
		}
		else 
		{
			$strValues = "";
		}

		if(trim($strValues)=="")
		{
			foreach ($aQuestion->ElemDimValSelectOptions as $key=>$dimElemValue)
			{
				$dimCatValue = $aQuestion->CatDimValSelectOptions[$key];
				$responseValue = 0;
				
				$arrayData["qCatDimVal"][] = $dimCatValue;
				$arrayData["qElemDimVal"][] = $dimElemValue;
				$arrayData["qCatDimValQID"][] = $questionID;
				$arrayData["qElemDimValResp"][] = $responseValue;
			}
		}
		else 
		{
			$arrayValues = explode("_SVSep_", $arrayData[$postKey]);

			foreach ($aQuestion->ElemDimValSelectOptions as $key=>$dimElemValue)
			{
				$dimCatValue = $aQuestion->CatDimValSelectOptions[$key];
				$responseValue = 0;
				
				if(isset($arrayValues[$key]))
				{
					$responseValue = (float)$arrayValues[$key];
				}
				
				$arrayData["qCatDimVal"][] = $dimCatValue;
				$arrayData["qElemDimVal"][] = $dimElemValue;
				$arrayData["qCatDimValQID"][] = $questionID;
				$arrayData["qElemDimValResp"][] = $responseValue;
			}
		}
		
		$aRS->MoveNext();
	}
	
	return $arrayData;
}

//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
//Reemplaza las variables de los textos de acciones por la correspondiente respuesta de la pregunta a la que se hace referencia
//@JAPR 2012-10-30: Agregado el parámetro $bReplaceByNumber para controlar si se debe reemplazar la respuesta basado en el número de pregunta
//o en el ID dentro del texto especificado
//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
//Se agregó el parámetro $bForceGetAttribs para indicar que en caso de no haber podido encontrar el atributo indicado como parte de las respuestas
//porque es mayor que el máximo usado, se force a ir al catálogo usando los valores actuales de los atributos previos para obtener el solicitado,
//aunque sólo funcionará si también se pasa la conexión al repositorio
/*
//@JAPR 2014-10-29: Agregado el mapeo de Scores
//Agregado el parámetro $aSectionEvalRecord que contiene el número de registro específico que se está procesando, lo cual según como se graban los
datos siempre tendría que corresponder con una única sección de múltiples registros. Si la pregunta que se reemplaza pertenece a la sección indicada
en este array (mediante el SectionID) entonces se usará el número de registro que se indica aquí, de lo contrario usará el último registro de su
sección correspondiente (en el caso de preguntas de secciones estándar no aplica). En el server no se puede resolver por ahora variables
inter-secciones como lo hace el App, así que las fórmulas deben ser mas simples pues no se permiten sumarizados de preguntas de otras secciones
Finalmente en lugar de un parámetro $aSectionEvalRecord, se decidió dejarla como variable global para no tener que estar especificandolo en cada caso
y para cada función involucrada en el proceso, sino que el propio saveData o updateData la asignarían según la sección que estén grabando
//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
Agregado el parámetro bReturnExtendedInfo, con este parámetro aunque el resultado será el mismo, se recibe un array por referencia al cual se
agregarán todas las expresiones variables encontradas conteniendo información adicional como el tipo (pregunta o sección), id,
atributo específico, parámetro solicitado, etc. y el valor correspondiente por el cual se tradujo en la expresión proporcionada, con dicho array
se pueden hacer procesos mas complejos como determinar si hubo variables inválidas o sólo variables con ciertas configuraciones. Esta información
se agregará en un índice llamado "elements" en dicho array
*/
function replaceQuestionVariables($sText, $arrFieldValues, $bReplaceByNumber = true, $aRepository = null, $aSurveyID = 0, $bQuoteText = true, $bForceGetAttribs = false, $bReturnExtendedInfo = false, &$arrInfo = array())
{
	//@JAPR 2014-10-29: Agregado el mapeo de Scores
	global $aSectionEvalRecord;
	global $arrQuestionScoresFromApp;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceQuestionVariables expression: {$sText}");
	}
	
	//@JAPR 2015-03-23: Corregido un bug, al agregar variables de sección de catálogo donde éste era de eBavel, si se intentaban resolver cuando
	//se encontraba en un proceso relacionado con eBavel (como generación de acciones) y el atributo solicitado no venía como parte del archivo
	//.dat, se tenía que cargar información de la metadata y el FetchMode venía numérico debido al proceso de eBavel donde se encontraba, por lo 
	//que no lograba cargar la metadata y el proceso de extracción de datos de eBavel mataba al proceso de PhP
	//Se respaldará el FetchMode en esta función para cambiarlo a Assoc temporalmente, al final se restaurará el FetchMode original para no
	//afectar el proceso relacionado con eBavel
	//El resto de las funciones de reemplazo se invocarán desde esta, así que en ellas no hay necesidad de este proceso extra
	$intADOConnectionFetchMode = null;
	if (!is_null($aRepository)) {
		$intADOConnectionFetchMode = $aRepository->ADOConnection->fetchMode;
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
	}
	//@JAPR
	
	if (!isset($arrQuestionScoresFromApp) || is_null($arrQuestionScoresFromApp) || !is_array($arrQuestionScoresFromApp)) {
		$arrQuestionScoresFromApp = array();
	}
	
	//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
	//Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
	if ($bReturnExtendedInfo) {
		if (!is_array($arrInfo)) {
			$arrInfo = array();
		}
		if (!isset($arrInfo['elements'])) {
			$arrInfo['elements'] = array();
		}
	}
	
	$strText = @replaceQuestionVariablesAdv($sText, $arrFieldValues, false, $aRepository, $aSurveyID, $bQuoteText, $bForceGetAttribs, $bReturnExtendedInfo, $arrInfo);
	if (!is_null($strText)) {
		$sText = $strText;
	}
	
	//@JAPR 2014-11-24: Agregadas variables para las secciones
	//Antes de iniciar el reemplazo de las variables de preguntas, invoca al reemplazo de las variables de sección
	$strText = @replaceSectionVariables($sText, $arrFieldValues, true, $aRepository, $aSurveyID, $bQuoteText, $bForceGetAttribs);
	if (!is_null($strText)) {
		$sText = $strText;
	}
	//@JAPR
	
	$intOffSet = 0;
	$strResult = '';
	$intPos = stripos($sText, '@Q', $intOffSet);
	if ($intPos === false || is_null($arrFieldValues) || !is_array($arrFieldValues))
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n replaceQuestionVariables no variables found");
		}
		//@JAPR 2015-03-23: Corregido un bug, al agregar variables de sección de catálogo donde éste era de eBavel, si se intentaban resolver cuando
		//se encontraba en un proceso relacionado con eBavel (como generación de acciones) y el atributo solicitado no venía como parte del archivo
		//.dat, se tenía que cargar información de la metadata y el FetchMode venía numérico debido al proceso de eBavel donde se encontraba, por lo 
		//que no lograba cargar la metadata y el proceso de extracción de datos de eBavel mataba al proceso de PhP
		//Se respaldará el FetchMode en esta función para cambiarlo a Assoc temporalmente, al final se restaurará el FetchMode original para no
		//afectar el proceso relacionado con eBavel
		//El resto de las funciones de reemplazo se invocarán desde esta, así que en ellas no hay necesidad de este proceso extra
		if (!is_null($intADOConnectionFetchMode)) {
			$aRepository->ADOConnection->SetFetchMode($intADOConnectionFetchMode);
		}
		//@JAPR
		return $sText;
	}
	
	//@JAPR 2014-10-29: Agregado el mapeo de Scores
	if (is_null($aSectionEvalRecord) || !is_array($aSectionEvalRecord)) {
		$aSectionEvalRecord = array();
	}
	
	//@JAPR 2012-10-30: Agregado el parámetro $bReplaceByNumber para controlar si se debe reemplazar la respuesta basado en el número de pregunta
	//o en el ID dentro del texto especificado
	$arrQuestionsByID = array();
	$arrQuestionObjsByNum = array();
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	//Ahora siempre se debe cargar la colección de preguntas
	//if (!$bReplaceByNumber) {
		//Carga la colección completa de preguntas de la encuesta (esta llamada debería estar en el caché, así que sólo la primera vez sería
		//tardada teoricamente)
		if (!is_null($aRepository)) {
			$aQuestionsColl = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
			if (!is_null($aQuestionsColl)) {
				foreach ($aQuestionsColl as $aQuestion) {
					$arrQuestionsByID[$aQuestion->QuestionID] = $aQuestion->QuestionNumber;
					$arrQuestionObjsByNum[$aQuestion->QuestionNumber] = $aQuestion;
				}
			}
		}
	//}
	//@JAPR
	
	$strText = $sText;
	while ($intPos !== false)
	{
		if ($intOffSet != $intPos)
		{
			$strResult .= substr($strText, $intOffSet, $intPos - $intOffSet);
		}
		
		$strQuestionID = '';
		$strAttributePos = '';
		$i = 2;
		$intID = substr($sText, $intPos + $i, 1);
		$intPosClosure = null;
		if (!is_numeric($intID))
		{
			$strResult .= "@Q";
		}
		else 
		{
			//@JAPR 2012-06-20: Modificado para que los parámetros para reemplazar las respuestas dentro de las Acciones de eBavel especifiquen el numero
			//de atributo del catálogo para el caso de las Simple Choice de catálogo
			//Se trata de un prefijo para una variable de reemplazo de respuesta, así que obtiene el ID de la pregunta. El formato es:
			//@Q###(pos), dónde ### es el ID de la pregunta, y pos es el número de atributo a utilizar en el caso de las preguntas simple choice
			//de catálogo (se puede omitir por completo el pos y en ese caso se usaría el último atributo del valor seleccionado, lo mismo si se
			//especifica una posición mayor a la última usada en la respuesta)
			while (is_numeric($intID))
			{
				$strQuestionID .= $intID;
				$i++;
				$intID = substr($sText, $intPos + $i, 1);
			}
			
			//@JAPR 2014-10-29: Agregado el mapeo de Scores
			$blnGetScore = false;
			//@JAPR
			
			//Obtiene la posición del atributo, lo cual se define por el siguiente valor entre () inmediato a la definición del parámetro
			$intPostOpening = stripos($sText, '(', $intPos + $i);
			$intPosClosure = stripos($sText, ')', $intPos + $i);
			$intPosNextParam = stripos($sText, '@Q', $intPos + $i);
			if ($intPostOpening !== false && $intPosClosure !== false && $intPostOpening < $intPosClosure && ($intPosNextParam === false || $intPosNextParam > $intPosClosure))
			{
				//Existen paréntesis después de la definición de este parámetro, ahora verifica que se encuentren antes del siguiente parámetro
				//e inmediatamente después del parámetro actual
				$strCharsBetween = trim(str_replace(".", "", (string) @substr($sText, $intPos + $i, $intPostOpening - ($intPos + $i))));
				//@JAPR 2014-10-29: Agregado el mapeo de Scores
				//Ahora se permitirá la variable en formato @Q.score() para permitir obtener el score de dicha pregunta
				$blnGetScore = (strtolower($strCharsBetween) == 'score');
				if ($strCharsBetween == '' || $blnGetScore)
				{
					$strAttributePos = (string) @substr($sText, $intPostOpening +1, $intPosClosure - ($intPostOpening +1));
					if (!is_numeric($strAttributePos) || $blnGetScore)
					{
						$strAttributePos = '';
					}
				}
				else 
				{
					//Esto evitará que se consideren los paréntesis siguientes a la variable como parte de la misma variable
					$intPosClosure = null;
				}
			}
			else 
			{
				//Esto evitará que se consideren los paréntesis siguientes a la variable como parte de la misma variable
				$intPosClosure = null;
			}
			
			//@JAPR 2012-10-30: Agregado el parámetro $bReplaceByNumber para controlar si se debe reemplazar la respuesta basado en el número de pregunta
			//o en el ID dentro del texto especificado
			if (!$bReplaceByNumber) {
				//Obtiene el número de pregunta basado en el ID (si ya no existe, se regresará 0 así que termina reemplazando por vacio)
				$strQuestionID = (int) @$arrQuestionsByID[$strQuestionID];
			}
			
			//@JAPR 2012-06-20: Modificado para que los parámetros para reemplazar las respuestas dentro de las Acciones de eBavel especifiquen el numero
			//de atributo del catálogo para el caso de las Simple Choice de catálogo
			//Si la respuesta es un Array, se verifica si se especificó una posición y de ser así se intenta usar dicho atributo si es que viene
			//en el array, de lo contrario si el atributo no viene o no se especificó entonces se usará el último atributo
			//@JAPR 2014-10-29: Agregado el mapeo de Scores
			if ($blnGetScore) {
				//Este es un array multidimensional con el valor por cada registro posible de esa pregunta si es que pertenecía a una sección
				//multi-registro (dinámica, Inline o maestro-detalle), así que se debe obtener el valor correcto según el parámetro $aSectionEvalRecord
				$anAnswer = @$arrQuestionScoresFromApp['qfield'.$strQuestionID];
			}
			else {
				$anAnswer = @$arrFieldValues['qfield'.$strQuestionID];
			}
			//@JAPR
			if (!is_null($anAnswer) && is_array($anAnswer))
			{
				$intNumAttrs = count($anAnswer);
				if ($intNumAttrs == 0)
				{
					$anAnswer = '';
				}
				else
				{
					$strAttributePos = (int) $strAttributePos;
					if ($strAttributePos > 0 && $strAttributePos <= $intNumAttrs)
					{
						$anAnswer = (string) @$anAnswer[$strAttributePos -1];
					}
					else 
					{
						//Usará el último elemento (también entra aquí si no se pide un atributo sino simplemente se quiere el valor normal)
						$arrAnswer = $anAnswer;
						//@JAPR 2014-10-29: Agregado el mapeo de Scores
						//Agregado el parámetro $aSectionEvalRecord que contiene el número de registro específico que se está procesando
						//Si la sección de esta pregunta se encuentra definida en este array, entonces se usará el valor de la posición indicada
						//por esta pregunta. Este proceso sólo aplica si se trata de obtener el score, ya que de tratarse del valor en si de la
						//pregunta entonces ya vendría asignado para el registro correspondiente que se estuviera procesando
						if ($blnGetScore) {
							//Primero debe obtener el ID de la sección correspondiente para determinar si usará o no un registro específico
							$intSectionID = 0;
							$intRecordNumber = 0;
							if (isset($arrQuestionObjsByNum[$strQuestionID])) {
								$aQuestion = $arrQuestionObjsByNum[$strQuestionID];
								$intSectionID = $aQuestion->SectionID;
								if (isset($aSectionEvalRecord[$intSectionID])) {
									$intRecordNumber = (int) @$aSectionEvalRecord[$intSectionID];
								}
							}
							
							$anAnswer = (string) @$anAnswer[$intRecordNumber];
						}
						else {
							$anAnswer = (string) @$anAnswer[$intNumAttrs -1];
						}
						
						//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
						//La pregunta tendría que ser de catálogo para haber llegado aquí además de haber pedido un atributo que no venía como
						//parte de la propia pregunta, por tanto lo que tiene que hacer es cargar el valor del catálogo siguiendo la combinación
						//que si se tiene contestada en esta pregunta, tomando el primer valor devuelto para el atributo especificado. Esto sólo
						//se puede si se mandó el repositorio y se está forzando a pedir todos los atributos
						if (isset($arrQuestionObjsByNum[$strQuestionID])) {
							$aQuestion = $arrQuestionObjsByNum[$strQuestionID];
							//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
							//@JAPR 2014-10-14: Corregido un bug, las preguntas tipo acción sólo deben usar como respuesta el texto de la acción
							switch ($aQuestion->QTypeID) {
								case qtpGPS:
									//Si se trata de una pregunta tipo GPS, no debe regresar la parte del Accuray
									$intPos = stripos($anAnswer, '(');
									if ($intPos !== false && $intPos > 0) {
										$anAnswer = substr($anAnswer, 0, $intPos);
									}
									break;
							}
							//@JAPR
							
							if ($bForceGetAttribs && $aRepository) {
								if ($aQuestion->CatalogID > 0) {
									$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($aRepository, $aQuestion->CatalogID);
									if (!is_null($objCatalogMembersColl)) {
										//No hay garantías sobre esta funcionalidad, ya que depende de que al momento de hacer el grabado, la combinación
										//referenciada aún existiera en el catálogo, si no es así simplemente no se mandarán EMails adicionales. Esto es
										//porque la captura no manda todos los datos del catálogo sino sólo lo que utiliza
										$strFields = "";
										$strGroupBy = "";
										$strFilter = '';
										$intClaDescripParent = 0;
										foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
											$intClaDescripParent = $objCatalogMember->ClaDescripParent;
											$strField = "DSC_{$objCatalogMember->ClaDescrip}";
											$strFields .= ", {$strField} AS Attr{$objCatalogMember->MemberOrder}";
											if (isset($arrAnswer[$objCatalogMember->MemberOrder -1])) {
												$strFilter .= " AND {$strField} = ".$aRepository->DataADOConnection->Quote($arrAnswer[$objCatalogMember->MemberOrder -1]);
												$strGroupBy .= ", $strField";
											}
										}
										
										//Si se encontró por lo menos un atributo, se pudo determinar el ID de la tabla de catálogo
										//Tiene que haber por lo menos un atributo en el filtro, de lo contrario no hubiera llegado aquí porque la mínima
										//pregunta de catálogo debe preguntar al menos un atributo. Agrupa por los atributos que si fueron respondidos
										//de tal forma que aquellos que no lo fueron obtengan sólo el primero de los posibles valores de cada uno
										if ($intClaDescripParent > 0) {
											$strFields = substr($strFields, 2);
											$strFilter = substr($strFilter, 5);
											$strGroupBy = substr($strGroupBy, 2);
											$sql = "SELECT {$strFields} 
												FROM RIDIM_{$intClaDescripParent} 
												WHERE {$strFilter} 
												GROUP BY {$strGroupBy}";
											$aRS = $aRepository->DataADOConnection->Execute($sql);
											if ($aRS && !$aRS->EOF) {
												//Si todo se resolvió correctamente, obtiene el valor del atributo indicado
												$anAnswer = (string) @$aRS->fields['attr'.$strAttributePos];
											}
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												echo("<br>\r\n replaceQuestionVariables for Catalog: {$aQuestion->CatalogID}, Attribute: {$strAttributePos}, Value: {$anAnswer}, Query: {$sql}");
											}
										}
									}
								}
							}
						}
						//@JAPR
					}
				}
			}
			
			//@JAPR 2014-10-14: Corregido un bug, las preguntas tipo acción sólo deben usar como respuesta el texto de la acción
			if (isset($arrQuestionObjsByNum[$strQuestionID])) {
				$aQuestion = $arrQuestionObjsByNum[$strQuestionID];
				switch ($aQuestion->QTypeID) {
					case qtpAction:
						//Si se trata de una pregunta tipo acción, la respuesta viene como la concatenación de sus 4 componentes, así
						//que se debe extraer el último dato que corresponde con la descripción de la acción y usar eso como la respuesta
						if (trim($anAnswer) != '') {
							$arrParts = @explode('_SVSep_', $anAnswer);
							if (!is_null($arrParts) && is_array($arrParts)) {
								$anAnswer = (string) @$arrParts[count($arrParts)-1];
							}
						}
						break;
				}
			}
			//@JAPR	
			
			$strAnswer = (string) $anAnswer;
			
			//$strAnswer = (string) @$arrFieldValues['qfield'.$strQuestionID];
			if (!is_numeric($strAnswer) && $bQuoteText)
			{
				$strAnswer = "'".$strAnswer."'";
			}
			
			$strResult .= $strAnswer;
		}
		
		//Si se definió un atributo específico a usar, se remueve también la parte de los paréntesis moviendo
		//el offset justo despues de los mismos, de lo contrario sólo remueve la definición de la variable
		if (!is_null($intPosClosure) && $intPosClosure > $intPos + $i)
		{
			//Se posiciona en el caracter siguiente al paréntesis de definición del número de atributo
			$intOffSet = $intPosClosure +1;
		}
		else 
		{
			//Se posiciona justo después de la definición de la variable
			$intOffSet = $intPos + $i;
		}
		$intPos = stripos($sText, '@Q', $intOffSet);
	}
	
	//@JAPR 2014-01-27: Corregido un bug, después de reemplazar las variables no estaba copiando el resto del texto sin variables
	if ($intOffSet > 0) {
		$strResult .= substr($strText, $intOffSet);
	}
	//@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceQuestionVariables result: {$strResult}");
	}
	
	//@JAPR 2015-03-23: Corregido un bug, al agregar variables de sección de catálogo donde éste era de eBavel, si se intentaban resolver cuando
	//se encontraba en un proceso relacionado con eBavel (como generación de acciones) y el atributo solicitado no venía como parte del archivo
	//.dat, se tenía que cargar información de la metadata y el FetchMode venía numérico debido al proceso de eBavel donde se encontraba, por lo 
	//que no lograba cargar la metadata y el proceso de extracción de datos de eBavel mataba al proceso de PhP
	//Se respaldará el FetchMode en esta función para cambiarlo a Assoc temporalmente, al final se restaurará el FetchMode original para no
	//afectar el proceso relacionado con eBavel
	//El resto de las funciones de reemplazo se invocarán desde esta, así que en ellas no hay necesidad de este proceso extra
	if (!is_null($intADOConnectionFetchMode)) {
		$aRepository->ADOConnection->SetFetchMode($intADOConnectionFetchMode);
	}
	//@JAPR
	return $strResult;
}

//@JAPR 2015-03-05: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
/* Función equivalente a replaceQuestionVariables pero con variables en el nuevo formato extendido. Realiza los mismos reemplazos que el server
había realizado hasta el día de hoy, los cuales no corresponden exactamente a los que son posible realizar en el App dado al cambio de contexto,
pero debe ser completamente compatible con la otra función de reemplazo de variables en el viejo formato
Esta función se utilizará para resolver filtros dinámicos generados para agendas durante la obtención de definiciones, simulando el array de
datos de un grabado de captura, esto para permitir generar el array de valores de todos los catálogos que pudieran estar filtrados a partir de
alguna pregunta que corresponda con el propio catálogo de la agenda
//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
*/
function replaceQuestionVariablesAdv($sText, $arrFieldValues, $bReplaceByNumber = true, $aRepository = null, $aSurveyID = 0, $bQuoteText = true, $bForceGetAttribs = false, $bReturnExtendedInfo = false, &$arrInfo = array())
{
	//@JAPR 2014-10-29: Agregado el mapeo de Scores
	global $aSectionEvalRecord;
	global $arrQuestionScoresFromApp;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceQuestionVariablesAdv expression: {$sText}");
	}
	
	if (!isset($arrQuestionScoresFromApp) || is_null($arrQuestionScoresFromApp) || !is_array($arrQuestionScoresFromApp)) {
		$arrQuestionScoresFromApp = array();
	}
	
	//@JAPR 2015-03-12: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
	//Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
	if ($bReturnExtendedInfo) {
		if (!is_array($arrInfo)) {
			$arrInfo = array();
		}
		if (!isset($arrInfo['elements'])) {
			$arrInfo['elements'] = array();
		}
	}
	//@JAPR
	
	$intOffSet = 0;
	$intPos = stripos($sText, '{$Q', $intOffSet);
	if ($intPos === false || is_null($arrFieldValues) || !is_array($arrFieldValues))
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n replaceQuestionVariablesAdv no variables found");
		}
		return $sText;
	}
	
	//@JAPR 2014-10-29: Agregado el mapeo de Scores
	if (is_null($aSectionEvalRecord) || !is_array($aSectionEvalRecord)) {
		$aSectionEvalRecord = array();
	}
	
	//@JAPR 2012-10-30: Agregado el parámetro $bReplaceByNumber para controlar si se debe reemplazar la respuesta basado en el número de pregunta
	//o en el ID dentro del texto especificado
	$arrQuestionsByID = array();
	$arrQuestionObjsByNum = array();
	$arrQuestionObjsByID = array();
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	//Ahora siempre se debe cargar la colección de preguntas
	//if (!$bReplaceByNumber) {
		//Carga la colección completa de preguntas de la encuesta (esta llamada debería estar en el caché, así que sólo la primera vez sería
		//tardada teoricamente)
		if (!is_null($aRepository)) {
			$aQuestionsColl = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
			if (!is_null($aQuestionsColl)) {
				foreach ($aQuestionsColl->Collection as $aQuestion) {
					$arrQuestionsByID[$aQuestion->QuestionID] = $aQuestion->QuestionNumber;
					$arrQuestionObjsByNum[$aQuestion->QuestionNumber] = $aQuestion;
					$arrQuestionObjsByID[$aQuestion->QuestionID] = $aQuestion;
				}
			}
		}
	//}
	//@JAPR
	
	$strText = $sText;
	$pattern = '/[\{]{1}[$]{1}[Q|q]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/';
	preg_match_all($pattern, $strText, $arrayEvalQuestionsAdv);
	$arrayQuestionsAdv = array();
	if (count($arrayEvalQuestionsAdv[0]) > 0) {
		$arrayQuestionsAdv = array_unique($arrayEvalQuestionsAdv[0]);
	}
	
	foreach ($arrayQuestionsAdv as $strExpr) {
		//Primero intentará separar por el total de propiedades/funciones especificadas, esto es las cadenas separadas por '.'. Siempre la última es la
		//única que podría ser una función así que se valida si está o no definda así como su parámetro en caso de haber alguno
		//Sólo puede haber una propiedad, una función, o una propiedad + una función, todo aplicando a una misma pregunta y/o sección
		$strProperty = '';
		$strFunction = '';
		$strParam = '';
		$arrParts = explode('.', $strExpr);
		$strQuestionID = $arrParts[0];
		$blnValid = true;
		
		$strQuestionID = (int) @str_ireplace(array('{', '}', '$', 'S', 'Q'), '', $strQuestionID);
		if ($strQuestionID <= 0) {
			continue;
		}
		
		//Identifica el resto de los componentes de la variable
		switch (count($arrParts)) {
			case 1:	//Sólo es la definición de la variable, así que la propiedad default es value
				$strProperty = 'value';
				break;
			case 2: //Es una propiedad o función basado en si la segunda parte contiene o no ()
				$strFunction = str_ireplace('}', '', $arrParts[1]);
				$arrParams = explode('(', $strFunction);
				switch ($arrParams.length) {
					case 1: //Es sólo una propiedad
						$strProperty = $strFunction;
						$strFunction = '';
						break;
					default: //Sólo debería haber un único paréntesis y entonces significa que es una función
						$strFunction = $arrParams[0];
						$strProperty = $strFunction;
						$strParam = str_ireplace(')', '', $arrParams[1]);
						break;
				}
				break;
			case 3: //Es una propiedad + función (caso no soportado con la actual expresión regular)
				$strProperty = str_ireplace('}', '', $arrParts[1]);
				$strFunction = str_ireplace('}', '', $arrParts[2]);
				$arrParams = explode('(', $strFunction);
				$strFunction = $arrParams[0];
				if (count($arrParams) > 1) {
					$strParam = str_ireplace(')', '', $arrParams[1]);
				}
				break;
			default: //No se soporta esta expresión, aunque no debería haber aceptado nada similar a este caso
				$blnValid = false;
				break;
		}
		
		if (!$blnValid) {
			continue;
		}
		
		$strPropertyCS = $strProperty;
		$strProperty = strtolower($strProperty);
		$strFunction = strtolower($strFunction);
		$strParam = strtolower($strParam);
		$strParam = str_ireplace("\'\"", '', $strParam);
		//@JAPR 2015-03-12: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		//Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
		if ($bReturnExtendedInfo) {
			$arrInfo['elements'][$strExpr] = array();
			$arrInfo['elements'][$strExpr]['type'] = otyQuestion;
			$arrInfo['elements'][$strExpr]['id'] = $strQuestionID;
			$arrInfo['elements'][$strExpr]['expr'] = $strExpr;
			$arrInfo['elements'][$strExpr]['prop'] = $strProperty;
			$arrInfo['elements'][$strExpr]['fn'] = $strFunction;
			$arrInfo['elements'][$strExpr]['param'] = $strParam;
		}
		//@JAPR
		
		//Hasta este punto ya se tienen identificados los diferentes componentes de la variable, ahora deber ejecutar procesos diferentes según tu tipo
		$anAnswer = '';
		//@JAPR 2015-03-23: Corregido un bug, no estaba usando el parámetro $bReplaceByNumber correctamente, a partir de ahora ya será usado como
		//debía, sin embargo los casos donde se hubieran grabado mal los mapeos tendrían que ser re-mapeados
		if (!$bReplaceByNumber) {
			//Obtiene el número de pregunta basado en el ID (si ya no existe, se regresará 0 así que termina reemplazando por vacio)
			$strQuestionID = (int) @$arrQuestionsByID[$strQuestionID];
		}
		$aQuestion = @$arrQuestionObjsByNum[$strQuestionID];
		//$aQuestion = @$arrQuestionObjsByID[$strQuestionID];
		//@JAPR
		
		switch ($strProperty) {
			case 'comment':
				//$anAnswer = objAnswer.comment;
				break;
			case 'days':
				//$anAnswer = objAnswer.daysDueDate;
				break;
			case 'value':
				//Este array siempre viene indexado por el número de pregunta, así que la variable ya debe estar ajustada
				$anAnswer = @$arrFieldValues['qfield'.$strQuestionID];
				if (!is_null($anAnswer) && is_array($anAnswer)) {
					$intNumAttrs = count($anAnswer);
					$anAnswer = (string) @$anAnswer[$intNumAttrs -1];
				}
				
				//@JAPR 2014-10-14: Corregido un bug, las preguntas tipo acción sólo deben usar como respuesta el texto de la acción
				if (!is_null($aQuestion)) {
					switch ($aQuestion->QTypeID) {
						case qtpAction:
							//Si se trata de una pregunta tipo acción, la respuesta viene como la concatenación de sus 4 componentes, así
							//que se debe extraer el último dato que corresponde con la descripción de la acción y usar eso como la respuesta
							if (trim($anAnswer) != '') {
								$arrParts = @explode('_SVSep_', $anAnswer);
								if (!is_null($arrParts) && is_array($arrParts)) {
									$anAnswer = (string) @$arrParts[count($arrParts)-1];
								}
							}
							break;
					}
				}
				break;
			case 'score':
				//Este es un array multidimensional con el valor por cada registro posible de esa pregunta si es que pertenecía a una sección
				//multi-registro (dinámica, Inline o maestro-detalle), así que se debe obtener el valor correcto según el parámetro $aSectionEvalRecord
				//Este array siempre viene indexado por el número de pregunta, así que la variable ya debe estar ajustada
				$anAnswer = @$arrQuestionScoresFromApp['qfield'.$strQuestionID];
				if (!is_null($anAnswer) && is_array($anAnswer)) {
					//Primero debe obtener el ID de la sección correspondiente para determinar si usará o no un registro específico
					$intSectionID = 0;
					$intRecordNumber = 0;
					if (!is_null($aQuestion)) {
						$intSectionID = $aQuestion->SectionID;
						if (isset($aSectionEvalRecord[$intSectionID])) {
							$intRecordNumber = (int) @$aSectionEvalRecord[$intSectionID];
						}
					}
					
					$anAnswer = (string) @$anAnswer[$intRecordNumber];
				}
				break;
			case 'attr':
			//@JAPR 2016-04-28: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
			case 'attrid':
			case 'attrdid':
				$blnLookForID = false;
				if ($strProperty == 'attrid' || $strProperty == 'attrdid') {
					$blnLookForID = true;
				}
				//@JAPR
				
				//Este array siempre viene indexado por el número de pregunta, así que la variable ya debe estar ajustada
				$anAnswer = @$arrFieldValues['qfield'.$strQuestionID];
				if (!is_null($anAnswer) && is_array($anAnswer)) {
					$intNumAttrs = count($anAnswer);
					if ($intNumAttrs == 0)
					{
						$anAnswer = '';
					}
					else {
						$intAttributeNum = -1;
						if (!is_null($aQuestion) && !is_null($aRepository) && $aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID > 0) {
							if ((int) $strParam > 0) {
								//Se asigna un número de atributo directo
								$intAttributeNum = (int) $strParam;
								//@JAPR 2016-04-28: Corregido un bug, se había implementado mal la revisión de propiedades attrid y attrdid, lo estaba haciendo cuando no era un número
								//el valor recibido entre paréntesis
								//@JAPR 2016-01-15: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
								$blnValidAttribute = true;
								if ($blnLookForID) {
									//En este caso el número realmente corresponde a un ID de atributo, así que obtiene la posición antes de continuar
									//En este caso intAttributeNum representa un ID de atributo en este punto, así que se busca la referencia directa por él para obtener el número
									$blnValidAttribute = false;
									$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($aRepository, $aQuestion->CatalogID);
									if (!is_null($objCatalogMembersColl)) {
										foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
											//OMMC 2016-03-01: Agregados los datasourcememberID
											switch ($strProperty) {
												case 'attrdid':
													if (strtolower($objCatalogMember->DataSourceMemberID) == $intAttributeNum) {
														//@JAPR 2016-04-28: Corregido un bug, esta asignación debe ser con el número de atributo, no con el ID
														$intAttributeNum = $objCatalogMember->MemberOrder;
														//Ahora verificará si el atributo es válido para cuando se pida por ID y el ID especificado ya no esté en el catálogo
														$blnValidAttribute = true;
													}
													break;
												case 'attrid':
													if (strtolower($objCatalogMember->MemberID) == $intAttributeNum) {
														//@JAPR 2016-04-28: Corregido un bug, se estaba restando uno al orden, pero en este contexto se espera numérico iniciando en 1
														$intAttributeNum = $objCatalogMember->MemberOrder;
														//Ahora verificará si el atributo es válido para cuando se pida por ID y el ID especificado ya no esté en el catálogo
														$blnValidAttribute = true;
													}
													break;
											}
										}
									}
								}
								
								//Si no se encontró el atributo, limpia el número de atributo para que no se procese mas abajo
								if (!$blnValidAttribute) {
									$intAttributeNum = -1;
								}
								//@JAPR
							}
							else {
								$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($aRepository, $aQuestion->CatalogID);
								if (!is_null($objCatalogMembersColl)) {
									foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
										if (strtolower($objCatalogMember->MemberName) == $strParam) {
											$intAttributeNum = $objCatalogMember->MemberOrder;
											//@JAPR 2016-04-28: Corregido un bug, había quedado mal implementado sin hacer un break sino con un else reseteando el valor, así que
											//aunque lo encontrara, si había mas atributos después de encontrarlo hubiera terminado removiendo la referencia
											break;
										}
									}
								}
							}
							
							$strAttributePos = (int) $intAttributeNum;
							if ($strAttributePos > 0 && $strAttributePos <= $intNumAttrs)
							{
								$anAnswer = (string) @$anAnswer[$strAttributePos -1];
							}
							else {
								//Usará el último elemento (también entra aquí si no se pide un atributo sino simplemente se quiere el valor normal)
								$arrAnswer = $anAnswer;
								$anAnswer = (string) @$anAnswer[$intNumAttrs -1];
							}
							
							//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
							//La pregunta tendría que ser de catálogo para haber llegado aquí además de haber pedido un atributo que no venía como
							//parte de la propia pregunta, por tanto lo que tiene que hacer es cargar el valor del catálogo siguiendo la combinación
							//que si se tiene contestada en esta pregunta, tomando el primer valor devuelto para el atributo especificado. Esto sólo
							//se puede si se mandó el repositorio y se está forzando a pedir todos los atributos
							if ($bForceGetAttribs && $aQuestion && $strAttributePos > $intNumAttrs) {
								$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($aRepository, $aQuestion->CatalogID);
								if (!is_null($objCatalogMembersColl)) {
									//No hay garantías sobre esta funcionalidad, ya que depende de que al momento de hacer el grabado, la combinación
									//referenciada aún existiera en el catálogo, si no es así simplemente no se mandarán EMails adicionales. Esto es
									//porque la captura no manda todos los datos del catálogo sino sólo lo que utiliza
									$strFields = "";
									$strGroupBy = "";
									$strFilter = '';
									$intClaDescripParent = 0;
									foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
										$intClaDescripParent = $objCatalogMember->ClaDescripParent;
										$strField = "DSC_{$objCatalogMember->ClaDescrip}";
										$strFields .= ", {$strField} AS Attr{$objCatalogMember->MemberOrder}";
										if (isset($arrAnswer[$objCatalogMember->MemberOrder -1])) {
											$strFilter .= " AND {$strField} = ".$aRepository->DataADOConnection->Quote($arrAnswer[$objCatalogMember->MemberOrder -1]);
											$strGroupBy .= ", $strField";
										}
									}
									
									//Si se encontró por lo menos un atributo, se pudo determinar el ID de la tabla de catálogo
									//Tiene que haber por lo menos un atributo en el filtro, de lo contrario no hubiera llegado aquí porque la mínima
									//pregunta de catálogo debe preguntar al menos un atributo. Agrupa por los atributos que si fueron respondidos
									//de tal forma que aquellos que no lo fueron obtengan sólo el primero de los posibles valores de cada uno
									if ($intClaDescripParent > 0) {
										$strFields = substr($strFields, 2);
										$strFilter = substr($strFilter, 5);
										$strGroupBy = substr($strGroupBy, 2);
										$sql = "SELECT {$strFields} 
											FROM RIDIM_{$intClaDescripParent} 
											WHERE {$strFilter} 
											GROUP BY {$strGroupBy}";
										$aRS = $aRepository->DataADOConnection->Execute($sql);
										if ($aRS && !$aRS->EOF) {
											//Si todo se resolvió correctamente, obtiene el valor del atributo indicado
											$anAnswer = (string) @$aRS->fields['attr'.$strAttributePos];
										}
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											echo("<br>\r\n replaceQuestionVariablesAdv for Catalog: {$aQuestion->CatalogID}, Attribute: {$strAttributePos}, Value: {$anAnswer}, Query: {$sql}");
										}
									}
								}
							}
						}
					}
				}
		}
		
		$strAnswer = (string) $anAnswer;
		
		//$strAnswer = (string) @$arrFieldValues['qfield'.$strQuestionID];
		if (!is_numeric($strAnswer) && $bQuoteText)
		{
			$strAnswer = "'".$strAnswer."'";
		}
		
		//@JAPR 2015-03-12: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		//Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
		if ($bReturnExtendedInfo) {
			$arrInfo['elements'][$strExpr]['value'] = $strAnswer;
		}
		//@JAPR
		
		$strText = str_ireplace($strExpr, $strAnswer, $strText);
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceQuestionVariablesAdv result: {$strText}");
	}
	return $strText;
}

//@JAPR 2014-11-24: Agregadas variables para las secciones
/*Reemplaza las variables de los textos de acciones por el correspondiente valor de la sección a la que se hace referencia. Es equivalente a la
función replaceQuestionVariables en cuanto al funcionamiento de los parámetros, y de hecho es invocada antes de ejecutar dicha función
//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
*/
function replaceSectionVariables($sText, $arrFieldValues = null, $bReplaceByNumber = true, $aRepository = null, $aSurveyID = 0, $bQuoteText = true, $bForceGetAttribs = false, $bReturnExtendedInfo = false, &$arrInfo = array())
{
	global $aSectionEvalPage;
	global $aSectionEvalRecord;
	global $arrSectionDescByID;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceSectionVariables expression: {$sText}");
	}
	
	//@JAPR 2014-11-24: Agregadas variables para las secciones
	//Antes de iniciar el reemplazo de las variables de preguntas, invoca al reemplazo de las variables de sección
	//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
	//Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
	/*
	if (bReturnExtendedInfo) {
		if (!is_array($arrInfo)) {
			$arrInfo = array();
		}
		if (!isset($arrInfo['elements'])) {
			$arrInfo['elements'] = array();
		}
	}
	
	$strText = @replaceSectionVariablesAdv($sText, $arrFieldValues, false, $aRepository, $aSurveyID, $bQuoteText, $bForceGetAttribs, $bReturnExtendedInfo, $arrInfo);
	if (!is_null($strText)) {
		$sText = $strText;
	}
	*/
	//@JAPR
	
	$intOffSet = 0;
	$strResult = '';
	$intPos = stripos($sText, '@S', $intOffSet);
	if ($intPos === false)
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n replaceSectionVariables no variables found");
		}
		return $sText;
	}
	
	//@JAPR 2012-10-30: Agregado el parámetro $bReplaceByNumber para controlar si se debe reemplazar la respuesta basado en el número de pregunta
	//o en el ID dentro del texto especificado
	$arrSectionsByID = array();
	$arrSectionObjsByNum = array();
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	//Ahora siempre se debe cargar la colección de secciones
	if (!is_null($aRepository)) {
		$aSectionsColl = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID);
		if (!is_null($aSectionsColl)) {
			foreach ($aSectionsColl->Collection as $aSection) {
				$arrSectionsByID[$aSection->SectionID] = $aSection->SectionNumber;
				$arrSectionObjsByNum[$aSection->SectionNumber] = $aSection;
			}
		}
	}
	//@JAPR
	
	$strText = $sText;
	while ($intPos !== false)
	{
		if ($intOffSet != $intPos)
		{
			$strResult .= substr($strText, $intOffSet, $intPos - $intOffSet);
		}
		
		$strSectionID = '';
		$strAttributePos = '';
		$strParameter = '';
		$i = 2;
		$intID = substr($sText, $intPos + $i, 1);
		$intPosClosure = null;
		if (!is_numeric($intID))
		{
			$strResult .= "@S";
		}
		else 
		{
			//@JAPR 2012-06-20: Modificado para que los parámetros para reemplazar las respuestas dentro de las Acciones de eBavel especifiquen el numero
			//de atributo del catálogo para el caso de las Simple Choice de catálogo
			//Se trata de un prefijo para una variable de reemplazo de respuesta, así que obtiene el ID de la pregunta. El formato es:
			//@S###(pos), dónde ### es el ID de la pregunta, y pos es el número de atributo a utilizar en el caso de las preguntas simple choice
			//de catálogo (se puede omitir por completo el pos y en ese caso se usaría el último atributo del valor seleccionado, lo mismo si se
			//especifica una posición mayor a la última usada en la respuesta)
			while (is_numeric($intID))
			{
				$strSectionID .= $intID;
				$i++;
				$intID = substr($sText, $intPos + $i, 1);
			}
			
			//@JAPR 2014-10-29: Agregado el mapeo de Scores
			$blnGetScore = false;
			//@JAPR
			
			//Obtiene la posición del atributo, lo cual se define por el siguiente valor entre () inmediato a la definición del parámetro
			$intPostOpening = stripos($sText, '(', $intPos + $i);
			$intPosClosure = stripos($sText, ')', $intPos + $i);
			$intPosNextParam = stripos($sText, '@S', $intPos + $i);
			if ($intPostOpening !== false && $intPosClosure !== false && $intPostOpening < $intPosClosure && ($intPosNextParam === false || $intPosNextParam > $intPosClosure))
			{
				//Existen paréntesis después de la definición de este parámetro, ahora verifica que se encuentren antes del siguiente parámetro
				//e inmediatamente después del parámetro actual
				$strCharsBetween = trim(str_replace(".", "", (string) @substr($sText, $intPos + $i, $intPostOpening - ($intPos + $i))));
				//@JAPR 2014-10-29: Agregado el mapeo de Scores
				//Ahora se permitirá la variable en formato @S.score() para permitir obtener el score de dicha pregunta
				$blnGetScore = (strtolower($strCharsBetween) == 'score');
				if ($strCharsBetween == '' || $blnGetScore)
				{
					$strAttributePos = (string) @substr($sText, $intPostOpening +1, $intPosClosure - ($intPostOpening +1));
					if (!is_numeric($strAttributePos) || $blnGetScore)
					{
						$strAttributePos = '';
					}
					
					//Si se está pidiendo el score, entonces el parámetro se cambia a Score precisamente
					if ($blnGetScore) {
						$strParameter = $strCharsBetween;
					}
					//@JAPR 2014-12-10: Corregido un bug, no estaba asignando el número de parámetro
					elseif ($strAttributePos !== '') {
						//Si es diferente de vacio, por fuerza tiene que ser numérico así que representa el número de atributo a pedir
						$strParameter = $strAttributePos;
					}
					//@JAPR
				}
				else 
				{
					//@JAPR 2014-12-10: Corregido un bug, no estaba considerando a los atributos en formato .attrib() sino que limpiaba todo rastro que no fuera
					//el formato de posición de atributo entre paréntesis
					//En este caso el parámetro será el dato antes de los paréntesis
					if (!is_numeric($strCharsBetween)) {
						$strParameter = $strCharsBetween;
					}
					else {
						//Esto evitará que se consideren los caracteres siguientes a la variable como parte de la misma variable
						$intPosClosure = null;
					}
				}
			}
			else 
			{
				//Esto evitará que se consideren los paréntesis siguientes a la variable como parte de la misma variable
				$intPosClosure = null;
			}
			
			//@JAPR 2012-10-30: Agregado el parámetro $bReplaceByNumber para controlar si se debe reemplazar la respuesta basado en el número de pregunta
			//o en el ID dentro del texto especificado
			if (!$bReplaceByNumber) {
				//Obtiene el número de pregunta basado en el ID (si ya no existe, se regresará 0 así que termina reemplazando por vacio)
				$strSectionID = (int) @$arrSectionsByID[$strSectionID];
			}
			
			//Obtiene la referencia a la sección a la que se hace referencia con la variable. El valor default de la variable es vacio
			$anAnswer = '';
			$objSection = @$arrSectionObjsByNum[$strSectionID];
			if (!is_null($objSection)) {
				switch (strtolower($strParameter)) {
					case 'name':
						$anAnswer = $objSection->SurveyName;
						break;
					case 'number':
						$anAnswer = $objSection->SectionNumber;
						break;
					case 'pagename':
						$anAnswer = (string) @$aSectionEvalPage[$objSection->SectionID];
						break;
					case 'pagenumber':
						$anAnswer = (int) @$aSectionEvalRecord[$objSection->SectionID];
						break;
					default:
						//Obtiene los valores de los atributos desde la colección recibida durante el grabado
						$arrSectionDesc = @$arrSectionDescByID[$objSection->SectionID][(int) @$aSectionEvalRecord[$objSection->SectionID]];
						if (is_null($arrSectionDesc) || !is_array($arrSectionDesc)) {
							$arrSectionDesc = array();
						}
						$intNumAttrs = count($arrSectionDesc);
						
						$strAttributePos = (int) $strAttributePos;
						if ($strAttributePos > 0 && $strAttributePos <= $intNumAttrs)
						{
							$anAnswer = (string) @$arrSectionDesc[$strAttributePos -1];
						}
						else 
						{
							//Usará el último elemento (también entra aquí si no se pide un atributo sino simplemente se quiere el valor normal)
							$arrAnswer = $arrSectionDesc;
							$anAnswer = (string) @$arrSectionDesc[$intNumAttrs -1];
							
							//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
							//La pregunta tendría que ser de catálogo para haber llegado aquí además de haber pedido un atributo que no venía como
							//parte de la propia pregunta, por tanto lo que tiene que hacer es cargar el valor del catálogo siguiendo la combinación
							//que si se tiene contestada en esta pregunta, tomando el primer valor devuelto para el atributo especificado. Esto sólo
							//se puede si se mandó el repositorio y se está forzando a pedir todos los atributos
							if (isset($arrSectionObjsByNum[$strSectionID])) {
								$aSection = $arrSectionObjsByNum[$strSectionID];
								if ($bForceGetAttribs && $aRepository) {
									$intCatalogID = $aSection->CatalogID;
									if ($intCatalogID > 0) {
										$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $intCatalogID);
										$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($aRepository, $intCatalogID);
										if (!is_null($objCatalog) && !is_null($objCatalogMembersColl)) {
											//No hay garantías sobre esta funcionalidad, ya que depende de que al momento de hacer el grabado, la combinación
											//referenciada aún existiera en el catálogo, si no es así simplemente no se podrán obtener atributos adicionales. Esto es
											//porque la captura no manda todos los datos del catálogo sino sólo lo que utiliza
											$strFields = "";
											$strGroupBy = "";
											$strFilter = '';
											$strAnd = '';
											$intCatMemberID = 0;
											foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
												$memberID = $objCatalogMember->MemberID;
												if ($objCatalogMember->MemberOrder == $strAttributePos) {
													$intCatMemberID = $memberID;
												}
												
												if (isset($arrAnswer[$objCatalogMember->MemberOrder -1])) {
													$memberValue = $aRepository->DataADOConnection->Quote($arrAnswer[$objCatalogMember->MemberOrder -1]);
													$strFilter .= $strAnd.("@CMID".$memberID."=".$memberValue);
													$strAnd = ' AND ';
												}
											}
											
											//Se obtiene el valor del atributo especificado mediante la función getAttributeValues, la cual ya soporta tanto
											//catálogos de eForms como de eBavel. La función está preparada para regresar los datos en el formato que requiere
											//el app (un array indexado en forma numérica con estructura de árbol recorriendo todos sus atributos), pero como
											//sólo se está pidiendo un atributo y como se asume que el filtro regresará una única combinación, se pide el
											//primer valor regresado directamente
											$strFilter = BITAMCatalog::TranslateAttributesVarsToFieldNames($aRepository, $intCatalogID, $strFilter);
											$arrAttributes = BITAMCatalog::GetCatalogAttributes($aRepository, $intCatalogID, 0, array($intCatMemberID));
											$objCatalogValues = $objCatalog->getAttributeValues(array_values($arrAttributes), -1, $strFilter, null, null, false, true);
											if (!is_null($objCatalogValues)) {
												//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
												$anAnswer = (string) @$objCatalogValues[0]['value'];
												
												if (getParamValue('DebugBreak', 'both', '(int)', true)) {
													echo("<br>\r\n replaceSectionVariables for Catalog: {$intCatalogID}, Attribute: {$strAttributePos}, Value: {$anAnswer}, Query: {$sql}");
												}
											}
										}
									}
								}
							}
							//@JAPR
						}
						break;
				}
			}
			
			$strAnswer = (string) $anAnswer;
			
			if (!is_numeric($strAnswer) && $bQuoteText)
			{
				$strAnswer = "'".$strAnswer."'";
			}
			
			$strResult .= $strAnswer;
		}
		
		//Si se definió un atributo específico a usar, se remueve también la parte de los paréntesis moviendo
		//el offset justo despues de los mismos, de lo contrario sólo remueve la definición de la variable
		if (!is_null($intPosClosure) && $intPosClosure > $intPos + $i)
		{
			//Se posiciona en el caracter siguiente al paréntesis de definición del número de atributo
			$intOffSet = $intPosClosure +1;
		}
		else 
		{
			//Se posiciona justo después de la definición de la variable
			$intOffSet = $intPos + $i;
		}
		$intPos = stripos($sText, '@S', $intOffSet);
	}
	
	//@JAPR 2014-01-27: Corregido un bug, después de reemplazar las variables no estaba copiando el resto del texto sin variables
	if ($intOffSet > 0) {
		$strResult .= substr($strText, $intOffSet);
	}
	//@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceSectionVariables result: {$strResult}");
	}
	return $strResult;
}

//@JAPR 2014-10-30: Agregado el mapeo de atributos de catálogos
/* Obtiene el valor del atributo especificado según el contexto en el que se está evaluando el registro durante el proceso de grabado, esto es,
identifica la pregunta mas próxima del mismo catálogo del que se trate y que pertenezca a la sección que se está evaluando, para pedir el valor del
atributo en cuestión. Internamente lo que hará es invocar al reemplazo de la variable de la pregunta identificada para que sea replaceQuestionVariables
quien se encargue de obtener el valor correcto, sólo que a la variable de dicha pregunta, la cual tendría que ser simple choice de catálogo, le
especificará ya el parámetro del número de atributo que se necesita recuperar
Por el momento no se podría obtener el valor directamente de las secciones, ya que no se cuenta con reemplazo de variables de sección
*/
function getEntryAttributeValueForAction($aRepository, $surveyID, $aMemberID, $arrFieldValues, $anEntrySectionID = 0, $anEntryCatDimID = 0, $aDynamicPageDSC = '', $aMultiRecordNum = 0) {
	$strAttribValue = '';
	
	$surveyID = (int) @$surveyID;
	$aMemberID = (int) @$aMemberID;
	if (!$aRepository || $surveyID <= 0 || $aMemberID <= 0) {
		return $strAttribValue;
	}
	
	if (is_null($arrFieldValues) || !is_array($arrFieldValues)) {
		$arrFieldValues = array();
	}
	
	//Obtiene la referencia a la sección en cuestión si es que es un registro de alguna sección multi-registro, ya que si no entonces utilizará
	//la última pregunta a nivel de toda la encuesta
	$objSection = null;
	if ($anEntrySectionID > 0) {
		$objSection = BITAMSection::NewInstanceWithID($aRepository, $anEntrySectionID);
	}
	
	//Obtiene la referencia del atributo especificado
	$objCatalogMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $aMemberID);
	if (is_null($objCatalogMember)) {
		return $strAttribValue;
	}
	
	$intMemberOrder = $objCatalogMember->MemberOrder;
	$intCatalogID = $objCatalogMember->CatalogID;
	
	//Asigna la colección de preguntas a utilizar, la cual consiste en la de la sección o bien de toda la encuesta si no es un contexto de sección
	$questionCollection = null;
	//@JAPR 2014-12-10: Corregido un bug, esta validación estuvo mal implementada, asumía que las secciones multi registro contendrían siempre
	//una pregunta de su mismo catálogo lo cual no es cierto, ahora validará contra toda la encuesta si en la propia sección no la encuentra
	$surveyQuestionCollection = null;
	//@JAPR
	if ($objSection) {
		//@JAPR 2014-12-10: Corregido un bug, si la sección es dinámica o Inline, ya hay forma de obtener los atributos aplicables a cada página
		//a través de las propias variables de sección, ya que obtenerlas desde una pregunta si es que no forma parte de la sección, repetiría
		//el mismo valor para todos los registros y sería el contexto equivocado
		if (($objSection->SectionType == sectDynamic || $objSection->SectionType == sectInline) && $objSection->CatalogID == $intCatalogID) {
			//En este caso se forzará a utilizar la variable de sección generando colecciones de preguntas vacias para que no entre a las
			//validaciones que se encuentran mas adelante, así al no encontrar una pregunta de catálogo y ser una sección que usa el mismo
			//catálogo pedido, se utilizará directamente la variable de sección
			$questionCollection = BITAMQuestionCollection::NewInstanceEmpty($aRepository, $surveyID);
			$surveyQuestionCollection = $questionCollection;
		}
		else {
			//En este caso o no es una sección aplicable, o bien es de un catálogo diferente al pedido, así que se graba en base a las preguntas
			$questionCollection = BITAMQuestionCollection::NewInstanceBySection($aRepository, $anEntrySectionID);
			$surveyQuestionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
		}
	}
	else {
		$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
		//@JAPR 2014-12-10: Corregido un bug, esta validación estuvo mal implementada, asumía que las secciones multi registro contendrían siempre
		//una pregunta de su mismo catálogo lo cual no es cierto, ahora validará contra toda la encuesta si en la propia sección no la encuentra
		$surveyQuestionCollection = $questionCollection;
		//@JAPR
	}
	
	if (is_null($questionCollection)) {
		return $strAttribValue;
	}
	
	//Debe identificar si hay o no alguna pregunta de este catálogo, si no es así entonces por ahora no puede resolver este tipo de variables pues
	//no hay aún soporte para variables de sección a nivel del server. Siempre utilizará la última pregunta de la colección, ya que esa es la que
	//tendría el mayor detalle. A la fecha de esta implementación no se podían tener varias ramas del mismo catálogo así que es seguro aplicar esta
	//búsqueda
	$objCatalogQuestion = null;
	foreach ($questionCollection->Collection as $aQuestion) {
		if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID == $intCatalogID) {
			$objCatalogQuestion = $aQuestion;
		}
	}
	
	//@JAPR 2014-12-10: Corregido un bug, esta validación estuvo mal implementada, asumía que las secciones multi registro contendrían siempre
	//una pregunta de su mismo catálogo lo cual no es cierto, ahora validará contra toda la encuesta si en la propia sección no la encuentra
	if (is_null($objCatalogQuestion) && $questionCollection !== $surveyQuestionCollection) {
		foreach ($surveyQuestionCollection->Collection as $aQuestion) {
			if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID == $intCatalogID) {
				$objCatalogQuestion = $aQuestion;
			}
		}
	}
	//@JAPR
	
	if (is_null($objCatalogQuestion)) {
		//@JAPR 2014-12-10: Corregido un bug, si no se encuentra una pregunta de catálogo para poder obtener el valor de un atributo, pero la
		//sección es dinámica o Inline, entonces se pueden usar las variables de sección recién agregadas para resolver este caso
		if ($objSection && ($objSection->SectionType == sectDynamic || $objSection->SectionType == sectInline) && $objSection->CatalogID == $intCatalogID) {
			//Obtiene el valor de la variable identificando el atributo deseado
			//A la fecha de implementación las variables de sección se asumían que siempre se enviaban numéricas, ya que no existía la validación
			//para capturarlas numéricas y grabarlas con ID, así que se quedó fijo dentro de replaceQuestionVariables que la invocación a
			//replaceSectionVariables siempre fuera por número de sección, pero se puede invocar directo al reemplazo de variables de sección
			//así que se mandará false en dicho parámetro en ese caso
			$strAttribVariable = "@S{$objSection->SectionID}({$intMemberOrder})";
			//@JAPR 2015-03-23: Corregido un bug, al agregar variables de sección de catálogo donde éste era de eBavel, si se intentaban resolver cuando
			//se encontraba en un proceso relacionado con eBavel (como generación de acciones) y el atributo solicitado no venía como parte del archivo
			//.dat, se tenía que cargar información de la metadata y el FetchMode venía numérico debido al proceso de eBavel donde se encontraba, por lo 
			//que no lograba cargar la metadata y el proceso de extracción de datos de eBavel mataba al proceso de PhP
			//Se respaldará el FetchMode en esta función para cambiarlo a Assoc temporalmente, al final se restaurará el FetchMode original para no
			//afectar el proceso relacionado con eBavel
			//El resto de las funciones de reemplazo se invocarán desde esta, así que en ellas no hay necesidad de este proceso extra
			//Se cambió la función replaceSectionVariables por replaceQuestionVariables para que se ejecute el proceso mencionado arriba de
			//forma centralizada
			//$strAttribValue = (string) @replaceSectionVariables($strAttribVariable, $arrFieldValues, false, $aRepository, $surveyID, false, true);
			$strAttribValue = (string) @replaceQuestionVariables($strAttribVariable, $arrFieldValues, false, $aRepository, $surveyID, false, true);
			//@JAPR
		}
		//@JAPR
		return $strAttribValue;
	}
	
	//Obtiene el valor de la variable identificando el atributo deseado
	$strAttribVariable = "@Q{$objCatalogQuestion->QuestionID}({$intMemberOrder})";
	$strAttribValue = (string) @replaceQuestionVariables($strAttribVariable, $arrFieldValues, false, $aRepository, $surveyID, false, true);
	return $strAttribValue;
}
//@JAPR
?>