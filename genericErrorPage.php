<?php
if(isset($_GET["StrError"]))
{
	$strGenericError = $_GET["StrError"];
}
else 
{
	//@JAPR 2016-11-14: Modificado el mensaje de error ya que el único punto donde no se mandaba el parámetro StrError era al no tener permisos de eForms, así que ahora mostrará
	//un mensaje mas adecuado para dicho caso
	//$strGenericError = "There is an error on page you are trying to access.";
	//@JAPR 2016-12-05: Corregido un bug, en este punto no hay una sesión iniciada ni un repositorio conectado, así que se debe basar en el idioma heredado del login de KPIOnline
	//para traducir el menesaje de error
	@session_start();
	require_once("initialize.php");
	
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
	//@JAPR
	
	$strGenericError = translate("Access denied").". ".translate("Please contact your subscription administrator to obtain the appropriate permissions");
	//@JAPR
}
?>
<html>
<head>
<title>ESurvey Error Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>

<script language="JavaScript" type="text/JavaScript">
function RedirectToPage()
{

}
</script>
</head>
<body bgcolor="#CADEE3" text="#000000" link="#006666" vlink="#009966" leftmargin="0" topmargin="0">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td bgcolor="#336699"><p><font color="#336699" size="1">.</font></p>
      <p><font color="#336699" size="1">.</font></p></td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"> <div align="center"> <font color="#FFFFFF" size="1" face="Verdana, Arial, Helvetica, sans-serif">.</font><br>
        <img src="images/LogoSurveys.jpg" width="239" height="83"><br>
        <font color="#FFFFFF" size="1" face="Verdana, Arial, Helvetica, sans-serif">.</font> 
      </div></td>
  </tr>
  <tr> 
    <td bgcolor="#C5E1B8"><font color="#C5E1B8" size="1" face="Verdana, Arial, Helvetica, sans-serif">.</font></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div id="Layer3" style="position:absolute; width:80%; height:188px; z-index:3; left: 166px; top: 180px;">
  <table width="80%" border="0" cellpadding="0" cellspacing="0">
  <p>&nbsp;</p>
  <p align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><?=$strGenericError?></strong></font></p>
  <p>&nbsp;</p>
  <p><font size="4" face="Verdana, Arial, Helvetica, sans-serif"><strong><font size="4">
  <!--<a href="javascript:RedirectToPage()">Back</a></font></strong></font></p>-->
  </table>
</div>
</body>
</html>
