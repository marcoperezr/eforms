<?php

global $BITAMPCPHostName;
$BITAMPCPHostName = 'alpha.axzas.com:8085';

global $BITAMAppUserClass;
$BITAMAppUserClass = null;

global $BITAMCatalogClass;
$BITAMCatalogClass = null;

global $BITAMCustomStoreSurveyCaptureFunction;
$BITAMCustomStoreSurveyCaptureFunction = null;

//@JAPR 2015-07-10: Rediseñado el Admin con DHTMLX
//Crea la instancia a partir de la clase que realmente ejecuta el método
function GetBITAMAppUserClass($aRepository, $strCalledClass = 'BITAMAppUser')
{
	global $BITAMAppUserClass;
	if (true)
	{
		$sql = "SELECT PCPUserID FROM SI_SV_Users WHERE 1 = 0";
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
			$BITAMAppUserClass = $strCalledClass; //'BITAMAppUser';
		}
		else
		{
			require_once('pcpAppUser.inc.php');
			$BITAMAppUserClass = 'BITAMPCPAppUser';
		}
	}
	return $BITAMAppUserClass;
}

//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
//Agregado el parámetro $aneBavelFormID para indicar cuando el catálogo se trata de un catálogo mapeado desde eBavel
function GetBITAMCatalogClass($aRepository, $aneBavelFormID = 0)
{
	global $BITAMCatalogClass;
	if (is_null($BITAMCatalogClass))
	{
		$sql = "SELECT PCPUserID FROM SI_SV_Users WHERE 1 = 0";
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			$BITAMCatalogClass = 'BITAMCatalog';
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
			if ($aneBavelFormID) {
				$BITAMCatalogClass = 'BITAMeBavelCatalog';
			}
			//@JAPR
		}
		else
		{
			require_once('pcpcatalog.inc.php');
			$BITAMCatalogClass = 'BITAMPCPCatalog';
		}
	}
	return $BITAMCatalogClass;
}

function GetBITAMCustomStoreSurveyCaptureFunction($aRepository)
{
	global $BITAMCustomStoreSurveyCaptureFunction;
	if (is_null($BITAMCustomStoreSurveyCaptureFunction))
	{
		$sql = "SELECT PCPUserID FROM SI_SV_Users WHERE 1 = 0";
		
		$aRS = $aRepository->ADOConnection->Execute($sql);

		if ($aRS === false)
		{
			$BITAMCustomStoreSurveyCaptureFunction = '';
		}
		else
		{
			require_once('pcpcatalog.inc.php');
			$BITAMCustomStoreSurveyCaptureFunction = 'BITAMPCPStoreSurveyCapture';
		}
	}
	return $BITAMCustomStoreSurveyCaptureFunction;
}


?>