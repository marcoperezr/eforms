<?php
/* Esta clase no se utiliza al día 2014-07-10 ya que esta funcionalidad originalmente fue solicitada sólo como algo interno que BITAM debería
configurar según el proyecto con el que se pretendiera utilizar, sin embargo eventualmente se puede permitir al cliente hacerlo por su propia
cuenta habilitando las ventanas que esta clase generaría. Está ligada a BITAMSurveyProfileRestriction
*/
require_once("repository.inc.php");
require_once("initialize.php");
require_once("surveyProfileRestriction.inc.php");

class BITAMSurveyProfile extends BITAMObject
{
	public $ProfileID;
	public $ProfileName;
	
	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		$this->ProfileID = -1;
		$this->ProfileName= "";
	}
	
	static function NewInstance($aRepository)
	{
		return new BITAMSurveyProfile($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aProfileID)
	{
		$anInstance = null;
		
		if (((int)  $aProfileID) < 0)
		{
			return $anInstance;
		}
		
		$anInstance =& BITAMGlobalFormsInstance::GetProfileInstanceWithID($aProfileID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		$sql = "SELECT ProfileID, ProfileName 
			FROM SI_SV_SurveyProfile 
			WHERE ProfileID = ".((int) $aProfileID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyProfile ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMSurveyProfile::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMSurveyProfile::NewInstance($aRepository);
		$anInstance->ProfileID = (int) @$aRS->fields["ProfileID"];
		$anInstance->ProfileName = (string) @$aRS->fields["ProfileName"];
		
		BITAMGlobalFormsInstance::AddProfileInstanceWithID($anInstance->ProfileID, $anInstance);
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("ProfileID", $aHTTPRequest->POST))
		{
			$aProfileID = $aHTTPRequest->POST["ProfileID"];
			
			if (is_array($aProfileID))
			{
				
				$aCollection = BITAMSurveyProfileCollection::NewInstance($aRepository, $aProfileID);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMSurveyProfile::NewInstanceWithID($aRepository, (int)$aProfileID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMSurveyProfile::NewInstance($aRepository);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMSurveyProfile::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("ProfileID", $aHTTPRequest->GET))
		{
			$aProfileID = $aHTTPRequest->GET["ProfileID"];
			
			$anInstance = BITAMSurveyProfile::NewInstanceWithID($aRepository,(int) $aProfileID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMSurveyProfile::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMSurveyProfile::NewInstance($aRepository);
		}
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("ProfileID", $anArray))
		{
			$this->ProfileID = (int) $anArray["ProfileID"];
		}
		
	 	if (array_key_exists("ProfileName", $anArray))
		{
			$this->ProfileName = trim($anArray["ProfileName"]);
		}
		
		return $this;
	}
	
	function save()
	{
		global $gblShowErrorSurvey;
		
	 	if ($this->isNewObject())
		{
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(ProfileID)", "0")." + 1 AS ProfileID".
				" FROM SI_SV_SurveyProfile";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyProfile ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyProfile ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$this->ProfileID = (int) $aRS->fields["ProfileID"];
			$sql = "INSERT INTO SI_SV_SurveyProfile (".
						"ProfileID".
			            ", ProfileName".
			            ") VALUES (".
			            $this->ProfileID.
						",".$this->Repository->DataADOConnection->Quote($this->ProfileName).
						")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyProfile ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyProfile ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		else
		{
			$sql = "UPDATE SI_SV_SurveyProfile SET ".
					" ProfileName = ".$this->Repository->DataADOConnection->Quote($this->ProfileName).
				" WHERE ProfileID = ".$this->ProfileID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyProfile ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyProfile ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		
		return $this;
	}

	function remove()
	{
		$sql = "DELETE FROM SI_SV_SurveyRestriction WHERE ProfileID = ".$this->ProfileID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyRestriction ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_SurveyProfile WHERE ProfileID = ".$this->ProfileID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyProfile ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->ProfileID < 0);
	}
		
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Profile");
		}
		else
		{
			return $this->ProfileName;
		}
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SurveyProfile";
		}
		else
		{
			return "BITAM_PAGE=SurveyProfile&ProfileID=".$this->ProfileID;
		}
	}

	function get_Parent()
	{
		return BITAMSurveyProfileCollection::NewInstance($this->Repository,null);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function generateBeforeFormCode($aUser)
 	{	
?>
 	<script language="JavaScript">
 		function verifyFieldsAndSave(target)
 		{
 			var strAlert = '';
 			
 			if(Trim(BITAMSurveyProfile_SaveForm.ProfileName.value)=='')
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Name"))?>';
 			}
 			
 			if(Trim(strAlert)!='')
 			{
 				alert(strAlert);
 			}
 			else
 			{
 				BITAMSurveyProfile_Ok(target);
 			}
 		}
 	</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
?>
	<script language="JavaScript">
		
		objBody= document.getElementsByTagName("body");
		objBody[0].onkeypress= function onc(event)
		{
			var event = (window.event) ? window.event : event;
			//if (window.event.keyCode ==13)
			if (event.keyCode ==13)
			return false ;
		}
		objOkSelfButton = document.getElementById("BITAMSurveyProfile_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMSurveyProfile_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMSurveyProfile_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
	</script>
<?
	}
	
	function get_FormIDFieldName()
	{
		return 'ProfileID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ProfileName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return true;
	}
	
	//Obtiene los datos del catálogo de usuarios (los valores en la dimensión creada para tal fin, no directamente de SI_USUARIO ni SI_SV_Users)
	function getUsers($includeNone = false, $bGetForService = false)
	{
		$arrUsers = array();
		if ($includeNone)
		{
			if ($bGetForService)
			{
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		        $arrUsers[] = array(
		            'userID' => -1,
		            'userName' => 'None'
		        );
			}
			else 
			{
				$arrUsers[-1] = translate('None');
			}
		}
		
		$theUserName = $_SESSION["PABITAM_UserName"];		
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$theUser = BITAMeFormsUser::WithUserName($aRepository, $theUserName);
		$theUserID = -1;
		if (!is_null($theUser))
		{
			if ($bGetForService)
			{
				$theUserID = $theUser->UserID;
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		        $arrUsers[] = array(
		            'userID' => $theUser->UserID,
		            'userName' => $theUser->FullName
		        );
			}
			else 
			{
				$arrUsers[$theUser->UserID] = $theUser->FullName;
			}
		}
		
        $sql = "SELECT DISTINCT CLA_USUARIO AS UserId, UserName 
        	FROM SI_SV_Users 
        	WHERE ProfileID = {$this->ProfileID} 
        	ORDER BY UserName";
        $recordset = $aRepository->ADOConnection->Execute($sql);
        if ($recordset)
        {
	        $arrProfileIDs = array();
	        while (!$recordset->EOF) {
				$userID = (int) @$recordset->fields["userid"];
				$userName = (string) @$recordset->fields["username"];
				if ($bGetForService)
				{
					if ($userID != $theUserID)
					{
						//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
				        $arrUsers[] = array(
				            'userID' => $userID,
				            'userName' => $userName
				        );
					}
				}
				else 
				{
					$arrUsers[$userID] = $userName;
				}
	            $recordset->MoveNext();
	        }
        }
		
        return $arrUsers;
	}
	
	/* Basado en la definición de este perfil, genera la porción WHERE del estatuto SQL correspondiente al mismo
	Estas claúsulas se basan en al parámetro $aTableAliasColl para verificar que alias deben utilizar para cada una de las tablas que se 
	asumirán existentes en el SELECT donde se aplicará (ya que las reglas son pensadas para los procesos que determinan la visibilidad de las
	encuestas, así que ya se tienen una serie de queries para tal fin con joins entre varias tablas), si no se especifica alguna tabla en
	este array, se asumirá que se debe utilizar el nombre de la misma como alias
	*/
	function getWhere($aTableAliasColl) {
		$objSurveyRestrictionColl = BITAMSurveyProfileRestrictionCollection::NewInstance($this->Repository, $this->ProfileID);
		if (is_null($objSurveyRestrictionColl)) {
			return '';
		}
		
		$strFullWhere = '';
		$strOr = '';
		foreach ($objSurveyRestrictionColl->Collection as $objSurveyRestriction) {
			$strWhere = trim($objSurveyRestriction->getWhere($aTableAliasColl));
			if ($strWhere) {
				$strFullWhere .= $strOr."({$strWhere})";
				$strOr = ' OR ';
			}
		}
		
		return ($strFullWhere)?"({$strFullWhere})":'';
	}
	
	/* Dada un perfil, regresa la colección de encuestas que si cumplen con las reglas por lo que se deben considerar como inválidas al
	momento de regresar las definiciones de encuestas a los dispositivos
	*/
	static function GetInvalidSurveys($aRepository, $aProfileID) {
		global $gblShowErrorSurvey;
		
		$arrSurveys = array();
		$aProfileID = (int) @$aProfileID;
		if ($aProfileID <= 0) {
			return $arrSurveys;
		}
		
		$objSurveyProfile = BITAMSurveyProfile::NewInstanceWithID($aRepository, $aProfileID);
		if (is_null($objSurveyProfile)) {
			return $arrSurveys;
		}
		
		$arrAlias = array();
		$arrAlias['SI_SV_Survey'] = 'A';
		$arrAlias['SI_SV_Section'] = 'B';
		$arrAlias['SI_SV_Question'] = 'C';
		$strWhere = $objSurveyProfile->getWhere($arrAlias);
		//@JAPR 2014-07-16: Corregido un bug, si no hay filtros en el perfil, no debe realizar el query para que ninguna encuesta se considere
		//como inválida
		if (!$strWhere) {
			return $arrSurveys;
		}
		
		$sql = "SELECT DISTINCT A.SurveyID, A.SurveyName 
			FROM SI_SV_Survey A 
				INNER JOIN SI_SV_Section B ON (A.SurveyID = B.SurveyID) 
				LEFT JOIN SI_SV_Question C ON (C.SectionID = B.SectionID)
			WHERE ".$strWhere;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n Query encuestas inválidas {$sql}");
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey, SI_SV_Section, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey, SI_SV_Section, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			$intSurveyID = (int) @$aRS->fields["surveyid"];
			$strSurveyName = (string) @$aRS->fields["surveyname"];
			if ($intSurveyID > 0) {
				$arrSurveys[$intSurveyID] = $strSurveyName;
			}
			
			$aRS->MoveNext();
		}
		
		return $arrSurveys;
	}
}

class BITAMSurveyProfileCollection extends BITAMCollection
{
  function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository, $anArrayOfProfileIDs = null)
	{
		global $gblShowErrorSurvey;
		
		$anInstance = new BITAMSurveyProfileCollection($aRepository);
		
		$where = "";
		if (!is_null($anArrayOfProfileIDs))
		{
			switch (count($anArrayOfProfileIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE ProfileID = ".((int) $anArrayOfProfileIDs[0]);
					break;
				default:
					foreach ($anArrayOfProfileIDs as $aProfileID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $aProfileID; 
					}
					if ($where != "")
					{
						$where = "WHERE ProfileID IN (".$where.")";
					}
					break;
			}
		}
		
		$sql = "SELECT ProfileID, ProfileName 
			FROM SI_SV_SurveyProfile ".$where." 
			ORDER BY ProfileName";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyProfile ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyProfile ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMSurveyProfile::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
	  return BITAMSurveyProfileCollection::NewInstance($aRepository, null);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Profiles");
	}
	
	function get_QueryString()
	{
		return "BITAM_SECTION=SurveyProfileCollection";
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_SECTION=SurveyProfile";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=SurveyProfile";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'ProfileID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ProfileName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return true;	
	}
}
?>