<?php

require_once("appUser.inc.php");

class BITAMPCPAppUser extends BITAMAppUser
{
	public $PCPUserID;
	public $PCPPassword;

	function __construct($aRepository)
	{
		BITAMAppUser::__construct($aRepository);
		$this->PCPUserID = -1;
		$this->PCPPassword = "";
	}

	static function NewInstance($aRepository)
	{
		return new BITAMPCPAppUser($aRepository);
	}

	static function NewInstanceWithID($aRepository, $anUserID)
	{
		$anInstance = null;
		
		if (((int)  $anUserID) < 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.CLA_USUARIO, A.NOM_CORTO AS ArtusUser, A.NOM_LARGO AS UserName, A.CUENTA_CORREO AS Email, B.SV_Admin, B.SV_User, B.UserID, B.SendPDFByEmail,B.Supervisor, B.PCPUserID, B.PCPPassword  
				FROM SI_USUARIO A LEFT JOIN SI_SV_Users B ON A.CLA_USUARIO = B.CLA_USUARIO WHERE B.UserID = ".((int)$anUserID);

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMPCPAppUser::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceWithClaUsuario($aRepository, $cla_usuario)
	{
		$anInstance = null;
		
		if (((int)  $cla_usuario) < 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.CLA_USUARIO, A.NOM_CORTO AS ArtusUser, A.NOM_LARGO AS UserName, A.CUENTA_CORREO AS Email, B.SV_Admin, B.SV_User, B.UserID, B.SendPDFByEmail, B.Supervisor, B.PCPUserID, B.PCPPassword 
		FROM SI_USUARIO A LEFT JOIN SI_SV_Users B ON A.CLA_USUARIO = B.CLA_USUARIO WHERE A.CLA_USUARIO = ".((int) $cla_usuario);

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$anInstance = BITAMPCPAppUser::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMPCPAppUser::NewInstance($aRepository);

		$anInstance->UserID = (int)$aRS->fields["userid"];
		$anInstance->UserName = $aRS->fields["username"];
		$anInstance->Email = $aRS->fields["email"];
		
		$anInstance->SV_Admin = (int)$aRS->fields["sv_admin"];
		$anInstance->SV_User = (int)$aRS->fields["sv_user"];
		
		$anInstance->ArtusUser = $aRS->fields["artususer"];
		$anInstance->CLA_USUARIO = (int)$aRS->fields["cla_usuario"];
		
	 	if(array_key_exists("sendpdfbyemail",$aRS->fields))
	 	{
	 		$anInstance->SendPDFByEmail = (int)$aRS->fields["sendpdfbyemail"];
	 	}
	 	
	 	if(array_key_exists("supervisor",$aRS->fields))
	 	{
	 		$anInstance->Supervisor = (int)$aRS->fields["supervisor"];
	 	}

		if(array_key_exists("PCPUserID",$aRS->fields))
	 	{
	 		$anInstance->PCPUserID = (int)$aRS->fields["pcpuserid"];
	 	}

		if(array_key_exists("PCPPassword",$aRS->fields))
	 	{
	 		$anInstance->PCPPassword = BITAMDecryptPassword($aRS->fields["pcppassword"]);
	 	}

		$anInstance->getRole();
		$anInstance->getArtusUserInfo();
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("CLA_USUARIO", $aHTTPRequest->POST))
		{
			$anUserID = $aHTTPRequest->POST["CLA_USUARIO"];
			
			if (is_array($anUserID))
			{
				$aCollection = BITAMPCPAppUserCollection::NewInstanceByClaUsuario($aRepository, $anUserID);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMPCPAppUser::NewInstanceWithClaUsuario($aRepository, $anUserID);

				if (is_null($anInstance))
				{
					$anInstance = BITAMPCPAppUser::NewInstance($aRepository);
				}

				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();

				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMPCPAppUser::NewInstance($aRepository);
					}
				}

				$aHTTPRequest->RedirectTo = $anInstance;
			}

			return null;
		}

		$anInstance = null;
		if (array_key_exists("UserID", $aHTTPRequest->GET))
		{
			$anUserID = $aHTTPRequest->GET["UserID"];
			$anInstance = BITAMPCPAppUser::NewInstanceWithClaUsuario($aRepository, $anUserID);

			if (is_null($anInstance))
			{
				$anInstance = BITAMPCPAppUser::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMPCPAppUser::NewInstance($aRepository);
		}

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		parent::updateFromArray($anArray);
		if (array_key_exists("PCPUserID", $anArray))
		{
			$this->PCPUserID = (int)$anArray["PCPUserID"];
		}

	 	if (array_key_exists("PCPPassword", $anArray))
		{
			$this->PCPPassword = stripslashes($anArray["PCPPassword"]);
		}
		return $this;
	}

	function save()
	{
	 	if ($this->isNewObject())
		{
			if($_SESSION["PAFBM_Mode"]!=1)
			{
				$sql =  "SELECT ".
							$this->Repository->ADOConnection->IfNull("MAX(UserID)", "0")." + 1 AS UserID".
							" FROM SI_SV_Users";
	
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				
				if (!$aRS || $aRS->EOF)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$this->UserID = (int) $aRS->fields["userid"];
				
				//Si nos encontramos en modo FBM en teoría no se debería permitir agregar usuarios 
				$initialKey = GetBitamInitialKey($this->Repository);
				
				$sql =  "SELECT ".$this->Repository->ADOConnection->IfNull("MAX(CLA_USUARIO)", "".$initialKey."")." + 1 AS UserID".
							" FROM SI_USUARIO";
	
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
	
				$temporalID = (int)$aRS->fields["userid"];
	
				if($temporalID<($initialKey+1))
				{
					$temporalID = $initialKey+1;
				}
	
				$this->CLA_USUARIO = $temporalID;
	
				// Obtenemos el id del usuario logeado, primero lo tomamos de la sesion
				$this->OwnerID = $_SESSION["PABITAM_UserID"];
	
				$sql = "INSERT INTO SI_USUARIO (".
				            "CLA_USUARIO".
				            ",NOM_CORTO".
				            ",PASSWORD".
				            ",NOM_LARGO".
				            ",PUESTO".
				            ",CUENTA_CORREO".
				            ",WHTMLANG".
				            ",EKTOS_ADMIN".
				            ",EKTOS_USER".
				            ",TIPO_USUARIO".
				            ",ARTUS_DESKTOP".
				            ",ARTUS_ADMIN".
				            ",ARTUS_DESIGNER".
				            ",PAPIRO_ADMIN".
							",PAPIRO_DESIGNER".
							",PAPIRO_DESKTOP".
							",ADVISOR_ADMIN".
							",ADVISOR_DESKTOP".
							",STRATEGO_ADMIN".
							",STRATEGO_DESKTOP".
				            ",CLA_OWNER".
							") VALUES (".
							$this->CLA_USUARIO.
							",".$this->Repository->ADOConnection->Quote(strtoupper($this->ArtusUser)).
							",".$this->Repository->ADOConnection->Quote(BITAMEncryptPassword($this->Password)).
							",".$this->Repository->ADOConnection->Quote($this->UserName).
							",".$this->Repository->ADOConnection->Quote($this->Position).
							",".$this->Repository->ADOConnection->Quote($this->Email).
							",".$_SESSION["PAuserLanguageID"].
							","." 0 ".
							","." 0 ".
							","." 2 ".
							","." 1 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							",".$this->OwnerID.
							")";
	
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$sql = "INSERT INTO SI_SV_Users (".
							"UserID".
							", UserName".
				            ", Email".
				            ", SV_Admin".
				            ", SV_User".
				            ", ArtusUser".
				            ", CLA_USUARIO".
				            ", SendPDFByEmail".
				            ", Supervisor".
							", PCPUserID".
							", PCPPassword".
				            ") VALUES (".
							$this->UserID.
							",".$this->Repository->ADOConnection->Quote($this->UserName).
							",".$this->Repository->ADOConnection->Quote($this->Email).
							",".$this->SV_Admin.
							",".$this->SV_User.
							",".$this->Repository->ADOConnection->Quote($this->ArtusUser).
							",".$this->CLA_USUARIO.
							",".$this->SendPDFByEmail.
							",".$this->Supervisor.
							",".((int) $this->PCPUserID).
							",".$this->Repository->ADOConnection->Quote(BITAMEncryptPassword($this->PCPPassword)).
							")";
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		else
		{
			$sqlExist = "SELECT CLA_USUARIO FROM SI_SV_Users WHERE CLA_USUARIO = ".$this->CLA_USUARIO;
			
			$aRSExist = $this->Repository->ADOConnection->Execute($sqlExist);
			
			if ($aRSExist === false)
			{
				die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlExist);
			}
			
			if ($aRSExist->EOF)
			{
				$sql =  "SELECT ".
							$this->Repository->ADOConnection->IfNull("MAX(UserID)", "0")." + 1 AS UserID".
							" FROM SI_SV_Users";
	
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				
				if (!$aRS || $aRS->EOF)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$this->UserID = (int) $aRS->fields["userid"];

				$sql = "INSERT INTO SI_SV_Users (".    //revisar si aqui tambien va supervisor (conchita)
							"UserID".
							", UserName".
				            ", Email".
				            ", SV_Admin".
				            ", SV_User".
				            ", ArtusUser".
				            ", CLA_USUARIO".
				            ", Supervisor".
							", PCPUserID".
							", PCPPassword".
				            ") VALUES (".
							$this->UserID.
							",".$this->Repository->ADOConnection->Quote($this->UserName).
							",".$this->Repository->ADOConnection->Quote($this->Email).
							",".$this->SV_Admin.
							",".$this->SV_User.
							",".$this->Repository->ADOConnection->Quote($this->ArtusUser).
							",".$this->CLA_USUARIO.
							",".$this->Supervisor.
							",".((int) $this->PCPUserID).
							",".$this->Repository->ADOConnection->Quote(BITAMEncryptPassword($this->PCPPassword)).
							")";

				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			else 
			{
				$sql = 	"UPDATE SI_SV_Users SET ".
						" UserName = ".$this->Repository->ADOConnection->Quote($this->UserName).
						", Email = ".$this->Repository->ADOConnection->Quote($this->Email).
						", SV_Admin = ".$this->SV_Admin.
						", SV_User = ".$this->SV_User.
						", SendPDFByEmail = ".$this->SendPDFByEmail.
						", Supervisor = ".$this->Supervisor.
						", PCPUserID = ".((int) $this->PCPUserID).
						", PCPPassword = ".$this->Repository->ADOConnection->Quote(BITAMEncryptPassword($this->PCPPassword));
						
				if ($this->intConfigSecurity == 0)
				{
					$sql .= ", ArtusUser = ".$this->Repository->ADOConnection->Quote($this->ArtusUser);
				}
	
				$sql .= " WHERE CLA_USUARIO = ".$this->CLA_USUARIO;
	
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}

			$sql = "";
			//@JAPR 2008-07-17: Con usuarios de LDAP no se debe actualizar el password ni el nombre corto
			//ConfigSecurity = 0 (Usuario BITAM), ConfigSecurity = 1 (Usuario LDAP)
			if ($this->intConfigSecurity == 0 && $_SESSION["PAFBM_Mode"]!=1)
			{
				$sql .= " NOM_CORTO = ".$this->Repository->ADOConnection->Quote($this->ArtusUser);
				
				if($this->ChangePassword)
				{	
					$sql .= ", PASSWORD = ".$this->Repository->ADOConnection->Quote(BITAMEncryptPassword($this->Password));
				}
				
				$sql .= ", ";
			}
			
			if($_SESSION["PAFBM_Mode"]!=1)
			{
				$sql .= " NOM_LARGO = ".$this->Repository->ADOConnection->Quote($this->UserName);
				$sql .= ",CUENTA_CORREO = ".$this->Repository->ADOConnection->Quote($this->Email);
				$sql .= ", PUESTO = ".$this->Repository->ADOConnection->Quote($this->Position);
			}
			
			if(trim($sql)!="")
			{
				$sqlUpd = "UPDATE SI_USUARIO SET ".$sql." WHERE CLA_USUARIO = ".$this->CLA_USUARIO;

				if ($this->Repository->ADOConnection->Execute($sqlUpd) === false)
				{
					die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
				}
			}
		}
		
		return $this;
	}	

	function get_FormFields($aUser)
	{
		$myFields = parent::get_FormFields($aUser);

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "PCPUserID";
		$aField->Title = translate("PCP User ID");
		$aField->Type = "Integer";
		$aField->FormatMask = "0";
		$aField->Size = 10;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "PCPPassword";
		$aField->Title = translate("PCP Password");
		$aField->Type = "Password";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		return $myFields;
	}
	
}

class BITAMPCPAppUserCollection extends BITAMAppUserCollection
{
	function __construct($aRepository)
	{
		BITAMAppUserCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository, $anArrayOfUserIDs = null)
	{
		$anInstance = new BITAMPCPAppUserCollection($aRepository);
		$where = "";

		if (!is_null($anArrayOfUserIDs))
		{
			switch (count($anArrayOfUserIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE B.UserID = ".((int) $anArrayOfUserIDs[0]);
					break;
				default:
					foreach ($anArrayOfUserIDs as $anUserID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $anUserID; 
					}
					if ($where != "")
					{
						$where = "WHERE B.UserID IN (".$where.")";
					}
					break;
			}
		}
		
		if($where=="")
		{
			$where.=" WHERE ";
		}
		else
		{
			$where.=" AND ";
		}
		
		$where.=" A.NOM_LARGO NOT LIKE (".$aRepository->ADOConnection->Quote("dummy").") ";
		
		if($_SESSION["PAFBM_Mode"])
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$where.= " AND A.CLA_USUARIO > 0";
			//@JAPR
		}
		
		$sql = "SELECT A.CLA_USUARIO, A.NOM_CORTO AS ArtusUser, A.NOM_LARGO AS UserName, A.CUENTA_CORREO AS Email, B.SV_Admin, B.SV_User, B.UserID, B.SendPDFByEmail, B.Supervisor, B.PCPUserID, B.PCPPassword 
				FROM SI_USUARIO A LEFT JOIN SI_SV_Users B ON A.CLA_USUARIO = B.CLA_USUARIO ".$where." ORDER BY A.NOM_LARGO";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMPCPAppUser::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function NewInstanceByClaUsuario($aRepository, $anArrayOfUserIDs = null)
	{
		$anInstance = new BITAMPCPAppUserCollection($aRepository);
		$where = "";

		if (!is_null($anArrayOfUserIDs))
		{
			switch (count($anArrayOfUserIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE A.CLA_USUARIO = ".((int) $anArrayOfUserIDs[0]);
					break;
				default:
					foreach ($anArrayOfUserIDs as $anUserID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $anUserID; 
					}
					if ($where != "")
					{
						$where = "WHERE A.CLA_USUARIO IN (".$where.")";
					}
					break;
			}
		}
		
		if($where=="")
		{
			$where.=" WHERE ";
		}
		else
		{
			$where.=" AND ";
		}
		
		$where.=" A.NOM_LARGO NOT LIKE (".$aRepository->ADOConnection->Quote("dummy").") ";
		
		if($_SESSION["PAFBM_Mode"])
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$where.= " AND A.CLA_USUARIO > 0";
			//@JAPR
		}
		
		$sql = "SELECT A.CLA_USUARIO, A.NOM_CORTO AS ArtusUser, A.NOM_LARGO AS UserName, A.CUENTA_CORREO AS Email, B.SV_Admin, B.SV_User, B.UserID, B.SendPDFByEmail, B.Supervisor, B.PCPUserID, B.PCPPassword 
				FROM SI_USUARIO A LEFT JOIN SI_SV_Users B ON A.CLA_USUARIO = B.CLA_USUARIO ".$where." ORDER BY A.NOM_LARGO";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMPCPAppUser::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
	  $objUserCollection = BITAMPCPAppUserCollection::NewInstanceByClaUsuario($aRepository, null);
	  if (array_key_exists("logfile", $aHTTPRequest->GET))
	  {
	    $objUserCollection->logfile = $aHTTPRequest->GET["logfile"];
	  }
	  return $objUserCollection;
		//return BITAMAppUserCollection::NewInstanceByClaUsuario($aRepository, null);
	}
	
}
?>