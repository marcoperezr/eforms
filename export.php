<?php
/* Este archivo se puede invocar directamente para iniciar una sesión de exportación según los parámetros, sin necesidad de que previamente se esté
logeado al eForms, ya que se requirió invocar a un proceso de forma externa para exportar a XLS pero bajo el esquema en que funciona eForms, los 
redireccionamientos que realiza el producto impidieron que la URL de expoertación se ejecutara correctamente
*/

//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
session_start();
$UserName = '';
$Password = '';
$theUserName = '';
$theRepositoryName = '';
$theRepository = null;
$theUser = null;
$intSurveyID = 0;
$strFormat = '';
$intDisplayAllFrames = null;
//Datos de los filtros
$strStartDate = '';
$strEndDate = '';
$intUserID = -2;
$attribSelected = null;

require_once("initialize.php");
require_once("utils.inc.php");
require_once("config.php");
require_once("survey.inc.php");
require_once("report.inc.php");
global $BITAMRepositories;

$intReportID = 0;		//Equivale a cualquier FactKey de la captura (exportación a PDF)
$intFactKeyDimVal = 0;	//Si se recibe, se utilizará este valor para obtener el primer FactKey (ReportID) aplicable
$EncryptedPassword = '';
if (array_key_exists('params', $_GET))
{
	//@JAPR 2012-07-26: Agregado el uso de la versión recibida como parámetro para controlar los campos/tablas que son consultados de la metadata
	@setAppVersion();
	//@JAPR
	
	$anArrayOfParams = explode("\r\n", BITAMDecode($_GET['params']));
	if (count($anArrayOfParams) > 2)
	{
		$UserName = strtoupper($anArrayOfParams[0]);
		$Password = $anArrayOfParams[1];
		$url_parts = parse_url($anArrayOfParams[2]);
		$query_string = array_key_exists("query", $url_parts) ? $url_parts["query"] : "";
		$query_array = array();
		parse_str($query_string, $query_array);
		$theRepositoryName = (string) @$query_array["RepositoryName"];
		$strFormat = (string) @$query_array["Format"];
		$intSurveyID = (int) @$query_array["SurveyID"];
		$intUserID = (int) @$query_array["UserID"];
		if(isset($query_array["DisplayAllFrames"]))
		{
			if(((int)$query_array["DisplayAllFrames"])==0)
			{
				$intDisplayAllFrames = 0;
			}
		}
		$strStartDate = (string) @$query_array["StartDate"];
		$strEndDate = (string) @$query_array["EndDate"];
		if (isset($query_array["Filters"])) {
			$attribSelected = $query_array["Filters"];
		}
		//@JAPR 2013-12-18: Agregada la exportación externa hacia PDF
		$intReportID = (int) @$query_array["FactKey"];
		$intFactKeyDimVal = (int) @$query_array["ReportID"];
	}
}
//@JAPR 2013-02-22: Agregado el soporte para recibir parámetros como POST que no vendrían encriptados
else {
	$UserName = getParamValue('UserName', 'both', '(string)');
	$Password = getParamValue('Password', 'both', '(string)');
	$EncryptedPassword = getParamValue('EncryptedPassword', 'both', '(string)');
	$theRepositoryName = getParamValue('Repository', 'both', '(string)');
	$strFormat = getParamValue('Format', 'both', '(string)');
	$intSurveyID = getParamValue('SurveyID', 'both', '(int)');
	$intUserID = getParamValue('UserID', 'both', '(int)');
	$intDisplayAllFrames = getParamValue('DisplayAllFrames', 'both', '(int)', true);
	$strStartDate = getParamValue('StartDate', 'both', '(string)');
	$strEndDate = getParamValue('EndDate', 'both', '(string)');
	$attribSelected = getParamValue('Filters', 'both', '(string)', true);
	//@JAPR 2013-12-18: Agregada la exportación externa hacia PDF
	$intReportID = getParamValue('FactKey', 'both', '(int)');
	$intFactKeyDimVal = getParamValue('ReportID', 'both', '(int)');
}
$UserName = trim($UserName);
$theRepositoryName = trim($theRepositoryName);
$strFormat = trim($strFormat);
if ($intUserID <= 0) {
	$intUserID = -2;
}

//@JAPR 2013-12-18: Agregado el parámetro para permitir recibir passwords encriptados
if (trim($EncryptedPassword) != '') {
	//@JAPR 2014-01-21: Corregido un bug, este debe ser el password encriptado con B64
	$Password = BITAMDecode($EncryptedPassword);
	//@JAPR
}

//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
$blnIncludePhotos = getParamValue('AddPhotos', 'both', '(int)');
//@JAPR

$blnNewExcelMethod = getParamValue('newExcelMethod', 'both', '(int)');

//Inicia la conexión con el repositorio según los datos especificados
if ($theRepositoryName == '' || !array_key_exists($theRepositoryName, $BITAMRepositories))
{
	//header("Location: index.php?error=1");
	//exit();
	die('errorDesc='.translate('The specified repository is not registered in this service'));
}
$theRepository = $BITAMRepositories[$theRepositoryName];
if (!$theRepository->open())
{
	//header("Location: index.php?error=2");
	//exit();
	die('errorDesc='.translate('Can not connect to Repository, please contact your System Administrator'));
}

if ($intSurveyID <= 0) {
	die('errorDesc='.translate('Survey not specified'));
}
$aSurvey = BITAMSurvey::NewInstanceWithID($theRepository, $intSurveyID);
if (is_null($aSurvey)) {
	die('errorDesc='.translate('Survey not found').' ('.$intSurveyID.')');
}

if ($UserName == '') {
	die('errorDesc='.translate('User account not specified'));
}

$theUserName = $UserName;
//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
$theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
if (is_null($theUser))
{
	//header("Location: index.php?error=3");
	//exit();
	die('errorDesc='.translate('You must enter a valid UserID and Password to access this resource').' ('.$theUserName.')');
}

//@JAPR 2009-02-10: Corregido el login desde enlaces externos para usuarios Bitam, falta integrar el cambio para usuarios LDAP pero desconozco como llegan los
//parámetros para esos casos
if (BITAMDecryptPassword($theUser->Password) != $Password)
{
	setcookie("PABITAM_RepositoryName", $theRepositoryName);
    setcookie("PABITAM_UserName", $theUserName);
	die('errorDesc='.translate('You must enter a valid UserID and Password to access this resource').' ('.$theUserName.')');
	//header("Location: index.php?error=3");
}
else
{	
	$_SESSION["PABITAM_RepositoryName"]=$theRepositoryName;
	//@JAPR 2013-05-08: Corregido un bug, debido al cambio para Agrega el Score al reporte (#28772), se necesitaba este valor y no estaba
	//asignandose en este punto de entrada para exportaciones independientes del login de eForms
    $_SESSION["PABITAM_UserID"] = $theUser->UserID;
	//@JAPR
	$_SESSION["PABITAM_UserName"]=$theUserName;
	$_SESSION["PABITAM_Password"]=$Password;
	$_SESSION["PAdisplayAllFrames"]=1;
	
	if(!is_null($intDisplayAllFrames))
	{
		if($intDisplayAllFrames == 0)
		{
			$_SESSION["PAdisplayAllFrames"] = 0;
		}
	}
}

if(isset($query_array["externalPage"]) && $query_array["externalPage"]==1)
{
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("SVExternalPage");
	$_SESSION["SVExternalPage"] = 1;

}

//Inicia el proceso de exportación según los parámetros especificados
switch (strtolower($strFormat)) {
	//@JAPR 2013-12-18: Agregada la exportación externa hacia PDF
	case 'pdf':
		if ($intReportID > 0 || $intFactKeyDimVal > 0) {
			//Si se recibió un ReportID, entonces realiza un query para obtener el primer FactKey aplicable
			if ($intFactKeyDimVal > 0) {
				$sql = "SELECT MIN(FactKey) AS ReportID 
					FROM {$aSurvey->SurveyTable} 
					WHERE FactKeyDimVal = {$intFactKeyDimVal}";
				$aRS = $theRepository->DataADOConnection->Execute($sql);
				if ($aRS === false) {
					die('errorDesc='.translate("Error accessing")." {$aSurvey->SurveyTable} ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg());
				}
				
				if ($aRS->EOF) {
					die('errorDesc='.translate("The specified report is empty"));
				}
				
				$intReportID = (int) @$aRS->fields["reportid"];
				if ($intReportID <= 0) {
					die('errorDesc='.translate("The specified report is empty"));
				}
			}
			
			if ($intReportID <= 0) {
				die('errorDesc='.translate("You must specify a valid Report ID for this survey"));
			}
		}
		else {
			die('errorDesc='.translate("Report ID not specified"));
		}
		
		$anInstance = @BITAMReport::NewInstanceWithID($theRepository, $intReportID, $intSurveyID, true, true);
		if (!is_null($anInstance)) {
			$anInstance->exportToPDF(false);
		}
		else {
			die('errorDesc='.translate("Error generating the report"));
		}
		break;
	//@JAPR
	
	case 'xls':
	default:
		//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
		return BITAMReportCollection::NewInstanceToExportDesg($theRepository, $intSurveyID, $attribSelected, $strStartDate, $strEndDate, $intUserID, $blnIncludePhotos, $blnNewExcelMethod);
		break;
}
?>
