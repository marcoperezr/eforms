<?php

class BITAMHTTPRequest
{
	public $GET;
	public $POST;
	public $FILES;
	public $RedirectTo;

	function __construct()
	{
		global $_GET, $_POST, $_FILES;
		
		$this->GET = $_GET;
		$this->POST = $_POST;
		$this->FILES = $_FILES;
		$this->RedirectTo = null;
	}
	
	static function NewHTTPRequest()
	{
		return new BITAMHTTPRequest();
	}
}

?>