<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$strWarning=translate("Warning: This computer program is protected by copyright law and international treaties");
$strSupportSystem=translate("Link to the support system");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KPIOnline Forms</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<style type="text/css">
<!--
body,td,th {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
body {
    margin: 10px;
}
a:link {
	color: #0077d4;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
.xx {
	font-weight: bold;
	color: #224b7f;
	text-align: center;
	font-size: 13px;
}
.adv {
	font-size: 9px;
	text-align: center;
	color: #5c5c5c;
	font-family: Verdana;
}
-->
</style></head>
<body style="background-color:#f5f5f5">
<table cellpadding="5" cellspacing="0">

<tr>
<td align="center">
<img src="images/admin/kpiformsLogo.png">
</td>
</tr>

<tr>
<td class="xx">
eForms <?=ESURVEY_SERVICE_VERSION?>
</td>
</tr>

<tr>
<td class="adv">
<?=$strWarning?>
</td>
</tr>

</table>
</body>
</html>