<?php 
require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("question.inc.php");
require_once("showquestion.inc.php");

class BITAMQuestionOption extends BITAMObject
{
	public $QuestionID;
	public $QuestionOptionID;
	public $QuestionOptionName;
	public $QuestionOptionNameBack;
	public $SortOrder;
	public $NextQuestion;
	public $IndDimID;
	public $IndicatorID;
	//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	public $QTypeID;					//Esta propiedad se asigna al cargar la instancia, al crearla vacía ya no se asignará puesto que no se usará en un contexto de esa manera, y en otros casos donde se use se toma directamente de $mainQuestion
	//@JAPR
	public $OptionActions;
	public $StrOptionActions;
	public $Score;
	//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple
	public $ActionText;					//Contiene la descripción extendida que se usará para la acción (puede contener variables en formato {@Q(#)}. El texto ya anteriormente capturado se concatena a este, a menos que se encuentre la variable {Mobile_Action_Desc} pues entonces se reemplaza en dicha posición
	public $ResponsibleID;				//Responsable de la acción a crear
	public $DaysDueDate;				//Días de deadline a partir de la fecha de fin de captura de la Encuesta/Acción
	public $CategoryID;					//Id del elemento seleccionado del catálogo de categorías (si se modifica el catálogo, los elementos seleccionados deberían resetearse)
	public $InsertWhen;					//Indica cuando se deberá insertar la Acción (0=Cuando NO se selecciona, 1=Cuando se selecciona)
	//@JAPR 2012-10-29: Agregada la información extra para las acciones
	public $ActionTitle;				//Contiene el texto que se usará como título de la acción (puede contener variables en formato {@Q(#)}
	//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
	public $ActionCondition;			//Contiene una condición a base de variables, la cual al ser evaluada en el server durante el grabado determinará si se mandan o no datos a eBavel para la acción correspondiente
	//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
	public $SurveyID;
	public $eBavelActionFormID;			//Id de la forma de eBavel sobre la que se crearán las Acciones
	public $eBavelActionFieldsData;		//Variable temporal (no se graba en la metadata) con los pares de datos "SurveyFieldMapName=eBavelFormFieldID" separados por "|" utilizados durante el grabado para mapear los campos de eBavel
	public $eBavelActionFieldsDataOld;	//Igual que la variable de arriba pero para detectar si hubo o no cambios en el mapeo
	public $QuestionOptionFieldMap;		//Objeto que contiene el mapeo de campos de eBavel a preguntas/datos de eForms (se asigna sólo al entrar a la función readeBavelActionFieldsMapping)
	//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
	public $Visible;					//Indica si la opción de respuesta se encuentra marcada para ser ocultada durante las capturas (no afecta a los reportes)
	//@JAPR
	
	public $CancelAgenda;
	
	public $DisplayImage;
	//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
	public $HTMLText;					//Texto en formato HTML para representar a la opción de respuesta
	//@JAPR 2014-08-01: Agregado el Responsive Design (Variables para configurar propiedades por dispositivo usando el Framework)
	public $HTMLTextiPod;
	public $DisplayImageiPod;
	public $HTMLTextiPad;
	public $DisplayImageiPad;
	public $HTMLTextiPadMini;
	public $DisplayImageiPadMini;
	public $HTMLTextiPhone;
	public $DisplayImageiPhone;
	public $HTMLTextCel;
	public $DisplayImageCel;
	public $HTMLTextTablet;
	public $DisplayImageTablet;
	public $ResponsiveDesignProps;	//Array indexado por el nombre de la propiedad de Responsive Design (formato NombreDeviceID) cuando dicha propiedad ya existía en la tabla, si no está asignado significa que es nueva (esto es útil durante el grabado)
	//05Feb2013: Agregar propiedad de Modo de despliegue de la pregunta
	//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	public $QDisplayMode;			//Esta propiedad se usa casi siempre desde mainQuestion, sin embargo se puede cargar directamente con la instancia para evitar crear una instancia de BITAMQuestion al crear un BITAMQuestionOption
	//@JAPR
	public $phoneNum; //agregado el numero de telefono 2013-04-29
	public $DefaultValue;
	//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
	public $CreationAdminVersion;		//Versión del código del Administrador de eForms que creó este objeto
	public $LastModAdminVersion;		//Versión del último código del Administrador de eForms que realizó el grabado de este objeto
	
	public $EditorType;

	public $HTMLTextDes;
	public $HTMLTextiPodDes;
	public $HTMLTextiPadDes;
	public $HTMLTextiPadMiniDes;
	public $HTMLTextiPhoneDes;
	public $HTMLTextCelDes;
	public $HTMLTextTabletDes;

	public $HTMLTextDesHTML;
	public $HTMLTextiPodDesHTML;
	public $HTMLTextiPadDesHTML;
	public $HTMLTextiPadMiniDesHTML;
	public $HTMLTextiPhoneDesHTML;
	public $HTMLTextCelDesHTML;
	public $HTMLTextTabletDesHTML;
	public $CopiedOptionID;
	public $ForceNew;
	public $bCopy;
	//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
	public $SourceID;				//ID del objeto original al que apunta esta instancia para mantener la sincronización entre formas de desarrollo/producción
	
	function __construct($aRepository, $aQuestionID)
	{
		BITAMObject::__construct($aRepository);
		
		$this->QuestionID = $aQuestionID;
		$this->QuestionOptionID = -1;
		$this->QuestionOptionName = "";
		$this->QuestionOptionNameBack = "";
		$this->SortOrder = 0;
		//NextQuestion representara realmente nuestro NextSection
		$this->NextQuestion = 0;
		$this->IndDimID = 0;
		$this->IndicatorID = 0;
		$this->OptionActions = array();
		$this->StrOptionActions = "";
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		$this->Visible = 1;
		$this->CancelAgenda = 0;
		
		$this->DefaultValue = "";
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		$this->CreationAdminVersion = 0;
		$this->LastModAdminVersion = 0;
		
		//Obtenemos instancia de la pregunta para determinar que tipo de pregunta es
		//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//La instancia de mainQuestion sólo se usaba en este punto para asignar las dos siguientes propiedades (QTypeID y QDisplayMode) las cuales realmente no
		//son utilizadas en eFormsV6 o posterior, se usaban para el viejo frameWork en la ventana de despliegue, así que se moverá su asignación al momento donde se
		//carga la instancia de BITAMQuestionOption o bien se usarán siempre directamente desde la instancia de mainQuestion, la cual NO se cargará en este punto
		//para evitar que se genere una llamada recursiva con los cambios para optimizar la carga de las opciones de respuesta durante la cargar de las preguntas
		/*$mainQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $this->QuestionID);
		//Esta propiedad se asigna al cargar la instancia, al crearla vacía ya no se asignará puesto que no se usará en un contexto de esa manera, y en otros casos donde se use se toma directamente de $mainQuestion
		$this->QTypeID = $mainQuestion->QTypeID;
		//05Feb2013: Agregar propiedad de Modo de despliegue de la pregunta
		//Esta propiedad se usa casi siempre desde mainQuestion, sin embargo se puede cargar directamente con la instancia para evitar crear una instancia de BITAMQuestion al crear un BITAMQuestionOption
		$this->QDisplayMode = $mainQuestion->QDisplayMode;
		//@JAPR
		*/
		$this->Score = "";
		//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple
		$this->ActionText = '';
		$this->ResponsibleID = -1;
		$this->CategoryID = 0;
		$this->DaysDueDate = 0;
		$this->InsertWhen = ioaChecked;
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		$this->ActionTitle = '';
		//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
		$this->ActionCondition = '';
		//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
		$this->eBavelActionFormID = 0;
		$this->eBavelActionFieldsData = '';
		$this->eBavelActionFieldsDataOld = '';
		$this->QuestionOptionFieldMap = null;
		//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
		$this->HTMLText = '';
		//@JAPR 2014-08-01: Agregado el Responsive Design (Variables para configurar propiedades por dispositivo usando el Framework)
		$this->HTMLTextiPod = '';
		$this->DisplayImageiPod = '';
		$this->HTMLTextiPad = '';
		$this->DisplayImageiPad = '';
		$this->HTMLTextiPadMini = '';
		$this->DisplayImageiPadMini = '';
		$this->HTMLTextiPhone = '';
		$this->DisplayImageiPhone = '';
		$this->HTMLTextCel = '';
		$this->DisplayImageCel = '';
		$this->HTMLTextTablet = '';
		$this->DisplayImageTablet = '';
		$this->ResponsiveDesignProps = array();
		//@JAPR
		
		$this->DisplayImage = '';
		$this->phoneNum = '';
		
		//08-Ene-2015: Se quitó el nuevo editor de esta pantalla ya que jpuente y lroux decidieron que no era necesario
		//por lo tanto se ocultó el combo para seleccionar el editor y se considerará siempre el tiny (EditorType = 0)
		$this->EditorType = 0;
	
		$this->HTMLTextDes = '';
		$this->HTMLTextiPodDes = '';
		$this->HTMLTextiPadDes = '';
		$this->HTMLTextiPadMiniDes = '';
		$this->HTMLTextiPhoneDes = '';
		$this->HTMLTextCelDes = '';
		$this->HTMLTextTabletDes = '';
	
		$this->HTMLTextDesHTML = '';
		$this->HTMLTextiPodDesHTML = '';
		$this->HTMLTextiPadDesHTML = '';
		$this->HTMLTextiPadMiniDesHTML = '';
		$this->HTMLTextiPhoneDesHTML = '';
		$this->HTMLTextCelDesHTML = '';
		$this->HTMLTextTabletDesHTML = '';

		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Marzo-2015: Quitar el path, ya que ahora la definición de una forma se mostrará desde el árbol y no debe permitir desde esta pantalla
		//cambiarse a la de colección de formas.
		$this->HasPath = false;
		//Marzo-2015: Quitar el botón de grabar e ir al padre, el título de la forma y la línea horizontal
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->CopiedOptionID = 0;
		$this->ForceNew = false;
		$this->bCopy = false;
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		$this->SourceID = 0;
	}

	//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	public static function getMainQuery() {
		//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		$strAdditionalFields = '';
		if (getMDVersion() >= esvExtendedActionsData) {
			$strAdditionalFields .= ', t1.ActionTitle';
		}
		if (getMDVersion() >= esvCallList) {
			$strAdditionalFields .= ', t1.phoneNumber';
		}
		//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
		if (getMDVersion() >= esveBavelSuppActions) {
			$strAdditionalFields .= ', t1.eBavelActionFormID';
		}
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		if (getMDVersion() >= esvHideAnswers) {
			$strAdditionalFields .= ', t1.Hidden';
		}
		//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$strAdditionalFields .= ', t1.HTMLText';
		}
		if (getMDVersion() >= esvDefaultOption) {
			$strAdditionalFields .= ', t1.DefaultValue';
		}
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		if (getMDVersion() >= esvExtendedModifInfo) {
			$strAdditionalFields .= ', t1.CreationVersion, t1.LastVersion ';
		}
		//@JAPR
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.CancelAgenda';
		}
		
		//@JAPR 2018-07-09: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		/*if (getMDVersion() >= esvEditorHTML)
		{
			$strAdditionalFields .= ', t1.EditorType ';
			$strAdditionalFields .= ', t1.HTMLTextDes ';
		}*/
		//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
		if (getMDVersion() >= esvActionConditions) {
			$strAdditionalFields .= ', t1.ActionCondition';
		}
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		if (getMDVersion() >= esvCopyForms) {
			$strAdditionalFields .= ', t1.SourceID';
		}
		//@JAPR
		$sql = "SELECT t1.QuestionID, t1.ConsecutiveID, t1.DisplayText, t1.SortOrder, t1.NextSection, t1.IndDimID, t1.IndicatorID, 
				t1.Score, t2.QTypeID, t1.ActionText, t1.ResponsibleID, t1.CategoryID, t1.DaysDueDate, t1.InsertWhen, t1.DisplayImage, t2.SurveyID,
				t2.QDisplayMode $strAdditionalFields 
			FROM SI_SV_QAnswers t1, SI_SV_Question t2 
			WHERE t1.QuestionID = t2.QuestionID ";
		return $sql;
	}
	//@JAPR
	
	static function NewInstance($aRepository, $aQuestionID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $aQuestionID);
	}

	static function NewInstanceWithID($aRepository, $aQuestionOptionID, $aGetValues = false)
	{
		$anInstance = null;
		
		if (((int) $aQuestionOptionID) <= 0)
		{
			return $anInstance;
		}
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		$anInstance =& BITAMGlobalFormsInstance::GetQuestionOptionInstanceWithID($aQuestionOptionID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		$sql = BITAMQuestionOption::getMainQuery().
			" AND t1.ConsecutiveID = ".$aQuestionOptionID;
		//@JAPR
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS, $aGetValues);
		}
		
		return $anInstance;
	}
	
	//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
	//Marzo-2015
	static function NewInstanceWithName($aRepository, $aQuestionOptionName, $aQuestionID, $aGetValues = false)
	{
		$anInstance = null;
		
		if (is_null($aQuestionOptionName))
		{
			return $anInstance;
		}
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		$sql = BITAMQuestionOption::getMainQuery().
			" AND t1.QuestionID = {$aQuestionID} AND t1.DisplayText = ".$aRepository->DataADOConnection->Quote($aQuestionOptionName);
		//@JAPR
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS, $aGetValues);
		}
		
		return $anInstance;
	}

	//@JAPR 2014-08-01: Agregado el Responsive Design
	/* Carga las propiedades específicas del Responsive Design basado en los parámetros, agregándolas a las instancias especificadas en la
	colección o array proporcionado, de tal forma que este método se puede invocar bajo demanda sólo en los casos donde realmente se necesiten
	estas propiedades sin tener que forzar a que el NewInstanceFromRS se sature con queries y asignaciones que no van a ser utilizadas
	El parámetro $anArrayOfObjects se asumirá que es un array ya indexado por el ConsecutiveID, sin embargo si se trata de una colección
	de Respuestas, entonces primero se procede a crear el array interno para facilitar el acceso a las instancias. En el caso de estos objetos,
	debido a que a nivel de pregunta se manejan como diferentes arrays dentro de la misma instancia de la pregunta, adicionalmente se puede
	recibir una instancia de BITAMQuestion, en ese caso se realizará un tratamiento especial para actualizar sólo directamente en los arrays
	de la instancia
	//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//@JAPRDescontinuada en V6
	*/
	static function GetResponsiveDesignProps($aRepository, $anArrayOfObjects, $aSurveyID = null, $anArrayOfObjectIDs = null, $aQuestionID = null) {
		return true;
		global $gblShowErrorSurvey;
		
		$intDeviceID = null;
		$aSurveyID = (int) $aSurveyID;
		$aQuestionID = (int) $aQuestionID;
		if (is_null($anArrayOfObjectIDs) || !is_array($anArrayOfObjectIDs)) {
			$anArrayOfObjectIDs = array();
		}
		
		//Asigna el array de preguntas indexado por el ID
		$blnIsParentInstance = false;
		$arrObjects = $anArrayOfObjects;
		if (is_object($anArrayOfObjects) && get_class($anArrayOfObjects) == 'BITAMQuestion') {
			//En este caso se recibirá tratamiento especial, se recibió una instancia de pregunta y se asume que se quieren procesar las
			//propiedades de sus opciones de respuesta directamente en la propia instancia de pregunta, en lugar de actualizarla en las
			//instancias de opciones de respuestas como funcionaría generalmente esta función para otros tipos de objetos
			$blnIsParentInstance = true;
			//Se debe asignar el DeviceID ya que como se va a cargar sólo para la instancia solicitada para regresar las definiciones, no
			//se tienen múltiples arrays por dispositivo
			$intDeviceID = identifyDeviceType();
		}
		else {
			if (!is_array($anArrayOfObjects)) {
				$arrObjects = array();
				foreach ($anArrayOfObjects->Collection as $objObject) {
					$arrObjects[$objObject->QuestionID] = $objObject;
				}
			}
		}
		
		//Si no se hubiera especificado ningún filtro, utiliza el array de instancias para obtener las llaves y cargar sólo los objetos que
		//fueron recibidos, de esta forma se puede sólo recibir el array de instancias sin especificar mas parámetros y funcionará bien
		$anArrayOfObjectIDsFlipped = array();
		if ($aSurveyID <= 0 && $aQuestionID <= 0 && count($anArrayOfObjectIDs) == 0) {
			if ($blnIsParentInstance) {
				$anArrayOfObjectIDs = @$arrObjects->QConsecutiveIDs;
				if (is_null($anArrayOfObjectIDs)) {
					$anArrayOfObjectIDs = array();
				}
				
				//Se necesitará accesar por ID el array, así que lo invierte ya que habiendo entrado como instancia de pregunta directamente,
				//los arrays vienen indexados por posición y no por ID
				$anArrayOfObjectIDsFlipped = array_flip($anArrayOfObjectIDs);
				if (count($anArrayOfObjectIDs) == 0) {
					//En este caso no continua sin un filtro, así que forzará a filtrar además por la pregunta, porque no habrá filtro por IDs
					$aQuestionID = (int) @$arrObjects->QuestionID;
				}
			}
			else {
				$anArrayOfObjectIDs = array_keys($arrObjects);
				if (count($anArrayOfObjectIDs) == 0) {
					//En este caso no continua sin un filtro, así que forzará a filtrar además por la pregunta, porque no habrá filtro por IDs
					$aQuestionID = (int) @$arrObjects->QuestionID;
				}
			}
		}
		elseif ($blnIsParentInstance) {
			//Si se recibió una instancia de question, de todas maneras se tienen que obtener las llaves de dicha instancia
			$arrTemp = @$arrObjects->QConsecutiveIDs;
			if (!is_null($arrTemp)) {
				$anArrayOfObjectIDsFlipped = array_flip($arrTemp);
			}
		}
		
		//Genera el filtro basado en los parámetros especificados
		$where = "";
		if ($aSurveyID > 0) {
			$where .= " AND A.SurveyID = {$aSurveyID}";
		}
		if ($aQuestionID > 0) {
			$where .= " AND B.QuestionID = {$aQuestionID}";
		}
		if (!is_null($intDeviceID)) {
			$where .= " AND A.DeviceID = {$intDeviceID}";
		}
		
		$filter = '';
		if (!is_null($anArrayOfObjectIDs))
		{
			switch (count($anArrayOfObjectIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND A.ObjectID = ".((int)$anArrayOfObjectIDs[0]);
					break;
				default:
					foreach ($anArrayOfObjectIDs as $anObjectID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $anObjectID; 
					}
					if ($filter != "")
					{
						$filter = " AND A.ObjectID IN (".$filter.")";
					}
					break;
			}
		}
		$where .= $filter;
		
		$strSelect = "";
		if (getMDVersion() >= esvEditorHTML)
		{
			$strSelect = ", A.HTMLTextDes";
		}
		
		$sql = "SELECT A.SurveyID, B.ConsecutiveID, A.DeviceID, A.FormattedText AS HTMLText, A.ImageText AS DisplayImage  ".$strSelect."
			FROM SI_SV_SurveyHTML A 
				INNER JOIN SI_SV_QAnswers B ON A.ObjectID = B.ConsecutiveID 
			WHERE A.ObjectType = ".otyAnswer.$where;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyHTML, SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyHTML, SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			$intObjectID = (int) @$aRS->fields["consecutiveid"];
			$intDeviceID = (int) @$aRS->fields["deviceid"];
			
			if ($blnIsParentInstance) {
				//En este caso se tiene que actualizar directamente en el objeto recibido a nivel de diferentes Arrays
				$intIndex = @$anArrayOfObjectIDsFlipped[$intObjectID];
				if (!is_null($intIndex)) {
					//En este caso se va a sobreescribir directamente el array ya que se utilizará para descargar las definiciones, por
					//tanto no tiene caso complicar mas el proceso creando arrays adicionales por tipo de dispositivo
					//Sólo sobreescribirá las propiedades si traen valor, ya que si no se dejará la default que es la directamente asignada
					//a la instancia
					$strProperty = "HTMLText";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
					if (trim($strPropValue) != '') {
						$arrObjects->HTMLText[$intIndex] = $strPropValue;
					}
					$strProperty = "DisplayImage";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
					if (trim($strPropValue) != '') {
						$arrObjects->DisplayImage[$intIndex] = $strPropValue;
					}
					
					if (getMDVersion() >= esvEditorHTML)
					{
						$strProperty = "HTMLText";
						$strPropValue = (string) @$aRS->fields[strtolower($strProperty)."des"];
						
						if (trim($strPropValue) != '') {
							$arrObjects->HTMLTextDes[$intIndex] = $strPropValue;
						}
						
						$strProperty = "HTMLText";
						$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
						
						if (trim($strPropValue) != '') {
							$arrObjects->HTMLTextDesHTML[$intIndex] = $strPropValue;
						}
					}
				}
			}
			else {
				$objObject = @$arrObjects[$intObjectID];
				if (!is_null($objObject)) {
					//En este caso si se tienen que llenar propiedades adicionales por dispositivo, ya que se utiliza la instancia directa de
					//opción de respuesta entre otras cosas para su captura, así que hay múltiples componentes a llenar cada uno por dispositivo
					$objObject->ResponsiveDesignProps[$intDeviceID] = 1;
					$strProperty = "HTMLText";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue);
					$strProperty = "DisplayImage";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue);
					
					if (getMDVersion() >= esvEditorHTML)
					{
						$strProperty = "HTMLText";
						$strPropValue = (string) @$aRS->fields[strtolower($strProperty)."des"];
						$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue, 1);
	
						$strProperty = "HTMLText";
						$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
						$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue, 1, 1);
					}
				}
			}
			
			$aRS->MoveNext();
		}
	}
	
	/* Asigna dinámicamente el valor de la propiedad especificada ajustada al tipo de dispositivo indicado en el parámetro $aDeviceName, 
	si este es numérico entonces se convierte a su equivalente en String para completar el nombre de propiedad
	*/
	public function setResponsiveDesignProperty($aPropertyName, $aDeviceName, $aValue, $editor=0, $html=0) {
		if (is_numeric($aDeviceName)) {
			$aDeviceName = getDeviceNameFromID($aDeviceName);
		}
		
		$strPropName = trim($aPropertyName.$aDeviceName);
		
		if ($editor==1)
		{
			$strPropName = $strPropName."Des";
			
			if($html==1)
			{
				$strPropName = $strPropName."HTML";
			}
		}
				
		if ($strPropName != '') {
			@$this->$strPropName = $aValue;
		}
	}
	
	/* Obtiene dinámicamente el valor de la propiedad especificada ajustada al tipo de dispositivo indicado en el parámetro $aDeviceName, 
	si este es numérico entonces se convierte a su equivalente en String para completar el nombre de propiedad
	*/
	public function getResponsiveDesignProperty($aPropertyName, $aDeviceName, $bDefaultIfEmpty = false, $editor=0) {
		if (is_numeric($aDeviceName)) {
			$aDeviceName = getDeviceNameFromID($aDeviceName);
		}
		
		$aValue = null;
		$strPropName = trim($aPropertyName.$aDeviceName);
		if ($strPropName != '') {
			
			if ($editor==1)
			{
				$strPropName = $strPropName."Des";
			}
			
			$aValue = @$this->$strPropName;
			
			//Si no se encuentra configurado nada específico para el tipo de dispositivo y no se trata del dispositivo default, entonces
			//extrae el valor default directo 
			if (trim($aValue) == '' && $aDeviceName != '' && $bDefaultIfEmpty) {
				$aPropertyName = trim($aPropertyName);
				
				if ($editor==1)
				{
					$aPropertyName = $aPropertyName."Des";
				}
								
				$aValue = @$this->$aPropertyName;
			}
		}
		
		return $aValue;
	}
	//@JAPR
	
	static function NewInstanceFromRS($aRepository, $aRS, $aGetValues = false)
	{
		$aQuestionID = (int)$aRS->fields["questionid"];
		
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
		//@JAPR
		$anInstance->QuestionOptionID = (int)$aRS->fields["consecutiveid"];
		$anInstance->QuestionOptionName = $aRS->fields["displaytext"];
		$anInstance->QuestionOptionNameBack = $aRS->fields["displaytext"];
		$anInstance->SortOrder = (int)$aRS->fields["sortorder"];
		$anInstance->NextQuestion = (int)$aRS->fields["nextsection"];
		$anInstance->IndDimID = (int)$aRS->fields["inddimid"];
		$anInstance->IndicatorID = (int)$aRS->fields["indicatorid"];
		$anInstance->QTypeID = (int)$aRS->fields["qtypeid"];
		//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		$anInstance->QDisplayMode = (int)@$aRS->fields["qdisplaymode"];
		//@JAPR
		$anInstance->Score = (string) @$aRS->fields["score"];
		//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple
		$anInstance->ActionText = (string) $aRS->fields["actiontext"];
		$anInstance->ResponsibleID = (int) $aRS->fields["responsibleid"];
		$anInstance->CategoryID = (int) $aRS->fields["categoryid"];
		$anInstance->DaysDueDate = abs((int) $aRS->fields["daysduedate"]);
		$anInstance->InsertWhen = (int) $aRS->fields["insertwhen"];
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		$anInstance->ActionTitle = (string) @$aRS->fields["actiontitle"];
		//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
		$anInstance->ActionCondition = (string) @$aRS->fields["actioncondition"];
		//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
		$anInstance->SurveyID = (int) @$aRS->fields["surveyid"];
		$anInstance->eBavelActionFormID = (int) @$aRS->fields["ebavelactionformid"];
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		$anInstance->Visible = (int) !((int) @$aRS->fields["hidden"]);
		$anInstance->DefaultValue = (string) @$aRS->fields["defaultvalue"];
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		$anInstance->LastModAdminVersion = (float) @$aRS->fields["lastversion"];
		$anInstance->CreationAdminVersion = (float) @$aRS->fields["creationversion"];
		
		//Sólo leerá los posibles valores de mapeo si se está solicitando, lo cual aplica básicamente para la edición, el resto de las veces
		//se debería de pedir explícitamente a la instancia que lea esta información exclusivamente cuando se va a utilizar
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			if (getMDVersion() >= esveBavelSuppActions) {
				if ($anInstance->eBavelActionFormID > 0 && $aGetValues) {
					$anInstance->readeBavelActionFieldsMapping();
				}
			}
		}
		//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
		$anInstance->HTMLText = (string) @$aRS->fields["htmltext"];
		//@JAPR
		
		$anInstance->DisplayImage = (string) $aRS->fields["displayimage"];
		$anInstance->phoneNum = (string) @$aRS->fields["phonenumber"];
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Estas propiedades están descontinuadas a partir de eForms v6
		//$anInstance->getOptionActions();
		//@JAPR
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$anInstance->CancelAgenda = (int) @$aRS->fields["cancelagenda"];
		}

		//08-Ene-2015: Se quitó el nuevo editor de esta pantalla ya que jpuente y lroux decidieron que no era necesario
		//por lo tanto se ocultó el combo para seleccionar el editor y se considerará siempre el tiny (EditorType = 0)
		//$anInstance->EditorType = (int) @$aRS->fields["editortype"];
		$anInstance->HTMLTextDes = trim(@$aRS->fields["htmltextdes"]);
		$anInstance->HTMLTextDesHTML = trim(@$aRS->fields["htmltext"]);
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		$anInstance->SourceID = (int) @$aRS->fields["sourceid"];
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		BITAMGlobalFormsInstance::AddQuestionOptionInstanceWithID($anInstance->QuestionOptionID, $anInstance);
		//@JAPR
		
		return $anInstance;
	}
	
	//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
	//Lee el mapeo de campos de eForms hacia eBavel y los almacena como un string de pares NombreCampo=Id que se puede usar en la ventana para su configuración
	function readeBavelActionFieldsMapping() {
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//@JAPRDescontinuada en v6
		return;
		//@JAPR
		require_once("questionOptionActionMap.inc.php");
		
		$this->eBavelActionFieldsData = '';
		$this->eBavelActionFieldsDataOld = '';
		$this->QuestionOptionFieldMap = null;
		$objQuestionOptionFieldMap = BITAMQuestionOptionActionMap::NewInstanceWithAnswerID($this->Repository, $this->SurveyID, $this->QuestionOptionID);
		if (is_null($objQuestionOptionFieldMap) || !isset($objQuestionOptionFieldMap->ArrayVariables) || count($objQuestionOptionFieldMap->ArrayVariables) == 0) {
			return;
		}
		$this->QuestionOptionFieldMap = $objQuestionOptionFieldMap;
		
		$strAnd = '';
		$streBavelFieldsData = '';
		foreach($objQuestionOptionFieldMap->ArrayVariables as $inteBavelFieldID => $strVariableName) {
			$streBavelFieldsData .= $strAnd.$strVariableName.'='.((int) @$objQuestionOptionFieldMap->ArrayVariableValues[$inteBavelFieldID]);
			//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
			//@JAPR 2014-10-28: Agregado el mapeo de Scores
			//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
			$streBavelFieldsData .= "=".((int) @$objQuestionOptionFieldMap->ArrayVariablesDataID[$inteBavelFieldID]).
				"=".((int) @$objQuestionOptionFieldMap->ArrayVariablesMemberID[$inteBavelFieldID]).
				"=".((string) @$objQuestionOptionFieldMap->ArrayVariablesFormula[$inteBavelFieldID]);
			//@JAPR
			$strAnd = '|';
		}
		
		$this->eBavelActionFieldsData = $streBavelFieldsData;
		$this->eBavelActionFieldsDataOld = $this->eBavelActionFieldsData;
	}
	
	//Graba el string de pares NombreCampo=Id en la tabla de mapeos de campos de eForms hacia eBavel sólo si hubo cambios
	function saveeBavelActionFieldsMapping() {
		if (trim((string) $this->eBavelActionFieldsData) == trim((string) $this->eBavelActionFieldsDataOld)) {
			return;
		}
		
		require_once("questionOptionActionMap.inc.php");
		
		//El siguiente objeto, esté o no algo grabado en la Metadata, contendrá la lista de los campos configurables por encuesta indexados por
		//el ID, con dicha lista se podrán actualizar los campos prviamente grabados o se agregarán las nuevas configuraciones
		$objQuestionOptionFieldMap = BITAMQuestionOptionActionMap::NewInstanceWithAnswerID($this->Repository, $this->SurveyID, $this->QuestionOptionID);
		if (is_null($objQuestionOptionFieldMap) || !isset($objQuestionOptionFieldMap->ArrayVariables) || count($objQuestionOptionFieldMap->ArrayVariables) == 0) {
			return;
		}
		
		$arrFieldsByName = array_flip($objQuestionOptionFieldMap->ArrayVariables);
		
		//Recorre el String recibido actualizando, insertando o eliminando según el caso los campos recibidos
		$arrFieldsData = explode('|', $this->eBavelActionFieldsData);
		$arrProcessedFields = array();
		
		foreach ($arrFieldsData as $aFieldData) {
			if (trim($aFieldData) == '') {
				continue;
			}
			
			$arrFieldInfo = explode('=', $aFieldData);
			if (count($arrFieldInfo) < 2) {
				continue;
			}
			
			$streBavelFieldName = $arrFieldInfo[0];
			$inteBavelFieldID = (int) @$arrFieldsByName[$streBavelFieldName];
			if ($inteBavelFieldID == 0) {
				continue;
			}
			
			//Es un mapeo de campo válido, identifica si debe agregar o actualizar la información
			if (!isset($objQuestionOptionFieldMap->ArrayVariableValues[$inteBavelFieldID])) {
				//Es un elemento nuevo, pero como el array ya debe venir con los defaults conocidos, se ignorará pues debe ser un error
				//No se intentará eliminar, simplemente al cargar la instancia nunca será reconocído como un tipo de mapeo válido en esta versión
			}
			else {
				//Es un elemento ya existente, se actualizará/insertará dependiendo de si ya estaba o no en la Metadata, pero se actualiza su
				//mapeo. El grabado se hace en base a la propiedad y no al array de valores, por lo tanto se agrega/modifica dicha propiedad
				$objQuestionOptionFieldMap->ArrayVariableValues[$inteBavelFieldID] = $arrFieldInfo[1];
				$objQuestionOptionFieldMap->$streBavelFieldName = $arrFieldInfo[1];
				$arrProcessedFields[$streBavelFieldName] = $streBavelFieldName;
				//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
				//@JAPR 2014-10-28: Agregado el mapeo de Scores
				//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
				$objQuestionOptionFieldMap->ArrayVariablesDataID[$inteBavelFieldID] = (int) @$arrFieldInfo[2];
				$objQuestionOptionFieldMap->ArrayVariablesMemberID[$inteBavelFieldID] = (int) @$arrFieldInfo[3];
				$objQuestionOptionFieldMap->ArrayVariablesFormula[$inteBavelFieldID] = (string) @$arrFieldInfo[4];
				//@JAPR
			}
		}
		
		//Invoca al método que graba los mapeos
		@$objQuestionOptionFieldMap->save();
	}
	//@JAPR
	
	function getOptionActions()
	{
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Estas propiedades están descontinuadas a partir de eForms v6
		//@JAPRDescontinuada en V6
		return;
		//@JAPR
		
		$sql = "SELECT ActionText FROM SI_SV_QOptionActions WHERE QuestionID = ".$this->QuestionID." 
				AND OptionText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName)." 
				ORDER BY SortOrder";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QOptionActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while(!$aRS->EOF)
		{
			$actionText = $aRS->fields["actiontext"];
			$this->OptionActions[] = $actionText;

			$aRS->MoveNext();
		}

		$LN = chr(13).chr(10);
		$this->StrOptionActions = implode($LN, $this->OptionActions);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		$aQuestionID = getParamValue('QuestionID', 'both', '(int)');
		if (array_key_exists("QuestionOptionID", $aHTTPRequest->POST))
		{
			$aQuestionOptionID = $aHTTPRequest->POST["QuestionOptionID"];
			
			if (is_array($aQuestionOptionID))
			{
				//@JAPR 2013-12-04: Agregada la opción para compartir las opciones de respuesta con otras preguntas
				//Se envía el ID de la pregunta donde se comparten las opciones, de tal forma que se carguen los valores específicos de cada pregunta
				//desde la pregunta compartida y sólo los datos generales desde la pregunta origen
				$aCollection = BITAMQuestionOptionCollection::NewInstance($aRepository, $aQuestionID, $aQuestionOptionID, false);
				//@JAPR
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}

				$anInstance = BITAMQuestionOption::NewInstance($aRepository, $aQuestionID);

				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aQuestionOptionID, true);
				if (is_null($anInstance))
				{
					//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
					$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
				}
				//@JAPR 2014-08-01: Agregado el Responsive Design
				else {
					//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
					//Estas propiedades están descontinuadas a partir de eForms v6
					if (getMDVersion() < esveFormsv6 ) {
						if (getMDVersion() >= esvResponsiveDesign) {
							//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
							$strCalledClass::GetResponsiveDesignProps($aRepository, array($anInstance->QuestionOptionID => $anInstance));
						}
					}
					//@JAPR
				}
				//@JAPR
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();

				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}

				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
						$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
					}
				}

				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		if (array_key_exists("QuestionOptionID", $aHTTPRequest->GET) || array_key_exists("QuestionOptionName", $aHTTPRequest->GET))
		{
			if(array_key_exists("QuestionOptionID", $aHTTPRequest->GET))
			{
				$aQuestionOptionID = $aHTTPRequest->GET["QuestionOptionID"];
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aQuestionOptionID, true);
			}
			else
			{
				//Marzo-2015: Cuando se dá clic en el árbol en el nodo de opción de respuesta se manda el nombre de la opción y el QuestionID
				if(array_key_exists("QuestionOptionName", $aHTTPRequest->GET) && array_key_exists("QuestionID", $aHTTPRequest->GET))
				{
					$aQuestionOptionName = trim($aHTTPRequest->GET["QuestionOptionName"]);
					$aQuestionID = (int) $aHTTPRequest->GET["QuestionID"];
					//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
					//@JAPR 2015-04-15: Corregido un bug, se estaba haciendo un cast a int del nombre de la opción
					$anInstance = $strCalledClass::NewInstanceWithName($aRepository, $aQuestionOptionName, $aQuestionID, true);
				}
			}

			if (is_null($anInstance))
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
			}
			//@JAPR 2014-08-01: Agregado el Responsive Design
			else {
				//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
				//Estas propiedades están descontinuadas a partir de eForms v6
				if (getMDVersion() < esveFormsv6 ) {
					if (getMDVersion() >= esvResponsiveDesign) {
						//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
						$strCalledClass::GetResponsiveDesignProps($aRepository, array($anInstance->QuestionOptionID => $anInstance));
					}
				}
				//@JAPR
			}
			//@JAPR
		}
		else
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
		}

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		global $garrUpdatedPropsWithVars;
		if (!isset($garrUpdatedPropsWithVars)) {
			$garrUpdatedPropsWithVars = array();
		}
		if (!isset($garrUpdatedPropsWithVars["Options"])) {
			$garrUpdatedPropsWithVars["Options"] = array();
		}
		//@JAPR
		
		if (array_key_exists("QuestionOptionID", $anArray))
		{
			$this->QuestionOptionID = (int)$anArray["QuestionOptionID"];
		}

		if (array_key_exists("QuestionOptionName", $anArray))
		{
			$this->QuestionOptionName = trim($anArray["QuestionOptionName"]);
		}

		//@JAPR 2019-02-16: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if( $this->QTypeID == qtpSingle || $this->QTypeID == qtpSketchPlus )
		{
	 		if (array_key_exists("NextQuestion", $anArray))
			{
				$this->NextQuestion = (int)$anArray["NextQuestion"];
			}
		}

		if (array_key_exists("Score", $anArray))
		{
			$this->Score = trim($anArray["Score"]);
			//@JAPR 2016-07-28: Corregido un bug, se estaba permitiendo grabad un Score como texto (#XYTODV)
			if (!is_numeric($this->Score)) {
				$this->Score = "";
			}
			//@JAPR
		}

		if (array_key_exists("StrOptionActions", $anArray))
		{
			$this->StrOptionActions = trim($anArray["StrOptionActions"]);
		}

		//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple
		if (array_key_exists("ActionText", $anArray))
		{
			$this->ActionText = trim($anArray["ActionText"]);
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->ActionText = $this->TranslateVariableQuestionNumbersByIDs($this->ActionText, optyActionText);
		}

		if (array_key_exists("ResponsibleID", $anArray))
		{
			$this->ResponsibleID = (int) $anArray["ResponsibleID"];
		}

		if (array_key_exists("CategoryID", $anArray))
		{
			$this->CategoryID = (int) $anArray["CategoryID"];
		}

		if (array_key_exists("DaysDueDate", $anArray))
		{
			$this->DaysDueDate = abs((int) $anArray["DaysDueDate"]);
		}
		
		if (array_key_exists("InsertWhen", $anArray))
		{
			$this->InsertWhen = (int) $anArray["InsertWhen"];
		}
		
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		if (array_key_exists("ActionTitle", $anArray)) {
			$this->ActionTitle = (string) $anArray["ActionTitle"];
			if (getMDVersion() >= esvExtendedActionsData) {
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->ActionTitle = $this->TranslateVariableQuestionNumbersByIDs($this->ActionTitle, optyActionTitle);
			}
		}
		
		//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
		if (getMDVersion() >= esvActionConditions) {
			if (array_key_exists("ActionCondition", $anArray)) {
				$this->ActionCondition = (string) $anArray["ActionCondition"];
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->ActionCondition = $this->TranslateVariableQuestionNumbersByIDs($this->ActionCondition, optyActionCondition);
			}
		}
		
		//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
		if (array_key_exists("eBavelActionFormID", $anArray))
		{
			$this->eBavelActionFormID = (int)$anArray["eBavelActionFormID"];
		}
		
		if (array_key_exists("eBavelActionFieldsData", $anArray))
		{
			$this->eBavelActionFieldsData = $anArray["eBavelActionFieldsData"];
		}
		//@JAPR
		
		if (array_key_exists("phoneNum", $anArray)) {
			$this->phoneNum = (string) $anArray["phoneNum"];
		}
		
		if (array_key_exists("DisplayImage", $anArray))
		{
			$this->DisplayImage = (string) $anArray["DisplayImage"];
		}
		
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		if (array_key_exists("Visible", $anArray))
		{
			$this->Visible = (int) $anArray["Visible"];
		}
		
		if (array_key_exists("CancelAgenda", $anArray))
		{
			$this->CancelAgenda = (int) $anArray["CancelAgenda"];
		}
		
		//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
		if (array_key_exists("HTMLText", $anArray))
		{
			$this->HTMLText = (string) $anArray["HTMLText"];
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->HTMLText = $this->TranslateVariableQuestionNumbersByIDs($this->HTMLText, optyLabel);
				//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
				$garrUpdatedPropsWithVars["Options"]["HTMLText"] = 1;
				//@JAPR
			}
		}
		//@JAPR
		
		if (array_key_exists("DefaultValue", $anArray))
		{
			$this->DefaultValue = @$anArray["DefaultValue"];
			if (getMDVersion() >= esvDefaultOption) {
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->DefaultValue = $this->TranslateVariableQuestionNumbersByIDs($this->DefaultValue, optyDefaultValue);
				//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
				$garrUpdatedPropsWithVars["Options"]["DefaultValue"] = 1;
				//@JAPR
			}
		}
		
		//@JAPR 2014-08-01: Agregado el Responsive Design
		if (getMDVersion() >= esvResponsiveDesign) {
			for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
				$strDeviceName = getDeviceNameFromID($intDeviceID);
				
				$strProperty = "HTMLText";
				$strFieldName = $strProperty.$strDeviceName;
				$strValue = '';
		 		if (array_key_exists($strFieldName, $anArray))
				{
					$strValue = $anArray[$strFieldName];
					//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
					//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
					$strValue = $this->TranslateVariableQuestionNumbersByIDs($strValue, optyLabel);
					//@JAPR
					$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue);
				}
				
				$strProperty = "DisplayImage";
				$strFieldName = $strProperty.$strDeviceName;
				$strValue = '';
		 		if (array_key_exists($strFieldName, $anArray))
				{
					$strValue = $anArray[$strFieldName];
					$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue);
				}
			}
		}
		//@JAPR
		
		if (getMDVersion() >= esvEditorHTML)
		{
	 		$editor = 1;
	 		
			if (array_key_exists("EditorType", $anArray))
			{
				$this->EditorType = (int) $anArray["EditorType"];
			}
			
	 		if (array_key_exists("HTMLTextDes", $anArray))
			{
				$this->HTMLTextDes = $anArray["HTMLTextDes"];
			}
			
	 		if (array_key_exists("HTMLTextDesHTML", $anArray))
			{
				$this->HTMLTextDesHTML = $anArray["HTMLTextDesHTML"];
			}
			
			if (getMDVersion() >= esvResponsiveDesign) {
				for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
					$strDeviceName = getDeviceNameFromID($intDeviceID);
					
					$strProperty = "HTMLText";
					//Campo correspondiente al nuevo editor por eso termina en "Des"
					$strFieldName = $strProperty.$strDeviceName."Des";
					$strValue = '';
					
			 		if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue, $editor);
					}
					
					$strFieldName = $strProperty.$strDeviceName."Des"."HTML";
					$strValue = '';
					
			 		if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue, $editor, 1);
					}
				}
			}
		
		}		
		
		return $this;
	}
	
	//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
	/* Función equivalente a ReplaceVariableQuestionNumbersByIDs, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionNumbersByIDs($sText, $iPropertyID = 0) {
		$blnServerVariables = false;
		return (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($this->Repository, $this->SurveyID, $sText, $this->LastModAdminVersion, otyAnswer, $this->SectionID, $iPropertyID, $blnServerVariables);
	}
	
	/* Función equivalente a ReplaceVariableQuestionIDsByNumbers, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionIDsByNumbers($sText, $bServerVariables = false) {
		return (string) @BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($this->Repository, $this->SurveyID, $sText, $this->LastModAdminVersion, $bServerVariables);
	}
	
	//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
	/* Esta función actualizará las propiedades que pudieran tener variables en su interior y que no fueron asignadas por la función UpdateFromArray, forzando a traducirlas primero 
	a números de pregunta para que en caso de que el objeto no hubiera sido grabado aún con la versión que ya corregía los IDs se puedan recuperar correctamente, posteriormente las
	volverá a traducir a IDs pero ahora aplicando ya la corrección para no realizar reemplazos parciales. Esta función debe utilizarse siempre antesde un Save del objeto para que 
	durante el primer grabado sea cual fuera la razón para hacerlo, se puedan traducir correctamente las propiedades al momento de asignar el numero de versión de este servicio que ya
	asumirá que graba correctamente este tipo de propiededes, por tanto su uso es:
	1- Esperar a un UpdateFromArray (opcional, si no se invocó, simplemente sobreescribirá todas las propiedades, como por ejemplo en el proceso de copiado)
	2- Ejecutar esta función (si hubo un UpdateFromArray, algunas propiedades podrían ya estar asignadas en el array de reemplazos, así que esas se ignoran)
	3- Ejecutar al Save (esto sobreescribirá al objeto y sus propiedades que utilizan variables
	*/
	function fixAllPropertiesWithVarsNotSet() {
		global $garrUpdatedPropsWithVars;
		
		if (!isset($garrUpdatedPropsWithVars) || !is_array($garrUpdatedPropsWithVars) || !isset($garrUpdatedPropsWithVars["Options"])) {
			$garrUpdatedPropsWithVars = array();
			$garrUpdatedPropsWithVars["Options"] = array();
		}
		
		//Si ya se había aplicado un grabado a este objeto por lo menos con la versión de este fix, entonces ya no tiene sentido continuar con el proceso pues no habra diferencia
		if ($this->LastModAdminVersion >= esvPartialIDsReplaceFix) {
			return;
		}
		
		//Actualiza las propiedades no asignadas en el array de propiedades modificadas
		//Para que el proceso funcione correctamente, es indispensable que se engañe a la instancia sobreescribiendo temporalmente LastModAdminVersion a la versión actual pero sólo
		//cuando ya se va a reemplazar por IDs, cuando se va a reemplazar por números si puede dejar la versión actual para que auto-corrija el error con el que había grabado usando IDs
		//previamente
		$dblLastModAdminVersion = $this->LastModAdminVersion;
		$blnUpdatedProperties = false;
		//Primero traducirá de IDs a números todas las propiedades no grabadas manualmente para corregir los IDs mal grabados
		if (!isset($garrUpdatedPropsWithVars["Options"]["HTMLText"])) {
			$this->HTMLText = $this->TranslateVariableQuestionIDsByNumbers($this->HTMLText);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Options"]["DefaultValue"])) {
			$this->DefaultValue = $this->TranslateVariableQuestionIDsByNumbers($this->DefaultValue);
			$blnUpdatedProperties = true;
		}
		
		if (!$blnUpdatedProperties) {
			return;
		}
		
		//Finalmente actualizará las propiedades no grabadas manualmente a IDs ya aplicando la corrección sobreescribiendo la versión del objeto
		//Como esta función se supone que se usará justo antes de un save, es correcto dejar la propiedad LastModAdminVersion actualizada, ya que de todas formas se modificaría en el save
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		if (!isset($garrUpdatedPropsWithVars["Options"]["HTMLText"])) {
			$this->HTMLText = $this->TranslateVariableQuestionNumbersByIDs($this->HTMLText, optyLabel);
		}
		if (!isset($garrUpdatedPropsWithVars["Options"]["DefaultValue"])) {
			$this->DefaultValue = $this->TranslateVariableQuestionNumbersByIDs($this->DefaultValue, optyDefaultValue);
		}
	}
	//@JAPR
	
	function save() {
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		if (getMDVersion() >= esvEditorHTML && $this->EditorType == 1)
		{
			//Los campos HTML del editor anterior serán sustituidos con el código HTML generado con el nuevo editor
			if($this->HTMLTextDes!="" || $this->HTMLTextiPodDes !="" || $this->HTMLTextiPadDes !=""
			|| $this->HTMLTextiPadMiniDes!="" || $this->HTMLTextiPhoneDes!="" || $this->HTMLTextCelDes!=""
			|| $this->HTMLTextTabletDes!="")
			{
				$this->HTMLText = $this->HTMLTextDesHTML;
				$this->HTMLTextiPod = $this->HTMLTextiPodDesHTML;
				$this->HTMLTextiPad = $this->HTMLTextiPadDesHTML;
				$this->HTMLTextiPadMini = $this->HTMLTextiPadMiniDesHTML;
				$this->HTMLTextiPhone = $this->HTMLTextiPhoneDesHTML;
				$this->HTMLTextCel = $this->HTMLTextCelDesHTML;
				$this->HTMLTextTablet = $this->HTMLTextTabletDesHTML;
			}
		}
		
		//$gblModMngrErrorMessage = '';
		//Agregado para tipo llamada, solo se validara
		//@JAPR 2015-07-18: Para las formas creadas en v6, no hay diferencia entre las opciones de simple choice y múltiple choice, se usa un mismo método
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
		
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//Al realizar el grabado del objeto (pero no copiado) se deben actualizar a la par todas las configuraciones que pudieran tener variables para arreglar los IDs mal grabados
		//Se debe realizar antes de la asignación de LastModAdminVersion o de lo contrario ya no tendría efecto pues ya consideraría que está grabado con la versión corregida
		if (!$this->bCopy) {
			$this->fixAllPropertiesWithVarsNotSet();
		}
		
		//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
		if ($this->QTypeID == qtpUpdateDest) {
			$this->saveForOtherQuestions();
		}
		//@JAPR
		else if ($this->QTypeID == qtpSingle || $this->QTypeID == qtpCallList || $surveyInstance->CreationAdminVersion >= esveFormsv6) {
			$this->saveForSimpleChoice();
		}
		//@JAPRDescontinuada, a partir de V6 el grabado se hace sin distinción del tipo de pregunta
		else if($this->QTypeID==qtpMulti)
		{
			$this->saveForMultipleChoice();
		}
		
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		if (isset($gblModMngrErrorMessage) && !is_null($gblModMngrErrorMessage) && trim($gblModMngrErrorMessage) != '') {
			die("(".__METHOD__.") ".translate("There were some errors while saving the question option").":\r\n".$gblModMngrErrorMessage.". ");
		}
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if(!$this->bCopy && getMDVersion() < esveFormsv6)
		{
			//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions) {
				if ($this->eBavelActionFormID > 0) {
					if ($this->eBavelActionFieldsData != $this->eBavelActionFieldsDataOld) {
						$this->saveeBavelActionFieldsMapping();
					}
				}
			}
			
			//@JAPR 2014-08-01: Agregado el Responsive Design
			//@JAPR 2018-07-09: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
			if (getMDVersion() < esveFormsv6 ) {
				if (getMDVersion() >= esvResponsiveDesign) {
					$this->saveResponsiveDesignProps();
				}
			}
			//@JAPR
		}
		return $this;
	}
	
	//@JAPR 2014-08-01: Agregado el Responsive Design
	/* Graba las propiedades que corresponden al Responsive Design de forma especial, ya que se graban en la misma tabla pero variando el ID
	en base al tipo de dispositivo del que se trate
	//@JAPR 2018-07-09: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//@JAPRDescontinuada en V6
	*/
	function saveResponsiveDesignProps() {
		return true;
		//Se debe verificar si este objeto ya tenía grabada su propiedad de Responsive Design para cierto tipo de dispositivo, de tal forma
		//que se pueda armar un INSERT o un UPDATE según el caso (No es necesario grabar la propiedad Default pues esa está directamente
		//en la tabla del objeto)
		for ($intDeviceID = dvciPod; $intDeviceID <= dvcTablet; $intDeviceID++) {
			$strHTMLText = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("HTMLText", $intDeviceID));
			$strDisplayImage = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("DisplayImage", $intDeviceID));
			$strEmpty = $this->Repository->DataADOConnection->Quote("");
			
			$strUpdate = "";
			$strInsertFields = "";
			$strInsertValues = "";
			if (getMDVersion() >= esvEditorHTML)
			{
				$strHTMLTextDes = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("HTMLText", $intDeviceID, false, 1));
				$strUpdate = ", HTMLTextDes = ". $strHTMLTextDes;
				$strInsertFields = ", HTMLTextDes";
				$strInsertValues = " ,".$strHTMLTextDes;
			}
						
			if (isset($this->ResponsiveDesignProps[$intDeviceID])) {
				$sql = "UPDATE SI_SV_SurveyHTML SET 
						FormattedText = {$strHTMLText} 
						, HTMLHeader = {$strEmpty} 
						, HTMLFooter = {$strEmpty} 
						, ImageText = {$strDisplayImage} 
						{$strUpdate}
					WHERE ObjectType = ".otyAnswer." AND DeviceID = {$intDeviceID} AND ObjectID = {$this->QuestionOptionID}";
			}
			else {
				$sql = "INSERT INTO SI_SV_SurveyHTML (SurveyID, ObjectType, ObjectID, DeviceID, FormattedText, HTMLHeader, HTMLFooter, ImageText {$strInsertFields})
					VALUES ({$this->SurveyID}, ".otyAnswer.", {$this->QuestionOptionID}, {$intDeviceID}, 
						{$strHTMLText}, {$strEmpty}, {$strEmpty}, {$strDisplayImage}  {$strInsertValues})";
			}
			@$this->Repository->DataADOConnection->Execute($sql);
		}
	}
	
	function saveOptionActions()
	{
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Estas propiedades están descontinuadas a partir de eForms v6
		//@JAPRDescontinuada en V6
		return;
		//@JAPR
		
		//Se eliminan las acciones anteriores y se almacenan las actuales
		$sql = "DELETE FROM SI_SV_QOptionActions WHERE QuestionID = ".$this->QuestionID." AND OptionText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QOptionActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$arrayOptionActions = array();
		
		//Validamos que las opciones sean unicas
		if(trim($this->StrOptionActions)!="")
		{
			$arrayOptionActions = explode("\r\n", $this->StrOptionActions);
		}
		
		$this->OptionActions = array();
		$count = 0;
		foreach ($arrayOptionActions as $optionKey => $optionValue)
		{
			if(trim($optionValue)!="")
			{
				$sql = "INSERT INTO SI_SV_QOptionActions (".
						"QuestionID".
						",OptionText".
						",SortOrder".
						",ActionText".
						") VALUES (".
						$this->QuestionID.
						",".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName).
						",".$optionKey.
						",".$this->Repository->DataADOConnection->Quote($optionValue).
						")";

				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QOptionActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				$this->OptionActions[$count] = $optionValue;
				$count++;
			}
		}
	}

	//@JAPRDescontinuada, a partir de V6 el grabado se hace sin distinción del tipo de pregunta
	function saveForMultipleChoice()
	{
		$currentDate = date("Y-m-d H:i:s");
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		//@JAPR
		if ($this->isNewObject())
		{
			//Generamos instancia de Question
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

			//Procesamos las acciones
			$this->OptionActions = array();
			
			//Validamos que las opciones sean unicas
			if($mainQuestion->MCInputType==mpcCheckBox && trim($this->StrOptionActions)!="")
			{
				$this->OptionActions = explode("\r\n", $this->StrOptionActions);	
			}

			$this->OptionActions = array_values(array_unique($this->OptionActions));

			$LN = chr(13).chr(10);
			$this->StrOptionActions = implode($LN, $this->OptionActions);
			
			//Vamos a simular parte de las funciones de la clase Question para utilizar funciones de ellas
			$arraySelectOptions = array();
			
			//Validamos que las opciones sean unicas
			if(trim($mainQuestion->StrSelectOptions)!="")
			{
				//Modificamos el atributo ->StrSelectOptions agregandole el nuevo si realmente no existe dicha opcion
				$arraySelectOptions = explode("\r\n", $mainQuestion->StrSelectOptions);
			}
			
			$arraySelectOptionsFlip = array_flip($arraySelectOptions);
			
			$optionName = $this->QuestionOptionName;
			if(isset($arraySelectOptionsFlip[$optionName]))
			{
				//Si existe dicha opcion no hay nada que hacer
				return;
			}
			
			$arraySelectOptions[] = $this->QuestionOptionName;
			$LN = chr(13).chr(10);
			$mainQuestion->StrSelectOptions = implode($LN, $arraySelectOptions);
			
			$sql = "UPDATE SI_SV_Question SET 
					OptionsText = ".$this->Repository->DataADOConnection->Quote($mainQuestion->StrSelectOptions)." 
					WHERE QuestionID = ".$mainQuestion->QuestionID." AND SurveyID = ".$mainQuestion->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
			//Ya no se eliminarán las opciones de respuesta previamente grabadas, ahora se insertará la opción simplemente desde este punto
			//Y guardamos las opciones en la tabla de SI_SV_QAnswers y en las tablas de la HandHeld
			//$mainQuestion->saveSelectOptions();
			//@JAPT
			
			//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
			//Ya no se tiene acceso a modelos ni dimensiones
			$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
			if ($surveyInstance->CreationAdminVersion < esveFormsv6) {
				//Si la pregunta es multidimension entonces se procede a crear la dimension faltante para la nueva opcion
				//La funcion createSelectOptionDims afortunadamente detecta cuales de las opciones todavia no les han creado 
				//su dimension a la opcion por lo tanto no se crearan mas dimensiones de las necesarias solo para aquellas 
				//opciones que las necesiten, y automaticamente inserta los valores de No aplica, 0 y 1 y actualiza tabla de hechos
				if($mainQuestion->IsMultiDimension==1)
				{
					$mainQuestion->createSelectOptionDims();
				}
				else if($mainQuestion->QDisplayMode==dspVertical && $mainQuestion->MCInputType==mpcNumeric && $mainQuestion->IsMultiDimension==0 && $mainQuestion->UseCategoryDimChoice>0)
				{
					$mainQuestion->createCategoryDimension();
				}
				
				//Si es MCInputType>mpcCheckBox entonces tenemos que agregarle el ; al final del campo dim_questionid
				//dentro de la tabla paralela para aquellos que no sean nulos
				if($mainQuestion->MCInputType>mpcCheckBox)
				{
					$sql = "UPDATE ".$mainQuestion->SurveyTable." SET ".$mainQuestion->SurveyField." = "." CONCAT(".$mainQuestion->SurveyField.", ".$this->Repository->DataADOConnection->Quote(";").") 
							WHERE ".$mainQuestion->SurveyField." IS NOT NULL AND TRIM(".$mainQuestion->SurveyField.") <> ".$this->Repository->DataADOConnection->Quote("");
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$mainQuestion->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
			//@JAPR
			
			//Obtenemos el ID de la opcion con la cual se inserto para establecer la propiedad QuestionOptionID
			$sql = "SELECT ConsecutiveID, SortOrder FROM SI_SV_QAnswers 
					WHERE QuestionID = ".$this->QuestionID." AND DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->QuestionOptionID = (int)$aRS->fields["consecutiveid"];
			$this->SortOrder = (int)$aRS->fields["sortorder"];
			
			//Y Actualizamos el Score
			//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
			//@JAPR 2012-10-29: Agregada la información extra para las acciones
			$strAdditionalValues = '';
			if (getMDVersion() >= esvExtendedActionsData) {
				$strAdditionalValues .= ', ActionTitle = '.$this->Repository->DataADOConnection->Quote($this->ActionTitle);
			}
			//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions) {
				$strAdditionalValues .= ', eBavelActionFormID = '.$this->eBavelActionFormID;
			}
			//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
			if (getMDVersion() >= esvHideAnswers) {
				$strAdditionalValues .= ', Hidden = '.(int) !$this->Visible;
			}
			//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				$strAdditionalValues .= ', HTMLText = '.$this->Repository->DataADOConnection->Quote($this->HTMLText);
			}
			if (getMDVersion() >= esvDefaultOption) {
				$strAdditionalValues .= ', DefaultValue = '.$this->Repository->DataADOConnection->Quote($this->DefaultValue);
			}
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strAdditionalValues .= ', LastUserID = '.$_SESSION["PABITAM_UserID"].
					', LastDateID = '.$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR
			
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalValues .= ', CancelAgenda = '.(int) $this->CancelAgenda;
			}
			
			if (getMDVersion() >= esvEditorHTML)
			{
				$strAdditionalValues .= ', EditorType = '.$this->EditorType
				.', HTMLTextDes = '.$this->Repository->DataADOConnection->Quote($this->HTMLTextDes);
			}
			//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
			if (getMDVersion() >= esvActionConditions) {
				$strAdditionalValues .= ', ActionCondition = '.$this->Repository->DataADOConnection->Quote($this->ActionCondition);
			}
			//@JAPR
			$sql = "UPDATE SI_SV_QAnswers SET 
					Score = ".$this->Repository->DataADOConnection->Quote($this->Score)." 
					, ActionText = ".$this->Repository->DataADOConnection->Quote($this->ActionText)." 
					, ResponsibleID = ".$this->ResponsibleID." 
					, CategoryID = ".$this->CategoryID." 
					, DaysDueDate = ".$this->DaysDueDate." 
					, InsertWhen = ".$this->InsertWhen."
					, DisplayImage = ".$this->Repository->DataADOConnection->Quote($this->DisplayImage)." 
					$strAdditionalValues 
				WHERE ConsecutiveID = ".$this->QuestionOptionID." AND QuestionID = ".$this->QuestionID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Volvemos a generar instancia de Question para obtener los arreglos de opciones, indicadores
			//y dimensiones de manera correcta
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			
			//Verificamos si hay un score capturado para esta opcion de pregunta de seleccion multiple
			//que no sea matriz o bien revisamos si ya se crearon indicadores anteriormente
			$existIndicators = false;
			
			//Recorremos las opciones para revisar los indicadores
			foreach ($mainQuestion->SelectOptions as $optionKey=>$optionValue)
			{
				$indicatorid = 0;
				
				if(isset($mainQuestion->QIndicatorIds[$optionKey]))
				{
					$indicatorid = (int)$mainQuestion->QIndicatorIds[$optionKey];
				}
				else 
				{
					$indicatorid = 0;
				}
				
				if(!is_null($indicatorid) && $indicatorid!=0)
				{
					$existIndicators = true;
					break;
				}
			}
			
			if(trim($this->Score)!="" || $existIndicators)
			{
				if($mainQuestion->QDisplayMode != dspMatrix)
				{
					$blnCreateIndicators = false;
					$arrayAnswerOptions = array();
					$strQIndDimIds = "";
			
					$arrayOptions = $mainQuestion->SelectOptions;
					$theQIndicatorIds = $mainQuestion->QIndicatorIds;
					
					//Recorremos las opciones para revisar los indicadores
					foreach ($arrayOptions as $optionKey=>$optionValue)
					{
						$indicatorid = 0;
						
						if(isset($theQIndicatorIds[$optionKey]))
						{
							$indicatorid = (int)$theQIndicatorIds[$optionKey];
						}
						else 
						{
							$indicatorid = 0;
						}
						
						if(is_null($indicatorid)||$indicatorid==0)
						{
							$blnCreateIndicators = true;
							break;
						}
					}
					
					if($blnCreateIndicators==true)
					{
						$mainQuestion->createSelectOptionInds();
					}
				}
			}
			
			//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
			//Estas propiedades están descontinuadas a partir de eForms v6
			//Y posterior a esto procedemos a almacenar las Acciones de dicha opcion
			//$this->saveOptionActions();
			//@JAPR
			
			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
			
			//@JAPR 2013-12-04: Agregada la opción para compartir las opciones de respuesta con otras preguntas
			//Si se trata de la pregunta original, verifica si hay alguna pregunta que comparte sus opciones de respuesta para también insertarla
			//en ellas, para lo cual invoca a este mismo método pero de una instancia nueva actualizando sólo la referencia a la pregunta
			if (getMDVersion() >= esvSimpleChoiceSharing && $mainQuestion->SharedQuestionID <= 0) {
				$sql = "SELECT QuestionID 
					FROM SI_SV_Question 
					WHERE SurveyID = {$mainQuestion->SurveyID} AND SharedQuestionID = {$mainQuestion->QuestionID}";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					while (!$aRS->EOF) {
						$intSharedQuestionID = (int) @$aRS->fields["questionid"];
						if ($intSharedQuestionID <= 0) {
							continue;
						}
						$objSharedQuestion = @BITAMQuestion::NewInstanceWithID($this->Repository, $intSharedQuestionID);
						if (is_null($objSharedQuestion)) {
							continue;
						}
						
						$aQuestionOption = clone($this);
						//Modifica la instancia clonada para que pertenezca a la pregunta compartida
						$aQuestionOption->QuestionOptionID = -1;
						$aQuestionOption->QuestionID = $intSharedQuestionID;
						$aQuestionOption->QTypeID = $objSharedQuestion->QTypeID;
						
						//Limpia las configuraciones que no se desean heredar en las preguntas compartidas, ya que ahí pudiera tener diferente
						//características la pregunta y no necesariamente aplicar, creando basura innecesaria, especialmente dimensiones o indicadores
						$aQuestionOption->Score = "";
						$aQuestionOption->ActionText = '';
						$aQuestionOption->ActionTitle = '';
						$aQuestionOption->eBavelActionFormID = 0;
						$aQuestionOption->Visible = 1;
						$aQuestionOption->CancelAgenda = 0;
						$aQuestionOption->ResponsibleID = -1;
						$aQuestionOption->CategoryID = 0;
						$aQuestionOption->DaysDueDate = 0;
						$aQuestionOption->InsertWhen = ioaChecked;
						//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
						$aQuestionOption->ActionCondition = '';
						//@JAPR
						
						//Invoca al grabado ya como una opción de respuesta de otra pregunta
						$aQuestionOption->save();
						
						//Inserta el mapeo de la opción de respuesta entre las preguntas
						$sql = "INSERT INTO SI_SV_QAnswersMatch (SourceQuestionID, SourceConsecutiveID, SharedQuestionID, SharedConsecutiveID) 
							VALUES ({$mainQuestion->QuestionID}, {$this->QuestionOptionID}, {$objSharedQuestion->QuestionID}, {$aQuestionOption->QuestionOptionID})";
						$this->Repository->DataADOConnection->Execute($sql);
						
						$aRS->MoveNext();
					}
				}
			}
			//@JAPR
		}
		else
		{
			//@JAPR 2013-08-08: Corregido un bug, no estaba validado que no se permitiera renombrar opciones usando nombres ya existentes
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			
			if($this->QuestionOptionName!=$this->QuestionOptionNameBack)
			{
				$arraySelectOptions = array();
				
				//Validamos que las opciones sean unicas
				if(trim($mainQuestion->StrSelectOptions)!="")
				{
					//Obtenemos las opciones para verificar si no se quiere editar la opcion 
					//con el nombre de otra opcion ya existente
					$arraySelectOptions = explode("\r\n", $mainQuestion->StrSelectOptions);
				}
				$arraySelectOptionsFlip = array_flip($arraySelectOptions);
				
				$key = $this->QuestionOptionName;
				
				//Si existe el valor entonces no debera ser modificado
				if(isset($arraySelectOptionsFlip[$key]))
				{
					$this->QuestionOptionName = $this->QuestionOptionNameBack;
					return;
				}
			}
			
			//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
			//@JAPR 2012-10-29: Agregada la información extra para las acciones
			$strAdditionalValues = '';
			if (getMDVersion() >= esvExtendedActionsData) {
				$strAdditionalValues .= ', ActionTitle = '.$this->Repository->DataADOConnection->Quote($this->ActionTitle);
			}
			//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions) {
				$strAdditionalValues .= ', eBavelActionFormID = '.$this->eBavelActionFormID;
			}
			//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
			if (getMDVersion() >= esvHideAnswers) {
				$strAdditionalValues .= ', Hidden = '.(int) !$this->Visible;
			}
			//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				$strAdditionalValues .= ', HTMLText = '.$this->Repository->DataADOConnection->Quote($this->HTMLText);
			}
			if (getMDVersion() >= esvDefaultOption) {
				$strAdditionalValues .= ', DefaultValue = '.$this->Repository->DataADOConnection->Quote($this->DefaultValue);
			}
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strAdditionalValues .= ', LastUserID = '.$_SESSION["PABITAM_UserID"].
					', LastDateID = '.$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR
			
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalValues .= ', CancelAgenda = '.(int) $this->CancelAgenda;
			}
			
			if (getMDVersion() >= esvEditorHTML)
			{
				$strAdditionalValues .= ', EditorType = '.$this->EditorType
				.', HTMLTextDes = '.$this->Repository->DataADOConnection->Quote($this->HTMLTextDes);
			}
			//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
			if (getMDVersion() >= esvActionConditions) {
				$strAdditionalValues .= ', ActionCondition = '.$this->Repository->DataADOConnection->Quote($this->ActionCondition);
			}
			//@JAPR
			$sql = "UPDATE SI_SV_QAnswers SET 
					DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName).", 
					Score = ".$this->Repository->DataADOConnection->Quote($this->Score)." 
					, ActionText = ".$this->Repository->DataADOConnection->Quote($this->ActionText)." 
					, ResponsibleID = ".$this->ResponsibleID." 
					, CategoryID = ".$this->CategoryID." 
					, DaysDueDate = ".$this->DaysDueDate." 
					, InsertWhen = ".$this->InsertWhen."
					, DisplayImage = ".$this->Repository->DataADOConnection->Quote($this->DisplayImage)." 
					$strAdditionalValues 
				WHERE ConsecutiveID = ".$this->QuestionOptionID." AND QuestionID = ".$this->QuestionID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->updateStrSelectOptions(); //selecciona opciones de respuesta y reemplaza en la pregunta

			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

			//Despues de actualizar dicha opcion se verifica si hubo un cambio en su descripcion si es asi es necesario 
			//actualizarlo en la tabla paralela de una pregunta de seleccion multiple, que no sea de catalogo
			if($mainQuestion->QTypeID==qtpMulti && $mainQuestion->UseCatToFillMC==0)
			{
				//Se verifica si hubo cambio en la descripcion de la opcion
				if($this->QuestionOptionName!=$this->QuestionOptionNameBack)
				{
					//Modificamos tabla paralela siempre cuando la entrada de valores sea por checkbox 
					//y no sean campos de captura numericos y string
					if($mainQuestion->MCInputType==mpcCheckBox)
					{
						$this->updateSVSurveyTable();
					}
					
					//Si la pregunta es de tipo MultiDimension entonces debera modificarse el nombre logico de dicha dimension
					if($mainQuestion->IsMultiDimension==1)
					{
						$this->updateOptionNomLogico();
						
						//Verificamos si la pregunta es de entrada numerica y si hay indicador creado para ella 
						//para hacer 
						if($mainQuestion->MCInputType==mpcNumeric && $mainQuestion->IsIndicatorMC==1)
						{
							$this->updateOptionNomLogicoInd();
						}
					}
					else if($mainQuestion->QDisplayMode==dspVertical && $mainQuestion->MCInputType==mpcNumeric && $mainQuestion->IsMultiDimension==0 && $mainQuestion->UseCategoryDimChoice>0)
					{
						$this->updateOptionInElementDim();
					}
				}
			}
			
			//Verificamos si hay un score capturado para esta opcion de pregunta de seleccion multiple
			//que no sea matriz
			if(trim($this->Score)!="")
			{
				if($mainQuestion->QDisplayMode != dspMatrix)
				{
					$blnCreateIndicators = false;
					$arrayAnswerOptions = array();
					$strQIndDimIds = "";
			
					$arrayOptions = $mainQuestion->SelectOptions;
					$theQIndicatorIds = $mainQuestion->QIndicatorIds;

					//Recorremos las opciones para revisar los indicadores
					foreach ($arrayOptions as $optionKey=>$optionValue)
					{
						$indicatorid = 0;
						
						if(isset($theQIndicatorIds[$optionKey]))
						{
							$indicatorid = (int)$theQIndicatorIds[$optionKey];
						}
						else 
						{
							$indicatorid = 0;
						}
			
						if(is_null($indicatorid)||$indicatorid==0)
						{
							$blnCreateIndicators = true;
							break;
						}
					}

					if($blnCreateIndicators==true)
					{
						$mainQuestion->createSelectOptionInds();
					}
				}
			}
			
			//Procesamos las acciones
			$this->OptionActions = array();
			
			//Validamos que las opciones sean unicas
			if($mainQuestion->MCInputType==mpcCheckBox && trim($this->StrOptionActions)!="")
			{
				$this->OptionActions = explode("\r\n", $this->StrOptionActions);	
			}

			$this->OptionActions = array_values(array_unique($this->OptionActions));

			$LN = chr(13).chr(10);
			$this->StrOptionActions = implode($LN, $this->OptionActions);
			
			//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
			//Estas propiedades están descontinuadas a partir de eForms v6
			//Y posterior a esto procedemos a almacenar las Acciones de dicha opcion
			//$this->saveOptionActions();
			//@JAPR
			
			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);

			//@JAPR 2013-12-04: Agregada la opción para compartir las opciones de respuesta con otras preguntas
			//Si se trata de la pregunta original, verifica si hay alguna pregunta que comparte sus opciones de respuesta para también editarla
			//en ellas, para lo cual invoca a este mismo método pero de la instancia correspondiente a la pregunta compartida, actualizando sólo la
			//etiqueta de la pregunta, ya que el resto de las opciones pudieran estar personalizadas en las preguntas compartidas
			if (getMDVersion() >= esvSimpleChoiceSharing && $mainQuestion->SharedQuestionID <= 0) {
				$sql = "SELECT SharedQuestionID AS 'QuestionID', SharedConsecutiveID AS 'ConsecutiveID' 
					FROM SI_SV_QAnswersMatch 
					WHERE SourceQuestionID = {$mainQuestion->QuestionID} AND SourceConsecutiveID = {$this->QuestionOptionID}";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					while (!$aRS->EOF) {
						$intSharedQuestionID = (int) @$aRS->fields["questionid"];
						$intSharedQuestionOptionID = (int) @$aRS->fields["consecutiveid"];
						if ($intSharedQuestionID <= 0 || $intSharedQuestionOptionID <= 0) {
							continue;
						}
						$objSharedQuestion = @BITAMQuestion::NewInstanceWithID($this->Repository, $intSharedQuestionID);
						if (is_null($objSharedQuestion)) {
							continue;
						}
						$aQuestionOption = @BITAMQuestionOption::NewInstanceWithID($this->Repository, $intSharedQuestionOptionID);
						if (is_null($aQuestionOption)) {
							continue;
						}
						
						//Sólo actualiza los datos comunes en todas las preguntas compartidas
						$aQuestionOption->QuestionOptionName = $this->QuestionOptionName;
						
						//Invoca al grabado ya como una opción de respuesta de otra pregunta
						$aQuestionOption->save();
						
						$aRS->MoveNext();
					}
				}
			}
			
			//@JAPR 2014-10-21: Corregido un bug, al editar una opción de respuesta hay que crear su dimensión si es que no la tenía asignada
			//Si la pregunta es multidimension entonces se procede a crear la dimension faltante para la nueva opcion
			//La funcion createSelectOptionDims afortunadamente detecta cuales de las opciones todavia no les han creado 
			//su dimension a la opcion por lo tanto no se crearan mas dimensiones de las necesarias solo para aquellas 
			//opciones que las necesiten, y automaticamente inserta los valores de No aplica, 0 y 1 y actualiza tabla de hechos
			if($mainQuestion->IsMultiDimension==1)
			{
				$mainQuestion->createSelectOptionDims();
			}
			//@JAPR
		}
	}
	
	function updateOptionInElementDim()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		$strOriginalWD = getcwd();

		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");

		//Generamos la instancia de la dimension Element para actualizar valor de la opcion
		//del miembro de dimension
		$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $mainQuestion->ElementDimID);
		
		chdir($strOriginalWD);
		
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		$sql = "UPDATE ".$tableName." SET ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName)." WHERE ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionNameBack);
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	function updateOptionNomLogico()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

		$strOriginalWD = getcwd();

		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");

		$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $this->IndDimID);
		$dimensionName = "Q".$mainQuestion->QuestionNumber."-".$mainQuestion->AttributeName."-".$this->QuestionOptionName;
		$cla_descrip = $this->getDimClaDescripByDimName($dimensionName);
				
		if($cla_descrip!=-1)
		{
			$dimensionName = "SV".$mainQuestion->SurveyID."Q".$mainQuestion->QuestionNumber."-".$mainQuestion->AttributeName."-".$this->QuestionOptionName;
		}
				
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->save();
		chdir($strOriginalWD);
	}
	
	function updateOptionNomLogicoInd()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

		$strOriginalWD = getcwd();

		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");

		$anInstanceOptionInd = BITAMIndicatorKPI::NewIndicatorWithID($this->Repository, $this->IndicatorID);
		$indicatorName = "Q".$mainQuestion->QuestionNumber."-".$mainQuestion->AttributeName."-".$this->QuestionOptionName;
		
		$anInstanceOptionInd->IndicatorName = $indicatorName;
		$anInstanceOptionInd->formato = "#,##0";
		$anInstanceOptionInd->agrupador = "SUM";
		$anInstanceOptionInd->sql_source = "SUM";
		
		$anInstanceOptionInd->save();
		chdir($strOriginalWD);
	}
	
	function getDimClaDescripByDimName($dimensionName)
	{
		$cla_descrip = -1;

		$sql = "SELECT CLA_DESCRIP FROM SI_DESCRIP_ENC WHERE NOM_LOGICO = ".$this->Repository->ADOConnection->Quote($dimensionName);
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$cla_descrip = (int)$aRS->fields["cla_descrip"];
		}
		
		return $cla_descrip;
	}
	
	function updateSVSurveyTable()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
	
		$tablaParalela = $mainQuestion->SurveyTable;
		$questionField = $mainQuestion->SurveyField;
		
		//Primero le agregamos un ; al inicio y final del valor del campo
		$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = CONCAT(".$this->Repository->DataADOConnection->Quote(";").", ".$questionField.", ".$this->Repository->DataADOConnection->Quote(";").")";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$strNewOption = ";".$this->QuestionOptionName.";";
		$strOldOption = ";".$this->QuestionOptionNameBack.";";

		//Reemplazamos el valor anterior por el valor nuevo
		$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = REPLACE(".$questionField.", ".$this->Repository->DataADOConnection->Quote($strOldOption).", ".$this->Repository->DataADOConnection->Quote($strNewOption).")";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Le quitamos el ; al inicio y al final del campo para dejarlo como antes
		$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = SUBSTRING(".$questionField.", 2, (CHAR_LENGTH(".$questionField.")-2))";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	function saveForSimpleChoice()
	{
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		global $form;
		
		$currentDate = date("Y-m-d H:i:s");
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		$intCreationUserID = $_SESSION["PABITAM_UserID"];
		$intLastUserID = $_SESSION["PABITAM_UserID"];
		$strCreationDate = $currentDate;
		$strLastDate = $currentDate;
		$dblCreationVersion = $this->LastModAdminVersion;
		$dblLastVersion = $this->LastModAdminVersion;
		
		//@JAPR 2019-03-12: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Se carga la instancia de la pregunta indistintamente de si es un grabado de una opción nueva o existente, en ambos casos se requerirá
		//Generamos instancia de Question
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		//@JAPR
		if ($this->isNewObject()) {
			$this->CreationAdminVersion = ESURVEY_SERVICE_VERSION;
			
			//Procesamos las acciones
			$this->OptionActions = array();
			
			//Validamos que las opciones sean unicas
			if ($mainQuestion->MCInputType == mpcCheckBox && trim($this->StrOptionActions) != "") {
				$this->OptionActions = explode("\r\n", $this->StrOptionActions);	
			}
			
			$this->OptionActions = array_values(array_unique($this->OptionActions));
			
			$LN = chr(13).chr(10);
			$this->StrOptionActions = implode($LN, $this->OptionActions);
			
			//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
			//Estas propiedades están descontinuadas a partir de eForms v6
			//Y posterior a esto procedemos a almacenar las Acciones de dicha opcion
			//$this->saveOptionActions();
			//@JAPR
			
			//Vamos a simular parte de las funciones de la clase Question para utilizar funciones de ellas
			$this->QTypeID = $mainQuestion->QTypeID;
			
			$arraySelectOptions = array();
			
			//Validamos que las opciones sean unicas
			if (trim($mainQuestion->StrSelectOptions) != "") {
				//Modificamos el atributo ->StrSelectOptions agregandole el nuevo
				$arraySelectOptions = explode("\r\n", $mainQuestion->StrSelectOptions);
			}
			
			//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
			//Se verifica que el nombre de la nueva opción no exista previamente entre los ya registrados, si es así, se le agrega un sufijo consecutivo incremental
			$intNewIdx = 1;
			$strQuestionOptionName = $this->QuestionOptionName;
			$arrUniqueOptions = array_flip($arraySelectOptions);
			while (isset($arrUniqueOptions[$strQuestionOptionName])) {
				$strQuestionOptionName = $this->QuestionOptionName.$intNewIdx++;
			}
			$this->QuestionOptionName = $strQuestionOptionName;
			//@JAPR
			
			$arraySelectOptions[] = $this->QuestionOptionName;
			$LN = chr(13).chr(10);
			$mainQuestion->StrSelectOptions = implode($LN, $arraySelectOptions);
			//Validamos las opciones
			$mainQuestion->validateSelectOptions();
			
			//Y guardamos las opciones en la tabla de SI_SV_QAnswers y en las tablas de la HandHeld
			//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
			//Ya no se eliminarán las opciones de respuesta previamente grabadas, ahora se insertará la opción simplemente desde este punto
			//$mainQuestion->saveSelectOptions();
			//@JAPR
			
			//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
			//Ya no se tiene acceso a modelos ni dimensiones
			$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
			if ($surveyInstance->CreationAdminVersion < esveFormsv6) {
				//Despues de grabar en la tabla SI_SV_Question y SI_SV_QAnswers verificamos se realiza la insercion de los valores que faltan en la dimension
				$strOriginalWD = getcwd();
				
				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				//session_register("BITAM_UserID");
				//session_register("BITAM_UserName");
				//session_register("BITAM_RepositoryName");
				$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
				$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
				$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
				
				require_once("../model_manager/model.inc.php");
				require_once("../model_manager/modeldata.inc.php");
				require_once("../model_manager/modeldimension.inc.php");
				
				$anInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $mainQuestion->IndDimID);
				chdir($strOriginalWD);
				
				//Se agregan los nuevos elementos a la dimension y se eliminan aquellos que ya no estan en uso
				$mainQuestion->updateMassiveDimensionValues($anInstanceDim);
			}
			
			//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
			//@JAPR 2012-10-29: Agregada la información extra para las acciones
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			if (getMDVersion() >= esvExtendedActionsData) {
				$strAdditionalFields .= ', ActionTitle';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->ActionTitle);
			}
			//@JAPR
			//Conchita 2013-04-30 agregado el num de telefono
			if (getMDVersion() >= esvCallList) {
				$strAdditionalFields .= ', phoneNumber';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->phoneNum);
			}
			//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
			if (getMDVersion() >= esvHideAnswers) {
				$strAdditionalFields .= ', Hidden';
				$strAdditionalValues .= ', '.(int) !$this->Visible;
			}
			//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				$strAdditionalFields .= ', HTMLText';
				$strAdditionalValues .= ', '.(string) $this->Repository->DataADOConnection->Quote($this->HTMLText);
			}
			
			if (getMDVersion() >= esvDefaultOption) {
				$strAdditionalFields .= ', DefaultValue';
				$strAdditionalValues .= ', '.(string) $this->Repository->DataADOConnection->Quote($this->DefaultValue);
			}
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strAdditionalFields .= ',CreationUserID,LastUserID,CreationDateID,LastDateID,CreationVersion,LastVersion';
				$strAdditionalValues .= ','.$intCreationUserID.','.$intLastUserID.
					','.$this->Repository->DataADOConnection->DBTimeStamp($strCreationDate).
					','.$this->Repository->DataADOConnection->DBTimeStamp($strLastDate).
					','.$dblCreationVersion.','.$dblLastVersion;
			}
			//@JAPR
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalFields .= ', CancelAgenda';
				$strAdditionalValues .= ', '.(int) $this->CancelAgenda;
			}
			if (getMDVersion() >= esvEditorHTML) {
				$strAdditionalFields .= ', EditorType';
				$strAdditionalValues .= ', '.$this->EditorType;
			}
			//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
			//$form sólo estará asignada si se trata de un proceso de generación de forma de desarrollo iniciado por processCopyForm.php
			if (getMDVersion() >= esvCopyForms) {
				$strAdditionalFields .= ', SourceID';
				if (isset($form) && $form->ActionType == devAction) {
					$strAdditionalValues .= ', '.$this->CopiedOptionID;
				}
				else {
					$strAdditionalValues .= ', 0';
				}
			}
			//@JAPR
			$sql = "INSERT INTO SI_SV_QAnswers (".
					"QuestionID".
					",SortOrder".
					",DisplayText".
					",NextSection".
					",IndDimID".
					",IndicatorID".
					",Score".
					",ActionText".
					",ResponsibleID".
					",CategoryID".
					",DaysDueDate".
					",InsertWhen".
					",DisplayImage".
					$strAdditionalFields.
					") VALUES (".
					$this->QuestionID.
					",".(count($arraySelectOptions) -1).
					",".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName).
					",".$this->NextQuestion.
					",0".
					",0".
					",".$this->Repository->DataADOConnection->Quote($this->Score).
					",".$this->Repository->DataADOConnection->Quote($this->ActionText).
					",".$this->ResponsibleID.
					",".$this->CategoryID.
					",".$this->DaysDueDate.
					",".$this->InsertWhen.
					",".$this->Repository->DataADOConnection->Quote($this->DisplayImage).
					$strAdditionalValues.
					")";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Inserting question option: {$sql}", 2, 0, "color:blue;");
			}
			if ($this->Repository->DataADOConnection->Execute($sql) === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			//Obtenemos el ID de la opcion con la cual se inserto para establecer la propiedad QuestionOptionID
			$sql = "SELECT ConsecutiveID, SortOrder 
				FROM SI_SV_QAnswers 
				WHERE QuestionID = ".$this->QuestionID." AND DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF) {
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->QuestionOptionID = (int) @$aRS->fields["consecutiveid"];
			$this->SortOrder = (int) @$aRS->fields["sortorder"];
			
			$sql = "UPDATE SI_SV_Question SET 
					OptionsText = ".$this->Repository->DataADOConnection->Quote($mainQuestion->StrSelectOptions)." 
				WHERE QuestionID = ".$mainQuestion->QuestionID." AND SurveyID = ".$mainQuestion->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			//@JAPR 2013-12-04: Agregada la opción para compartir las opciones de respuesta con otras preguntas
			//Si se trata de la pregunta original, verifica si hay alguna pregunta que comparte sus opciones de respuesta para también insertarla
			//en ellas, para lo cual invoca a este mismo método pero de una instancia nueva actualizando sólo la referencia a la pregunta
			if (getMDVersion() >= esvSimpleChoiceSharing && $mainQuestion->SharedQuestionID <= 0) {
				$sql = "SELECT QuestionID 
					FROM SI_SV_Question 
					WHERE SurveyID = {$mainQuestion->SurveyID} AND SharedQuestionID = {$mainQuestion->QuestionID}";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					while (!$aRS->EOF) {
						$intSharedQuestionID = (int) @$aRS->fields["questionid"];
						if ($intSharedQuestionID <= 0) {
							continue;
						}
						$objSharedQuestion = @BITAMQuestion::NewInstanceWithID($this->Repository, $intSharedQuestionID);
						if (is_null($objSharedQuestion)) {
							continue;
						}
						
						$aQuestionOption = clone($this);
						//Modifica la instancia clonada para que pertenezca a la pregunta compartida
						$aQuestionOption->QuestionOptionID = -1;
						$aQuestionOption->QuestionID = $intSharedQuestionID;
						$aQuestionOption->QTypeID = $objSharedQuestion->QTypeID;
						
						//Limpia las configuraciones que no se desean heredar en las preguntas compartidas, ya que ahí pudiera tener diferente
						//características la pregunta y no necesariamente aplicar, creando basura innecesaria, especialmente dimensiones o indicadores
						$aQuestionOption->Score = "";
						$aQuestionOption->ActionText = '';
						$aQuestionOption->ActionTitle = '';
						$aQuestionOption->eBavelActionFormID = 0;
						$aQuestionOption->Visible = 1;
						$aQuestionOption->CancelAgenda = 0;
						$aQuestionOption->ResponsibleID = -1;
						$aQuestionOption->CategoryID = 0;
						$aQuestionOption->DaysDueDate = 0;
						$aQuestionOption->InsertWhen = ioaChecked;
						//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
						$aQuestionOption->ActionCondition = '';
						//@JAPR
						
						//Invoca al grabado ya como una opción de respuesta de otra pregunta
						$aQuestionOption->save();
						
						//Inserta el mapeo de la opción de respuesta entre las preguntas
						$sql = "INSERT INTO SI_SV_QAnswersMatch (SourceQuestionID, SourceConsecutiveID, SharedQuestionID, SharedConsecutiveID) 
							VALUES ({$mainQuestion->QuestionID}, {$this->QuestionOptionID}, {$objSharedQuestion->QuestionID}, {$aQuestionOption->QuestionOptionID})";
						$this->Repository->DataADOConnection->Execute($sql);
						
						$aRS->MoveNext();
					}
				}
			}
			//@JAPR
		}
		else {
			if($this->QuestionOptionName!=$this->QuestionOptionNameBack)
			{
				$arraySelectOptions = array();
				
				//Validamos que las opciones sean unicas
				if(trim($mainQuestion->StrSelectOptions)!="")
				{
					//Obtenemos las opciones para verificar si no se quiere editar la opcion 
					//con el nombre de otra opcion ya existente
					$arraySelectOptions = explode("\r\n", $mainQuestion->StrSelectOptions);
				}
				$arraySelectOptionsFlip = array_flip($arraySelectOptions);
				
				$key = $this->QuestionOptionName;
				
				//Si existe el valor entonces no debera ser modificado
				if(isset($arraySelectOptionsFlip[$key]))
				{
					$this->QuestionOptionName = $this->QuestionOptionNameBack;
					return;
				}
			}
			
			//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
			//@JAPR 2012-10-29: Agregada la información extra para las acciones
			$strAdditionalValues = '';
			if (getMDVersion() >= esvExtendedActionsData) {
				$strAdditionalValues .= ', ActionTitle = '.$this->Repository->DataADOConnection->Quote($this->ActionTitle);
			}
			if (getMDVersion() >= esvCallList) {
				$strAdditionalValues .= ', phoneNumber = '.$this->Repository->DataADOConnection->Quote($this->phoneNum);
			}
			//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions) {
				$strAdditionalValues .= ', eBavelActionFormID = '.$this->eBavelActionFormID;
			}
			//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
			if (getMDVersion() >= esvHideAnswers) {
				$strAdditionalValues .= ', Hidden = '.(int) !$this->Visible;
			}
			//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				$strAdditionalValues .= ', HTMLText = '.$this->Repository->DataADOConnection->Quote($this->HTMLText);
			}
			if (getMDVersion() >= esvDefaultOption) {
				$strAdditionalValues .= ', DefaultValue = '.$this->Repository->DataADOConnection->Quote($this->DefaultValue);
			}
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strAdditionalValues .= ', LastUserID = '.$_SESSION["PABITAM_UserID"].
					', LastDateID = '.$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR
			
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalValues .= ', CancelAgenda = '.(int) $this->CancelAgenda;
			}
			
			if (getMDVersion() >= esvEditorHTML)
			{
				$strAdditionalValues .= ', EditorType = '.$this->EditorType
				.', HTMLTextDes = '.$this->Repository->DataADOConnection->Quote($this->HTMLTextDes);
			}
			//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
			if (getMDVersion() >= esvActionConditions) {
				$strAdditionalValues .= ', ActionCondition = '.$this->Repository->DataADOConnection->Quote($this->ActionCondition);
			}
			//@JAPR
			$sql = "UPDATE SI_SV_QAnswers SET 
					NextSection = ".$this->NextQuestion.", 
					DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName).", 
					Score = ".$this->Repository->DataADOConnection->Quote($this->Score)." 
					, ActionText = ".$this->Repository->DataADOConnection->Quote($this->ActionText)." 
					, ResponsibleID = ".$this->ResponsibleID." 
					, CategoryID = ".$this->CategoryID." 
					, DaysDueDate = ".$this->DaysDueDate."
					, DisplayImage = ".$this->Repository->DataADOConnection->Quote($this->DisplayImage)."  
					$strAdditionalValues 
				WHERE ConsecutiveID = ".$this->QuestionOptionID." AND QuestionID = ".$this->QuestionID;
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Updating question option: {$sql}", 2, 0, "color:blue;");
			}
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->updateStrSelectOptions();

			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			
			//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
			/*
			//Despues de actualizar dicha opcion se verifica si hubo un cambio en su descripcion
			//si es asi es necesario actualizarlo en el catalogo y en la tabla paralela siempre y cuando se trate
			//de una pregunta de seleccion simple que no utilice catalogo
			if($mainQuestion->QTypeID==qtpSingle && $mainQuestion->UseCatalog==0)
			{
				if($this->QuestionOptionName!=$this->QuestionOptionNameBack)
				{
					$strOriginalWD = getcwd();
					
					//@JAPR 2015-02-09: Agregado soporte para php 5.6
					//session_register("BITAM_UserID");
					//session_register("BITAM_UserName");
					//session_register("BITAM_RepositoryName");
					$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
					$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
					$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
				
					require_once("../model_manager/model.inc.php");
					require_once("../model_manager/modeldata.inc.php");
					require_once("../model_manager/modeldimension.inc.php");
				
					$anInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $mainQuestion->IndDimID);
					chdir($strOriginalWD);
					
					$this->updateDimensionValueInQuestion($anInstanceDim);
				}
			}
			
			//Verificamos si hay un score capturado para esta opcion de pregunta de seleccion simple
			if(trim($this->Score)!="")
			{
				if($mainQuestion->QDisplayMode <> dspMatrix)
				{
					//Y verificamos si para esta pregunta de selección simple no se ha creado el indicador anteriormente
					if(is_null($mainQuestion->IndicatorID) || $mainQuestion->IndicatorID==0)
					{
						$mainQuestion->createIndicatorKPI();
					}
				}
			}
			*/
			
			//Procesamos las acciones
			$this->OptionActions = array();
			
			//Validamos que las opciones sean unicas
			if($mainQuestion->MCInputType==mpcCheckBox && trim($this->StrOptionActions)!="")
			{
				$this->OptionActions = explode("\r\n", $this->StrOptionActions);	
			}
			
			$this->OptionActions = array_values(array_unique($this->OptionActions));
			
			$LN = chr(13).chr(10);
			$this->StrOptionActions = implode($LN, $this->OptionActions);
			
			//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
			//Estas propiedades están descontinuadas a partir de eForms v6
			//Y posterior a esto procedemos a almacenar las Acciones de dicha opcion
			//$this->saveOptionActions();
			//@JAPR
			
			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
			
			//@JAPR 2013-12-04: Agregada la opción para compartir las opciones de respuesta con otras preguntas
			//Si se trata de la pregunta original, verifica si hay alguna pregunta que comparte sus opciones de respuesta para también editarla
			//en ellas, para lo cual invoca a este mismo método pero de la instancia correspondiente a la pregunta compartida, actualizando sólo la
			//etiqueta de la pregunta, ya que el resto de las opciones pudieran estar personalizadas en las preguntas compartidas
			if (getMDVersion() >= esvSimpleChoiceSharing && $mainQuestion->SharedQuestionID <= 0) {
				$sql = "SELECT SharedQuestionID AS 'QuestionID', SharedConsecutiveID AS 'ConsecutiveID' 
					FROM SI_SV_QAnswersMatch 
					WHERE SourceQuestionID = {$mainQuestion->QuestionID} AND SourceConsecutiveID = {$this->QuestionOptionID}";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					while (!$aRS->EOF) {
						$intSharedQuestionID = (int) @$aRS->fields["questionid"];
						$intSharedQuestionOptionID = (int) @$aRS->fields["consecutiveid"];
						if ($intSharedQuestionID <= 0 || $intSharedQuestionOptionID <= 0) {
							continue;
						}
						$objSharedQuestion = @BITAMQuestion::NewInstanceWithID($this->Repository, $intSharedQuestionID);
						if (is_null($objSharedQuestion)) {
							continue;
						}
						$aQuestionOption = @BITAMQuestionOption::NewInstanceWithID($this->Repository, $intSharedQuestionOptionID);
						if (is_null($aQuestionOption)) {
							continue;
						}
						
						//Sólo actualiza los datos comunes en todas las preguntas compartidas
						$aQuestionOption->QuestionOptionName = $this->QuestionOptionName;
						
						//Invoca al grabado ya como una opción de respuesta de otra pregunta
						$aQuestionOption->save();
						
						$aRS->MoveNext();
					}
				}
			}
			//@JAPR
		}
		
		//@JAPR 2019-03-12: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if ( $this->QTypeID == qtpSketchPlus ) {
			//Cuando se graba una opción de respuesta de pregunta Sketch+, se debe verificar si hay o no una sección asociada, y en caso de existir invocar a la generación de las preguntas
			//de apoyo automáticas de dicha sección en caso de que hubieran sido eliminadas
			if ( $this->NextQuestion ) {
				$objSection = BITAMSection::NewInstanceWithID($this->Repository, $this->NextQuestion);
				if ( $objSection ) {
					$objSection->createSketchDataQuestions();
				}
			}
		}
		//@JAPR
	}
	
	//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
	/* Graba la opción de respuesta para algún tipo de pregunta que no es Simple o Múltiple choice (o sus derivados como la Call-list). Originalmente usado para el tipo de pregunta
	Update Data, en la cual se generan opciones de respuesta automáticas que se graban como de preguntas Simple choice o Múltiple choice, pero que no son una entidad independiente, no
	pueden cambiar su valor, y no deben incrementar la versión de la forma donde están asociadas, ya que se graban única y exclusivamente al momento de grabar la pregunta a la que pertenecen
	*/
	function saveForOtherQuestions() {
		$currentDate = date("Y-m-d H:i:s");
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		$intCreationUserID = $_SESSION["PABITAM_UserID"];
		$intLastUserID = $_SESSION["PABITAM_UserID"];
		$strCreationDate = $currentDate;
		$strLastDate = $currentDate;
		$dblCreationVersion = $this->LastModAdminVersion;
		$dblLastVersion = $this->LastModAdminVersion;
		
		//Generamos instancia de Question
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		if ($this->isNewObject()) {
			$this->CreationAdminVersion = ESURVEY_SERVICE_VERSION;
			
			//Vamos a simular parte de las funciones de la clase Question para utilizar funciones de ellas
			$this->QTypeID = $mainQuestion->QTypeID;
			
			$arraySelectOptions = array();
			
			//Validamos que las opciones sean unicas
			if (trim($mainQuestion->StrSelectOptions) != "") {
				//Modificamos el atributo ->StrSelectOptions agregandole el nuevo
				$arraySelectOptions = explode("\r\n", $mainQuestion->StrSelectOptions);
			}
			
			//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
			//Se verifica que el nombre de la nueva opción no exista previamente entre los ya registrados, si es así, se le agrega un sufijo consecutivo incremental
			$intNewIdx = 1;
			$strQuestionOptionName = $this->QuestionOptionName;
			$arrUniqueOptions = array_flip($arraySelectOptions);
			while (isset($arrUniqueOptions[$strQuestionOptionName])) {
				$strQuestionOptionName = $this->QuestionOptionName.$intNewIdx++;
			}
			$this->QuestionOptionName = $strQuestionOptionName;
			//@JAPR
			
			$arraySelectOptions[] = $this->QuestionOptionName;
			$LN = chr(13).chr(10);
			$mainQuestion->StrSelectOptions = implode($LN, $arraySelectOptions);
			//Validamos las opciones
			$mainQuestion->validateSelectOptions();
			
			//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
			//@JAPR 2012-10-29: Agregada la información extra para las acciones
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			if (getMDVersion() >= esvExtendedActionsData) {
				$strAdditionalFields .= ', ActionTitle';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->ActionTitle);
			}
			//@JAPR
			//Conchita 2013-04-30 agregado el num de telefono
			if (getMDVersion() >= esvCallList) {
				$strAdditionalFields .= ', phoneNumber';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->phoneNum);
			}
			//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
			if (getMDVersion() >= esvHideAnswers) {
				$strAdditionalFields .= ', Hidden';
				$strAdditionalValues .= ', '.(int) !$this->Visible;
			}
			//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				$strAdditionalFields .= ', HTMLText';
				$strAdditionalValues .= ', '.(string) $this->Repository->DataADOConnection->Quote($this->HTMLText);
			}
			
			if (getMDVersion() >= esvDefaultOption) {
				$strAdditionalFields .= ', DefaultValue';
				$strAdditionalValues .= ', '.(string) $this->Repository->DataADOConnection->Quote($this->DefaultValue);
			}
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strAdditionalFields .= ',CreationUserID,LastUserID,CreationDateID,LastDateID,CreationVersion,LastVersion';
				$strAdditionalValues .= ','.$intCreationUserID.','.$intLastUserID.
					','.$this->Repository->DataADOConnection->DBTimeStamp($strCreationDate).
					','.$this->Repository->DataADOConnection->DBTimeStamp($strLastDate).
					','.$dblCreationVersion.','.$dblLastVersion;
			}
			//@JAPR
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalFields .= ', CancelAgenda';
				$strAdditionalValues .= ', '.(int) $this->CancelAgenda;
			}
			if (getMDVersion() >= esvEditorHTML) {
				$strAdditionalFields .= ', EditorType';
				$strAdditionalValues .= ', '.$this->EditorType;
			}
			$sql = "INSERT INTO SI_SV_QAnswers (".
					"QuestionID".
					",SortOrder".
					",DisplayText".
					",NextSection".
					",IndDimID".
					",IndicatorID".
					",Score".
					",ActionText".
					",ResponsibleID".
					",CategoryID".
					",DaysDueDate".
					",InsertWhen".
					",DisplayImage".
					$strAdditionalFields.
					") VALUES (".
					$this->QuestionID.
					",".(count($arraySelectOptions) -1).
					",".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName).
					",".$this->NextQuestion.
					",0".
					",0".
					",".$this->Repository->DataADOConnection->Quote($this->Score).
					",".$this->Repository->DataADOConnection->Quote($this->ActionText).
					",".$this->ResponsibleID.
					",".$this->CategoryID.
					",".$this->DaysDueDate.
					",".$this->InsertWhen.
					",".$this->Repository->DataADOConnection->Quote($this->DisplayImage).
					$strAdditionalValues.
					")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Obtenemos el ID de la opcion con la cual se inserto para establecer la propiedad QuestionOptionID
			$sql = "SELECT ConsecutiveID, SortOrder 
				FROM SI_SV_QAnswers 
				WHERE QuestionID = ".$this->QuestionID." AND DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF) {
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->QuestionOptionID = (int) @$aRS->fields["consecutiveid"];
			$this->SortOrder = (int) @$aRS->fields["sortorder"];
			
			$sql = "UPDATE SI_SV_Question SET 
					OptionsText = ".$this->Repository->DataADOConnection->Quote($mainQuestion->StrSelectOptions)." 
				WHERE QuestionID = ".$mainQuestion->QuestionID." AND SurveyID = ".$mainQuestion->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else {
			if($this->QuestionOptionName!=$this->QuestionOptionNameBack)
			{
				$arraySelectOptions = array();
				
				//Validamos que las opciones sean unicas
				if(trim($mainQuestion->StrSelectOptions)!="")
				{
					//Obtenemos las opciones para verificar si no se quiere editar la opcion 
					//con el nombre de otra opcion ya existente
					$arraySelectOptions = explode("\r\n", $mainQuestion->StrSelectOptions);
				}
				$arraySelectOptionsFlip = array_flip($arraySelectOptions);
				
				$key = $this->QuestionOptionName;
				
				//Si existe el valor entonces no debera ser modificado
				if(isset($arraySelectOptionsFlip[$key]))
				{
					$this->QuestionOptionName = $this->QuestionOptionNameBack;
					return;
				}
			}
			
			//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
			//@JAPR 2012-10-29: Agregada la información extra para las acciones
			$strAdditionalValues = '';
			if (getMDVersion() >= esvExtendedActionsData) {
				$strAdditionalValues .= ', ActionTitle = '.$this->Repository->DataADOConnection->Quote($this->ActionTitle);
			}
			if (getMDVersion() >= esvCallList) {
				$strAdditionalValues .= ', phoneNumber = '.$this->Repository->DataADOConnection->Quote($this->phoneNum);
			}
			//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions) {
				$strAdditionalValues .= ', eBavelActionFormID = '.$this->eBavelActionFormID;
			}
			//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
			if (getMDVersion() >= esvHideAnswers) {
				$strAdditionalValues .= ', Hidden = '.(int) !$this->Visible;
			}
			//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				$strAdditionalValues .= ', HTMLText = '.$this->Repository->DataADOConnection->Quote($this->HTMLText);
			}
			if (getMDVersion() >= esvDefaultOption) {
				$strAdditionalValues .= ', DefaultValue = '.$this->Repository->DataADOConnection->Quote($this->DefaultValue);
			}
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strAdditionalValues .= ', LastUserID = '.$_SESSION["PABITAM_UserID"].
					', LastDateID = '.$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR
			
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalValues .= ', CancelAgenda = '.(int) $this->CancelAgenda;
			}
			
			if (getMDVersion() >= esvEditorHTML)
			{
				$strAdditionalValues .= ', EditorType = '.$this->EditorType
				.', HTMLTextDes = '.$this->Repository->DataADOConnection->Quote($this->HTMLTextDes);
			}
			//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
			if (getMDVersion() >= esvActionConditions) {
				$strAdditionalValues .= ', ActionCondition = '.$this->Repository->DataADOConnection->Quote($this->ActionCondition);
			}
			//@JAPR
			$sql = "UPDATE SI_SV_QAnswers SET 
					NextSection = ".$this->NextQuestion.", 
					DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName).", 
					Score = ".$this->Repository->DataADOConnection->Quote($this->Score)." 
					, ActionText = ".$this->Repository->DataADOConnection->Quote($this->ActionText)." 
					, ResponsibleID = ".$this->ResponsibleID." 
					, CategoryID = ".$this->CategoryID." 
					, DaysDueDate = ".$this->DaysDueDate."
					, DisplayImage = ".$this->Repository->DataADOConnection->Quote($this->DisplayImage)."  
					$strAdditionalValues 
				WHERE ConsecutiveID = ".$this->QuestionOptionID." AND QuestionID = ".$this->QuestionID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->updateStrSelectOptions();
			
			//Actualizar datos de modificacion de la encuesta (ver comentario de la función, para este tipo de opciones NO se actualiza versión pues no se graban independientemente)
			//BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
		}
	}
	//@JAPR
	
	function updateStrSelectOptions()
	{
		$arrayOptions = array();
		$LN = chr(13).chr(10);

		$sql = "SELECT DisplayText FROM SI_SV_QAnswers WHERE QuestionID = ".$this->QuestionID." ORDER BY SortOrder";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$arrayOptions[] = $aRS->fields["displaytext"];

			$aRS->MoveNext();
		}
		
		$strSelectOptions = implode($LN, $arrayOptions);
		
		//Actualizamos StrSelectOptions
		$sql = "UPDATE SI_SV_Question SET OptionsText = ".$this->Repository->DataADOConnection->Quote($strSelectOptions)." 
				WHERE QuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}

	function updateDimensionValueInQuestion($anInstanceModelDim)
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

		//Se actualiza el valor de la dimension en el catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldKey = $tableName."KEY";
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		//Verificamos primero si la opcion no se encuentra ya en la tabla de dimension
		//si es asi no es necesaria agregarla en el catalogo porque ya existe
		$sqlExist = "SELECT ".$fieldDescription." FROM ".$tableName." WHERE ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
		
		$aRSExist = $this->Repository->DataADOConnection->Execute($sqlExist);
		
		if ($aRSExist === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlExist);
		}
		
		//Si no existe dicho valor en el catalogo entonces si se procede a almacenar ese valor
		if($aRSExist->EOF)
		{
			$sql = "UPDATE ".$tableName." SET ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName)." 
					WHERE ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionNameBack);
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Se actualiza el valor de la dimension en la tabla paralela
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
		
		$tablaParalela = $mainQuestion->SurveyTable;
		$questionField = $mainQuestion->SurveyField;
		
		$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName)." 
				WHERE ".$questionField." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionNameBack);
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}

	function remove()
	{
		//@JAPR 2013-05-07: Agregado el tipo de pregunta CallList
		//@JAPR 2015-07-18: Para las formas creadas en v6, no hay diferencia entre las opciones de simple choice y múltiple choice, se usa un mismo método
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
		if ($this->QTypeID==qtpSingle || $this->QTypeID==qtpCallList || $surveyInstance->CreationAdminVersion >= esveFormsv6) {
			$this->removeForSimpleChoice();
		}
		else if($this->QTypeID==qtpMulti)
		{
			$this->removeForMultipleChoice();
		}

		return $this;
	}
	
	function removeForMultipleChoice()
	{
		//Vamos a simular parte de las funciones de la clase Question para utilizar funciones de ellas
		//Generamos instancia de Question
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		$arraySelectOptions = array();
		
		//Validamos que las opciones sean unicas
		if(trim($mainQuestion->StrSelectOptions)!="")
		{
			//Modificamos el atributo ->StrSelectOptions haciendo nulo aquel que se va a eliminar
			$arraySelectOptions = explode("\r\n", $mainQuestion->StrSelectOptions);
		}
		$arraySelectOptionsFlip = array_flip($arraySelectOptions);
		unset($arraySelectOptionsFlip[$this->QuestionOptionName]);
		$arraySelectOptions = array_flip($arraySelectOptionsFlip);
		$arraySelectOptions = array_values($arraySelectOptions);
		
		//Obtenemos nuevo valor de ->StrSelectOptions
		$LN = chr(13).chr(10);
		$mainQuestion->StrSelectOptions = implode($LN, $arraySelectOptions);
		
		$sql = "UPDATE SI_SV_Question SET 
				OptionsText = ".$this->Repository->DataADOConnection->Quote($mainQuestion->StrSelectOptions)." 
				WHERE QuestionID = ".$mainQuestion->QuestionID." AND SurveyID = ".$mainQuestion->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
		//En este caso, la opción eliminada se remueve de la tabla de mapeos. El resto de las opciones van a cambiar de OptionID dentro de la
		//función saveSelectOptions, sin embargo en dicha función se hará un UPDATE a la tabla de mapeos del viejo ID al nuevo ID
		$sql = "DELETE FROM SI_SV_SurveyAnswerActionFieldsMap WHERE QuestionOptionID = ".$this->QuestionOptionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false) {
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
		//Ya no se eliminarán las opciones de respuesta previamente grabadas, ahora se insertará la opción simplemente desde este punto
		//Y guardamos las opciones en la tabla de SI_SV_QAnswers y en las tablas de la HandHeld
		//$mainQuestion->saveSelectOptions();
		//@JAPR
		
		//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
		//Ya no se tiene acceso a modelos ni dimensiones
		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
		if ($surveyInstance->CreationAdminVersion < esveFormsv6) {
			//Eliminamos la dimension si dicha opcion pertenece a una pregunta multidimension
			if($mainQuestion->IsMultiDimension==1)
			{
				//Eliminamos dimensiones
				$this->removeSelectOptionDim();
				
				//Eliminamos indicadores
				$this->removeSelectOptionInd();
			}
			
			//Ahora procedemos a actualizar la tabla de hechos para eliminar aquella opcion
			//que se elimino siempre y cuando dicha pregunta no contenga campos de captura numericos o string
			if($mainQuestion->MCInputType==mpcCheckBox)
			{
				$tablaParalela = $mainQuestion->SurveyTable;
				$questionField = $mainQuestion->SurveyField;
				
				//Primero le agregamos un ; al inicio y final del valor del campo
				$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = CONCAT(".$this->Repository->DataADOConnection->Quote(";").", ".$questionField.", ".$this->Repository->DataADOConnection->Quote(";").")";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$strNew = ";";
				$strOld = ";".$this->QuestionOptionName.";";
				
				//Reemplazamos el valor de la opcion eliminada por el valor de ;
				$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = REPLACE(".$questionField.", ".$this->Repository->DataADOConnection->Quote($strOld).", ".$this->Repository->DataADOConnection->Quote($strNew).")";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Le quitamos el ; al inicio y al final del campo para dejarlo como antes pero con la opcion eliminada
				$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = SUBSTRING(".$questionField.", 2, (CHAR_LENGTH(".$questionField.")-2))";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		//@JAPR

		//Eliminamos las acciones relacionadas con la opcion que se elimino
		$sql = "DELETE FROM SI_SV_QOptionActions WHERE QuestionID = ".$this->QuestionID." AND OptionText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QOptionActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Aunque en teoria no debe haber ShowQuestions para las preguntas de Multiple Opcion
		//Como quiera eliminamos los showquestions relacionados con la opcion que se elimino
		$sql = "DELETE FROM SI_SV_ShowQuestions WHERE QuestionID = ".$this->QuestionID." AND ConsecutiveID = ".$this->QuestionOptionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Actualizar datos de modificacion de la encuesta
		BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
		
		//@JAPR 2013-12-05: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		//Si se trata de la pregunta original, verifica si hay alguna pregunta que comparte sus opciones de respuesta para también editarla
		//en ellas, para lo cual invoca a este mismo método pero de la instancia correspondiente a la pregunta compartida, actualizando sólo la
		//etiqueta de la pregunta, ya que el resto de las opciones pudieran estar personalizadas en las preguntas compartidas
		if (getMDVersion() >= esvSimpleChoiceSharing && $mainQuestion->SharedQuestionID <= 0) {
			$sql = "SELECT SharedQuestionID AS 'QuestionID', SharedConsecutiveID AS 'ConsecutiveID' 
				FROM SI_SV_QAnswersMatch 
				WHERE SourceQuestionID = {$mainQuestion->QuestionID} AND SourceConsecutiveID = {$this->QuestionOptionID}";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				while (!$aRS->EOF) {
					$intSharedQuestionID = (int) @$aRS->fields["questionid"];
					$intSharedQuestionOptionID = (int) @$aRS->fields["consecutiveid"];
					if ($intSharedQuestionID <= 0 || $intSharedQuestionOptionID <= 0) {
						continue;
					}
					$objSharedQuestion = @BITAMQuestion::NewInstanceWithID($this->Repository, $intSharedQuestionID);
					if (is_null($objSharedQuestion)) {
						continue;
					}
					$aQuestionOption = @BITAMQuestionOption::NewInstanceWithID($this->Repository, $intSharedQuestionOptionID);
					if (is_null($aQuestionOption)) {
						continue;
					}
					
					//Invoca al borrado ya como una opción de respuesta de otra pregunta
					@$aQuestionOption->remove();
					
					//Elimina el mapeo de esta opción de respuesta en la pregunta compartida
					$sql = "DELETE FROM SI_SV_QAnswersMatch WHERE SharedQuestionID = {$intSharedQuestionID} AND SharedConsecutiveID = {$intSharedQuestionOptionID}";
					$this->Repository->DataADOConnection->Execute($sql);
					
					$aRS->MoveNext();
				}
			}
		}
		//@JAPR
	}

	function removeSelectOptionDim()
	{
		//Generamos instancia de Question
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

		//Verificamos que sea multiple opcion la pregunta, que sea multidimension y tenga un valor valido el atributo IndDimID
		//En este caso no entra el caso de la pregunta Category Dimension ya que la opcion no tiene valor en el campo IndDimID
		//Y no es multidimension
		if($this->QTypeID==qtpMulti && $mainQuestion->IsMultiDimension==1 && !is_null($this->IndDimID) && $this->IndDimID>0)
		{
			$strOriginalWD = getcwd();
			//Eliminar una dimension
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			require_once("../model_manager/modeldata.inc.php");

			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $this->IndDimID);
			$anInstanceModelDim->Dimension->remove();

			chdir($strOriginalWD);
		}
	}
	
	function removeSelectOptionInd()
	{
		//Generamos instancia de Question
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

		//Verificamos que sea multiple opcion la pregunta, que sea multidimension y tenga marcado
		//las entradas numericas y que haya indicador definidp para dicha opcion
		//En este caso tampoco entra la Category Dimension ya que no tiene indicador definido en la opcion
		if(($this->QTypeID==qtpMulti && $mainQuestion->IsMultiDimension==1 && $mainQuestion->MCInputType==mpcNumeric && $mainQuestion->IsIndicatorMC==1 && !is_null($this->IndicatorID) && $this->IndicatorID>0)
			|| ($this->QTypeID==qtpMulti && !is_null($this->IndicatorID) && $this->IndicatorID>0))
		{
			$strOriginalWD = getcwd();
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager

			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/model.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			require_once("../model_manager/indicatorkpi.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			
			$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($this->Repository, $this->IndicatorID);
			$anInstanceIndicator->remove();

			chdir($strOriginalWD);
		}
	}

	function removeForSimpleChoice() {
		//Vamos a simular parte de las funciones de la clase Question para utilizar funciones de ellas
		//Generamos instancia de Question
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		$arraySelectOptions = array();
		
		//Validamos que las opciones sean unicas
		if(trim($mainQuestion->StrSelectOptions)!="")
		{
			//Modificamos el atributo ->StrSelectOptions haciendo nulo aquel que se va a eliminar
			$arraySelectOptions = explode("\r\n", $mainQuestion->StrSelectOptions);
		}

		$arraySelectOptionsFlip = array_flip($arraySelectOptions);
		unset($arraySelectOptionsFlip[$this->QuestionOptionName]);
		$arraySelectOptions = array_flip($arraySelectOptionsFlip);
		$arraySelectOptions = array_values($arraySelectOptions);
		//Obtenemos nuevo valor de ->StrSelectOptions
		$LN = chr(13).chr(10);
		$mainQuestion->StrSelectOptions = implode($LN, $arraySelectOptions);
		//Validamos las opciones
		$mainQuestion->validateSelectOptions();
		
		$sql = "UPDATE SI_SV_Question SET 
				OptionsText = ".$this->Repository->DataADOConnection->Quote($mainQuestion->StrSelectOptions)." 
			WHERE QuestionID = ".$mainQuestion->QuestionID." AND SurveyID = ".$mainQuestion->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false) {
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
		//En este caso, la opción eliminada se remueve de la tabla de mapeos. El resto de las opciones van a cambiar de OptionID dentro de la
		//función saveSelectOptions, sin embargo en dicha función se hará un UPDATE a la tabla de mapeos del viejo ID al nuevo ID
		$sql = "DELETE FROM SI_SV_SurveyAnswerActionFieldsMap WHERE QuestionOptionID = ".$this->QuestionOptionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false) {
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
		//Ya no se eliminarán las opciones de respuesta previamente grabadas, ahora se insertará la opción simplemente desde este punto
		//Y guardamos las opciones en la tabla de SI_SV_QAnswers y en las tablas de la HandHeld
		//$mainQuestion->saveSelectOptions();
		//@JAPR
		
		//@JAPR 2015-07-17: Agregado el soporte de Formas v6 con un grabado completamente diferente
		//Ya no se tiene acceso a modelos ni dimensiones
		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
		if ($surveyInstance->CreationAdminVersion < esveFormsv6) {
			//En la tabla de dimension se eliminara la opcion si es que esta fue eliminada de las opciones
			//de la pregunta
			$strOriginalWD = getcwd();
			
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
			
			require_once("../model_manager/model.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			
			$anInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $mainQuestion->IndDimID);
			chdir($strOriginalWD);
			
			//Se agregan los nuevos elementos a la dimension y se eliminan aquellos que ya no estan en uso
			$mainQuestion->updateMassiveDimensionValues($anInstanceDim);
		}
		//@JAPR

		//Eliminamos las acciones relacionadas con la opcion que se elimino
		$sql = "DELETE FROM SI_SV_QOptionActions WHERE QuestionID = ".$this->QuestionID." AND OptionText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
		if ($this->Repository->DataADOConnection->Execute($sql) === false) {
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QOptionActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos los ShowQuestions para el ConsecutiveID de la opcion que se esta eliminando
		$sql = "DELETE FROM SI_SV_ShowQuestions WHERE QuestionID = ".$this->QuestionID." AND ConsecutiveID = ".$this->QuestionOptionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false) {
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Actualizar datos de modificacion de la encuesta
		BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
		
		//@JAPR 2013-12-05: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		//Si se trata de la pregunta original, verifica si hay alguna pregunta que comparte sus opciones de respuesta para también editarla
		//en ellas, para lo cual invoca a este mismo método pero de la instancia correspondiente a la pregunta compartida, actualizando sólo la
		//etiqueta de la pregunta, ya que el resto de las opciones pudieran estar personalizadas en las preguntas compartidas
		if (getMDVersion() >= esvSimpleChoiceSharing && $mainQuestion->SharedQuestionID <= 0) {
			$sql = "SELECT SharedQuestionID AS 'QuestionID', SharedConsecutiveID AS 'ConsecutiveID' 
				FROM SI_SV_QAnswersMatch 
				WHERE SourceQuestionID = {$mainQuestion->QuestionID} AND SourceConsecutiveID = {$this->QuestionOptionID}";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				while (!$aRS->EOF) {
					$intSharedQuestionID = (int) @$aRS->fields["questionid"];
					$intSharedQuestionOptionID = (int) @$aRS->fields["consecutiveid"];
					if ($intSharedQuestionID <= 0 || $intSharedQuestionOptionID <= 0) {
						continue;
					}
					$objSharedQuestion = @BITAMQuestion::NewInstanceWithID($this->Repository, $intSharedQuestionID);
					if (is_null($objSharedQuestion)) {
						continue;
					}
					$aQuestionOption = @BITAMQuestionOption::NewInstanceWithID($this->Repository, $intSharedQuestionOptionID);
					if (is_null($aQuestionOption)) {
						continue;
					}
					
					//Invoca al grabado ya como una opción de respuesta de otra pregunta
					@$aQuestionOption->remove();
					
					//Elimina el mapeo de esta opción de respuesta en la pregunta compartida
					$sql = "DELETE FROM SI_SV_QAnswersMatch WHERE SharedQuestionID = {$intSharedQuestionID} AND SharedConsecutiveID = {$intSharedQuestionOptionID}";
					$this->Repository->DataADOConnection->Execute($sql);
					
					$aRS->MoveNext();
				}
			}
		}
		
		//Elimina la opción de respuesta directamente en este método ahora que ya desapareció el borrado mediante un grabado de todas las opciones desde la pregunta
		$sql = "DELETE FROM SI_SV_QAnswers WHERE ConsecutiveID = ".$this->QuestionOptionID;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Deleting question option: {$sql}", 2, 0, "color:blue;");
		}
		if ($this->Repository->DataADOConnection->Execute($sql)) {
			//Reordena las opciones restantes recorriendo una posición hacia arriba
			$sql = "UPDATE SI_SV_QAnswers SET SortOrder = SortOrder -1 
				WHERE QuestionID = {$mainQuestion->QuestionID} AND SortOrder > {$this->SortOrder}";
			$this->Repository->DataADOConnection->Execute($sql);
		}
		//@JAPR
	}

	function isNewObject()
	{
		return ($this->QuestionOptionID <= 0  || $this->ForceNew);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Question Option");
		}
		else
		{
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			
			if($mainQuestion->QTypeID==qtpSingle && $mainQuestion->QDisplayMode == dspMatrix)
			{
				return translate("If row item is")." \"".$this->QuestionOptionName."\"";
			}
			else 
			{
				return str_ireplace("{var1}", " \"".$this->QuestionOptionName."\"", translate("If response option is {var1}"));
			}
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=QuestionOption&QuestionID=".$this->QuestionID;
		}
		else
		{
			return "BITAM_PAGE=QuestionOption&QuestionID=".$this->QuestionID."&QuestionOptionID=".$this->QuestionOptionID;
		}
	}

	function get_Parent()
	{
		return BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'QuestionOptionID';
	}
	
	function insideEdit()
	{
		//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
		if (getMDVersion() >= esveBavelSuppActions && ((int) @$this->eBavelAppID) > 0) {
?>
		objSpanChange = document.getElementById("mapeBavelActionFields");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground.gif)';
		objSpanChange.style.cursor = 'pointer';
		objSpanChange.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChange.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChange.onclick = new Function("openAssigneBavelActionFields();");
<?
		}
	}
	
	function insideCancel()
	{
		//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
		if (getMDVersion() >= esveBavelSuppActions && ((int) @$this->eBavelAppID) > 0) {
?>
		objSpanChange = document.getElementById("mapeBavelActionFields");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground_off.gif)';
		objSpanChange.style.cursor = 'default';
		objSpanChange.onmouseover = "";
		objSpanChange.onmouseout = "";
		objSpanChange.onclick = "";
<?
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
				
			//Si está en desarrollo la encuesta si se puede editar la pregunta
			if($aSurveyInstance->Status==0)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
		
		//Si está en desarrollo la encuesta si se puede remover la pregunta
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		if($_SESSION["PABITAM_UserRole"]==1 && $aSurveyInstance->Status==0 && !$mainQuestion->IsSelector)
		{
			//Por el momento deshabilitamos el remove de las opciones
			//de las preguntas de matriz hasta q haya mas tiempo para programarlo
			if(($mainQuestion->QTypeID==qtpSingle && $mainQuestion->UseCatalog==0 && $mainQuestion->QDisplayMode==dspMatrix))
			{
				return false;
			}
			else if($mainQuestion->QTypeID==qtpMulti && $mainQuestion->MCInputType!=mpcCheckBox && $mainQuestion->UseCategoryDimChoice==0)
			{
				return false;
			}
			else 
			{
				return true;
			}
		}
		else 
		{
			return false;
		}
	}

	//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
	function hideBackButton($aUser)
	{
		return true;
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QuestionOptionName";
		$aField->Title = translate("Label");
		$aField->Type = "String";
		$aField->Size = 255;
		
		//@JAPR 2013-12-05: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		if ($mainQuestion->SharedQuestionID > 0) {
			$aField->IsDisplayOnly = true;
		}
		else if($this->isNewObject() && ($this->QTypeID==qtpSingle || $this->QTypeID==qtpCallList) && $mainQuestion->QDisplayMode!=dspMatrix)
		{
			$aField->IsDisplayOnly = false;
		}
		//@JAPR 2013-05-07: Agregado el tipo de pregunta CallList
		else if(!$this->isNewObject() && ($this->QTypeID==qtpSingle || $this->QTypeID == qtpCallList) && $mainQuestion->QDisplayMode!=dspMatrix)
		{
			//Verificar si esta opcion existe en otra dimension compartida de otra encuesta
			$existQOption = $this->existQOptionInOtherSharedDims();
			
			if($existQOption==true)
			{
				$aField->IsDisplayOnly = true;
			}
			else 
			{
				$aField->IsDisplayOnly = false;
			}
		}
		else if($this->QTypeID==qtpMulti && $mainQuestion->QDisplayMode==dspVertical && $mainQuestion->MCInputType==mpcNumeric && $mainQuestion->IsMultiDimension==0 && $mainQuestion->UseCategoryDimChoice>0)
		{
			$aField->IsDisplayOnly = false;
		}
		else if($this->QTypeID==qtpMulti && $mainQuestion->MCInputType==mpcCheckBox)
		{
			$aField->IsDisplayOnly = false;
		}
		else if(!$this->isNewObject()) 
		{
			if($this->QTypeID==qtpMulti && $mainQuestion->QDisplayMode==dspVertical && $mainQuestion->MCInputType==mpcNumeric && $mainQuestion->IsMultiDimension==1) {
				$aField->IsDisplayOnly = false;
			} else {
				$aField->IsDisplayOnly = true;
			}
		}
		
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			//@JAPR 2014-08-05: Agregado el Responsive Design
			$arrDevices = array();
			if (getMDVersion() >= esvResponsiveDesign) {
				$arrDevices[] = getDeviceNameFromID(dvciPod);
				$arrDevices[] = getDeviceNameFromID(dvciPad);
				$arrDevices[] = getDeviceNameFromID(dvciPadMini);
				$arrDevices[] = getDeviceNameFromID(dvciPhone);
				$arrDevices[] = getDeviceNameFromID(dvcCel);
				$arrDevices[] = getDeviceNameFromID(dvcTablet);
			}
			
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
			$this->HTMLText = $this->TranslateVariableQuestionIDsByNumbers($this->HTMLText);
			if (getMDVersion() >= esvResponsiveDesign) {
				for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
					$strDeviceName = getDeviceNameFromID($intDeviceID);
					$strProperty = "HTMLText";
					$strValue = $this->getResponsiveDesignProperty($strProperty, $strDeviceName);
					$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $this->TranslateVariableQuestionIDsByNumbers($strValue));
				}
			}
			//@JAPR
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "HTMLText";
			$aField->Title = translate("Formatted text");
			$aField->Type = "LargeStringHTML";
			$aField->HTMLFormatButtons = "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink";
			//@JAPR 2014-08-05: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign) {
				$aField->MultiDeviceNames = $arrDevices;
			}
			//@JAPR
			$aField->Size = 5000;
			$aField->UseHTMLDes = 1;
			$aField->CompToAdd = "'LBL,ADD'";
			$aField->CompToAddDes = "ML[157]+'_AWSep_comp_agregar.gif',ML[584]+'_AWSep_rep_etiqueta.gif'";
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		if($mainQuestion->QTypeID==qtpSingle || ($mainQuestion->QTypeID==qtpMulti && $mainQuestion->MCInputType==mpcCheckBox))
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Score";
			$aField->Title = translate("Score");
			$aField->Type = "Float";
			$aField->DefaultNumberZero = false;
			$aField->FormatMask = "#,##0.00";
			$myFields[$aField->Name] = $aField;
		}

		if($mainQuestion->QTypeID==qtpSingle && $mainQuestion->QDisplayMode!=dspMatrix)
		{
			$arrayNextSections = $this->getNextSections();
				
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "NextQuestion";
			$aField->Title = translate("Skip to section");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrayNextSections;
			//@JAPR 2014-05-20: Agregado el tipo de sección Inline
			if(!$this->isNewObject()) {
				if ($mainQuestion->IsSelector) {
					$aField->IsDisplayOnly = true;
				}
			}
			//@JAPR
			$myFields[$aField->Name] = $aField;
		}
		
		//Conchita 2013-04-29 Agregada la opcion de numero telefonico
		if($this->QTypeID==qtpCallList){
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "phoneNum";
			$aField->Title = translate("Telephone Number");
			$aField->Type = "String";
			$aField->Size = 255;
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2012-05-21: Agregadas las acciones en preguntas de selección sencilla/múltiple
		if(($mainQuestion->QTypeID==qtpSingle && $mainQuestion->QDisplayMode!=dspMatrix) || ($mainQuestion->QTypeID==qtpMulti && $mainQuestion->MCInputType==mpcCheckBox))
		{
			//@JAPR 2012-10-29: Agregada la información extra para las acciones
			if (getMDVersion() >= esvExtendedActionsData) {
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
				$this->ActionTitle = $this->TranslateVariableQuestionIDsByNumbers($this->ActionTitle);
				//@JAPR
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "ActionTitle";
				$aField->Title = translate("Action title");
				$aField->Type = "String";
				$aField->Size = 255;
				/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
				$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpionline8.bitam.com/artus/gen8/ESurveyV5/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'ActionTitle\', ' . $mainQuestion->SurveyID . ', ' . $this->QuestionID . ');">';
				$myFields[$aField->Name] = $aField;
			}
			//@JAPR

			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
			$this->ActionText = $this->TranslateVariableQuestionIDsByNumbers($this->ActionText);
			//@JAPR
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ActionText";
			$aField->Title = translate("Action description");
			$aField->Type = "LargeString";
			$aField->Size = 5000;
			/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
			$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpionline8.bitam.com/artus/gen8/ESurveyV5/btnGenerar.gif); background-size: 100% 100%; margin: 5px 535px; background-repeat: no-repeat;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'ActionText\', ' . $mainQuestion->SurveyID . ', ' . $this->QuestionID . ');">';
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "CategoryID";
			$aField->Title = translate("Category");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = BITAMCatalog::GetCategories($this->Repository, true);
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ResponsibleID";
			$aField->Title = translate("Responsible");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			//@JAPR 2013-07-15: Agregado el mapeo de Supervisores y MySelf como responsables de acciones desde el servicio
			$userList = BITAMCatalog::GetUsers($this->Repository);
			if (getMDVersion() >= esveBavelSuppActions123) {
				//En este caso como es la primera versión que va a incluir esta configuración desde el servidor, se pueden utilizar los valores que
				//sean convenientes, por lo que los valores negativos serán utilizados para representar a los Supervisores, mientras el valor 0
				//representará a MySql (que así es como ya funcionaba de todas formas). Sin embargo debido a que en v4 el valor default para indicador
				//que la pregunta NO tenía un responsable especificado al grabar las definiciones de los móviles era el -1, se tendrá que empezar con
				//el valor -2 para los Supervisores
				$userList[(string) 0] = '('.translate("Myself").')';								//En version >= esveBavelSuppActions123 Se manda 0
				$userList[(string) -2] = '('.translate("Supervisor").' 1)';	//En version >= esveBavelSuppActions123 Realmente se manda -2
				$userList[(string) -3] = '('.translate("Supervisor").' 2)';	//En version >= esveBavelSuppActions123 Realmente se manda -3
				$userList[(string) -4] = '('.translate("Supervisor").' 3)';	//En version >= esveBavelSuppActions123 Realmente se manda -4
			}
			$aField->Options = $userList;
			//@JAPR
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "DaysDueDate";
			$aField->Title = translate("Days due date");
			$aField->Type = "Integer";
			//$aField->FormatMask = "##0.00";
			$myFields[$aField->Name] = $aField;
			
			//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions) {
				//Obtiene la lista de formas de eBavel creadas en el repositorio a partir de la aplicación que se definió en la encuesta
				$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
				$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
				$inteBavelAppID = $aSurveyInstance->eBavelAppID;
				//$this->SurveyID = $mainQuestion->SurveyID;
				if ($inteBavelAppID > 0) {
					$this->eBavelAppID = $inteBavelAppID;
					$aFieldApps = BITAMFormField::NewFormField();
					$aFieldApps->Name = "eBavelAppID";
					$aFieldApps->Title = translate("eBavel application");
					$aFieldApps->Type = "Object";
					$aFieldApps->VisualComponent = "Combobox";
					$aFieldApps->Options = array($inteBavelAppID => 'eBavel App');
					//$aFieldApps->OnChange = "cleareBavelFields();cleareBavelActionFields();";
					$aFieldApps->InTitle = true;
					$aFieldApps->IsVisible = false;
					$myFields[$aFieldApps->Name] = $aFieldApps;
					
					$arreBavelForms = array('' => '('.translate('None').')');
					//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
					require_once('eBavelIntegration.inc.php');
					$arreBavelForms = GetEBavelCollectionByField(@GetEBavelSections($this->Repository, true, $inteBavelAppID), 'sectionName', 'key', true);
					
					$aFieldActionForms = BITAMFormField::NewFormField();
					$aFieldActionForms->Name = "eBavelActionFormID";
					$aFieldActionForms->Title = translate("eBavel action data form");
					$aFieldActionForms->Type = "Object";
					$aFieldActionForms->VisualComponent = "Combobox";
					$aFieldActionForms->Options = $arreBavelForms;
					$aFieldActionForms->OnChange = "cleareBavelActionFields();";
					if (getMDVersion() >= esveBavelSuppActions) {
						//$aFieldActionForms->AfterMessage = "<span id=\"mapeBavelActionFields\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);background-repeat:no-repeat;background-position:100% 100%;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssigneBavelActionFields();\">".translate("Map action fields")."</span>";
						$aFieldActionForms->AfterMessage = "<span id=\"mapeBavelActionFields\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;
							font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" 
							onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssigneBavelActionFields();\">".
							"<img src=\"btngenerar.gif\" style=\"position:relative; left:0px; top:2px; width:150px; height:16px;\"/ displayMe=1>".
							"<span style=\"position:relative; left:-140px; top:-3px\">".
							translate("Map action fields")."</span></span>";
					}
					else {
						$aFieldActionForms->InTitle = true;
						$aFieldActionForms->IsVisible = false;
					}
					//@JAPR
					$myFields[$aFieldActionForms->Name] = $aFieldActionForms;
					//$aFieldActionForms->Parent = $aFieldApps;
					//$aFieldApps->Children[] = $aFieldActionForms;
					
					//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
					if (getMDVersion() >= esvActionConditions) {
						$this->ActionCondition = $this->TranslateVariableQuestionIDsByNumbers($this->ActionCondition);
						$aField = BITAMFormField::NewFormField();
						$aField->Name = "ActionCondition";
						$aField->Title = translate("Action condition");
						$aField->Type = "LargeString";
						$aField->Size = 5000;
						$myFields[$aField->Name] = $aField;
					}
					//@JAPR
					
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "eBavelActionFieldsData";
					$aField->Title = "eBavel action fields mapping";
					$aField->Type = "LargeString";
					$aField->Size = 5000;
					$aField->InTitle = true;
					$aField->IsVisible = false;
					$myFields[$aField->Name] = $aField;
				}
			}
			//@JAPR
		}
		
		if (($mainQuestion->QTypeID==qtpMulti && $mainQuestion->MCInputType==mpcCheckBox))
		{
			$arrInsertOptions = array();
			$arrInsertOptions[ioaUnChecked] = translate('Unselected');
			$arrInsertOptions[ioaChecked] = translate('Selected');
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "InsertWhen";
			$aField->Title = translate("Insert action when this option is").'...';
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrInsertOptions;
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		if (getMDVersion() >= esvHideAnswers) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Visible";
			$aField->Title = translate("Visible");
			$aField->Type = "Boolean";
			$aField->VisualComponent = "Checkbox";
			//@JAPR 2014-05-20: Agregado el tipo de sección Inline
			if(!$this->isNewObject()) {
				if ($mainQuestion->IsSelector) {
					//@JAPR 2014-07-18: Corregido un bug, si se deshabilita un componente Checkbox pero aún así se muestra y ademas se va a
					//grabar, debido al disabled aparentemente estaba limpíando el valor, así que se optará por no mostrar el campo
					$aField->Title = "";
					$aField->IsVisible = false;
					//$aField->IsDisplayOnly = true;
				}
			}
			//@JAPR
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		if (getMDVersion() >= esvDefaultOption) {
			if($mainQuestion->QTypeID==qtpMulti) {
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
				$this->DefaultValue = $this->TranslateVariableQuestionIDsByNumbers($this->DefaultValue);
				//@JAPR
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "DefaultValue";
				switch($mainQuestion->MCInputType) {
					case mpcNumeric:
						$aField->Title = translate("Default value");
						$aField->Type = "Integer";
						break;
					case mpcText:
						$aField->Title = translate("Default value");
						$aField->Type = "String";
						$aField->Size = 255;
						break;
					case mpcCheckBox: 
					default:
						$aField->Title = translate("Is default?");
						$aField->Type = "Boolean";
						$aField->VisualComponent = "Checkbox";
						break;
				}
				/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
				$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpionline8.bitam.com/artus/gen8/ESurveyV5/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'DefaultValue\', ' . $mainQuestion->SurveyID . ', ' . $this->QuestionID . ');">';
				$myFields[$aField->Name] = $aField;
			}
		}
		
		if (getMDVersion() >= esvAgendaRedesign) {
			if ($mainQuestion->QTypeID==qtpSingle) {
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "CancelAgenda";
				$aField->Title = translate("Cancel agenda");
				$aField->Type = "Boolean";
				$aField->VisualComponent = "Checkbox";
				/*if(!$this->isNewObject()) {
					if ($mainQuestion->IsSelector) {
						$aField->Title = "";
						$aField->IsVisible = false;
					}
				}*/
				$myFields[$aField->Name] = $aField;
			}
		}
		
		if (getMDVersion() >= esvEditorHTML)
		{
			if($this->isNewObject())
			{
				$arrEditorType[0] = "Tinymce";
				$arrEditorType[1] = translate("Image Editor");
			}
			else
			{
				if($this->EditorType==0)
				{
					$arrEditorType[0] = "Tinymce";
				}
				else
				{
					$arrEditorType[1] = translate("Image Editor");
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "EditorType";
			//$aField->Title = translate("Editor");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrEditorType;
			$aField->OnChange = "onChangeEditor()";
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
		}		
		
		return $myFields;
	}
	
	function existQOptionInOtherSharedDims()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		$sql = "SELECT DisplayText FROM SI_SV_QAnswers WHERE QuestionID IN
				(
  					SELECT QuestionID FROM SI_SV_Question WHERE IndDimID = ".$mainQuestion->IndDimID." 
  					AND QuestionID <> ".$this->QuestionID." 
				) AND DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
		
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if($aRS->EOF)
		{
			return false;
		}
		else 
		{
			return true;
		}
	}
	
	function getNextQuestions()
	{
		$arrayNextQuestions = array();
		$arrayNextQuestions[0] = "Continue";
		$arrayNextQuestions[-1] = "End Survey";
		
		$aQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$questionNumber = $aQuestion->QuestionNumber;
		$surveyID = $aQuestion->SurveyID;
		
		$sql = "SELECT QuestionID, QuestionNumber, QuestionText FROM SI_SV_Question 
				WHERE SurveyID = ".$surveyID." AND QuestionNumber > ".$questionNumber." ORDER BY QuestionNumber";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$theQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $theQuestion->SurveyID);

		while(!$aRS->EOF)
		{
			$questionid = (int)$aRS->fields["questionid"];
			$questiontext = $aRS->fields["questionnumber"].". ".$aRS->fields["questiontext"];
			$aQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $questionid);
			//si la pregunta esta en la seccion dinamica, solo podra saltar 
			//en preguntas que se encuentrren dentro de la misma seccion dinamica
			if($theQuestion->SectionID == $dynamicSectionID && $aQuestion->SectionID == $dynamicSectionID) {
					$arrayNextQuestions[$questionid] = $questiontext;
			} else {
				// si la pregunta no esta en la seccion dinamica
				//entonces no debe poder hacer un salto a preguntas dentro de la seccion dinamica
				if($aQuestion->SectionID != $dynamicSectionID) {
					$arrayNextQuestions[$questionid] = $questiontext;
				}
			}
			$aRS->MoveNext();
		}
		
		return $arrayNextQuestions;
	}
	
	function getNextSections()
	{
		$arrayNextSections = array();
		$arrayNextSections[0] = "Continue";
		
		$aQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$sectionNumber = $aQuestion->SectionNumber;
		$surveyID = $aQuestion->SurveyID;
		
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		$arrInvalidSections = array();
		if (getMDVersion() >= esvSectionQuestions) {
			$arrInvalidSections = @BITAMSection::GetMappedSections($this->Repository, $surveyID, false);
			if (!is_null($arrInvalidSections) && is_array($arrInvalidSections)) {
				$arrInvalidSections = array_keys($arrInvalidSections);
			}
		}
		//@JAPR
		
		$sql = "SELECT SectionID, SectionNumber, SectionName FROM SI_SV_Section 
				WHERE SurveyID = ".$surveyID." AND SectionNumber > ".$sectionNumber." ORDER BY SectionNumber";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$sectionid = (int)$aRS->fields["sectionid"];
			//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
			//No debe considerar a las secciones que ya se han mapeado pues ya no funcionarán como secciones en páginas independientes
			if (!in_array($sectionid, $arrInvalidSections)) {
				$sectionname = $aRS->fields["sectionname"];
				$arrayNextSections[$sectionid] = $sectionname;
			}
			//@JAPR

			$aRS->MoveNext();
		}
		
		return $arrayNextSections;
	}
	
	function generateInsideFormCode($aUser)
	{
		if($this->SurveyID>0)
		{
		//Obtener secciones de la encuesta
		$sql = "SELECT t1.SectionID, t1.SectionName
				FROM SI_SV_Section t1
				WHERE t1.SurveyID = ".$this->SurveyID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		}
		
		$aSections = array();
		
?>
		<script language="JavaScript">
		var htmlOptionsBtn = '';
		aButtonElements[0]= "Exit";
<?			
		if($this->SurveyID>0)
		{
		while (!$aRS->EOF)
		{
			$aSections[(int)$aRS->fields["sectionid"]]=$aRS->fields["sectionname"];
?>
			aButtonElements[<?=(int)$aRS->fields["sectionid"]?>] = '<?=str_replace("'", "\'", $aRS->fields["sectionname"])?>';
			htmlOptionsBtn += '<option value="<?=(int)$aRS->fields["sectionid"]?>"><?=str_replace("'", "\'", $aRS->fields["sectionname"])?></option>';
<?			
			$aRS->MoveNext();
		}
		}
?>
		</script>
<?		
		//05Feb2013: Las preguntas simple choice (qtpSingle) o multiple choice (qtpMulti) 
		//con tipo de despliegue menú no deben permitir agregar imagenes
		if(!(($this->QTypeID==qtpSingle || $this->QTypeID==qtpMulti) && $this->QDisplayMode==dspMenu))
		{
			include("formattedTextQuestionOption.php");
		}
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
 		$myFormName = get_class($this);
?>
		<!--<script language="javascript" src="js/dialogs.js"></script>-->
	 	<script language="JavaScript">
	 	var myServerPath='<?=(((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";?>';

		function verifyFieldsAndSave(target)
 		{
			//Llenar los campos en los que se guardarán los componentes del diseñador
			var newEditor = false;
			for(var divName in aHTMLDivObjects)
			{
				var textAreaName = divName.replace("HDiv", "");
				textAreaName = textAreaName.replace("Default", "");
				textAreaNameObj = document.getElementById(textAreaName);
				textAreaNameObj.value=aHTMLDivObjects[divName].oPart.save(true,',');
				if(textAreaNameObj.value!='')
				{
					newEditor = true;
				}
			}
			//Si ya se utilizó por lo menos una vez el nuevo editor en la pantalla de sección
			//entonces los campos HTML que se manejaban anteriormente serán reemplazados con el HTML
			//generado a partir del nuevo editor, esto se hará para todos los dispositivos
			if(newEditor==true)
			{
				for(var divName in aHTMLDivObjects)
				{
					var textAreaName = divName.replace("HDiv", "");
					textAreaName = textAreaName.replace("Default", "");
					textAreaName = textAreaName+'HTML';
					textAreaNameObj = document.getElementById(textAreaName);
					textAreaNameObj.value=aHTMLDivObjects[divName].oPart.saveHTML();
					//alert(textAreaName+' - saveHTML(): '+aHTMLDivObjects[divName].oPart.saveHTML());
					//alert(textAreaName+' - textAreaNameObj.value: '+textAreaNameObj.value);
					//alert(textAreaName+' - textAreaNameObj.innerHTML: '+textAreaNameObj.innerHTML);
	
				}
			}

			var strBlanksField='';
 			if(Trim(<?=$myFormName?>_SaveForm.QuestionOptionName.value)=='')
 			{	
				strBlanksField = '\n- ' + '<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Label"))?>';
			}
			
			if(strBlanksField!="")
  			{
				alert('<?=translate("Please verify")?>:\n' + strBlanksField );
  			}
 			else
 			{
	 			<?=$myFormName?>_Ok(target);
 			}
 		}

 		/*@AAL 25/02/2015: Abre la ventana de dialogo donde se mostrará el editor de fórmulas, recibe como 
		parámetro el nombre del elemento HTML (NameElement) de donde se tomará y escribirá la fórmula.*/
 		function ShowWindowsDialogEditor(NameElement)
		{
			var SurveyID = '<?= $this->SurveyID ?>';
			var QuestionID = '<?= $this->QuestionID ?>';
			var aText = document.getElementsByName(NameElement);
			var text = "";
			if (NameElement.indexOf('HTMLText') != -1 && aText[0].style.display == "none")
				text = tinyMCE.activeEditor.selection.getContent({format: 'text'});
			else
				text = aText[0].value;

			var Parameters = 'SurveyID=' + SurveyID + '&QuestionID=' + QuestionID + '&NameElement=' + NameElement + '&Type=Q&aTextEditor=' + encodeURIComponent(text);
			openWindowDialog('formulaEditor.php?' + Parameters, [window], [NameElement], 620, 460, ResultEditFormulas, 'ResultEditFormulas');
		}

		/*@AAL 25/02/2015: Esta función recibe como parámetro sValue que corresponde al texto devuelto del editor de fórmulas 
		y sParams corresponde al nombre del elemento HTML donde se asignará la nueva fórmula o texto*/
		function ResultEditFormulas(sValue, sParams)
		{
			var aText = document.getElementsByName(sParams[0]);
			if (sParams[0].indexOf('HTMLText') != -1 && aText[0].style.display == "none")
				tinyMCE.activeEditor.selection.setContent(sValue + " ");
			else
				aText[0].value = sValue;
		}
 		//@AAL

		function cleareBavelActionFields() {
			var objField = <?=$myFormName?>_SaveForm.eBavelActionFieldsData;
			if (objField != null) {
				objField.value = '';
			}
			
			inteBavelFormID = 0;
			var objField = <?=$myFormName?>_SaveForm.eBavelActionFormID;
			if (objField != null) {
				inteBavelFormID = objField.value;
			}
			
			var rowActionCondition = document.getElementById("Row_ActionCondition");
			if (rowActionCondition) {
				rowActionCondition.style.display = (inteBavelFormID)?((bIsIE)?"inline":"table-row"):"none";
			}
		}
		
		function openAssigneBavelActionFields()
		{
			var inteBavelAppID = 0;
			var inteBavelFormID = 0;
			var objField = <?=$myFormName?>_SaveForm.eBavelAppID;
			if (objField != null) {
				inteBavelAppID = objField.value;
			}
			var objField = <?=$myFormName?>_SaveForm.eBavelActionFormID;
			if (objField != null) {
				inteBavelFormID = objField.value;
			}
			
			openWindowDialog('main.php?BITAM_PAGE=QuestionOptionActionMap&SurveyID=<?=$this->SurveyID?>&QuestionOptionID=<?=$this->QuestionOptionID?>&eBavelAppID='+inteBavelAppID+'&eBavelFormID='+inteBavelFormID, [window], [], 600, 600, openAssigneBavelActionFieldsDone, 'openAssigneBavelActionFieldsDone');
		}
		
		function openAssigneBavelActionFieldsDone(sValue, sParams)
		{
			var strInfo = sValue;
			//Actualiza la cadena de los campos mapeados de eBavel
			if(strInfo!=null && strInfo!=undefined)
			{
				try {
					<?=$myFormName?>_SaveForm.eBavelActionFieldsData.value = strInfo;
				} catch (e) {
				}
			}
		}
		
		/*@AAL 01/04/2015: Validación modificada para mostrar u ocultar ya sea Image Editor o tinyMCE cuado se quiera editar */
 		var IsEdit = false;
 		function onChangeEditor(IsNew)
 		{
 			var objCmbEditor = <?=$myFormName?>_SaveForm.EditorType;
 			var editorTypeValue = parseInt(objCmbEditor.options[objCmbEditor.selectedIndex].value);

 			if(IsNew)
 			{
	 			if(editorTypeValue == 1){
	 				document.getElementById("HTMLTextSpanDesigner").style.display="inline";
	 				document.getElementById("DisplayImageDefaultDiv").style.display="inline";
	 			}
	 			else{
	 				document.getElementById("HTMLTextSpanDesigner").style.display="none";
	 				document.getElementById("DisplayImageDefaultDiv").style.display="none";
	 			}
 			}
 			else{//Si se va a editar entonces se verifica el tipo de editor para mostrarlo al usuario
 				if (IsEdit) {
 					if(editorTypeValue == 1){
 						document.getElementById("HTMLTextSpanDesigner").style.display="inline";
 						document.getElementById("DisplayImageDefaultDiv").style.display="inline";
 					}
		 			else{
						document.getElementById("HTMLTextTabs").style.display="inline";
						document.getElementById("DisplayImageDefaultDiv").style.display="inline";
		 			}
					IsEdit = false;
 				}
 				else{
 					document.getElementById("HTMLTextSpanDesigner").style.display="none";
 					document.getElementById("HTMLTextTabs").style.display="none";
 					document.getElementById("DisplayImageDefaultDiv").style.display="none";
 					IsEdit = true;
 				}
 			}
 			
			//Cargar el div de la pestaña default
			var elemDefault = document.getElementById("defaultHTMLText");
			changeTab(elemDefault,'HTMLText');
 		}		
  		</script>
<?

	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
 		$myFormName = get_class($this);
?>
	 	<script language="JavaScript">
		
	 	objOkSelfButton = document.getElementById("<?=$myFormName?>_OkSelfButton");
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");

		objOkParentButton = document.getElementById("<?=$myFormName?>_OkParentButton");
		if(objOkParentButton)
		{
			objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		}
		
<?
		if($this->isNewObject())
 		{
?>
			objOkNewButton = document.getElementById("<?=$myFormName?>_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
		</script>
<?

	}
	
	function get_Children($aUser)
	{
		$myChildren = array();
		
		//@JAPR 2012-07-13: Validado para que se permitan ocultar preguntas independientemente de si se hace o no un salto (originalmente
		//se tenía bloqueado, pero se habilitó como Workaround a la versión inicial de la v3 en la que no se ocultaban las preguntas saltadas
		//automáticamente debido al cambio para validar requeridas hasta el momento de grabado, así que esas preguntas no ocultas pero saltadas
		//aun se seguian solicitando como requeridas)
		//if (!$this->isNewObject() && $this->NextQuestion==0)
		if (!$this->isNewObject())
		{
			$aQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			if(($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==0 && $aQuestion->QDisplayMode!=dspMatrix) || (getMDVersion()>=esvHideAnswers && $aQuestion->QTypeID==qtpMulti && $aQuestion->UseCatToFillMC==0 && $aQuestion->QDisplayMode!=dspMatrix))
			{
				//Verificamos si hay mas preguntas
				$questionNumber = $aQuestion->QuestionNumber;
				$surveyID = $aQuestion->SurveyID;
				$sectionID = $aQuestion->SectionID;
				
				//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
				/*
				$sql = "SELECT QuestionID, QuestionNumber, QuestionText FROM SI_SV_Question 
						WHERE SurveyID = ".$surveyID." AND SectionID = ".$sectionID." 
						AND QuestionNumber > ".$questionNumber." ORDER BY QuestionNumber";
				*/
				$sql = "SELECT COUNT(*) AS RemainingQuestions 
					FROM SI_SV_Question 
					WHERE SurveyID = ".$surveyID." AND QuestionNumber > ".$questionNumber;
				//@JAPR
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				if(!$aRS->EOF)
				{
					//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
					$intRemainingQuestions = (int) @$aRS->fields['remainingquestions'];
					if ($intRemainingQuestions > 0)
					{
						$myChildren[] = BITAMShowQuestionCollection::NewInstance($this->Repository, $this->QuestionOptionID);
					}
					//@JAPR
				}
			}
		}

		return $myChildren;
	}
	
	//@MABH 2014-06-18
	static function GetActionMapFieldsIDs ($aRepository, $aQuestionID, $aQuestionOptionID) {
		$mapFields = array();
		$sql = "SELECT FieldMapID FROM SI_SV_SurveyAnswerActionFieldsMap WHERE QuestionID = " . $aQuestionID . " AND QuestionOptionID = " . $aQuestionOptionID;
        $aRS = $aRepository->DataADOConnection->Execute($sql);
        if (!$aRS) {
            die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_SurveyAnswerActionFieldsMap " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
        }
		$countMapFields = 0;
        while (!$aRS->EOF) {
			$mapFields[$countMapFields++] = (int) @$aRS->fields['fieldmapid'];
			$aRS->MoveNext();
        }
        return $mapFields;
	}
	//@MABH
	
	//@JAPR 2014-10-17: Agregado el método para obtener la definición de este objeto
	//@JAPRWarning: Se implementó este método pero falta optimizar el caché de preguntas, así que no se utilizará hasta que en globalFormsInstance
	//se programe el caché, sin embargo se debería dar mantenimiento a esta función tal como se hace en el llenado de los options de Question
	function getJSonDefinition() {
		//@JAPR 2013-03-12: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
		global $blnWebMode;
		if (!isset($blnWebMode)) {
			$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
		}
		global $appVersion;
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		global $gbDesignMode;
		
		$arrDef = array();
		
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['name'] = $this->QuestionOptionName;
		$arrDef['nextSection'] = $this->NextQuestion;
		$strDisplayImage = trim($this->DisplayImage);
		//@JAPR 2012-10-11: Agregada la lectura de la imagen asociada para regresar en el servicio
		if ($strDisplayImage != '') {
			$strDisplayImage = trim(str_replace(array('<p>', '</p>'), '', $strDisplayImage));
			//@JAPR 2013-03-11: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
			//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
			if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
				$strDisplayImage = (string) @BITAMSection::convertImagesToBase64($strDisplayImage);
			}
			elseif (!$blnWebMode) {
				//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
				$strDisplayImage = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $strDisplayImage, true);
			}
			else {
				//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
				if ($this->CreationAdminVersion >= esveFormsv6) {
					$strDisplayImage = '<img src="'.$strDisplayImage.'" />';
				}
				//@JAPR
			}
			//@JAPR
		}
		//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
		$arrDef['displayImage'] = $strDisplayImage;
		//@JAPR
		
		$strHTMLText = trim($this->HTMLText);
		if ($strHTMLText != '') {
			$strHTMLText = trim(str_replace(array('<p>', '</p>'), '', $strHTMLText));
			//@JAPR 2013-03-11: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
			//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
			if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
				$strHTMLText = (string) @BITAMSection::convertImagesToBase64($strHTMLText);
			}
			//@JAPR 2013-08-06: Corregido un bug, las rutas de imagenes deben convertirse a utf-8
			elseif (!$blnWebMode) {
				$strHTMLText = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $strHTMLText);
			}
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
			$strHTMLText = $this->TranslateVariableQuestionIDsByNumbers($strHTMLText);
		}
		//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
		$arrDef['displayText'] = $strHTMLText;
		
		//conchita 2013-04-30 agregado el num de tel
		//@JAPR 2013-05-15: Corregido un bug, esta propiedad no tiene por qué enviarse en cuentas que no tenían dicha funcionalidad habilitada
		if (getMDVersion() >= esvCallList) {
			$strPhoneNum = trim($this->phoneNum); //Conchita 2013-04-30 num de tel
			$arrDef['phoneNum'] = $strPhoneNum;
		}
		
		//conchita
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		if (getMDVersion() >= esvHideAnswers) {
			$intVisible = (int) @$this->Visible;
			//Sólo se agregará la propiedad para ocultar, porque por default se asume que todas las respuestas son visibles
			if (!$intVisible) {
				$arrDef['hidden'] = 1;
			}
		}
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$cancelAgenda = (int) @$this->CancelAgenda;
				//Sólo se agregará la propiedad para ocultar, porque por default se asume que todas las respuestas son visibles
			if ($cancelAgenda) {
				$arrDef['cancelAgenda'] = 1;
			}
		}
		
		if (getMDVersion() >= esvDefaultOption) {
			$optiondefaultValue = (string) @$this->DefaultValue;
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
			$optiondefaultValue = $this->TranslateVariableQuestionIDsByNumbers($optiondefaultValue);
			//@JAPR
			$arrDef['defaultValue'] = $optiondefaultValue;
		}
		
		if (getMDVersion() >= esvSectionRecap) {
		
			//@JAPR 2015-04-15: Rediseñado el Admin con DHTMLX
			if ($gbDesignMode) {
				$arrDef['consecutiveID'] = $this->QuestionOptionID;
				$arrDef['id'] = $this->QuestionOptionID;
				$arrDef['questionID'] = $this->QuestionID;
			}
			//@JAPR
			$optionActionFormID = $this->eBavelActionFormID;
			if($optionActionFormID > 0) {
				if($this->QuestionOptionID > 0) {
					$arrDef['optionActionFormID'] = $optionActionFormID;
					//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
					//Estas propiedades están descontinuadas a partir de eForms v6
					if (getMDVersion() < esveFormsv6 ) {
						$mapFields = BITAMQuestionOption::GetActionMapFieldsIDs($this->Repository, $this->QuestionID, $this->QuestionOptionID);
						$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
						if (!is_null($mainQuestion)) {
							foreach($mapFields as $aMapFieldID) { 
								include_once("questionOptionActionMap.inc.php");
								$mapOptionObj = BITAMQuestionOptionActionMap::NewInstanceWithID($this->Repository, $aMapFieldID);
								if (!isset($arrDef['optionFieldMap'])) {
									$arrDef['optionFieldMap'] = array();
								}
								$arrDef['optionFieldMap'][$mapOptionObj->eBavelFieldID] = $mainQuestion->GetDefaultValueForMapped($mapOptionObj->SurveyFieldID);
							}
						}
					}
					//@JAPR
				}
			}
		}
		
		$arrDef['score'] = $this->Score;
    	$arrDef['showQuestionList'] = array();
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		if ($gbDesignMode) {
			$arrDef['objectType'] = otyOption;
			$arrDef['showQuestionCons'] = array();
			$arrDef['showQuestionShowIDs'] = array();
			$arrDef['showQuestionText'] = array();
		}
		//@JAPR
		
		return $arrDef;
	}

	//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
	function hideRemoveButton($aUser)
	{
		return true;
	}
}

class BITAMQuestionOptionCollection extends BITAMCollection
{
	public $QuestionID;				//ID de la pregunta donde existen grabadas esta colección de respuestas

	function __construct($aRepository, $aQuestionID)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->QuestionID = $aQuestionID;
		
		//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Con los cambios para reutilizar de caché e invocar a BITAMQuestionOptionCollection al cargar las opciones de respuesta desde BITAMQuestion, cargar la
		//instancia de mainQuestion desde la propia creación de instancias de BITAMQuestionOption provocaba una carga recursiva infinita, además que este valor
		//no aplica pues toda opción de respuesta para todo tipo de pregunta siempre viene ordenada por SortOrder, por lo que se comentó el código siguiente
		/*$mainQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $this->QuestionID);
		//@JAPR 2013-05-07: Agregado el tipo de pregunta CallList
		if(($mainQuestion->QTypeID==qtpSingle || $mainQuestion->QTypeID == qtpCallList) && $mainQuestion->QDisplayMode <> dspMatrix)
		{
			$this->OrderPosField = "SortOrder";
		}*/
		//@JAPR
	}
	
	static function NewInstance($aRepository, $aQuestionID, $anArrayOfQuestionOptionIDs = null, $aGetValues = false)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aQuestionID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);

		$filter = "";

		if (!is_null($anArrayOfQuestionOptionIDs))
		{
			switch (count($anArrayOfQuestionOptionIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND t1.ConsecutiveID = ".((int)$anArrayOfQuestionOptionIDs[0]);
					break;
				default:
					foreach ($anArrayOfQuestionOptionIDs as $aQuestionOptionID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aQuestionOptionID; 
					}
					if ($filter != "")
					{
						$filter = " AND t1.ConsecutiveID IN (".$filter.")";
					}
					break;
			}
		}
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		$sql = BITAMQuestionOption::getMainQuery().
			" AND t1.QuestionID = ".$aQuestionID.$filter.
			" ORDER BY t1.SortOrder";
		//@JAPR
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($aRepository, $aRS, $aGetValues);

			$aRS->MoveNext();
		}

		return $anInstance;
	}
	
	//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	/* Carga la colección de opciones de respuesta filtradas por le ID de la pregunta a la que pertenecen */
	static function NewInstanceByQuestion($aRepository, $aQuestionID)
	{
		$anInstance =& BITAMGlobalFormsInstance::GetQuestionOptionCollectionByQuestionWithID($aQuestionID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aQuestionID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$filter = "";
		
		$sql = BITAMQuestionOption::getMainQuery().
			" AND t1.QuestionID = ".$aQuestionID;
			" ORDER BY t1.SortOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$objQuestionOption = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
			$anInstance->Collection[] = $objQuestionOption;
			
			$aRS->MoveNext();
		}
		
		BITAMGlobalFormsInstance::AddQuestionOptionCollectionByQuestionWithID($aQuestionID, $anInstance);
		
		return $anInstance;
	}
	
	/* Obtiene la colección de opciones de respuesta pertenecientes a las formas indicadas en el parámetro, grabando en el caché de objetos indexado como colección 
	para cada forma y pregunta además de directamente cada objeto para optimizar su carga posterior (esta función se asume que es equivalente a invocar a NewInstance 
	pero con todos los parámetros en null excepto el SurveyID correspondiente) */
	static function NewInstanceFromDBFull($aRepository, $anArrayOfSurveyIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrQuestionOptionsBySurveyID = array();
		$arrQuestionOptionsByQuestionID = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " t2.SurveyID = ".((int) $anArrayOfSurveyIDs[0])." ";
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = " t2.SurveyID IN (".$filter.") ";
					}
					break;
			}
		}
		
		$sql = BITAMQuestionOption::getMainQuery().
			(($filter)?"AND ":"").$filter.
			" ORDER BY t2.QuestionID, t1.SortOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$objQuestionOption = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
			$anInstance->Collection[] = $objQuestionOption;
			
			//Agrega el objeto a la colección indexada por los padres
			$intSurveyID = $objQuestionOption->SurveyID;
			$intQuestionID = $objQuestionOption->QuestionID;
			if ( !isset($arrQuestionOptionsBySurveyID[$intSurveyID]) ) {
				$arrQuestionOptionsBySurveyID[$intSurveyID] = array();
			}
			$arrQuestionOptionsBySurveyID[$intSurveyID][] = $objQuestionOption;
			if ( !isset($arrQuestionOptionsByQuestionID[$intQuestionID]) ) {
				$arrQuestionOptionsByQuestionID[$intQuestionID] = array();
			}
			$arrQuestionOptionsByQuestionID[$intQuestionID][] = $objQuestionOption;
			
			$aRS->MoveNext();
		}
		
		//Finalmente procesa el array de formas y preguntas para indexar por todos los padres incluyendo aquellos que no tenían elementos y agregarlos al caché 
		//(en este punto se asume que este método se ejecutó posterior a la carga masiva de todos los padres, así que se usarán las colecciones que ya deberían 
		//estar cachadas). Esta colección realmente no puede ser procesada mediante el caché debido a que para cargar las preguntas se requiere al mismo tiempo
		//cargar las opciones de respuesta lo cual es precisamente este punto, así que si se invoca antes este método no se tendrían las preguntas cargadas, por
		//lo que se tendrá que hacer un query para obtener todas las preguntas de la forma y así poder dejar una colección vacía donde no existan opciones de
		//respuesta
		$sql = "SELECT t2.QuestionID 
			FROM SI_SV_Question t2 ".
			(($filter)?"WHERE ":"").$filter;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		$arrAllQuestionOptionsCollection = array();
		while (!$aRS->EOF) {
			$intQuestionID = (int) @$aRS->fields["questionid"];
			/*if (isset(BITAMGlobalFormsInstance::$arrAllQuestionCollectionsBySurvey)) {
				foreach(BITAMGlobalFormsInstance::$arrAllQuestionCollectionsBySurvey as $objQuestionCollection) {
					foreach ($objQuestionCollection->Collection as $objQuestion) {
						$intQuestionID = $objQuestion->QuestionID;*/
						//Se debe usar directamente el elemento desde el array en lugar de una variable local, porque BITAMGlobalFormsInstance lo recibe por referencia
						$arrAllQuestionOptionsCollection[$intQuestionID] = new $strCalledClassColl($aRepository, $intQuestionID);
						//$objQuestionOptionsCollection = new $strCalledClassColl($aRepository, $intQuestionID);
						if ( isset($arrQuestionOptionsByQuestionID[$intQuestionID]) ) {
							$arrAllQuestionOptionsCollection[$intQuestionID]->Collection = $arrQuestionOptionsByQuestionID[$intQuestionID];
						}
						BITAMGlobalFormsInstance::AddQuestionOptionCollectionByQuestionWithID($intQuestionID, $arrAllQuestionOptionsCollection[$intQuestionID]);
					/*}
				}
			}*/
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("QuestionID", $aHTTPRequest->GET))
		{
			$aQuestionID = (int) $aHTTPRequest->GET["QuestionID"];
		}
		else
		{
			$aQuestionID = 0;
		}
		if (array_key_exists("SortOrder", $aHTTPRequest->POST)) {
         
            $orderPos = (int) $aHTTPRequest->POST["SortOrder"];
            $aQuestionOptionID = (int) $aHTTPRequest->POST["QuestionOptionID"];
            BITAMQuestionOptionCollection::Reorder($aRepository, $aQuestionID, $aQuestionOptionID, $orderPos);
        }

        if (array_key_exists("SortOrder", $aHTTPRequest->POST)) {
            $anInstance = BITAMQuestionOption::NewInstance($aRepository, $aQuestionID);
            $anInstance = $anInstance->get_Parent();
            $aHTTPRequest->RedirectTo = $anInstance;
            return null;
        }
		else{
			return BITAMQuestionOptionCollection::NewInstance($aRepository, $aQuestionID);
		}
	}
	
	function get_Parent()
	{
		return BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
	}

	function get_Title()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		if($mainQuestion->QTypeID==qtpSingle && $mainQuestion->QDisplayMode == dspMatrix)
		{
			return translate("Response items")."- ".translate("Rows");
		}
		else 
		{
			return translate("Response items");
		}
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=QuestionOptionCollection&QuestionID=".$this->QuestionID;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=QuestionOption&QuestionID=".$this->QuestionID;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'QuestionOptionID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QuestionOptionName";
		if($mainQuestion->QTypeID==qtpSingle && $mainQuestion->QDisplayMode == dspMatrix)
		{
			$aField->Title = translate("Row item");
		}
		else 
		{
			$aField->Title = translate("Question Option");
		}
		
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		if($mainQuestion->QTypeID==qtpSingle || ($mainQuestion->QTypeID==qtpMulti && $mainQuestion->MCInputType==mpcCheckBox))
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Score";
			$aField->Title = translate("Score");
			$aField->Type = "Float";
			$aField->DefaultNumberZero = false;
			$aField->FormatMask = "#,##0.00";
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		if (getMDVersion() >= esvHideAnswers) {
			$arrVisibleState = array(translate("No"), translate("Yes"));
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Visible";
			$aField->Title = translate("Is visible?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrVisibleState;
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);

		//Si está en desarrollo la encuesta si se puede editar la pregunta
		//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		//Las preguntas que comparten opciones de respuesta con otra, no pueden agregar sus propias opciones de respuesta
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		if($_SESSION["PABITAM_UserRole"]==1 && $aSurveyInstance->Status==0 && $mainQuestion->SharedQuestionID <= 0 && !$mainQuestion->IsSelector)
		{
			if($mainQuestion->QTypeID==qtpMulti && $mainQuestion->MCInputType!=mpcCheckBox)
			{
				if($mainQuestion->QDisplayMode==dspVertical && $mainQuestion->MCInputType==mpcNumeric && $mainQuestion->IsMultiDimension==0 && $mainQuestion->UseCategoryDimChoice!=0)
				{
					return true;
				}
				else 
				{
					//Ya se permiten agregar campos de entradas numericas y alfanumericas
					//return false;
					return true;
				}
			}
			//Por el momento deshabilitamos el add de las opciones
			//de las preguntas de matriz hasta q haya mas tiempo para programarlo
			else if($mainQuestion->QTypeID==qtpSingle && $mainQuestion->UseCatalog==0 && $mainQuestion->QDisplayMode==dspMatrix)
			{
				return false;
			}
			else 
			{
				return true;
			}
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
		
		//Si está en desarrollo la encuesta si se puede remover la pregunta
		//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		//Las preguntas que comparten opciones de respuesta con otra, no pueden agregar sus propias opciones de respuesta
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		if($_SESSION["PABITAM_UserRole"]==1 && $aSurveyInstance->Status==0 && $mainQuestion->SharedQuestionID <= 0 && !$mainQuestion->IsSelector)
		{
			//Por el momento deshabilitamos el remove de las opciones
			//de las preguntas de matriz hasta q haya mas tiempo para programarlo
			if(($mainQuestion->QTypeID==qtpSingle && $mainQuestion->UseCatalog==0 && $mainQuestion->QDisplayMode==dspMatrix))
			{
				return false;
			}
			else if($mainQuestion->QTypeID==qtpMulti && $mainQuestion->MCInputType!=mpcCheckBox && $mainQuestion->UseCategoryDimChoice==0)
			{
				return false;
			}
			else 
			{
				return true;
			}
		}
		else 
		{
			return false;
		}
	}

	static function Reorder($aRepository, $aQuestionID, $aQuestionOptionID, $orderPos) 
	{
    	$prevOrderPos = BITAMQuestionOptionCollection::getCurrentOrderPos($aRepository, $aQuestionID, $aQuestionOptionID);
        
        if ($prevOrderPos != -1) 
        {
            if ($orderPos > $prevOrderPos) 
            {
                $prevOrderPos = $prevOrderPos +1;
                $sql = "UPDATE SI_SV_QAnswers SET SortOrder = SortOrder - 1 
						WHERE ( SortOrder BETWEEN " . $prevOrderPos . " AND " . $orderPos . " ) AND QuestionID = " . $aQuestionID;
                if ($aRepository->DataADOConnection->Execute($sql) === false) 
                {
                    die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_QAnswers " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
                }
            }

            if ($prevOrderPos > $orderPos) 
            {
                $sql = "UPDATE SI_SV_QAnswers SET SortOrder = SortOrder + 1 
						WHERE ( SortOrder BETWEEN " . $orderPos . " AND " . $prevOrderPos . " ) AND QuestionID = " . $aQuestionID;
                if ($aRepository->DataADOConnection->Execute($sql) === false) {
                    die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_QAnswers " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
                }
            }

            $sql = "UPDATE SI_SV_QAnswers SET SortOrder = " . $orderPos . " 
					WHERE QuestionID  = " . $aQuestionID . " AND ConsecutiveID = " . $aQuestionOptionID;
            if ($aRepository->DataADOConnection->Execute($sql) === false) 
            {
                die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_QAnswers " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
            }
            
            //@JAPR 2014-09-25: Corregido un bug, no estaba actualizando la versión de la definición al reordenar elementos
            $mainQuestion = @BITAMQuestion::NewInstanceWithID($aRepository, $aQuestionID);
            if (!is_null($mainQuestion)) {
            	BITAMSurvey::updateLastUserAndDateInSurvey($aRepository, $mainQuestion->SurveyID);
            }
            //@JAPR
        }
    }
    
	static function getCurrentOrderPos($aRepository, $aQuestionID, $aQuestionOptionID) 
	{
        $sql = "SELECT SortOrder FROM SI_SV_QAnswers WHERE QuestionID = " . $aQuestionID . " AND ConsecutiveID = " . $aQuestionOptionID;

        $aRS = $aRepository->DataADOConnection->Execute($sql);

        if (!$aRS) {
            die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_QAnswers " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
        }

        $orderPos = -1;

        if (!$aRS->EOF) 
        {
            $orderPos = (int) $aRS->fields["sortorder"];
        }

        return $orderPos;
    }
}
?>