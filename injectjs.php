<?php
//header("Content-Type: text/html; charset=UTF-8");
function stripAccents($string){
	return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}

$jsfunc = "SectionCls.prototype.setActive = function(aRecordNumber, bChangeActiveData) {
			Debugger.register('Section.setActive ' + this.id + ', aRecordNumber == ' + aRecordNumber + ', bChangeActiveData == ' + bChangeActiveData);
			if (!aRecordNumber || aRecordNumber < 0) {
				aRecordNumber = 0;
			}
			if (bChangeActiveData === undefined) {
				bChangeActiveData = true;
			}
			var changingSection = true;	
			if (this.survey) {
				//Actualiza la página previa para que se pueda hacer el Back
				if (bChangeActiveData && this.survey.previousCapture && this.survey.currentCapture) {
					if(this.survey.previousCapture.section && this.survey.currentCapture.section && this.survey.previousCapture.section.id == this.survey.currentCapture.section.id) {
						changingSection = false;
					}			
					this.survey.previousCapture.section = this.survey.currentCapture.section;
					this.survey.previousCapture.recordNum = this.survey.currentCapture.recordNum;
					//Asigna la pregunta actual como la última capturada en la sección previa
					this.survey.previousCapture.question = this.survey.currentCapture.question;
				}
				
				//Registra la página especificada como la actual
				if (this.survey.currentCapture) {
					if (bChangeActiveData) {
						this.survey.currentCapture.section = this;
						this.survey.currentCapture.recordNum = aRecordNumber;
						this.survey.currentCapture.section.recordNum = aRecordNumber;
						
						//Asigna la primer pregunta visible como la activa
						this.survey.currentCapture.question = undefined;
						if (this.questionsOrder && this.questionsOrder.length) {
							for (var intQuestionNum in this.questionsOrder) {
								var objQuestion = this.questions[this.questionsOrder[intQuestionNum]];
								//La primer pregunta de una sección dinámica o maestro-detalle no puede estar oculta por registro, así que es válido
								//preguntar solo la visibilidad afectada por preguntas externas
								if (objQuestion.isVisible()) {
									this.survey.currentCapture.question = objQuestion;
									break;
								}
							}
						}
					}
					
					//Si es una sección Maestro-Detalle, cambia las respuestas activas de cada pregunta de la sección para que apunten al número de
					//registro especificado, si no había registro entonces crea uno en este momento
					var blnCheckQuestionAnswers = false;
					if (this.type == SectionType.Multiple) {
						//Primero determina si se debe o no mostrar la página de Summary, esto se hace validando si la sección activa es diferente
						//a la sección previa, ya que si fuera la misma y siendo una sección Maestro-Detalle, sólo podría significar que entró aquí
						//para pintar precisamente el Summary o para pintar un registro específico lo cual sucede desde el propio Summary
						if (bChangeActiveData) {
							this.showSummary = false;
							if(!this.summaryInMDSection){
							if (this.survey.previousCapture && this.survey.previousCapture.section && this.survey.previousCapture.section.id != this.id) {
								//Como son secciones diferentes automáticamente mostrará la ventana de Summary, excepto si sólo hubiera hasta un registro,
								//en cuyo caso muestra directamente a dicho registro
								//JAPR 2012-11-29: Modificado para que ahora se verifique si está o no vacia la sección para determinar
								//si muestra o no el Summary
								//if (this.maxRecordNum > 1) {
								if (!this.empty) {
									this.showSummary = true;
								}
									
								}
							}
							else { //si esta summaryInMDSection entonces hay que poner el record num en cero
								if(this.maxRecordNum>0 && !this.newRecord){ //record num mayor a cero
									if (changingSection) {
										this.recordNum = 0;
									}
									this.newRecord = false;
								}
								else {
									this.newRecord = true;
								
								}
							}
						}
						blnCheckQuestionAnswers = true;
					}
					
					//Si es una sección Dinámica, debe reconstruir las opciones de las preguntas que pertenecen a ella antes de pintar la sección
					//JAPR 2014-05-20: Agregado el tipo de sección Inline
					if (this.type == SectionType.Dynamic || this.type == SectionType.Inline) {
						blnCheckQuestionAnswers = true;
					}
					
					//Tanto en secciones Dinámicas como Maestro-Detalle debe actualizar la respuesta activa al número de registro activo o bien crearla
					//si es que no se había procesado aún
					if (blnCheckQuestionAnswers) {
						if (this.questionsOrder && this.questionsOrder.length) {
							var blnAddRecord = false;
							
							for (var intQuestionNum in this.questionsOrder) {
								var objQuestion = this.questions[this.questionsOrder[intQuestionNum]];
								if (objQuestion.answers) {
									if (!objQuestion.answers[aRecordNumber]) {
										//JAPRWarning: Posiblemente sea mejor cambiar esta validación para comparar contra this.maxRecordNum, ya que es
										//mas fácil que se pueda desincronizar la cantidad de respuestas de una pregunta respecto a las demás, a que 
										//ocurra un error por intentar accesar a un answer con un índice que no existe si se usa maxRecordNum, para 
										//asegurarse se pueden usar ambos acercamientos (validar por maxRecordNum - sólo si no es una dinámica, ya que
										//en ese caso maxRecordNum es fijo - y aparte crear si no existe)
										
										//La respuesta de ese registro no existe, así que la genera y apunta a ella, si fuera mayor al máximo actual
										//entonces simplemente agrega un nuevo registro con el índice máximo
										blnAddRecord = true;
										//JAPR 2012-11-30: Mejor en caso de inconsistencia de respuestas, de todas maneras usará
										//el número de registro pedido porque al final quien invoca a este método eso es lo que buscará
										//aRecordNumber = objQuestion.answers.length;
										objQuestion.answers[aRecordNumber] = new QuestionAnswerCls(objQuestion.id);
										//A los nuevos elementos tiene que asignar la referencia del catalogFilter de la primer respuesta para que se
										//aplique el mismo filtro a todos los registros nuevos
										//JAPR 2012-11-16: Corregido un bug, si la pregunta en si no tiene una padre del mismo catálogo, entonces esta asignación
										//sería incorrecta porque la jerarquía inicia dentro de esta sección y si se hace una referencia entonces cada registro
										//tendría el mismo valor
										if (objQuestion.fatherQuestionID) {
											//JAPR 2013-02-15: Corregido un bug, si la pregunta es de catálogo pero tiene varios atributos, su propio catalogFilter
											//contiene los valores de los atributos adicionales que ella misma captura, si se sobreescribiera con el catalogFilter
											//de la pregunta padre ademas de perder esa información, en secciones múltiples todas quedarían apuntando al mismo lugar,
											//sin embargo si llega a esta parte quiere decir que es un registro multiple que apenas se está agregando, así que
											//no estaba respondido, en este caso si es válido sobreescribir con el mismo filtro de la pregunta padre ya que el
											//registro está en blanco
											var objFatherQuestion = this.survey.questions[objQuestion.fatherQuestionID];
											if (objFatherQuestion) {
												$.extend(objQuestion.answers[aRecordNumber].catalogFilter, objFatherQuestion.answer.catalogFilter);
												var intAttribNum = Object.keys(objFatherQuestion.answer.catalogFilter).length;
												objQuestion.updateValueByFilter(intAttribNum, aRecordNumber, false, objFatherQuestion.answer.catalogFilter);
											}
											//objQuestion.answers[aRecordNumber].catalogFilter = objQuestion.answers[0].catalogFilter;
											//JAPR
										}
										
										//Agrega propiedades adicionales según el tipo de pregunta
										//JAPR 2014-01-14: Corregido un bug, se usaba referencia a this en lugar de a la pregunta
										objQuestion.answers[aRecordNumber].addProperties(objQuestion.type);
										//JAPR
									}
									
									//La respuesta ya existe para ese registro (o acaba de ser creada), así que apunta a ella
									objQuestion.answer = objQuestion.answers[aRecordNumber];
									//Actualiza el objeto global ya que el reemplazo de variable se hace desde él
								}
								
								//JAPR 2014-05-20: Agregado el tipo de sección Inline
								var intSectionType = this.getBehaviourType();
								
								//Si es una sección Dinámica entonces reconstruye las opciones de la pregunta
								//JAPR 2014-05-20: Agregado el tipo de sección Inline
								if (intSectionType == SectionType.Dynamic) {
									objQuestion.generate();
								}
							}
							
							switch (this.type) {
								case SectionType.Multiple:
									//Si se agregó un nuevo registro incrementa el número total
									if (blnAddRecord) {
										this.maxRecordNum++;
										selSurvey.currentCapture.maxRecordNum = this.maxRecordNum;
										//Registra en LocalStorage los datos de la sección que se está capturando
										if (objSettings && objSettings.lsActive && objSettings.lsActive()) {
											window.localStorage.setItem(lsPrefix+'Section'+this.id, this.maxRecordNum);
										}
									}
									break;
							}
						}
					}
					
					//JAPR 2014-03-20: Agregada la asignación de valores vía un lector de código de barras
					//Al entrar a una nueva sección, si la pregunta en la que se posiciona es de captura mediante un cuadro de texto, coloca el foco en
					//el para permitir la captura continua (esto es especialmente útil si viene de una lectura de código de barras y acaba de cambiar de
					//sección justo después de leer el código anterior)
					delete this.survey.currentCapture.forceQuestionFocus;
					if (bChangeActiveData && this.survey.currentCapture.question) {
						switch (this.survey.currentCapture.question.type) {
							case Type.multiplechoice:
								switch (this.survey.currentCapture.question.mcInputType) {
									case MCInputType.mpcNumeric:
									case MCInputType.mpcText:
										this.survey.currentCapture.forceQuestionFocus = true;
										break;
								}
								break;
							case Type.number:
							//case Type.date:
							case Type.text:
							case Type.alfanum:
							//case Type.action:
							//case Type.time:
							case Type.password:
								this.survey.currentCapture.forceQuestionFocus = true;
								break;
						}
					}
					//JAPR
				}
			}
		}
	}";

	$jsfuncpart = "function jsfun (aRecordNumber, bChangeActiveData) {
			Debugger.register('Section.setActive ' + this.id + ', aRecordNumber == ' + aRecordNumber + ', bChangeActiveData == ' + bChangeActiveData);
			if (!aRecordNumber || aRecordNumber < 0) {
				aRecordNumber = 0;
			}
			if (bChangeActiveData === undefined) {
				bChangeActiveData = true;
			}
			var changingSection = true;	
			if (this.survey) {
				//Actualiza la página previa para que se pueda hacer el Back
			}
		}";
	
$replacingJumplines = str_replace("\r\n", " \\\n", $jsfuncpart);
$escapingSlashes = str_replace("/", "\/", $replacingJumplines);
$escapingQuotations = str_replace("'", "\\'", $escapingSlashes);
$removingAccents = stripAccents($escapingQuotations);
$newSetActive = $removingAccents;
?>

<br>
<h2>ABC</h2>

<script>
	console.log(eval("<?=$newSetActive?>"));
	//console.log(eval("jsfunc()"));
</script>