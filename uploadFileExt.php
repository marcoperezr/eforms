<?php
global $garrInvalidExt;

if ( !isset($garrInvalidExt) ) {
	$garrInvalidExt = array();
}

$garrInvalidExt["APK"] = "";
$garrInvalidExt["APP"] = "";
$garrInvalidExt["BAT"] = "";
$garrInvalidExt["BIN"] = "";
$garrInvalidExt["CMD"] = "";
$garrInvalidExt["COM"] = "";
$garrInvalidExt["COMMAND"] = "";
$garrInvalidExt["CPL"] = "";
$garrInvalidExt["CSH"] = "";
$garrInvalidExt["EXE"] = "";
$garrInvalidExt["GADGET"] = "";
$garrInvalidExt["INF1"] = "";
$garrInvalidExt["INS"] = "";
$garrInvalidExt["INX"] = "";
$garrInvalidExt["IPA"] = "";
$garrInvalidExt["ISU"] = "";
$garrInvalidExt["JOB"] = "";
$garrInvalidExt["JSE"] = "";
$garrInvalidExt["KSH"] = "";
$garrInvalidExt["LNK"] = "";
$garrInvalidExt["MSC"] = "";
$garrInvalidExt["MSI"] = "";
$garrInvalidExt["MSP"] = "";
$garrInvalidExt["MST"] = "";
$garrInvalidExt["OSX"] = "";
$garrInvalidExt["OUT"] = "";
$garrInvalidExt["PAF"] = "";
$garrInvalidExt["PHP"] = "";
$garrInvalidExt["PIF"] = "";
$garrInvalidExt["PRG"] = "";
$garrInvalidExt["PS1"] = "";
$garrInvalidExt["REG"] = "";
$garrInvalidExt["RGS"] = "";
$garrInvalidExt["RUN"] = "";
$garrInvalidExt["SCT"] = "";
$garrInvalidExt["SHB"] = "";
$garrInvalidExt["SHS"] = "";
$garrInvalidExt["U3P"] = "";
$garrInvalidExt["VB"] = "";
$garrInvalidExt["VBE"] = "";
$garrInvalidExt["VBS"] = "";
$garrInvalidExt["VBSCRIPT"] = "";
$garrInvalidExt["WORKFLOW"] = "";
$garrInvalidExt["WS"] = "";
$garrInvalidExt["WSF"] = "";

global $garrValidImagesExt;
if ( !isset($garrValidImagesExt) ) {
	$garrValidImagesExt = array();
}

$garrValidImagesExt["JPG"] = "JPG";
$garrValidImagesExt["GIF"] = "GIF";
$garrValidImagesExt["PNG"] = "PNG";

if (!function_exists('isValidUploadExtension')) {
	/* Verifica si la extensión del archivo provisto es válida (el archivo puede incluir todo el path a donde se encuentra) según el array de extensiones permitidas
	definido. El parámetro $bJustExtension indica que el parámetro $sFilename representa solo la extensión a verificar en lugar de un nombre de archivo completo */
	function isValidUploadExtension($sFilename, $bJustExtension = false) {
		global $garrInvalidExt;
		
		$blnValidFile = false;
		
		if ( !$sFilename ) {
			$sFilename = '';
		}
		
		if ( $bJustExtension ) {
			if ( !isset($garrInvalidExt[strtoupper($sFilename)]) ) {
				$blnValidFile = true;
			}
		}
		else {
			$infoFile = @pathinfo($sFilename);
			if ( $infoFile ) {
				$strExtension = strtoupper($infoFile["extension"]);
				if ( !isset($garrInvalidExt[$strExtension]) ) {
					$blnValidFile = true;
				}
			}
		}
		
		return $blnValidFile;
	}
}

if (!function_exists('replaceInvalidFilenameChars')) {
	/* Reemplaza caracters inválidos o que pueden permitir hacks de nombres de archivos que se están intentando subir al servidor */
	function replaceInvalidFilenameChars($sFilename) {
		$sFilename = str_ireplace(chr(0), "", $sFilename);
		
		return $sFilename;
	}
}

if (!function_exists('getExtendedFileInfo')) {
	/* Dado un nombre de archivo con todo y su ruta, utiliza la función pathinfo para obtener las diferentes partes del archivo y regresar un array procesado
	para ser utilizado en diferentes validaciones */
	function getExtendedFileInfo($sFilename) {
		$arrFileInfo = array();
		
		try {
			$pathinfo = pathinfo($sFilename);
			
			$arrFileInfo["dirName"] = strtolower((string) @$pathinfo['dirname']);
			$arrFileInfo["fileName"] = (string) @$pathinfo['basename'];
			$arrFileInfo["fileNameWithoutExt"] = (string) @$pathinfo['filename'];
			$arrFileInfo["fileExtension"] = strtolower((string) @$pathinfo['extension']);
		} catch (Exception $e) {
		}
		
		return $arrFileInfo;
	}
}

if (!function_exists('checkValidExtension')) {
	/* Verifica si la extensión indicada es válida según el array de extensiones permitidas */
	function checkValidExtension($ext, $allowedExtensions = null, $blnCheckImages = false) {
		global $garrValidImagesExt;
		
		if (!is_array($allowedExtensions)) {
			if ( $blnCheckImages ) {
				//Si se marcó la bandera para verificar imágenes, utiliza el array default de imágenes en caso de no haber recibido alguno
				$allowedExtensions = array_values($garrValidImagesExt);
			}
			else {
				return false;
			}
		}
		
		if (!in_array(strtolower($ext), array_map('strtolower', $allowedExtensions))) {
			return false;
		}
		
		//Valida que la extensión no se encuentre entre las inválidas configuradas
		if ( !isValidUploadExtension($ext, true) ) {
			return false;
		}
		
		return true;
	}
}
?>