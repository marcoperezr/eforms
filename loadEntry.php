<?php
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
session_start();
//@JAPR 2015-02-05: Agregado soporte para php 5.6
if (isset($_SESSION["PABITAM_RepositoryName"]) && isset($_SESSION["PABITAM_UserName"])) {
    require_once("repository.inc.php");
    require_once('config.php');
    require_once('cubeClasses.php');

    global $BITAMRepositories;
	//@JAPR 2013-06-27: Agregada la integración con IExplorer desde edición y captura vía EMail
	global $strWebEntryURL;
	$strWebEntryURL = @curPageURL();
	//@JAPR
	
    $url = "build_page.php";

    $theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
    if (!array_key_exists($theRepositoryName, $BITAMRepositories)) {
        header("Location: index.php?error=1");
        exit();
    }

    $theRepository = $BITAMRepositories[$theRepositoryName];
    if (!$theRepository->open()) {
        header("Location: index.php?error=2");
        exit();
    }

    $theUserName = $_SESSION["PABITAM_UserName"];
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
    $theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);

    if (is_null($theUser)) {
        header("Location: index.php?error=3");
        exit();
    }

    //Cargar diccionario del lenguage a utilizar
    if (array_key_exists("PAuserLanguage", $_SESSION)) {
        InitializeLocale($_SESSION["PAuserLanguageID"]);
        LoadLanguageWithName($_SESSION["PAuserLanguage"]);
    } else { //English
        InitializeLocale(2);
        LoadLanguageWithName("EN");
    }

    $surveyID = 0;
    if (array_key_exists("surveyID", $_GET)) {
        $surveyID = $_GET["surveyID"];
    }

    $entryID = 0;
    if (array_key_exists("EntryID", $_GET)) {
        $entryID = $_GET["EntryID"];
    }

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
    //session_register("SVFromESurveyAdmin");
    $_SESSION["SVFromESurveyAdmin"] = 1;

    require_once("survey.inc.php");

    $surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $surveyID);
    
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
    /*session_register("SVExternalEmailFilter");
    session_register("SVExternalSchedulerID");
    session_register("SVExternalCatalogID");
    session_register("SVExternalEmail");
    session_register("SVExternalEmailKey");*/
    $_SESSION["SVExternalEmailFilter"] = "";
    $_SESSION["SVExternalSchedulerID"] = "";
    $_SESSION["SVExternalCatalogID"] = "";
    $_SESSION["SVExternalEmail"] = "";
    $_SESSION["SVExternalEmailKey"] = "";

    //sacar de la tabla de hechos el valor de DimEmail y DimScheduler
    //ya que tengo el entryid
    $dimEmail = "RIDIM_" . $surveyInstance->EmailDimID;
    $dimEmailKey = $dimEmail . "KEY";
    $dimScheduler = "RIDIM_" . $surveyInstance->SchedulerDimID;
    $dimSchedulerKey = $dimScheduler . "KEY";
    $factTable = "RIFACT_" . $surveyInstance->ModelID;
	
    $sql = "SELECT " . $dimEmailKey . ", " . $dimSchedulerKey . " FROM " . $factTable . " WHERE RIDIM_".$surveyInstance->FactKeyDimID."KEY = " . $entryID;
    $aRS = $theRepository->DataADOConnection->Execute($sql);
	
    if ($aRS === false) {
        die(translate("Error accessing") . " SI_SV_Catalog, SI_DESCRIP_ENC " . translate("table") . ": " . $theRepository->ADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
    }
    if (!$aRS->EOF) {
        $emailKeyVal = (int) $aRS->fields[$dimEmailKey];
        $schedulerKeyVal = (int) $aRS->fields[$dimSchedulerKey];
    }
    
    //Verificamos si es valor de No Aplica el valor de Scheduler Key
    if($schedulerKeyVal!=1)
    {
	    //obtengo el schedulerid de la tabla del DimScheduler
	    $sql = "SELECT KEY_" . $surveyInstance->SchedulerDimID . " FROM " . $dimScheduler . " WHERE " . $dimSchedulerKey . " = " . $schedulerKeyVal;
	
	    $aRS = $theRepository->DataADOConnection->Execute($sql);
	
	    if ($aRS === false) {
	        die(translate("Error accessing") . " ".$dimScheduler." " . translate("table") . ": " . $theRepository->ADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
	    }
	
	    if (!$aRS->EOF) {
	        $schedulerID = (int) $aRS->fields["KEY_" . $surveyInstance->SchedulerDimID];
	    }
	
	    //creamos instancia de Scheduler
	    $schedulerInstance = BITAMSurveyScheduler::NewInstanceWithID($theRepository, $schedulerID);
	
	    //obtenemos el email de la tabla de dim
	    $sql = "SELECT ".$dimEmailKey.", DSC_" . $surveyInstance->EmailDimID." FROM ".$dimEmail." WHERE ".$dimEmailKey." = ".$emailKeyVal;
	
	    $aRS = $theRepository->DataADOConnection->Execute($sql);
	
	    if ($aRS === false) {
	        die(translate("Error accessing") . " ".$dimScheduler." " . translate("table") . ": " . $theRepository->ADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
	    }
	
	    if (!$aRS->EOF) {
	        $captureEmail = $aRS->fields["DSC_" . $surveyInstance->EmailDimID];
	        $captureEmailKey = $aRS->fields[$dimEmailKey];
	    }
	    
		if(is_null($schedulerInstance))
		{
			$strError = "The scheduler of the survey does not exist";
			$pageName = "genericErrorPage.php?StrError=".urlencode($strError);
			exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=".$pageName."\">\n</head>\n<body>\n</body>\n</html>\n");
		}

	    //verificamos si tiene catalogo el scheduler y que tenga filter per row o por email
	    if ($schedulerInstance->UseCatalog == 1) {
	        
	        //sacamos el emailfilter
	        $emailFilter = $schedulerInstance->EmailFilter;
	             
	        //obtenemos el valor guardado del catalogo para ese registro
	        $sql = "SELECT TableName FROM SI_SV_Catalog WHERE CatalogID = ".$schedulerInstance->CatalogID;
	        
	        $aRS = $theRepository->DataADOConnection->Execute($sql);
	
	        if ($aRS === false) {
	            die(translate("Error accessing") . " ".$dimScheduler." " . translate("table") . ": " . $theRepository->ADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
	        }
	
	        if (!$aRS->EOF) {
	            $catalogTableName = $aRS->fields["tablename"];
	        }
	        
	        //obtenemos el captureemailkey de la tabla de hechos
	        $sql = "SELECT " . $catalogTableName . "KEY FROM " . $factTable . " WHERE RIDIM_".$surveyInstance->FactKeyDimID."KEY = " . $entryID;
	        
	        $aRS = $theRepository->DataADOConnection->Execute($sql);
	
	        if ($aRS === false) {
	            die(translate("Error accessing") . " ".$dimScheduler." " . translate("table") . ": " . $theRepository->ADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
	        }
	
	        if (!$aRS->EOF) {
	            $captureEmailKey = $aRS->fields[$catalogTableName . "KEY"];
	        }
	        
	        //validamos email filter
	        switch ($emailFilter) {
	            case 0:
                    //Verificamos si existe informacion en la encuesta para el email y scheduler que se indica
                    $arrayData = BITAMSurvey::hasSurveyDataInThisSchedEmail($theRepository, $schedulerID, $captureEmail);
                    break;
	            case 1:
					//@JAPR 2015-02-09: Agregado soporte para php 5.6
                    /*session_register("SVExternalEmailFilter");
                    session_register("SVExternalSchedulerID");
                    session_register("SVExternalCatalogID");
                    session_register("SVExternalEmail");
                    session_register("SVExternalEmailKey");*/
                    $_SESSION["SVExternalEmailFilter"] = $schedulerInstance->EmailFilter;
                    $_SESSION["SVExternalSchedulerID"] = $schedulerID;
                    $_SESSION["SVExternalCatalogID"] = $schedulerInstance->CatalogID;
                    $_SESSION["SVExternalEmail"] = $captureEmail;
                    $_SESSION["SVExternalEmailKey"] = $captureEmailKey;
                    
                    //Verificamos si existe informacion en la encuesta para el email y scheduler que se indica
                    $arrayData = BITAMSurvey::hasSurveyDataInThisSchedEmailKey($theRepository, $schedulerID, $captureEmail, $captureEmailKey);
                    break;
	            case 2:
					//@JAPR 2015-02-09: Agregado soporte para php 5.6
                    /*session_register("SVExternalEmailFilter");
                    session_register("SVExternalSchedulerID");
                    session_register("SVExternalCatalogID");
                    session_register("SVExternalEmail");
                    session_register("SVExternalEmailKey");*/
                    $_SESSION["SVExternalEmailFilter"] = $schedulerInstance->EmailFilter;
                    $_SESSION["SVExternalSchedulerID"] = $schedulerID;
                    $_SESSION["SVExternalCatalogID"] = $schedulerInstance->CatalogID;
                    $_SESSION["SVExternalEmail"] = $captureEmail;
                    $_SESSION["SVExternalEmailKey"] = 0;
					
                    //Verificamos si existe informacion en la encuesta para el email y scheduler que se indica
                    $arrayData = BITAMSurvey::hasSurveyDataInThisSchedEmail($theRepository, $schedulerID, $captureEmail);
                    break;
	        }
	    }
	    else
	    {
	        $arrayData = BITAMSurvey::hasSurveyDataInThisSchedEmail($theRepository, $schedulerID, $captureEmail);
	    }
	        
	    if(is_null($arrayData)) {
	        $strError = "You can not access this survey '".$surveyInstance->SurveyName."'";
	        $pageName = "genericErrorPage.php?StrError=".urlencode($strError);
	        exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=".$pageName."\">\n</head>\n<body>\n</body>\n</html>\n");
	    }
	}

    $arrayDateAndUser = BITAMSurvey::getDateAndUserFromEntryID($theRepository, $surveyInstance, $entryID);
    $dateID = (string) @$arrayDateAndUser["DateID"];
    $endDateID = (string) @$arrayDateAndUser["EndDateID"];
    if (trim($endDateID) == '') {
    	$endDateID = $dateID;
    }
    $hourID = (string) @$arrayDateAndUser["HourID"];
    $userID = (string) @$arrayDateAndUser["UserID"];
    $endHourID = (string) @$arrayDateAndUser["EndTime"];
    $latitude = (string) @$arrayDateAndUser["Latitude"];
    $longitude = (string) @$arrayDateAndUser["Longitude"];
	//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
    $accuracy = (string) @$arrayDateAndUser["Accuracy"];
} else {
	//@JAPR 2012-11-28: Validado que si perdió la sesión al intentar editar, entonces reenvie al login de kpionline
    exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=loadKPILogin.php\">\n</head>\n<body>\n</body>\n</html>\n");
}
?>
<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>KPIOnline Forms</title>
    </head>
    <body style="background-color:#f5f5f5">
        <form id="frmLoadEntry" name="frmLoadEntry" method="POST" action="generateSurvey.php?surveyID=<?= $surveyID ?>">
            <input type="hidden" id="DateID" name="DateID" value="<?= $dateID ?>"/>
            <input type="hidden" id="EndDateID" name="DateID" value="<?= $endDateID ?>"/>
            <input type="hidden" id="HourID" name="HourID" value="<?= $hourID ?>"/>
            <input type="hidden" id="EndHourID" name="EndHourID" value="<?= $endHourID ?>"/>
            <input type="hidden" id="Latitude" name="Latitude" value="<?= $latitude ?>"/>
            <input type="hidden" id="Longitude" name="Longitude" value="<?= $longitude ?>"/>
            <input type="hidden" id="Accuracy" name="Accuracy" value="<?= $accuracy ?>"/>
            <input type="hidden" id="ApplicatorID" name="ApplicatorID" value="<?= $userID ?>"/>
            <input type="hidden" id="EntryID" name="EntryID" value="<?= $entryID ?>"/>
            <input type="hidden" id="WebEntryURL" name="WebEntryURL" value="<?=$strWebEntryURL?>"/>
        </form>
        <script language="JavaScript">
            frmLoadEntry.submit();
        </script>
    </body>
</html>