<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("appuser.inc.php");
require_once("survey.inc.php");
require_once("artusreporter.inc.php");

class BITAMSurveyScheduler extends BITAMObject
{
	public $SchedulerID;
	public $SurveyID;
	public $SchedulerName;
	public $AllowMultipleSurveys;
	public $CaptureStartDate;
	public $CaptureEndDate;
	public $WhenToCaptureType;
	public $WhenToCaptureValue;
	public $CaptureVia;
	public $UseCatalog;
	public $CatalogID;
	public $AttributeID;
	public $EmailFilter;
	public $CaptureEmails;
	public $StrCaptureEmails;
	public $EmailSubject;
	public $EmailBody;
	public $StartDay;
	public $Duration;
	public $UserIDs;
	public $UsrRoles;
	public $StrUserRol;
	public $TempWeek;
	public $TempMonth;
	public $TempTwoMonths;
	public $TempQuarter;
	public $EmailReplyTo; //Conchita 10-nov-2011
	
	public $EditorType;
	public $EmailBodyDes;
	public $EmailBodyDesHTML;
	
	function __construct($aRepository)
	{
		global $gbIsGeoControl;
		
		BITAMObject::__construct($aRepository);
		$this->SchedulerID = -1;
		$this->SurveyID = -1;
		$this->SchedulerName= "";
		//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
		$this->AllowMultipleSurveys = ($gbIsGeoControl)?0:1;
		//@JAPR
		$this->CaptureStartDate= "";
		$this->CaptureEndDate= "";
		$this->WhenToCaptureType=0;
		$this->WhenToCaptureValue="";
		
		$this->CaptureVia = 0;
		$this->UseCatalog = 0;
		$this->CatalogID = 0;
		$this->AttributeID = 0;
		$this->EmailFilter = 0;
		$this->CaptureEmails = array();
		$this->StrCaptureEmails = "";
		$this->EmailSubject = "";
		$this->EmailBody = "";
		$this->EmailReplyTo=""; //Conchita 10-nov-2011
		$this->StartDay = 0;
		$this->Duration = 0;
		$this->UserIDs = array();
		$this->UsrRoles = array();
		$this->StrUserRol="";
		$this->TempWeek = array();
		$this->TempMonth = 0;
		$this->TempTwoMonths = 0;
		$this->TempQuarter = 0;

		$this->EditorType=1;
		$this->EmailBodyDes='';
		$this->EmailBodyDesHTML='';

	}

	static function NewInstance($aRepository)
	{
		return new BITAMSurveyScheduler($aRepository);
	}

	static function NewInstanceWithID($aRepository, $aSchedulerID)
	{
		$anInstance = null;
		
		if (((int)  $aSchedulerID) < 0)
		{
			return $anInstance;
		}
		
		//Sep-2014
		$strFieldsByVersion="";
		if (getMDVersion() >= esvEditorHTML)
		{
			$strFieldsByVersion .= ', t1.EditorType, t1.EmailBodyDes ';
		}
				
		//Conchita 10-nov-2011 Agregado EmailReplyTo
		$sql = "SELECT t1.SchedulerID, t1.SurveyID, t1.SchedulerName, t1.AllowMultipleSurveys, t1.CaptureStartDate, t1.CaptureEndDate, t1.CaptureType, t1.WhenToCapture,
				t1.CaptureVia, t1.UseCatalog, t1.CatalogID, t1.AttributeID, t1.EmailFilter, t1.EmailSubject, t1.EmailBody,
				t1.StartDay, t1.Duration,t1.EmailReplyTo ".$strFieldsByVersion."
				FROM SI_SV_SurveyScheduler t1 WHERE t1.SchedulerID = ".((int)$aSchedulerID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$anInstance = BITAMSurveyScheduler::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMSurveyScheduler::NewInstance($aRepository);
		$anInstance->SchedulerID = (int) $aRS->fields["schedulerid"];
		$anInstance->SurveyID = (int) $aRS->fields["surveyid"];
		$anInstance->SchedulerName = $aRS->fields["schedulername"];
		$anInstance->AllowMultipleSurveys = (int)$aRS->fields["allowmultiplesurveys"];
		
		if(!is_null($aRS->fields["capturestartdate"]) && trim($aRS->fields["capturestartdate"])!="")
		{
			$anInstance->CaptureStartDate = substr($aRS->fields["capturestartdate"], 0, 10);
		}

		if(!is_null($aRS->fields["captureenddate"]) && trim($aRS->fields["captureenddate"])!="")
		{
			$anInstance->CaptureEndDate = substr($aRS->fields["captureenddate"], 0, 10);
		}

		$anInstance->WhenToCaptureType=(int) $aRS->fields["capturetype"];
		$anInstance->WhenToCaptureValue= $aRS->fields["whentocapture"];
		
		$anInstance->CaptureVia = (int) $aRS->fields["capturevia"];
		$anInstance->UseCatalog = (int) $aRS->fields["usecatalog"];
		$anInstance->CatalogID = (int) $aRS->fields["catalogid"];
		$anInstance->AttributeID = (int) $aRS->fields["attributeid"];
		$anInstance->EmailFilter = (int) $aRS->fields["emailfilter"];
		$anInstance->EmailSubject = $aRS->fields["emailsubject"];
		$anInstance->EmailBody = $aRS->fields["emailbody"];
		$anInstance->EmailReplyTo = $aRS->fields["emailreplyto"]; //Conchita 10-nov-2011
		$anInstance->StartDay = (int)$aRS->fields["startday"];
		$anInstance->Duration = (int)$aRS->fields["duration"];
		
		//Llenar valores temporales de la instancia
		switch($anInstance->WhenToCaptureType)
		{
			case 1:
				if(trim($anInstance->WhenToCaptureValue)!="")
				{
					$anInstance->TempWeek = explode(",", $anInstance->WhenToCaptureValue);
				}
				break;
			
			case 2:
				$anInstance->TempMonth = (int)$anInstance->WhenToCaptureValue;
				
				break;
			
			case 3:
				$anInstance->TempTwoMonths = (int)$anInstance->WhenToCaptureValue;

				break;
			
			case 4:
				$anInstance->TempQuarter = (int)$anInstance->WhenToCaptureValue;
				break;
		}
	
		//Obtener los usuarios a los cuales se les permite acceder a esta encuesta
		$anInstance->readUserIDs();
		
		//Obtener los roles a los cuales se les permite acceder a esta encuesta
		$anInstance->readUsrRoles();

		//Obtiene la cadena que concatena los usuarios y los roles
		$anInstance->readStrUserRol();
		
		//Obtener los captureEmails siempre y cuando los correos no se extrajeron desde un catalogo
		$anInstance->readCaptureEmails();
		
		$anInstance->EditorType = (int) @$aRS->fields["editortype"];
		$anInstance->EmailBodyDes = trim(@$aRS->fields["emailbodydes"]);
		$anInstance->EmailBodyDesHTML = trim(@$aRS->fields["emailbody"]);

		return $anInstance;
	}
	
	function readUserIDs()
	{
		$this->UserIDs = array();
		$sql = "SELECT UserID FROM SI_SV_SurveySchedulerUser WHERE SchedulerID = ".$this->SchedulerID;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySchedulerUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$this->UserIDs[] = (int)$aRS->fields["userid"];
			$aRS->MoveNext();
		}
	}
	
	function readUsrRoles()
	{
		$this->UsrRoles = array();
		$sql = "SELECT RolID FROM SI_SV_SurveySchedulerRol WHERE SchedulerID = ".$this->SchedulerID;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySchedulerRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$this->UsrRoles[] = (int)$aRS->fields["rolid"];
			$aRS->MoveNext();
		}
	}
	
	function readCaptureEmails()
	{
		if($this->CaptureVia == 1 && $this->UseCatalog == 0)
		{
			$this->StrCaptureEmails = "";
			$this->CaptureEmails = array();
			$sql = "SELECT Email FROM SI_SV_SchedulerEmail WHERE SchedulerID = ".$this->SchedulerID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SchedulerEmail ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while(!$aRS->EOF)
			{
				$this->CaptureEmails[] = $aRS->fields["email"];
				$aRS->MoveNext();
			}
			
			$LN = chr(13).chr(10);
			$this->StrCaptureEmails = implode($LN, $this->CaptureEmails);
		}
	}

	function readStrUserRol()
	{
		//Juntamos usuarios y roles en una sola cadena
		$strUsers = "";
		$strRoles = "";
		
		$UserIDs = array();
		$UsrRoles = array();
		
		foreach ($this->UserIDs as $element)
		{
			$UserIDs[] = 'User_'.$element;
		}
		
		foreach ($this->UsrRoles as $element)
		{
			$UsrRoles[] = 'Rol_'.$element;
		}
		
		if(count($UserIDs)>0)
		{
			$strUsers = implode("_AWElem_", $UserIDs);
		}
		
		if(count($UsrRoles)>0)
		{
			$strRoles = implode("_AWElem_", $UsrRoles);
		}
		
		$this->StrUserRol = $strUsers;
		
		if(trim($this->StrUserRol)!="")
		{
			if(trim($strRoles)!="")
			{
				$this->StrUserRol.="_AWElem_";
			}
		}
		
		$this->StrUserRol.=$strRoles;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("SchedulerID", $aHTTPRequest->POST))
		{
			$aSchedulerID = $aHTTPRequest->POST["SchedulerID"];
			
			if (is_array($aSchedulerID))
			{
				
				$aCollection = BITAMSurveySchedulerCollection::NewInstance($aRepository, $aSchedulerID);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMSurveyScheduler::NewInstanceWithID($aRepository, (int)$aSchedulerID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMSurveyScheduler::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMSurveyScheduler::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("SchedulerID", $aHTTPRequest->GET))
		{
			$aSchedulerID = $aHTTPRequest->GET["SchedulerID"];

			$anInstance = BITAMSurveyScheduler::NewInstanceWithID($aRepository, (int)$aSchedulerID);

			if (is_null($anInstance))
			{
				$anInstance = BITAMSurveyScheduler::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMSurveyScheduler::NewInstance($aRepository);
		}
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("SchedulerID", $anArray))
		{
			$this->SchedulerID = (int)$anArray["SchedulerID"];
		}

		if (array_key_exists("SurveyID", $anArray))
		{
			$this->SurveyID = (int)$anArray["SurveyID"];
		}
		
	 	if (array_key_exists("SchedulerName", $anArray))
		{
			$this->SchedulerName = rtrim($anArray["SchedulerName"]);
		}
		
		/****************************************************************/
		if(array_key_exists("CaptureVia", $anArray)) 
		{
			$this->CaptureVia = (int) $anArray["CaptureVia"];
		
			//Capture by Email
			if($this->CaptureVia == 1) 
			{
				$this->CaptureStartDate = $anArray["CaptureStartDate"];
				$this->CaptureEndDate = $anArray["CaptureEndDate"];
				$this->EmailSubject = $anArray["EmailSubject"];
				$this->EmailBody = $anArray["EmailBody"];
				$this->UseCatalog = (int) $anArray["UseCatalog"];
				$this->EmailReplyTo = $anArray["EmailReplyTo"]; //Conchita 10-nov-2011
				if($this->UseCatalog==1)
				{
					$this->StrCaptureEmails = "";
					$this->CatalogID = (int) $anArray["CatalogID"];
					$this->AttributeID = (int) $anArray["AttributeID"];
					$this->EmailFilter = (int) $anArray["EmailFilter"];
				}
				else 
				{
					$this->StrCaptureEmails = $anArray["StrCaptureEmails"];
					$this->CatalogID = 0;
					$this->AttributeID = 0;
					$this->EmailFilter = 0;
				}
				
				$this->StrUserRol = "";
			}
			else 
			{
				$this->StrCaptureEmails = "";
				$this->EmailSubject = "";
				$this->EmailBody = "";
				$this->UseCatalog = 0;
				$this->CatalogID = 0;
				$this->AttributeID = 0;
				$this->EmailReplyTo =""; //Conchita 10-nov-2011
				$this->StrUserRol = $anArray["StrUserRol"];
			}
			
			$this->AllowMultipleSurveys = (int)$anArray["AllowMultipleSurveys"];
			
			if($this->AllowMultipleSurveys==1)
			{
				$this->WhenToCaptureType = (int)$anArray["WhenToCaptureType"];
				
				switch ($this->WhenToCaptureType) 
				{
					case 0:
						$this->CaptureStartDate = $anArray["CaptureStartDate"];
						$this->CaptureEndDate = $anArray["CaptureEndDate"];
						$this->WhenToCaptureValue = "";
						$this->StartDay = 0;
						$this->Duration = 0;
						
						break;
					case 1:
						$this->CaptureStartDate = $anArray["CaptureStartDate"];
						$this->CaptureEndDate = $anArray["CaptureEndDate"];
						
						if (array_key_exists("TempWeek", $anArray))
						{
							$this->TempWeek = $anArray["TempWeek"];
						}
						else
						{
							if (array_key_exists("TempWeek_EmptySelection", $anArray))
							{
				 				if ((bool)$anArray["TempWeek_EmptySelection"])
				 				{
				 					$this->TempWeek = array();
				 				}
							}
						}

						$this->WhenToCaptureValue = implode(",", $this->TempWeek);
						$this->StartDay = 0;
						$this->Duration = 0;
						
						break;
					case 2:
						$this->CaptureStartDate = $anArray["CaptureStartDate"];
						$this->CaptureEndDate = $anArray["CaptureEndDate"];
						$this->TempMonth = $anArray["TempMonth"];
						$this->WhenToCaptureValue = $this->TempMonth;
						$this->StartDay = 0;
						
						$intDuration = (int)$anArray["Duration"];
						if($intDuration==0)
						{
							$intDuration = 1;
						}
						$this->Duration = $intDuration;

						break;
					case 3:
						$this->CaptureStartDate = $anArray["CaptureStartDate"];
						$this->CaptureEndDate = $anArray["CaptureEndDate"];
						$this->TempTwoMonths = $anArray["TempTwoMonths"];
						$this->WhenToCaptureValue = $this->TempTwoMonths;
						$this->StartDay = (int)$anArray["StartDay"];
						
						$intDuration = (int)$anArray["Duration"];
						if($intDuration==0)
						{
							$intDuration = 1;
						}
						$this->Duration = $intDuration;
						
						break;
					case 4:
						$this->CaptureStartDate = $anArray["CaptureStartDate"];
						$this->CaptureEndDate = $anArray["CaptureEndDate"];
						$this->TempQuarter = $anArray["TempQuarter"];
						$this->WhenToCaptureValue = $this->TempQuarter;
						$this->StartDay = (int)$anArray["StartDay"];
						
						$intDuration = (int)$anArray["Duration"];
						if($intDuration==0)
						{
							$intDuration = 1;
						}
						$this->Duration = $intDuration;

						break;
				}
			}
			else 
			{
				if($this->CaptureVia == 1) 
				{
					$this->CaptureStartDate = $anArray["CaptureStartDate"];
					$this->CaptureEndDate = $anArray["CaptureEndDate"];
					$this->WhenToCaptureType = 0;
					$this->WhenToCaptureValue = "";
					$this->StartDay = 0;
					$this->Duration = 0;
				}
				else 
				{
					$this->CaptureStartDate = "";
					$this->CaptureEndDate = "";
					$this->WhenToCaptureType = 0;
					$this->WhenToCaptureValue = "";
					$this->StartDay = 0;
					$this->Duration = 0;
				}
			}
		}
		
		if (getMDVersion() >= esvEditorHTML)
		{
	 		$editor = 1;
	 		
			if (array_key_exists("EditorType", $anArray))
			{
				$this->EditorType = (int) $anArray["EditorType"];
			}
			
	 		if (array_key_exists("EmailBodyDes", $anArray))
			{
				$this->EmailBodyDes = $anArray["EmailBodyDes"];
			}
			
	 		if (array_key_exists("EmailBodyDesHTML", $anArray))
			{
				$this->EmailBodyDesHTML = $anArray["EmailBodyDesHTML"];
			}
		}		
		
		return $this;
	}
	
	function extractCatalogEmails() 
	{
		//Obtener cla_descrip del catalogo
		$sql = "SELECT ParentID FROM SI_SV_Catalog WHERE CatalogID = ".$this->CatalogID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if ($aRS->EOF)
		{
			return;
		}
		$parentCladescrip = (int)$aRS->fields["parentid"];
		
		$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->CatalogID." AND MemberID = ".$this->AttributeID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$cla_descrip = -1;
		if (!$aRS->EOF)
		{
			$cla_descrip = (int)$aRS->fields["parentid"];
		}
		if ($cla_descrip == -1)
		{
			return;
		}
		
		$field = "DSC_".$cla_descrip;
		$sql = "SELECT DISTINCT ".$field.
			" FROM RIDIM_".$parentCladescrip.
			" WHERE RIDIM_".$parentCladescrip."KEY <> 1".
			" ORDER BY ".$field;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS || $aRS->EOF)
		{
			return;
		}
		$catEmails = array();
		while(!$aRS->EOF)
		{
			$catEmails[] = $aRS->fields["DSC_".$cla_descrip];
			$aRS->MoveNext();
		}
		return $catEmails;
	}

	function save()
	{
		$strOriginalWD = getcwd();
        
        $isnewobject = $this->isNewObject();
		
		if (getMDVersion() >= esvEditorHTML && $this->EditorType == 1)
		{
			//Los campos HTML del editor anterior serán sustituidos con el código HTML generado con el nuevo editor
			if($this->EmailBodyDes!="")
			{
				$this->EmailBody = $this->EmailBodyDesHTML;
			}
		}
		
        if ($isnewobject)
		{
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(SchedulerID)", "0")." + 1 AS SchedulerID".
						" FROM SI_SV_SurveyScheduler";

			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->SchedulerID = (int)$aRS->fields["schedulerid"];
			
			
			//Agregado el nuevo campo para definir que editor se utiliza
			$strFieldsByVersion="";
			$strValuesByVersion="";
			if (getMDVersion() >= esvEditorHTML)
			{
				$strFieldsByVersion .= ', EditorType, EmailBodyDes';
				$strValuesByVersion .= ', '.$this->EditorType.", ".$this->Repository->DataADOConnection->Quote($this->EmailBodyDes);
			}
			
			//Conchita 10-nov-2011 Agregado EmailReplyTo
			$sql = "INSERT INTO SI_SV_SurveyScheduler (".
						" SchedulerID".
			            ", SurveyID".
			            ", SchedulerName".
			            ", AllowMultipleSurveys".
						", CaptureStartDate".
						", CaptureEndDate".
						", CaptureType".
						", CaptureVia".
						", UseCatalog".
						", CatalogID".
						", AttributeID".
						", EmailFilter".
						", EmailSubject".
						", EmailBody".
						", WhenToCapture".
						", StartDay".
						", Duration".
						", EmailReplyTo".
						$strFieldsByVersion.
			            ") VALUES (".
			            $this->SchedulerID.
			            ",".$this->SurveyID.
						",".$this->Repository->DataADOConnection->Quote($this->SchedulerName).
						",".$this->AllowMultipleSurveys.
						",".(($this->CaptureStartDate!="")?$this->Repository->DataADOConnection->DBTimeStamp($this->CaptureStartDate):"NULL").
						",".(($this->CaptureEndDate!="")?$this->Repository->DataADOConnection->DBTimeStamp($this->CaptureEndDate):"NULL").
						",".$this->WhenToCaptureType.
						",".$this->CaptureVia.
						",".$this->UseCatalog.
						",".$this->CatalogID.
						",".$this->AttributeID.
						",".$this->EmailFilter.
						",".$this->Repository->DataADOConnection->Quote($this->EmailSubject).
			            ",".$this->Repository->DataADOConnection->Quote($this->EmailBody).
						",".$this->Repository->DataADOConnection->Quote($this->WhenToCaptureValue).
						",".(($this->StartDay!=0)?$this->StartDay:"NULL").
						",".(($this->Duration!=0)?$this->Duration:"NULL").
						",".$this->Repository->DataADOConnection->Quote($this->EmailReplyTo).
						$strValuesByVersion.
					")"; //Conchita 10-nov-2011 Agregado EmailReplyTo
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
        } else {

        	$strValuesByVersion = "";
        	if (getMDVersion() >= esvEditorHTML)
			{
				$strValuesByVersion .= ', EditorType = '.$this->EditorType
				.', EmailBodyDes = '.$this->Repository->DataADOConnection->Quote($this->EmailBodyDes);
			}
        	
        	$sql = "UPDATE SI_SV_SurveyScheduler SET ".
					"SchedulerName = ".$this->Repository->DataADOConnection->Quote($this->SchedulerName).
					", AllowMultipleSurveys = ".$this->AllowMultipleSurveys.
					", CaptureStartDate = ".(($this->CaptureStartDate!="")?$this->Repository->DataADOConnection->DBTimeStamp($this->CaptureStartDate):"NULL").
					", CaptureEndDate = ".(($this->CaptureEndDate!="")?$this->Repository->DataADOConnection->DBTimeStamp($this->CaptureEndDate):"NULL").
					", CaptureType = ".$this->WhenToCaptureType.
					", CaptureVia = ".$this->CaptureVia.
					", UseCatalog = ".$this->UseCatalog.
					", CatalogID = ".$this->CatalogID.
					", AttributeID = ".$this->AttributeID.
					", EmailFilter = ".$this->EmailFilter.
					", EmailSubject = ".$this->Repository->DataADOConnection->Quote($this->EmailSubject).
					", EmailBody = ".$this->Repository->DataADOConnection->Quote($this->EmailBody).
					", WhenToCapture = ".$this->Repository->DataADOConnection->Quote($this->WhenToCaptureValue).
					", StartDay = ".(($this->StartDay!=0)?$this->StartDay:"NULL").
					", Duration = ".(($this->Duration!=0)?$this->Duration:"NULL").
					", EmailReplyTo = ".$this->Repository->DataADOConnection->Quote($this->EmailReplyTo).//Conchita 10-nov-2011 Agregado EmailReplyTo
					$strValuesByVersion.
					" WHERE SchedulerID = ".$this->SchedulerID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
            
        }
        
        //Se procede a almacenar la relacion de scheduler-usuarios
        $this->UpdateUserIDsFromScheduler();

        $dimEmails = array();
        //Si la captura es via Email
        if($this->CaptureVia == 1) 
        {
            //Se va a comparar los emails capturados con los elementos
            //que estan en la tabla de SI_SV_SchedulerEmails para no agregar emails repetidos
            $sql = "SELECT Email from SI_SV_SchedulerEmail WHERE SchedulerID =".$this->SchedulerID;
            $aRS = $this->Repository->DataADOConnection->Execute($sql);
            if (!$aRS)
            {
                die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SchedulerEmail ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
            }

            $scheduledEmails = array();
            while(!$aRS->EOF)
            {
                $scheduledEmails[] = $aRS->fields["email"];
                $aRS->MoveNext();
            }
            
            $arrayEmails = array();
            //si usa catalogo
            if($this->UseCatalog == 1) 
            {
                //se obtiene los correos del catalogo
                $catalogEmails = array();
                $catalogEmails = array_unique($this->extractCatalogEmails());
                $catalogEmails = $this->filterEmails($catalogEmails);
                $arrayEmails = $catalogEmails;
                
            } else {
                $newEmails = array();
                $newEmails = explode("\r\n",$this->StrCaptureEmails);
                $newEmails = array_unique($newEmails);
                $newEmails = $this->filterEmails($newEmails);
                $this->CaptureEmails = array();
                $this->CaptureEmails = $newEmails;
                $LN = chr(13).chr(10);
                $this->StrCaptureEmails = implode($LN, $this->CaptureEmails);
                $arrayEmails = $newEmails;
            }
            
            foreach($arrayEmails as $theEmail) 
            {
                if(!in_array($theEmail,$scheduledEmails))
                {
                    $dimEmails[] = $theEmail;

                    $sql = "INSERT INTO SI_SV_SchedulerEmail (SchedulerID, Email) 
                            VALUES (".$this->SchedulerID.", ".$this->Repository->DataADOConnection->Quote($theEmail).");";
                    if ($this->Repository->DataADOConnection->Execute($sql) === false)
                    {
                        die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SchedulerEmail ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
                    }
                }
            }
            
            //se eliminan de SI_SV_SchedulerEmail los elementos anteriores
            //que no fueron guardados nuevamente
            foreach($scheduledEmails as $anEmail) {
                if(!in_array($anEmail,$arrayEmails)) {
                    $sql = "DELETE FROM SI_SV_SchedulerEmail WHERE SchedulerID = ".$this->SchedulerID
                            ." AND Email = ".$this->Repository->DataADOConnection->Quote($anEmail);
                    if ($this->Repository->DataADOConnection->Execute($sql) === false)
                    {
                        die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SchedulerEmail ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
                    }
                }
            }
        }
        
		//@JAPR 2015-07-09 Agregado el soporte de Formas v6 con un grabado completamente diferente
		//Se remueven todas las referencias a modelos, dimensiones e indicadores
        //A partir de este momento lanzamos la funcion que realiza el envio de correos con la liga de la encuesta
        if($this->CaptureVia==1)
        {
            $this->SendMailFromScheduler();
        }
		
		return $this;
	}
    
    function filterEmails($arrayEmails) {
        $filterEmails = array();
        foreach($arrayEmails as $anEmail) {
            if($anEmail != "") {
                $filterEmails[] = trim($anEmail);
            }
        }
        return $filterEmails;
    }
    	
	function SendMailFromScheduler($statusSurvey=null)
	{
		set_time_limit(0);

		global $KPAMailTesting;
		$arrayEMails = array();
		
		//Realizamos instancia de Survey
		require_once("survey.inc.php");
		$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
		
		//Verificamos si los correos electronicos provienen de un catalogo o bien fueron insertados manualmente
		if($this->UseCatalog==0)
		{
			if(is_null($statusSurvey))
			{
				$sql = "SELECT Email FROM SI_SV_SchedulerEmail WHERE SchedulerID = ".$this->SchedulerID." ORDER BY Email";
			}
			else 
			{
				//Si es diferente de Not Started
				if($statusSurvey!=2)
				{
					$sql = "SELECT A.Email AS Email FROM SI_SV_SchedulerEmail A 
							LEFT JOIN RIDIM_{$aSurveyInstance->SchedulerDimID} B ON A.SchedulerID = B.KEY_{$aSurveyInstance->SchedulerDimID} 
							LEFT JOIN RIDIM_{$aSurveyInstance->EmailDimID} D ON A.Email = D.DSC_{$aSurveyInstance->EmailDimID} 
							LEFT JOIN RIFACT_{$aSurveyInstance->ModelID} C ON C.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY = B.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY AND C.RIDIM_{$aSurveyInstance->EmailDimID}KEY = D.RIDIM_{$aSurveyInstance->EmailDimID}KEY 
							LEFT JOIN RIDIM_{$aSurveyInstance->StatusDimID} E ON E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = C.RIDIM_{$aSurveyInstance->StatusDimID}KEY 
							WHERE B.KEY_{$aSurveyInstance->SchedulerDimID} = ".$this->SchedulerID." AND E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = ".$statusSurvey." 
							ORDER BY A.Email";
				}
				else 
				{
					$sql = "SELECT A.Email AS Email FROM SI_SV_SchedulerEmail A 
							LEFT JOIN RIDIM_{$aSurveyInstance->SchedulerDimID} B ON A.SchedulerID = B.KEY_{$aSurveyInstance->SchedulerDimID} 
							LEFT JOIN RIDIM_{$aSurveyInstance->EmailDimID} D ON A.Email = D.DSC_{$aSurveyInstance->EmailDimID} 
							LEFT JOIN RIFACT_{$aSurveyInstance->ModelID} C ON C.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY = B.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY AND C.RIDIM_{$aSurveyInstance->EmailDimID}KEY = D.RIDIM_{$aSurveyInstance->EmailDimID}KEY 
							LEFT JOIN RIDIM_{$aSurveyInstance->StatusDimID} E ON E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = C.RIDIM_{$aSurveyInstance->StatusDimID}KEY 
							WHERE B.KEY_{$aSurveyInstance->SchedulerDimID} = ".$this->SchedulerID." AND E.RIDIM_{$aSurveyInstance->StatusDimID}KEY IS NULL 
							ORDER BY A.Email";
				}
			}
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SchedulerEmail ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$count = 0;
			while(!$aRS->EOF)
			{
				$arrayEMails[$count] = array();
				$arrayEMails[$count]["key"] = $count;
				$arrayEMails[$count]["email"] = $aRS->fields["email"];
				
				$count++;
				$aRS->MoveNext();
			}
		}
		else 
		{
			//Obtener cla_descrip del catalogo
			$sql = "SELECT ParentID FROM SI_SV_Catalog WHERE CatalogID = ".$this->CatalogID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS && $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$parentCladescrip = (int)$aRS->fields["parentid"];
			
			$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->CatalogID." AND MemberID = ".$this->AttributeID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS && $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			if (!$aRS->EOF)
			{
				$cla_descrip = (int)$aRS->fields["parentid"];
			}
			
			$fieldEmailKey = "RIDIM_".$parentCladescrip."KEY";
			$fieldEmailDesc = "DSC_".$cla_descrip;
			$tableName = "RIDIM_".$parentCladescrip;

			//Si se utiliza dicho catalogo entonces se tendrá que verificar el EmailFilter
			if($this->EmailFilter==0)
			{
				if(is_null($statusSurvey))
				{
					$sql = "SELECT ".$fieldEmailDesc." AS Email FROM ".$tableName." 
							WHERE ".$fieldEmailKey." <> 1 ORDER BY ".$fieldEmailDesc;
				}
				else 
				{
					//Si es diferente de Not Started
					if($statusSurvey!=2)
					{
						$sql = "SELECT A.Email AS Email FROM SI_SV_SchedulerEmail A 
								LEFT JOIN RIDIM_{$aSurveyInstance->SchedulerDimID} B ON A.SchedulerID = B.KEY_{$aSurveyInstance->SchedulerDimID} 
								LEFT JOIN RIDIM_{$aSurveyInstance->EmailDimID} D ON A.Email = D.DSC_{$aSurveyInstance->EmailDimID} 
								LEFT JOIN RIDIM_{$parentCladescrip} F ON D.DSC_{$aSurveyInstance->EmailDimID} = F.DSC_{$cla_descrip} 
								LEFT JOIN RIFACT_{$aSurveyInstance->ModelID} C ON C.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY = B.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY AND C.RIDIM_{$aSurveyInstance->EmailDimID}KEY = D.RIDIM_{$aSurveyInstance->EmailDimID}KEY 
								LEFT JOIN RIDIM_{$aSurveyInstance->StatusDimID} E ON E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = C.RIDIM_{$aSurveyInstance->StatusDimID}KEY 
								WHERE B.KEY_{$aSurveyInstance->SchedulerDimID} = ".$this->SchedulerID." AND E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = ".$statusSurvey." 
								ORDER BY A.Email";
					}
					else 
					{
						$sql = "SELECT A.Email AS Email FROM SI_SV_SchedulerEmail A 
								LEFT JOIN RIDIM_{$aSurveyInstance->SchedulerDimID} B ON A.SchedulerID = B.KEY_{$aSurveyInstance->SchedulerDimID} 
								LEFT JOIN RIDIM_{$aSurveyInstance->EmailDimID} D ON A.Email = D.DSC_{$aSurveyInstance->EmailDimID} 
								LEFT JOIN RIDIM_{$parentCladescrip} F ON D.DSC_{$aSurveyInstance->EmailDimID} = F.DSC_{$cla_descrip} 
								LEFT JOIN RIFACT_{$aSurveyInstance->ModelID} C ON C.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY = B.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY AND C.RIDIM_{$aSurveyInstance->EmailDimID}KEY = D.RIDIM_{$aSurveyInstance->EmailDimID}KEY 
								LEFT JOIN RIDIM_{$aSurveyInstance->StatusDimID} E ON E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = C.RIDIM_{$aSurveyInstance->StatusDimID}KEY 
								WHERE B.KEY_{$aSurveyInstance->SchedulerDimID} = ".$this->SchedulerID." AND E.RIDIM_{$aSurveyInstance->StatusDimID}KEY IS NULL 
								ORDER BY A.Email";
					}
				}
		
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				$count = 0;
				while(!$aRS->EOF)
				{
					$arrayEMails[$count] = array();
					$arrayEMails[$count]["key"] = $count;
					$arrayEMails[$count]["email"] = $aRS->fields["email"];
					
					$count++;
					$aRS->MoveNext();
				}
			}
			else if ($this->EmailFilter==1)
			{
				if(is_null($statusSurvey))
				{
					$sql = "SELECT ".$fieldEmailKey." AS EmailKey, ".$fieldEmailDesc." AS EmailDesc FROM ".$tableName." 
							WHERE ".$fieldEmailKey." <> 1 ORDER BY ".$fieldEmailKey;
				}
				else 
				{
					//Si es diferente de Not Started
					if($statusSurvey!=2)
					{
						$sql = "SELECT F.RIDIM_{$parentCladescrip}KEY AS EmailKey, F.DSC_{$cla_descrip} AS EmailDesc FROM SI_SV_SchedulerEmail A 
								INNER JOIN RIDIM_{$aSurveyInstance->SchedulerDimID} B ON A.SchedulerID = B.KEY_{$aSurveyInstance->SchedulerDimID} 
								INNER JOIN RIDIM_{$parentCladescrip} F ON A.Email = F.DSC_{$cla_descrip} 
								INNER JOIN RIDIM_{$aSurveyInstance->EmailDimID} D ON F.DSC_{$cla_descrip} = D.DSC_{$aSurveyInstance->EmailDimID} 
								LEFT JOIN RIFACT_{$aSurveyInstance->ModelID} C ON C.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY = B.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY AND C.RIDIM_{$parentCladescrip}KEY = F.RIDIM_{$parentCladescrip}KEY 
								LEFT JOIN RIDIM_{$aSurveyInstance->StatusDimID} E ON E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = C.RIDIM_{$aSurveyInstance->StatusDimID}KEY 
								WHERE B.KEY_{$aSurveyInstance->SchedulerDimID} = ".$this->SchedulerID." AND E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = ".$statusSurvey." 
								ORDER BY F.RIDIM_{$parentCladescrip}KEY";
					}
					else 
					{
						$sql = "SELECT F.RIDIM_{$parentCladescrip}KEY AS EmailKey, F.DSC_{$cla_descrip} AS EmailDesc FROM SI_SV_SchedulerEmail A 
								INNER JOIN RIDIM_{$aSurveyInstance->SchedulerDimID} B ON A.SchedulerID = B.KEY_{$aSurveyInstance->SchedulerDimID} 
								INNER JOIN RIDIM_{$parentCladescrip} F ON A.Email = F.DSC_{$cla_descrip} 
								INNER JOIN RIDIM_{$aSurveyInstance->EmailDimID} D ON F.DSC_{$cla_descrip} = D.DSC_{$aSurveyInstance->EmailDimID} 
								LEFT JOIN RIFACT_{$aSurveyInstance->ModelID} C ON C.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY = B.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY AND C.RIDIM_{$parentCladescrip}KEY = F.RIDIM_{$parentCladescrip}KEY 
								LEFT JOIN RIDIM_{$aSurveyInstance->StatusDimID} E ON E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = C.RIDIM_{$aSurveyInstance->StatusDimID}KEY 
								WHERE B.KEY_{$aSurveyInstance->SchedulerDimID} = ".$this->SchedulerID." AND E.RIDIM_{$aSurveyInstance->StatusDimID}KEY IS NULL 
								ORDER BY F.RIDIM_{$parentCladescrip}KEY";
					}
				}
		
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				$count = 0;
				while(!$aRS->EOF)
				{
					$arrayEMails[$count] = array();
					$arrayEMails[$count]["key"] = $aRS->fields["emailkey"];
					$arrayEMails[$count]["email"] = $aRS->fields["emaildesc"];
					
					$count++;
					$aRS->MoveNext();
				}
			}
			else 
			{
				if(is_null($statusSurvey))
				{
					$sql = "SELECT DISTINCT ".$fieldEmailDesc." AS Email FROM ".$tableName." 
							WHERE ".$fieldEmailKey." <> 1 ORDER BY ".$fieldEmailDesc;
				}
				else 
				{
					//Si es diferente de Not Started
					if($statusSurvey!=2)
					{
						$sql = "SELECT A.Email AS Email FROM SI_SV_SchedulerEmail A 
								LEFT JOIN RIDIM_{$aSurveyInstance->SchedulerDimID} B ON A.SchedulerID = B.KEY_{$aSurveyInstance->SchedulerDimID} 
								LEFT JOIN RIDIM_{$aSurveyInstance->EmailDimID} D ON A.Email = D.DSC_{$aSurveyInstance->EmailDimID} 
								LEFT JOIN RIDIM_{$parentCladescrip} F ON D.DSC_{$aSurveyInstance->EmailDimID} = F.DSC_{$cla_descrip} 
								LEFT JOIN RIFACT_{$aSurveyInstance->ModelID} C ON C.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY = B.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY AND C.RIDIM_{$aSurveyInstance->EmailDimID}KEY = D.RIDIM_{$aSurveyInstance->EmailDimID}KEY 
								LEFT JOIN RIDIM_{$aSurveyInstance->StatusDimID} E ON E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = C.RIDIM_{$aSurveyInstance->StatusDimID}KEY 
								WHERE B.KEY_{$aSurveyInstance->SchedulerDimID} = ".$this->SchedulerID." AND E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = ".$statusSurvey." 
								ORDER BY A.Email";
					}
					else 
					{
						$sql = "SELECT A.Email AS Email FROM SI_SV_SchedulerEmail A 
								LEFT JOIN RIDIM_{$aSurveyInstance->SchedulerDimID} B ON A.SchedulerID = B.KEY_{$aSurveyInstance->SchedulerDimID} 
								LEFT JOIN RIDIM_{$aSurveyInstance->EmailDimID} D ON A.Email = D.DSC_{$aSurveyInstance->EmailDimID} 
								LEFT JOIN RIDIM_{$parentCladescrip} F ON D.DSC_{$aSurveyInstance->EmailDimID} = F.DSC_{$cla_descrip} 
								LEFT JOIN RIFACT_{$aSurveyInstance->ModelID} C ON C.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY = B.RIDIM_{$aSurveyInstance->SchedulerDimID}KEY AND C.RIDIM_{$aSurveyInstance->EmailDimID}KEY = D.RIDIM_{$aSurveyInstance->EmailDimID}KEY 
								LEFT JOIN RIDIM_{$aSurveyInstance->StatusDimID} E ON E.RIDIM_{$aSurveyInstance->StatusDimID}KEY = C.RIDIM_{$aSurveyInstance->StatusDimID}KEY 
								WHERE B.KEY_{$aSurveyInstance->SchedulerDimID} = ".$this->SchedulerID." AND E.RIDIM_{$aSurveyInstance->StatusDimID}KEY IS NULL 
								ORDER BY A.Email";
					}
				}
		
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				$count = 0;
				while(!$aRS->EOF)
				{
					$arrayEMails[$count] = array();
					$arrayEMails[$count]["key"] = $count;
					$arrayEMails[$count]["email"] = $aRS->fields["email"];
					
					$count++;
					$aRS->MoveNext();
				}
			}
		}

		require_once("bpaemailconfiguration.inc.php");
		$anInstanceEmailConf = BPAEmailConfiguration::readConfiguration();
		
		if(is_null($anInstanceEmailConf))
		{
			return false;
		}
		
		$SMTPServer = $anInstanceEmailConf->fbm_email_server;
		
		if (strlen($SMTPServer) == 0) 
		{
			return false;
		}
	
		$SMTPPort = $anInstanceEmailConf->fbm_email_port;
		
		if (strlen($SMTPPort) == 0) 
		{
			return false;
		}
		
		$SMTPUserName = $anInstanceEmailConf->fbm_email_username;
		
		if (strlen($SMTPUserName) == 0)
		{
			return false;
		}
	
		$SMTPPassword = $anInstanceEmailConf->fbm_email_pwd;
		
		$objAppUser = BITAMAppUser::NewInstanceWithClaUsuario($this->Repository, $_SESSION["PABITAM_UserID"]);
		
		$strCreatorStatusEmail = $objAppUser->Email;
		$strCreatorStatusUserName = $objAppUser->Email;

		require_once("mail/class.phpmailer.php");

		//Recorremos todo el arreglo para enviar el correo
		foreach ($arrayEMails as $objEmail)
		{
			$anEmail = $objEmail["email"];
			$mail = new PHPMailer();
			$mail->Mailer = "smtp";
			$mail->Host = $SMTPServer;
			$mail->Port = $SMTPPort;
			$mail->SMTPAuth = TRUE;
			//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
			//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
			$mail->SMTPAutoTLS = false;
			//$mail->SMTPDebug = false;
			//$mail->Debugoutput = 'echo';
			//$mail->SMTPSecure = '';
			//$mail->CharSet = 'UTF-8';
			//@JAPR
			$mail->Username = $SMTPUserName;
			$mail->Password = $SMTPPassword;
		
			//Se modifico el envio de correos para que el usuario que se loguea al servidor de correos
			//sea el mismo que vaya en el FROM, pero en el momento en que se contesta el correo entonces
			//el Reply To va dirigido a la persona q realmente si creo el correo
			$mail->From = $anInstanceEmailConf->fbm_email_source;
			$mail->FromName = $strCreatorStatusUserName;
			
			//Conchita 10-nov-2011 Agregado un campo de EmailReplyTo, si este campo esta vacío entonces se toma el email original del usuario loggeado
			//en caso de que exista un correo en el campo EmailReplyTo se toma este email como opción de responder a en el correo
			if($this->EmailReplyTo!="")
			{
				$mail->AddReplyTo($this->EmailReplyTo, $this->EmailReplyTo);				
			}
			else 
			{
				$mail->AddReplyTo($strCreatorStatusEmail, $strCreatorStatusUserName);
			}
		
			$mail->IsHTML(TRUE);
			
			$maillanguage = $_SESSION["PAuserLanguage"];
			$mail->SetLanguage(strtolower($maillanguage),"mail/language/");
			
			$mail->Subject = $this->EmailSubject;
		
			$existRecipients = false;
			
			$tempEmail = $anEmail;
			//$tempEmail = "iris.suarez@gmail.com";
			$tempUserName = $anEmail;
			
			$mail->Body = $this->CreateNewSurveyNotificationMailBody($objEmail);
			
			$blnCheckMail = BPAEmailConfiguration::check_email_address($tempEmail);
			if ($blnCheckMail)
			{
				$mail->AddAddress($tempEmail, $tempUserName);
				$existRecipients = true;
			}

			//Enviaremos copias ocultas a los correos que se encuentran en la variable global
			//$KPAMailTesting definida en el config con la finalidad de monitorear el envio de correos
			foreach ($KPAMailTesting as $emailTest)
			{
				$blnCheckMail = BPAEmailConfiguration::check_email_address($emailTest);
				if ($blnCheckMail)
				{
					$mail->AddBCC($emailTest, "");
					$existRecipients = true;
				}
			}

			if($existRecipients)
			{
				AddCuentaCorreoAltToMail($objAppUser, $mail);
				if (!@$mail->Send())
				{
					//Enviamos al log el error generado
					global $SendMailLogFile;
					
					$aLogString = "\r\n\r\neSurvey - Scheduler: ".$this->SchedulerName.": ".date('Y-m-d H:i:s');
					$aLogString.="\r\n";
					$aLogString.="\r\n\r\n".$mail->ErrorInfo;
					$aLogString.="\r\n";
		
					//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
					error_log($aLogString, 3, GeteFormsLogPath().$SendMailLogFile);
					//@JAPR
				}
			}
			
			unset($mail);
		}

		return true;
	}

	function CreateNewSurveyNotificationMailBody($objEmail)
	{
		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
		
		$strEMailBody = $this->EmailBody;
		$strLink = $this->encodeSurveyLink($objEmail);
		$emailsignature="<br><br><p align='right' style=\"font-family:verdana;color: rgb(64, 64, 64);font-weight:normal;font-size:8pt\">Formulario electrónico generado por <b> KPI Online FORMS </b> | e-Form powered by <b>KPI Online FORMS</b> | <a href='www.kpionline.com'>www.kpionline.com</a></p>";
		$strToFind = "[KPI%20eSurvey%20Link]";
		$position = strpos($strEMailBody, $strToFind);
		
		if($position!==false)
		{
			$strEMailBody = str_replace($strToFind, $strLink, $strEMailBody);
		}
		
		$strtemp = "<html>"."\n"
		."<style type=text/css>"."\n"
		."  A:active { color: #00008b }"."\n"
		."  A:visited { color: #708090 }"."\n"
		."  A:link { color: #00008b }"."\n"
		."  A:hover { color: #4169e1 }"."\n"
		."  A { font-family: verdana; font-size: 8pt }"."\n"
		."</style>"."\n"
		."<body>"."\n"
		."<table border=0 cellPadding=5 cellSpacing=1 style=\"font-family:verdana;font-weight:normal;font-size:10pt\">"."\n"
		."  <tr>"."\n"
		."    <td>".$strEMailBody."</td>"."\n"
		."  </tr>"."\n"
		."  <tr>"."\n"
		."    <td>&nbsp;</td>"."\n"
		."  </tr>"."\n";

		//Sino hay enlaces a la encuesta entonces se agregara al final
		if($position===false)
		{
			$strtemp.="  <tr>"."\n"
			."    <td><a href='".$strLink."' target='_blank'>".$strLink."</a></td>"."\n"
			."  </tr>"."\n";
		}

		$strtemp.="</table>"."\n"
		."<br>"."\n";
		$strtemp.=$emailsignature;
	
		$strtemp.="</body>"."\n"
		."</html>"."\n";
	
		return $strtemp;
	}
	
	function encodeSurveyLink($objEmail)
	{
		require_once("user.inc.php");
		require_once("utils.inc.php");

		//$server = "http://localhost/artus/genvi/ESurvey/";
		//Se obtiene el servidor donde se esta ejecutando EktosCS
		$host_string = $_SERVER["SERVER_NAME"];
		$port = (int)$_SERVER["SERVER_PORT"];
		
		if($port!=80)
		{
			$port_string = ":".$port;
		}
		else 
		{
			$port_string = "";
		}
		
		$port_string = "";

		$protocol_string = "http://";
		
		$script = $_SERVER["REQUEST_URI"];
		$position = strrpos($script, "/");
		$dir = substr($script, 0, $position);
		
		$strLenDir = strlen($dir);
		$lastPosChar = $strLenDir - 1;
		$lastChar = substr($dir, $lastPosChar);
		
		if($lastChar!="/")
		{
			$dir = $dir."/";
		}
		
		$server = $protocol_string.$host_string.$port_string.$dir;

		$surveyLink = $server."loadsurvey.php?params=";
		$userName = $_SESSION["PABITAM_UserName"];
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$theUser = BITAMeFormsUser::WithUserName($this->Repository, $userName);
		$password = BITAMDecryptPassword($theUser->Password);
		
		$sep = "\r\n";
		$goto="generateSurvey.php?RepositoryName=".$_SESSION["PABITAM_RepositoryName"]."&externalPage=1&surveyID=".$this->SurveyID."&";
		$goto.="SchedulerID=".$this->SchedulerID."&CaptureVia=1&CaptureEmail=".$objEmail["email"]."&EmailKey=".$objEmail["key"];
		
		$params = $userName.$sep.$password.$sep.$goto;
		$params = urlencode(BITAMEncode($params));
		
		$surveyLink.=$params;
		//@JAPR 2012-07-26: Agregado el uso de la versión recibida como parámetro para controlar los campos/tablas que son consultados de la metadata
		$surveyLink.="&appVersion=".((string) @$_SESSION["appVersion"]);
		//@JAPR
		
		return $surveyLink;
	}
	
	static function existEmailInThisScheduler($aRepository, $schedulerID, $email)
	{
		$sql = "SELECT Email FROM SI_SV_SchedulerEmail WHERE SchedulerID = ".$schedulerID." AND Email = ".$aRepository->DataADOConnection->Quote($email);
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SchedulerEmail ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
	/* Agregado el parámetro oItemsToUpdate que es un array con los posibles valores como llave otyUser y otyUserGroup, indicando el tipo de objeto que debe
	ser actualizado con este método, ya sea individual o ambos (si no se especifica se asume que se actualizarán ambos)
	*/
	function UpdateUserIDsFromScheduler($oItemsToUpdate = null)
	{
		if (is_null($oItemsToUpdate) || !is_array($oItemsToUpdate)) {
			$oItemsToUpdate = array(otyUser => otyUser, otyUserGroup => otyUserGroup);
		}
		
		$blnUpdateUsers = isset($oItemsToUpdate[otyUser]);
		$blnUpdateGroups = isset($oItemsToUpdate[otyUserGroup]);
		if ($blnUpdateUsers) {
			//Elimino los registros anteriores y se vuelven a insertar nuevamente:
			$sql = "DELETE FROM SI_SV_SurveySchedulerUser WHERE SchedulerID = ".$this->SchedulerID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySchedulerUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$this->UserIDs = array();
		}
		
		if ($blnUpdateGroups) {
			//Elimino los registros anteriores y se vuelven a insertar nuevamente:
			$sql = "DELETE FROM SI_SV_SurveySchedulerRol WHERE SchedulerID = ".$this->SchedulerID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySchedulerRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$this->UsrRoles = array();
		}
		
		$this->StrUserRol = trim($this->StrUserRol);
		if($this->StrUserRol!="")
		{
			$arrayElements = explode("_AWElem_", $this->StrUserRol);
		}
		else 
		{
			$arrayElements = array();
		}
		
		foreach($arrayElements as $anElement)
		{
			$arrInfo = explode("_", $anElement);
			if(substr($anElement, 0, 5)=="User_")
			{
				if ($blnUpdateUsers) {
					$this->UserIDs[] = (int)$arrInfo[1];
				}
			}
			else
			{
				if ($blnUpdateGroups) {
					$this->UsrRoles[] = (int)$arrInfo[1];
				}
			}
		}
		
		$numUsers = count($this->UserIDs);
		for($i=0; $i<$numUsers; $i++)
		{
			$sqlIns = "INSERT INTO SI_SV_SurveySchedulerUser (SchedulerID, UserID) 
						VALUES (".$this->SchedulerID.",".$this->UserIDs[$i].")";
			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySchedulerUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
		}

		$numRoles = count($this->UsrRoles);
		for($i=0; $i<$numRoles; $i++)
		{
			$sqlIns = "INSERT INTO SI_SV_SurveySchedulerRol (SchedulerID, RolID) 
						VALUES (".$this->SchedulerID.",".$this->UsrRoles[$i].")";
			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySchedulerRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
		}
	}
	
	function remove()
	{
		$sql = "DELETE FROM SI_SV_SurveySchedulerUser WHERE SchedulerID = ".$this->SchedulerID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySchedulerUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$sql = "DELETE FROM SI_SV_SurveySchedulerRol WHERE SchedulerID = ".$this->SchedulerID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySchedulerRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		// Si se indica captura por email se debe llenar la dimension
		// correo electronico con los valores
		require_once("survey.inc.php");
		
		//Se genera instancia de encuesta dado un SurveyID
		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
		
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		
		//se elimina la dimension scheduler
		$schedulerDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $surveyInstance->ModelID, -1, $surveyInstance->SchedulerDimID);
			
		//Se agrega en la dimension Scheduler el registro del scheduler creado
		BITAMSurvey::deleteSchedulerDimensionValue($this->Repository,$schedulerDimension,$this->SchedulerID);
		
		//Se eliminan los emails de la tabla SI_SV_SchedulerEmail
		$sql = "DELETE FROM SI_SV_SchedulerEmail WHERE SchedulerID = ".$this->SchedulerID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SchedulerEmail ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$sql = "DELETE FROM SI_SV_SurveyScheduler WHERE SchedulerID = ".$this->SchedulerID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->SchedulerID < 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Scheduler");
		}
		else
		{
			return $this->SchedulerName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SurveyScheduler";
		}
		else
		{
			return "BITAM_PAGE=SurveyScheduler&SchedulerID=".$this->SchedulerID;
		}
	}
	
	function get_Children($aUser)
	{
		$myChildren = array();

		if (!$this->isNewObject())
		{
			$myChildren[] = BITAMArtusReporterCollection::NewInstance($this->Repository, $aUser, $this->SchedulerID, $this->SurveyID);
		}
		
		return $myChildren;
	}

	function get_Parent()
	{
		return BITAMSurveySchedulerCollection::NewInstance($this->Repository, null);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SchedulerID';
	}

	function get_FormFields($aUser)
	{
		global $gbIsGeoControl;
		
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SchedulerName";
		$aField->Title = translate("Description");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$isReadOnly = false;
		if(!$this->isNewObject())
		{
			$isReadOnly = true;
		}
		
		$arrayStatusSurvey = array();
		$arrayStatusSurvey[0] = 0;
		$arrayStatusSurvey[1] = 1;

		$surveyField = BITAMFormField::NewFormField();
		$surveyField->Name = "SurveyID";
		$surveyField->Title = translate("Survey");
		$surveyField->Type = "Object";
		$surveyField->VisualComponent = "Combobox";
		$surveyField->Options = BITAMSurvey::getSurveys($this->Repository, $arrayStatusSurvey);
		$surveyField->OnChange = "showHideEmailFilter();";
		$surveyField->IsDisplayOnly = $isReadOnly;
		$myFields[$surveyField->Name] = $surveyField;
		
		//CAPTURE VIA
		//opts for via
		$captureViaOpts = array();
		$captureViaOpts[0] = translate("Web/Mobile");
		$captureViaOpts[1] = translate("E-Mail");
	
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CaptureVia";
		$aField->Title = translate("Capture via");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $captureViaOpts;
		$aField->OnChange = "showHideEmailComp();";
		if(!$this->isNewObject())
		{
			$aField->IsDisplayOnly = true;
		}
		//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
		if ($gbIsGeoControl) {
			$aField->IsVisible = false;
			$aField->Title = "";
		}
		//@JAPR
		$myFields[$aField->Name] = $aField;
		
		$userIDsField = BITAMFormField::NewFormField();
		$userIDsField->Name = "StrUserRol";
		$userIDsField->Title = translate("Users");
		$userIDsField->Type = "LargeString";
		$userIDsField->Size = 1000;
		$userIDsField->IsVisible = false;
		$userIDsField->AfterMessage = "<span id=\"changeUserIDs\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);backgroun-position:center center;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssignUsers();\">".translate("Change")."</span>";
		$myFields[$userIDsField->Name] = $userIDsField;
		
		$options = array();
		$options[0] = "No";
		$options[1] = "Yes";
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AllowMultipleSurveys";
		$aField->Title = translate("Has Frequency?");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $options;
		$aField->OnChange = "showHideFrequency();";
		//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
		if ($gbIsGeoControl) {
			$aField->IsVisible = false;
			$aField->Title = "";
		}
     	//@JAPR
		$myFields[$aField->Name] = $aField;
		
		//MAILS FROM CATALOG OR INPUT
		//email from catalog opts (Yes/No)
		$catOpts = array();
		$catOpts[0] = "No";
		$catOpts[1] = "Yes";
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UseCatalog";
		$aField->Title = translate("Use email catalog?");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $catOpts;
		$aField->OnChange = "showHideCatalogs();";
		if(!$this->isNewObject())
		{
			$aField->IsDisplayOnly = true;
		}
		$myFields[$aField->Name] = $aField;
		
		//combo from catalog
		$catalogField = BITAMFormField::NewFormField();
		$catalogField->Name = "CatalogID";
		$catalogField->Title = translate("Select Catalog");
		$catalogField->Type = "Object";
		$catalogField->VisualComponent = "Combobox";
		$catalogField->Options = BITAMCatalog::getCatalogs($this->Repository);
		$catalogField->OnChange = "updateFields()";
		if(!$this->isNewObject())
		{
			$catalogField->IsDisplayOnly = true;
		}
		$myFields[$catalogField->Name] = $catalogField;
		
		//atributes combo
		$catmemberField = BITAMFormField::NewFormField();
		$catmemberField->Name = "AttributeID";
		$catmemberField->Title = translate("Select Attribute");
		$catmemberField->Type = "Object";
		$catmemberField->VisualComponent = "Combobox";
		$catmemberField->Options = BITAMSurveyScheduler::getCatMembersByCatalogID($this->Repository);
		if(!$this->isNewObject())
		{
			$catmemberField->IsDisplayOnly = true;
		}
		$myFields[$catmemberField->Name] = $catmemberField;
		
		$catmemberField->Parent = $catalogField;
		$catalogField->Children[] = $catmemberField;
                
        //email filter combo
		$emailFilterOpts = array();
		$emailFilterOpts[0] = translate("None");
		$emailFilterOpts[1] = translate("One survey per row");
		$emailFilterOpts[2] = translate("One survey per email address");
                
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "EmailFilter";
		$aField->Title = translate("Email Filter");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $emailFilterOpts;
		$aField->IsDisplayOnly = $isReadOnly;
		$myFields[$aField->Name] = $aField;
		
		//input for emails
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "StrCaptureEmails";
		$aField->Title = translate("Capture emails");
		$aField->Type = "LargeString";
		$aField->Size = 5000;
		$myFields[$aField->Name] = $aField;

		//Conchita 10-nov-2011 EmailReplyTo
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "EmailReplyTo";
		$aField->Title = translate("Reply To");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
				
		//input for email subject
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "EmailSubject";
		$aField->Title = translate("Email subject");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		if (getMDVersion() >= esvEditorHTML)
		{
			if($this->isNewObject())
			{
				$arrEditorType[0] = "Tinymce";
				$arrEditorType[1] = translate("Image Editor");
			}
			else
			{
				if($this->EditorType==0)
				{
					$arrEditorType[0] = "Tinymce";
				}
				else
				{
					$arrEditorType[1] = translate("Image Editor");
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "EditorType";
			$aField->Title = translate("Editor");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrEditorType;
			$aField->OnChange = "onChangeEditor()";
			$myFields[$aField->Name] = $aField;
		}
		
		//input for email body
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "EmailBody";
		$aField->Title = translate("Email body");
		$aField->Type = "LargeStringHTML";
		$aField->Size = 5000;
		$aField->UseHTMLDes = 1;
		$aField->CompToAdd = "'LBL,ADD','IMG,ADD'";
		$aField->CompToAddDes = "ML[157]+'_AWSep_comp_agregar.gif',ML[584]+'_AWSep_rep_etiqueta.gif',ML[361]+'_AWSep_rep_imagen.gif'";
		$myFields[$aField->Name] = $aField;
				
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CaptureStartDate";
		$aField->Title = translate("Start Date");
		$aField->Type = "DateTime";
		$aField->FormatMask = "yyyy-MM-dd";
		//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
		if ($gbIsGeoControl) {
			$aField->IsVisible = false;
			$aField->Title = "";
		}
		//@JAPR
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CaptureEndDate";
		$aField->Title = translate("End Date");
		$aField->Type = "DateTime";
		$aField->FormatMask = "yyyy-MM-dd";
		//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
		if ($gbIsGeoControl) {
			$aField->IsVisible = false;
			$aField->Title = "";
		}
		//@JAPR
		$myFields[$aField->Name] = $aField;
		
		$optionsFrequency = array();
		$optionsFrequency[0] = translate("Daily");
		$optionsFrequency[1] = translate("Weekly");
		$optionsFrequency[2] = translate("Monthly");
		$optionsFrequency[3] = translate("Two Months");
		$optionsFrequency[4] = translate("Quarter");

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "WhenToCaptureType";
		$aField->Title = translate("Frequency");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsFrequency;
		$aField->OnChange = "showHidePeriods();";
		//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
		if ($gbIsGeoControl) {
			$aField->IsVisible = false;
			$aField->Title = "";
		}
		//@JAPR
		$myFields[$aField->Name] = $aField;

		$optionsWeek = array();
		$optionsWeek[1] = translate("Monday");
		$optionsWeek[2] = translate("Tuesday");
		$optionsWeek[3] = translate("Wednesday");
		$optionsWeek[4] = translate("Thursday");
		$optionsWeek[5] = translate("Friday");
		$optionsWeek[6] = translate("Saturday");
		$optionsWeek[0] = translate("Sunday");

		$weekField = BITAMFormField::NewFormField();
		$weekField->Name = "TempWeek";
		$weekField->Title = translate("Weekly");
		$weekField->Type = "Array";
		$weekField->Options = $optionsWeek;
		$myFields[$weekField->Name] = $weekField;

		$optionsMonth = BITAMSurveyScheduler::getAllMonthDays();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TempMonth";
		$aField->Title = translate("Day of the month");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsMonth;
		$myFields[$aField->Name] = $aField;
		
		$optionsTwoMonths = array();
		$optionsTwoMonths[1] = translate("Odd Month");
		$optionsTwoMonths[2] = translate("Even Month");

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TempTwoMonths";
		$aField->Title = translate("Two Months");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsTwoMonths;
		$myFields[$aField->Name] = $aField;
		
		$optionsQuarter = array();
		$optionsQuarter[1] = translate("Month 1");
		$optionsQuarter[2] = translate("Month 2");
		$optionsQuarter[3] = translate("Month 3");

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TempQuarter";
		$aField->Title = translate("Quarter");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsQuarter;
		$myFields[$aField->Name] = $aField;
		
		$optionsDays = BITAMSurveyScheduler::getAllMonthDays();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "StartDay";
		$aField->Title = translate("Start Day");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsDays;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Duration";
		$aField->Title = translate("Duration");
		$aField->Type = "Integer";
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	static function getCatMembersByCatalogID($aRepository)
	{		
		$sql = "SELECT CatalogID, MemberID, MemberName, MemberOrder FROM SI_SV_CatalogMember ORDER BY CatalogID, MemberOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();

		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["catalogid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["catalogid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["catalogid"];
					$aGroup = array();
				}
				
				$aGroup[$aRS->fields["memberid"]] = $aRS->fields["membername"];
  				$aRS->MoveNext();

			}while (!$aRS->EOF);
			
			$allValues[$lastID] = $aGroup;
		}

		return $allValues;
	}

	static function getAllMonthDays()
	{
		$optionsMonth = array();
		
		for($i=1; $i<=31; $i++)
		{
			$optionsMonth[$i] = $i;
		}	
		
		return $optionsMonth;
	}

	function generateBeforeFormCode($aUser)
 	{	
	?>
		<!--<script language="javascript" src="js/dialogs.js"></script>-->
 		<script language="JavaScript">
		var myServerPath='<?=(((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";?>';
 		initialSelectionCatMember = null;
		
 		function setInitialValues()
		{
            var catMember = BITAMSurveyScheduler_SaveForm.AttributeID;
            initialSelectionCatMember = null;
            if (catMember.selectedIndex >= 0)
            {
	        	initialSelectionCatMember = catMember.options[catMember.selectedIndex].value;
            }
		}

 		function updateFields()
 		{
 			if (cancel==true)
 			{
 				BITAMSurveyScheduler_AttributeID_Update();
 				
 				var catMember = BITAMSurveyScheduler_SaveForm.AttributeID;
 				
 				if (initialSelectionCatMember!=null)
 				{
 					for(i=0;i<catMember.options.length;i++)
 					{
 						if (catMember.options[i].value==initialSelectionCatMember)
 						{	
 							catMember.selectedIndex=i;
 							break;
 						}
 					}
 				}
 			}
			showHideEmailFilter();
 			cancel=false;
 		}
 		
		function showHideFrequency()
		{
	 		var objCmb = BITAMSurveyScheduler_SaveForm.AllowMultipleSurveys;
	 		var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);

	 		var rowStartDate = document.getElementById("Row_CaptureStartDate");
	 		var rowEndDate = document.getElementById("Row_CaptureEndDate");
	 		var rowFrequency = document.getElementById("Row_WhenToCaptureType");
	 		
	 		var rowTempWeek = document.getElementById("Row_TempWeek");
	 		var rowTempMonth = document.getElementById("Row_TempMonth");
	 		var rowTempTwoMonths = document.getElementById("Row_TempTwoMonths");
	 		var rowTempQuarter = document.getElementById("Row_TempQuarter");
	 		var rowStartDay = document.getElementById("Row_StartDay");
	 		var rowDuration = document.getElementById("Row_Duration");
	 		
 			var objCmbVia = BITAMSurveyScheduler_SaveForm.CaptureVia;
 		 	var valueViaID = parseInt(objCmbVia.options[objCmbVia.selectedIndex].value);

	 		//Valor en Has Frequency
			if(valueID==1)
			{
	 			if(bIsIE)
	 			{
					rowStartDate.style.display = 'inline';
					rowEndDate.style.display = 'inline';
					rowFrequency.style.display = 'inline';
	 			}
	 			else
	 			{
	 				rowStartDate.style.display = 'table-row';
	 				rowEndDate.style.display = 'table-row';
	 				rowFrequency.style.display = 'table-row';
	 			}
	 			
	 			showHidePeriods();
			}
			else
			{
				if(valueViaID==1)
				{
		 			if(bIsIE)
		 			{
						rowStartDate.style.display = 'inline';
						rowEndDate.style.display = 'inline';
		 			}
		 			else
		 			{
		 				rowStartDate.style.display = 'table-row';
		 				rowEndDate.style.display = 'table-row';
		 			}
				}
				else
				{
					rowStartDate.style.display = 'none';
					rowEndDate.style.display = 'none';
				}
				
				rowFrequency.style.display = 'none';
				rowTempWeek.style.display = 'none';
				rowTempMonth.style.display = 'none';
				rowTempTwoMonths.style.display = 'none';
				rowTempQuarter.style.display = 'none';
				rowStartDay.style.display = 'none';
				rowDuration.style.display = 'none';
			}
		}

		function showHidePeriods()
		{
	 		var objCmb = BITAMSurveyScheduler_SaveForm.WhenToCaptureType;
	 		var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);

	 		var rowTempWeek = document.getElementById("Row_TempWeek");
	 		var rowTempMonth = document.getElementById("Row_TempMonth");
	 		var rowTempTwoMonths = document.getElementById("Row_TempTwoMonths");
	 		var rowTempQuarter = document.getElementById("Row_TempQuarter");
	 		var rowStartDay = document.getElementById("Row_StartDay");
	 		var rowDuration = document.getElementById("Row_Duration");
	 		
			switch (valueID)
			{
				case 0:
					rowTempWeek.style.display = 'none';
					rowTempMonth.style.display = 'none';
					rowTempTwoMonths.style.display = 'none';
					rowTempQuarter.style.display = 'none';
					rowStartDay.style.display = 'none';
					rowDuration.style.display = 'none';

					break;
				case 1:
		 			if(bIsIE)
		 			{
						rowTempWeek.style.display = 'inline';
		 			}
		 			else
		 			{
		 				rowTempWeek.style.display = 'table-row';
		 			}
		 			
					rowTempMonth.style.display = 'none';
					rowTempTwoMonths.style.display = 'none';
					rowTempQuarter.style.display = 'none';
					rowStartDay.style.display = 'none';
					rowDuration.style.display = 'none';
		 			
					break;
				case 2:
		 			if(bIsIE)
		 			{
						rowTempMonth.style.display = 'inline';
						rowDuration.style.display = 'inline';
		 			}
		 			else
		 			{
		 				rowTempMonth.style.display = 'table-row';
		 				rowDuration.style.display = 'table-row';
		 			}
		 			
		 			rowTempWeek.style.display = 'none';
					rowTempTwoMonths.style.display = 'none';
					rowTempQuarter.style.display = 'none';
					rowStartDay.style.display = 'none';

					break;
				case 3:
		 			if(bIsIE)
		 			{
						rowTempTwoMonths.style.display = 'inline';
						rowStartDay.style.display = 'inline';
						rowDuration.style.display = 'inline';
		 			}
		 			else
		 			{
		 				rowTempTwoMonths.style.display = 'table-row';
		 				rowStartDay.style.display = 'table-row';
		 				rowDuration.style.display = 'table-row';
		 			}
		 			
		 			rowTempWeek.style.display = 'none';
					rowTempMonth.style.display = 'none';
					rowTempQuarter.style.display = 'none';

					break;
				case 4:
		 			if(bIsIE)
		 			{
						rowTempQuarter.style.display = 'inline';
						rowStartDay.style.display = 'inline';
						rowDuration.style.display = 'inline';
		 			}
		 			else
		 			{
		 				rowTempQuarter.style.display = 'table-row';
		 				rowStartDay.style.display = 'table-row';
		 				rowDuration.style.display = 'table-row';
		 			}
		 			
		 			rowTempWeek.style.display = 'none';
					rowTempMonth.style.display = 'none';
					rowTempTwoMonths.style.display = 'none';
				break;
			}
		}

 		function showHideEmailComp() 
 		{
 			//debugger;
 			var objCmbFreq = BITAMSurveyScheduler_SaveForm.AllowMultipleSurveys;
	 		var valueIDFreq = parseInt(objCmbFreq.options[objCmbFreq.selectedIndex].value);
 	 		
 			var objCmb = BITAMSurveyScheduler_SaveForm.CaptureVia;
 		 	var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);
 		 	
 		 	var rowStrUserRol = document.getElementById("Row_StrUserRol");
 		 	var rowUseCatalog = document.getElementById("Row_UseCatalog");
	 		var rowCatalogID = document.getElementById("Row_CatalogID");
	 		var rowAttributeID = document.getElementById("Row_AttributeID");
 		 	var rowEmailSubject = document.getElementById("Row_EmailSubject");
 		 	var rowStrCaptureEmails = document.getElementById("Row_StrCaptureEmails");
 		 	var rowEmailBody = document.getElementById("Row_EmailBody");
 		 	var rowCaptureVia = document.getElementById("Row_CaptureVia");
 		 	var rowEmailReplyTo = document.getElementById("Row_EmailReplyTo"); //Conchita 10-nov-2011 
 		 	
 		 	if(valueID==1)
 		 	{
 		 		rowStrUserRol.style.display = 'none';
 		 		
 		 		if(bIsIE)
 				{
 					rowUseCatalog.style.display = 'inline';
	 		 		rowEmailSubject.style.display = 'inline';
					rowEmailBody.style.display = 'inline';
					rowEmailReplyTo.style.display ='inline'; //Conchita 10-nov-2011
 				}
 				else
 				{
 					rowUseCatalog.style.display = 'table-row';
	 		 		rowEmailSubject.style.display = 'table-row';
					rowEmailBody.style.display = 'table-row';
					rowEmailReplyTo.style.display='table-row'; //conchita 10-nov-2011
 				}
 				
 				showHideCatalogs();
 		 	}
 		 	else
 		 	{
 		 		if(bIsIE)
 				{
	 		 		rowStrUserRol.style.display = 'inline';
 				}
 				else
 				{
 					rowStrUserRol.style.display = 'table-row';
 				}

 		 		rowUseCatalog.style.display = 'none';
				rowCatalogID.style.display = 'none';
				rowAttributeID.style.display = 'none';
 		 		rowEmailSubject.style.display = 'none';
				rowEmailBody.style.display = 'none';
				rowStrCaptureEmails.style.display = 'none';
				rowEmailReplyTo.style.display = 'none'; //Conchita 10-nov-2011
 		 	}
 		 	
 		 	showHideFrequency();
 		 	
	 		//Validación para que el combo del editor HTML sólo se muestre en la captura tipo E-Mail
 			var objEditorCmb = BITAMSurveyScheduler_SaveForm.EditorType;
 			if(objEditorCmb)
 			{
 				if(valueID == 1)
		 		{	//Tipo de captura: "E-Mail"
		 			Row_EditorType.style.display = (bIsIE)?"inline":"table-row";
		 		}
		 		else
		 		{	//Tipo de captura: "Web/Mobile"
		 			Row_EditorType.style.display='none';
		 		}
 			}
 		}
                
        function showHideEmailFilter() {
            var objCmb = BITAMSurveyScheduler_SaveForm.UseCatalog;
            var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);
            var rowEmailFilter = document.getElementById("Row_EmailFilter");
            
            //si esta use email catalog verificamos que el catalogo seleccionado
            //sea el mismo que el de la encuesta seleccionada
            if(valueID==1) {
                
                //obtenemos el surveyid
                var surveyID = parseInt(BITAMSurveyScheduler_SaveForm.SurveyID.value);
                //obtenemos el valor del catalogo seleccionado
                var catalogID = parseInt(BITAMSurveyScheduler_SaveForm.CatalogID.value);
                
                var xmlObj = getXMLObject();
                if (xmlObj == null)
                {
                    alert('<?=translate('XML Object not supported')?>');
                    return(new Array);
                }
                
                //en verifySurveySchedulerCatalog.php se verifica la condicion de que
                //el catalogo del scheduler sea el mismo que el de la encuesta seleccionada
                //y tambien que el catalogo tenga mas d eun atributo
                xmlObj.open('POST', 'verifySurveySchedulerCatalog.php', false);
                xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
                xmlObj.send('SurveyID='+surveyID+'&CatalogID='+catalogID);

                if(xmlObj.status == 404)
                {
                        alert('<?=translate('Unable to launch validation process')?>');
                        return false;
                }
                var strResponseData = Trim(unescape(xmlObj.responseText));
                
                if(strResponseData == '') {
                    if(bIsIE) {
                        rowEmailFilter.style.display = 'inline';
                    } else {
                        rowEmailFilter.style.display = 'table-row';
                    }
                } else {
                    rowEmailFilter.style.display = 'none';
                }
            } else {
                rowEmailFilter.style.display = 'none';
            }
        }
 		
 		function showHideCatalogs()
 	 	{
 	 		//debugger;
 			var objCmbVia = BITAMSurveyScheduler_SaveForm.CaptureVia;
 		 	var valueIDVia = parseInt(objCmbVia.options[objCmbVia.selectedIndex].value);
 	 		
 			var objCmb = BITAMSurveyScheduler_SaveForm.UseCatalog;
 		 	var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);

 		 	var rowUseCatalog = document.getElementById("Row_UseCatalog");
 		 	var rowCatalogID = document.getElementById("Row_CatalogID");
 		 	var rowAttributeID = document.getElementById("Row_AttributeID");
 		 	var rowCaptureEmails = document.getElementById("Row_StrCaptureEmails");
 		 	var rowCaptureVia = document.getElementById("Row_CaptureVia");

 			var objCmbVia = BITAMSurveyScheduler_SaveForm.CaptureVia;
 		 	var valueViaID = parseInt(objCmbVia.options[objCmbVia.selectedIndex].value);

	 		if(valueViaID==1)
	 		{
		 		//UseCatalog = Yes
		 		if(valueID==1)
		 		{
		 			if(bIsIE)
		 			{
		 				rowCatalogID.style.display = 'inline';
		 				rowAttributeID.style.display = 'inline';
		 			}
		 			else
		 			{
		 				rowCatalogID.style.display = 'table-row';
		 				rowAttributeID.style.display = 'table-row';
		 			}
		 			
		 			rowCaptureEmails.style.display = 'none';
		 		}
		 		else
		 		{
		 			//UseCatalog = No
		 			if(bIsIE)
		 			{
		 				rowCaptureEmails.style.display = 'inline';
		 			}
		 			else
		 			{
		 				rowCaptureEmails.style.display = 'table-row';
		 			}
		 			
		 			rowCatalogID.style.display = 'none';
		 			rowAttributeID.style.display = 'none';
		 		}
	 		}
			showHideEmailFilter();
 	 	}

 		function verifyFieldsAndSave(target)
 		{
			var strAlert = '';
			
 			if(Trim(BITAMSurveyScheduler_SaveForm.SchedulerName.value)=='')
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Description"))?>';
 			}
 			
  	 		var objCmb = BITAMSurveyScheduler_SaveForm.AllowMultipleSurveys;
	 		var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);

	 		if(valueID==1)
	 		{
	 			var CaptureStartDateTime = new Date();
				var CaptureEndDateTime = new Date();
				
				var strToSplitCaptureStartDate = BITAMSurveyScheduler_SaveForm.CaptureStartDate.value.split(" ", 2);
				var strToSplitCaptureEndDate = BITAMSurveyScheduler_SaveForm.CaptureEndDate.value.split(" ", 2);
				strToSplitCaptureStartDate = strToSplitCaptureStartDate[0].split("-",3);
				strToSplitCaptureEndDate = strToSplitCaptureEndDate[0].split("-",3);
				
				if (strToSplitCaptureStartDate[0]  == '0000' || strToSplitCaptureStartDate[1] == '00' || strToSplitCaptureStartDate[2] == '00')
				{
					strAlert+= '\n'+"<?=translate("Invalid start date of capture")?>" + ": " + strToSplitCaptureStartDate[0] + "-" + strToSplitCaptureStartDate[1] + "-"+ strToSplitCaptureStartDate[2] + "\n";
				}
	
				if (strToSplitCaptureEndDate[0] == '0000' || strToSplitCaptureEndDate[1] == '00' || strToSplitCaptureEndDate[2] == '00') 
				{
					strAlert+= '\n'+"<?=translate("Invalid end date of capture")?>" + ": " + strToSplitCaptureEndDate[0] + "-" + strToSplitCaptureEndDate[1] + "-"+ strToSplitCaptureEndDate[2] + "\n";
				}
	
				CaptureStartDateTime.setFullYear(parseInt(strToSplitCaptureStartDate[0], 10), parseInt(strToSplitCaptureStartDate[1], 10), parseInt(strToSplitCaptureStartDate[2], 10));
				CaptureEndDateTime.setFullYear(parseInt(strToSplitCaptureEndDate[0], 10), parseInt(strToSplitCaptureEndDate[1], 10), parseInt(strToSplitCaptureEndDate[2], 10));
				
				if (CaptureStartDateTime > CaptureEndDateTime)
				{
					strAlert+= '\n'+"<?=translate("The start date of capture must be greater than or equal to the end date of capture")?>";
				}
	 		}
                        
            //si esta habilitada la captura por email se deben de valid los emails
            var useCatalog = parseInt(BITAMSurveyScheduler_SaveForm.UseCatalog.value);
            if(useCatalog == 1) {
                //obtenemos el surveyid
                var surveyID = parseInt(BITAMSurveyScheduler_SaveForm.SurveyID.value);
                //obtenemos el valor del catalogo seleccionado
                var catalogID = parseInt(BITAMSurveyScheduler_SaveForm.CatalogID.value);
                //var obtenemos el atributo
                var attributeID = parseInt(BITAMSurveyScheduler_SaveForm.AttributeID.value);
                var xmlObj = getXMLObject();
                if (xmlObj == null)
                {
                        alert('<?=translate('XML Object not supported')?>');
                        return(new Array);
                }
                xmlObj.open('POST', 'verifyAttributeEmail.php', false);
                xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
                xmlObj.send('AttributeID='+attributeID+'&CatalogID='+catalogID);

                if(xmlObj.status == 404)
                {
                        alert('<?=translate('Unable to launch validation process')?>');
                        return false;
                }
                var strResponseData = Trim(unescape(xmlObj.responseText));
                strAlert += '\n'+strResponseData;
            }
 			
 			if(Trim(strAlert)!='')
 			{
 				alert(strAlert);
 			}
 			else
 			{
  				//Llenar los campos en los que se guardarán los componentes del diseñador
  				var newEditor = false;
  				for(var divName in aHTMLDivObjects)
  				{
  					var textAreaName = divName.replace("HDiv", "");
  					textAreaName = textAreaName.replace("Default", "");
  					textAreaNameObj = document.getElementById(textAreaName);
  					textAreaNameObj.value=aHTMLDivObjects[divName].oPart.save(true,',');
  					if(textAreaNameObj.value!='')
  					{
  						newEditor = true;
  					}
  				}
  				//Si ya se utilizó por lo menos una vez el nuevo editor en la pantalla de sección
  				//entonces los campos HTML que se manejaban anteriormente serán reemplazados con el HTML
  				//generado a partir del nuevo editor, esto se hará para todos los dispositivos
  				if(newEditor==true)
  				{
	  				for(var divName in aHTMLDivObjects)
	  				{
	  					var textAreaName = divName.replace("HDiv", "");
	  					textAreaName = textAreaName.replace("Default", "");
						textAreaName = textAreaName+'HTML';
	  					textAreaNameObj = document.getElementById(textAreaName);
	  					textAreaNameObj.value=aHTMLDivObjects[divName].oPart.saveHTML();
  						//alert(textAreaName+' - saveHTML(): '+aHTMLDivObjects[divName].oPart.saveHTML());
  						//alert(textAreaName+' - textAreaNameObj.value: '+textAreaNameObj.value);
  						//alert(textAreaName+' - textAreaNameObj.innerHTML: '+textAreaNameObj.innerHTML);

	  				}
  				} 				
 				BITAMSurveyScheduler_Ok(target);
 			}
 		}
 		
		function openAssignUsers()
		{
			userIDsObj = BITAMSurveyScheduler_SaveForm.StrUserRol;
			//window.showModalDialog('assignSchedulerUsers.php?SchedulerID=<?=$this->SchedulerID?>', [window], "dialogHeight:320px; dialogWidth:600px; center:yes; help:no; resizable:yes; status:no");
			openWindowDialog('assignSchedulerUsers.php?SchedulerID=<?=$this->SchedulerID?>', [window], [], 600, 320, openAssignUsersDone, 'openAssignUsersDone');
			/*
			var strInfo = window.returnValue;

			//Actualiza la cadena de los menus visibles
			if(strInfo!=null && strInfo!=undefined)
			{
				userIDsObj.value = strInfo;
			}
			*/
		}
		
		function openAssignUsersDone(sValue, sParams)
		{
			var strInfo = sValue;
			//Actualiza la cadena de los menus visibles
			if(strInfo!=null && strInfo!=undefined)
			{
				userIDsObj.value = strInfo;
			}
		}
		
<?
		if(!$this->isNewObject() && $this->UseCatalog==1 && $this->CatalogID>0 && $this->AttributeID>0)
		{
?>
		function showSavingMessage(showMsg) 
		{
			var objUpdMsg = document.getElementById("spanUpdMsg");
			var divMsg = document.getElementById('saveMsg'); 
			
			if(showMsg==1)
			{
				objUpdMsg.innerHTML='<img src="images/progressbar.gif"><?=translate("Scheduler Email is being updated")?>'; 
			}
			
			objUpdMsg.style.display = (showMsg) ? 'inline':'none'
			
			return true;
		}
		
 		function updateSchedEmail()
 		{
 			//Indicar que se esta iniciando el proceso para actualizar mapeo en la FactAccounts
 			showSavingMessage(1);
 			
 			//Ejecutar forma que actualiza el mapeo de la FactAccounts
 			frmUpdSchedEmail.submit();
 		}
<?
		}
?>
	
 		function onChangeEditor()
 		{
 			var objCmbEditor = BITAMSurveyScheduler_SaveForm.EditorType;
 			var editorTypeValue = parseInt(objCmbEditor.options[objCmbEditor.selectedIndex].value);

 			if(editorTypeValue==1)
 			{
 				document.getElementById("EmailBodySpanDesigner").style.display="inline";
 				document.getElementById("EmailBodyDefaultDesDiv").style.display="block";
 				document.getElementById("EmailBodyDefaultDiv").style.display="none";
 			}
 			else
 			{
 				document.getElementById("EmailBodySpanDesigner").style.display="none";
 				document.getElementById("EmailBodyDefaultDesDiv").style.display="block";
 				document.getElementById("EmailBodyDefaultDiv").style.display="block";
 			}
 		}
	</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
		global $gbIsGeoControl;
        //2014.11.19 JCEM #A123
?>
	<script language="JavaScript">
		objOkSelfButton = document.getElementById("BITAMSurveyScheduler_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMSurveyScheduler_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		
        var objAx = document.getElementsByName('AllowMultipleSurveys');
        if (objAx[0]) {
            objAx[0].value = 0;
            showHideFrequency();
            
            var rowField = document.getElementById("Row_AllowMultipleSurveys");
            if (rowField) {
                rowField.style.display = 'none';
            }
        }       

        
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMSurveyScheduler_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
		
 		if ($gbIsGeoControl) {
?>
 		var rowField = document.getElementById("Row_CaptureVia");
 		if (rowField) {
 			rowField.style.display = 'none';
 		}
 		var rowField = document.getElementById("Row_AllowMultipleSurveys");
 		if (rowField) {
 			rowField.style.display = 'none';
 		}
 		var rowField = document.getElementById("Row_CaptureStartDate");
 		if (rowField) {
 			rowField.style.display = 'none';
 		}
 		var rowField = document.getElementById("Row_CaptureEndDate");
 		if (rowField) {
 			rowField.style.display = 'none';
 		}
 		var rowField = document.getElementById("Row_Frequency");
 		if (rowField) {
 			rowField.style.display = 'none';
 		}
<?
 		}
 		
?>
	</script>

<?
		if(!$this->isNewObject() && $this->UseCatalog==1 && $this->CatalogID>0 && $this->AttributeID>0)
		{
?>
	<!--<iframe name="frameUpdateSchedulerEmail" style="display:none"></iframe>-->
	<form name="frmUpdSchedEmail" id="frmUpdSchedEmail" method="POST" action="updateSchedulerEmail.php" target="load">
		<input type="hidden" id="SchedulerID" name="SchedulerID" value="<?=$this->SchedulerID?>">
	</form>
<?
		}
	}

	function insideEdit()
	{
?>
		objSpanChange = document.getElementById("changeUserIDs");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground.gif)';
		objSpanChange.style.cursor = 'pointer';
		objSpanChange.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChange.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChange.onclick = new Function("openAssignUsers();");
<?
	}
	
	function insideCancel()
	{
?>
		objSpanChange = document.getElementById("changeUserIDs");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground_off.gif)';
		objSpanChange.style.cursor = 'default';
		objSpanChange.onmouseover = "";
		objSpanChange.onmouseout = "";
		objSpanChange.onclick = "";
<?
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}

	function isEnabledCaptureThisDay($aDate=null)
	{
		require_once("utils.inc.php");

		$schedulerInstance = $this;
		$isEnabledCapture = false;
		
		if(is_null($aDate))
		{
			$today = date("Y-m-d");
		}
		else 
		{
			$today = substr($aDate, 0, 10);
		}
		
		$captureStartDate = array();
		
		if(!is_null($schedulerInstance->CaptureStartDate) && trim($schedulerInstance->CaptureStartDate)!="")
		{
			$schedulerInstance->CaptureStartDate = substr($schedulerInstance->CaptureStartDate, 0, 10);
		}
		else 
		{
			$schedulerInstance->CaptureStartDate = "";
		}

		if(!is_null($schedulerInstance->CaptureEndDate) && trim($schedulerInstance->CaptureEndDate)!="")
		{
			$schedulerInstance->CaptureEndDate = substr($schedulerInstance->CaptureEndDate, 0, 10);
		}
		
		$whenToCaptureValue = 0;
		
		if($schedulerInstance->CaptureVia==0)
		{
			$whenToCaptureValue = $schedulerInstance->WhenToCaptureType;
		}
		else 
		{
			$whenToCaptureValue = 0;
		}
		
		//Obtener fecha de inicio de captura
		switch($whenToCaptureValue)
		{
			//Caso en el que se selecciona la opción 'Todos los días'
			case 0:
			{
				if($today >= $schedulerInstance->CaptureStartDate && $today <= $schedulerInstance->CaptureEndDate)
				{
					$captureStartDate[0] = $today;
				}
				break;	
			}

			//Caso en el que se especifica frecuencia semanal y tales dias de la semana
			case 1:
			{
				
				if($today >= $schedulerInstance->CaptureStartDate && $today <= $schedulerInstance->CaptureEndDate)
				{
					
					$month = mon($today);
					$year = year($today);
	
					if(trim($schedulerInstance->WhenToCaptureValue)!="")
					{
						$capInfo = explode(',',$schedulerInstance->WhenToCaptureValue);
						
						//$week=0 (Todas las semanas), $week=1 (primer semana), ..., $week=4 (cuarta semana), $week=5 (última semana del mes)
						//$day=0 (Domingo), $day=1 (Lunes) ... $day=6 (Sábado)
						foreach($capInfo as $day)
						{
							$tempCaptureStartDate = getDatesByYearMonthDay($year, $month, $day);
							$numCaptureStartDates = count($tempCaptureStartDate);
							
							for($i=0; $i<$numCaptureStartDates ;$i++)
							{
								$captureStartDate[] = formatADateTime($tempCaptureStartDate[$i],'yyyy-mm-dd');
							}
						}
					}
				}
				break;
			}
			
			//Caso en el que se especifica frecuencia mensual, tal dia del mes y duracion en dias 
			case 2:
			{
				if($today >= $schedulerInstance->CaptureStartDate && $today <= $schedulerInstance->CaptureEndDate)
				{
					
					//Se considera el mes y año actual
					$year = year($today);
					$month = mon($today);
					$numDaysOfMonth = numDaysOfMonth($year, $month);

					if($month < 10)
					{
						$month='0'.$month;
					}

					//Se considera el primer día seleccionado en el calendario y si no está ninguno seleccionado se considera el día 1
					if(trim($schedulerInstance->WhenToCaptureValue)!="")
					{
						$positionDay = (int)$schedulerInstance->WhenToCaptureValue;
						
						if($positionDay<=$numDaysOfMonth)
						{
							$daysArray[] = $positionDay;
							$duration = (int)$schedulerInstance->Duration;

							for($i=1; $i<$duration; $i++)
							{
								$positionDay = $positionDay + 1;
	
								if($positionDay<=$numDaysOfMonth)
								{
									$daysArray[] = $positionDay;
								}
								else 
								{
									break;
								}
							}
							
							foreach ($daysArray as $day)
							{
								if($day < 10)
								{
									$day='0'.$day;
								}
	
								$captureStartDate[] = $year ."-".$month."-".$day;
							}
						}
					}
				}
				break;
			}
			
			
			//Caso en el que se especifica frecuencia bimestral, tal mes non o par, dia de inicio del mes y duracion en dias 
			case 3:
			{
				if($today >= $schedulerInstance->CaptureStartDate && $today <= $schedulerInstance->CaptureEndDate)
				{
					//Se considera el mes y año actual
					$year = year($today);
					$month = mon($today);
					$numDaysOfMonth = numDaysOfMonth($year, $month);

					//Se considera el primer día seleccionado en el calendario y si no está ninguno seleccionado se considera el día 1
					if(trim($schedulerInstance->WhenToCaptureValue)!="")
					{
						$typeMonth = (int)$schedulerInstance->WhenToCaptureValue;
						
						if($typeMonth==2)
						{
							if(($month%2)==0)
							{
								$condition = true;
							}
							else 
							{
								$condition = false;
							}
						}
						else 
						{
							if(($month%2)!=0)
							{
								$condition = true;
							}
							else 
							{
								$condition = false;
							}
						}
						
						if($condition==true)
						{
							$positionDay = (int)$schedulerInstance->StartDay;
							
							if($positionDay<=$numDaysOfMonth)
							{
								$daysArray[] = $positionDay;
								$duration = (int)$schedulerInstance->Duration;
								
								for($i=1; $i<$duration; $i++)
								{
									$positionDay = $positionDay + 1;
		
									if($positionDay<=$numDaysOfMonth)
									{
										$daysArray[] = $positionDay;
									}
									else 
									{
										break;
									}
									
								}
								
								if($month < 10)
								{
									$month='0'.$month;
								}

								foreach ($daysArray as $day)
								{
									if($day < 10)
									{
										$day='0'.$day;
									}
		
									$captureStartDate[] = $year ."-".$month."-".$day;
								}
							}
						}
					}
				}
				break;
			}
			//Caso en el que se especifica frecuencia trimestral, tal mes 1,2 o 3, dia de inicio del mes y duracion en dias 
			case 4:
			{
				if($today >= $schedulerInstance->CaptureStartDate && $today <= $schedulerInstance->CaptureEndDate)
				{
					//Se considera el mes y año actual
					$year = year($today);
					$month = mon($today);
					$numDaysOfMonth = numDaysOfMonth($year, $month);

					//Se considera el primer día seleccionado en el calendario y si no está ninguno seleccionado se considera el día 1
					if(trim($schedulerInstance->WhenToCaptureValue)!="")
					{
						$typeMonth = (int)$schedulerInstance->WhenToCaptureValue;
						
						if($typeMonth==1)
						{
							switch($month)
							{
								case 1:
								case 4:
								case 7:
								case 10:
									$condition = true;
									break;
								default:
									$condition = false;
									break;
							}
						}
						else if($typeMonth==2)
						{
							switch($month)
							{
								case 2:
								case 5:
								case 8:
								case 11:
									$condition = true;
									break;
								default:
									$condition = false;
									break;
							}
						}
						else if($typeMonth==3)
						{
							switch($month)
							{
								case 3:
								case 6:
								case 9:
								case 12:
									$condition = true;
									break;
								default:
									$condition = false;
									break;
							}
						}
						
						if($condition==true)
						{
							$positionDay = (int)$schedulerInstance->StartDay;
							
							if($positionDay<=$numDaysOfMonth)
							{
								$daysArray[] = $positionDay;
								$duration = (int)$schedulerInstance->Duration;

								for($i=1; $i<$duration; $i++)
								{
									$positionDay = $positionDay + 1;
		
									if($positionDay<=$numDaysOfMonth)
									{
										$daysArray[] = $positionDay;
									}
									else 
									{
										break;
									}
								}
								
								if($month < 10)
								{
									$month='0'.$month;
								}
		
								foreach ($daysArray as $day)
								{
									if($day < 10)
									{
										$day='0'.$day;
									}
		
									$captureStartDate[] = $year ."-".$month."-".$day;
								}
							}
						}
					}
				}
				break;
			}
		}

		//Si la fecha de hoy cumple con la condición del scheduler entonces si se indica que el dia de hoy
		//esta habilitada la captura de encuestas
		
		if(in_array($today, $captureStartDate))
		{
			$isEnabledCapture = true;
		}
		
		return $isEnabledCapture;
	}
	
	function  addButtons($aUser)
	{
		if(!$this->isNewObject() && $this->UseCatalog==1 && $this->CatalogID>0 && $this->AttributeID>0)
		{
?>
			<!--<a href="#" id="updateSchedEmail" class="alinkescfav" onclick="updateSchedEmail();"><img src="images/generate.png" displayMe="1"><?=translate("Update Scheduler Email")?>&nbsp;&nbsp;</a>-->
			<button  id="btnUpdateSchedEmail" class="alinkescfav" onclick="updateSchedEmail();"><img src="images/generate.png" alt="<?=translate('Update Scheduler Email')?>" title="<?=translate('Update Scheduler Email')?>" displayMe="1" /> <?=translate("Update Scheduler Email")?></button>
			&nbsp;&nbsp;&nbsp;
			<span id="spanUpdMsg" class="spanInfo" style="display:none;"></span>
<?
		}
	}

	//GCRUZ 2015-08-20 Actualizar usuario/rol del scheduler
	// intObjectType: otyUser=24, otyUserGroup=rol
	// intOperation: 'add'=agregar, 'remove'=remover
	// intObjectID: id de usuario/rol
	public function UpdateSchedulerUserRol($intObjectType, $intObjectID, $strOperation)
	{
		$tableName = '';
		$objectField = '';
		switch($intObjectType)
		{
			case otyUser:
				$tableName = 'si_sv_surveyscheduleruser';
				$objectField = 'UserID';
				break;
			case otyUserGroup:
				$tableName = 'si_sv_surveyschedulerrol';
				$objectField = 'RolID';
				break;
		}

		$aSQL = "";
		switch($strOperation)
		{
			case 'add':
				//Validar si no existe ya
				$sql = "SELECT COUNT(*) AS 'count' FROM ".$tableName." WHERE SchedulerID = ".$this->SchedulerID." AND ".$objectField." = ".$intObjectID;
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS && $aRS->_numOfRows != 0)
					if (intval($aRS->fields['count']) > 0)
						return array(true, '');
				$aSQL .= "INSERT INTO ".$tableName." (SchedulerID, ".$objectField.") VALUES (".$this->SchedulerID.",".$intObjectID.")";
				break;
			case 'remove':
				$aSQL .= "DELETE FROM ".$tableName." WHERE SchedulerID = ".$this->SchedulerID." AND ".$objectField." = ".$intObjectID;
				break;
		}
		$aRS = $this->Repository->DataADOConnection->Execute($aSQL);
		if (!$aRS || $this->Repository->DataADOConnection->Affected_Rows() == 0)
			return array(false, 'UpdateError. ErrorNo='.$this->Repository->DataADOConnection->ErrorNo().' ErrorMsg='.$this->Repository->DataADOConnection->ErrorMsg().' Executing:'.$aSQL);
		return array(true, '');
	}
}

class BITAMSurveySchedulerCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository, $anArrayOfSchedulerIDs = null)
	{
		$anInstance = new BITAMSurveySchedulerCollection($aRepository);
		
		$where = "";
		if (!is_null($anArrayOfSchedulerIDs))
		{
			switch (count($anArrayOfSchedulerIDs))
			{
				case 0:
					break;
				case 1:
					$where = " AND t1.SchedulerID = ".((int) $anArrayOfSchedulerIDs[0]);
					break;
				default:
					foreach ($anArrayOfSchedulerIDs as $aSchedulerID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$aSchedulerID; 
					}
					if ($where != "")
					{
						$where = " AND t1.SchedulerID IN (".$where.")";
					}
					break;
			}
		}
		//Conchita Agregado EmailReplyTo 10-nov-2011
		$sql = "SELECT t1.SchedulerID, t1.SurveyID, t1.SchedulerName, t1.AllowMultipleSurveys, t1.CaptureStartDate, t1.CaptureEndDate, t1.CaptureType, t1.WhenToCapture,
				t1.CaptureVia, t1.UseCatalog, t1.CatalogID, t1.AttributeID, t1.EmailFilter, t1.EmailSubject, t1.EmailBody,
				t1.StartDay, t1.Duration, t1.EmailReplyTo
				FROM SI_SV_SurveyScheduler t1, SI_SV_Survey t2 WHERE t1.SurveyID = t2.SurveyID AND t2.Status <> 2 ".$where." ORDER BY t1.SchedulerName";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMSurveyScheduler::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function NewInstanceBySurveyAndUser($aRepository, $surveyID, $userID)
	{
		$anInstance = new BITAMSurveySchedulerCollection($aRepository);
		
		//Obtenemos todos los SchedulerIDs donde el usuario esta registrado
		//ya sea por el UserID o por el RolID
		
		//Obtenemos  los roles de los usuarios
		$roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $userID);
		
		//Obtener el listado de schedulers disponibles para el usuario logueado x user
		$sql = "SELECT DISTINCT A.SchedulerID FROM SI_SV_SurveyScheduler A, SI_SV_SurveySchedulerUser B
				WHERE A.SchedulerID = B.SchedulerID AND A.SurveyID = ".$surveyID." AND B.UserID = ".$userID;
		$recordset = $aRepository->DataADOConnection->Execute($sql);
		if ($recordset === false) 
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	
		$anArrayOfSchedulerIDs = array();
		
		while (!$recordset->EOF)
		{
			$schedulerID = (int)$recordset->fields["schedulerid"];
			$anArrayOfSchedulerIDs[$schedulerID] = $schedulerID;
			
			$recordset->MoveNext();
		}
	
		if(count($roles)>0)
		{
			//Obtener el listado de schedulers disponibles para el usuario logueado x rol
			$sql = "SELECT DISTINCT A.SchedulerID FROM SI_SV_SurveyScheduler A, SI_SV_SurveySchedulerRol B
					WHERE A.SchedulerID = B.SchedulerID AND A.SurveyID = ".$surveyID." AND B.RolID IN (".implode(",", $roles).")";
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			if ($recordset === false) 
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			while (!$recordset->EOF)
			{
				$schedulerID = (int)$recordset->fields["schedulerid"];
				$anArrayOfSchedulerIDs[$schedulerID] = $schedulerID;
				
				$recordset->MoveNext();
			}
		}
		
		/*****************************************************************************************************************/

		$where = "";
		if (count($anArrayOfSchedulerIDs)>0)
		{
			switch (count($anArrayOfSchedulerIDs))
			{
				case 0:
					break;
				case 1:
					foreach ($anArrayOfSchedulerIDs as $aSchedulerID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where = "WHERE t1.SchedulerID = ".((int)$aSchedulerID);
					}
					break;
				default:
					foreach ($anArrayOfSchedulerIDs as $aSchedulerID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$aSchedulerID; 
					}
					if ($where != "")
					{
						$where = "WHERE t1.SchedulerID IN (".$where.")";
					}
					break;
			}
		}
		
		if(count($anArrayOfSchedulerIDs)>0)
		{ //Conchita Agregado EmailReplyTo 10-nov-2011
			$sql = "SELECT t1.SchedulerID, t1.SurveyID, t1.SchedulerName, t1.AllowMultipleSurveys, t1.CaptureStartDate, t1.CaptureEndDate, t1.CaptureType, t1.WhenToCapture,
					t1.CaptureVia, t1.UseCatalog, t1.CatalogID, t1.AttributeID, t1.EmailFilter, t1.EmailSubject, t1.EmailBody,
					t1.StartDay, t1.Duration, t1.EmailReplyTo
					FROM SI_SV_SurveyScheduler t1 ".$where." ORDER BY t1.SchedulerName";
			$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				$anInstance->Collection[] = BITAMSurveyScheduler::NewInstanceFromRS($anInstance->Repository, $aRS);
				$aRS->MoveNext();
			}
		}
		
		return $anInstance;
	}
	
	function hasFrequency()
	{
		$hasFrequency = false;

		//Vamos a recorrer toda la coleccion para determinar si algunos de sus schedulers 
		//tiene activada la propiedad AllowMultipleSurveys = 1, en caso contrario sera 0
		if(!is_null($this->Collection))
		{
			foreach ($this->Collection as $anScheduler)
			{
				if($anScheduler->AllowMultipleSurveys==1)
				{
					$hasFrequency = true;
					break;
				}
			}
		}
		
		return $hasFrequency;
	}
	
	function isEnabledCaptureThisDay($aDate=null)
	{
		$isEnabledCapture = false;
		
		//Vamos a recorrer toda la coleccion para determinar si algunos de sus schedulers 
		//aplica en que el dia de hoy se puede capturar encuesta, en caso contrario se envia false
		if(!is_null($this->Collection))
		{
			foreach ($this->Collection as $anScheduler)
			{
				$blnAllowCapture = $anScheduler->isEnabledCaptureThisDay($aDate);
				if($blnAllowCapture==true)
				{
					$isEnabledCapture = true;
					break;
				}
			}
		}
		
		return $isEnabledCapture;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		return BITAMSurveySchedulerCollection::NewInstance($aRepository, null);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Schedulers");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=SurveySchedulerCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=SurveyScheduler";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SchedulerID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SchedulerName";
		$aField->Title = translate("Description");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}
?>