<?php

require_once("repository.inc.php");

trait BITAMObjectExt
{
	//@JAPR 2016-07-12: Corregido un bug, aún se seguía invocando a este método, y en algunas clases hacía referencia a Repository por lo cual se alcanzaba a ver un path que decía
	//"Home"/"Inicio", así que se sobreescribirá la función para ya no regresar elementos en el path
	function get_PathArray() {
		return array();
	}
	//@JAPR
	
	//Para la lista tipo Metro regresa la imagen correspondiente al elemento
	function get_Image() {
		$strImage = "";
		if (property_exists($this, "DisplayImage")) {
			$strImage = $this->DisplayImage;
		}
		return $strImage;
	}
	
	/* Regresa un array con la lista de los botones adicionales que se deben integrar a la toolbar (adicional a los defaults manejados por el propio framework),
	es el equivalente al addButtons del framework original con object.inc.php, sólo que en este caso no es HTML sino una definición en forma de array de objetos
	*/
	function get_CustomButtons() {
		return array();
	}
	
	function generateDetailForm($aUser)
	{
		require_once("formfield.inc.php");
		
		global $widthByDisp;
		
    	$myFormName = get_class($this);
    	$myFormTitle = $this->get_Title();
 		$myFormIDFieldName = $this->get_FormIDFieldName();
    	$myFormFields = $this->get_FormFields($aUser);
    	//HTML Designer
    	//Si algún campo utilizará el diseñador HTML entonces se incluyen las librerías
    	$includeHTMLDes = false;
	    foreach ($myFormFields as $aField)
		{
			if($aField->UseHTMLDes)
			{
				$includeHTMLDes=true;
				break;
			}
		}

?>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/<?=$_SESSION["PAuserLanguage"]?>.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/genericComponents.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/components.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/menucomponent.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/menu.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/dialogs.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/move_resize.js"></script>
		<link rel="stylesheet" type="text/css" href="css/menu.css">

		<script language="javascript">
		var visibleDivArray = new Array();
		</script>

	<div class="object_new" id="<?= $myFormName ?>">
	 	<script language="javascript">
			var cancel = false;
		</script>
<?
		$this->generateBeforeFormCode($aUser);
?>
	    <script language="javascript">
<?
	    foreach ($myFormFields as $aField)
		{
			if (!is_null($aField->Parent))
			{
?>
	        var <?= $myFormName ?>_<?= $aField->Name ?> = new Array();
<?
				foreach ($aField->Options as $owner => $values)
				{
			    	if ($aField->Type == "Object" || $aField->Type == "Array")
			    	{
?>
            <?= $myFormName ?>_<?= $aField->Name ?>['<?= $owner ?>'] = [
<?
						$first = true;
						foreach ($values as $key => $value)
						{
?>
			<?= $first ? "" : "," ?>['<?= $key ?>', '<?= str_replace("'", "\'", $value) ?>']
<?
							$first = false;
						}
?>
				];
<?
			    	}
			    	else
			    	{
?>
            <?= $myFormName ?>_<?= $aField->Name ?>['<?= $owner ?>'] = '<?= str_replace("'", "\'", $values) ?>';
<?
			    	}
				}
			}
		}
		if (!$this->IsNewObject())
 		{
?>
   	        function <?= $myFormName ?>_Remove()
	        {
<?
	        	$myFormTitleAux = str_replace("\\", "\\\\", $myFormTitle);
	        	$myFormTitleAux = str_replace("'", "\'", $myFormTitleAux);
	        	
	        	if($this->MsgConfirmRemove!="")
	        	{
?>
	        	var answer = confirm('<?=translate($this->MsgConfirmRemove)?>');
<?
	        	}
	        	else 
	        	{
?>
	        	var answer = confirm('<?= sprintf(translate("Do you want to remove this %s?"), $myFormTitleAux) ?>');
<?
	        	}
?>
				if (answer)
				{
                	<?= $myFormName ?>_RemoveForm.submit();
				}
			}
<?
		}
?>
 			function <?= $myFormName ?>_Edit()
			{
				if(typeof(<?= $myFormName ?>_SaveForm) == 'undefined'){
 					var <?= $myFormName ?>_SaveForm = document.getElementById('<?= $myFormName ?>_SaveForm');
				}
				
				var elements = <?= $myFormName ?>_SaveForm.elements;
				var firstElement = null;
				var tinyEditorCreated = false;
				for (var i = 0; i < elements.length; i++)
				{
					if (firstElement == null && elements[i].type !== 'hidden' && elements[i].id !== '1')
					{
						firstElement = elements[i];
					}
					if (elements[i].id != '1')
					{
						elements[i].disabled = false;
						var strCustomizedClass = elements[i].getAttribute('customizedClass');
						if (strCustomizedClass) {
							elements[i].className = strCustomizedClass;
						}
						else {
							elements[i].className = '';
						}
					}
					
					try
					{
						var tinyEditor = null;
						var strID = elements[i].id;
						if (strID !== null && strID && isNaN(strID)) {
							tinyEditor = tinymce.get(elements[i].id);
						}
						if(tinyEditor && !tinyEditorCreated)
						{
							if(document.getElementById(elements[i].id+'_toolbargroup') == undefined) {
								tinyEditor.remove();
								tinyEditor.settings.readonly = 0;
								tinyEditor.init();
								tinyEditorCreated = true;
							}
						}
					}
					catch(err)
					{
					}
				}
				
				var images = document.images;
				var length=images.length;
				var showImg;
				for (var i = 0; i < length; i++)
				{
					showImg = true;
					attrValue = images[i].getAttribute('displayMe');
					if (attrValue != null && !isNaN(attrValue))
					{
						if (parseInt(attrValue) == 0)
						{
							showImg = false;
						}
					}

					if (showImg)
					{
						images[i].style.display='inline';
					}

				}
				
<?
		foreach ($myFormFields as $aField)
    	{
    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    		{
				switch ($aField->Type)
				{
					case "Date":
					case "DateTime":
?>
				document.images['calendar_<?= $myFormName ?>_<?= $aField->Name ?>_img'].style.display = 'inline';
<?
						break;
				}
			}
		}
    	foreach ($myFormFields as $aField)
    	{
    		if ($aField->SubmitOnChange)
    		{
?>
    			<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value;
<?
    		}
    	}
		$aFieldIdx = 0;
    	foreach ($myFormFields as $aField)
    	{
    		if ($aField->OnChange != "")
    		{
?>
				<?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_Changed();
<?
    		}
    		$aFieldIdx++;
    	}
?>
			if(typeof(<?= $myFormName ?>_OkSelfButton) == 'undefined'){
					var <?= $myFormName ?>_OkSelfButton = document.getElementById('<?= $myFormName ?>_OkSelfButton');
			}
			
			if(typeof(<?= $myFormName ?>_CancelButton) == 'undefined'){
					var <?= $myFormName ?>_CancelButton = document.getElementById('<?= $myFormName ?>_CancelButton');
			}
			
			if(typeof(<?= $myFormName ?>_OkParentButton) == 'undefined'){
					var <?= $myFormName ?>_OkParentButton = document.getElementById('<?= $myFormName ?>_OkParentButton');
			}

				if (firstElement !== null)
				{
					//firstElement.focus();
				}
				<?= $myFormName ?>_OkSelfButton.style.display = 'inline';
<?
	    if(!$this->IsDialog)
	    {
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			if($this->HasSaveAndGoParent)
			{
?>
				<?= $myFormName ?>_OkParentButton.style.display = 'inline';
<?
			}
	    }
?>
<?
		if ($this->isNewObject())
		{
?>
<?
		    if(!$this->IsDialog)
		    {
?>

				<?= $myFormName ?>_OkNewButton.style.display = 'inline';
<?
		    }
?>
<?
		}
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		if($this->HasCancel)
		{
?>
				<?= $myFormName ?>_CancelButton.style.display = 'inline';
<?
		}
				$this->insideEdit();
?>
			}
			
			function <?= $myFormName ?>_Ok(target)
			{
				
<?
	foreach ($myFormFields as $aField)
    	{
    		if ($aField->IsDisplayOnly)
    		{
    				if($aField->Type == "Array")
    				{
?>
						var items = GetElementsByName(<?= $myFormName ?>_SaveForm, "<?= $aField->Name ?>[]");
						for (var i = 0; i < items.length; i++)
						{
							items[i].disabled = false;
						}
<?
    				}
					else 
					{
?>
				<?= $myFormName ?>_SaveForm.elements.namedItem("<?= $aField->Name ?>").disabled = false;
<?
					}
    		}
    	}
	foreach ($myFormFields as $aField)
    	{
    		if ($aField->Type == "Array")
    		{
?>
	        	var hasChecked = false;
	        	var field = GetElementsByName(<?= $myFormName ?>_SaveForm,'<?= $aField->Name ?>[]');
				for (var i = 0; (hasChecked == false) && (i < field.length); i++)
				{
					if (field[i].checked)
					{
	        			hasChecked = true;
					}
	        	}
	        	if (hasChecked)
	        	{
	        		<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>_EmptySelection.value = '0';
	        	}
	        	else
	        	{
	        		<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>_EmptySelection.value = '1';
	        	}
	        	//alert(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>_EmptySelection.value);
<?
    		}
    		if ($aField->FormatMask != "")
    		{
				switch ($aField->Type)
				{
					case "String":
					case "Password":
					case "Integer":
					case "Float":
					case "Date":
					case "DateTime":
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'];
<?
						break;
				}	
			}
		}

		if( $this->isNewObject()&&$this->IsDialog && array_key_exists($myFormIDFieldName,$myFormFields) && !$this->HasAutoKeyGeneration)
		{
?>
			if(Trim(<?= $myFormName ?>_SaveForm.<?= $myFormIDFieldName ?>.value) == "")
			{
					//Se require de return porq aunq cierre la ventana la ejecucion continua hasta q termina la funcion
					window.returnValue = null;
   					top.close();
   					return;
			}
			else
			{
					//Es un nuevo objeto en el q se puede modificar el ID
					<?= $myFormName ?>_SaveForm.go.value = target;
					<?= $myFormName ?>_SaveForm.submit();
			}
<?
		}
		else
		{
?>			
				//Es un nuevo objeto en el q no se muestra ID y por lo tanto no se puede modificar el ID
				<?= $myFormName ?>_SaveForm.go.value = target;
				<?= $myFormName ?>_SaveForm.submit();
<?
		}
		
    	foreach ($myFormFields as $aField)
    	{
    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    		{
				switch ($aField->Type)
				{
					case "Date":
					case "DateTime":
?>
				document.images['calendar_<?= $myFormName ?>_<?= $aField->Name ?>_img'].style.display = 'none';
<?
						break;
				}
			}
		}
?>
				var elements = <?= $myFormName ?>_SaveForm.elements;
				for (var i = 0; i < elements.length; i++)
				{
					if (elements[i].id != '2')
					{
						var strCustomizedClass = elements[i].getAttribute('customizedClass');
						if (strCustomizedClass) {
							strCustomizedClass = ' ' + strCustomizedClass;
						}
						else {
							strCustomizedClass = ' ';
						}
						elements[i].className = 'disabled' + strCustomizedClass;
						elements[i].disabled = true;
					}
				}
				
				var images = document.images;
				var length=images.length;
				var hideImg;
				for (var i = 0; i < length; i++)
				{
					hideImg = true;
					attrValue = images[i].getAttribute('displayMe');
					if (attrValue != null && !isNaN(attrValue))
					{
						if (parseInt(attrValue) == 1)
						{
							hideImg = false;
						}
					}
					if (hideImg)
					{
						images[i].style.display='none';
					}
				}
				
				
				<?= $myFormName ?>_OkSelfButton.style.display = 'none';
<?
	    if(!$this->IsDialog)
	    {
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			if($this->HasSaveAndGoParent)
			{
?>
				<?= $myFormName ?>_OkParentButton.style.display = 'none';
<?
			}
	    }
?>
<?
		if ($this->isNewObject())
		{
?>
<?
		    if(!$this->IsDialog)
		    {
?>

				<?= $myFormName ?>_OkNewButton.style.display = 'none';
<?
	    	}
		}
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		if($this->HasCancel)
		{
?>
				<?= $myFormName ?>_CancelButton.style.display = 'none';

<?		}
		if($this->IsDialog)
	    {		
	    		$fieldsToReturn=$this->GetFieldNamesToReturn();
	    		$NewID=$this->GetNewID();
	    		
?>				
				<?= $myFormName ?>_arrayToReturn=new Array;
				<?= $myFormName ?>_NewID=<?=$NewID?>;
<?
				for($iField=0;$iField<count($fieldsToReturn);$iField++)
				{
					
						if($fieldsToReturn[$iField]==$myFormIDFieldName&&$this->isNewObject()&&!array_key_exists($fieldsToReturn[$iField],$myFormFields))
						{
	?>					
						<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?= $myFormName ?>_NewID;
	<?
						}
						else
						{ 
							if(array_key_exists($fieldsToReturn[$iField],$myFormFields))
							{
								if($myFormFields[$fieldsToReturn[$iField]]->VisualComponent=="Radio")
								{
?>
							lengthRadio = <?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>.length;
							
							for(i=0; i<lengthRadio; i++)
							{
								if(<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>[i].checked)
								{
							<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>[i].value;
									break;
								}
							}
<?
								}
								else if($myFormFields[$fieldsToReturn[$iField]]->VisualComponent=="Checkbox" && $myFormFields[$fieldsToReturn[$iField]]->Type=="Boolean")
								{
?>									//Como es un dato tipo checkbox booleano se guardan el valor un arreglo de 2 elementos
									//el cual el primero campo (campo[0]) es un elemento tipo hidden y el segundo campo 
									//(campo[1]) es un elemento tipo checkbox
									//Cuando el campo[1] esta marcado entonces se toma su valor de dicho campo
									if(<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>[1].checked)
									{
										<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>[1].value;
									}
									else
									{
										//En caso contrario el valor se toma del campo[0]
										<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>[0].value;
									}
<?
								}
								else 
								{
	?>
							<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>.value;
						
	<?
								}						
							}
							else 
							{
								eval('$valueField=$this->'.$fieldsToReturn[$iField].";");
	?>
								
								<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?=$valueField?>;
	<?
							}
						}
				}
?>				
 				//top.returnValue = <?= $myFormName ?>_arrayToReturn;
 				window.top.returnValue = <?= $myFormName ?>_arrayToReturn;
   				//top.close();
<?
	    }
?>
			}
			
			function <?= $myFormName ?>_Cancel(user_click)
			{
				if(typeof(user_click) == 'undefined'){
					user_click = false;
				} else {
				}				
				
				cancel = true;
				
				if(typeof(<?= $myFormName ?>_SaveForm) == 'undefined'){
 					var <?= $myFormName ?>_SaveForm = document.getElementById('<?= $myFormName ?>_SaveForm');
				}
				
				<?= $myFormName ?>_SaveForm.reset();
<?
                $this->generateOnCancelFormCode($aUser);
?>
				
<?
    	foreach ($myFormFields as $aField)
    	{
    		if ($aField->FormatMask != "")
    		{
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.defaultValue;
<?
				switch ($aField->Type)
				{
					case "String":
					case "Password":
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatString(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'], '<?= $aField->FormatMask ?>');
<?
						break;
					case "Integer":
					case "Float":
?>

				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.style.textAlign = 'right';
<?
				if($aField->DefaultNumberZero == true)
				{
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatNumber(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'], '<?= $aField->FormatMask ?>');
<?
				}
				else
				{
?>
					if(Trim(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value)!='')
					{
					<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatNumber(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'], '<?= $aField->FormatMask ?>');
					}
					else
					{
						<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = '';
					}
<?
				}
						break;
						
					case "Date":
					case "DateTime":
?>

				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatDateTime(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'], '<?= $aField->FormatMask ?>');

<?
						break;
				}
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.onblur();

<?
			}
    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    		{
				switch ($aField->Type)
				{
					case "Date":
					case "DateTime":
?>
				document.images['calendar_<?= $myFormName ?>_<?= $aField->Name ?>_img'].style.display = 'none';
<?
						break;
				}
			}
		}
		
		$aFieldIdx = 0;
    	foreach ($myFormFields as $aField)
    	{
    		if ($aField->OnChange != "")
    		{
?>
				<?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_Changed();
<?
    		}
    		$aFieldIdx++;
    	}
?>

				if(typeof(<?= $myFormName ?>_SaveForm) == 'undefined'){
						var <?= $myFormName ?>_SaveForm = document.getElementById('<?= $myFormName ?>_SaveForm');
				}

				var elements = <?= $myFormName ?>_SaveForm.elements;
				for (var i = 0; i < elements.length; i++)
				{
					if (elements[i].id != '2')
					{
						var strCustomizedClass = elements[i].getAttribute('customizedClass');
						if (strCustomizedClass) {
							strCustomizedClass = ' ' + strCustomizedClass;
						}
						else {
							strCustomizedClass = ' ';
						}
						elements[i].className = 'disabled' + strCustomizedClass;
						elements[i].disabled = true;
					}
				}
				
				var images = document.images;
				var length=images.length;
				var hideImg;
				for (var i = 0; i < length; i++)
				{
					hideImg = true;
					attrValue = images[i].getAttribute('displayMe');
					if (attrValue != null && !isNaN(attrValue))
					{
						if (parseInt(attrValue) == 1)
						{
							hideImg = false;
						}
					}
					if (hideImg)
					{
						images[i].style.display='none';
					}
				}
				
				if(typeof(<?= $myFormName ?>_OkSelfButton) == 'undefined'){
 					var <?= $myFormName ?>_OkSelfButton = document.getElementById('<?= $myFormName ?>_OkSelfButton');
				}

				if(typeof(<?= $myFormName ?>_CancelButton) == 'undefined'){
 					var <?= $myFormName ?>_CancelButton = document.getElementById('<?= $myFormName ?>_CancelButton');
				}
				
				if(typeof(<?= $myFormName ?>_OkParentButton) == 'undefined'){
 					var <?= $myFormName ?>_OkParentButton = document.getElementById('<?= $myFormName ?>_OkParentButton');
				}

				<?= $myFormName ?>_OkSelfButton.style.display = 'none';
				
<?
	    if(!$this->IsDialog)
	    {
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			if($this->HasSaveAndGoParent)
			{
?>
				<?= $myFormName ?>_OkParentButton.style.display = 'none';
<?
			}
	    }

	    if ($this->isNewObject())
		{
		    if(!$this->IsDialog)
		    {
?>

				<?= $myFormName ?>_OkNewButton.style.display = 'none';
<?
		    }
		}
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		if($this->HasCancel)
		{
?>
				<?= $myFormName ?>_CancelButton.style.display = 'none';
                    
<?
		}
				$this->insideCancel();
?>

			}
			
			function <?= $myFormName ?>_CancelDialog()
			{
				window.returnValue = null;
   				top.close();
			}
<?
		$aFieldIdx = 0;
    	foreach ($myFormFields as $aField)
    	{
    		$aFieldName = $aField->Name;
    		if (is_null($aField->Parent))
    		{
    			$aFieldParentName = "";
    		}
    		else
    		{    		
    			$aFieldParentName = $aField->Parent->Name;
    		}
    		if ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange)
    		{
?>
	        function <?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_OnChange()
	        {
<?
				//@JAPR 2012-05-30: Agregada la propiedad OnChangeBeforeUpdate porque en un caso especial de carga de datos dinámica, cambiar uno 
				//de los padres necesitaba limpiar el array de datos dinámicos debido a filtros dependientes del padre, así que si primero se
				//ejecutaba el Update de los FormFields dependientes, mostraba los datos ya cargados en lugar de ir al server por nuevos datos,
				//lo cual se logra precisamente limpiando el array primero (tiene que ver con la propiedad DynamicLoadFn)
				if (isset($aField->OnChangeBeforeUpdate) && $aField->OnChangeBeforeUpdate == true)
				{
		    		if ($aField->OnChange != "")
	    			{
?>
				var result = <?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_Changed();
<?
	    			}
	    			else
	    			{
?>
				var result = true;
<?
	    			}
				}
				//@JAPR

				foreach ($aField->Children as $aChild)
				{
?>
            	<?= $myFormName ?>_<?= $aChild->Name ?>_Update();
<?
				}
				
				//@JAPR 2012-05-30: Agregada la propiedad OnChangeBeforeUpdate porque en un caso especial de carga de datos dinámica, cambiar uno 
				//de los padres necesitaba limpiar el array de datos dinámicos debido a filtros dependientes del padre, así que si primero se
				//ejecutaba el Update de los FormFields dependientes, mostraba los datos ya cargados en lugar de ir al server por nuevos datos,
				//lo cual se logra precisamente limpiando el array primero (tiene que ver con la propiedad DynamicLoadFn)
				if (!isset($aField->OnChangeBeforeUpdate) || $aField->OnChangeBeforeUpdate == false)
				{
		    		if ($aField->OnChange != "")
	    			{
?>
				var result = <?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_Changed();
<?
	    			}
	    			else
	    			{
?>
				var result = true;
<?
	    			}
				}
				//@JAPR
    			if ($aField->SubmitOnChange)
    			{
?>
    				if (<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] != <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value)
    				{
    					<?= $myFormName ?>_Ok('<?= $aField->Name ?>');
    				}
    				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value;
<?
    			}
?>
				return result;
			}
<?
    		}
    		if ($aField->OnChange != "")
    		{
?>
	        function <?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_Changed()
	        {
	            <?= $aField->OnChange."\n" ?>
			}
			
<?
    		}	
			if (!is_null($aField->Parent))
			{
?>
	        function <?= $myFormName ?>_<?= $aField->Name ?>_Update()
	        {
<?
		    	switch ($aField->Type)
		    	{
		    		case "Object":
		        		switch ($aField->VisualComponent)
		        		{
		            		case "Radio":
?>
	        	var me = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>;
	        	var mySelection = null;
	        	if (me != null)
	        	{
					for (var i = 0; i < me.length; i++)
					{
						if (me[i].checked)
						{
	        				mySelection = me[i].value;
						}
					}
	        	}
<?
								switch ($aField->Parent->Type)
								{
									case "Object":
			        					switch ($aField->Parent->VisualComponent)
			        					{
			            					case "Radio":
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null)
	        	{
					for (var i = 0; i < myParent.length; i++)
					{
						if (myParent[i].checked)
						{
	        				myParentSelection = myParent[i].value;
						}
					}
	        	}
<?
												break;
			            					case "Combobox":
			            					case "List":
			            					default:
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null && myParent.selectedIndex >= 0)
	        	{
					var myParentSelection = myParent.options[myParent.selectedIndex].value;
	        	}
<?
												break;
			        					}
?>
	        	if (myParentSelection == null)
	        	{
   	        		var myNewValues = new Array();
	        	}
	        	else
	        	{
   	        		var myNewValues = <?= $myFormName ?>_<?= $aField->Name ?>[myParentSelection];
		        }
<?
			        					break;
			        				case "Array":
?>
				var myParent = GetElementsByName(<?= $myFormName ?>_SaveForm,'<?= $aField->Name ?>[]');
	        	var myParentSelection = null;
   	        	var myNewValues = new Array();
	        	if (myParent != null)
	        	{
	        		myParentSelection = new Array();
					for (var i = 0; i < myParent.length; i++)
					{
						if (myParent[i].checked)
						{
	        				myParentSelection[myParentSelection.length] = myParent[i].value;
	        				var temp = <?= $myFormName ?>_<?= $aField->Name ?>[myParent[i].value];
	        				for (var t = 0; t < temp.length; t++)
	        				{
	        					myNewValues[myNewValues.length] = temp[t];
	        				}
						}
					}
	        	}
<?
	        							break;
			        			}
?>
	        	var myContainer = <?= $myFormName ?>_<?= $aField->Name ?>_Container;
	        	var newContents = '';
	        	var newSelection = null;
	        	if (myNewValues != null)
	        	{
					for (var i = 0; i < myNewValues.length; i++)
					{
					    newContents += ('<input type="radio"'
										+ ' id="<?= $aField->IsDisplayOnly ?>"'
										+ '	<?= $aField->IsVisible ? "" : "style=\"display:none\"" ?>'
										+ ' name="<?= $aField->Name ?>"'
										+ ' value="'+myNewValues[i][0]+'"'
										+ ' <?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>'
										+ ' >&nbsp;'+myNewValues[i][1]+'<br>');
						if (mySelection == myNewValues[i][0])
						{
							newSelection = i;
						}
					}
	        	}
				myContainer.innerHTML = newContents;
				if (newSelection != null)
				{
					me[newSelection].checked = true;
				}
<?
								break;
		            		case "Combobox":
		            		case "List":
		            		default:
?>
	        	var me = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>;
	        	var mySelection = null;
	        	if (me.selectedIndex >= 0)
	        	{
	        		mySelection = me.options[me.selectedIndex].value;
	        	}
<?
								switch ($aField->Parent->Type)
								{
									case "Object":
			        					switch ($aField->Parent->VisualComponent)
			        					{
			            					case "Radio":
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null)
	        	{
					for (var i = 0; i < myParent.length; i++)
					{
						if (myParent[i].checked)
						{
	        				myParentSelection = myParent[i].value;
						}
					}
	        	}
<?
												break;
			            					case "Combobox":
			            					case "List":
			            					default:
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null && myParent.selectedIndex >= 0)
	        	{
					var myParentSelection = myParent.options[myParent.selectedIndex].value;
	        	}
<?
												break;
			        					}
?>
	        	if (myParentSelection == null)
	        	{
   	        		var myNewValues = new Array();
	        	}
	        	else
	        	{
   	        		var myNewValues = <?= $myFormName ?>_<?= $aField->Name ?>[myParentSelection];
		        }
<?
			        					break;
			        				case "Array":
?>
				var myParent = GetElementsByName(<?= $myFormName ?>_SaveForm, '<?= $aField->Name ?>[]');
	        	var myParentSelection = null;
   	        	var myNewValues = new Array();
	        	myParentSelection = new Array();
				for (var i = 0; i < myParent.length; i++)
				{
					if (myParent[i].checked)
					{
						myParentSelection[myParentSelection.length] = myParent[i].value;
						var temp = <?= $myFormName ?>_<?= $aField->Name ?>[myParent[i].value];
						for (var t = 0; t < temp.length; t++)
						{
							myNewValues[myNewValues.length] = temp[t];
						}
					}
				}
<?
										break;			        			
			        			}
?>
	        	me.length = 0;
	        	if (myNewValues != null)
	        	{
					for (var i = 0; i < myNewValues.length; i++)
					{
						var newOption = new Option(myNewValues[i][1], myNewValues[i][0]);
						me.options[me.options.length] = newOption;
						if (mySelection == myNewValues[i][0])
						{
							me.selectedIndex = i;
						}
					}
				}
<?
				if (isset($aField->DynamicLoadFn) && trim($aField->DynamicLoadFn) != '')
				{
?>
				else
				{
					<?=$aField->DynamicLoadFn?>;
				}
<?
				}

								break;
		        		}
		        		break;
		    		case "Array":
?>
				var me = GetElementsByName(<?= $myFormName ?>_SaveForm, '<?= $aField->Name ?>[]');
								
	        	var mySelection = new Array();
	        	
	        	if (me != null)
	        	{
	        		if(me.length != undefined && me.length != null)
	        		{
						for (var i = 0; i < me.length; i++)
						{
							if (me[i].checked)
							{
		        				mySelection[mySelection.length] = me[i].value;
							}
						}
	        		}
	        		else
	        		{
						if (me.checked)
						{
	        				mySelection[mySelection.length] = me.value;
						}
	        		}
	        	}
	        	
<?
						switch ($aField->Parent->Type)
						{
							case "Object":
			        			switch ($aField->Parent->VisualComponent)
			        			{
			            			case "Radio":
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null)
	        	{
					for (var i = 0; i < myParent.length; i++)
					{
						if (myParent[i].checked)
						{
	        				myParentSelection = myParent[i].value;
						}
					}
	        	}
<?
										break;
			            			case "Combobox":
			            			case "List":
			            			default:
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null && myParent.selectedIndex >= 0)
	        	{
					myParentSelection = myParent.options[myParent.selectedIndex].value;
	        	}
<?
										break;
			        			}
?>
	        	if (myParentSelection == null)
	        	{
	        	}
	        	else
	        	{
   	        		var myNewValues = <?= $myFormName ?>_<?= $aField->Name ?>[myParentSelection];
		        }
<?
			        					break;
			        				case "Array":
?>
				var myParent = GetElementsByName(<?= $myFormName ?>_SaveForm,'<?= $aField->Name ?>[]');
	        	var myParentSelection = null;
   	        	var myNewValues = new Array();
	        	if (myParent != null)
	        	{
	        		myParentSelection = new Array();
					for (var i = 0; i < myParent.length; i++)
					{
						if (myParent[i].checked)
						{
	        				myParentSelection[myParentSelection.length] = myParent[i].value;
	        				var temp = <?= $myFormName ?>_<?= $aField->Name ?>[myParent[i].value];
	        				for (var t = 0; t < temp.length; t++)
	        				{
	        					myNewValues[myNewValues.length] = temp[t];
	        				}
						}
					}
	        	}
<?
	        							break;
			        			}
?>
	        	var myContainer = <?= $myFormName ?>_<?= $aField->Name ?>_Container;
	        	var newContents = '';
	        	var newSelection = new Array();
	        	if (myNewValues != null)
	        	{
					for (var i = 0; i < myNewValues.length; i++)
					{
					    newContents += ('<input type="checkbox"'
										+ ' id="<?= $aField->IsDisplayOnly ?>"'
										+ ' <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?>'
										+ ' name="<?= $aField->Name ?>[]"'
										+ ' value="'+myNewValues[i][0]+'"'
										+ ' <?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>'
										+ ' >&nbsp;'+myNewValues[i][1]+'<br>');
						for (var t = 0; t < mySelection.length; t++)
						{ 
							if (mySelection[t] == myNewValues[i][0])
							{
								newSelection[newSelection.length] = i;
							}
						}
					}
	        	}
	        	newContents += '<input type="hidden"'
	        						+ ' name="<?= $aField->Name ?>_EmptySelection"'
	        						+ ' value="' + (newSelection.length == 0 ? '1' : '0') + '">';
				myContainer.innerHTML = newContents;
				
	        	if (me != null)
	        	{
	        		if(me.length != undefined && me.length != null)
	        		{
						for (var t = 0; t < newSelection.length; t++)
						{ 
							me[newSelection[t]].checked = true;
						}
	        		}
	        		else
	        		{
						if (newSelection.length>0)
						{
							me = <?= $myFormName ?>_SaveForm.elements.namedItem('<?= $aField->Name ?>[]');
	        				me.checked = true;
						}
	        		}
	        	}
<?
		        		break;
		        	default:
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
				var myParentSelection = myParent.value;
				var myNewValue = <?= $myFormName ?>_<?= $aField->Name ?>[myParentSelection];
				if (myNewValue != null)
				{
<?
						if ($aField->FormatMask != "")
						{
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = myNewValue;
<?
							switch ($aField->Type)
							{
								case "String":
								case "Password":
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatString(myNewValue, '<?= $aField->FormatMask ?>');
<?
									break;
								case "Integer":
								case "Float":
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatNumber(myNewValue, '<?= $aField->FormatMask ?>');
<?
									break;
								case "Date":
								case "DateTime":
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatDateTime(myNewValue, '<?= $aField->FormatMask ?>');
<?
									break;
							}
						}
						else
						{
?>
   	        	<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = myNewValue;
<?
						}
?>
				}
<?
						break;
	        	}
				foreach ($aField->Children as $aChild)
				{
?>
            	<?= $myFormName ?>_<?= $aChild->Name ?>_Update();
<?
				}
?>
			}
<?
			}
			$aFieldIdx++;
		}
?>
		function body_click()
		{	
		}
		
	    </script>
<?
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		if($this->HasFormTitle)
		{
?>
	    <div class="object_title">
			<?= $this->get_Image().$myFormTitle ?>

	    </div>
<?
		}
?>
<?
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		if($this->HasHr)
		{
?>

	    <hr class="object_title_hr">
<?
		}
?>
	    <div class="object_buttons">
<?
	    if(!$this->IsDialog)
	    {
	    	if(!$this->hideBackButton($aUser))
	    	{
?>
			<div class="collectionButtonsSelection">
			<button title="<?= translate('Home') ?>" class="object_homeButton" onclick="javascript: document.location.href = '<?= 'main.php?'.$this->get_Parent()->get_QueryString() ?>';">
				<img src="images/home.png" alt="<?= translate('Home') ?>" title="<?= translate('Home') ?>" displayMe="1" /> <?= translate("Home") ?>
            </button>
    	    </div>
<?
	    	}
	    }
?>

<?
		if (!$this->isNewObject())
		{
			$addSpaces = false;
			if ($this->canRemove($aUser) && !$this->hideRemoveButton($aUser))
			{
?>
			<button title="<?= translate('Remove') ?>" class="object_removeButton" onclick="javascript: <?= $myFormName ?>_Remove();" id="<?= $myFormName ?>_RemoveButton">
            	<img src="images/remove.png" alt="<?= translate('Remove') ?>" title="<?= translate('Remove') ?>" displayMe="1" /> <?= translate("Remove") ?>
            </button>
			
<?
				$addSpaces = true;
			}
			if ($this->canEdit($aUser))
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				if($this->HasEdit)
				{
?>
			<button title="<?= translate('Edit') ?>" id="<?= $myFormName ?>_EditButton" class="object_editButton" onclick="javascript: <?= $myFormName ?>_Edit();">
				<img src="images/edit.png" alt="<?= translate('Edit') ?>" title="<?= translate('Edit') ?>" displayMe="1" /> <?= translate("Edit") ?>
           </button>
<?
				}
				$addSpaces = true;
			}
			if ($addSpaces)
			{
?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
			}
		}
?>
			<button title="<?= translate('Save') ?>" id="<?= $myFormName ?>_OkSelfButton" style="display:none" class="object_okButton" onclick="javascript: <?= $myFormName ?>_Ok('self');">
				<img src="images/ok.png" alt="<?= translate('Save') ?>" title="<?= translate('Save') ?>" displayMe="1" /> <?= translate("Save") ?>
            </button>
			
<?
	    if(!$this->IsDialog)
	    {
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			if($this->HasSaveAndGoParent)
			{
?>
			<button title="<?= translate('Save & Go Parent') ?>" id="<?= $myFormName ?>_OkParentButton" style="display:none" class="object_okButton" onclick="javascript: <?= $myFormName ?>_Ok('parent');">
				<img src="images/okparent.png" alt="<?= translate('Save & Go Parent') ?>" title="<?= translate('Save & Go Parent') ?>" displayMe="1" /> <?= translate("Save & Go Parent") ?>
            </button>
			
<?	
			}
	    } else {
    	}
?>

<?
		if ($this->isNewObject())
		{
?>

<?
		    if(!$this->IsDialog)
	    	{
?>
			<button title="<?= translate('Save & Go New') ?>" id="<?= $myFormName ?>_OkNewButton" style="display:none" class="object_okButton" onclick="javascript: <?= $myFormName ?>_Ok('new');">
			<img src="images/oknew.png" alt="<?= translate('Save & Go New') ?>" title="<?= translate('Save & Go New') ?>" displayMe="1" /> <?= translate("Save & Go New") ?>
            </button>

<?
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				if($this->HasCancel)
				{
?>
			<button title="<?= translate('Cancel') ?>" id="<?= $myFormName ?>_CancelButton" style="display:none" class="object_cancelButton" onclick="javascript: document.location.href = '<?= 'main.php?'.$this->get_Parent()->get_QueryString() ?>';">
				<img src="images/cancel.png" alt="<?= translate('Cancel') ?>" title="<?= translate('Cancel') ?>" displayMe="1" /> <?= translate("Cancel") ?>
            </button>
<?
				}
	    	}
	    	else
	    	{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				if($this->HasCancel)
				{
?>
			<button title="<?= translate('Cancel') ?>" id="<?= $myFormName ?>_CancelButton" style="display:none" class="object_cancelButton" onclick="javascript: <?= $myFormName ?>_CancelDialog();">
				<img src="images/cancel.png" alt="<?= translate('Cancel') ?>" title="<?= translate('Cancel') ?>" displayMe="1" /> <?= translate("Cancel") ?>
            </button>
			
<?
				}
	    	}
		}
		else
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			if($this->HasCancel)
			{
?>
			<button title="<?= translate('Cancel') ?>" id="<?= $myFormName ?>_CancelButton" style="display:none" class="object_cancelButton" onclick="javascript: <?= $myFormName ?>_Cancel(true);">
				<img src="images/cancel.png" alt="<?= translate('Cancel') ?>" title="<?= translate('Cancel') ?>" displayMe="1" /> <?= translate("Cancel") ?>
            </button>
<?
			}
		}
        
		$this->addButtons($aUser);
?>
 		</div><div style="clear:both;"></div>
<?
		if (!$this->isNewObject())
		{
?>
 		<form id="<?= $myFormName ?>_RemoveForm" action="<?= 'main.php?'.$this->get_QueryString() ?>" method="post">
  			<input type="hidden" name="<?= $myFormIDFieldName ?>[]" value="<?= $this->get_FieldValueOf($myFormIDFieldName) ?>">
		</form>
<?
		}
?>
 		<form id="<?= $myFormName ?>_SaveForm" action="<?= 'main.php?'.$this->get_QueryString() ?>" method="post" enctype="<?=$this->get_FormEncType()?>">
  			<input type="hidden" name="go" value="self">
<?
		if(!array_key_exists($myFormIDFieldName,$myFormFields))
		{
?>
  			<input type="hidden" name="<?= $myFormIDFieldName ?>" value="<?= $this->get_FieldValueOf($myFormIDFieldName) ?>">
<?
		}
?>
    		<table class="object_table_new">
<?
		$this->insertPreviewRows($aUser);
?>

<?
		$rowCols = 0;
		$inTitle = false;
		$inCell = false;
		$aFieldIdx = 0;
    	foreach ($myFormFields as $aField)
    	{
    		$aFieldName = $aField->Name;
			if (!$inTitle && !$inCell && $rowCols > 1)
			{
?>
				</tr>
<?
				$rowCols = 0;
			}
			if (!$inTitle && !$inCell && $rowCols < 1)
			{
				if($aField->CodeBeforeField!=="")
				{
?>				
				<tr id="RowBeforeField_<?=$aField->Name?>"><td colspan="4"><?=$aField->CodeBeforeField?></td></tr>
<?
				}
				
				if(count($aField->InsertRowsBeforeField)>0)
				{
					foreach($aField->InsertRowsBeforeField as $rowKey=>$rowCode)
					{
?>				
				<tr id="RowInsertField_<?=$aField->Name?>_<?=$rowKey?>"><?=$rowCode?></tr>
<?
					}
				}
?>				
        		<tr id="Row_<?=$aField->Name?>">
<?
			}
			$cellCols = 0;
			switch ($aField->Type)
			{
				case "String":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
			    			if ($aField->Size <= 32)
			    			{
			    				$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							}
							else
							{
								$cellCols = 2;
							}
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td id="tdPrincipal" class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
					//@JAPR 2014-06-17: Agregada la propiedad para agregar estilos personalizados a los campos
					//@JAPR 2014-10-29: Corregido un bug, agregando los dos Styles no estaba funcionando, se tenía que agregar como uno solo
					$strClass = ($aField->CustomizedClass)?" customizedClass=\"{$aField->CustomizedClass}\" ":"";
?>
						<?= $aField->BeforeMessage ?>
						<input type="text"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?=$strClass?>
						        style="border:0px;padding:0px;width:100%;
								<?= $aField->IsVisible ? "" : "display:none;" ?> 
						        <?= (!is_null($aField->FixedWidth))? "width:".$aField->FixedWidth.";" : "" ?>
						        "
								name="<?= $aField->Name ?>"
								
								maxlength="<?= $aField->Size ?>"
								value="<?=htmlspecialchars($aFieldValue, ENT_QUOTES)?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
?>
								onfocus="javascript:
											var v = formatString(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatStringRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
	            							var v = unformatString(this.value, '<?= $aField->FormatMask ?>');
	            							this['_value'] = v;
											this.value = formatString(v, '<?= $aField->FormatMask ?>');"
<?
					}
?>
						>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Integer":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
					//@JAPR 2014-10-29: Corregido un bug, agregando los dos Styles no estaba funcionando, se tenía que agregar como uno solo
?>
						<?= $aField->BeforeMessage ?>
						<input type="text"
						        id="<?= $aField->IsDisplayOnly ?>"
						        style="border:0px;padding:0px;width:100%;
						        <?= $aField->IsVisible ? "" : "display:none;" ?> 
						        <?= (!is_null($aField->FixedWidth))? "width:".$aField->FixedWidth.";" : "" ?>
						        "
								name="<?= $aField->Name ?>"
								size="12"
								maxlength="11"
								value="<?= $aFieldValue ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
						if($aField->DefaultNumberZero==true)
						{
							$strOnBlur = "
											var v = unformatNumber(this.value, '".$aField->FormatMask."');
		            						this['_value'] = v;
											this.value = formatNumber(v, '".$aField->FormatMask."');\"";
						}
						else 
						{
							$strOnBlur = "	
											if(Trim(this.value)!='')
											{
												var v = unformatNumber(this.value, '".$aField->FormatMask."');
			            						this['_value'] = v;
												this.value = formatNumber(v, '".$aField->FormatMask."');
											}
											else
											{
												this['_value'] = '';
												this.value = '';
											}\"";
						}
?>
								onfocus="javascript:
											var v = formatNumber(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatNumberRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
  											<?=$strOnBlur?>
<?


					}
?>
						>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Float":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<input type="text"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								size="20"
								maxlength="18"
								value="<?= $aFieldValue ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
						if($aField->DefaultNumberZero==true)
						{
							$strOnBlur = "
											var v = unformatNumber(this.value, '".$aField->FormatMask."');
		            						this['_value'] = v;
											this.value = formatNumber(v, '".$aField->FormatMask."');\"";
						}
						else 
						{
							$strOnBlur = "	
											if(Trim(this.value)!='')
											{
												var v = unformatNumber(this.value, '".$aField->FormatMask."');
			            						this['_value'] = v;
												this.value = formatNumber(v, '".$aField->FormatMask."');
											}
											else
											{
												this['_value'] = '';
												this.value = '';
											}\"";
						}
?>
								onfocus="javascript:
											var v = formatNumber(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatNumberRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
											<?=$strOnBlur?>
<?
					}
?>
						>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Date":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<input type="text"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								size="12"
								maxlength="10"
								value="<?= $aFieldValue ?>"
								<?= ($aField->OnChange != NULL || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
?>
								onfocus="javascript:
											var v = formatDateTime(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
	            							var v = unformatDate(this.value, '<?= $aField->FormatMask ?>');
	            							this['_value'] = v;
<?
			    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    					{
?>
											var today = new Date();
											var year = new Number(this['_value'].substr(0, 4));
											if (year == 0)
											{
												year = today.getFullYear();
											}
											var month = new Number(this['_value'].substr(5, 2));
											if (month == 0)
											{
												month = today.getMonth() + 1;
											}
	            							calendar_<?= $myFormName ?>_<?= $aField->Name ?>.setCurrentYear(year);
	            							calendar_<?= $myFormName ?>_<?= $aField->Name ?>.setCurrentMonth(month - 1);
<?
						}
?>

											this.value = formatDateTime(v, '<?= $aField->FormatMask ?>');"
<?
					}
?>
						>
<?
		    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    				{
?>
						<script language="JavaScript">
							function calendarCallback_<?= $myFormName ?>_<?= $aField->Name ?>(day, month, year)
							{
								if (String(month).length == 1)
								{
									month = '0' + month;
								}
								if (String(day).length == 1)
								{
									day = '0' + day;
								}
	            				var v = year + '-' + month + '-' + day;
	            				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = v;
								<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatDateTime(v, '<?= $aField->FormatMask ?>');
    						}
    						calendar_<?= $myFormName ?>_<?= $aField->Name ?> = new calendar('calendar_<?= $myFormName ?>_<?= $aField->Name ?>', 'calendarCallback_<?= $myFormName ?>_<?= $aField->Name ?>');
						</script>
<?
					}
?>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "DateTime":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<input type="text"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								size="22"
								maxlength="19"
								value="<?= $aFieldValue ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
?>
								onfocus="javascript:
											var v = formatDateTime(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
	            							var v = unformatDateTime(this.value, '<?= $aField->FormatMask ?>');
	            							this['_value'] = v;
<?
			    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    					{
?>
											var today = new Date();
											var year = new Number(this['_value'].substr(0, 4));
											if (year == 0)
											{
												year = today.getFullYear();
											}
											var month = new Number(this['_value'].substr(5, 2));
											if (month == 0)
											{
												month = today.getMonth() + 1;
											}
	            							calendar_<?= $myFormName ?>_<?= $aField->Name ?>.setCurrentYear(year);
	            							calendar_<?= $myFormName ?>_<?= $aField->Name ?>.setCurrentMonth(month - 1);
<?
						}
?>
											this.value = formatDateTime(v, '<?= $aField->FormatMask ?>');"
<?
					}
?>
						>
<?
		    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    				{
?>
						<script language="JavaScript">
							function calendarCallback_<?= $myFormName ?>_<?= $aField->Name ?>(day, month, year)
							{
								if (String(month).length == 1)
								{
									month = '0' + month;
								}
								if (String(day).length == 1)
								{
									day = '0' + day;
        						}
        						var v = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'];
								v = v.substr(10);
        						v = year + '-' + month + '-' + day + v;
	            				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = v;
								<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatDateTime(v, '<?= $aField->FormatMask ?>');
    						}
    						calendar_<?= $myFormName ?>_<?= $aField->Name ?> = new calendar('calendar_<?= $myFormName ?>_<?= $aField->Name ?>', 'calendarCallback_<?= $myFormName ?>_<?= $aField->Name ?>');
						</script>
<?
					}
?>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "LargeString":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = 2;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<textarea
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								cols="88"
								rows="5"
								onkeydown="javascript: return TextArea_onkeydown(this);"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								><?= $aFieldValue ?></textarea>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "LargeStringHTML":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = 2;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
						<script type="text/javascript">
							//var tinyMCEmode = true; //Conchita 16-nov-2011 cambio para toggle tinymce
							if (!objTinyMCEmode) {
								var objTinyMCEmode = new Object();
							}
							objTinyMCEmode["<?=$aField->Name?>"] = true;
							
							visibleDivArray["<?=$aField->Name?>"] = '<?=$aField->Name?>DefaultDesHDiv';
							function changeTab(oEvt, sPropName) {
								
								if (!oEvt || !sPropName) {
									return;
								}
								
								objEditorType=<?=$myFormName?>_SaveForm.EditorType;
								if(objEditorType)
								{
									editorTypeValue = objEditorType.options[objEditorType.selectedIndex].value;
								}
								else
								{
									editorTypeValue = 0;
								}
								
								
								if(editorTypeValue==0)
								{
									var strTabId = sPropName+oEvt.getAttribute('name')+'Div';
								}
								else
								{
									var strTabId = sPropName+oEvt.getAttribute('name')+'DesDiv';
									if (document.getElementById(strTabId))
									{
										visibleDivArray[sPropName] = sPropName+oEvt.getAttribute('name')+'DesHDiv';
									}
									else 
									{
										var strTabId = sPropName+oEvt.getAttribute('name')+'Div';
									}									
								}
								
								document.getElementById(sPropName+'DefaultDiv').style.display = 'none';
								var objDiv = document.getElementById(sPropName+'iPodDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPadDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPadMiniDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPhoneDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'CelDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'TabletDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								
								var objDiv = document.getElementById(sPropName+'DefaultDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPodDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPadDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPadMiniDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPhoneDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'CelDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'TabletDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								
								var objDiv = document.getElementById(strTabId);
								if (objDiv) {
									objDiv.style.display = 'block';
								}
								try {
									var objDiv = document.getElementById(sPropName+'Menu');
									if (objDiv) {
										var arrLi = objDiv.getElementsByTagName('li');
										if (arrLi) {
											for (var intIdx in arrLi) {
												arrLi[intIdx].className = "menuOption";
											}
										}
									}
									
									if (oEvt) {
										oEvt.className = "menuOption selectedTab"						
									}
								} catch(e) {}
							}
						</script>
						
<?
					//@JAPR 2014-08-04: Agregado el Responsive Design
					//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
					//el componente default
					if (getMDVersion() >= esvResponsiveDesign && $aField->MultiDeviceNames && is_array($aField->MultiDeviceNames) && count($aField->MultiDeviceNames)) {
?>
						<div id="<?=$aField->Name?>Menu" class="tabbed" style="height:40px;">
							<ul class="menu">
								<li id="default<?=$aField->Name?>" name="Default" class="menuOption selectedTab" onclick="changeTab(this,'<?=$aField->Name?>')">
									<a><?=translate("Default")?></a>
						        </li>					
<?
						//Posteriormente se agregará un tab por cada opción específica de dispositivo asignada
						foreach ($aField->MultiDeviceNames as $intIndex => $strDevice) {
							$strDeviceLabel = (string) @$aField->MultiDeviceLabels[$intIndex];
							if ($strDeviceLabel == '') {
								$strDeviceLabel = $strDevice;
							}
?>
								<li name="<?=$strDevice?>" class="menuOption" onclick="changeTab(this,'<?=$aField->Name?>')">
									<a><?=$strDeviceLabel?></a>
						        </li>					
<?
						}
?>
							</ul>
						</div>
<?
					}
					//@JAPR
					
					//HTML Designer
					$styleDefaultDiv = "";
					$styleDefaultDesDiv = "";
					if($aField->UseHTMLDes==1)
					{
						if($this->EditorType==0)
						{
							$styleDefaultDiv = '';
							$styleDefaultDesDiv = 'style="display: none;width: '.$widthByDisp["Default"].'; height: 200px;overflow-x: auto;overflow-y: auto;"';
						}
						else
						{
							$styleDefaultDiv = 'style="display: none;"';
							$styleDefaultDesDiv = 'style="width: '.$widthByDisp["Default"].'; height: 200px;overflow-x: auto;overflow-y: auto;"';
						}
					}
?>
						<div id="<?=$aField->Name?>Tabs">
							<div id="<?=$aField->Name?>DefaultDiv"  <?=$styleDefaultDiv?>>
								<textarea
							        id="<?= $aField->Name ?>"
							        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>"
									cols="88"
									rows="5"
									onkeydown="javascript: return TextArea_onkeydown(this);"
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									><?= $aFieldValue ?></textarea><br>
									<!-- @AAL 25/02/2015: Agregado Div y un Button que abre la ventana del editor de fórmulas -->
									<div style="display: inline-flex">
										<a href="#" title="toogle TinyMCE" onclick="toogleEditorModeFields('<?= $aField->Name ?>');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
<?										/*@AAL 25/02/2015: En preguntas tipo QuestionImageSketch no se debe mostrar el botón que me permita abrir el editor de fórmulas */
										if($aField->Name != "QuestionImageSketch")
											echo '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpimirror1/artus/genvii/esurveyv5/btngenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\'; ShowWindowsDialogEditor(\'' . $aField->Name . '\');">';
?>
									</div>
									<!-- @AAL -->
							</div>
<?
					//@JAPR 2014-08-04: Agregado el Responsive Design
					//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
					//el componente default
					if (getMDVersion() >= esvResponsiveDesign) {
						foreach ($aField->MultiDeviceNames as $intIndex => $strDevice) {
							$strDeviceFieldName = $aField->Name.$strDevice;
							$aFieldValueTmp = $this->get_FieldValueOf($strDeviceFieldName);
?>
							<div id="<?=$strDeviceFieldName?>Div" style="display: none;">
								<textarea
							        id="<?=$strDeviceFieldName?>"
							        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?=$strDeviceFieldName?>"
									cols="88"
									rows="5"
									onkeydown="javascript: return TextArea_onkeydown(this);"
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$strDeviceFieldName."_".$aFieldIdx."_OnChange();\"" : "" ?>
									><?= $aFieldValueTmp ?></textarea><br>
									<!-- @AAL 25/02/2015: Agregado Div y un Button que abre la ventana del editor de fórmulas -->
									<div style="display: inline-flex">
										<a href="#" title="toogle TinyMCE" onclick="toogleEditorModeFields('<?=$strDeviceFieldName?>');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
<?										/*@AAL 25/02/2015: En preguntas tipo QuestionImageSketch no se debe mostrar el botón que me permita abrir el editor de fórmulas */
										if(strpos($strDeviceFieldName, "QuestionImageSketch") === false)
											echo '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpimirror1/artus/genvii/esurveyv5/btngenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\'; ShowWindowsDialogEditor(\'' . $strDeviceFieldName . '\');">';
?>
									</div>
									<!-- @AAL -->
							</div>
<?
						}
					}
					//@JAPR
?>
						</div>
						
<?
					//HTML Designer
					//Nuevos divs para el nuevo editor
					if (getMDVersion() >= esvEditorHTML && $aField->UseHTMLDes)
					{
?>
							
				<span id="<?=$aField->Name?>SpanDesigner">
					<table id="<?=$aField->Name?>_tbl" style="border-top:1px solid #CCC; border-bottom:1px solid #CCC; border-left:1px solid #CCC;border-right:1px solid #CCC; border-collapse:collapse;">
					<tbody>
						<tr height=25px>
						<td margin="0" style="background: #F0F0EE;border: 0; border-bottom:1px solid #CCC;padding:0px">
						<div id="<?=$aField->Name?>_toolbargroup" tabindex="-1">
						<span>
							<table id="<?=$aField->Name?>_toolbar1" cellpadding="0" cellspacing="0" tabindex="-1">
								<tbody>
								<tr>
									<td style="position: relative;padding:0px">
									<?
									if(strpos($aField->CompToAdd, 'LBL,ADD',0)!==false)
									{
									?>
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['<?=$aField->Name?>'],visibleDivArray['<?=$aField->Name?>'],'LBL,ADD',0,0);" title="Insert Text">
											<img src="./images/insertText.png" style="display:inline;cursor:pointer;">
										</a>
									<?
									}
									if(strpos($aField->CompToAdd, 'BTN,ADD',0)!==false)
									{
									?>
									
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['<?=$aField->Name?>'],visibleDivArray['<?=$aField->Name?>'],'BTN,ADD',0,0);" title="Insert Button">
											<img src="./images/insertButton.png" style="display:inline;cursor:pointer;">
										</a>
									<?
									}
									if(strpos($aField->CompToAdd, 'IMG,ADD',0)!==false)
									{
									
									?>
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['<?=$aField->Name?>'],visibleDivArray['<?=$aField->Name?>'],'IMG,ADD',0,0);" title="Insert Image">
											<img src="./images/insertImage.png" style="display:inline;cursor:pointer;">
										</a>
									<?
									}
									?>
									</td>
								</tr>
								</tbody>
							</table>
						</span>
						</div>
						</td>
						</tr>
						<tr>
						<td style="border-top:1px solid #CCC; border-bottom:1px solid #CCC; border-left:1px solid #CCC;border-right:1px solid #CCC;padding:0px">
					<div id="<?=$aField->Name?>DefaultDesDiv" <?=$styleDefaultDesDiv?>>
					<?$aFieldNameDes = $aField->Name.'Des';?>
						<textarea id="<?=$aField->Name?>Des" name="<?=$aField->Name?>Des" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->$aFieldNameDes?></textarea>
						<textarea id="<?=$aField->Name?>DesHTML" name="<?=$aField->Name?>DesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$aFieldValue?></textarea>
					</div>

					<?
						//@JAPR 2014-07-30: Agregado el Responsive Design
						//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
						//el componente default
						if (getMDVersion() >= esvResponsiveDesign) 
						{
							
						foreach ($aField->MultiDeviceNames as $intIndex => $strDevice) 
						{
							$strDeviceFieldName = $aField->Name.$strDevice.'Des';
							$aFieldValueTmp = $this->get_FieldValueOf($strDeviceFieldName);
							//$widthDes=getWidthByDeviceID($intIndex);
							//$heightDes=getHeightByDeviceID($intIndex);
?>
					<div id="<?=$strDeviceFieldName?>Div"  style="display: none; width: <?=$widthByDisp[$strDevice]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="<?=$strDeviceFieldName?>" name="<?=$strDeviceFieldName?>" cols="150" rows="15" onkeydown="" style="display: none;"><?=$aFieldValueTmp?></textarea>
						<textarea id="<?=$strDeviceFieldName?>HTML" name="<?=$strDeviceFieldName?>HTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$aFieldValueTmp?></textarea>
					</div>
<?
						}
						}
?>
						</td>
						</tr>
					</tbody>
					</table>
				</span>
		
				<script type="text/javascript">				
				 function onLoad<?=$aField->Name?>()
				 {
					aCompToAdd = new Array();
				 	aCompToAdd = [<?=$aField->CompToAdd?>];
				 	aCompToAddDes = new Array();
				 	aCompToAddDes = [<?=$aField->CompToAddDes?>];
					
				 	if(document.getElementById('<?=$aField->Name?>DefaultDesDiv'))
				 	{
				 		aHTMLDivObjects['<?=$aField->Name?>DefaultDesHDiv'] = new reportPart('<?=$aField->Name?>DefaultDesDiv', '', defaultWidth, defaultHeight, '<?=$aField->Name?>DefaultDesHDiv', '<?=$aField->Name?>Des');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>iPodDesDiv'))
				 	{				 	
						aHTMLDivObjects['<?=$aField->Name?>iPodDesHDiv'] = new reportPart('<?=$aField->Name?>iPodDesDiv', '', iPodWidth, iPodHeight, '<?=$aField->Name?>iPodDesHDiv', '<?=$aField->Name?>iPodDes');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>iPadDesDiv'))
				 	{				 	
						aHTMLDivObjects['<?=$aField->Name?>iPadDesHDiv'] = new reportPart('<?=$aField->Name?>iPadDesDiv', '', iPadWidth, iPadHeight, '<?=$aField->Name?>iPadDesHDiv', '<?=$aField->Name?>iPadDes');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>iPadMiniDesDiv'))
				 	{					
				 		aHTMLDivObjects['<?=$aField->Name?>iPadMiniDesHDiv'] = new reportPart('<?=$aField->Name?>iPadMiniDesDiv', '', iPadMiniWidth, iPadMiniHeight, '<?=$aField->Name?>iPadMiniDesHDiv', '<?=$aField->Name?>iPadMiniDes');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>iPhoneDesDiv'))
				 	{
						aHTMLDivObjects['<?=$aField->Name?>iPhoneDesHDiv'] = new reportPart('<?=$aField->Name?>iPhoneDesDiv', '', iPhoneWidth, iPhoneHeight, '<?=$aField->Name?>iPhoneDesHDiv', '<?=$aField->Name?>iPhoneDes');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>CelDesDiv'))
				 	{
						aHTMLDivObjects['<?=$aField->Name?>CelDesHDiv'] = new reportPart('<?=$aField->Name?>CelDesDiv', '', celAndroidWidth, celAndroidHeight, '<?=$aField->Name?>CelDesHDiv', '<?=$aField->Name?>CelDes');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>TabletDesDiv'))
				 	{
						aHTMLDivObjects['<?=$aField->Name?>TabletDesHDiv'] = new reportPart('<?=$aField->Name?>TabletDesDiv', '', tabletAndroidWidth, tabletAndroidHeight, '<?=$aField->Name?>TabletDesHDiv', '<?=$aField->Name?>TabletDes');
				 	}
				 }
								 
				if (document.attachEvent){
					document.attachEvent("onload", onLoad<?=$aField->Name?>());
				}
				else if (document.addEventListener) {
					document.addEventListener("load", onLoad<?=$aField->Name?>(), false);
				}				 
				</script>
				
<?
					}
?>						
						<script type="text/javascript">
							if (!arrResponsiveDesignObjectsFields) {
								var arrResponsiveDesignObjectsFields = new Array();
								var arrResponsiveDesignObjectsFieldsLoaded = new Array();
							}
							arrResponsiveDesignObjectsFields.push("<?=$aField->Name?>");
							arrResponsiveDesignObjectsFieldsLoaded.push(0);
<?
					//@JAPR 2014-08-04: Agregado el Responsive Design
					//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
					//el componente default
					if (getMDVersion() >= esvResponsiveDesign) {
						//El campo se puede configurar para tener valores específicos por dispositivo, si lo hace entonces se prepara el
						//array para generar tantos componentes como dispositivos se especifiquen, de lo contrario sólo se agrega el
						//componente propio del campo, el cual a diferencia de los demás, simplemente no tendrá un sufijo asociado
						if ($aField->MultiDeviceNames && is_array($aField->MultiDeviceNames) && count($aField->MultiDeviceNames)) {
							foreach ($aField->MultiDeviceNames as $strDevice) {
								$strDeviceFieldName = $aField->Name.$strDevice;
?>
							arrResponsiveDesignObjectsFields.push("<?=$strDeviceFieldName?>");
							arrResponsiveDesignObjectsFieldsLoaded.push(0);
							objTinyMCEmode["<?=$strDeviceFieldName?>"] = true;
<?
							}
						}
?>
<?
					}
					
					//@JAPR 2014-08-04: No logré identificar por qué no creaba las instancias dle tinyMCE al hacerlo directo siendo que
					//otros casos como en FormattedText.php si lo hace, así que se tuvo que usar un setTimeout para forzar a crear las
					//instancias hasta que se terminaba de cargar la página
?>
							setTimeout(function () {
							for (var intIdx in arrResponsiveDesignObjectsFields) {
								if (intIdx == 'contains') {
									continue;
								}
								
								var strTinyMCEObjectName = arrResponsiveDesignObjectsFields[intIdx];
								if (arrResponsiveDesignObjectsFieldsLoaded[intIdx]) {
									continue;
								}
								
								arrResponsiveDesignObjectsFieldsLoaded[intIdx] = 1;
								tinyMCE.init({
									// General options
									mode : "exact",
									readonly : 0,
							        elements : strTinyMCEObjectName + ",ajaxfilemanager",
							      	theme : "advanced",
									file_browser_callback : "ajaxfilemanager",
									entity_encoding : "raw",
									// Theme options  //Conchita 9-nov agregado forecolor
						<?
							//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
							$strTinyMCEToolBar = "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink,image";
							if (isset($aField->HTMLFormatButtons) && trim($aField->HTMLFormatButtons) != '') {
								$strTinyMCEToolBar = $aField->HTMLFormatButtons;
							}
						?>
									theme_advanced_buttons1 : "<?=$strTinyMCEToolBar?>",
									theme_advanced_buttons2: "",
									theme_advanced_buttons3: "",
									theme_advanced_buttons4: "",		
									theme_advanced_toolbar_location : "top",
									theme_advanced_toolbar_align : "left",
									theme_advanced_statusbar_location : "none",
									theme_advanced_resizing : true,
									//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
									theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
	                										"Arial=arial,helvetica,sans-serif;"+
	                										"Arial Black=arial black,avant garde;"+
	                										"Book Antiqua=book antiqua,palatino;"+
											                "Comic Sans MS=comic sans ms,sans-serif;"+
											                "Courier New=courier new,courier;"+
											                "Georgia=georgia,palatino;"+
											                "Helvetica=helvetica;"+
											                "Impact=impact,chicago;"+
											                "Symbol=symbol;"+
											                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
											                "Terminal=terminal,monaco;"+
											                "Times New Roman=times new roman,times;"+
											                "Trebuchet MS=trebuchet ms,geneva;"+
											                "Verdana=verdana,geneva;"+
											                "Webdings=webdings;"+
											                "Wingdings=wingdings,zapf dingbats",
									//Conchita 4 nov 2011 se agregaron estas opciones para el upload de imagenes
	           						relative_urls : false,
	        						content_css : "",
	        						convert_urls : false,   //Conchita 7nov2011 agregado para que tome las urls bien sin convertirlas                
									//fin conchita agregado
									// Drop lists for link/image/media/template dialogs
								//	external_image_list_url : "lists/image_list.js",  //Conchita 10-nov-2011 Eliminar image list
									external_link_list_url : "lists/link_list.js",
									save_callback : "myCustomSaveContent",
									oninit: "myInitTinyMCEFields",
									cleanup: false,
									verify_html : false,
									forced_root_block : false
								});
							}
							}, 100);
							
							function myInitTinyMCEFields() {
								for (var intIdx in arrResponsiveDesignObjectsFields) {
									if (intIdx == 'contains' || isNaN(intIdx)) {
										continue;
									}
									
									var strTinyMCEObjectName = arrResponsiveDesignObjectsFields[intIdx];
									if (tinyMCE && tinyMCE.editors && tinyMCE.editors[strTinyMCEObjectName]) {
										var htmlSrc = tinyMCE.editors[strTinyMCEObjectName].getContent();
										
										if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
											setTimeout(function () {toogleEditorModeFields(strTinyMCEObjectName);}, 300);
										}
									}
								}
							}
							
							function myCustomSaveContent(element_id, html, body) {
							        // Do some custom HTML cleanup
							        if (html) {
							        	html = html.replace(/\/\/ <!\[cdata\[/gi, '').replace(/\/\/ \]\]>/gi,'');
							        }
							
							        return html;
							}
							
							function ajaxfilemanager(field_name, url, type, win) 
							{
								var ajaxfilemanagerurl = "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
								var view = 'detail';
								switch (type) 
								{
									case "image":
										view = 'thumbnail';
										break;
									case "media":
										break;
									case "flash": 
										break;
									case "file":
										break;
									default:
										return false;
								}
            					
								tinyMCE.activeEditor.windowManager.open({
									url: "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
									width: 782,
                					height: 440,
									inline : "yes",
									close_previous : "no"
								},{
									window : win,
									input : field_name
								}); 
							}
							
							//Inicio conchita 16-nov-2011 toggle tinymce
							function toogleEditorModeFields(sEditorID) { 
								var x;
								var tmp;
								var form="<?= $myFormName ?>";
							    try {
							        if(objTinyMCEmode[sEditorID]) { 
							        	//x=removeHTML();
							       		tinyMCE.execCommand('mceRemoveControl', false, sEditorID); 
							           	//document.getElementById(sEditorID).value=x; 
							            objTinyMCEmode[sEditorID] = false; 
							        } else {   tinyMCE.execCommand('mceAddControl', false, sEditorID);
							            //tinyMCE.addMCEControl(document.getElementById(''), sEditorID); 
							            objTinyMCEmode[sEditorID] = true; 
							        } 
							    } catch(e) { 
							    	alert('An error occur, try again. Error: '+ e);
							        //error handling 
							    } 
							}
							
						  	function removeHTML() {
								var htmlSrc = tinyMCE.activeEditor.getContent();
								var rexp = /<[^<>]+>/g;
								
								var cleanSrc = htmlSrc.replace(rexp,"");
								
								return cleanSrc;
						  	}
							//Fin Conchita toggle tinymce 16-nov-2011
						</script>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Boolean":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
<?
					switch ($aField->VisualComponent)
					{
						case "Checkbox":
		    				if (is_null($aField->Value))
		    				{
								if (intval($aFieldValue) == 0)
				    			{
									$disabledFalse = "disabled";
									$checkedTrue = "";
								}
								else
								{
									$disabledFalse = "";
									$checkedTrue = "checked";
								}
?>
						<input type="hidden"
						        id="boolean_<?= $myFormName ?>_<?= $aField->Name ?>_hidden"
								name="<?= $aField->Name ?>"
								value="0" <?= $disabledFalse ?>
								>
						<input type="checkbox"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								value="1" <?= $checkedTrue ?>
								onclick="javascript: 
											if (this.checked) 
											{
												boolean_<?= $myFormName ?>_<?= $aField->Name ?>_hidden.disabled = true;			
											}
											else
											{
												boolean_<?= $myFormName ?>_<?= $aField->Name ?>_hidden.disabled = false;
											}											
											<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();" : "" ?>"
								>
<?
		    				}
		    				else
		    				{
								if ($aFieldValue == $aField->Value)
				    			{
									$checked = "checked";
								}
								else
								{
									$checked = "";
								}
?>
						<input type="radio"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								value="<?= $aField->Value ?>" <?= $checked ?>
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>
<?
		    				}
							break;
		    			case "Radio":
		    			default:
		    				if (is_null($aField->Value))
		    				{
								if (intval($aFieldValue) == 0)
				    			{
									$checkedFalse = "checked";
									$checkedTrue = "";
								}
								else
								{
									$checkedFalse = "";
									$checkedTrue = "checked";
								}
?>
						<input type="radio"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								value="0" <?= $checkedFalse ?>
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>&nbsp;<?= "False" ?>
                        <br>
						<input type="radio"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								value="1" <?= $checkedTrue ?>
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>&nbsp;<?= "True" ?>
<?
		    				}
		    				else
		    				{
								if ($aFieldValue == $aField->Value)
				    			{
									$checked = "checked";
								}
								else
								{
									$checked = "";
								}
?>
						<input type="radio"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								value="<?= $aField->Value ?>" <?= $checked ?>
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>
<?
		    				}
							break;
					}
?>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Password":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
			    			if ($aField->Size <= 32)
			    			{
			    				$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							}
							else
							{
								$cellCols = 2;
							}
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<input type="password"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								size="<?= $aField->Size < 86 ? ($aField->Size + 2) : 88 ?>"
								maxlength="<?= $aField->Size ?>"
								value="<?= $aFieldValue ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
?>
								onfocus="javascript:
											var v = formatString(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatStringRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
	            							var v = unformatString(this.value, '<?= $aField->FormatMask ?>');
	            							this['_value'] = v;
											this.value = formatString(v, '<?= $aField->FormatMask ?>');"
<?
					}
?>
						>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Object":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?> <?=(rtrim($this->StyleCellTitle) == '')?'':$this->StyleCellTitle?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
<?
					switch ($aField->VisualComponent)
					{
		    			case "Radio":
							if (is_null($aField->Parent))
							{
								if($aField->IsVisible)
								{
									$styleDiv = "";
									$strStyleDiv = "";
								}
								else 
								{
									$styleDiv = "display:none;";
									$strStyleDiv = "style=\"display:none;\"";
								}
?>
						<div id="<?= $myFormName ?>_<?= $aField->Name ?>_Container" <?= ($aField->OptionsSize > 0 && (count($aField->Options) > $aField->OptionsSize)) ? "style=\"overflow:auto;border:1px solid #666;height:".(20 + (20 * $aField->OptionsSize))."px;".$styleDiv."\"" : "style=\"overflow:auto;border:1px solid #666;".$styleDiv."\"" ?>>
<?
								foreach ($aField->Options as $key => $value)
								{
									if ($key == $aFieldValue)
									{
										$checked = "checked";
									}
									else
									{
										$checked = "";
									}
?>
							<input type="radio"
						        	id="<?= $aField->IsDisplayOnly ?>"
						        	<?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>"
									value="<?= $key ?>" <?= $checked ?>
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									>&nbsp;<?= $value ?><br>
<?
								}
							}
							else
							{
								if($aField->IsVisible)
								{
									$styleDiv = "";
								}
								else 
								{
									$styleDiv = "display:none;";
								}

?>
						<div id="<?= $myFormName ?>_<?= $aField->Name ?>_Container" <?= ($aField->OptionsSize > 0 && (count($aField->Options) > $aField->OptionsSize)) ? "style=\"overflow:auto;border:1px solid #666;height:".(20 + (20 * $aField->OptionsSize))."px;".$styleDiv."\"" : "style=\"overflow:auto;border:1px solid #666;".$styleDiv."\"" ?>>
<?
								$aFieldParentName = $aField->Parent->Name;
    							$aFieldParentValue = $this->get_FieldValueOf($aFieldParentName);
								if (array_key_exists($aFieldParentValue, $aField->Options))
								{
									$aSelectedFieldOptions = $aField->Options[$aFieldParentValue];
								}
								else
								{
									$aSelectedFieldOptions = array();
								}
								foreach ($aSelectedFieldOptions as $key => $value)
								{
									if ($key == $aFieldValue)
									{
										$checked = "checked";
									}
									else
									{
										$checked = "";
									}
?>
							<input type="radio"
						        	id="<?= $aField->IsDisplayOnly ?>"
							        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>"
									value="<?= $key ?>" <?= $checked ?>
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									>&nbsp;<?= $value ?><br>
<?
								}
							}
?>
					    </div>
<?
							break;
		    			case "Combobox":
		    			case "List":
				    	default:
				    		//@JAPR 2014-10-28: Agregado el soporte de FixedWidth para campos tipo Object->ComboBox/List
							//@JAPR 2014-10-29: Corregido un bug, agregando los dos Styles no estaba funcionando, se tenía que agregar como uno solo
							//@JAPR 2014-10-31: Con el valor FixedWidth=0 se forzará a respetar el tamaño natural del combo box sin ajustarlo automáticamente a 200px
							//como lo hace el framework cuando es menor que dicho tamaño, esto porque este campo está oculto por default así que la manera de revisar
							//su tamaño provocaba que si se considerara menor que 0px debido a eso
?>
						<select
						        id="<?= $aField->IsDisplayOnly ?>"
						        style="border:0px;padding:0px;
						        <?= $aField->IsVisible ? "" : "display:none;" ?> 
						        <?= (!is_null($aField->FixedWidth) && $aField->FixedWidth != '0')? "width:".$aField->FixedWidth.";" : "" ?>
						        "
						        <?= (!is_null($aField->FixedWidth))? " fixedWidth=\"".$aField->FixedWidth."\"" : "" ?>
								name="<?= $aField->Name ?>" <?= (!is_null($aField->FixedWidth))? " style=\"width:".$aField->FixedWidth."\"" : "" ?> 
<?
							$aBackgroundStyle = '';
							if (!is_null($aField->BackgroundColor))
							{
								$aBackgroundStyle = "style=\"background-color:".$aField->BackgroundColor."\"";
								echo ($aBackgroundStyle);
							}

							if ($aField->VisualComponent == "List")
							{
						    	if (is_null($aField->Parent))
						    	{
?>
							size="<?= count($aField->Options) > $aField->OptionsSize ? $aField->OptionsSize + 1 : (count($aField->Options) < 2 ?  2 : count($aField->Options) + 1) ?>"
<?
						    	}
						    	else
						    	{
?>
							size="<?= $aField->OptionsSize + 1 ?>"
<?
						    	}
							}
?>
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>
<?
							if (is_null($aField->Parent))
							{
								foreach ($aField->Options as $key => $value)
								{
									if ($key == $aFieldValue)
									{
										$selectedOption = "selected";
									}
									else
									{
										$selectedOption = "";
									}
?>
							<option <?= $selectedOption ?> value="<?= $key ?>" <?=$aBackgroundStyle?>><?= $value ?></option>
<?
								}
							}
							else
							{
								$aFieldParentName = $aField->Parent->Name;
    							$aFieldParentValue = $this->get_FieldValueOf($aFieldParentName);
								if (array_key_exists($aFieldParentValue, $aField->Options))
								{
									$aSelectedFieldOptions = $aField->Options[$aFieldParentValue];
								}
								else
								{
									$aSelectedFieldOptions = array();
								}
								foreach ($aSelectedFieldOptions as $key => $value)
								{
									if ($key == $aFieldValue)
									{
										$selectedOption = "selected";
									}
									else
									{
										$selectedOption = "";
									}
?>
							<option <?= $selectedOption ?> value="<?= $key ?>" <?=$aBackgroundStyle?>><?= $value ?></option>
<?
								}
							}
?>
						</select>
<?
							break;
					}
?>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Array":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
<?
					if (is_null($aField->Parent))
					{
						if($aField->IsVisible)
						{
							$styleDiv = "";
						}
						else 
						{
							$styleDiv = "display:none;";
						}

?>
						<div id="<?= $myFormName ?>_<?= $aField->Name ?>_Container" <?= ($aField->OptionsSize > 0 && (count($aField->Options) > $aField->OptionsSize)) ? "style=\"overflow:auto;border:1px solid #666;height:".(20 + (20 * $aField->OptionsSize))."px;".$styleDiv."\"" : "style=\"overflow:auto;border:1px solid #666;".$styleDiv."\"" ?>>
<?
						switch($aField->VisualComponent){
							case 'Radio':
								
								$hasChecked = FALSE;
								
								foreach ($aField->Options as $key => $value)
								{
									if (in_array($key, $aFieldValue))
									{
										$hasChecked = true; 
										$checked = "checked";
									}
									else
									{
										$hasChecked = $hasChecked ? $hasChecked : false; 
										$checked = "";
									}
	?>
								<input type="radio"
							        	id="<?= $aField->IsDisplayOnly ?>"
							        	<?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
										name="<?= $aField->Name ?>[]"
										value="<?= $key ?>" <?= $checked ?>
										<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
										>&nbsp;<?= $value ?><br>
	<?
								}								
								
								break;
							default:

						if($aField->IsSingleDimensionalOptionsArray==true)
						{
							$hasChecked = false;
							foreach ($aField->Options as $key => $value)
							{
								if (in_array($key, $aFieldValue))
								{
									$hasChecked = true; 
									$checked = "checked";
								}
								else
								{
									$hasChecked = $hasChecked ? $hasChecked : false; 
									$checked = "";
								}
?>
							<input type="checkbox"
						        	id="<?= $aField->IsDisplayOnly ?>"
						        	<?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>[]"
									value="<?= $key ?>" <?= $checked ?>
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									>&nbsp;<?= $value ?><br>
<?
							}
						}
						else 
						{
							$hasChecked = false;
							foreach ($aField->Options as $arrayKey => $arrayValues)
							{
								foreach ($arrayValues as $key => $value)
								{
									if (in_array($key, $aFieldValue))
									{
										$hasChecked = true; 
										$checked = "checked";
									}
									else
									{
										$hasChecked = $hasChecked ? $hasChecked : false; 
										$checked = "";
									}
?>
							<input type="checkbox"
						        	id="<?= $aField->IsDisplayOnly ?>"
						        	<?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>[]"
									otherid="<?= $arrayKey ?>"
									value="<?= $key ?>" <?= $checked ?>
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									>&nbsp;<?= $value ?><br>
<?
								}
							}
						}
						}
					}
					else
					{
						if($aField->IsVisible)
						{
							$styleDiv = "";
						}
						else 
						{
							$styleDiv = "display:none;";
						}

?>
					    <div id="<?= $myFormName ?>_<?= $aField->Name ?>_Container" style="overflow:auto;border:1px solid #666;height:<?= 20 + (20 * $aField->OptionsSize) ?>px;<?=$styleDiv?>">
<?
						$aFieldParentName = $aField->Parent->Name;
    					$aFieldParentValue = $this->get_FieldValueOf($aFieldParentName);
						if (array_key_exists($aFieldParentValue, $aField->Options))
						{
							$aSelectedFieldOptions = $aField->Options[$aFieldParentValue];
						}
						else
						{
							$aSelectedFieldOptions = array();
						}
						$hasChecked = false;
						foreach ($aSelectedFieldOptions as $key => $value)
						{
							if (in_array($key, $aFieldValue))
							{
								$hasChecked = true; 
								$checked = "checked";
							}
							else
							{
								$hasChecked = $hasChecked ? $hasChecked : false; 
								$checked = "";
							}
?>
							<input type="checkbox"
						        	id="<?= $aField->IsDisplayOnly ?>"
							        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>[]"
									value="<?= $key ?>" <?= $checked ?>
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									>&nbsp;<?= $value ?><br>
<?
						}
					}
?>
					      	<input type="hidden" name="<?= $aField->Name ?>_EmptySelection" value="<?= $hasChecked ? "1" : "0" ?>">
					    </div>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Time":
					$aFieldName_Hours = $aFieldName."_Hours";
					$aFieldValue_Hours = $this->get_FieldValueOf($aFieldName_Hours);
    				$aFieldName_Minutes = $aFieldName."_Minutes";
					$aFieldValue_Minutes = $this->get_FieldValueOf($aFieldName_Minutes);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<select
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name."_Hours" ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>
<?
							for ($i = 0; $i < 24; $i++)
							{
								if ($i == $aFieldValue_Hours)
								{
										$selectedOption = "selected";
								}
								else
								{
									$selectedOption = "";
								}
?>
							<option <?= $selectedOption ?> value="<?= sprintf("%02d", $i) ?>"><?= sprintf("%02d", $i) ?></option>
<?
							}
?>
						</select>
						:
						<select
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name."_Minutes" ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>
<?
							for ($i = 0; $i < 60; $i++)
							{
								if ($i == $aFieldValue_Minutes)
								{
										$selectedOption = "selected";
								}
								else
								{
									$selectedOption = "";
								}
?>
							<option <?= $selectedOption ?> value="<?= sprintf("%02d", $i) ?>"><?= sprintf("%02d", $i) ?></option>
<?
							}
?>
						</select>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
   				default:
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title_new"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
					    &nbsp;
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
			}
			$rowCols += $cellCols;
			$aFieldIdx++;
		}
		switch ($rowCols)
		{
			case 0:
				if ($inTitle)
				{
					$inTitle = false;
?>
					</td>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
<?
				}
				break;
			case 1:
				if ($inTitle)
				{
					$inTitle = false;
?>
					</td>
<?
				}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
<?
				break;
			case 2:
?>
				</tr>
<?
				break;
		}
?>
			</table>
<?
		$this->generateInsideFormCode($aUser);
?>
		</form>
 	    <script language="javascript">
		 	<?= $myFormName ?>_Cancel();

<?
		if ($this->isNewObject() || $this->Mode == self::MODE_EDIT)
		{
?>
   	        <?= $myFormName ?>_Edit();
<?
		}
?>
<?
		if ($this->Message != "")
		{
?>
   	        alert('<?= str_replace("'", "\'", $this->Message) ?>');
<?
		}
?>
		</script>
<?
		$this->generateAfterFormCode($aUser);
		//@JAPR 2014-10-28: Agregado el soporte de FixedWidth para campos tipo Object->ComboBox/List
?>
		<script language="javascript">
		var arraySelects = document.getElementsByTagName('select');
		var numSelect = arraySelects.length;
		
		for(var iSelect=0; iSelect<numSelect; iSelect++)
		{
			newNode = arraySelects[iSelect].cloneNode(true);
			document.body.appendChild(newNode);
			//if(newNode.offsetWidth < 200 && !arraySelects[iSelect].getAttribute('fixedWidth'))
			if(!arraySelects[iSelect].getAttribute('fixedWidth'))
			{
				arraySelects[iSelect].style.width = "100%";
			}
			document.body.removeChild(newNode);
		}
		
		</script>
	</div>
<?
	}
}

trait BITAMCollectionExt {
	/* Regresa un array con la lista de los botones adicionales que se deben integrar a la toolbar (adicional a los defaults manejados por el propio framework),
	es el equivalente al addButtons del framework original con object.inc.php, sólo que en este caso no es HTML sino una definición en forma de array de objetos
	*/
	public $ContainerID="";
	
	//@JAPR 2016-07-12: Corregido un bug, aún se seguía invocando a este método, y en algunas clases hacía referencia a Repository por lo cual se alcanzaba a ver un path que decía
	//"Home"/"Inicio", así que se sobreescribirá la función para ya no regresar elementos en el path
	function get_PathArray() {
		return array();
	}
	//@JAPR
	
	function get_CustomButtons() {
		return array();
	}
	
	//Genera la colección en vista tipo Metro
	function generateDetailForm($aUser, $isAssociateDissociateMode = false) {
    	require_once("formfield.inc.php");
		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		$strScriptPath = $path_parts["dirname"];
    	$myFormName = get_class($this);
    	$myFormTitle = $this->get_Title();
    	$myURLform = $this->get_QueryString();
		$myFormIDFieldName = $this->get_FormIDFieldName();
		$mySupportFormIDFieldName = $this->get_SupportFormIDFieldName();
    	$myFormFields = $this->get_FormFields($aUser);
    	
    	if($this->displayCheckBox($aUser)===false)
    	{
    		$styleDisplayCheckBox='style="display:none"';
    	}
    	else
    	{
    		$styleDisplayCheckBox="";
    	}
?>
		<link type="text/css" href="loadexcel/css/bootstrap.min.css" rel="stylesheet"/>
		<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
		<link rel="stylesheet" type="text/css" href="css/default.css">
		<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>
		<link rel="stylesheet" type="text/css" href="js/libs/metisMenu/src/metisFolder.css"/>
		<link rel="stylesheet" type="text/css" href="js/libs/selectize.js/dist/css/selectize.bootstrap3.css"/>
		<link rel="stylesheet" type="text/css" href="css/materialModal.css"/>
		<link href="css/font/roboto/robotofont.css" rel="stylesheet" type="text/css">
		<script src="js/codebase/dhtmlxFmtd.js"></script>
      	<script type="text/javascript" src="js/json2.min.js"></script>
		<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
		<!--2015-06-26 JRPP Agrego librerias y css para el pintado de la importacion de catalagos-->
		<link type="text/css" href="loadexcel/css/font-awesome.min.css" rel="stylesheet"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="loadexcel/js/libs/bootstrap/bootstrap.min.js"></script>
		<script type="text/javascript" data-main="loadexcel/js/loadexcel.js" src="loadexcel/js/libs/require/require.js"></script>
		<style>
			html, body {
				width: 100%;
				height: 100%;
				margin: 0px;
				overflow: hidden;
			}
			.dhx_dataview .dhx_dataview_default_item, .dhx_dataview .dhx_dataview_default_item_selected {
				  border-right: 0px solid #cccccc;
			}
			.dhx_dataview {
				overflow-y:auto !important;
			}
			.dhx_dataview_custom_item_selected {
				/*
				padding-left: 33px !important;
				padding-top: 15px !important;
				padding-right: 33px !important;
				padding-bottom: 15px !important;
				*/
				text-align: center;
			}
			.dhx_dataview_custom_item {
				/*
				padding-left: 33px !important;
				padding-top: 15px !important;
				padding-right: 33px !important;
				padding-bottom: 15px !important;
				*/
				text-align: center;
			}
		</style>
	    <script language="javascript">
			var objCollectionLayOut = undefined;
			var objListDataView = undefined;
			var dhxList = "a";
			var objMousePopUp;

			function <?= $myFormName ?>_RemoveCancel() {

				if (!objToolbar) {
					return;
				}

				objToolbar.bitRemoveState = 0;

				objListDataView.$view.classList.remove('dhx_dataview_selecting');

				objToolbar.setItemText('remove', '<?= translate("Edit") ?>');

				objToolbar.setItemImage('remove', 'edit.png');

				objToolbar.hideItem('removeCancel');

				objListDataView && objListDataView.unselectAll();
			}
			
			//Verifica si el elemento seleccionado puede o no ser removido y ejecuta el comando correspondiente si hay elementos válidos
			//A la fecha de implementación, sólo se permitía selección sencilla
  	        function <?= $myFormName ?>_Remove()
	        {
				if (!objToolbar) {
					return;
				}
				
				//Si aún no se había presionado el botón de eliminar, entonces cambia la imagen y deshabilita el click en la dataView para permitir selección múltiple
				if (!objToolbar.bitRemoveState) {
					objToolbar.bitRemoveState = 1;

					objToolbar.setItemText('remove', '<?= translate("Remove") ?>');
					objToolbar.setItemImage('remove', 'remove.png');

					objToolbar.showItem('removeCancel');

					objListDataView.$view.classList.add('dhx_dataview_selecting');

					return;
				}
				
				//JAPR 2015-10-08: Corregido un bug, el estado de selección debe restaurarse hasta que concluya el proceso
				//objToolbar.bitRemoveState = (objToolbar.bitRemoveState)?0:1;
				
				if (!objListDataView) {
					return;
				}
				
	        	var selected = 0;
				var toignore = 0;
				//var elements = <?= $myFormName ?>_SelectForm.elements;
				var arrSelected = objListDataView.getSelected();
				var arrValid = new Array();
				var arrInvalid = new Array();
				var blnMulti = $.isArray(arrSelected);
				if (!blnMulti) {
					var strId = arrSelected;
					arrSelected = new Array();
					if (strId) {
						arrSelected.push(strId);
					}
				}
				
				for (var i = 0; i < arrSelected.length; i++)
				{
					selected++;
					var strId = arrSelected[i];
					var objItem = objListDataView.get(strId);
					if (!objItem || !objItem.canRemove) {
						toignore++;
						arrInvalid.push(strId);
					}
					else {
						arrValid.push(strId);
					}
				}
				if (selected == 0)
				{
				<?
					if(trim($this->MsgNoSelectedRemove)!="")
					{
?>
					alert('<?= str_replace("'", "\'", translate($this->MsgNoSelectedRemove)) ?>');
<?
					}
					else 
					{
?>
					alert('<?= str_replace("'", "\'", sprintf(translate("Please select which %s to remove"), $myFormTitle)) ?>');
<?
					}
				?>				

					return;
				}
				<?
					if(trim($this->MsgCancelRemove)!="")
					{
						$strAllDelete = $this->MsgCancelRemove;
					}
					else 
					{
						$strAllDelete = translate("No selected %s can be removed");
					}			
				?>
				if (selected == toignore)
				{
					alert('<?= str_replace("'", "\'", sprintf($strAllDelete, $myFormTitle)) ?>');
					return;
				}
				<?
					if(trim($this->MsgCancelRemove)!="")
					{
						$strSomeDelete = translate("Some %s can not be removed")." ".translate("because")." ".$this->MsgCancelRemove.", ".translate("they will be deselected");
					}
					else 
					{
						$strSomeDelete = translate("Some %s can not be removed, they will be deselected");
					}			
				?>				
				if (toignore != 0)
				{
					alert('<?=str_replace("'", "\'", sprintf($strSomeDelete, $myFormTitle)) ?>');
					for (var i = 0; i < arrInvalid.length; i++) {
						var strId = arrInvalid[i];
						if (strId) {
							objListDataView.unselect(strId);
						}
					}
				}
				
				<?
					if(trim($this->MsgConfirmRemove)!="")
					{
?>
						var msgElements = "";
						var MsgByElement= new Array();
<?
						if(count($this->MsgByElement)>0)
						{
							foreach($this->MsgByElement as $msgElemID => $msg)
							{
?>
								MsgByElement[<?=$msgElemID?>] = "<?=$msg?>";
<?
							}
?>
							for (var i = 0; i < arrValid.length; i++) {
								var strId = arrValid[i];
								if(MsgByElement[strId]!=undefined)
								{
									msgElements+=MsgByElement[strId];
								}
							}
<?
						}
?>
						if(MsgByElement.length>0)
						{
							if(msgElements!="")
							{
								//14Feb2013: Si hay mensajes por elementos entonces 
								//MsgConfirmRemove se utiliza sólo cuando alguno de los elementos a remover
								//tiene mensaje personalizado los cuales están concatenados en msgElements
								var answer = confirm('<?= str_replace("'", "\'", translate($this->MsgConfirmRemove)) ?>'+msgElements);
							}
							else
							{
								var answer = confirm('<?= str_replace("'", "\'", sprintf(translate("Do you want to remove all selected %s?"), $myFormTitle)) ?>');
							}
							
						}
						else
						{
							var answer = confirm('<?= str_replace("'", "\'", translate($this->MsgConfirmRemove)) ?>');
						}
<?
					}
					else 
					{
?>
				var answer = confirm('<?= str_replace("'", "\'", sprintf(translate("Do you want to remove all selected %s?"), $myFormTitle)) ?>');
<?
					}
				?>
				
				if (answer)
				{
					//Primero desmarca todos los checkboxes de elementos para no enviarlos al borrado
					var elements = <?= $myFormName ?>_SelectForm.elements;
					for (var i = 0; i < elements.length; i++)
					{
		 				if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 				{
							elements[i].checked = false;
   						}
					}
					
					$.post('processRequest.php', { 'Process': 'Delete', 'RequestType': 1, 'responseformat': 'json', 'ObjectType': <?= isset( $this->ObjectType ) ? $this->ObjectType : 0 ?>, <?= $myFormIDFieldName ?> : arrValid }).then( function( r ) {
						if( !r.error ) {
							//Si se regresó un status de confirmación, debe preguntar si desea continuar con el proceso e intentar de nuevo el mismo request
							if (r.confirm && r.confirm.desc) {
								var answer = confirm(r.confirm.desc);
								if (answer) {
									$.post('processRequest.php', { 'Process': 'Delete', 'SkipValidation':1, 'RequestType': 1, 'responseformat': 'json', 'ObjectType': <?= isset( $this->ObjectType ) ? $this->ObjectType : 0 ?>, <?= $myFormIDFieldName ?> : arrValid }).then( function( r ) {
										if( !r.error ) {
											objListDataView.remove( arrValid );
											objListDataView.refresh();
											<?$this->afterRemove($aUser);?>
										} else {
											alert( r.error.desc );
										}
										<?= $myFormName ?>_RemoveCancel();
									}, function(){debugger;} );
								}
							}
							else {
								objListDataView.remove( arrValid );
								objListDataView.refresh();
								<?$this->afterRemove($aUser);?>
							}
						} else {
							alert( r.error.desc );
						}
						<?= $myFormName ?>_RemoveCancel();
					}, function(){debugger;} );

				} else {
					<?= $myFormName ?>_RemoveCancel();
				}
			}
			
<?
		if (trim($this->OrderPosField) != "")
		{
?>
			var <?= $myFormName ?>_oDraggedObj = null;
			var <?= $myFormName ?>_nLastScreenXClicked = 0;
			var <?= $myFormName ?>_nLastScreenYClicked = 0;

  	        function <?= $myFormName ?>_Reorder(objectID, orderPos)
	        { 
	        	var canReorder = true;
<?
				//En esta función se puede realizar una validación para asignar el valor a la variable canDrop
				//y definir si es valido realizar el reordenamiento
				$this->validateReorder();
?>
	        	<?= $myFormName ?>_ReorderForm.<?= $myFormIDFieldName ?>.value = objectID;
				<?= $myFormName ?>_ReorderForm.<?= $this->OrderPosField ?>.value = orderPos;
				<?= $myFormName ?>_releaseDrag();
				
				if(canReorder)
				{
					<?= $myFormName ?>_ReorderForm.submit();
				}
				else
				{
					alert('<?=$this->MsgValidateReorder?>');
				}
			}
			
			function <?= $myFormName ?>_InitEventsToReorder()
	        {
	        	var i, objChild, objParent;
    			var arrayImgReorder = document.getElementsByName('<?= $myFormName ?>_imgReorder');

    			if(arrayImgReorder!=undefined && arrayImgReorder!=null)
    			{
    				for(i=0; i<arrayImgReorder.length; i++)
    				{
    					objChild = arrayImgReorder[i];
    					
    					if(bIsIE)
						{
							objChild.attachEvent("ondragover", function(evt) { evt.returnValue = false; });
							objChild.attachEvent("ondragenter", function(evt) { evt.returnValue = false; });
							objChild.attachEvent("ondragstart", function(evt) { evt.returnValue = false; });
							objChild.attachEvent("ondrop", function(evt) { evt.returnValue = false; });
							objChild.attachEvent("onmousemove", function(evt) { <?= $myFormName ?>_mousemove(evt); });
							objChild.attachEvent("onmouseout", function() { <?= $myFormName ?>_mouseout(); });
							objChild.attachEvent("onmousedown", function(evt) { <?= $myFormName ?>_mousedown(evt); });
							objChild.attachEvent("onmouseup", function(evt) { <?= $myFormName ?>_mouseup(evt); });
							
							objParent = objChild.parentElement;
							objParent.attachEvent("onmouseup", function(evt) { <?= $myFormName ?>_tdmouseup(evt); } );
							objParent.attachEvent("onmousemove", function(evt) { <?= $myFormName ?>_tdmousemove(evt); } );
						}
						else
						{
							objChild.addEventListener("mousemove", function(evt) { <?= $myFormName ?>_mousemove(evt); }, true);
							objChild.addEventListener("mouseout", function() { <?= $myFormName ?>_mouseout(); }, true);
							objChild.addEventListener("mousedown", function(evt) { <?= $myFormName ?>_mousedown(evt); }, true);
							objChild.addEventListener("mouseup", function(evt) { <?= $myFormName ?>_mouseup(evt); }, true);
							
							objParent = objChild.parentNode;
							objParent.addEventListener("mouseup", function(evt) { <?= $myFormName ?>_tdmouseup(evt); }, true);
							objParent.addEventListener("mousemove", function(evt) { <?= $myFormName ?>_tdmousemove(evt); }, true);
						}
    				}
    			}

    			if (bIsIE)
				{
					document.body.attachEvent("onmouseup", function(evt) { <?= $myFormName ?>_releaseDrag(); } );
					document.body.attachEvent("onkeypress", function(evt) { <?= $myFormName ?>_body_keypress(evt); } );
				}
				else
				{
					document.documentElement.addEventListener("mouseup", function(evt) { <?= $myFormName ?>_releaseDrag(); }, false );
					document.documentElement.addEventListener("keypress", function(evt) { <?= $myFormName ?>_body_keypress(evt); }, false );
				}
			}
			
			function <?= $myFormName ?>_mousemove(evt)
			{
				if (<?= $myFormName ?>_oDraggedObj == null && <?= $myFormName ?>_nLastScreenXClicked != 0 && (<?= $myFormName ?>_nLastScreenXClicked != evt.screenX || <?= $myFormName ?>_nLastScreenYClicked != evt.screenY))
				{
					<?= $myFormName ?>_oDraggedObj = bIsIE ? evt.srcElement.parentElement : evt.target.parentNode;
				}
			}
			
			function <?= $myFormName ?>_mouseout()
			{

			}

			function <?= $myFormName ?>_mousedown(evt)
			{
				if (bIsIE)
				{
					if (evt.button != 1) 
					{
						return;
					}
					
					evt.returnValue = false;
				}
				else
				{
					if (evt.button != 0) 
					{
						return;
					}
					evt.preventDefault();
				}
				
				<?= $myFormName ?>_nLastScreenXClicked = evt.screenX;
				<?= $myFormName ?>_nLastScreenYClicked = evt.screenY;
			}

			function <?= $myFormName ?>_mouseup(evt)
			{
				if (<?= $myFormName ?>_oDraggedObj != null)
				{
					<?= $myFormName ?>_ObjDropped(evt);
				}
			}

			function <?= $myFormName ?>_releaseDrag()
			{
				<?= $myFormName ?>_oDraggedObj = null;
				<?= $myFormName ?>_releaseHint();
				<?= $myFormName ?>_nLastScreenXClicked = 0;
				<?= $myFormName ?>_nLastScreenYClicked = 0;
			}
			
			function <?= $myFormName ?>_releaseHint()
			{
				var oHint = document.getElementById("<?= $myFormName ?>_Hint");
				var oOutline = document.getElementById("<?= $myFormName ?>_Outline");
				oHint.innerHTML = '';
				oHint.style.display = "none";
				oOutline.style.display = "none";
			}
			
			function <?= $myFormName ?>_ObjDropped(evt)
			{
				if (bIsIE)
				{
					evt.cancelBubble = true;
				}
				else
				{
					evt.stopPropagation();
				}
				
				<?= $myFormName ?>_releaseHint()
				
				if (<?= $myFormName ?>_oDraggedObj != null)
				{
					var osrc = bIsIE ? evt.srcElement : evt.target;
					
					var objectId = parseInt(<?= $myFormName ?>_oDraggedObj.getAttribute("id"));
					var objectPos = parseInt(<?= $myFormName ?>_oDraggedObj.getAttribute("orderpos"));
				    var position = parseInt(osrc.getAttribute("orderpos"));

				    if(objectPos!=position)
				    {
						// a drop action has just happened!
						<?= $myFormName ?>_Reorder(objectId, position);
				    }
				    else
				    {
				    	<?= $myFormName ?>_releaseDrag();
				    }
				}
			}
			
			function <?= $myFormName ?>_tdmousemove(evt)
			{
				var osrc, position;
				if (<?= $myFormName ?>_oDraggedObj == null) 
				{ 
					return;
				}
				
				if (<?= $myFormName ?>_nLastScreenXClicked == evt.screenX && <?= $myFormName ?>_nLastScreenYClicked == evt.screenY)
				{
					return;
				}
				
				var oOutline = document.getElementById("<?= $myFormName ?>_Outline");
				var oHint = document.getElementById("<?= $myFormName ?>_Hint");
				
				oOutline.style.left = (document.body.scrollLeft + evt.clientX - 7) + "px";
				oOutline.style.top = (document.body.scrollTop + evt.clientY - 15) + "px";
				oOutline.style.display = "inline";
				
				oHint.style.left = (document.body.scrollLeft + evt.clientX - 40) + "px";
				oHint.style.top = (document.body.scrollTop + evt.clientY - 30) + "px";
				oHint.style.display = "inline";
				
				osrc = bIsIE ? evt.srcElement : evt.target;
				
				position = osrc.getAttribute("orderpos")
				oHint.innerHTML = "<nobr><?=translate("Position")?> " + position + "</nobr>"
			}

			function <?= $myFormName ?>_tdmouseup(evt)
			{
				if (<?= $myFormName ?>_oDraggedObj != null)
				{
					<?= $myFormName ?>_ObjDropped(evt);
				}
			}
			
			function <?= $myFormName ?>_body_keypress(evt)
			{
				if (evt.keyCode == 27)
				{
					if (<?= $myFormName ?>_oDraggedObj != null)
					{
						<?= $myFormName ?>_releaseDrag();
					}
				}
			}
<?
		}
?>

  	        function <?= $myFormName ?>_Dissociate()
	        {
				var selected = 0;
				var elements = <?= $myFormName ?>_SelectForm.elements;
				for (var i = 0; i < elements.length; i++)
				{
		 			if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 			{
		 				if (elements[i].checked)
		 				{
		 					selected++;
		 				}
   					}
				}
				if (selected == 0)
				{
					alert('<?= str_replace("'", "\'", sprintf(translate("Please select which %s to dissociate"), $myFormTitle)) ?>');
					return;
				}
				var answer = confirm('<?= str_replace("'", "\'", sprintf(translate("Do you want to dissociate all selected %s?"), $myFormTitle)) ?>');
				if (answer)
				{
					var elements = <?= $myFormName ?>_SelectForm.elements;
					for (var i = 0; i < elements.length; i++)
					{
		 				if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 				{
		 					elements[i].disabled = false;
   						}
					}
					<?= $myFormName ?>_SelectForm.action = '<?= 'main.php?action=dissociate&'.$this->get_AssociateDissociateQueryString() ?>';
                	<?= $myFormName ?>_SelectForm.submit();
				}
			}
  	        function <?= $myFormName ?>_Associate()
	        {
				var answer = confirm('<?= str_replace("'", "\'", sprintf(translate("Do you want to associate all selected %s?"), $myFormTitle)) ?>');
				if (answer)
				{
					var elements = <?= $myFormName ?>_SelectForm.elements;
					for (var i = 0; i < elements.length; i++)
					{
		 				if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 				{
		 					elements[i].disabled = false;
   						}
					}
					<?= $myFormName ?>_SelectForm.action = '<?= 'main.php?action=associate&'.$this->get_AssociateDissociateQueryString() ?>';
                	<?= $myFormName ?>_SelectForm.submit();
				}
			}

<?
		if ($isAssociateDissociateMode)
		{
?>
	        window.returnValue = false;
	        
  	        function <?= $myFormName ?>_AssociateDone(answer)
	        {
	        	if (answer != undefined && answer != '')
	        	{
	        		if (parent && parent.window && parent.window.opener) {
	        			parent.window.opener.dialogValue = true;
	        		}
	        		window.returnValue = true;
	        		top.close();
	        	}
	        }

  	        function <?= $myFormName ?>_Cancel()
	        {
				top.close();
	        }
<?
		}
?>
		</script>
		
		<form id="<?= $myFormName ?>_SelectForm" method="post"<?= $isAssociateDissociateMode ? "target=\"".$myFormName."_AssociateIFrame\"" : "" ?>>
 	    	<table id="<?= $myFormName ?>_Table" class="collection_table" style="display:none;">
<?
		$iRow = 0;
    	foreach ($this->Collection as $anObject) {
			$anID = $anObject->get_FieldValueOf($myFormIDFieldName);
    			//No separar tr y td de la siguiente linea porque causa error en ciertas funciones de JS que accesan elementos por childNodes y firstChild
    			//ya que me regresaria como primer elemento el enter (\n)
				//JAPR: Para el rediseño no se accesarán, así que se pueden separar, o bien utilizar JQuery
?>
				<tr id="tr_collection_checkbox" name="tr_collection_checkbox">
					<td id="<?= $iRow ?>" class="collection_value_checkbox" name="td_collection_checkbox">
						<input id="1" type="checkbox" name="<?= $myFormIDFieldName ?>[]" value="<?= $anID ?>">
					</td>
				</tr>
<?
			$iRow++;
		}
?>
			</table>
		</form>
		
		<script language="JavaScript">
		objCollectionLayOut = new dhtmlXLayoutObject({
			parent: document.body,
			pattern: "1C"
		});
		objCollectionLayOut.cells(dhxList).hideHeader();
		objCollectionLayOut.setOffsets({top:0, left:0, right:0, bottom:0});
		
<? 		//@JAPR 2016-07-25: Corregido un bug, se estaba permitiendo capturar nombres con demasiados espacios en blanco que la interfaz no presentaba, se elimiarán (#070KU7) ?>
		objListDataView = objCollectionLayOut.cells(dhxList).attachDataView({
			container:"listCell",
			drag:false, 
			select: 'multiselect',
			type:{
				template:"#picture#<div style='font-family:Roboto Regular;font-size:13px;font-weight:normal; text-align: center;' class='multiLineEllipsis' data-tooltip='#tooltip#'>#description#</div><div class='checkContainer'><div><i class='icon-check-sign'></i></div></div>",
				css:"custom",
				width:160,
				border:0,
				//padding:20,
				height:140
			}
			//x_count: 8
		});	//<img src='images/admin/unchecked.png' style='position:relative;top:10px;left:20px;'></img>
		
		//Genera la toolbar, primero determina si hay o no algún botón a mostrar basado en la definición de la colección
		var objCustomizedButtons = new Array();
		var objCustomBehaviour = new Object();
		var objCustomizedFns = new Object();		//Array con funciones personalizadas en algunos botones (si no se trata de una URL)
		var intButtons = 0;
		var blnCanAdd = <?=(int) $this->canAdd($aUser)?>;
		var blnCanRemove = <?=(int) $this->canRemove($aUser)?>;
		intButtons += (blnCanAdd)?1:0;
		intButtons += (blnCanRemove)?1:0;
<?
		$arrDefaultButtons = array("add" => "add", "remove" => "remove","import" => "import");
		$arrButtons = @$this->get_CustomButtons();
		if (!is_array($arrButtons)) {
			$arrButtons = array();
		}
		//GCRUZ 2015-09-17. Agregar botones personalizados al objecto de comportamientos
		if (method_exists($this, 'get_CustomButtonsBehaviour'))
		{
			$this->get_CustomButtonsBehaviour();
		}
		
		//Asigna el objeto de comportamientos personalizados para cambiar el evento o imagen a usar
		foreach ($arrButtons as $objButton) {
			$strButtonID = strtolower($objButton['id']);
			if (isset($arrDefaultButtons[$strButtonID])) {
?>
		objCustomBehaviour["<?=$strButtonID?>"] = {customized:true};
<?
			}
			
			//Genera el array de funciones personalizadas, estas deberían ejecutarse en el mismo contexto de este iFrame
			if (isset($objButton["onclick"]) && $objButton["onclick"]) {
?>
		objCustomizedFns["<?=$strButtonID?>"] = function() {
			try {
				<?=$objButton["onclick"]?>
			
			} catch(e) {
				console.log("Error executing button code (<?=$strButtonID?>): " + e);
			}
		}
<?
			}
		}
		
		//Genera el array de botones personalizados
		if (count($arrButtons)) {
			$strButtons = json_encode($arrButtons);
?>
		objCustomizedButtons = JSON.parse("<?=addslashes($strButtons)?>");
		var intCont = objCustomizedButtons.length;
		for (var intIndex = 0; intIndex < intCont; intIndex++) {
			var objButton = objCustomizedButtons[intIndex];
			if (objButton && objCustomBehaviour[objButton.id]) {
				objCustomBehaviour[objButton.id] = objButton;
			}
		}
<?
		}
?>
		var intCustomizedButtons = <?=count($arrButtons)?>;
		intButtons += intCustomizedButtons;
		
		//objCollectionLayOut.cells(dhxList).detachToolbar()
		//objCollectionLayOut.cells(dhxList).detachObject(true)
		if (intButtons) {
			objCollectionLayOut.cells(dhxList).attachToolbar({
				parent:"objectsCollection"
			});
			
			var objToolbar = objCollectionLayOut.cells(dhxList).getAttachedToolbar();
			objToolbar.setIconsPath("<?=$strScriptPath?>/images/admin/");
			var intPos = 0;
			if (blnCanAdd) {
				var strId = "add";
				var strLabel = "<?=translate("Add")?>";
				var strImage = "add.png";
				if (objCustomBehaviour[strId]) {
					var objButton = objCustomBehaviour[strId];
					if (objButton.label) {
						strLabel = objButton.label;
					}
					if (objButton.image) {
						strImage = objButton.image;
					}
				}
				objToolbar.addButton(strId, intPos++, strLabel, strImage);
			}
			
			if (blnCanRemove) {
				var strId = "remove";
				var strLabel = "<?=translate("Edit")?>";	//"<?=translate("Remove")?>";
				var strImage = "edit.png";	// "remove.png";
				if (objCustomBehaviour[strId]) {
					var objButton = objCustomBehaviour[strId];
					if (objButton.label) {
						strLabel = objButton.label;
					}
					if (objButton.image) {
						strImage = objButton.image;
					}
				}
				objToolbar.addButton(strId, intPos++, strLabel, strImage);

				objToolbar.addButton('removeCancel', intPos++, "<?=translate("Cancel")?>", strImage);

				objToolbar.hideItem('removeCancel');
			}
			
			//Agrega todos los botones personalizados, excepto si son considerados default, ya que en ese caso se personalizó sólo su diseño pero no debe
			//duplicar el botón (debe ser un array)
			if (intCustomizedButtons) {
				var intCont = objCustomizedButtons.length;
				for (var intIndex = 0; intIndex < intCont; intIndex++) {
					var objButton = objCustomizedButtons[intIndex];
					if (objButton && !objButton.isDefault) {
						objToolbar.addButton(objButton.id, intPos++, objButton.label, objButton.image);
					}
				}
			}
			
			//Prepara los eventos a utilizar, en el caso de los botones personalizados, el evento puede ser la invocación a código o a una URL, para los botones
			//fijos generalmente es una URL. Los IDs de los botones fijos también son fijos, con esto se puede sobreescribir el comportamiento de dicho botón
			//tal como si fuera personalizado
			objToolbar.attachEvent("onClick", function(id) {
				//Si hay un comportamiento personalizado (o botón personalizado), lo que se hace es invocar al código o URL personalizada según el caso
				var blnCustomized = false;
				if (objCustomBehaviour[id]) {
					blnCustomized = true;
				}
				
				//En este caso se trata de un comportamiento normal, se asume que hay una URL y por tanto se carga en el propio frame de esta celda
				if (!blnCustomized) {
					var strURL = "";
					switch (id) {
						case "add":
							strURL = "<?='main.php?'.$this->get_AddRemoveQueryString()?>";
							break;
						case "remove":
							<?= $myFormName ?>_Remove();
							break;
						case "removeCancel":
							<?= $myFormName ?>_RemoveCancel();
							break;
					}
					
					if (strURL && parent.doExecuteURL) {
						parent.doExecuteURL(strURL);
					}
					return;
				}
				
				//Si llega a este punto se trata de un comportamiento personalizado
				var strURL = "";
				var objFn = undefined;
				var intCont = objCustomizedButtons.length;
				for (var intIndex = 0; intIndex < intCont; intIndex++) {
					var objButton = objCustomizedButtons[intIndex];
					if (objButton) {
						if (objButton.id == id) {
							strURL = objButton.url;
							objFn = objCustomizedFns[id];
							break;
						}
					}
				}
				
				//Ejecuta la URL o función personalizada según el caso (no deben especificarse ambas configuraciones)
				if (strURL && parent.doExecuteURL) {
					parent.doExecuteURL(strURL);
				}
				else {
					if (objFn) {
						objFn();
					}
				}
			});
		}
		
		//Genera el contenido de la colección
<?
		//Remover esta asignación cuando sólo quede un menu de Surveys que siempre entre por las ventanas rediseñadas
		$_GET["Design"] = 1;
    	foreach ($this->Collection as $anObject) {
			//@JAPR 2015-07-09: Valida que sólo muestre el nuevo diseñador aquellos elementos creados con la nueva versión V6, los anteriores a ella serán validos si el objeto en si
			//no soporta identificador de versión solamente
			$blnValidItem = true;
			if (property_exists($anObject, "CreationAdminVersion")) {
				if ($anObject->CreationAdminVersion < esveFormsv6) {
					$blnValidItem = false;
				}
			}
			
			if (!$blnValidItem) {
				continue;
			}
			//@JAPR
			
    		$id = $anObject->get_FormIDFieldName();
    		$name = $anObject->get_FormFieldName();
    		$imagen = $anObject->get_Image();
			$intCanRemove = ($anObject->canRemove($aUser))?1:0;
    		if ($imagen=="") {
    			$imagen = '<img style="height:100; width:100;" src="images/standard.png" />';
    		}
			else {
    			$imagen = str_ireplace('src=', 'style="width:86; height:97;cursor:pointer;" src=', $imagen);
    		}
			
			//@JAPR 2016-07-25: Corregido un bug, se estaba permitiendo capturar nombres con demasiados espacios en blanco que la interfaz no presentaba, se elimiarán (#070KU7)
			//Se modificaron los espacios en blanco para convertirlos a su representación HTML de tal manera que el tooltip si los pueda representar correctamente
			//@JAPR 2018-11-09: Corregido el escape de algunos caracteres en strings (#KGW0CU)
?>
		objListDataView.add({id:'<?= $anObject->$id ?>', description:<?=  $anObject->Repository->ADOConnection->quote($anObject->$name) ?>, picture: '<?= $imagen ?>', 
			canRemove:<?=$intCanRemove?>, url:"main.php?<?=$anObject->get_QueryString()?>", tooltip:<?=str_replace(" ", "&nbsp;", $anObject->Repository->ADOConnection->quote(GetValidJScriptText($anObject->$name, array("fullhtmlspecialchars" => 1))))?>
		});
<?
    	}
?>
		//Carga el detalle del objeto en el que se hace click invocando a la función del padre para refresh de la celda
		objListDataView.attachEvent("onItemClick", function (id, ev, html) {//2015-06-15@JRPP cambio onItemDblClick por peticion de claudia
			console.log("objListDataView.onItemClick " + id);
			if (objToolbar && objToolbar.bitRemoveState) {

				objListDataView.select(id, true);

				return false;
			}
			
			if (!objListDataView || !id || !parent.doExecuteURL) {
				return;
			}
			
			var objItem = objListDataView.get(id);
			if (objItem.url) {
				parent.doExecuteURL("<?=$this->ContainerID?>", undefined, undefined, objItem.url);
			}
		});

		objListDataView.attachEvent("onMouseMove", function (id, ev, html) {
			var tooltip = $(html).find('div').attr('data-tooltip');
			doShowPopUp(id, ev, html,tooltip);
		});
		

		objListDataView.attachEvent("onMouseOut", function (ev) {
			doHidePopUp();
		});

		function doShowPopUp(id, ev, html, tooltip) {
			if (!objCollectionLayOut) {
				return;
			}
			
			var objDataView = objCollectionLayOut.cells(dhxList).getAttachedObject();
			if (!objDataView) {
				return;
			}
			
			var objOffset = $(ev.target || ev.srcElement).offset();
			var objElement = $(ev.target || ev.srcElement)[0];
			//Primero crea el PopUp en el objeto seleccionado
			if (!objMousePopUp) {
				objMousePopUp = new dhtmlXPopup();
			}
			objMousePopUp.clear();
			objMousePopUp.attachHTML(tooltip);
			//objMousePopUp.show(objOffset.left, objOffset.top, 300, 50);
			objMousePopUp.show(window.dhx4.absLeft(objElement), window.dhx4.absTop(objElement), objElement.offsetWidth, objElement.offsetHeight);
		}
		function doHidePopUp() {
			if (!objMousePopUp) {
				return;
			}
			
			objMousePopUp.hide();
		}

		</script>
		
<?
		if (trim($this->OrderPosField) != "")
		{
?>
		<script language="JavaScript">
			<?= $myFormName ?>_InitEventsToReorder();
		</script>
<?
		}
?>
		<?$this->generateAfterFormCode($aUser);?>
<?
		if ($this->Message != "")
		{
?>
 	    <script>
   	        alert('<?= str_replace("'", "\'", $this->Message) ?>');
		</script>
<?
		}
?>
	</div>
	<img id="<?= $myFormName ?>_Outline" src="images/pointer.gif" style="position:absolute; display:none" width="11" height="11" displayMe="0">
	<div id="<?= $myFormName ?>_Hint" style="position:absolute; width:auto; height:auto; display:none; background-color:#5C86B8; color:#ffffff; font-family:verdana; font-size:10px; font-weight:bold; padding:2px; text-align:center"></div>
<?
	}
	function afterRemove($aUser)
	{
	}
}
?>