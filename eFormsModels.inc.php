<?php

class BITAMeFormsModels extends BITAMObject
{

	/**
	 * Id del model de artus
	 * @var integer
	 */
	public $id_modelartus;

	/**
	 * Id de la forma en eForms
	 * @var integer
	 */
	public $id_survey;

	/**
	 * Nombre del modelo definido en eForms
	 * @var string
	 */
	public $name;

	/**
	 * Descripcion del modelo
	 * @var string
	 */
	public $description;


	static function NewInstance($aRepository)
	{
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;

		return eval('return new '.$strCalledClass.'($aRepository);');
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$strCalledClass = static::class;
		
		$anInstance = $strCalledClass::NewInstance($aRepository);
		
		$anInstance->id_modelartus = (int) $aRS->fields["id_modelartus"];
		
		$anInstance->id_survey = $aRS->fields["id_survey"];
		
		$anInstance->name = $aRS->fields["name"];

		$anInstance->description = $aRS->fields["description"];	

		return $anInstance;
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Model");
		}
		else
		{
			return $this->name;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=BITAMeFormsModels";
		}
		else
		{
			return "BITAM_PAGE=BITAMeFormsModels&idModel=".$this->id_modelartus;
		}
	}

	function getJSonDefinition()
	{
		return array();
	}

	function remove()
	{
		require_once("artusmodelsurvey.class.php");
		
		ARTUSModelFromSurvey::delete ($this->Repository, $this->id_modelartus);

		$this->Repository->DataADOConnection->Execute( "DELETE FROM si_sv_modelartus WHERE id_modelartus = " . $this->id_modelartus );

		$this->Repository->DataADOConnection->Execute( "DELETE FROM si_sv_modelartus_mapping WHERE id_modelartus = " . $this->id_modelartus );

		$this->Repository->DataADOConnection->Execute( "DELETE FROM si_sv_modelartus_questions WHERE id_modelartus = " . $this->id_modelartus );

	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		if (array_key_exists("idModel", $aHTTPRequest->POST))
		{
			$ids = $aHTTPRequest->POST["idModel"];
			if (is_array($ids))
			{
				
				$aCollection = BITAMeFormsModelsCollection::NewInstance($aRepository, $ids);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
		}

	}

}

class BITAMeFormsModelsCollection extends BITAMCollection
{

	public $NoHeader = true;
	
	public $HasPath = false;

	static function NewInstance($aRepository, $anArrayOfCatalogIDs = null)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		//@JRPP 2015-06-27: Rediseñado el Admin con DHTMLX
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		
		$anInstance = new $strCalledClass($aRepository);
		
		$sql = "SELECT id_modelartus, id_survey, `name`, description  FROM si_sv_modelartus";

		if( is_array( $anArrayOfCatalogIDs ) )
		{
			$sql .= ' WHERE id_modelartus IN ('. implode(',', $anArrayOfCatalogIDs) .')';
		}

		$sql .= " ORDER BY `name`";
		
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
			$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
			
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			
			$aRS->MoveNext();
		}

		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;

		return $strCalledClass::NewInstance($aRepository, null);
	}

	function generateForm($aUser)
	{
		//header("Content-Type:text/javascript; charset=UTF-8");
		header("Content-Type:application/json; charset=UTF-8");

		$sAction = getParamValue("action", 'both', '', true);

		$id_modelartus = getParamValue("idModel", 'both', '(int)', true);

		$res = array( 'questions' => array(), 'eBavelApps' => array(), 'tree' => array(), 'infoModel' => array(), 'collection' => array(), 'questionSelect' => '<option value=""></option>', 'ebavelListApps' => '<option value=""></option>' );

		if( $sAction == 'getCollection' )
		{
			$rs = $this->Repository->DataADOConnection->Execute('SELECT id_modelartus, `name`, description FROM si_sv_modelartus WHERE id_survey = ' . getParamValue('idSurvey', 'both', '(int)', true) );

			while($rs && !$rs->EOF)
			{
				$res['collection'][] = array('id' => $rs->fields['id_modelartus'], 'name' => $rs->fields['name'], 'description' => $rs->fields['description']);
				
				$rs->MoveNext();
			}
		}

		/* Obtiene las colecciones de elementos que se usarán como posibles valores a seleccionar en la interfaz Wizard de la configuración de un Modelo de Artus
		directamente desde la metadata, y la formatea para que sea aplicable en los componentes de la ventana
		*/
		if( $sAction == "getInfoCreateArtusModel" && getParamValue("idSurvey", 'both', '(int)', true) )
		{
			include_once('question.inc.php');

			$aQuestionCollection = BITAMQuestionCollection::NewInstance($this->Repository, getParamValue("idSurvey", 'both', '(int)', true));

			$lastSection = '';

			for($i = 0; $i < count($aQuestionCollection->Collection); $i++)
			{
				//Excluye los tipos de preguntas que no son mapeables por no tener un valor
				//@JAPR 2016-04-12: Corregido un bug, no estaba excluyendo todos los tipos de pregunta sin respuesta y estaba incluyendo a los tipos Llamada
				if( !in_array($aQuestionCollection->Collection[$i]->QTypeID, array(qtpShowValue,qtpAction,qtpMessage,qtpSkipSection,qtpSync,qtpPassword,qtpMapped,qtpSection,qtpOCR,qtpExit,qtpUpdateDest)) && @$aQuestionCollection->Collection[$i]->UseCatalog != 1 )
				{
					
					if( $lastSection != $aQuestionCollection->Collection[$i]->SectionName )
					{
						if($lastSection != '')
						{
							$res['questionSelect'] .= '</optgroup>';
						}

						$res['questionSelect'] .= '<optgroup label="'. $aQuestionCollection->Collection[$i]->SectionName .'">';
					}

					$res['questionSelect'] .= '<option value="'. $aQuestionCollection->Collection[$i]->QuestionID .'">'. $aQuestionCollection->Collection[$i]->QuestionText .'</option>';
				
					$lastSection = $aQuestionCollection->Collection[$i]->SectionName;
				}				
			}
			
			/** Obtenemos la lista de aplicaciones->formas->campos */
			$rs = $this->Repository->ADOConnection->Execute("SELECT A.applicationName, A.codeName, B.sectionName, C.label, B.id as idForm, C.id as idField FROM si_forms_apps A, si_forms_sections B, si_forms_fieldsforms C WHERE A.id_app = B.appId AND B.id_section = C.section_id  AND C.tagname IN ('widget_field_alphanumeric', 'widget_field_date', 'widget_field_numeric', 'widget_field_time', 'widget_field_email', 'widget_uniqueid', 'widget_callphone') ORDER BY A.applicationName, B.sectionName, C.label");
			
			$lastApp = '';
			$lastForm = '';

			while($rs && !$rs->EOF)
			{

				if( $lastApp != $rs->fields['codeName'] )
				{
					$res['ebavelListApps'] .= '<option value="'. $rs->fields['codeName'] .'">'. $rs->fields['applicationName'] .'</option>';
				}

				if( !isset($res['eBavelApps'][$rs->fields['codeName']]) ) $res['eBavelApps'][$rs->fields['codeName']] = '<option value=""></option>';

				if( $lastForm != $rs->fields['idForm'] )
				{
					if($lastForm != '')
					{
						$res['eBavelApps'][$rs->fields['codeName']] .= '</optgroup>';
					}

					$res['eBavelApps'][$rs->fields['codeName']] .= '<optgroup label="'. $rs->fields['sectionName'] .'">';
				}

				$res['eBavelApps'][$rs->fields['codeName']] .= '<option value="'. $rs->fields['idField'] . '|' . $rs->fields['idForm'] .'">'. $rs->fields['label'] .'</option>';				
				
				$lastApp = $rs->fields['codeName'];

				$lastForm = $rs->fields['idForm'];

				$rs->MoveNext();
			}

			if( $idModel =  getParamValue("idModel", 'both', '(int)', true) )			
			{
				/** Obtenemos la informacion basica del modelo */
				$rs = $this->Repository->DataADOConnection->Execute('SELECT `name`, description FROM si_sv_modelartus WHERE id_modelartus = ' . $idModel);

				if( $rs && $rs->RecordCount() )
				{
					
					$res['infoModel'] = array('name' => $rs->fields['name'], 'description' => $rs->fields['description']);

				}
			}

			/** Obtenemos la informacion de mapeo si existe */
			$rs = $this->Repository->DataADOConnection->Execute("SELECT A.id_question, A.id_field, A.ebavel_form, B.codeName FROM si_sv_modelartus_mapping A, si_forms_apps B, si_forms_sections C WHERE C.id = A.ebavel_form AND C.appID = B.id_app AND A.id_modelartus = " . $id_modelartus);

			while($rs && !$rs->EOF)
			{

				$res['infoModel']['mapping'][] = array( 'id_question' => $rs->fields['id_question'], 'value' => $rs->fields['id_field'] . "|" . $rs->fields['ebavel_form'], 'form' => $rs->fields['ebavel_form'], 'app' => $rs->fields['codeName'] );

				$rs->MoveNext();
			}

			/** Obtenemos la informacion de los atributos */
			$rs = $this->Repository->DataADOConnection->Execute('SELECT id, id_section, id_question, `path`, `type` FROM si_sv_modelartus_questions WHERE id_modelartus = ' . $id_modelartus);

			while ($rs && !$rs->EOF)
			{
				$resAttr = array('id' => $rs->fields['id'] ,'path' => ( trim($rs->fields['path']) != '' ? $rs->fields['path'] : $rs->fields['id_question'] . '['. $rs->fields['type'] .']' ), 'questionid' => @$rs->fields['id_question'], 'isEBavel' => false);
								
				if( strpos($resAttr['path'], 'FRM_') !== false ) {
					$resAttr['isEBavel'] = true;
				}

				$res['infoModel']['attributes'][] = $resAttr;

				$rs->MoveNext();
			}

		}

		if( $sAction == 'buildTree' )
		{

			if( $arrMapging = getParamValue("mapping", 'both', '(array)', true) )
			{
				$lastFetchMode = $this->Repository->ADOConnection->setFetchMode( ADODB_FETCH_NUM );

				/** Construimos el arbol de campos de eBavel */
				for($i = 0; $i < count( $arrMapging ); $i++)
				{
					list($idQuestion, $sField, $sForm) = explode('|', $arrMapging[$i]);
					/** Buscamos el ID numerico de la forma */
					//$rs = $this->Repository->ADOConnection->Execute('SELECT id_section, sectionName, id FROM SI_FORMS_SECTIONS WHERE id = ' . $this->Repository->ADOConnection->quote( $sForm ) );
					$rs = $this->Repository->ADOConnection->Execute('SELECT B.id_section, B.sectionName, A.label, B.id FROM si_forms_fieldsforms A, si_forms_sections B WHERE B.id_section = A.section_id AND A.id = ' . $this->Repository->ADOConnection->quote( $sField ) );

					$rsQ = $this->Repository->DataADOConnection->Execute('SELECT QuestionText FROM SI_SV_Question WHERE QuestionID = ' . $idQuestion);

					if( !isset( $res['tree'][$sForm] ) && $rs && $rs->RecordCount() > 0 )
					{
						$res['tree'][$arrMapging[$i]] = BuildTree::createAttrNodeList( $rsQ->fields['QuestionText'] . ' : ' . $rs->fields[2] . '@' . $rs->fields[1], $rs->fields[0], $this->Repository->ADOConnection, null, '', $rs->fields[3], null, array(), '', array('data-key="'. $arrMapging[$i] .'"'));
					}
				}

				$this->Repository->ADOConnection->setFetchMode( $lastFetchMode );
			}

			include_once('question.inc.php');

			/** Construimos el arbol de preguntas de forms */
			$aQuestionCollection = BITAMQuestionCollection::NewInstance($this->Repository, getParamValue("idSurvey", 'both', '(int)', true));

			$htmlList = array();

			for($i = 0; $i < count($aQuestionCollection->Collection); $i++)
			{
				//Excluye los tipos de preguntas que no son mapeables por no tener un valor
				//@JAPR 2016-04-12: Corregido un bug, no estaba excluyendo todos los tipos de pregunta sin respuesta y estaba incluyendo a los tipos Llamada
				if( !in_array($aQuestionCollection->Collection[$i]->QTypeID, array(qtpShowValue,qtpAction,qtpMessage,qtpSkipSection,qtpSync,qtpPassword,qtpMapped,qtpSection,qtpOCR,qtpExit,qtpUpdateDest)) )
				{
					if( !isset( $htmlList[ $aQuestionCollection->Collection[$i]->SectionName ] ) )
					{
						$htmlList[ $aQuestionCollection->Collection[$i]->SectionName ]['li'] = '<a data-pathnumeric="" data-pathlabel="" data-label="'. $aQuestionCollection->Collection[$i]->SectionName .'" class="" data-path=""><div class="icon-expand-alt eforms"></div> '. $aQuestionCollection->Collection[$i]->SectionName .'</a>';

						//2015-08-27@HADR (#INLINESECTIONVALUE) La seccion tipo inline debe mostrar una pregunta ficticia (Section Value) para seleccionar el dato configurado en la seccion para poblar la inline
						$aSection = BITAMSection::NewInstanceWithID($this->Repository, $aQuestionCollection->Collection[$i]->SectionID);
						if (!is_null($aSection) && $aSection->SectionType == 4) { //Inline Section
							switch($aSection->ValuesSourceType){
								case 1://Catalogo
									if($aSection->DataSourceID){
										/** Buscamos los atributos del catalogo */
										$rs = $this->Repository->DataADOConnection->Execute("SELECT MemberID, MemberName FROM si_sv_datasourcemember WHERE DataSourceID = {$aSection->DataSourceID} ORDER BY 2");

										$arrTmpDataSourceMembers = array();

										while($rs && !$rs->EOF)
										{
											$arrTmpDataSourceMembers[] = '<li><a data-questionid="-'.$aQuestionCollection->Collection[$i]->SectionID.'" data-pathnumeric="" data-pathlabel="" data-label="Section Value ('.$aQuestionCollection->Collection[$i]->SectionName.') -> ' . $rs->fields['MemberName'] .'" class="attrField" data-path="'. $rs->fields['MemberID'] .'[CAT]">
																			<div class="icon-dimension"></div> '. $rs->fields['MemberName'] .'</a></li>';
											$rs->MoveNext();
										}
										$li = '<li><a data-isquestioncatalog="true" data-pathnumeric="" data-pathlabel="" data-label="Section Value ('.$aQuestionCollection->Collection[$i]->SectionName.')" class="" data-path="">
										 		<div class="icon-expand-alt"></div> Section Value ('.$aQuestionCollection->Collection[$i]->SectionName.')</a><ul>' . implode('', $arrTmpDataSourceMembers) . '</ul></li>';
										$htmlList[ $aQuestionCollection->Collection[$i]->SectionName ]['ul'][] = $li;
									}
									break;
								default://Captura de usuario y otros 2 casos
									$li = '<li><a data-questionid="-'.$aQuestionCollection->Collection[$i]->SectionID.'" data-pathnumeric="" data-pathlabel="" data-label="Section Value ('.$aQuestionCollection->Collection[$i]->SectionName.')" class="attrField" data-path="-'.$aQuestionCollection->Collection[$i]->SectionID.'[DIM]">
											<div class="icon-dimension"></div> Section Value ('.$aQuestionCollection->Collection[$i]->SectionName.')</a></li>';
									$htmlList[ $aQuestionCollection->Collection[$i]->SectionName ]['ul'][] = $li;
									break;
							}
						}
					}

					$sClass = 'icon-dimension';

					if( in_array($aQuestionCollection->Collection[$i]->QTypeID, array(1,14)) )
					{
						$sClass = 'icon-indicator';
					}

					$dataQuestionid = 'data-questionid="'. $aQuestionCollection->Collection[$i]->QuestionID .'"';

					$dataPath = 'data-path="'. $aQuestionCollection->Collection[$i]->QuestionID . ( $sClass == 'icon-dimension' ? '[DIM]' : '[IND]' ) . '"';

					if( @$aQuestionCollection->Collection[$i]->UseCatalog )
					{
						$sClass = 'icon-expand-alt';

						$dataPath = '';

						$dataQuestionid = 'data-isquestioncatalog="true"';

						/** Buscamos los atributos del catalogo */
						//$rs = $this->Repository->DataADOConnection->Execute("SELECT t3.MemberID, t3.MemberName FROM si_sv_questionmembers AS t1 INNER JOIN si_sv_datasourcemember t3 ON (t1.MemberID = t3.MemberID) WHERE t1.QuestionID = {$aQuestionCollection->Collection[$i]->QuestionID} ORDER BY 2");
						$rs = $this->Repository->DataADOConnection->Execute("SELECT t3.MemberID, t3.MemberName FROM si_sv_question AS t1 INNER JOIN si_sv_datasourcemember t3 ON (t1.DataSourceID = t3.DataSourceID) WHERE t1.QuestionID = {$aQuestionCollection->Collection[$i]->QuestionID} ORDER BY 2");

						$arrTmpDataSourceMembers = array();

						while($rs && !$rs->EOF)
						{
							$arrTmpDataSourceMembers[] = '<li><a data-questionid="'. $aQuestionCollection->Collection[$i]->QuestionID .'" data-pathnumeric="" data-pathlabel="" data-label="' . $aQuestionCollection->Collection[$i]->QuestionText . ' -> ' . $rs->fields['MemberName'] .'" class="attrField" data-path="'. $rs->fields['MemberID'] .'[CAT]"><div class="icon-dimension"></div> '. $rs->fields['MemberName'] .'</a></li>';		
							$rs->MoveNext();
						}

					}

					$htmlList[ $aQuestionCollection->Collection[$i]->SectionName ]['ul'][] = '<li><a '. $dataQuestionid .' data-pathnumeric="" data-pathlabel="" data-label="'. $aQuestionCollection->Collection[$i]->QuestionText .'" class="'. ( @$aQuestionCollection->Collection[$i]->UseCatalog ? '': 'attrField' ) .'" '. $dataPath .'><div class="'. $sClass .'"></div> '. $aQuestionCollection->Collection[$i]->QuestionText .'</a>'. ( @$aQuestionCollection->Collection[$i]->UseCatalog ? '<ul>' . implode('', $arrTmpDataSourceMembers) . '</ul>' : '' ) .'</li>';
				}				
			}

			if( count( $htmlList ) )
			{
				foreach ($htmlList as $key => $value) 
				{
					
					$res['tree']['ev_' . $key] = '<li>' . $value['li'] . '<ul>'. implode('', $value['ul']) .'</ul></li>';

				}
			}
		}

		if( $sAction == 'saveArtusModel' )
		{
			$bIsNew = false;

			if( $id_modelartus > 0 )
			{
				/** Edicion de un modelo */
				$this->Repository->DataADOConnection->Execute("UPDATE si_sv_modelartus SET name = ". $this->Repository->ADOConnection->quote( getParamValue('name', 'both', '(string)', true) ) .", description = ". $this->Repository->ADOConnection->quote( getParamValue('description', 'both', '(string)', true) ) .", modifiedDate = ". $this->Repository->ADOConnection->quote( date("Y-m-d H:i:s") ) . " WHERE id_survey = ". getParamValue('idSurvey', 'both', '(int)', true) ." AND id_modelartus = {$id_modelartus}");
			} else
			{
				$bIsNew = true;
				/** Insertamos un nuevo modelo */
				$this->Repository->DataADOConnection->Execute("INSERT INTO si_sv_modelartus (`id_survey`, `name`, `description`, `createdDate`, `modifiedDate`) VALUES (". getParamValue('idSurvey', 'both', '(int)', true) .", ". $this->Repository->ADOConnection->quote( getParamValue('name', 'both', '(string)', true) ) .", ". $this->Repository->ADOConnection->quote( getParamValue('description', 'both', '(string)', true) ) .", ". $this->Repository->ADOConnection->quote( date("Y-m-d H:i:s") ) .", ". $this->Repository->ADOConnection->quote( date("Y-m-d H:i:s") ) .")");

				$id_modelartus = $this->Repository->DataADOConnection->_insertid();
			}

			$arrMapging = getParamValue("mapping", 'both', '(array)', true);

			/** Borramos el mapeo anterior e insertamos el nuevo */
			$this->Repository->DataADOConnection->Execute("DELETE FROM si_sv_modelartus_mapping WHERE id_modelartus = " . $id_modelartus);

			for($i = 0; $i < count( $arrMapging ); $i++)
			{
				/** Buscamos la tabla del campo */
				$rs = $this->Repository->DataADOConnection->Execute("SELECT CONCAT('SI_FORMS_', A.codeName, '_' ,B.id) AS `table`, B.id as idForm, C.id FROM si_forms_apps A, si_forms_sections B, si_forms_fieldsforms C WHERE A.id_app = B.appId AND B.id_section = C.section_id AND C.id = " . $this->Repository->ADOConnection->quote( $arrMapging[$i]['field'] ));

				if( $rs && $rs->RecordCount() > 0 )
				{
					//$arrMappingeBavel[] = array('')

					/** Insertamos la definicion del mapeo de la pregunta de eForms con el campo de eBavel */
					$this->Repository->DataADOConnection->Execute("INSERT INTO si_sv_modelartus_mapping (`id_modelartus`, `id_question`, `id_field`, `ebavel_table`, `ebavel_form`) VALUES ({$id_modelartus}, {$arrMapging[$i]['idquestion']}, ". $this->Repository->ADOConnection->quote( $arrMapging[$i]['field'] ) .", ". $this->Repository->ADOConnection->quote( $rs->fields['table'] ) .", ". $this->Repository->ADOConnection->quote( $rs->fields['idForm'] ) .")");
				}
			}

			/** Guardamos las dimensiones / indicadores seleccionados */
			$arrAttributes = getParamValue('attributes', 'both', '(array)', true);

			/** Estos son los IDs de los atributos que existen en el modelo de artus */
			$arrIdsAttrs = array();

			for( $i = 0; $i < count( $arrAttributes ); $i++ )
			{
				$sPath = $eachPath = $arrAttributes[$i]['path'];

				$match = null;

				$rename = '';

				/** Primero reemplaza el {} por vacio, para dejar el puro path */
				$sPath = preg_replace("/{.*}/", "", $sPath);

				/** Obtenemos el nombre, si existe */
				preg_match_all("/{.*}/", $eachPath, $match);

				if($match && @$match[0] && $match[0][0] )
				{
					$rename = $match[0][0];
				}

				$intQuestionId = null;
				if( $arrAttributes[$i]['pathori'] != 'null' )
				{
					$sPath = $arrAttributes[$i]['pathori'];

					$intQuestionId = $arrAttributes[$i]['questionid'];
				}
				$idModelArtusQuestion = $arrAttributes[$i]['idModelArtusQuestion'];
/*
				$sQuery = "SELECT id FROM si_sv_modelartus_questions WHERE id_modelartus = {$id_modelartus} AND path = " . $this->Repository->DataADOConnection->quote( $sPath );

				if( $intQuestionId )
				{
					$sQuery .= " AND id_question = {$intQuestionId} ";
				}
				$rs = $this->Repository->DataADOConnection->Execute( $sQuery );
				
				if( $rs && $rs->RecordCount() == 0 )
				{
					// Si no lo encontro lo buscamos con el nombre con comodin 
					$sQuery = 'SELECT id FROM si_sv_modelartus_questions WHERE path LIKE "'. $sPath .'{%}" AND id_modelartus = ' . $id_modelartus;

					if( $intQuestionId )
					{
						$sQuery .= " AND id_question = {$intQuestionId} ";
					}
					$rs = $this->Repository->DataADOConnection->Execute( $sQuery );
				}
*/
				/** Si no lo encontro entonces es un atributo nuevo */
				if($idModelArtusQuestion < 0 )
				{
					$idSection = 0;
					$idQuestion = 0;
					$arrType = null;

					if( is_numeric($arrAttributes[$i]['questionid']) )
					{
						$idQuestion = $arrAttributes[$i]['questionid'];

						preg_match_all("/DIM|IND/", $eachPath, $match);
						
						$arrType = $match[0][0];

						if( $arrAttributes[$i]['pathori'] != 'null' &&  $arrAttributes[$i]['pathori'] != '' )
						{
							$eachPath = $arrAttributes[$i]['pathori'] . $rename;
						}

						//$eachPath = null;
					} else
					{
						/** Es un mapeo de pregunta a eBavel */
						list($idQuestion) = explode("|", $arrAttributes[$i]['key']);
					}

					//2015-08-27@HADR (#INLINESECTIONVALUE) COmprobar si es una pregunta ficticia Section Value
					if($idQuestion < 0){
						$idSection = str_replace("-", "", $idQuestion);
					}else{
						/** Buscamos la seccion de la pregunta de eForms */
						$rs = $this->Repository->DataADOConnection->Execute("SELECT SectionID FROM SI_SV_Question WHERE questionID = " . $idQuestion);

						if( $rs && $rs->RecordCount() > 0 )
						{
							$idSection = $rs->fields['SectionID'];
						}
					}

					$this->Repository->DataADOConnection->Execute('INSERT INTO si_sv_modelartus_questions (id_modelartus, path, id_section, id_question, type) VALUES ('. $id_modelartus .', '. $this->Repository->DataADOConnection->Quote( $eachPath ) .", {$idSection}, {$idQuestion}, ". $this->Repository->DataADOConnection->Quote( $arrType ) .") ");
					$lastId = $this->Repository->DataADOConnection->_insertid();

				} else 
				{
					$lastId = $idModelArtusQuestion;

					if( $arrAttributes[$i]['pathori'] != 'null' &&  $arrAttributes[$i]['pathori'] != '' )
					{
						$eachPath = $arrAttributes[$i]['pathori'] . $rename;
					}
					
					$idSection = 0;
					$idQuestion = 0;

					if( is_numeric($arrAttributes[$i]['questionid']) ){
						$idQuestion = $arrAttributes[$i]['questionid'];
					}else{
						/** Es un mapeo de pregunta a eBavel */
						list($idQuestion) = explode("|", $arrAttributes[$i]['key']);
					}

					//2015-08-27@HADR (#INLINESECTIONVALUE) COmprobar si es una pregunta ficticia Section Value
					if($idQuestion < 0){
						$idSection = str_replace("-", "", $idQuestion);
					}else{
						/** Buscamos la seccion de la pregunta de eForms */
						$rs = $this->Repository->DataADOConnection->Execute("SELECT SectionID FROM SI_SV_Question WHERE questionID = " . $idQuestion);

						if( $rs && $rs->RecordCount() > 0 )
						{
							$idSection = $rs->fields['SectionID'];
						}
					}
					
					/** Si lo encontro, entonces lo actualizamos con su nuevo nombre */
					//2016-01-15@JRPP A la hora de actualizar no actualizamos el campo id_section
					$this->Repository->DataADOConnection->Execute("UPDATE si_sv_modelartus_questions SET path = '". $eachPath ."' WHERE id = " . $lastId);
				}

				$arrIdsAttrs[ ] = $lastId;

			}

			/** Eliminamos los que ya no necesito de la tabla de atributos del modelo */
			$this->Repository->DataADOConnection->Execute( 'DELETE FROM si_sv_modelartus_questions WHERE id NOT IN ('. implode(',', $arrIdsAttrs) .') AND id_modelartus = ' . $id_modelartus );	

			$res['collection'][] = array('id' => $id_modelartus, 'name' => getParamValue('name', 'both', '(string)', true), 'description' => getParamValue('description', 'both', '(string)', true));			

			/** Creamos el modelo de artus */
			/** $id_modelartus => id del modelo de artus en la tabla si_sv_modelartus */

			require_once("artusmodelsurvey.class.php");

			$theArtusModelFromSurvey = ARTUSModelFromSurvey::buildARTUSModelWithKey($this->Repository, $id_modelartus);
		    if ($theArtusModelFromSurvey)
		    {
				$theArtusModelFromSurvey->save();
			}


		}

		echo json_encode($res);

		exit();
	}

	function generateDetailForm($aUser, $isAssociateDissociateMode = false) {

		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		
		$strScriptPath = $path_parts["dirname"];

		$myFormName = get_class($this);

		

	
		exit();

	}

	function get_CustomButtons()
	{
		return array();
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=" . static::class;
	}

	function get_Title()
	{	
		return translate("Models");
	}

	function get_PathArray()
	{
		$pathArray = array();
		
		return $pathArray;
	}

}

class BuildTree {

	/**
	 * Crea el nodo raiz del arbol
	 * @param  string 			$label       Nombre del arbol
	 * @param  integer 			$id          Id de la forma de eBavel
	 * @param  ADOConnection 	$aConnection Objeto conexion a BMD
	 * @param  [type] $type        [description]
	 * @param  [type] $path        [description]
	 * @param  [type] $nodeName    [description]
	 * @param  [type] $leafName    [description]
	 * @param  array  $pathLabel   [description]
	 * @param  string $idJoin      [description]
	 * @return [type]              [description]
	 */
	public static function createAttrNodeList($label, $id, $aConnection, $type, $path, $nodeName, $leafName, $pathLabel = array(), $idJoin = '', $attrLi = array()) 
	{		
		$sPathLabel = '';
		$sPathNumeric = '';
		$icon = 'compass';
		if($type == 'numeric')
			$icon = 'indicator';
		else if ($type == 'string')
			$icon = 'dimension';

		if($type == 'numeric' && count($pathLabel) >= 1)
			return '';
		
		if( $id && strpos($path, $nodeName) === false )
			$icon = 'expand-alt';
		else {
			$path .= $leafName . "[". ( $type == 'numeric' ? 'IND' : 'DIM' ) ."]";

			for($i = 0; $i < count($pathLabel); $i++)
			{
				if(count($pathLabel) == $i+1)
				{
					$sPathLabel .= ($sPathLabel != '' ? '.' : '' ) . $pathLabel[$i] . '.'/*. '.' . $label*/;
					$sPathNumeric .= ($sPathNumeric != '' ? '.' : '' ) . substr($pathLabel[$i], 0, strpos($pathLabel[$i], '.'));
				}
				else
				{
					$sPathLabel .= ($sPathLabel != '' ? '.' : '' ) . substr($pathLabel[$i], 0, strpos($pathLabel[$i], '.'));
					$sPathNumeric .= ($sPathNumeric != '' ? '.' : '' ) . substr($pathLabel[$i], 0, strpos($pathLabel[$i], '.'));
				}
			}
		}

		$res = '';
		
		if( strpos($path, $nodeName) === false ) 
		{
			$res = "<li ". implode(' ', $attrLi) ." ><a data-pathnumeric=\"". $sPathNumeric ."\" data-pathlabel=\"". ($sPathLabel ? $sPathLabel : '') ."\" data-label=\"". $label ."\" class=\"". ( $id && strpos($path, $nodeName) === false ? '' : 'attrField' ) ."\" data-path=\"". $path ."\"><div class=\"icon-". $icon ." ". ( is_null($type) ? 'ebavel' : '' ) ."\"></div> $label</a>";

			if( $id  )
			{
				$num = substr($label, 0, strpos($label, '.'));
				if($num && is_numeric($num))
					$pathLabel[] = $label;

				$res .= self::createFormNodeList($id, $aConnection, $path . ( $nodeName ? "{$nodeName}[" . ($idJoin ? $idJoin : ("id_" . $nodeName . ( $leafName ? ":{$leafName}" : "" ) ) ) ."]->" : '' ), $pathLabel);
			}

			$res .= "</li>";

		}
		
		return $res;
	}

	public static function createFormNodeList($id, $aConnection, $path, $pathLabel)
	{
		$catalogs = array();
		$attr = array();

		/** Mostramos los atributos de la tabla de usuarios */
		if( $id == -1 )
		{
			$attr['cuenta_correo'] = array( 'label' => 'eMail', 'id' => null, 'nodeName' => null, 'tagname' => 'widget_field_alphanumeric', 'attrId' => 'cuenta_correo'/*, 'idJoin' => 'cla_usuario:' . 'id_' . $rs->fields[4] */ );
			$attr['nom_largo'] = array( 'label' => 'Long name', 'id' => null, 'nodeName' => null, 'tagname' => 'widget_field_alphanumeric', 'attrId' => 'nom_largo'/*, 'idJoin' => 'cla_usuario:' . 'id_' . $rs->fields[4] */ );
		} else if( $id == -2 )
		{
			$arrMatch = array();
			preg_match("/:(.*)]->/", $path, $arrMatch);
			$sFieldLabel = "Description";
			if( isset($arrMatch[1]) )
			{
				$rs = $aConnection->Execute( "SELECT label FROM si_forms_fieldsforms WHERE id = " . $aConnection->quote( $arrMatch[1] ) );
				if( $rs && !$rs->EOF ) $sFieldLabel = $rs->fields[0];
			}
			$attr['field'] = array( 'label' => $sFieldLabel, 'id' => null, 'nodeName' => null, 'tagname' => 'widget_field_alphanumeric', 'attrId' => 'value'/*, 'idJoin' => 'cla_usuario:' . 'id_' . $rs->fields[4] */ );
		} else {
			$aSQL = "SELECT A.label, A.id, A.tagname, B.value, C.id_section, C.id, C.sectionName, D.value
	FROM si_forms_fieldsforms A
	LEFT JOIN si_forms_elementattrs B ON B.fieldform_id = A.id_fieldform AND B.attrName = 'catalog_form'
	LEFT JOIN si_forms_sections C ON C.id = B.value
	LEFT JOIN si_forms_elementattrs D ON D.fieldform_id = A.id_fieldform AND D.attrName IN ('catalog_filter', 'defaultValue')
	WHERE  A.section_id = {$id} AND A.tagname NOT IN ('widget_secheader', 'widget_file', 'widget_signature', 'widget_photo', 'widget_calculatedfield', 'widget_formdetail') AND A.id NOT LIKE 'createdDate_%' AND A.id NOT LIKE 'modifiedDate_%'
	ORDER BY A.sortableKey IS NULL ASC, A.sortableKey ASC";
			$rs = $aConnection->Execute($aSQL);
			//echo "\n\n-- ===== Fieldsforms =====\n\n".$aSQL.";";

			while($rs && !$rs->EOF)
			{
				//2015-03-02@HADR (#UBGNZX) Hfrias; No esta creando correctamante la vista: agrega los campos cuenta_correo y nom_largo para ser tratados como catalogo
				/** Si es un campo especial a la tabla de usuarios */
				if( strpos($rs->fields[1], 'modifiedUser_') === 0 || strpos($rs->fields[1], 'createdUser_') === 0 || strpos($rs->fields[1], 'owner_') === 0 || strpos($rs->fields[1], 'cuenta_correo_') === 0 || strpos($rs->fields[1], 'nom_largo_') === 0)
				{
					$attr[$rs->fields[1]] = array( 'label' => $rs->fields[0] . '@Users', 'id' => -1, 'nodeName' => 'SI_USUARIO', 'tagname' => null, 'attrId' => $rs->fields[1]  );
				} 
				/** si el campo es de tipo catalogo y tiene un catalog_filter buscamos si esta ligado con un campo anterior */
				else if($rs->fields[4])
				{
					
					if(!self::hasReachedLimitedLevels($path)){
						$bInsert = false;

						if( $rs->fields[7] )
						{
							/** buscamos en los campos anteriores si este campo se filtra por los campo anterior */
							for($i = 0; $i < count($catalogs['forms'][$rs->fields[5]]); $i++)
							{
								/** encontramos que el campo se filtra por el valor de un campo anterior, no lo colocamos y el campo anterior lo eliminamos de los atributos a mostrar */
								if(preg_match ( '/' . implode("|", $catalogs['forms'][$rs->fields[5]]) . '/'  , $rs->fields[7]) === 1)
								{
									//2015-04-21@HADR (#KIXQJH) Juega y Gana | Modelos Artus no considera mas de una dimension para un mismo catalogo
									//los campos de tipo catalogo que no tienen catalog_filter son guardados como campos normales hasta que se detecta que hay otro campo catalogo que apunta al mismo catalogo 
									//y que tiene un catalog filter con el campo anterior, entonces se maneja como un campo enlazado y se elimina el campo anterior de la lista de atributos
									foreach($catalogs['forms'][$rs->fields[5]] as $eachField){
										unset($attr[$eachField]);
									}
									/** Agregamos el atributo unico de catalogo que representa a los campos ligados */
									$attr[$rs->fields[5]] = array( 'label' => encodeUTF8($rs->fields[6]), 'id' => $rs->fields[4], 'nodeName' => $rs->fields[5], 'tagname' => $rs->fields[2], 'attrId' => $rs->fields[1]  );
									$bInsert = true;
								}
							}
						} 

						if(!$bInsert || is_null( $rs->fields[7] )) 
						{
							//2015-04-21@HADR (#KIXQJH) Juega y Gana | Modelos Artus no considera mas de una dimension para un mismo catalogo
							//Se guarda en la posicion del array con el id del campo en vez del id de la forma catalogo
							$attr[$rs->fields[1]] = array( 'label' => encodeUTF8($rs->fields[0] . '@' . $rs->fields[6]), 'id' => $rs->fields[4], 'nodeName' => $rs->fields[5], 'tagname' => $rs->fields[2], 'attrId' => $rs->fields[1]  );
						}

						$catalogs['fields'][$rs->fields[1]] = $rs->fields[5];
						$catalogs['forms'][$rs->fields[5]][] = $rs->fields[1];
					}
				} else 
				{
					$attr[$rs->fields[1]] = array( 'label' => encodeUTF8($rs->fields[0]), 'id' => $rs->fields[4], 'nodeName' => $rs->fields[5], 'tagname' => $rs->fields[2], 'attrId' => $rs->fields[1]  );

					if( $attr[$rs->fields[1]]['tagname'] == 'widget_selectlist' || $attr[$rs->fields[1]]['tagname'] == 'widget_checkbox_multiple-horizontal' || $attr[$rs->fields[1]]['tagname'] == 'widget_checkbox_multiple-vertical' || $attr[$rs->fields[1]]['tagname'] == 'widget_radiobutton_single-vertical' || $attr[$rs->fields[1]]['tagname'] == 'widget_radiobutton_single-horizontal' )
					{
						$attr[$rs->fields[1]]['id'] = -2;
						$attr[$rs->fields[1]]['nodeName'] = 'SI_FORMS_ELEMENTATTRS';
					}
				}

				$rs->MoveNext();
			}

			if(!self::hasReachedLimitedLevels($path)){
				/** Creamos los nodos de los detalles de la forma */
				$aSQL = "SELECT B.section_id, C.id, C.sectionName, A.id, D.id FROM si_forms_formsdetails A, si_forms_views B, si_forms_sections C, si_forms_sections D WHERE A.section_id = {$id} AND A.view_id = B.id_view AND C.id_section = B.section_id AND D.id_section = A.section_id";
				$rs = $aConnection->Execute($aSQL);
				//echo "\n\n-- ===== Details =====\n\n".$aSQL.";";
				while($rs && !$rs->EOF) {
					
					$attr[$rs->fields[1]] = array( 'label' => encodeUTF8($rs->fields[2]),
						'id' => $rs->fields[0], 'nodeName' => $rs->fields[1], 
						'tagname' => '', 'attrId' => 'id_' . $rs->fields[4],
						'idJoin' => $rs->fields[3] . ':' . 'id_' . $rs->fields[4]
					);
					
					$rs->MoveNext();
				}

				//2015-03-02@HADR (#UBGNZX) Hfrias; No esta creando correctamante la vista: Detectamos si la forma padre es una forma extendida de usuarios para agregar un posfijo y evitar sobreescribir uno de los campos de la forma de hechos
				/** Creamos los nodos de los maestros de la forma */
				$aSQL = "SELECT C.id_section, C.id, C.sectionName, A.id, D.id, C.editType FROM si_forms_formsdetails A, si_forms_views B, si_forms_sections C, si_forms_sections D WHERE B.section_id = {$id} AND B.id_view = A.view_id AND C.id_section = A.section_id AND D.id_section = B.section_id";
				$rs = $aConnection->Execute($aSQL);
				//echo "\n\n-- ===== Masters =====\n\n".$aSQL.";";
				while($rs && !$rs->EOF) {
					
					$attr[$rs->fields[1].($rs->fields[5]==10?"_EXT":"")] = array( 'label' => encodeUTF8($rs->fields[2]),
						'id' => $rs->fields[0], 'nodeName' => $rs->fields[1], 
						'tagname' => '', 'attrId' => 'id_' . $rs->fields[4],
						'idJoin' => 'id_' . $rs->fields[1] . ':' .$rs->fields[3]
					);
					
					$rs->MoveNext();
				}
			}
		}

		$res = "<ul>";

		/** Ordenamos el array */
		uasort($attr, array('self', 'cmpAttrTree'));

		$i = 1;
		foreach ($attr as $eachAttr)
		{
			if( $eachAttr['nodeName'] )
			{
				$eachAttr['label'] = $i . '.' . $eachAttr['label'];
				$i++;
			}

			$type = 'string';
			if($eachAttr['tagname'] == 'widget_field_numeric')
				$type = 'numeric';

			$res .= self::createAttrNodeList($eachAttr['label'], $eachAttr['id'], $aConnection, $type, $path, $eachAttr['nodeName'], $eachAttr['attrId'], $pathLabel, @$eachAttr['idJoin']);
			
		}

		/*while($rs && !$rs->EOF)
		{
			$type = 'string';
			if($rs->fields[2] == 'widget_field_numeric')
				$type = 'numeric';

			$res .= createAttrNodeList(( $rs->fields[4] ? $rs->fields[0] . '@' . $rs->fields[6] : $rs->fields[0]), $rs->fields[4], $aConnection, $type, $path, $rs->fields[5]);
			$rs->MoveNext();
		}*/

		return $res .= "</ul>";
	}

	//2014-12-10@HADR (#5ZDBPS) Interfaz para definir un modelo de Artus desde formas de Ebavel
	//devuelve un true si se ha alcanzado el maximo de niveles permitidos para configurar un cubo
	public static function hasReachedLimitedLevels($path){
		return ( count(explode("->", $path)) > 3 );
	}

	public static function cmpAttrTree($a, $b) {
	   	
	   	$sA = $a['label'];
	   	$sB = $b['label'];

	   	if($a['nodeName']) {
	   		$sA = '/'.$sA;
	   	}

	   	if($b['nodeName']) {
	   		$sB = '/'.$sB;
	   	}

	    if($sA == $sB) {
	        return 0;
	    }

	    return ($sA > $sB) ? 1 : -1;
	}

}

?>