<?php

//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
    InitializeLocale($_SESSION["PAuserLanguageID"]);
    LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{   //English
    InitializeLocale(2);    
    LoadLanguageWithName("EN");
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Redirect</title>
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="browser_error.css">
</head>
<body>
	<div class="div_main">
		<div class="div_center">
			<div class="spacer" style="height: 100px;"></div>
			<div class="div_image_warning">
				<div class="spacer" style="height: 50px"></div>
				<div class="div_image_message">
					<span>
						<p><?=translate("Sorry for the inconvenience")?>.</p><br><br>
						<p><?=translate("The browser you're using isn't supported, we suggest using Google Chrome")?>.<p>
					</span>
				</div>
				<div class="div_browserLogo_container">
					<div id="browser_Safari">
						<div class="div_warning shown"></div>
					</div>
					<div id="browser_Firefox">
						<div class="div_warning shown"></div>
					</div>
					<div id="browser_Chrome">
						<div class="div_warning hidden"></div>
					</div>
					<div id="browser_IE">
						<div class="div_warning shown"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>