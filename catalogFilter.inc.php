<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("catalog.inc.php");
require_once("filtersv.inc.php");

class BITAMCatalogFilter extends BITAMObject
{
	public $FilterID;
	public $CatalogID;
	public $FilterName;
	public $FilterText;
	public $FilterLevels;
	
	public $AttribFields;
	public $AttribNames;
	public $AttribIDs;
	public $AttribPositions;
	public $AttribMemberIDs;
	public $NumAttribFields;
	public $TableName;
	public $ModelID;
	//Se utilizaron estas variables para reutilizar el codigo de los filtros de report.inc.php
	//Se obtiene el catalogID del filtro que se esta utilizando y el CatMemberID siempre sera 
	//el ultimo elemento de la jerarquia del catalogo
	public $CatalogDimID;
	public $CatMemberID;
	
	public $UserIDs;
	public $UsrRoles;
	public $StrUserRol;
	//@JAPR 2014-05-16: Agregados los catálogos tipo eBavel (estas propiedades sólo se asignan después de un getCatalogValues de catálogos de eBavel
	public $FormID;
	//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
	public $RSFields;			//Array con los campos que se regresarán (cuando se pide un recordset) en formato [DSC_ClaDescrip] => FieldID de EBavel
	//@JAPR
			
	//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
	//Agregado el parámetro $bNullFilters para inicializar las propiedades que representan los valores filtrados con un valor null en lugar de hacerlo
	//con cadena vacia (esto es especialmente útil cuando se está aplicando filtros de eBavel, ya que ahi se hará diferencia entre el valor null o
	//vacio, porque para obtener las categorías basadas en formas de eBavel se reutilizó esta instancia, pero hay valores de eBavel que son vacios
	//y estaba prefiltrando por esos atributos padre en lugar de traer todas las categorías del atributo especificado)
	//@JAPR 2014-08-21: Agregado el parámetro $bSetAllValue para indicar cuando se está en la ventana de filtros y se está cargando
	//uno vacio, de forma que en eBavel no se intenten obtener valores en ese caso porque algunos campos no soportan recibir vacio
	//como valor, y realmente no tiene sentido hacer una consulta que esperamos que falle (esto NO se debería usar si se espera
	//recibir un RecordSet, porque lo que se va a hacer es un simple return de null en caso de filtrar por All)
	function __construct($aRepository, $aCatalogID, $bNullFilters = false, $bSetAllValue = false)
	{
		BITAMObject::__construct($aRepository);
		$this->FilterID = -1;
		$this->CatalogID = $aCatalogID;
		$this->FilterName = "";
		
		$this->AttribFields = array();
		$this->AttribNames = array();
		$this->AttribIDs = array();
		$this->AttribPositions = array();
		$this->AttribMemberIDs = array();
		$this->NumAttribFields = 0;
		$this->CatMemberID = -1;
		//@JAPR 2014-05-16: Agregados los catálogos tipo eBavel
		$this->FormID = '';
		//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
		$this->RSFields = array();
		
		//Como las dimensiones no estan ligadas a un cubo entonces el ModelID tendra valor de -1
		$this->ModelID = -1;
		
		$anInstanceCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $this->CatalogID);
		$this->TableName = $anInstanceCatalog->TableName;
		$this->CatalogDimID = $anInstanceCatalog->ParentID;
		
		$attributes = BITAMCatalogMemberCollection::NewInstance($aRepository, $this->CatalogID);
		$numFields = count($attributes->Collection);

		for($i=0; $i<$numFields; $i++)
		{
			$fieldName = 'DSC_'.$attributes->Collection[$i]->ClaDescrip;
			$fieldNameSTR = $fieldName."STR";
			$this->AttribFields[$i] = 'DSC_'.$attributes->Collection[$i]->ClaDescrip;
			$this->AttribIDs[$i] = $attributes->Collection[$i]->ClaDescrip;
			$this->AttribNames[$i] = $attributes->Collection[$i]->MemberName;
			$this->AttribMemberIDs[$fieldName] = $attributes->Collection[$i]->MemberID;
			$this->AttribPositions[$fieldName] = $i;
			$this->CatMemberID = $attributes->Collection[$i]->MemberID;
			
			//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
			//Agregado el parámetro $bNullFilters para inicializar las propiedades que representan los valores filtrados con un valor null en lugar de hacerlo
			//con cadena vacia
			if ($bNullFilters) {
				$this->$fieldName = null;
				$this->$fieldNameSTR = null;
			}
			else {
				//@JAPR 2014-08-21: Agregado el parámetro $bSetAllValue para indicar cuando se está en la ventana de filtros y se está cargando
				//uno vacio, de forma que en eBavel no se intenten obtener valores en ese caso porque algunos campos no soportan recibir vacio
				//como valor, y realmente no tiene sentido hacer una consulta que esperamos que falle (esto NO se debería usar si se espera
				//recibir un RecordSet, porque lo que se va a hacer es un simple return de null en caso de filtrar por All)
				if ($anInstanceCatalog->eBavelAppID > 0 && $anInstanceCatalog->eBavelFormID > 0 && $bSetAllValue) {
					$this->$fieldName = "sv_ALL_sv";
					$this->$fieldNameSTR = "All";
				}
				else {
					$this->$fieldName = "";
					$this->$fieldNameSTR = "";
				}
			}
			//@JAPR
		}
		
		$this->NumAttribFields = $numFields;
		
		$this->UserIDs = array();
		$this->UsrRoles = array();
		$this->StrUserRol = "";
		
		//@JAPR 2014-05-30: Agregados los catálogos tipo eBavel
		if ($anInstanceCatalog->eBavelAppID > 0 && $anInstanceCatalog->eBavelFormID > 0) {
            /*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel*/
      		if (getMDVersion() >= esvRedesign) {
	            //Si hemos seleccionado una Forma de eBavel entonces pedimos sus atributos.
	            if($anInstanceCatalog->eBavelFormType == ebftForms)
	                $arreBavelFormColl = @GetEBavelSections($aRepository, false, $anInstanceCatalog->eBavelAppID, array($anInstanceCatalog->eBavelFormID));        
	            else//Si hemos seleccionado una Vista de eBavel entonces pedimos sus atributos.
	                $arreBavelFormColl = @GetEBavelViews($aRepository, false, $anInstanceCatalog->eBavelAppID, array($anInstanceCatalog->eBavelFormID));
      		}
      		else //Se trata de un catálogo extraído de eBavel, por lo que debe asignar el FormID
				$arreBavelFormColl = @GetEBavelSections($aRepository, false, $anInstanceCatalog->eBavelAppID, array($anInstanceCatalog->eBavelFormID));
      		//@AAL

			//Se trata de un catálogo extraído de eBavel, por lo que debe asignar el FormID
			$arreBavelFormColl = @GetEBavelSections($aRepository, false, $anInstanceCatalog->eBavelAppID, array($anInstanceCatalog->eBavelFormID));
			if (!is_null($arreBavelFormColl) && is_array($arreBavelFormColl) && count($arreBavelFormColl) > 0) {
				$objForm = @$arreBavelFormColl[0];
				$strFormID = (string) @$objForm['id'];
				$this->FormID = $strFormID;
			}
		}
		//@JAPR
	}

	//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
	//Agregado el parámetro $bNullFilters para inicializar las propiedades que representan los valores filtrados con un valor null en lugar de hacerlo
	//con cadena vacia
	//@JAPR 2014-08-21: Agregado el parámetro $bSetAllValue para indicar cuando se está en la ventana de filtros y se está cargando
	//uno vacio, de forma que en eBavel no se intenten obtener valores en ese caso porque algunos campos no soportan recibir vacio
	//como valor, y realmente no tiene sentido hacer una consulta que esperamos que falle (esto NO se debería usar si se espera
	//recibir un RecordSet, porque lo que se va a hacer es un simple return de null en caso de filtrar por All)
	static function NewInstance($aRepository, $aCatalogID, $bNullFilters = false, $bSetAllValue = false)
	{
		return new BITAMCatalogFilter($aRepository, $aCatalogID, $bNullFilters, $bSetAllValue);
	}

	static function NewInstanceWithID($aRepository, $aFilterID)
	{
		$anInstance = null;
		
		if (((int) $aFilterID) <= 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT CatalogID, FilterID, FilterName, FilterText, FilterLevels FROM SI_SV_CatFilter WHERE FilterID = ".$aFilterID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilter ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$anInstance = BITAMCatalogFilter::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aCatalogID = $aRS->fields["catalogid"];
		$anInstance = BITAMCatalogFilter::NewInstance($aRepository, $aCatalogID);
		$anInstance->FilterID = $aRS->fields["filterid"];
		$anInstance->FilterName = $aRS->fields["filtername"];
		
		$filterText = is_null($aRS->fields["filtertext"])?"":$aRS->fields["filtertext"];
		$filterLevels = is_null($aRS->fields["filterlevels"])?"":$aRS->fields["filterlevels"];
		
		$anInstance->FilterText = $filterText;
		$anInstance->FilterLevels = $filterLevels;
		
		$filterCollection = BITAMFilteredSVCollection::NewInstanceByStrFilter($aRepository, $filterText, $filterLevels, $anInstance->ModelID);
		
		//Todos los elementos de los atributos del catalogo los ponemos por 
		//default con el valor de ALL
		for($i=0; $i<$anInstance->NumAttribFields; $i++)
		{
			$fieldName = $anInstance->AttribFields[$i];
			$fieldNameSTR = $fieldName."STR";
			$anInstance->$fieldName = "sv_ALL_sv";
			$anInstance->$fieldNameSTR = "All";
		}

		//Llenamos los elementos con los valores obtenidos del filtro
		foreach ($filterCollection as $element)
		{
			$fieldName = $element->nom_fisicok;
			$fieldNameSTR = $fieldName."STR";
			$fieldValue = $element->values[0];
			$anInstance->$fieldName = $fieldValue;
			$anInstance->$fieldNameSTR = $fieldValue;
			
			if($fieldValue=="sv_ALL_sv")
			{
				$anInstance->$fieldNameSTR = "All";
			}
		}
		
		//Obtener los usuarios a los cuales se les permite acceder a este filtro
		$anInstance->readUserIDs();
		
		//Obtener los roles a los cuales se les permite acceder a este filtro
		$anInstance->readUsrRoles();

		//Obtiene la cadena que concatena los usuarios y los roles
		$anInstance->readStrUserRol();

		return $anInstance;
	}
	
	function readUserIDs()
	{
		$this->UserIDs = array();
		$sql = "SELECT UserID FROM SI_SV_CatFilterUser WHERE FilterID = ".$this->FilterID;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$this->UserIDs[] = (int)$aRS->fields["userid"];
			$aRS->MoveNext();
		}
	}

	function readUsrRoles()
	{
		$this->UsrRoles = array();
		$sql = "SELECT RolID FROM SI_SV_CatFilterRol WHERE FilterID = ".$this->FilterID;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$this->UsrRoles[] = (int)$aRS->fields["rolid"];
			$aRS->MoveNext();
		}
	}

	function readStrUserRol()
	{
		//Juntamos usuarios y roles en una sola cadena
		$strUsers = "";
		$strRoles = "";
		
		$UserIDs = array();
		$UsrRoles = array();
		
		foreach ($this->UserIDs as $element)
		{
			$UserIDs[] = 'User_'.$element;
		}
		
		foreach ($this->UsrRoles as $element)
		{
			$UsrRoles[] = 'Rol_'.$element;
		}
		
		if(count($UserIDs)>0)
		{
			$strUsers = implode("_AWElem_", $UserIDs);
		}
		
		if(count($UsrRoles)>0)
		{
			$strRoles = implode("_AWElem_", $UsrRoles);
		}
		
		$this->StrUserRol = $strUsers;
		
		if(trim($this->StrUserRol)!="")
		{
			if(trim($strRoles)!="")
			{
				$this->StrUserRol.="_AWElem_";
			}
		}
		
		$this->StrUserRol.=$strRoles;
	}

	static function NewInstanceFromRSCollection($aRepository, $aRS)
	{
		$aCatalogID = $aRS["catalogid"];
		$anInstance = BITAMCatalogFilter::NewInstance($aRepository, $aCatalogID);
		$anInstance->FilterID = $aRS["filterid"];
		$anInstance->FilterName = $aRS["filtername"];
		
		$filterText = is_null($aRS["filtertext"])?"":$aRS["filtertext"];
		$filterLevels = is_null($aRS["filterlevels"])?"":$aRS["filterlevels"];
		
		$anInstance->FilterText = $filterText;
		$anInstance->FilterLevels = $filterLevels;

		for ($i=0; $i<$anInstance->NumAttribFields; $i++)
		{
			$fieldName = $anInstance->AttribFields[$i];
			$fieldNameSTR = $fieldName."STR";
			
			$anInstance->$fieldName = $aRS[strtolower($fieldName)];
			$anInstance->$fieldNameSTR = $aRS[strtolower($fieldName)];
			
			if($aRS[strtolower($fieldName)]=="sv_ALL_sv")
			{
				$anInstance->$fieldNameSTR = "All";
			}
		}
		
		//Obtener los usuarios a los cuales se les permite acceder a este filtro
		$anInstance->readUserIDs();
		
		//Obtener los roles a los cuales se les permite acceder a este filtro
		$anInstance->readUsrRoles();

		//Obtiene la cadena que concatena los usuarios y los roles
		$anInstance->readStrUserRol();

		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("CatalogID", $aHTTPRequest->GET))
		{
			$aCatalogID = (int) $aHTTPRequest->GET["CatalogID"];
		}
		else
		{
			$aCatalogID = 0;
		}

		if (array_key_exists("FilterID", $aHTTPRequest->POST))
		{
			$aFilterID = $aHTTPRequest->POST["FilterID"];
			
			if (is_array($aFilterID))
			{
				$aCollection = BITAMCatalogFilterCollection::NewInstance($aRepository, $aCatalogID, $aFilterID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}

				$anInstance = BITAMCatalogFilter::NewInstance($aRepository, $aCatalogID);

				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				$anInstance = BITAMCatalogFilter::NewInstanceWithID($aRepository, (int)$aFilterID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMCatalogFilter::NewInstance($aRepository, $aCatalogID);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();

				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}

				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMCatalogFilter::NewInstance($aRepository, $aCatalogID);
					}
				}

				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("FilterID", $aHTTPRequest->GET))
		{
			$aFilterID = $aHTTPRequest->GET["FilterID"];
			$anInstance = BITAMCatalogFilter::NewInstanceWithID($aRepository, (int)$aFilterID);

			if (is_null($anInstance))
			{
				$anInstance = BITAMCatalogFilter::NewInstance($aRepository, $aCatalogID);
			}
		}
		else
		{
			//@JAPR 2014-08-21: Agregado el parámetro $bSetAllValue para indicar cuando se está en la ventana de filtros y se está cargando
			//uno vacio, de forma que en eBavel no se intenten obtener valores en ese caso porque algunos campos no soportan recibir vacio
			//como valor, y realmente no tiene sentido hacer una consulta que esperamos que falle (esto NO se debería usar si se espera
			//recibir un RecordSet, porque lo que se va a hacer es un simple return de null en caso de filtrar por All)
			$anInstance = BITAMCatalogFilter::NewInstance($aRepository, $aCatalogID, false, true);
		}

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("FilterID", $anArray))
		{
			$this->FilterID = (int)$anArray["FilterID"];
		}
		
		if (array_key_exists("FilterName", $anArray))
		{
			$this->FilterName = rtrim($anArray["FilterName"]);
		}

		for ($i=0; $i<$this->NumAttribFields; $i++)
		{
			$fieldName = $this->AttribFields[$i];

			if (array_key_exists($fieldName, $anArray))
			{
				$this->$fieldName = trim($anArray[$fieldName]);
			}
		}
		
	 	if (array_key_exists("StrUserRol", $anArray))
		{
			$this->StrUserRol = $anArray["StrUserRol"];
		}

		return $this;
	}
	
	function getFilterTextAndLevels()
	{
		$arrayFilter = array();
		$arrayFilter["FilterText"] = "";
		$arrayFilter["FilterLevels"] = "";
		
		$strFilterText = "";
		$strFilterLevels = "";
		for ($i=0; $i<$this->NumAttribFields; $i++)
		{
			$attribID = $this->AttribIDs[$i];
			$fieldName = $this->AttribFields[$i];
			$fieldValue = $this->$fieldName;
			
			if($fieldValue=="sv_ALL_sv")
			{
				continue;
			}
			
			if($strFilterText!="")
			{
				$strFilterText.=" AND ";
			}
			
			if($strFilterLevels!="")
			{
				$strFilterLevels.="|";
			}

			$strFilterText.="[".$this->TableName."].[".$fieldName."_".$attribID."].[".$fieldValue."]";
			$strFilterLevels.=$attribID;
		}
		
		$arrayFilter["FilterText"] = $strFilterText;
		$arrayFilter["FilterLevels"] = $strFilterLevels;
		
		return $arrayFilter;
	}

	function save()
	{
	 	if ($this->isNewObject())
		{
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(FilterID)", "0")." + 1 AS FilterID".
						" FROM SI_SV_CatFilter";

			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$this->FilterID = (int)$aRS->fields["filterid"];
			
			//Obtener el FilterText y el FilterLevels capturado
			$arrayFilter = $this->getFilterTextAndLevels();
			$this->FilterText = $arrayFilter["FilterText"];
			$this->FilterLevels = $arrayFilter["FilterLevels"];

			$sql = "INSERT INTO SI_SV_CatFilter (".
						"CatalogID".
						",FilterID".
						",FilterName".
						",FilterText".
						",FilterLevels".
			            ") VALUES (".
						$this->CatalogID.
						",".$this->FilterID.
						",".$this->Repository->DataADOConnection->Quote($this->FilterName).
						",".$this->Repository->DataADOConnection->Quote($this->FilterText).
						",".$this->Repository->DataADOConnection->Quote($this->FilterLevels).
						")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Insertamos/Actualizamos usuarios y roles para el filtro capturado
			$this->UpdateUserIDsFromFilter();
		}
		else
		{
			//Obtener el FilterText y el FilterLevels capturado
			$arrayFilter = $this->getFilterTextAndLevels();
			$this->FilterText = $arrayFilter["FilterText"];
			$this->FilterLevels = $arrayFilter["FilterLevels"];

			$sql = "UPDATE SI_SV_CatFilter SET ".
					" FilterName = ".$this->Repository->DataADOConnection->Quote($this->FilterName).
					", FilterText = ".$this->Repository->DataADOConnection->Quote($this->FilterText).
					", FilterLevels = ".$this->Repository->DataADOConnection->Quote($this->FilterLevels).
					" WHERE FilterID = ".$this->FilterID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Insertamos/Actualizamos usuarios y roles para el filtro capturado
			$this->UpdateUserIDsFromFilter();
		}
		
		//@JAPR 2013-02-21: Corregido un bug, no estaba incrementando la versión del catálogo al configurar filtros (#28327)
		$aCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		if (!is_null($aCatalog))
		{
			$aCatalog->save();
		}
		//@JAPR
		
		return $this;
	}
	
	function UpdateUserIDsFromFilter()
	{
		//Elimino los registros anteriores y se vuelven a insertar nuevamente:
		$sql = "DELETE FROM SI_SV_CatFilterUser WHERE FilterID = ".$this->FilterID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$this->UserIDs = array();

		//Elimino los registros anteriores y se vuelven a insertar nuevamente:
		$sql = "DELETE FROM SI_SV_CatFilterRol WHERE FilterID = ".$this->FilterID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$this->UsrRoles = array();
		
		$arrayElements = explode("_AWElem_", $this->StrUserRol);
		
		foreach($arrayElements as $anElement)
		{
			$arrInfo = explode("_", $anElement);
			if(substr($anElement, 0, 5)=="User_")
			{
				$this->UserIDs[] = (int)$arrInfo[1];
			}
			else
			{
				$this->UsrRoles[] = (int)$arrInfo[1];
			}
		}
		
		$numUsers = count($this->UserIDs);

		for($i=0; $i<$numUsers; $i++)
		{
			$sqlIns = "INSERT INTO SI_SV_CatFilterUser (FilterID, UserID) 
						VALUES (".$this->FilterID.",".$this->UserIDs[$i].")";

			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
		}
		
		$numRoles = count($this->UsrRoles);
		
		for($i=0; $i<$numRoles; $i++)
		{
			$sqlIns = "INSERT INTO SI_SV_CatFilterRol (FilterID, RolID) 
						VALUES (".$this->FilterID.",".$this->UsrRoles[$i].")";

			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
		}
	}

	function remove()
	{	
		$sql = "DELETE FROM SI_SV_CatFilterUser WHERE FilterID = ".$this->FilterID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$sql = "DELETE FROM SI_SV_CatFilterRol WHERE FilterID = ".$this->FilterID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$sql = "DELETE FROM SI_SV_CatFilter WHERE FilterID = ".$this->FilterID;

		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2013-02-21: Corregido un bug, no estaba incrementando la versión del catálogo al configurar filtros (#28327)
		$aCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		if (!is_null($aCatalog))
		{
			$aCatalog->save();
		}
		//@JAPR
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->FilterID <= 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Filter");
		}
		else
		{
			return $this->FilterName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=CatalogFilter&CatalogID=".$this->CatalogID;
		}
		else
		{
			return "BITAM_PAGE=CatalogFilter&CatalogID=".$this->CatalogID."&FilterID=".$this->FilterID;
		}
	}

	function get_Parent()
	{
		return BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return "FilterID";
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$userIDsField = BITAMFormField::NewFormField();
		$userIDsField->Name = "StrUserRol";
		$userIDsField->Title = translate("Users");
		$userIDsField->Type = "LargeString";
		$userIDsField->Size = 1000;
		$userIDsField->IsVisible = false;
		$tmplabel="Change";
		if($this->isNewObject())
		{
			$tmplabel="Add";
		}
		$userIDsField->AfterMessage = "<span id=\"changeUserIDs\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);backgroun-position:center center;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssignUsers();\">".translate($tmplabel)."</span>";
		$myFields[$userIDsField->Name] = $userIDsField;


		for ($i=0; $i<$this->NumAttribFields; $i++)
		{
			$fieldName = $this->AttribFields[$i];
			$labelText = $this->AttribNames[$i];

			$aField = BITAMFormField::NewFormField();
			$aField->Name = $fieldName;
			$aField->Title = $labelText;
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $this->getCatalogValues($i);
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	function generateBeforeFormCode($aUser)
	{
?>
	<script language="javascript" src="js/dialogs.js"></script>
	<script language="JavaScript">
		var filterAttribIDs = new Array;
		var filterAttribNames = new Array;
		var filterAttribPos = new Array;
		var filterAttribMembers = new Array;
		var filterAttribMemberFields = new Array;
<?
		foreach ($this->AttribIDs as $position => $attribID)
		{
			$attribName = $this->AttribFields[$position];
			$attribMemberID = $this->AttribMemberIDs[$attribName]
?>
		filterAttribIDs[<?=$position?>]=<?=$attribID?>;
		filterAttribNames[<?=$position?>]='<?=$attribName?>';
		filterAttribPos['<?=$attribName?>']=<?=$position?>;
		filterAttribMembers['<?=$attribName?>']=<?=$attribMemberID?>;
		filterAttribMemberFields[<?=$attribMemberID?>]='<?=$attribName?>';
<?
		}
?>
		var numFilterAttributes = <?=$this->NumAttribFields?>;
		
<?
		if(count($this->AttribFields)>0)
		{
?>
		function refreshAttributes(attribName, event)
		{
			var event = (window.event) ? window.event : event;
			
			objAttrib = document.getElementsByName(attribName)[0];
			
			if(objAttrib.value=='sv_ALL_sv')
			{
				for(i=0; i<numFilterAttributes; i++)
				{
					var thisPosition = filterAttribPos[attribName];
					var searchAttrib = 'DSC_' + filterAttribIDs[i];
					var position = filterAttribPos[searchAttrib];
					
					if(position > thisPosition)
					{
						var obj = document.getElementsByName(searchAttrib)[0];
						obj.length = 0;
						var newOption = new Option('<?=translate("All")?>', 'sv_ALL_sv');
						obj.options[obj.options.length] = newOption;
					}
				}
				return;
			}
			
			var xmlObj = getXMLObject();
			
			if (xmlObj == null)
			{
				alert("XML Object not supported");
				return(new Array);
			}
			
			strMemberIDs="";
			strParentIDs="";
			strValues="";
			
			for(i=0; i<numFilterAttributes; i++)
			{
				var thisPosition = filterAttribPos[attribName];
				var searchAttrib = 'DSC_' + filterAttribIDs[i];
				var position = filterAttribPos[searchAttrib];
				var obj = document.getElementsByName(searchAttrib)[0];
				
				if(position <= thisPosition)
				{
					if(strMemberIDs!="")
					{
						strMemberIDs+="_SVSEPSV_";
					}
					strMemberIDs+=filterAttribMembers[searchAttrib];
					
					if(strParentIDs!="")
					{
						strParentIDs+="_SVSEPSV_";
					}
					strParentIDs+=filterAttribIDs[position];
					
					if(strValues!="")
					{
						strValues+="_SVSEPSV_";
					}
					
					strValues+=obj.value;
				}
				else
				{
					break;
				}
			}
			
		 	xmlObj.open("POST", "getAttribValues.php?CatalogID=<?=$this->CatalogID?>&CatMemberID=<?=$this->CatMemberID?>&ChangedMemberID="+filterAttribMembers[attribName]+"&strMemberIDs="+strMemberIDs+"&strParentIDs="+strParentIDs+"&strValues="+strValues, false);
		
		 	xmlObj.send(null);
		 	
		 	strResult = Trim(unescape(xmlObj.responseText));
			
		 	arrayAttrib = strResult.split("_SV_AT_SV_");
		 	var numAttrib = arrayAttrib.length;
		 	var refreshAttrib = new Array();
		 	
		 	for (var i=0; i<numAttrib; i++)
		 	{
		 		if(arrayAttrib[i]!='')
		 		{
			 		var atribInfo = arrayAttrib[i].split("_SV_ID_SV_");
			 		var membID = atribInfo[0];
			 		var attribValues = atribInfo[1].split("_SV_VAL_SV_");
			 		var numAtVal = attribValues.length;
					
			 		var obj = document.getElementsByName(filterAttribMemberFields[membID])[0];
			 		
			 		obj.length = 0;
			 		
					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;
					
			 		for(j=0; j<numAtVal; j++)
			 		{
						var newOption = new Option(attribValues[j], attribValues[j]);
						obj.options[obj.options.length] = newOption;
			 		}
			 		
			 		refreshAttrib[i]=membID;
		 		}
		 	}
			
		 	//Asignarle sólo la opción All a los combos que tienen orden de jerarquía mayor al orden del combo cambiado
		 	// y que no sea el combo al que se le acaban de reasignar valores en el ciclo anterior
			for(i=0; i<numFilterAttributes; i++)
			{
				var searchAttrib = 'DSC_' + filterAttribIDs[i];
				var obj = document.getElementsByName(searchAttrib)[0];
				
				if(filterAttribPos[searchAttrib] > filterAttribPos[attribName] && !inArray(refreshAttrib, filterAttribMembers[searchAttrib]))
				{
			 		obj.length = 0;
			 		 
					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;
				}
			}
		}
<?
		}
?>
		
		function openAssignUsers()
		{
			userIDsObj = BITAMCatalogFilter_SaveForm.StrUserRol;
			//window.showModalDialog('assignCatFilterUsers.php?FilterID=<?=$this->FilterID?>', [window], "dialogHeight:320px; dialogWidth:600px; center:yes; help:no; resizable:yes; status:no");
			openWindowDialog('assignCatFilterUsers.php?FilterID=<?=$this->FilterID?>', [window], [], 600, 320, openAssignUsersDone, 'openAssignUsersDone');
			/*
			var strInfo = window.returnValue;
			
			//Actualiza la cadena de los menus visibles
			if(strInfo!=null && strInfo!=undefined)
			{
				userIDsObj.value = strInfo;
			}
			*/
		}
		
		function openAssignUsersDone(sValue, sParams)
		{
			var strInfo = sValue;
			
			//Actualiza la cadena de los menus visibles
			if(strInfo!=null && strInfo!=undefined)
			{
				userIDsObj.value = strInfo;
			}
		}
	</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
?>
	 	<script language="JavaScript">
<?
		if($this->NumAttribFields>0)
		{	
			foreach($this->AttribFields as $position => $attribName)
			{
?>
		if (document.getElementsByName('<?=$attribName?>')[0].addEventListener)
		{
			document.getElementsByName('<?=$attribName?>')[0].addEventListener("change", function(evt){ refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name, evt); }, true);
		}
		else 
		{
		    document.getElementsByName('<?=$attribName?>')[0].attachEvent("onchange", function () { refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name); })
		}
<?
			}
		}
?>
	</script>
<?
	}

	function insideEdit()
	{
?>
		objSpanChange = document.getElementById("changeUserIDs");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground.gif)';
		objSpanChange.style.cursor = 'pointer';
		objSpanChange.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChange.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChange.onclick = new Function("openAssignUsers();");
<?
	}

	function insideCancel()
	{
?>
		objSpanChange = document.getElementById("changeUserIDs");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground_off.gif)';
		objSpanChange.style.cursor = 'default';
		objSpanChange.onmouseover = "";
		objSpanChange.onmouseout = "";
		objSpanChange.onclick = "";
<?
	}

	//Carga la lista de valores del atributo especificado y regresa un array conteniendo sólo los diferentes valores
	//El parámetro $bGetRecordset permite que se regrese sólo el RecordSet en lugar de la colección de valores
	//El parámetro $bIncludeSystemFields permite recuperar también los campos de sistema como la fecha o el key de cada valor (sólo aplica si $bGetRecordset == true)
	//El parámetro $bFilterAttribPosition indica si se desea que también se aplique el filtro por el atributo en la posición $attribPosition (esto es
	//útil sólo si se quiere recuperar el id_FormId, ya que se traerá exclusivamente el valor con todos los filtros, en lugar de todos los valores
	//de dicho atributo filtrados por los padres, que era el uso original de la función para la ventana de filtros)
	//@JAPR 2014-05-30: Agregado el tipo de sección Inline
	//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar (y por tanto agrupar), pero sólo se debe utilizar
	//si este método se usará para extraer un RecordSet, ya que de lo contrario se asume que entre otras cosas se puede estar usando para la ventana
	//de filtros donde originalmente se diseñó, así que en esos casos no debe regresar mas de un campo pues para empezar el resultado de la función
	//es sólo un array de valores, no un array de campos con sus respectivos valores
	//El parámetro $sWhere permite especificar un filtro directo en lugar de armarlo mediante las propiedades de la instancia
	function getCatalogValues($attribPosition, $bGetRecordset = false, $bIncludeSystemFields = false, $bFilterAttribPosition = false, $aFieldIDs = null, $sWhere = '')
	{
		global $gblShowErrorSurvey;
		
		//@JAPR 2014-05-14: Agregados los catálogos tipo eBavel
		$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		//@JAPR 2014-05-30: Agregado el tipo de sección Inline
		//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar
		//Valida que sólo se reciban IDs de campos si se planea regresar el recordset
		if (is_null($aFieldIDs) || !is_array($aFieldIDs)) {
			$aFieldIDs = array();
		}
		if (count($aFieldIDs) > 0 && !$bGetRecordset) {
			$aFieldIDs = array();
		}
		$aFieldIDs = array_flip($aFieldIDs);
		//@JAPR
		$arrayValues = array();
		$arrayValues["sv_ALL_sv"] = translate("All");
		
		$fieldName = $this->AttribFields[$attribPosition];
		$fieldValue = $this->$fieldName;
		
		$beforeFieldName = "";
		$beforeFieldValue = "";
		
		if($attribPosition>0)
		{
			$beforeFieldName = $this->AttribFields[$attribPosition-1];
			$beforeFieldValue = $this->$beforeFieldName;
		}
		
		if($attribPosition==0 || ($attribPosition>0 && $beforeFieldValue!="sv_ALL_sv"))
		{
			//@JAPR 2014-05-14: Agregados los catálogos tipo eBavel
			if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
                /*@AAL 19/03/2015: Modificado para soportar Formas y vistas de eBavel*/
	      		if (getMDVersion() >= esvRedesign) {
	                //Si hemos seleccionado una Forma entonces pedimos sus atributos
	                if($objCatalog ->eBavelFormType == ebftForms){
	                    //Carga la definición de la forma de eBavel a utilizar
	                    $arreBavelFormColl = @GetEBavelSections($this->Repository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
	                    if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0)
	                		return $arrayValues;
	                    $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Forms');        
	                }else{//Si hemos seleccionado una Vista entonces solicitamos sus atributos.
	                    //Carga la definición de la forma de eBavel a utilizar
	                    $arreBavelFormColl = @GetEBavelViews($this->Repository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
	                    if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0)
	                    	return $arrayValues;
	                    $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Views');
	                }
	      		}
	      		else{//Se trata de un catálogo extraído de eBavel, así que obtiene todos los valores filtrando por los padres usando las clases de eBavel
					$arreBavelFormColl = @GetEBavelSections($this->Repository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
					if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0)
						return $arrayValues;
					$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');
	      		}

				$objForm = $arreBavelFormColl[0];
				$strFormID = $objForm['id'];
				//@JAPR 2014-05-16: Agregados los catálogos tipo eBavel
				$this->FormID = $strFormID;
				//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
				$this->RSFields = array();
				
				$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
				$intFieldIDToGet = 0;
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				$intFieldDataIDToGet = ebfdValue;
				//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
				$strFieldDescToGet = '';
				$arreBavelFieldIDsStr = array();	//array('id_'.$strFormID);
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				//Este array sólo contiene la indicación de que campos de eBavel fueron utilizados, no importa si Geolocation está o no unificado
				$arreBavelFieldIDs = array();
				//Este aray contiene los valores de cada campo, en el caso de Geolocation se tiene que manejar unificado, pero lo hará en 2
				//partes concatenando según como lleguen los valores
				$arreBavelFieldIDsValue = array();
				//Este array realmente no se utilizó
				$arreBavelFieldIDsName = array();
				//Este array realmente no se utilizó
				$arreBavelFieldIDsNameID = array();
				//Este array contiene los nombres de campos genéricos basados en CLA_DESCRIP que corresponden con los campos pedidos a eBavel,
				//en el caso de los de Geolocation, sólo uno de los dos atributos se agregaría al array, así que se tendría que procesar el
				//correcto al momento de resolver el query
				$arrFieldDescToGet = array();
				foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
					if ($objCatalogMember->MemberOrder > $attribPosition+1) {
						break;
					}
					
					//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
					$intEBavelFieldID = $objCatalogMember->eBavelFieldID;
					if ($intEBavelFieldID > 0) {
						$arreBavelFieldIDs[$intEBavelFieldID] = $intEBavelFieldID;
						$tempFieldName = $this->AttribFields[$objCatalogMember->MemberOrder -1];
						$tempFieldValue = $this->$tempFieldName;
						//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						//Este aray contiene los valores de cada campo, en el caso de Geolocation se tiene que manejar unificado, pero lo hará en 2
						//partes concatenando según como lleguen los valores
						//@JAPR 2014-11-14: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						//Los campos Geolocation no se pueden usar a la fecha de implementación directamente con la función where, así que se
						//preparará un whereRaw para agregarlo adicional a los where normales que pudieran existir
						$strSuffix = '';
						switch ($objCatalogMember->eBavelFieldDataID) {
							case ebfdLatitude:
								/*
								//Si es la latitude y ya había un valor previo, sólo puede significar que era la longitude, así que concatena al inicio
								if (!isset($arreBavelFieldIDsValue[$intEBavelFieldID]) || trim($arreBavelFieldIDsValue[$intEBavelFieldID]) == '') {
									$arreBavelFieldIDsValue[$intEBavelFieldID] = $tempFieldValue;
								}
								else {
									$arreBavelFieldIDsValue[$intEBavelFieldID] = $tempFieldValue.','.$arreBavelFieldIDsValue[$intEBavelFieldID];
								}
								*/
								$strSuffix = '_L';
								break;
								
							case ebfdLongitude:
								/*
								//Si es la longitude y ya había un valor previo, sólo puede significar que era la latitude, así que concatena al final
								if (!isset($arreBavelFieldIDsValue[$intEBavelFieldID]) || trim($arreBavelFieldIDsValue[$intEBavelFieldID]) == '') {
									$arreBavelFieldIDsValue[$intEBavelFieldID] = $tempFieldValue;
								}
								else {
									$arreBavelFieldIDsValue[$intEBavelFieldID] .= ','.$arreBavelFieldIDsValue[$intEBavelFieldID];
								}
								*/
								$strSuffix = '_A';
								break;
							/*
							case ebfdValue:
							default:
								$arreBavelFieldIDsValue[$intEBavelFieldID] = $tempFieldValue;
								break;
							*/
						}
						
						//Tiene que concatenar el sufijo por si era un campo Geolocation que se tiene que pedir por medio de sus diferentes
						//componentes, de lo contrario no es necesario el sufijo
						$strEBavelFieldID = $intEBavelFieldID;
						if ($strSuffix) {
							$strEBavelFieldID .= $strSuffix;
						}
						$arreBavelFieldIDsValue[$strEBavelFieldID] = $tempFieldValue;
						//@JAPR
						
						$arreBavelFieldIDsName[$intEBavelFieldID] = $tempFieldName;
						$arreBavelFieldIDsNameID[$intEBavelFieldID] = '';
						//@JAPR 2014-05-30: Agregado el tipo de sección Inline
						//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar
						//Se agregarán todos los campos que se encuentren en el array especificado + el pedido por posición, por lo que siempre se
						//asume que el pedido por posición tiene que ser como mínimo el último en el orden de los especificados en el array
						if ($objCatalogMember->MemberOrder == $attribPosition+1 || isset($aFieldIDs[$intEBavelFieldID])) {
							if ($objCatalogMember->MemberOrder == $attribPosition+1) {
								$intFieldIDToGet = $intEBavelFieldID;
								//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
								$intFieldDataIDToGet = $objCatalogMember->eBavelFieldDataID;
								//@JAPR
							}
							
							//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
							$strFieldDescToGet = "DSC_".$objCatalogMember->ClaDescrip;
							$arrFieldDescToGet[$intEBavelFieldID] = $strFieldDescToGet;
						}
					}
					//@JAPR
				}
				
				if (count($arreBavelFieldIDs) == 0) {
					return $arrayValues;
				}
				
				//Siempre el primer campo es el ID consecutivo único del valor
				$strFieldIDToGet = '';
				$anBavelFieldIDsColl = array(); //array(array('id' => 'id_'.$strFormID, 'tagname' => 'widget_field_numeric', 'label' => 'id', 'section_id' => $objCatalog->eBavelFormID, 'appId' => $objCatalog->eBavelAppID, 'id_fieldform' => -1, 'key' => -1, 'formID' => $strFormID));
				foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
					foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
						if (isset($arreBavelFieldIDs[$intFieldID])) {
							$arreBavelFieldIDsStr[$intFieldID] = $arrFieldData['id'];
							$anBavelFieldIDsColl[] = $arrFieldData;
							$arreBavelFieldIDsNameID[$intFieldID] = $arrFieldData['id'];
							//@JAPR 2014-05-30: Agregado el tipo de sección Inline
							//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar
							//Se agregarán todos los campos que se encuentren en el array especificado + el pedido por posición, por lo que siempre se
							//asume que el pedido por posición tiene que ser como mínimo el último en el orden de los especificados en el array
							//if ($intFieldID == $intFieldIDToGet) {
							if ($intFieldID == $intFieldIDToGet || isset($aFieldIDs[$intFieldID])) {
								$strFieldIDToGet = $arrFieldData['id'];
								//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
								$strFieldDescToGet = $arrFieldDescToGet[$intFieldID];
								$this->RSFields[$strFieldDescToGet] = $strFieldIDToGet;
							}
						}
					}
				}
				
				require_once('settingsvariable.inc.php');
				$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'EBAVELURL');
				if (is_null($objSetting) || trim($objSetting->SettingValue == '')) {
					return $arrayValues;
				}
				
				//Verica si el código de eBavel está disponible
			    $strPath = '';
			    $streBavelPath = '';
				$arrURL = parse_url($objSetting->SettingValue);
				$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
				$strPath = str_replace('/', '\\', $strPath);
				$strPath = str_replace('\\\\', '\\', $strPath);
				if (file_exists($strPath))
				{
					@require_once($strPath);
					$streBavelPath = str_ireplace('service.php', '', $strPath);
				}
				
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					if (!function_exists('NotifyError')) {
						echo("<br>\r\nNo NotifyError function");
					}
					else {
						echo("<br>\r\nNotifyError function exists");
					}
				}
				
				if (!class_exists('Tables'))
				{
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\nNo eBavel code loaded");
					}
					return $arrayValues;
				}
                /*@AAL 19/03/2015: Modificado para verificar si existen las clases DBForms y DBViews*/
				if (!class_exists('DBForm') || !class_exists('DBView'))
				{
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\nNo eBavel code loaded");
					}
					return $arrayValues;
				}
				
				//@JAPR 2014-05-20: Corregido un bug, para que eBavel use seguridad, necesita tener esta variable de sesión asignada
				//@JAPR 2016-07-06: Corregido un bug, para el acceso al API simplemente se sobrescriben las variables, condicionarlo de esta manera evitaba que se pudiera usar el
				//login de eForms independiente (sin pasar por KPIOnline) ya que ese no sobreescribe estas variables, así que quedaban mezcladas (#S36ANP)
				//if (is_null(@$_SESSION["BITAM_UserName"]) || trim($_SESSION["BITAM_UserName"]) == '') {
				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				//@session_register("BITAM_UserName");
				$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
				$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
				$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
				//}
				//@JAPR
				
				$this->Repository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
				//@JAPR 2014-05-30: Agregado el tipo de sección Inline
				//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar
				//$objQuery = DBForm::name($strFormID)->fields(array($strFieldIDToGet));
                
                /*@AAL 19/03/2015: Modificado para soportar Formas y vistas de eBavel*/
	      		if (getMDVersion() >= esvRedesign) {
	      			if($objCatalog ->eBavelFormType == ebftForms)
	                    $objQuery = DBForm::name($strFormID)->fields(array_values($this->RSFields));
	                else
	                    $objQuery = DBView::name($strFormID)->fields(array_values($this->RSFields));
	                //@AAL
	      		}
	      		else
	      			$objQuery = DBForm::name($strFormID)->fields(array_values($this->RSFields));
                
                
				//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
				//Agregado el GROUP BY por el campo que se está pidiendo
				//@$objQuery->groupBy(array($strFieldIDToGet));
				@$objQuery->groupBy(array_values($this->RSFields));
				//$objQuery->onlyFields(false);
				//Si sólo se pedirá el RecordSet, hay que validar si se incluyen o no los campos de sistema
				if ($bGetRecordset) {
					$objQuery->onlyFields(!$bIncludeSystemFields);
				}
				
				//Genera el filtro para eBavel
				//@JAPR 2014-05-30: Agregado el tipo de sección Inline
				//Si hay un filtro directo, entonces simplemente se agrega a la clase en lugar de recorrer las propiedades
				if (trim($sWhere) != '') {
					$objQuery->whereRaw($sWhere);
				}
				else {
					//El filtro se arma directamente desde la instancia
					$strAnd = '';
					$intAdd = ($bFilterAttribPosition)?2:1;
					//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
					//Validado que no se puedan reutilizar los mismos campos en atributos diferentes, esto porque adicionalmente los campos
					//Geolocation se dividieron en sus porciones de Latitude y Longitude, así que no puede mandar el filtro por separado ya que
					//eBavel lo maneja unificado
					$strWhereRaw = '';
					$strAndRaw = '';
					$arrProcessedFields = array();
					//@JAPR
					foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
						if ($objCatalogMember->MemberOrder >= $attribPosition+$intAdd) {
							break;
						}
						
						$intFieldID = $objCatalogMember->eBavelFieldID;
						//@JAPR 2014-11-14: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						//Los campos Geolocation no se pueden usar a la fecha de implementación directamente con la función where, así que se
						//preparará un whereRaw para agregarlo adicional a los where normales que pudieran existir
						$strSuffix = '';
						if ($objCatalogMember->eBavelFieldDataID > ebfdValue) {
							$blnValid = true;
							switch ($objCatalogMember->eBavelFieldDataID) {
								case ebfdLatitude:
									$blnValid = false;
									$strSuffix = '_L';
									break;
								case ebfdLongitude:
									$blnValid = false;
									$strSuffix = '_A';
									break;
							}
							
							//Si no es un atributo válido para un filtro con la función where, significa que es un atributo que a la fecha de
							//implementación se tenía que filtrar con un whereRaw, así que modifica dicho filtro y continua con el siguiente
							//atributo en el order
							if (!$blnValid) {
								if ($intFieldID > 0 && isset($arreBavelFieldIDsStr[$intFieldID])) {
									$strEBavelFieldID = $intFieldID;
									if ($strSuffix) {
										$strEBavelFieldID .= $strSuffix;
									}
									$streBavelFieldIDsValue = $arreBavelFieldIDsValue[$strEBavelFieldID];
									//@JAPR 2014-12-18: Corregido un bug, faltaba validar contra NULL para cuando no tiene valor este campo
									if (!is_null($streBavelFieldIDsValue)) {
										if ($streBavelFieldIDsValue == 'sv_ALL_sv') {
											return $arrayValues;
										}
										
										//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
										$strWhereRaw .= $strAndRaw."`".$arreBavelFieldIDsStr[$intFieldID].$strSuffix."`".'='.$streBavelFieldIDsValue;
										$strAndRaw = ' AND ';
									}
									//@JAPR
								}
								
								continue;
							}
						}
						//@JAPR
						
						if ($intFieldID > 0 && isset($arreBavelFieldIDsStr[$intFieldID])) {
							//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
							//Validado que sólo intente aplicar filtros si realmente se especificaron
							//@JAPR 2014-11-14: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
							//Los campos Geolocation no se pueden usar a la fecha de implementación directamente con la función where, así que se
							//preparará un whereRaw para agregarlo adicional a los where normales que pudieran existir
							$strEBavelFieldID = $intFieldID;
							if ($strSuffix) {
								$strEBavelFieldID .= $strSuffix;
							}
							$streBavelFieldIDsValue = $arreBavelFieldIDsValue[$strEBavelFieldID];
							if (!is_null($streBavelFieldIDsValue)) {
								//@JAPR 2014-08-21: Validado que si se recibe el valor de All (gracias al nuevo parámetro $bSetAllValue)
								//en alguno de los atributos previos, entonces simplemente se regrese el array vacio (con el valor de All)
								//porque quiere decir que al menos alguno de los padres no ha sido filtrado. Teóricamente no debería entrar
								//a esta parte, porque el IF que encierra a esto ya validó contra este valor, pero con esto se verifican
								//los atributos previos también
								if ($streBavelFieldIDsValue == 'sv_ALL_sv') {
									return $arrayValues;
								}
								//@JAPR
								if ($streBavelFieldIDsValue == '') {
									continue;
								}
								//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
								$objQuery->where($arreBavelFieldIDsStr[$intFieldID], '=', $streBavelFieldIDsValue, $strAnd);
							}
							//@JAPR
						}
						$strAnd = 'AND';
						//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						$arrProcessedFields[$intFieldID] = $intFieldID;
						//@JAPR
					}
					
					//@JAPR 2014-11-14: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
					//Los campos Geolocation no se pueden usar a la fecha de implementación directamente con la función where, así que se
					//preparará un whereRaw para agregarlo adicional a los where normales que pudieran existir
					if ($strWhereRaw != '') {
						$objQuery->whereRaw($strWhereRaw, $strAnd);
					}
				}
				//@JAPR
				
				$sql = '';
				if (method_exists($objQuery, 'compileQuery')) {
					//@JAPR 2014-08-19: Agregada esta asignación por compatibilidad con eBavel a partir de cambios de esta fecha
					//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
					//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
					//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
					//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
					//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
					global $eBavelServiceVersion;
					global $theUser;
					if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
						$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
						Metadata::setRepository($aneBavelRepository);
					}
					else {
						if (class_exists('Metadata')) {
							@Metadata::setRepository($this->Repository);
						}
					}
					//@JAPR
					
					$sql = (string) @$objQuery->compileQuery($this->Repository->ADOConnection);
					global $queriesLogFile;
					$aLogString="\r\n--------------------------------------------------------------------------------------";
					$aLogString.="\r\BITAMCatalogFilter->getCatalogValues: \r\n";
					$aLogString.=$sql;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n CatalogFilter->getCatalogValues eBavel Query: {$sql}");
					}
					@updateQueriesLogFile($queriesLogFile,$aLogString);
				}
				
				$aRS = $objQuery->get($this->Repository->ADOConnection);
			   	$this->Repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
				if ($aRS === false)
				{
					//@JAPR 2014-09-12: Corregido un bug, se estaba reportando el error con la conexión equivocada
					if($gblShowErrorSurvey) {
						die("(".__METHOD__.") ".translate("Error accessing")." ".$strFormID." ".translate("form").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					else {
						return("(".__METHOD__.") ".translate("Error accessing")." ".$strFormID." ".translate("form").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
				
				//Si solo se pidió el recordset, entonces se regresa directamente para que lo procese quien invoca al método, pero no se puede usar así
				//como parte del FrameWork
				if ($bGetRecordset) {
					return $aRS;
				}
				
				//@JAPR 2014-05-20: Agregada validación para controlar cuando eBavel no pueda regresar el RecordSet
				if (property_exists($aRS, 'EOF')) {
					while(!$aRS->EOF)
					{
						$value = $aRS->fields[$strFieldIDToGet];
						//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						switch ($intFieldDataIDToGet) {
							case ebfdLatitude:
								$value = (float) @$aRS->fields[$strFieldIDToGet."_L"];
								break;
							case ebfdLongitude:
								$value = (float) @$aRS->fields[$strFieldIDToGet."_A"];
								break;
						}
						//@JAPR
						$arrayValues[$value] = $value;
						$aRS->MoveNext();
					}
				}
				//@JAPR
			}
			else {
				$sql = "SELECT ".$fieldName." FROM ".$this->TableName;
				$keyField = $this->TableName."KEY";
				$where = " WHERE ".$keyField." <> 1";
				//@JAPR 2014-12-10: Agregado el soporte para el parámetro $sWhere en catálogos de eForms, ya que hasta esta fecha sólo se había
				//utilizado en catálogos de eBavel
				if (trim($sWhere) != '') {
					$where .= " AND ".$sWhere;
				}
				else {
					for($i=0;$i<$attribPosition;$i++)
					{
						$tempFieldName = $this->AttribFields[$i];
						$tempFieldValue = $this->$tempFieldName;
						if (isset($this->AttribFields[$i]) && $tempFieldValue != '') {
							$where.=" AND ".$tempFieldName." = ".$this->Repository->DataADOConnection->Quote($tempFieldValue);
						}
					}
				}
				//@JAPR
				
				$sql.=$where;
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS === false)
				{
					if($gblShowErrorSurvey) {
						die("(".__METHOD__.") ".translate("Error accessing")." ".$this->TableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					else {
						return("(".__METHOD__.") ".translate("Error accessing")." ".$this->TableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
				
				while(!$aRS->EOF)
				{
					$field = strtolower($fieldName);
					$value = $aRS->fields[$field];
					$arrayValues[$value] = $value;
					$aRS->MoveNext();
				}
			}
			//JAPR
		}
		
		return $arrayValues;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}

class BITAMCatalogFilterCollection extends BITAMCollection
{
	public $CatalogID;
	public $AttribFields;
	public $AttribNames;
	public $AttribIDs;
	public $NumAttribFields;
	public $TableName;
	public $ModelID;
	
	function __construct($aRepository, $aCatalogID)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->CatalogID = $aCatalogID;
		
		$this->AttribFields = array();
		$this->AttribNames = array();
		$this->NumAttribFields = 0;
		
		//se asigna -1 porque estas dimensiones no estan ligadas a un cubo en especifico
		$this->ModelID = -1;
		
		$anInstanceCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $this->CatalogID);
		$this->TableName = $anInstanceCatalog->TableName;
		
		$attributes = BITAMCatalogMemberCollection::NewInstance($aRepository, $this->CatalogID);
		$numFields = count($attributes->Collection);

		for($i=0; $i<$numFields; $i++)
		{
			$this->AttribFields[$i] = 'DSC_'.$attributes->Collection[$i]->ClaDescrip;
			$this->AttribIDs[$i] = $attributes->Collection[$i]->ClaDescrip;
			$this->AttribNames[$i] = $attributes->Collection[$i]->MemberName;
		}
		
		$this->NumAttribFields = $numFields;
	}

	static function NewInstance($aRepository, $aCatalogID, $anArrayOfFilterIDs = null)
	{
		$anInstance = new BITAMCatalogFilterCollection($aRepository, $aCatalogID);
		
		$filter = "";

		if (!is_null($anArrayOfFilterIDs))
		{
			switch (count($anArrayOfFilterIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND FilterID = ".((int)$anArrayOfFilterIDs[0]);
					break;
				default:
					foreach ($anArrayOfFilterIDs as $aFilterID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aFilterID; 
					}
					
					if ($filter != "")
					{
						$filter = " AND FilterID IN (".$filter.")";
					}
					break;
			}
		}

		$sql = "SELECT FilterID, CatalogID, FilterName, FilterText, FilterLevels FROM SI_SV_CatFilter WHERE CatalogID = ".$aCatalogID.$filter." ORDER BY FilterID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilter ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilter ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		$arrayCollections =  array();
		$arrayFilterIDs = array();
		$arrayFilterNames = array();
		$arrayFilterText = array();
		$arrayFilterLevels = array();

		while(!$aRS->EOF)
		{
			$filterID = (int)$aRS->fields["filterid"];
			$filterName = $aRS->fields["filtername"];
			$filterText = $aRS->fields["filtertext"];
			$filterLevels = $aRS->fields["filterlevels"];
			$arrayFilterIDs[] = $filterID;
			$arrayFilterNames[] = $filterName;
			$arrayFilterText[] = $filterText;
			$arrayFilterLevels[] = $filterLevels;
			$arrayCollections[] = BITAMFilteredSVCollection::NewInstanceByStrFilter($aRepository, $filterText, $filterLevels, $anInstance->ModelID);
			
			$aRS->MoveNext();
		}
		
		$arrayValuesRS = array();
		
		foreach ($arrayCollections as $keyCollection => $filterCollection)
		{
			$arrayValuesRS = array();
			$arrayValuesRS["catalogid"] = $aCatalogID;
			$arrayValuesRS["filterid"] = $arrayFilterIDs[$keyCollection];
			$arrayValuesRS["filtername"] = $arrayFilterNames[$keyCollection];
			$arrayValuesRS["filtertext"] = $arrayFilterText[$keyCollection];
			$arrayValuesRS["filterlevels"] = $arrayFilterLevels[$keyCollection];
			
			//Todos los elementos de los atributos del catalogo los ponemos por 
			//default con el valor de ALL
			for($i=0; $i<$anInstance->NumAttribFields; $i++)
			{
				$arrayValuesRS[strtolower($anInstance->AttribFields[$i])] = "sv_ALL_sv";
			}

			foreach ($filterCollection as $element)
			{
				$fieldName = strtolower($element->nom_fisicok);
				$fieldValue = $element->values[0];
				
				$arrayValuesRS[$fieldName] = $fieldValue;
			}
			
			$anInstance->Collection[] = BITAMCatalogFilter::NewInstanceFromRSCollection($anInstance->Repository, $arrayValuesRS);
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("CatalogID", $aHTTPRequest->GET))
		{
			$aCatalogID = (int)$aHTTPRequest->GET["CatalogID"];
		}
		else
		{
			$aCatalogID = 0;
		}

		return BITAMCatalogFilterCollection::NewInstance($aRepository, $aCatalogID);
	}
	
	function get_Parent()
	{
		return BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
	}
	
	function get_Title()
	{	
		return translate("Filters");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=Catalog&CatalogID=".$this->CatalogID;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=CatalogFilter&CatalogID=".$this->CatalogID;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'FilterID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		for ($i=0; $i<$this->NumAttribFields; $i++)
		{
			$fieldName = $this->AttribFields[$i];
			$fieldNameSTR = $fieldName."STR";
			$labelText = $this->AttribNames[$i];

			$aField = BITAMFormField::NewFormField();
			$aField->Name = $fieldNameSTR;
			$aField->Title = $labelText;
			$aField->Type = "String";
			$aField->Size = 255;
			
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}
?>