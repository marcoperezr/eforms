<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("catalog.inc.php");
require_once("filtersv.inc.php");
require_once('appuser.inc.php');
require_once("dataSource.inc.php");

class BITAMDataSourceFilter extends BITAMObject
{
	public $NoHeader = 1;
	public $FilterID;
	public $CatalogID;
	public $FilterName;
	//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
	public $FilterFormula;
	
	public $AttribFields;
	public $AttribNames;
	public $AttribIDs;
	public $AttribPositions;
	public $AttribMemberIDs;
	public $NumAttribFields;
	public $TableName;
	public $ModelID;
	public $arrFilter;
	public $sqlwhere;
	//Se utilizaron estas variables para reutilizar el codigo de los filtros de report.inc.php
	//Se obtiene el catalogID del filtro que se esta utilizando y el CatMemberID siempre sera 
	//el ultimo elemento de la jerarquia del catalogo
	public $CatMemberID;
	public $typeofSave; /* 1: User, 2: Group*/
	public $Status = 1; /* 1: Acitvo, 0: Inactivo*/
	public $UserIDs;
	public $UsrRoles;
	public $StrUserRol;
	//@JAPR 2014-05-16: Agregados los catálogos tipo eBavel (estas propiedades sólo se asignan después de un getCatalogValues de catálogos de eBavel
	public $FormID;
	//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
	public $RSFields;			//Array con los campos que se regresarán (cuando se pide un recordset) en formato [DSC_ClaDescrip] => FieldID de EBavel
	//@JAPR
	//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
	//Agregado el parámetro $bNullFilters para inicializar las propiedades que representan los valores filtrados con un valor null en lugar de hacerlo
	//con cadena vacia (esto es especialmente útil cuando se está aplicando filtros de eBavel, ya que ahi se hará diferencia entre el valor null o
	//vacio, porque para obtener las categorías basadas en formas de eBavel se reutilizó esta instancia, pero hay valores de eBavel que son vacios
	//y estaba prefiltrando por esos atributos padre en lugar de traer todas las categorías del atributo especificado)
	//@JAPR 2014-08-21: Agregado el parámetro $bSetAllValue para indicar cuando se está en la ventana de filtros y se está cargando
	//uno vacio, de forma que en eBavel no se intenten obtener valores en ese caso porque algunos campos no soportan recibir vacio
	//como valor, y realmente no tiene sentido hacer una consulta que esperamos que falle (esto NO se debería usar si se espera
	//recibir un RecordSet, porque lo que se va a hacer es un simple return de null en caso de filtrar por All)
	function __construct($aRepository, $aCatalogID, $bNullFilters = false, $bSetAllValue = false)
	{
		BITAMObject::__construct($aRepository);
		$this->FilterID = -1;
		$this->CatalogID = $aCatalogID;
		$this->FilterName = "";
		//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
		$this->FilterFormula = "";
		
		$this->AttribFields = array();
		$this->AttribNames = array();
		$this->AttribIDs = array();
		$this->AttribPositions = array();
		$this->AttribMemberIDs = array();
		$this->NumAttribFields = 0;
		//@JAPR 2014-05-16: Agregados los catálogos tipo eBavel
		$this->FormID = '';
		//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
		$this->RSFields = array();
		
		//Como las dimensiones no estan ligadas a un cubo entonces el ModelID tendra valor de -1
		$this->ModelID = -1;

		$anInstanceCatalog = BITAMDataSource::NewInstanceWithID($aRepository, $this->CatalogID);
		$this->TableName = $anInstanceCatalog->TableName;
				
		$attributes = BITAMCatalogMemberCollection::NewInstance($aRepository, $this->CatalogID);
		$numFields = count($attributes->Collection);

		for($i=0; $i<$numFields; $i++)
		{
			$fieldName = 'DSC_'.$attributes->Collection[$i]->ClaDescrip;
			$fieldNameSTR = $fieldName."STR";
			$this->AttribFields[$i] = 'DSC_'.$attributes->Collection[$i]->ClaDescrip;
			$this->AttribIDs[$i] = $attributes->Collection[$i]->ClaDescrip;
			//ears 2017-01-13 validación de caracteres especiales
			$this->AttribNames[$i] = GetValidJScriptText($attributes->Collection[$i]->MemberName,array("fullhtmlspecialchars" => 1));
			$this->AttribMemberIDs[$fieldName] = $attributes->Collection[$i]->MemberID;
			$this->AttribPositions[$fieldName] = $i;
			
			//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
			//Agregado el parámetro $bNullFilters para inicializar las propiedades que representan los valores filtrados con un valor null en lugar de hacerlo
			//con cadena vacia
			if ($bNullFilters) {
				$this->$fieldName = null;
				$this->$fieldNameSTR = null;
			}
			else {
				//@JAPR 2014-08-21: Agregado el parámetro $bSetAllValue para indicar cuando se está en la ventana de filtros y se está cargando
				//uno vacio, de forma que en eBavel no se intenten obtener valores en ese caso porque algunos campos no soportan recibir vacio
				//como valor, y realmente no tiene sentido hacer una consulta que esperamos que falle (esto NO se debería usar si se espera
				//recibir un RecordSet, porque lo que se va a hacer es un simple return de null en caso de filtrar por All)
				if ($anInstanceCatalog->eBavelAppID > 0 && $anInstanceCatalog->eBavelFormID > 0 && $bSetAllValue) {
					$this->$fieldName = "sv_ALL_sv";
					$this->$fieldNameSTR = "All";
				}
				else {
					$this->$fieldName = "";
					$this->$fieldNameSTR = "";
				}
			}
			//@JAPR
		}
		
		$this->NumAttribFields = $numFields;
		
		$this->UserIDs = array();
		$this->UsrRoles = array();
		$this->StrUserRol = "";
		$this->Status = 1;
		
		//@JAPR 2014-05-30: Agregados los catálogos tipo eBavel
		if ($anInstanceCatalog->eBavelAppID > 0 && $anInstanceCatalog->eBavelFormID > 0) {
            /*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel*/
      		if (getMDVersion() >= esvRedesign) {
	            //Si hemos seleccionado una Forma de eBavel entonces pedimos sus atributos.
	            if($anInstanceCatalog->eBavelFormType == ebftForms)
	                $arreBavelFormColl = @GetEBavelSections($aRepository, false, $anInstanceCatalog->eBavelAppID, array($anInstanceCatalog->eBavelFormID));        
	            else//Si hemos seleccionado una Vista de eBavel entonces pedimos sus atributos.
	                $arreBavelFormColl = @GetEBavelViews($aRepository, false, $anInstanceCatalog->eBavelAppID, array($anInstanceCatalog->eBavelFormID));
      		}
      		else //Se trata de un catálogo extraído de eBavel, por lo que debe asignar el FormID
				$arreBavelFormColl = @GetEBavelSections($aRepository, false, $anInstanceCatalog->eBavelAppID, array($anInstanceCatalog->eBavelFormID));
      		//@AAL

			//Se trata de un catálogo extraído de eBavel, por lo que debe asignar el FormID
			$arreBavelFormColl = @GetEBavelSections($aRepository, false, $anInstanceCatalog->eBavelAppID, array($anInstanceCatalog->eBavelFormID));
			if (!is_null($arreBavelFormColl) && is_array($arreBavelFormColl) && count($arreBavelFormColl) > 0) {
				$objForm = @$arreBavelFormColl[0];
				$strFormID = (string) @$objForm['id'];
				$this->FormID = $strFormID;
			}
		}
		//@JAPR
	}

	//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
	//Agregado el parámetro $bNullFilters para inicializar las propiedades que representan los valores filtrados con un valor null en lugar de hacerlo
	//con cadena vacia
	//@JAPR 2014-08-21: Agregado el parámetro $bSetAllValue para indicar cuando se está en la ventana de filtros y se está cargando
	//uno vacio, de forma que en eBavel no se intenten obtener valores en ese caso porque algunos campos no soportan recibir vacio
	//como valor, y realmente no tiene sentido hacer una consulta que esperamos que falle (esto NO se debería usar si se espera
	//recibir un RecordSet, porque lo que se va a hacer es un simple return de null en caso de filtrar por All)
	static function NewInstance($aRepository, $aCatalogID, $bNullFilters = false, $bSetAllValue = false)
	{
		return new BITAMDataSourceFilter($aRepository, $aCatalogID, $bNullFilters, $bSetAllValue);
	}

	static function NewInstanceWithID($aRepository, $aFilterID)
	{
		$anInstance = null;
		
		if (((int) $aFilterID) <= 0)
		{
			return $anInstance;
		}
		
		//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
		$strAddFields = '';
		if (getMDVersion() >= esvDataSourceFilterFormula) {
			$strAddFields .= ', FilterFormula';
		}
		$sql = "SELECT CatalogID, FilterID, FilterName, Status {$strAddFields} FROM si_sv_datasourcefilter WHERE FilterID = ".$aFilterID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$anInstance = BITAMDataSourceFilter::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}

	static function NewInstanceWithCatalogIDandUserID($aRepository, $aCatalogID, $aUserID)
	{
		$anInstance = null;
		
		if (((int) $aCatalogID) <= 0)
		{
			return $anInstance;
		}
		
		//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
		$strAddFields = '';
		if (getMDVersion() >= esvDataSourceFilterFormula) {
			$strAddFields .= ', T1.FilterFormula';
		}
		$sql = "SELECT T1.CatalogID, T1.FilterID, T1.FilterName, T1.Status {$strAddFields} FROM si_sv_datasourcefilter T1
					INNER JOIN si_sv_datasourcefilterUser T2
					ON T2.FilterID = T1.FilterID
				WHERE T1.CatalogID = ".$aCatalogID." 
					AND T2.UserID = ".$aUserID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$anInstance = BITAMDataSourceFilter::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}
	static function generateResultFilterByUser($aRepository, $aCatalogID, $userID)
	{
		$resultFilter = "";
		$arrayTmpFilters = array();	
		$arrayFilterID = array();
		$arrFilters = array();
		$dataSouceinstance = BITAMDataSource::NewInstanceWithID($aRepository, $aCatalogID);
		//Obtenemos todos los filtros para el usuario especificado pero primero
		//obtenemos los roles de dicho usuario
		$roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $userID);
		
		$sql = "SELECT FilterID FROM si_sv_datasourcefilter WHERE CatalogID = ". $aCatalogID ." AND Status = 1 ORDER BY FilterID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		while(!$aRS->EOF)
		{
			$filterID = (int) $aRS->fields["filterid"];
			$arrayTmpFilters[$filterID] = $filterID;
			
			$aRS->MoveNext();
		}
		
		//Si no hay filtros especificados para dicho catalogo entonces se envia un filtro vacio
		if(count($arrayTmpFilters)<=0)
		{
			$resultFilter = "";
		}
		else 
		{
			//Obtenemos los filtros que si estan asociados al usuario
			$sql = "SELECT FilterID FROM si_sv_datasourcefilterUSER 
					WHERE UserID = ".$userID." AND FilterID IN (".implode(",", $arrayTmpFilters).")";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			while(!$aRS->EOF)
			{
				$filterID = (int) $aRS->fields["filterid"];
				$arrayFilterID[] = $filterID;
				
				$aRS->MoveNext();
			}
			
			if(count($roles)>0)
			{
				//Obtenemos los filtros que si estan asociados al usuario pero por rol
				$sql = "SELECT FilterID FROM si_sv_datasourcefilterrol 
						WHERE RolID IN (".implode(",", $roles).") AND FilterID IN (".implode(",", $arrayTmpFilters).")";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				while(!$aRS->EOF)
				{
					$filterID = (int) $aRS->fields["filterid"];
					$arrayFilterID[] = $filterID;
					
					$aRS->MoveNext();
				}
			}
			$arrFilters = array();
			$arrFilters = self::fillArrayvalues($aRepository,$arrayFilterID);
			/*Ya con todos los arreglos de posibles valores los recorro*/
			if (count($arrFilters) > 0){
				$arFilter = array();
				foreach($arrFilters as $key => $arrmembervalue){
					//GCRUZ 2016-05-26. Si existe la llave FilterFormula en el array, es fórmula, de lo contrario son valores seleccionados
					if (!isset($arrmembervalue['FilterFormula']))
					{
						foreach ($arrmembervalue as $membername => $arrvalues) {
							if ($dataSouceinstance->eBavelAppID > 0 && $dataSouceinstance->eBavelFormID > 0) {
								$query = "SELECT eBavelFieldID FROM SI_SV_DatasourceMember WHERE fieldName = " . $aRepository->DataADOConnection->Quote($membername); 
								$aRS = $aRepository->DataADOConnection->Execute($query);
								if ($aRS && !$aRS->EOF){
									$eBavelFieldID = $aRS->fields["ebavelfieldid"];
									$query = "SELECT id FROM SI_FORMS_FIELDSFORMS WHERE id_fieldform = " . $eBavelFieldID; 
									$aRS2 = $aRepository->ADOConnection->Execute($query);
									if ($aRS2 && !$aRS2->EOF){
										$membername = $aRS2->fields["id"];
									}
								}
							}
							$arFilter[] = $membername .  " in (" . implode(",", $arrvalues) . ")";
						}
					}
					else
					{
						$strFilter = BITAMDataSource::TranslateAttributesVarsToFieldNames($aRepository, $arrmembervalue['CatalogID'], $arrmembervalue['FilterFormula']);
						$strFilter = TranslateGlobalVariables($aRepository, $strFilter, $userID);
						$arFilter[] = '('.$strFilter.')';
					}
				}
				$resultFilter = implode(" AND ", $arFilter) ;
			}
		}
		return $resultFilter;
	}
	
	//@JAPR 2019-02-27: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
	/* Aunque esta función en principio parece que hace lo mismo que readFiler pues el resultado final es obtener los valores definidos en el filtro, su uso es muy diferente, puesto que 
	esta función es utilizada exclusivamente para el proceso de aplicación de los filtros de seguridad de catálogos en las diversas funciones que necesiten obtener datos del usuario, por lo
	tanto, aquí si es indistinto si se regresa un único registro que contenga una lista separada por "," de los valores, o bien muchos registros donde cada uno representa un valor diferente
	sin contener "," que separen su contenido en mas de un valor (o bien combinación de ambos), pues al final el proceso de esta función dentro de generateResultFilterByUser, concatenará
	todos los valores devueltos en este array como una lista única separada por "," para permitir realizar un IN de un filtro SQL, así que es irrelevante la manera en que los regrese */
	static function fillArrayvalues($aRepository,$arrFilterId) {
		$finalArray= array();
		/*Agrupo primero por miembro para poder ordenar el arreglo a ese nivel*/
		$sql = "SELECT DISTINCT T3.fieldName , T2.filterID, T2.DataSourceMemberID
				FROM si_sv_datasourcefilter T1
					INNER JOIN si_sv_datasourcefilter_det T2
					ON T1.FilterID = T2.FilterID
					INNER JOIN si_sv_datasourcemember T3
					ON T3. MemberID = T2.DataSourceMemberID  
				WHERE T1.FilterID in (". implode(", ",$arrFilterId) . ")";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		while ($aRS && !$aRS->EOF){
			$arrFilter = array();
			$FilterID = $aRS->fields["filterid"];
			$fieldName = $aRS->fields["fieldName"];
			$member = $aRS->fields["datasourcememberid"];
			$sqlMember = "SELECT  T2.Value
							FROM si_sv_datasourcefilter T1
								INNER JOIN si_sv_datasourcefilter_det T2
								ON T1.FilterID = T2.FilterID
							WHERE T2.FilterID = ". $FilterID . " AND  T2.DataSourceMemberID =" . $member;
			$aRS2 = $aRepository->DataADOConnection->Execute($sqlMember);
			$arrValue = array();
			while ($aRS2 && !$aRS2->EOF){
				//@JAPR 2018-11-12: Corregido un bug, si había múltiples valores en el filtro, estaba manejando todos como un único valor delimitándolos entre comillas lo cual no había
				//funcionado en un IN (#TA488W)
				$strValuesList = (string) @$aRS2->fields["value"];
				$arrValuesList = explode(',', $strValuesList);
				array_walk($arrValuesList, function (&$item, $key) use ($aRepository) {
					$item = $aRepository->DataADOConnection->Quote($item);
				});
				$strValuesList = implode(',', $arrValuesList);
				$arrValue[]=$strValuesList;
				//@JAPR
				$aRS2->MoveNext();
			}
			$arrFilter[$fieldName] = $arrValue;
			$finalArray[] = $arrFilter;
			$aRS->MoveNext();
		}

		//GCRUZ 2016-05-25. ahora los que son por fórmula
		$sql = "SELECT FilterFormula, CatalogID FROM si_sv_datasourcefilter WHERE FilterID IN (".implode(", ",$arrFilterId).") AND FilterFormula <> '' AND FilterFormula IS NOT NULL";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		while ($aRS && !$aRS->EOF){
			$arrFilterFormula = array();
			$arrFilterFormula['FilterFormula'] = $aRS->fields["filterformula"];
			$arrFilterFormula['CatalogID'] = $aRS->fields["catalogid"];
			$finalArray[] = $arrFilterFormula;
			$aRS->MoveNext();
		}

		return $finalArray;
	}
	function generateForm($aUser) {
		//GCRUZ 2016-07-21. Eliminar <ENTER> de las fórmulas
		$this->FilterFormula = str_replace(PHP_EOL, ' ', $this->FilterFormula);
		$this->FilterFormula = str_replace("\r", ' ', $this->FilterFormula);
		$this->FilterFormula = str_replace("\n", ' ', $this->FilterFormula);
?>
	<html>
		<head>
			<meta http-equiv="x-ua-compatible" content="IE=10" />
			<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
			<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
			<link href="css/font/roboto/robotofont.css" rel="stylesheet" type="text/css">
			<script src="js/codebase/dhtmlxFmtd.js"></script>
	      	<script type="text/javascript" src="js/json2.min.js"></script>
			<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
			<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
			<script type="text/javascript" data-main="js/_requireconfig.js" src="js/libs/require/require.js"></script>
			<script type="text/javascript" src="js/underscore-min.js"></script>
			<script>
				var idcatalog = '<?= $this->CatalogID ?>';
				var filterid = '<?= $this->FilterID ?>';
				//ears 2017-01-13 validación de caracteres especiales
				var filterName = '<?= GetValidJScriptText($this->FilterName, array("fullhtmlspecialchars" => 1)) ?>';
				//var filterFormula = '<?= addslashes( BITAMDataSource::ReplaceMemberIDsByNames($this->Repository, $this->CatalogID, $this->FilterFormula)) ?>';
				//ears 2017-01-13 validación de caracteres especiales
				var filterFormula = '<?= GetValidJScriptText(BITAMDataSource::ReplaceMemberIDsByNames($this->Repository, $this->CatalogID, $this->FilterFormula), array("fullhtmlspecialchars" => 1)) ?>';
				var Status = '<?= $this->Status ?>';
				var filterType = 'values';
				var otyAttribute = '<?=otyAttribute?>'; //Identificador en el processRequest.php
				//@AAL Agragado para pintar los valores en la columna value del grid que el usuario a guardado previamente
				var typeofSave = '<?= $this->typeofSave ?>';
				var Values = JSON.parse(<?= $this->Repository->DataADOConnection->Quote(json_encode($this->arrFilter))?>);
				var idUser = JSON.parse(<?= $this->Repository->DataADOConnection->Quote(json_encode($this->UserIDs))?>);
				var idRoll = JSON.parse(<?= $this->Repository->DataADOConnection->Quote(json_encode($this->UsrRoles))?>);
				//JAPR 2018-07-27: Modificado el request para permitir filtrar por atributo y así optimizar la carga por demanda (#2PB40B)
				var ValuesLoaded = {};
				//JAPR
				function doOnLoad()	{
					var Layout = new dhtmlXLayoutObject({
						parent: document.body,
						pattern: "2E"
					});
					Layout.cells("a").hideHeader();
					Layout.cells("a").fixSize(true, true); //Bloque el Rezise del Content a)
					Layout.cells("b").hideHeader();
					Layout.cells("b").setHeight(45);
					Layout.cells("b").fixSize(true, true); //Bloque el Rezise del Content b)

					//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
					var objFormData = [
						{type:"block", name:'blockFiltertype', list:[
							{type:"radio", name: "filterType", value:'values' <?= ($this->FilterFormula == '')? ',checked:true ' : '' ?>, label:"<?=translate("Select Values")?>"},
							{type:"newcolumn", offset:20},
							{type:"radio", name: "filterType", value:'formula' <?= ($this->FilterFormula != '')? ',checked:true ' : '' ?>, label:"<?=translate("Use Formula")?>"}
						]},
						{type:"container", name:"grdFilters", inputWidth:650, inputHeight:250, className:"selection_container"},
						{type:"block", name:'blockFormula', list:[
							{type:"label", label:"<?=translate("Formula")?>"},
							{type:"input", name: "txtFormula", inputWidth:600, rows:10, value:filterFormula},
							{type:"button", name: "btnFormula", value:'<img src="images/admin/editFormula.gif">', width:25}
						]},
					];

					var objFormFilters = Layout.cells("a").attachForm(objFormData);
<?
					//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
					if (getMDVersion() < esvDataSourceFilterFormula) {
?>
						objFormFilters.hideItem('blockFiltertype');
<?
					}
					else
					{
						if ($this->FilterFormula != '')
						{
?>
							objFormFilters.hideItem('grdFilters');
							objFormFilters.showItem('blockFormula');
							filterType = 'formula';
<?
						}
						else
						{
?>
							objFormFilters.hideItem('blockFormula');
							objFormFilters.showItem('grdFilters');
							filterType = 'values';
<?
						}
					}
?>
					objFormFilters.attachEvent("onChange", function(name, value, state) {
						switch(name)
						{
							case 'filterType':
								if (value=='values')
								{
									objFormFilters.hideItem('blockFormula');
									objFormFilters.showItem('grdFilters');
									filterType = 'values';
								}
								else
								{
									objFormFilters.showItem('blockFormula');
									objFormFilters.hideItem('grdFilters');
									filterType = 'formula';
								}
								break;
						}
					});

					objFormFilters.attachEvent("onButtonClick", function(name) {
						switch (name) {
							case 'btnFormula':
								var objInput = this.getInput("txtFormula");
								if (objInput) {
									fnOpenFormulaEditor(objInput, objInput.value);
								}
								break;
						}
					});

					var objGridValues = new dhtmlXGridObject(objFormFilters.getContainer("grdFilters"));
					//var objGridValues = Layout.cells("a").attachGrid();
					objGridValues.setHeader("<?=translate("Member")?>,<?=translate("Values")?>");//La cabecera de las columnas
			        //objGridValues.setInitWidths("100%");          //el ancho de las columna s 
					objGridValues.setInitWidthsP("30,70");
			        objGridValues.setColAlign("left,left");       //la alineacion de las columnas  
			        //JAPR 2019-02-26: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
			        objGridValues.setColTypes("ro,gridComboMultiCheck"); 
			        //JAPR
			        objGridValues.setStyle("background-color:rgb(254,204,98);font-family:Arial;font-weight:bold;font-size:10pt;", "background-color:rgb(240,240,240);color:rgb(51,51,51);font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);color:rgb(240,240,240);font-family:Arial;font-size:9pt;");
					objGridValues.enableLightMouseNavigation(true);
					objGridValues.init();
					getMembers(idcatalog, objGridValues);
					/*objGridValues.attachEvent("onEditCell", function(stage,id,index) {
						//JAPR 2018-07-27: Modificado el request para permitir filtrar por atributo y así optimizar la carga por demanda (#2PB40B)
						var objGrid = this;
						//JAPR
						switch (stage) {
							case 0: //Before start
								//JAPR 2018-07-27: Modificado el request para permitir filtrar por atributo y así optimizar la carga por demanda (#2PB40B)
								//Antes de iniciar, se debe llenar la columna con la lista de opciones corresponiente utilizando sólo las descripciones, ya que este
								//componente no soporta uso de ids
								/ *if ( !ValuesLoaded[id] ) {
									parent.objDialog.progressOn();
									var urlPath = 'loadexcel/';
									$.get(urlPath +'action.php?action=getmembers&idcatalog=' + idcatalog + '&idattrib=' + id).done(function( r ) {
										parent.objDialog.progressOff();
										ValuesLoaded[id] = 1;
										if (r.success) {
											if ( r.arrValueMember[id] ) {
												arrvalueMembers[id] = r.arrValueMember[id];
												objGrid.registerCList(1, arrvalueMembers[id]);
												objGrid.selectCell(objGrid.getRowIndex(id), index);
												objGrid.editCell();
											}
										}
									});
									objGrid.registerCList(1, ['']);
									setTimeout(function() {
										objGrid.editStop(true);
									}, 100);
								}
								else {
									objGrid.registerCList(1, arrvalueMembers[id]);
								}* /
								//JAPR
								break;
							case 1: //The editor is opened
								//JAPR 2015-07-30: Agregados botones para seleccionar/deseleccionar todos los elementos
								/ *$('.dhx_clist input:button').before('<img src="images/admin/selectall.jpg" style="width:22px; height:22px;" onclick="javascript:fnSelectAllCListItems($(this).parents(\'.dhx_clist\')[0], true);" />');
								$('.dhx_clist input:button').before('<img src="images/admin/unselect.jpg" style="width:22px; height:22px;" onclick="javascript:fnSelectAllCListItems($(this).parents(\'.dhx_clist\')[0], false);" />');
								//JAPR
								$('.dhx_clist input:button').attr('value', '<?=translate("Apply")?>')* /
								break;

							case 2: //Editor closed
								break;
						}* /
						//@AAL 28/07/2015: Agregado para ajustar el Height del componente MiltipleList, cuado hay una lista grande, ésta desbordaba.
						/ *if ($(".dhx_clist").height() > 80) {
							$(".dhx_clist").css({"height": "100px", "overflow-y": "scroll"});
						}* /

						//Return true permite la edición, false la cancela
						return true;
					});*/
					//@AAL Agregado para mostrar la sección de los botones
					var objFormData = [
							{type:"block", blockOffset:240, offsetLeft:198, list:[
								{type:"button", name: "btnOk", value:"<?=translate("OK")?>",width:60},
								{type:"newcolumn"},
								{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>",width:60}
							]}
						];

					//Removemos la clase para no mostrar los bordes
					Layout.cells("b").cell.childNodes[1].removeAttribute("class");
					var objForm = Layout.cells("b").attachForm(objFormData);
					objForm.attachEvent("onButtonClick", function(name) {
						switch (name) {
							case "btnCancel":
								setTimeout(function () {
									doUnloadDialog();
								}, 100);
								break;
								
							case "btnOk":
								//JAPR 2019-02-27: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
								//Agregado el setTimeout debido a que con los cambios para usar la combo de auto-complete, si estando en la combo se presionaba el botón para grabar y cerrar
								//el diálogo, no había suficiente tiempo para que la combo actualizara el valor del grid y no estaba grabando el cambio realizado
								setTimeout(function () {
									var data = [];
									var arrfiltertext = [];
									objGridValues.forEachRow(function(id) {
										//@AAL 30/07/2015 Preparación de los datos que el usuario selecciono y que se enviaran al server para su guardado
										//JAPR 2018-11-01: Corregido un bug con el escape de caracteres en requests (#S0SRC1)
										//JAPR 2019-02-27: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
										//Ahora ya no se trata de un string separado por "," sino literal un array de opciones seleccionadas, por lo tanto no necesita codificar nada en este punto
										//var values = encodeURIComponent(this.cells(id, 1).getValue());
										var values = this.cells(id, 1).getValue();
										//JAPR
										var member = this.cells(id, 0).getValue();
										if (member != "" && values != ""){
											//JAPR 2019-02-27: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
											//Ahora values es directamente el array con las opciones seleccionadas, así que no necesita hacer split ni reemplazar delimitadores
											//JAPR 2019-02-27: Corregido un bug introducido con el issue #S0SRC1, al escapar el string del componente original se estaba generando un único string con 
											//las "," escapadas, así que el filtro ya no grababa la tabal de detalle con un registro por valor seleccionado, sino un único registro con todos los 
											//valores separados por "," (#HLCG9K)
											//values = values.replace(/%2c/gi, ",");
											data.push({id:id, data:values});
											arrfiltertext.push(member + " = {" + values.join(',') + "}");
											//data.push({id:id, data:values.split(',')});
											//arrfiltertext.push(member + " = {" + values.split(',') + "}");
											//JAPR
										}
										
									});
									//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
									//JAPR 2018-11-01: Corregido un bug con el escape de caracteres en requests (#S0SRC1)
									var filterFormula = encodeURIComponent(objFormFilters.getItemValue('txtFormula'));
									//JAPR
									data = JSON.stringify(data);
									filterName = arrfiltertext.join(" ; ");
									if (filterType == 'values')
									{
										filterFormula = '';
									}
									else
									{
										filterName = '';
									}
									var strParams = "CatalogID=" + idcatalog + "&FilterID=" + filterid + "&Process=Add&RequestType=1&ObjectType=" + otyAttribute + "&data=" + data + "&FilterName=" + filterName + "&FilterFormula=" + filterFormula + (typeofSave==1?"&User=" + idUser[0]:"&UserRol=" + idRoll[0]);
									setTimeout(function() {
										//@AAL Se manda a guardar los datos al servido
										window.dhx4.ajax.post("processRequest.php", strParams, doSaveConfirmation);
									}, 100);
								}, 100);
								//JAPR
								break;
						}
					});
				}

				//JAPR 2019-02-26: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
				/* Crea una combo con Checkboxes que inicialmente solo muestra la lista de opciones seleccionadas, pero al mostrar la combo funcionará como una auto-complete con
				carga dinámica de las opciones incluyendo las actualmente seleccionadas, incluyendo Checkboxes para permitir seleccionar diferentes opciones
				Se tuvo la necesidad de crear este componente debido a que no hay manera de modificar la combo nativa del dhtmlxGrid para habilitar los checkboxes, solo se puede hacer
				durante la inicialización de la combo con el constructor o vía XML, pero dentro de un dhtmlxGrid la inicialización es un simple tipo en un string que contiene un único valor
				por cada columna, o bien mediante la función setCellExcellType que no permite especificar el typo de "checkbox" para la combo, solo cambiar del tipo original de celda a "combo"
				para forzar a una dhtmlxCombo
				*/
				function eXcell_gridComboMultiCheck(cell) {
					if (cell) {
						this.cell = cell;
						this.grid = this.cell.parentNode.grid;
					}
					
					//La función de edición creará la combo y la llenará con todas las opciones correspondientes al texto actualmente tecleado de búsqueda, almacenando en la instancia
					//de la celda las opciones previamente cargadas para solo hacer un nuevo request en caso de haber cambiado el texto a buscar. Adicionalmente agregará las opciones
					//previamente seleccionadas
					this.edit = function() {
						var self = this;
						
						this.val = this.getValue();
						var strRowId = this.cell.parentNode.idd;
						var strComboId = strRowId + "_combo";
						//Solo se crea un div para contener la dhtmlxCombo
						this.cell.innerHTML = "<div id='" + strComboId + "' style='cursor:pointer;' class='dhx_combo_edit' >";
						this.objCombo = new dhtmlXCombo(strComboId, strComboId, undefined, "checkbox");
						//Elimina el espacio a la izquierda del input (posiblemente para mostrar una imagen de la opción seleccionada) para permitir escribir desde la orilla
						$(this.objCombo.DOMelem_input).css('margin-left', '');
						
						//Habilita la carga dinámica de las opciones de la combo, para ello la activa pero no define una URL para carga sino un dummy, ya que será personalizado
						//mediante el evento onDynXLS
						this.objCombo.enableFilteringMode(true,"dummy");
						if ( !this.fnLoadDyncOptions ) {
							this.fnLoadDyncOptions = function(text) {	// where 'text' is the text typed by the user into Combo
								//Limpia las opciones actualmente disponibles en la combo, pero primero necesita respaldar las opciones actualmente marcadas ya que esas son las que
								//se deberán mantener después de que cambie el estado de la combo según la selección (las ya no marcadas desaparecerían, self.val NO cambiará porque ese
								//es el valor que había originalmente cuando entró en modo de diseño)
								var objSelected = self.objCombo.getChecked(), objSelectedKeys = _.invert(objSelected);
								self.objCombo.clearAll();
								
								//URL para carga dinámica de valores, adicional a eso, debe dejar los valores actualmente seleccionados
								var strParams = 'action=getmembers&idcatalog=' + idcatalog + '&idattrib=' + strRowId + '&searchtext=' + text;
								var urlPath = 'loadexcel/';
								parent.objDialog.progressOn();
								window.dhx4.ajax.post(urlPath +'action.php', strParams, function(loader) {
									if (!loader || !loader.xmlDoc) {
										parent.objDialog.progressOff();
										return;
									}
									
									if (loader.xmlDoc && loader.xmlDoc.status != 200) {
										alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
										parent.objDialog.progressOff();
										return;
									}
									
									//Si llega a este punto es que se recibió una respuesta correctamente
									var response = loader.xmlDoc.responseText;
									try {
										var objResponse = JSON.parse(response);
										
										//Reconstruye las opciones de la lista además de mostrarla si es que estaba oculta, al final de la lista de opciones recibidas agregará los elementos 
										//previamente seleccionados
										var objSearchOptions = objResponse.arrValueMember[strRowId];
										if ( objSearchOptions && $.isArray(objSearchOptions) ) {
											for (var i = 0, l = objSearchOptions.length; i < l; i++) {
												var strOption = objSearchOptions[i];
												if ( !objSelectedKeys[strOption] ) {
													self.objCombo.addOption(strOption, strOption);
													//self.objCombo.setChecked(self.objCombo.getOption(strOption).index, true);
												}
											}
										}
										
										//Agrega las opciones previamente seleccionadas, independientemente de si se encontraban o no en las opciones resultantes de la búsqueda
										for (var i = 0, l = objSelected.length; i < l; i++) {
											self.objCombo.addOption(objSelected[i], objSelected[i]);
											self.objCombo.setChecked(self.objCombo.getOption(objSelected[i]).index, true);
										}
										
										self.objCombo.openSelect();
										parent.objDialog.progressOff();
									} catch(e) {
										alert(e + "\r\n" + response.substr(0, 1000));
										parent.objDialog.progressOff();
										return;
									}
									//self.objCombo.load(xml.xmlDoc.responseText);
								});
							};
						}
						
						this.objCombo.attachEvent("onDynXLS", _.debounce(this.fnLoadDyncOptions, 600));
						
						this.objCombo.attachEvent("onOpen", function(){
							console.log ('this.objCombo.attachEvent("onOpen")');
							if ( this.list && !this.list.onclick ) {
								//Esto es requerido o de lo contrario cualquier interacción con la Lista de la combo provocará que el evento click en el body entre e invoque a detach borrando la Combo
								this.list.onclick = function(e) {(e||event).cancelBubble = true;} //blocks onclick event
							}
						});
						
						/*this.objCombo.attachEvent("onFocus", function(){
							(event).cancelBubble = true;	//blocks onclick event
							(event).stopPropagation();
						});*/

						/*this.objCombo.attachEvent("onBeforeCheck", function(value, state){
							(event).cancelBubble = true;	//blocks onclick event
							(event).stopPropagation();
							return true;
						});
						
						this.objCombo.attachEvent("onCheck", function(value, state){
							(event).cancelBubble = true;	//blocks onclick event
							(event).stopPropagation();
						});*/
						
						//Agrega las opciones de respuesta seleccionadas por default a la combo
						for (var i = 0, l = this.val.length; i < l; i++) {
							this.objCombo.addOption(this.val[i], this.val[i]);
							this.objCombo.setChecked(this.objCombo.getOption(this.val[i]).index, true);
						}
						
						//Esto es requerido o de lo contrario cualquier interacción con la Combo provocará que el evento click en el grid entre e invoque a detach borrando la Combo
						this.cell.childNodes[0].onclick = function(e) {(e||event).cancelBubble = true;} //blocks onclick event
					}
					
					//La función que asigna el valor simplemente colocará una lista de opciones separadas por "," como si fuera un label
					this.setValue = function(val) {
						if (val === null) {
							return;
						}
						
						//this.setCValue(val);
						//El valor internamente grabado es un array con los valores recibidos de este atributo, pero el valor desplegado será solo el string separado por "," de dicho array
						$(this.cell).data("sel_options", val);
						
						if ( $.isArray(val) ) {
							this.setCTxtValue(val.join(','));
						}
						else {
							this.setCTxtValue(val);
						}
					}
					
					//El valor devuelto será la lista de opciones actualmente marcada de la combo
					this.getValue = function() {
						return $(this.cell).data("sel_options");
						//return this.cell.innerText;
						//return this.cell.innerHTML;
					}
					
					//Al destruir el componente se tiene que actualizar el valor con las opciones actualmente marcadas resultantes de las búsquedas de auto-completado + los cambios en los
					//checkboxes de la combo
					this.detach = function() {
						this.setValue(this.objCombo.getChecked()); //sets the new value
						//this.setValue(this.cell.childNodes[1].value); //sets the new value
						return compareArrays(this.val, this.getValue());
						//return this.val != this.getValue(); // compares the new and the old values
					}
				}
				eXcell_gridComboMultiCheck.prototype = new eXcell;    // nests all other methods from base class
				
				/* Compara 2 arrays proporcionados asumiendo que sus valores no son objetos ni otros arrays. Regresa un 1 si son iguales o 0 si hay alguina diferencia, false en caso de error */
				function compareArrays(oArr1, oArr2) {
					if ( !$.isArray(oArr1) || !$.isArray(oArr2) ) {
						return false;
					}
					
					//No tienen el mismo tamaño, por tanto son diferentes
					if ( oArr1.length != oArr2.length ) {
						return 1;
					}
					
					for (var i = 0, l = oArr1.length; i < l; i++) {
						//Los elementos en la misma posición son distintos, por lo tanto son diferentes (aunque técnicamente para este caso no importaría eso, si significaría que el
						//usuario tuvo que haber jugado con las opciones agregando y/o quitando y finalmente dejando las mismas opciones que con las que inició solo que en un orden diferente,
						//si bien eso no asegura que sea diferente, por ahora se considerará de dicha manera)
						if ( oArr1[i] != oArr2[i] ) {
							return 1;
						}
					}
					
					//Los arrays son iguales
					return 0;
				}
				//JAPR

			/*@OMMC 2015/09/04: Se agrega funcionalidad para agregar fórmulas al editor HTML*/
			/* Abre el editor de fórmula con el texto por default sText, y sobreescribe el objeto oObj al terminar si no se cancela la edición
			//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
			*/
			function fnOpenFormulaEditor(oObj, sText, iDataSourceID, htmlRange, sourceID, sourceType) {
				var intDialogWidth = 800;
				var intDialogHeight = 575;
				objDialogFn = parent.parent.objWindowsFn.createWindow({
					id:"editFormula",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialogFn.setText("<?=translate("Edit formula")?>");
				objDialogFn.denyPark();
				objDialogFn.denyResize();
				objDialogFn.setIconCss("without_icon");
				
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialogFn.attachEvent("onClose", function() {
					return true;
				});
				
				//Para permitir accesar a la celda que se recibió como parámetro, se tuvo que agregar una propiedad dinámica al diálogo, ya que se perdía la referencia dentro del
				//evento onContentLoaded
				objDialogFn.bitCellObj = oObj;
				
				//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
				if (iDataSourceID === undefined || !$.isNumeric(iDataSourceID)) {
					iDataSourceID = 0;
				}
				//Entrada por editor HTML
				var intSurveyID = 308;
				var intUserID = 107;
				iDataSourceID = idcatalog;
				if(htmlRange != undefined){
					objDialogFn.attachURL("formulaEditorForDataSourceFilter.php?SurveyID=" + intSurveyID + "&aTextEditor=" + encodeURIComponent('') + "&DataSourceID=" + iDataSourceID);
				}else{
					var objText = {aTextEditor: sText};
					objDialogFn.attachURL("formulaEditorForDataSourceFilter.php?SurveyID=" + intSurveyID /*+ "&aTextEditor=" + encodeURIComponent(sText)*/ + "&DataSourceID=" + iDataSourceID + "&sourceID=" + sourceID + "&sourceType=" + sourceType + '&UserID=' + intUserID, false, objText);
				}
				
				objDialogFn.attachEvent("onContentLoaded", function(win) {
					var objFrame = objDialogFn.getFrame();
					if (objFrame && objFrame.contentWindow && objFrame.contentWindow.document) {
						var objButton = $(objDialogFn.getFrame().contentWindow.document).find("#closeButton");
						if (objButton && objButton.length) {
							//objButton.attr('click', '');
							objButton.on('click', function() {
								if(this.getAttribute('allowClose') == 'false'){
									return;
								}
								var strValue = '';
								if (objDialogFn.bitCellObj) {
									try {
										strValue = objFrame.contentWindow.sParamReturn;
										//Si es un editor HTML el que llama al editor de Fórmulas
										if(htmlRange != undefined){
											//Obtiene el texto que se va a agregar y se lo concatena al final.
											var htmlValue = objDialogFn.bitCellObj[0].value;
											htmlValue = htmlValue.concat(strValue);
											objDialogFn.bitCellObj[0].value = htmlValue;
										}else{
											//Para un input dentro de un componente GFieldTypes.formula de un grid de propiedades
											if (objDialogFn.bitCellObj.childNodes[1]) {
												objDialogFn.bitCellObj.childNodes[1].value = strValue;
											}
											else {
												//Para un input dentro de un div cuando es su primer elemento hijo
												if (objDialogFn.bitCellObj.childNodes[0]) {
													objDialogFn.bitCellObj.childNodes[0].value = strValue;
												}
												else {
													//Para un input directo (por ejemplo, en el diálogo de preguntas, el input de la fórmula)
													objDialogFn.bitCellObj.value = strValue;
													$(objDialogFn.bitCellObj).focus();
												}
											}
										}
									} catch(e) {
										console.log("Error updating the formula : " + e);
									}
								}
								
								setTimeout(function () {
									var strRowId = "";
									var objGrid = undefined;
									if (objDialogFn.bitCellObj && objDialogFn.bitCellObj.parentNode && objDialogFn.bitCellObj.parentNode.grid) {
										var strRowId = objDialogFn.bitCellObj.parentNode.idd;
										objGrid = objDialogFn.bitCellObj.parentNode.grid;
									}
									
									if (objGrid) {
										//Por la manera en que funcionan el grid, al aceptar la fórmula se va a forzar un editStop, esto automáticamente cancelaría el evento blur porque
										//el input que ya no tenía el foco de todas maneras no habría detectado el evento keydown con <enter> así que no se consideraría pérdida de foco,
										//por el contrario se hará el grabado directamente en este punto. En resumen, NO se pondrá el foco en el input para delegarle el grabado a él, sino
										//que cancelará el modo edición y se grabará aquí mismo
										//$(objDialogFn.bitCellObj).find("input:first").focus();
										doUpdateProperty(objGrid, objGrid.bitObjectType, objGrid.bitObjectID, strRowId, 1, strValue);
										//Se envía el parámetro en true, porque de lo contrario el grid revierte el valor a lo que él consideraba el último "grabado"
										objGrid.editStop(true);
									}
									objDialogFn.bitCellObj = undefined;
									//OMMC 2015/09/08: Si el editor de fórmulas fue llamado del editor HTML, antes de cerrarse, regresa el foco al editor HTML lo que permite al valor refrescarse.
									//OMMC 2015/10/15: Validación modificada.
									if(htmlRange != undefined){
										doUnloadDialogFn();
										var redactorArea = $(htmlRange.startContainer).parent('div').last();
										var areaCount = redactorArea.find('div').length;
										if(areaCount && areaCount > 0){
											redactorArea = redactorArea.find(".redactor-editor").focus();
										}else{
											redactorArea.focus();
										}
										objDialogEd.bringToTop();
									}else{
										doUnloadDialogFn();
									}
									
								}, 100);
							});
						}
					}
				});
			}

			function doUnloadDialogFn() {
				console.log('doUnloadDialogFn');
				if (!parent.parent.objWindowsFn || !objDialogFn) {
					return;
				}
				
				var objForm = objDialogFn.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
				
				if (parent.parent.objWindowsFn.unload) {
					objDialogFn.close();
					//parent.parent.objWindowsFn.unload();
					//parent.parent.objWindowsFn = null;
					//parent.parent.objWindowsFn = new dhtmlXWindows({
					//	image_path:"images/"
					//});
				}
				//objDialogFn = null;
			}


				function fnSelectAllCListItems(oCList, bCheck) {
					if (!oCList) {
						return;
					}
					
					$(oCList).find(':checkbox').prop('checked', (bCheck?true:false));
				}
				function doUnloadDialog() {
					if (parent.objDialog) {
						var objForm = parent.objDialog.getAttachedObject();
						if (objForm && objForm.unload) {
							objForm.unload();
							objForm = null;
						}
					}
					
					if (parent.objWindows) {
						if (parent.objWindows.unload) {
							parent.objWindows.unload();
							parent.objWindows = null;
							parent.objWindows = new dhtmlXWindows({
								image_path:"images/"
							});
						}
					}
					return true;
				}

				function getMembers(idcatalog,objGrid) {
					var rows = [];
					var data = {};
					var urlPath = 'loadexcel/';
					//JAPR 2018-07-27: Modificado el request para permitir filtrar por atributo y así optimizar la carga por demanda (#2PB40B)
					$.get(urlPath +'action.php?action=getmembers&idcatalog=' + idcatalog + '&novalues=1').done(function( r ) {
						if (r.success) {
							arrvalueMembers = r.arrValueMember;
							if (r.arrMembers){
								for (var i = 0; i<r.arrMembers.length;i++){
									//JAPR 2019-02-26: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
									var intAttribID = r.fieldName[i], strValues = '';
									/*if ( Values && Values[intAttribID] ) {
										strValues = Values[intAttribID];
									}*/
									
									rows.push({id:intAttribID, data: [r.arrMembers[i], strValues]});
									//JAPR
								}
								data.rows = rows;
								objGrid.parse(data,"json");
								
								objGrid.forEachRow(function(id) {
									this.setCellTextStyle(id,1,"cursor:pointer;");
									//JAPR 2019-02-26: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
									//Inicializa la combo solo con los elementos actualmente seleccionados para este atributo
									/*var objCombo = this.getCustomCombo(id, 1);
									this.setCellExcellType(id, 1, "combo");
									for(var index in Values){
										if(index == id){
											var arrValues = Values[index][0].split(',');
											for (var intIdx in arrValues) {
												var strValue = arrValues[intIdx];
												objCombo.addOption(strValue, strValue);
											}
										}
									}*/
									//this.setCellExcellType(id, 1, "clist");
									//Asigna el array de valores directamente al widget de la celda, ya que no se debe generar como string para el parse arriba, sino dejar que el campo 
									//lo genere como necesite
									for(var index in Values){
										if(index == id){
											this.cells(id,1).setValue(Values[index]);
										}
									}
									//JAPR
								});
								objGrid.addRow('finalRow1',"");
								objGrid.addRow('finalRow2',"");
								parent.objDialog.progressOff();
							}
						}
					});
				}
				
				function doSaveConfirmation(loader) {
					console.log('doSaveConfirmation');
					
					if (!loader || !loader.xmlDoc) {
						return;
					}
					
					if (loader.xmlDoc && loader.xmlDoc.status != 200) {
						alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
						return;
					}
					
					//Si llega a este punto es que se recibió una respuesta correctamente
					var response = loader.xmlDoc.responseText;
					try {
						var objResponse = JSON.parse(response);
						//@AAL 31/07/2015 Cambiamos el nombre del filtro con el correspondiente
						parent.objFormsGrid.cells(idcatalog,3).setValue(objResponse.newObject.FilterID);
						//GCRUZ 2016-07-19. Filtro por fórmula.
						if (objResponse.newObject.FilterName == '' && objResponse.newObject.FilterFormula != '')
						{
							parent.objFormsGrid.cells(idcatalog,2).setValue(objResponse.newObject.FilterFormula);
						}
						else
						{
							parent.objFormsGrid.cells(idcatalog,2).setValue(objResponse.newObject.FilterName);
						}
						doUnloadDialog(); //Cerrar la ventana

					} catch(e) {
						alert(e + "\r\n" + response.substr(0, 1000));
						return;
					}
					
					if (objResponse.error) {
						alert(objResponse.error.desc);
						return;
					}
					else {
						if (objResponse.warning) {
							console.log(objResponse.warning.desc);
						}
					}
					
					//La respuesta no tiene error, debió haber terminado de grabar correctamente
					//alert('Todo actualizado ok: ' + response);
				}
			</script>
		</head>
		<body onload="doOnLoad();" style="width: 100%; height: 100%;">
		</body>
	</html>
<?	
		die();

	}
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aCatalogID = $aRS->fields["catalogid"];
		$anInstance = BITAMDataSourceFilter::NewInstance($aRepository, $aCatalogID);
		$anInstance->FilterID = $aRS->fields["filterid"];
		$anInstance->FilterName = $aRS->fields["filtername"];
		$anInstance->Status = $aRS->fields["status"];

		//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
		if (getMDVersion() >= esvDataSourceFilterFormula) {
			$anInstance->FilterFormula = @$aRS->fields["filterformula"];
		}
		
		//Todos los elementos de los atributos del catalogo los ponemos por 
		//default con el valor de ALL
		for($i=0; $i<$anInstance->NumAttribFields; $i++)
		{
			$fieldName = $anInstance->AttribFields[$i];
			$fieldNameSTR = $fieldName."STR";
			$anInstance->$fieldName = "sv_ALL_sv";
			$anInstance->$fieldNameSTR = "All";
		}

		
		//Obtener los usuarios a los cuales se les permite acceder a este filtro
		$anInstance->readUserIDs();
		
		//Obtener los roles a los cuales se les permite acceder a este filtro
		$anInstance->readUsrRoles();

		$anInstance->readFiler();

		//$anInstance->generateSqlWhere();

		return $anInstance;
	}
	
	function readUserIDs()
	{
		$this->UserIDs = array();
		$sql = "SELECT UserID FROM si_sv_datasourcefilterUser WHERE FilterID = ".$this->FilterID;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$this->UserIDs[] = (int)$aRS->fields["userid"];
			$aRS->MoveNext();
		}
	}

	function readUsrRoles()
	{
		$this->UsrRoles = array();
		$sql = "SELECT RolID FROM si_sv_datasourcefilterRol WHERE FilterID = ".$this->FilterID;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilterRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$this->UsrRoles[] = (int)$aRS->fields["rolid"];
			$aRS->MoveNext();
		}
	} 
	
	//@JAPR 2019-02-27: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
	/* Esta función lee el filtro pero sólo se utiliza para el proceso de definición de los mismos, ya que lo asigna a la instancia de esta clase la cual se usará dentro de este mismo
	archivo para generar la ventana de configuración. En este sentido, el filtro generado SI debe ser compatible como la ventana lo espera para que se pueda configurar correctamente, ya
	que debido al bug introducido en el issue #S0SRC1, en la base de datos se estaban grabando los filtros como una lista de valores separada por "," en un único string, por lo que esta
	función lo regresaba de la misma manera como un único valor y eso funcionaba correctamente antes del cambio del issue #L6ADN8 pues el componente finalmente así lo requería, pero dado
	a que el nuevo componente de combo auto-complete si necesita un array de valores, ahora esta función tendrá que dividir esos casos de uni-registro en tantos registros como sea necesario
	para que cada uno represente un valor individual sin mas divisiones de "," */
	function readFiler()
	{
		$this->arrFilter = array();
	 	if ($this->isNewObject()) {
	 		return;
		}
		else {
			/*Agrupo primero por miembro para poder ordenar el arreglo a ese nivel*/
			$sql = "SELECT DISTINCT T2.DataSourceMemberID
					FROM si_sv_datasourcefilter T1
						INNER JOIN si_sv_datasourcefilter_det T2
						ON T1.FilterID = T2.FilterID
					WHERE T1.FilterID = ".$this->FilterID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			while ($aRS && !$aRS->EOF){
				$arrValue = array();
				$member = $aRS->fields["datasourcememberid"];
				$sqlMember = "SELECT  T2.Value
								FROM si_sv_datasourcefilter T1
									INNER JOIN si_sv_datasourcefilter_det T2
									ON T1.FilterID = T2.FilterID
								WHERE T2.FilterID =  " . $this->FilterID . " AND  T2.DataSourceMemberID =" . $member;
				$aRS2 = $this->Repository->DataADOConnection->Execute($sqlMember);
				while ($aRS2 && !$aRS2->EOF){
					//@JAPR 2019-02-27: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
					//Agregada validación de compatibilidad debido al error introducido en el issue #S0SRC1, el cual provocaba que todos los valores se grabaran como un único registro
					//que contenía la lista de valores separada por ",". Si se detecta un valor con "," se separará en una lista para agregarlo como valores individuales (por ahora NO se
					//soportan valores reales que ya contengan una ",", se asumirá que si había alguna, era el separador de listas instroducido por el bug mencionado)
					//JAPR 2019-02-27: Corregido un bug introducido con el issue #S0SRC1, al escapar el string del componente original se estaba generando un único string con 
					//las "," escapadas, así que el filtro ya no grababa la tabal de detalle con un registro por valor seleccionado, sino un único registro con todos los 
					//valores separados por "," (#HLCG9K)
					$strValuesList = (string) @$aRS2->fields["value"];
					if ( strpos($strValuesList, ",") !== false ) {
						$arrValuesList = explode(',', $strValuesList);
						foreach ($arrValuesList as $strFilterValue) {
							if ( trim($strFilterValue) ) {
								$arrValue[]=$strFilterValue;
							}
						}
					}
					else {
						$arrValue[]=$aRS2->fields["value"];
					}
					//@JAPR
					$aRS2->MoveNext();
				}
				$this->arrFilter[$member] = $arrValue;
				$aRS->MoveNext();
			}

		}
	}

	function generateSqlWhere()
	{
		$this->sqlwhere = "";
		$arrWheremember = array();
		foreach($this->arrFilter as $position => $valor){
			$sqlMember = "SELECT fieldname FROM SI_SV_DataSourceMember WHERE memberID =" . $position;
			$aRS = $this->Repository->DataADOConnection->Execute($sqlMember);
			if ($aRS && !$aRS->EOF){
				$fieldname = $aRS->fields["fieldname"];
				$aRS->MoveNext();
				if (count($this->arrFilter[$position])>0){
					foreach ($this->arrFilter[$position]as $key2 => $value) {
						$this->arrFilter[$position][$key2] = $this->Repository->DataADOConnection->Quote($value);
					}
					$arrWheremember[] = $fieldname . " in (" . implode(",", $this->arrFilter[$position]) . ")";
				}
			}
		}
		if(count($arrWheremember)>0){
			$this->sqlwhere = implode(" AND ", $arrWheremember);
		}
	}
	static function NewInstanceFromRSCollection($aRepository, $aRS)
	{
		$aCatalogID = $aRS["catalogid"];
		$anInstance = BITAMDataSourceFilter::NewInstance($aRepository, $aCatalogID);
		$anInstance->FilterID = $aRS["filterid"];
		$anInstance->FilterName = $aRS["filtername"];
		$anInstance->Status = $aRS["status"];

		//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
		if (getMDVersion() >= esvDataSourceFilterFormula) {
			$anInstance->FilterFormula = @$aRS->fields["filterformula"];
		}
		
		for ($i=0; $i<$anInstance->NumAttribFields; $i++)
		{
			$fieldName = $anInstance->AttribFields[$i];
			$fieldNameSTR = $fieldName."STR";
			
			$anInstance->$fieldName = $aRS[strtolower($fieldName)];
			$anInstance->$fieldNameSTR = $aRS[strtolower($fieldName)];
			
			if($aRS[strtolower($fieldName)]=="sv_ALL_sv")
			{
				$anInstance->$fieldNameSTR = "All";
			}
		}
		
		//Obtener los usuarios a los cuales se les permite acceder a este filtro
		$anInstance->readUserIDs();
		
		//Obtener los roles a los cuales se les permite acceder a este filtro
		$anInstance->readUsrRoles();


		return $anInstance;
	}
	function getJSonDefinition() {
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		global $blnWebMode;
		if (!isset($blnWebMode)) {
			$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
		}

		$arrDef = array();
		$arrDef['FilterID'] = $this->FilterID;
		$arrDef['CatalogID'] = $this->CatalogID;
		$arrDef['FilterName'] = $this->FilterName;

		//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
		if (getMDVersion() >= esvDataSourceFilterFormula) {
			$arrDef['FilterFormula'] = $this->FilterFormula;
		}
		
		return $arrDef;
	}
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("CatalogID", $aHTTPRequest->GET)) {
			$aCatalogID = (int) $aHTTPRequest->GET["CatalogID"];
		}
		else if (array_key_exists("CatalogID", $aHTTPRequest->POST)) {
			$aCatalogID = (int) $aHTTPRequest->POST["CatalogID"];
		}else {
			$aCatalogID = 0;
		}

		if (array_key_exists("FilterID", $aHTTPRequest->POST))
		{
			$aFilterID = $aHTTPRequest->POST["FilterID"];
			
			if (is_array($aFilterID))
			{
				$aCollection = BITAMDataSourceFilterCollection::NewInstance($aRepository, $aCatalogID, $aFilterID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}

				$anInstance = BITAMDataSourceFilter::NewInstance($aRepository, $aCatalogID);

				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				$anInstance = BITAMDataSourceFilter::NewInstanceWithID($aRepository, (int)$aFilterID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMDataSourceFilter::NewInstance($aRepository, $aCatalogID);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();

				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}

				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMDataSourceFilter::NewInstance($aRepository, $aCatalogID);
					}
				}

				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("FilterID", $aHTTPRequest->GET))
		{
			$aFilterID = $aHTTPRequest->GET["FilterID"];
			$aFilterName = $aHTTPRequest->GET["FilterName"];
			$anInstance = BITAMDataSourceFilter::NewInstanceWithID($aRepository, (int)$aFilterID);

			if (is_null($anInstance))
			{
				$anInstance = BITAMDataSourceFilter::NewInstance($aRepository, $aCatalogID);
			}
			$anInstance->FilterName = $aFilterName;
			$anInstance->typeofSave = $aHTTPRequest->GET["typesave"];
			$anInstance->Status = $aHTTPRequest->GET["Status"];
			if ($anInstance->typeofSave == 1){
				$anInstance->UserIDs = array($aHTTPRequest->GET["UserID"]);
			}else{
				$anInstance->UsrRoles = array($aHTTPRequest->GET["UserGroupID"]);
			}
		}
		else
		{
			//@JAPR 2014-08-21: Agregado el parámetro $bSetAllValue para indicar cuando se está en la ventana de filtros y se está cargando
			//uno vacio, de forma que en eBavel no se intenten obtener valores en ese caso porque algunos campos no soportan recibir vacio
			//como valor, y realmente no tiene sentido hacer una consulta que esperamos que falle (esto NO se debería usar si se espera
			//recibir un RecordSet, porque lo que se va a hacer es un simple return de null en caso de filtrar por All)
			$anInstance = BITAMDataSourceFilter::NewInstance($aRepository, $aCatalogID, false, true);
		}

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("FilterID", $anArray))
		{
			$this->FilterID = (int)$anArray["FilterID"];
		}
		
		if (array_key_exists("FilterName", $anArray))
		{
			$this->FilterName = rtrim($anArray["FilterName"]);
		}

		//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
		if (getMDVersion() >= esvDataSourceFilterFormula) {
			if (array_key_exists("FilterFormula", $anArray))
			{
				$this->FilterFormula = (string) $anArray["FilterFormula"];
				$this->FilterFormula = BITAMDataSource::ReplaceMemberNamesByIDs($this->Repository, $this->CatalogID, $this->FilterFormula);
			}
		}

		for ($i=0; $i<$this->NumAttribFields; $i++)
		{
			$fieldName = $this->AttribFields[$i];

			if (array_key_exists($fieldName, $anArray))
			{
				$this->$fieldName = trim($anArray[$fieldName]);
			}
		}
		
	 	if (array_key_exists("UserRol", $anArray))
		{
			$this->UsrRoles = array($anArray["UserRol"]);
		}

		if (array_key_exists("User", $anArray))
		{
			$this->UserIDs = array($anArray["User"]);
		}

	 	if (array_key_exists("data", $anArray))
		{
			$this->arrFilter = array();
			$data = json_decode($anArray["data"], true);
			foreach ($data as $key => $value) {
				$memberid = $value["id"];
				$arrValues = array();
				foreach ($value["data"] as $key => $val) {
					if ($val != ""){
						$arrValues[] = $val;
					}
				}
				if (count($arrValues)>0){
					$this->arrFilter[$memberid] = $arrValues;
				}
			}
		}
		return $this;
	}
	
	function save()
	{
	 	if ($this->isNewObject())
		{
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(FilterID)", "0")." + 1 AS FilterID".
						" FROM si_sv_datasourcefilter";

			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$this->FilterID = (int)$aRS->fields["filterid"];

			$strAddFields = '';
			$strAddValues = '';
			//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
			if (getMDVersion() >= esvDataSourceFilterFormula) {
				$strAddFields .= ',FilterFormula';
				$strAddValues .= ','.$this->Repository->DataADOConnection->Quote($this->FilterFormula);
			}
			
			$sql = "INSERT INTO si_sv_datasourcefilter (".
						"CatalogID".
						",FilterID".
						",FilterName".
						",Status {$strAddFields} ".
			            ") VALUES (".
						$this->CatalogID.
						",".$this->FilterID.
						",".$this->Repository->DataADOConnection->Quote($this->FilterName).
						",".$this->Status.
						" {$strAddValues} )";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Insertamos/Actualizamos usuarios y roles para el filtro capturado
			$this->UpdateUserIDsFromFilter();
			$this->saveFilterRule();
		}
		else
		{
			$strAddFields = '';
			//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
			if (getMDVersion() >= esvDataSourceFilterFormula) {
				$strAddFields .= ',FilterFormula = '.$this->Repository->DataADOConnection->Quote($this->FilterFormula);
			}
			
			$sql = "UPDATE si_sv_datasourcefilter SET ".
					" FilterName = ".$this->Repository->DataADOConnection->Quote($this->FilterName).
					" {$strAddFields} WHERE FilterID = ".$this->FilterID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Insertamos/Actualizamos usuarios y roles para el filtro capturado
			$this->UpdateUserIDsFromFilter();
			$this->saveFilterRule();
		}
		
		//@JAPR 2013-02-21: Corregido un bug, no estaba incrementando la versión del catálogo al configurar filtros (#28327)
		$aCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		if (!is_null($aCatalog))
		{
			$aCatalog->save();
		}
		//@JAPR
		
		return $this;
	}
	function saveMember($values)
	{
		$data = json_decode($values, true);
		$memberID = $data->id;
		$sql = "DELETE FROM si_sv_datasourcefilter_det WHERE FilterID = " . $this->FilterID . "AND DataSourceMemberID = " . $memberID;
		$this->Repository->DataADOConnection->Execute($sql);
		$sql =  "SELECT ".
			$this->Repository->DataADOConnection->IfNull("MAX(FilterdetID)", "0")." + 1 AS FilterdetID".
			" FROM si_sv_datasourcefilter_det";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS || $aRS->EOF)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter_det ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		$filterdetID = (int)$aRS->fields["filterdetid"];
		foreach($data->id as $key => $value ){
			
			$sqlIns = "INSERT INTO si_sv_datasourcefilter_det (FilterdetID, FilterID, DataSourceMemberID, Value) 
			VALUES (" . $filterdetID . "," . $this->FilterID . "," . $memberID . "," . $this->Repository->DataADOConnection->Quote($value) . ")";

			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter_det ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
			$filterdetID++;
		}
	}
	function UpdateUserIDsFromFilter()
	{
		//Elimino los registros anteriores y se vuelven a insertar nuevamente:
		$sql = "DELETE FROM si_sv_datasourcefilterUser WHERE FilterID = ".$this->FilterID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Elimino los registros anteriores y se vuelven a insertar nuevamente:
		$sql = "DELETE FROM si_sv_datasourcefilterRol WHERE FilterID = ".$this->FilterID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilterRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (count($this->UserIDs)>0) {
			$sqlIns = "INSERT INTO si_sv_datasourcefilterUser (FilterID, UserID) 
						VALUES (".$this->FilterID.",".$this->UserIDs[0].")";

			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
		} else {
			$sqlIns = "INSERT INTO si_sv_datasourcefilterRol (FilterID, RolID) 
						VALUES (".$this->FilterID.",".$this->UsrRoles[0].")";

			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilterRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
		}
	}

	function saveFilterRule()
	{
		/*Primero eliminamos todas las configuraciones actuales*/
		$sql = "DELETE FROM si_sv_datasourcefilter_det WHERE FilterID = ".$this->FilterID;
		$this->Repository->DataADOConnection->Execute($sql);
		foreach($this->arrFilter as $position => $valor){
			foreach ($this->arrFilter[$position] as $key2 => $value) {
				$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(FilterdetID)", "0")." + 1 AS FilterdetID".
						" FROM si_sv_datasourcefilter_det";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter_det ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$filterdetID = (int)$aRS->fields["filterdetid"];
				$sqlIns = "INSERT INTO si_sv_datasourcefilter_det (FilterdetID, FilterID, DataSourceMemberID, Value) 
				VALUES (" . $filterdetID . "," . $this->FilterID . "," . $position . "," . $this->Repository->DataADOConnection->Quote($value) . ")";

				if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter_det ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
				}
			}
		}
	}

	function remove()
	{	
		$sql = "DELETE FROM si_sv_datasourcefilterUser WHERE FilterID = ".$this->FilterID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$sql = "DELETE FROM si_sv_datasourcefilterRol WHERE FilterID = ".$this->FilterID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilterRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$sql = "DELETE FROM si_sv_datasourcefilter WHERE FilterID = ".$this->FilterID;

		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2013-02-21: Corregido un bug, no estaba incrementando la versión del catálogo al configurar filtros (#28327)
		$aCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		if (!is_null($aCatalog))
		{
			$aCatalog->save();
		}
		//@JAPR
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->FilterID <= 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Filter");
		}
		else
		{
			return $this->FilterName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=DataSourceFilter&CatalogID=".$this->CatalogID;
		}
		else
		{
			return "BITAM_PAGE=DataSourceFilter&CatalogID=".$this->CatalogID."&FilterID=".$this->FilterID;
		}
	}

	function get_Parent()
	{
		return BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return "FilterID";
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$userIDsField = BITAMFormField::NewFormField();
		$userIDsField->Name = "StrUserRol";
		$userIDsField->Title = translate("Users");
		$userIDsField->Type = "LargeString";
		$userIDsField->Size = 1000;
		$userIDsField->IsVisible = false;
		$tmplabel="Change";
		if($this->isNewObject())
		{
			$tmplabel="Add";
		}
		$userIDsField->AfterMessage = "<span id=\"changeUserIDs\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);backgroun-position:center center;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssignUsers();\">".translate($tmplabel)."</span>";
		$myFields[$userIDsField->Name] = $userIDsField;


		for ($i=0; $i<$this->NumAttribFields; $i++)
		{
			$fieldName = $this->AttribFields[$i];
			$labelText = $this->AttribNames[$i];

			$aField = BITAMFormField::NewFormField();
			$aField->Name = $fieldName;
			$aField->Title = $labelText;
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $this->getCatalogValues($i);
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	function generateBeforeFormCode($aUser)
	{
?>
	<script language="javascript" src="js/dialogs.js"></script>
	<script language="JavaScript">
		var filterAttribIDs = new Array;
		var filterAttribNames = new Array;
		var filterAttribPos = new Array;
		var filterAttribMembers = new Array;
		var filterAttribMemberFields = new Array;
<?
		foreach ($this->AttribIDs as $position => $attribID)
		{
			$attribName = $this->AttribFields[$position];
			$attribMemberID = $this->AttribMemberIDs[$attribName]
?>
		filterAttribIDs[<?=$position?>]=<?=$attribID?>;
		filterAttribNames[<?=$position?>]='<?=$attribName?>';
		filterAttribPos['<?=$attribName?>']=<?=$position?>;
		filterAttribMembers['<?=$attribName?>']=<?=$attribMemberID?>;
		filterAttribMemberFields[<?=$attribMemberID?>]='<?=$attribName?>';
<?
		}
?>
		var numFilterAttributes = <?=$this->NumAttribFields?>;
		
<?
		if(count($this->AttribFields)>0)
		{
?>
		function refreshAttributes(attribName, event)
		{
			var event = (window.event) ? window.event : event;
			
			objAttrib = document.getElementsByName(attribName)[0];
			
			if(objAttrib.value=='sv_ALL_sv')
			{
				for(i=0; i<numFilterAttributes; i++)
				{
					var thisPosition = filterAttribPos[attribName];
					var searchAttrib = 'DSC_' + filterAttribIDs[i];
					var position = filterAttribPos[searchAttrib];
					
					if(position > thisPosition)
					{
						var obj = document.getElementsByName(searchAttrib)[0];
						obj.length = 0;
						var newOption = new Option('<?=translate("All")?>', 'sv_ALL_sv');
						obj.options[obj.options.length] = newOption;
					}
				}
				return;
			}
			
			var xmlObj = getXMLObject();
			
			if (xmlObj == null)
			{
				alert("XML Object not supported");
				return(new Array);
			}
			
			strMemberIDs="";
			strParentIDs="";
			strValues="";
			
			for(i=0; i<numFilterAttributes; i++)
			{
				var thisPosition = filterAttribPos[attribName];
				var searchAttrib = 'DSC_' + filterAttribIDs[i];
				var position = filterAttribPos[searchAttrib];
				var obj = document.getElementsByName(searchAttrib)[0];
				
				if(position <= thisPosition)
				{
					if(strMemberIDs!="")
					{
						strMemberIDs+="_SVSEPSV_";
					}
					strMemberIDs+=filterAttribMembers[searchAttrib];
					
					if(strParentIDs!="")
					{
						strParentIDs+="_SVSEPSV_";
					}
					strParentIDs+=filterAttribIDs[position];
					
					if(strValues!="")
					{
						strValues+="_SVSEPSV_";
					}
					
					strValues+=obj.value;
				}
				else
				{
					break;
				}
			}
			
		 			
		 	xmlObj.send(null);
		 	
		 	strResult = Trim(unescape(xmlObj.responseText));
			
		 	arrayAttrib = strResult.split("_SV_AT_SV_");
		 	var numAttrib = arrayAttrib.length;
		 	var refreshAttrib = new Array();
		 	
		 	for (var i=0; i<numAttrib; i++)
		 	{
		 		if(arrayAttrib[i]!='')
		 		{
			 		var atribInfo = arrayAttrib[i].split("_SV_ID_SV_");
			 		var membID = atribInfo[0];
			 		var attribValues = atribInfo[1].split("_SV_VAL_SV_");
			 		var numAtVal = attribValues.length;
					
			 		var obj = document.getElementsByName(filterAttribMemberFields[membID])[0];
			 		
			 		obj.length = 0;
			 		
					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;
					
			 		for(j=0; j<numAtVal; j++)
			 		{
						var newOption = new Option(attribValues[j], attribValues[j]);
						obj.options[obj.options.length] = newOption;
			 		}
			 		
			 		refreshAttrib[i]=membID;
		 		}
		 	}
			
		 	//Asignarle sólo la opción All a los combos que tienen orden de jerarquía mayor al orden del combo cambiado
		 	// y que no sea el combo al que se le acaban de reasignar valores en el ciclo anterior
			for(i=0; i<numFilterAttributes; i++)
			{
				var searchAttrib = 'DSC_' + filterAttribIDs[i];
				var obj = document.getElementsByName(searchAttrib)[0];
				
				if(filterAttribPos[searchAttrib] > filterAttribPos[attribName] && !inArray(refreshAttrib, filterAttribMembers[searchAttrib]))
				{
			 		obj.length = 0;
			 		 
					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;
				}
			}
		}
<?
		}
?>
		
		function openAssignUsers()
		{
			userIDsObj = BITAMDataSourceFilter_SaveForm.StrUserRol;
			//window.showModalDialog('assignCatFilterUsers.php?FilterID=<?=$this->FilterID?>', [window], "dialogHeight:320px; dialogWidth:600px; center:yes; help:no; resizable:yes; status:no");
			openWindowDialog('assignCatFilterUsers.php?FilterID=<?=$this->FilterID?>', [window], [], 600, 320, openAssignUsersDone, 'openAssignUsersDone');
			/*
			var strInfo = window.returnValue;
			
			//Actualiza la cadena de los menus visibles
			if(strInfo!=null && strInfo!=undefined)
			{
				userIDsObj.value = strInfo;
			}
			*/
		}
		
		function openAssignUsersDone(sValue, sParams)
		{
			var strInfo = sValue;
			
			//Actualiza la cadena de los menus visibles
			if(strInfo!=null && strInfo!=undefined)
			{
				userIDsObj.value = strInfo;
			}
		}
	</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
?>
	 	<script language="JavaScript">
<?
		if($this->NumAttribFields>0)
		{	
			foreach($this->AttribFields as $position => $attribName)
			{
?>
		if (document.getElementsByName('<?=$attribName?>')[0].addEventListener)
		{
			document.getElementsByName('<?=$attribName?>')[0].addEventListener("change", function(evt){ refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name, evt); }, true);
		}
		else 
		{
		    document.getElementsByName('<?=$attribName?>')[0].attachEvent("onchange", function () { refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name); })
		}
<?
			}
		}
?>
	</script>
<?
	}

	function insideEdit()
	{
?>
		objSpanChange = document.getElementById("changeUserIDs");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground.gif)';
		objSpanChange.style.cursor = 'pointer';
		objSpanChange.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChange.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChange.onclick = new Function("openAssignUsers();");
<?
	}

	function insideCancel()
	{
?>
		objSpanChange = document.getElementById("changeUserIDs");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground_off.gif)';
		objSpanChange.style.cursor = 'default';
		objSpanChange.onmouseover = "";
		objSpanChange.onmouseout = "";
		objSpanChange.onclick = "";
<?
	}

	//Carga la lista de valores del atributo especificado y regresa un array conteniendo sólo los diferentes valores
	//El parámetro $bGetRecordset permite que se regrese sólo el RecordSet en lugar de la colección de valores
	//El parámetro $bIncludeSystemFields permite recuperar también los campos de sistema como la fecha o el key de cada valor (sólo aplica si $bGetRecordset == true)
	//El parámetro $bFilterAttribPosition indica si se desea que también se aplique el filtro por el atributo en la posición $attribPosition (esto es
	//útil sólo si se quiere recuperar el id_FormId, ya que se traerá exclusivamente el valor con todos los filtros, en lugar de todos los valores
	//de dicho atributo filtrados por los padres, que era el uso original de la función para la ventana de filtros)
	//@JAPR 2014-05-30: Agregado el tipo de sección Inline
	//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar (y por tanto agrupar), pero sólo se debe utilizar
	//si este método se usará para extraer un RecordSet, ya que de lo contrario se asume que entre otras cosas se puede estar usando para la ventana
	//de filtros donde originalmente se diseñó, así que en esos casos no debe regresar mas de un campo pues para empezar el resultado de la función
	//es sólo un array de valores, no un array de campos con sus respectivos valores
	//El parámetro $sWhere permite especificar un filtro directo en lugar de armarlo mediante las propiedades de la instancia
	function getCatalogValues($attribPosition, $bGetRecordset = false, $bIncludeSystemFields = false, $bFilterAttribPosition = false, $aFieldIDs = null, $sWhere = '')
	{
		global $gblShowErrorSurvey;
		
		//@JAPR 2014-05-14: Agregados los catálogos tipo eBavel
		$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		//@JAPR 2014-05-30: Agregado el tipo de sección Inline
		//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar
		//Valida que sólo se reciban IDs de campos si se planea regresar el recordset
		if (is_null($aFieldIDs) || !is_array($aFieldIDs)) {
			$aFieldIDs = array();
		}
		if (count($aFieldIDs) > 0 && !$bGetRecordset) {
			$aFieldIDs = array();
		}
		$aFieldIDs = array_flip($aFieldIDs);
		//@JAPR
		$arrayValues = array();
		$arrayValues["sv_ALL_sv"] = translate("All");
		
		$fieldName = $this->AttribFields[$attribPosition];
		$fieldValue = $this->$fieldName;
		
		$beforeFieldName = "";
		$beforeFieldValue = "";
		
		if($attribPosition>0)
		{
			$beforeFieldName = $this->AttribFields[$attribPosition-1];
			$beforeFieldValue = $this->$beforeFieldName;
		}
		
		if($attribPosition==0 || ($attribPosition>0 && $beforeFieldValue!="sv_ALL_sv"))
		{
			//@JAPR 2014-05-14: Agregados los catálogos tipo eBavel
			if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
                /*@AAL 19/03/2015: Modificado para soportar Formas y vistas de eBavel*/
	      		if (getMDVersion() >= esvRedesign) {
	                //Si hemos seleccionado una Forma entonces pedimos sus atributos
	                if($objCatalog ->eBavelFormType == ebftForms){
	                    //Carga la definición de la forma de eBavel a utilizar
	                    $arreBavelFormColl = @GetEBavelSections($this->Repository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
	                    if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0)
	                		return $arrayValues;
	                    $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Forms');        
	                }else{//Si hemos seleccionado una Vista entonces solicitamos sus atributos.
	                    //Carga la definición de la forma de eBavel a utilizar
	                    $arreBavelFormColl = @GetEBavelViews($this->Repository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
	                    if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0)
	                    	return $arrayValues;
	                    $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Views');
	                }
	      		}
	      		else{//Se trata de un catálogo extraído de eBavel, así que obtiene todos los valores filtrando por los padres usando las clases de eBavel
					$arreBavelFormColl = @GetEBavelSections($this->Repository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
					if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0)
						return $arrayValues;
					$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');
	      		}

				$objForm = $arreBavelFormColl[0];
				$strFormID = $objForm['id'];
				//@JAPR 2014-05-16: Agregados los catálogos tipo eBavel
				$this->FormID = $strFormID;
				//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
				$this->RSFields = array();
				
				$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
				$intFieldIDToGet = 0;
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				$intFieldDataIDToGet = ebfdValue;
				//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
				$strFieldDescToGet = '';
				$arreBavelFieldIDsStr = array();	//array('id_'.$strFormID);
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				//Este array sólo contiene la indicación de que campos de eBavel fueron utilizados, no importa si Geolocation está o no unificado
				$arreBavelFieldIDs = array();
				//Este aray contiene los valores de cada campo, en el caso de Geolocation se tiene que manejar unificado, pero lo hará en 2
				//partes concatenando según como lleguen los valores
				$arreBavelFieldIDsValue = array();
				//Este array realmente no se utilizó
				$arreBavelFieldIDsName = array();
				//Este array realmente no se utilizó
				$arreBavelFieldIDsNameID = array();
				//Este array contiene los nombres de campos genéricos basados en CLA_DESCRIP que corresponden con los campos pedidos a eBavel,
				//en el caso de los de Geolocation, sólo uno de los dos atributos se agregaría al array, así que se tendría que procesar el
				//correcto al momento de resolver el query
				$arrFieldDescToGet = array();
				foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
					if ($objCatalogMember->MemberOrder > $attribPosition+1) {
						break;
					}
					
					//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
					$intEBavelFieldID = $objCatalogMember->eBavelFieldID;
					if ($intEBavelFieldID > 0) {
						$arreBavelFieldIDs[$intEBavelFieldID] = $intEBavelFieldID;
						$tempFieldName = $this->AttribFields[$objCatalogMember->MemberOrder -1];
						$tempFieldValue = $this->$tempFieldName;
						//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						//Este aray contiene los valores de cada campo, en el caso de Geolocation se tiene que manejar unificado, pero lo hará en 2
						//partes concatenando según como lleguen los valores
						//@JAPR 2014-11-14: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						//Los campos Geolocation no se pueden usar a la fecha de implementación directamente con la función where, así que se
						//preparará un whereRaw para agregarlo adicional a los where normales que pudieran existir
						$strSuffix = '';
						switch ($objCatalogMember->eBavelFieldDataID) {
							case ebfdLatitude:
								/*
								//Si es la latitude y ya había un valor previo, sólo puede significar que era la longitude, así que concatena al inicio
								if (!isset($arreBavelFieldIDsValue[$intEBavelFieldID]) || trim($arreBavelFieldIDsValue[$intEBavelFieldID]) == '') {
									$arreBavelFieldIDsValue[$intEBavelFieldID] = $tempFieldValue;
								}
								else {
									$arreBavelFieldIDsValue[$intEBavelFieldID] = $tempFieldValue.','.$arreBavelFieldIDsValue[$intEBavelFieldID];
								}
								*/
								$strSuffix = '_L';
								break;
								
							case ebfdLongitude:
								/*
								//Si es la longitude y ya había un valor previo, sólo puede significar que era la latitude, así que concatena al final
								if (!isset($arreBavelFieldIDsValue[$intEBavelFieldID]) || trim($arreBavelFieldIDsValue[$intEBavelFieldID]) == '') {
									$arreBavelFieldIDsValue[$intEBavelFieldID] = $tempFieldValue;
								}
								else {
									$arreBavelFieldIDsValue[$intEBavelFieldID] .= ','.$arreBavelFieldIDsValue[$intEBavelFieldID];
								}
								*/
								$strSuffix = '_A';
								break;
							/*
							case ebfdValue:
							default:
								$arreBavelFieldIDsValue[$intEBavelFieldID] = $tempFieldValue;
								break;
							*/
						}
						
						//Tiene que concatenar el sufijo por si era un campo Geolocation que se tiene que pedir por medio de sus diferentes
						//componentes, de lo contrario no es necesario el sufijo
						$strEBavelFieldID = $intEBavelFieldID;
						if ($strSuffix) {
							$strEBavelFieldID .= $strSuffix;
						}
						$arreBavelFieldIDsValue[$strEBavelFieldID] = $tempFieldValue;
						//@JAPR
						
						$arreBavelFieldIDsName[$intEBavelFieldID] = $tempFieldName;
						$arreBavelFieldIDsNameID[$intEBavelFieldID] = '';
						//@JAPR 2014-05-30: Agregado el tipo de sección Inline
						//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar
						//Se agregarán todos los campos que se encuentren en el array especificado + el pedido por posición, por lo que siempre se
						//asume que el pedido por posición tiene que ser como mínimo el último en el orden de los especificados en el array
						if ($objCatalogMember->MemberOrder == $attribPosition+1 || isset($aFieldIDs[$intEBavelFieldID])) {
							if ($objCatalogMember->MemberOrder == $attribPosition+1) {
								$intFieldIDToGet = $intEBavelFieldID;
								//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
								$intFieldDataIDToGet = $objCatalogMember->eBavelFieldDataID;
								//@JAPR
							}
							
							//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
							$strFieldDescToGet = "DSC_".$objCatalogMember->ClaDescrip;
							$arrFieldDescToGet[$intEBavelFieldID] = $strFieldDescToGet;
						}
					}
					//@JAPR
				}
				
				if (count($arreBavelFieldIDs) == 0) {
					return $arrayValues;
				}
				
				//Siempre el primer campo es el ID consecutivo único del valor
				$strFieldIDToGet = '';
				$anBavelFieldIDsColl = array(); //array(array('id' => 'id_'.$strFormID, 'tagname' => 'widget_field_numeric', 'label' => 'id', 'section_id' => $objCatalog->eBavelFormID, 'appId' => $objCatalog->eBavelAppID, 'id_fieldform' => -1, 'key' => -1, 'formID' => $strFormID));
				foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
					foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
						if (isset($arreBavelFieldIDs[$intFieldID])) {
							$arreBavelFieldIDsStr[$intFieldID] = $arrFieldData['id'];
							$anBavelFieldIDsColl[] = $arrFieldData;
							$arreBavelFieldIDsNameID[$intFieldID] = $arrFieldData['id'];
							//@JAPR 2014-05-30: Agregado el tipo de sección Inline
							//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar
							//Se agregarán todos los campos que se encuentren en el array especificado + el pedido por posición, por lo que siempre se
							//asume que el pedido por posición tiene que ser como mínimo el último en el orden de los especificados en el array
							//if ($intFieldID == $intFieldIDToGet) {
							if ($intFieldID == $intFieldIDToGet || isset($aFieldIDs[$intFieldID])) {
								$strFieldIDToGet = $arrFieldData['id'];
								//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
								$strFieldDescToGet = $arrFieldDescToGet[$intFieldID];
								$this->RSFields[$strFieldDescToGet] = $strFieldIDToGet;
							}
						}
					}
				}
				
				require_once('settingsvariable.inc.php');
				$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'EBAVELURL');
				if (is_null($objSetting) || trim($objSetting->SettingValue == '')) {
					return $arrayValues;
				}
				
				//Verica si el código de eBavel está disponible
			    $strPath = '';
			    $streBavelPath = '';
				$arrURL = parse_url($objSetting->SettingValue);
				$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
				$strPath = str_replace('/', '\\', $strPath);
				$strPath = str_replace('\\\\', '\\', $strPath);
				if (file_exists($strPath))
				{
					@require_once($strPath);
					$streBavelPath = str_ireplace('service.php', '', $strPath);
				}
				
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					if (!function_exists('NotifyError')) {
						echo("<br>\r\nNo NotifyError function");
					}
					else {
						echo("<br>\r\nNotifyError function exists");
					}
				}
				
				if (!class_exists('Tables'))
				{
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\nNo eBavel code loaded");
					}
					return $arrayValues;
				}
                /*@AAL 19/03/2015: Modificado para verificar si existen las clases DBForms y DBViews*/
				if (!class_exists('DBForm') || !class_exists('DBView'))
				{
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\nNo eBavel code loaded");
					}
					return $arrayValues;
				}
				
				//@JAPR 2014-05-20: Corregido un bug, para que eBavel use seguridad, necesita tener esta variable de sesión asignada
				//@JAPR 2016-07-06: Corregido un bug, para el acceso al API simplemente se sobrescriben las variables, condicionarlo de esta manera evitaba que se pudiera usar el
				//login de eForms independiente (sin pasar por KPIOnline) ya que ese no sobreescribe estas variables, así que quedaban mezcladas (#S36ANP)
				//if (is_null(@$_SESSION["BITAM_UserName"]) || trim($_SESSION["BITAM_UserName"]) == '') {
				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				//@session_register("BITAM_UserName");
				$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
				$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
				$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
				//}
				//@JAPR
				
				$this->Repository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
				//@JAPR 2014-05-30: Agregado el tipo de sección Inline
				//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar
				//$objQuery = DBForm::name($strFormID)->fields(array($strFieldIDToGet));
                
                /*@AAL 19/03/2015: Modificado para soportar Formas y vistas de eBavel*/
	      		if (getMDVersion() >= esvRedesign) {
	      			if($objCatalog ->eBavelFormType == ebftForms)
	                    $objQuery = DBForm::name($strFormID)->fields(array_values($this->RSFields));
	                else
	                    $objQuery = DBView::name($strFormID)->fields(array_values($this->RSFields));
	                //@AAL
	      		}
	      		else
	      			$objQuery = DBForm::name($strFormID)->fields(array_values($this->RSFields));
                
                
				//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
				//Agregado el GROUP BY por el campo que se está pidiendo
				//@$objQuery->groupBy(array($strFieldIDToGet));
				@$objQuery->groupBy(array_values($this->RSFields));
				//$objQuery->onlyFields(false);
				//Si sólo se pedirá el RecordSet, hay que validar si se incluyen o no los campos de sistema
				if ($bGetRecordset) {
					$objQuery->onlyFields(!$bIncludeSystemFields);
				}
				
				//Genera el filtro para eBavel
				//@JAPR 2014-05-30: Agregado el tipo de sección Inline
				//Si hay un filtro directo, entonces simplemente se agrega a la clase en lugar de recorrer las propiedades
				if (trim($sWhere) != '') {
					$objQuery->whereRaw($sWhere);
				}
				else {
					//El filtro se arma directamente desde la instancia
					$strAnd = '';
					$intAdd = ($bFilterAttribPosition)?2:1;
					//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
					//Validado que no se puedan reutilizar los mismos campos en atributos diferentes, esto porque adicionalmente los campos
					//Geolocation se dividieron en sus porciones de Latitude y Longitude, así que no puede mandar el filtro por separado ya que
					//eBavel lo maneja unificado
					$strWhereRaw = '';
					$strAndRaw = '';
					$arrProcessedFields = array();
					//@JAPR
					foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
						if ($objCatalogMember->MemberOrder >= $attribPosition+$intAdd) {
							break;
						}
						
						$intFieldID = $objCatalogMember->eBavelFieldID;
						//@JAPR 2014-11-14: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						//Los campos Geolocation no se pueden usar a la fecha de implementación directamente con la función where, así que se
						//preparará un whereRaw para agregarlo adicional a los where normales que pudieran existir
						$strSuffix = '';
						if ($objCatalogMember->eBavelFieldDataID > ebfdValue) {
							$blnValid = true;
							switch ($objCatalogMember->eBavelFieldDataID) {
								case ebfdLatitude:
									$blnValid = false;
									$strSuffix = '_L';
									break;
								case ebfdLongitude:
									$blnValid = false;
									$strSuffix = '_A';
									break;
							}
							
							//Si no es un atributo válido para un filtro con la función where, significa que es un atributo que a la fecha de
							//implementación se tenía que filtrar con un whereRaw, así que modifica dicho filtro y continua con el siguiente
							//atributo en el order
							if (!$blnValid) {
								if ($intFieldID > 0 && isset($arreBavelFieldIDsStr[$intFieldID])) {
									$strEBavelFieldID = $intFieldID;
									if ($strSuffix) {
										$strEBavelFieldID .= $strSuffix;
									}
									$streBavelFieldIDsValue = $arreBavelFieldIDsValue[$strEBavelFieldID];
									//@JAPR 2014-12-18: Corregido un bug, faltaba validar contra NULL para cuando no tiene valor este campo
									if (!is_null($streBavelFieldIDsValue)) {
										if ($streBavelFieldIDsValue == 'sv_ALL_sv') {
											return $arrayValues;
										}
										
										//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
										$strWhereRaw .= $strAndRaw."`".$arreBavelFieldIDsStr[$intFieldID].$strSuffix."`".'='.$streBavelFieldIDsValue;
										$strAndRaw = ' AND ';
									}
									//@JAPR
								}
								
								continue;
							}
						}
						//@JAPR
						
						if ($intFieldID > 0 && isset($arreBavelFieldIDsStr[$intFieldID])) {
							//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
							//Validado que sólo intente aplicar filtros si realmente se especificaron
							//@JAPR 2014-11-14: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
							//Los campos Geolocation no se pueden usar a la fecha de implementación directamente con la función where, así que se
							//preparará un whereRaw para agregarlo adicional a los where normales que pudieran existir
							$strEBavelFieldID = $intFieldID;
							if ($strSuffix) {
								$strEBavelFieldID .= $strSuffix;
							}
							$streBavelFieldIDsValue = $arreBavelFieldIDsValue[$strEBavelFieldID];
							if (!is_null($streBavelFieldIDsValue)) {
								//@JAPR 2014-08-21: Validado que si se recibe el valor de All (gracias al nuevo parámetro $bSetAllValue)
								//en alguno de los atributos previos, entonces simplemente se regrese el array vacio (con el valor de All)
								//porque quiere decir que al menos alguno de los padres no ha sido filtrado. Teóricamente no debería entrar
								//a esta parte, porque el IF que encierra a esto ya validó contra este valor, pero con esto se verifican
								//los atributos previos también
								if ($streBavelFieldIDsValue == 'sv_ALL_sv') {
									return $arrayValues;
								}
								//@JAPR
								if ($streBavelFieldIDsValue == '') {
									continue;
								}
								//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
								$objQuery->where($arreBavelFieldIDsStr[$intFieldID], '=', $streBavelFieldIDsValue, $strAnd);
							}
							//@JAPR
						}
						$strAnd = 'AND';
						//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						$arrProcessedFields[$intFieldID] = $intFieldID;
						//@JAPR
					}
					
					//@JAPR 2014-11-14: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
					//Los campos Geolocation no se pueden usar a la fecha de implementación directamente con la función where, así que se
					//preparará un whereRaw para agregarlo adicional a los where normales que pudieran existir
					if ($strWhereRaw != '') {
						$objQuery->whereRaw($strWhereRaw, $strAnd);
					}
				}
				//@JAPR
				
				$sql = '';
				if (method_exists($objQuery, 'compileQuery')) {
					//@JAPR 2014-08-19: Agregada esta asignación por compatibilidad con eBavel a partir de cambios de esta fecha
					//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
					//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
					//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
					//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
					//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
					global $eBavelServiceVersion;
					global $theUser;
					if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
						$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
						Metadata::setRepository($aneBavelRepository);
					}
					else {
						if (class_exists('Metadata')) {
							@Metadata::setRepository($this->Repository);
						}
					}
					//@JAPR
					
					$sql = (string) @$objQuery->compileQuery($this->Repository->ADOConnection);
					global $queriesLogFile;
					$aLogString="\r\n--------------------------------------------------------------------------------------";
					$aLogString.="\r\BITAMDataSourceFilter->getCatalogValues: \r\n";
					$aLogString.=$sql;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n DataSourceFilter->getCatalogValues eBavel Query: {$sql}");
					}
					@updateQueriesLogFile($queriesLogFile,$aLogString);
				}
				
				$aRS = $objQuery->get($this->Repository->ADOConnection);
			   	$this->Repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
				if ($aRS === false)
				{
					//@JAPR 2014-09-12: Corregido un bug, se estaba reportando el error con la conexión equivocada
					if($gblShowErrorSurvey) {
						die("(".__METHOD__.") ".translate("Error accessing")." ".$strFormID." ".translate("form").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					else {
						return("(".__METHOD__.") ".translate("Error accessing")." ".$strFormID." ".translate("form").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
				
				//Si solo se pidió el recordset, entonces se regresa directamente para que lo procese quien invoca al método, pero no se puede usar así
				//como parte del FrameWork
				if ($bGetRecordset) {
					return $aRS;
				}
				
				//@JAPR 2014-05-20: Agregada validación para controlar cuando eBavel no pueda regresar el RecordSet
				if (property_exists($aRS, 'EOF')) {
					while(!$aRS->EOF)
					{
						$value = $aRS->fields[$strFieldIDToGet];
						//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						switch ($intFieldDataIDToGet) {
							case ebfdLatitude:
								$value = (float) @$aRS->fields[$strFieldIDToGet."_L"];
								break;
							case ebfdLongitude:
								$value = (float) @$aRS->fields[$strFieldIDToGet."_A"];
								break;
						}
						//@JAPR
						$arrayValues[$value] = $value;
						$aRS->MoveNext();
					}
				}
				//@JAPR
			}
			else {
				$sql = "SELECT ".$fieldName." FROM ".$this->TableName;
				$keyField = $this->TableName."KEY";
				$where = " WHERE ".$keyField." <> 1";
				//@JAPR 2014-12-10: Agregado el soporte para el parámetro $sWhere en catálogos de eForms, ya que hasta esta fecha sólo se había
				//utilizado en catálogos de eBavel
				if (trim($sWhere) != '') {
					$where .= " AND ".$sWhere;
				}
				else {
					for($i=0;$i<$attribPosition;$i++)
					{
						$tempFieldName = $this->AttribFields[$i];
						$tempFieldValue = $this->$tempFieldName;
						if (isset($this->AttribFields[$i]) && $tempFieldValue != '') {
							$where.=" AND ".$tempFieldName." = ".$this->Repository->DataADOConnection->Quote($tempFieldValue);
						}
					}
				}
				//@JAPR
				
				$sql.=$where;
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS === false)
				{
					if($gblShowErrorSurvey) {
						die("(".__METHOD__.") ".translate("Error accessing")." ".$this->TableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					else {
						return("(".__METHOD__.") ".translate("Error accessing")." ".$this->TableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
				
				while(!$aRS->EOF)
				{
					$field = strtolower($fieldName);
					$value = $aRS->fields[$field];
					$arrayValues[$value] = $value;
					$aRS->MoveNext();
				}
			}
			//JAPR
		}
		
		return $arrayValues;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}

class BITAMDataSourceFilterCollection extends BITAMCollection
{
	public $CatalogID;
	public $AttribFields;
	public $AttribNames;
	public $AttribIDs;
	public $NumAttribFields;
	public $TableName;
	public $ModelID;
	
	function __construct($aRepository, $aCatalogID)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->CatalogID = $aCatalogID;
		
		$this->AttribFields = array();
		$this->AttribNames = array();
		$this->NumAttribFields = 0;
		
		//se asigna -1 porque estas dimensiones no estan ligadas a un cubo en especifico
		$this->ModelID = -1;
		
		$anInstanceCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $this->CatalogID);
		$this->TableName = $anInstanceCatalog->TableName;
		
		$attributes = BITAMCatalogMemberCollection::NewInstance($aRepository, $this->CatalogID);
		$numFields = count($attributes->Collection);

		for($i=0; $i<$numFields; $i++)
		{
			$this->AttribFields[$i] = 'DSC_'.$attributes->Collection[$i]->ClaDescrip;
			$this->AttribIDs[$i] = $attributes->Collection[$i]->ClaDescrip;
			$this->AttribNames[$i] = $attributes->Collection[$i]->MemberName;
		}
		
		$this->NumAttribFields = $numFields;
	}

	static function NewInstance($aRepository, $aCatalogID, $anArrayOfFilterIDs = null)
	{
		$anInstance = new BITAMDataSourceFilterCollection($aRepository, $aCatalogID);
		
		$filter = "";

		if (!is_null($anArrayOfFilterIDs))
		{
			switch (count($anArrayOfFilterIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND FilterID = ".((int)$anArrayOfFilterIDs[0]);
					break;
				default:
					foreach ($anArrayOfFilterIDs as $aFilterID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aFilterID; 
					}
					
					if ($filter != "")
					{
						$filter = " AND FilterID IN (".$filter.")";
					}
					break;
			}
		}

		$sql = "SELECT FilterID, CatalogID, FilterName FROM si_sv_datasourcefilter WHERE CatalogID = ".$aCatalogID.$filter." ORDER BY FilterID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." si_sv_datasourcefilter ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		$arrayCollections =  array();
		$arrayFilterIDs = array();
		$arrayFilterNames = array();

		while(!$aRS->EOF)
		{
			$filterID = (int)$aRS->fields["filterid"];
			$filterName = $aRS->fields["filtername"];
			$arrayFilterIDs[] = $filterID;
			$arrayFilterNames[] = $filterName;
			$aRS->MoveNext();
		}
		
		$arrayValuesRS = array();
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("CatalogID", $aHTTPRequest->GET))
		{
			$aCatalogID = (int)$aHTTPRequest->GET["CatalogID"];
		}
		else
		{
			$aCatalogID = 0;
		}

		return BITAMDataSourceFilterCollection::NewInstance($aRepository, $aCatalogID);
	}
	
	function get_Parent()
	{
		return BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
	}
	
	function get_Title()
	{	
		return translate("Filters");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=Catalog&CatalogID=".$this->CatalogID;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=DataSourceFilter&CatalogID=".$this->CatalogID;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'FilterID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		for ($i=0; $i<$this->NumAttribFields; $i++)
		{
			$fieldName = $this->AttribFields[$i];
			$fieldNameSTR = $fieldName."STR";
			$labelText = $this->AttribNames[$i];

			$aField = BITAMFormField::NewFormField();
			$aField->Name = $fieldNameSTR;
			$aField->Title = $labelText;
			$aField->Type = "String";
			$aField->Size = 255;
			
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}
?>