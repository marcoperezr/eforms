<?php
require_once("utils.inc.php");
require_once('surveyAgendaScheduler.inc.php');

/* Recorre todos los Schedulers de Agendas definidos y genera la agenda correspondiente para aquellos que cumple la frecuencia tanto del día actual como de mañana, para todos los
usuarios que tengan configurado el Scheduler. Es idéntico al proceso de descarga de definiciones desde el App, sólo que en este caso para todos los usuarios. Se invoca desde el Agente
de eForms pero puede invocarse de manera independiente mediante el servicio adecuado
*/
function SyncAgendas($aRepository, $theAppUser) {
	global $appVersion;
	global $kpiUser;
	global $gbDesignMode;
	
	$return = array();
	$return['error'] = false;
	
	$strOriginalWD = getcwd();
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
        ECHOString("Processing Agenda Schedulers");
	}
	
	//El proceso realmente no genera errores, simplemenete se registrará en el log el proceso realizado, cualquier error existente sería algún die no planeado dentro del proceso,
	//probablemente de las versiones originales y sin validar, ya que es el mismo proceso de obtención de definiciones del App y ese no debería generar dies
	BITAMSurveyAgendaSchedulerCollection::GenerateAgendasFromAgendaSchedulersByUser($aRepository, -2);
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
        ECHOString("Process finished");
	}
	
	if (!$return['error']) {
		$return['errmsg'] = "Agenda Schedulers were processed successfully.";
	}
	return $return;
}
?>