<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);
	LoadLanguageWithName("EN");
}

//@JAPR 2014-04-09: Agregada la variable para identificar el envío de reportes desde la captura o desde la opción para forzarlo en la lista de reportes
global $blnSendPDFEntries;
//Con esta variable se permitirá que en los métodos que envían el reporte vía EMail (NotifyNewSurveyCapturedByMail y NotifySurveyDashboardPDFByMail)
//se controle si se mandan a todos los correos o sólo a algunos para depuración, incluso en proyectos que se encuentren en producción, esto se logra
//modificando las funciones de envío para preguntar por esta variable y agregar como destinatario a las cuentas de prueba siempre y cuando esté
//en true esta bandera, porque de lo contrario se trata del envío como parte de una captura y por tanto no se debe alterar
$blnSendPDFEntries = true;
//@JAPR

$userFullName = $theUser->FullName;

$surveyID = 0;
if(array_key_exists("surveyID", $_GET))
{
	$surveyID = $_GET["surveyID"];
}

require_once("appuser.inc.php");
require_once("survey.inc.php");
require_once("processSurveyFunctions.php");

$reportIDs = array();
$reportIDs = explode(",", $_GET["ReportSendPDFIDs"]);
$aRepository = $theRepository;
$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
$userID = isset($_GET["userID"])?$_GET["userID"]:"";
$sendEmailDashboards = true;

foreach($reportIDs as $factKeyDimVal) {
    //var_dump($userID);
    
    $statusSendDashboards = sendDashboards($aRepository, $surveyInstance, $userID, $factKeyDimVal, $sendEmailDashboards);
    if(!$statusSendDashboards)
    {
        //Solo si esta activado el centinela entonces se enviara el correo electronico del PDF
        
    }
}
?>