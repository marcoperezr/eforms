<?php
global $widthByDisp;
$myFormName = get_class($this);
?>
&nbsp;
<br>

<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
				visibleDivArray['QuestionMessage'] = 'QuestionMessageDefaultDesHDiv';
				function changeTab(oEvt, sPropName) {
					if (!oEvt || !sPropName) {
						return;
					}
					objEditorType=<?=$myFormName?>_SaveForm.EditorType;
					if(objEditorType)
					{
						editorTypeValue = objEditorType.options[objEditorType.selectedIndex].value;
					}
					else
					{
						editorTypeValue = 0;
					}
					
					//Estas propiedades siempre utilizarán el editor anterior por que
					//sólo se inserta una imagen, aunque el combo selector de editor tenga
					//seleccionado el nuevo estos dos campos utilizarán el tinymce
					if(sPropName=='QuestionImageText' || sPropName=='QuestionImageSketch')
					{
						editorTypeValue = 0;
					}
					
					if(editorTypeValue==0)
					{
						var strTabId = sPropName+oEvt.getAttribute('name')+'Div';
					}
					else
					{
						var strTabId = sPropName+oEvt.getAttribute('name')+'DesDiv';
						visibleDivArray[sPropName] =  sPropName+oEvt.getAttribute('name')+'DesHDiv';
					}
					
					document.getElementById(sPropName+'DefaultDiv').style.display = 'none';
					var objDiv = document.getElementById(sPropName+'iPodDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadMiniDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPhoneDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'CelDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'TabletDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					
					var objDiv = document.getElementById(sPropName+'DefaultDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPodDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadMiniDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPhoneDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'CelDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'TabletDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					
					var objDiv = document.getElementById(strTabId);
					if (objDiv) {
						objDiv.style.display = 'block';
					}
					try {
						var objDiv = document.getElementById(sPropName+'Menu');
						if (objDiv) {
							var arrLi = objDiv.getElementsByTagName('li');
							if (arrLi) {
								for (var intIdx in arrLi) {
									arrLi[intIdx].className = "menuOption";
								}
							}
						}
						
						if (oEvt) {
							oEvt.className = "menuOption selectedTab"						
						}
					} catch(e) {}
				}
				
				var tinyMCEmode = true; //Conchita 16-nov-2011 cambio para toggle tinymce
					tinyMCE.init({
						// General options
						mode : "exact",
						readonly : 0,
				        elements : "QuestionImageText,ajaxfilemanager",
				      	theme : "advanced",
				      	width: 150,
        				height: 150,
						file_browser_callback : "ajaxfilemanager",
						handle_event_callback : "formattedtextchanged",
						entity_encoding : "raw",
						object_resizing : true,
						theme_advanced_buttons1 : "image",
						theme_advanced_buttons2: "",
						theme_advanced_buttons3: "",
						theme_advanced_buttons4: "",		
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "none",
						theme_advanced_resizing : true,
						//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
						theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
        										"Arial=arial,helvetica,sans-serif;"+
        										"Arial Black=arial black,avant garde;"+
        										"Book Antiqua=book antiqua,palatino;"+
								                "Comic Sans MS=comic sans ms,sans-serif;"+
								                "Courier New=courier new,courier;"+
								                "Georgia=georgia,palatino;"+
								                "Helvetica=helvetica;"+
								                "Impact=impact,chicago;"+
								                "Symbol=symbol;"+
								                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
								                "Terminal=terminal,monaco;"+
								                "Times New Roman=times new roman,times;"+
								                "Trebuchet MS=trebuchet ms,geneva;"+
								                "Verdana=verdana,geneva;"+
								                "Webdings=webdings;"+
								                "Wingdings=wingdings,zapf dingbats",
   						relative_urls : false,
						content_css : "",
						convert_urls : false,
						editor_selector : "mceSimple",
						// Drop lists for link/image/media/template dialogs
						external_link_list_url : "lists/link_list.js",
						save_callback : "myCustomSaveContent",
						oninit: "myInitTinyMCE",
						cleanup: false,
						verify_html : false,
						forced_root_block : false
					});
					
					tinyMCE.init({
						// General options
						mode : "exact",
						readonly : 0,
				        elements : "QuestionMessage,ajaxfilemanager",
				      	theme : "advanced",
						file_browser_callback : "ajaxfilemanager",
						handle_event_callback : "formattedtextchanged",
						entity_encoding : "raw",
						object_resizing : true,
						theme_advanced_buttons1 : "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink,image",
						theme_advanced_buttons2: "",
						theme_advanced_buttons3: "",
						theme_advanced_buttons4: "",		
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "none",
						theme_advanced_resizing : true,
						//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
						theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
        										"Arial=arial,helvetica,sans-serif;"+
        										"Arial Black=arial black,avant garde;"+
        										"Book Antiqua=book antiqua,palatino;"+
								                "Comic Sans MS=comic sans ms,sans-serif;"+
								                "Courier New=courier new,courier;"+
								                "Georgia=georgia,palatino;"+
								                "Helvetica=helvetica;"+
								                "Impact=impact,chicago;"+
								                "Symbol=symbol;"+
								                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
								                "Terminal=terminal,monaco;"+
								                "Times New Roman=times new roman,times;"+
								                "Trebuchet MS=trebuchet ms,geneva;"+
								                "Verdana=verdana,geneva;"+
								                "Webdings=webdings;"+
								                "Wingdings=wingdings,zapf dingbats",
   						relative_urls : false,
						content_css : "",
						convert_urls : false,
						editor_selector : "mceAdvanced",
						// Drop lists for link/image/media/template dialogs
						external_link_list_url : "lists/link_list.js",
						save_callback : "myCustomSaveContent",
						oninit: "myInitTinyMCE",
						cleanup: false,
						verify_html : false,
						forced_root_block : false
					});
					
					var arrResponsiveDesignObjectsMsg = new Array();
					var arrResponsiveDesignObjectsImg = new Array();
<?
		//@JAPR 2014-08-01: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
					arrResponsiveDesignObjectsMsg.push('QuestionMessageiPod');
					arrResponsiveDesignObjectsMsg.push('QuestionMessageiPad');
					arrResponsiveDesignObjectsMsg.push('QuestionMessageiPadMini');
					arrResponsiveDesignObjectsMsg.push('QuestionMessageiPhone');
					arrResponsiveDesignObjectsMsg.push('QuestionMessageCel');
					arrResponsiveDesignObjectsMsg.push('QuestionMessageTablet');
					
					arrResponsiveDesignObjectsImg.push('QuestionImageTextiPod');
					arrResponsiveDesignObjectsImg.push('QuestionImageTextiPad');
					arrResponsiveDesignObjectsImg.push('QuestionImageTextiPadMini');
					arrResponsiveDesignObjectsImg.push('QuestionImageTextiPhone');
					arrResponsiveDesignObjectsImg.push('QuestionImageTextCel');
					arrResponsiveDesignObjectsImg.push('QuestionImageTextTablet');
<?
		}
		//@JAPR
?>
					
					for (var intIdx in arrResponsiveDesignObjectsMsg) {
						tinyMCE.init({
							// General options
							mode : "exact",
							readonly : 0,
					        elements : arrResponsiveDesignObjectsMsg[intIdx] + ",ajaxfilemanager",
					      	theme : "advanced",
							file_browser_callback : "ajaxfilemanager",
							handle_event_callback : "formattedtextchanged",
							entity_encoding : "raw",
							object_resizing : true,
							theme_advanced_buttons1 : "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink,image",
							theme_advanced_buttons2: "",
							theme_advanced_buttons3: "",
							theme_advanced_buttons4: "",		
							theme_advanced_toolbar_location : "top",
							theme_advanced_toolbar_align : "left",
							theme_advanced_statusbar_location : "none",
							theme_advanced_resizing : true,
							//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
							theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
	        										"Arial=arial,helvetica,sans-serif;"+
	        										"Arial Black=arial black,avant garde;"+
	        										"Book Antiqua=book antiqua,palatino;"+
									                "Comic Sans MS=comic sans ms,sans-serif;"+
									                "Courier New=courier new,courier;"+
									                "Georgia=georgia,palatino;"+
									                "Helvetica=helvetica;"+
									                "Impact=impact,chicago;"+
									                "Symbol=symbol;"+
									                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
									                "Terminal=terminal,monaco;"+
									                "Times New Roman=times new roman,times;"+
									                "Trebuchet MS=trebuchet ms,geneva;"+
									                "Verdana=verdana,geneva;"+
									                "Webdings=webdings;"+
									                "Wingdings=wingdings,zapf dingbats",
	   						relative_urls : false,
							content_css : "",
							convert_urls : false,
							editor_selector : "mceAdvanced",
							// Drop lists for link/image/media/template dialogs
							external_link_list_url : "lists/link_list.js",
							save_callback : "myCustomSaveContent",
							oninit: "myInitTinyMCE",
							cleanup: false,
							verify_html : false,
							forced_root_block : false
						});
					}
					
					for (var intIdx in arrResponsiveDesignObjectsImg) {
						tinyMCE.init({
							// General options
							mode : "exact",
							readonly : 0,
					        elements : arrResponsiveDesignObjectsImg[intIdx] + ",ajaxfilemanager",
					      	theme : "advanced",
					      	width: 150,
	        				height: 150,
							file_browser_callback : "ajaxfilemanager",
							handle_event_callback : "formattedtextchanged",
							entity_encoding : "raw",
							object_resizing : true,
							theme_advanced_buttons1 : "image",
							theme_advanced_buttons2: "",
							theme_advanced_buttons3: "",
							theme_advanced_buttons4: "",		
							theme_advanced_toolbar_location : "top",
							theme_advanced_toolbar_align : "left",
							theme_advanced_statusbar_location : "none",
							theme_advanced_resizing : true,
							//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
							theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
	        										"Arial=arial,helvetica,sans-serif;"+
	        										"Arial Black=arial black,avant garde;"+
	        										"Book Antiqua=book antiqua,palatino;"+
									                "Comic Sans MS=comic sans ms,sans-serif;"+
									                "Courier New=courier new,courier;"+
									                "Georgia=georgia,palatino;"+
									                "Helvetica=helvetica;"+
									                "Impact=impact,chicago;"+
									                "Symbol=symbol;"+
									                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
									                "Terminal=terminal,monaco;"+
									                "Times New Roman=times new roman,times;"+
									                "Trebuchet MS=trebuchet ms,geneva;"+
									                "Verdana=verdana,geneva;"+
									                "Webdings=webdings;"+
									                "Wingdings=wingdings,zapf dingbats",
	   						relative_urls : false,
							content_css : "",
							convert_urls : false,
							editor_selector : "mceSimple",
							// Drop lists for link/image/media/template dialogs
							external_link_list_url : "lists/link_list.js",
							save_callback : "myCustomSaveContent",
							oninit: "myInitTinyMCE",
							cleanup: false,
							verify_html : false,
							forced_root_block : false
						});
					}
					
					function myInitTinyMCE() {
						/*
						if (tinyMCE && tinyMCE.editors && tinyMCE.editors["QuestionImageText"]) {
							var htmlSrc = tinyMCE.editors["QuestionImageText"].getContent()
							
							if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
								toogleEditorMode('QuestionImageText');
							}
						}
						*/
						
						if (tinyMCE && tinyMCE.editors && tinyMCE.editors["QuestionMessage"]) {
							var htmlSrc = tinyMCE.editors["QuestionMessage"].getContent()
							
							if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
								setTimeout(function () {toogleEditorMode('QuestionMessage');}, 300);
							}
						}
						
						for (var intIdx in arrResponsiveDesignObjectsMsg) {
							var strTinyMCEObjectName = arrResponsiveDesignObjectsMsg[intIdx];
							if (tinyMCE && tinyMCE.editors && tinyMCE.editors[strTinyMCEObjectName]) {
								var htmlSrc = tinyMCE.editors[strTinyMCEObjectName].getContent()
								
								if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
									setTimeout(function () {toogleEditorMode(strTinyMCEObjectName);}, 300);
								}
							}
						}
						
						for (var intIdx in arrResponsiveDesignObjectsImg) {
							var strTinyMCEObjectName = arrResponsiveDesignObjectsImg[intIdx];
							if (tinyMCE && tinyMCE.editors && tinyMCE.editors[strTinyMCEObjectName]) {
								var htmlSrc = tinyMCE.editors[strTinyMCEObjectName].getContent()
								
								if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
									setTimeout(function () {toogleEditorMode(strTinyMCEObjectName);}, 300);
								}
							}
						}
					}
					
					function myCustomSaveContent(element_id, html, body) {
					        // Do some custom HTML cleanup
					        if (html) {
					        	html = html.replace(/\/\/ <!\[cdata\[/gi, '').replace(/\/\/ \]\]>/gi,'');
					        }
					
					        return html;
					}
					
					function formattedtextchanged(event) {
						
						var htmlSrc = tinyMCE.activeEditor.getContent();
			            
						if(event.type == "keydown") {
							
						}
						
						/*
						var rexp = [a-zA-z0-9\t\n ./<>?;:"'`!@#$%^&*()[]{}_+=-|\\];
			          
			            var cleanSrc = htmlSrc.replace(rexp,"");
			      		*/
			         
					}

					function ajaxfilemanager(field_name, url, type, win) 
					{
						var ajaxfilemanagerurl = "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
						var view = 'detail';
						switch (type) 
						{
							case "image":
								view = 'thumbnail';
								break;
							case "media":
								break;
							case "flash": 
								break;
							case "file":
								break;
							default:
								return false;
						}
    					
						tinyMCE.activeEditor.windowManager.open({
							url: "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
							width: 900,
        					height: 600,
							inline : "yes",
							close_previous : "no"
						},{
							window : win,
							input : field_name
						}); 
					}
					
					//Inicio conchita 16-nov-2011 toggle tinymce
			        function toogleEditorMode(sEditorID) { 
				        var x;
				        var tmp;
			        
			            try { 
			                if(tinyMCEmode) { 
			                	//x=removeHTML();
			               		tinyMCE.execCommand('mceRemoveControl', false, sEditorID); 
			                   	//document.getElementById(sEditorID).value=x; 
			                    tinyMCEmode = false; 
			                } else {   tinyMCE.execCommand('mceAddControl', false, sEditorID);
			                    //tinyMCE.addMCEControl(document.getElementById(''), sEditorID); 
			                    tinyMCEmode = true; 
			                } 
			            } catch(e) { 
			            	alert('An error occur, try again. Error: '+ e);
			                //error handling 
			            } 
			        }
					
			        function removeHTML() {
			            var htmlSrc = tinyMCE.activeEditor.getContent();
			            var rexp = /<[^<>]+>/g;
			          
			            var cleanSrc = htmlSrc.replace(rexp,"");
			      
			            return cleanSrc;
			        }
					//Fin Conchita toggle tinymce 16-nov-2011	
</script>

<div style="position:absolute; display: block; width: 1357px;height: 157px; " id="disableQuestionImage"></div>
<!-- @AAL 25/02/2015: Agregado para enviar el SurveyID y el QuestionID a traves de un elemento oculto.
     estos valores son empleados al abrir un nuevo Editor de texto en una Pregunta-->
<input type="hidden" id="SurveyAndSectOrQuestIDs" value="<?=$this->SurveyID . ':' . $this->QuestionID . ':QuestionID'?>">
<div style="display:none;" id="questionImageTextArea" width="100%">
	<table class="object_table" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tbody>
		<tr>
			<td>
				<strong><?=translate("Image")?></strong>
			</td>
		</tr>
		<tr>
			<td class="object_field_value_3" colspan="3">
<?
		//@JAPR 2014-08-01: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
				<div id="QuestionImageTextMenu" class="tabbed" style="height:40px;">
					<ul class="menu">
						<li name="Default" class="menuOption selectedTab" onclick="changeTab(this,'QuestionImageText')">
							<a>Default</a>
				        </li>					
						<li name="iPod" class="menuOption" onclick="changeTab(this,'QuestionImageText')">
							<a>iPod</a>
				        </li>					
						<li name="iPad" class="menuOption" onclick="changeTab(this,'QuestionImageText')">
							<a>iPad</a>
				        </li>					
						<li name="iPadMini" class="menuOption" onclick="changeTab(this,'QuestionImageText')">
							<a>iPad Mini</a>
				        </li>					
						<li name="iPhone" class="menuOption" onclick="changeTab(this,'QuestionImageText')">
							<a>iPhone</a>
				        </li>					
						<li name="Cel" class="menuOption" onclick="changeTab(this,'QuestionImageText')">
							<a>Android Cel</a>
				        </li>					
						<li name="Tablet" class="menuOption" onclick="changeTab(this,'QuestionImageText')">
							<a>Android Tablet</a>
				        </li>					
					</ul>
				</div>
<?
		}
		//@JAPR
?>
				<div id="QuestionImageTextTabs">
					<div id="QuestionImageTextDefaultDiv">
						<textarea class="mceSimple" id="QuestionImageText" name="QuestionImageText" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionImageText?>
						</textarea>
					</div>
<?
		//@JAPR 2014-08-01: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
					<div id="QuestionImageTextiPodDiv" style="display: none;">
						<textarea class="mceSimple" id="QuestionImageTextiPod" name="QuestionImageTextiPod" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionImageTextiPod?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionImageTextiPod');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
					<div id="QuestionImageTextiPadDiv" style="display: none;">
						<textarea class="mceSimple" id="QuestionImageTextiPad" name="QuestionImageTextiPad" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionImageTextiPad?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionImageTextiPad');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
					<div id="QuestionImageTextiPadMiniDiv" style="display: none;">
						<textarea class="mceSimple" id="QuestionImageTextiPadMini" name="QuestionImageTextiPadMini" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionImageTextiPadMini?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionImageTextiPadMini');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
					<div id="QuestionImageTextiPhoneDiv" style="display: none;">
						<textarea class="mceSimple" id="QuestionImageTextiPhone" name="QuestionImageTextiPhone" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionImageTextiPhone?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionImageTextiPhone');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
					<div id="QuestionImageTextCelDiv" style="display: none;">
						<textarea class="mceSimple" id="QuestionImageTextCel" name="QuestionImageTextCel" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionImageTextCel?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionImageTextCel');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
					<div id="QuestionImageTextTabletDiv" style="display: none;">
						<textarea class="mceSimple" id="QuestionImageTextTablet" name="QuestionImageTextTablet" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionImageTextTablet?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionImageTextTablet');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
<?
		}
		//@JAPR
?>
				</div>
			</td>
		</tr>
		
	</tbody>
	</table>
</div>

<div style="display:none;" id="questionMessageTextArea" width="100%">
	<table class="object_table" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tbody>
		<tr>
			<td>
				<strong><?=translate("Formatted text")?></strong>
			</td>
		</tr>
		<tr>
			<td class="object_field_value_3" colspan="3">
<?
		//@JAPR 2014-08-01: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
			<div id="QuestionMessageMenu" class="tabbed" style="height:40px;">
					<ul class="menu">
						<li id="defaultQuestionMessage" name="Default" class="menuOption selectedTab" onclick="changeTab(this,'QuestionMessage')">
							<a>Default</a>
				        </li>					
						<li name="iPod" class="menuOption" onclick="changeTab(this,'QuestionMessage')">
							<a>iPod</a>
				        </li>					
						<li name="iPad" class="menuOption" onclick="changeTab(this,'QuestionMessage')">
							<a>iPad</a>
				        </li>					
						<li name="iPadMini" class="menuOption" onclick="changeTab(this,'QuestionMessage')">
							<a>iPad Mini</a>
				        </li>					
						<li name="iPhone" class="menuOption" onclick="changeTab(this,'QuestionMessage')">
							<a>iPhone</a>
				        </li>					
						<li name="Cel" class="menuOption" onclick="changeTab(this,'QuestionMessage')">
							<a>Android Cel</a>
				        </li>					
						<li name="Tablet" class="menuOption" onclick="changeTab(this,'QuestionMessage')">
							<a>Android Tablet</a>
				        </li>					
					</ul>
				</div>
<?
		}
		//@JAPR
		if($this->EditorType==0)
		{
			$styleDefaultDiv = '';
			$styleDefaultDesDiv = 'style="display: none;width: '.$widthByDisp["Default"].'; height: 200px;overflow-x: hidden;overflow-y: auto;"';
		}
		else
		{
			$styleDefaultDiv = 'style="display: none;"';
			$styleDefaultDesDiv = 'style="width: '.$widthByDisp["Default"].'; height: 200px;overflow-x: hidden;overflow-y: auto;"';
		}
?>
				<div id="QuestionMessageTabs">
					<div id="QuestionMessageDefaultDiv" <?=$styleDefaultDiv?>>
						<textarea class="mceAdvanced" id="QuestionMessage" name="QuestionMessage" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionMessage?>
						</textarea>	  <br>
						<div style="display: inline-flex">
							<a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionMessage');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
							<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpimirror1/artus/genvii/esurveyv5/btngenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color='#98fb98'" onmouseout="this.style.color='#d5d5bc'" onclick="this.style.color='#d5d5bc'; ShowWindowsDialogEditor('QuestionMessage');">
						</div>
					</div>
<?
		//@JAPR 2014-08-01: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
					<div id="QuestionMessageiPodDiv" style="display: none;">
						<textarea class="mceAdvanced" id="QuestionMessageiPod" name="QuestionMessageiPod" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionMessageiPod?>
						</textarea>   <br>
						<!-- @AAL 25/02/2015: Agregado Div y un Button que abre la ventana del editor de fórmulas -->
						<div style="display: inline-flex">
							<a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionMessageiPod');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
							<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpimirror1/artus/genvii/esurveyv5/btngenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color='#98fb98'" onmouseout="this.style.color='#d5d5bc'" onclick="this.style.color='#d5d5bc'; ShowWindowsDialogEditor('QuestionMessageiPod');">
						</div>
						<!-- @AAL -->
					</div>
					<div id="QuestionMessageiPadDiv" style="display: none;">
						<textarea class="mceAdvanced" id="QuestionMessageiPad" name="QuestionMessageiPad" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionMessageiPad?>
						</textarea>   <br>
						<!-- @AAL 25/02/2015: Agregado Div y un Button que abre la ventana del editor de fórmulas -->
						<div style="display: inline-flex">
							<a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionMessageiPad');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
							<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpimirror1/artus/genvii/esurveyv5/btngenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color='#98fb98'" onmouseout="this.style.color='#d5d5bc'" onclick="this.style.color='#d5d5bc'; ShowWindowsDialogEditor('QuestionMessageiPad');">
						</div>
						<!-- @AAL -->
					</div>
					<div id="QuestionMessageiPadMiniDiv" style="display: none;">
						<textarea class="mceAdvanced" id="QuestionMessageiPadMini" name="QuestionMessageiPadMini" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionMessageiPadMini?>
						</textarea>   <br>
						<!-- @AAL 25/02/2015: Agregado Div y un Button que abre la ventana del editor de fórmulas -->
						<div style="display: inline-flex">
							<a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionMessageiPadMini');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
							<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpimirror1/artus/genvii/esurveyv5/btngenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color='#98fb98'" onmouseout="this.style.color='#d5d5bc'" onclick="this.style.color='#d5d5bc'; ShowWindowsDialogEditor('QuestionMessageiPadMini');">
						</div>
						<!-- @AAL -->
					</div>
					<div id="QuestionMessageiPhoneDiv" style="display: none;">
						<textarea class="mceAdvanced" id="QuestionMessageiPhone" name="QuestionMessageiPhone" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionMessageiPhone?>
						</textarea>   <br>
						<!-- @AAL 25/02/2015: Agregado Div y un Button que abre la ventana del editor de fórmulas -->
						<div style="display: inline-flex">
							<a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionMessageiPhone');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
							<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpimirror1/artus/genvii/esurveyv5/btngenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color='#98fb98'" onmouseout="this.style.color='#d5d5bc'" onclick="this.style.color='#d5d5bc'; ShowWindowsDialogEditor('QuestionMessageiPhone');">
						</div>
						<!-- @AAL -->
					</div>
					<div id="QuestionMessageCelDiv" style="display: none;">
						<textarea class="mceAdvanced" id="QuestionMessageCel" name="QuestionMessageCel" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionMessageCel?>
						</textarea>   <br>
						<!-- @AAL 25/02/2015: Agregado Div y un Button que abre la ventana del editor de fórmulas -->
						<div style="display: inline-flex">
							<a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionMessageCel');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
							<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpimirror1/artus/genvii/esurveyv5/btngenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color='#98fb98'" onmouseout="this.style.color='#d5d5bc'" onclick="this.style.color='#d5d5bc'; ShowWindowsDialogEditor('QuestionMessageCel');">
						</div>
						<!-- @AAL -->
					</div>
					<div id="QuestionMessageTabletDiv" style="display: none;">
						<textarea class="mceAdvanced" id="QuestionMessageTablet" name="QuestionMessageTablet" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->QuestionMessageTablet?>
						</textarea>   <br>
						<!-- @AAL 25/02/2015: Agregado Div y un Button que abre la ventana del editor de fórmulas -->
						<div style="display: inline-flex">
							<a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('QuestionMessageTablet');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
							<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpimirror1/artus/genvii/esurveyv5/btngenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color='#98fb98'" onmouseout="this.style.color='#d5d5bc'" onclick="this.style.color='#d5d5bc'; ShowWindowsDialogEditor('QuestionMessageTablet');">
						</div>
						<!-- @AAL -->
					</div>
<?
		}
		//@JAPR
?>
				</div>
<?				
					//Nuevos divs para el nuevo editor
					if (getMDVersion() >= esvEditorHTML)
					{
?>
							
				<span id="QuestionMessageSpanDesigner">
					<table id="QuestionMessage_tbl" style="border-top:1px solid #CCC; border-bottom:1px solid #CCC; border-left:1px solid #CCC;border-right:1px solid #CCC; border-collapse:collapse;">
					<tbody>
						<tr height=25px>
						<td margin="0" style="background: #F0F0EE;border: 0; border-bottom:1px solid #CCC;padding:0px">
						<div id="QuestionMessage_toolbargroup" tabindex="-1">
						<span>
							<table id="QuestionMessage_toolbar1" cellpadding="0" cellspacing="0" tabindex="-1">
								<tbody>
								<tr>
									<td style="position: relative;padding:0px">
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['QuestionMessage'],visibleDivArray['QuestionMessage'],'LBL,ADD',0,0);" title="Insert Text">
											<img src="./images/insertText.png" style="display:inline;cursor:pointer;">
										</a>
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['QuestionMessage'],visibleDivArray['QuestionMessage'],'BTN,ADD',0,0);" title="Insert Button">
											<img src="./images/insertButton.png" style="display:inline;cursor:pointer;">
										</a>
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['QuestionMessage'],visibleDivArray['QuestionMessage'],'IMG,ADD',0,0);" title="Insert Image">
											<img src="./images/insertImage.png" style="display:inline;cursor:pointer;">
										</a>
									</td>
								</tr>
								</tbody>
							</table>
						</span>
						</div>
						</td>
						</tr>
						<tr>
						<td style="border-top:1px solid #CCC; border-bottom:1px solid #CCC; border-left:1px solid #CCC;border-right:1px solid #CCC;padding:0px">
					<div id="QuestionMessageDefaultDesDiv" <?=$styleDefaultDesDiv?>>
						<textarea id="QuestionMessageDes" name="QuestionMessageDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageDes?></textarea>
						<textarea id="QuestionMessageDesHTML" name="QuestionMessageDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessage?></textarea>
					</div>

					<?
						//@JAPR 2014-07-30: Agregado el Responsive Design
						//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
						//el componente default
						if (getMDVersion() >= esvResponsiveDesign) 
						{
?>
					<div id="QuestionMessageiPodDesDiv"  style="display: none; width: <?=$widthByDisp["iPod"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="QuestionMessageiPodDes" name="QuestionMessageiPodDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageiPodDes?></textarea>
						<textarea id="QuestionMessageiPodDesHTML" name="QuestionMessageiPodDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageiPodDes?></textarea>
					</div>
					<div id="QuestionMessageiPadDesDiv" style="display: none;width: <?=$widthByDisp["iPad"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="QuestionMessageiPadDes" name="QuestionMessageiPadDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageiPadDes?></textarea>
						<textarea id="QuestionMessageiPadDesHTML" name="QuestionMessageiPadDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageiPadDes?></textarea>
					</div>
					<div id="QuestionMessageiPadMiniDesDiv" style="display: none;width: <?=$widthByDisp["iPadMini"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="QuestionMessageiPadMiniDes" name="QuestionMessageiPadMiniDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageiPadMiniDes?></textarea>
						<textarea id="QuestionMessageiPadMiniDesHTML" name="QuestionMessageiPadMiniDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageiPadMiniDes?></textarea>
					</div>
					<div id="QuestionMessageiPhoneDesDiv" style="display: none;width: <?=$widthByDisp["iPhone"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="QuestionMessageiPhoneDes" name="QuestionMessageiPhoneDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageiPhoneDes?></textarea>
						<textarea id="QuestionMessageiPhoneDesHTML" name="QuestionMessageiPhoneDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageiPhoneDes?></textarea>
					</div>
					<div id="QuestionMessageCelDesDiv" style="display: none;width: <?=$widthByDisp["Cel"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="QuestionMessageCelDes" name="QuestionMessageCelDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageCelDes?></textarea>
						<textarea id="QuestionMessageCelDesHTML" name="QuestionMessageCelDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageCelDes?></textarea>
					</div>
					<div id="QuestionMessageTabletDesDiv" style="display: none;width: <?=$widthByDisp["Tablet"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="QuestionMessageTabletDes" name="QuestionMessageTabletDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageTabletDes?></textarea>
						<textarea id="QuestionMessageTabletDesHTML" name="QuestionMessageTabletDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->QuestionMessageTabletDes?></textarea>
					</div>
<?
						}
?>
						</td>
						</tr>
					</tbody>
					</table>
				</span>
<?
					}
?>
				
			</td>
		</tr>
		
	</tbody>
	</table>
</div>
<script type="text/javascript">
function StartQuestion()
{
<?
		if (getMDVersion() >= esvEditorHTML) {
?>
	aHTMLDivObjects['QuestionMessageDefaultDesHDiv'] = new reportPart('QuestionMessageDefaultDesDiv', '', defaultWidth, defaultHeight, 'QuestionMessageDefaultDesHDiv', 'QuestionMessageDes');
	aHTMLDivObjects['QuestionMessageiPodDesHDiv'] = new reportPart('QuestionMessageiPodDesDiv', '', iPodWidth, iPodHeight, 'QuestionMessageiPodDesHDiv', 'QuestionMessageiPodDes');
	aHTMLDivObjects['QuestionMessageiPadDesHDiv'] = new reportPart('QuestionMessageiPadDesDiv', '', iPadWidth, iPadHeight, 'QuestionMessageiPadDesHDiv', 'QuestionMessageiPadDes');
	aHTMLDivObjects['QuestionMessageiPadMiniDesHDiv'] = new reportPart('QuestionMessageiPadMiniDesDiv', '', iPadMiniWidth, iPadMiniHeight, 'QuestionMessageiPadMiniDesHDiv', 'QuestionMessageiPadMiniDes');
	aHTMLDivObjects['QuestionMessageiPhoneDesHDiv'] = new reportPart('QuestionMessageiPhoneDesDiv', '', iPhoneWidth, iPhoneHeight, 'QuestionMessageiPhoneDesHDiv', 'QuestionMessageiPhoneDes');
	aHTMLDivObjects['QuestionMessageCelDesHDiv'] = new reportPart('QuestionMessageCelDesDiv', '', celAndroidWidth, celAndroidHeight, 'QuestionMessageCelDesHDiv', 'QuestionMessageCelDes');
	aHTMLDivObjects['QuestionMessageTabletDesHDiv'] = new reportPart('QuestionMessageTabletDesDiv', '', tabletAndroidWidth, tabletAndroidHeight, 'QuestionMessageTabletDesHDiv', 'QuestionMessageTabletDes');
<?
		}
?>
}

if (document.attachEvent){
	document.attachEvent("onload", StartQuestion());
}
else if (document.addEventListener) {
	document.addEventListener("load", StartQuestion(), false);
}
</script>