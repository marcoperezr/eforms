<?php
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language='JavaScript' src='js/EditorHTML/utils.js'></script>
<script language="JavaScript" src="js/EditorHTML/dialogs.js"></script>
<style type="text/css">@import url("css/fudgeEditorHTML.css"); </style>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
var window_dialogArguments = getDialogArgument();
owner = window_dialogArguments[1];
/*@AAL 25/02/2015: Se obtiene el SurveyID y el SectionID o QuestionID enviado cuando se abre el editor de texto.*/
var SurveyAndSectOrQuestIDs = window_dialogArguments[window_dialogArguments.length-1];
var sParams = window_dialogArguments[2];
//alert('lblTiny sParams:'+sParams);
var AWProps = '_#AWProps#_';
var elemParams = sParams.split(AWProps);
//alert('elemParams[0]:'+elemParams[0]);
//alert('elemParams[1]:'+elemParams[1]);
var elemData = elemParams[0].split(',');
var lblValue = elemData[2];
var tinyMCEmode = true; //Conchita 16-nov-2011 cambio para toggle tinymce
function OnStartTiny()
{
	//debugger;
	GetElementWithId("FormattedText").value=lblValue;
/*	alert(lblValue);
	debugger;
	GetElementWithId("FormattedText").value=lblValue;
	alert('FormattedText.value='+GetElementWithId("FormattedText").value);
	alert('FormattedText.innerHTML='+GetElementWithId("FormattedText").innerHTML);
	*/


	tinyMCE.init({
		// General options
		mode : "exact",
		readonly : 0,
        elements : "FormattedText,ajaxfilemanager",
      	theme : "advanced",
		file_browser_callback : "ajaxfilemanager",
		entity_encoding : "raw",
		theme_advanced_buttons1 : "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,",
		theme_advanced_buttons2: "",
		theme_advanced_buttons3: "",
		theme_advanced_buttons4: "",		
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "none",
		theme_advanced_resizing : true,
		//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
		theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
								"Arial=arial,helvetica,sans-serif;"+
								"Arial Black=arial black,avant garde;"+
								"Book Antiqua=book antiqua,palatino;"+
				                "Comic Sans MS=comic sans ms,sans-serif;"+
				                "Courier New=courier new,courier;"+
				                "Georgia=georgia,palatino;"+
				                "Helvetica=helvetica;"+
				                "Impact=impact,chicago;"+
				                "Symbol=symbol;"+
				                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
				                "Terminal=terminal,monaco;"+
				                "Times New Roman=times new roman,times;"+
				                "Trebuchet MS=trebuchet ms,geneva;"+
				                "Verdana=verdana,geneva;"+
				                "Webdings=webdings;"+
				                "Wingdings=wingdings,zapf dingbats",

			relative_urls : false,
		content_css : "",
		convert_urls : false,
		
		// Drop lists for link/image/media/template dialogs
		external_link_list_url : "lists/link_list.js",
		save_callback : "myCustomSaveContent",
		oninit: "myInitTinyMCE",
		cleanup: false,
		verify_html : false,
		forced_root_block : false
	});
}
	
	function myInitTinyMCE() {
		if (tinyMCE && tinyMCE.editors && tinyMCE.editors["FormattedText"]) {
			var htmlSrc = tinyMCE.editors["FormattedText"].getContent()
			
			if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
				setTimeout(function () {toogleEditorMode('FormattedText');}, 300);
			}
		}
	}
	
	function myCustomSaveContent(element_id, html, body) {
	        // Do some custom HTML cleanup
	        if (html) {
	        	html = html.replace(/\/\/ <!\[cdata\[/gi, '').replace(/\/\/ \]\]>/gi,'');
	        }
	
	        return html;
	}

	/*@AAL 25/02/2015: Abre la ventana de dialogo donde se mostrará el editor de fórmulas.*/
	function ShowWindowsDialogEditor(){
		var IDs = SurveyAndSectOrQuestIDs.split(':'); //Se obtienen los IDs (SurveyID y SectionID o QuestionID)
		//tinyMCE.activeEditor.selection.select(tinyMCE.activeEditor.getBody(), true);
		//Solo el texto seleccionado es enviado al editor de fórmulas
		var Parameters = 'SurveyID=' + IDs[0] + '&' + IDs[2] + '=' + IDs[1] + '&Type=' + IDs[2][0] + '&aTextEditor=' + encodeURIComponent(tinyMCE.activeEditor.selection.getContent({format: 'text'}));
		//var Parameters = 'SurveyID=' + IDs[0] + '&' + IDs[2] + '=' + IDs[1] + '&Type=' + IDs[2][0] + '&aTextEditor=' + encodeURIComponent(tinyMCE.activeEditor.selection.getContent({format: 'text'}));
		openWindowDialog('formulaEditor.php?' + Parameters, [window], [], 620, 460, ResultEditFormulas, 'ResultEditFormulas');
		
	}

	/*@AAL 25/02/2015: Esta función recibe como parámetro sValue que corresponde al texto devuelto del 
	editor de fórmulas, éste se sobreescribira en el texto seleccionado. sParams es vacío para este caso*/
	function ResultEditFormulas(sValue, sParams)
	{
		tinyMCE.activeEditor.selection.setContent(sValue + " ");
		//Formas de agregar texto al tinyMCE activo.
		/*tinyMCE.activeEditor.setContent(sValue);
		//tinyMCE.get('FormattedText').setContent(sValue);*/
	}
	//@AAL

	function formattedtextchanged(event) {
		
		var htmlSrc = tinyMCE.activeEditor.getContent();
        
		if(event.type == "keydown") {
			
		}
		
		/*
		var rexp = [a-zA-z0-9\t\n ./<>?;:"'`!@#$%^&*()[]{}_+=-|\\];
      
        var cleanSrc = htmlSrc.replace(rexp,"");
  		*/
     
	}
	
	function ajaxfilemanager(field_name, url, type, win) 
	{
		var ajaxfilemanagerurl = "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
		var view = 'detail';
		switch (type) 
		{
			case "image":
				view = 'thumbnail';
				break;
			case "media":
				break;
			case "flash": 
				break;
			case "file":
				break;
			default:
				return false;
		}
		
		tinyMCE.activeEditor.windowManager.open({
			url: "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
			width: 900,
			height: 600,
			inline : "yes",
			close_previous : "no"
		},{
			window : win,
			input : field_name
		}); 
	}
	
	//Inicio conchita 16-nov-2011 toggle tinymce
    function toogleEditorMode(sEditorID) { 
    	var x;
    	var tmp;
    	
        try { 
            if(tinyMCEmode) { 
            	//x=removeHTML();
           		tinyMCE.execCommand('mceRemoveControl', false, sEditorID); 
               	//document.getElementById(sEditorID).value=x; 
                tinyMCEmode = false; 
            } else {   tinyMCE.execCommand('mceAddControl', false, sEditorID);
                //tinyMCE.addMCEControl(document.getElementById(''), sEditorID); 
                tinyMCEmode = true; 
            } 
        } catch(e) { 
        	alert('An error occur, try again. Error: '+ e);
            //error handling 
        } 
    }
	
    function removeHTML() {
        var htmlSrc = tinyMCE.activeEditor.getContent();
        var rexp = /<[^<>]+>/g;
      
        var cleanSrc = htmlSrc.replace(rexp,"");
  
        return cleanSrc;
    }
	//Fin Conchita toggle tinymce 16-nov-2011	
	
	//Funciones relacionadas con el nuevo editor
function On_Ok(){
	var AWComa = '_#AWComa#_';
	var lblValue=GetElementWithId("FormattedText").value;
	frmTinyMce.submit();
	lblValue=GetElementWithId("FormattedText").value;
	var regexp = new RegExp(',',"gi");
	lblValue = lblValue.replace(regexp,AWComa);
	//alert('despues del submit lblValue:'+lblValue);
	var sParamReturn=elemData[0]+','+elemData[1]+','+lblValue+'_#AWProps#_'+elemParams[1];
	//alert(sParamReturn);
	setDialogReturnValue(sParamReturn);
	closeDialog();
}
</script>
	</head>
<body onload="OnStartTiny();" >
<br>
<form id="frmTinyMce" name="frmTinyMce" action="lblTinyMCESubmit.php" method="post" target="iFrameTinyMCE">
<div id="formattedTextArea" width="100%">
	<table class="object_table" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tbody>
		<tr>
			<td>
				<strong><?=("Label")?></strong>
			</td>
		</tr>
		<tr>
			<td class="object_field_value_3" colspan="3">
				<div id="FormattedTextTabs">
					<div id="FormattedTextDefaultDiv">
						<textarea id="FormattedText" name="FormattedText" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
						</textarea>
						<br>
						<div style="display: inline-flex">
							<a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('FormattedText');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
							<!-- @AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas -->
							<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpionline8.bitam.com/artus/gen8/ESurveyV5/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color='#98fb98'" onmouseout="this.style.color='#d5d5bc'" onclick="this.style.color='#d5d5bc';ShowWindowsDialogEditor();">
						</div>
					</div>
				</div>
			</td>
		</tr>
	</tbody>
	</table>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tbButtons" style="position:absolute; bottom:20px;">
		<tr>
			<td align="right">
				<input id=botOK class="Nbutton" type="button" name="OK" value="Ok" onclick="On_Ok();">
				<input id=botCancel class="Nbutton" type="button" name="Cancel" value="Cancel" onclick="closeDialog()">
			</td>
		</tr>
	</table>
</div>
</form>
<iframe id="iFrameTinyMCE" name="iFrameTinyMCE" style="display:none"></iframe>
</body>
</html>
