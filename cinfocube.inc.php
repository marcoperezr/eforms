<?php

defined('PK_HOUR') or define ('PK_HOUR', 0);
defined('PK_DAY') or define ('PK_DAY', 1);
defined('PK_WEEK') or define ('PK_WEEK', 2);
defined('PK_TWOWEEK') or define ('PK_TWOWEEK', 3);
defined('PK_MONTH') or define ('PK_MONTH', 4);
defined('PK_YEAR') or define ('PK_YEAR', 5);
defined('PK_WEEK_MONTH') or define ('PK_WEEK_MONTH', 6);
defined('DATE_FORMAT') or define ('DATE_FORMAT', 'Y-m-d');

if (!class_exists('BITAMConnection'))
{
	require_once('cconnection.inc.php');
}

$ComCube = array();

class BITAMCInfoAggregationCount
{
// @EA 2011-02-18
	public $indicator_key;
	public $formula_bd;
}

class BITAMConnAux
{
	public $ADOConnection;
}

if (!class_exists('BITAMCDatesRange'))
{
	class BITAMCDatesRange
	{
		public $range_bottom;
		public $range_top;
		public $description = '';
	}
}

if (!class_exists('BITAMCIndicatorUser'))
{
	class BITAMCIndicatorUser
	{
		public $indicator_key;
		public $cube_key;
		public $indicator_name;
		public $indicator_group;
	}
}

if (!class_exists('BITAMCInfoAtrib'))
{
	class BITAMCInfoAtrib
	{
		public $key = 0;
		public $nom_fisico = '';
		public $nom_logico = '';	
		public $agrupador = '';
		public $formula_indicator = '';
		public $cla_indicador = -1;
		public $default_key = '';
		public $sql_source = '';
		public $sql_column = '';
	
		function SaveFormula()
		{
			$this->formula_indicator = $this->agrupador.'('.$this->nom_fisico.')';
		}
	}
}

if (!class_exists('BITAMCInfoAgregation'))
{
	class BITAMCInfoAgregation
	{
		public $key;
		public $physical_name;
		public $dimension_list;
	
		function __construct($precordset)
		{
			$this->key = $precordset->fields[strtolower('ID')];
			$this->physical_name = trim($precordset->fields[strtolower('NOM_FISICO')]);
			$this->dimension_list = trim($precordset->fields[strtolower('dimension_list')]);
	
		}
	
		static function NewInstance($precordset)
		{
			return new BITAMCInfoAgregation($precordset);
		}
	}
}

class BITAMCInfoConstant
{
	public $key;
	public $name;
	public $value;
	public $type;

	function __construct($precordset)
	{
		$this->key = $precordset->fields[strtolower('CLA_CONSTANTE')];
		$this->name = $precordset->fields[strtolower('NOM_CONSTANTE')];
		$this->value = $precordset->fields[strtolower('VALOR')];
		$this->type = $precordset->fields[strtolower('TIPO_DE_DATO')];
		if ($this->type == 2)
			$this->value = '"'.$this->value.'"';
	}

	static function NewInstance($precordset)
	{
		return new BITAMCInfoConstant($precordset);
	}
}

class BITAMCInfoConstantCollection
{
	public $a_constants;

	function __construct(&$prepository)
	{

		if (!is_null($prepository))
		{
			$ssql =
			'select CLA_CONSTANTE, NOM_CONSTANTE, VALOR, TIPO_DE_DATO '.
	       	'from	SI_CONSTANTE';

//			$prepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);

			$recordset = $prepository->ADOConnection->Execute($ssql);

			if (!$recordset)
				print $prepository->ADOConnection->ErrorMsg();
			else
			{
			    while (!$recordset->EOF)
			    {
// Ligamos la definicion de constantes
			        $this->a_constants[] = BITAMCInfoConstant::NewInstance($recordset);
			        $recordset->MoveNext();
			    }
			}
		}
	}

	static function NewInstance(&$prepository)
	{
		return new BITAMCInfoConstantCollection($prepository);
	}
}

class BITAMCInfoPeriod
{
	public $key;
	public $type;
	public $name;
	public $base;
	public $date_ref;
	public $format;
	public $relative_to;
	public $catalog;
	public $variable;
	public $a_data = array();

	function __construct($precordset)
	{
		// FD 2007-03-01
		// el uso del construct aqui esta mal !!!
		// esto debe hacerse en NewInstance y no a nivel de construct

		if (!is_null($precordset))
		{
			$this->key = $precordset->fields[strtolower('CLA_PERIODO')];
			$this->type = $precordset->fields[strtolower('TIPO_PERIODO')];
			$this->name = $precordset->fields[strtolower('NOM_PERIODO')];
			$this->base = $precordset->fields[strtolower('BASE_PERIODO')];
			$this->date_ref = $precordset->fields[strtolower('FECHA_REF')];
			$this->format = $precordset->fields[strtolower('FMT_PERIODO')];
			$this->relative_to = $precordset->fields[strtolower('RELATIVOA')];
			$this->catalog = $precordset->fields[strtolower('CATALOGO')];
			$this->variable = $precordset->fields[strtolower('CATALOGO')];
		}
	}

	static function NewInstance($precordset)
	{
		return new BITAMCInfoPeriod($precordset);
	}

	function GetVPeriodRange($fecha, &$DatesR, $avance = 0)
	{
		$cont = count( $this->a_data );
		for ($index = 0; $index < $cont; $index++ )
		{
			if ( $fecha >= $this->a_data[$cont]->range_bottom and
				 $fecha <= $this->a_data[$cont]->range_top )
			{
				$avance = $avance + $index;
		        if ($avance >= 0 or $avance < $cont )
		        {
		            $DatesR->range_bottom = $this->a_data[$avance]->range_bottom;
		            $DatesR->range_top = $this->a_data[$avance]->range_top;
		            $DatesR->description = $this->a_data[$avance]->description;
		            return true;
				}
				else
					return false;
			}
		}
		return false;
	}


	function GetPeriodRange( $poCurDate, $pnPeriodsShift )
	{
		if (is_null($poCurDate) or is_null($pnPeriodsShift))
		{
			return;
		}

		$oRange = new BITAMCDatesRange();
		$oRange->range_bottom = $poCurDate->range_bottom;
		$oRange->range_top = $poCurDate->range_top;

		if ($this->catalog)
		{
// Periodo Variable
			if ($this->GetVPeriodRange($poCurDate->range_bottom, $oRange, $pnPeriodsShift))
			{
				return $oRange;
			}
			return;
		}

		$year_bottom = intval(substr($oRange->range_bottom, 0, 4));
		$month_bottom = intval(substr($oRange->range_bottom, 5, 2));

		$day = intval(substr($oRange->range_bottom, 8, 2));

		$ntimes = $this->base;

		switch ( $this->type )
		{
		    case PK_YEAR:
		    	$oRange->range_bottom = date(DATE_FORMAT, mktime(0,0,0,1,1, $year_bottom));
		    	$oRange->range_bottom = years_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
				$oRange->range_top = years_add($oRange->range_bottom, $ntimes );
				$oRange->range_top = days_add($oRange->range_top, -1 );
				break;
		    case PK_MONTH:
		    	$oRange->range_bottom = date(DATE_FORMAT, mktime(0,0,0, $month_bottom,1, $year_bottom));
		    	$oRange->range_bottom = months_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
				$oRange->range_top = months_add($oRange->range_bottom, $ntimes );
				$oRange->range_top = days_add($oRange->range_top, -1 );
				break;
		    case PK_DAY:
		    	$oRange->range_bottom = days_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
		    	$oRange->range_top = $oRange->range_bottom;
		    	break;
		    case PK_WEEK:
				$x = $this->GetWeekNumberDate(wday($oRange->range_bottom));
				$oRange->range_bottom = days_add($oRange->range_bottom, ($x-1)*-1);
		    	$oRange->range_bottom = weeks_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
				$oRange->range_top = weeks_add($oRange->range_bottom, $ntimes );
				$oRange->range_top = days_add($oRange->range_top, -1 );
		    	break;
		    case PK_TWOWEEK:
		    	break;
		    case PK_WEEK_MONTH:
		    	break;

		}
// Si el tipo de periodo no es hora
		if ($this->key != PK_HOUR )
		{
			$oRange->range_bottom = substr($oRange->range_bottom, 0, 10);
			$oRange->range_top = substr($oRange->range_top, 0, 10);
		}

		return $oRange;
	}

	private function GetWeekNumberDate($sDayName)
	{
		switch (strtolower($sDayName))
		{
			case 'sunday':
				return 1;
				break;
			case 'monday':
				return 2;
				break;
			case 'tuesday':
				return 3;
				break;
			case 'wednesday':
				return 4;
				break;
			case 'thursday':
				return 5;
				break;
			case 'friday':
				return 6;
				break;
			case 'saturday':
				return 7;
				break;
		}
	}

	function GetInitialDate( $pCurDate )
	{

		if (is_null($pCurDate))
		{
			return;
		}

		if ($this->catalog)
		{
// Period Variable
			return;
		}

		if ($this->base <= 1)
		{
			$oRange = new BITAMCDatesRange();

			$oRange->range_bottom = $pCurDate;
			$oRange->range_top = $pCurDate;

			$oRangeInitial = $this->GetPeriodRange( $oRange, 0 );

			return $oRangeInitial->range_bottom;
		}

		return;
	}

	function GetPeriods( $pDateBottom, $pDateTop )
	{
		$pDateTmp = '';
		$sDates = '';
	    if ($pDateTop < $pDateBottom)
	    {
	        $pDateTmp = $pDateBottom;
	        $pDateBottom = $pDateTop;
	        $pDateTop = $pDateTmp;
	    }
	    $pDateTmp = $this->GetInitialDate($pDateBottom);
	    $sDates = $pDateTmp;

	    $RawDate = new BITAMCDatesRange();

	    $RawDate->range_bottom = $pDateTmp;
	    $RawDate->range_top = $pDateTmp;

    	$DateLast = $RawDate->range_bottom;
	    $i = 0;

	    while ($RawDate->range_bottom <= $pDateTop)
	    {
	        $i++;
	        $RawDate->range_bottom = $pDateTmp;
	        $RawDate->range_top = $pDateTmp;

	        $RawDate = $this->GetPeriodRange($RawDate, $i);

	        if ($DateLast >= $RawDate->range_bottom)
	            break;

	        $DateLast = $RawDate->range_bottom;

	        if ($RawDate->range_bottom <= $pDateTop)
	        {
	            $sDates = $sDates.",".$DateLast;
	        }
	    }
	    return $sDates;
	}

}

class BITAMCInfoPeriodCollection
{
	public $a_periods;

	function __construct(&$prepository)
	{

		if (!is_null($prepository))
		{
			$ssql =
			'select CLA_PERIODO, TIPO_PERIODO, NOM_PERIODO, '.
	       	'		BASE_PERIODO, FECHA_REF, FMT_PERIODO, RELATIVOA, CATALOGO '.
	       	'from	SI_PERIODO';

//			$prepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);

			$recordset = $prepository->ADOConnection->Execute($ssql);

			if (!$recordset)
				print $prepository->ADOConnection->ErrorMsg();
			else
			{
			    while (!$recordset->EOF)
			    {
// Ligamos la definicion de llaves al cubo
			        $this->a_periods[$recordset->fields[strtolower('CLA_PERIODO')]] = BITAMCInfoPeriod::NewInstance($recordset);
			        $recordset->MoveNext();
			    }
			}
		}
	}

	static function NewInstance(&$prepository)
	{
		return new BITAMCInfoPeriodCollection($prepository);
	}

}

class BITAMCInfoCube
{

	public $key;
	public $logical_name;
	public $physical_name;
	public $servidor;
	public $database;
	public $user;
	public $pasword;
	public $tipo_conn;
	public $service_name;
	public $database_name;
	public $table_date;
	public $field_date;
	public $use_filter_keys;
	public $use_period_less;
	public $use_cache;
	public $use_agregation;
	public $olap_catalog;
	public $a_join_keys;
	public $a_dimensions;
	public $a_dimensions_others;
	public $a_indicators;
	public $a_atribs;
// @EA 2011-02-18 Contiene la lista de indicadores que se almacenarian en las tablas de agregados y que solo aplican a nivel registro
	public $a_counts_aggregations;
//
	public $a_alarms;
	public $a_agregations;
	public $connection;
	public $a_period_keys;
	public $null_indicators = true;
	public $zero_indicators = true;
	public $type = 1;
	public $datekey = 0;
	public $cla_bd;
	public $rolap;
	public $ag_new = false;
	public $s_agregaciones = '';
	public $s_agdimensions = '';
	public $s_agtables = '';
	public $driver = '';
	public $owner_key = 0;

	function __construct($precordset)
	{
		if (!is_null($precordset))
		{
			// FD 2007-03-01
			// el uso del construct aqui esta mal !!!
			// esto debe hacerse en NewInstance y no a nivel de construct
			$this->key = (int) $precordset->fields[strtolower('CLA_CONCEPTO')];
			$this->logical_name = rtrim($precordset->fields[strtolower('NOM_CONCEPTO')]);
			$this->physical_name = rtrim($precordset->fields[strtolower('NOM_TABLA')]);
			$this->servidor = rtrim($precordset->fields[strtolower('SERVIDOR')]);
			$this->service_name = rtrim($precordset->fields[strtolower('SERVICE_NAME')]);
			$this->database_name = rtrim($precordset->fields[strtolower('DATABASE_NAME')]);
			$this->tipo_conn = (int) $precordset->fields[strtolower('TIPO_CONN')];
			$this->cla_bd = (int) $precordset->fields[strtolower('CLA_BD')];			
			if ($this->tipo_conn == 1)
			{
				if ($this->cla_bd == 25)
				{
					$this->driver = 'mysql';
					$this->servidor = $this->service_name;
				}
				elseif ($this->cla_bd == 1)
				{
					$this->driver = 'mssql';
					$this->servidor = $this->service_name;					
				}
				elseif ($this->cla_bd == 3)
				{
					$this->driver = 'oci8';
					$this->servidor = $this->service_name;					
				}
				else
					$this->driver = 'odbc';
			}
			else
				$this->driver = 'odbc';
			
			$this->database = rtrim($precordset->fields[strtolower('CATALOGO_OLAP')]);
			$this->user = $precordset->fields[strtolower('USUARIO')];
	//		$this->password = BITAMDecryptPassword(rtrim($precordset->fields[strtolower('PASSWORD')]));
			$this->password = rtrim($precordset->fields[strtolower('PASSWORD')]);
			$this->table_date = $precordset->fields[strtolower('DIM_PERIODO')];
			$this->field_date = $precordset->fields[strtolower('FECHA')];
			$this->use_filter_keys = ((int) $precordset->fields[strtolower('SHOW_ALIAS')] == 1);
			$this->use_period_less = ((int) $precordset->fields[strtolower('USA_SINPERIODO')] == 1);
			$this->use_cache = ((int) $precordset->fields[strtolower('USE_CACHE')] == 1);
			$this->use_agregation = ((int) $precordset->fields[strtolower('USA_AGREGACION')] == 1);
			$this->olap_catalog = rtrim($precordset->fields[strtolower('CATALOGO_OLAP')]);
			$this->datekey = (int) $precordset->fields[strtolower('DATE_KEY')];
			$this->ag_new = is_null($precordset->fields[strtolower('AG_NEW')]) ? false : ($precordset->fields[strtolower('AG_NEW')] == 1);
			$this->type = (int) $precordset->fields[strtolower('TIPO_CUBO')];
			$this->null_indicators = is_null($precordset->fields[strtolower('NO_NULLS')]) ? true : ($precordset->fields[strtolower('NO_NULLS')] == 0);
			$this->zero_indicators = is_null($precordset->fields[strtolower('NO_ZEROS')]) ? true : ($precordset->fields[strtolower('NO_ZEROS')] == 0);
			
			$this->owner_key = (int) $precordset->fields['owner_key'];
			
			$this->a_join_keys = array();
			$this->a_dimensions = array();
			$this->a_dimensions_others = array();
			$this->a_indicators = array();
			$this->a_atribs = array();
// @EA 2011-02-18
			$this->a_counts_aggregations = array();
//			
			$this->a_agregations = array();
			$this->a_period_keys = array();

//			$this->connection = BITAMConnection::NewInstance($this->servidor, $this->user, $this->password, $this->database_name, $this->driver);
		}
	}

	static function NewInstance(&$prepository, $pcubekey, $bFilesCreator = false)
	{
		$theInfoCube=null;
/*		
		$theInfoCube =& BITAMGlobalInstance::GetSDKCubeWithID($pcubekey);
		if(!is_null($theInfoCube))
		{
			return $theInfoCube;
		}
*/		
		if (!is_null($prepository))
		{
			$ssql =
			'select CLA_CONCEPTO, NOM_CONCEPTO, NOM_TABLA, SERVIDOR, CATALOGO_OLAP, USUARIO, PASSWORD, '.
			'		DIM_PERIODO, FECHA, USA_AGREGACION, USA_SINPERIODO, SHOW_ALIAS, USE_CACHE, USA_AGREGACION, CATALOGO_OLAP, CLA_BD, DATE_KEY, AG_NEW, TIPO_CUBO, NO_NULLS, NO_ZEROS, TIPO_CONN, SERVICE_NAME, DATABASE_NAME, CLA_OWNER as owner_key '.
			'from	SI_CONCEPTO '.
			'where	CLA_CONCEPTO = '.$pcubekey;

//			$prepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);

			$recordset = $prepository->ADOConnection->Execute($ssql);

			if (!$recordset)
			{
				print $prepository->ADOConnection->ErrorMsg();
			}
			else
			{
				$theInfoCube = new BITAMCInfoCube($recordset);

				if (!is_null($theInfoCube))
				{
// llenamos las llaves para los joins
					$ssql =
					'select  NOM_TABLA, NOM_FISICOK, NOM_FISICOK_JOIN, CONSECUTIVO '.
					'from    SI_DESCRIP_KEYS '.
					'where   CLA_CONCEPTO = '.$pcubekey.' '.
					'order by NOM_TABLA, CONSECUTIVO';
// Es importante que el recordset sea en base al nombre para que sea mas claro el codigo
//					$prepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
//
					$recordset = $prepository->ADOConnection->Execute($ssql);
					if (!$recordset)
						print $prepository->ADOConnection->ErrorMsg();
					else
					{
						$npos = 0;
					    while (!$recordset->EOF)
					    {
// Ligamos la definicion de llaves al cubo
					        $theInfoCube->a_join_keys[$npos] = BITAMCInfoKeys::NewInstance($recordset);
					        $recordset->MoveNext();
					        $npos++;
					    }
					}

// @EA 2011-01-26					
					$ssql = 'select PARENT_KEY from SI_DESCRIP_ENC where 1 = 0';
					$recordset = $prepository->ADOConnection->Execute($ssql);
					
					if (!$recordset)
					{
						$ssql = 'ALTER TABLE SI_DESCRIP_ENC ADD PARENT_KEY int NULL';
						$prepository->ADOConnection->Execute($ssql);
					}
//					
					
// llenamos las dimensiones
					$ssql =
		            'select A.CONSECUTIVO as m_key, '.
		            '		A.NOM_LOGICO as m_logical_name, '.
		            '		A.NOM_FISICO as m_physical_name_key, '.
		            '		A.TIPO_DATO as m_datatype_key, '.
		            '		A.LONG_DATO as m_long_key, '.
		            '       A.CLA_DESCRIP as m_descriptor_key, '.
		            '		B.NOM_FISICO as m_physical_name_descrip, '.
		            '		B.TIPO_DATO as m_datatype_descrip, '.
		            '		B.LONG_DATO as m_long_descrip, '.
		            '       A.ORDEN_DEL_SERVER, '.
		            '		B.AGRUPADOR as m_group_global, '.
		            '       A.CLA_PERIODO as m_period_key, '.
		            '		A.FMT_PERIODO as m_period_fmt, '.
		            '		A.PAR_DESCRIPTOR as m_parameters, '.
		            '       A.ES_METRICA, '.
		            '		B.NOM_TABLA as m_physical_table, '.
		            '		A.MEASUREDEF as m_measuredef, '.
		            '       A.EDIT_OUTLINE as m_edit_outline, '.
		            '		A.NO_NONEMPTY, '.
		            '		A.NIVEL as m_level, '.
		            '       B.ORDEN_DEL_SERVER, '.
		            '		A.ESTATUS as m_estatus, '.
		            '       A.MAXRECORDS, '.
		            '		A.FILTERDEP, '.
		            '		A.ALLCAPTION as IsIncremental, '.
		            '       A.ORDEN_PERIODO, '.
		            '		A.CLA_CONCEPTO, '.
		            '		A.AGRUPADOR as m_group_local, '.
		            '		A.CPAAD, '.
		            '		A.ORDEN_PERIODO as m_orden_periodo, '.
		            '		A.ORDINALLEVEL as m_ordinal_level, '.
		            '		B.DEFAULT_KEY as m_default_key, '.
		            '		B.DESCRIPTION_SOURCE as m_description_source, '.
		            '		B.NATURALKEY_SOURCE as m_naturalkey_source, '.
		            '		B.CREATED_BY as created_by, '.
		            '		B.PARENT_KEY as parent_key '.
		            'from   SI_CPTO_LLAVE A, SI_DESCRIP_ENC B '.
		            'where  A.CLA_CONCEPTO = '.$pcubekey.' AND '.
		            '       A.CLA_DESCRIP = B.CLA_DESCRIP '.
		            'order by A.CONSECUTIVO';
		            
// 		            '       A.ESTATUS = 1 AND '.		            

					$recordset = $prepository->ADOConnection->Execute($ssql);

					if (!$recordset)
						print $prepository->ADOConnection->ErrorMsg();
					else
					{
						$npos = 0;
					    while (!$recordset->EOF)
					    {

					        $npos++;
					        $theDimension = BITAMCInfoDimensions::NewInstance($recordset);
// Obtenemos el consecutivo de la dimension
							$nkey = $theDimension->key;
// Ligamos la dimension al cubo

							$bAdd = true;

							if ($bFilesCreator and (int) $recordset->fields['m_ordinal_level'] == -2)
								$bAdd = false;

							if ($bAdd)
							{
								$theInfoCube->a_dimensions[$nkey] = $theDimension;
	
								if ($theDimension->period_key != -1)
								{
									$theInfoCube->a_period_keys[] = $theDimension->period_key;
								}
							}

//							print $nkey.'->'.count($theInfoCube->a_dimensions).'<BR>';

					        $recordset->MoveNext();
					    }
					}
					
					$ssql =	
					'select 0 as m_key, '.
		            '		NOM_LOGICO as m_logical_name, '.
		            '		NOM_FISICO as m_physical_name_key, '.
		            '		TIPO_DATO as m_datatype_key, '.
		            '		LONG_DATO as m_long_key, '.
		            '       CLA_DESCRIP as m_descriptor_key, '.
		            '		NOM_FISICO as m_physical_name_descrip, '.
		            '		TIPO_DATO as m_datatype_descrip, '.
		            '		LONG_DATO as m_long_descrip, '.
		            '		AGRUPADOR as m_group_global, '.
		            '       -1 as m_period_key, '.
		            '		\'\' as m_period_fmt, '.
		            '		\'\' as m_parameters, '.
		            '		NOM_TABLA as m_physical_table, '.
		            '		\'\' as m_measuredef, '.
		            '       0 as m_edit_outline, '.
		            '		0 as m_level, '.
		            '		\'\' as IsIncremental, '.
		            '		0 as CLA_CONCEPTO, '.
		            '		0 as m_orden_periodo, '.
		            '		0 as m_ordinal_level, '.
		            '		1 as m_estatus, '.		            
		            '		DEFAULT_KEY as m_default_key, '.
		            '		DESCRIPTION_SOURCE as m_description_source, '.
		            '		NATURALKEY_SOURCE as m_naturalkey_source, '.
		            '		CREATED_BY as created_by, '.
		            '		-1 as parent_key '.
		            'from	SI_DESCRIP_ENC '.
					'where 	CLA_DESCRIP in '.
					'(select CLA_DESCRIP from SI_DESCRIP_DET where CLA_PADRE in ( select CLA_DESCRIP from SI_CPTO_LLAVE where CLA_CONCEPTO = '.$pcubekey.' ) ) '.
					' and not CLA_DESCRIP in ( select CLA_DESCRIP from SI_CPTO_LLAVE where CLA_CONCEPTO = '.$pcubekey.' )';
					
					$recordset = $prepository->ADOConnection->Execute($ssql);

					$consecutivo = 0;
					if (!$recordset)
						print $prepository->ADOConnection->ErrorMsg();
					else
					{
						$npos = 0;
					    while (!$recordset->EOF)
					    {

					        $consecutivo++;
					        $theDimension = BITAMCInfoDimensions::NewInstance($recordset);
// Ligamos la dimension al cubo
							$theInfoCube->a_dimensions_others[$consecutivo] = $theDimension;

					        $recordset->MoveNext();
					    }
					}
					
// llenamos los indicadores
					$ssql =
					'select t1.CLA_INDICADOR as m_key, '.
					'		t1.NOM_INDICADOR as m_indicator_name, '.
					'		t1.CLA_CONCEPTO as m_cube_key, '.
			        '   	t1.FORMULA_BD as m_formule_db, '.
			        '		t1.FORMATO as m_format,'.
			        '		t1.DESCRIPCION as m_description, '.
			        '		t1.FILTRO as m_filter, '.
			        '		t1.PARETTO as m_paretto, '.
			        '		t2.NOM_TABLA as m_table_name, '.
			        '   	t1.NO_EJECUTIVO as m_not_ejecutive, '.
			        '		t1.DIMENSIONES as m_dimensions, '.
			        '		t1.FORMULA_USR as m_formule_user, '.
			        '   	t1.NIVEL_JERARQUIA as m_hierarchy_level, '.
			        '		t1.TIPO_INDICADOR as m_indicator_type, '.
			        '		t1.ES_ATRIBUTO as m_is_atributte, '.
			        '   	t1.ES_DIVISOR as m_is_divisor, '.
			        '		t1.ACTUAL as m_actual, '.
			        '		t1.CLA_PERIODO as m_period_key, '.
			        '		t1.DECREMENTAL as m_decrement, '.
			        '		t1.DIM_DEPEND as m_dim_depend, '.
			        '	    t1.FILTER_SQL as m_filter_sql, '.
			        '		t1.NIVELES as m_niveles '.
			        'from   SI_INDICADOR t1, SI_CONCEPTO t2 '.
			        'where  t1.CLA_CONCEPTO = t2.CLA_CONCEPTO '.
			        '		and t1.TIPO_INDICADOR in (1, 3, 5)'.
			        '		and t2.CLA_CONCEPTO = '.$pcubekey;

					$recordset = $prepository->ADOConnection->Execute($ssql);
					if (!$recordset)
						print $prepository->ADOConnection->ErrorMsg();
					else
					{
					    while (!$recordset->EOF)
					    {

					        $theIndicator = BITAMCInfoIndicators::NewInstance($recordset);
// Obtenemos el consecutivo de la dimension
							$nkey = $theIndicator->key;
// Ligamos los indicadores al cubo
							$theInfoCube->a_indicators[$nkey] = $theIndicator;

					        $recordset->MoveNext();
					    }
					}

// Llenamos las alarmas
					$ssql =
					'select	CLA_INDICADOR, CLA_PERIODO, CLA_PROP, PRIORIDAD, '.
					'		OPERADOR, VALOR_BD, CLA_INDICADOR_PROP '.
					'from	SI_PROPxIND '.
					'where	CLA_INDICADOR in '.
					'		( select CLA_INDICADOR from SI_INDICADOR '.
					'		  where	 TIPO_INDICADOR in (1, 3, 5) and CLA_CONCEPTO = '.$pcubekey.' ) '.
					'order by CLA_INDICADOR, PRIORIDAD';

					$recordset = $prepository->ADOConnection->Execute($ssql);

					if (!$recordset)
						print $prepository->ADOConnection->ErrorMsg();
					else
					{
						$indicator_key_prev = -99;
						$ncont = 0;
						while (!$recordset->EOF)
						{
							if ($indicator_key_prev != $recordset->fields[strtolower('CLA_INDICADOR')])
							{
								$indicator_key_prev = $recordset->fields[strtolower('CLA_INDICADOR')];

								$theInfoCube->a_alarms[$indicator_key_prev] = new BITAMCIndicatorAlarms();
								$theInfoCube->a_alarms[$indicator_key_prev]->indicatorkey = $indicator_key_prev;

								$ncont = 1;
								$theInfoCube->a_alarms[$indicator_key_prev]->a_alarms[$ncont] = new BITAMCAlarm($recordset);

							}
							else
							{
								$ncont++;
								$a_indicatorskey[$indicator_key_prev]->a_alarms[$ncont] = new BITAMCAlarm($recordset);
							}

							$recordset->MoveNext();
						}

					}

/*
					$theIndicatorAlarms = $theInfoCube->a_alarms;

					if ( $theIndicatorAlarms )
						foreach ($theIndicatorAlarms as $theIndicatorAlarms)
						{
							print '<BR>'.$theIndicatorAlarms->indicatorkey.'<BR>';

							foreach ($theIndicatorAlarms->a_alarms as $theIndicatorAlarm)
							{
								print '<BR>'.$theIndicatorAlarms->indicatorkey.'<BR>';
								print '<BR>'.$theIndicatorAlarm->key.'<BR>';
								print '<BR>'.$theIndicatorAlarm->condition.'<BR>';
								print '<BR>'.$theIndicatorAlarm->formula_bd.'<BR>';
							}

						}
	*/

// atributos o indicadores base
					$ssql =
					'select	CONSECUTIVO, NOM_FISICO, AGRUPADOR, NOM_LOGICO, CREA_INDICADOR, VALOR_DEFAULT, SQL_SOURCE, SQL_COLUMN '.
					'from	SI_CPTO_ATRIB '.
					'where	CLA_CONCEPTO = '.$pcubekey. ' order by CONSECUTIVO';

					$recordset = $prepository->ADOConnection->Execute($ssql);

					if (!$recordset)
						print $prepository->ADOConnection->ErrorMsg();
					else
					{
						while (!$recordset->EOF)
						{
							$oatrib = new BITAMCInfoAtrib();
							
							$oatrib->key = (int) $recordset->fields[strtolower('CONSECUTIVO')];
							$oatrib->nom_fisico = rtrim($recordset->fields[strtolower('NOM_FISICO')]);
							$oatrib->nom_logico = rtrim($recordset->fields[strtolower('NOM_LOGICO')]);
							$oatrib->agrupador = rtrim($recordset->fields[strtolower('AGRUPADOR')]);
							$oatrib->cla_indicador = (int) $recordset->fields[strtolower('CREA_INDICADOR')];
							$oatrib->default_key = (int) $recordset->fields[strtolower('VALOR_DEFAULT')];
							
							$oatrib->sql_source = rtrim($recordset->fields[strtolower('SQL_SOURCE')]);
							$oatrib->sql_column = rtrim($recordset->fields[strtolower('SQL_COLUMN')]);
							
							$oatrib->SaveFormula();

							$theInfoCube->a_atribs[] = $oatrib;
							$recordset->MoveNext();
						}
					}

// @EA 2011-02-18
					unset($recordset);
					unset($oatrib);
					$sql = 
					'select	CLA_INDICADOR as key_indicator, FORMULA_BD as formula_bd from SI_INDICADOR where CLA_CONCEPTO = '.$pcubekey.
					' and NOT IN_AGGREGATION is NULL and IN_AGGREGATION = 1 '.
					' order by CLA_INDICADOR';
					$aRS = $prepository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						savelogfile("ModelAddAgregation : ".$prepository->ADOConnection->ErrorMsg().". Executing: ".$sql, true);
						$sql = 'ALTER TABLE SI_INDICADOR ADD IN_AGGREGATION int NULL';
						$prepository->ADOConnection->Execute($sql);
					}
					else
					{
						while (!$aRS->EOF)
						{
							$ocounts_aggregation = new BITAMCInfoAggregationCount();
							
							$ocounts_aggregation->indicator_key = (int) $aRS->fields['key_indicator'];
							$ocounts_aggregation->formula_bd = trim($aRS->fields['formula_bd']);
							
							$theInfoCube->a_counts_aggregations[] = $ocounts_aggregation;
							$aRS->MoveNext();
						}
					}
					unset($aRS);
					unset($ocounts_aggregation);
//

// Agregaciones del cubo
					if ($theInfoCube->ag_new)
					{
						$ssql =
						'select	CONSECUTIVO as ID, NOM_FISICO, DIMENSIONES as dimension_list '.
						'from	SI_AG_NEW '.
						'where	CLA_CONCEPTO = '.$pcubekey.' '.
						'order by PESO';
					}
					else
					{
						$ssql =
						'select	ID, NOM_FISICO, \'\' as dimension_list '.
						'from	SI_AGREGACIONES '.
						'where	CLA_CONCEPTO = '.$pcubekey.' '.
						'order by ID';
					}

					$recordset = $prepository->ADOConnection->Execute($ssql);

					if (!$recordset)
						print $prepository->ADOConnection->ErrorMsg();
					else
					{
						while (!$recordset->EOF)
						{
//							$theInfoCube->a_agregations[number_format($recordset->fields[strtolower('ID')])] = new BITAMCInfoAgregation($recordset);
							$theInfoCube->a_agregations[] = new BITAMCInfoAgregation($recordset);
							$recordset->MoveNext();
						}
					}

// Creamos una instancia, para obtener los queries de tipo ROLAP
//					$theRolap = BITAMCCreateQuery::NewInstance();
					$theRolap = null;
					if (!is_null($theRolap))
					{
/*======================================================================
	Aqui empieza la funcionalidad del componente de ROLAP
  ======================================================================*/
// 1) Constantes
						$theRolap->a_constants = BITAMCInfoConstantCollection::NewInstance($prepository)->a_constants;
// a) Carga de Llaves para los JOINS
						$theRolap->FillJoinKeys($theInfoCube->a_join_keys);
// b) Soporte de 'Sin Periodo'
						$theRolap->bSinPeriodo = $theInfoCube->use_period_less;
// c) Nombre fisico del Cubo, que corresponde con el nombre fisico de la tabla de hechos
						$theRolap->sCube = $theInfoCube->physical_name;
// d) Fecha, campo que representa la fecha
						$theRolap->sfecha = $theInfoCube->field_date;
// e) Clave del cubo
						$theRolap->nCubeKey = $theInfoCube->key;
// Tipo de cubo
						$theRolap->type = $theInfoCube->type;

						$theRolap->bZeroIndicators = $theInfoCube->zero_indicators;
						$theRolap->bNullIndicators = $theInfoCube->null_indicators;


// f) Tabla que representa la dimension de tiempo
						$theRolap->sDimPeriod = $theInfoCube->table_date;
// f2) Usa agregados propietarios
						$theRolap->use_agregation = $theInfoCube->use_agregation;
// g) Agrega las dimensiones del cubo
						$datatypefilter = -1;
						foreach ($theInfoCube->a_dimensions as $odimension)
						{
							if ($theInfoCube->use_filter_keys)
								$datatypefilter = $odimension->data_type_key;
							else
								$datatypefilter = $odimension->data_type_descrip;

							$theRolap->AddDimension( $odimension->physical_table,
													 $odimension->physical_name_key,
													 $odimension->key,
													 $odimension->physical_name_descrip,
													 $odimension->period_key,
													 $odimension->parameters,
													 $odimension->period_fmt,
													 $datatypefilter );
						}
// h) Usa cache
						$theRolap->bCache = $theInfoCube->use_cache;
// i) Mapea, periodos disponibles ??? mejor no lo pongo. Lo dejamos en el InsertPeriod

					}

					$theInfoCube->rolap = $theRolap;

					if (class_exists('BITAMGlobalInstance'))
						BITAMGlobalInstance::AddSDKCubeWithID($pcubekey, $theInfoCube);

					return $theInfoCube;
				}
			}
		}
		return null;
	}

// Soporte de agregaciones

	function GetExactlyAgregationId($sDimensionsInQuery)
	{
		$npos = 0;
		$nid = 0;

    	$nGetExactlyAgregationId = 0;
// Quitamos la primera coma
    	$sDimensionsInQuery = substr($sDimensionsInQuery, 1);
// Quitamos la ultima coma
//    	$sDimensionsInQuery = substr($sDimensionsInQuery, 0, strlen($sDimensionsInQuery)-1);

//    	print '<BR>$sDimensionsInQuery = '.$sDimensionsInQuery;

    	while (true)
    	{
	        $npos = strpos($sDimensionsInQuery, ',');
	        if ($npos === false)
	            break;

	        $nid = substr($sDimensionsInQuery, 0, $npos);
	        $sDimensionsInQuery = substr($sDimensionsInQuery, $npos + 1);

//    		print '<BR>$nid = '.$nid;
//    		print '<BR>$sDimensionsInQuery = '.$sDimensionsInQuery;

	        $nGetExactlyAgregationId = $nGetExactlyAgregationId + (pow(2, $nid));
//    		print '<BR>$nGetExactlyAgregationId = '.$nGetExactlyAgregationId;
    	}
    	return $nGetExactlyAgregationId;
	}


	function GetListAgregations(&$stablas_mapeadas = '')
	{
		$sGetAgregaciones = '';
		$stablas_mapeadas = '';
	    foreach ( $this->a_agregations as $agregation )
	    {
			if ($sGetAgregaciones == '')
			{
				$sGetAgregaciones = number_format($agregation->key,'','','');
				$stablas_mapeadas = $agregation->physical_name;
			}
			else
			{
				$sGetAgregaciones = $sGetAgregaciones.','.number_format($agregation->key,'','','');
				$stablas_mapeadas = $stablas_mapeadas.','.$agregation->physical_name;
			}
	    }
	    return $sGetAgregaciones;
	}
	
	function RefreshAgregationsForDimension($fecha_ini = null, $fecha_fin = null, $DimensionID)
	{
// @EA 2010-07-06

// Agregacion con combinacion de dimensiones				
			for ($nvcc = 0; $nvcc < count($this->a_agregations); $nvcc++ )
			{
// Solo hay que borrar las agregaciones que contienen la dimension
				if (!$this->ag_new)
				{
					$aDimensions = array();
					$AgregationID = $this->a_agregations[$nvcc]->key;
					$this->GetAgregationsKeys($AgregationID, $aDimensions);
					$this->a_agregations[$nvcc]->dimension_list = '';
					for ($nvcc2 = 0; $nvcc2 < top($aDimensions); $nvcc2++)
					{
						$this->a_agregations[$nvcc]->dimension_list = $this->a_agregations[$nvcc]->dimension_list.','.$aDimensions[$nvcc2];
					}
				}

				$sDimensiones = ','.$this->a_agregations[$nvcc]->dimension_list.',';
				
				if (!(strpos($sDimensiones, ','.$DimensionID.',') === false))
				{
					$AgregationID = $this->a_agregations[$nvcc]->key;
					$this->RefreshAgregations($fecha_ini, $fecha_fin, '', $AgregationID, $this->a_agregations[$nvcc]->dimension_list, true, false);
				}
				
			}
			
			if (count($this->a_agregations) > 0)
			{
				$this->RefreshAgregations($fecha_ini, $fecha_fin, '', null, null, false, true);
			}
	}
	
	function RefreshAgregations($fecha_ini = null, $fecha_fin = null, $sDates = '', $AgregationID = null, $DimensionListIDs = null, $RefreshDimensions = true, $RefreshTotals = true, $OnlyDelete = false)
	{
// @EA 2006-07-17 Recibimos la lista de fechas que corresponden con los periodos a actualizar, en los agregados

		if ($this->use_agregation)
		{

//			print '<br>usa agregaciones<br>';

// Hay que recorrer la lista de agregaciones disponibles
			$topAg = count($this->a_agregations);
			
			if ($topAg > 0 )
			{
				
				$topPeriod = count($this->a_period_keys);
	
	//			print '<br>$topAg'.$topAg.'<br>';
	//			print '<br>$topPeriod'.$topPeriod.'<br>';
	
	
				$indexAg = 0;
				$indexPeriod = 0;

				if ($RefreshDimensions)
				{
					if (is_null($AgregationID))
					{
						for ($indexAg = 0; $indexAg < $topAg; $indexAg++)
						{
		// Hay que recorrer la lista de periodos disponibles
							for ($indexPeriod = 0; $indexPeriod < $topPeriod; $indexPeriod++)
							{
		//					print '<br>$this->a_agregations[$indexAg]->key = '.$this->a_agregations[$indexAg]->key.'<br>';
								$this->RefreshAgregation($sDates, $this->a_agregations[$indexAg]->key, $this->a_period_keys[$indexPeriod], $fecha_ini, $fecha_fin, $this->a_agregations[$indexAg]->dimension_list, $OnlyDelete);
							}
						}
					}
					else
					{
		// Hay que recorrer la lista de periodos disponibles
						for ($indexPeriod = 0; $indexPeriod < $topPeriod; $indexPeriod++)
						{
		//					print '<br>$this->a_agregations[$indexAg]->key = '.$this->a_agregations[$indexAg]->key.'<br>';
		
							$this->RefreshAgregation($sDates, $AgregationID, $this->a_period_keys[$indexPeriod], $fecha_ini, $fecha_fin, $DimensionListIDs, $OnlyDelete);
						}
					}
				}
	// Ahora actualizamos los agregados ceros
				if ($RefreshTotals)
				{
					for ($indexPeriod = 0; $indexPeriod < $topPeriod; $indexPeriod++)
					{
						$this->RefreshAgregation($sDates, 0, $this->a_period_keys[$indexPeriod], $fecha_ini, $fecha_fin, '', $OnlyDelete);
					}
				}
			}
		}
		
		return true;
	}

	function IncrementalAgregations($arrDates, $arrDimValues, $arrDimDescrips, $arrIndValues, $arrDimIDs, $arrIndFields, &$sErrDescription='')
	{
		$sDates = '';
		$sErrDescription = '';
// arrDates = Lista de fechas a agregar por registro
// arrDimValues = Matriz por registro/columna con los valores de dimensiones
// arrIndValues = Matriz por registro/columna con los valores de indicadores
// arrDimIDs = Matriz con los consecutivos equivalentes al orden de las columnas de dimensiones de arrDimValues
// arrIndFields = Matriz con los nombres de campos equivalentes al orden de las columnas de indicadores de arrIndValues

// @EA 2006-07-17 Recibimos la lista de fechas que corresponden con los periodos a actualizar, en los agregados
		if ($this->use_agregation)
		{

			$topDates = count($arrDates);
//			print '<br>usa agregaciones<br>';

// Hay que recorrer la lista de agregaciones disponibles
			$topAg = count($this->a_agregations);
			$topPeriod = count($this->a_period_keys);

//			print '<br>$topAg'.$topAg.'<br>';
//			print '<br>$topPeriod'.$topPeriod.'<br>';


			$indexAg = 0;
			$indexPeriod = 0;
			for ($indexAg = 0; $indexAg < $topAg; $indexAg++)
			{
// Hay que recorrer la lista de periodos disponibles
				for ($indexPeriod = 0; $indexPeriod < $topPeriod; $indexPeriod++)
				{
//					print '<br>$this->a_agregations[$indexAg]->key = '.$this->a_agregations[$indexAg]->key.'<br>';
					for ($indexDates=0; $indexDates < $topDates; $indexDates++)
					{
						$sDates = $arrDates[$indexDates];
						$this->IncrementalAgregation($indexDates, $sDates, $arrDimValues, $arrDimDescrips, $arrIndValues, $arrDimIDs, $arrIndFields, $this->a_agregations[$indexAg]->key, $this->a_period_keys[$indexPeriod], $sErrDescription, $this->a_agregations[$indexAg]->consecutivo);
					}
				}
			}
// Ahora actualizamos los agregados ceros
			for ($indexPeriod = 0; $indexPeriod < $topPeriod; $indexPeriod++)
			{
				for ($indexDates=0; $indexDates < $topDates; $indexDates++)
				{
					$sDates = $arrDates[$indexDates];
					$this->IncrementalAgregation($indexDates, $sDates, $arrDimValues, $arrDimDescrips, $arrIndValues, $arrDimIDs, $arrIndFields, 0, $this->a_period_keys[$indexPeriod], $sErrDescription, 0);
				}
			}
		}
		return true;
	}
	
	function IncrementalAgregation($indexDates, $sDates, $arrDimValues, $arrDimDescrips, $arrIndValues,
								   $arrDimIDs, $arrIndFields, $IDAgregation, $PeriodKey, &$sErrDescription = '', $consecutivo = 0)
	{

/*

	$arrDimKeyValues = array ( 0 => array (0 => '6-01-02-000-00           ', 1 => 0),
									1 => array (0 => '6-01-02-000-00           ', 1 => 4),
									2 => array (0 => '6-01-02-000-00           ', 1 => 7));
	$arrIndicValues = array (0 => array (0 => 992), 1 => array (0 => 465), 2 => array (0 => 2985));
	$arrDateValues = array(0 => '2006-04-01 00:00:00', 1 => '2006-02-01 00:00:00', 2 => '2006-01-01 00:00:00');
	$selectedDim = array (0 => 3, 1=> 6);
	$arrIndicFields = array (0 => 'pres_corp');
*/
		$sPeriodColumnName = '';
		$sDimValues = '';
		$sErrDescription = '';
// Dimension principal del periodo de tiempo
		foreach ($this->a_dimensions as $odimension)
		{
			if ($PeriodKey == $odimension->period_key)
			{
				$sPeriodColumnName = $odimension->physical_name_key;
				break;
			}
		}
		
		$theCubeRolap = $this->rolap;
		
		$theCubeRolap->Initialize();

		$theCubeRolap->bTendencia = true;

// Dimensiones involucradas en el agregado
		if ($IDAgregation != 0)
		{
			$aDimensions = array();
			$topDimIDs = count($arrDimIDs);
			$this->GetAgregationsKeys($IDAgregation, $aDimensions);

			$topDimensions = count($aDimensions);

//			print '<br>$topDimensions ='.$topDimensions.'<br>';

			$sDimensionsInsert = '';

			for ($indexDimension = $topDimensions; $indexDimension >= 1; $indexDimension--)
			{

//				print '<br>$aDimensions[$indexDimension] ='.$aDimensions[$indexDimension].'<br>';

				$odimension = $this->a_dimensions[$aDimensions[$indexDimension]];

				for ($indexDimIDs = 0; $indexDimIDs < $topDimIDs; $indexDimIDs++)
				{
					if ($arrDimIDs[$indexDimIDs] == $aDimensions[$indexDimension])
					{
						break;
					}
				}

				if ($sDimensionsInsert == '')
				{
					$sDimensionsInsert = $odimension->physical_name_key.$aDimensions[$indexDimension];
					$sDimValues = $arrDimValues[$indexDates][$indexDimIDs];
				}
				else
				{
					$sDimensionsInsert = $sDimensionsInsert.','.$odimension->physical_name_key.$aDimensions[$indexDimension];
					$sDimValues = $sDimValues.','.$arrDimValues[$indexDates][$indexDimIDs];
				}

				if ($odimension->physical_name_key != $odimension->physical_name_descrip)
				{
					$sDimensionsInsert = $sDimensionsInsert.','.$odimension->physical_name_descrip.$aDimensions[$indexDimension];
					$sDimValues = $sDimValues.','.$arrDimDescrips[$indexDates][$indexDimIDs];
				}

				//$theCubeRolap->InsertDimension('['.$odimension->physical_table.')].['.$odimension->physical_name_descrip.')]');
			}
		}

		$theCubeRolap->InsertPeriod($PeriodKey, '2005-01-01', '2005-01-31', true, false, $sDates, '', false, '' );

		$topAtribs = 0;
		$topAtribs = count($this->a_atribs);

		$sAtribsInsert = '';
		$sIndValues = '';
		$sIndVal = '';

		$topIndValues = count($arrIndFields);

// Lista de indicadores
		for ($indexAtrib = 0; $indexAtrib < $topAtribs; $indexAtrib++)
		{
			$oatrib = $this->a_atribs[$indexAtrib];

			$sIndVal = '0';


			if (array_key_exists($oatrib->nom_fisico, $arrIndValues[$indexDates]))
			{
				$sIndVal = $arrIndValues[$indexDates][$oatrib->nom_fisico];
			}
/*
			for ($indexIndValues=0; $indexIndValues < $topIndValues; $indexIndValues++)
			{
				if ($oatrib->nom_fisico == $arrIndFields[$indexIndValues])
				{
					$sIndVal = $arrIndValues[$indexDates][$indexIndValues];
					break;
				}
			}
*/

			if ($indexAtrib == 0)
			{
				$sAtribsInsert = $oatrib->nom_fisico;
				$sIndValues = $sIndVal;
			}
			else
			{
				$sAtribsInsert = $sAtribsInsert.', '.$oatrib->nom_fisico;
				$sIndValues = $sIndValues.','.$sIndVal;
			}
			$theCubeRolap->InsertIndicator($oatrib->formula_indicator, false, 0, -1, -1);
		}


		if ($this->ag_new)
			$sTable = 'AN_'.$this->key.'_'.$PeriodKey.'_'.formatNumber($consecutivo, '0');
		else
			$sTable = 'A_'.$this->key.'_'.$PeriodKey.'_'.formatNumber($IDAgregation, '0');


		$sInsert = 'insert into '.$sTable;

		$sInsertValues = ' values ( '.$theCubeRolap->sfechas;

		if ($IDAgregation != 0)
		{
			$sInsert = $sInsert.' ( '.$sPeriodColumnName.','.$sDimensionsInsert.','.$sAtribsInsert.' )';
			$sInsertValues = $sInsertValues.','.$sDimValues.','.$sIndValues.' )';
		}
		else
		{
			$sInsert = $sInsert.' ( '.$sPeriodColumnName.','.$sAtribsInsert.' )';
			$sInsertValues = $sInsertValues.','.$sIndValues.' )';
		}

//		print '<br>'.$sInsert.$sInsertValues;


		if ($this->connection->ADOConnection->Execute($sInsert.$sInsertValues) === false)
		{
			$sErrDescription = $this->connection->ADOConnection->ErrorMsg();
			return false;
//			die( "cinfocube.inc.php -> IncrementalAgregation: ".$this->connection->ADOConnection->ErrorMsg().' '.$sInsert.' '.$sSelect);
		}
		else
			return true;
	}

	
	function RefreshAgregation($sDates, $IDAgregation, $PeriodKey, $fecha_ini = null, $fecha_fin = null, $DimensionListIDs = null, $OnlyDelete = false)
	{
// @EA 2006-07-14 Realiza la actualizacion a la base de datos, correspondiente al agregado + periodo de tiempo en la
// 	lista de fechas correspondiente
		$sPeriodColumnName = '';
		
// Dimension principal del periodo de tiempo
		foreach ($this->a_dimensions as $odimension)
		{
			if ($PeriodKey == $odimension->period_key)
			{
				$sPeriodColumnName = $odimension->physical_name_key;
				break;
			}
		}

		if (is_null($this->connection))
			$this->connection = BITAMConnection::NewInstance($this->servidor, $this->user, $this->password, $this->database_name, $this->driver);
		
		$theCubeRolap = $this->rolap;
		
		if (is_null($theCubeRolap))
		{
			$this->CreateRolap();
			$theCubeRolap = $this->rolap;
		}
		
		$theCubeRolap->Initialize();

		$theCubeRolap->bTendencia = true;
		$theCubeRolap->use_agregation = false;

// Dimensiones involucradas en el agregado
		if ($IDAgregation != 0)
		{
			$aDimensions = array();
			
			if ($this->ag_new)
				$this->GetAgregationsKeys($DimensionListIDs, $aDimensions);
			else
				$this->GetAgregationsKeys($IDAgregation, $aDimensions);

			$topDimensions = count($aDimensions);

//			print '<br>$topDimensions ='.$topDimensions.'<br>';

			$sDimensionsInsert = '';

			for ($indexDimension = $topDimensions; $indexDimension >= 1; $indexDimension--)
			{

//				print '<br>$aDimensions[$indexDimension] ='.$aDimensions[$indexDimension].'<br>';

				$odimension = $this->a_dimensions[$aDimensions[$indexDimension]];

				if ($sDimensionsInsert == '')
					$sDimensionsInsert = $odimension->physical_name_key.$aDimensions[$indexDimension];
				else
					$sDimensionsInsert = $sDimensionsInsert.','.$odimension->physical_name_key.$aDimensions[$indexDimension];

				if ($odimension->physical_name_key != $odimension->physical_name_descrip)
				{
					$sDimensionsInsert = $sDimensionsInsert.','.$odimension->physical_name_descrip.$aDimensions[$indexDimension];
				}
				
				@savelogfile('InsertDimension ->'.'['.$odimension->physical_table.'].['.$odimension->physical_name_descrip.']');

				$theCubeRolap->InsertDimension('['.$odimension->physical_table.'].['.$odimension->physical_name_descrip.']');
			}
		}

//		$theCubeRolap->InsertPeriod($PeriodKey, '2005-01-01', '2005-01-31', true, false, '', '', false, '' );

// @EA 2011-03-03
		$DimTime = $this->table_date;
		$DateID = $this->field_date;
//		
		
		if (is_null($fecha_ini))
			$theCubeRolap->InsertPeriod($PeriodKey, '2005-01-01', '2005-01-31', true, false, $sDates, '', false, '' );
		else
		{
			
// @EA 2011-03-03
// Obtener las fechas reales para el periodo determinado, ya que si la fecha inicial y final corresponden a un mes
// pero si el periodo es anual tendria que redefinirse al rango anual
			$sSelect = 
			'select	min(t2.'.$DateID.') as fecha_ini_real, 	max(t2.'.$DateID.') as fecha_fin_real '.
			'from '.$DimTime.' t1, '.$DimTime.' t2 '.
			'where t2.'.$sPeriodColumnName.' = t1.'.$sPeriodColumnName.' and t1.'.$DateID.' between '.$this->connection->ADOConnection->DBDate($fecha_ini).' and '.$this->connection->ADOConnection->DBDate($fecha_fin);
			$rst = $this->connection->ADOConnection->Execute($sSelect);
			
			savelogfile('$sSelect -> '.$sSelect);
			
			$fecha_ini_real = '';
			$fecha_fin_real = '';

			if ($rst)
			{
				$fecha_ini_real = date('Y-m-d', strtotime($rst->fields['fecha_ini_real']));
				$fecha_fin_real = date('Y-m-d', strtotime($rst->fields['fecha_fin_real']));
			}
			
			unset($rst);

			savelogfile('$fecha_ini -> '.$fecha_ini);
			savelogfile('$fecha_ini_real -> '.$fecha_ini_real);
			savelogfile('$fecha_fin -> '.$fecha_fin);
			savelogfile('$fecha_fin_real -> '.$fecha_fin_real);
				
			if ($fecha_ini > $fecha_ini_real or $fecha_fin < $fecha_fin_real)
			{
				$OnlyDelete = false;
				
				$fecha_ini = $fecha_ini_real;
				$fecha_fin = $fecha_fin_real;
				
				savelogfile('$fecha_ini OK -> '.$fecha_ini);
				savelogfile('$fecha_fin OK -> '.$fecha_fin);
				
			}

			unset($rst);
//

			$theCubeRolap->InsertPeriod($PeriodKey, $fecha_ini, $fecha_fin, true, false, '', '', false, '' );
			$theCubeRolap->sfecha_group_sel = 't2.'.$sPeriodColumnName;
			$theCubeRolap->sfecha_group = 't2.'.$sPeriodColumnName;
			$theCubeRolap->HayFechasQueAgrupar = true;
		}

		$topAtribs = 0;
		$topAtribs = count($this->a_atribs);

		$sAtribsInsert = '';

// Lista de indicadores
		for ($indexAtrib = 0; $indexAtrib < $topAtribs; $indexAtrib++)
		{
			$oatrib = $this->a_atribs[$indexAtrib];
			if ($indexAtrib == 0)
			{
				$sAtribsInsert = $oatrib->nom_fisico;
			}
			else
			{
				$sAtribsInsert = $sAtribsInsert.', '.$oatrib->nom_fisico;
			}
			$theCubeRolap->InsertIndicator($oatrib->formula_indicator, false, 0, -1, -1);
		}
		
// @EA 2011-02-18 Lista de indicadores tipo COUNT los cuales solo se aplicarian en las consultas donde sea a nivel registro ( todos los elemenetos filtrados )		
		foreach ($this->a_counts_aggregations as $ocounts_aggregation)
		{
			$sAtribsInsert .= ', COUNT_'.$ocounts_aggregation->indicator_key;
			$theCubeRolap->InsertIndicator($ocounts_aggregation->formula_bd, false, 0, -1, -1);
		}
		
		unset($ocounts_aggregation);
//

		if ($this->ag_new)
			$sTable = 'AN_'.$this->key.'_'.$PeriodKey.'_'.formatNumber($IDAgregation, '0');
		else
			$sTable = 'A_'.$this->key.'_'.$PeriodKey.'_'.formatNumber($IDAgregation, '0');

		$sInsert = 'insert into '.$sTable;

		if ($IDAgregation != 0)
			$sInsert = $sInsert.' ( '.$sPeriodColumnName.','.$sDimensionsInsert.','.$sAtribsInsert.' )';
		else
			$sInsert = $sInsert.' ( '.$sPeriodColumnName.','.$sAtribsInsert.' )';

		$theCubeRolap->bMigrador = true;

		$sSelect = '';
		$sSelect = $theCubeRolap->QueryText();
		$sSelect = str_replace('<BR>', ' ', $sSelect );
		
		if (is_null($fecha_ini))
			$sDelete = 'delete from '.$sTable.' where '.$sPeriodColumnName.' in ('.$theCubeRolap->sfechas.')';
		else
		{
			$sDelete = 'delete from '.$sTable.' where '.$sPeriodColumnName.' in (select '.$sPeriodColumnName.' from '.$DimTime.' where '.$DateID.' between '.$this->connection->ADOConnection->DBDate($fecha_ini).' and '.$this->connection->ADOConnection->DBDate($fecha_fin).')';
		}
		
		@savelogfile(' $sDelete -> '.$sDelete);
		
		if (!$OnlyDelete)
		{
			@savelogfile(' $sInsert -> '.$sInsert);
			@savelogfile(' $sSelect -> '.$sSelect);
		}

		$this->connection->ADOConnection->Execute($sDelete);

		if ($this->connection->ADOConnection->Execute($sDelete) === false)
		{
			die( "cinfocube.inc.php -> RefreshAgregation: ".$this->connection->ADOConnection->ErrorMsg().' '.$sDelete);
		}
		
		if (!$OnlyDelete)
		{
			if ($this->connection->ADOConnection->Execute($sInsert.' '.$sSelect) === false)
			{
				die( "cinfocube.inc.php -> RefreshAgregation: ".$this->connection->ADOConnection->ErrorMsg().' '.$sInsert.' '.$sSelect);
			}
		}

// En el caso de ORACLE hacemos un COMMIT
		if($this->cla_bd == 3)
			$this->connection->ADOConnection->Execute('commit');
//
		return true;

	}
	
	function CreateRolap()
	{
		if (!class_exists('BITAMCCreateQuery')) {
		    require_once('../rolap/ccreatequery.inc.php');
		}
		
		$the_rolap = BITAMCCreateQuery::NewInstance();
		$the_rolap->type = 3;
//		$the_rolap->languageid = $this->languageid;
	
		$the_rolap->FillJoinKeys($this->a_join_keys);
		$the_rolap->bSinPeriodo = $this->use_period_less;
		$the_rolap->sCube = $this->physical_name;
		$the_rolap->sfecha = $this->field_date;
		$the_rolap->nCubeKey = $this->key;
		$the_rolap->sDimPeriod = $this->table_date;
		$the_rolap->use_agregation = $this->use_agregation;
		$data_type_filter = -1;
		$the_rolap->bNullIndicators = $this->null_indicators;
		$the_rolap->bZeroIndicators = $this->zero_indicators;
		foreach ($this->a_dimensions as $odimension)
		{
		  if ($odimension->period_key != -1 and strtolower($this->table_date) == strtolower($odimension->physical_table) ) {
		       if ($this->datekey == 1)
		            $data_type_filter = $odimension->data_type_key;
		       else
		            $data_type_filter = $odimension->data_type_descrip;
		  } else { 
		  if ($this->use_filter_keys)
		      $data_type_filter = $odimension->data_type_key;
		  else
		      $data_type_filter = $odimension->data_type_descrip; }
		  if ($data_type_filter == 0 )
		    $data_type_filter = 2;
		  elseif ($data_type_filter == 2 )
		    $data_type_filter = 1;
		  else 
		    $data_type_filter = -1;
		  $the_rolap->AddDimension( $odimension->physical_table,
		                            $odimension->physical_name_key,
		                            $odimension->key,
		                            $odimension->physical_name_descrip,
		                            $odimension->period_key,
		                            $odimension->parameters,
		                            $odimension->period_fmt,
		                            $data_type_filter );
		}
		$the_rolap->bCache = false;
		
		$the_rolap->oConnection = new BITAMConnAux();
		$the_rolap->oConnection->ADOConnection = $this->connection->ADOConnection;
		
		$this->rolap = $the_rolap;
		
		global $ComCube;
		
		$ComCube[$this->key] = $this;
	}

	function GetAgregationsKeys($id, &$Llaves)
	{
		if ($this->ag_new)
		{
			$i = 0;
			$sdim = BITAMGetSiguiente($id);
			while ($sdim !== '')
			{
			    $i++;
				$Llaves[$i] = $sdim;
				$sdim = BITAMGetSiguiente($id);
			}
		}
		else
		{
			$Aux = 0.0;
			$i = 0;
			$Paso = 0;
			$ID_Actual = 0;
			$Resta = 0;

			$Aux = $id;
			$i = 0;

			while ($Aux > 0)
			{
			    $i++;
			    $Paso = $Aux;

			    $ID_Actual = 0;
			    $Resta = 1;
			    while ($Paso >= 2)
			    {
			        $ID_Actual++;
			        $Resta = $Resta * 2;

	//		        print '<br>'.$Paso.'<br>';

			        $Paso = $Paso - ($Paso % 2);
			        $Paso = $Paso / 2;
			    }
			    $Llaves[$i] = $ID_Actual;

	//	        print '<br>$ID_Actual = '.$ID_Actual.'<br>';
	//	        print '<br>$i = '.$i.'<br>';

			    $Aux = $Aux - $Resta;
			}
		}
	//		sort($Llaves);
	//		reset($Llaves);
	}

	function GetAgregation($id, &$sAgregacionMapeada = '', $sIndicadores = '')
	{
		$LlavesA = array();
		$LlavesB = array();
		$Paso = '';
		$nAgregacion = 0;
		$sTablasMapeadas = '';
		$sTablaMapeada = '';

		$nRegresa_Agregacion = -1;

		if (!$this->use_agregation)
		    return;

		if ($id == -1)
		    return;


		$sDimsNoVisibles = '';
/*
		$sDimsNoVisibles = $this->GetDimsNoVisibles($sIndicadores);
*/

		if ($id > 0)
		{
		    $Paso = $this->GetListAgregations($sTablasMapeadas);

//		    print '<BR>$Paso = '.$Paso;

		    $sTablaMapeada = BITAMGetSiguiente($sTablasMapeadas);

		    $nAgregacion = floatval(BITAMGetSiguiente($Paso));

//		    print '<BR>$nAgregacion = '.$nAgregacion;

		    while ($nAgregacion > 0 and $nAgregacion < $id)
		    {
		        $nAgregacion = floatval(BITAMGetSiguiente($Paso));
		        $sTablaMapeada = BITAMGetSiguiente($sTablasMapeadas);
		    }


		    if ($nAgregacion > 0)
		    {
		        if ($nAgregacion == $id)
		        {
		            $nRegresa_Agregacion = $id;
		            $sAgregacionMapeada = $sTablaMapeada;
		        }
		        else
		        {
		            $TopA = 0;
		            $TopB = 0;
		            $i = 0;
		            $contador = 0;
		            $LlavesA = array();

		            $this->GetAgregationsKeys($id, $LlavesA);

		            $TopA = count($LlavesA);

		            while ($nAgregacion > 0)
		            {
		            	$LlavesB = array();

		                $this->GetAgregationsKeys($nAgregacion, $LlavesB);

		                $TopB = count($LlavesB);
		                $contador = 1;

		                if ($TopB >= $TopA)
		                {
		                	if ($sDimsNoVisibles != '')
		                	{
			                    for ($i = 1; $i <= $TopB; $i++)
			                    {
			                        if (strpos($sDimsNoVisibles, ','.$LlavesB($i).',') !== false)
			                        {
			                            $TopB = 0;
			                            break;
			                        }
			                    }
		                	}
		                    for ($i = 1; $i <= $TopB; $i++)
		                    {
		                        if ($LlavesA[$contador] == $LlavesB[$i])
		                            $contador++;
		                        elseif ($LlavesA[$contador] > $LlavesB[$i])
		                            $i = $TopB;

		                        if ($contador - 1 == $TopA)
		                            break;
		                    }
		                }
		                if ($TopA == $contador - 1)
		                {
		                    $nRegresa_Agregacion = $nAgregacion;
		                    $sAgregacionMapeada = $sTablaMapeada;
		                    break;
		                }
		                $nAgregacion = floatval(BITAMGetSiguiente($Paso));
		                $sTablaMapeada = BITAMGetSiguiente($sTablasMapeadas);
		            }
		        }
		    }
		}
		else
		{
		    $nRegresa_Agregacion = 0;
		    if (trim($this->olap_catalog) != '')
		        $sAgregacionMapeada = trim($this->olap_catalog);
		}

		return $nRegresa_Agregacion;

	}
}

class BITAMCInfoKeys
{
	public $table_name = "";
	public $source_key = "";
	public $target_key = "";

	function __construct($precordset)
	{
		if (! empty( $precordset)) {
		$this->table_name = $precordset->fields[strtolower('NOM_TABLA')];
		$this->source_key = $precordset->fields[strtolower('NOM_FISICOK')];
		$this->target_key = $precordset->fields[strtolower('NOM_FISICOK_JOIN')];
		}
	}

	static function NewInstance($precordset = null)
	{
		return new BITAMCInfoKeys($precordset);
	}
}

class BITAMCInfoDimensions
{
	public $key;
	public $logical_name;
	public $physical_table;
    public $physical_name_key;
    public $data_type_key;
    public $long_key;
    public $physical_name_descrip;
    public $data_type_descrip;
    public $long_descrip;
    public $estatus;
	public $level;
	public $parameters;
	public $descriptor_key;
    public $group;
    public $edit_outline;
    public $period_key;
    public $period_fmt;
    public $IsIncremental;
    public $parent_keys = '';
    public $period_order;
    public $ordinal_level = -1;
    public $indicators = '';
    public $default_key = '';
    public $description_source = '';
    public $ExistsHomologeMembers = false;    
    public $naturalkey_source = '';
    public $InLayout = true;
    public $InETL = true;
    public $IsCreatedByMDAdministrator = false;
// @EA 2011-01-26    
    public $parent_key = -1;
//    

	function __construct($precordset)
	{
		$this->key = $precordset->fields[strtolower('m_key')];

//		print $this->key.'<BR>';

		$this->logical_name = rtrim($precordset->fields[strtolower('m_logical_name')]);
		$this->physical_table = rtrim($precordset->fields[strtolower('m_physical_table')]);
	    $this->physical_name_key =rtrim($precordset->fields[strtolower('m_physical_name_key')]);
	    $this->data_type_key = (int) $precordset->fields[strtolower('m_datatype_key')];
	    $this->long_key = (int) $precordset->fields[strtolower('m_long_key')];
	    $this->physical_name_descrip = rtrim($precordset->fields[strtolower('m_physical_name_descrip')]);
	    $this->data_type_descrip = (int) $precordset->fields[strtolower('m_datatype_descrip')];
	    $this->long_descrip = (int) $precordset->fields[strtolower('m_long_descrip')];
	    $this->estatus = (int) $precordset->fields[strtolower('m_estatus')];
		$this->level = (int) $precordset->fields[strtolower('m_level')];
		$this->parameters = rtrim($precordset->fields[strtolower('m_parameters')]);
		$this->descriptor_key = (int) $precordset->fields[strtolower('m_descriptor_key')];
	    $this->group = rtrim($precordset->fields[strtolower('m_group_global')]);
	    $this->edit_outline = (int) $precordset->fields[strtolower('m_edit_outline')];

// 0 = Layout 
// -1 = NO Layout
		$this->InLayout = true;
	    if ($this->edit_outline == -1)
	    {
	    	$this->InLayout = false;
	    }
	    
	    $this->period_key = is_null($precordset->fields[strtolower('m_period_key')]) ? -1 : $precordset->fields[strtolower('m_period_key')];
	    $this->period_fmt = rtrim($precordset->fields[strtolower('m_period_fmt')]);
	    $this->IsIncremental = rtrim($precordset->fields[strtolower('IsIncremental')]);
	    $this->period_order = (int) $precordset->fields[strtolower('m_orden_periodo')];
	    $this->ordinal_level = (int) $precordset->fields[strtolower('m_ordinal_level')];
	    
// 0 = ETL
// -1 = NO ETL
		$this->InETL = true;
	    if ($this->ordinal_level == -1)
	    {
	    	$this->InETL = false;
	    }
	    
	    $this->indicators = rtrim($precordset->fields[strtolower('m_measuredef')]);
	    $this->default_key = rtrim($precordset->fields[strtolower('m_default_key')]);

	    $this->description_source = rtrim($precordset->fields[strtolower('m_description_source')]);
	    
		$this->ExistsHomologeMembers = false;
	    if ($this->description_source == 'HOMOLOGE')
	    	$this->ExistsHomologeMembers = true;
/*	    
	    $this->naturalkey_source = rtrim($precordset->fields[strtolower('m_naturalkey_source')]);
*/	    
	    $this->description_source = '';
	    $this->naturalkey_source = '';
	    
	    if ($this->default_key == '')
	    {
	    	if ($this->data_type_key == 1 OR $this->data_type_key == 2)
	    		$this->default_key = '*NA';
	    	elseif ($this->data_type_key == 12)
	    		$this->default_key = '2000-01-01';
	    	else 
	    		$this->default_key = '-99999';
	    }
	    
	    $this->IsCreatedByMDAdministrator = false;
	    
	    $created_by = (int) $precordset->fields['created_by'];
	    
	    if ($created_by == 1)
	    	$this->IsCreatedByMDAdministrator = true;
	    	
// @EA 2011-01-26	    	
	    $this->parent_key = (int) $precordset->fields[strtolower('parent_key')];
	    if ($this->parent_key == 0)
	    	$this->parent_key = -1;
//	    	

	}

	static function NewInstance($precordset)
	{
		return new BITAMCInfoDimensions($precordset);
	}
	public function FormatedName(){
		return '['.$this->physical_table.'].['.$this->physical_name_descrip.']';
	}
}


class BITAMCAlarm
{
	public $key;
	public $periodkey;
	public $condition;
	public $formula_bd;
	public $prioridad;
	public $indicatorkey_prop;

	function __construct(&$precordset)
	{
		$key = $precordset->fields[strtolower('CLA_PROP')];
		$periodkey = $precordset->fields[strtolower('CLA_PERIODO')];
		$condition = $precordset->fields[strtolower('OPERADOR')];
		$formula_bd = $precordset->fields[strtolower('VALOR_BD')];
		$prioridad = $precordset->fields[strtolower('PRIORIDAD')];
		$indicatorkey_prop = $precordset->fields[strtolower('CLA_INDICADOR_PROP')];
	}
}

class BITAMCIndicatorAlarms
{
	public $indicatorkey;
	public $a_alarms;
}

class BITAMCInfoIndicators
{
	public $key;
	public $indicator_name;
	public $cube_key;
	public $formule_db;
	public $format;
	public $description;
	public $filter;
	public $paretto;
	public $table_name;
	public $not_ejecutive;
	public $dimensions;
	public $formule_user;
	public $hierarchy_level;
	public $indicator_type;
	public $is_atributte;
	public $is_divisor;
	public $actual;
	public $period_key;
	public $decrement;
	public $dim_depend;
	public $filter_sql;
	public $niveles;

	function __construct($precordset)
	{
		$this->key = $precordset->fields[strtolower('m_key')];
		$this->indicator_name = $precordset->fields[strtolower('m_indicator_name')];
		$this->cube_key = $precordset->fields[strtolower('m_cube_key')];
		$this->formule_db = $precordset->fields[strtolower('m_formule_db')];
		$this->format = $precordset->fields[strtolower('m_format')];
		$this->description = $precordset->fields[strtolower('m_description')];
		$this->filter = $precordset->fields[strtolower('m_filter')];
		$this->paretto = $precordset->fields[strtolower('m_paretto')];
		$this->table_name = $precordset->fields[strtolower('m_table_name')];
		$this->not_ejecutive = $precordset->fields[strtolower('m_not_ejecutive')];
		$this->dimensions = $precordset->fields[strtolower('m_dimensions')];
		$this->formule_user = $precordset->fields[strtolower('m_formule_user')];
		$this->hierarchy_level = $precordset->fields[strtolower('m_hierarchy_level')];
		$this->indicator_type = $precordset->fields[strtolower('m_indicator_type')];
		$this->is_atributte = $precordset->fields[strtolower('m_is_atributte')];
		$this->is_divisor = $precordset->fields[strtolower('m_is_divisor')];
		$this->actual = $precordset->fields[strtolower('m_actual')];
		$this->period_key = $precordset->fields[strtolower('m_period_key')];
		$this->decrement = $precordset->fields[strtolower('m_decrement')];
		$this->dim_depend = $precordset->fields[strtolower('m_dim_depend')];
		$this->filter_sql = $precordset->fields[strtolower('m_filter_sql')];
		$this->niveles = $precordset->fields[strtolower('m_niveles')];
	}

	static function NewInstance($precordset)
	{
		return new BITAMCInfoIndicators($precordset);
	}

}

?>