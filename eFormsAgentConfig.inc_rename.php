<?php

//@JAPR 2013-06-21: Agregado el agente de eForms
global $blnIsAgentEnabled;
$blnIsAgentEnabled = true;
//@JAPR 2013-07-11: Movidas estas configuraciones al archivo eFormsAgentConfig.inc.php para permitir incluirlo tanto en el servicio como en el propio
//agente y que en ambos casos tome las configuraciones sin tener que mantenerlas en varios lugares
global $arrDBsExcludedFromAgent;
$arrDBsExcludedFromAgent = array();
//Esta es una lista de bases de datos que son las únicas que van a usar el agente. Si no contiene elementos entonces se asume que todas las bases
//de datos usarán el agente El formato es $arrDBsExcludedFromAgent['FBM_BMD_####'] = 1;
//Las bases de datos que no se encuentren en esta lista, funcionarán tal como si estuviera en el array $arrDBsExcludedFromAgent
//@JAPR 2017-09-21: Agregado el array para forzar a generar tareas del agente a pesar de que el repositorio se encuentre excluído dentro de $arrDBsExcludedFromAgent,
//ya que se generará un agente específico que procesará las tareas de dicho repositorio, sólo el agente general NO los procesará (#0LC7NT)
global $arrDBsForcedForAgent;
$arrDBsForcedForAgent = array();
//@JAPR
global $arrDBsEnabledForAgent;
$arrDBsEnabledForAgent = array();
//@JAPR 2016-01-14: Modificado para que ahora el Agente sólo procese aquellas tareas que pertenecen exclusivamente a repositorios que están configurados en este
//servicio donde está corriendo el archivo php del Agente, independientemente de si dicho repositorio existe en el array $arrDBsEnabledForAgent, dicho array sólo
//servirá a partir de ahora para limitar las BDs que sean las únicas que se deban procesar con el Agente, lo cual en v6 es completamente irrelevante ya que todo
//repositorio debería ser procesado precisamente por el agente, así que con este cambio se podrá dejar $arrDBsEnabledForAgent completamente vacío, sólo llenar
//$arrDBsExcludedFromAgent con repositorios que por alguna extraña razón se quisieran poner en Stand By (es decir, no procesar sus capturas) y simplemente va a
//procesar todos los repositorios que se encuentren en el archivo .ini (#WUH8DI)
//Para facilitar las cosas, simplemente se sobreescribirá internamente el array $arrDBsEnabledForAgent con los repositorios del archivo EKT_cfg.ini, de tal manera
//que lo configurado manualmente en el archivo eFormsAgentConfig.inc.php será completamente irrelevante, así todos los repositorios del archivo .ini se considerarán
//válidos para procesar, excepto si están configurados en el array $arrDBsExcludedFromAgent
//Dejar el registro dummy por cualquier posible rollback a este cambio: $arrDBsEnabledForAgent['FBM_BMD_0000'] = 1;
//$arrDBsEnabledForAgent['FBM_BMD_0532'] = 1;
//@JAPR 2016-10-26: Adaptado el agente de eForms para que se pueda forzar al procesamiento de tareas de repositorios fijos pre-generando una tarea después de haber confirmado que
//existe por lo menos un mensaje sin procesar en el repositorio directamente, pero sólo para los que se encuentran en el array de repositorios especiapes para dicho trato
//Originalmente utilizado para Petrotech, donde MLopez vendió la idea de llenar desde una forma de eBavel la tabla de mensajes Push de eForms, pero no había manera de que se generara
//la tarea de este agente, así que había que forzar la revisión directa de la tabla de mensajes de eForms cada vez que el agente entraba a su proceso
//@JAPR
global $arrPushAgentDBsWithAutoTasks;
$arrPushAgentDBsWithAutoTasks = array();
//$arrPushAgentDBsWithAutoTasks['FBM_BMD_0532'] = 1;			//Llenar de esta manera en el servicio correspondiente, todos los repositorios que deberán auto-generar tareas de push
//@JAPR
?>