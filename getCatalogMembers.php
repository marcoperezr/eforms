<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$ObjectID = -1;
if (array_key_exists("CatalogID", $_GET))
{
	$ObjectID = $_GET["CatalogID"];
}

$isAgenda = false;
if (array_key_exists("isAgenda", $_GET))
{
	$isAgenda = $_GET["isAgenda"];
}

$getIDs = false;
if (array_key_exists("getIDs", $_GET))
{
	$getIDs = $_GET["getIDs"];
}

require_once("catalogmember.inc.php");

$strMembers = "";
$anInstanceCollection = BITAMCatalogMemberCollection::NewInstance($theRepository, $ObjectID);

if(!is_null($anInstanceCollection))
{
	foreach($anInstanceCollection->Collection as $objMember)
	{
		if ($isAgenda) {
			if(is_null($objMember->KeyOfAgenda) || $objMember->KeyOfAgenda == 0) {
				continue;
			}
		}
		if($strMembers!="")
		{
			$strMembers.="_SVSep_";
		}
		if ($getIDs) {
			$strMembers .= "".$objMember->MemberID."";
		} else {
			$strMembers.= "".$objMember->MemberName."";
		}
	}
}

header('Content-Type: text/plain; charset=utf-8');
echo $strMembers;
?>