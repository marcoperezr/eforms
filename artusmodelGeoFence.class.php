<?php

define('CREATED_BY_EBAVEL', 9);
define('CREATED_BY_EFORMSV6', 4);
define('CREATED_BY_EFORMSV5', 8);
define('CREATED_BY_SCHEDULER', 5);
define('CREATED_BY_MODELMANAGER', 6);

define('PRE_DIM_MPC_TABLE', 'RIMDIM_');
define('PRE_DIM_WPC_TABLE', 'RIWDIM_');
define('PRE_DIM_SEC_TABLE', 'RISDIM_');
define('PRE_DIM_CAT_TABLE', 'RICDIM_');
define('PRE_DIM_SVY_TABLE', 'RIVDIM_');
define('PRE_DIM_MPC_FIELD', 'MPCQ_');
define('PRE_DIM_DSC_FIELD', 'DSCQ_');
define('PRE_DIM_DSC_FIELD_INLINE', 'DSCS_');
define('PRE_DIM_MEM_FIELD', 'DSCM_');
define('PRE_DIM_MEM_FIELD_INLINE', 'DSCI_');
define('PRE_IND_FIELD', 'INDQ_');
define('PRE_IND_SCORE', 'SCOREQ_');

include_once('error.class.php');

require_once("../model_manager/model.inc.php");
require_once("../model_manager/modeldimension.inc.php");
require_once("../model_manager/attribdimension.inc.php");
require_once("../model_manager/indicatorkpi.inc.php");
require_once("../model_manager/indicator.inc.php");
require_once("../model_manager/modeldata.inc.php");
require_once("../model_manager/filescreator.inc.php");

class ARTUSModelFunctionBasics
{
	public static function existRow($aConnection, $sql, &$consecutivo=-1)
	{
		$consecutivo = -1;
		
		$recordSet = $aConnection->Execute($sql);
		
		//print '<br>'.$sql;

		if($recordSet)
		{
			if (!$recordSet->EOF)	
			{
				$consecutivo = (int) $recordSet->fields['consecutivo'];
				return true;
			}
		}
		else
		{
			$ErrSQL = $aConnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}

		return false;
	}
	
	public static function insertAttribute(&$metaconnection, $cla_descrip_parent, $cla_descrip_child, $nom_dimension, $nom_tabla, $nom_fisico)
	{
		// @EA 2016-01-12 Generar atributos para latitude, longitude y accuracy
		$ErrSQL = '';
		
		$sql =
		"INSERT INTO SI_ATRIBUTOS(CLA_DESCRIP, CONSECUTIVO, NOM_LOGICO, NOM_TABLA, NOM_FISICO, TIPO_DATO)
		 VALUES ({$cla_descrip_parent}, {$cla_descrip_child}, {$metaconnection->quote($nom_dimension)}, {$metaconnection->quote($nom_tabla)}, {$metaconnection->quote($nom_fisico)}, 2)";

		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
			
			//print '<br>'.$sql.' -> '.$ErrSQL;
		}		
		
		return $ErrSQL;		 
	}
	
	public static function insertGlobalDimension(&$metaconnection, $cla_descrip, $nom_dimension, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup)
	{
		$tipo_dimension = 3;
		$default_key = '\'NA\'';
		$ErrSQL = '';
		
		$sql  = 
		"INSERT INTO SI_DESCRIP_ENC
		(cla_descrip, cla_conexion, nom_logico, nom_fisico, tipo_dato,
			long_dato, nom_fisicok, tipo_datok, long_datok,
			nom_tabla, tipo_dimension, default_key, created_by, agrupador
			) VALUES (
			{$cla_descrip}, 1, {$metaconnection->quote($nom_dimension)},
			{$metaconnection->quote($nom_fisico)}, {$tipo_dato}, {$long_dato},
			{$metaconnection->quote($nom_fisico)}, {$tipo_dato}, {$long_dato},
			{$metaconnection->quote($nom_tabla)}, {$tipo_dimension}, {$default_key},
			".CREATED_BY_EFORMSV6.", {$metaconnection->quote($dimensionGroup)}
			)";
			
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}		
		
		return $ErrSQL;
	}
	
	public static function insertLocalDimension(&$metaconnection, $consecutivo, $nivel, $cla_descrip, $nom_dimension, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup, $cla_concepto, $par_descriptor = '')
	{
		$ErrSQL = '';
		
		if ($par_descriptor == '')
			$par_descriptor = $consecutivo;
			
		$sql = 
		"INSERT INTO si_cpto_llave
			(cla_concepto, consecutivo, nivel, cla_descrip, nom_descriptor, par_descriptor,
				nom_fisico, nom_logico, tipo_dato, long_dato, estatus, agrupador 
				) VALUES (
				{$cla_concepto}, {$consecutivo}, {$nivel}, {$cla_descrip},
				{$metaconnection->quote($nom_fisico)},
				{$metaconnection->quote($par_descriptor)},
				{$metaconnection->quote($nom_fisico)},
				{$metaconnection->quote($nom_dimension)},
				{$tipo_dato}, {$long_dato}, 1, {$metaconnection->quote($dimensionGroup)})";
				
		//print '<br>'.$sql;

		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}		
		return $ErrSQL;
	}
	
	public static function getMaxClaDescrip($aConnection)
	{
		$maxId = 1;
		
		$aQuery = 
		"SELECT MAX(CLA_DESCRIP) as cla_descrip 
		 FROM SI_DESCRIP_ENC ";
		 
		$recordSet = $aConnection->Execute($aQuery);
		if($recordSet)
			$maxId = $recordSet->fields['cla_descrip'] + 1;
		else
		{
			$ErrSQL = $aConnection->ErrorMsg();
			savelogfile($aQuery.' -> '.$ErrSQL);
		}
		
		return $maxId;
	}
	
	public static function getMaxConsecutivoNivel($cla_concepto, $aConnection)
	{
		$arrMax = array("consecutivo" => 1, "nivel" => 1);
		$aQuery = "SELECT MAX(consecutivo) as consecutivo, MAX(nivel) as nivel FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = {$cla_concepto}";
		$recordSet = $aConnection->Execute($aQuery);
		if($recordSet && !$recordSet->EOF)
		{
			$arrMax["consecutivo"] = $recordSet->fields['consecutivo'] + 1;
			$arrMax["nivel"] = $recordSet->fields['nivel'] + 1;
		}
		else
		{
			$ErrSQL = $aConnection->ErrorMsg();
			savelogfile($aQuery.' -> '.$ErrSQL);
		}
		
		return $arrMax;
	}
	
	public static function insertSI_DESCRIP_KEYS(&$metaconnection, $nom_tabla, $nom_fisicok, $nom_fisicok_join, $cla_concepto )
	{
		$sql =
		"select count(1) as nveces from si_descrip_keys where NOM_TABLA = '".$nom_tabla."' and CLA_CONCEPTO = {$cla_concepto}";
		
		$rst = $metaconnection->Execute($sql);
		
		if ($rst)
		{
			if ($rst->fields['nveces'] == 0)
			{
				$sql  = 
				"INSERT INTO si_descrip_keys
					(cla_concepto, nom_tabla, consecutivo, nom_fisicok, nom_fisicok_join
						) VALUES ( {$cla_concepto}, {$metaconnection->quote($nom_tabla)},
						1,
						{$metaconnection->quote($nom_fisicok)},
						{$metaconnection->quote($nom_fisicok_join)}
						)
				";
				if (!$metaconnection->Execute($sql))
				{
					$ErrSQL = $metaconnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}
			}
		}
	}
	
	public static function addModelIndicatorCalculated($Repository, $cla_concepto, $indicator_name, $formula_bd, $formato, $consecutivo=-1)
	{
		$strOriginalWD = getcwd();
		
		$anInstanceIndicator = BITAMIndicator::NewIndicator($Repository);
		$anInstanceIndicator->CubeID = $cla_concepto;
		$anInstanceIndicator->IndicatorName = $indicator_name;
		$anInstanceIndicator->formula_bd = $formula_bd;
		$anInstanceIndicator->formato = $formato;
		$anInstanceIndicator->is_custom = 1;
		$anInstanceIndicator->no_ejecutivo = 0;
		
		if ($consecutivo > 0)
			$anInstanceIndicator->dim_depend[$consecutivo] = $consecutivo;
		
		@$anInstanceIndicator->save();
		
		chdir($strOriginalWD);
		
		return $anInstanceIndicator->IndicatorID;
	}
	
	public static function deleteFromSI_INDICADOR(&$metaconnection, $cla_indicador)
	{
		$sql = "delete from SI_GPO_IND_DET where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_IND_LINEA where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_PROPxIND where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_IND_REPORTE where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_COMENTARIO where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_INDxLINEA where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_OPxIND where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_INDREL where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_REPORTExIND where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_IND_REPORTE6 where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_USR_CONFIG_IND where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_ROL_CONFIG_IND where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_INDICADOR where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
	}

}

class ARTUSModelGeoFence extends ARTUSModelFunctionBasics
{
	public $Repository;
	public $cla_concepto = -1;
	public $cla_descrip_user = -1;
	public $cla_descrip_user_centerlat = -1;
	public $cla_descrip_user_centerlng = -1;
	public $cla_descrip_date = -1;
	public $cla_descrip_date_centerlat = -1;
	public $cla_descrip_date_centerlng = -1;
	public $cla_descrip_hour = -1;
	public $cla_descrip_hour_lat = -1;
	public $cla_descrip_hour_lng = -1;
	//GCRUZ 2018-05-21. Agregar dimensiones:DeviceModel, DeviceModelVersion, DeviceManufacturer, BatteryLevel, PlugStatus (GeoFence Model Version 1.0001, antes 1.0000)
	public $cla_descript_deviceModel = -1;
	public $cla_descript_deviceModelVersion = -1;
	public $cla_descript_deviceManufacturer = -1;
	public $cla_descript_batteryLevel = -1;
	public $cla_descript_plugStatus = -1;
	
	public $cla_gpo_ind = -1;
	
	public $cla_indicator_visitasplaneadas = -1;
	public $cla_indicador_visitasreales = -1;
	public $cla_indicator_visitasdiferencia = -1;
	public $cla_indicator_visitasporcentaje = -1;
	
	public $cla_indicador_duracionplaneada = -1;
	public $cla_indicador_duracionreal = -1;
	public $cla_indicador_duraciondiferencia = -1;
	public $cla_indicador_duracionporcentaje = -1;
	
	public $cla_indicador_distanciadiferencia = -1;
	
	public $cla_indicador_tiempodetranslado = -1;

	function __destruct() {

	}
	
	static function NewInstance(&$Repository)
	{
		$aArtusModel = new ARTUSModelGeoFence();
		$aArtusModel->Repository = $Repository;
		$aArtusModel->error = BITAMAppError::NewInstance();
		$aArtusModel->warning = BITAMAppError::NewInstance();

		return $aArtusModel;
	}

	function getDimClaDescrip($dimensionName, $CREATE_BY = -1)
	{
		if ($CREATE_BY == -1)
			$CREATE_BY = CREATED_BY_EFORMSV5;

		$sql = 'select CLA_DESCRIP as cla_descrip from SI_DESCRIP_ENC where CREATED_BY = '.$CREATE_BY.' and NOM_LOGICO = '.$this->Repository->ADOConnection->Quote($dimensionName);
		$rst = $this->Repository->ADOConnection->Execute($sql);

		if ($rst && !$rst->EOF)
			return $rst->fields['cla_descrip'];
		else
		{
			$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
			
		return -1;
	}

	function addModelDimension($dimensionName, $dimensionID, $dimensionGroup, &$consecutivo_dim_survey=-1, $CREATE_BY = -1, $useKey = 0)
	{
		$strOriginalWD = getcwd();
		
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->cla_concepto);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		
		if ($CREATE_BY == -1)
			$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMSV5;
		else
			$anInstanceModelDim->Dimension->Created_By = $CREATE_BY;
			
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->DimensionGroup = $dimensionGroup;
		$anInstanceModelDim->Dimension->UseKey = $useKey;
		$anInstanceModelDim->Dimension->DimensionClaDescrip = $dimensionID;
		
		$objMMAnswer = @$anInstanceModelDim->save();
		
		if ($CREATE_BY != -1 && $anInstanceModelDim->Dimension->DimensionClaDescrip != -1)
		{
			$sql = "update SI_DESCRIP_ENC set CREATED_BY = {$CREATE_BY} where CLA_DESCRIP = {$anInstanceModelDim->Dimension->DimensionClaDescrip}";
			$this->Repository->ADOConnection->Execute($sql);
		}
		
		$consecutivo_dim_survey = $anInstanceModelDim->Dimension->DimensionID;
		
		chdir($strOriginalWD);
		
		$strMMError = identifyModelManagerError($objMMAnswer);
		if ($strMMError == '') {
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
			return $anInstanceModelDim->Dimension->DimensionClaDescrip;
		}
		else {
			$sErr = "\r\n(".__METHOD__.") ".translate("Error adding {$dimensionName} dimension").": ".$strMMError.". ";
			savelogfile($sErr);
		}
		
		return -1;
	}

	function addModelAttribute($cla_descrip_parent, $cla_descrip, $AttribName, $dimensionGroup, $IsGPS=false)
	{
		$cla_concepto = $this->cla_concepto;
		$strOriginalWD = getcwd();
		
		$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
		$anInstanceAttrib->Dimension->DimensionName = $AttribName;
		$anInstanceAttrib->Dimension->DimensionGroup = $dimensionGroup;
		$anInstanceAttrib->Dimension->ParentDimensionID = $cla_descrip_parent;
		$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
		$anInstanceAttrib->Dimension->DimensionID = -1;
		//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
		$objMMAnswer = @$anInstanceAttrib->save();
		
		chdir($strOriginalWD);
		
		$strMMError = identifyModelManagerError($objMMAnswer);
		if ($strMMError != '') {
			$sErr = "\r\n(".__METHOD__.") ".translate("Error creating {$AttribName} dimension").": ".$strMMError.". ";
			savelogfile($sErr);
		}
		
		if ($IsGPS)
		{
			$cla_descrip = $anInstanceAttrib->Dimension->DimensionClaDescrip;
			$table_name = $anInstanceAttrib->Dimension->TableName;
			$field_key = $anInstanceAttrib->Dimension->FieldKey;
			
			$this->insertAttribute($this->Repository->ADOConnection, $cla_descrip_parent, $cla_descrip, $AttribName, $table_name, $field_key);
		}
		
		return $anInstanceAttrib->Dimension->DimensionClaDescrip;
	}

	function addBaseIndicator($cla_concepto, $nom_indicador, $nom_tabla, $nom_fisico, &$cla_indicador=-1, $consecutivo_dim=-1)
	{
		// Consideramos el indicador de tipo score
		$formato = '#,##0';
		$formula_bd = 'SUM(t1.'.$nom_fisico.')';
	
		$sql = 
		"select cla_indicador as consecutivo from SI_INDICADOR where FORMULA_BD = '{$formula_bd}' and CLA_CONCEPTO = {$cla_concepto}";
		$ExistIndicator = $this->existRow($this->Repository->ADOConnection, $sql);

		if (!$ExistIndicator)
		{
			$dataconnection = $this->Repository->DataADOConnection;
			
			$sql = 
			"alter table {$nom_tabla} add {$nom_fisico} double null";
			
			if (!$dataconnection->Execute($sql))
			{
				$ErrSQL = $dataconnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}
			
			$cla_indicador = $this->addModelIndicatorCalculated($this->Repository, $cla_concepto, $nom_indicador, $formula_bd, $formato, $consecutivo_dim);
		}

	}

	public function getSQLSchedulerFactTable($FactTable, $cla_descrip_user, $cla_descrip_puntodevisita, $cla_descrip_agenda)
	{
		$subrrogatekeyUser = "RIDIM_{$cla_descrip_user}KEY";
		$subrrogatekeyCheckPoint = "RIDIM_{$cla_descrip_puntodevisita}KEY";
		$subrrogatekeyScheduler = "RIDIM_{$cla_descrip_agenda}KEY";
		
		$userDSC = "DSC_{$cla_descrip_user}";
		$checkpointDSC = "DSC_{$cla_descrip_puntodevisita}";
		$schedulerDSC = "DSC_{$cla_descrip_agenda}";
		
		$tableUser = "RIDIM_{$cla_descrip_user}";
		$tableCheckPoint = "RIDIM_{$cla_descrip_puntodevisita}";
		$tableScheduler = "RIDIM_{$cla_descrip_agenda}";
		
		$sql =
		"
		
		/*== Sincronize FactTable ==*/
		
		delete from {$FactTable} where DateKey between DATE_FORMAT(vDateIni, '%Y%m%d') and DATE_FORMAT(vDateFin, '%Y%m%d');
		
		INSERT INTO {$FactTable} ({$subrrogatekeyUser}, {$subrrogatekeyCheckPoint}, {$subrrogatekeyScheduler}, DateKey, REALVISIT, PLANED_DURATION, REAL_DURATION, LOCATION_DIFERENCE, TIME_TRANSLADE)
		SELECT COALESCE(t2.{$subrrogatekeyUser}, 1), COALESCE(t3.{$subrrogatekeyCheckPoint}, 1), COALESCE(t4.{$subrrogatekeyScheduler}, 1), DATE_FORMAT(t1.FechaDeCaptura, '%Y%m%d'), 
			t1.VisitasReales, t1.DuracionPlaneada, t1.DuracionReal, t1.DistanciaVariacion, t1.TiempoDeTraslado
		FROM TempFactScheduler AS t1 
			LEFT JOIN $tableUser AS t2 ON (t2.{$userDSC} = t1.Usuario)
			LEFT JOIN $tableCheckPoint AS t3 ON (t3.{$checkpointDSC} = t1.PuntoDeVisita)
			LEFT JOIN $tableScheduler AS t4 ON (t4.{$schedulerDSC} = t1.Agenda)
		where t1.FechaDeCaptura between vDateIni and vDateFin;
		
		";
		
		return $sql;
	}

	public function getSQLSchedulerGlobalDimension($cla_descrip, $campoTMP)
	{
		$sql = '';
		if ($cla_descrip > 0)
		{
			$tabla = "RIDIM_{$cla_descrip}";
			$campo = "DSC_{$cla_descrip}";
		
			$sql =
			"
			/*== Sincronize Catalog Dim -> {$campoTMP} */
			
			CREATE TEMPORARY TABLE IF NOT EXISTS TEMP1{$tabla} (
                 {$campo} VARCHAR(255)  NOT NULL
    		)ENGINE=MyISAM;
    		
			delete from TEMP1{$tabla};
    		
 			INSERT INTO TEMP1{$tabla} ({$campo})
    		SELECT distinct COALESCE(src.{$campoTMP},'ND')
    		FROM  TempFactScheduler src  
    		WHERE src.FechaDeCaptura between vDateIni and vDateFin;
    		
			INSERT INTO {$tabla} ({$campo})
			SELECT COALESCE(src.{$campo}, 'ND')
    		FROM TEMP1{$tabla} src   LEFT JOIN {$tabla} dim  ON src.{$campo} =  dim.{$campo}
    		WHERE	 dim.{$campo} IS NULL
    		GROUP BY src.{$campo};
    		
    		DROP TABLE TEMP1{$tabla};

    		";
    	}
		return $sql;
	}
	
	public function createSchedulerProc()
	{
		if ($this->cla_concepto > 0)
		{
			$sql = "drop procedure IF EXISTS sp_load_scheduler";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$sql =
			"CREATE PROCEDURE sp_load_scheduler (IN vDateIni datetime, IN vDateFin datetime)
			BEGIN
			";
			
			$sql .= $this->getSQLSchedulerGlobalDimension($this->cla_descrip_user, 'Usuario');
			$sql .= $this->getSQLSchedulerGlobalDimension($this->cla_descrip_puntodevisita, 'PuntoDeVisita');
			$sql .= $this->getSQLSchedulerGlobalDimension($this->cla_descrip_agenda, 'Agenda');
			
			$FactTable = "RIFACT_{$this->cla_concepto}";
			
			$sql .= $this->getSQLSchedulerFactTable($FactTable, $this->cla_descrip_user, $this->cla_descrip_puntodevisita, $this->cla_descrip_agenda);
			
			$sql .= "
			END
			";

			if (!$this->Repository->DataADOConnection->Execute($sql))
			{
				$serr = $this->Repository->DataADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$serr);
				//print '<br>'.$sql.' -> '.$serr;
				
			}
			
		}
	}
	
	public function runSchedulerProc($fecha_ini, $fecha_fin, &$error)
	{
		$sql = "call sp_load_scheduler('{$fecha_ini}', '{$fecha_fin}')";
		
		if (!$this->Repository->DataADOConnection->Execute($sql))
		{
			$serr = $this->Repository->DataADOConnection->ErrorMsg();
			$error = $sql.' -> '.$serr;
			savelogfile($sql.' -> '.$serr);
			//print '<br>'.$sql.' -> '.$serr;
		}
	}
	
	public function createIfNotExistsGeoFenceModel($bExists, $intModelID = -1, $modelVersion = 1.0)
	{
		// @EA 2016-05-26 Checa si existe el modelo de agenda en la metadata y si no existe lo crea
		global $generateXLS, $generatePHP, $PHPSincronize;
		$generateXLS = false;
		$generatePHP = false;
		$PHPSincronize = false;

		$metaconnection = $this->Repository->ADOConnection;
		
		if (!$bExists && $intModelID == -1)
		{
			$sql = "create index idxRI_DEFINITION on SI_CONCEPTO (RI_DEFINITION)";
			$metaconnection->Execute($sql);

			$strOriginalWD = getcwd();
			$anInstanceModel = BITAMModel::NewModel($this->Repository);
			$anInstanceModel->ModelName = 'GeoFence Model';
			$anInstanceModel->readPeriods();

			@$anInstanceModel->ri_definition = CREATED_BY_SCHEDULER;
			@$anInstanceModel->use_cache = false;

			$objMMAnswer = @$anInstanceModel->save();
			chdir($strOriginalWD);
			
			$fact_table = '';
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding survey's model").": ".$strMMError.". ";
				savelogfile($gblModMngrErrorMessage);
			}
			else				
			{
				$this->cla_concepto = $anInstanceModel->ModelID;
				$this->cla_gpo_ind = (int) $anInstanceModel->model_options;
				$fact_table = $anInstanceModel->nom_tabla;
			}

			$dimensionGroup = '';
			//=================================================================
			$dimensionName = 'User';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			$this->cla_descrip_user = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);
			
			$dimensionName = 'UserCenterLatitude';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			//$this->cla_descrip_centerlat = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup, $dimensionID);
			$this->cla_descrip_user_centerlat = $this->addModelAttribute($this->cla_descrip_user, -1, $dimensionName, $dimensionGroup, true);

			$dimensionName = 'UserCenterLongitude';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			//$this->cla_descrip_centerlat = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup, $dimensionID);
			$this->cla_descrip_user_centerlng = $this->addModelAttribute($this->cla_descrip_user, -1, $dimensionName, $dimensionGroup, true);

			$dimensionName = 'Date';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			$consecutivo = -1;
			$this->cla_descrip_date = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup, $consecutivo, -1, 1);
			
			$dimensionName = 'DateCenterLatitude';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			//$this->cla_descrip_centerlat = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup, $dimensionID);
			$this->cla_descrip_date_centerlat = $this->addModelAttribute($this->cla_descrip_date, -1, $dimensionName, $dimensionGroup, true);

			$dimensionName = 'DateCenterLongitude';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			//$this->cla_descrip_centerlat = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup, $dimensionID);
			$this->cla_descrip_date_centerlng = $this->addModelAttribute($this->cla_descrip_date, -1, $dimensionName, $dimensionGroup, true);

			$dimensionName = 'Hour';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			$this->cla_descrip_hour = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup, $consecutivo, -1, 1);
			
			$dimensionName = 'HourLatitude';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			//$this->cla_descrip_centerlat = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup, $dimensionID);
			$this->cla_descrip_hour_lat = $this->addModelAttribute($this->cla_descrip_hour, -1, $dimensionName, $dimensionGroup, true);

			$dimensionName = 'HourLongitude';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			//$this->cla_descrip_centerlat = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup, $dimensionID);
			$this->cla_descrip_hour_lng = $this->addModelAttribute($this->cla_descrip_hour, -1, $dimensionName, $dimensionGroup, true);

			//GCRUZ 2018-05-21. Agregar dimensiones:DeviceModel, DeviceModelVersion, DeviceManufacturer, BatteryLevel, PlugStatus (GeoFence Model Version 1.0001, antes 1.0000)
			$dimensionName = 'DeviceModel';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			$this->cla_descrip_deviceModel = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);

			$dimensionName = 'DeviceModelVersion';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			$this->cla_descrip_deviceModelVersion = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);

			$dimensionName = 'DeviceManufacturer';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			$this->cla_descrip_deviceManufacturer = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);

			$dimensionName = 'BatteryLevel';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			$this->cla_descrip_batteryLevel = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);

			$dimensionName = 'PlugStatus';
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			$this->cla_descrip_plugStatus = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);
			//=================================================================
			$indicatorName = 'Latitude';
			$fieldName = "LATITUDE";
			$this->cla_indicador_latitude = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_latitude);

			$indicatorName = 'Longitude';
			$fieldName = "LONGITUDE";
			$this->cla_indicador_longitude = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_longitude);

			$indicatorName = 'CenterLatitude';
			$fieldName = "CENTERLATITUDE";
			$this->cla_indicador_centerlatitude = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_centerlatitude);

			$indicatorName = 'CenterLongitude';
			$fieldName = "CENTERLONGITUDE";
			$this->cla_indicador_centerlongitude = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_centerlongitude);

			$indicatorName = 'GeoFenceRadius';
			$fieldName = "GEOFENCERADIUS";
			$this->cla_indicador_geofenceradius = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_geofenceradius);

			$indicatorName = 'TransferTime';
			$fieldName = "TRANSFERTIME";
			$this->cla_indicador_transfertime = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_transfertime);

			$indicatorName = 'TransferDistance';
			$fieldName = "TRANSFERDISTANCE";
			$this->cla_indicador_transferdistance = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_transferdistance);

			$indicatorName = 'GeoFenceDistance';
			$fieldName = "GEOFENCEDISTANCE";
			$this->cla_indicador_clatclngdistance = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_clatclngdistance);

			$indicatorName = 'GeoFenceCount';
			$formula_bd = "COUNT(1)";
			$formato = '#,##0';
			$this->cla_indicator_geofencecount = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);
			//=================================================================
			/*
			$indicatorName = 'Planned Visit';
			$formula_bd = "COUNT(1)";
			$formato = '#,##0';
			$this->cla_indicator_visitasplaneadas = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);
			
			$indicatorName = 'Real Visit';
			$fieldName = "REALVISIT";
			$this->cla_indicador_visitasreales = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_visitasreales);
			
			$indicatorName = 'Visit Difference';
			$formula_bd = "SUM(t1.REALVISIT)-COUNT(1)";
			$formato = '#,##0';
			$this->cla_indicator_visitasdiferencia = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);

			$indicatorName = 'Visit Percentage';
			$formula_bd = "(SUM(t1.REALVISIT)-COUNT(1)) / SUM(t1.REALVISIT)";
			$formato = '%0';
			$this->cla_indicator_visitasporcentaje = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);
			//=================================================================
			$indicatorName = 'Planned Duration';
			$fieldName = "PLANED_DURATION";
			$this->cla_indicador_duracionplaneada = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_duracionplaneada);
			
			$indicatorName = 'Real Duration';
			$fieldName = "REAL_DURATION";
			$this->cla_indicador_duracionreal = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_duracionreal);
			
			$indicatorName = 'Duration Difference';
			$formula_bd = "SUM(t1.REAL_DURATION)-SUM(t1.PLANED_DURATION)";
			$formato = '#,##0';
			$this->cla_indicator_duraciondiferencia = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);

			$indicatorName = 'Duration Percentage';
			$formula_bd = "(SUM(t1.REAL_DURATION)-SUM(t1.PLANED_DURATION)) / SUM(t1.REAL_DURATION)";
			$formato = '%0';
			$this->cla_indicator_duracionporcentaje = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);
			//=================================================================
			$indicatorName = 'Location Diference';
			$fieldName = "LOCATION_DIFERENCE";
			$this->cla_indicador_distanciadiferencia = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_distanciadiferencia);
			//=================================================================
			$indicatorName = 'Time Traslade';
			$fieldName = "TIME_TRANSLADE";
			$this->cla_indicador_tiempodetranslado = -1;
			$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_tiempodetranslado);
			*/
			//=================================================================
		}
		else
		{
			$this->cla_concepto = $intModelID;
			
			//GCRUZ 2018-05-21. Agregar dimensiones:DeviceModel, DeviceModelVersion, DeviceManufacturer, BatteryLevel, PlugStatus (GeoFence Model Version 1.0001, antes 1.0000)
			if ($modelVersion < esvGeoFenceModel_1_0001)
			{
				$dimensionGroup = '';
				//=================================================================
				$dimensionName = 'DeviceModel';
				$dimensionID = $this->getDimClaDescrip($dimensionName);
				$this->cla_descrip_deviceModel = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);
			
				$dimensionName = 'DeviceModelVersion';
				$dimensionID = $this->getDimClaDescrip($dimensionName);
				$this->cla_descrip_deviceModelVersion = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);

				$dimensionName = 'DeviceManufacturer';
				$dimensionID = $this->getDimClaDescrip($dimensionName);
				$this->cla_descrip_deviceManufacturer = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);

				$dimensionName = 'BatteryLevel';
				$dimensionID = $this->getDimClaDescrip($dimensionName);
				$this->cla_descrip_batteryLevel = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);

				$dimensionName = 'PlugStatus';
				$dimensionID = $this->getDimClaDescrip($dimensionName);
				$this->cla_descrip_plugStatus = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);
				
			}
			
		}
		
		if ($this->cla_concepto > 0)
		{
			//$this->createSchedulerProc();
			
			$generateXLS = false;
			$generatePHP = true;
			$PHPSincronize = true;
			@ExportToPHP($this->Repository, $this->cla_concepto);
		}
	}
}

?>