var bitamTrees = new Array;

function BitamTN(aContentsID,aContents, aState, afatherNodeID, status,childrenURL)
{	
	this.id = null;
	this.contents = aContents;
	this.contentsID = aContentsID;
	this.state = aState;
	this.childrenURL = childrenURL;
	this.level = 0;
	this.children = new Array();
	this.childrenCreated = false;
	this.tree = null;
	this.fatherNodeID = afatherNodeID;
	this.childrenLength = 0;
	this.status = parseInt(status);
	
	this.notChildren = new Array();
	this.notChildrenCreated = false;
	this.notChildrenURL = "";
}

function BitamTNotChild(aContentsID,aContents, afatherNodeID, status)
{	
	this.id = null;
	this.contents = aContents;
	this.contentsID = aContentsID;
	this.level = 1;
	this.tree = null;
	this.fatherNodeID = afatherNodeID;
	this.notChildrenLength = 0;
	this.status = parseInt(status);
}

function BitamT()
{
	this.id = bitamTrees.length;

	this.innerText = "";

	this.nodes = new Array();
	this.roots = new Array();
	
	this.notChildNodes = new Array();
	
	this.expandedBranchImage = "images/minus.gif";
	this.collapsedBranchImage = "images/plus.gif";
	this.leafImage = "images/bullet.gif";
	
	this.readOnly = false;

	bitamTrees[bitamTrees.length] = this;
}

function createNotChildNodesFrom(aNode)
{
	var aRow = getElementWithID('row' + aNode.id);
	var rowIndex = aRow.rowIndex;

	var aTable = aRow.parentNode.parentNode;
	var newRow, newCell, aCode;

	var i = aNode.notChildren.length - 1;
	
	var strNodeUser = "";

	while (i >= 0)
	{
		var subNode = aNode.notChildren[i];

		bitamRegisterNotChildNodeOnTree(subNode, aNode.tree, aNode.level + 1);
		
		if(strNodeUser!="")
		{
			strNodeUser	= strNodeUser + "_EKT_";
		}
		
		strNodeUser = strNodeUser + subNode.contentsID + "|" + subNode.id;

		newRow = aTable.insertRow(rowIndex + 1);
		newRow.id = 'rowNotChild' + subNode.id;
		 
		newCell = newRow.insertCell(0);
		aCode = bitamNotChildNodeString(subNode);
		newCell.innerHTML = aCode;
		newCell.id='cellNodeNotChild' + subNode.id; 

		i--;
	}
	
	var IsChild = false;
	setSessionUsersNodes(strNodeUser, IsChild);
}

function bitamNotChildNodeString(aNode)
{	
	var str = "";
	var menuImage;
	menuImage=getMenuImage(aNode);
	titleImage=getTitleImage(aNode);
	
	str += '<table id=tableNotChild' + aNode.id + '  border=0 cellspacing=0 cellpadding=0 class=treeNodeStyle>';
	
	str += '<tbody id=tbodyNotChild' + aNode.id + ' >'
	str += '<tr id=tableRowNotChild' + aNode.id + '><td id=spaceNotChild' + aNode.id + ' noWrap width=' + (25 * aNode.level) + 'px>&nbsp;</td>';
	
	var anImage = aNode.tree.leafImage;
	str += '<td noWrap id=cellImgNotChild' + aNode.id +'><img id=imgNotChild' + aNode.id + ' border=0 src=' + anImage + '></td>';
	str += '<td noWrap id=cellImgMenuNotChild' + aNode.id + ' ><img id=imgMenuNotChild' + aNode.id + ' border=0 src=' + menuImage + ' title='+titleImage+' style="cursor:pointer"></td>';
	
	
	str += '<td  class="NotChildUserTree" id=contentsNotChild' + aNode.id + ' noWrap style="cursor:pointer" onmouseover="this.style.backgroundColor=\'#FFFFD8\'; " onmouseout="this.style.backgroundColor=\'transparent\'"	onclick="showBudgetNotChild(' + aNode.id + ',' + aNode.tree.id + ',' + aNode.fatherNodeID + ')">' + aNode.contents + '</td>';
	
	str += '</tr></tbody></table>';

	return(str);
}

function bitamRegisterNotChildNodeOnTree(aNode, aTree, aLevel)
{
	aNode.id = aTree.notChildNodes.length;
	aNode.level = aLevel;
	aNode.tree = aTree;
	aTree.notChildNodes[aNode.id] = aNode;
	
	if(aNode.fatherNodeID != -1)
	{	aFatherNodeID = aNode.fatherNodeID;
		aTree.notChildNodes[aFatherNodeID].notChildrenLength++;
	}
}

function addTemporalNodesFromServer(aNode,strURL)
{
	aNode.notChildrenURL = strURL;
	
	if (!aNode.notChildrenCreated)
	{
		if (aNode.notChildrenURL != null && aNode.notChildrenURL != "" )
			fillNotChildrenFromServer(aNode);

		createNotChildNodesFrom(aNode);
		aNode.notChildrenCreated = true;
	}
}

function fillNotChildrenFromServer(aNode)
{
	var xmlObj = getXMLObject();
	
	//Variable Global que se define en Tree.php
	nodeLabel;
	
	if (xmlObj == null)
	{
		alert("XML Object not supported");
		return(new Array);
	}

 
	aux=aNode.notChildrenURL+"&fatherID="+aNode.id;
 	xmlObj.open("POST", aNode.notChildrenURL+"&fatherNodeID="+aNode.id+"&nodeLabel="+nodeLabel, false);

 	xmlObj.send(null);
 	
	aNode.notChildren = convertStringToBitamTNotChildForNode(Trim(xmlObj.responseText));
}

function convertStringToBitamTNotChildForNode(aString)
{	
	var nodes = new Array();

	if (aString.length == 0)
		return(nodes);

	var realString = unescape(aString);

	var sep = String.fromCharCode(30);

	var elements = realString.split(sep);

	sep = String.fromCharCode(31);

	for (var i = 0; i < elements.length; i++)
	{
		var parts = elements[i].split(sep);

		nodes[nodes.length] = new BitamTNotChild(parts[0],parts[1], parts[2], parts[3]);
	}

	return(nodes);
}

function getElementWithID(anID)
{
	return (document.getElementById(anID));
}

function buildTree(aTree)
{
	//debugger;
	aTree.innerText = '<table border=0 cellspacing=0 cellpadding=0>';

	var size = aTree.roots.length;
	for (var i = 0; i < size; i++)
		buildTreeNodeOn(aTree.roots[i], aTree, 0);

	aTree.innerText += '</table>';
}

function getMenuImage(aNode)
{
	//debugger;
	var status=aNode.status;
	var menuImage;
	switch (status)
	{
		case -1:
			menuImage="images/waiting.gif";
			break;
   		case 0:
			menuImage="images/DefinitionOnTime.png";
			break;
   		case 1:
			menuImage="images/DefinitionOnWarning.png";
			break;
   		case 2:
			menuImage="images/DefinitionLate.png";
			break;
   		case 3:
			menuImage="images/AuthorizationOnTime.png";
			break;
		case 4:
			menuImage="images/AuthorizationOnWarning.png";
			break;
		case 5:
			menuImage="images/AuthorizationLate.png";
			break;
		case 6:
			menuImage="images/RedefinitionOnTime.png";
			break;
		case 7:
			menuImage="images/RedefinitionOnWarning.png";
			break;
		case 8:
			menuImage="images/RedefinitionLate.png";
			break;
		case 9:
			menuImage="images/AuthorizedTemp.gif";
			break;
		case 10:
			menuImage="images/AuthorizedOnTime.png";
			break;
		/*	
		case 10:
			menuImage="images/AuthorizedOnWarning.png";
			break;
		case 11:
			menuImage="images/AuthorizedLate.png";
			break;
		*/
   		default:
   			menuImage="";
			break;
   		
   }
   return(menuImage);
}

function getTitleImage(aNode)
{
	//debugger;
	var status=aNode.status;
	var titleImage;
	switch (status)
	{
		case -1:
			titleImage="Waiting";
			break;
		case 0:
			titleImage="Definition-OnTime";
			break;
   		case 1:
			titleImage="Definition-OnWarning";
			break;
   		case 2:
			titleImage="Definition-Late";
			break;
   		case 3:
			titleImage="Authorization-OnTime";
			break;
   		case 4:
			titleImage="Authorization-OnWarning";
			break;
		case 5:
			titleImage="Authorization-Late";
			break;	
		case 6:
			titleImage="Redefinition-OnTime";
			break;
		case 7:
			titleImage="Redefinition-OnWarning";
			break;
		case 8:
			titleImage="Redefinition-Late";
			break;
		case 9:
			titleImage="Temporally Authorized";
			break;
		case 10:
			titleImage="Authorized-OnTime";
			break;
		/*	
		case 10:
			titleImage="Authorized-OnWarning";
			break;
		case 11:
			titleImage="Authorized-Late";
			break;
		*/
		default:
   			titleImage="";
			break;
   }
   return(titleImage);
}

function bitamNodeString(aNode)
{	
	var str = "";
	var menuImage;
	menuImage=getMenuImage(aNode);
	titleImage=getTitleImage(aNode);
	
	readOnlyTree = aNode.tree.readOnly;
	
	if(readOnlyTree==false)
	{
		strMultipleDrag = 'ondragenter="dragEnter();" ondragover="dragOver();" ondrop="dropped();"';
		strOnlyDrag = 'ondragstart="startedDragging();"';
		strShowMenu = 'onclick="showHierarchyMenu(' + aNode.id + ',' + aNode.tree.id + ')" ';
		
	}
	else
	{
		strMultipleDrag = '';
		strOnlyDrag = '';
		strShowMenu = '';
	}
	
	//titleImage=menuImageArray[1];
	
	str += '<table id=table' + aNode.id + '  border=0 cellspacing=0 cellpadding=0 class=treeNodeStyle ' + strMultipleDrag + '>';
	
	str += '<tbody id=tbody' + aNode.id + ' >'
	str += '<tr id=tableRow' + aNode.id + '><td id=space' + aNode.id + ' noWrap width=' + (25 * aNode.level) + 'px>&nbsp;</td>';

	if (aNode.childrenLength == 0 && (aNode.childrenURL == null ))
	{
		var anImage = aNode.tree.leafImage;
		str += '<td noWrap id=cellImg' + aNode.id +'><img id=img' + aNode.id + ' border=0 src=' + anImage + '></td>';
		str += '<td noWrap id=cellImgMenu' + aNode.id + ' ><img id=imgMenu' + aNode.id + ' border=0 src=' + menuImage + ' title='+titleImage+' style="cursor:pointer" ' + strShowMenu + strOnlyDrag + '></td>';
	}
	else
	{
		if (aNode.state)
		{
			var anImage = aNode.tree.expandedBranchImage;
		}
		else
		{
			var anImage = aNode.tree.collapsedBranchImage;
		}
		
		str += '<td noWrap id=cellImg' + aNode.id +'><a href=javascript:bitamNodeIDClicked(' + aNode.id + ',' + aNode.tree.id + ')><img id=img' + aNode.id + ' border=0 src=' + anImage + '></a></td>';
			
		str += '<td noWrap id=cellImgMenu' + aNode.id + ' ><img id=imgMenu' + aNode.id + ' border=0 src=' + menuImage +' title='+titleImage+ ' style="cursor:pointer" ' + strShowMenu + strOnlyDrag + '></td>';
	}
	
	str += '<td  class="UserTree" id=contents' + aNode.id + ' noWrap style="cursor:pointer" onmouseover="this.style.backgroundColor=\'#E6F2FF\'; " onmouseout="this.style.backgroundColor=\'transparent\'"	onclick="showBudget(' + aNode.id + ',' + aNode.tree.id + ')">' + aNode.contents + '</td>';
	
	str += '</tr></tbody></table>';

	return(str);
}

function bitamRegisterNodeOnTree(aNode, aTree, aLevel)
{
	//debugger;
	aNode.id = aTree.nodes.length;
	aNode.level = aLevel;
	aNode.tree = aTree;
	aTree.nodes[aNode.id] = aNode;
	
	if(aNode.fatherNodeID != -1)
	{	aFatherNodeID = aNode.fatherNodeID;
		aTree.nodes[aFatherNodeID].childrenLength++;
	}
}

function buildTreeNodeOn(aNode, aTree, aLevel)
{
	//debugger;
	bitamRegisterNodeOnTree(aNode, aTree, aLevel);
	var strNodeUser = aNode.contentsID + "|" + aNode.id;
	var IsChild = true;
	setSessionUsersNodes(strNodeUser, IsChild);

	aTree.innerText += '<tr id=row' + aNode.id + ' ><td id=cellNode' + aNode.id + '>';

	aTree.innerText += bitamNodeString(aNode);

	aTree.innerText += '</td></tr>';

	if (aNode.state)
	{
		aNode.childrenCreated = true;

		var size = aNode.childrenLength;
		for (var i = 0; i < size; i++)
			buildTreeNodeOn(aNode.children[i], aTree, aLevel + 1);
	}
}

function bitamNodeIDClicked(aNodeID, aTreeID)
{//debugger
	var aTree = bitamTrees[aTreeID];
	var aNode = aTree.nodes[aNodeID];

	if (aNode.state)
	{	
		bitamCollapseNode(aNode);
	}
	else
	{	
		bitamExpandNode(aNode);
	}
}

function createChildNodesFrom(aNode)
{
	var aRow = getElementWithID('row' + aNode.id);
	var rowIndex = aRow.rowIndex;

	var aTable = aRow.parentNode.parentNode;
	var newRow, newCell, aCode;

	var i = aNode.children.length - 1;
	
	var strNodeUser = "";

	while (i >= 0)
	{
		var subNode = aNode.children[i];

		bitamRegisterNodeOnTree(subNode, aNode.tree, aNode.level + 1);
		
		if(strNodeUser!="")
		{
			strNodeUser	= strNodeUser + "_EKT_";
		}
		
		strNodeUser = strNodeUser + subNode.contentsID + "|" + subNode.id;
		
		newRow = aTable.insertRow(rowIndex + 1);
		newRow.id = 'row' + subNode.id;
		 
		//newRow.ondragenter="dragEnter();"
		//newRow.ondragover="dragOver();"
		
		newCell = newRow.insertCell(0);
		aCode = bitamNodeString(subNode);
		newCell.innerHTML = aCode;
		newCell.id='cellNode' + subNode.id; 

		i--;
	}
	
	var IsChild = true;
	setSessionUsersNodes(strNodeUser, IsChild);
}

function bitamExpandNode(aNode)
{	
	var anObject;
	
	stateAux = aNode.state;
	
	aNode.state = true;
	anObject = getElementWithID('img' + aNode.id);
	imageAux=anObject.src;
	anObject.src = 'images/minus.gif';

	if (!aNode.childrenCreated)
	{
		if (aNode.childrenURL != null )
			fillChildrenFromServer(aNode);

		createChildNodesFrom(aNode);
		aNode.childrenCreated = true;
	}
	else
	{
		var size = aNode.children.length;
		for (var i = 0; i < size; i++)
		{
			var childNode = aNode.children[i];//
			
			if (childNode!=null)
			{
				anObject = getElementWithID('row' + childNode.id);//
				anObject.style.display = '';
				//
				if (childNode.childrenLength > 0 && childNode.childrenCreated&& childNode.state==true)
				{	
					bitamExpandNode(childNode);
					
				}
				//
			}
		}
	}
}

function bitamCollapseNode(aNode)
{
	stateAux = aNode.state;//
	
	aNode.state = false;
	
	anObject = getElementWithID('img' + aNode.id);
	imageAux=anObject.src;
	anObject.src = 'images/plus.gif';			
	
	var size = aNode.children.length;
	for (var i = 0; i < size; i++)
	{
		var childNode = aNode.children[i];

		if (childNode!=null)
		{
			anObject = getElementWithID('row' + childNode.id);
			anObject.style.display = 'none';
		

			if (childNode.childrenLength > 0 && childNode.childrenCreated & childNode.state==true)
			{	
				anObjectChild = getElementWithID('img' + childNode.id);
				imageAux=anObjectChild.src;
				bitamCollapseNode(childNode);
				childNode.state=true;
				anObjectChild.src = imageAux;			
			}
		}
	}
}

function convertStringToBitamTNForNode(aString)
{	
	var nodes = new Array();

	if (aString.length == 0)
		return(nodes);

	var realString = unescape(aString);

	var sep = String.fromCharCode(30);

	var elements = realString.split(sep);

	sep = String.fromCharCode(31);

	for (var i = 0; i < elements.length; i++)
	{
		var parts = elements[i].split(sep);

		nodes[nodes.length] = new BitamTN(parts[0],parts[1], parts[2] == 'true', parts[3], parts[4],parts[5]);
	}

	return(nodes);
}

function fillChildrenFromServer(aNode)
{
	// debugger;
	
	//Variable Global que se define en Tree.php
	nodeLabel;
	
	var xmlObj = getXMLObject();

	if (xmlObj == null)
	{
		alert("XML Object not supported");
		return(new Array);
	}

	//xmlObj.onreadystatechange = function() 
	//	{
  	//		if (xmlObj.readyState == 4) 
  	//		{
  	//			aNode.children = convertStringToBitamTNForNode(xmlObj.responseText);
   	//		}
  	//	}
 
	aux=aNode.childrenURL+"&fatherID="+aNode.id;
 	xmlObj.open("POST", aNode.childrenURL+"&fatherNodeID="+aNode.id+"&nodeLabel="+nodeLabel, false);

 	xmlObj.send(null);

 	aNode.children = convertStringToBitamTNForNode(Trim(xmlObj.responseText));
}

function handleDragStart()
{
	var dragData = window.event.dataTransfer;

	dragData.setData('text', window.event.srcElement.innerText);
	dragData.effectAllowed = 'copy';
	dragData.dropEffect = 'copy';
}

function handleDragEnd()
{
	window.event.dataTransfer.clearData();
}

function addNode(aNode, aFather)
{
	var aRow = getElementWithID('row' + aFather.id);
	var rowIndex = aRow.rowIndex;

	var aTable = aRow.parentNode.parentNode;
	var newRow, newCell, aCode;

	aFather.children[aFather.children.length]=aNode;
		
	bitamRegisterNodeOnTree(aNode, aFather.tree, aFather.level + 1);
	
	var strNodeUser = aNode.contentsID + "|" + aNode.id;
	var IsChild = true;
	setSessionUsersNodes(strNodeUser, IsChild);

	newRow = aTable.insertRow(rowIndex + 1);
	
	newRow.id = 'row' + aNode.id;
	
	newCell = newRow.insertCell(0);
	aCode = bitamNodeString(aNode);
	newCell.innerHTML = aCode;
	newCell.id='cellNode' + aNode.id; 
}

function setSessionUsersNodes(StrUsersNodes, IsChild)
{	
	var xmlObj = getXMLObject();

	if (xmlObj == null)
	{
		alert("XML Object not supported");
		return(new Array);
	}
	
	if(IsChild==true)
	{
	 	xmlObj.open("POST", "setUsersNodes.php?StrUsersNodes="+StrUsersNodes, false);
	
	 	xmlObj.send(null);
	}
	else
	{
	 	xmlObj.open("POST", "setUsersNodes.php?StrTempUsersNodes="+StrUsersNodes, false);
	
	 	xmlObj.send(null);
	}
}