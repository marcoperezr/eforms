<?php
//@JAPR 2015-08-29: Modificado para que se pueda integrar al control de versiones y copiar entre servicios
/* Ahora este archivo contendrá únicamente las definiciones de constantes, de tal manera que se pueda copiar en cada actualización de servicio sin afectar a las configuraciones
específicas, las cuales se encontrarán en el archivo configSettings.php, sin embargo las definiciones de variables con el default original >>>> TAMBIÉN tienen que estar aquí <<<<, ya
que de lo contrario no se respaldarán en el control de versiones, simplemente toda variable se redefinirá dentro de configSettings.php
*/

//@JAPR 2016-06-14: Modificado el valor default de preguntas alfanuméricas (#Q0GXKE)
//Si existe, se agregará el archivo de configuración de constantes por servicio, de tal manera que si no existe (como será en todos los servicios previamente existentes) en este archivo 
//se puedan definir con el valor default
$strRequirePath = '';
$strRequirePath = getcwd()."/configSettingsConstants.php";
$strRequirePath = str_replace('/', '\\', $strRequirePath);
$strRequirePath = str_replace('\\\\', '\\', $strRequirePath);
if (file_exists($strRequirePath)) {
	require_once("configSettingsConstants.php");
}

//Tipos de modo de trabajo
define('wrkmMDAdmin', 0);			// Modo de trabajo de Normal, el MDAdmnistrator se conecta a los repositorios mediante el archivo Projects.ini
define('wrkmBITAM', 1);				// Modo de trabajo de BITAM, el MDAdministrator recibe le nombre de proyecto como parámetro y lo busca en los archivos de Artus Web (se asume que está siempre dentro de este último)

define('ARTUS', 0);
define('OLAP', 1);
define('ESSBASE', 2);
define('ROLAP', 3);

//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
//Constantes con los tipos de productos de KPIOnline (equivalente a SI_MD_VERSION y al nuevo campo de esta funcionalidad SI_USUARIO.KPI_DEFAULT)
define('prodDefault', 0);			//Selección automática basada en el Template de la cuenta (es el equivalente a no haber asignado nunca el valor)
define('prodARTUS', 1);				//Artus Web
define('prodEFORMS', 6);			//eForms Admin
define('prodEBAVEL', 7);			//eBavel Admin
//@JAPR

define('PRODUCT_VERSION', '6.00140');
define('CREATED_BY_RAPID_WEB', 6);
define('CREATED_BY_EFORMS', 8);
define('DEFAULT_MODEL_MANAGER_CONNECTION', -7);
//@JAPR 2011-06-29: Agregada la longitud de los campos Alfanumericos
//@JAPR 2015-05-13: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
//estaban limitadas a 255 caracteres (#BA7866)
//990 es una limitante técnica identificada para Model Manager mientras se corregía el ticket #S7OWW3, así que ese será el máximo posible a la
//fecha de esta implementación
//@JAPR 2016-06-14: Modificado el valor default de preguntas alfanuméricas (#Q0GXKE)
//Debido a un problema con el límite de bytes por tabla en MySQL (alrededor de 8000bytes) se reduce el tamaño de las preguntas alfanuméricas de 255 a 100
if (!defined('DEFAULT_ALPHA_LENGTH')) {
	define('DEFAULT_ALPHA_LENGTH', 100);	//255 (ahora esta configuración se definirá dentro de configSettingsConstants.php)
}
define('MAX_ALPHA_LENGTH', 990);	//255
define('DEFAULT_TEXT_LENGTH', 990);		//Agregada exclusivamente para preguntas tipo TEXT mientras existía la limitante descrita arriba en Model Manager
//@JAPR 2016-06-15: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
//Tamaño default para preguntas Simple choice y múltiple choice (independientemente de si es o no de catálogo)
if (!defined('DEFAULT_SCHMCH_LENGTH')) {
	define('DEFAULT_SCHMCH_LENGTH', 255);
}
//Tamaño default para preguntas generales que requieren un campo tipo varchar con contenido variable (originalmente usado para numéricas, calculadas y barcode)
if (!defined('DEFAULT_GENERICQ_LENGTH')) {
	define('DEFAULT_GENERICQ_LENGTH', 100);
}
//Tamaño default para preguntas tipo Fecha/Hora
if (!defined('DEFAULT_DATETIME_LENGTH')) {
	define('DEFAULT_DATETIME_LENGTH', 20);
}
//Tamaño default para campos de Imagen (varios tipos de preguntas)
if (!defined('DEFAULT_IMAGE_LENGTH')) {
	define('DEFAULT_IMAGE_LENGTH', 100);
}
//@JAPR
define('SV_PHOTO_SIZELIMIT', 10 * 1024 * 1024);
define('SV_DOCUMENT_SIZELIMIT', 30 * 1024 * 1024);
//@JAPR 2013-05-06: Agregada la opción para enviara las acciones a ciertos usuarios especiales
//@JAPR 2013-05-06: Corregido un bug, la funcionalidad de Supervisores adicionales no funcionaba con Apps antes de esveBavelMapping, así que se
//validará para que en esos casos se force a enviar IDs positivos que permitan usarla, y solo en las mas recientes envíe IDs negativos como debe ser
define('MAX_LONG', 2147483647);
//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)
define('DEFAULT_DATASOURCE_VALUESPERPAGE', 50);	//Cantidad de valores a mostrar por cada página de la lista de valores de DataSources dentro del Administrador
define('DEFAULT_ADDITIONAL_PAGESTOSHOW', 11);		//Cantidad de páginas adicionales a mostrar en el navegador de paginación de valores de DataSources, el mínimo es 3 (menos que eso no se mostrarán) y siempre deberá ser número non
//@JAPR

$ekt_config = array (
  'upload_dir' => '',
  'upload_maxsize' => 3000000,
    'upload_badext' => 
  array (
    0 => 'php',
    1 => 'php3',
    2 => 'php4',
    3 => 'php5',
    4 => 'pl',
    5 => 'cgi',
    6 => 'py',
    7 => 'asp',
    8 => 'cfm',
    9 => 'js',
    10 => 'vbs',
    11 => 'html',
    12 => 'htm',
  )
);

require_once("repository.inc.php");
$PHPExePath = "C:\\PHP\\PHP.EXE";

//Cargar datos de los repositorios
require_once("ektrepository.inc.php");
//Ruta donde se encuentra el archivo txt que contiene los repositorios dados de alta
$EKTCfgPath='./config/EKT_cfg.ini';
$BITAMRepositories = array();
$BITAMRepositories=BITAMEKTRepository::loadBITAMRepositories();

//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//$ImportXMLLogFile ="./log/importXML_".session_id().".log";
//Ahora estas variables contendrán solo el nombre de archivo pero no el path, ya que este depende del contexto donde se ejecuta la función para obtener el path de los logs
$queriesLogFile ="queries_".session_id().".log";
$SendMailLogFile = "sendMail_".session_id().".log";
//@JAPR

$ADODBLogFile = "";
//$ADODBLogFile = "./log/ADODB_".session_id().".log";

//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Variable para indicar que el proceso se está ejecutando desde un agente de eForms en general, sea cual sea dicho agente
global $gbIsAneFormsAgent;
$gbIsAneFormsAgent = false;
//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
$gbIsPushAgent = false;		//Variable que indica si el proceso se está o no ejecutando desde un agente de eForms, originalmente utilizado por el agente de Push (asignada exclusivamente por eSurveyServicePushAgentDaemon.php)

//@JAPR 2008-12-08: Definición de versiones mínimas de Metadata de acuerdo a funcionalidad agregada
// Primer versión que exportaba la Metadata a PhP con soporte para Rutas alternas
define('ARTUS_MD_EXPORT_METADATA_TO_PHPDIRS', 6.00070);
define('ARTUS_MD_LDAPFIX_FOR_UNIX', 6.00079);
//Versiones mínimas requeridas actualmente
define('ARTUS_MD_REQUIRED_VERSION', ARTUS_MD_LDAPFIX_FOR_UNIX);

//@JAPR 2016-11-04: Validado que WebDesigner sólo funcione a partir de la versión donde fue implementado correctamente por parte de Artus (#IP53DM)
//@JAPR 2017-03-07: Agregado el enlace alternativo para Web Designer dependiendo de la versión de Artus Web instalada (#FVYYQS)
//Se incrementó la versión de 9.0 a la versión en que finalmente Web Designer quedó estable para ser utilizado
define('ARTUSWEB_WEBDESIGNER_VERSION', 9.00002);

//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
define('tCHAR', 1);
define('tVARCHAR', 2);
define('tSMALLINT', 6);
define('tINT', 7);
define('tFLOAT', 8);
define('tDATE', 12);
define('tSMALLMONEY', 21);
define('tSMALLDATE', 22);
define('tREAL', 23);
define('tDECIMAL', 24);

//@JAPR 2009-07-14: Agregadas validaciones por tipo de BD
define('SQL_SERVER', 1);
define('ORACLE', 3);
define('MYSQL', 25);

//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas (corresponde con QTypeID)
//@JAPR 2013-04-29: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
//Agregados por compatibilidad con eBavel para uso en el método GetMappedEBavelFieldsForms, realmente no existen como tipos de preguntas
define('qtpInvalid', -255);
//@JAPR 2014-10-27: Agregado el mapeo de atributos de catálogos
define('qtpAttribute', -3);		//Este tipo de campo representa a un atributo utilizado en algún catálogo de la forma, sin especificar exactamente a que pregunta o sección pertenece (se aplicará un criterio de contexto según el registro donde se mapeó)
//@JAPR 2014-02-19: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
define('qtpLocation', -2);		//Este tipo de campo es equivalente a qtpGPS, por error se agregó este último para correspondencia con QTypeID, sin recordar que ya existía el primero, asi que se respetará el primero pero se usarán indistintamente
//@JAPR
define('qtpEMail', -1);
//Tipos de Preguntas
define('qtpOpen', 0);
define('qtpOpenNumeric', 1);
define('qtpSingle', 2);
define('qtpMulti', 3);
define('qtpOpenDate', 4);
define('qtpOpenString', 5);
define('qtpOpenAlpha', 6);
define('qtpShowValue', 7);
//@JAPR 2012-05-10: Agregados nuevos tipos de pregunta Foto y Action
define('qtpPhoto', 8);
define('qtpAction', 9);
define('qtpSignature', 10);
//@JAPR
define('qtpMessage', 11);
define('qtpOpenTime', 12);
define('qtpSkipSection', 13);
define('qtpCalc', 14);
define('qtpDocument', 15);
//@JAPR 2013-04-12: Agregados nuevos tipos de pregunta Sincronización
define('qtpSync', 16);
//@JAPR
define('qtpCallList', 17);
//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
define('qtpGPS', 18);
//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
define('qtpPassword', 19);
define('qtpMapped', 20);
define('qtpAudio', 21);
define('qtpVideo', 22);
define('qtpSketch', 23);
//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
define('qtpSection', 24);
//@AAL 06/05/2015: Agregado para preguntas tipo BarCode
define('qtpBarCode', 25);
//OMMC 2015-11-27: Agregado para preguntas OCR
define('qtpOCR', 26);
//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
define('qtpExit', 27);
//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
define('qtpUpdateDest', 28);
//OMMC 2016-04-14: Agregado el tipo de pregunta Mi ubicación
define('qtpMyLocation', 29);
//@JAPR 2019-02-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
define('qtpSketchPlus', 30);

//@JAPR 2015-06-11: Agregada la subclasificación de algunos tipos de pregunta para el rediseño
define('qtpSingleMenu', 2);
define('qtpSingleVert', 201);
define('qtpSingleHoriz', 202);
define('qtpSingleAuto', 203);
define('qtpSingleMap', 204);
define('qtpSingleGetData', 205);
//@JAPR 2016-05-10: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
define('qtpSimpleMetro', 206);
//@JAPR
//OMMC 2019-03-25: Pregunta AR #4HNJD9
define('qtpSimpleAR', 207);
define('qtpMultiMenu', 3);
define('qtpMultiVert', 301);
define('qtpMultiHoriz', 302);
define('qtpMultiAuto', 303);
define('qtpMultiRank', 304);
//@JAPR 2015-08-15: Agregado el ipo de pregunta HTML. Internamente es una tipo Mensaje, se reutilizará el campo SI_SV_Question.EditorType para identificarla como Mensaje (Editor Designer de Raquel en v5) o HTML (TinyMCE en v5)
define('qtpHTML', 1100);
//@JAPR

//Tipos de dato de las preguntas (corresponde con el QTypeID original para los tipo Open, es el nuevo QDataType)
define('dtpNumeric', 1);
define('dtpDate', 4);
define('dtpString', 5);
define('dtpAlpha', 6);
define('dtpTime', 12);
//@JAPRWarning: No se debería usar este enumerado para agregar tipos de preguntas sino sólo tipos de dato, así que ShowValue no es un tipo de dato
//tiene los mismos IDs para los elementos previos porque en el enumerado QTypeID que originalmente era el único, ya estaba implícito un tipo de
//dato, por tanto por consistencia se mantuvieron los mismos IDs. Photo por ejemplo es un tipo de dato, ya que la pregunta en ese caso no registra
//ningún otro tipo de captura sino sólo graba una foto (a diferencia de las fotos opcionales en preguntas Open, donde el tipo de dato puede ser 
//numérico o alfanumerico para la respuesta, ahí la foto no es el tipo de la pregunta en realidad)
define('dtpShowValue', 7);
define('dtpPhoto', 8);
define('dtpMultiField', 9);		//Indica que se capturarán varios valores de diferentes tipos de dato en esta pregunta (como un registro de BD)

//Tipos de captura de preguntas de opción múltiple (corresponde con el MCInputType)
define('mpcCheckBox', 0);
define('mpcNumeric', 1);
define('mpcText', 2);

//Tipos de despliegue (corresponde con QDisplayMode)
define('dspVertical', 0);
define('dspHorizontal', 1);
define('dspMenu', 2);
define('dspMatrix', 3);
define('dspEntry', 4);
define('dspAutocomplete', 5);
define('dspLabelnum', 6);
//Exclusivos de tipo foto
define('dspDefault', 4);		//Es el mismo ID que Entry para respetar todas las preguntas tipo foto que se han creado con el default anterior
define('dspCustomized', 7);		//La foto se ve con el mismo diseño que la pregunta Skip to Section y en lugar "Photo" pone el texto de la etiqueta de la pregunta
define('dspMap', 8);			//Utilizada para que el nuevo diseñador de formas pueda configurar este tipo de pregunta automáticamente sin la configuración UseMap explícita (En realidad no fue usado, porque si el QDisplayMode de una pregunta mapa no es vertical, en el App no se muestra)
//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
define('dspGetData', 9);		//Internamente se visualiza como una Simple choice tipo Menú para que tome automáticamente un valor, sin embargo esta tiene un filtro dinámico automático que debería representar sólo un valor y no está visible, puede usar cualquier atributo mediante variables
//@JAPR 2016-04-28: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
define('dspMetro', 10);			//Despliega una lista de elementos HTML (con posibilidad de personalizar el HTML al gusto del usuario) donde se despliegan los atributos seleccionados (como en tipo Menu) verticalmente a la derecha de la imagen asociada como atributo imagen. Cada elemento ocupa el 100% del width y un alto fijo ajustado al máximo generado
define('dspImage', 11);     //Despliegue especial para las imágenes, permite el un click en el app para que se muestren de manera maximizada, se tratarán como una nueva pregunta pero sólo será una pregunta foto disfrazada.
//OMMC 2019-03-25: Pregunta AR #4HNJD9
define('dspAR', 12);      //Despliegue definido para las preguntas AR, ya que serán una simpleChoice de catálogo con foto

//@JAPR 2013-04-15: Agregada la opción de despliegue de texto para algunas preguntas (originalmente usado para las skip to section, foto personalizada y sincronización)
//Este campo aparentemente se planeaba usar en conjunto con la pregunta ShowValue/ShowQuestion, pero dicha pregunta nunca se implementó y por tanto ninguna pregunta tendría
//un valor diferente de 0. Originalmente contendría valores de Single line y multi line así que si se retoma la idea se dejarán combos correspondientes con dichos valores, pero 
//por ahora se aplicará para la posición del texto en las preguntas que se basan en generar un botón con imagen
//Tipos de despliegue de texto para preguntas ShowValue (funcionalidad no terminada) (TextDisplayStyle)
define('dspTxtSingleline', 0);
define('dspTxtMultiLine', 1);

//@JAPR 2014-05-21: Agregado el tipo de sección Inline
//Tipos de despliegue de secciones (originalmente usado sólo para las secciones Inline)
//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
//Ahora esta propiedad puede configurar el tipo de despliegue de Maestro-detalle. sdspDefault correspondería a dicho comportamiento
define('sdspDefault', 0);				//Indicaría que se debe utilizar el propio tipo de despliegue que por naturaleza tenía la sección (para dinámicas y maestro detalle es paginación, para estándards es generar una única página, para Inline es captura en línea de todos sus registros). A partir de V6 este despliegue representa a las secciones maestro-detalle exclusivamente, aunque SectionType debería seguir siendo sectMasterDet
define('sdspInline', 1);				//Captura en línea de todos los registros, es decir, en lugar de generar una página para cada uno como las dinámicas, se presenta una única página con una tabla que contiene todas las preguntas como columna y los registros como renglones (tipo Excel)
define('sdspPages', 2);					//Por cada registro de la sección se genera una página de captura de sus preguntas (es como las dinámicas o Maestro-detalle)
//@JAPR

//Tipos de despliegue de texto para preguntas basadas en un botón con imagen (Skip to section, Foto personalizada, Sincronización)
define('butTxtNone', 0);				//No se mostrará el texto en el botón sino solo la imagen
define('butTxtTop', 1);					//El texto se mostrará arriba de la imagen
define('butTxtLeft', 2);				//El texto se mostrará a la izquierda de la imagen
define('butTxtBottom', 3);				//El texto se mostrará abajo de la imagen
define('butTxtRight', 4);				//El texto se mostrará a la derecha de la imagen

//Tipos de secciones (corresponde con SectionType)
define('sectAll', -1);					//(Sólo usado en la ventana de configuración de templates) Se refiere a todas las secciones en general
define('sectNormal', 0);				//Sección normal, sólo captura una vez las respuestas de sus preguntas
define('sectDynamic', 1);				//Sección dinámica la cual genera "n" subsecciones dependiendo del valor del catálogo, permitiendo capturar respuestas a sus preguntas por cada subsección
define('sectFormatted', 2);				//Sección formateada la cual no contiene preguntas, sólo texto HTML
define('sectMasterDet', 3);				//Sección maestro-detalle la cual registra "n" capturas de las mismas preguntas, cada una grabada en un renglón diferente de la Fact
define('sectInline', 4);				//Sección llenada en línea, la cual se basará como una dinámica en un catalogo y tal como aquella, generara un registro por cada elemendo del catálogo
define('sectRecap', 5);

define('ebftForms', 0);                 //@AAL Empleado para identificar si es una Forma de eBavel.
define('ebftViews', 1);                 //@AAL Empleado para identificar si es una Vista de eBavel.

//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple
define('ioaUnChecked', 0);				//Insertar Actión asociado a respuesta de selección múltiple cuando la respuesta NO se selecciona
define('ioaChecked', 1);				//Insertar Actión asociado a respuesta de selección múltiple cuando la respuesta NO se selecciona

//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas (SI_SV_SurveyAgendaDet)
//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
//Ahora estos estados además aplican para la agenda, pero aún siguen aplicando para las encuestas particulares de cada agenda
define('agdOpen', 0);					//Indica que la agenda aun no ha sido capturada (no ha iniciado siquiera)
define('agdAnswered', 1);				//Indica que la agenda ya fué capturada (para encuestas)/iniciada (para agendas)
define('agdRescheduled', 2);			//Indica que la agenda fué recalendarizada (no hubo capturas, se cambio la fecha desde el Administrador)
//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
define('agdFinished', 3);				//Indica que la agenda ha sido terminada por completo (sólo para agendas, equivale a agdAnswered para encuesta)
define('agdCanceled', 4);				//Indica que la agenda fue cancelada (ya no volverá a cambiar de estado, sólo puede ocurrir en la encuesta de Check-in)
//@JAPR

//@JAPR 2015-07-26: Removidas todas las referencias a las tablas enc_ pues tenía años que no se usaba, SurveyType no indica nada en absoluto a la fecha de
//esta limpieza, eventualmente se podría llegar a usar para algún fin pero por ahora todas son consideradas svtNormal
//Tipos de encuesta
define('svtNormal', 0);			//Tipo de encuesta normal (no tiene funcionalidad especial)

//@JAPR 2015-07-14: Agregadas las constantes para el tipo de log a utilizar BITAMReport => $TypeID
define('rptSyncs', 0);
define('rptEntries', 1);
define('rptDataDest', 2);

//@JAPR 2014-06-25: Agregado el estatus especial de Ciudadano Vigilante, lo cual significa que no sólo no se puede editar sino que ya no será ni
//siquiera mostrada en el Administrador de eForms (precisamente para evitar que se pueda editar) y no se descargará al App de eForms, exclusivamente
//lo hará para Ciudadano Vigilante
define('svstInDevelopment', 0);		//La encuesta se puede modificar sin problema
define('svstActive', 1);			//La encuesta ya está en producción, no se puede editar pero es visible (a partir de cierta v4 ya no se puede regresar a Status de Development)
define('svstInactive', 2);			//La encuesta no está visible ni se puede capturar (en el Admin aparece en una sección especial)
define('svstVigilant', 3);			//La encuesta no aparece en el Admin (así que no se puede editar mas), sólo es capturable por ciudadano Vigilante

//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
define('otyItem', 0);
define('otySurvey', 1);
define('otySection', 2);
define('otyQuestion', 3);
define('otyAnswer', 4);
define('otyAgenda', 5);
define('otyCatalog', 6);
define('otyAttribute', 7);
define('otyDraft', 8);
define('otyOutbox', 9);
define('otyPhoto', 10);
define('otySignature', 11);
//@JAPR 2014-08-05: Agregado el Responsive Design
define('otySetting', 12);
//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
define('otySurveyFilter', 13);
define('otySectionFilter', 14);
define('otyMenu', 15);		//En el App corresponde con otyMenu: 11
define('otyAudio', 16);		//En el App corresponde con otyAudio: 12
define('otyVideo', 17);		//En el App corresponde con otyVideo: 13
define('otyQuestionFilter', 18);	//A partir de esta constante se volvió a sincronizar con el App
define('otyStatusAgenda', 19);
define('otyStatusDocument', 20);
define('otySketch', 21);
define('otyOption', 22);
define('otyShowQuestion', 23);
//@JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
define('otyUser', 24);
define('otyUserGroup', 25);
/*2015-07-19@JRPP Se agrega Datasource y dataSourceMember*/
define('otyDataSource', 26);
define('otyDataSourceMember', 27);
define('otyDataDestinations', 28);
define('otyArtusModels', 29);
//@JAPR 2015-08-13: Agregado el borrado de reportes a partir del ID de la captura
define('otyReport', 30);
define('otyAppCustomization', 31);
//@JAPR 2015-08-27: Agregadas constantes para procesos de Agendas (Antonio)
define('otyAgendasCheckpoint', 32);
define('otyAgendasScheduler', 33);
define('otyAgendasMonitor', 34);
define('otyLink', 35);
//@JAPR 2016-02-23: Agregados los templates de estilos personalizados de v6
define('otyAppCustStyles', 36);			//Utilizado para diferenciar entre el grabado de estilos de v5 respecto al de v6
//@JAPR 2016-04-15: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
define('otySkipRule', 37);				//Reglas para saltar a partir de expresión con variables (usando atributos de la misma pregunta) para preguntas Simple choice de catálogo
//GCRUZ 2016-05-10. Agregado grabado de mensajes push
define('otyPushMsg', 38);
//@JAPR 2016-06-23: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
define('otyImagePackage', 39);			//Agregado por los catálogos para subir el archivo .zip con imágenes que deben grabarse en customerImages
//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
define('otySyncLog', 40);				//Agregado para controlar la ventana del log de sincronizaciones
define('otyDataDestinationLog', 41);	//Agregado para controlar la ventana del log de destinos de datos
define('otySubscription', 42);			//Agregado para controlar la ventana de permisos de login
//@JAPR 2016-11-04: Agregada la configuración de permisos en el Administrador (#KY40NF)
//Habían agregado nuevas opciones al menú lateral pero no se habían validado correctamente según lo indicado en este issue
define('otyWebDesigner', 43);			//Agregado para controlar la ventana de Web Designer
define('otyPortal', 44);				//Agregado para controlar la ventana de Portal

//@JAPR 2014-09-30: Agregados los filtros de preguntas para definir colores de los marcadores en simple choice con mapa
define('qftpMarkerColors', 0);		//Filtros de preguntas simple choice con catálogo configuradas para mostrar mapas. Se usan para pintar de diferentes colores a los markers del mapa según atributos del catálogo usado

//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
//Estas constantes representan nombres de campos en diferentes tablas (configuraciones de los objetos) utilizados para almacenar el momento en que se
//realizó una actualización de fórmula para cambiar de grabar con números a grabar con IDs en la función BITAMSurvey::ReplaceVariableQuestionNumbersByIDs
//a partir de la versión del Admin esvExtendedModifInfo. La tabla donde originalmente se utilizaron es SI_SV_SurveyFormulaReplace
define('optyAdditionalEMails', 1);
define('optyActionText', 2);
define('optyActionTitle', 3);
define('optyShowCondition', 4);
define('optyFormattedText', 5);
define('optyHTMLHeader', 6);
define('optyHTMLFooter', 7);
define('optyFilterText', 8);
define('optyDefaultValue', 9);
define('optyFormula', 10);
define('optyLabel', 11);
define('optyActionCondition', 12);

//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
//Estas constantes representan el tipo de permiso de la tabla SI_SV_Security, basado en el tipo de objeto definido por las constantes "oty" descritas arriba
define('asecShow', 0);					//Permiso de ver la ventana de configuración correspondiente al objeto "oty" configurado en seguridad
define('asecCreate', 1);				//Permiso para crear el objeto en cuestión
define('asecEdit', 2);					//Permiso para editar el objeto en cuestión
define('asecDelete', 3);				//Permiso para eliminar el objeto en cuestión
define('asecGrant', 4);					//Permiso para asignar permisos al objeto en cuestión

//Estados de los permisos en la tabla SI_SV_Security para cada columna de los tipos de constantes de permisos mencionadas arriba
define('perDenied', 0);					//Indica que el permiso está bloqueado
define('perGranted', 1);				//Indica que el permiso está habilitado

//Constantes para referirse a usuario o grupos específicos
define('usrSUPERVISOR', 0);
define('usrMASTER', 1);

//@JAPR 2014-07-30: Agregado el Responsive Design (DeviceID)
define('dvcWeb', 0);
define('dvciPod', 1);
define('dvciPad', 2);
define('dvciPadMini', 3);
define('dvciPhone', 4);
define('dvcCel', 5);
define('dvcTablet', 6);

//@JAPR 2016-07-18: Validado para permitir limpiar los mapeos hacia eBavel (#4LROGA)
define('sfidNoMapping', 0);
//@JAPR 2013-04-26: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
define('sfidStartDate', 1);
define('sfidStartHour', 2);
define('sfidEndDate', 3);
define('sfidEndHour', 4);
define('sfidLocation', 5);
define('sfidSyncDate', 6);
define('sfidSyncHour', 7);
define('sfidServerStartDate', 8);
define('sfidServerStartHour', 9);
define('sfidServerEndDate', 10);
define('sfidServerEndHour', 11);
define('sfidScheduler', 12);
define('sfidEMail', 13);
define('sfidDuration', 14);
define('sfidAnsweredQuestions', 15);
define('sfidEntrySection', 16);
define('sfidDynamicPage', 17);
define('sfidDynamicOption', 18);
define('sfidFactKey', 19);
define('sfidEntryID', 20);
define('sfidUserID', 21);
define('sfidUserName', 22);
//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
define('sfidGPSCountry', 23);
define('sfidGPSState', 24);
define('sfidGPSCity', 25);
define('sfidGPSAddress', 26);
define('sfidGPSZipCode', 27);
define('sfidGPSFullAddress', 28);
//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
//@JAPR 2013-07-03: Este día cambió el requerimiento, aunque sea una tabla diferente que el mapeo de datos, se van a reutilizar las constantes
//sfid pero en forma negativa para representar los mismos valores obtenibles desde la captura de la encuesta, por lo que las constantes safid sólo
//serán una continuación del último sfid existente hasta esta fecha y se usarán para identificar exclusivamente a datos que existian en las acciones
//originales que apuntaban a eMeeting antes del cambio para mapear a formas de eBavel. Algunas de las constantes safid no necesariamente existen
//por tanto en cualquier encuesta, ya que serían exclusivas de preguntas relacionadas con acciones. Al grabar en las tablas se hará de la siguiente
//forma según el caso:
//SI_SV_SurveyFieldsMap 					-> Valores positivos (sólo safid)
//SI_SV_SurveyActionFieldsMap 				-> Valores negativos (sfid + safid)
//SI_SV_SurveyAnswerActionFieldsMap			-> Valores negativos (sfid + safid)
define('safidDescription', 29);				//Texto de descripción de la acción (es el mismo que la respuesta de la pregunta tipo Acción o el texto de la Simple/Multiple option de acción seleccionada)
define('safidResponsible', 30);				//Responsable al que se dirige la acción (pedido/configurado por la pregunta). Es eMail del usuario que resulte el responsable
define('safidCategory', 31);				//Valor obtenido directamente de un catálogo de eForms (pedido/configurado por la pregunta)
define('safidDueDate', 32);					//Fecha de caducidad de la acción (pedido/configurado por la pregunta)
define('safidStatus', 33);					//Valor del Status default de las acciones creadas por eForms. Default == 'Request' (diferentes formas de eBavel podrían manejar diferentes descripciones, este valor se obtendrá desde un Setting de eForms y depende del usuario estandarizarlo en las formas de eBavel)
define('safidSource', 34);					//Valor del Source default de las acciones creadas por eForms. Default == 'eForms' (diferentes formas de eBavel podrían manejar diferentes descripciones, este valor se obtendrá desde un Setting de eForms y depende del usuario estandarizarlo en las formas de eBavel)
define('safidSourceID', 35);				//ID de la encuesta que genera la acción de eBavel (numérico si el campo es numérico en eBavel, nombre de la encuesta si el campo es alfanumérico en eBavel)
define('safidSourceRowKey', 19);			//(equivale a sfidFactKey) FactKey del registro específico de la captura de la encuesta que genera la acción de eBavel (numérico)
//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
//@JAPR 2014-05-20: Corregido un bug, se le había dado el mismo valor que safidDescription así que estaba regresando mal el valor
define('sfidLocationAcc', 36);
//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
define('safidTitle', 37);					//Texto del título de la acción (sólo es configurable fijo desde la Simple/Multiple option o desde la pregunta tipo acción)
//@JAPR 2014-10-23: Agregado el mapeo de Constantes fijas
define('sfidFixedStrExpression', 38);		//Cuando se seleccione este tipo de mapeo, se resolverá una expresión tipo texto la cual finalmente será la que se grabará en eBavel
define('sfidFixedNumExpression', 39);		//Cuando se seleccione este tipo de mapeo, se resolverá una expresión tipo numérica (flotante) la cual finalmente será la que se grabará en eBavel
//@JAPR 2014-10-23: Agregado el mapeo de Scores
define('sfidSurveyScore', 40);				//Cuando se seleccione este tipo de mapeo, se grabará en eBavel el score completo de la captura de la encuesta, considerando todas las preguntas de todas las secciones para todos los registros capturados
		//El Score particular de una pregunta se obtendrá mapeando directamente a la pregunta, pero en lugar del DataID == 0 se usará DataID == 1 (ver las constantes 'sdid' siguientes a esta lista)
//@JAPR 2014-10-23: Agregado el mapeo de atributos de catálogos
define('sfidCatalogAttribute', 41);			//Cuando se seleccione este tipo de mapeo, se grabará en eBavel el score completo de la captura de la encuesta, considerando todas las preguntas de todas las secciones para todos los registros capturados
//@JAPR 2017-02-01: Agregado el mapeo del UUID (#TDYU15)
define('sfidUUID', 42);						//Identificador único del dispositivo (UUID = Universally unique identifier)

//@JAPR 2014-10-23: Agregado el mapeo de Scores
//Estas constantes identifica el tipo de información (DataID) que se pedirá de las preguntas que son mapeadas a eBavel. Por default se pide el valor respondido
define('sdidValue', 0);						//Se graba en eBavel el valor de la pregunta
define('sdidScore', 1);						//Se graba en eBavel el score de la pregunta si es que lo soporta (la versión mínima del App debe ser >= esveBavelScoreMapping)
		
//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
//Estas constantes definen el tipo de información que se obtendrá de los atributos mapeados desde campos de eBavel
define('ebfdValue', 0);						//El valor por default según el tipo de campo. Para los que son geolocalización, sería el equivalente al valor directo de la posición separada por "," (ej: Lat, Long)
define('ebfdLatitude', 1);					//Sólo para campos tipo Gegolocalización. Extrae únicamente la porción de la Latitude
define('ebfdLongitude', 2);					//Sólo para campos tipo Gegolocalización. Extrae únicamente la porción de la Longitude

//@JAPR 2013-03-26: Agregadas las constantes de eEMails fijos en algunos procesos (reporte de errores entre otros)
define('eFormsDeveloper1', 'jpuente@bitam.com');
define('eFormsDeveloper2', 'omellado@bitam.com');
//@JAPR 2019-05-20: Ajustada la cuenta de correo al desarrollador actual
define('eFormsDeveloper3', 'mperez@bitam.com');
//@JAPR 2019-05-20: ACeballos solicitó que se removiera su correo de estas notificaciones
define('KPIAdmin1', 'eformssoporte@bitam.com');
define('KPIAdmin2', 'sbrande@bitam.com');
//@JAPR 2019-05-20: Por petición de CGil se removió a todo el equipo de QA de estas notificaciones
define('QAStaff1', 'eformssoporte@bitam.com');
define('QAStaff2', 'eformssoporte@bitam.com');
define('QAStaff3', 'eformssoporte@bitam.com');
define('QAStaff4', 'eformssoporte@bitam.com');
define('QAStaff5', 'eformssoporte@bitam.com');
//@JAPR 2019-05-20: Ajustada la cuenta de correo al encargado de soporte actual
define('SupportStaff1', 'mdorantes@bitam.com');
define('SupportStaff2', 'jgonzalez@bitam.com');
//@JAPR 2015-12-07: Modificada la cuenta de soporte ya que la original fue dada de baja (#NVL2WM)
define('KPIOnlineSupportTeam', 'eformssoporte@bitam.com');	//Original: support@kpionline.com
//@JAPR 2015-12-08: Agregada la lista de distribución para desarrollo, calidad y administradores de KPIOnline
define('eFormsDevelopmentTeam', 'desarrolloeforms@bitam.com');
define('eFormsQualityTeam', 'calidadtampico@bitam.com');
define('KPIOnlineAdminTeam', 'adminkpionline@bitam.com');
define('InternalEForms', 'internal_esurvey@bitam.com');		//Pwd: 4321
define('KPIOnlineeSurvey', 'esurvey@bitam.com');			//Pwd: 4321
define('KPIOnlineBarceleSurvey', 'barcel@kpionline.com');	//Pwd: 8754

//@JAPR 2013-08-29: Agregadas las constantes para el CustomerType
define('cmrtCustomer', 1);
define('cmrtFreeTrial', 2);
define('cmrtInternal', 3);
define('cmrtPartner', 4);
define('cmrtPOC', 5);
define('cmrtPartnerProspect', 6);
define('cmrtFreeTrialCont', 7);
define('cmrtCustomerDead', 8);
define('cmrtPartnerDead', 9);
define('cmrtDelete', 10);
define('cmrtDemo', 11);
define('cmrtTesting', 12);

//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
define('rgpsAtEnd', 0);
define('rgpsAtStart', 1);

//@JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
define('plyoLeft', 0);
define('plyoTop', 1);
define('plyoBehind', 2);

//@JAPR 2015-08-15: Agregado el ipo de pregunta HTML. Internamente es una tipo Mensaje, se reutilizará el campo SI_SV_Question.EditorType para identificarla como Mensaje (Editor Designer de Raquel en v5) o HTML (TinyMCE en v5)
define('edtpHTML', 0);
define('edtpMessage', 1);

//OMMC 2016-01-18: Agregado para el tipo de pregunta HTML. Guarda los estados del editor HTML (redactor), si se desea que se abra en modo gráfico o modo HTML
define('edstGraphic', 0);
define('edstHTML', 1);

//@JAPR 2015-08-15: Agregada la configuración para el tipo de teclado en preguntas numéricas
define('kybNumeric', 0);
define('kybTelephone', 1);

//@JAPR 2015-09-17: Agregadas las constantes para RecurrPatternType de SurveyAgendaScheduler y DataSource (frecuencias de generación / carga)
define('frecNone', 0);
define('frecHour', 0);
define('frecDay', 1);
define('frecWeek', 2);
define('frecWeekOfMonth', 8);
define('frecMonth', 4);
define('frecYear', 5);

//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
define('ebavMapValue', 0);					//Mapeo del valor base de la pregunta (según el tipo, puede ser una foto, un string, un número, etc.)
define('ebavMapStrokes', 1);				//Mapeo de los trazos de preguntas Sketch+
define('ebavMapSrcImage', 2);				//Mapeo de la imagen original de la pregunta Sketch+
//@JAPR

// Versiones del ESurvey Service / Aplicación móvil (la Aplicación enviará el ID de versión y el Web Service identificará que debe regresar
//basandose en este número, en general el # de Versión del WebService debe ser >= que el de la aplicación, o si no se debería regresar un
//mensaje de error para que la aplicación cambie a otra ruta/solicite actualizarse)
//@JAPR 2018-09-26: Validado que todo upload realizado al server busque explícitamente la respuesta de que todo ok, de lo contrario se asumirá que hubo algún tipo de error, esto
//debido a que se habían perdido imágenes al realizar uploads con display_errors = 0, cuando ocurrían errores de php pero aún así regresaban un http code 200 en lugar de un 500 (#961S7M)
define('ESURVEY_SERVICE_VERSION', '6.03000');		//Versión actual del ESurvey Service (eSurveyService.php)

define('esvOriginalVersion', '1.00000');			//Versión inicial del ESurvey (sin SkipLogic)
define('esvNewSkipLogic', '2.00000');				//Versión que agregó el cambio en los SkipLogic a como actualmente se conocen
define('esvIntelligentBehaviour', '3.00000');		//Versión con soporte de descargas inteligentes de catálogos a través de la versión de los registros (es la primera en la que se integró JPuente)
define('esvAgendaCatalogFilter', '3.00002');		//Versión con soporte de filtrado de agendas cuando el Web Service responde el array completo de filtro para cada padre incluyendo el Atributo usado en la Agenda
define('esvSurveyVersionNumber', '3.00003');		//Versión con actualización del número de versión de las encuestas (sólo registra cambios en su estructura, no en la de los catálogos asociados a esta)
define('esvSurveyAddNewAnswers', '3.00004');		//@JAPR 2012-04-02: Versión que permite agregar nuevas respuestas en las preguntas de Selección sencilla sin catálogo en modo de despliegue vertical/menu (opción de "Otro")
define('esvSurveyMasterDetSection', '3.00005');		//@JAPR 2012-04-02: Versión que soporta secciones Maestro - Detalle
//Por problemas de generación de varios Apps que incrementaron el número de versión, las versiones 3.00006, 3.00007 y 3.00008 NO se sincronizaron en cuanto a número con el Web Service, el cual siguió siendo 3.0005
//a partir de 3.0009 se va a mantener siempre sincronizado
define('esvGlobalCatalogs', '3.00009');				//@JAPR 2012-05-07: Versión que soporta catálogos que sólo se usan en preguntas y no en encuestas, además ya no regresa el attributeValues a nivel de las preguntas
define('esvActionQuestions', '3.00010');			//@JAPR 2012-05-15: Versión que soporta preguntas de tipo action para sincronizar con el eBavel
define('esvSurveyReschedule', '3.00011');			//@JAPR 2012-05-29: Versión que soporta recalendarizar las agendas en base a las encuestas contestadas, además de permitir especificar la cantidad de posiciones decimales en las preguntas numéricas
define('esvHelloAndSettings', '3.00012');			//@JAPR 2012-05-29: Soporte de "Hello" para verificación de conexión. Correcciones variadas tanto en servicio como en móviles (configuraciones extras para foto, GPS y demás)
//La versión 3.00013 del Web Service no tuvo cambios por lo que realmente no se instaló con dicho número
define('esvFormatMaskHideAttribs', '3.00014');		//@JAPR 2012-08-02: Versión que permite especificar un formato para las preguntas numéricas, además de ocultar atributos del catálogo
define('esvAppOptimization300015', '3.00015');		//@JAPR 2012-08-31: La versión del servicio no cambió. Se generó por compatibilidad con la versión del App que integró el PhoneGap0.9.5 nuevamente y optimizó el ChangePage (versión de release a la tienda)
define('esvPhotosFromCanvas', '3.00016');			//Versión que genera las fotos utilizando el canvas, debido a que en el phoneGap 0.9.5 no funcionan los parámetros Width y Height, y era necesario usar esa versión para que funcionara establemente en IOs
define('esveBavelSuppScoresCube', '3.00016');		//@JAPR 2012-09-05: Versión que corrige un bug en la integración con eBavel por nombres de clases repetidos entre productos. Contiene además el cubo global de Scores para las capturas de encuestas
define('esvAltEMailForNotif', '3.00017');			//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
define('esvExtendedActionsData', '3.00018');		//@JAPR 2012-10-29: Agregada la información extra con variables que se refieren a las preguntas en las acciones de eBavel
define('esvServerDateTime', '3.00019');				//@MABH 2012-12-12: Agregadas las dimensiones con la fecha de captura pero según el servidor
define('esvMultiDynamicAndStandarization', '4.00000');//@JAPR 2012-05-29: Soporte para múltiples secciones dinámicas y estandarización para soportar todo tipo de preguntas/opciones en todas las secciones
define('esveFormsV41Func', '4.01000');				//@JAPR 2012-10-19: Soporte para las característivas de v4 Reestructurada (v4.1)
define('esveFormsStdQSingleRec', '4.01001');		//@JAPR 2013-01-10: Agregada la configuración por encuesta para permitir generar un registro especial para las preguntas de secciones estáticas y con esto impedir que los registros de otras secciones repitan valores de dichas preguntas
define('esvCatalogDissociation', '4.02000');		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
define('esvShowHideQuestionsImpr', '4.03000');		//@JAPR 2013-01-18: Modificado el método de ocultar/mostrar preguntas así como los saltos para optimizar y corregir de raiz todos los casos
define('esvSendReportButton', '4.03001');			//@JAPR 2013-01-18: Agregado el botón para subir los archivos locales del móvil, además se incluyó la versión del App que graba los outbox
define('esvCustomOtherComentLabelv403', '4.03005');	//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones de preguntas
//@JAPR Agregada funcionalidad para soporte de encuestas tipo iusacell
define('esvSyncQuestionAndSkipFoto', '4.04000');	//@JAPR 2013-01-18: Agregado el tipo de pregunta Sincronización y la opción para que al tomar foto haga un brinco de sección (tipo Skip to section, no Change Section)
define('esveBavelMapping', '4.04002');				//@JAPR 2013-01-18: Agregado el mapeo directo de campos de eBavel para agregar valores en las formas de dicho producto durante la captura de eForms (independiente a las preguntas tipo acción, aunque si se combinan tendrían que usar la misma forma de eBavel para ambas cosas)
//define('esvCallList', '4.04002');					//@JAPR 2013-07-02: Por solicitud de LRoux, se incrementó la versión de metadata de esta funcionalidad para evitar que al liberar nueva funcionalidad se utiliza esta, ya que al dia de este cambio no aplicaba para IOs todavía, el valor original era 4.04002
define('esvCustomOtherComentLabelv404', '4.04004');	//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones de preguntas
define('esveFormsAgent', '4.04006');				//@JAPR 2013-05-06: Versión que introduce el uso del eForms Agent para cargar los datos posterior al proceso de sincronización, el cual simplemente dejará el archivo de outbox en el server a partir de ahora
define('esveBavelSuppActions', '4.04007');			//@JAPR 2013-05-06: Agregado el soporte de acciones de eBavel mapeadas a una forma específica desde acciones fijas en opciones de respuesta de simple y multiple choice
define('esvPulsoSaveFromCatFilter', '4.04009');		//@JAPR 2013-08-26: Esta realmente no es una versión del App sino una validación en el server para grabar las preguntas Simple choice de catálogo a partir de la propiedad catalogFilter en lugar de la propiedad answer, a raiz de un problema con celulares LG Optimus L5 que mandaban mal los datos
define('esveBavelSuppActions123', '4.04010');		//@JAPR 2013-05-06: Corregido un bug con los usuarios Supervisores 1 al 3 y el MySelf como responsables de acciones, antes de esta versión no se aceptaban números negativos así que se incluyeron dummies con valores cercanos al MAX_LONG para forzar a que se vieran en las Apps
define('esvHideAnswers', '4.04013');				//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas de simple y multiple choice durante la captura nada mas, ya que en reportes seguirán apareciendo. Agregado el soporte para ShowQuestions en múltiple choice
define('esvAutoRedrawFmtd', '4.04017');				//@JAPR 2013-11-19: Agregada la opción para redibujar automáticamente las secciones formateadas cada que se piden, además se agregaron algunas variables de sistema
define('esvGPSFixAndAppUpdateScript', '4.04019');	//@JAPR 2013-11-19: Agregado en el App código para invocar al SetGPSWatch cada 30 segundos, buscando con eso corregir los problemas de GPS. Agregado código para permitir inyectar código javascript en las Apps. Agregado el log de sincronizaciones
define('esvAccuracyValues', '4.04020');				//@JAPR 2013-12-02: Agregadas las dimensiones e indicadores de Accuracy tanto a las encuestas como al cubo Global de Encuestas (agregadas dinámicamente la primera vez que se carga una instancia individual de encuesta)
define('esvSimpleChoiceSharing', '4.04021');		//@JAPR 2013-12-05: Agregada la opción para que las preguntas de tipo simple choice puedan compartir las mismas opciones de respuesta definidas en otra pregunta anterior, de forma que sólo se tengan que agregar/editar en un mismo lugar
define('esvSectionVariables', '4.04021');			//@JAPR 2013-12-05: (En el App) Agregadas las variables de secciones para permitir obtener datos de su definición, así como información del catálogo en el caso de las secciones dinámicas
define('esvShowSectionCondition', '4.04022');		//@JAPR 2013-12-10: Agregada la condición para determinar si una sección se muestra o no durante la captura
define('esvDefaultValueFixApp', '4.04023');			//@JAPR 2013-12-16: Corrección en el App para el despliegue y grabado de preguntas (open y SCH) que tenían un defaultValue dentro de secciones dinámicas, ya que no consideraba el número de registro ni su visibilidad por registro correctamente
define('esvGPSSettingAndOutboxCheck', '4.04024');	//@JAPR 2013-12-17: Ahora la opción de Enable GPS Watch ya no es configurable y estará siempre activa. Se agregó una configuración en el server para registrar los datos del GPS al inicio de la captura. Agregado código para verificar si el archivo de Outbox se subió correctamente al server en lugar de mostrar error inmediatamente
define('esvReportEMailsByCatalog', '4.04025');		//@JAPR 2014-01-24: Agregada la opción de la encuesta para definir EMails adicionales a los que se enviarán los reportes de las capturas, pudiendo utilizar referencias a preguntas mediante variables
define('esvGPSQuestion', '4.04026');				//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
define('esvGPSVariables', '4.04027');				//@JAPR 2014-02-12: Agregadas las variables tipo GPS (sólo aplica en los móviles por ahora). Ajustadas opciones de configuración de encuesta y pregunta, removiendo cosas innecesarias y compactando otras (validaciones extras)
define('esvAppUserLanguage', '4.04030');			//@JAPR 2014-06-30: Agregadas la posibilidad de variar el idioma de la Aplicación basado en el idioma del usuario en KPIOnline
define('esvAppCustomBackgroundV4', '4.04031');
define('esvAppCustomBackgroundVersionV4', '4.04032');
define('esvSurveyProfiles', '4.04033');				//@JAPR 2014-07-09: Agregada la funcionalidad para definir perfiles que limiten las encuestas que los usuarios pueden ver a partir de ciertos elementos presentes en la definición de las mismas (por ahora todo se configura directo en la metadata)
define('esvCuentaCorreoAlt', '4.04033');
define('esvCallList', '5.00000');					//@JAPR 2013-07-02: Por solicitud de LRoux, se incrementó la versión de metadata de esta funcionalidad para evitar que al liberar nueva funcionalidad se utiliza esta, ya que al dia de este cambio no aplicaba para IOs todavía, el valor original era 4.04002
define('esvPasswordLangAndRedesign', '5.00000');	//@JAPR 2013-07-02: Agregada el tipo de pregunta Password. Agregadas las traducciones. Rediseñado eForms en cuanto a colores y estética de algunas ventanas / opciones (no hay rediseño del grabado)
define('esvHideNavigationButtons', '5.00004');		//@JAPR 2014-04-05: Agregada la opción para ocultar los botones de navegación (Next y Back) durante la captura de una encuesta
define('esveBavelCatalogs', '5.00005');				//@JAPR 2014-05-13: Agregado el soporte para mapear catálogos de eBavel
define('esvNearBy', '5.00005');						//@MABH 2014-05-16: Agregado el uso de Google Maps para preguntas de catálogo
define('esvDefaultSurvey', '5.00006');				//@MABH 2014-05-21: Agregada la encuesta default a cargar durante el login al App
define('esvInlineSection', '5.00006');				//@JAPR 2014-05-19: Agregado el tipo de sección Inline
define('esvDefaultOption', '5.00006');				//@MABH 2014-05-16: Agregada la configuración para asignar opciones por default a preguntas múltiple choice
define('esvSurveyMenu', '5.00007');					//@MABH 2014-05-27: Agregado el uso de agrupadores de menu para las encuestas
define('esvMCHSwitchBehav', '5.00008');				//@JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice. Agregada la opción para mostrar un Checkbox en preguntas múltiple choice con captura numérica o texto (el checkbox indicaría si se capturó o no un valor)
define('esvSectionFmtdHeadFoot', '5.00009');		//@JAPR 2014-06-02: Agregado el header y footer formateado de las secciones
define('esvCatalogDynFilters', '5.00010');			//@JAPR 2014-06-02: Agregado el soporte para filtros dinámicos de catálogo a nivel de encuesta. Realmente no hay cambios de metadata ni validaciones, pero hasta esta versión es cuando se empezaron a aplicar verdaderamente estos filtros
define('esvAppColorTmpls', '5.00011');				//@JAPR 2014-06-18: Agregado el soporte para templates personalizados de la interface del App
define('esvSectionRecap', '5.00012');				//@MABH 2014-06-18: Agregadas las secciones tipo Reporte para mostrar un resumen de las acciones que se generarán en eBavel al grabar la captura actual
define('esvFixedInlineSections', '5.00012');		//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus renglones en lugar de utilizar un catálogo
define('esvAppUserLanguageV5', '5.00013');			//@JAPR 2014-06-30: Agregadas la posibilidad de variar el idioma de la Aplicación basado en el idioma del usuario en KPIOnline
define('esvAppCustomBackground', '5.00014');
define('esvDeviceDataLog', '5.00016'); 				//Conchita Agregada informacion extra al log (tamaño de pantalla y dispositivo)
define('esvResponsiveDesign', '5.00017');			//@JAPR 2014-07-30: Agregado el Responsive Design para responder con HTML o imagenes ajustadas al tipo de dispositivo que hace el request
define('esvFormsMenuPosition', '5.00017');			//@MABH 2014-08-05: Agregada la configuración para posicionar el menu de formas en el App
define('esvShowInSummary', '5.00017');				//@MABH 2014-08-07: Agregada la opción para indicar si las preguntas de secciones maestro-detalle se deben o no ver en el summary
define('esvSurveySectDisableOpts', '5.00018');		//@MABH 2014-08-07: Agregada la opción para indicar si la encuesta deberá o no grabar datos. Agregada la opción para ocultar secciones del menú de navegación
define('esvShowInOnePageMD', '5.00018');			//@Conchita 2014-08-07 Agregado el mostrar en una sola pagina el summary y el section de las maestro detalle
define('esvRecapDynamicData', '5.00018');			//@JAPR 2014-08-21: Agregada la descarga dinámica de valores de eBavel en secciones tipo Recap
define('esvMenuImage', '5.00018');
define('esvQuestionAudioVideo', '5.00019');
define('esvSketch', '5.00021'); 					//Conchita Tipo sketch con width, height e imagen desde el server
define('esvIndependentImgDownload', '5.00021');		//@JAPR 2014-09-03: Agregada la descarga independiente de las definiciones (es decir, ya no como B64) de todas las imagenes asociadas a las encuestas, de tal forma que se descarguen una a una mediante FileTransfer y el HTML haga referencia a ellas como imagenes directamente
define('esvExtendedModifInfo', '5.00021');			//@JAPR 2014-09-03: Agregados los datos extendidos de modificación, adicionalmente ahora las configuraciones que usen variables grabarán internamente los IDs de preguntas aunque seguirán regresando al App los números de preguntas y se configurarán en pantalla a partir del número
define('esvAgendaRedesign', '5.00022');				//@MABH 2014-09-26: Agregado el rediseño de las Agendas
define('esvInlineMerge', '5.00023');				//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa, de tal forma que todas se vean en una misma página (sólo aplica a secciones con despliegue Inline)
define('esvQuestionFilters', '5.00023');			//@JAPR 2014-09-30: Agregados los filtros de preguntas para definir colores de los marcadores en simple choice con mapa
define('esvSectionQuestions', '5.00024');			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
define('esvSketchImage', '5.00025');				//Agregado para las imagenes desde el server de las tipo sketch
define('esvInlineTypeOfFilling', '5.00025');		//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
define('esveBavelScoreMapping', '5.00026');			//@JAPR 2014-10-23: Agregado el mapeo de Scores
define('esvEditorHTML', '5.00027');					//Sep-2014: Versión con el nuevo diseñador de html
define('esveBavelGeolocationAttribs', '5.00029');	//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
define('esvAgendaDimID', '5.00030');				//@JAPR 2014-11-18: Agregada la dimensión Agenda
define('esvIsInvisible', '5.00033');				//Conchita agregada propiedad para question para que sea invisible la pregunta
define('esvAgendaWithValues', '5.00034');			//@JAPR 2014-12-03: Agregado el array de valores del catálogo de la agenda
define('esvCatalogAttributeImage', '5.00035');		//@MABH 2014-12-01: Agregado el uso de imagenes en atributos de catalogo para secciones inline
define('esvAgendaScheduler', '5.00035');            //JCEM  2014-11-24: Agrega la generación de agendas desde planificador
define('esvDeleteObjectsValidation', '5.00040');	//Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar si es utilizado y advertir al usuario (#75TPJE)
define('esvMinRecordsToSelect', '5.00041');			//2015.02.10 JCEM #9T6ASP Cantidad minima de registros a seleccionar en seccion inline
define('esvAgendaWithCatValues', '5.00043');		//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
define('esvActionConditions', '5.00043');			//@JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
define('esvRedesign', '5.01000');			        //@AAL 19/03/2015 Agregado para el Rediseño del Admin
define('esveFormsv6', '6.00000');			  		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente. Esta constante sólo se debe usar con temas relacionados con el grabado o validaciones exclusivas de esta versión, no para validaciones de Metadata ni aunque sean exclusivas de esta versión
define('esvAdminWYSIWYG', '6.00000');			  	//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX. Se puede utilizar para validaciones de Metadata o relacionadas con la ventana de Diseño de Formas WYSIWYG
define('esveFormsDHTMLX', '6.00006');			  	//@JAPR 2015-08-04: Integrado el primer componente DHTMLX para eForms App (el DataView de las formas)
define('esveServicesConnection', '6.00008');		//Cambios a DataDestinations
define('esvDeleteReports', '6.00009');				//@JAPR 2015-08-13: Implementado el método para eliminar las capturas (se agregó el campo SurveyKey a todas las tablas)
define('esvMenuImageConfig', '6.00009');			//@GCRUZ 2015-08-21: Configuración de imagen de menu (se agregó el campo SurveyMenuImage a todas las tablas)
define('esveAgendas', '6.00010');					//@JAPR 2015-08-27: Había faltado esta constante de ARamirez). Agregada la integración de Agendas v6
define('esveHideNavButtonsSection', '6.00011');   	//@RTORRES 2015-08-28: Agregado la fucionalidad de ocutar los botoenes de next,back
define('esvDataSourceDocument', '6.00012');			//@JRPP 2015-09-03: Adrego campo documento 
define('esvDataSourceAgent', '6.00014');      		//@JAPR 2015-09-06: Agregado el cambio para que el agente procese los DataSource desde origenes de DropBox y demás en base a una frecuencia
define('esvDataSourceAgentRecurActive', '6.00015'); //@JAPR 2015-09-06: Agregado el cambio para que el agente procese los DataSource desde origenes de DropBox y demás en base a una frecuencia
define('esvDataSourceIdGdrive', '6.00016');     	//@JRPP 2015-09-11: Columna agregada para manipular guardar el id del documento de googledrive
define('esvDataSourceAgentHours', '6.00016');		//@JRPP 2015-09-11: Columna agregada para manipular guardar el id del documento de googledrive
define('esvLinks', '6.00025');						//Agregar módulo de Links
define('esvDataSourceErrors', '6.00025');     		//@JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
define('esvDataDestination', '6.00026');			//@JRPR 2015-12-02: Se agrega las opciones de envio de dashboard y ARS por email.
define('esvMaxQLength', '6.00026');    				//@JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
define('esvHideDescription', '6.00026');    		//OMMC 2015-12-15: Agregado para ocultar la descripción de las formas en el admin.
//@JAPR 2016-02-24: Debido al problema de haber actualizado en tienda a GeoControl a la 6.00028, se decidió este día cambiar el esquema de versiones de eForms, de tal manera que la que
//en esta fecha era 6.00028 se eliminó por completo, la que hubiera sido 6.00029 se movió a la 6.01000, mientras que se tuvo que hacer un rollback de GeoControl (y por ende de eForms)
//donde la versión 6.00027 se renombró a 6.00029 y se volvió a subir a las tiendas, la que hubiera sido 6.00028 en caso de necesitarse, se subirá a las tiendas como 6.00030 pero NO para
//GeoControl, ya que GeoControl sólo se actualizará bajo estricta autorización de David, y tentativamente viendo estos problemas, seguiría su propio número de versión independiente
//a eForms dado a que no necesariamente quieren igualar en funcionalidad a eForms, sólo algunas correcciones
//A partir de este punto hacia abarjo, los números 6.00030 originalmente eran 6.00028, mientras que los 6.01000 originalmente eran 6.00029, NO habría números >= 6.00030 del esquema antes de este problema
define('esvOCRQuestionFields', '6.01000');        	//OMMC 2015-11-27: Agregado para propiedades de preguntas OCR.
define('esvParameterHTTP', '6.00030');        		//@JRPP 2016-01-14: Agregado los parametros en tabla datadestination
define('esvTypeOfDatabase', '6.00030');    			//@JRPP 2016-01-14: Agregado el tipo de Base de datos en servicesconnection
define('esvHTMLEditorState', '6.00030');        	//OMMC 2016-01-18: Agregado el estado en el que se abre el editorHTML
define('esveServicesConnectionName', '6.00030');	//@JRPP 2016-01-14: Agregado isIncremental en tabla datadestination
define('esvShowTotalsInline', '6.00030');			//@JAPR 2016-01-22: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
define('esvAllowGallery', '6.00030');         		//OMMC 2016-02-24: Agregada para permitir acceso a la galería
define('esvExitQuestion', '6.01000');				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
define('esvDestinationIncremental', '6.01000');		//@JRPP 2016-01-14: Agregado isIncremental en tabla datadestination
define('esvadditionaleMail', '6.01000');      		//@JRPP 2016-01-28: Se agrega nuevo campo a la tabla si_sv_datadestinations
define('esvFormsLayouts', '6.01000');				//@JAPR 2016-01-28: Agregados los LayOuts personalizados por forma (#FSMUZC)
define('esvAgendaFirsDayWeek', '6.01000');			//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
define('esvATTRDIDSyntax', '6.01000');        		//OMMC 2016-02-24: Agregada la variable para soportar la sintaxis nueva en el editor de fórmulas.
define('esvAgendaExportTemplate', '6.01000');		//@GCRUZ 2016-02-25: Agregada configuración para seleccionar formas al exportar el template para agendas
define('esvUpdateData', '6.01002');					//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
define('esvShowQNumbers', '6.01002');            	//OMMC 2016-04-06: Agregado para ocultar números de preguntas
define('esvTemplateStyles', '6.02000');				//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6 (@JAPR 2016-03-16: Este día se solicitaron funcionalidades urgentes, así que se decidió mandar este cambio hasta 6.02000)
define('esvSimpleChoiceCatOpts', '6.02001');		//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
define('esvConfLinks', '6.02000');					//RV 2016-04-14: Se valida con metadata el campo para configurar el modo de despliegue de los enlaces (frame o ventana nueva)
define('esvMyLocation', '6.02001');             	//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
define('esvNoFlow', '6.02001');                 	//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
define('esvAppMenuOptions', '6.02001');				//@GCRUZ 2016-04-18: Agregadas opciones configurables para menú de 3 rayas en app.
define('esvMChoiceCat', '6.02001');                 //@JAPR 2016-04-27: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
define('esvSChoiceMetro', '6.02001');               //@JAPR 2016-05-05: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
define('esvUserLanguage', '6.02001');				//@GCRUZ 2016-04-29: Agregada configuración para lenguaje de usuario.
define('esvPushMsgLog', '6.02001');	
define('esvCaptureTransfer', '6.02001');			//@GCRUZ 2016-04-29: Agregada tiempo y distancia de traslado de captura.
define('esvDataSourceFilterFormula', '6.02001');	//@GCRUZ 2016-04-29: Agregado campo para fórmula escrita en datasourcefilter
define('esvFormsDuration', '6.02001');				//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
define('esvAgendaDetNewModelFields', '6.02001');	//@GCRUZ 2016-06-02: Agregados campos a si_sv_surveyagendadet: RealDuration, EntryLatitude, EntryLongitude
define('esvColumnWidthString', '6.02001');			//@JAPR 2016-06-08: Modificada la propiedad (BITAMQuestion.ColumnWidth) para que ahora pueda ser un String (#WFLVTR)
define('esvGeoFence', '6.02001');					//@GCRUZ 2016-06-02: Agregada GeoCerca
define('esvGoogleMapsAPIKey', '6.02001');          	//@JAPR 2016-06-24: Modificada la generación de URLs de Google Maps debido al requerimiento de llave a partir de Junio 2016, para poder cambiar la URL desde el server (#F3M0A7)
define('esvOCRAPIKey', '6.02001');                	//OMMC 2016-07-26: Modificada la generación de la llamada al API de la pregunta OCR ya que ahora requiere un key, se opta por igualarlo al llamado del API de Google Maps
define('esvNewQuestionLengths', '6.02002');			//@JAPR 2016-06-15: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
define('esvQNameCorrection', '6.02002');          	//OMMC 2016-05-02: Agregada la corrección de nombres de pregunta con múltiples espacios
define('esvNavigationHistory', '6.02003');          //@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
define('esvAutoFitFormsImage', '6.02003');          //@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
define('esveFormsUserLicense', '6.02005');   		//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
define('esvRealPushMsg', '6.02007');   				//@JAPR 2016-10-14: Modificado el esquema de Push para envíar mensajes al último dispositivo utilizado por el usuario solamente (#L9T2IU)
define('esvDataDestHTTPSinglePOST', '6.02012');		//@GCRUZ 2016-12-13: Envío de imágenes para destino de datos HTTP en un sólo POST
define('esvPushMsgUserGroups', '6.02012');   		//@EARS 2016-11-09: Cambios en el esquema de generación de notificaciones, se genera un encabezado de mensaje y un detalle de usuarios/grupos 
define('esvServerEditDates', '6.02012');			//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
//@JAPR 2017-01-06: Corregido un bug, la configuración debe tener NULL por default, de tal manera que se asume todas las configuraciones habilitadas por default
//y así todas las opciones aparezcan seleccionadas automáticamente, que es diferente a si explícitamente fuera vacía lo que significaría que ni siquiera se debería ver
//el menú de 3 rayas del App (#EJTH2E)
define('esvHideMenuOptions', '6.02012');			//@JAPR 2016-12-29: Permitir ocultar el menú de 3 rayas vaciando su configuración, así como tomar el default correcto si no existe la configuración (#EJTH2E)
define('esvImageDisplay', '6.02013');         //OMMC 2017-01-11: Agregado el nuevo display de imagen para maximizar las fotos en el app, es una pregunta foto disfraada.
//@JAPR 2017-02-07: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
define('esvCopyForms', '6.02015');					//RV: Agregada la funcionalidad de copiado de formas
//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales de IDs en expresiones con variables de preguntas, lo cual implicaba que internamente quedaban grabados IDs
//que no correspondía a la forma, mismos que al descargar definiciones se corregían, pero al copiar la forma empeoraba causando que no se resolvieran correctamente (#)
define('esvPartialIDsReplaceFix', '6.02016');		//@JAPR 2017-02-17: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
define('esvDynAppImagesFromServer', '6.02017');		//@JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
define('esvEntryDescription', '6.02020');			// MAPR 2017-04-11: Funcionalidad nueva para agregar una descripción a las capturas, JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
define('esvDataDestEmailDetail', '6.02022');		//@JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
define('esvDataDestErrorCheck', '6.02022');			//@JAPR 2018-01-25: Agregado control de errores en los request tipo HTTP (#RXVH44)
define('esvAllMobile', '6.02022');    //MAPR 2018-02-20: A petición de DAbdo ahora el push se mandará a todos los dispositivos que usen la misma cuenta.
define('esvBackgroundMode', '6.02025');         //MAPR 2018-05-21: Agregada la configuración para activar y desactivar el modo segundo plano activo en la aplicación.
define('esvReloadSurveyFromSection', '6.02026'); //MAPR 2018-06-08: Agregada la configuración para permitir escoger al usuario la sección a la que quiere regresar la encuesta al terminar de grabar.
define('esvMapTypeSelector', '6.02028');         	//@JAPR 2018-10-19: Integrado los Mapas de Apple (#64ISFM)
//@JAPR 2018-09-26: Validado que todo upload realizado al server busque explícitamente la respuesta de que todo ok, de lo contrario se asumirá que hubo algún tipo de error, esto
//debido a que se habían perdido imágenes al realizar uploads con display_errors = 0, cuando ocurrían errores de php pero aún así regresaban un http code 200 en lugar de un 500 (#961S7M)
define('esvHTTPCode200RealOkCheck', '6.02028'); 	//Valida que los requests de upload de archivos desde el dispositivo respondan con un verdadero mensaje de que todo ok en lugar de responder con un http status 200 incluso cuando ocurran errores de PhP debido al display_errors = 0 que se configuró en los servidores
//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
define('esvTableSectDescRow', '6.02028'); 			//Activa el modo en el que las secciones tabla habilitan una row adicional con la descripción del registro en lugar de utilizar una columna para ello
define('esvUploadMediaFix', '6.02028'); 			//@JAPR 2018-11-30: Corregidos bugs con la sincronización, se estaba utilizando la misma función para el upload de archivos desde dispositivo que para el upload desde browser (#RTYZBZ)
//@JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
//@JAPR 2019-02-28: Corregido un bug, no se había considerado la compatibilidad hacia atrás en casos donde se usaba la pregunta UpdateData, ya que dicha pregunta no sincronizaba 
//archivos de ningún tipo sino solo respuestas hasta que se cambió en el issue #OA1YQ0, por lo tanto la revisión de esta función no aplica antes del App que sube los archivos (#DIA5G4)
define('esvUpdateDataWithFiles', '6.02031'); 		//Validación de versión para cuando la pregunta UpdateData ya sube los archivos media de la captura como parte de su sincronización
define('esvSketchPlus', '6.02031'); 				//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
define('esvForgetCredentials', '6.02032'); 			//@JAPR 2019-06-19: Agregada la configuración para olvidar el último login al App y solicitad credenciales nuevamente al ingresar (#GHEM2Z)
define('esvDataDestEmailComposite', '6.02035');   //MAPR 2019-07-22 (#280RV6) Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos.
define('esvRecalcDep', '6.02035'); 					//@JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
define('esvAutoSelectSChOpt', '6.02036');			//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
//@JAPR
define('esvQuestionAR', '6.03000');         		//OMMC 2019-03-25: Pregunta AR #4HNJD9
define('esvResetMapRadio', '6.03000');        //OMMC 2019-04-29: Agregada configuración del radio para cálculo de coordenadas #QEGRGA 
define('esvDataSourceDataVersion', '6.04000');		//@JAPR 2018-01-25: Agregado el soporte de catálogos offline (#)
//@JAPR 2012-03-12: Agregado el uso del servicio como un servidor Web para cualquier tipo de aplicación
define('SV_ITEM', '_SV_');
define('SV_SRV_ERROR', '_SV_ERR_');
define('SV_DELIMITER', '_SV_SEP_');
define('SV_DATA_FIELD', '_SV_FLD_');

//@JAPR 2012-04-02: Agregadas constantes varias
define('svstNotStarted', 2);
define('svstNotFinished', 3);
define('svstFinished', 4);

//@JAPR 2015-07-11: Rediseñado el Admin con DHTMLX
//Ahora existirá un campo para indicar el tipo de sección como repeticiones de registros, así que adapta esa configuracion al tipo de sección definido
define('srptSingle', 0);				//La sección es estándar, sólo permite un único registro
define('srptMulti', 1);					//La sección es Maestro-detalle, permite múltiples registros según lo decida el usuario
define('srptCatalog', 2);				//La sección es Inline configurada en modo paginación y de catálogo, hay tantos registros como la combinación del catálogo usado para el atributo indicado

//@JAPR 2014-02-07: Agrupadas las opciones de llenado de preguntas simple y multiple choice
define('tofFixedAnswers', 0);	//Las opciones de respuesta son manualmente agregadas a la pregunta
define('tofCatalog', 1);		//Las opciones de respuesta se obtiene desde el atributo del catálogo especificado
define('tofMCHQuestion', 2);	//Las opciones de respuesta se obtiene dinámicamente durante la captura, a partir de las respuestas de una pregunta múltiple choice
define('tofSharedAnswers', 3);	//Las opciones de respuesta se comparten tal como existan en otra pregunta
//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
define('tofInlineSection', 4);	//Las opciones de respuesta se obtienen dinámicamente durante la captura, a partir de los rows marcados como contestados en una sección Inline

//@JAPR 2014-08-18: Agregada una función genérica (GetEBavelFieldValues) para extraer valores de una forma de eBavel según los parámetros
define('ebdtSectionFilters', 0);					//String regresado para utilizar con sectionFilter.inc.php al momento de llenar dinámicamentes las combos de valores por campo
define('ebdtJSonForApps', 1);						//String JSon en formato útil para las Apps de eForms
define('ebdtJSonArrayForApps', 2);					//Array previo a convertir en formato JSon útil para las Apps de eForms, este es necesario si se invocará como función para posteriormente incluir los datos en un array mayor que se convertirá en JSon
//@JAPR

//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
define('efagtTries', 3);		//Cantidad de intentos por subir la misma tarea con tareas recalendarizadas antes de considerar que el error no se puede reparar por si solo
define('efagtMinutes', 5);		//Cantidad de minutos entre cada intento de ejecución de una tarea recalendarizada

//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
define('asfwdSunday', 0);		//Día inicial de la semana en domingo (default)
define('asfwdMonday', 1);		//Día inicial de la semana en lunes (caso LAMOSA - Solutek, donde los lunes 1° de mes y toda esa semana se consideraban parte de la semana previa)

//@JAPR 2015-10-27: Agregado el proceso para generar todas las Agendas desde el Agente de eForms
//Lista de procesos que pueden agregar registros en la tabla SI_SV_ProcessLog
define('efapAgendas', 1);		//Tarea de generación de Agendas a partir de los Schedulers configurados
//GCRUZ 2016-01-19. Proceso para generar destinos de datos incrementales
define('efapIncrementalDataDestinations', 2);
//GCRUZ 2016-06-06. Proceso para generar cubo de agendas
define('efapAgendaCubeGenerationAM', 3);
define('efapAgendaCubeGenerationPM', 4);

//Tipos de status de ejecución de una tarea de carga de encuestas
define('stsPending', 0);		//La tarea no se ha procesado (de hecho, en la table SI_SV_SurveyTaskLog NUNCA debería existir una tarea en este Status, sólo en SI_SV_SurveyTasks)
define('stsActive', 1);			//La tarea se está procesando (similar al caso previo)
define('stsFailed', 2);			//La tarea terminó con error, hay que revisar el log del proceso
define('stsCompleted', 3);		//La tarea terminó correctamente
//@JAPR 2015-01-29: Agregados nuevos Status para las tareas, para un eventual ajuste en el proceso del Agente
define('stsRescheduled', 4);	//La tarea no se pudo procesar, y el mecanismo que reintenta procesarla falló "n" veces, así que se decidió re-calendarizar para otro día. Esto básicamente quiere decir que esa tarea se desactiva y va a generar otra copia de la tarea para que se vuelva a procesar
								//Este estado puede existir en la tabla de Log (la tarea fue recalendarizada) o en la de pendientes (la tarea es recalendarizada y aún no se inicia su procesamiento)
define('stsManualUpload', 5);	//La tarea fue subida manualmente, cualquier otra tarea debería ser marcada como ignorada
define('stsIgnored', 6);		//La tarea fue ignorada porque ya se había subido previamente, seguramente de forma manual (este Status sólo lo puede realizar el proceso del Agente si antes de procesar la tarea detecta que para esa carga ya existe una tarea en Status == stsManualUpload, la cual finalmente cambiará a este estado)
define('stsTest', 7);			//La tarea probablemente falló, pero era una prueba interna así que no representa un problema

//Tipos de status para los destinos
define('stsPendingDD', 0);			//El destino no se ha reprocesado
define('stsActiveDD', 1);			//El destino se está reprocesando
define('stsFailedDD', 2);			//El reproceso del destino terminó con error
define('stsCompletedDD', 3);		//El reproceso del destino terminó correctamente

//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
//Prefijo para los nombres de dimensiones desasociadas de catálogo, ya que no se puede repetir el mismo nombre de dimensión 2 veces, así que la
//alternativa se generará con este prefijo
define('dsdALTERNATIVE_DIMENSION_PREFIX', 'FCD ');

//@JAPR 2015-02-27: Agregado el Monitor de Operaciones
define('procServiceHello', 300);			//Verifica si KPIOnline está escuchando con un request muy simple
define('procAuthenticateAcc', 301);			//Login de un App de eForms
define('procGetDefinitions', 302);			//Obtiene las definiciones (si es vía Web, se hacen "n" requests, uno por archivo)
define('procGetDefinitionsVersions', 303);	//Obtiene las versiones de objetos para identificar lo que tiene que actualizar
define('procGetLanguageFile', 304);			//Obtiene la definición de idioma
define('procSyncOutbox', 305);				//Sincroniza el archivo de una captura
define('procSyncPhoto', 306);				//Sincroniza una Foto, Sketch o pregunta tipo Firma
define('procSyncSignature', 307);			//Sincroniza la Firma de la Forma
define('procSyncAudio', 308);				//Sincroniza (o sube vía Web, hasta la fecha de implementación había un bug) un archivo de Audio
define('procSyncVideo', 309);				//Sincroniza (o sube vía Web, hasta la fecha de implementación había un bug) un archivo de Video
define('procSyncUsrLog', 310);				//Sincroniza el Log de usuario del App
define('procUploadReportFile', 311);		//Sube un archivo (de cualquier tipo) de definición que se tiene en el App (botón de Send Report)
define('procUploadPhoto', 312);				//Sube vía web un archivo de Foto o pregunta tipo Firma
define('procUploadDocument', 313);			//Sube vía web un archivo de Documento
define('procUploadSketch', 314);			//Sube vía web un archivo de Sketch
define('procGetDynCatValues', 315);			//Obtiene los valores dinámicos de un catálogo
define('procGetRecapSectValues', 316);		//Obtiene los valores dinámicos de una sección Recap
define('procEntryReportData', 317);			//Carga los datos para una edición de captura vía Web
define('procCheckOutboxFile', 318);			//Verifica si el archivo de outbox llegó correctamente al server
define('procAddAgentTask', 319);			//Crea una tarea del agente
//@JAPR 2018-04-16: Agregados registros nuevos al Monitor de Operaciones para los procesos del Agente (#PCRUVT)
define('procProcessAgendas', 320);			//Lanza el proceso de generación automatizado de Agendas a partir de los Schedulers
define('procProcessIncDestinations', 321);	//Lanza el proceso de generación automatizado de Destinos de datos incrementales
define('procProcessAgendasCube', 322);		//Lanza el proceso de generación automatizado del cubo de agendas
define('procProcessDataSources', 323);		//Lanza el proceso de importación automatizado de fuentes de datos esternas
//@JAPR
//Tipos de request del attachURL o invocación al save de una forma DHTMLX (usado en ProcessRequest.php)
define('reqIFrame', 0);		//Se hizo una petición vía attachURL(url, false/null, data) o bien mediante un POST/URL normal de una forma HTML, se espera una 
							//respuesta que ejecute código o bien que devuelva en formato texto el resultado del proceso
define('reqAjax', 1);		//Se hizo una petición vía attachURL(url, true, data) o bien mediante un objForm.send(url, verb, callback) de DHTMLX, se espera una
							//respuesta en formato JSON sobre el resultado del proceso

//@JAPR 2015-09-05: Agregadas las constantes para DataDestinations
define('ddesNone', 0);
define('ddeseBavel', 1);
define('ddesDropBox', 2);
define('ddesGoogleDrive', 3);
define('ddesEMail', 4);
define('ddesFTP', 5);
define('ddesHTTP', 6);
define('ddesTable', 7);

//@JAPR 2016-08-16: Agregadas las constantes para tipos de archivos en destinos de datos tipo EMail (SI_SV_DataDestinations.emailfotmat: PDF, ARS o Dashboard)
define('ddeftNone', 0);				//Usado como valor default cuando el tipo de destino no es EMail
define('ddeftPDF', 3);				//Se envía el PDF default generado por eForms
define('ddeftDashboard', 4);		//Se envía el escenario de Artus asociado al destino
define('ddeftARS', 5);				//Se envía el reporte de ARS asociado al destino

//@JAPR 2015-12-11: Agregadas las constantes para tipos de DataSources (desafortunadamente no hubo acuerdo y no se estandarizaron con DataDestinations, aunque se compartan tipos)
//@JAPR 2015-12-11: Agregado el procesamiento de DataSources de tipo HTTP
define('dstyManual', 1);
define('dstyExcel', 2);
define('dstyeBavel', 3);
define('dstyDropBox', 4);
define('dstyGoogleDrive', 5);
define('dstyFTP', 6);
define('dstyHTTP', 7);
define('dstyTable', 8);

//SI_SV_ServicesConnection.TypeRequest
define('ddrtPOST', 1);
define('ddrtPUT', 2);

//SI_SV_ServicesConnection.Servicedbtype
define('ddrtMySQL', 0);
define('ddrtSQLServer', 1);

//@JAPR 2016-01-28: Agregados los LayOuts personalizados por forma (#FSMUZC)
//SI_SV_Survey.TextPosition
define('svtxpNone', -1);				//No se desplegará nombre/descripción (texto), sólo la imagen asociada a la forma (se requiere tener una imagen personalizada o no habría manera de identificar una forma de otra, si no hay imagen personalizada se asume que el valor es el default o svtxpRight)
define('svtxpDefault', 0);				//Se utilizará la configuración default global, o en todo caso el default codificado
define('svtxpBottom', 1);				//Se desplegará el texto abajo de la imagen
define('svtxpLeft', 2);					//Se desplegará el texto a la izquierda de la imagen
define('svtxpRight', 3);				//Se desplegará el texto a la derecha de la imagen
define('svtxpTop', 4);					//Se desplegará el texto arriba de la imagen
define('svtxOverTop', 5);				//Se desplegará el texto sobre la imagen en la parte superior
define('svtxOverMiddle', 6);			//Se desplegará el texto sobre la imagen a la mitad
define('svtxOverBottom', 7);			//Se desplegará el texto sobre la imagen en la parte inferior
define('svtxpCustom', 8);				//Se desplegará un HTML personalizado definido por objeto/global

//@JAPR 2016-03-08: Agregadas las constantes para el tipo de disposición de las preguntas respecto a sus respuestas al generar el HTML del App
define('qlayUpDown', 0);				//Question Up, Answer Down
define('qlayDownUp', 1);
define('qlayLeftRight', 2);				//Question Left, Answer Right
define('qlayRightLeft', 2);

//@JAPR 2016-03-09: Agregadas las constantes para los bordes seleccionables desde la ventana de templates de estilos
define('bortNoSel', -1);				//Sin selección de configuración de borde
define('bortDefault', 0);				//Configuración default de borde
define('bortNone', 1);					//Sin borde
define('bortAll', 2);					//Los 4 bordes

//Variable Global de Valor de No Aplica
$gblEFormsNA = 'NA';
$gblEFormsNAKey = 1;
$strNewAttribSep = '_SVElem_';
$strNewDataSep = '_SVSep_';
$intCreatedBy = CREATED_BY_EFORMS;
//@JAPRWarning: Temporalmente se creará con un tipo de cubo inexistente para verificar el proceso, remover esta línea al terminar las pruebas
//$intCreatedBy = 10;

$ekt_ArrDeniedExts = array('php','php2','php3','php4','php5','phtml','pwml','inc','asp','aspx','ascx','jsp','cfm','cfc','pl','bat','exe','com','dll','vbs','js','reg','cgi');

define('AWDIM','_AWDim_');
define('bCFG_VERSION_CUBE', 2);

//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
//Configuraciones de KPIOnline de la tabla saas_settings
define('kpisMultiPasswordEnabled', 1);

//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
global $garrActionFields;
$garrActionFields = array();
$garrActionFields[] = sfidStartDate;
$garrActionFields[] = sfidStartHour;
$garrActionFields[] = sfidEndDate;
$garrActionFields[] = sfidEndHour;
$garrActionFields[] = sfidLocation;
$garrActionFields[] = sfidSyncDate;
$garrActionFields[] = sfidSyncHour;
$garrActionFields[] = sfidServerStartDate;
$garrActionFields[] = sfidServerStartHour;
$garrActionFields[] = sfidServerEndDate;
$garrActionFields[] = sfidServerEndHour;
$garrActionFields[] = sfidScheduler;
$garrActionFields[] = sfidEMail;
$garrActionFields[] = sfidDuration;
$garrActionFields[] = sfidAnsweredQuestions;
$garrActionFields[] = sfidEntrySection;
$garrActionFields[] = sfidDynamicPage;
$garrActionFields[] = sfidDynamicOption;
$garrActionFields[] = sfidFactKey;
$garrActionFields[] = sfidEntryID;
$garrActionFields[] = sfidUserID;
$garrActionFields[] = sfidUserName;
$garrActionFields[] = sfidGPSCountry;
$garrActionFields[] = sfidGPSState;
$garrActionFields[] = sfidGPSCity;
$garrActionFields[] = sfidGPSAddress;
$garrActionFields[] = sfidGPSZipCode;
$garrActionFields[] = sfidGPSFullAddress;
$garrActionFields[] = safidDescription;
$garrActionFields[] = safidTitle;
$garrActionFields[] = safidResponsible;
$garrActionFields[] = safidCategory;
$garrActionFields[] = safidDueDate;
$garrActionFields[] = safidStatus;
$garrActionFields[] = safidSource;
$garrActionFields[] = safidSourceID;
$garrActionFields[] = safidSourceRowKey;
$garrActionFields[] = sfidLocationAcc;
//$garrActionFields[] = safidTitle;
//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
$garrActionFields[] = sfidFixedStrExpression;
$garrActionFields[] = sfidFixedNumExpression;
$garrActionFields[] = sfidSurveyScore;
$garrActionFields[] = sfidCatalogAttribute;

//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
global $gblModMngrErrorMessage;
$gblModMngrErrorMessage = '';
//Si se deja activada, algunos procesos que generalmente marcarían un error e interrumpirían la ejecución serán omitidos (en la interfaz del Admin).
//Cambiarla en este punto NO afecta al proceso de grabado de capturas, ya que para tal proceso siempre se asigna en False para no interrumpirlo
//Su uso especialmente es para procesos de migración que no deberían interrumpir la ejecución, pero las ventanas del Admin si deben reportar los
//errores directamente cuando sucedan
global $gblShowErrorSurvey;
$gblShowErrorSurvey = true;
//@JAPR
global $debugValuesProcess;						//Habilita la depuración en pantalla de los proceso relacionados con la captura de Valores de Dimensiones
$debugValuesProcess=false;
global $debugExcelProcess;						//Habilita la depuración en pantalla de los proceso relacionados con MS Excel
$debugExcelProcess=false;
global $aMSEIncludeStatCols;					//Determina si se deberán o no incluir las columnas estadísticas en el Template de MS Excel exportado
$aMSEIncludeStatCols = false;
global $showDebugProcessText;
$showDebugProcessText=false;
global $debugSaveBudget;
$debugSaveBudget=false;
global $keepTempFiles;
$keepTempFiles=false;
global $blnTestingServer;
$blnTestingServer = false;
//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
global $gbEnableIncrementalAggregations;
//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
//Si la forma a grabar fue definida con eForms v6 o posterior, es irrelevante el valor de esta configuración, se desactivará durante el proceso de grabado
$gbEnableIncrementalAggregations = false;
global $gbTestIncrementalAggregationsData;
//Colocando esta variable en true, se forzará a procesar el grabado como si fuera a usar agregaciones incrementales (aún si no existen agregaciones)
//pero no se invocará al método. Esto se debe usar en conjunto con DebugPrint para permitir analizar los datos previos a este proceso de agregaciones
//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
//Si la forma a grabar fue definida con eForms v6 o posterior, es irrelevante el valor de esta configuración, se desactivará durante el proceso de grabado
$gbTestIncrementalAggregationsData = false;
global $arrDefDimValues;
$arrDefDimValues = array();
//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
//Activar esta variable limitará la funcionalidad para ocultar opciones no aplicables a los proyectos como Vigilante o GeoControl, los cuales
//se basan sólo en captura con dispositivos y tienes páginas de inicio o reportes personalizados entre otras cosas
global $gbIsGeoControl;
$gbIsGeoControl = false;
//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
global $strAlternativeLogPath;
$strAlternativeLogPath = '';
//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Ahora se generarán los logs en una carpeta alternativa de logs fuera del web service, sin embargo este valor se debe especificar por servicio, así que NO estará en este archivo sino
//dentro de configSettings.php, aquí solo quedará como referencia la manera en que se debería armar par especificar el path alternativo con la sintaxis: e:\logsbitam\artus\gen9_test\eSurveyV602_test\OtrasRutasAdicionales
//Descomentar el siguiente código en configSettings.php si se quiere variar el path de grabado de los logs
/*$strAlternativeLogPathBase = (string) @pathinfo((string) @$_SERVER["SCRIPT_NAME"])['dirname'];
if ( ($intAltRootPos = stripos($strAlternativeLogPathBase, "wwwroot\\")) !== false ) {
	$strAlternativeLogPathBase = substr($strAlternativeLogPathBase, $intAltRootPos + 7);
}
$strAlternativeLogPath = strtolower("E:\\LogsBITAM".str_replace('/', "\\", $strAlternativeLogPathBase));*/
global $strLogExtraPath;
$strLogExtraPath = date("Ymd");
//@JAPR

$generateLog=false;
//@JAPR 2013-07-17: Agregado el archivo log de procesamiento de acciones
//Activar esta variable si se desea grabar el archivo log. El nombre del archivo será el mismo siempre (eBavelActions.log), pero se puede variar modificando el código
//justo después de la conexión para asignar la variable $strDefaulteBavelLogStringFile con el nombre que se desee (anexando el repositorio por ejemplo)
//No dejar activada esta variable permanentemente porque generará un log demasiado grande cada vez que se haga una captura
$blnSaveeBavelLogStringFile = true;
//@JAPR 2014-08-25: Agregado el log de requests
$blnSaveRequestsLog = false;
//@JAPR

//Variables para el manejo de las librerias de Artus Web
global $SAAS, $ModelSincronize, $PHPSincronize, $gVersionGlobal, $sModelSQL, $bSimulatedBitamMode;
$SAAS = true;
$bSimulatedBitamMode = true;
     
$ModelSincronize = true;
$PHPSincronize = true;
$gVersionGlobal = '6.00130';
$sModelSQL = '';

$gsURLWEBHTML = '';

$KPAMailTesting = array (
    0 => 'internal_esurvey@bitam.com'
);

//@JAPR 2016-06-13: Agregada la configuración para limitar los resultados de los valores de catálogo en modo de diseño exclusivamente (#14KN4H)
define('DEFAULT_CATALOGVALUES_LIMIT', 100);

//@JAPR 2011-06-21: Agregado el soporte para el catálogo de dimensiones con captura de valores
define('DEFAULT_MAX_DIM_RECORDS', 100);			//Cantidad default de valores de dimensiones a mostrar en la sección de valores.
global $maxDimensionRecords;					//Cantidad máxima de valores de dimensiones a mostrar en la sección de valores. Si se intenta grabar como 0 (infinito) realmente no lo graba, sólo aplica durante la ejecución en memoria y al reiniciar se tomaría el valor realmente grabado (es un candado para evitar que se cicle)
if(isset($_SESSION["PAMaxDimRecords"]))
{
	$maxDimensionRecords = $_SESSION["PAMaxDimRecords"];
}
else
{
	$maxDimensionRecords = DEFAULT_MAX_DIM_RECORDS;
}

global $dimValuesFilter;
if(isset($_SESSION["DefDimValuesFilter"]))
{
	$dimValuesFilter = $_SESSION["DefDimValuesFilter"];
}
else
{
	$dimValuesFilter = '';
}

global $bShowNullParentValues;
if(isset($_SESSION["ShowNullParentValues"]))
{
	$bShowNullParentValues = $_SESSION["ShowNullParentValues"];
}
else
{
	$bShowNullParentValues = false;
}

global $appSettings;							//Variable de configuración de opciones habilitadas
$appSettings = array();
$appSettings["Cubes"] = 0;
$appSettings["Dimensions"] = 1;
$appSettings["DimensionFields"] = 0;
$appSettings["DimensionAttributes"] = 1;
$appSettings["DimensionValuesCols"] = array("Table" => 0, "Key" => 0, "Desc" => 1);
$appSettings["UseGlobalDimensions"] = 1;

$newExcelMethod = true;
//@JAPR

//Default 	(width: 700px)
//iPod 		(width: 320px)
//iPad		(width: 768px)
//iPadMini	(width: 768px)
//iPhone	(width: 640px) 
//Cel		(width: 640px)
//Tablet	(width: 800px)
$widthByDisp = array();
$widthByDisp["Default"]="1440px";
$widthByDisp["iPod"]="320px";
$widthByDisp["iPad"]="768px";
$widthByDisp["iPadMini"]="768px";
$widthByDisp["iPhone"]="320px";
$widthByDisp["Cel"]="360px";//"640px";
$widthByDisp["Tablet"]="600px";//"800px";

$heightByDisp = array();
$heightByDisp["Default"]="900px";
$heightByDisp["iPod"]="480px";
$heightByDisp["iPad"]="1024px";
$heightByDisp["iPadMini"]="1024px";
$heightByDisp["iPhone"]="568px";
$heightByDisp["Cel"]="640px";//"640px";
$heightByDisp["Tablet"]="1024px";//"800px";

$widthContainerByDisp = array();
$widthContainerByDisp["Default"]="1440";
$widthContainerByDisp["iPod"]="440";
$widthContainerByDisp["iPad"]="1050";
$widthContainerByDisp["iPadMini"]="768";
$widthContainerByDisp["iPhone"]="430";
$widthContainerByDisp["Cel"]="490";//"640px";
$widthContainerByDisp["Tablet"]="803";//"800px";

$heightContainerByDisp = array();
$heightContainerByDisp["Default"]="900";
$heightContainerByDisp["iPod"]="700";
$heightContainerByDisp["iPad"]="1300";
$heightContainerByDisp["iPadMini"]="1024";
$heightContainerByDisp["iPhone"]="805";
$heightContainerByDisp["Cel"]="880";//"640px";
$heightContainerByDisp["Tablet"]="1200";//"800px";

//Marzo 2015
global $gbDesignMode;
$gbDesignMode = false;

//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
global $garrReservedDimNames;
$garrReservedDimNames = array('agenda' => 1, 'date' => 1, 'email' => 1, 'end date' => 1, 'end time' => 1, 'latitude' => 1, 'longitude' => 1, 
	'scheduler' => 1, 'server start date' => 1, 'server start time' => 1, 'server end date' => 1, 'server end time' => 1, 'start date' => 1, 
	'start time' => 1, 'status survey' => 1, 'sync date' => 1, 'sync time' => 1, 'user' => 1);

//@JAPR 2015-10-05: Modificada la redirección en caso de pérdida de sesión para que dependa del servicio donde se encuentra (originalmente usado por GeoControl)
global $loginKpiOnline;
//@JAPR 2017-06-09: Modificada la URL de redirección cuando se pierde la sesión
$loginKpiOnline = "https://www.bitam.com/";

//@JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
global $gbGettingDynamicValues;
$gbGettingDynamicValues = false;

//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
define('tsstGlobal', -1);
define('tsstObject', 0);

//@JAPR 2016-05-21: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
define('imgptHTML', 0);					//Indica que la imagen puede venir en cualquier patrón HTML, pero sigue siendo un tag <img>
define('imgpPicture', 1);				//Indica que la imagen es una propiedad directa, sólo viene una y es simplemente una ruta
define('imgpCSSStyle', 2);				//Indica que la imagen puede venir en cualquier patrón CSS, en este caso se trata de un url de un css style

//@JAPR 2016-03-10: Redefinidas las constantes de los tipos de elementos para secciones y preguntas
define('selemPage', 1);
define('selemHdrBar', 2);
define('selemHdrLab', 3);
define('selemBdy', 4);
define('selemFtrBar', 5);
define('selemFtrLab', 6);
//@JAPR 2016-05-20: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
define('selemLeftBtn', 7);
define('selemRigthBtn', 8);
define('selemHdrTbl', 9);
define('selemLabelFirstCol', 10);
define('selemTotalTbl', 11);
define('selemSecTable', 12);
define('selemRowOdd', 13);
define('selemRowEven', 14);
define('selemSummary', 15);
define('selemSummaryFirstAns', 16);
define('selemSummaryAnswers', 17);
define('selemAddBtn', 18);
define('selemRemoveBtn', 19);
define('selemFinishBtn', 20);
define('selemGroupBtn', 21);
//@JAPR

define('qelemLabTab', 1);
define('qelemLabTR', 2);
define('qelemLabTD', 3);
define('qelemLabCont', 4);
define('qelemLabElem', 5);
define('qelemQtnTab', 6);
define('qelemQtnTR', 7);
define('qelemQtnTD', 8);
define('qelemQtnCont', 9);
define('qelemQtnElem', 10);
define('qelemPicCont', 11);
define('qelemCmtCont', 12);
define('qelemCmtLab', 13);
define('qelemCmtElem', 14);
define('qelemArea', 15);
define('qelemQtnOpSelec', 16);
define('qelemQtnInput', 17);
define('qelemQtnMetroMainAttr', 18);
define('qelemQtnMetroExtraAttr', 19);


//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
define('ordbAsc', 0);		//ORDER BY ASC (default)
define('ordbDsc', 1);		//ORDER BY DESC

//GCRUZ 2016-04-20. Opciones para configuración de menú de 3 rayas en app
//OMMC 2016-09-27: Agregada la opción de bitácora de mensajes push
global $gbarrAppMenuOptions;
//@JAPR 2017-01-06: Removida la opción de Update Definitions (#EJTH2E)
$gbarrAppMenuOptions = array(1=>'My Schedule', 2=>'Forms', 3=>'Drafts', 4=>'Outbox', /*5=>'Update Definitions',*/ 6=>'Advanced Settings', 7=>'Login Page'/*, 8=>'Save Draft'*/, 9=>'About', 10=>'Push Notifications');
//GCRUZ 2016-05-04. Tipos de destinos para PushMsg
define('pushMsgUser', 1);
define('pushMsgGroup', 2);
//GCRUZ 2016-06-16. Geofence model version
//GCRUZ 2018-05-21. Agregar dimensiones:DeviceModel, DeviceModelVersion, DeviceManufacturer, BatteryLevel, PlugStatus (GeoFence Model Version 1.0001, antes 1.0000)
define('esvGeoFenceModel', '1.0000');
define('esvGeoFenceModel_1_0001', '1.0001');

//OMMC 2016-09-29: Agregados los status de las notificaciones push
define('pushMsgPending', 0);
define('pushMsgSent', 1);
define('pushMsgError', 2);
define('pushMsgReprocess', 3);
define('pushMsgRead', 4);
define('pushMsgDeleted', 5);

//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
define('apptTest', 0);					//Indica dentro de SI_SV_UserDevices que el dispositivo registrado corresponde a una App de liga
define('apptStore', 1);					//Indica dentro de SI_SV_UserDevices que el dispositivo registrado corresponde a una App de tienda
define('apptDev', 2);					//Indica dentro de SI_SV_UserDevices que el dispositivo registrado corresponde a una App de depuración (usando XCode)

//Feb 2017: Agregadas constantes para manejo de tipo de forma (BITAMSurvey->FormType)
define('formProduction', 0);			//Indica que es una forma de producción, el común de las formas son de este tipo, ya que el esquema de desarrollo-producción es bajo demanda
define('formDevelopment', 1);			//Indica que es una forma de desarrollo, esto es, contiene apuntadores hacia la forma original para todos los objetos para permitir su sincronización automática al modificar el diseño de la misma
define('formProdWithDev', 2);			//Indica que es una forma de producción que tiene ambiente (forma) de desarrollo asociada, esta forma NO puede ser editada en producción

//Feb 2017: Agregar constantes para identificar si la forma se crea manualmente, si se creo por una copia o si se generó como desarrollo (BITAMSurvey->ActionType)
define('createAction', 0);				//Indica que la acción del método save de las clases principales del diseño de una forma servirá sólo para crear nuevos objetos (es el comportamiento default durante el diseño)
define('copyAction', 1);				//Indica que la acción del método save de las clases principales del diseño de una forma está ocurriendo para crear un clon de otra forma (botón de "Copiar" de una forma en el diseño)
define('devAction', 2);					//Indica que la acción del método save de las clases principales del diseño de una forma está ocurriendo para generar el ambiente de desarrollo (botón de "Generar forma de desarrollo" en el diseño)

//OMMC 2017-06-28: Agregada la key de googleMaps como constante para estandarizarse en los servicios así como en el admin.
//define('googleAPIKey', '&client=gme-bitamdemexicosade&channel=kpiforms');
//@JAPR 2018-08-17: Modificada la llave de Google Maps API para identificar al usuario que está realizando las llamadas mediante el "channel"
//@JAPR 2018-10-08: Cambiada la llave de GoogleMaps debido a sobrecosto de créditos del uso de la llave anterior en lo que se migra a Mapkit
//@JAPR 2018-12-17: Se determinó que el uso de Geocoding de Google Maps desde el server no era el problema, así que se regresa esta funcionalidad (#09FEOR)
//Modificada la llave temporal para agregar el soporte de Geocoding, ya que la llave temporal anterior impedía traducir las posiciones GPS en direcciones
//@JAPR 2019-01-18: Modificadas nuevamente las llaves, la última que se había asignado era solo para Geocoding así que los mapas dejaron de funcionar, ahora se generará una llave exclusiva
//para Geocoding (solo servidor) mientras que las otras dos serán para mapas (Servidor y App de captura) (#J1MXYY)
define('googleAPIKey', '&key=AIzaSyCqe3KfbvJJHzioR8x0xbwG9GZfPGWZjHs&');    //client=gme-bitamdemexicosade&channel=kpiforms
//@JAPR 2018-03-02: Corregido un bug, faltaba el API Key de google en las llamadas para usar el API, así que excedía el límite de peticiones (#KRZ34J)
//define('googleServerAPIKey', '&key=AIzaSyCZoYMCYivZIbkxHidTLX4gjL2i_u_KK4w');
//@JAPR 2018-10-08: Cambiada la llave de GoogleMaps debido a sobrecosto de créditos del uso de la llave anterior en lo que se migra a Mapkit
define('googleServerAPIKey', '&key=AIzaSyCqe3KfbvJJHzioR8x0xbwG9GZfPGWZjHs&');    //key=AIzaSyCZoYMCYivZIbkxHidTLX4gjL2i_u_KK4w (1), AIzaSyCqe3KfbvJJHzioR8x0xbwG9GZfPGWZjHs (2)
//@JAPR 2019-01-18: Modificadas nuevamente las llaves, la última que se había asignado era solo para Geocoding así que los mapas dejaron de funcionar, ahora se generará una llave exclusiva
//para Geocoding (solo servidor) mientras que las otras dos serán para mapas (Servidor y App de captura) (#J1MXYY)
define('googleServerGeocodingAPIKey', '&key=AIzaSyAKOUtNhmh7s81AesWBih64Tu4uX_FP2N8&');
//OMMC 2018-03-27: Agregada variable de passphrase para los .PEM de los certificados push de iOS, en push para apps de liga no es importante esto pero para la tienda se tiene que hacer una autenticación
define('PEMPassphrase', 'b1t4m1nc');

//@JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
define('maptGoogle', 0);				//Mapas de Google Maps (hasta 6.02027 por default)
define('maptApple', 1);					//Mapas de Apple Mapkit (6.02028 en adelante)

//OMMC 2018-12-17: Proceso de obtención de coordenadas para el administrador usando mapkit y phantom
define('mapkitTimeout', 15000);
//OMMC 2019-04-12: Cambiada a que sea estática en el control de versión del código, cada servicio deberá tener un valor distinto el cual deberá estar asignado en configSettings.php
global $gbKPIFormsService;
$gbKPIFormsService = "https://kpionline10.bitam.com/artus/gen9_test/ESurveyV602_test/";
define('mapkitSleepLength', 1);
define('mapkitWaitLength', 15);

//OMMC 2019-04-29: Agregada configuración del radio para cálculo de coordenadas #QEGRGA
define('defaultMapRadio', 0.01);

//Requerido para hacer los ajustes a estas configuraciones por servicio de eForms, ya que estos sólo son los defaults originales
require_once("configSettings.php");
?>