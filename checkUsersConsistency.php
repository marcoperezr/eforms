<?php
//@JAPR 2012-10-24: Agregados headers para permitir que las peticiones por XML (proceso de sincronización desde Browser) se completen correctamente
header("Access-Control-Allow-Origin: *");
//@JAPR 2012-12-13: Agregados los headers X-FileName y Content-Type para soportar upload crossDomain de fotos y documentos
header("Access-Control-Allow-Headers: X-Requested-With, X-File-Name, Content-Type, User-Agent, Referer, Origin");
header("Access-Control-Allow-Methods: GET, POST");

$blnShowQueries = !getParamValue('HideQueries', 'both', '(bool)');
$blnOnlyErrors = getParamValue('OnlyErrors', 'both', '(bool)');
if (!function_exists('BITAMHasInvalidChar'))
{
	function BITAMHasInvalidChar($aString)
	{
		$STRVALID = utf8_decode("!¡#$%&'()*+,-./0123456789:;<=>?¿@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]_abcdefghijklmnopqrstuvwxyz");
		$n = strlen($aString);
		for ($i = 0; $i < $n; $i++)
		{
			if (strpos($STRVALID, $aString[$i]) === false)
			{
				return $i;
			}
		}
		return false;
	}
}

if (!function_exists('LiberaInValidChars'))
{
	function LiberaInValidChars($aString, &$aPassPos, &$aPassChar)
	{
	    while (BITAMHasInvalidChar($aString) !==false)
	    {
	        $iPos  = BITAMHasInvalidChar($aString);
	        $sChar = substr($aString, $iPos, 1);

	        $aPassPos[]   = $iPos;
	        $aPassChar[]  = $sChar;

	        $sS1     = BITAMGetSiguiente($aString, $sChar);
	        $aString = $sS1.$aString;
	    }
	    return($aString);
	}
}

if (!function_exists('ReturnInValidChars'))
{
	function ReturnInValidChars($aString, $aPassPos, $aPassChar)
	{
	    $j = count($aPassPos);
	    for($i=0; $i<$j; $i++)
	    {
	        $iPos  = $aPassPos[$i] + $i;
	        $sChar = $aPassChar[$i];

	        $sS1  = substr($aString, 0, $iPos);
	        $sNew = $sS1.$sChar.substr($aString, $iPos);
	        $aString = $sNew;
	    }
	    return($aString);
	}
}

if (!function_exists('BITAMEncryptPassword'))
{
	function BITAMEncryptPassword($inString)
	{
		$inString = utf8_decode($inString);
		$STRVALID     = utf8_decode("!¡#$%&'()*+,-./0123456789:;<=>?¿@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]_abcdefghijklmnopqrstuvwxyz");
		$aPassPos     = array();
		$aPassChar    = array();
		$inString0rig = $inString;
		$inString     = trim($inString);
		$inString = str_replace(' ','_#ArtusSpace#_', $inString );
	    if ($inString == "")
	    {
	        return "";
	    }
		elseif (BITAMHasInvalidChar($inString)!==false)
		{   //EOAC 16Abr09 Soportar caracteres invalidos
			$inString=LiberaInValidChars($inString, $aPassPos, $aPassChar);
			if($inString=='') return($inString0rig); //Passwords con puros caracteres invalidos se regresaran =
			//return "";
		}

	    $movPos = rand(1, strlen($STRVALID));

		$outString = "";
		$n = strlen($inString);
		for ($i = 0; $i < $n; $i++)
		{
	    	$currentChar = $inString[$i];
	    	$newPos = strpos($STRVALID, $currentChar) + 1 + $movPos;
	        If ($newPos > strlen($STRVALID))
	        {
	            $newPos = $newPos - strlen($STRVALID);
	        }
	        $outString .= $STRVALID[$newPos - 1];
		}
	    if(strlen($inString)>0) $outString .= $inString[0];
		if(count($aPassChar)>0) //EOAC 16Abr09 Soportar caracteres invalidos
		{
			$outString=ReturnInValidChars($outString, $aPassPos, $aPassChar);
		}
		if(strlen($inString0rig)>2 && strpos(strtoupper($outString),  strtoupper($inString0rig))!==false)
		{
			$outString=BITAMEncryptPassword($inString0rig);
		}
	    return ($outString);
	}
}

if (!function_exists('BITAMDecryptPassword'))
{
	function BITAMDecryptPassword($inString)
	{
		$inString = utf8_decode($inString);
		$STRVALID = utf8_decode("!¡#$%&'()*+,-./0123456789:;<=>?¿@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]_abcdefghijklmnopqrstuvwxyz");
		$aPassPos = array();
		$aPassChar= array();
		$inString = trim($inString);
	    if ($inString == "")
	    {
	        return "";
	    }
		elseif (BITAMHasInvalidChar($inString)!==false)
		{   //EOAC 16Abr09 Soportar caracteres invalidos
			$inString2=LiberaInValidChars($inString, $aPassPos, $aPassChar);
			if($inString2=='') return $inString;
			$inString=$inString2;
		}

		$firstChar = $inString[0];
		$lastChar = $inString[strlen($inString) - 1];
	    $firstPos = strpos($STRVALID, $firstChar) + 1;
	    $lastPos = strpos($STRVALID, $lastChar) + 1;
	    $movPos = $lastPos - $firstPos;

		$outString = "";
		$n = strlen($inString) - 1;
	    for ($i = 0; $i < $n; $i++)
	    {
	    	$currentChar = $inString[$i];
	    	$newPos = strpos($STRVALID, $currentChar) + 1 + $movPos;
	        if ($newPos <= 0)
	        {
	            $newPos = strlen($STRVALID) - abs($newPos);
			}
	        If ($newPos > strlen($STRVALID))
	        {
	            $newPos = $newPos - strlen($STRVALID);
	        }

			$outString .= $STRVALID[$newPos - 1];
		}
		if(count($aPassChar)>0) //EOAC 16Abr09 Soportar caracteres invalidos
		{
			$outString=ReturnInValidChars($outString, $aPassPos, $aPassChar);
		}
		$outString = str_replace('_#ArtusSpace#_',' ', $outString );
		return ($outString);
	}
}

global $server;
global $server_user;
global $server_pwd;
global $masterdbname;

@require_once('../../../fbm/conns.inc.php');

$mysql_Connection = @mysql_connect($server, $server_user, $server_pwd);
if (!$mysql_Connection) {
	die('Connection error!');
}

$db_selected = @mysql_select_db($masterdbname, $mysql_Connection);
if (!$db_selected) {
	die('Error, could not select ' . $masterdbname);
}

//Obtiene los repositorios a procesar
$aSQL = "select a.KPIServer, a.ArtusPath, a.FormsPath, a.DBServer, a.DBServerUsr, a.DBServerPwd, a.userID, b.email, b.AccPassword, a.Repository, a.DataWarehouse 
	from saas_databases a, saas_users b 
	where a.userID = b.userID and a.repository like 'fbm_bmd_%'";
//************************************************************************************************************************************************
//Poner aquí los filtros necesarios
$aSQL .= " and a.FormsPath like '%eSurveyV602_test%'";
//$aSQL .= " and (a.Repository like 'fbm_bmd_0237')";
/*
$aSQL .= " and (a.Repository like 'fbm_bmd_0237' or a.Repository like 'fbm_bmd_0878')";
$aSQL .= " and b.email like '%promotoria@cuervo.com.mx%'";
*/
if ($blnShowQueries) {
	ECHOString("Query: {$aSQL}", 1, 1, "color:purple;");
}
//********************** Descomentar esta línea para revisar que los filtros sean correctos ********************** 
//die();
//************************************************************************************************************************************************
$result = @mysql_query($aSQL, $mysql_Connection);
if (!$result) {
	die('Error, retrieving repositories');
}

if (@mysql_num_rows($result) <= 0) {
	die('No repositories found with the specified filter: '.$aSQL);
}

$blnDifferentMySQLConn = false;
$mysql_Connection_DWH = null;
$intCounter = 0;
$row = @mysql_fetch_assoc($result);
do {
	//Obtener resultado de la consulta
	$intUserID = $row["userID"];
	$strUserEmail = $row["email"];
	$strEncryptedPass = $row["AccPassword"];
	$strUserPass = BITAMDecryptPassword($strEncryptedPass);

	//$intTemplateID = (int) @$row["TemplateID"];
	$strServer = (string) @$row["DBServer"];
	$strServerUsr = (string) @$row["DBServerUsr"];
	$strServerPwd = (string) @$row["DBServerPwd"];
	$strRepository = (string) @$row["Repository"];
	$strDWH = (string) @$row["DataWarehouse"];
	$strKPIServer = (string) @$row["KPIServer"];
	$strArtusPath = (string) @$row["ArtusPath"];
	$strFormsPath = (string) @$row["FormsPath"];
	
	//Realiza la conexión al BMD
	$blnConnectionDone = false;
	$blnDatabaseSelected = false;
	if ($mysql_Connection_DWH && $blnDifferentMySQLConn) {
		mysql_close($mysql_Connection_DWH);
		$mysql_Connection_DWH = null;
	}
	$blnDifferentMySQLConn = false;
	
	if (!$blnOnlyErrors) {
		ECHOString("Connecting to repository {$strRepository}, strServer == {$strServer}, strServerUsr == {$strServerUsr}", 1, 1, "color:blue;");
	}
	if (strlen($strServer) > 0) {
		//@JAPR 2015-05-04: Agregado soporte para php 5.6
		$mysql_Connection_DWH = @mysql_connect($strServer, $strServerUsr, BITAMDecryptPassword($strServerPwd));
		if ($mysql_Connection_DWH) {
			$blnDifferentMySQLConn = true;
			$blnConnectionDone = true;
			if (@mysql_select_db($strRepository, $mysql_Connection_DWH)) {
				$blnDatabaseSelected = true;
			}
		}
	}
	else {
		$mysql_Connection_DWH = $mysql_Connection;
		$blnConnectionDone = true;
	}
	
	//Varifica la integridad del repositorio consultando a los usuarios existentes
	if (!$blnConnectionDone) {
		ECHOString("Could not connect to repository {$strRepository}", 1, 1, "color:red;");
	}
	else {
		if (!$blnDatabaseSelected) {
			ECHOString("Could not select database {$strRepository}", 1, 1, "color:red;");
		}
	}
	
	$strReport = "";
	while ($blnConnectionDone && $blnDatabaseSelected) {
		if (!$blnOnlyErrors) {
			ECHOString("Verifying repository integrity for {$strRepository}", 1, 1, "color:blue;");
		}
		
		//Primero verifica la cantidad de usuarios en las 3 tablas (todos los queries son lanzados al DBM, cuando se requiere el DWH se usará el Alias correspondiente
		//SI_USUARIO
		$intSIUsuarioCount = 0;
		$sql = "select count(*) AS NumUsers 
			from {$strRepository}.SI_USUARIO A 
			WHERE A.NOM_LARGO <> 'dummy' 
			AND A.CLA_USUARIO > 0";
		if ($blnShowQueries) {
			ECHOString("Query: {$sql}", 1, 1, "color:purple;");
		}
		$aRS = @mysql_query($sql, $mysql_Connection_DWH);
		if ($aRS && @mysql_num_rows($aRS) != 0) {
			$aRow = @mysql_fetch_assoc($aRS);
			if ($aRow) {
				$intSIUsuarioCount = (int) @$aRow["NumUsers"];
			}
		}
		
		//SI_SV_Users
		$intSVUsersCount = 0;
		$sql = "select count(*) AS NumUsers 
			from {$strRepository}.SI_SV_Users A 
			WHERE A.Email <> '' 
			AND A.CLA_USUARIO > 0";
		if ($blnShowQueries) {
			ECHOString("Query: {$sql}", 1, 1, "color:purple;");
		}
		$aRS = @mysql_query($sql, $mysql_Connection_DWH);
		if ($aRS && @mysql_num_rows($aRS) != 0) {
			$aRow = @mysql_fetch_assoc($aRS);
			if ($aRow) {
				$intSVUsersCount = (int) @$aRow["NumUsers"];
			}
		}
		
		//Dimensión usuario
		$intUserDimensionID = 0;
		$sql = "select UserDimID 
			from {$strDWH}.SI_SV_GblSurveyModel";
		if ($blnShowQueries) {
			ECHOString("Query: {$sql}", 1, 1, "color:purple;");
		}
		$aRS = @mysql_query($sql, $mysql_Connection_DWH);
		if ($aRS && @mysql_num_rows($aRS) != 0) {
			$aRow = @mysql_fetch_assoc($aRS);
			if ($aRow) {
				$intUserDimensionID = (int) @$aRow["UserDimID"];
			}
		}
		
		//Verifica si hay consistencias en el número de usuarios entre las tablas
		$intUserDimCount = 0;
		if ($intUserDimensionID > 0) {
			$sql = "select count(*) AS NumUsers 
				from {$strDWH}.RIDIM_{$intUserDimensionID} A 
				WHERE A.RIDIM_{$intUserDimensionID}KEY > 0";
			if ($blnShowQueries) {
				ECHOString("Query: {$sql}", 1, 1, "color:purple;");
			}
			$aRS = @mysql_query($sql, $mysql_Connection_DWH);
			if ($aRS && @mysql_num_rows($aRS) != 0) {
				$aRow = @mysql_fetch_assoc($aRS);
				if ($aRow) {
					$intUserDimCount = (int) @$aRow["NumUsers"];
				}
			}
		}
		else {
			if (!$blnOnlyErrors) {
				$strReport .= 'ECHOString("This repository do not have a User Dimension", 1, 1, "color:black;");';
				$strReport .= 'ECHOString("'.$sql.'", 1, 1, "color:gray;");';
			}
		}
		
		//Verifica que los EMails sean distintos entre todas las cuentas
		//SI_USUARIO
		$intSIUsuarioDistCount = 0;
		$sql = "select count(distinct A.NOM_LARGO) AS NumUsers 
			from {$strRepository}.SI_USUARIO A 
			WHERE A.NOM_LARGO <> 'dummy' AND A.CLA_USUARIO > 0";
		if ($blnShowQueries) {
			ECHOString("Query: {$sql}", 1, 1, "color:purple;");
		}
		$aRS = @mysql_query($sql, $mysql_Connection_DWH);
		if ($aRS && @mysql_num_rows($aRS) != 0) {
			$aRow = @mysql_fetch_assoc($aRS);
			if ($aRow) {
				$intSIUsuarioDistCount = (int) @$aRow["NumUsers"];
			}
		}
		
		if ($intSIUsuarioCount != $intSIUsuarioDistCount) {
			$strReport .= 'ECHOString("There are '.($intSIUsuarioCount - $intSIUsuarioDistCount).' duplicated users by email in SI_USUARIO", 1, 1, "color:black;");';
			$strReport .= 'ECHOString("'.$sql.'", 1, 1, "color:gray;");';
		}
		
		//SI_SV_Users
		$intSVUsersDistCount = 0;
		$sql = "select count(distinct A.Email) AS NumUsers 
			from {$strRepository}.SI_SV_Users A 
			WHERE A.Email <> '' AND A.CLA_USUARIO > 0";
		if ($blnShowQueries) {
			ECHOString("Query: {$sql}", 1, 1, "color:purple;");
		}
		$aRS = @mysql_query($sql, $mysql_Connection_DWH);
		if ($aRS && @mysql_num_rows($aRS) != 0) {
			$aRow = @mysql_fetch_assoc($aRS);
			if ($aRow) {
				$intSVUsersDistCount = (int) @$aRow["NumUsers"];
			}
		}
		
		if ($intSIUsuarioCount != $intSVUsersDistCount) {
			$strReport .= 'ECHOString("There are '.($intSIUsuarioCount - $intSVUsersDistCount).' duplicated users by email in SI_SV_Users", 1, 1, "color:black;");';
			$strReport .= 'ECHOString("'.$sql.'", 1, 1, "color:gray;");';
		}
		
		$intUserDimDistCount = 0;
		if ($intUserDimensionID > 0) {
			$sql = "select count(distinct A.DSC_{$intUserDimensionID}) AS NumUsers 
				from {$strDWH}.RIDIM_{$intUserDimensionID} A 
				WHERE A.RIDIM_{$intUserDimensionID}KEY > 0";
			if ($blnShowQueries) {
				ECHOString("Query: {$sql}", 1, 1, "color:purple;");
			}
			$aRS = @mysql_query($sql, $mysql_Connection_DWH);
			if ($aRS && @mysql_num_rows($aRS) != 0) {
				$aRow = @mysql_fetch_assoc($aRS);
				if ($aRow) {
					$intUserDimDistCount = (int) @$aRow["NumUsers"];
				}
			}
			
			if ($intSIUsuarioCount != $intUserDimDistCount) {
				$strReport .= 'ECHOString("There are '.($intSIUsuarioCount - $intUserDimDistCount).' duplicated users by email in the User Dimension RIDIM_'.$intUserDimensionID.'", 1, 1, "color:black;");';
				$strReport .= 'ECHOString("'.$sql.'", 1, 1, "color:gray;");';
			}
		}
		
		//Compara la cantidad de usuarios existentes
		if ($intSIUsuarioCount != $intSVUsersCount) {
			if ($intSIUsuarioCount > $intSVUsersCount) {
				$strReport .= 'ECHOString("SI_USUARIO has '.($intSIUsuarioCount - $intSVUsersCount).' more users than SI_SV_Users", 1, 1, "color:black;");';
			}
			else {
				$strReport .= 'ECHOString("SI_SV_Users has '.($intSVUsersCount - $intSIUsuarioCount).' more users than SI_USUARIO", 1, 1, "color:black;");';
			}
		}
		
		if ($intUserDimensionID > 0) {
			if ($intSIUsuarioCount != $intUserDimCount) {
				if ($intSIUsuarioCount > $intUserDimCount) {
					$strReport .= 'ECHOString("SI_USUARIO has '.($intSIUsuarioCount - $intUserDimCount).' more users than the User Dimension RIDIM_'.$intUserDimensionID.'", 1, 1, "color:black;");';
				}
				else {
					$strReport .= 'ECHOString("The User Dimension RIDIM_'.$intUserDimensionID.' has '.($intUserDimCount - $intSIUsuarioCount).' more users than SI_USUARIO", 1, 1, "color:black;");';
				}
			}
		}
		
		//Verifica si hay consistencia entre los IDs de los usuarios en todas las tablas
		//Join SI_USUARIO - SI_SV_Users
		$intUsersJoinCount = 0;
		$sql = "select count(*) AS NumUsers 
			FROM {$strRepository}.SI_USUARIO A 
				LEFT JOIN {$strRepository}.SI_SV_Users B ON A.CLA_USUARIO = B.CLA_USUARIO 
			WHERE A.NOM_LARGO <> 'dummy' AND A.CLA_USUARIO > 0 AND B.UserID > 0";
		if ($blnShowQueries) {
			ECHOString("Query: {$sql}", 1, 1, "color:purple;");
		}
		$aRS = @mysql_query($sql, $mysql_Connection_DWH);
		if ($aRS && @mysql_num_rows($aRS) != 0) {
			$aRow = @mysql_fetch_assoc($aRS);
			if ($aRow) {
				$intUsersJoinCount = (int) @$aRow["NumUsers"];
			}
		}
		
		if ($intSIUsuarioCount != $intUsersJoinCount) {
			$strReport .= 'ECHOString("SI_USUARIO and SI_SV_Users IDs do not match in '.($intSIUsuarioCount - $intUsersJoinCount).' users", 1, 1, "color:black;");';
			$strReport .= 'ECHOString("'.$sql.'", 1, 1, "color:gray;");';
		}
		
		//Join SI_USUARIO - RIDIM_####
		$intUsersDimJoinCount = 0;
		if ($intUserDimensionID > 0) {
			$sql = "select count(*) AS NumUsers 
				FROM {$strRepository}.SI_USUARIO A 
					LEFT JOIN {$strDWH}.RIDIM_{$intUserDimensionID} B ON A.CLA_USUARIO = B.RIDIM_{$intUserDimensionID}KEY 
				WHERE A.NOM_LARGO <> 'dummy' AND A.CLA_USUARIO > 0 AND B.RIDIM_{$intUserDimensionID}KEY > 0";
			if ($blnShowQueries) {
				ECHOString("Query: {$sql}", 1, 1, "color:purple;");
			}
			$aRS = @mysql_query($sql, $mysql_Connection_DWH);
			if ($aRS && @mysql_num_rows($aRS) != 0) {
				$aRow = @mysql_fetch_assoc($aRS);
				if ($aRow) {
					$intUsersDimJoinCount = (int) @$aRow["NumUsers"];
				}
			}
			
			if ($intSIUsuarioCount != $intUsersDimJoinCount) {
				$strReport .= 'ECHOString("SI_USUARIO and RIDIM_ IDs do not match in '.($intSIUsuarioCount - $intUsersDimJoinCount).' users", 1, 1, "color:black;");';
				$strReport .= 'ECHOString("'.$sql.'", 1, 1, "color:gray;");';
			}
		}
		
		//Verifica si hay consistencia entre los EMails de los usuarios en todas las tablas
		//Join SI_USUARIO - SI_SV_Users
		$intUsersJoinEMailCount = 0;
		$sql = "select count(*) AS NumUsers 
			FROM {$strRepository}.SI_USUARIO A 
				LEFT JOIN {$strRepository}.SI_SV_Users B ON A.CLA_USUARIO = B.CLA_USUARIO AND A.CUENTA_CORREO = B.EMail 
			WHERE A.NOM_LARGO <> 'dummy' AND A.CLA_USUARIO > 0 AND B.UserID > 0";
		if ($blnShowQueries) {
			ECHOString("Query: {$sql}", 1, 1, "color:purple;");
		}
		$aRS = @mysql_query($sql, $mysql_Connection_DWH);
		if ($aRS && @mysql_num_rows($aRS) != 0) {
			$aRow = @mysql_fetch_assoc($aRS);
			if ($aRow) {
				$intUsersJoinEMailCount = (int) @$aRow["NumUsers"];
			}
		}
		
		if ($intSIUsuarioCount != $intUsersJoinEMailCount) {
			$strReport .= 'ECHOString("SI_USUARIO and SI_SV_Users emails do not match in '.($intSIUsuarioCount - $intUsersJoinEMailCount).' users", 1, 1, "color:black;");';
			$strReport .= 'ECHOString("'.$sql.'", 1, 1, "color:gray;");';
		}
		
		//Join SI_USUARIO - RIDIM_####
		$intUsersDimEMailJoinCount = 0;
		if ($intUserDimensionID > 0) {
			$sql = "select count(*) AS NumUsers 
				FROM {$strRepository}.SI_USUARIO A 
					LEFT JOIN {$strDWH}.RIDIM_{$intUserDimensionID} B ON A.CLA_USUARIO = B.RIDIM_{$intUserDimensionID}KEY AND A.CUENTA_CORREO = B.DSC_{$intUserDimensionID} 
				WHERE A.NOM_LARGO <> 'dummy' AND A.CLA_USUARIO > 0 AND B.RIDIM_{$intUserDimensionID}KEY > 0";
			if ($blnShowQueries) {
				ECHOString("Query: {$sql}", 1, 1, "color:purple;");
			}
			$aRS = @mysql_query($sql, $mysql_Connection_DWH);
			if ($aRS && @mysql_num_rows($aRS) != 0) {
				$aRow = @mysql_fetch_assoc($aRS);
				if ($aRow) {
					$intUsersDimEMailJoinCount = (int) @$aRow["NumUsers"];
				}
			}
			
			if ($intSIUsuarioCount != $intUsersDimEMailJoinCount) {
				$strReport .= 'ECHOString("SI_USUARIO and RIDIM_ emails do not match in '.($intSIUsuarioCount - $intUsersDimEMailJoinCount).' users", 1, 1, "color:black;");';
				$strReport .= 'ECHOString("'.$sql.'", 1, 1, "color:gray;");';
			}
		}
		
		/*
		if ($blnShowQueries) {
			ECHOString("Query: {$sql}", 1, 1, "color:purple;");
		}
		$aRS = @mysql_query($sql, $mysql_Connection_DWH);
		if ($aRS && @mysql_num_rows($aRS) != 0) {
			$aRow = @mysql_fetch_assoc($aRS);
			if ($aRow) {
				$intSettingID = (int) @$aRow["SettingID"];
			}
		}
		*/
		
		//Termina el ciclo While forzado, ya que se usó un ciclo innecesariamente para permitir salir con un break al encontrar el primer error de consistencia que impediría continuar
		//con una revisión mas profunda
		break;
	}
	
	if ($blnConnectionDone && $blnDatabaseSelected) {
		if ($strReport) {
			//En caso de error, regresa el detalle del mismo almacenado en la variable $strReport
			ECHOString("Repository integrity check failed for {$strRepository}", 1, 1, "color:red;");
			//echo($strReport);
			eval($strReport);
		}
		else {
			if (!$blnOnlyErrors) {
				ECHOString("Repository integrity ok for {$strRepository}", 1, 1, "color:green;");
			}
		}
	}
	
	$row = @mysql_fetch_assoc($result);
	/*$intCounter++;
	if ($intCounter > 10) {
		die("Failsafe");
		break;
	}*/
} while($row);

ECHOString("Repository integrity check finished", 1, 1, "color:green;");

unset($row);
unset($mysql_Connection);

/* Manda un echo a la salida formateandolo para presentación adecuada
*/
function ECHOString($sText, $iLnBefore = 1, $iLnAfter = 0, $sStyle = "") {
	$ln = "\r\n";
	if ($iLnBefore) {
		echo(nl2br(str_repeat($ln, $iLnBefore)));
	}
	if ($sStyle) {
		echo("<span style=\"".$sStyle."\">");
	}
	echo(nl2br(date("Y-m-d H:i:s")." - ".$sText));
	if ($sStyle) {
		echo("</span>");
	}
	if ($iLnAfter) {
		echo(nl2br(str_repeat($ln, $iLnAfter)));
	}
}

//@JAPR 2012-03-12: Agregado el uso del servicio como un servidor Web para cualquier tipo de aplicación
// Lee el parámetro especificado aplicando el tipo de dato indicado, ya sea del $_GET, $_POST o de ambos
/* El parámetro $bEncrypted funciona así: si se manda 'false' los parámetros se regresan tal cual, si se manda 'true' los parámetros se desencriptan antes de regresar, pero
si se manada null entonces primero se verifica si existe el parámetro 'encrypted' y dependiendo de esto activa/desactiva localmente dicha variable (los parámetros 'encrypted' y
'debug' son los únicos que jamás podrían venir encriptados) */
function getParamValue($sParamName, $sFrom = 'both', $sDataType = "(string)", $bNullIfMissing = false, $bEncrypted = null)
{
	$sValue = '';
	if (is_null($bEncrypted))
	{
		$bEncrypted = (bool) @getParamValue('encrypted', 'both','(int)', false, false);
	}
	
	if (strtolower($sFrom) == 'both')
	{
		$sValue = @getParamValue($sParamName, 'get',$sDataType, true, $bEncrypted);
		if (is_null($sValue))
		{
			$sValue = @getParamValue($sParamName, 'post',$sDataType, $bNullIfMissing, $bEncrypted);
		}
	}
	else
	{
		if (strpos(strtolower($sFrom), "get") !== false)
		{
			$sFrom = '$_GET';
		}
		else 
		{
			$sFrom = '$_POST';
		}
		
		@eval('$sValue = isset('.$sFrom.'[\''.$sParamName.'\']);');
		if (!$sValue && $bNullIfMissing)
		{
			return null;
		}
		
		if ($bEncrypted && $sParamName != 'debug' && $sParamName != 'encrypted' && $sParamName != 'encoding')
		{
			@eval('$sValue = @'.$sFrom.'[\''.$sParamName.'\'];');
			$sValue = BITAMDecode($sValue);
			@eval('$sValue = '.$sDataType.'$sValue;');
		}
		else 
		{
			@eval('$sValue = '.$sDataType.'@'.$sFrom.'[\''.$sParamName.'\'];');
		}
	}

	return $sValue;
}
?>