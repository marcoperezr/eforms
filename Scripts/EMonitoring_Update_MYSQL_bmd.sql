﻿ALTER TABLE SI_DESCRIP_ENC ADD IS_CATALOG INT NULL;

ALTER TABLE SI_SV_Users ADD SendPDFByEmail INT NULL;

UPDATE SI_SV_Users SET SendPDFByEmail = 1 WHERE SendPDFByEmail IS NULL;

ALTER TABLE SI_SV_Users ADD Supervisor INT NULL;

CREATE TABLE si_sv_artusreporter 
(
  schedulerID integer,
  tipo_objeto integer,
  cla_objeto integer,
  cla_dimension integer,
  cla_user integer,
  id integer unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
);

INSERT INTO SI_CONFIGURA (CLA_CONFIGURA, CLA_USUARIO, NOM_CONFIGURA, REF_CONFIGURA)
VALUES (615, -2, 'Advisor: Web HTML URL', 'https://kpionline5.bitam.com/artus/genvi_test/');

INSERT INTO SI_CONFIGURA (CLA_CONFIGURA, CLA_USUARIO, NOM_CONFIGURA, REF_CONFIGURA)
VALUES (616, -2, 'Advisor: Web HTML Project', DATABASE());

ALTER TABLE SI_SV_Users ADD AltEmail varchar(255);

-- Cambios 2014-05-20 v5.00006: Carga de una encuesta default
ALTER TABLE SI_SV_Users ADD DefaultSurvey INTEGER NULL;

UPDATE SI_SV_Users SET DefaultSurvey = 0 WHERE DefaultSurvey IS NULL;

-- Cambios 2014-06-17 v5.00011: Agregada la personalizacion de la aplicacion
ALTER TABLE SI_SV_Users ADD TemplateID INTEGER NULL;

UPDATE SI_SV_Users SET TemplateID = -1 WHERE TemplateID IS NULL;

-- Cambios 2014-07-09 v4.04033: Permisos de acceso por perfil de usuario
ALTER TABLE SI_SV_Users ADD ProfileID INTEGER NULL;
 
UPDATE SI_SV_Users SET ProfileID = 0 WHERE ProfileID IS NULL;

ALTER TABLE SI_USUARIO ADD CUENTA_CORREO_ALT VARCHAR(255) NULL;

UPDATE SI_USUARIO SET CUENTA_CORREO_ALT = '' WHERE CUENTA_CORREO_ALT IS NULL;

ALTER TABLE SI_ROL ADD DefaultSurvey INTEGER DEFAULT 0;

ALTER TABLE SI_ROL ADD TemplateID INTEGER DEFAULT 0;

ALTER TABLE SI_SV_Users ADD Image VARCHAR(255);

CREATE INDEX idxtabla_field ON si_descrip_enc (NOM_TABLA, NOM_FISICO);

-- Cambios 2015-08-24 v6.00010: Agregados los campos para habilitar/deshabilitar acceso vía KPIOnline
ALTER TABLE SI_USUARIO ADD EFORMS_ADMIN INTEGER NULL;

ALTER TABLE SI_USUARIO ADD EFORMS_DESKTOP INTEGER NULL;

ALTER TABLE SI_SV_Users ADD AgendasSurveysTemplate VARCHAR(255) NULL;

ALTER TABLE SI_SV_Users ADD GeoFenceRadius INTEGER NULL;

ALTER TABLE SI_SV_Users ADD GeoFenceLat DOUBLE NULL;

ALTER TABLE SI_SV_Users ADD GeoFenceLng DOUBLE NULL;

ALTER TABLE SI_SV_Users MODIFY AgendasSurveysTemplate LONGTEXT NULL;

INSERT INTO SI_MD_VERSION (TIPO_PRODUCTO, PRODUCTO, VERSION) VALUES (6, 'eForms', 6.03000);

