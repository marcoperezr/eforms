﻿CREATE TABLE SI_SV_Survey 
(
  SurveyID integer,
  SurveyName varchar(255),
  UserIDs varchar(255),
  Status integer,
  ModelID integer,
  ClosingMsg LONGTEXT,
  AllowMultipleSurveys integer,
  CountIndID integer,
  AnsweredQuestionsIndID integer,
  DurationIndID integer,
  LatitudeIndID integer,
  LongitudeIndID integer,
  LatitudeAttribID integer,
  LongitudeAttribID integer,
  ActionIndInlineID integer,
  UserDimID integer,
  SectionDimID integer,
  EmailDimID integer,
  StatusDimID integer,
  SchedulerDimID integer,
  FactKeyDimID integer,
  CreationUserID integer,
  CreationDateID datetime,
  LastUserID integer,
  LastDateID datetime,
  CaptureStartTime varchar(64),
  CaptureEndTime varchar(64),
  HelpMsg LONGTEXT,
  CatalogID integer,
  SurveyType integer,
  HasSignature integer,
  AllowPartialSave integer,
  VersionNum integer,
  ElementQuestionID INTEGER NULL,
  eBavelAppID INTEGER NULL,
  eBavelFormID INTEGER NULL,
  StartDateDimID INTEGER NULL,
  EndDateDimID INTEGER NULL,
  StartTimeDimID INTEGER NULL,
  EndTimeDimID INTEGER NULL,
  EnableReschedule INTEGER NULL,
  RescheduleCatalogID INTEGER NULL,
  RescheduleCatMemberID INTEGER NULL,
  RescheduleStatus VARCHAR(255) NULL,
  SyncDateDimID INTEGER NULL,
  SyncTimeDimID INTEGER NULL,
  SyncWhenSaving INT NULL,
  ServerStartDateDimID INTEGER NULL,
  ServerEndDateDimID INTEGER NULL,
  ServerStartTimeDimID INTEGER NULL,
  ServerEndTimeDimID INTEGER NULL,
  UseStdSectionSingleRec INTEGER NULL,
  DissociateCatDimens INTEGER NULL,
  OtherLabel VARCHAR(50) NULL, 
  CommentLabel VARCHAR(50) NULL, 
  eBavelActionFormID INTEGER NULL,
  AccuracyAttribID INTEGER NULL,
  AccuracyIndID INTEGER NULL,
  AdditionalEMails VARCHAR(255), 
  SurveyImageText LONGTEXT,
  SurveyMenuImage LONGTEXT,
  HideNavButtons INTEGER NULL,
  HideSectionNames INTEGER NULL,
  DisableEntry INTEGER NULL,
  CreationVersion FLOAT NULL,
  LastVersion FLOAT NULL,
  IsAgendable INTEGER NULL,
  RadioForAgenda INTEGER NULL,
  SchedulerID INTEGER NULL,
  SurveyDesc VARCHAR(255) NULL,
  incrementalFile VARCHAR(255) NULL,
  Width INTEGER NULL,
  Height INTEGER NULL,
  TextPosition INTEGER NULL,
  TextSize VARCHAR(10) NULL,
  CustomLayout LONGTEXT NULL,
  ShowQNumbers INTEGER NULL,
  TemplateID INTEGER NULL,
  PlannedDuration FLOAT NULL,
  EnableNavHistory INTEGER NULL,
  AutoFitImage INTEGER NULL,
  AllMobile INTEGER NULL,
  SourceID INTEGER NULL,
  FormType INTEGER NULL,
  QuestionIDForEntryDesc INTEGER NULL,
  ReloadSurveyFromSection INTEGER NULL,
  PRIMARY KEY (SurveyID)
);

CREATE TABLE SI_SV_SurveyUser 
(
  SurveyID integer,
  UserID integer,
  PRIMARY KEY (SurveyID,UserID)
);

CREATE TABLE SI_SV_SurveyRol 
(
  SurveyID integer,
  RolID integer,
  PRIMARY KEY (SurveyID,RolID)
);

CREATE TABLE SI_SV_Scheduler 
(
  SurveyID integer,
  CaptureStartDate datetime,
  CaptureEndDate datetime,
  CaptureType integer,
  WhenToCapture varchar(255),
  StartDay integer,
  Duration integer,
  PRIMARY KEY (SurveyID)
);

CREATE TABLE SI_SV_SurveyFilter
(
  FilterID integer,
  SurveyID integer,
  FilterName varchar(255),
  CatalogID integer,
  CatDimID integer,
  FilterText LONGTEXT,
  FilterLevels varchar(255),
  CreationUserID INTEGER NULL,
  CreationDateID DATETIME NULL,
  LastUserID INTEGER NULL,
  LastDateID DATETIME NULL,
  CreationVersion FLOAT NULL,
  LastVersion FLOAT NULL,
  PRIMARY KEY (FilterID)
);

CREATE TABLE SI_SV_Section 
(
  SurveyID integer,
  SectionID bigint,
  SectionName varchar(255),
  SectionNumber integer,
  SectionDimValID integer,
  SectionType integer,
  CatalogID integer,
  CatMemberID integer,
  FormattedText LONGTEXT,
  SendThumbnails integer,
  EnableHyperlinks integer,
  NextSectionID integer,
  AutoRedraw integer NULL,
  ShowCondition LONGTEXT NULL,
  SelectorQuestionID INTEGER NULL,
  DisplayMode INTEGER NULL,
  SwitchBehaviour INTEGER NULL,
  HTMLHeader LONGTEXT NULL,
  HTMLFooter LONGTEXT NULL,
  OptionsText LONGTEXT NULL,
  SectionFormID INTEGER NULL,
  SummaryInMDSection INTEGER NULL,
  ShowInNavMenu INTEGER NULL,
  CreationUserID INTEGER NULL,
  CreationDateID DATETIME NULL,
  LastUserID INTEGER NULL,
  LastDateID DATETIME NULL,
  CreationVersion FLOAT NULL,
  LastVersion FLOAT NULL,
  ShowSelector INTEGER NULL,
  ShowAsQuestion INTEGER NULL,
  SourceQuestionID INTEGER NULL,
  SourceSectionID INTEGER NULL,
  EditorType INTEGER,
  FormattedTextDes LONGTEXT,
  HTMLHeaderDes LONGTEXT,
  HTMLFooterDes LONGTEXT,
  MinRecordsToSelect INTEGER NULL,
  SectionNameHTML LONGTEXT NULL,
  ValuesSourceType INTEGER NULL,
  DataSourceID INTEGER NULL,
  DataSourceMemberID INTEGER NULL,
  DataSourceFilter LONGTEXT NULL,
  HideNavButtons INTEGER NULL,
  HTMLHEditorState INTEGER DEFAULT 0,
  HTMLFEditorState INTEGER DEFAULT 0,
  SourceID INTEGER NULL,
  UseDescriptorRow INTEGER NULL,
  XPosQuestionID INTEGER NULL,
  YPosQuestionID INTEGER NULL,
  ItemNameQuestionID INTEGER NULL,
  PRIMARY KEY (SectionID)
);

CREATE INDEX idxCatalogID ON SI_SV_Section(CatalogID);

CREATE TABLE SI_SV_Question 
(
  SurveyID integer,
  SectionID bigint,
  QuestionID bigint,
  QuestionNumber integer,
  QuestionText varchar(255),
  QTypeID integer,
  IsMultiDimension integer,
  UseCatToFillMC integer,
  QComment varchar(255),
  QActions LONGTEXT,
  HasValidation integer,
  MinValue varchar(64),
  `MaxValue` varchar(64),
  MinDate varchar(10),
  MaxDate varchar(10),
  OptionsText LONGTEXT,
  IndDimID integer,
  ImgDimID integer,
  AttributeName varchar(255),
  IsFieldSurveyID integer,
  IsIndicator integer,
  IsIndicatorMC integer,
  IndicatorID integer,
  IsReqQuestion integer,
  HasReqComment integer,
  HasReqPhoto integer,
  HasActions integer,
  MCInputType integer,
  UseCatalog integer,
  CatalogID integer,
  CatMemberID integer,
  QDisplayMode integer,
  OptionsTextCol LONGTEXT,
  QLength integer,
  FatherQuestionID integer,
  ChildQuestionID integer,
  OutsideOfSection integer,
  GoToSection integer,
  OneChoicePer integer,
  TextDisplayStyle integer,
  UseCategoryDim integer,
  CategoryDimName varchar(255),
  ElementDimName varchar(255),
  ElementIndName varchar(255),
  CategoryDimID integer,
  ElementDimID integer,
  ElementIndID integer,
  AllowAdditionalAnswers INTEGER NULL,
  QComponentStyleID INTEGER NULL,
  ResponsibleID INTEGER NULL, 
  CategoryID INTEGER NULL, 
  DaysDueDate INTEGER NULL,
  DecimalPlaces INTEGER NULL, 
  SourceQuestionID INTEGER NULL,
  FormatMask VARCHAR(30) NULL,
  ActionTitle LONGTEXT NULL,
  ActionText LONGTEXT NULL,
  DefaultValue LONGTEXT NULL,
  HasQuestionDep INTEGER NULL,
  QuestionImageText LONGTEXT,
  DateFormatMask VARCHAR(30),
  QuestionMessage LONGTEXT NULL,
  ExcludeSelected INTEGER NULL,
  SourceSimpleQuestionID INTEGER NULL,
  SummaryTotal INTEGER NULL,
  MaxChecked INTEGER NULL,
  ReadOnly INT NULL,
  Formula LONGTEXT NULL,
  HasReqDocument INT NULL,
  DocDimID integer,
  ShowCondition LONGTEXT NULL, 
  ShowImageAndText INTEGER NULL,
  eBavelFieldID INTEGER NULL,
  OtherLabel VARCHAR(50) NULL, 
  CommentLabel VARCHAR(50) NULL,
  eBavelActionFormID INTEGER NULL,
  SharedQuestionID INTEGER NULL,
  RegisterGPSAt INT NULL, 
  LatitudeAttribID INT NULL, 
  LongitudeAttribID INT NULL, 
  AccuracyAttribID INT NULL, 
  InputWidth INT NULL,
  PictLayOut INT NULL,
  UseMap INTEGER NULL,
  MapRadio INTEGER NULL,
  CatMemberLatitudeID INTEGER NULL,
  CatMemberLongitudeID INTEGER NULL,
  EnableCheckBox INTEGER NULL, 
  SwitchBehaviour INTEGER NULL,
  ShowInSummary INTEGER NULL,
  CanvasHeight varchar(10) NULL,
  CanvasWidth varchar(10) NULL,
  CreationUserID INTEGER NULL,
  CreationDateID DATETIME NULL,
  LastUserID INTEGER NULL,
  LastDateID DATETIME NULL,
  CreationVersion FLOAT NULL,
  LastVersion FLOAT NULL,
  ColumnWidth VARCHAR(20) NULL,
  SourceSectionID INTEGER NULL,
  SketchImage LONGTEXT NULL,
  AutoRedraw INTEGER NULL,
  EditorType INTEGER,
  QuestionMessageDes LONGTEXT,
  IsInvisible INTEGER,
  QuestionTextHTML LONGTEXT NULL,
  ValuesSourceType INTEGER NULL,
  DataSourceID INTEGER NULL,
  DataSourceMemberID INTEGER NULL,
  DataMemberLatitudeID INTEGER NULL,
  DataMemberLongitudeID INTEGER NULL,
  DataSourceFilter LONGTEXT NULL,
  SavePhoto INTEGER NULL,
  MaxQLength INTEGER NULL,
  HTMLEditorState INTEGER DEFAULT 0,
  ShowTotals INTEGER NULL,
  TotalsFunction VARCHAR(10) NULL,
  AllowGallery INTEGER DEFAULT 0,
  CatMemberImageID INTEGER NULL,
  DataMemberImageID INTEGER NULL,
  DataDestinationID INTEGER NULL,
  NoFlow INTEGER NULL,
  CatMemberOrderID INTEGER NULL,
  DataMemberOrderID INTEGER NULL,
  DataMemberOrderDir INTEGER NULL,
  CustomLayout LONGTEXT NULL,
  ImageHeight varchar(10) NULL,
  ImageWidth varchar(10) NULL,
  SourceID INTEGER NULL,
  UploadMediaFiles INTEGER NULL,
  DataMemberZipFileID INTEGER NULL,
  CatMemberZipFileID INTEGER NULL,
  DataMemberVersionID INTEGER NULL,
  CatMemberVersionID INTEGER NULL,
  DataMemberPlatformID INTEGER NULL,
  CatMemberPlatformID INTEGER NULL,
  DataMemberTitleID INTEGER NULL,
  CatMemberTitleID INTEGER NULL,
  RecalculateDependencies INTEGER NULL,
  AutoSelectEmptyOpt INTEGER NULL,
  PRIMARY KEY (QuestionID)
);

CREATE INDEX idxCatalogID ON SI_SV_Question(CatalogID);

CREATE TABLE SI_SV_QType 
(
  QTypeID integer,
  QTypeName varchar(255),
  PRIMARY KEY (QTypeID)
);

INSERT INTO SI_SV_QType (QTypeID, QTypeName) VALUES (1,'Number');
INSERT INTO SI_SV_QType (QTypeID, QTypeName) VALUES (2,'Simple Choice');
INSERT INTO SI_SV_QType (QTypeID, QTypeName) VALUES (3,'Multiple Choice');
INSERT INTO SI_SV_QType (QTypeID, QTypeName) VALUES (4,'Date');
INSERT INTO SI_SV_QType (QTypeID, QTypeName) VALUES (5,'Textbox');
INSERT INTO SI_SV_QType (QTypeID, QTypeName) VALUES (6,'Alphanumeric');

CREATE TABLE SI_SV_QAnswers 
(
  ConsecutiveID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  QuestionID integer,
  SortOrder integer,
  DisplayText varchar(255),
  NextSection integer,
  IndDimID integer,
  IndicatorID integer,
  Score varchar(64),
  ActionTitle LONGTEXT NULL,
  ActionText VARCHAR(255) NULL,
  ResponsibleID INTEGER NULL,
  CategoryID INTEGER NULL,
  DaysDueDate INTEGER NULL,
  InsertWhen INTEGER NULL,
  DisplayImage LONGTEXT,
  phoneNumber varchar(255),
  eBavelActionFormID INTEGER NULL,
  Hidden INTEGER NULL,
  HTMLText LONGTEXT,
  DefaultValue VARCHAR(255) NULL,
  CreationUserID INTEGER NULL,
  CreationDateID DATETIME NULL,
  LastUserID INTEGER NULL,
  LastDateID DATETIME NULL,
  CreationVersion FLOAT NULL,
  LastVersion FLOAT NULL,
  CancelAgenda INTEGER NULL,
  EditorType INTEGER,
  HTMLTextDes LONGTEXT,
  ActionCondition LONGTEXT NULL,
  SourceID INTEGER NULL,
  PRIMARY KEY (ConsecutiveID)
);

CREATE TABLE SI_SV_QAnswersCol
(
  ConsecutiveID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  QuestionID integer,
  SortOrder integer,
  DisplayText varchar(255),
  IndDimID integer,
  IndicatorID integer,
  Score varchar(64),
  ScoreOperation integer,
  PRIMARY KEY (ConsecutiveID)
);

CREATE TABLE SI_SV_Catalog 
(
  CatalogID integer,
  CatalogName varchar(255),
  ParentID integer,
  TableName varchar(255),
  ModelID integer,
  VersionIndID integer,
  VersionNum INTEGER NULL,
  DissociateCatDimens INTEGER NULL,
  HierarchyID INTEGER NULL,
  eBavelAppID INTEGER NULL,
  eBavelFormID INTEGER NULL,
  eBavelFormType INTEGER NULL,
  DataSourceID INTEGER NULL,
  CreationVersion FLOAT NULL,
  LastVersion FLOAT NULL,
  DataVersionDate DateTIME NULL,
  IsModel INTEGER NULL,
  PRIMARY KEY (CatalogID)
);

CREATE INDEX idxDataSource ON SI_SV_Catalog(DataSourceID);

CREATE TABLE SI_SV_CatalogMember
(
  CatalogID integer,
  MemberID bigint,
  MemberName varchar(255),
  ParentID integer,
  MemberOrder integer,
  DescriptorID INTEGER NULL,
  IndDimID INTEGER NULL,
  eBavelFieldID INTEGER NULL,
  KeyOfAgenda INTEGER NULL,
  AgendaLatitude INTEGER NULL,
  AgendaLongitude INTEGER NULL,
  AgendaDisplay INTEGER NULL,
  eBavelFieldDataID INTEGER NULL,
  IsImage INTEGER NULL,
  DataSourceMemberID INTEGER NULL,
  CreationVersion FLOAT NULL,
  LastVersion FLOAT NULL,
 PRIMARY KEY (MemberID)
);

CREATE TABLE SI_SV_SurveyDimKeys
(
  SurveyID integer,
  DimClaDescrip integer,
  PRIMARY KEY (SurveyID, DimClaDescrip)
);

CREATE TABLE SI_SV_CatFilter
(
  FilterID integer,
  CatalogID integer,
  FilterName varchar(255),
  FilterText LONGTEXT,
  FilterLevels varchar(255),
  PRIMARY KEY (FilterID)
);

CREATE TABLE SI_SV_CatFilterUser 
(
  FilterID integer,
  UserID integer,
  PRIMARY KEY (FilterID,UserID)
);

CREATE TABLE SI_SV_CatFilterRol 
(
  FilterID integer,
  RolID integer,
  PRIMARY KEY (FilterID,RolID)
);

CREATE TABLE SI_SV_SurveyScheduler 
(
  SchedulerID integer,
  SchedulerName varchar(255),
  SurveyID integer,
  AllowMultipleSurveys integer,
  CaptureStartDate datetime,
  CaptureEndDate datetime,
  CaptureType integer,
  WhenToCapture varchar(255),
  StartDay integer,
  Duration integer,
  CaptureVia integer,
  UseCatalog integer,
  CatalogID integer,
  AttributeID integer,
  EmailSubject varchar(255),
  EmailBody LONGTEXT,  
  EmailReplyTo varchar(255),
  EmailFilter integer,
  EditorType INTEGER,
  EmailBodyDes LONGTEXT,
  PRIMARY KEY (SchedulerID)
);

CREATE TABLE SI_SV_SurveySchedulerUser 
(
  SchedulerID integer,
  UserID integer,
  PRIMARY KEY (SchedulerID,UserID)
);

CREATE TABLE SI_SV_SurveySchedulerRol 
(
  SchedulerID integer,
  RolID integer,
  PRIMARY KEY (SchedulerID,RolID)
);

CREATE TABLE SI_SV_SurveyAnswerImage 
(
  SurveyID integer,
  QuestionID integer,
  FactKey bigint,
  MainFactKey bigint,
  PathImage VARCHAR(255)
);

CREATE TABLE SI_SV_SurveyAnswerComment 
(
  SurveyID integer,
  QuestionID integer,
  FactKey bigint,
  MainFactKey bigint,
  StrComment LONGTEXT
);

CREATE TABLE SI_SV_SurveyQuestionActions 
(
  SurveyID integer,
  QuestionID integer,
  FactKey bigint,
  StrActions LONGTEXT
);

CREATE TABLE SI_SV_SurveyMap 
(
  ObjectID INTEGER NOT NULL,
  SourceID INTEGER NOT NULL,
  TargetID INTEGER NOT NULL,
  PRIMARY KEY (ObjectID, SourceID, TargetID)
);

CREATE TABLE SI_SV_SurveySignatureImg 
(
  SurveyID integer,
  FactKey bigint,
  PathImage VARCHAR(255)
);

CREATE TABLE SI_SV_GblSurveyModel
(
  GblSurveyModelID integer,
  SurveyDimID integer,
  SurveyGlobalDimID integer,
  SurveySingleRecordDimID integer,
  UserDimID integer,
  StartDateDimID integer,
  EndDateDimID integer,
  StartTimeDimID integer,
  EndTimeDimID integer,
  CountIndID integer,
  AnsweredQuestionsIndID integer,
  DurationIndID integer,
  LatitudeDimID INTEGER NULL,
  LatitudeIndID integer,
  LongitudeDimID INTEGER NULL,
  LongitudeIndID integer,
  AccuracyAttribID INTEGER NULL,
  AccuracyIndID INTEGER NULL,
  SyncDateDimID INTEGER NULL,
  SyncTimeDimID INTEGER NULL,
  ServerStartDateDimID INTEGER NULL,
  ServerEndDateDimID INTEGER NULL,
  ServerStartTimeDimID INTEGER NULL,
  ServerEndTimeDimID INTEGER NULL
);

CREATE TABLE SI_SV_SchedulerEmail
(
  ConsecutiveID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  SchedulerID integer,
  Email varchar(255),
  PRIMARY KEY (ConsecutiveID)
);

CREATE TABLE SVSurveyActions
(
  SurveyDimVal bigint,
  SurveyID integer,
  FactKey bigint,
  QuestionID integer,
  OptionText varchar(255),
  Action LONGTEXT,
  Responsible integer,
  Deadline datetime,
  Status integer
);

CREATE TABLE SI_SV_QOptionActions 
(
  ConsecutiveID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  QuestionID integer,
  OptionText varchar(255),
  SortOrder integer,
  ActionText varchar(255),
  PRIMARY KEY (ConsecutiveID)
);

-- Tabla que almacena la relacion de opciones con Ocultar/Mostrar Preguntas
CREATE TABLE SI_SV_ShowQuestions
(
  ShowID BIGINT,
  QuestionID INTEGER,
  ConsecutiveID INTEGER,
  ShowQuestionID INTEGER,
  PRIMARY KEY (ShowID)
);

-- Tabla con las definiciones de Agendas
CREATE TABLE SI_SV_SurveyAgenda
(
   AgendaID INTEGER NOT NULL,
   AgendaName VARCHAR(255) NULL,
   UserID INTEGER NOT NULL,
   CatalogID INTEGER NULL,
   AttributeID INTEGER NULL,
   CaptureStartDate DATETIME NULL,
   CaptureStartTime VARCHAR(64) NULL,
   FilterText VARCHAR(255) NULL,
   CreationUserID INTEGER NULL,
   CreationDateID DATETIME NULL,
   LastUserID INTEGER NULL,
   LastDateID DATETIME NULL,
   VersionNum INTEGER NULL,
   CheckInSurvey INTEGER NULL,
   `Status` INTEGER NULL,
   EntryCheckInStartDate DATETIME NULL,
	CreationVersion FLOAT NULL,
	LastVersion FLOAT NULL,
  `checkpointID` int(11) DEFAULT NULL,
  `FilterTextAttr` varchar(255) DEFAULT NULL,
   AgendaSchedID INTEGER NULL,
   PRIMARY KEY (AgendaID)
);

-- Tabla con las encuestas disponibles por Agenda
CREATE TABLE SI_SV_SurveyAgendaDet
(
  AgendaID INTEGER NOT NULL,
  SurveyID INTEGER NOT NULL,
  Status INTEGER NULL,
  PRIMARY KEY (AgendaID, SurveyID)
);

-- Tabla para almacenar configuraciones globales
CREATE TABLE SI_SV_Settings (
  SettingID INTEGER NOT NULL,
  SettingName VARCHAR(255),
  SettingValue LONGTEXT NULL,
  PRIMARY KEY (SettingID)
);

-- Cambios 2012-05-23: Log de procesos de las Apps móviles
CREATE TABLE SI_SV_AppLog (
  LogKey INTEGER NOT NULL AUTO_INCREMENT,
  UserID INTEGER NOT NULL,
  AgendaID INTEGER NULL,
  SurveyID INTEGER NULL,
  EventDateID DATETIME NULL,
  UploadDateID DATETIME NULL,
  GMTZone INTEGER NULL,
  Latitude DOUBLE NULL,
  Longitude DOUBLE NULL,
  DeviceName VARCHAR(255) NULL,
  PlataformName VARCHAR(255) NULL,
  OSVersion VARCHAR(255) NULL,
  AppsRunnning INTEGER NULL,
  UsedMemory INTEGER NULL,
  EventName VARCHAR(255),
  AppSessionID VARCHAR(20) NULL,
  EventID INTEGER NULL,
  EventStepID INTEGER NULL,
  ConnectionType VARCHAR(50) NULL,
  PRIMARY KEY (LogKey)
);

-- Cambios 2012-07-28: Agregado el valor default de preguntas y dependencia de auto-evaluación
CREATE TABLE SI_SV_QuestionDep
(
  AutoEvalID INT NOT NULL,
  QuestionID INTEGER NOT NULL,
  EvalQuestionID INTEGER NOT NULL,
  PRIMARY KEY (AutoEvalID)
);

CREATE TABLE SI_SV_QuestionFormulaDep
(
  AutoEvalID INT NOT NULL,
  QuestionID INTEGER NOT NULL,
  EvalQuestionID INTEGER NOT NULL,
  PRIMARY KEY (AutoEvalID)
);

CREATE TABLE SI_SV_SurveyAnswerDocument
(
  SurveyID integer,
  QuestionID integer,
  FactKey bigint,
  MainFactKey bigint,
  PathDocument VARCHAR(255)
);

CREATE TABLE SI_SV_ShowConditionDep
(
  AutoEvalID INT NOT NULL,
  ElementID INTEGER NOT NULL,
  ElementType INTEGER NOT NULL,
  EvalElementID INTEGER NOT NULL,
  PRIMARY KEY (AutoEvalID)
);

-- Version 4.04002
-- Cambios 2013-04-18: Agregada la opción para mapear las preguntas de eForms con formularios de eBavel y grabar directamente en la forma de eBavel
CREATE TABLE SI_SV_SurveyFieldsMap
(
  FieldMapID INTEGER,
  SurveyID INTEGER,
  SurveyFieldID INTEGER,
  EBavelFieldID INTEGER,
  PRIMARY KEY (FieldMapID)
);

-- Version 4.04007
-- Cambios 2013-07-01: Mapeo de acciones a eBavel a través de una forma
CREATE TABLE SI_SV_SurveyActionFieldsMap
(
  FieldMapID INTEGER NOT NULL,
  SurveyID INTEGER NOT NULL,
  SurveyFieldID INTEGER,
  EBavelFieldID INTEGER,
  PRIMARY KEY (FieldMapID)
);

-- Cambios 2013-07-03: Mapeo de acciones a eBavel a través de una forma
CREATE TABLE SI_SV_SurveyAnswerActionFieldsMap
(
  FieldMapID INTEGER NOT NULL,
  SurveyID INTEGER NOT NULL,
  QuestionID INTEGER NOT NULL,
  QuestionOptionID INTEGER NOT NULL,
  SurveyFieldID INTEGER,
  EBavelFieldID INTEGER,
  DataID INTEGER NULL,
  MemberID INTEGER NULL,
  Formula VARCHAR(255),
  PRIMARY KEY (FieldMapID)
);

-- Cambios 2013-07-09: Mapeo de acciones a eBavel a través de una forma
CREATE TABLE SI_SV_SurveyQuestionActionFieldsMap
(
  FieldMapID INTEGER NOT NULL,
  SurveyID INTEGER NOT NULL,
  QuestionID INTEGER NOT NULL,
  SurveyFieldID INTEGER,
  EBavelFieldID INTEGER,
  DataID INTEGER NULL,
  MemberID INTEGER NULL,
  Formula VARCHAR(255),
  PRIMARY KEY (FieldMapID)
);

-- Cambios 2013-12-03 v4.04021: Compartir opciones de respuesta entre preguntas
CREATE TABLE SI_SV_QAnswersMatch
(
  SourceQuestionID INTEGER NOT NULL,
  SourceConsecutiveID INTEGER NOT NULL,
  SharedQuestionID INTEGER NOT NULL,
  SharedConsecutiveID INTEGER NOT NULL,
  PRIMARY KEY (SourceQuestionID, SourceConsecutiveID, SharedQuestionID, SharedConsecutiveID)
);

-- Cambios 2014-04-01 v5.00000: Envio de mensajes Push desde el Admin de eForms
CREATE TABLE SI_SV_PushMessage (
  MessageID INTEGER NOT NULL,
  CreationDateID DATETIME NOT NULL, 
  CreationUserID INTEGER NOT NULL,
  UserID INTEGER NOT NULL,
  Title VARCHAR(100) NULL,
  PushMessage VARCHAR(500) NULL,
  State INT NOT NULL,
  PRIMARY KEY (CreationDateID, UserID)
);

CREATE TABLE SI_SV_PushMessageDevices (
  MessageID INT NOT NULL,
  DeviceID INT NOT NULL,
  PRIMARY KEY (MessageID, DeviceID)
);

CREATE TABLE SI_SV_Menu (
  MenuID INT(11) NOT NULL,
  MenuName VARCHAR(255) NULL,
  MenuImageText LONGTEXT NULL,
  VersionNum INT(11) NULL,
  Width INTEGER NULL,
  Height INTEGER NULL,
  TextPosition INTEGER NULL,
  TextSize VARCHAR(10) NULL,
  CustomLayout LONGTEXT NULL,
  AutoFitImage INTEGER NULL,
  PRIMARY KEY (MenuID)
);

CREATE TABLE SI_SV_SurveyMenu (
  MenuID INT(11) NOT NULL,
  SurveyID INT(11) NOT NULL,
  VersionNum INT(11) NOT NULL,
  PRIMARY KEY (MenuID, SurveyID)
);

-- Cambios 2014-06-17 v5.00011: Agregada la personalizacion de la aplicacion
CREATE TABLE SI_SV_AppCustomization (
  TemplateID INTEGER NOT NULL,
  UserID INTEGER NOT NULL,
  SettingID INTEGER NOT NULL,
  SettingName VARCHAR(255),
  SettingValue VARCHAR(255),
  PRIMARY KEY (TemplateID, UserID, SettingID)
);

CREATE TABLE SI_SV_AppCustomizationTpls (
  TemplateID INTEGER NOT NULL,
  TemplateName VARCHAR(255) NULL,
  PRIMARY KEY (TemplateID)
);

-- Cambios 2014-07-09 v4.04033: Permisos de acceso por perfil de usuario
CREATE TABLE SI_SV_SurveyProfile (
 ProfileID INTEGER NOT NULL,
 ProfileName VARCHAR(255) NULL,
 PRIMARY KEY (ProfileID)
);
 
CREATE TABLE SI_SV_SurveyRestriction (
 RestrictionID INTEGER NOT NULL,
 ProfileID INTEGER NOT NULL,
 SectionType INTEGER NULL,
 QuestionType INTEGER NULL,
 CatalogID INTEGER NULL,
 Description VARCHAR(255) NULL,
 PRIMARY KEY (RestrictionID)
);

-- Cambios 2014-07-30 v5.00017: Responsive Design en Secciones
CREATE TABLE SI_SV_SurveyHTML (
  SurveyID INTEGER NOT NULL,
  ObjectType INTEGER NOT NULL,
  ObjectID INTEGER NOT NULL,
  DeviceID INTEGER NOT NULL,
  FormattedText LONGTEXT NULL,
  HTMLHeader LONGTEXT NULL,
  HTMLFooter LONGTEXT NULL,
  ImageText LONGTEXT NULL,
  SketchImage LONGTEXT NULL,
  QuestionMessageDes LONGTEXT,
  HTMLTextDes LONGTEXT,
  FormattedTextDes LONGTEXT,
  HTMLHeaderDes LONGTEXT,
  HTMLFooterDes LONGTEXT,
  PRIMARY KEY (SurveyID, ObjectType, ObjectID, DeviceID)
);

-- Cambios 2014-07-30 v5.00017: Datos adicionales desde eBavel para secciones tipo Recap
CREATE TABLE SI_SV_SectionFilter
(
  FilterID integer,
  SectionID integer,
  FilterName varchar(255),
  FilterText LONGTEXT,
  FilterLevels varchar(255),
  PRIMARY KEY (FilterID)
);

-- Cambios 2014-09-30 v5.00023: Filtros de atributos para markers en preguntas simple choice con mapa
CREATE TABLE SI_SV_QuestionFilter
(
  FilterID INTEGER NOT NULL,
  QuestionID INTEGER NOT NULL,
  FilterType INTEGER NOT NULL,
  FilterName VARCHAR(255) NULL,
  FilterText LONGTEXT NULL,
  FilterLevels VARCHAR(255) NULL,
  Color VARCHAR(10) NULL, 
  PRIMARY KEY (FilterID)
);


-- Cambios 2014.12.02 v5.00032 #O5YTV8  agendas a través de planificación recurrente-----------
CREATE TABLE IF NOT EXISTS SI_SV_SurveyAgendaScheduler (
    AgendaSchedID INTEGER NOT NULL,
    AgendaSchedName VARCHAR(255) NULL,
    UserID INTEGER NOT NULL,
    CatalogID INTEGER NULL,
    AttributeID INTEGER NULL,
    FilterText VARCHAR(255) NULL,
    FilterTextAttr VARCHAR(255) NULL,
	`checkpointID` INT NULL,
    CreationUserID INTEGER NULL,
    CreationDateID DATETIME NULL,
    LastUserID INTEGER NULL,
    LastDateID DATETIME NULL,
    VersionNum INTEGER NULL,
    CheckInSurvey INTEGER NULL,
    `Status` INTEGER NULL,
    CaptureStartTime VARCHAR(64),
    EntryCheckInStartDate DATETIME NULL,
    RecurPatternType VARCHAR(1) NULL,
    RecurPatternOpt VARCHAR(50) NULL,
    RecurPatternNum INTEGER NULL,
    RecurRangeOpt VARCHAR(50) NULL,
    Ocurrences INTEGER NULL,
    GenerateAgendasNextDayTime VARCHAR(64) NULL,
	CreationVersion FLOAT NULL,
	LastVersion FLOAT NULL,
	FirstDayOfWeek INTEGER NULL,
    PRIMARY KEY (AgendaSchedID)
);

CREATE TABLE IF NOT EXISTS SI_SV_SurveyAgendaSchedDet (
  `AgendaSchedID` int(11) NOT NULL,
  `SurveyID` int(11) NOT NULL,
  `Status` int(11) DEFAULT NULL,
  PRIMARY KEY (`AgendaSchedID`,`SurveyID`)
);

-- Cambios 2015-04-27: v5.01000 Actualización de código del App desde el servicio
CREATE TABLE SI_SV_AppUpdate (
	VersionNum INTEGER NOT NULL,
	UserID INTEGER NOT NULL,
	MinVersionToUpdate FLOAT NULL,
	MaxVersionToUpdate FLOAT NULL,
	Description LONGTEXT NULL,
	UpdateCode LONGTEXT NULL,
	PRIMARY KEY (VersionNum, UserID)
);

CREATE TABLE SI_SV_QuestionMembers (
	QuestionID INTEGER NOT NULL,
	MemberID INTEGER NOT NULL,
	MemberOrder INTEGER NULL,
	PRIMARY KEY (QuestionID, MemberID)
);

CREATE TABLE SI_SV_SectionMembers (
	SectionID INTEGER NOT NULL,
	MemberID INTEGER NOT NULL,
	MemberOrder INTEGER NULL,
	PRIMARY KEY (SectionID, MemberID)
);

-- Cambios 2015-07-15: v6.00000 Mapeo de cubos + Liga Catalogos vs DataSources
CREATE TABLE `si_sv_modelartus_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_modelartus` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `id_field` varchar(255) NOT NULL,
  `ebavel_table` varchar(255) NOT NULL,
  `ebavel_form` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_modelartus` (`id_modelartus`),
  KEY `id_question` (`id_question`),
  KEY `id_field` (`id_field`)
);

CREATE TABLE `si_sv_modelartus` (
  `id_modelartus` INT(11) NOT NULL AUTO_INCREMENT,
  `id_survey` INT(11) DEFAULT NULL,
  `cla_concepto` INT(11) DEFAULT NULL,
  `name` VARCHAR(255) DEFAULT NULL,
  `description` LONGTEXT,
  `createdUserKey` INT(11) DEFAULT NULL,
  `modifiedUserKey` INT(11) DEFAULT NULL,
  `createdDate` DATETIME DEFAULT NULL,
  `modifiedDate` DATETIME DEFAULT NULL,
  `syncDate` DATETIME DEFAULT NULL,
  `id_owner` INT(11) DEFAULT NULL,
  `cla_descrip_user` INT(11) DEFAULT NULL,
  `cla_descrip_agenda` INT(11) DEFAULT NULL,
  `cla_descrip_sv` INT(11) DEFAULT NULL,
  `cla_descrip_sv_latitude` INT(11) DEFAULT NULL,
  `cla_descrip_sv_longitude` INT(11) DEFAULT NULL,
  `cla_descrip_sv_accuracy` INT(11) DEFAULT NULL,
  `cla_descrip_sv_date` INT(11) DEFAULT NULL,
  `cla_descrip_sv_hour` INT(11) DEFAULT NULL,
  `cla_descrip_sv_enddate` INT(11) DEFAULT NULL,
  `cla_descrip_sv_endhour` INT(11) DEFAULT NULL,
  `cla_descrip_version` INT(11) DEFAULT NULL,
  `cla_descrip_country` INT(11) DEFAULT NULL,
  `cla_descrip_state` INT(11) DEFAULT NULL,
  `cla_descrip_city` INT(11) DEFAULT NULL,
  `cla_descrip_zipcode` INT(11) DEFAULT NULL,
  `cla_descrip_address` INT(11) DEFAULT NULL,
  `cla_indicador_count` INT(11) DEFAULT NULL,
  `cla_indicador_elapsed` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id_modelartus`),
  KEY `id_survey` (`id_survey`),
  KEY `cla_concepto` (`cla_concepto`)
) AUTO_INCREMENT=1;

CREATE TABLE `si_sv_modelartus_questions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_modelartus` INT(11) NOT NULL,
  `id_section` INT(11) NOT NULL,
  `id_question` INT(11) NOT NULL,
  `cla_descrip` INT(11) DEFAULT NULL,
  `cla_indicador` INT(11) DEFAULT NULL,
  `cla_concepto` INT(11) DEFAULT NULL,
  `path` LONGTEXT,
  `type` VARCHAR(3) DEFAULT NULL,
  `cla_descrip_atributo` INT(11) DEFAULT NULL,
  `cla_descrip_foto` INT(11) DEFAULT NULL,
  `cla_descrip_documento` INT(11) DEFAULT NULL,
  `cla_descrip_comentario` INT(11) DEFAULT NULL,
  `cla_indicador_score` INT(11) DEFAULT NULL,
  `cla_indicador_atributo` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_modelartus` (`id_modelartus`),
  KEY `id_section` (`id_section`),
  KEY `id_question` (`id_question`),
  KEY `cla_descrip` (`cla_descrip`),
  KEY `cla_indicador` (`cla_indicador`),
  KEY `cla_indicador_score` (`cla_indicador_score`)
) AUTO_INCREMENT=1;

CREATE TABLE `SI_SV_DataSource` (
  `DataSourceID` INTEGER NOT NULL DEFAULT 0,
  `DataSourceName` VARCHAR(255) DEFAULT NULL,
  `TableName` VARCHAR(255) DEFAULT NULL,
  `VersionNum` INTEGER DEFAULT NULL,
  `eBavelAppID` INTEGER DEFAULT NULL,
  `eBavelFormID` INTEGER DEFAULT NULL,
  `eBavelFormType` INTEGER DEFAULT NULL,
  `CreationUserID` INTEGER DEFAULT NULL,
  `LastModUserID` INTEGER DEFAULT NULL,
  `CreationDate` DATETIME DEFAULT NULL,
  `LastModDate` DATETIME DEFAULT NULL,
  `Type` INTEGER NULL,
  `Document` VARCHAR(255) DEFAULT NULL,
  RecurPatternType VARCHAR (1) NULL,
  RecurPatternOpt VARCHAR (50) NULL,
  RecurPatternNum INT NULL,
  RecurRangeOpt VARCHAR (50) NULL,
  id_ServiceConnection INT NULL,
  LastSyncDate DATETIME NULL,
  SheetName VARCHAR(255) NULL,
  `Path` LONGTEXT NULL,
  `RecurActive` INT NULL,
  `IdGoogleDrive` LONGTEXT NULL,
  `interval` INTEGER NULL,
  `CaptureStartTime` VARCHAR(64) NULL,
  LastSyncErrorDate DATETIME NULL,
  LastSyncError LONGTEXT NULL,
  parameters LONGTEXT NULL,
  servicedbname LONGTEXT NULL,
  servicetablename LONGTEXT NULL,
  servicedbtype INT NULL,
  DataVersionDate DATETIME NULL,
  IsModel INT NULL,
  PRIMARY KEY (`DataSourceID`)
);

CREATE TABLE `SI_SV_DataSourceMember` (
  `DataSourceID` INTEGER DEFAULT NULL,
  `MemberID` INTEGER NOT NULL DEFAULT 0,
  `MemberName` VARCHAR(255) DEFAULT NULL,
  `fieldName` VARCHAR(255) DEFAULT NULL,
  `MemberOrder` INTEGER DEFAULT NULL,
  `KeyOfAgenda` INTEGER DEFAULT NULL,
  `AgendaLatitude` INTEGER DEFAULT NULL,
  `AgendaLongitude` INTEGER DEFAULT NULL,
  `AgendaDisplay` INTEGER DEFAULT NULL,
  `eBavelFieldID` INTEGER DEFAULT NULL,
  `eBavelDataID` INTEGER DEFAULT NULL,
  `eBavelFieldDataID` INTEGER DEFAULT NULL,
  `MemberType` INTEGER DEFAULT NULL,
  `CreationUserID` INTEGER DEFAULT NULL,
  `LastModUserID` INTEGER DEFAULT NULL,
  `CreationDate` DATETIME DEFAULT NULL,
  `LastModDate` DATETIME DEFAULT NULL,
  `IsImage` INTEGER NULL,
  PRIMARY KEY (`MemberID`)
);

-- Cambios 2015-07-24: Nuevas dimensiones del cubo global surveys
ALTER TABLE SI_SV_GblSurveyModel ADD StartDateEditDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD EndDateEditDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD StartTimeEditDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD EndTimeEditDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD SyncDateEndDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD SyncTimeEndDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD CountryDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD StateDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD CityDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD ZipCodeDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD AddressDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD SyncLatitudeDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD SyncLongitudeDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD SyncAccuracyDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD LastCheckDate DATETIME NULL;

-- Cambios 2015-07-30: Mapeos de DataDestinations
CREATE TABLE `si_sv_datadestinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_survey` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  `condition` LONGTEXT,
  `type` int(11) DEFAULT NULL,
  `eBavelForm` varchar(255) DEFAULT NULL,
  `id_connection` int(11) DEFAULT NULL,
  `fileformat` int(11) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `real_path` varchar(255) DEFAULT NULL,
  `cla_dashboar` int(11) DEFAULT NULL,
  `cla_ars` int(11) DEFAULT NULL,
  `parameters` LONGTEXT NULL,
  `isIncremental` INT(11) DEFAULT 0,
  `additionaleMail` LONGTEXT NULL,
  `httpSendSinglePOST` INT(11) DEFAULT 0,
  `cla_dimension` INT NULL,
  `emailformat` INT NULL,
  `emailsubject` LONGTEXT NULL,
  `emailbody` LONGTEXT NULL,
  `additionaleMailComposite` LONGTEXT NULL,
  PRIMARY KEY (`id`,`id_survey`)
);

CREATE TABLE `si_sv_datadestinationsmapping` (
  `id_datadestinations` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `fieldformid` varchar(255),
  `formula` LONGTEXT,
  `additional_data` INTEGER NULL,
  PRIMARY KEY (`id_datadestinations`,`id_question`)
);

CREATE TABLE `SI_SV_DataSourceFilter` (
  `FilterID` INT(11) NOT NULL DEFAULT 0,
  `CatalogID` INT(11) DEFAULT NULL,
  `FilterName` LONGTEXT DEFAULT NULL,
  `FilterFormula` LONGTEXT DEFAULT NULL,
  `Status` INT NULL,
  PRIMARY KEY (`FilterID`)
);

CREATE TABLE `SI_SV_DataSourceFilter_Det` (
  `FilterdetID` INT(11) NOT NULL DEFAULT 0,
  `FilterID` INT(11) NOT NULL DEFAULT 0,
  `DataSourceMemberID` INT(11) DEFAULT NULL,
  `Value` LONGTEXT DEFAULT NULL,
  PRIMARY KEY (`FilterdetID`)
);

CREATE TABLE `SI_SV_DataSourceFilterRol` (
  `FilterID` INT(11) NOT NULL DEFAULT 0,
  `RolID` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`FilterID`,`RolID`)
);

CREATE TABLE `SI_SV_DataSourceFilterUser` (
  `FilterID` INT(11) NOT NULL DEFAULT 0,
  `UserID` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`FilterID`,`UserID`)
);

CREATE TABLE SI_SV_SurveyLog ( 
	CreationDateID DATETIME NOT NULL, 
	Repository VARCHAR(50) NOT NULL, 
	SurveyID INTEGER NOT NULL, 
	AgendaID INTEGER NOT NULL,
	UserID VARCHAR(255) NOT NULL, 
	WebMode INTEGER NULL, 
	Latitude DOUBLE NULL, 
	Longitude DOUBLE NULL, 
	Accuracy DOUBLE NULL,
	SyncLatitude DOUBLE NULL, 
	SyncLongitude DOUBLE NULL, 
	SyncAccuracy DOUBLE NULL,
	SurveyDate DATETIME NULL, 
	FileName VARCHAR(255) NULL, 
	FormsPath VARCHAR(255) NULL, 
	FormsVersion VARCHAR(50) NULL, 
	UserAgent LONGTEXT NULL,
	screenSize VARCHAR(255) NULL,
	platform VARCHAR(255) NULL,
	`Status` INTEGER DEFAULT 0,
	SessionID VARCHAR(60) NULL,
	DeviceName VARCHAR(60) NULL,
	UDID VARCHAR(100) NULL,
	DeviceID INTEGER NULL,
	Deleted INTEGER NULL
);

CREATE INDEX idxSurvey on SI_SV_SurveyLog (SurveyID, UserID);

CREATE INDEX idxUser on SI_SV_SurveyLog (UserID , SurveyID);

CREATE INDEX idxCreationDate on SI_SV_SurveyLog (CreationDateID, SurveyID, UserID);

CREATE INDEX idxSurvey ON SI_SV_Section (SurveyID, SectionID);

CREATE INDEX idxSurvey ON SI_SV_Question (SurveyID, QuestionID);

CREATE TABLE `si_sv_servicesconnection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `access_token` LONGTEXT DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `ftp` varchar(255) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `servicedbtype` int NULL,
  `servicename` LONGTEXT NULL,
  `parameters` LONGTEXT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `si_sv_checkpoint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `si_sv_checkpointattr` (
  `checkpoint_id` int(11) NOT NULL,
  `attr_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`checkpoint_id`,`attr_id`,`type`)
);

-- Funcionalidad de Enlazadores
CREATE TABLE SI_SV_Link
(
 LinkID INTEGER PRIMARY KEY,
 LinkName VARCHAR (255),
 Link LONGTEXT,
 Image LONGTEXT,
 Status INTEGER,
 DisplayMode INTEGER
);

-- Funcionalidad de preguntas OCR
ALTER TABLE SI_SV_Question ADD SavePhoto INTEGER NULL;

UPDATE SI_SV_Question SET SavePhoto = 0 WHERE SavePhoto IS NULL;

CREATE TABLE SI_SV_OCRQuestion
(
  OCRQuestionID INTEGER NOT NULL,
  OpenQuestionID INTEGER NOT NULL,
  QuestionOrder INTEGER NULL,
  PRIMARY KEY (OCRQuestionID, OpenQuestionID)
);

CREATE TABLE `si_sv_incdatadestinations` (
  `datadestination_id` int(11) DEFAULT NULL,
  `surveyid` int(11) DEFAULT NULL,
  `surveykey` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  KEY `idx1` (`datadestination_id`,`surveyid`,`status`)
);

CREATE TABLE SI_SV_TemplateStyle
(
    TemplateID INTEGER NOT NULL,
    ObjectType INTEGER NOT NULL,
    SectionType INTEGER NOT NULL,
    QuestionType INTEGER NOT NULL,
    DisplayType INTEGER NOT NULL,
    ElementType INTEGER NOT NULL,
    Style LONGTEXT NULL,
	SettingType INTEGER NULL,
    PRIMARY KEY (TemplateID,ObjectType,SectionType,QuestionType,DisplayType,ElementType)
);

CREATE TABLE SI_SV_TemplateStyleTpls (
  TemplateID INTEGER NOT NULL,
  TemplateName VARCHAR(255) NULL,
  PRIMARY KEY (TemplateID)
);

CREATE TABLE SI_SV_AppUpdateUsers (
	VersionNum INT NOT NULL,
	UserID INT NOT NULL,
	PRIMARY KEY (VersionNum, UserID)
);

CREATE TABLE SI_SV_SkipRules (
	SkipRuleID INTEGER NOT NULL,
	QuestionID INTEGER NOT NULL,
	RuleName VARCHAR(255) NULL,
	SkipCondition LONGTEXT not null,
	NextSectionID INTEGER NOT NULL,
	CreationUserID INTEGER NULL,
	CreationDateID DATETIME NULL,
	LastUserID INTEGER NULL,
	LastDateID DATETIME NULL,
	CreationVersion FLOAT NULL,
	LastVersion FLOAT NULL,
	PRIMARY KEY (SkipRuleID)
);

CREATE TABLE `SI_SV_PushMsg` (
  `MessageID` int(11) NOT NULL AUTO_INCREMENT,
  `CreationDateID` datetime NOT NULL,
  `CreationUserID` int(11) NOT NULL,
  `DestinationType` int(11) NOT NULL,
  `DestinationID` int(11) NOT NULL,
  `Title` varchar(100) DEFAULT NULL,
  `PushMessage` varchar(500) DEFAULT NULL,
  `RegID` varchar(255) DEFAULT NULL,
  `State` int(11) NOT NULL,
  PRIMARY KEY (`MessageID`),
  KEY `DateID` (`CreationDateID`),
  KEY `DestinationType` (`DestinationType`),
  KEY `DestinationID` (`DestinationID`)
);

-- JAPR 2016-10-14: Modificado el esquema de Push para enviar mensajes al ultimo dispositivo utilizado por el usuario solamente
CREATE TABLE SI_SV_PushMsgDet (
	MessageID INTEGER NOT NULL,
	UserID INTEGER NOT NULL,
	State INTEGER NULL,
	RegID VARCHAR(255) NULL,
	ErrorDesc LONGTEXT NULL,
	PRIMARY KEY (MessageID, UserID)
);

CREATE TABLE SI_SV_PushMsgUserGroups (
	MessageID INTEGER NOT NULL,
	UserGroupID INTEGER NOT NULL,
	PRIMARY KEY (MessageID, UserGroupID)
);

-- JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
CREATE TABLE SI_SV_SurveyAnswerImageSrc 
(
  SurveyID integer,
  QuestionID integer,
  FactKey bigint,
  MainFactKey bigint,
  PathImage VARCHAR(255)
);

CREATE TABLE SI_SV_EditorCustomColors (
	UserID INTEGER NOT NULL,
	ColorID INTEGER NOT NULL,
	Color VARCHAR(10) NULL,
	PRIMARY KEY (UserID, ColorID)
);
