-- Verificar antes que no exista el campo ConsecutiveID en la tabla SI_SV_QAnswersCol 
SELECT ConsecutiveID FROM SI_SV_QAnswersCol;

-- En el dado caso q no exista dicho campo, entonces se elimina la tabla
DROP TABLE SI_SV_QAnswersCol;

-- Despues de su eliminación se vuelve a crear esa tabla con los nuevos campos
CREATE TABLE SI_SV_QAnswersCol
(
  ConsecutiveID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  QuestionID integer,
  SortOrder integer,
  DisplayText varchar(255),
  IndDimID integer,
  IndicatorID integer,
  Score varchar(64),
  ScoreOperation integer,
  PRIMARY KEY (ConsecutiveID)
);
