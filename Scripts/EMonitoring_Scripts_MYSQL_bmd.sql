﻿CREATE TABLE SI_SV_Users 
(
  UserID integer unsigned NOT NULL AUTO_INCREMENT,
  UserName varchar(255),
  Email varchar(255),
  AltEmail varchar(255),
  SV_Admin integer,
  SV_User integer,
  ArtusUser varchar(64),
  CLA_USUARIO integer,
  SendPDFByEmail integer,
  Supervisor integer,
  DefaultSurvey INTEGER,
  TemplateID INTEGER,
  ProfileID INTEGER,
  Image VARCHAR(255),
  AgendasSurveysTemplate LONGTEXT,
  GeoFenceRadius integer,
  GeoFenceLat double,
  GeoFenceLng double,
  UserParam1 varchar(255),
  UserParam2 varchar(255),
  UserParam3 varchar(255),
  PRIMARY KEY (UserID)
);

INSERT INTO SI_SV_Users (UserName, Email, SV_Admin, SV_User, ArtusUser, CLA_USUARIO)
SELECT CUENTA_CORREO, CUENTA_CORREO, 1, 0, NOM_CORTO, CLA_USUARIO FROM SI_USUARIO 
WHERE CLA_USUARIO <> 0 AND CLA_USUARIO NOT IN (SELECT CLA_USUARIO FROM SI_SV_Users);

ALTER TABLE SI_DESCRIP_ENC ADD IS_CATALOG INT NULL;

CREATE TABLE si_sv_artusreporter 
(
  schedulerID integer,
  tipo_objeto integer,
  cla_objeto integer,
  cla_dimension integer,
  cla_user integer,
  id integer unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
);

INSERT INTO SI_CONFIGURA (CLA_CONFIGURA, CLA_USUARIO, NOM_CONFIGURA, REF_CONFIGURA)
VALUES (615, -2, 'Advisor: Web HTML URL', 'https://kpionline5.bitam.com/artus/genvi_test/');

INSERT INTO SI_CONFIGURA (CLA_CONFIGURA, CLA_USUARIO, NOM_CONFIGURA, REF_CONFIGURA)
VALUES (616, -2, 'Advisor: Web HTML Project', DATABASE());

-- Cambios 2014-07-09 v4.04033: Permisos de acceso por perfil de usuario
ALTER TABLE SI_SV_Users ADD ProfileID INTEGER NULL;
 
UPDATE SI_SV_Users SET ProfileID = 0 WHERE ProfileID IS NULL;

ALTER TABLE SI_USUARIO ADD CUENTA_CORREO_ALT VARCHAR(255) NULL;

UPDATE SI_USUARIO SET CUENTA_CORREO_ALT = '' WHERE CUENTA_CORREO_ALT IS NULL;

ALTER TABLE SI_ROL ADD DefaultSurvey INTEGER DEFAULT 0;

ALTER TABLE SI_ROL ADD TemplateID INTEGER DEFAULT 0;

ALTER TABLE SI_SV_Users ADD Image VARCHAR(255);

CREATE INDEX idxtabla_field ON si_descrip_enc (NOM_TABLA, NOM_FISICO);

-- Cambios 2015-08-24 v6.00010: Agregados los campos para habilitar/deshabilitar acceso vía KPIOnline
ALTER TABLE SI_USUARIO ADD EFORMS_ADMIN INTEGER NULL;

ALTER TABLE SI_USUARIO ADD EFORMS_DESKTOP INTEGER NULL;

INSERT INTO SI_MD_VERSION (TIPO_PRODUCTO, PRODUCTO, VERSION) VALUES (6, 'eForms', 6.03000);
