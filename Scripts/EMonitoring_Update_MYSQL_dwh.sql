﻿ALTER TABLE SI_SV_Question MODIFY QuestionID BIGINT;

ALTER TABLE SI_SV_Survey ADD ClosingMsg LONGTEXT;

UPDATE SI_SV_Survey SET ClosingMsg = '' WHERE ClosingMsg IS NULL;

ALTER TABLE SI_SV_Survey ADD AllowMultipleSurveys INTEGER;

UPDATE SI_SV_Survey SET AllowMultipleSurveys = 1 WHERE AllowMultipleSurveys IS NULL;

ALTER TABLE SI_SV_Survey ADD CountIndID INTEGER;

ALTER TABLE SI_SV_Survey ADD DurationIndID INTEGER;

ALTER TABLE SI_SV_Survey ADD UserDimID INTEGER;

ALTER TABLE SI_SV_Question ADD AttributeName VARCHAR(255);

ALTER TABLE SI_SV_Question ADD IsFieldSurveyID INTEGER;

ALTER TABLE SI_SV_Question ADD IsIndicator INTEGER;

ALTER TABLE SI_SV_Question ADD IndicatorID INTEGER;

ALTER TABLE SI_SV_Survey ADD SectionDimID INTEGER;

ALTER TABLE SI_SV_Question ADD SectionID BIGINT;

ALTER TABLE SI_SV_Survey ADD CreationUserID INTEGER;

ALTER TABLE SI_SV_Survey ADD CreationDateID DATETIME;

ALTER TABLE SI_SV_Survey ADD LastUserID INTEGER;

ALTER TABLE SI_SV_Survey ADD LastDateID DATETIME;

ALTER TABLE SI_SV_Survey ADD CaptureStartTime VARCHAR(64);

ALTER TABLE SI_SV_Survey ADD CaptureEndTime VARCHAR(64);

CREATE TABLE SI_SV_Section 
(
  SurveyID INTEGER,
  SectionID BIGINT,
  SectionName VARCHAR(255),
  SectionDimValID INTEGER,
  PRIMARY KEY (SectionID)
);

CREATE TABLE SI_SV_Scheduler 
(
  SurveyID INTEGER,
  CaptureStartDate DATETIME,
  CaptureEndDate DATETIME,
  CaptureType INTEGER,
  WhenToCapture VARCHAR(255),
  PRIMARY KEY (SurveyID)
);

CREATE TABLE SI_SV_Catalog 
(
  CatalogID INTEGER,
  CatalogName VARCHAR(255),
  PRIMARY KEY (CatalogID)
);

CREATE TABLE SI_SV_CatalogMember
(
  CatalogID INTEGER,
  MemberID BIGINT,
  MemberName VARCHAR(255),
  PRIMARY KEY (MemberID)
);

CREATE TABLE SI_SV_SurveyDimKeys
(
  SurveyID INTEGER,
  DimClaDescrip INTEGER,
  PRIMARY KEY (SurveyID, DimClaDescrip)
);

ALTER TABLE SI_SV_Survey ADD HelpMsg LONGTEXT;

CREATE TABLE SI_SV_SurveyRol 
(
  SurveyID INTEGER,
  RolID INTEGER,
  PRIMARY KEY (SurveyID,RolID)
);

ALTER TABLE SI_SV_Scheduler ADD StartDay INTEGER;

ALTER TABLE SI_SV_Scheduler ADD Duration INTEGER;

ALTER TABLE SI_SV_Question ADD IsReqQuestion INTEGER;

ALTER TABLE SI_SV_Question ADD HasReqComment INTEGER;

ALTER TABLE SI_SV_Question ADD HasReqPhoto INTEGER;

ALTER TABLE SI_SV_Question ADD UseCatalog INTEGER;
ALTER TABLE SI_SV_Question ADD CatalogID INTEGER;
ALTER TABLE SI_SV_Question ADD CatMemberID INTEGER;

ALTER TABLE SI_SV_Catalog ADD ParentID INTEGER;
ALTER TABLE SI_SV_Catalog ADD TableName VARCHAR(255);

ALTER TABLE SI_SV_CatalogMember ADD ParentID INTEGER;
ALTER TABLE SI_SV_CatalogMember ADD MemberOrder INTEGER;

ALTER TABLE SI_SV_Catalog ADD ModelID INTEGER;

ALTER TABLE SI_SV_Question ADD QDisplayMode INTEGER;
 
UPDATE SI_SV_Question SET QDisplayMode = 4 WHERE QDisplayMode IS NULL AND QTypeID IN (1, 4, 5);
 
UPDATE SI_SV_Question SET QDisplayMode = 2 WHERE QDisplayMode IS NULL AND QTypeID IN (2, 3);
 
ALTER TABLE SI_SV_Question ADD OptionsTextCol LONGTEXT NULL;
 
UPDATE SI_SV_Question SET OptionsTextCol = '' WHERE OptionsTextCol IS NULL;
 
CREATE TABLE SI_SV_QAnswersCol 
(
  QuestionID INTEGER,
  SortOrder INTEGER,
  DisplayText VARCHAR(255),
  PRIMARY KEY (QuestionID, SortOrder)
);

INSERT INTO SI_SV_QType (QTypeID, QTypeName) VALUES (6,'Alphanumeric');

CREATE TABLE SI_SV_SurveyFilter
(
  FilterID INTEGER,
  SurveyID INTEGER,
  CatalogID INTEGER,
  CatDimID INTEGER,
  FilterText LONGTEXT,
  PRIMARY KEY (FilterID)
);

ALTER TABLE SI_SV_Survey ADD CatalogID INTEGER;

ALTER TABLE SI_SV_Question ADD QLength INTEGER NULL;

ALTER TABLE SI_SV_SurveyFilter ADD FilterLevels VARCHAR(255);

ALTER TABLE SI_SV_SurveyFilter ADD FilterName VARCHAR(255);

CREATE TABLE SI_SV_CatFilter
(
  FilterID INTEGER,
  CatalogID INTEGER,
  FilterName VARCHAR(255),
  FilterText LONGTEXT,
  FilterLevels VARCHAR(255),
  PRIMARY KEY (FilterID)
);

CREATE TABLE SI_SV_CatFilterUser 
(
  FilterID INTEGER,
  UserID INTEGER,
  PRIMARY KEY (FilterID,UserID)
);

CREATE TABLE SI_SV_CatFilterRol 
(
  FilterID INTEGER,
  RolID INTEGER,
  PRIMARY KEY (FilterID,RolID)
);

CREATE TABLE SI_SV_SurveyScheduler 
(
  SchedulerID INTEGER,
  SchedulerName VARCHAR(255),
  SurveyID INTEGER,
  CaptureStartDate DATETIME,
  CaptureEndDate DATETIME,
  CaptureType INTEGER,
  WhenToCapture VARCHAR(255),
  StartDay INTEGER,
  Duration INTEGER,
  PRIMARY KEY (SchedulerID)
);

CREATE TABLE SI_SV_SurveySchedulerUser 
(
  SchedulerID INTEGER,
  UserID INTEGER,
  PRIMARY KEY (SchedulerID,UserID)
);

CREATE TABLE SI_SV_SurveySchedulerRol 
(
  SchedulerID INTEGER,
  RolID INTEGER,
  PRIMARY KEY (SchedulerID,RolID)
);

ALTER TABLE SI_SV_SurveyScheduler ADD AllowMultipleSurveys INTEGER;

UPDATE SI_SV_SurveyScheduler SET AllowMultipleSurveys = 1 WHERE AllowMultipleSurveys IS NULL;

CREATE TABLE SI_SV_SurveyAnswerImage 
(
  SurveyID INTEGER,
  QuestionID INTEGER,
  FactKey BIGINT,
  PathImage VARCHAR(255)
);

CREATE TABLE SI_SV_SurveyAnswerComment 
(
  SurveyID INTEGER,
  QuestionID INTEGER,
  FactKey BIGINT,
  StrComment VARCHAR(255)
);

ALTER TABLE SI_SV_Survey ADD SurveyType INTEGER;

UPDATE SI_SV_Survey SET SurveyType = 0 WHERE SurveyType IS NULL;

CREATE TABLE SI_SV_SurveyMap (
  ObjectID INTEGER NOT NULL,
  SourceID INTEGER NOT NULL,
  TargetID INTEGER NOT NULL,
  PRIMARY KEY (ObjectID, SourceID, TargetID)
);

ALTER TABLE SI_SV_Survey ADD HasSignature INTEGER;

UPDATE SI_SV_Survey SET HasSignature = 0 WHERE HasSignature IS NULL;

CREATE TABLE SI_SV_SurveySignatureImg 
(
  SurveyID INTEGER,
  FactKey BIGINT,
  PathImage VARCHAR(255)
);

ALTER TABLE SI_SV_Section ADD SectionNumber INTEGER;

ALTER TABLE SI_SV_Survey ADD LatitudeIndID INTEGER;
ALTER TABLE SI_SV_Survey ADD LongitudeIndID INTEGER;

-- Este campo permite almacenar a q ID de dimension se asocio dicha respuesta
ALTER TABLE SI_SV_QAnswers ADD IndDimID INTEGER;

-- Eliminar llave primaria para que se pueda actualizar correctamente el campo id_registro_seq
ALTER TABLE enc_encuesta_det_control DROP PRIMARY KEY;

ALTER TABLE SI_SV_Question ADD IsMultiDimension INTEGER;

UPDATE SI_SV_Question SET IsMultiDimension = 1 WHERE QTypeID = 3 AND IsMultiDimension IS NULL;

CREATE TABLE SI_SV_GblSurveyModel
(
  GblSurveyModelID INTEGER,
  SurveyDimID INTEGER,
  UserDimID INTEGER,
  StartDateDimID INTEGER,
  EndDateDimID INTEGER,
  StartTimeDimID INTEGER,
  EndTimeDimID INTEGER,
  CountIndID INTEGER,
  DurationIndID INTEGER,
  LatitudeIndID INTEGER,
  LongitudeIndID INTEGER
);

ALTER TABLE SI_SV_Question ADD UseCatToFillMC INTEGER;

UPDATE SI_SV_Question SET UseCatToFillMC = 0 WHERE QTypeID = 3 AND UseCatToFillMC IS NULL;

ALTER TABLE SI_SV_Survey ADD EmailDimID INTEGER;

ALTER TABLE SI_SV_Survey ADD StatusDimID INTEGER;

ALTER TABLE SI_SV_Survey ADD SchedulerDimID INTEGER;

ALTER TABLE SI_SV_SurveyScheduler ADD CaptureVia INTEGER;
ALTER TABLE SI_SV_SurveyScheduler ADD UseCatalog INTEGER;
ALTER TABLE SI_SV_SurveyScheduler ADD CatalogID INTEGER;
ALTER TABLE SI_SV_SurveyScheduler ADD AttributeID INTEGER;
ALTER TABLE SI_SV_SurveyScheduler ADD EmailSubject VARCHAR(255);
ALTER TABLE SI_SV_SurveyScheduler ADD EmailBody LONGTEXT;

UPDATE SI_SV_SurveyScheduler SET CaptureVia = 0 WHERE CaptureVia IS NULL;
UPDATE SI_SV_SurveyScheduler SET UseCatalog = 0 WHERE UseCatalog IS NULL;

CREATE TABLE SI_SV_SchedulerEmail
(
  ConsecutiveID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  SchedulerID INTEGER,
  Email VARCHAR(255),
  PRIMARY KEY (ConsecutiveID)
);

ALTER TABLE SI_SV_Question ADD MCInputType INTEGER;
ALTER TABLE SI_SV_Survey ADD AnsweredQuestionsIndID INTEGER;
ALTER TABLE SI_SV_GblSurveyModel ADD AnsweredQuestionsIndID INTEGER;
ALTER TABLE SI_SV_Question ADD HasActions INTEGER;
ALTER TABLE SI_SV_Question ADD QActions LONGTEXT;

CREATE TABLE SI_SV_SurveyQuestionActions 
(
  SurveyID INTEGER,
  QuestionID INTEGER,
  FactKey BIGINT,
  StrActions LONGTEXT
);

ALTER TABLE SI_SV_SurveyAnswerComment MODIFY StrComment LONGTEXT;

ALTER TABLE SI_SV_Question ADD FatherQuestionID INTEGER;

ALTER TABLE SI_SV_Question ADD ChildQuestionID INTEGER;

ALTER TABLE SI_SV_Section ADD SectionType INTEGER;

UPDATE SI_SV_Section SET SectionType = 0 WHERE SectionType IS NULL;

ALTER TABLE SI_SV_Section ADD CatalogID INTEGER;

ALTER TABLE SI_SV_Section ADD CatMemberID INTEGER;

ALTER TABLE SI_SV_Question ADD OutsideOfSection INTEGER;

ALTER TABLE SI_SV_Question ADD GoToQuestion INTEGER;

UPDATE SI_SV_Question SET GoToQuestion = 0 WHERE GoToQuestion IS NULL;

ALTER TABLE SI_SV_Survey ADD AllowPartialSave INTEGER;

UPDATE SI_SV_Survey SET AllowPartialSave = 0 WHERE AllowPartialSave IS NULL;

ALTER TABLE SI_SV_Question ADD IsIndicatorMC INTEGER;

UPDATE SI_SV_Question SET IsIndicatorMC = 0 WHERE IsIndicatorMC IS NULL;

ALTER TABLE SI_SV_Survey ADD FactKeyDimID INTEGER;

ALTER TABLE SI_SV_Question ADD ImgDimID INTEGER;

-- Este campo permite almacenar a q ID de indicado se asocio dicha respuesta
-- de multiple choice tipo entrada numerica
ALTER TABLE SI_SV_QAnswers ADD IndicatorID INTEGER;

CREATE TABLE SVSurveyActions
(
  SurveyDimVal BIGINT,
  SurveyID INTEGER,
  FactKey BIGINT,
  QuestionID INTEGER,
  OptionText VARCHAR(255),
  ACTION LONGTEXT,
  Responsible INTEGER,
  Deadline DATETIME,
  STATUS INTEGER
);

CREATE TABLE SI_SV_QOptionActions 
(
  ConsecutiveID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  QuestionID INTEGER,
  OptionText VARCHAR(255),
  SortOrder INTEGER,
  ActionText VARCHAR(255),
  PRIMARY KEY (ConsecutiveID)
);

ALTER TABLE SI_SV_Survey ADD ActionIndInlineID INTEGER;

ALTER TABLE SI_SV_SurveyAnswerImage ADD MainFactKey BIGINT;
ALTER TABLE SI_SV_SurveyAnswerComment ADD MainFactKey BIGINT;

-- Conchita, Agregado 10-nov-2011 EmailReplyTo
ALTER TABLE SI_SV_SurveyScheduler ADD EmailReplyTo VARCHAR(255);

ALTER TABLE SI_SV_Section ADD FormattedText LONGTEXT;

ALTER TABLE SI_SV_Section ADD SendThumbnails INTEGER;

ALTER TABLE SI_SV_Section ADD EnableHyperlinks INTEGER;

ALTER TABLE SI_SV_Question MODIFY MinValue VARCHAR(64);

ALTER TABLE SI_SV_Question MODIFY `MaxValue` VARCHAR(64);

-- Modificar campo GoToQuestion a GoToSection
ALTER TABLE SI_SV_Question ADD GoToSection INTEGER;

-- Modificar campo NextQuestion a NextSection
ALTER TABLE SI_SV_QAnswers ADD NextSection INTEGER;

-- Tabla que almacena la relacion de opciones con Ocultar/Mostrar Preguntas
CREATE TABLE SI_SV_ShowQuestions
(
  ShowID BIGINT,
  QuestionID INTEGER,
  ConsecutiveID INTEGER,
  ShowQuestionID INTEGER,
  PRIMARY KEY (ShowID)
);

-- Agregar campo Score en tabla SI_SV_QAnswers
ALTER TABLE SI_SV_QAnswers ADD Score VARCHAR(64);

-- Agregar campo EmailFilter en tabla SI_SV_SurveyScheduler
ALTER TABLE SI_SV_SurveyScheduler ADD EmailFilter INTEGER;

-- Agregar campo OneChoicePer en tabla SI_SV_Question
ALTER TABLE SI_SV_Question ADD OneChoicePer INTEGER;

-- Agregar campos a tabla SI_SV_GblSurveyModel que representan dimensiones Survey Global ID y Survey Single Record ID
ALTER TABLE SI_SV_GblSurveyModel ADD SurveyGlobalDimID INTEGER;
ALTER TABLE SI_SV_GblSurveyModel ADD SurveySingleRecordDimID INTEGER;

-- Eliminamos un campo que no debe estar en la tabla SI_SV_GblSurveyModel
-- ALTER TABLE SI_SV_GblSurveyModel DROP COLUMN AnsweredQuestions;

-- Se da de alta el campo que indica el tipo de despliegue del texto
ALTER TABLE SI_SV_Question ADD TextDisplayStyle INTEGER;

-- Campo donde se almacena el atributo latitud
ALTER TABLE SI_SV_Survey ADD LatitudeAttribID INTEGER;

-- Campo donde se almacena el atributo longitud
ALTER TABLE SI_SV_Survey ADD LongitudeAttribID INTEGER;

-- Se da de alta el campo que indica si la pregunta utiliza Category Dimension
ALTER TABLE SI_SV_Question ADD UseCategoryDim INTEGER;

-- Se da de alta el campo CategoryDimName
ALTER TABLE SI_SV_Question ADD CategoryDimName VARCHAR(255);

-- Se da de alta el campo ElementDimName
ALTER TABLE SI_SV_Question ADD ElementDimName VARCHAR(255);

-- Se da de alta el campo ElementIndName
ALTER TABLE SI_SV_Question ADD ElementIndName VARCHAR(255);

-- Se da de alta el campo CategoryDimID
ALTER TABLE SI_SV_Question ADD CategoryDimID INTEGER;

-- Se da de alta el campo ElementDimID
ALTER TABLE SI_SV_Question ADD ElementDimID INTEGER;

-- Se da de alta el campo ElementIndID
ALTER TABLE SI_SV_Question ADD ElementIndID INTEGER;

-- Campo indicador de version en el catalogo
ALTER TABLE SI_SV_Catalog ADD VersionIndID INTEGER NULL;

-- Actualizacion de Campo indicador de version en el catalogo
UPDATE SI_SV_Catalog SET VersionIndID = 0 WHERE VersionIndID IS NULL;

-- Tabla con las definiciones de Agendas
CREATE TABLE SI_SV_SurveyAgenda
(
  AgendaID INTEGER NOT NULL,
  AgendaName VARCHAR(255) NULL,
  UserID INTEGER NOT NULL,
  CatalogID INTEGER NULL,
  AttributeID INTEGER NULL,
  CaptureStartDate DATETIME NULL,
  CaptureStartTime VARCHAR(64) NULL,
  FilterText VARCHAR(255) NULL,
  PRIMARY KEY (AgendaID)
);

-- Tabla con las encuestas disponibles por Agenda
CREATE TABLE SI_SV_SurveyAgendaDet
(
  AgendaID INTEGER NOT NULL,
  SurveyID INTEGER NOT NULL,
  PRIMARY KEY (AgendaID, SurveyID)
);

-- Campos de edición de las Agendas
ALTER TABLE SI_SV_SurveyAgenda ADD CreationUserID INTEGER NULL;

ALTER TABLE SI_SV_SurveyAgenda ADD CreationDateID DATETIME NULL;

ALTER TABLE SI_SV_SurveyAgenda ADD LastUserID INTEGER NULL;

ALTER TABLE SI_SV_SurveyAgenda ADD LastDateID DATETIME NULL;

-- Campo para indicar si las encuestas por agenda ya fueron capturadas
ALTER TABLE SI_SV_SurveyAgendaDet ADD STATUS INTEGER NULL;

-- Campo para almacenar la versión de las encuestas
ALTER TABLE SI_SV_Survey ADD VersionNum INTEGER NULL;

-- Campo para indicar si se muestra la opción de "Otro" en preguntas de selección sencilla sin catálogo
ALTER TABLE SI_SV_Question ADD AllowAdditionalAnswers INTEGER NULL;

UPDATE SI_SV_Question SET AllowAdditionalAnswers = 0 WHERE AllowAdditionalAnswers IS NULL;

-- Opción para seleccionar el estilo visual del componente Select en preguntas de selección sencilla (Nativo o JQuery Mobile)
ALTER TABLE SI_SV_Question ADD QComponentStyleID INTEGER NULL;

UPDATE SI_SV_Question SET QComponentStyleID = 0 WHERE QComponentStyleID IS NULL;

-- Cambios 2012-05-15: Preguntas tipo Action
CREATE TABLE SI_SV_Settings (
  SettingID INTEGER NOT NULL,
  SettingName VARCHAR(255),
  SettingValue VARCHAR(255),
  PRIMARY KEY (SettingID)
);

ALTER TABLE SI_SV_Survey ADD ElementQuestionID INTEGER NULL;

UPDATE SI_SV_Survey SET ElementQuestionID = 0 WHERE ElementQuestionID IS NULL;

ALTER TABLE SI_SV_Question ADD ResponsibleID INTEGER NULL;

UPDATE SI_SV_Question SET ResponsibleID = -1 WHERE ResponsibleID IS NULL;

ALTER TABLE SI_SV_Question ADD CategoryID INTEGER NULL;

UPDATE SI_SV_Question SET CategoryID = 0 WHERE CategoryID IS NULL;

ALTER TABLE SI_SV_Question ADD DaysDueDate INTEGER NULL;

UPDATE SI_SV_Question SET DaysDueDate = 0 WHERE DaysDueDate IS NULL;

ALTER TABLE SI_SV_Survey ADD eBavelAppID INTEGER NULL;

UPDATE SI_SV_Survey SET eBavelAppID = 0 WHERE eBavelAppID IS NULL;

ALTER TABLE SI_SV_Survey ADD eBavelFormID INTEGER NULL;

UPDATE SI_SV_Survey SET eBavelFormID = 0 WHERE eBavelFormID IS NULL;

-- Nuevas dimensiones en el cubo de la encuesta
ALTER TABLE SI_SV_Survey ADD StartDateDimID INTEGER;
ALTER TABLE SI_SV_Survey ADD EndDateDimID INTEGER;
ALTER TABLE SI_SV_Survey ADD StartTimeDimID INTEGER;
ALTER TABLE SI_SV_Survey ADD EndTimeDimID INTEGER;

-- Cambios 2012-05-18: Preguntas Single Choice con Acciones
ALTER TABLE SI_SV_QAnswers ADD ActionText VARCHAR(255) NULL;

UPDATE SI_SV_QAnswers SET ActionText = '' WHERE ActionText IS NULL;

ALTER TABLE SI_SV_QAnswers ADD ResponsibleID INTEGER NULL;

UPDATE SI_SV_QAnswers SET ResponsibleID = -1 WHERE ResponsibleID IS NULL;

ALTER TABLE SI_SV_QAnswers ADD CategoryID INTEGER NULL;

UPDATE SI_SV_QAnswers SET CategoryID = 0 WHERE CategoryID IS NULL;

ALTER TABLE SI_SV_QAnswers ADD DaysDueDate INTEGER NULL;

UPDATE SI_SV_QAnswers SET DaysDueDate = 0 WHERE DaysDueDate IS NULL;

ALTER TABLE SI_SV_QAnswers ADD InsertWhen INTEGER NULL;

UPDATE SI_SV_QAnswers SET InsertWhen = 1 WHERE InsertWhen IS NULL;

-- Cambios 2012-05-23: Log de procesos de las Apps móviles
CREATE TABLE SI_SV_AppLog (
  LogKey INTEGER NOT NULL AUTO_INCREMENT,
  UserID INTEGER NOT NULL,
  AgendaID INTEGER NULL,
  SurveyID INTEGER NULL,
  EventDateID DATETIME NULL,
  UploadDateID DATETIME NULL,
  GMTZone INTEGER NULL,
  Latitude DOUBLE NULL,
  Longitude DOUBLE NULL,
  DeviceName VARCHAR(255) NULL,
  PlataformName VARCHAR(255) NULL,
  OSVersion VARCHAR(255) NULL,
  AppsRunnning INTEGER NULL,
  UsedMemory INTEGER NULL,
  EventName VARCHAR(255),
  AppSessionID VARCHAR(20) NULL,
  EventID INTEGER NULL,
  EventStepID INTEGER NULL,
  ConnectionType VARCHAR(50) NULL,
  PRIMARY KEY (LogKey)
);

-- Cambios 2012-05-28: Agregado el soporte para reagendar encuestas al ser contestadas
ALTER TABLE SI_SV_Survey ADD EnableReschedule INTEGER NULL;

UPDATE SI_SV_Survey SET EnableReschedule = 0 WHERE EnableReschedule IS NULL;

ALTER TABLE SI_SV_Survey ADD RescheduleCatalogID INTEGER NULL;

UPDATE SI_SV_Survey SET RescheduleCatalogID = 0 WHERE RescheduleCatalogID IS NULL;

ALTER TABLE SI_SV_Survey ADD RescheduleCatMemberID INTEGER NULL;

UPDATE SI_SV_Survey SET RescheduleCatMemberID = 0 WHERE RescheduleCatMemberID IS NULL;

ALTER TABLE SI_SV_Survey ADD RescheduleStatus VARCHAR(255) NULL;

UPDATE SI_SV_Survey SET RescheduleStatus = '' WHERE RescheduleStatus IS NULL;

-- Cambios 2012-05-28: Agregado el soporte para capturar decimales con el teclado telefónico de las Apps agregando mas dígitos al número tecleado
ALTER TABLE SI_SV_Question ADD DecimalPlaces INTEGER NULL;

UPDATE SI_SV_Question SET DecimalPlaces = 0 WHERE DecimalPlaces IS NULL;

-- Cambios 2012-05-28: Agregado el soporte para creación de preguntas (single y multi) cuyas respuestas dependan de las seleccionadas de una pregunta multi previa
ALTER TABLE SI_SV_Question ADD SourceQuestionID INTEGER NULL;

UPDATE SI_SV_Question SET SourceQuestionID = 0 WHERE SourceQuestionID IS NULL;

-- Cambios 2012-06-14: Agregados los campos con el SurveyID y AgendaID en el Log
ALTER TABLE SI_SV_AppLog ADD SurveyID INTEGER NULL;

UPDATE SI_SV_AppLog SET SurveyID = 0 WHERE SurveyID IS NULL;

ALTER TABLE SI_SV_AppLog ADD AgendaID INTEGER NULL;

UPDATE SI_SV_AppLog SET AgendaID = 0 WHERE AgendaID IS NULL;

-- Cambios 2012-07-26: Configuración de salto de sección a sección
ALTER TABLE SI_SV_Section ADD NextSectionID INTEGER NULL;

UPDATE SI_SV_Section A
  INNER JOIN SI_SV_Question B ON A.SectionID = B.SectionID
SET A.NextSectionID = B.GotoSection
WHERE CONCAT(B.SurveyID, '-', B.QuestionNumber) IN (
  SELECT CONCAT(SurveyID, '-', MAX(QuestionNumber))
  FROM SI_SV_Question
  WHERE GotoSection > 0
  GROUP BY SurveyID, SectionID
) AND A.NextSectionID IS NULL;

ALTER TABLE SI_SV_Question ADD FormatMask VARCHAR(30) NULL;

UPDATE SI_SV_Question SET FormatMask = '' WHERE FormatMask IS NULL;

ALTER TABLE SI_SV_CatalogMember ADD DescriptorID INTEGER NULL;

UPDATE SI_SV_CatalogMember SET DescriptorID = 0 WHERE DescriptorID IS NULL;

-- Cambios 2012-07-28: Agregado el valor default de preguntas y dependencia de auto-evaluación
ALTER TABLE SI_SV_Question ADD DefaultValue LONGTEXT;

UPDATE SI_SV_Question SET DefaultValue = '' WHERE DefaultValue IS NULL;

CREATE TABLE SI_SV_QuestionDep
(
  AutoEvalID INT NOT NULL,
  QuestionID INTEGER NOT NULL,
  EvalQuestionID INTEGER NOT NULL,
  PRIMARY KEY (AutoEvalID)
);

ALTER TABLE SI_SV_Question ADD HasQuestionDep INTEGER NULL;

UPDATE SI_SV_Question SET HasQuestionDep = 0 WHERE HasQuestionDep IS NULL;

-- Cambios 2012-08-08: Campo para almacenar la versión de las encuestas
ALTER TABLE SI_SV_SurveyAgenda ADD VersionNum INTEGER NULL;

UPDATE SI_SV_SurveyAgenda SET VersionNum = 0 WHERE VersionNum IS NULL;

ALTER TABLE SI_SV_Catalog ADD VersionNum INTEGER NULL;

UPDATE SI_SV_Catalog SET VersionNum = 0 WHERE VersionNum IS NULL;

-- 2012-08-13: Cambios para agregar dimensiones con fecha de sincronización
ALTER TABLE SI_SV_Survey ADD SyncDateDimID INTEGER NULL;
 
ALTER TABLE SI_SV_Survey ADD SyncTimeDimID INTEGER NULL;
 
ALTER TABLE SI_SV_GblSurveyModel ADD SyncDateDimID INTEGER NULL;
 
ALTER TABLE SI_SV_GblSurveyModel ADD SyncTimeDimID INTEGER NULL;

-- 2012-09-30: Cambios para extender la información de las acciones
ALTER TABLE SI_SV_Question ADD ActionTitle LONGTEXT NULL;

ALTER TABLE SI_SV_Question ADD ActionText LONGTEXT NULL;

ALTER TABLE SI_SV_QAnswers ADD ActionTitle LONGTEXT NULL;

ALTER TABLE SI_SV_QAnswers MODIFY ActionText LONGTEXT NULL;

-- 2012-11-02: Cambio para permitir sincronización inmediata al grabado
ALTER TABLE SI_SV_Survey ADD SyncWhenSaving INT NULL;

UPDATE SI_SV_Survey SET SyncWhenSaving = 0 WHERE SyncWhenSaving IS NULL;

-- Cambios 2012-08-30: 
ALTER TABLE SI_SV_Question ADD QuestionImageText LONGTEXT;
ALTER TABLE SI_SV_QAnswers ADD DisplayImage LONGTEXT;

-- Cambios 2012-09-03: 
ALTER TABLE SI_SV_Question ADD DateFormatMask VARCHAR(30);

-- Version 3.00019
ALTER TABLE SI_SV_GblSurveyModel ADD ServerStartDateDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD ServerEndDateDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD ServerStartTimeDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD ServerEndTimeDimID INTEGER NULL;

ALTER TABLE SI_SV_Survey ADD ServerStartDateDimID INTEGER NULL;

ALTER TABLE SI_SV_Survey ADD ServerEndDateDimID INTEGER NULL;

ALTER TABLE SI_SV_Survey ADD ServerStartTimeDimID INTEGER NULL;

ALTER TABLE SI_SV_Survey ADD ServerEndTimeDimID INTEGER NULL;

-- Version 4.01000
-- Cambios 2012-09-07: 
ALTER TABLE SI_SV_Question ADD QuestionMessage LONGTEXT;

ALTER TABLE SI_SV_Question ADD ExcludeSelected INTEGER;

UPDATE SI_SV_Question SET ExcludeSelected = 0 WHERE ExcludeSelected IS NULL;

ALTER TABLE SI_SV_Question ADD SourceSimpleQuestionID INTEGER NULL;

UPDATE SI_SV_Question SET SourceSimpleQuestionID = 0 WHERE SourceSimpleQuestionID IS NULL;

ALTER TABLE SI_SV_Question ADD SummaryTotal INTEGER NULL;

UPDATE SI_SV_Question SET SummaryTotal = 0 WHERE SummaryTotal IS NULL;

ALTER TABLE SI_SV_Question ADD MaxChecked INTEGER NULL;

UPDATE SI_SV_Question SET MaxChecked = 0 WHERE MaxChecked IS NULL;

-- 2012-10-19: Cambios para permitir preguntas tipo Read-Only
ALTER TABLE SI_SV_Question ADD ReadOnly INT NULL;

UPDATE SI_SV_Question SET ReadOnly = 0 WHERE ReadOnly IS NULL;

ALTER TABLE SI_SV_Question ADD Formula LONGTEXT NULL;

CREATE TABLE SI_SV_QuestionFormulaDep
(
  AutoEvalID INT NOT NULL,
  QuestionID INTEGER NOT NULL,
  EvalQuestionID INTEGER NOT NULL,
  PRIMARY KEY (AutoEvalID)
);

ALTER TABLE SI_SV_Question ADD HasReqDocument INTEGER;

UPDATE SI_SV_Question SET HasReqDocument = 0 WHERE HasReqDocument IS NULL;

ALTER TABLE SI_SV_Question ADD DocDimID INTEGER;

UPDATE SI_SV_Question SET DocDimID = 0 WHERE DocDimID IS NULL;

CREATE TABLE SI_SV_SurveyAnswerDocument
(
  SurveyID INTEGER,
  QuestionID INTEGER,
  FactKey BIGINT,
  MainFactKey BIGINT,
  PathDocument VARCHAR(255)
);

ALTER TABLE SI_SV_Question ADD ShowCondition LONGTEXT NULL;

UPDATE SI_SV_Question SET ShowCondition = '' WHERE ShowCondition IS NULL;

CREATE TABLE SI_SV_ShowConditionDep
(
  AutoEvalID INT NOT NULL,
  ElementID INTEGER NOT NULL,
  ElementType INTEGER NOT NULL,
  EvalElementID INTEGER NOT NULL,
  PRIMARY KEY (AutoEvalID)
);

-- Version 4.01001
-- Cambios 2013-01-10: Agregada la opción para utilizar registro único para secciones estándar
ALTER TABLE SI_SV_Survey ADD UseStdSectionSingleRec INTEGER NULL;

UPDATE SI_SV_Survey  SET UseStdSectionSingleRec = 0 WHERE UseStdSectionSingleRec IS NULL;

-- Version 4.02000
-- Cambios 2013-01-18: Agregada la opción para separar las preguntas de los catálogos que originan sus datos
ALTER TABLE SI_SV_Survey ADD DissociateCatDimens INTEGER NULL;

UPDATE SI_SV_Survey SET DissociateCatDimens = 0 WHERE DissociateCatDimens IS NULL;

ALTER TABLE SI_SV_Catalog ADD DissociateCatDimens INTEGER NULL;

UPDATE SI_SV_Catalog SET DissociateCatDimens = 0 WHERE DissociateCatDimens IS NULL;

ALTER TABLE SI_SV_Catalog ADD HierarchyID INTEGER NULL;

UPDATE SI_SV_Catalog SET HierarchyID = 0 WHERE HierarchyID IS NULL;

ALTER TABLE SI_SV_CatalogMember ADD IndDimID INTEGER NULL;

UPDATE SI_SV_CatalogMember SET IndDimID = 0 WHERE IndDimID IS NULL;

ALTER TABLE SI_SV_Question ADD ShowImageAndText INTEGER NULL;

UPDATE SI_SV_Question SET ShowImageAndText = 0 WHERE ShowImageAndText IS NULL;

-- Version 4.03005
ALTER TABLE SI_SV_Question ADD OtherLabel VARCHAR(50) NULL;
 
UPDATE SI_SV_Question SET OtherLabel = '' WHERE OtherLabel IS NULL;
 
ALTER TABLE SI_SV_Question ADD CommentLabel VARCHAR(50) NULL;
 
UPDATE SI_SV_Question SET CommentLabel = '' WHERE CommentLabel IS NULL;

ALTER TABLE SI_SV_Survey ADD OtherLabel VARCHAR(50) NULL;
 
UPDATE SI_SV_Survey SET OtherLabel = '' WHERE OtherLabel IS NULL;
 
ALTER TABLE SI_SV_Survey ADD CommentLabel VARCHAR(50) NULL;
 
UPDATE SI_SV_Survey SET CommentLabel = '' WHERE CommentLabel IS NULL;

-- Version 4.04002
-- Cambios 2013-04-18: Agregada la opción para mapear las preguntas de eForms con formularios de eBavel y grabar directamente en la forma de eBavel
CREATE TABLE SI_SV_SurveyFieldsMap
(
  FieldMapID INTEGER,
  SurveyID INTEGER,
  SurveyFieldID INTEGER,
  EBavelFieldID INTEGER,
  PRIMARY KEY (FieldMapID)
);

ALTER TABLE SI_SV_Question ADD eBavelFieldID INTEGER NULL;

UPDATE SI_SV_Question SET eBavelFieldID = 0 WHERE eBavelFieldID IS NULL;

ALTER TABLE si_sv_qanswers ADD COLUMN phoneNumber VARCHAR(255);

-- Version 4.04007
-- Cambios 2013-07-01: Mapeo de acciones a eBavel a través de una forma
CREATE TABLE SI_SV_SurveyActionFieldsMap
(
  FieldMapID INTEGER NOT NULL,
  SurveyID INTEGER NOT NULL,
  SurveyFieldID INTEGER,
  EBavelFieldID INTEGER,
  PRIMARY KEY (FieldMapID)
);

ALTER TABLE SI_SV_Survey ADD eBavelActionFormID INTEGER NULL;

UPDATE SI_SV_Survey SET eBavelActionFormID = 0 WHERE eBavelActionFormID IS NULL;

CREATE TABLE SI_SV_SurveyAnswerActionFieldsMap
(
  FieldMapID INTEGER NOT NULL,
  SurveyID INTEGER NOT NULL,
  QuestionID INTEGER NOT NULL,
  QuestionOptionID INTEGER NOT NULL,
  SurveyFieldID INTEGER,
  EBavelFieldID INTEGER,
  PRIMARY KEY (FieldMapID)
);

ALTER TABLE SI_SV_QAnswers ADD eBavelActionFormID INTEGER NULL;

UPDATE SI_SV_QAnswers SET eBavelActionFormID = 0 WHERE eBavelActionFormID IS NULL;

-- Cambios 2013-07-09: Mapeo de acciones a eBavel a través de una forma
CREATE TABLE SI_SV_SurveyQuestionActionFieldsMap
(
  FieldMapID INTEGER NOT NULL,
  SurveyID INTEGER NOT NULL,
  QuestionID INTEGER NOT NULL,
  SurveyFieldID INTEGER,
  EBavelFieldID INTEGER,
  PRIMARY KEY (FieldMapID)
);

ALTER TABLE SI_SV_Question ADD eBavelActionFormID INTEGER NULL;

UPDATE SI_SV_Question SET eBavelActionFormID = 0 WHERE eBavelActionFormID IS NULL;

-- Cambios 2013-07-23: Ocultar opciones de respuesta
ALTER TABLE SI_SV_QAnswers ADD Hidden INTEGER NULL;

UPDATE SI_SV_QAnswers SET Hidden = 0 WHERE Hidden IS NULL;

-- Cambios 2013-11-29 v4.04017: Auto-refresh de secciones formateadas
ALTER TABLE SI_SV_Section ADD AutoRedraw INTEGER NULL;

UPDATE SI_SV_Section SET AutoRedraw = 0 WHERE AutoRedraw IS NULL;

-- Cambios 2013-11-29 v4.04020: Atributos e Indicadores Accuracy, Latitude y Longitude (globales)
ALTER TABLE SI_SV_Survey ADD AccuracyAttribID INTEGER NULL;

UPDATE SI_SV_Survey SET AccuracyAttribID = 0 WHERE AccuracyAttribID IS NULL;

ALTER TABLE SI_SV_Survey ADD AccuracyIndID INTEGER NULL;

UPDATE SI_SV_Survey SET AccuracyIndID = 0 WHERE AccuracyIndID IS NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD LatitudeDimID INTEGER NULL;

UPDATE SI_SV_GblSurveyModel SET LatitudeDimID = 0 WHERE LatitudeDimID IS NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD LongitudeDimID INTEGER NULL;

UPDATE SI_SV_GblSurveyModel SET LongitudeDimID = 0 WHERE LongitudeDimID IS NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD AccuracyAttribID INTEGER NULL;

UPDATE SI_SV_GblSurveyModel SET AccuracyAttribID = 0 WHERE AccuracyAttribID IS NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD AccuracyIndID INTEGER NULL;

UPDATE SI_SV_GblSurveyModel SET AccuracyIndID = 0 WHERE AccuracyIndID IS NULL;

-- Cambios 2013-12-03 v4.04021: Compartir opciones de respuesta entre preguntas
ALTER TABLE SI_SV_Question ADD SharedQuestionID INTEGER NULL;

UPDATE SI_SV_Question SET SharedQuestionID = 0 WHERE SharedQuestionID IS NULL;

CREATE TABLE SI_SV_QAnswersMatch
(
  SourceQuestionID INTEGER NOT NULL,
  SourceConsecutiveID INTEGER NOT NULL,
  SharedQuestionID INTEGER NOT NULL,
  SharedConsecutiveID INTEGER NOT NULL,
  PRIMARY KEY (SourceQuestionID, SourceConsecutiveID, SharedQuestionID, SharedConsecutiveID)
);

-- Cambios 2013-12-09 v4.04022: Ocultar secciones basado en una condición
ALTER TABLE SI_SV_Section ADD ShowCondition LONGTEXT NULL;

UPDATE SI_SV_Section SET ShowCondition = '' WHERE ShowCondition IS NULL;

-- Cambios 2014-01-25 v4.04025: EMails adicionales para envio de reportes
ALTER TABLE SI_SV_Survey ADD AdditionalEMails VARCHAR(255);

UPDATE SI_SV_Survey SET AdditionalEMails = '' WHERE AdditionalEMails IS NULL;

-- Cambios 2014-01-27 v4.04026: Pregunta tipo GPS
ALTER TABLE SI_SV_Question ADD RegisterGPSAt INT NULL;

UPDATE SI_SV_Question SET RegisterGPSAt = 0 WHERE RegisterGPSAt IS NULL;

ALTER TABLE SI_SV_Question ADD LatitudeAttribID INT NULL;

ALTER TABLE SI_SV_Question ADD LongitudeAttribID INT NULL;

ALTER TABLE SI_SV_Question ADD AccuracyAttribID INT NULL;

-- Cambios 2014-03-11 v5.00000: Multiple Choice horizonal con imagen y rediseño
ALTER TABLE SI_SV_Question ADD InputWidth INT NULL;

UPDATE SI_SV_Question SET InputWidth = 0 WHERE InputWidth IS NULL;

ALTER TABLE SI_SV_Question ADD PictLayOut INT NULL;

UPDATE SI_SV_Question SET PictLayOut = 0 WHERE PictLayOut IS NULL;

ALTER TABLE SI_SV_QAnswers ADD HTMLText LONGTEXT;

UPDATE SI_SV_QAnswers SET HTMLText = '' WHERE HTMLText IS NULL;

ALTER TABLE SI_SV_Survey ADD SurveyImageText LONGTEXT;

UPDATE SI_SV_Survey SET SurveyImageText = '' WHERE SurveyImageText IS NULL;

-- Cambios 2014-04-01 v5.00000: Envio de mensajes Push desde el Admin de eForms
CREATE TABLE SI_SV_PushMessage (
  MessageID INTEGER NOT NULL,
  CreationDateID DATETIME NOT NULL, 
  CreationUserID INTEGER NOT NULL,
  UserID INTEGER NOT NULL,
  Title VARCHAR(100) NULL,
  PushMessage VARCHAR(500) NULL,
  State INT NOT NULL,
  PRIMARY KEY (CreationDateID, UserID)
);

CREATE TABLE SI_SV_PushMessageDevices (
  MessageID INT NOT NULL,
  DeviceID INT NOT NULL,
  PRIMARY KEY (MessageID, DeviceID)
);

-- Cambios 2014-05-05 v5.00003: Piloto Barcel
ALTER TABLE SI_SV_Survey ADD HideNavButtons INTEGER NULL;

UPDATE SI_SV_Survey SET HideNavButtons = 0 WHERE HideNavButtons IS NULL;

ALTER TABLE SI_SV_Survey ADD HideSectionNames INTEGER NULL;

UPDATE SI_SV_Survey SET HideSectionNames = 0 WHERE HideSectionNames IS NULL;

-- Cambios 2014-05-13 v5.00005: Catalogos mapeados de eBavel
ALTER TABLE SI_SV_Catalog ADD eBavelAppID INTEGER NULL;

UPDATE SI_SV_Catalog SET eBavelAppID = 0 WHERE eBavelAppID IS NULL;

ALTER TABLE SI_SV_Catalog ADD eBavelFormID INTEGER NULL;

UPDATE SI_SV_Catalog SET eBavelFormID = 0 WHERE eBavelFormID IS NULL;

ALTER TABLE SI_SV_CatalogMember ADD eBavelFieldID INTEGER NULL;

UPDATE SI_SV_CatalogMember SET eBavelFieldID = 0 WHERE eBavelFieldID IS NULL;

-- Cambios 2014-05-16 v5.00005: Opcion de Usar Mapa en simple choice de catalogo
ALTER TABLE SI_SV_Question ADD UseMap INTEGER NULL;

UPDATE SI_SV_Question SET UseMap = 0 WHERE UseMap IS NULL;

ALTER TABLE SI_SV_Question ADD MapRadio INTEGER NULL;

UPDATE SI_SV_Question SET MapRadio = 0 WHERE MapRadio IS NULL;

ALTER TABLE SI_SV_Question ADD CatMemberLatitudeID INTEGER NULL;

UPDATE SI_SV_Question SET CatMemberLatitudeID = 0 WHERE CatMemberLatitudeID IS NULL;

ALTER TABLE SI_SV_Question ADD CatMemberLongitudeID INTEGER NULL;

UPDATE SI_SV_Question SET CatMemberLongitudeID = 0 WHERE CatMemberLongitudeID IS NULL;

-- Cambios 2014-05-19 v5.00006: Secciones tipo inline
ALTER TABLE SI_SV_Section ADD SelectorQuestionID INTEGER NULL;

UPDATE SI_SV_Section SET SelectorQuestionID = 0 WHERE SelectorQuestionID IS NULL;

ALTER TABLE SI_SV_Section ADD DisplayMode INTEGER NULL;

UPDATE SI_SV_Section SET DisplayMode = 0 WHERE DisplayMode IS NULL;

-- Cambios 2014-05-23 v5.00006: Agregados los defaults para preguntas multiple choice
ALTER TABLE SI_SV_QAnswers ADD DefaultValue VARCHAR(255) NULL;

UPDATE SI_SV_QAnswers SET DefaultValue = '' WHERE DefaultValue IS NULL;

-- Cambios 2014-05-26 v5.00007: Agregado el switch de seleccion de las secciones Inline
ALTER TABLE SI_SV_Section ADD SwitchBehaviour INTEGER NULL;

UPDATE SI_SV_Section SET SwitchBehaviour = 0 WHERE SwitchBehaviour IS NULL;

-- Cambios 2014-05-27 v5.00007: Agregados los agrupadores de encuestas
CREATE TABLE SI_SV_Menu (
  MenuID INT(11) NOT NULL,
  MenuName VARCHAR(255) NULL,
  VersionNum INT(11) NULL,
  PRIMARY KEY (MenuID)
);

CREATE TABLE SI_SV_SurveyMenu (
  MenuID INT(11) NOT NULL,
  SurveyID INT(11) NOT NULL,
  VersionNum INT(11) NOT NULL,
  PRIMARY KEY (MenuID, SurveyID)
);

-- Cambios 2014-06-02 v5.00008: Agregado el switch de seleccion de las preguntas multiple choice, asi como el checkbox para MCH numericas o texto
ALTER TABLE SI_SV_Question ADD EnableCheckBox INTEGER NULL;

UPDATE SI_SV_Question SET EnableCheckBox = 0 WHERE EnableCheckBox IS NULL;

ALTER TABLE SI_SV_Question ADD SwitchBehaviour INTEGER NULL;

UPDATE SI_SV_Question SET SwitchBehaviour = 0 WHERE SwitchBehaviour IS NULL;

-- Cambios 2014-06-04 v5.00009: Agregado el header y footer formateado de las secciones
ALTER TABLE SI_SV_Section ADD HTMLHeader LONGTEXT NULL;

UPDATE SI_SV_Section SET HTMLHeader = '' WHERE HTMLHeader IS NULL;

ALTER TABLE SI_SV_Section ADD HTMLFooter LONGTEXT NULL;

UPDATE SI_SV_Section SET HTMLFooter = '' WHERE HTMLFooter IS NULL;

-- Cambios 2014-06-17 v5.00011: Agregada la personalizacion de la aplicacion
CREATE TABLE SI_SV_AppCustomization (
  TemplateID INTEGER NOT NULL,
  UserID INTEGER NOT NULL,
  SettingID INTEGER NOT NULL,
  SettingName VARCHAR(255),
  SettingValue VARCHAR(255),
  PRIMARY KEY (TemplateID, UserID, SettingID)
);

CREATE TABLE SI_SV_AppCustomizationTpls (
  TemplateID INTEGER NOT NULL,
  TemplateName VARCHAR(255) NULL,
  PRIMARY KEY (TemplateID)
);

-- Cambios 2014-06-20 v5.00012: Agregadas las secciones Inline con opciones fijas + secciones Recap
ALTER TABLE SI_SV_Section ADD OptionsText LONGTEXT NULL;

UPDATE SI_SV_Section SET OptionsText = '' WHERE OptionsText IS NULL;

ALTER TABLE SI_SV_Section ADD SectionFormID INTEGER NULL;

UPDATE SI_SV_Section SET SectionFormID = 0 WHERE SectionFormID IS NULL;

-- Cambios 2014-07-09 v4.04033: Permisos de acceso por perfil de usuario
CREATE TABLE SI_SV_SurveyProfile (
 ProfileID INTEGER NOT NULL,
 ProfileName VARCHAR(255) NULL,
 PRIMARY KEY (ProfileID)
);
 
CREATE TABLE SI_SV_SurveyRestriction (
 RestrictionID INTEGER NOT NULL,
 ProfileID INTEGER NOT NULL,
 SectionType INTEGER NULL,
 QuestionType INTEGER NULL,
 CatalogID INTEGER NULL,
 Description VARCHAR(255) NULL,
 PRIMARY KEY (RestrictionID)
);

ALTER TABLE SI_SV_Question ADD ShowInSummary INTEGER NULL;

UPDATE SI_SV_Question SET ShowInSummary = 0 WHERE ShowInSummary IS NULL;

-- Cambios 2014-07-30 v5.00017: Responsive Design en Secciones
CREATE TABLE SI_SV_SurveyHTML (
  SurveyID INTEGER NOT NULL,
  ObjectType INTEGER NOT NULL,
  ObjectID INTEGER NOT NULL,
  DeviceID INTEGER NOT NULL,
  FormattedText LONGTEXT NULL,
  HTMLHeader LONGTEXT NULL,
  HTMLFooter LONGTEXT NULL,
  ImageText LONGTEXT NULL,
  PRIMARY KEY (SurveyID, ObjectType, ObjectID, DeviceID)
);

-- Cambios 2014-07-30 v5.00017: Datos adicionales desde eBavel para secciones tipo Recap
CREATE TABLE SI_SV_SectionFilter
(
  FilterID INTEGER,
  SectionID INTEGER,
  FilterName VARCHAR(255),
  FilterText LONGTEXT,
  FilterLevels VARCHAR(255),
  PRIMARY KEY (FilterID)
);

-- Cambios 2014-08-15 v5.00018: SummaryInMDSection maestro detalle con summary en la seccion
ALTER TABLE SI_SV_Section ADD SummaryInMDSection INTEGER NULL;

UPDATE SI_SV_Section SET SummaryInMDSection = 0 WHERE SummaryInMDSection IS NULL;

-- v5.00018: Agregada la configuración para deshabilitar el grabado de formas
ALTER TABLE SI_SV_Survey ADD DisableEntry INTEGER NULL;

UPDATE SI_SV_Survey SET DisableEntry = 0 WHERE DisableEntry IS NULL;

-- v5.00018: Agregada la configuración para ocultar secciones del menú de navegación
ALTER TABLE SI_SV_Section ADD ShowInNavMenu INTEGER NULL;

UPDATE SI_SV_Section SET ShowInNavMenu = 1 WHERE ShowInNavMenu IS NULL;

ALTER TABLE SI_SV_Menu ADD MenuImageText LONGTEXT NULL;

UPDATE SI_SV_Menu SET MenuImageText = '' WHERE MenuImageText IS NULL;

-- Cambio para 5.00021 para tipo sketch
ALTER TABLE SI_SV_Question ADD CanvasHeight VARCHAR(5) NULL;

UPDATE SI_SV_Question SET CanvasHeight = '' WHERE CanvasHeight IS NULL;

ALTER TABLE SI_SV_Question ADD CanvasWidth VARCHAR(5) NULL;

UPDATE SI_SV_Question SET CanvasWidth = '' WHERE CanvasWidth IS NULL;

-- v5.00021: Agregada la version de modificacion y grabadas configuraciones con IDs en lugar de numeros
ALTER TABLE SI_SV_Survey ADD CreationVersion FLOAT NULL;

UPDATE SI_SV_Survey SET CreationVersion = 0 WHERE CreationVersion IS NULL;

ALTER TABLE SI_SV_Survey ADD LastVersion FLOAT NULL;

UPDATE SI_SV_Survey SET LastVersion = 0 WHERE LastVersion IS NULL;

ALTER TABLE SI_SV_Section ADD CreationUserID INTEGER NULL;

ALTER TABLE SI_SV_Section ADD CreationDateID DATETIME NULL;

ALTER TABLE SI_SV_Section ADD LastUserID INTEGER NULL;

ALTER TABLE SI_SV_Section ADD LastDateID DATETIME NULL;

ALTER TABLE SI_SV_Section ADD CreationVersion FLOAT NULL;

UPDATE SI_SV_Section SET CreationVersion = 0 WHERE CreationVersion IS NULL;

ALTER TABLE SI_SV_Section ADD LastVersion FLOAT NULL;

UPDATE SI_SV_Section SET LastVersion = 0 WHERE LastVersion IS NULL;

ALTER TABLE SI_SV_Question ADD CreationUserID INTEGER NULL;

ALTER TABLE SI_SV_Question ADD CreationDateID DATETIME NULL;

ALTER TABLE SI_SV_Question ADD LastUserID INTEGER NULL;

ALTER TABLE SI_SV_Question ADD LastDateID DATETIME NULL;

ALTER TABLE SI_SV_Question ADD CreationVersion FLOAT NULL;

UPDATE SI_SV_Question SET CreationVersion = 0 WHERE CreationVersion IS NULL;

ALTER TABLE SI_SV_Question ADD LastVersion FLOAT NULL;

UPDATE SI_SV_Question SET LastVersion = 0 WHERE LastVersion IS NULL;

ALTER TABLE SI_SV_QAnswers ADD CreationUserID INTEGER NULL;

ALTER TABLE SI_SV_QAnswers ADD CreationDateID DATETIME NULL;

ALTER TABLE SI_SV_QAnswers ADD LastUserID INTEGER NULL;

ALTER TABLE SI_SV_QAnswers ADD LastDateID DATETIME NULL;

ALTER TABLE SI_SV_QAnswers ADD CreationVersion FLOAT NULL;

UPDATE SI_SV_QAnswers SET CreationVersion = 0 WHERE CreationVersion IS NULL;

ALTER TABLE SI_SV_QAnswers ADD LastVersion FLOAT NULL;

UPDATE SI_SV_QAnswers SET LastVersion = 0 WHERE LastVersion IS NULL;

-- v5.00023: Cambios a secciones Inline
ALTER TABLE SI_SV_Section ADD ShowSelector INTEGER NULL;

UPDATE SI_SV_Section SET ShowSelector = 0 WHERE ShowSelector IS NULL AND SectionType <> 4;

UPDATE SI_SV_Section SET ShowSelector = 1 WHERE ShowSelector IS NULL AND SectionType = 4;

ALTER TABLE SI_SV_Section ADD ShowAsQuestion INTEGER NULL;

UPDATE SI_SV_Section SET ShowAsQuestion = 0 WHERE ShowAsQuestion IS NULL;

ALTER TABLE SI_SV_Question ADD ColumnWidth INTEGER NULL;

UPDATE SI_SV_Question SET ColumnWidth = 0 WHERE ColumnWidth IS NULL;

ALTER TABLE SI_SV_CatalogMember ADD KeyOfAgenda INTEGER NULL;
UPDATE SI_SV_CatalogMember SET KeyOfAgenda = 0 WHERE KeyOfAgenda IS NULL;

ALTER TABLE SI_SV_CatalogMember ADD AgendaLatitude INTEGER NULL;
UPDATE SI_SV_CatalogMember SET AgendaLatitude = 0 WHERE AgendaLatitude IS NULL;

ALTER TABLE SI_SV_CatalogMember ADD AgendaLongitude INTEGER NULL;
UPDATE SI_SV_CatalogMember SET AgendaLongitude = 0 WHERE AgendaLongitude IS NULL;

ALTER TABLE SI_SV_CatalogMember ADD AgendaDisplay INTEGER NULL;
UPDATE SI_SV_CatalogMember SET AgendaDisplay = 0 WHERE AgendaDisplay IS NULL;

ALTER TABLE SI_SV_Survey ADD IsAgendable INTEGER NULL;
UPDATE SI_SV_Survey SET IsAgendable = 0 WHERE IsAgendable IS NULL;

ALTER TABLE SI_SV_Survey ADD RadioForAgenda INTEGER NULL;
UPDATE SI_SV_Survey SET RadioForAgenda = 0 WHERE RadioForAgenda IS NULL;

ALTER TABLE SI_SV_SurveyAgenda ADD CheckinSurvey INTEGER NULL;
UPDATE SI_SV_SurveyAgenda SET CheckinSurvey = 0 WHERE CheckinSurvey IS NULL;

-- Cambios 2014-09-30 v5.00023: Filtros de atributos para markers en preguntas simple choice con mapa
CREATE TABLE SI_SV_QuestionFilter
(
  FilterID INTEGER NOT NULL,
  QuestionID INTEGER NOT NULL,
  FilterType INTEGER NOT NULL,
  FilterName VARCHAR(255) NULL,
  FilterText LONGTEXT NULL,
  FilterLevels VARCHAR(255) NULL,
  Color VARCHAR(10) NULL, 
  PRIMARY KEY (FilterID)
);

-- Cambios 2014-10-03 v5.00024: Preguntas tipo seccion
ALTER TABLE SI_SV_Question ADD SourceSectionID INTEGER NULL;

UPDATE SI_SV_Question SET SourceSectionID = 0 WHERE SourceSectionID IS NULL;

-- Cambios 2014-10-15 v5.00025: Sketch con imagen de fondo desde el server
ALTER TABLE SI_SV_SurveyHTML ADD SketchImage LONGTEXT NULL;

ALTER TABLE SI_SV_Question ADD SketchImage LONGTEXT NULL;

-- Cambios 2014-10-21 v5.00025: Origen de opciones de respuesta de secciones Inline
ALTER TABLE SI_SV_Section ADD SourceQuestionID INTEGER NULL;

UPDATE SI_SV_Section SET SourceQuestionID = 0 WHERE SourceQuestionID IS NULL;

ALTER TABLE SI_SV_Section ADD SourceSectionID INTEGER NULL;

UPDATE SI_SV_Section SET SourceSectionID = 0 WHERE SourceSectionID IS NULL;

-- Cambios 2014-10-22 v5.00025: Auto refresh de preguntas tipo mensaje
ALTER TABLE SI_SV_Question ADD AutoRedraw INTEGER NULL;

UPDATE SI_SV_Question SET AutoRedraw = 0 WHERE AutoRedraw IS NULL;

-- Cambios 2014-10-23 v5.00025: Mapeo de expresiones hacia eBavel
ALTER TABLE SI_SV_SurveyAnswerActionFieldsMap ADD DataID INTEGER NULL;

UPDATE SI_SV_SurveyAnswerActionFieldsMap SET DataID = 0 WHERE DataID IS NULL;

ALTER TABLE SI_SV_SurveyAnswerActionFieldsMap ADD MemberID INTEGER NULL;

UPDATE SI_SV_SurveyAnswerActionFieldsMap SET MemberID = 0 WHERE MemberID IS NULL;

ALTER TABLE SI_SV_SurveyAnswerActionFieldsMap ADD Formula VARCHAR(255);

UPDATE SI_SV_SurveyAnswerActionFieldsMap SET Formula = '' WHERE Formula IS NULL;

ALTER TABLE SI_SV_SurveyQuestionActionFieldsMap ADD DataID INTEGER NULL;

UPDATE SI_SV_SurveyQuestionActionFieldsMap SET DataID = 0 WHERE DataID IS NULL;

ALTER TABLE SI_SV_SurveyQuestionActionFieldsMap ADD MemberID INTEGER NULL;

UPDATE SI_SV_SurveyQuestionActionFieldsMap SET MemberID = 0 WHERE MemberID IS NULL;

ALTER TABLE SI_SV_SurveyQuestionActionFieldsMap ADD Formula VARCHAR(255);

UPDATE SI_SV_SurveyQuestionActionFieldsMap SET Formula = '' WHERE Formula IS NULL;

ALTER TABLE SI_SV_QAnswers ADD CancelAgenda INTEGER NULL;

UPDATE SI_SV_QAnswers SET CancelAgenda = 0 WHERE CancelAgenda IS NULL;

-- Cambios 2014-11-07: Modificacion al esquema de Agendas
ALTER TABLE SI_SV_SurveyAgenda ADD `Status` INTEGER NULL;

UPDATE SI_SV_SurveyAgenda SET `Status` = 0 WHERE `Status` IS NULL;

ALTER TABLE SI_SV_SurveyAgenda ADD EntryCheckInStartDate DATETIME NULL;

-- Editor HTML
-- Question (Editor HTML)
ALTER TABLE SI_SV_Question ADD EditorType INTEGER;
ALTER TABLE SI_SV_Question ADD QuestionMessageDes LONGTEXT;
ALTER TABLE SI_SV_SurveyHTML ADD QuestionMessageDes LONGTEXT;
-- Question Option (Editor HTML)
ALTER TABLE SI_SV_QAnswers ADD EditorType INTEGER;
ALTER TABLE SI_SV_QAnswers ADD HTMLTextDes LONGTEXT;
ALTER TABLE SI_SV_SurveyHTML ADD HTMLTextDes LONGTEXT;
-- Section (Editor HTML)
ALTER TABLE SI_SV_Section ADD EditorType INTEGER;
ALTER TABLE SI_SV_Section ADD FormattedTextDes LONGTEXT;
ALTER TABLE SI_SV_Section ADD HTMLHeaderDes LONGTEXT;
ALTER TABLE SI_SV_Section ADD HTMLFooterDes LONGTEXT;
ALTER TABLE SI_SV_SurveyHTML ADD FormattedTextDes LONGTEXT;
ALTER TABLE SI_SV_SurveyHTML ADD HTMLHeaderDes LONGTEXT;
ALTER TABLE SI_SV_SurveyHTML ADD HTMLFooterDes LONGTEXT;
-- Planificador (Editor HTML)
ALTER TABLE SI_SV_SurveyScheduler ADD EditorType INTEGER;
ALTER TABLE SI_SV_SurveyScheduler ADD EmailBodyDes LONGTEXT;

-- Cambios 2014-10-22 v5.00028: Mapeo de atributos de eBavel tipo Geolocation
ALTER TABLE SI_SV_CatalogMember ADD eBavelFieldDataID INTEGER NULL;

UPDATE SI_SV_CatalogMember SET eBavelFieldDataID = 0 WHERE eBavelFieldDataID IS NULL;

ALTER TABLE SI_SV_Question ADD IsInvisible INTEGER;

-- Cambios 2014-12-01 v5.00030: Imagen en atributo de catalogo
ALTER TABLE SI_SV_CatalogMember ADD IsImage INTEGER NULL;
UPDATE SI_SV_CatalogMember SET IsImage = 0 WHERE IsImage IS NULL;

-- Cambios 2014-12-19: Modificacion al esquema de Agendas
ALTER TABLE SI_SV_SurveyAgenda ADD AgendaSchedID INTEGER NULL;

-- Cambios 2014.12.02 v5.00032 #O5YTV8  agendas a través de planificación recurrente
CREATE TABLE IF NOT EXISTS SI_SV_SurveyAgendaScheduler (
    AgendaSchedID INTEGER NOT NULL,
    AgendaSchedName VARCHAR(255) NULL,
    UserID INTEGER NOT NULL,
    CatalogID INTEGER NULL,
    AttributeID INTEGER NULL,
    FilterText VARCHAR(255) NULL,
    CreationUserID INTEGER NULL,
    CreationDateID DATETIME NULL,
    LastUserID INTEGER NULL,
    LastDateID DATETIME NULL,
    VersionNum INTEGER NULL,
    CheckInSurvey INTEGER NULL,
    `Status` INTEGER NULL,
    CaptureStartTime VARCHAR(64),
    EntryCheckInStartDate DATETIME NULL,
    RecurPatternType VARCHAR(1) NULL,
    RecurPatternOpt VARCHAR(50) NULL,
    RecurPatternNum INTEGER NULL,
    RecurRangeOpt VARCHAR(50) NULL,
    Ocurrences INTEGER NULL,
    GenerateAgendasNextDayTime VARCHAR(64) NULL,
    PRIMARY KEY (AgendaSchedID)
);

CREATE TABLE IF NOT EXISTS SI_SV_SurveyAgendaSchedDet (
  `AgendaSchedID` INT(11) NOT NULL,
  `SurveyID` INT(11) NOT NULL,
  `Status` INT(11) DEFAULT NULL,
  PRIMARY KEY (`AgendaSchedID`,`SurveyID`)
);

-- 2015.02.10 JCEM cantidad de registros minimos necesarios en una seccion inline
ALTER TABLE SI_SV_Section ADD MinRecordsToSelect INTEGER NULL;
UPDATE SI_SV_Section SET MinRecordsToSelect = 0 WHERE MinRecordsToSelect IS NULL;

-- @AAL 19/03/2015 v5.01000: Agregado para soportar Formas y Vistas de eBavel --
ALTER TABLE SI_SV_Catalog ADD eBavelFormType INTEGER NULL;
-- @AAL 19/03/2015 v5.01000: La Forma es identificada por la constante 1, la vista por la constante 2 y 0 para ninguno de los casos  --
UPDATE SI_SV_Catalog SET eBavelFormType = 0 WHERE eBavelFormType IS NULL;

-- Cambios 2015-03-20: Condiciones de grabado de acciones
ALTER TABLE SI_SV_QAnswers ADD ActionCondition LONGTEXT NULL;

UPDATE SI_SV_QAnswers SET ActionCondition = '' WHERE ActionCondition IS NULL;

-- Cambios 2015-04-27: v5.01000 Actualización de código del App desde el servicio
CREATE TABLE SI_SV_AppUpdate (
	VersionNum INTEGER NOT NULL,
	UserID INTEGER NOT NULL,
	MinVersionToUpdate FLOAT NULL,
	MaxVersionToUpdate FLOAT NULL,
	Description LONGTEXT NULL,
	UpdateCode LONGTEXT NULL,
	PRIMARY KEY (VersionNum, UserID)
);

-- Cambios 2015-04-27: v6.00000 Rediseño v6
ALTER TABLE SI_SV_Survey ADD SchedulerID INTEGER NULL;

UPDATE SI_SV_Survey SET SchedulerID = 0 WHERE SchedulerID IS NULL;

ALTER TABLE SI_SV_Survey ADD SurveyDesc VARCHAR(255) NULL;

UPDATE SI_SV_Survey SET SurveyDesc = '' WHERE SurveyDesc IS NULL;

ALTER TABLE SI_SV_Question ADD QuestionTextHTML LONGTEXT NULL;

UPDATE SI_SV_Question SET QuestionTextHTML = '' WHERE QuestionTextHTML IS NULL;

ALTER TABLE SI_SV_Question ADD ValuesSourceType INTEGER NULL;

UPDATE SI_SV_Question SET ValuesSourceType = 0 WHERE ValuesSourceType IS NULL;

ALTER TABLE SI_SV_Section ADD SectionNameHTML LONGTEXT NULL;

UPDATE SI_SV_Section SET SectionNameHTML = '' WHERE SectionNameHTML IS NULL;

ALTER TABLE SI_SV_Section ADD ValuesSourceType INTEGER NULL;

UPDATE SI_SV_Section SET ValuesSourceType = 0 WHERE ValuesSourceType IS NULL;

-- Cambios 2015-07-15: v6.00000 Mapeo de cubos + Liga Catalogos vs DataSources
ALTER TABLE SI_SV_Catalog ADD DataSourceID INTEGER NULL;

UPDATE SI_SV_Catalog SET DataSourceID = 0 WHERE DataSourceID IS NULL;

ALTER TABLE SI_SV_CatalogMember ADD DataSourceMemberID INTEGER NULL;

UPDATE SI_SV_CatalogMember SET DataSourceMemberID = 0 WHERE DataSourceMemberID IS NULL;

CREATE TABLE SI_SV_QuestionMembers (
	QuestionID INTEGER NOT NULL,
	MemberID INTEGER NOT NULL,
	MemberOrder INTEGER NULL,
	PRIMARY KEY (QuestionID, MemberID)
);

CREATE TABLE SI_SV_SectionMembers (
	SectionID INTEGER NOT NULL,
	MemberID INTEGER NOT NULL,
	MemberOrder INTEGER NULL,
	PRIMARY KEY (SectionID, MemberID)
);

CREATE TABLE `si_sv_modelartus_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_modelartus` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `id_field` varchar(255) NOT NULL,
  `ebavel_table` varchar(255) NOT NULL,
  `ebavel_form` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_modelartus` (`id_modelartus`),
  KEY `id_question` (`id_question`),
  KEY `id_field` (`id_field`)
);

CREATE TABLE `si_sv_modelartus` (
  `id_modelartus` INT(11) NOT NULL AUTO_INCREMENT,
  `id_survey` INT(11) DEFAULT NULL,
  `cla_concepto` INT(11) DEFAULT NULL,
  `name` VARCHAR(255) DEFAULT NULL,
  `description` LONGTEXT,
  `createdUserKey` INT(11) DEFAULT NULL,
  `modifiedUserKey` INT(11) DEFAULT NULL,
  `createdDate` DATETIME DEFAULT NULL,
  `modifiedDate` DATETIME DEFAULT NULL,
  `syncDate` DATETIME DEFAULT NULL,
  `id_owner` INT(11) DEFAULT NULL,
  `cla_descrip_user` INT(11) DEFAULT NULL,
  `cla_descrip_sv` INT(11) DEFAULT NULL,
  `cla_descrip_sv_latitude` INT(11) DEFAULT NULL,
  `cla_descrip_sv_longitude` INT(11) DEFAULT NULL,
  `cla_descrip_sv_accuracy` INT(11) DEFAULT NULL,
  `cla_descrip_sv_date` INT(11) DEFAULT NULL,
  `cla_descrip_sv_hour` INT(11) DEFAULT NULL,
  `cla_descrip_sv_enddate` INT(11) DEFAULT NULL,
  `cla_descrip_sv_endhour` INT(11) DEFAULT NULL,
  `cla_descrip_version` INT(11) DEFAULT NULL,
  `cla_descrip_country` INT(11) DEFAULT NULL,
  `cla_descrip_state` INT(11) DEFAULT NULL,
  `cla_descrip_city` INT(11) DEFAULT NULL,
  `cla_descrip_zipcode` INT(11) DEFAULT NULL,
  `cla_descrip_address` INT(11) DEFAULT NULL,
  `cla_indicador_count` INT(11) DEFAULT NULL,
  `cla_indicador_elapsed` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id_modelartus`),
  KEY `id_survey` (`id_survey`),
  KEY `cla_concepto` (`cla_concepto`)
) AUTO_INCREMENT=1;

CREATE TABLE `si_sv_modelartus_questions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_modelartus` INT(11) NOT NULL,
  `id_section` INT(11) NOT NULL,
  `id_question` INT(11) NOT NULL,
  `cla_descrip` INT(11) DEFAULT NULL,
  `cla_indicador` INT(11) DEFAULT NULL,
  `cla_concepto` INT(11) DEFAULT NULL,
  `path` LONGTEXT,
  `type` VARCHAR(3) DEFAULT NULL,
  `cla_descrip_atributo` INT(11) DEFAULT NULL,
  `cla_descrip_foto` INT(11) DEFAULT NULL,
  `cla_descrip_documento` INT(11) DEFAULT NULL,
  `cla_descrip_comentario` INT(11) DEFAULT NULL,
  `cla_indicador_score` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_modelartus` (`id_modelartus`),
  KEY `id_section` (`id_section`),
  KEY `id_question` (`id_question`),
  KEY `cla_descrip` (`cla_descrip`),
  KEY `cla_indicador` (`cla_indicador`),
  KEY `cla_indicador_score` (`cla_indicador_score`)
) AUTO_INCREMENT=1;

ALTER TABLE SI_SV_Question ADD DataSourceID INTEGER NULL;

UPDATE SI_SV_Question SET DataSourceID = 0 WHERE DataSourceID IS NULL;

ALTER TABLE SI_SV_Question ADD DataSourceMemberID INTEGER NULL;

UPDATE SI_SV_Question SET DataSourceMemberID = 0 WHERE DataSourceMemberID IS NULL;

ALTER TABLE SI_SV_Section ADD DataSourceID INTEGER NULL;

UPDATE SI_SV_Section SET DataSourceID = 0 WHERE DataSourceID IS NULL;

ALTER TABLE SI_SV_Section ADD DataSourceMemberID INTEGER NULL;

UPDATE SI_SV_Section SET DataSourceMemberID = 0 WHERE DataSourceMemberID IS NULL;

CREATE TABLE `SI_SV_DataSource` (
  `DataSourceID` INTEGER NOT NULL DEFAULT 0,
  `DataSourceName` VARCHAR(255) DEFAULT NULL,
  `TableName` VARCHAR(255) DEFAULT NULL,
  `VersionNum` INTEGER DEFAULT NULL,
  `eBavelAppID` INTEGER DEFAULT NULL,
  `eBavelFormID` INTEGER DEFAULT NULL,
  `eBavelFormType` INTEGER DEFAULT NULL,
  `CreationUserID` INTEGER DEFAULT NULL,
  `LastModUserID` INTEGER DEFAULT NULL,
  `CreationDate` DATETIME DEFAULT NULL,
  `LastModDate` DATETIME DEFAULT NULL,
  `Type` INTEGER NULL,
  PRIMARY KEY (`DataSourceID`)
);

CREATE TABLE `SI_SV_DataSourceMember` (
  `DataSourceID` INTEGER DEFAULT NULL,
  `MemberID` INTEGER NOT NULL DEFAULT 0,
  `MemberName` VARCHAR(255) DEFAULT NULL,
  `fieldName` VARCHAR(255) DEFAULT NULL,
  `MemberOrder` INTEGER DEFAULT NULL,
  `KeyOfAgenda` INTEGER DEFAULT NULL,
  `AgendaLatitude` INTEGER DEFAULT NULL,
  `AgendaLongitude` INTEGER DEFAULT NULL,
  `AgendaDisplay` INTEGER DEFAULT NULL,
  `eBavelFieldID` INTEGER DEFAULT NULL,
  `eBavelDataID` INTEGER DEFAULT NULL,
  `eBavelFieldDataID` INTEGER DEFAULT NULL,
  `MemberType` INTEGER DEFAULT NULL,
  `CreationUserID` INTEGER DEFAULT NULL,
  `LastModUserID` INTEGER DEFAULT NULL,
  `CreationDate` DATETIME DEFAULT NULL,
  `LastModDate` DATETIME DEFAULT NULL,
  `IsImage` INTEGER NULL,
  PRIMARY KEY (`MemberID`)
);

-- Cambios 2015-07-18: v6.00000 Creación y edición de catálogos
ALTER TABLE SI_SV_Catalog ADD CreationVersion FLOAT NULL;

UPDATE SI_SV_Catalog SET CreationVersion = 0 WHERE CreationVersion IS NULL;

ALTER TABLE SI_SV_Catalog ADD LastVersion FLOAT NULL;

UPDATE SI_SV_Catalog SET LastVersion = 0 WHERE LastVersion IS NULL;

ALTER TABLE SI_SV_CatalogMember ADD CreationVersion FLOAT NULL;

UPDATE SI_SV_CatalogMember SET CreationVersion = 0 WHERE CreationVersion IS NULL;

ALTER TABLE SI_SV_CatalogMember ADD LastVersion FLOAT NULL;

UPDATE SI_SV_CatalogMember SET LastVersion = 0 WHERE LastVersion IS NULL;

ALTER TABLE SI_SV_Question ADD DataMemberLatitudeID INTEGER NULL;

UPDATE SI_SV_Question SET DataMemberLatitudeID = 0 WHERE DataMemberLatitudeID IS NULL;

ALTER TABLE SI_SV_Question ADD DataMemberLongitudeID INTEGER NULL;

UPDATE SI_SV_Question SET DataMemberLongitudeID = 0 WHERE DataMemberLongitudeID IS NULL;

-- Cambios 2015-07-24: Nuevas dimensiones del cubo global surveys
ALTER TABLE SI_SV_GblSurveyModel ADD StartDateEditDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD EndDateEditDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD StartTimeEditDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD EndTimeEditDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD SyncDateEndDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD SyncTimeEndDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD CountryDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD StateDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD CityDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD ZipCodeDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD AddressDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD SyncLatitudeDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD SyncLongitudeDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD SyncAccuracyDimID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD LastCheckDate DATETIME NULL;

-- Cambios 2015-07-30: Mapeos de DataDestinations
CREATE TABLE `si_sv_datadestinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_survey` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  `condition` LONGTEXT,
  `type` int(11) DEFAULT NULL,
  `eBavelForm` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`id_survey`)
);

CREATE TABLE `si_sv_datadestinationsmapping` (
  `id_datadestinations` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `fieldformid` varchar(255),
  `formula` LONGTEXT,
  PRIMARY KEY (`id_datadestinations`,`id_question`)
);

ALTER TABLE SI_SV_Question ADD DataSourceFilter LONGTEXT NULL;

UPDATE SI_SV_Question SET DataSourceFilter = '' WHERE DataSourceFilter IS NULL;

ALTER TABLE SI_SV_Section ADD DataSourceFilter LONGTEXT NULL;

UPDATE SI_SV_Section SET DataSourceFilter = '' WHERE DataSourceFilter is null;

CREATE TABLE `SI_SV_DataSourceFilter` (
  `FilterID` INT(11) NOT NULL DEFAULT 0,
  `CatalogID` INT(11) DEFAULT NULL,
  `FilterName` LONGTEXT DEFAULT NULL,
  `Status` INT NULL,
  PRIMARY KEY (`FilterID`)
);

CREATE TABLE `SI_SV_DataSourceFilter_Det` (
  `FilterdetID` INT(11) NOT NULL DEFAULT 0,
  `FilterID` INT(11) NOT NULL DEFAULT 0,
  `DataSourceMemberID` INT(11) DEFAULT NULL,
  `Value` LONGTEXT DEFAULT NULL,
  PRIMARY KEY (`FilterdetID`)
);

CREATE TABLE `SI_SV_DataSourceFilterRol` (
  `FilterID` INT(11) NOT NULL DEFAULT 0,
  `RolID` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`FilterID`,`RolID`)
);

CREATE TABLE `SI_SV_DataSourceFilterUser` (
  `FilterID` INT(11) NOT NULL DEFAULT 0,
  `UserID` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`FilterID`,`UserID`)
);

CREATE TABLE SI_SV_SurveyLog ( 
	CreationDateID DATETIME NOT NULL, 
	Repository VARCHAR(50) NOT NULL, 
	SurveyID INTEGER NOT NULL, 
	AgendaID INTEGER NOT NULL,
	UserID VARCHAR(255) NOT NULL, 
	WebMode INTEGER NULL, 
	Latitude DOUBLE NULL, 
	Longitude DOUBLE NULL, 
	Accuracy DOUBLE NULL,
	SyncLatitude DOUBLE NULL, 
	SyncLongitude DOUBLE NULL, 
	SyncAccuracy DOUBLE NULL,
	SurveyDate DATETIME NULL, 
	FileName VARCHAR(255) NULL, 
	FormsPath VARCHAR(255) NULL, 
	FormsVersion VARCHAR(50) NULL, 
	UserAgent LONGTEXT NULL,
	screenSize VARCHAR(255) NULL,
	platform VARCHAR(255) NULL,
	`Status` INTEGER DEFAULT 0,
	SessionID VARCHAR(60) NULL,
	DeviceName VARCHAR(60) NULL,
	UDID VARCHAR(100) NULL
);

CREATE INDEX idxSurvey on SI_SV_SurveyLog (SurveyID, UserID);

CREATE INDEX idxUser on SI_SV_SurveyLog (UserID , SurveyID);

CREATE INDEX idxCreationDate on SI_SV_SurveyLog (CreationDateID, SurveyID, UserID);

CREATE INDEX idxSurvey ON SI_SV_Section (SurveyID, SectionID);

CREATE INDEX idxSurvey ON SI_SV_Question (SurveyID, QuestionID);

ALTER TABLE SI_SV_DataSourceFilter ADD `Status` INT NULL;

ALTER TABLE si_sv_datadestinations ADD `id_connection` INT NULL;

ALTER TABLE si_sv_datadestinations ADD `fileformat` INT NULL;

ALTER TABLE si_sv_datadestinations ADD `path` VARCHAR(255);

CREATE TABLE `si_sv_servicesconnection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `access_token` varchar(1000) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `ftp` varchar(255) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE SI_SV_SurveyLog ADD Accuracy DOUBLE NULL;

ALTER TABLE SI_SV_SurveyLog ADD SyncAccuracy DOUBLE NULL;

ALTER TABLE SI_SV_SurveyLog ADD screenSize VARCHAR(255) NULL;

ALTER TABLE SI_SV_SurveyLog ADD platform VARCHAR(255) NULL;

ALTER TABLE SI_SV_SurveyLog ADD `Status` INTEGER DEFAULT 0;

ALTER TABLE SI_SV_SurveyLog ADD SessionID VARCHAR(60) NULL;

ALTER TABLE SI_SV_SurveyLog ADD DeviceName VARCHAR(60) NULL;

ALTER TABLE SI_SV_SurveyLog ADD UDID VARCHAR(100) NULL;

ALTER TABLE SI_SV_SurveyLog ADD DeviceID INTEGER NULL;

-- Cambios 2015-08-14: Borrado logico de capturas desde bitacora
ALTER TABLE SI_SV_SurveyLog ADD Deleted INTEGER NULL;

UPDATE SI_SV_SurveyLog SET Deleted = 0 WHERE Deleted IS NULL;

CREATE TABLE `si_sv_checkpoint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `si_sv_checkpointattr` (
  `checkpoint_id` int(11) NOT NULL,
  `attr_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`checkpoint_id`,`attr_id`,`type`)
);

ALTER TABLE si_sv_datadestinations ADD `real_path` VARCHAR(255);

ALTER TABLe SI_SV_Survey ADD SurveyMenuImage LONGTEXT;

ALTER TABLE si_sv_servicesconnection ADD `user_parameter` VARCHAR(255);

ALTER TABLE si_sv_servicesconnection ADD `pass_parameter` VARCHAR(255);

ALTER TABLE si_sv_servicesconnection ADD `type_request` VARCHAR(255);

ALTER TABLE si_sv_surveyagendascheduler ADD `checkpointID` INT NULL;

ALTER TABLE si_sv_surveyagendascheduler ADD `FilterTextAttr` VARCHAR(255) NULL;

ALTER TABLE SI_SV_Section ADD `hideNavButtons` INT NULL; 

-- Cambios 2015-08-28: Agregadas las versiones de modificacion de Agendas y Schedulers
ALTER TABLE SI_SV_SurveyAgenda ADD CreationVersion FLOAT NULL;

UPDATE SI_SV_SurveyAgenda SET CreationVersion = 0 WHERE CreationVersion IS NULL;

ALTER TABLE SI_SV_SurveyAgenda ADD LastVersion FLOAT NULL;

ALTER TABLE SI_SV_SurveyAgenda ADD checkpointID INT NULL;

ALTER TABLE SI_SV_SurveyAgenda ADD `FilterTextAttr` VARCHAR(255) NULL;

UPDATE SI_SV_SurveyAgenda SET LastVersion = 0 WHERE LastVersion IS NULL;

UPDATE SI_SV_SurveyAgenda SET LastVersion = 0 WHERE LastVersion IS NULL;

ALTER TABLE SI_SV_SurveyAgendaScheduler ADD CreationVersion FLOAT NULL;

UPDATE SI_SV_SurveyAgendaScheduler SET CreationVersion = 0 WHERE CreationVersion IS NULL;

ALTER TABLE SI_SV_SurveyAgendaScheduler ADD LastVersion FLOAT NULL;

UPDATE SI_SV_SurveyAgendaScheduler SET LastVersion = 0 WHERE LastVersion IS NULL;

-- Cambios 2015-09-07: Agregadas las fuentes externas de DataSources
ALTER TABLE SI_SV_DataSource ADD Document VARCHAR (255) NULL;

UPDATE SI_SV_DataSource SET Document = "" WHERE Document IS NULL;

ALTER TABLE SI_SV_DataSource ADD RecurPatternType VARCHAR (1) NULL;

ALTER TABLE SI_SV_DataSource ADD RecurPatternOpt VARCHAR (50) NULL;

ALTER TABLE SI_SV_DataSource ADD RecurPatternNum INT NULL;

ALTER TABLE SI_SV_DataSource ADD RecurRangeOpt VARCHAR (50) NULL;

ALTER TABLE SI_SV_DataSource ADD id_ServiceConnection INT NULL;

ALTER TABLE SI_SV_DataSource ADD LastSyncDate DATETIME NULL;

ALTER TABLE SI_SV_DataSource ADD SheetName VARCHAR(255) NULL;

ALTER TABLE SI_SV_DataSource ADD `Path` LONGTEXT NULL;

ALTER TABLE SI_SV_DataSource ADD RecurActive INT;

ALTER TABLE SI_SV_DataSource ADD `IdGoogleDrive` LONGTEXT NULL;

ALTER TABLE SI_SV_DataSource ADD `interval` INT;

ALTER TABLE SI_SV_DataSource ADD CaptureStartTime VARCHAR(64);

ALTER TABLE si_sv_servicesconnection MODIFY access_token LONGTEXT;

ALTER TABLE si_sv_datadestinationsmapping drop PRIMARY KEY;

-- Cambios 2015-11-20: v6.00025 Agregada la descripcion de errores de carga de catalogos
ALTER TABLE SI_SV_DataSource ADD LastSyncErrorDate DATETIME NULL;

ALTER TABLE SI_SV_DataSource ADD LastSyncError LONGTEXT NULL;

-- Cambios 2015-10-08: Agregados campos que no se habian incluido en el create table
ALTER TABLE si_sv_modelartus ADD cla_descrip_agenda INT NULL;

ALTER TABLE si_sv_modelartus_questions ADD cla_indicador_atributo INT NULL;

-- Funcionalidad de Enlazadores
CREATE TABLE SI_SV_Link
(
 LinkID INTEGER PRIMARY KEY,
 LinkName VARCHAR (255),
 Link LONGTEXT,
 Image LONGTEXT,
 Status INTEGER
);

ALTER TABLE SI_SV_datadestinations ADD cla_dimension INT NULL;

ALTER TABLE SI_SV_datadestinations ADD emailformat INT NULL;

-- Funcionalidad de preguntas OCR
ALTER TABLE SI_SV_Question ADD SavePhoto INTEGER NULL;

UPDATE SI_SV_Question SET SavePhoto = 0 WHERE SavePhoto IS NULL;

CREATE TABLE SI_SV_OCRQuestion
(
  OCRQuestionID INTEGER NOT NULL,
  OpenQuestionID INTEGER NOT NULL,
  QuestionOrder INTEGER NULL,
  PRIMARY KEY (OCRQuestionID, OpenQuestionID)
);

-- Cambio 2015-12-10: v6.00026 Validado que mantenga una longitud maxima historica
ALTER TABLE SI_SV_Question ADD MaxQLength INTEGER NULL;
ALTER TABLE si_sv_servicesconnection ADD parameters LONGTEXT NULL;
-- ALTER TABLE si_sv_servicesconnection DROP parameters;

ALTER TABLE SI_SV_Datasource ADD parameters LONGTEXT NULL;
ALTER TABLE SI_SV_Datasource ADD `servicedbname` LONGTEXT NULL;
ALTER TABLE SI_SV_Datasource ADD `servicetablename` LONGTEXT NULL;
ALTER TABLE SI_SV_datadestinations ADD parameters LONGTEXT NULL;

ALTER TABLE SI_SV_datadestinations ADD cla_dashboar INT NULL;
ALTER TABLE SI_SV_datadestinations ADD cla_ars INT NULL;

ALTER TABLE SI_SV_datasource ADD servicedbtype INT NULL;
-- ALTER TABLE SI_SV_datasource DROP servicedbtype INT NULL;

ALTER TABLE si_sv_servicesconnection ADD servicedbtype INT NULL;

ALTER TABLE si_sv_survey ADD incrementalFile VARCHAR(255) NULL;
ALTER TABLE si_sv_datadestinations ADD isIncremental INT DEFAULT 0;

-- Agregado para las preguntas HTML, header y footer, permite guardar el estado del editor.
ALTER TABLE SI_SV_Question ADD HTMLEditorState INTEGER DEFAULT 0;
ALTER TABLE SI_SV_Section ADD HTMLHEditorState INTEGER DEFAULT 0;
ALTER TABLE SI_SV_Section ADD HTMLFEditorState INTEGER DEFAULT 0;

ALTER TABLE SI_SV_servicesconnection ADD `servicename` LONGTEXT NULL;
UPDATE SI_SV_servicesconnection SET servicename = NAME WHERE servicename IS NULL;

CREATE TABLE `si_sv_incdatadestinations` (
  `datadestination_id` int(11) DEFAULT NULL,
  `surveyid` int(11) DEFAULT NULL,
  `surveykey` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  KEY `idx1` (`datadestination_id`,`surveyid`,`status`)
);

ALTER TABLE SI_SV_Question ADD ShowTotals INTEGER NULL;

UPDATE SI_SV_Question SET ShowTotals = 0 WHERE ShowTotals IS NULL;

ALTER TABLE SI_SV_Question ADD TotalsFunction VARCHAR(10) NULL;

UPDATE SI_SV_Question SET TotalsFunction = '' WHERE TotalsFunction IS NULL;

ALTER TABLE si_sv_datadestinations  ADD `additionaleMail` LONGTEXT NULL;

UPDATE SI_SV_Datasourcemember SET fieldName = CONCAT(fieldname,'_LONG') WHERE eBavelFieldDataID = 2 AND fieldname NOT LIKE '%_LONG';

UPDATE SI_SV_Datasourcemember SET fieldName = CONCAT(fieldname,'_LATI') WHERE eBavelFieldDataID = 1 AND fieldname NOT LIKE '%_LATI';

-- Cambio 2016-01-28: v6.01000 Agregado el LayOut independiente de cada forma en el App
ALTER TABLE SI_SV_Survey ADD Width INTEGER NULL;

UPDATE SI_SV_Survey SET Width = 0 WHERE Width IS NULL;

ALTER TABLE SI_SV_Survey ADD Height INTEGER NULL;

UPDATE SI_SV_Survey SET Height = 0 WHERE Height IS NULL;

ALTER TABLE SI_SV_Survey ADD TextPosition INTEGER NULL;

UPDATE SI_SV_Survey SET TextPosition = 0 WHERE TextPosition IS NULL;

ALTER TABLE SI_SV_Survey ADD TextSize VARCHAR(10) NULL;

UPDATE SI_SV_Survey SET TextSize = '' WHERE TextSize IS NULL;

ALTER TABLE SI_SV_Survey ADD CustomLayout LONGTEXT NULL;

UPDATE SI_SV_Survey SET CustomLayout = '' WHERE CustomLayout IS NULL;

ALTER TABLE SI_SV_Menu ADD Width INTEGER NULL;

UPDATE SI_SV_Menu SET Width = 0 WHERE Width IS NULL;

ALTER TABLE SI_SV_Menu ADD Height INTEGER NULL;

UPDATE SI_SV_Menu SET Height = 0 WHERE Height IS NULL;

ALTER TABLE SI_SV_Menu ADD TextPosition INTEGER NULL;

UPDATE SI_SV_Menu SET TextPosition = 0 WHERE TextPosition IS NULL;

ALTER TABLE SI_SV_Menu ADD TextSize VARCHAR(10) NULL;

UPDATE SI_SV_Menu SET TextSize = '' WHERE TextSize IS NULL;

ALTER TABLE SI_SV_Menu ADD CustomLayout LONGTEXT NULL;

UPDATE SI_SV_Menu SET CustomLayout = '' WHERE CustomLayout IS NULL;

ALTER TABLE SI_SV_SETTINGS MODIFY SettingValue LONGTEXT NULL;

-- Cambio 2016-02-12: v6.01000 Agregada configuracion para inicio de semana en Agenda Schedulers
ALTER TABLE SI_SV_SurveyAgendaScheduler ADD FirstDayOfWeek INTEGER NULL;

UPDATE SI_SV_SurveyAgendaScheduler SET FirstDayOfWeek = 0 WHERE FirstDayOfWeek IS NULL;

ALTER TABLE SI_SV_Question ADD AllowGallery INTEGER NULL;

UPDATE SI_SV_Question SET AllowGallery = 0 WHERE AllowGallery IS NULL;

ALTER TABLE SI_SV_Survey ADD ShowQNumbers INTEGER NULL;

UPDATE SI_SV_Survey SET ShowQNumbers = -1 WHERE ShowQNumbers IS NULL;

-- Cambio 2016-03-17: v6.01001 Agregadas configuraciones extendidas para preguntas simple choice
ALTER TABLE SI_SV_Question ADD CatMemberImageID INTEGER NULL;

UPDATE SI_SV_Question SET CatMemberImageID = 0 WHERE CatMemberImageID IS NULL;

ALTER TABLE SI_SV_Question ADD DataMemberImageID INTEGER NULL;

UPDATE SI_SV_Question SET DataMemberImageID = 0 WHERE DataMemberImageID IS NULL;

-- Cambio 2016-04-01: v6.01002 Agregada el tipo de pregunta Update Data
ALTER TABLE SI_SV_Question ADD DataDestinationID INTEGER NULL;

UPDATE SI_SV_Question SET DataDestinationID = 0 WHERE DataDestinationID IS NULL;

-- Cambio 2016-02-20: v6.02000 Agregada configuracion para cambiar apariencia del App en v6
CREATE TABLE SI_SV_TemplateStyle
(
    TemplateID INTEGER NOT NULL,
    ObjectType INTEGER NOT NULL,
    SectionType INTEGER NOT NULL,
    QuestionType INTEGER NOT NULL,
    DisplayType INTEGER NOT NULL,
    ElementType INTEGER NOT NULL,
    Style LONGTEXT NULL,
	SettingType INTEGER NULL,
    PRIMARY KEY (TemplateID,ObjectType,SectionType,QuestionType,DisplayType,ElementType)
);

CREATE TABLE SI_SV_TemplateStyleTpls (
  TemplateID INTEGER NOT NULL,
  TemplateName VARCHAR(255) NULL,
  PRIMARY KEY (TemplateID)
);

-- Correccion a la tabla de actualizaciones
ALTER TABLE SI_SV_AppUpdate ADD MinVersionToUpdate FLOAT NULL;

CREATE TABLE SI_SV_AppUpdateUsers (
	VersionNum INT NOT NULL,
	UserID INT NOT NULL,
	PRIMARY KEY (VersionNum, UserID)
);

ALTER TABLE SI_SV_AppUpdate MODIFY MinVersionToUpdate FLOAT NULL;

ALTER TABLE SI_SV_AppUpdate MODIFY MaxVersionToUpdate FLOAT NULL;

-- Cambio 2016-03-22: v6.02000 Agregada configuracion para cambiar apariencia del App en v6
ALTER TABLE SI_SV_Survey ADD TemplateID INTEGER NULL;

UPDATE SI_SV_Survey SET TemplateID = -1 WHERE TemplateID IS NULL;

-- Configuración para definir si el enlace se despliega en la ventana actual o en una ventana nueva
ALTER TABLE SI_SV_Link ADD DisplayMode INTEGER;

-- Cambio 2016-04-14: v6.02000 Incrementada la longitud de los campos para tamaño de canvas y opciones
ALTER TABLE SI_SV_Question MODIFY CanvasWidth VARCHAR(10) NULL;

ALTER TABLE SI_SV_Question MODIFY CanvasHeight VARCHAR(10) NULL;

CREATE TABLE SI_SV_SkipRules (
	SkipRuleID INTEGER NOT NULL,
	QuestionID INTEGER NOT NULL,
	RuleName VARCHAR(255) NULL,
	SkipCondition LONGTEXT not null,
	NextSectionID INTEGER NOT NULL,
	PRIMARY KEY (SkipRuleID)
);

-- OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
ALTER TABLE SI_SV_Question ADD NoFlow INTEGER NULL;

-- Cambio 2016-03-17: v6.01001 Agregadas configuraciones extendidas para preguntas simple choice metro
ALTER TABLE SI_SV_Question ADD CatMemberOrderID INTEGER NULL;

UPDATE SI_SV_Question SET CatMemberOrderID = 0 WHERE CatMemberOrderID IS NULL;

ALTER TABLE SI_SV_Question ADD DataMemberOrderID INTEGER NULL;

UPDATE SI_SV_Question SET DataMemberOrderID = 0 WHERE DataMemberOrderID IS NULL;

ALTER TABLE SI_SV_Question ADD DataMemberOrderDir INTEGER NULL;

UPDATE SI_SV_Question SET DataMemberOrderDir = 0 WHERE DataMemberOrderDir IS NULL;

ALTER TABLE SI_SV_Question ADD CustomLayout LONGTEXT NULL;

UPDATE SI_SV_Question SET CustomLayout = '' WHERE CustomLayout IS NULL;

CREATE TABLE `SI_SV_PushMsg` (
  `MessageID` int(11) NOT NULL AUTO_INCREMENT,
  `CreationDateID` datetime NOT NULL,
  `CreationUserID` int(11) NOT NULL,
  `DestinationType` int(11) NOT NULL,
  `DestinationID` int(11) NOT NULL,
  `Title` varchar(100) DEFAULT NULL,
  `PushMessage` varchar(500) DEFAULT NULL,
  `State` int(11) NOT NULL,
  PRIMARY KEY (`MessageID`),
  KEY `DateID` (`CreationDateID`),
  KEY `DestinationType` (`DestinationType`),
  KEY `DestinationID` (`DestinationID`)
);

ALTER TABLE `SI_SV_DataSourceFilter` ADD `FilterFormula` LONGTEXT NULL;

-- JAPR Cambio 2016-05-27: v6.02001 Agregada duracion de captura para las formas
ALTER TABLE SI_SV_Survey ADD PlannedDuration FLOAT NULL;

UPDATE SI_SV_Survey SET PlannedDuration = 0 WHERE PlannedDuration IS NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD TransferTimeIndID INTEGER NULL;

ALTER TABLE SI_SV_GblSurveyModel ADD TransferDistanceIndID INTEGER NULL;

ALTER TABLE SI_SV_SurveyAgendaDet ADD RealDuration DOUBLE NULL;

ALTER TABLE SI_SV_SurveyAgendaDet ADD RealLatitude DOUBLE NULL;

ALTER TABLE SI_SV_SurveyAgendaDet ADD RealLongitude DOUBLE NULL;

-- JAPR Cambio 2016-06-06: v6.02001 Modificado el tipo de dato para permitir texto
ALTER TABLE SI_SV_Question MODIFY ColumnWidth VARCHAR(20) NULL;

-- JAPR 2016-08-16: v6.02003 Modificado el esquema de saltos para que se controle el regreso con una pila
ALTER TABLE SI_SV_Survey ADD EnableNavHistory INTEGER NULL;

UPDATE SI_SV_Survey SET EnableNavHistory = -1 WHERE EnableNavHistory IS NULL;

-- JAPR 2016-08-22: v6.02003 Agregada la configuracion para autoajustar la imagen de las formas en el menú del App
ALTER TABLE SI_SV_Survey ADD AutoFitImage INTEGER NULL;

UPDATE SI_SV_Survey SET AutoFitImage = -1 WHERE AutoFitImage IS NULL;

ALTER TABLE SI_SV_Menu ADD AutoFitImage INTEGER NULL;

UPDATE SI_SV_Menu SET AutoFitImage = -1 WHERE AutoFitImage IS NULL;

-- MAPR 2018-02-20: v6.02020 

ALTER TABLE SI_SV_Settings ADD AllMobile INTEGER NULL;

UPDATE SI_SV_Settings SET AllMobile = -1 WHERE AllMobile IS NULL;

-- OMMC 2016-10-07: Agregado el campo de regID a la tabla de mensajes push
ALTER TABLE SI_SV_PushMsg ADD RegID VARCHAR(255) NULL;

-- JAPR 2016-10-14: Modificado el esquema de Push para enviar mensajes al ultimo dispositivo utilizado por el usuario solamente
CREATE TABLE SI_SV_PushMsgDet (
	MessageID INTEGER NOT NULL,
	UserID INTEGER NOT NULL,
	State INTEGER NULL,
	RegID VARCHAR(255) NULL,
	ErrorDesc LONGTEXT NULL,
	PRIMARY KEY (MessageID, UserID)
);

CREATE TABLE SI_SV_PushMsgUserGroups (
	MessageID INTEGER NOT NULL,
	UserGroupID INTEGER NOT NULL,
	PRIMARY KEY (MessageID, UserGroupID)
);

ALTER TABLE SI_SV_PushMsg MODIFY RegID VARCHAR(255);

ALTER TABLE si_sv_datadestinations ADD httpSendSinglePOST INT DEFAULT 0;

-- //OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
ALTER TABLE SI_SV_Question ADD ImageHeight VARCHAR(10) NULL;

UPDATE SI_SV_Question SET ImageHeight = '' WHERE ImageHeight IS NULL;

ALTER TABLE SI_SV_Question ADD ImageWidth VARCHAR(10) NULL;

UPDATE SI_SV_Question SET ImageWidth = '' WHERE ImageWidth IS NULL;

-- JAPR 2017-02-08: Agregado el proceso de generacion de forma de desarrollo para sincronizar a forma de produccion
ALTER TABLE SI_SV_Survey ADD SourceID INTEGER NULL;

UPDATE SI_SV_Survey SET SourceID = 0 WHERE SourceID IS NULL;

ALTER TABLE SI_SV_Survey ADD FormType INTEGER NULL;

UPDATE SI_SV_Survey SET FormType = 0 WHERE FormType IS NULL;

ALTER TABLE SI_SV_Section ADD SourceID INTEGER NULL;

UPDATE SI_SV_Section SET SourceID = 0 WHERE SourceID IS NULL;

ALTER TABLE SI_SV_Question ADD SourceID INTEGER NULL;

UPDATE SI_SV_Question SET SourceID = 0 WHERE SourceID IS NULL;

ALTER TABLE SI_SV_QAnswers ADD SourceID INTEGER NULL;

UPDATE SI_SV_QAnswers SET SourceID = 0 WHERE SourceID IS NULL;

-- JAPR 2017-02-21: v6.02016 Agregados los campos de auditoria de creacion y modificacion
ALTER TABLE SI_SV_SkipRules ADD CreationUserID INTEGER NULL;

ALTER TABLE SI_SV_SkipRules ADD CreationDateID DATETIME NULL;

ALTER TABLE SI_SV_SkipRules ADD LastUserID INTEGER NULL;

ALTER TABLE SI_SV_SkipRules ADD LastDateID DATETIME NULL;

ALTER TABLE SI_SV_SkipRules ADD CreationVersion FLOAT NULL;

UPDATE SI_SV_SkipRules SET CreationVersion = 0 WHERE CreationVersion IS NULL;

ALTER TABLE SI_SV_SkipRules ADD LastVersion FLOAT NULL;

UPDATE SI_SV_SkipRules SET LastVersion = 0 WHERE LastVersion IS NULL;

ALTER TABLE SI_SV_SurveyFilter ADD CreationUserID INTEGER NULL;

ALTER TABLE SI_SV_SurveyFilter ADD CreationDateID DATETIME NULL;

ALTER TABLE SI_SV_SurveyFilter ADD LastUserID INTEGER NULL;

ALTER TABLE SI_SV_SurveyFilter ADD LastDateID DATETIME NULL;

ALTER TABLE SI_SV_SurveyFilter ADD CreationVersion FLOAT NULL;

UPDATE SI_SV_SurveyFilter SET CreationVersion = 0 WHERE CreationVersion IS NULL;

ALTER TABLE SI_SV_SurveyFilter ADD LastVersion FLOAT NULL;

UPDATE SI_SV_SurveyFilter SET LastVersion = 0 WHERE LastVersion IS NULL;

-- JAPR 2017-04-19: v6.02020 Agregadas las descripciones de las capturas basadas en una pregunta o expresion de la forma
ALTER TABLE SI_SV_Survey ADD QuestionIDForEntryDesc INTEGER NULL;

UPDATE SI_SV_Survey SET QuestionIDForEntryDesc = 0 WHERE QuestionIDForEntryDesc IS NULL;

-- JAPR 2017-12-19: Agregados ALTER TABLE que se estaban ejecutando directo en código pero provocaban table locks, para esta fecha toda BD de eForms ya deben tener
-- estos campos, pero se integran al script por si alguna BD mas vieja se migra de versión
ALTER TABLE SI_SV_AppLog ADD AppSessionID VARCHAR(20) NULL;

ALTER TABLE SI_SV_AppLog ADD EventID INTEGER NULL;

ALTER TABLE SI_SV_AppLog ADD EventStepID INTEGER NULL;

ALTER TABLE SI_SV_AppLog ADD ConnectionType VARCHAR(50) NULL;

ALTER TABLE SI_SV_AppLog MODIFY COLUMN AgendaID INTEGER NULL;

ALTER TABLE SI_SV_AppLog MODIFY COLUMN SurveyID INTEGER NULL;

ALTER TABLE SI_SV_AppLog MODIFY COLUMN EventDateID DATETIME NULL;

ALTER TABLE SI_SV_AppLog MODIFY COLUMN UploadDateID DATETIME NULL;

ALTER TABLE SI_SV_AppLog MODIFY COLUMN GMTZone INTEGER NULL;

ALTER TABLE SI_SV_Survey ADD ReloadSurveyFromSection INTEGER NULL;

UPDATE SI_SV_Survey SET ReloadSurveyFromSection = 0 WHERE ReloadSurveyFromSection IS NULL;

-- JAPR 2018-01-16: v6.02022 Agregada la configuración para el subject y body de los destinos tipo EMail (#)
ALTER TABLE si_sv_datadestinations  ADD `emailsubject` LONGTEXT NULL;

ALTER TABLE si_sv_datadestinations  ADD `emailbody` LONGTEXT NULL;

-- JAPR 2018-05-07: v6.02025 Optimizada la carga de eForms (#)
ALTER TABLE SI_SV_DataSource ADD DataVersionDate DateTIME NULL;

ALTER TABLE SI_SV_Catalog ADD DataVersionDate DateTIME NULL;

-- JAPR 2018-11-05: v6.02028 Agregada la configuración para agregar la tr de la descripción del registor en lugar de la columna correspondiente (#VU53QY)
ALTER TABLE SI_SV_Section ADD UseDescriptorRow INTEGER NULL;

-- JAPR 2017-04-19: v6.02020 Agregadas las descripciones de las capturas basadas en una pregunta o expresion de la forma
ALTER TABLE SI_SV_Survey ADD ReloadSurveyFromSection INTEGER NULL;

UPDATE SI_SV_Survey SET ReloadSurveyFromSection = 0 WHERE ReloadSurveyFromSection IS NULL;

-- JAPR 2019-03-05: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
ALTER TABLE SI_SV_Question ADD UploadMediaFiles INTEGER NULL;

-- JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
CREATE TABLE SI_SV_SurveyAnswerImageSrc 
(
  SurveyID integer,
  QuestionID integer,
  FactKey bigint,
  MainFactKey bigint,
  PathImage VARCHAR(255)
);

-- JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
ALTER TABLE SI_SV_DataDestinationsMapping ADD additional_data INTEGER NULL;

ALTER TABLE SI_SV_Section ADD XPosQuestionID INTEGER NULL;

ALTER TABLE SI_SV_Section ADD YPosQuestionID INTEGER NULL;

ALTER TABLE SI_SV_Section ADD ItemNameQuestionID INTEGER NULL;

-- OMMC 2019-03-25: Pregunta AR #4HNJD9
ALTER TABLE SI_SV_DataSource ADD IsModel INTEGER NULL;
ALTER TABLE SI_SV_Catalog ADD IsModel INTEGER NULL;
ALTER TABLE SI_SV_Question ADD DataMemberZipFileID INTEGER NULL;
ALTER TABLE SI_SV_Question ADD CatMemberZipFileID INTEGER NULL;
ALTER TABLE SI_SV_Question ADD DataMemberVersionID INTEGER NULL;
ALTER TABLE SI_SV_Question ADD CatMemberVersionID INTEGER NULL;
ALTER TABLE SI_SV_Question ADD DataMemberPlatformID INTEGER NULL;
ALTER TABLE SI_SV_Question ADD CatMemberPlatformID INTEGER NULL;
ALTER TABLE SI_SV_Question ADD DataMemberTitleID INTEGER NULL;
ALTER TABLE SI_SV_Question ADD CatMemberTitleID INTEGER NULL;

-- JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
CREATE INDEX idxDataSource ON SI_SV_Catalog(DataSourceID);

CREATE INDEX idxCatalogID ON SI_SV_Question(CatalogID);

CREATE INDEX idxCatalogID ON SI_SV_Section(CatalogID);

-- JAPR 2019-08-08: v6.02035 Agregada la posibilidad de almacenar colores personalizados en algunos componentes de diseño del usuario (#BT0NMN)
CREATE TABLE SI_SV_EditorCustomColors (
	UserID INTEGER NOT NULL,
	ColorID INTEGER NOT NULL,
	Color VARCHAR(10) NULL,
	PRIMARY KEY (UserID, ColorID)
);

ALTER TABLE si_sv_datadestinations  ADD `additionaleMailComposite` LONGTEXT NULL;

-- JAPR 2019-07-16: v6.02035 Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
ALTER TABLE SI_SV_Question ADD RecalculateDependencies INTEGER NULL;

UPDATE SI_SV_Question SET RecalculateDependencies = 0 WHERE RecalculateDependencies IS NULL;

-- JAPR 2019-09-11: v6.02036 Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
ALTER TABLE SI_SV_Question ADD AutoSelectEmptyOpt INTEGER NULL;

UPDATE SI_SV_Question SET AutoSelectEmptyOpt = 0 WHERE AutoSelectEmptyOpt IS NULL;
