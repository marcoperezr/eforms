CREATE TABLE `enc_atributo_usuario` (
  `id_usuario_atr` int(11) NOT NULL default '0',
  `secuencia` decimal(8,4) default NULL,
  `resp_texto` varchar(255) default NULL,
  `estatus` tinyint(4) default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_encuesta` int(11) NOT NULL,
  `id_registro` int(11) NOT NULL,
  `id_registro_seq` int(11) NOT NULL,
  PRIMARY KEY USING BTREE (`id_usuario_atr`,`id_encuesta`,`id_registro`,`id_registro_seq`)
);

CREATE TABLE `enc_ayuda` (
  `ayuda_texto` varchar(400) character set utf8 default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_ayuda` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id_ayuda`)
);

CREATE TABLE `enc_control` (
  `tipo_control` tinyint(4) default NULL ,
  `descripcion` varchar(100) default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_control` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id_control`)
);

CREATE TABLE `enc_encuesta_det` (
  `id_seccion` int(11) default NULL,
  `secuencia` decimal(8,4) default NULL,
  `foto_req` tinyint(4) default NULL,
  `id_foto` int(11) default NULL,
  `id_control` int(11) default NULL,
  `id_ayuda` int(11) default NULL,
  `comentario_opc` tinyint(4) default NULL,
  `pregunta_opc` tinyint(4) default NULL,
  `pregunta` varchar(1000) character set utf8 default NULL,
  `respuesta_opc` tinyint(4) default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_encuesta` int(11) NOT NULL,
  `id_registro` int(11) NOT NULL,
  PRIMARY KEY  (`id_encuesta`,`id_registro`),
  KEY `fk_ref_det_enc_ayuda` (`id_ayuda`),
  KEY `fk_ref_det_enc_foto` (`id_foto`),
  KEY `fk_ref_enc_det_ctrol` (`id_control`),
  KEY `fk_ref_enc_sec` (`id_seccion`),
  KEY `id_registro_idx` (`id_registro`)
);

CREATE TABLE `enc_encuesta_det_control` (
  `columna_renglon` tinyint(4) default NULL,
  `titulo` varchar(255) default NULL,
  `secuencia` decimal(8,4) default NULL,
  `tipo_respuesta` tinyint(4) default NULL,
  `estatus` tinyint(1) default NULL,
  `sig_pregunta` int(11) default '0',
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_encuesta` int(11) NOT NULL,
  `id_registro` int(11) NOT NULL,
  `id_registro_seq` int(11) NOT NULL
);

CREATE TABLE `enc_encuesta_gral` (
  `descripcion` varchar(100) default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_encuesta` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id_encuesta`)
);

CREATE TABLE `enc_encuesta_res_det` (
  `id_folio` int(11) NOT NULL default '0',
  `id_seccion` int(11) default NULL,
  `secuencia` decimal(8,4) default NULL,
  `secuencia_col` decimal(8,4) default NULL,
  `secuencia_reng` decimal(8,4) default NULL,
  `respuesta` varchar(4000) character set utf8 default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_encuesta` int(11) NOT NULL,
  `id_registro` int(11) NOT NULL,
  `id_registro_seq` int(11) NOT NULL,
  PRIMARY KEY  (`id_folio`,`id_encuesta`,`id_registro`,`id_registro_seq`),
  KEY `fk_ref_enc_det_gral` (`id_folio`),
  KEY `fk_ref_res_enc` (`id_encuesta`,`id_registro`)
);

CREATE TABLE `enc_encuesta_res_gral` (
  `id_folio` int(11) NOT NULL,
  `trasmitido` tinyint(4) default NULL,
  `id_estatus` tinyint(4) default NULL,
  `posicion` varchar(40) default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `fecha_servidor` datetime default NULL,
  PRIMARY KEY  (`id_folio`)
);

CREATE TABLE `enc_fotos` (
  `escape_foto` longblob,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_foto` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id_foto`)
);

CREATE TABLE `enc_params_encuesta` (
  `tipo_param` varchar(6) NOT NULL,
  `id_param` int(11) NOT NULL,
  `c_fecha` datetime default NULL,
  `c_texto` varchar(255) default NULL,
  `c_integer` int(11) default NULL,
  `c_money` float(8,2) default NULL,
  `c_decimal` decimal(16,6) default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  PRIMARY KEY  (`tipo_param`,`id_param`)
);

CREATE TABLE `enc_res_coment_resp` (
  `comentario` varchar(4000) character set utf8 default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_folio` int(11) NOT NULL,
  `id_encuesta` int(11) NOT NULL,
  `id_registro` int(11) NOT NULL,
  `coment_resp` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id_folio`,`id_encuesta`,`id_registro`,`coment_resp`),
  KEY `fk_ref_com_resp_res_det` (`id_encuesta`,`id_registro`)
);

CREATE TABLE `enc_res_imagen` (
  `escape_foto` longblob,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_folio` int(11) NOT NULL,
  `id_encuesta` int(11) NOT NULL,
  `id_registro` int(11) NOT NULL,
  PRIMARY KEY  (`id_folio`,`id_encuesta`,`id_registro`)
);

CREATE TABLE `enc_seccion` (
  `descripcion` varchar(60) default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  `id_seccion` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id_seccion`)
);

CREATE TABLE `enc_usuario` (
  `usuario` varchar(128) default NULL,
  `password` varchar(16) default NULL,
  `nombre_usuario` varchar(60) default NULL,
  `id_cia` int(11) default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) NOT NULL auto_increment,
  `permiso_carga` tinyint(1) default NULL,
  `permiso_descarga` tinyint(1) default NULL,
  PRIMARY KEY  (`id_usuario`)
);

CREATE TABLE `enc_usuario_enc_proc` (
  `id_usuario` int(11) default NULL,
  `id_encuesta` int(11) default NULL,
  `fecha_modif` datetime default NULL,
  UNIQUE KEY `enc_usu_enc_proc_i1` (`id_usuario`,`id_encuesta`)
);

CREATE TABLE `enc_usuario_encuesta` (
  `id_encuesta` int(11) NOT NULL,
  `id_usuario_encuesta` int(11) NOT NULL,
  `estatus` tinyint(1) default NULL,
  `fecha_alta` datetime default NULL,
  `fecha_baja` datetime default NULL,
  `fecha_modif` datetime default NULL,
  `id_usuario` int(11) default NULL,
  PRIMARY KEY  (`id_encuesta`,`id_usuario_encuesta`),
  KEY `fk_ref_enc_user` (`id_usuario_encuesta`)
);

