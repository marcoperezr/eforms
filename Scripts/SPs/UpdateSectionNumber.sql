DELIMITER $$

DROP PROCEDURE IF EXISTS SV_UpdateSectionNumber $$

CREATE PROCEDURE SV_UpdateSectionNumber
(
)
BEGIN
	DECLARE myIsNullSectionNumber INTEGER;
	DECLARE mySurveyID INTEGER;
	DECLARE myLastSurveyID INTEGER;
	DECLARE mySectionID INTEGER;
	DECLARE mySectionNumber INTEGER;
	DECLARE	myFlagSections INTEGER;
	DECLARE FETCH_STATUS INTEGER;

	DECLARE SectionsCursor CURSOR FOR
	SELECT A.SurveyID, A.SectionID FROM SI_SV_Section A, SI_SV_Question B 
	WHERE A.SectionID = B.SectionID GROUP BY A.SurveyID, A.SectionID ORDER BY A.SurveyID, B.QuestionNumber;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET FETCH_STATUS = 1;
	
	SET mySurveyID = 0;
	SET myLastSurveyID = 0;
	SET mySectionID = 0;
	SET mySectionNumber = 0;
	SET FETCH_STATUS = 0;

	SELECT SectionNumber INTO myIsNullSectionNumber FROM SI_SV_Section LIMIT 1;
	
	-- Si es nulo el valor eso quiere decir q no se ha realizado la actualizacion de SectionNumber en la tabla SI_SV_Section
	IF (myIsNullSectionNumber IS NULL) THEN
		-- Abrimos cursor de las secciones
		OPEN SectionsCursor;
		FETCH SectionsCursor
		INTO mySurveyID,
		mySectionID;
		
		IF (FETCH_STATUS = 0) THEN
			SET myFlagSections = 0;
		ELSE
			SET myFlagSections = 1;
		END IF;

		SectionsCursor_Loop:WHILE (myFlagSections = 0) DO
			
			IF (myLastSurveyID <> mySurveyID) THEN
				SET mySectionNumber = 0;
			END IF;
			
			SET mySectionNumber = mySectionNumber + 1;
			
			UPDATE SI_SV_Section SET SectionNumber = mySectionNumber WHERE SurveyID = mySurveyID AND SectionID = mySectionID;
			
			SET myLastSurveyID = mySurveyID;

		FETCH SectionsCursor
		INTO mySurveyID,
		mySectionID;

		IF (FETCH_STATUS = 0) THEN
			SET myFlagSections = 0;
		ELSE
			SET myFlagSections = 1;
		END IF;

		END WHILE SectionsCursor_Loop;

		CLOSE SectionsCursor;

	END IF;

END $$

DELIMITER ;