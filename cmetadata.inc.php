<?php

define ('PK_HOUR', 0);
define ('PK_DAY', 1);
define ('PK_WEEK', 2);
define ('PK_TWOWEEK', 3);
define ('PK_MONTH', 4);
define ('PK_YEAR', 5);
define ('PK_WEEK_MONTH', 6);

define ('DATE_FORMAT', 'Y-m-d');

$ComCube = array();

class BITAMCINPC
{
	public $fecha;
	public $inpc;
}

class BITAMCOperand
{
	public $key;
	public $name;
	public $is_reexp = false;
}

class BITAMCIndicatorOperand
{
	public $key;
	public $format;
	public $formula;
	public $indicator_key = -1;
}

class BITAMCAgregation
{
	public $key;
	public $physical_name;
}

class BITAMCConstant
{
	public $key;
	public $name;
	public $value;
	public $type;
}

class BITAMCDatesRange
{
	public $range_bottom;
	public $range_top;
	public $description = '';
}

class BITAMCPeriod
{
	public $key;
	public $type;
	public $name;
	public $base;
	public $date_ref;
	public $format;
	public $relative_to;
	public $catalog;
	public $variable;
	public $a_data = array();
	
	function GetVPeriodRange($fecha, &$DatesR, $avance = 0)
	{
		$cont = count( $this->a_data );
		for ($index = 0; $index < $cont; $index++ )
		{
			if ( $fecha >= $this->a_data[$cont]->range_bottom and 
				 $fecha <= $this->a_data[$cont]->range_top )
			{
				$avance = $avance + $index;
		        if ($avance >= 0 or $avance < $cont ) 
		        {
		            $DatesR->range_bottom = $this->a_data[$avance]->range_bottom;
		            $DatesR->range_top = $this->a_data[$avance]->range_top;
		            $DatesR->description = $this->a_data[$avance]->description;
		            return true;
				}
				else
					return false;
			}
		}
		return false;
	}

	
	function GetPeriodRange( $poCurDate, $pnPeriodsShift )
	{
		
/*
		if ($this->type == 4 and $this->base == 3)
		{
			print '<br>Cuarto<br>';
		}
*/		
		
		if (is_null($poCurDate) or is_null($pnPeriodsShift))
		{
			return;
		}
		
		
		$oRange = new BITAMCDatesRange();
		$oRange->range_bottom = $poCurDate->range_bottom;
		$oRange->range_top = $poCurDate->range_top;	
		
		
		if ($this->variable)
		{
// Periodo Variable
			if ($this->GetVPeriodRange($poCurDate->range_bottom, $oRange, $pnPeriodsShift))
			{
				return $oRange;
			}
			return;
		}
		
		$year_bottom = intval(substr($oRange->range_bottom, 0, 4));
		$month_bottom = intval(substr($oRange->range_bottom, 5, 2));
		
		$day = intval(substr($oRange->range_bottom, 8, 2));

		$ntimes = $this->base;

//		print '<br>$poCurDate->range_bottom = '.$poCurDate->range_bottom.'<br>';
//		print '<br>$oRange->range_top = '.$oRange->range_top.'<br>';		
		
		
		switch ( $this->type )
		{
		    case PK_YEAR: 
		    	$oRange->range_bottom = date(DATE_FORMAT, mktime(0,0,0,1,1, $year_bottom));
		    	$oRange->range_bottom = years_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
				$oRange->range_top = years_add($oRange->range_bottom, $ntimes );		    	
				$oRange->range_top = days_add($oRange->range_top, -1 );		    	
				break;
		    case PK_MONTH:              
		    	$oRange->range_bottom = date(DATE_FORMAT, mktime(0,0,0, $month_bottom,1, $year_bottom));
		    	$oRange->range_bottom = months_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
				$oRange->range_top = months_add($oRange->range_bottom, $ntimes );		    	
				$oRange->range_top = days_add($oRange->range_top, -1 );		    	
				break;
		    case PK_DAY:                
		    	$oRange->range_bottom = days_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
		    	$oRange->range_top = $oRange->range_bottom;
		    	break;
		    case PK_WEEK:
				$x = $this->GetWeekNumberDate(wday($oRange->range_bottom));
				$oRange->range_bottom = days_add($oRange->range_bottom, ($x-1)*-1);
		    	$oRange->range_bottom = weeks_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
				$oRange->range_top = weeks_add($oRange->range_bottom, $ntimes );		    	
				$oRange->range_top = days_add($oRange->range_top, -1 );		    	
		    	break;
		    case PK_TWOWEEK:
		    	break;
		    case PK_WEEK_MONTH:
		    	break;
		    
		}
// Si el tipo de periodo no es hora		
		if ($this->key != PK_HOUR )
		{
			$oRange->range_bottom = substr($oRange->range_bottom, 0, 10);
			$oRange->range_top = substr($oRange->range_top, 0, 10);		
		}
		
		return $oRange;
	}
	
	function MoveDate( $poCurDate, $pnPeriodsShift )
	{
		if (is_null($poCurDate) or is_null($pnPeriodsShift))
		{
			return;
		}
		
		$oRange = new BITAMCDatesRange();
		$oRange->range_bottom = $poCurDate->range_bottom;
		$oRange->range_top = $poCurDate->range_top;	
		
		if ($this->variable)
		{
			// not supported
			return $poCurDate;
		}
				
		$ntimes = $this->base;
		
		switch ( $this->type )
		{
		    case PK_YEAR: 		    	
		    	$oRange->range_bottom = years_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
		    	$oRange->range_top = years_add($oRange->range_top , $pnPeriodsShift * $ntimes);				
				break;
		    case PK_MONTH:              
		    	$oRange->range_bottom = months_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
		    	$oRange->range_top = months_add($oRange->range_top, $pnPeriodsShift * $ntimes);				
				break;
		    case PK_DAY:                
		    	$oRange->range_bottom = days_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
		    	$oRange->range_top = days_add($oRange->range_top, $pnPeriodsShift * $ntimes);		    	
		    	break;
		    case PK_WEEK:
		    	$oRange->range_bottom = weeks_add($oRange->range_bottom, $pnPeriodsShift * $ntimes);
		    	$oRange->range_top = weeks_add($oRange->range_top, $pnPeriodsShift * $ntimes);				
		    	break;
		    case PK_TWOWEEK:
		    	break;
		    case PK_WEEK_MONTH:
		    	break;
		    
		}
		
		return $oRange;
	}

	private function GetWeekNumberDate($sDayName)
	{
		switch (strtolower($sDayName))
		{
			case 'sunday':
				return 1;
				break;
			case 'monday':
				return 2;			
				break;	
			case 'tuesday':
				return 3;
				break;
			case 'wednesday':
				return 4;			
				break;
			case 'thursday':
				return 5;
				break;
			case 'friday':
				return 6;
				break;
			case 'saturday':
				return 7;			
				break;
		}
	}
	
	function GetInitialDate( $pCurDate )
	{
		
		if (is_null($pCurDate))
		{
			return;
		}
		
		if ($this->variable)
		{
// Period Variable			
			return;
		}
		
//		print '<br>$this->base = '.$this->base.'<br>';
		
//		if ($this->base <= 1)
//		{
			$oRange = new BITAMCDatesRange();
			
			$oRange->range_bottom = $pCurDate;
			$oRange->range_top = $pCurDate;
			
			$oRangeInitial = $this->GetPeriodRange( $oRange, 0 );
			
			return $oRangeInitial->range_bottom;
//		}
		
//		return;
	}
	
	function GetPeriods( $pDateBottom, $pDateTop )
	{
		$pDateTmp = '';
		$sDates = '';
	    if ($pDateTop < $pDateBottom)
	    {
	        $pDateTmp = $pDateBottom;
	        $pDateBottom = $pDateTop;
	        $pDateTop = $pDateTmp;
	    }
	    
			    
	    
	    $pDateTmp = $this->GetInitialDate($pDateBottom);
	    $sDates = $pDateTmp;
	    
	    $RawDate = new BITAMCDatesRange();
	    
	    $RawDate->range_bottom = $pDateTmp;
	    $RawDate->range_top = $pDateTmp;

    	$DateLast = $RawDate->range_bottom;
	    $i = 0;
	    
	    while ($RawDate->range_bottom <= $pDateTop)
	    {
	        $i++;
	        $RawDate->range_bottom = $pDateTmp;
	        $RawDate->range_top = $pDateTmp;
	        
	        $RawDate = $this->GetPeriodRange($RawDate, $i);

	        if ($DateLast >= $RawDate->range_bottom)
	            break;

	        $DateLast = $RawDate->range_bottom;
	        
	        if ($RawDate->range_bottom <= $pDateTop)
	        {
	            $sDates = $sDates.",".$DateLast;
	        }
	    }
	    return $sDates;
	}
	
}

class BITAMCCube
{
	public $key;
	public $logical_name;
	public $physical_name;
	public $servidor;
	public $database;
	public $user;
	public $password;	
	public $table_date;
	public $field_date;
	public $use_filter_keys;	
	public $use_period_less;
	public $use_cache;
	public $use_agregation;
	public $olap_catalog;
	public $a_join_keys = array();
	public $a_dimensions = array();
	public $a_indicators = array();
	public $a_agregations = array();	
	public $connection;
	public $indicator_inpc = -1;
	public $null_indicators = true;
	public $zero_indicators = true;
	
// Soporte de agregaciones	

	function GetExactlyAgregationId($sDimensionsInQuery)
	{
		$npos = 0;
		$nid = 0;

    	$nGetExactlyAgregationId = 0;
// Quitamos la primera coma
    	$sDimensionsInQuery = substr($sDimensionsInQuery, 1);
// Quitamos la ultima coma
//    	$sDimensionsInQuery = substr($sDimensionsInQuery, 0, strlen($sDimensionsInQuery)-1);
    	
//    	print '<BR>$sDimensionsInQuery = '.$sDimensionsInQuery;
    	
    	while (true)
    	{
	        $npos = strpos($sDimensionsInQuery, ',');
	        if ($npos === false) 
	            break;

	        $nid = substr($sDimensionsInQuery, 0, $npos);
	        $sDimensionsInQuery = substr($sDimensionsInQuery, $npos + 1);

//    		print '<BR>$nid = '.$nid;
//    		print '<BR>$sDimensionsInQuery = '.$sDimensionsInQuery;

	        $nGetExactlyAgregationId = $nGetExactlyAgregationId + (pow(2, $nid));
//    		print '<BR>$nGetExactlyAgregationId = '.$nGetExactlyAgregationId;
    	}
    	return $nGetExactlyAgregationId;
	}


	function GetListAgregations(&$stablas_mapeadas = '')
	{
		$sGetAgregaciones = '';
		$stablas_mapeadas = '';
	    foreach ( $this->a_agregations as $agregation )
	    {
			if ($sGetAgregaciones == '')
			{
				$sGetAgregaciones = $agregation->key;
				$stablas_mapeadas = $agregation->physical_name;
			}
			else
			{
				$sGetAgregaciones = $sGetAgregaciones.','.$agregation->key;
				$stablas_mapeadas = $stablas_mapeadas.','.$agregation->physical_name;
			}
	    }
	    return $sGetAgregaciones;
	}
	
	function GetAgregationsKeys($id, &$Llaves)
	{
		$Aux = 0.0;
		$i = 0;
		$Paso = 0;
		$ID_Actual = 0;
		$Resta = 0;
	
		$Aux = $id;
		$i = 0;
		while ($Aux > 0)
		{
		    $i++;
		    $Paso = $Aux;
		    $ID_Actual = 0;
		    $Resta = 1;
		    while ($Paso >= 2)
		    {
		        $ID_Actual++;
		        $Resta = $Resta * 2;
		        $Paso = $Paso / 2;
		    }
		    $Llaves[$i] = $ID_Actual;
		    $Aux = $Aux - $Resta;
		}
	}
	
	function GetAgregation($id, &$sAgregacionMapeada = '', $sIndicadores = '')
	{
		$LlavesA = array();
		$LlavesB = array();
		$Paso = '';
		$nAgregacion = 0;
		$sTablasMapeadas = '';
		$sTablaMapeada = '';
		
		$nRegresa_Agregacion = -1;
		
		if (!$this->use_agregation)
		    return;
		
		if ($id == -1)
		    return;

		    
		$sDimsNoVisibles = '';
/*		
		$sDimsNoVisibles = $this->GetDimsNoVisibles($sIndicadores);
*/		
		
		if ($id > 0) 
		{
		    $Paso = $this->GetListAgregations($sTablasMapeadas);
		    
//		    print '<BR>$Paso = '.$Paso;
		    
		    $sTablaMapeada = BITAMGetSiguiente($sTablasMapeadas);

		    $nAgregacion = floatval(BITAMGetSiguiente($Paso));
		    
//		    print '<BR>$nAgregacion = '.$nAgregacion;
		    
		    while ($nAgregacion > 0 and $nAgregacion < $id)
		    {
		        $nAgregacion = floatval(BITAMGetSiguiente($Paso));
		        $sTablaMapeada = BITAMGetSiguiente($sTablasMapeadas);
		    }
		    
		    
		    if ($nAgregacion > 0)
		    {
		        if ($nAgregacion == $id)
		        {
		            $nRegresa_Agregacion = $id;
		            $sAgregacionMapeada = $sTablaMapeada;
		        }
		        else
		        {
		            $TopA = 0;
		            $TopB = 0;
		            $i = 0;
		            $contador = 0;
		            $LlavesA = array();
		            
		            $this->GetAgregationsKeys($id, $LlavesA);
		            
		            $TopA = count($LlavesA);
		            
		            while ($nAgregacion > 0)
		            {
		            	$LlavesB = array();
		            	
		                $this->GetAgregationsKeys($nAgregacion, $LlavesB);
		                
		                $TopB = count($LlavesB);
		                $contador = 1;
		                
		                if ($TopB >= $TopA)
		                {
		                	if ($sDimsNoVisibles != '')
		                	{
			                    for ($i = 1; $i <= $TopB; $i++)
			                    {
			                        if (strpos($sDimsNoVisibles, ','.$LlavesB($i).',') !== false)
			                        {
			                            $TopB = 0;
			                            break;
			                        }
			                    }
		                	}
		                    for ($i = 1; $i <= $TopB; $i++)
		                    {
		                        if ($LlavesA[$contador] == $LlavesB[$i])
		                            $contador++;
		                        elseif ($LlavesA[$contador] > $LlavesB[$i])
		                            $i = $TopB;

		                        if ($contador - 1 == $TopA)
		                            break;
		                    }
		                }
		                if ($TopA == $contador - 1)
		                {
		                    $nRegresa_Agregacion = $nAgregacion;
		                    $sAgregacionMapeada = $sTablaMapeada;
		                    break;
		                }
		                $nAgregacion = floatval(BITAMGetSiguiente($Paso));
		                $sTablaMapeada = BITAMGetSiguiente($sTablasMapeadas);
		            }
		        }
		    }
		}
		else
		{
		    $nRegresa_Agregacion = 0;
		    if (trim($this->olap_catalog) != '')
		        $sAgregacionMapeada = trim($this->olap_catalog);
		}
		
		return $nRegresa_Agregacion;		
		
	}
	
}

class BITAMCJoinKey
{
	public $table_name = '';
	public $source_key = '';
	public $target_key = '';
}

class BITAMCDimension
{
	public $key;	
	public $logical_name;
	public $physical_table;
    public $physical_name_key;	
    public $data_type_key;
    public $long_key;    
    public $physical_name_descrip;	
    public $data_type_descrip;
    public $long_descrip;
    public $estatus;
	public $level;
	public $parameters;
	public $descriptor_key;
    public $group;
    public $edit_outline;
    public $period_key;
    public $period_fmt;
    public $all_caption;
    
	function FormatedName()
	{    	
		if (intval($this->key) <10)
			return '['.$this->physical_table.'].['.$this->physical_name_key.'_0'.$this->key.']';
		else
    		return '['.$this->physical_table.'].['.$this->physical_name_key.'_'.$this->key.']';
    }
    function FormatedNameWOK()
	{    	
    	return '['.$this->physical_table.'].['.$this->physical_name_key.']';
    }
}

class BITAMCIndicator
{
	public $key;
	public $indicator_name;
	public $cube_key;
	public $formule_db;
	public $format;
	public $description;
	public $filter;
	public $paretto;
	public $table_name;
	public $not_ejecutive;
	public $dimensions;
	public $formule_user;
	public $hierarchy_level;
	public $indicator_type;
	public $is_atributte;
	public $is_divisor;
	public $is_only_last_period;
	public $actual;
	public $decrement;
	public $a_alarms = array();
	public $a_operands = array();
	public $dim_depend;
	public $filter_sql;
	public $niveles;
}

class BITAMCIndicatorAlarm
{
	public $key;
	public $period_key;
	public $condition;
	public $formula_bd;
	public $prioridad;
	public $indicator_key_prop;
	public $is_target = false;
	public $is_init = false;
}

class BITAMCIndicatorUser
{
	public $indicator_key;
	public $cube_key;
	public $indicator_name;
	public $indicator_group;
}

class BITAMCProperty
{
	public $key;
	public $name;
	public $fore_color;
	public $back_color;
	public $font_name;	
	public $nback_color;
	public $nfont_name;	
	public $is_bold;		
	public $is_italic;		
	public $is_underline;		
	public $default_color;
	public $path_image;
	public $target;		
}

class BITAMCStageUser
{
	public $key;
	public $name;
	public $paso_fp_default;
	public $user_key;
	public $is_global;
	public $is_public;
	public $stages_links;
	public $date_reexp;
}

class BITAMCCubeUser
{
	public $key;
	public $min;
	public $levels;
	public $filter;
	public $hierarchy;
}

?>