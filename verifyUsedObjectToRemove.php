<?php
//@JAPR 2015-01-20: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
//si es utilizado y advertir al usuario (#75TPJE)
/* Este archivo verificará si el objeto especificado se encuentra utilizado en algún otro objeto según el tipo, si es así, regresará una lista
con la propiedad y objetos específicos donde se utiliza ese elemento
*/
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual

//@JAPR 2015-10-21: Agregada la validación de eliminación de mas tipos de objetos para v6, ahora este archivo ya no auto-ejecutará el código de validación en base a parámetros, sino
//que únicamente cargará a otro que contiene la función que realiza dicha validación para permitir utilizar la función dentro de processRequest.php
require_once("checkCurrentSession.inc.php");
require_once("verifyUsedObjectToRemove.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$intObjectType = getParamValue('ObjectType', 'both', '(int)');
$intObjectID = getParamValue('ObjectID', 'both', '(int)');
$intValidationType = getParamValue('ValidationType', 'both', '(int)');
$intReturArray = getParamValue('ReturnArray', 'both', '(int)');
$intGetIDs = getParamValue('GetIDs', 'both', '(int)');

header('Content-Type: text/plain; charset=utf-8');
$response = CheckObjectReferences($theRepository, $intObjectType, $intObjectID, $intValidationType, null, $intReturArray, $intGetIDs);

if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	echo("<br>\r\n<br>\r\n Request response<br>\r\n");
}

if ($intReturArray) {
	PrintMultiArray($response);
}
else {
	echo($response);
}
?>