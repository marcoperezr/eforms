<?php 
require_once("repository.inc.php");
require_once("initialize.php");
require_once("catalog.inc.php");
require_once("catalogmember.inc.php");
require_once("catalogfilter.inc.php");
require_once("appuser.inc.php");
require_once("survey.inc.php");

//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
require_once("customClasses.inc.php");
require_once("eBavelCatalog.inc.php");
require_once('eBavelIntegration.inc.php');

class BITAMeBavelCatalogValue extends BITAMObject
{
	public $CatalogID;
	public $CatalogValueID;
	
	function __construct($aRepository, $aCatalogID)
	{
		BITAMObject::__construct($aRepository);
		$this->CatalogValueID = null;
		$this->CatalogID = $aCatalogID;
	}
	
	/*El parámetro $bLoadAdditionalFields indica que se deben crear propiedades para almacenar los campos del catálogo que se deben
	capturar adicionalmente con la dimensión. El parámetro $bLoadParentFields indica que se deben crear dichas propiedades además
	para tantos padres de SnowFlake como existan en la dimensión
	*/
	static function NewInstance($aRepository, $aCatalogID, $anBavelFieldIDsColl = null)
	{
		$anInstance = new BITAMeBavelCatalogValue($aRepository, $aCatalogID);
		
		if (is_null($anBavelFieldIDsColl)) {
			$anBavelFieldIDsColl = BITAMeBavelCatalogValue::GeteBavelCatalogDefinition($aRepository, $aCatalogID);
		}
		
		foreach ($anBavelFieldIDsColl as $anAttributeInfo)
		{
			//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			//Si el atributo estaba dividido en sus componentes, entonces tiene que agregarlo según el tipo de dato especificado
			$strSuffix = '';
			$inteBavelFieldDataID = (int) @$anAttributeInfo['dataID'];
			if ($inteBavelFieldDataID > ebfdValue) {
				switch ($inteBavelFieldDataID) {
					case ebfdLatitude:
						$strSuffix = '_L';
						break;
					case ebfdLongitude:
						$strSuffix = '_A';
						break;
				}
			}
			
			@eval('$anInstance->'.$anAttributeInfo['id'].$strSuffix.' = null;');
			//@JAPR
		}
		
		return $anInstance;
	}

	static function NewInstanceWithID($aRepository, $aCatalogValueID, $aCatalogID, $anBavelFieldIDsColl = null)
	{
		$anInstance = null;
		$anInstance =& BITAMGlobalFormsInstance::GetCatalogValueInstanceWithID($aCatalogID, $aCatalogValueID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		require_once('settingsvariable.inc.php');
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'EBAVELURL');
		if (is_null($objSetting) || trim($objSetting->SettingValue == '')) {
			return null;
		}
		
		//Verica si el código de eBavel está disponible
	    $strPath = '';
	    $streBavelPath = '';
		$arrURL = parse_url($objSetting->SettingValue);
		$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
		$strPath = str_replace('/', '\\', $strPath);
		$strPath = str_replace('\\\\', '\\', $strPath);
		if (file_exists($strPath))
		{
			@require_once($strPath);
			$streBavelPath = str_ireplace('service.php', '', $strPath);
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			if (!function_exists('NotifyError')) {
				echo("<br>\r\nNo NotifyError function");
			}
			else {
				echo("<br>\r\nNotifyError function exists");
			}
		}
		
		if (!class_exists('Tables'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			return null;
		}
        /*@AAL 19/03/2015: Modificado para soportar la clase DBView*/
		if (!class_exists('DBForm') || !class_exists('DBView'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			return null;
		}
		
		if (is_null($anBavelFieldIDsColl)) {
			$anBavelFieldIDsColl = BITAMeBavelCatalogValue::GeteBavelCatalogDefinition($aRepository, $aCatalogID);
		}
		$arreBavelFieldIDs = array();
		foreach ($anBavelFieldIDsColl as $anAttributeInfo) {
			//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			//En este caso no se agrega el sufijo específico del dato pedido a los campos de eBavel, porque en el select se deben pedir los
			//campos unificados directamente
			$arreBavelFieldIDs[] = $anAttributeInfo['id'];
			//@JAPR
		}
		if (count($arreBavelFieldIDs) == 0) {
			return BITAMeBavelCatalogValue::NewInstance($aRepository, $aCatalogID, $anBavelFieldIDsColl);
		}
		$strFormID = (string) @$anBavelFieldIDsColl[0]['formID'];
		$strCatalogValueFieldID = (string) @$anBavelFieldIDsColl[0]['id'];
		
		//@JAPR 2014-05-20: Corregido un bug, para que eBavel use seguridad, necesita tener esta variable de sesión asignada
		//@JAPR 2016-07-06: Corregido un bug, para el acceso al API simplemente se sobrescriben las variables, condicionarlo de esta manera evitaba que se pudiera usar el
		//login de eForms independiente (sin pasar por KPIOnline) ya que ese no sobreescribe estas variables, así que quedaban mezcladas (#S36ANP)
		//if (is_null(@$_SESSION["BITAM_UserName"]) || trim($_SESSION["BITAM_UserName"]) == '') {
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//@session_register("BITAM_UserName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		//}
		//@JAPR
		
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
		/*@AAL 19/03/2015: Agregado para soportar Formas y Vistas de eBavel*/
        if (getMDVersion() >= esvRedesign) {
        	/*@AAL 19/03/2015: Agregado para verificar si se va a tratar con una Forma o una Vista de eBavel*/
	        $objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aCatalogID);
	        if($objCatalog->eBavelFormType == ebftForms)
	            $objQuery = DBForm::name($strFormID)->fields($arreBavelFieldIDs)->where($strCatalogValueFieldID, '=', $aCatalogValueID);
	        else//Como el ID de la vista viene como id_FRM_54F49F74E4275 entonces removemos id_ (ver función GeteBavelCatalogDefinition)
	            $objQuery = DBView::name(substr($strCatalogValueFieldID, 3, strlen($strCatalogValueFieldID)-1))->fields($arreBavelFieldIDs)->where('id_' . $strFormID, '=', $aCatalogValueID);
			//@AAL
        }
        else //@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
			$objQuery = DBForm::name($strFormID)->fields($arreBavelFieldIDs)->where($strCatalogValueFieldID, '=', $aCatalogValueID);       
        
        $objQuery->onlyFields(false);
		$objQuery->limit = 1;
		
		$sql = '';
		if (method_exists($objQuery, 'compileQuery')) {
			//@JAPR 2014-08-19: Agregada esta asignación por compatibilidad con eBavel a partir de cambios de esta fecha
			//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
			//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
			//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
			//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
			//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
			global $eBavelServiceVersion;
			global $theUser;
			if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
				$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
				Metadata::setRepository($aneBavelRepository);
			}
			else {
				if (class_exists('Metadata')) {
					@Metadata::setRepository($aRepository);
				}
			}
			//@JAPR
			
			$sql = (string) @$objQuery->compileQuery($aRepository->ADOConnection);
			global $queriesLogFile;
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMeBavelCatalogValue->NewInstanceWithID: \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
		}
		
		$aRS = $objQuery->get($aRepository->ADOConnection);
	   	$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		if ($aRS === false)
		{
			//@JAPR 2014-09-12: Corregido un bug, se estaba reportando el error con la conexión equivocada
			die("(".__METHOD__.") ".translate("Error accessing")." ".$strFormID." ".translate("form").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$anInstance = BITAMeBavelCatalogValue::NewInstanceFromRS($aRepository, $aRS, $aCatalogID, $anBavelFieldIDsColl);
		}
		
		return $anInstance;
	}

	// El parámetro $aAdditionalFields es la lista de campos adicionales que fueron cargados (null si solo es el valor)
	static function NewInstanceFromRS($aRepository, $aRS, $aCatalogID, $anBavelFieldIDsColl = null)
	{
		if (is_null($anBavelFieldIDsColl)) {
			$anBavelFieldIDsColl = BITAMeBavelCatalogValue::GeteBavelCatalogDefinition($aRepository, $aCatalogID);
		}
		
		$strFormID = (string) @$anBavelFieldIDsColl[0]['formID'];
		$strCatalogValueFieldID = (string) @$anBavelFieldIDsColl[0]['id'];
        $aCatalogValueID = (int) @$aRS->fields['id_' . $strFormID];
        //$aCatalogValueID = (int) @$aRS->fields[$strCatalogValueFieldID];
    
		$anInstance = null;
		//@JAPR 2015-01-23: Corregido un bug, dado a que los campos múltiple choice de eBavel se graban en una tabla independiente donde cada
		//opción es un registro, al obtener los datos se estaba repidiendo la llave única de la tabla de la forma por cada opción de respuesta
		//de la múltiple choice, pero esta validación provocaba que los datos sólo grabaran una única instancia en el cache de objetos, así que
		//se removerá para permitir obtener todos los valores, aunque ya no habrá caché para las formas de eBavel (#CQ3J31)
		//$anInstance =& BITAMGlobalFormsInstance::GetCatalogValueInstanceWithID($aCatalogID, $aCatalogValueID);
		//@JAPR
		if(is_null($anInstance))
		{
			$anInstance = BITAMeBavelCatalogValue::NewInstance($aRepository, $aCatalogID, $anBavelFieldIDsColl);
			$anInstance->CatalogValueID = $aCatalogValueID;
			
			//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
			//Recorre cada uno de los atributos configurados buscando el valor que tiene actualmente asignado
			
			foreach ($anBavelFieldIDsColl as $anAttributeInfo)
			{
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				//Si el atributo estaba dividido en sus componentes, entonces tiene que agregarlo según el tipo de dato especificado
				$strSuffix = '';
				$inteBavelFieldDataID = (int) @$anAttributeInfo['dataID'];
				if ($inteBavelFieldDataID > ebfdValue) {
					switch ($inteBavelFieldDataID) {
						case ebfdLatitude:
							$strSuffix = '_L';
							break;
						case ebfdLongitude:
							$strSuffix = '_A';
							break;
					}
				}
				
				$anAttributeFieldName = (string) @$anAttributeInfo['id'].$strSuffix;
				//@JAPR
				$anAttributeFieldValue = @$aRS->fields[$anAttributeFieldName];
				//@JAPR 2016-09-06: Validado que en caso de tratarse de un campo tipo Hora de eBavel, se aplique una conversión a formato de 24hrs vía un objeto DateTime
				//@JAPR 2016-09-07: Finalmente el problema por el cual se grababan las fechas en campos de eBavel con formato AM/PM fue debido a cargas directas que hacía ACeballos, así
				//que LRoux decidió que eForms NO aplicara este parche, pero se guardara el código por si eventualmente se tenía que hacer algo similar por otra causa
				/*$intQTypeID = (int) @$anAttributeInfo['qtypeid'];
				if ($intQTypeID == qtpOpenTime && trim($anAttributeFieldValue) != '') {
					try {
						$dteTime = new DateTime($anAttributeFieldValue);
						$anAttributeFieldValue = $dteTime->format("H:i:s");
					} catch (Exception $e) {
					}
				}*/
				//@JAPR

				eval ('$anInstance->'.$anAttributeFieldName.' = $anAttributeFieldValue;');
			}
			
		 	BITAMGlobalFormsInstance::AddCatalogValueInstanceWithID($aCatalogID, $aCatalogValueID, $anInstance);
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		global $appSettings;
		global $strVersionFieldName;
		
		$aCatalogID = getParamValue('CatalogID', 'both', '(int)');
		$anInstance = null;
		if(array_key_exists("CatalogValueID", $aHTTPRequest->GET))
		{
			//Si llega hasta aquí, entonces solo es la carga normal de un valor existente
			$aCatalogValueID = $aHTTPRequest->GET["CatalogValueID"];
			$anInstance = BITAMeBavelCatalogValue::NewInstanceWithID($aRepository, $aCatalogValueID, $aCatalogID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMeBavelCatalogValue::NewInstance($aRepository, $aCatalogID);
			}
		}
		else
		{
			//Si llega hasta aquí, entonces solo estamos creando un nuevo valor normal
			$anInstance = BITAMeBavelCatalogValue::NewInstance($aRepository, $aDimensionID, $aCatalogID);
		}
		return $anInstance;
	}
	
	function get_FormIDFieldName()
	{
		return 'CatalogValueID';
	}
	
	function get_QueryString()
	{
		return 'BITAM_PAGE=eBavelCatalogValue&CatalogID='.$this->CatalogID.'&CatalogValueID='.$this->CatalogValueID;
	}

	function get_Title()
	{
		return translate("Value");
	}
	
	function isNewObject()
	{
		return (is_null($this->CatalogValueID) || $this->CatalogValueID <= 0);
	}

	function get_Parent()
	{
		if ($this->CatalogID > 0)
		{
			return BITAMeBavelCatalogValueCollection::NewInstance($this->Repository, $this->CatalogID);
		}
		else 
		{
			return $this->Repository;
		}
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		return false;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormFields($aUser)
	{
		global $appSettings;
		global $workingMode;
		
		require_once("formfield.inc.php");
		
		$myFields = array();
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogValueID";
		$aField->Title = translate("ID");
		$aField->Type = "Integer";
		$aField->Size = 25;
		$myFields[$aField->Name] = $aField;
		
		$anBavelFieldIDsColl = BITAMeBavelCatalogValue::GeteBavelCatalogDefinition($this->Repository, $this->CatalogID);
		foreach ($anBavelFieldIDsColl as $anAttributeInfo)
		{
			$intQTypeID = convertEBavelToEFormsFieldType($anAttributeInfo['tagname']);
			switch ($intQTypeID)
			{
				case qtpOpenNumeric:
				case qtpCalc:
					$strDataType = "Integer";
					break;
				default:
					$strDataType = "String";
					$intSize = 100;
					break;
			}
			
			$aField = BITAMFormField::NewFormField();
			//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			//Si el atributo estaba dividido en sus componentes, entonces tiene que agregarlo según el tipo de dato especificado
			$strSuffix = '';
			$inteBavelFieldDataID = (int) @$anAttributeInfo['dataID'];
			if ($inteBavelFieldDataID > ebfdValue) {
				switch ($inteBavelFieldDataID) {
					case ebfdLatitude:
						$strSuffix = '_L';
						break;
					case ebfdLongitude:
						$strSuffix = '_A';
						break;
				}
			}
			//@JAPR

			/*@AAL 19/03/2015: Verificamos que sea una Clave valida para poder mostrar los campos */
			if(@$anAttributeInfo['key'] != -1){
				$aField->Name = $anAttributeInfo['id'].$strSuffix;
				$aField->Title = encodeUTF8($anAttributeInfo['label']);
				$aField->Type = $strDataType;
				if ($strDataType == 'String')
				{
					$aField->Size = $intSize;
				}
				else 
				{
					$aField->Size = 25;
				}
				$myFields[$aField->Name] = $aField;
			}
			//@AAL
		}
		
		return $myFields;
	}

	function updateFromArray($anArray)
	{
 		if (array_key_exists("CatalogValueID", $anArray))
		{
			$this->CatalogValueID = $anArray["CatalogValueID"];
		}
		
		if (array_key_exists("CatalogID", $anArray))
		{
			$this->CatalogID = $anArray["CatalogID"];
		}
		
		return $this;
	}
	
	//Obtiene la definición del catálogo basado en una forma de eBavel y la regresa como un array
	static function GeteBavelCatalogDefinition($aRepository, $aCatalogID) {
		$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aCatalogID);
		//@JAPR 2014-05-20: Validado que si no se puede cargar el catálogo, regrese un array vacio para que no truene
		if (is_null($objCatalog)) {
			return array();
		}
        /*@AAL 19/03/2015: Modificado para soportar Formas y Vistas de eBavel*/
        $FormID = "";
        if (getMDVersion() >= esvRedesign) {
        	$arrEBavelFormsFields = array();
	        //Si hemos seleccionado una Forma entonces pedimos sus atributos
	        if($objCatalog->eBavelFormType == ebftForms){
	            //Carga la definición de la forma de eBavel a utilizar
	            $arreBavelFormColl = @GetEBavelSections($aRepository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
	            if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0)
	        		return $arreBavelFormColl;
	            $arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Forms');        
	        }else{//Si hemos seleccionado una Vista entonces solicitamos sus atributos.
	            //Carga la definición de la vista de eBavel a utilizar
	            $arreBavelFormColl = @GetEBavelViews($aRepository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
	            if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0)
	                return $arreBavelFormColl;
	            $FormID = $arreBavelFormColl[0]['id_Section'];
	            $arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Views');
	        }
        }//@AAL
        else{
        	//Carga la definición de la forma de eBavel a utilizar
			$arreBavelFormColl = @GetEBavelSections($aRepository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));			
			if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0) {
				return array();
			}
			$arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');
        }

        $objForm = $arreBavelFormColl[0];
		$strFormID = $objForm['id'];
        
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
		$arreBavelFieldIDs = array();
		foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
			if ($objCatalogMember->eBavelFieldID > 0) {
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				if ($objCatalogMember->eBavelFieldDataID > ebfdValue) {
					//En este caso el atributo que mapea a un campo tipo Geolocalización utilizará el id del campo, sin hacer referencia a cual de
					//las partes (Latitude o Longitude) especificamente utilizará, ya que los filtros de eBavel lo resuelven unificado
					//Se agregará un array para saber que valores se utilizaron de este campo
					if (!isset($arreBavelFieldIDs[$objCatalogMember->eBavelFieldID])) {
						$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID] = array();
					}
					$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID][] = $objCatalogMember->eBavelFieldDataID;
				}
				else {
					$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID] = $objCatalogMember->eBavelFieldID;
				}
				//@JAPR
			}
		}
		
		/*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel*/
		if (getMDVersion() >= esvRedesign) {
			if($objCatalog ->eBavelFormType == ebftForms) //Siempre el primer campo es el ID consecutivo único del valor
	            $anBavelFieldIDsColl = array(0 => array('id' => 'id_'.$strFormID, 'tagname' => 'widget_field_numeric', 'label' => 'id', 'section_id' => $objCatalog->eBavelFormID, 'appId' => $objCatalog->eBavelAppID, 'id_fieldform' => -1, 'key' => -1, 'formID' => $strFormID));
	        else
	            $anBavelFieldIDsColl = array(0 => array('id' => 'id_'.$strFormID, 'tagname' => 'widget_field_numeric', 'label' => 'id', 'view_id' => $objCatalog->eBavelFormID, 'appId' => $objCatalog->eBavelAppID, 'id_fieldlist' => -1, 'key' => -1, 'formID' => $FormID));
			//@AAL
		}
		else //Siempre el primer campo es el ID consecutivo único del valor
			$anBavelFieldIDsColl = array(0 => array('id' => 'id_'.$strFormID, 'tagname' => 'widget_field_numeric', 'label' => 'id', 'section_id' => $objCatalog->eBavelFormID, 'appId' => $objCatalog->eBavelAppID, 'id_fieldform' => -1, 'key' => -1, 'formID' => $strFormID));
        
		foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
			foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
				if (isset($arreBavelFieldIDs[$intFieldID])) {
					//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
					//Modificado para que no utilice el ID de campo sino que se agreguen tantos campos como atributos de eForms existan, sin
					//embargo para atributos que mapean campos Geolocation, hay que agregar al $arrFieldData la indicación del tipo de información
					//que se desea extraer (Latitude o Longitude)
					//$anBavelFieldIDsColl[$intFieldID] = $arrFieldData;
					if ($intQTypeID == qtpLocation) {
						//En este caso se trata de un campo tipo Geolocation, así que se tuvo que haber recibido un array si es que se mapeaaron
						//atributos divididos en sus componentes, de lo contrario era un valor directo de GPS
						$arrDataIDs = $arreBavelFieldIDs[$intFieldID];
						if (is_array($arrDataIDs)) {
							//Si eran atributos divididos, tiene que agregar ambos a la definición
							foreach ($arrDataIDs as $inteBavelFieldDataID) {
								$arrFieldData['dataID'] = $inteBavelFieldDataID;
								$anBavelFieldIDsColl[] = $arrFieldData;
							}
						}
					}
					else {
						$anBavelFieldIDsColl[] = $arrFieldData;
					}
					//@JAPR
				}
			}
		}
		
		return $anBavelFieldIDsColl;
	}
}

class BITAMeBavelCatalogValueCollection extends BITAMCollection
{
	public $CatalogID;
	public $catalogValuesFilter;	//Filtro a aplicar para la carga de valores (temporal, se pierde al termina la sesión)
	public $maxCatalogValueToShow;	//Cantidad de valores máxima a mostrar en el listado de valores (temporal, se pierde al termina la sesión)
	
	function __construct($aRepository, $aCatalogID = -1, $bApplyFilters = false, $filter = '', $iTopValues = -1)
	{
		global $maxDimensionRecords;
		
		BITAMCollection::__construct($aRepository);
		$this->CatalogID = $aCatalogID;
		if($bApplyFilters)
		{
			$dimValuesFilter = '';
			if($filter != '')
			{
				$dimValuesFilter = $filter;
			}
			$this->catalogValuesFilter = $dimValuesFilter;
			if($iTopValues < 0)
			{
				$this->maxCatalogValueToShow = $maxDimensionRecords;
			}
			else
			{
				$this->maxCatalogValueToShow = $iTopValues;
			}
		}
	}
	
	//Carga la lista de valores y regresa instancias de BITAMeBavelCatalogValue de todos los valores existentes
	//El parámetro $bGetRecordset permite que se regrese sólo el RecordSet en lugar de la colección de valores
	//El parámetro $bIncludeSystemFields permite recuperar también los campos de sistema como la fecha o el key de cada valor (sólo aplica si $bGetRecordset == true)
	//@JAPR 2014-05-28: Agregado el parámetro $bGetDistinctValues para que en el caso de catálogos mapeados de eBavel se obtengan los valores únicos
	//para la combinación de atributos, ya que al ser formas de captura de eBavel y no necesariamente catálogos reales, se pueden llegar a repetir
	//(esto es especialmente útil al momento de descargar las definiciones a los móviles para reducir el tiempo)
	//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
	//Agregado el parámetro $filter para permitir especificar un Filtro SQL directo al momento de cargar los valores (no se debe combinar
	static function NewInstance($aRepository, $aCatalogID, $bApplyFilters=false, $filter = '', $iTopValues = -1, $bGetRecordset = false, $bIncludeSystemFields = false, $bGetDistinctValues = false)
	{
		global $appSettings;
		//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		//@JAPR 2015-03-11: A la fecha de implementación, eBavel Si reportaba los errores y de hecho detenía la ejecución del producto,
		//lo cual no era deseable, mientras que los catálogos de eForms no reportaban los errores y dejaban continuar, esto tampoco es muy bueno pero
		//por lo menos no detenía al App. Se modificará para que eBavel utilice una variable global cuando se estén obteniendo los valores de los
		//catálogos para las agendas, de tal manera que no truene eBavel sino que regrese indicación de error, y con este nuevo parámetro se
		//decidirá si hay o no error para saber si se debe usar la colección de valores vacia o si simplemente no se debe incluir. Los errores que
		//reportaba eBavel no incluían errores por no tener las clases necesarias, así que se modificará también la respuesta en esos casos, bajo
		//el entendido que en caso de faltar algo importante de todas maneras fallará el código y detendrá la ejecución
		global $gbProcessingAgendaCatalogs;
		if (!isset($gbProcessingAgendaCatalogs)) {
			$gbProcessingAgendaCatalogs = false;
		}
		//@JAPR
		
		$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aCatalogID);
		$anEmptyInstance = BITAMeBavelCatalogValueCollection::NewInstanceEmpty($aRepository);
		require_once('settingsvariable.inc.php');
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'EBAVELURL');
		if (is_null($objSetting) || trim($objSetting->SettingValue == '')) {
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:$anEmptyInstance;
			//@JAPR
		}
		
		//Verica si el código de eBavel está disponible
	    $strPath = '';
	    $streBavelPath = '';
		$arrURL = parse_url($objSetting->SettingValue);
		$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
		$strPath = str_replace('/', '\\', $strPath);
		$strPath = str_replace('\\\\', '\\', $strPath);
		if (file_exists($strPath))
		{
			@require_once($strPath);
			$streBavelPath = str_ireplace('service.php', '', $strPath);
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			if (!function_exists('NotifyError')) {
				echo("<br>\r\nNo NotifyError function");
			}
			else {
				echo("<br>\r\nNotifyError function exists");
			}
		}
		
		if (!class_exists('Tables'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:$anEmptyInstance;
			//@JAPR
		}

        /*@AAL 19/03/2015: Modificado para soportar Formas y vistas */
		if (!class_exists('DBForm') || !class_exists('DBView'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:$anEmptyInstance;
			//@JAPR
		}

        /*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel*/
        $FormID = "";
		if (getMDVersion() >= esvRedesign) {
			$arrEBavelFormsFields = array();
	        //Si hemos seleccionado una Forma entonces pedimos sus atributos
	        if($objCatalog->eBavelFormType == ebftForms){
	            //Carga la definición de la forma de eBavel a utilizar
	            $arreBavelFormColl = @GetEBavelSections($aRepository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
	            if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0)
	        		//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
					return ($gbProcessingAgendaCatalogs)?false:$anEmptyInstance;
	            $arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Forms');        
	        }else{//Si hemos seleccionado una Vista entonces solicitamos sus atributos.
	            //Carga la definición de la vista de eBavel a utilizar
	            $arreBavelFormColl = @GetEBavelViews($aRepository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
	            if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0)
	                //@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
					return ($gbProcessingAgendaCatalogs)?false:$anEmptyInstance;
	            $FormID = $arreBavelFormColl[0]['id_Section'];
	            $arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Views');
	        }   
		} //@AAL
		else{
			//Carga la definición de la forma de eBavel a utilizar
			$arreBavelFormColl = @GetEBavelSections($aRepository, false, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
			if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0) {
				//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
				return ($gbProcessingAgendaCatalogs)?false:$anEmptyInstance;
			//@JAPR
			}
			$arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');
		}
		
        $objForm = $arreBavelFormColl[0];
		$strFormID = $objForm['id'];
        
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
		//@JAPR 2014-05-28: Modificado para no especificar explícitamente el campo id_FormID, porque mas abajo siempre se habilitan los campos de
		//sistema de eBavel y ahí está incluído, además que debido a la necesidad de agrupar por los campos, si se agrega a este array se hubiera
		//tenido que quitar antes de usarlo en el Group By para no agrupar por la clave única
		$arreBavelFieldIDsStr = array();	//array(0 => 'id_'.$strFormID);
		//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel
		//Este array contiene los campos en el orden del catálogo de eForms, ya que en ese se esperan
		$arreBavelOrderByFieldIDs = array();
		//Este array contiene los campos en el orden de eBavel, sólo se usa temporalmente para al final procesar los campos según el orden
		$arreBavelOrderByeBavelFieldIDs = array();
		//de eForms
		//@JAPR
		$arreBavelFieldIDs = array();
		foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
			if ($objCatalogMember->eBavelFieldID > 0) {
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				if ($objCatalogMember->eBavelFieldDataID > ebfdValue) {
					//En este caso el atributo que mapea a un campo tipo Geolocalización utilizará el id del campo, sin hacer referencia a cual de
					//las partes (Latitude o Longitude) especificamente utilizará, ya que los filtros de eBavel lo resuelven unificado
					//Se agregará un array para saber que valores se utilizaron de este campo
					if (!isset($arreBavelFieldIDs[$objCatalogMember->eBavelFieldID])) {
						$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID] = array();
					}
					$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID][] = $objCatalogMember->eBavelFieldDataID;
				}
				else {
					$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID] = $objCatalogMember->eBavelFieldID;
				}
				//@JAPR
			}
		}
		
		if (count($arreBavelFieldIDs) == 0) {
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:$anEmptyInstance;
		}
		/*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel*/
		if (getMDVersion() >= esvRedesign) {
			if($objCatalog ->eBavelFormType == ebftForms) //Siempre el primer campo es el ID consecutivo único del valor
	            $anBavelFieldIDsColl = array(0 => array('id' => 'id_'.$strFormID, 'tagname' => 'widget_field_numeric', 'label' => 'id', 'section_id' => $objCatalog->eBavelFormID, 'appId' => $objCatalog->eBavelAppID, 'id_fieldform' => -1, 'key' => -1, 'formID' => $strFormID));
	        else
	            $anBavelFieldIDsColl = array(array('id' => 'id_' . $FormID, 'tagname' => 'widget_field_numeric', 'label' => 'id', 'view_id' => $objCatalog->eBavelFormID, 'appId' => $objCatalog->eBavelAppID, 'id_fieldlist' => -1, 'key' => -1, 'formID' => $FormID));
			//@AAL
		}
		else //Siempre el primer campo es el ID consecutivo único del valor
			$anBavelFieldIDsColl = array(0 => array('id' => 'id_'.$strFormID, 'tagname' => 'widget_field_numeric', 'label' => 'id', 'section_id' => $objCatalog->eBavelFormID, 'appId' => $objCatalog->eBavelAppID, 'id_fieldform' => -1, 'key' => -1, 'formID' => $strFormID));
        
        foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
			foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
				if (isset($arreBavelFieldIDs[$intFieldID])) {
					$arreBavelFieldIDsStr[$intFieldID] = $arrFieldData['id'];
					//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel
					$arreBavelOrderByeBavelFieldIDs[$intFieldID] = array('id' => $arrFieldData['id'], 'order' => 1);
					
					//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
					//Modificado para que no utilice el ID de campo sino que se agreguen tantos campos como atributos de eForms existan, sin
					//embargo para atributos que mapean campos Geolocation, hay que agregar al $arrFieldData la indicación del tipo de información
					//que se desea extraer (Latitude o Longitude)
					//$anBavelFieldIDsColl[$intFieldID] = $arrFieldData;
					if ($intQTypeID == qtpLocation) {
						//En este caso se trata de un campo tipo Geolocation, así que se tuvo que haber recibido un array si es que se mapeaaron
						//atributos divididos en sus componentes, de lo contrario era un valor directo de GPS
						$arrDataIDs = $arreBavelFieldIDs[$intFieldID];
						if (is_array($arrDataIDs)) {
							//Si eran atributos divididos, tiene que agregar ambos a la definición
							foreach ($arrDataIDs as $inteBavelFieldDataID) {
								$arrFieldData['dataID'] = $inteBavelFieldDataID;
								$anBavelFieldIDsColl[] = $arrFieldData;
							}
						}
					}
					else {
						$anBavelFieldIDsColl[] = $arrFieldData;
					}
					//@JAPR
				}
			}
		}
		
		//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel
		//El array de campos a ordenar se debe generar en el orden de eForms, no el de eBavel, así que se recorre el mismo array de eForms y se
		//actualiza la información de ordenamiento obtenida del array de campos usados en el query
		foreach ($arreBavelFieldIDs as $intFieldID => $arrDataIDs) {
			if (isset($arreBavelOrderByeBavelFieldIDs[$intFieldID])) {
				$arreBavelOrderByFieldIDs[$intFieldID] = $arreBavelOrderByeBavelFieldIDs[$intFieldID];
			}
		}
		
		//@JAPR 2014-05-20: Corregido un bug, para que eBavel use seguridad, necesita tener esta variable de sesión asignada
		//@JAPR 2016-07-06: Corregido un bug, para el acceso al API simplemente se sobrescriben las variables, condicionarlo de esta manera evitaba que se pudiera usar el
		//login de eForms independiente (sin pasar por KPIOnline) ya que ese no sobreescribe estas variables, así que quedaban mezcladas (#S36ANP)
		//if (is_null(@$_SESSION["BITAM_UserName"]) || trim($_SESSION["BITAM_UserName"]) == '') {
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//@session_register("BITAM_UserName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		//}
		//@JAPR
		
		//$aRS = DBForm::name($strFormID)->fields($arreBavelFieldIDsStr)->whereRaw("FFRMS_10026 = 'The Home Depot'")->get($aRepository->ADOConnection);
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
        
        /*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel */
		if (getMDVersion() >= esvRedesign) {
			if($objCatalog->eBavelFormType == ebftForms)
	            $objQuery = DBForm::name($strFormID)->fields($arreBavelFieldIDsStr);
	        else
	            $objQuery = DBView::name($strFormID)->fields($arreBavelFieldIDsStr);
	        //@AAL
		}
        else
            $objQuery = DBForm::name($strFormID)->fields($arreBavelFieldIDsStr);
        
		//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
		//Agregado el parámetro $filter para permitir especificar un Filtro SQL directo al momento de cargar los valores (no se debe combinar
		if (trim($filter) != '') {
			$objQuery->whereRaw($filter);
		}
		
		//@JAPR 2014-05-28: Agregado el parámetro $bGetDistinctValues para que en el caso de catálogos mapeados de eBavel se obtengan los valores únicos
		if ($bGetDistinctValues) {
			@$objQuery->groupBy($arreBavelFieldIDsStr);
		}
		//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel cuando no estaban
		//grabados consecutivamente
		@$objQuery->orderBy($arreBavelOrderByFieldIDs);
		//@JAPR
		$objQuery->onlyFields(false);
		//Si sólo se pedirá el RecordSet, hay que validar si se incluyen o no los campos de sistema
		if ($bGetRecordset) {
			$objQuery->onlyFields(!$bIncludeSystemFields);
		}
		if ($bApplyFilters && $iTopValues > 0) {
			$objQuery->limit = $iTopValues;
		}
		
		$sql = '';
		if (method_exists($objQuery, 'compileQuery')) {
			//@JAPR 2014-08-19: Agregada esta asignación por compatibilidad con eBavel a partir de cambios de esta fecha
			//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
			//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
			//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
			//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
			//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
			global $eBavelServiceVersion;
			global $theUser;
			if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
				$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
				Metadata::setRepository($aneBavelRepository);
			}
			else {
				if (class_exists('Metadata')) {
					@Metadata::setRepository($aRepository);
				}
			}
			//@JAPR
			
			$sql = (string) @$objQuery->compileQuery($aRepository->ADOConnection);
			global $queriesLogFile;
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMeBavelCatalogValueCollection->NewInstance: \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				if ($filter) {
					echo("<br>\r\nBITAMeBavelCatalogValueCollection::NewInstance ({$aCatalogID}) Filter:\r\n{$filter}<br>\r\n");
				}
				echo("<br>\r\nBITAMeBavelCatalogValueCollection::NewInstance ({$aCatalogID}):\r\n{$sql}<br>\r\n");
			}
		}
		
		$aRS = $objQuery->get($aRepository->ADOConnection);
	   	$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		if ($aRS === false)
		{
			//@JAPR 2014-09-12: Corregido un bug, se estaba reportando el error con la conexión equivocada
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			if ($gbProcessingAgendaCatalogs) {
				return false;
			}
			else {
				die("(".__METHOD__.") ".translate("Error accessing")." ".$strFormID." ".translate("form").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			//@JAPR
		}
		
		//Si solo se pidió el recordset, entonces se regresa directamente para que lo procese quien invoca al método, pero no se puede usar así
		//como parte del FrameWork
		if ($bGetRecordset) {
			return $aRS;
		}
		
		$anInstance = new BITAMeBavelCatalogValueCollection($aRepository, $aCatalogID, $bApplyFilters, $filter, $iTopValues);
		while (!$aRS->EOF) {
			$anObject = BITAMeBavelCatalogValue::NewInstanceFromRS($anInstance->Repository, $aRS, $aCatalogID, $anBavelFieldIDsColl);
			$anInstance->Collection[] = $anObject;
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewInstanceEmpty($aRepository)
	{
		$anInstance = new BITAMeBavelCatalogValueCollection($aRepository);
		
		return $anInstance;
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();

		$anBavelFieldIDsColl = BITAMeBavelCatalogValue::GeteBavelCatalogDefinition($this->Repository, $this->CatalogID);
		foreach ($anBavelFieldIDsColl as $anAttributeInfo){ 
			$aField = BITAMFormField::NewFormField();
			//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			//Si el atributo estaba dividido en sus componentes, entonces tiene que agregarlo según el tipo de dato especificado
			$strSuffix = '';
			$inteBavelFieldDataID = (int) @$anAttributeInfo['dataID'];
			if ($inteBavelFieldDataID > ebfdValue) {
				switch ($inteBavelFieldDataID) {
					case ebfdLatitude:
						$strSuffix = '_L';
						break;
					case ebfdLongitude:
						$strSuffix = '_A';
						break;
				}
			}
            //@JAPR
            
			/*@AAL 19/03/2015: Modificado para asignar el Id correcto, las vistas requieren almacenar el Id de la forma y el propio
			de la vista (el id de la vista se guarda en [formID] y no en [id]), para el caso de las Formas da igual. */
			$aField->Name = (@$anAttributeInfo['formID']) ? 'id_' . $anAttributeInfo['formID'].$strSuffix : $anAttributeInfo['id'].$strSuffix;
			//@AAL
			$aField->Title = decodeUTF8($anAttributeInfo['label']);
			$aField->Type = "String";
			$aField->Size = 100;
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$intMaxCatalogValueToShow = 0;
		$strCatalogValuesFilter = '';
		if(array_key_exists("txtDefDimRec", $aHTTPRequest->GET))
		{
			$intMaxCatalogValueToShow = (int) $aHTTPRequest->GET[txtDefDimRec];
		}
		if(array_key_exists("txtDimFilter", $aHTTPRequest->GET))
		{
			$strCatalogValuesFilter = $aHTTPRequest->GET["txtDimFilter"];
		}
		if(array_key_exists("txtDefDimRec", $aHTTPRequest->POST))
		{
			$intMaxCatalogValueToShow = (int) $aHTTPRequest->POST[txtDefDimRec];
		}
		if(array_key_exists("txtDimFilter", $aHTTPRequest->POST))
		{
			$strCatalogValuesFilter = $aHTTPRequest->POST["txtDimFilter"];
		}
		
		$aCatalogID = (int) @$aHTTPRequest->GET["CatalogID"];
		$anInstance = BITAMeBavelCatalogValueCollection::NewInstance($aRepository,$aCatalogID,true,$strCatalogValuesFilter,$intMaxCatalogValueToShow);
		
		return $anInstance;
	}
	
	function get_Parent()
	{
		global $appSettings;
		
		if ($this->CatalogID > 0)
		{
			return BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		}
		else 
		{
			return $this->Repository;
		}
	}
	
	function get_Title()
	{	
		$title = translate("Values");
		
		return $title;
	}
	
	function get_AddRemoveQueryString()
	{
		return "";
	}
	
	function get_QueryString()
	{
		return "BITAM_SECTION=eBavelCatalogValueCollection&CatalogID=".$this->CatalogID;
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_PAGE=eBavelCatalogValue";
	}

	function get_FormIDFieldName()
	{
		return 'CatalogValueID';
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		return false;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
}
?>