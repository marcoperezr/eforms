<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

$strOriginalWD = getcwd();

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	$tableName = "SVSurveyMatrixData_".$surveyInstance->ModelID;

	$sql = "SELECT FactKey FROM ".$tableName;

	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	//si la consulta regresa un error eso quiere decir que no se ha creado la tabla
	if ($aRS === false)
	{
		$sql = "CREATE TABLE ".$tableName." 
				(
					FactKey BIGINT, 
					FactKeyDimVal BIGINT, 
					QuestionID INTEGER, 
					ConsecutiveID BIGINT, 
					dim_value VARCHAR(255)
				)";

		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql."<br/>");
		}
	}
}
?>