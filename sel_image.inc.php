<?php
session_start();
$repository=$_SESSION["PABITAM_RepositoryName"];
session_write_close();
$repository="customerimages/".$repository;
echo "<script language='JavaScript' src='js/EditorHTML/".$_SESSION["PAuserLanguage"].".js'></script>\n";
$valid_extensions = array('.gif', '.png', '.bmp', '.jpg', '.jpeg', '.ico');

$url_images = 'url_images = [';
$img_types = 'img_types = [';
$img_names = 'img_names = [';

$filenames = array();

$default_dir=$repository; //indicamos el directorio donde inicia el explorador
$filenames = array();
if(!file_exists($default_dir)) //verificamos que existe el directorio
{
	mkdir($default_dir);
	//echo "<br>Error... El directorio no existe: $default_dir...";
   	//exit();
}

$dir = $default_dir;

$dp = opendir($dir); //abrimos el directorio
if ($dp)
{
	$bban = true;
	while($bban) //hacemos un ciclo para guardar los nombres de los directorios y archivos que se tienen en el directorio actual
	{
		$file = readdir($dp);
		if ($file) {
			$filenames[]=$file;
		}
		else {
			$bban = false;
		}
	}
}

sort($filenames); //ordenamos los directorios alfabéticamente
for($i=0;$i<count($filenames);$i++) //hacemos un ciclo para ir mostrando el contenido del directorio
{
	$file=$filenames[$i];
    //checamos si la variable dir es igual al directorio por defecto y ademas PHP al abrir un directorio pone los puntos (./ y ../ que sirve para ir al directorio raíz o uno mas abajo) pero como comparamos que la variable dir sea igual al default entonces no hay ni directorio raís (porque no encontramos en él) ni subdirectorios entonces mandamos continue que se regresaría al ciclo del for
    if($dir==$default_dir && ($file=="."||$file==".."))
        continue;
    //cuando checamos que la variable dir no es el default pero recordamos que php manda el ./ lo vamos a omitir, por lo que con el continue regresamos al ciclo del for
    if(is_dir("$dir/$file")&&$file==".")
        continue;
    if(!is_dir("$dir/$file")) //Si es un archivo
    {
        //echo "$dir/$file><br>";
        $ext_type = strtolower(substr($file, strrpos($file,'.')));
        if (in_array($ext_type, $valid_extensions))
        {
        	$sFName = str_replace("'", "\'", $file);
        	$url_images .= "'".str_replace("'", "\'", $dir)."/".$sFName."',";
        	$img_types .= "'".$ext_type."',";
        	$img_names .= "'".substr($sFName,0,strrpos($sFName,'.'))."',";
        	unset($sFName);
        }
	}
} // llavel del for

if (substr($url_images, strlen($url_images)-1) == ',')
{
	$url_images = substr($url_images, 0, strlen($url_images)-1).'];';
	$img_types = substr($img_types, 0, strlen($img_types)-1).'];';
	$img_names = substr($img_names, 0, strlen($img_names)-1).'];';
}
else
{
	$url_images .= '];';
	$img_types .= '];';
	$img_names .= '];';
}
unset($filenames);
?>