<?php
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
?>
<html>
<head>
	<meta http-equiv="x-ua-compatible" content="IE=10" />
	<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
	<meta http-equiv="MSThemeCompatible" content="Yes">
	<title><?= $theTitle ?></title>
	<link type="text/css" href="loadexcel/css/bootstrap.min.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="css/default.css">
	<link rel="stylesheet" type="text/css" href="css/calendar.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/default2.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/buildformula.css">
	<link rel="stylesheet" type="text/css" href="css/tabs.css">
	<link rel="stylesheet" type="text/css" href="css/base.css">
	<link rel="stylesheet" type="text/css" href="css/tabbedUl.css">
<!--2015-06-26 JRPP Agrego librerias y css para el pintado de la importacion de catalagos-->
	
	<link type="text/css" href="loadexcel/css/font-awesome.min.css" rel="stylesheet"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="loadexcel/js/libs/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" data-main="loadexcel/js/loadexcel.js" src="loadexcel/js/libs/require/require.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/browserSniffer.js"></script>
	<script language="JavaScript">
<?
	global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
	global $MONTH_NAMES, $MONTH_ABBR;
	global $DAY_NAMES, $DAY_ABBR;

	$first = true;
	echo("		var MONTH_NAMES = new Array(");
	foreach ($MONTH_NAMES as $aName)
	{
		if (!$first) { echo(","); }
		$first = false;
		echo("\"".$aName."\"");
	}
	echo(");\n");
	$first = true;
	echo("		var MONTH_ABBR = new Array(");
	foreach ($MONTH_ABBR as $anAbbr)
	{
		if (!$first) { echo(","); }
		$first = false;
		echo("\"".$anAbbr."\"");
	}
	echo(");\n");
	$first = true;
	echo("		var DAY_NAMES = new Array(");
	foreach ($DAY_NAMES as $aName)
	{
		if (!$first) { echo(","); }
		$first = false;
		echo("\"".$aName."\"");
	}
	echo(");\n");
	$first = true;
	echo("		var DAY_ABBR = new Array(");
	foreach ($DAY_ABBR as $anAbbr)
	{
		if (!$first) { echo(","); }
		$first = false;
		echo("\"".$anAbbr."\"");
	}
	echo(");\n");
?>
		var MONTH_REGEXP = '(';
		for (var _i = MONTH_NAMES.length - 1; _i >= 0; _i--)
		{
			if (_i == MONTH_NAMES.length - 1)
			{
				MONTH_REGEXP += MONTH_NAMES[_i];
			}
			else
			{
				MONTH_REGEXP += ('|' + MONTH_NAMES[_i]);
			}
			MONTH_REGEXP += ('|' + MONTH_ABBR[_i]);
			if ((_i + 1) < 10)
			{
				MONTH_REGEXP += ('|0' + (_i + 1));
			}
			MONTH_REGEXP += ('|' + (_i + 1));
		}
		MONTH_REGEXP += ')?';
		
		var DECIMAL_SYMBOL = '<?= $DECIMAL_SYMBOL ?>';
		var DIGIT_GROUPING_SYMBOL = '<?= $DIGIT_GROUPING_SYMBOL ?>';
	</script>
	<script language="JavaScript" type="text/javascript" src="js/calendar.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/main.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/autofill.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/common.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/popup.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/toggleDiv.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/CmbMultiSel.js"></script>

	<SCRIPT LANGUAGE="JavaScript" SRC="BITAMXMLObject.js"></SCRIPT>
</head>
<body style="border:0;margin:0;background-color:#f5f5f5">