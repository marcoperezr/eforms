<?php

/* Esta clase permite configurar una recalendarización de agendas
La clase realmente es genérica, no captura sobre un elemento específico ni tiene accesos a la base de datos, sólo se usa para generar un template
de captura de cierta información utilizando el framework, el resultado de esta forma se regresará a la forma padre que invocó el diálogo donde
debe estar generada, puesto que no sirve como una clase normal del framework
*/

require_once("repository.inc.php");
require_once("initialize.php");

class BITAMSurveyAgendaReschedule extends BITAMObject
{
	public $AgendaID;					//Siempre es -1, sólo sirve como referencia del objeto en el framework
	public $CaptureStartDate;			//Fecha de inicio de captura, las agendas serán mostradas cuando su fecha sea <= que la actual de la aplicación (DateTime)
	public $CaptureStartTime;			//Hora de inicio de captura en formato hh:mm (Varchar)
	public $CaptureStartTime_Hours;		//Requerido por el componente Time para el campo CaptureStartTime
	public $CaptureStartTime_Minutes;	//Requerido por el componente Time para el campo CaptureStartTime
	public $isNew;
	
  	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->AgendaID = -1;
		$this->CaptureStartDate = date('Y-m-d', strtotime(date('Y-m-d').' + 1 day'));
		$this->CaptureStartTime = array(9,0);
		$this->CaptureStartTime_Hours = 9;
		$this->CaptureStartTime_Minutes = 0;
		$this->isNew = true;
	}
	
	static function NewInstance($aRepository)
	{
		return new BITAMSurveyAgendaReschedule($aRepository);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$anInstance = BITAMSurveyAgendaReschedule::NewInstance($aRepository);
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("AgendaID", $anArray)) {
			$this->AgendaID = $anArray["AgendaID"];
		}
		
		if (array_key_exists("CaptureStartDate", $anArray))
		{
			$this->CaptureStartDate = $anArray["CaptureStartDate"];
		}
		
		if (array_key_exists("CaptureStartTime_Hours", $anArray))
		{
			$this->CaptureStartTime[0] = (int)$anArray["CaptureStartTime_Hours"];
			$this->CaptureStartTime_Hours = $this->CaptureStartTime[0];
		}
		if (array_key_exists("CaptureStartTime_Minutes", $anArray))
		{
			$this->CaptureStartTime[1] = (int)$anArray["CaptureStartTime_Minutes"];
			$this->CaptureStartTime_Minutes = $this->CaptureStartTime[1];
		}
		
		return $this;
	}
	
	function isNewObject()
	{
		return $this->isNew;
	}

	function get_Title()
	{
		return translate("Reschedule");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=SurveyAgendaReschedule&AgendaID={$this->AgendaID}";
	}

	function get_Parent()
	{
		return $this;
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'SettingID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		$categoryCatalogField = null;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CaptureStartDate";
		$aField->Title = translate("Capture Start Date");
		$aField->Type = "Date";
		$aField->FormatMask = "yyyy-MM-dd";
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CaptureStartTime";
		$aField->Title = translate("Capture Start Time");
		$aField->Type = "Time";
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}
	
	function generateAfterFormCode($aUser)
	{
?>
	<script type="text/javascript" language="javascript" src="js/dialogs.js"></script>
	<script language="JavaScript">
		function returnValues()
		{
			var intHours = 0;
			if (BITAMSurveyAgendaReschedule_SaveForm.CaptureStartTime_Hours) {
				intHours = parseInt(BITAMSurveyAgendaReschedule_SaveForm.CaptureStartTime_Hours.value);
			}
			if (intHours < 10) {
				intHours = '0' + intHours;
			}
			
			var intMinutes = 0;
			if (intMinutes < 0) {
				intMinutes = '0' + intMinutes;
			}
			if (BITAMSurveyAgendaReschedule_SaveForm.CaptureStartTime_Minutes) {
				intMinutes = parseInt(BITAMSurveyAgendaReschedule_SaveForm.CaptureStartTime_Minutes.value);
			}
			
			var strDate = '';
			if (BITAMSurveyAgendaReschedule_SaveForm.CaptureStartDate) {
				strDate = BITAMSurveyAgendaReschedule_SaveForm.CaptureStartDate.value;
			}
			
			var strReturnValue = JSON.stringify({'date':strDate, 'hour':intHours, 'minutes':intMinutes});
			try {
				if (window.setDialogReturnValue)
				{
					setDialogReturnValue(strReturnValue);
				}
			}
			catch (e) {}
			try {
				owner.returnValue = strReturnValue;
			}
			catch (e) {}
			
			cerrar();
		}
		
		objOkCancelButton = document.getElementById("BITAMSurveyAgendaReschedule_CancelButton");
		objOkCancelButton.onclick = cerrar;
		
		objOkSelfButton = document.getElementById("BITAMSurveyAgendaReschedule_OkSelfButton");
		objOkSelfButton.onclick = returnValues;
		
		objOkParentButton = document.getElementById("BITAMSurveyAgendaReschedule_OkParentButton");
		if (objOkParentButton) {
			objOkParentButton.style.display = "none";
		}
		
		objOkNewButton = document.getElementById("BITAMSurveyAgendaReschedule_OkNewButton");
		if (objOkNewButton) {
			objOkNewButton.style.display = "none";
		}
		
		objHomeButton = document.getElementsByClassName("object_homeButton");
		if (objHomeButton && objHomeButton[0]) {
			objHomeButton[0].style.display = "none";
		}
	</script>
<?
	}
}
?>