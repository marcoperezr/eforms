<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("question.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	//Se genera instancia de coleccion de preguntas dado un SurveyID
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyInstance->SurveyID);
	
	foreach ($questionCollection->Collection as $questionInstance) 
	{
		if(!is_null($questionInstance->HasReqComment) && $questionInstance->HasReqComment>0)
		{
			//Actualizamos el MainFactKey de la tabla SI_SV_SurveyAnswerComment con el valor de FactKey ya que son
			//identicos porque todas las preguntas pertenecieron a secciones estaticas
			$sql = "UPDATE SI_SV_SurveyAnswerComment A
					SET A.MainFactKey = A.FactKey 
					WHERE A.SurveyID = ".$surveyInstance->SurveyID." AND A.QuestionID = ".$questionInstance->QuestionID." 
					AND A.MainFactKey IS NULL";
			
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			
			if ($aRepository->DataADOConnection->Execute($sql) === false)
			{
				echo(translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
			}
			else 
			{
				echo('UPDATE: '.$sql.'<br>');
			}
		}
	}
}
?>