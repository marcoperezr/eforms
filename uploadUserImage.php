<?php
require_once("checkCurrentSession.inc.php");
require_once("utils.inc.php");
//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
require_once("uploadFileExt.php");
//@JAPR
$validExt=array();
$validExt=["JPG", "GIF", "PNG"];
$isValidExt=true;
$objectType = getParamValue("objectType", 'both', '(int)');
$strId = getParamValue("strId", 'both', '(string)');
$tabName = getParamValue("tabName", 'both', '(string)');
//Sólo usados si se trata de una propiedad imagen configurada en un subgrid detalle de un objeto que está en un grid de tipo collección
$objectID = getParamValue("objectID", 'both', '(int)');
$childType = getParamValue("childType", 'both', '(int)');
$childID = getParamValue("childID", 'both', '(int)');
$collTabName = getParamValue("collTabName", 'both', '(string)');
$strRowId = getParamValue("strRowId", 'both', '(string)');
//var_dump($strId);
//var_dump($_FILES['file'.$strId]['tmp_name']);
//var_dump($_FILES);
//@JAPR 2015-07-22: Corregido un bug, no se estaba creando la ruta donde se colocará el archivo, así que no podía moverlo
$blnError = false;
$strErrDesc = '';
$path = str_replace("\\", "/", realpath("."))."/customerimages/".$_SESSION["PABITAM_RepositoryName"];
ob_start();
if (!file_exists($path)) {
	if (!mkdir($path, null, true)) {
		$strErrDesc = trim(ob_get_contents());
		$strErrDesc = translate("File not uploaded").", ".translate("Error").": ".$strErrDesc;
		$blnError = true;
	}
}

$bitEditorDivID = "";
if (isset($_GET['bitEditorDivID']) && trim($_GET['bitEditorDivID'])!="")
{
	$bitEditorDivID=trim($_GET['bitEditorDivID']);
}

$fieldName = "";
if (isset($_GET['fieldName']) && trim($_GET['fieldName'])!="")
{
	$fieldName=trim($_GET['fieldName']);
}

$popupTimeID = "";
if (isset($_GET['strPopupTimeID']) && trim($_GET['strPopupTimeID'])!="")
{
	$popupTimeID=trim($_GET['strPopupTimeID']);
}


$action="load";
if (isset($_GET['action']) && trim($_GET['action'])!="")
{
	$action=trim($_GET['action']);
}
$strFileName = replaceInvalidFilenameChars(StripSpecialCharsFromFileNames($_FILES["file".$strId]["name"], "photo"));
if($action=="load" && trim($strFileName)=="")
{
	//Si no hay un archivo seleccionado no se ejecuta el procedimiento
	die();
}
$pos = strpos($_SESSION["PAHTTP_REFERER"],"build_page.php");
$pathImg = substr($_SESSION["PAHTTP_REFERER"],0,$pos);
$variableValue = "";
$strErrExt="";
switch($action)
{
	case "load":
	{
		//Ejemplo de como debe estar $path "E:/inetpub/wwwroot/artus/gen8/ESurveyV6/customerimages/fbm_bmd_0532/form.GIF";
		//@JAPR 2017-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
		$strXLSFileName = $path."/".$strFileName;
		//@JAPR
		$infoFile = pathinfo($strXLSFileName);
		//Corrección Ticket #QND4OY: Sólo se aceptan imagenes JPG, GIF y PNG
		//La validación del tipo de imagen no aplica para la imagen del usuario ni para la imagen del link ni para paquete de archivos
		//@JAPR 2017-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
		//La validación tal como estaba excluía a las configuraciones de link, usuario, PERO INCLUÍA al .zip con imágenes, así que se modificó para incluir aquellas
		//configuraciones que deben ser de imágenes, y en el caso del ZIP validar contra dicha extensión, excluyendo a otros casos que suben archivos diversos (se
		//incluyó el caso de settings para la imagen general del App de captura, ya que no se estaba considerando tampoco)
		//if($objectType!=otyLink && $objectType!=otyUser && $objectType!=otyImagePackage)
		if($objectType==otyLink || $objectType==otyUser || $objectType==otySetting) {
			if(!in_array(strtoupper($infoFile["extension"]), $validExt))
			{
				$isValidExt=false;
			}
		}
		elseif($objectType==otyImagePackage) {
			if(!in_array(strtoupper($infoFile["extension"]), array("ZIP")))
			{
				$isValidExt=false;
			}
		}
		
		//Agregada la validación para exclusión de tipos de archivos no soportados (aplicaria para casos donde no entró a la validación de tipos permitidos en realidad)
		$arrFileInfo = getExtendedFileInfo($strXLSFileName);
		
		if ( $arrFileInfo && is_array($arrFileInfo) ) {
			if ( !checkValidExtension(@$arrFileInfo["fileExtension"], null, true) ) {
				$isValidExt=false;
			}
		}
		//@JAPR
		if($isValidExt)
		{
			$strXLSFileTempName = $_FILES['file'.$strId]['tmp_name'];
			if (!$blnError) {
				//OMMC 2015-10-08: si $strXLSFileName que es el nombre de la imagen contenía algún caracter especial, no se mostraba, se codifica como UTF8 para que se muestre.
				move_uploaded_file($strXLSFileTempName, utf8_decode($strXLSFileName));
				//move_uploaded_file($strXLSFileTempName, $strXLSFileName);
				$strErrDesc = trim(ob_get_contents());
				if ($strErrDesc != '') {
					$strErrDesc = translate("File not uploaded").", ".translate("Error").": ".$strErrDesc;
					$blnError = true;
				}
			}
			//@JAPR 2017-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
			$variableValue="customerimages/".$_SESSION["PABITAM_RepositoryName"]."/".$strFileName;
			$src = $pathImg."customerimages/".$_SESSION["PABITAM_RepositoryName"]."/".$strFileName;
			//@JAPR
		}
		else
		{
			//No es una extesión válida de archivo
			$strErrExt = translate("File not uploaded").", ".translate("Error").": ".translate("Invalid file extension");
			$blnError = true;
		}
		break;
	}
	case "delete":
	{
		$variableValue="";
		$src = $pathImg."images/admin/emptyimg.png";
		break;
	}
	default:
	{
		break;
	}
}	
ob_end_clean();
//@JAPR

//RV Dic2016: Validar que si ocurrió un error no continue, ya que si se tenía una imagen grabada anteriormente
//y ocurría un error al agregar una nueva como quiera se ejecutaba el query actualizando el campo de imagen con cadena vacía y entonces se 
//perdía la imagen anterior.
if (!$blnError)
{
	switch($objectType)
	{
		case otySetting:
		{
			//Grabar el nombre de la imagen en settingsvariables
			$sql = "UPDATE SI_SV_Settings SET SettingValue =".$theRepository->DataADOConnection->Quote($variableValue) ." WHERE ".
					"SettingName=".$theRepository->DataADOConnection->Quote("BACKGROUNDIMAGE");
			if ($theRepository->DataADOConnection->Execute($sql) === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Settings ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				//@JAPR 2015-08-07: Agregado el incremento en la versión de la imagen configurada
				$sql = "UPDATE SI_SV_SETTINGS SET SettingValue = COALESCE(SettingValue, 0) +1 
					WHERE SettingName = ".$theRepository->DataADOConnection->Quote("BACKGROUNDIMAGEVERSION");
				$theRepository->DataADOConnection->Execute($sql);
			}
			break;
		}
		case otyUser:
		{
			//@AAL 21/07/2015: Para este caso el objectID equivale al UserID del usuario
			//Aquí se actualiza la Ruta de la imagen seleccionada en la tabla SI_SV_Users
			$sql = "UPDATE SI_SV_Users SET Image =".$theRepository->ADOConnection->Quote($variableValue) ." WHERE UserID = " . $objectID;
			if ($theRepository->ADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			break;
		}
		case otyLink:
		{
			$sql = "UPDATE SI_SV_Link SET Image =".$theRepository->ADOConnection->Quote($variableValue) ." WHERE LinkID = " . $objectID;
			if ($theRepository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Link ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			break;
		}
		//@JAPR 2016-06-23: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
		case otyImagePackage:
			//En este caso el archivo recibido es un archivo .zip, así que se debe descomprimir en la carpeta de customerImages correspondiente (se asume que todo su contenido serán imágenes,
			//pero realmente no se comprueba que lo sean)
			if ($variableValue && file_exists($variableValue)) {
				$zip = new ZipArchive;
				try {
					if ($zip->open($variableValue) === TRUE) {
						$blnError = !$zip->extractTo($path);
						if ($blnError) {
							$strErrDesc = translate("File not uploaded");
						}
						$zip->close();
					} else {
						$strErrDesc = translate("File not uploaded");
						$blnError = true;
						//die ('Error to (PHP) unzip file. Error Code=' . $zip);
					}
				} catch (Exception $e) {
					$strErrDesc = translate("File not uploaded").", ".translate("Error").": ".$e->getMessage();
					$blnError = true;
				}
			}
			break;
		//@JAPR
		default:
		{
			break;
		}
	}
}
?>
<HTML>
	<head></head>
	<body>
		<script language="JavaScript">
<?
//Cambiar la imagen en el div cuando se termina de subir al servidor
if ($blnError) {
	if($strErrExt!="")
	{
?>
	if(window.parent.errorMsg!=undefined)
	{
		window.parent.errorMsg='<?=$strErrExt?>';
	}
<?
	}
?>
	/*
<?
	echo($strErrDesc);
?>
	*/
	if(window.parent.blnError!=undefined)
	{
		window.parent.blnError=true;
	}
	<?
	//RV Dic2016: Cuando ocurre un error al subir una imagen desde el editor, se debe llamar a la función que se utiliza en el editor.
	if ($objectType == "-1")
	{
	?>
	window.parent.setUserImageForEditor('<?=$strId?>', '<?=$tabName?>', '<?=$variableValue?>', '<?=$strRowId?>','<?=$objectType?>', '<?=$objectID?>', '<?=$childType?>', '<?=$childID?>', '<?=$collTabName?>','<?=$popupTimeID?>','<?=$fieldName?>','<?=$bitEditorDivID?>');
	<?
	}
	else
	{
	//@JAPR 2017-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
	?>
	window.parent.setUserImageForObject('<?=$strId?>', '<?=$tabName?>', '<?="customerimages/".$_SESSION["PABITAM_RepositoryName"]."/".$strFileName?>', '<?=$strRowId?>','<?=$objectType?>', '<?=$objectID?>', '<?=$childType?>', '<?=$childID?>', '<?=$collTabName?>');
	<?
	//@JAPR
	}
	?>
<?
}
else {
	//@JAPR 2016-06-23: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
	if ($objectType == otyImagePackage) {
?>
		window.parent.setUserImageForObject('refresh', 'refresh', '', 'refresh');
<?
	}
	//@JAPR
	elseif ($objectType==otySetting) {
?>
		window.parent.frmUploadFile<?=$strId?>.img<?=$strId?>.src="<?=$src?>";
<?
	}
	elseif ($objectType==otyLink) {
?>
		window.parent.frmUploadFile<?=$strId?>.img<?=$strId?>.src="<?=$src?>";
		window.parent.objLink.Image = "<?=$src?>";
		window.parent.parent.parent.refreshDataViewMenu(window.parent.objLink);
<?
	}
	elseif ($objectType == otyUser) {
		//@AAL 21/07/2015: Regresa el control al archivo appUserExt.inc.php para continuar con el mostrado de la imagen, a través de la función setUserImageForObject()
?> 
		window.parent.setUserImageForObject('<?=$strId?>', '<?=$tabName?>', '<?=$variableValue?>', '<?=$strRowId?>','<?=$objectType?>', '<?=$objectID?>');
<?
	}
	elseif ($objectType == "-1") 
	{
?>
		window.parent.setUserImageForEditor('<?=$strId?>', '<?=$tabName?>', '<?=$variableValue?>', '<?=$strRowId?>','<?=$objectType?>', '<?=$objectID?>', '<?=$childType?>', '<?=$childID?>', '<?=$collTabName?>','<?=$popupTimeID?>','<?=$fieldName?>','<?=$bitEditorDivID?>');
<?		
	}
	else
	{
?>
		window.parent.setUserImageForObject('<?=$strId?>', '<?=$tabName?>', '<?=$variableValue?>', '<?=$strRowId?>','<?=$objectType?>', '<?=$objectID?>', '<?=$childType?>', '<?=$childID?>', '<?=$collTabName?>');
<?
	}
}
?>
		</script>
	</body>
</HTML>