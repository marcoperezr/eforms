<?php 

require_once("repository.inc.php");

class BITAMPropCollection extends BITAMCollection
{
	function __construct($aRepository, $anArrayOfPropIDs)
	{
		BITAMCollection::__construct($aRepository);

		$filter = "";
		if (!is_null($anArrayOfPropIDs))
		{
			foreach ($anArrayOfPropIDs as $aPropID)
			{
				if ($filter != "")
				{
					$filter .= ", ";
				}
				$filter .= (int) $aPropID; 
			}
			if ($filter != "")
			{
				$filter = "and t1.CLA_PROP IN (".$filter.")";
			}
		}
		
		if ($filter == "")
		{
			$filter = "and t1.CLA_PROP > -1";
		}		

		$sql = 
		"SELECT t1.CLA_PROP AS PropID ".
		"		,t1.NOM_PROP AS PropName ".
		"		,t1.OPERADOR ".
		"		,t1.COLOR_LETRA ".
		"		,t1.COLOR_FONDO ".
		"		,t1.TIPO_LETRA ".
		"		,t1.NEGRITA ".
		"		,t1.ITALICA ".
		"		,t1.SUBRAYADO ".
		"		,t1.TARGET ".
		"		,t1.CLA_OWNER AS OwnerID ".
		"		,t2.NOM_CORTO AS OwnerName ".
		"		,t1.COLOR_DEFAULT as color_default ".		
		"FROM 	SI_PROPIEDAD t1, SI_USUARIO t2 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER ".$filter." ".
		"	and t1.NOM_PROP <> 'Dummy' ".
		"ORDER BY 2";		
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_PROPIEDAD table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$this->Collection[] = BITAMProp::NewPropFromRS($this->Repository, $aRS);
			$aRS->MoveNext();
		}
	}

	static function NewPropCollection($aRepository, $anArrayOfPropIDs = null)
	{
		return new BITAMPropCollection($aRepository, $anArrayOfPropIDs);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		return BITAMPropCollection::NewPropCollection($aRepository);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Image()
	{
		return '<img src="images/properties.gif">';
	}
	
	function get_Title()
	{
		return "Alerts";
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=PropCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=Prop";
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'PropID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "PropName";
		$aField->Title = translate("Alert Name");
		$aField->Type = "String";
		$aField->Size = 30;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Operador";
		$aField->Title = translate("Operator");
		$aField->Type = "String";
		$aField->Size = 2;
		$myFields[$aField->Name] = $aField;
		


		if (!InModeSAAS())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "OwnerName";
			$aField->Title = translate("Owner");
			$aField->Type = "String";
			$aField->Size = 15;		
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		// @EA 2005-06-22
		$RepositoryType = $_SESSION["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			$RepositoryType = $_COOKIE["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			// Produccion
			$RepositoryType = 2;		

		if ($RepositoryType == 1)
			return true;
		else 
			return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
}

class BITAMProp extends BITAMObject
{
	public $PropID;
	public $PropName;
	public $Operador;
	public $Color_Letra;
	public $Color_Fondo;
	public $Tipo_Letra;
	public $Negrita;
	public $Italica;
	public $Subrayado;
	public $Target;
	public $Target_Original;	
	public $OwnerName;
	public $color_default;

	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->PropID = -1;
		$this->PropName = "";
		$this->Operador = ">";
		$this->Color_Letra = '000000';
		$this->Color_Fondo = $this->RGBColor(16777215);
		$this->color_default = 0;
		$this->Tipo_Letra = "Verdana";
		$this->Negrita = 0;
		$this->Italica = 0;
		$this->Subrayado = 0;
		$this->Target = 0;
		$this->Target_Original = 0;
		
		// Obtenemos el nombre del usuario logeado, primero lo tomamos de la sesion
		//@JAPR 2015-07-10: Corregido un bug, estas clases deben usar la variable de sesión de eForms
		$UserNameOwner = @$_SESSION["PABITAM_UserName"];
		// Si no existe en la sesion, lo tomamos de la cookie			
		if (is_null($UserNameOwner))
			$UserNameOwner = @$_COOKIE["BITAM_UserName"];
		// Si aun asi es nulo, lo inicializamos en cero
		if (is_null($UserNameOwner))
			$UserNameOwner = "SUPERVISOR";		

		$this->OwnerName = $UserNameOwner;
		
	}

	static function NewProp($aRepository)
	{
		return new BITAMProp($aRepository);
	}

	static function NewPropWithID($aRepository, $aPropID)
	{
		$anInstance = null;
		if (((int) $aPropID) < 0)
		{
			return $anInstance;
		}
		$sql = 
		"SELECT t1.CLA_PROP AS PropID ".
		"		,t1.NOM_PROP AS PropName ".
		"		,t1.OPERADOR ".
		"		,t1.COLOR_LETRA ".
		"		,t1.COLOR_FONDO ".
		"		,t1.TIPO_LETRA ".
		"		,t1.NEGRITA ".
		"		,t1.ITALICA ".
		"		,t1.SUBRAYADO ".
		"		,t1.TARGET ".
		"		,t1.CLA_OWNER AS OwnerID ".
		"		,t2.NOM_CORTO AS OwnerName ".
		"		,t1.COLOR_DEFAULT as color_default ".
		"FROM 	SI_PROPIEDAD t1, SI_USUARIO t2 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER and t1.CLA_PROP = ".((int) $aPropID);
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_PROPIEDAD table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMProp::NewPropFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewPropFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMProp::NewProp($aRepository);
		$anInstance->PropID = (int) $aRS->fields["propid"];
		$anInstance->PropName = rtrim($aRS->fields["propname"]);
		$anInstance->Operador = rtrim($aRS->fields["operador"]);
		$anInstance->Color_Letra = rtrim($aRS->fields["color_letra"]);
		
		if ($anInstance->Color_Letra == '')
			$anInstance->Color_Letra = 0;
			
		$anInstance->Color_Letra = $anInstance->RGBColor($anInstance->Color_Letra);
		
		$anInstance->Color_Fondo = rtrim($aRS->fields["color_fondo"]);
		
		if ($anInstance->Color_Fondo == '')
			$anInstance->Color_Fondo = 16777215;
		
		$anInstance->Color_Fondo = $anInstance->RGBColor($anInstance->Color_Fondo);
		
		$anInstance->color_default = (int) $aRS->fields["color_default"];
				
		$anInstance->Tipo_Letra = rtrim($aRS->fields["tipo_letra"]);
		$anInstance->Negrita = (int) $aRS->fields["negrita"];
		$anInstance->Italica = (int) $aRS->fields["italica"];
		$anInstance->Subrayado = (int) $aRS->fields["subrayado"];
		
		$anInstance->Target = (int) $aRS->fields["target"];
		$anInstance->Target_Original = $anInstance->Target;
		
		$anInstance->OwnerName = $aRS->fields["ownername"];
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("PropID", $aHTTPRequest->POST))
		{
			$aPropID = $aHTTPRequest->POST["PropID"];
			if (is_array($aPropID))
			{
				$aCollection = BITAMPropCollection::NewPropCollection($aRepository, $aPropID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
					if (!($anInstanceToRemove->Message == ""))
					{
						$anInstanceToRemove->Message = "CanŽt delete this alarms because was used in production indicators";
						return $anInstanceToRemove;
					}
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMProp::NewPropWithID($aRepository, (int) $aPropID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMProp::NewProp($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("PropID", $aHTTPRequest->GET))
		{
			$aPropID = $aHTTPRequest->GET["PropID"];
			$anInstance = BITAMProp::NewPropWithID($aRepository, (int) $aPropID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMProp::NewProp($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMProp::NewProp($aRepository);
		}
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
 		if (array_key_exists("PropID", $anArray))
		{
			$this->PropID = (int) $anArray["PropID"];
		}
 		if (array_key_exists("PropName", $anArray))
		{
			$this->PropName = stripslashes($anArray["PropName"]);
		}
 		if (array_key_exists("Operador", $anArray))
		{
			$this->Operador = stripslashes($anArray["Operador"]);
		}
 		if (array_key_exists("Tipo_Letra", $anArray))
		{
			$this->Tipo_Letra = stripslashes($anArray["Tipo_Letra"]);
		}
 		if (array_key_exists("Negrita", $anArray))
		{
			$this->Negrita = (int) $anArray["Negrita"];
		}		
 		if (array_key_exists("Italica", $anArray))
		{
			$this->Italica = (int) $anArray["Italica"];
		}		
 		if (array_key_exists("Subrayado", $anArray))
		{
			$this->Subrayado = (int) $anArray["Subrayado"];
		}
 		if (array_key_exists("Target", $anArray))
		{
			$this->Target = (int) $anArray["Target"];
		}		
 		if (array_key_exists("color_default", $anArray))
		{
			$this->color_default = (int) $anArray["color_default"];
		}		
 		if (array_key_exists("Color_Letra", $anArray))
		{
			$this->Color_Letra = $anArray["Color_Letra"];
		}		
 		if (array_key_exists("Color_Fondo", $anArray))
		{
			$this->Color_Fondo = $anArray["Color_Fondo"];
		}		
 		if (array_key_exists("OwnerName", $anArray))
		{
			$this->OwnerName = stripslashes($anArray["OwnerName"]);
		}
		return $this;
	}
	
	function RGBColor($color)
	{
		$color_hex = trim(dechex($color));
		if (strlen($color_hex) < 6)
		{
			$color_hex = '00000'.$color_hex;
			$color_hex = substr($color_hex, strlen($color_hex)-6, 6);
		}
		else
			$color_hex = substr($color_hex, 0, 6);
			
		$color_hex = substr($color_hex, 4, 2).substr($color_hex, 2, 2).substr($color_hex, 0, 2);
		
		return $color_hex;
	}
	
	function NormalColor($color_hex)
	{
		$color_hex = substr($color_hex, 4, 2).substr($color_hex, 2, 2).substr($color_hex, 0, 2);
		
		$color_dec = hexdec($color_hex);
		
		return $color_dec;
	}

	function save()
	{
		CheckMetadataDuplicated($this->Repository->ADOConnection, 'SI_PROPIEDAD', 'NOM_PROP', $this->PropName, 'CLA_PROP', $this->PropID, '');
		
		$Color_Fondo = $this->NormalColor($this->Color_Fondo);		
		$Color_Letra = $this->NormalColor($this->Color_Letra);
		
		if ($this->Target != $this->Target_Original)
		{
			$sql = 'update SI_PROPIEDAD set TARGET = 0';
			$this->Repository->ADOConnection->Execute($sql);
		}
			
		
	 	if ($this->isNewObject())
		{
/*
			$sql =  "SELECT MAX(CLA_PROP) As PropID".
						" FROM SI_PROPIEDAD";

			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("Error accessing SI_PROPIEDAD table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}

			if (is_null($aRS->fields["propid"]))
				$this->PropID = 1;
			else
				$this->PropID = $aRS->fields["propid"] + 1;
*/
			$this->PropID = BITAMGetNextID($this->Repository->ADOConnection, 'CLA_PROP', 'SI_PROPIEDAD', '');

			// Obtenemos el id del usuario logeado, primero lo tomamos de la sesion
			$UserIDOwner = $_SESSION["BITAM_UserID"];
			// Si no existe en la sesion, lo tomamos de la cookie			
			if (is_null($UserIDOwner))
				$UserIDOwner = $_COOKIE["BITAM_UserID"];
			// Si aun asi es nulo, lo inicializamos en cero
			if (is_null($UserIDOwner))
				$UserIDOwner = "0";
								
			$sql = 
			"INSERT INTO SI_PROPIEDAD (".
			"CLA_PROP,".
			"NOM_PROP,".
			"OPERADOR,".
			"NEGRITA,".
			"ITALICA,".
			"SUBRAYADO,".
			"TARGET,".
			"CLA_OWNER, ".
			"COLOR_DEFAULT,".
			"TIPO_LETRA,".
			"COLOR_LETRA,".						
			"COLOR_FONDO".
			") VALUES (".
			$this->PropID.",".
			$this->Repository->ADOConnection->Quote($this->PropName).",".
			$this->Repository->ADOConnection->Quote($this->Operador).",".
			$this->Negrita.",".
			$this->Italica.",".
			$this->Subrayado.",".
			$this->Target.",".
			$UserIDOwner.",".
			$this->color_default.",".
			$this->Repository->ADOConnection->Quote($this->Tipo_Letra).",".
			$Color_Letra.",".
			$Color_Fondo.
			")";
			
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_PROPIEDAD table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
		}
		else
		{
			$sql = "UPDATE SI_PROPIEDAD SET ".
						"NOM_PROP = ".$this->Repository->ADOConnection->Quote($this->PropName).
						",OPERADOR = ".$this->Repository->ADOConnection->Quote($this->Operador).
						",NEGRITA = ".$this->Negrita.						
						",ITALICA = ".$this->Italica.						
						",SUBRAYADO = ".$this->Subrayado.						
						",TARGET = ".$this->Target.
						",COLOR_DEFAULT = ".$this->color_default.
						",TIPO_LETRA = ".$this->Repository->ADOConnection->Quote($this->Tipo_Letra).
						",COLOR_LETRA = ".$Color_Letra.
						",COLOR_FONDO = ".$Color_Fondo.
					" WHERE CLA_PROP = ".$this->PropID;
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_PROPIEDAD table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
		}
		
		ExportPropertiesToPHP($this->Repository);
		
		return $this;
	}

	function remove()
	{		
		/* ====================================================================================
			Garantizamos la integridad del repositorio
		   ==================================================================================== */
		$sql = "select COUNT(*) AS CountProp from SI_PROPxIND where CLA_PROP = ".$this->PropID;
		$rst = $this->Repository->ADOConnection->Execute($sql);
		
		$ncount = $rst->fields["countprop"];
		
		if ($ncount > 0)
		{
			$this->Message = "0";
			return $this;
		}
		
		// Finalmente los grupos de indicadores		
		$sql = "DELETE FROM SI_PROPIEDAD WHERE CLA_PROP = ".$this->PropID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_PROPIEDAD table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		ExportPropertiesToPHP($this->Repository);
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->PropID < 0);
	}

	function get_Image()
	{
		return '<img src="images/properties.gif">';
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New Alarm");
		}
		else
		{
			return $this->PropName;
		}
	}

	function get_QueryString()
	{
		if ($this->PropID < 0)
		{
			return "BITAM_PAGE=Prop";
		}
		else
		{
			return "BITAM_PAGE=Prop&PropID=".$this->PropID;
		}
	}

	function get_Parent()
	{
		return BITAMPropCollection::NewPropCollection($this->Repository);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'PropID';
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
?>		
<script language="javascript">
			function ChangeFontType()
			{
				var prop = document.getElementsByName('PropName');
				
				var negrita = document.getElementsByName('Negrita');
				var italica = document.getElementsByName('Italica');
				var subrayado = document.getElementsByName('Subrayado');
								
				if (negrita[1].checked)
					prop[0].style.fontWeight = 'bold';
				else
					prop[0].style.fontWeight = 'normal';
	
				if (italica[1].checked)
					prop[0].style.fontStyle = 'italic';
				else
					prop[0].style.fontStyle = 'normal';			
					
				if (subrayado[1].checked)
					prop[0].style.textDecoration = 'underline';
				else
					prop[0].style.textDecoration = 'none';
			}
			
			function ChangeFontFamily()
			{
				var prop = document.getElementsByName('PropName');
				var afonts = document.getElementsByName('Tipo_Letra');
				var font_family = afonts[0].options(afonts[0].selectedIndex).value;
				prop[0].style.fontFamily = font_family;
			}
</script>
<?
	}
	

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
?>		
<OBJECT id=paleta CLASSID="clsid:3050f819-98b5-11cf-bb82-00aa00bdce0b" width="0px" height="0px"></OBJECT>
<script language="javascript">
			var prop = document.getElementsByName('pbColor_Fondo');
			prop[0].style.backgroundColor = <?="'".$this->Color_Fondo."'";?>;
			
			var prop = document.getElementsByName('pbColor_Letra');
			prop[0].style.backgroundColor = <?="'".$this->Color_Letra."'";?>;
			
			var prop = document.getElementsByName('PropName');
			prop[0].style.backgroundColor = <?="'".$this->Color_Fondo."'";?>;
			prop[0].style.color = <?="'".$this->Color_Letra."'";?>;			
			prop[0].style.fontFamily = <?="'".$this->Tipo_Letra."'";?>;
			
			function GetPropBackgroundColor()
			{
				var prop = document.getElementsByName('pbColor_Fondo');
				prop[0].style.backgroundColor = paleta.ChooseColorDlg(prop[0].style.backgroundColor);
				
				var color_fondo = document.getElementsByName('Color_Fondo');
				color_fondo[0].value = prop[0].style.backgroundColor;
				
				if (color_fondo[0].value.substring(0,1) == '#')
					color_fondo[0].value = color_fondo[0].value.substring(1);
				
				var propName = document.getElementsByName('PropName');
				propName[0].style.backgroundColor = prop[0].style.backgroundColor;
			}
			
			function GetPropForeColor()
			{
				var prop = document.getElementsByName('pbColor_Letra');
				prop[0].style.backgroundColor = paleta.ChooseColorDlg(prop[0].style.color);
				
				var color_letra = document.getElementsByName('Color_Letra');
				color_letra[0].value = prop[0].style.backgroundColor;
				
				if (color_letra[0].value.substring(0,1) == '#')
					color_letra[0].value = color_letra[0].value.substring(1);
				
				var propName = document.getElementsByName('PropName');
				propName[0].style.color = prop[0].style.backgroundColor;
			}
</script>
<?
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "PropName";
		$aField->Title = translate("Alert name");
		$aField->Type = "String";
		$aField->Size = 30;
		$aField->AfterMessage = '<br><br><h5><font color="#999999">'.translate('Colors').'</font></h5><hr>';				
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Color_Fondo";
		$aField->Title = translate("Background color");
		$aField->Type = "String";
		$aField->IsVisible = false;
		$aField->AfterMessage = 
		"<input type=\"button\" name=\"pbColor_Fondo\" value=\"\" style=\"text-align:center;vertical-align:top;width:20px\" ".
		"onclick=\"javascript: GetPropBackgroundColor()\">";

		$myFields[] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Color_Letra";
		$aField->Title = translate("Font color");
		$aField->Type = "String";
		$aField->IsVisible = false;		
		$aField->AfterMessage = 
		"<input type=\"button\" name=\"pbColor_Letra\" value=\"\" style=\"text-align:center;vertical-align:top;width:20px\" ".
		"onclick=\"javascript: GetPropForeColor()\">".'<br><br><h5><font color="#999999">'.translate('Default').'</font></h5><hr>';;
		
		$myFields[] = $aField;
		
		$aDefaults = array();
		$aDefaults[0] = 'Background color';
		$aDefaults[1] = 'Font color';		
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "color_default";
		$aField->Title = "";
		$aField->Type = "Object";
		$aField->Options = $aDefaults;
		$aField->VisualComponent = "Radio";
		$aField->AfterMessage = '<br><br><h5><font color="#999999">'.translate('Font type').'</font></h5><hr>';				
		$myFields[] = $aField;
		
		$afonts = array();

		$afonts['Andale Mono'] = 'Andale Mono';		
		$afonts['Arial'] = 'Arial';
		$afonts['Arial Black'] = 'Arial Black';		
		$afonts['Cambria'] = 'Cambria';		
		$afonts['Constantia'] = 'Constantia';
		$afonts['Comic Sans MS'] = 'Comic Sans MS';
		$afonts['Courier'] = 'Courier';
		$afonts['Courier New'] = 'Courier New';		
		$afonts['Century Gothic'] = 'Century Gothic';		
		$afonts['Helvetica'] = 'Helvetica';		
		$afonts['Georgia'] = 'Georgia';
		$afonts['Impact'] = 'Impact';
		$afonts['Modern'] = 'Modern';		
		$afonts['Tahoma'] = 'Tahoma';
		$afonts['Times'] = 'Times';
		$afonts['Times New Roman'] = 'Times New Roman';		
		$afonts['Trebuchet MS'] = 'Trebuchet MS';		
		$afonts['Verdana'] = 'Verdana';
		
//		asort($afonts);
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Tipo_Letra";
		$aField->Title = "Font type";
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $afonts;
		$aField->OnChange = 'ChangeFontFamily()';
		$myFields[] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Negrita";
		$aField->Title = "";
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		$aField->AfterMessage = translate("Bold");
		$aField->OnChange = 'ChangeFontType()';
		$myFields[] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Italica";
		$aField->Title = "";
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		$aField->AfterMessage = translate("Italic");
		$aField->OnChange = 'ChangeFontType()';		
		$myFields[] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Subrayado";
		$aField->Title = "";
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		$aField->AfterMessage = translate("Underline").'<br><br><hr>';
		$aField->OnChange = 'ChangeFontType()';		
		$myFields[] = $aField;		
		
		$aOperators = array();
		$aOperators[">"] = ">";
		$aOperators[">="] = ">=";
		$aOperators["="] = "=";
		$aOperators["<"] = "<";
		$aOperators["<="] = "<=";
		$aOperators["<>"] = "<>";
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Operador";
		$aField->Title = translate("Operator");
		$aField->Type = "Object";
		$aField->Options = $aOperators;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Target";
		$aField->Title = "Stratego";
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		$aField->AfterMessage = translate("Target");
		$myFields[] = $aField;				
		
/*		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "OwnerName";
		$aField->Title = translate("Owner");
		$aField->Type = "String";
		$aField->Size = 15;
		$aField->IsDisplayOnly = true;
		$myFields[$aField->Name] = $aField;
*/		
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		
		// @EA 2005-06-22
		$RepositoryType = $_SESSION["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			$RepositoryType = $_COOKIE["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			// Produccion
			$RepositoryType = 2;		

		if ($RepositoryType == 1)
		{
			return true;				
		}
		else 
			return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}		
}
?>