<?php
    //@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
    require_once("checkCurrentSession.inc.php");
	
    //Cargar diccionario del lenguage a utilizar
    if (array_key_exists("PAuserLanguage", $_SESSION))
    {
        InitializeLocale($_SESSION["PAuserLanguageID"]);
        LoadLanguageWithName($_SESSION["PAuserLanguage"]);
    }
    else 
    {   //English
        InitializeLocale(2);    
        LoadLanguageWithName("EN");
    }
    //@JAPR
    $qTypeID = getParamValue('qTypeID', 'both', '(string)');
    $aState = getParamValue('HTMLEditorState', 'both', '(int)'); //Estado en el que viene el editor HTML (0 - Gráfico, 1 - HTML)
    $aText = getParamValue('aTextEditor', 'both', '(string)'); //Contenido del campo de texto de donde se mando a llamar a el editor HTML
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <script language="javascript" src="js/EditorHTML/dialogs.js"></script>
	<!--
	Se requiere jQuery 1.9.1 o mayor para que el plugin funcione
	* Latest Chrome (desktop and mobile)
	* Latest Firefox (desktop only)
	* Latest Safari (desktop and mobile)
	* Latest Opera (webkit)
	* Internet Explorer 11
	-->
	<script src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
	<script src="js/jquery-migrate-1.2.1.min.js"></script>
	<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.js"></script-->
	<!--Librerias de Redactor-->
	<link rel="stylesheet" href="js/redactor/redactor/redactor.css" />
	<script src="js/redactor/redactor/redactor.js"></script>
	<!--Librería de font-awesome-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!--customDHTMLX-->
	<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>
	
	<!--Plugins para la toolbar-->
	<script src="js/redactor/imagemanage.js"></script>
	<script src="js/redactor/underline.js"></script>
	<script src="js/redactor/fontsize.js"></script>
	<script src="js/redactor/fontfamily.js"></script>
	<!--En el archivo "fontcolor.js" buscar la línea que dice: $dropdown.width(242); 
	Cambiar el valor de (242) por: (287) Esto hace que se puedan alinear los colores de la paleta-->
	<script src="js/redactor/fontcolor.js"></script>
	<script src="js/redactor/formulaEditor.js"></script>
	
	<!--Selección del lenguaje-->
	<script src="js/redactor/lang/es.js"></script>
	<script src="js/redactor/lang/en.js"></script>
	
		
    <script type="text/javascript">
		var sParamReturn = '';
		//Estado en el que se va a iniciar el editor HTML
		aState = "<?=$aState?>";
		oQuestion = "<?=($qTypeID != '')?$qTypeID:''?>";
		//JAPR 2016-06-15: Agregada la opción de alineación de texto (#XWUYJ5)
		if(oQuestion == 'message'){
			redactorButtons = ['bold', 'italic', 'alignment', 'image', 'file'];
		}else{
			redactorButtons = ['html', 'bold', 'italic', 'alignment', 'image', 'file'];	
		}
		
		//@OMMC 2015/09/01 Variable de lenguaje para redactor
		redactorLang = "<?=$_SESSION['PAuserLanguage']?>";
		if (redactorLang) {
			switch (redactorLang) {
				case "SP":
					redactorLang = "es";
					break;
				case 'ES':
					redactorLang = "es";
					break;
				case 'EN':
					redactorLang = "es";
					break;
				case 'BE':
					redactorLang = "es";
					break;
				default:
					redactorLang = "es";
					break;
			}
		}
        /*@AAL 25/02/2015: Se ejecuta al enviar la fórmula o el texto tecleado en el editor, al hacer click
        sobre el botón Send & Close. Principalmente se valida que antes de enviar el texto este sea convertido
        nuevamente al formato de número {$(Q|S)Number.{attr(atributeName) | function() | properties}*/
        function ResultEditHTML()
        {
            sParamReturn = document.getElementById("content").value;
			
            try {
                if (window.setDialogReturnValue)
                	setDialogReturnValue(aState);
                    setDialogReturnValue(sParamReturn);
			} catch (e) {
				alert(e.message);
			}
			//alert("sParamReturn"+sParamReturn);
            window.close();
        }

        /*@AAL 25/02/2015: Función que evita maximizar o redimensionar la ventana. Simplemente la regresa a su namaño normal*/
        function Resize () { window.resizeTo(625, 525); }
		var HTMLcodeCache = "";
		isFromFormulaEditor = false;
		function buildRedactor()
		{
			//Se liga el ID del textarea del HTML al plugin
			$('#content').redactor({
				focus: true,
				lang: redactorLang,
				tabIndex: -1,
				//Definición de la altura del textarea
				minHeight: 250,
				maxHeight: 250,
				linebreaks: true,
				//Botones que se van a diujar
				buttons: redactorButtons,
				//JAPR 2016-06-15: Corregido un bug, debido a que el default de replaceDivs es true, se estaban eliminando todos los divs que el usuario introducía (#XWUYJ5)
				replaceDivs:false,
				//OMMC 2015-10-14: Este par de callbacks revisan el código HTML periódicamente, si existe algun cambio, el código se refresca.
				initCallback: function(e){
					var formEditorIcon = this.$toolbar[0].children[this.$toolbar[0].children.length-1];
					formEditorIcon.addEventListener("click", function(){
						isFromFormulaEditor = true;
					});

					var firstIcon = $(this.$toolbar[0].children[0]).find('a[rel=html]');
					//Encontró que el HTML está habilitado
					if(firstIcon.length > 0){
						//OMMC 2016-01-16: Se agrega el listener del boton del estado del editor HTML
						var HTMLStatusIcon = this.$toolbar[0].children[0];
						HTMLStatusIcon.addEventListener("click", function(){
							//Agregados estados del editor HTML, 1 - HTML, 2 - Gráfico
							if(aState == 1){
								aState = 0;
							}else{
								aState = 1;
							}
						});
					}

					//Se revisa el estado, para mostrar el editor en la forma correcta cuando se inicializa.
					if(aState == 1){
						this.code.showCode();
					}
				},
				focusCallback: function(e)
			    {
			    	if(isFromFormulaEditor){
			    		var HTMLcodeLive = this.code.get();
				        if(HTMLcodeCache != HTMLcodeLive){
				        	this.code.set(HTMLcodeLive);
				        	isFromFormulaEditor = false;
				        }
			    	}			    		
			    },
			    blurCallback: function(e)
			    {
			    	HTMLcodeCache = this.code.get();
			    },
				changeCallback: function(e)
				{
					this.HTMLcodeCache = this.code.get();
				},
				//JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
				/* Atrapa los errores de upload de imágenes, el parámetro contendrá la descripción del error en la propiedad message */
				imageUploadErrorCallback: function(e) {
					if ( e.error ) {
						alert(e.message || '');
					}
				},
				//JAPR
			    //OMMC
				//Otros botones que se pueden dibujar
				/*'formatting', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'alignment', 'horizontalrule'*/
				//Botones que se pueden ocultar
				/*buttonsHide: ['formatting', 'deleted', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'alignment', 'horizontalrule'],*/
				
				//OMMC 2015-10-07  Se deshabilita el editor de fórmulas.
				//Plugins de subrayar, subir imágenes, tamaño de letra, tipo de leta y color de letra
				plugins:[
					'underline',
					'imagemanage',
					'fontsize',
					'fontfamily',
					'fontcolor',
					'formulaEditor'
				],
				deniedTags: ['style', 'head', 'body', 'html'],
				//Incluir la ruta del archivo encargado de subir las imágenes al servidor
				imageEditable: false,
				imageResizable: false,
				dragImageUpload: false,
				dragFileUpload: false,
				imageLink: false,
				imageUpload: 'redactorImageManagement.php'
			});
		}
    </script>
</head>
    <body onLoad="buildRedactor()" onresize="Resize()">
        <div style="width: 655px; height: 200px;">
			<textarea id="content" name="content"><?echo $aText;?></textarea>
            <div style="width: 425px; height: 50px; display: inline-block">
            <button id="closeButton" onClick="ResultEditHTML()" style="height: 22px; margin: 5px -5px; border-radius: 5px;border-color: burlywood; cursor: pointer;font-family: Arial; font-size: 13px;"> <?=translate("Save")?> </button> 
            </div>
        </div>
    </body>
</html>