<?php

require_once("repository.inc.php");
require_once("initialize.php");

//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
class BITAMeFormsUser extends BITAMObject
{
	public $UserID;
	public $UserName;
	public $Password;
	public $FullName;
	public $Position;
	public $EMail;
	public $UserLanguageID;
	public $UserLanguageTerminator;
	public $Role;
	public $intConfigSecurity;
	public $OwnerID;
	public $OwnerName;
	public $Ektos_Admin;
	public $Ektos_User;
	public $ChangePassword;
	public $LdapPath;
	
	//@JAPR 2012-08-30: Agregada por compatibilidad con eBavel
	const mainSQL = "select A.CLA_USUARIO, A.NOM_LARGO, A.NOM_CORTO, A.CUENTA_CORREO, A.PASSWORD
	, A.CONFIG_SECURITY, A.TIPO_USUARIO, A.ADVISOR_ADMIN, A.ADVISOR_DESKTOP
	, A.LAST_DISABLED, A.LAST_PWD_UPDATED, A.PASSWORDV, A.LDAP_PATH from SI_USUARIO A";
	//@JAPR
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->UserID = -1;
		$this->UserName = "";
		$this->Password = "";
		$this->FullName = "";
		$this->Position = "";
		$this->EMail = "";
		$this->UserLanguageID = 0;
		$this->UserLanguageTerminator = "";
		$this->Role = -1;
		$this->intConfigSecurity = 0;
		$this->OwnerID = -1;
		$this->OwnerName = "";
		$this->Ektos_Admin = -1;
		$this->Ektos_User = -1;
		$this->ChangePassword = 0;
		$this->LdapPath = "";
	}

	static function NewInstance($aRepository)
	{
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		return new BITAMeFormsUser($aRepository);
	}

	static function NewInstanceWithID($aRepository, $aUserID, $withPasswords = true)
	{
		$anInstance = null;

		if (((int) $aUserID) < 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.CLA_USUARIO AS UserID ".
				", A.NOM_CORTO AS UserName ".
				", A.PASSWORD AS Password ".
				", A.NOM_LARGO AS FullName ".
				", A.PUESTO AS Position ".
				", A.CUENTA_CORREO AS EMail ".
				", A.WHTMLANG AS UserLanguageID ".
				", A.CONFIG_SECURITY AS ConfigSecurity ".
				", A.CLA_OWNER AS OwnerID ".
				", A.LDAP_PATH AS LdapPath ".
				", B.NOM_CORTO AS OwnerName ".
			"FROM SI_USUARIO A, SI_USUARIO B ".
			"WHERE A.CLA_OWNER = B.CLA_USUARIO AND A.CLA_USUARIO = ".((int) $aUserID);

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$anInstance = BITAMeFormsUser::NewInstanceFromRS($aRepository, $aRS, $withPasswords);
		}

		return $anInstance;
	}
	
	static function getIDWithEmail ($aRepository, $emails = array()) {
		$arrUsersIDs = array();
		$usersEmails = ("'" . implode('", "', $emails) . "'");
		$sql = "SELECT A.CUENTA_CORREO AS EMail, A.A.CLA_USUARIO AS UserID FROM SI_USUARIO A, SI_USUARIO B ".
				"WHERE A.CLA_OWNER = B.CLA_USUARIO AND A.CUENTA_CORREO IN (".	$usersEmails .")";
				
		var_dump($sql);
		$aRS = $aRepository->ADOConnection->Execute($sql);
		while (!$aRS->EOF) {
			$intClaUsuario = (int) @$aRS->fields["userid"];
			//$strEMail = (string) @$aRS->fields["email"];
			if ($intClaUsuario > 0) {
				$arrUsersIDs[] = $intClaUsuario;
			}
			$aRS->MoveNext();
		}
		return $arrUsersIDs;
	}

	//@JAPR 2013-02-27: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
	static function NewInstanceWithEMail($aRepository, $anEMail, $withPasswords = true)
	{
		//@JAPR 2013-08-09: Agregado el proceso de regrabado de preguntas basado en los archivos .dat sobre capturas ya almacenadas en las tablas de datos
		global $gblShowErrorSurvey;
		
		$anInstance = null;
		
		if (trim((string) $anEMail) == '')
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.CLA_USUARIO AS UserID ".
				", A.NOM_CORTO AS UserName ".
				", A.PASSWORD AS Password ".
				", A.NOM_LARGO AS FullName ".
				", A.PUESTO AS Position ".
				", A.CUENTA_CORREO AS EMail ".
				", A.WHTMLANG AS UserLanguageID ".
				", A.CONFIG_SECURITY AS ConfigSecurity ".
				", A.CLA_OWNER AS OwnerID ".
				", A.LDAP_PATH AS LdapPath ".
				", B.NOM_CORTO AS OwnerName ".
			"FROM SI_USUARIO A, SI_USUARIO B ".
			"WHERE A.CLA_OWNER = B.CLA_USUARIO AND A.CUENTA_CORREO = ".$aRepository->ADOConnection->Quote((string) $anEMail);
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
		    if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		    else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		}
		
		if (!$aRS->EOF)
		{
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$anInstance = BITAMeFormsUser::NewInstanceFromRS($aRepository, $aRS, $withPasswords);
		}
		
		return $anInstance;
	}
	
	//Regresa un array con los nombres largos (cuentas de correo) de todas las ocurrencias de usuarios que incluyan la porción especificada,
	//indexada por la clave del usuario
	static function GetUsersArrayWithEMailLike($aRepository, $anEMail)
	{
		//@JAPR 2013-08-09: Agregado el proceso de regrabado de preguntas basado en los archivos .dat sobre capturas ya almacenadas en las tablas de datos
		global $gblShowErrorSurvey;
		
		$arrUsers = array();
		if (trim((string) $anEMail) == '')
		{
			return $arrUsers;
		}
		
		//@JAPR 2016-12-28: Corregido un bug, al ser un like, estaba permitiendo entrar con cuentas ligeramente diferentes siempre y cuando tuvieran un patrón LIKE válido (#IF5J19)
		$sql = "SELECT A.CLA_USUARIO AS UserID ".
				", A.NOM_CORTO AS UserName ".
				", A.PASSWORD AS Password ".
				", A.NOM_LARGO AS FullName ".
				", A.PUESTO AS Position ".
				", A.CUENTA_CORREO AS EMail ".
				", A.WHTMLANG AS UserLanguageID ".
				", A.CONFIG_SECURITY AS ConfigSecurity ".
				", A.CLA_OWNER AS OwnerID ".
				", A.LDAP_PATH AS LdapPath ".
				", B.NOM_CORTO AS OwnerName ".
			"FROM SI_USUARIO A, SI_USUARIO B ".
			"WHERE A.CLA_OWNER = B.CLA_USUARIO AND A.CUENTA_CORREO = ".$aRepository->ADOConnection->Quote('%'.((string) $anEMail).'%');
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
		    if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		    else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		}
		
		while (!$aRS->EOF) {
			$intClaUsuario = (int) @$aRS->fields["userid"];
			$strEMail = (string) @$aRS->fields["email"];
			if ($intClaUsuario > 0) {
				$arrUsers[$intClaUsuario] = $strEMail;
			}
			$aRS->MoveNext();
		}
		
		return $arrUsers;
	}
	
	//@JAPR 2012-08-30: Agregada por compatibilidad con eBavel
	//GCruz - Cargar el usuario con el NOM_CORTO
	public static function withNomCortoFromDB($sUser, $aConnection)
	{
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$aSQL = BITAMeFormsUser::mainSQL;

		if (is_null($sUser)||$sUser=="")
		{
			return NULL;
		}
		else
		{
			$aSQL .= " where A.NOM_CORTO = ".$aConnection->Quote($sUser);
		}

		$aSQL .= " order by A.NOM_CORTO";
		$recordSet = $aConnection->Execute($aSQL);

		$result = array();

		if(!$recordSet->EOF)
		{
			//@JAPR 2012-08-30: Para no replicar todo el modo de trabajo de eBavel, simplemente se forzará a cargar la instancia cada que se pida
			$aUser = null;
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			//$aUser = BITAMeFormsUser::withKey($recordSet->fields[0]);
			
			if (is_null($aUser))
			{
				//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
				$aUser = new BITAMeFormsUser($aConnection);
				$aUser->fillFromRowDB($recordSet->fields);
				//@JAPR 2012-08-30: Para no replicar todo el modo de trabajo de eBavel, simplemente se forzará a cargar la instancia cada que se pida
				//$aUser->register();
			}
		}
		else
		{
			return NULL;
		}

		$recordSet->close();
		
		return($aUser);
	}
	
	function getSubordinados($aConnection)
	{
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$aSQL = BITAMeFormsUser::mainSQL;

		$aSQL .= " LEFT JOIN si_forms_users_det B on B.cla_master = A.CLA_USUARIO
				WHERE B.cla_detail = ". $this->cla_usuario;
		$recordSet = $aConnection->Execute($aSQL);

		$result = array();

		while (!$recordSet->EOF)
		{
			//@JAPR 2012-08-30: Para no replicar todo el modo de trabajo de eBavel, simplemente se forzará a cargar la instancia cada que se pida
			$aUser = null;
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			//$aUser = BITAMeFormsUser::withKey($recordSet->fields[0]);

			if (is_null($aUser))
			{
				//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
				$aUser = new BITAMeFormsUser($aConnection);
				$aUser->fillFromRowDB($recordSet->fields);
				//@JAPR 2012-08-30: Para no replicar todo el modo de trabajo de eBavel, simplemente se forzará a cargar la instancia cada que se pida
				//$aUser->register();
			}

			$result[] = $aUser;

			$recordSet->MoveNext();
		}

		$recordSet->close();
		
		return $result;
	}	
	//@JAPR
	
	static function WithUserName($aRepository, $aUserName)
	{
		$anInstance = null;
		if ($aUserName == "")
		{
			return $anInstance;
		}

		$sql = "SELECT A.CLA_USUARIO AS UserID ".
				", A.NOM_CORTO AS UserName ".
				", A.PASSWORD AS Password ".
				", A.NOM_LARGO AS FullName ".
				", A.PUESTO AS Position ".
				", A.CUENTA_CORREO AS EMail ".
				", A.WHTMLANG AS UserLanguageID ".
				", A.CONFIG_SECURITY AS ConfigSecurity ".
				", A.CLA_OWNER AS OwnerID ".
				", A.LDAP_PATH AS LdapPath ".
				", B.NOM_CORTO AS OwnerName ".
			"FROM SI_USUARIO A, SI_USUARIO B ".
			"WHERE A.CLA_OWNER = B.CLA_USUARIO AND A.NOM_CORTO = ".$aRepository->ADOConnection->Quote($aUserName);

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$anInstance = BITAMeFormsUser::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}
	
	static function GetUserName($aRepository, $anUserID)
	{
		$anInstance =& BITAMGlobalFormsInstance::GetUserNameWithID($anUserID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}

		$userName="";
		$userFullName="";
		
		$sql = "SELECT NOM_CORTO, NOM_LARGO FROM SI_USUARIO WHERE CLA_USUARIO = ".$anUserID;
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
 		    $userName = $aRS->fields["nom_corto"];
			$userFullName = $aRS->fields["nom_largo"];
		}

		if( trim($userFullName)!="")
		{
			BITAMGlobalFormsInstance::AddUserNameWithID($anUserID, $userFullName);
			return($userFullName);
		}
		else 
		{
			BITAMGlobalFormsInstance::AddUserNameWithID($anUserID, $userName);
			return($userName);		
		}
	}

	//@JAPR 2012-08-30: Agregada por compatibilidad con eBavel
	public function fillFromRowDB($aRow)
	{
		$this->UserName = rtrim($aRow[2]);
		$this->cla_usuario = $aRow[0];
		$this->nom_largo = rtrim($aRow[1]);
		$this->nom_corto = rtrim($aRow[2]);
		$this->cuenta_correo = rtrim($aRow[3]);
		$this->password = BITAMDecryptPassword(rtrim($aRow[4]));
		$this->config_security = rtrim($aRow[5]);
		//$this->ebavel_user = intval($aRow[13]);
	}
	//@JAPR
	
	static function NewInstanceFromRS($aRepository, $aRS, $withPasswords=true)
	{
		$aUserID=(int) $aRS->fields["userid"];
		$anInstance=null;
		$anInstance=BITAMGlobalInstance::GetUserInstanceWithID($aUserID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}

		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$anInstance = BITAMeFormsUser::NewInstance($aRepository);
		$anInstance->UserID = (int) $aRS->fields["userid"];
		$anInstance->UserName = strtoupper(rtrim($aRS->fields["username"]));
		
		if($anInstance->UserID==0)
		{
			$anInstance->intConfigSecurity = 0;
		}
		else
		{
			$anInstance->intConfigSecurity = $aRS->fields["configsecurity"];	
		}
		
		if($withPasswords)
		{
			if($anInstance->intConfigSecurity == 1)
			{
				if(isset($_SESSION["PAtheUserID"]) && $anInstance->UserID==$_SESSION["PAtheUserID"])
				{
					$anInstance->Password = (isset($_SESSION["PAthePassword"])?$_SESSION["PAthePassword"]:"");
				}
				else 
				{
					$anInstance->Password = GetADPwd($anInstance->UserName);
				}
			}
			else 
			{
				$anInstance->Password = rtrim($aRS->fields["password"]);
			}
		}
		
		$anInstance->FullName = rtrim($aRS->fields["fullname"]);
		$anInstance->Position = rtrim($aRS->fields["position"]);
		$anInstance->EMail = rtrim($aRS->fields["email"]);
		$anInstance->UserLanguageID = (int) $aRS->fields["userlanguageid"];
		$anInstance->OwnerID = (int) $aRS->fields["ownerid"];
		$anInstance->OwnerName = $aRS->fields["ownername"];
		$anInstance->LdapPath = rtrim($aRS->fields["ldappath"]);
		$anInstance->getUserLanguageTerminator();
		$anInstance->getRole();

		BITAMGlobalInstance::AddUserInstanceWithID($aUserID,$anInstance);

		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("UserID", $aHTTPRequest->POST))
		{
			$aUserID = $aHTTPRequest->POST["UserID"];
			if (is_array($aUserID))
			{
				//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
				$aCollection = BITAMeFormsUserCollection::NewInstance($aRepository, $aUserID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
				$anInstance = BITAMeFormsUser::NewInstanceWithID($aRepository, (int) $aUserID);
				if (is_null($anInstance))
				{
					//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
					$anInstance = BITAMeFormsUser::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
						$anInstance = BITAMeFormsUser::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("UserID", $aHTTPRequest->GET))
		{
			$aUserID = $aHTTPRequest->GET["UserID"];
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$anInstance = BITAMeFormsUser::NewInstanceWithID($aRepository, (int) $aUserID, false);
			if (is_null($anInstance))
			{
				//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
				$anInstance = BITAMeFormsUser::NewInstance($aRepository);
			}
		}
		else
		{
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$anInstance = BITAMeFormsUser::NewInstance($aRepository);
		}
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
 		if (array_key_exists("UserID", $anArray))
		{
			$this->UserID = (int) $anArray["UserID"];
		}
 		if (array_key_exists("UserName", $anArray))
		{
			$this->UserName = strtoupper(stripslashes($anArray["UserName"]));
		}
 		if (array_key_exists("Password", $anArray))
		{
			$this->Password = stripslashes($anArray["Password"]);
		}
 		if (array_key_exists("FullName", $anArray))
		{
			$this->FullName = stripslashes($anArray["FullName"]);
		}

		if (array_key_exists("Position", $anArray))
		{
			$this->Position = stripslashes($anArray["Position"]);
		}

 		if (array_key_exists("EMail", $anArray))
		{
			$this->EMail = stripslashes($anArray["EMail"]);
		}
		if (array_key_exists("UserLanguageID", $anArray))
		{
			$this->UserLanguageID = (int) $anArray["UserLanguageID"];
			$this->getUserLanguageTerminator();
		}
		if (array_key_exists("Role", $anArray))
		{
			$this->Role = (int) $anArray["Role"];
		}
		
		if(array_key_exists("intConfigSecurity", $anArray))
		{
			$this->intConfigSecurity = (int) $anArray["intConfigSecurity"];
		}
		
	 	if (array_key_exists("ChangePassword", $anArray))
		{
			$this->ChangePassword = (int) $anArray["ChangePassword"];
		}

		return $this;
	}

	function save()
	{
	 	if ($this->isNewObject())
		{
			$initialKey = GetBitamInitialKey($this->Repository);
			
			$sql =  "SELECT ".
						$this->Repository->ADOConnection->IfNull("MAX(CLA_USUARIO)", "".$initialKey."")." + 1 AS UserID".
						" FROM SI_USUARIO";

			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				//die("(".__METHOD__.") "."Error accessing SI_USUARIO table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
				die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$temporalID = (int)$aRS->fields["userid"];

			if($temporalID<($initialKey+1))
			{
				$temporalID = $initialKey+1;
			}

			$this->UserID = $temporalID;

			// Obtenemos el id del usuario logeado, primero lo tomamos de la sesion
			$this->OwnerID = $_SESSION["PABITAM_UserID"];

			$sql = "INSERT INTO SI_USUARIO (".
			            "CLA_USUARIO".
			            ",NOM_CORTO".
			            ",PASSWORD".
			            ",NOM_LARGO".
			            ",PUESTO".
			            ",CUENTA_CORREO".
			            ",WHTMLANG".
			            ",EKTOS_ADMIN".
			            ",EKTOS_USER".
			            ",TIPO_USUARIO".
			            ",ARTUS_DESKTOP".
			            ",ARTUS_ADMIN".
			            ",ARTUS_DESIGNER".
			            ",PAPIRO_ADMIN".
						",PAPIRO_DESIGNER".
						",PAPIRO_DESKTOP".
						",ADVISOR_ADMIN".
						",ADVISOR_DESKTOP".
						",STRATEGO_ADMIN".
						",STRATEGO_DESKTOP".
			            ",CLA_OWNER".
						") VALUES (".
						$this->UserID.
						",".$this->Repository->ADOConnection->Quote(strtoupper($this->UserName)).
						",".$this->Repository->ADOConnection->Quote(BITAMEncryptPassword($this->Password)).
						",".$this->Repository->ADOConnection->Quote($this->FullName).
						",".$this->Repository->ADOConnection->Quote($this->Position).
						",".$this->Repository->ADOConnection->Quote($this->EMail).
						",".$this->UserLanguageID.
						",".( ( $this->Role == 1 ) ? 1 : 0).
						",".( ( $this->Role == 0 ) ? 1 : 0).
						","." 2 ".
						","." 1 ".
						","." 0 ".
						","." 0 ".
						","." 0 ".
						","." 0 ".
						","." 0 ".
						","." 0 ".
						","." 0 ".
						","." 0 ".
						","." 0 ".
						",".$this->OwnerID.
						")";
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else
		{
			$sql= "SELECT WHTMLANG AS UserLanguageID FROM SI_USUARIO WHERE CLA_USUARIO =".$this->UserID;
			$aRS=$this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$oldUserLanguageID=null;
			if(!$aRS->EOF)
			{
				$oldUserLanguageID = (int) $aRS->fields["userlanguageid"];
			}
			
			$sql = "UPDATE SI_USUARIO SET ";
			//@JAPR 2008-07-17: Con usuarios de LDAP no se debe actualizar el password ni el nombre corto
			//ConfigSecurity = 0 (Usuario BITAM), ConfigSecurity = 1 (Usuario LDAP)
			if ($this->intConfigSecurity == 0)
			{
				$sql .= "NOM_CORTO = ".$this->Repository->ADOConnection->Quote($this->UserName);
				
				if($this->ChangePassword)
				{	
					$sql .= ", PASSWORD = ".$this->Repository->ADOConnection->Quote(BITAMEncryptPassword($this->Password));
				}
				
				$sql.=", ";
			}
			//@JAPR
			$sql .= "NOM_LARGO = ".$this->Repository->ADOConnection->Quote($this->FullName).
						",PUESTO = ".$this->Repository->ADOConnection->Quote($this->Position).
						",CUENTA_CORREO = ".$this->Repository->ADOConnection->Quote($this->EMail).
						",WHTMLANG = ".$this->UserLanguageID.
						",EKTOS_ADMIN = ".( ( $this->Role == 1 ) ? 1 : 0).
						",EKTOS_USER = ".( ( $this->Role == 0 ) ? 1 : 0).
						" WHERE CLA_USUARIO = ".$this->UserID;
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Si el usuario modificado es el usuario logueado y se cambió su lenguage,
			//se debe actualizar la variable $_SESSION["PAuserLanguage"]
			if($this->UserID==$_SESSION["PAtheUserID"])
			{
				if ($oldUserLanguageID!==$this->UserLanguageID)
				{
					if($this->UserLanguageID<=0 || $this->UserLanguageID>8)
					{	require_once("settingsvariable.inc.php");
						list($userLanguageID, $userLanguage) = BITAMSettingsVariable::getDefautlLanguageTerminator($this->Repository);
					}
					else
					{
						$userLanguage=$this->UserLanguageTerminator;
						$userLanguageID=$this->UserLanguageID;
					}
					
					$_SESSION["PAuserLanguageID"]=$userLanguageID;
					$_SESSION["PAuserLanguage"]=$userLanguage;
					InitializeLocale($_SESSION["PAuserLanguageID"]);
					LoadLanguageWithName($_SESSION["PAuserLanguage"]);
					$_SESSION["PAchangeLanguage"]=true;
				}
				$_SESSION["PABITAM_UserName"] = $this->UserName;
				setcookie("PABITAM_UserName", $this->UserName);
			}
		}
		
		$this->Repository->addPhPCacheRequest(MDOBJECT_User, $this->UserID, MDOBJECTDATA_All);
		return $this;
	}
	
	function remove()
	{		
		$sql = "DELETE FROM SI_USUARIO WHERE CLA_USUARIO = ".$this->UserID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->UserID < 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("User");
		}
		else
		{
			return $this->UserName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=User";
		}
		else
		{
			return "BITAM_PAGE=User&UserID=".$this->UserID;
		}
	}

	function get_Parent()
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			return BITAMeFormsUserCollection::NewInstance($this->Repository);
		}
		else 
		{
			//return $this->Repository;
			return $this;
		}
	}
	
	function get_PathArray()
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$parent = $this->get_Parent();
			$pathArray = $parent->get_PathArray();
		}
		else
		{
			$pathArray = array();
		}
		
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'UserID';
	}

	function get_FormFields($aUser)
	{
		$displayFields = true;
		$isReadOnly = false;
		
		//Verificar si se está en modo FBM y con usuario logueado diferente a Supervisor
		if($_SESSION["PAFBM_Mode"]==1 && $_SESSION["PAtheUserID"]!=0)
		{
			$displayFields = false;
			$isReadOnly = true;
		}
		
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserName";
		$aField->Title = translate("User Name");
		$aField->Type = "String";
		$aField->Size = 15;
		$aField->FormatMask = "aW";
		
		//Si es usuario nuevo, se le asigna como propietario el usuario logueado
		if($this->UserID==-1)
		{
			$this->OwnerID = $_SESSION["PAtheUserID"];
		}
		//@JAPR 2008-07-17: Con usuarios de LDAP no se debe actualizar el password ni el nombre corto
		//No se debe permitir cambiar el nombre corto del usuario SUPERVISOR (userID=0)
		//Sólo el owner del usuario puede cambiar el nombre corto
		//Sólo puede cambiarse el nombre corto si el campo Ldap_Path es vacío
		if ($this->intConfigSecurity == 1 || $this->UserID == 0 || $this->OwnerID!=$_SESSION["PAtheUserID"] || rtrim($this->LdapPath)!="" || $isReadOnly==true)
		{
			$aField->IsDisplayOnly = true;
		}
	
		//@JAPR
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Password";
		$aField->Title = translate("Password");
		$aField->Type = "String";
		$aField->IsVisible = false;
		$aField->IsDisplayOnly = $isReadOnly;
		$aField->AfterMessage = "<span id=spanChangePassword unselectable=on style=\"padding-top:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);backgroun-position:center center;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openChangePassword()\">"
		.translate("Change").
		"</span>";
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FullName";
		$aField->Title = translate("Full Name");
		$aField->Type = "String";
		$aField->Size = 60;
		$aField->IsDisplayOnly = $isReadOnly;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Position";
		$aField->Title = translate("Job Position");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsDisplayOnly = $isReadOnly;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "EMail";
		$aField->Title = translate("E-Mail");
		$aField->Type = "String";
		$aField->Size = 250;
		$aField->IsDisplayOnly = $isReadOnly;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$languages=BITAMeFormsUser::getLanguages($this->Repository, true);
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserLanguageID";
		$aField->Title = translate("Language");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $languages;
		$aField->IsDisplayOnly = $isReadOnly;
		$myFields[$aField->Name] = $aField;

		$aTypes = array();
		$aTypes[0] = translate("User");
		$aTypes[1] = translate("Administrator");
			
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Role";
		$aField->Title = translate("Role");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			if($isReadOnly==true)
			{
				$aField->IsDisplayOnly = true;
			}
			else 
			{
				$aField->IsDisplayOnly = false;
			}
		}
		else 
		{
			$aField->IsDisplayOnly = true;
		}
		
		$aField->Options = $aTypes;
		$myFields[$aField->Name] = $aField;
		
		//if($displayFields==true)
		//{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "OwnerName";
			$aField->Title = (($displayFields==true)?(translate("Owner")):(""));
			$aField->Type = "String";
			$aField->Size = 15;
			$aField->IsDisplayOnly = true;
			$aField->IsVisible = $displayFields;
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ChangePassword";
			$aField->Title = "";
			$aField->Type = "Integer";
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
			
			if($_SESSION["PABITAM_UserRole"]==1)
			{
				$isDisplayOnly = false;
			}
			else 
			{
				$isDisplayOnly = true;
			}
		//}
		return $myFields;
	}
	
	function insideEdit()
	{
		//Sólo si es usuario BITAM se puede cambiar el Password
		//Con usuarios de LDAP no se debe actualizar el password
		if ($this->intConfigSecurity == 0)
		{
?>		
		objSpanChangePassword = document.getElementById("spanChangePassword");
		objSpanChangePassword.style.backgroundImage = 'url(images\\btnBackground.gif)';
		objSpanChangePassword.style.cursor = 'pointer';
		objSpanChangePassword.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChangePassword.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChangePassword.onclick = new Function("openChangePassword();");
<?
		}
	}
	
	function insideCancel()
	{
?>
		objSpanChangePassword = document.getElementById("spanChangePassword");
		objSpanChangePassword.style.backgroundImage = 'url(images\\btnBackground_off.gif)';
		objSpanChangePassword.style.cursor = 'default';
		objSpanChangePassword.onmouseover = "";
		objSpanChangePassword.onmouseout = "";
		objSpanChangePassword.onclick = "";
<?
	}
	
	function getLanguages($aRepository, $withDefaultOption)
	{
		$languagesArray=array();
		
		/*
		Idiomas soportados por EktosBS:
		(2) "English";
		(8) "English - United Kingdom";
		(7) "Español España";
		(1) "Español Mexico";
		(6) "Português Brasil";
		(4) "Portuguese Portugal";
		*/

		$sql="SELECT CLA_IDIOMA, IDIOMA FROM SI_IDIOMA WHERE CLA_IDIOMA IN (2, 8, 7, 1, 6, 4) ORDER BY IDIOMA";
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_IDIOMA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if($withDefaultOption)
		{
			$languagesArray[0]="DEFAULT";
		}
		while (!$aRS->EOF)
		{
			$languageID=(int)$aRS->fields["cla_idioma"];
			$languageDesc=trim($aRS->fields["idioma"]);
			$languagesArray[$languageID]=$languageDesc;
			$aRS->MoveNext();
		}

		return $languagesArray;		
	}
	
	function getUserLanguageTerminator()
	{	
		if(is_null($this->UserLanguageID))
		{
			return;
		}
		$sql="SELECT TERMINADOR FROM SI_IDIOMA WHERE CLA_IDIOMA=".$this->UserLanguageID;
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_IDIOMA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$this->UserLanguageTerminator = trim($aRS->fields["terminador"]);
		}
	}
	
	function getDashboardsIDsByUser($aRepository, $aUserID)
	{
		 $sql = " SELECT  CLA_ESCENARIO AS DashboardID, NOM_ESCENARIO AS DashboardName FROM SI_ESCENARIO a, SI_GPO_ESCENARIO b ".
				" WHERE a.CLA_USUARIO = b.CLA_USUARIO AND a.CLA_GPO_ESC = b.CLA_GPO_ESC AND (a.CLA_USUARIO =".$aUserID." OR ".
				" CLA_ESCENARIO IN (SELECT CLA_ESCENARIO AS DashboardID FROM SI_ESC_PUB WHERE CLA_SUSCRIPTOR =".$aUserID.") OR ".
				" CLA_ESCENARIO IN (SELECT CLA_ESCENARIO AS DashboardID FROM SI_ESC_GPO WHERE CLA_GPO IN (SELECT CLA_ROL FROM".
				" SI_ROL_USUARIO WHERE CLA_USUARIO =".$aUserID."))) ORDER BY NOM_ESCENARIO";
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_ESCENARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();
		$count=0;
		
		while (!$aRS->EOF)
		{
			$allValues[$count] = (int) $aRS->fields["dashboardid"];
			$aRS->MoveNext();
			$count++;
		}
		
		return $allValues;
	}
	
	function FavoritesIDsByUser($aRepository, $aUserID)
	{
		if($_SESSION["PAArtusWebVersion"]==5.0)
		{
			$sql=" SELECT CLA_ANF,NOMBRE FROM SI_FAVORITOS A WHERE A.CONSECUTIVO = 1 AND (A.CLA_USUARIO =".$aUserID." OR 
			 CLA_ANF IN (SELECT CLA_ANF FROM SI_FAVORITOS_USER WHERE CLA_USUARIO =".$aUserID.") OR 
			 CLA_ANF IN (SELECT CLA_ANF FROM SI_FAVORITOS_ROL WHERE CLA_ROL IN (SELECT CLA_ROL FROM SI_ROL_USUARIO WHERE CLA_USUARIO =".$aUserID.")))
			 ORDER BY NOMBRE";
		}
		else
		{
			$sql=" SELECT CLA_ANF,NOMBRE FROM SI_FAV6 A WHERE (A.CLA_USUARIO =".$aUserID." OR 
			 CLA_ANF IN (SELECT CLA_ANF FROM SI_FAV6_USER WHERE CLA_USUARIO =".$aUserID.") OR 
			 CLA_ANF IN (SELECT CLA_ANF FROM SI_FAV6_ROL WHERE CLA_ROL IN (SELECT CLA_ROL FROM SI_ROL_USUARIO WHERE CLA_USUARIO =".$aUserID.")))
			 ORDER BY NOMBRE";
		}

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_FAVORITOS ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();
		$count=0;
		
		while (!$aRS->EOF)
		{
			$allValues[$count] = (int) $aRS->fields["cla_anf"];
			$aRS->MoveNext();
			$count++;
		}
		
		return $allValues;
	}

	function getRole()
	{
		$sql = "SELECT EKTOS_ADMIN as RoleAdmin, EKTOS_USER as RoleUser FROM SI_USUARIO WHERE CLA_USUARIO = ".$this->UserID;
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$roleAdmin = (int) $aRS->fields["roleadmin"];
			$roleUser = (int) $aRS->fields["roleuser"];
			
			$this->Ektos_Admin=$roleAdmin;
			$this->Ektos_User=$roleUser;
			
			if($roleAdmin >= 1)
			{
				$this->Role = 1;
			}
			else
			{
				$this->Role = 0;
			}
		}
	}
	
	static function getUserPassword($aRepository, $aUserID)
	{
		$userPassword="";
		
		$sql = "SELECT PASSWORD FROM SI_USUARIO WHERE CLA_USUARIO = ".$aUserID;
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
 		    $userPassword = rtrim($aRS->fields["password"]);
 		    if($userPassword!="")
 		    {
 		    	$userPassword = BITAMDecryptPassword($userPassword);
 		    }
		}
		
		return $userPassword;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{	
 		$userNames=array();
 		
		$sql = "SELECT NOM_CORTO AS UserName FROM SI_USUARIO";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		while (!$aRS->EOF)
		{
			$userNames[] = strtoupper($aRS->fields["username"]);
			$aRS->MoveNext();
		}
		
		$numUsers=count($userNames);

 ?>
 		<script language="JavaScript">
 		
 		userNames= new Array;
 		
<?
		for($i=0;$i<$numUsers;$i++)
		{
?>
			userNames[<?=$i?>]='<?=$userNames[$i]?>';
<?
		}
?>
 		function verifyFieldsAndSave(target)
 		{
 			var oldUserName='<?=$this->UserName?>';
			//JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
 			var UserNameStrAux=Trim(BITAMeFormsUser_SaveForm.UserName.value);
 			var UserNameStr=UserNameStrAux.toUpperCase()
 			var statusTimeFields = true;
 			var i;
 			
 			if(UserNameStr=='')
 			{
 				alert ('<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Name"))?>');
 			}
 			else
 			{   
 				if(inArray(userNames,UserNameStr)==true && oldUserName != UserNameStr)
 				{
 					alert ('<?=sprintf(translate("%s already exists"),translate("This User Name"))?>');
 				}
 				else
 				{
					//JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
 					BITAMeFormsUser_Ok(target);
 				}
 			}
 		}
		
	 	function openChangePassword()
	 	{
			//JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			var userIDAux = BITAMeFormsUser_SaveForm.UserID.value;
			strPassword=window.showModalDialog('dialog.html', 'changeUserPassword.php?userID='+ userIDAux, 'help: no; status: no; scroll: yes; resizable: no; dialogHeight:250px; dialogWidth:280px;');
			//strPassword=window.open('changeUserPassword.php?userID='+ userIDAux);
		
			if(strPassword!=null && strPassword!= undefined)
			{
				//JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
				objChangePassword = BITAMeFormsUser_SaveForm.ChangePassword;
				objChangePassword.value = 1;
				//JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
				objPassword = BITAMeFormsUser_SaveForm.Password;
				objPassword.value = strPassword;
			}
	 	}
	 	
		function analyzeCharacter(obj)
		{
			//Obtener el valor del input text
			var temp = obj.value;
		
			//Crear nuestra expresion regular con digitos
			var reg0Str = '^[0-2]{1}[0-9]{1}:?[0-5]{1}[0-9]{1}$';	
		
			//Hacemos la instancia RegExp para que se cree
			//un objeto tipo expresion regular
			var reg0 = new RegExp(reg0Str);
		
			//Evaluamos nuestra cadena o valor que se encuentra en nuestro input text
			//Si regresa true esto quiere decir que hasta el momento si se ha cumplido
			//con la expresión regular
			if (reg0.test(temp)) return true;
		
			//Creamos una cadena con todos los caracteres permitidos en nuestra entrada de texto, esto es digitos 
			//y los dos puntos, el [^.....] indica que se hara una busqueda para todos aquellos caracteres 
			//que no esten entre los corchetes
			var reg1Str = '[^0-9' + ':' + ']';
		
			//Creamos nuestra expresión regular, pero se le indica ahora, que es una expresión regular
			//con búsqueda global para todas las entradas de texto que no cumplen con lo especificado en reg1Str
			var reg1 = new RegExp(reg1Str, 'g');
			
			//Se reemplaza dichos caracteres con vacío
			temp = temp.replace(reg1, '');
		
			//Creamos otra expresión regular que se aplica para todas
			//aquellas entradas de texto que tengan dos puntos 
			var reg3 = /:/g;
				
			//Se revisa si la cadena tiene dos puntos
			var reg3Array = reg3.exec(temp);
		
			//En este caso cuando tenga dos puntos
			if (reg3Array != null) 
			{
				if(temp.length<3)
				{
					//Se debe haber capturado la fecha en formato HH:MM
					//se procede a eliminar el dos puntos
					
					//Se obtiene la cadena antes de los dos puntos
					temp = temp.substring(0, reg3Array.index);
				}
				else
				{
					// Se mantiene solo la primera ocurrencia de los dos puntos
					// y dos numeros siguientes como maximo
					
					//Se obtiene la cadena despues de los dos puntos
					var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
					
					//En cada ocurrencia adicional de dos puntos se reemplaza por cadena vacia
					reg3Right = reg3Right.replace(reg3, '');
					
					//Despues de ello se obtiene las dos primeras posiciones siguientes
					reg3Right = reg3Right.substring(0, 2);
					
					if(reg3Right.length>0)
					{
						var firstMinute = parseInt(reg3Right.charAt(0));
						
						//Si el primer digito del minuto es mayor q 5 se tiene q eliminar dicho digito
						if(firstMinute>5)
						{
							reg3Right = reg3Right.substring(1);
						}
					}
					
					//Se forma la nueva cadena con dos puntos
					temp = temp.substring(0,reg3Array.index) + ':' + reg3Right;
				}
			}
			else
			{
				//Si se introdujo un digito mas, se elimina porque el formato debe ser HH:MM
				if(temp.length > 2)
				{
					temp = temp.substring(0, 2);		
				}
				else if(temp.length == 1)
				{
					//Revisar que primer digito de hora sea entre 1-2, el [^.....] indica que se hara una busqueda
					//para todos aquellos caracteres que no esten entre los corchetes
					var regStrHour1 = '[^0-2]';
				
					//Creamos nuestra expresión regular, pero se le indica ahora, que es una expresión regular
					//con búsqueda global para todas las entradas de texto que no cumplen con lo especificado en regHour1
					var regHour1 = new RegExp(regStrHour1, 'g');
					
					//Se reemplaza dichos caracteres con vacío
					temp = temp.replace(regHour1, '');
				}
				else if(temp.length == 2)
				{
					//Revisar que segundo digito de hora sea entre 1-9 el [^.....] indica que se hara una busqueda
					//para todos aquellos caracteres que no esten entre los corchetes
					var regStrHour2 = '[^0-9]';
				
					//Creamos nuestra expresión regular, pero se le indica ahora, que es una expresión regular
					//con búsqueda global para todas las entradas de texto que no cumplen con lo especificado en regHour2
					var regHour2 = new RegExp(regStrHour2, 'g');
					
					//Se reemplaza dichos caracteres con vacío
					temp = temp.replace(regHour2, '');
					
					if(temp.length == 2)
					{
						var firstDigit = parseInt(temp.charAt(0));
						var secondDigit = parseInt(temp.charAt(1));
						
						if(firstDigit == 2 && secondDigit >3)
						{
							temp = temp.substring(0, 1);
						}
					}
				}
			}
		
			//Se asigna nuevamente el valor a obj.value
			obj.value = temp;
		}

		function blockNonNumbers(obj, e)
		{
			var key;
			var isCtrl = false;
			var keychar;
			var reg;
		
			//Para Internet Explorer por eso de las compatibilidades
			if(window.event) 
			{
				key = e.keyCode;
				isCtrl = window.event.ctrlKey
			}
			//Para navagadores basados en NetScape
			else if(e.which) 
			{
				key = e.which;
				isCtrl = e.ctrlKey;
			}
		
			//Si no es un número se regresa true
			if (isNaN(key)) return true;
		
			//Si no lo es, se obtiene su equivalente en codigo ascii
			keychar = String.fromCharCode(key);
		
			//Si es backspace o tecla de control, solo
			//se deja que se ejecute
			if (key == 8 || isCtrl)
			{
				return true;
			}
		
			//Se crea expresion regular para digitos
			reg = /\d/;
		
			//Se verifica si son dos puntos y que no haya sido
			//tecleado con anterioridad
			var isFirstD = (keychar == ':' && obj.value.indexOf(':') == -1 );
		
			//Si cumple con la expresión regular de los digitos
			//se regresa true
			return isFirstD || reg.test(keychar);
		}

		function checktime(obj)
		{
			var hora=obj.value;

			if (hora=='') 
			{
				return;
			} 

			if (hora.length!=5) 
			{
				alert('<?=translate("Enter time in HH:MM format")?>');
				return;
			}

			//<=2 
			a=parseInt(hora.charAt(0));
			//<4 
			b=parseInt(hora.charAt(1));
			//:
			c=hora.charAt(2);
			//<=5
			d=parseInt(hora.charAt(3));
			
			if ((a==2 && b>3) || (a>2)) 
			{
				alert('<?=translate("The entered value in the hour does not correspond, enter a value between 00 and 23")?>');
				return;
			}
			
			if (d>5) 
			{
				alert('<?=translate("The entered value in the minutes does not correspond, enter a value between 00 and 59")?>');
				return;
			}
			
			if (c!=':') 
			{
				alert('<?=translate("Enter the \":\" character to divide the hour and the minutes")?>');
				return;
			}
		}
 		</script>
 <?
 	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
 	{	
?>
	 	<script language="JavaScript">
			//JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			objOkSelfButton = document.getElementById("BITAMeFormsUser_OkSelfButton");
 			objOkParentButton = document.getElementById("BITAMeFormsUser_OkParentButton");
 			
 			objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
 			objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
 			
<?
		if($this->isNewObject())
 		{
?>
			//JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
 			objOkNewButton = document.getElementById("BITAMeFormsUser_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
		
		if($_SESSION["PAchangeLanguage"]==true)
		{	
?>			
			//Refrescar el menú principal para q tome el nuevo idioma
			window.parent.frames['header'].location.reload();
<?
		}
?>
		</script> 		
<?
 	}
 	
 	static function getSecurityFilterByUserAndCubeID($aRepository, $anUserID, $aCubeID)
 	{
 		$arrayFilter = array();
 		//0 - Cadena de Filtro
 		//1 - Cadena de los consecutivos mostrados de acuerdo a como se encuentran en la cadena de filtro
 		//@JAPR 2008-09-19: Agregado un índice mas al arreglo de filtro devuelto, se supone que no debe afectar porque solo se pide
 		//actualmente el índice 0 y el 1
 		//2 - Indicación de donde obtuvo el filtro (usado para saber si se hace INSERT o UPDATE): 0 = No hay filtro, 1 = Directo, 2 = Roles
 		//3 - Valor del campo MINIMO
 		//4 - Valor del campo JERARQUIA
 		
 		$strFilter = "";
 		$strLevels = "";

		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
 		$arrayFilter = BITAMeFormsUser::getSecurityFilterByUserAndCubeIDFromUsers($aRepository, $anUserID, $aCubeID);
 		
 		$strFilter = $arrayFilter[0];

 		if($strFilter=="")
 		{
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
 			$arrayFilter = BITAMeFormsUser::getSecurityFilterByUserAndCubeIDFromRoles($aRepository, $anUserID, $aCubeID);
 		}
 		
 		return $arrayFilter;
 		
 	}
 	
 	static function getSecurityFilterByUserAndCubeIDFromUsers($aRepository, $anUserID, $aCubeID)
 	{
 		$arrayFilter = array();
 		//0 - Cadena de Filtro
 		//1 - Cadena de los consecutivos mostrados de acuerdo a como se encuentran en la cadena de filtro
 		//@JAPR 2008-09-19: Agregado un índice mas al arreglo de filtro devuelto, se supone que no debe afectar porque solo se pide
 		//actualmente el índice 0 y el 1
 		//2 - Indicación de donde obtuvo el filtro (usado para saber si se hace INSERT o UPDATE): 0 = No hay filtro, 1 = Directo, 2 = Roles
 		//3 - Valor del campo MINIMO
 		//4 - Valor del campo JERARQUIA
 		
 		$strFilter = "";
 		$strLevels = "";
 		
 		$arrayFilter[0] = $strFilter;
 		$arrayFilter[1] = $strLevels;
 		$arrayFilter[2] = 0;
		$arrayFilter[3] = '';
		$arrayFilter[4] = 0;
 		
		$theInfoUsrFilter=null;
		$theInfoUsrFilter =& BITAMGlobalInstance::GetUsrFilter($anUserID, $aCubeID);
		if(!is_null($theInfoUsrFilter))
		{
			return $theInfoUsrFilter;
		}
 		
 		$sql = "SELECT FILTRO, NIVELES, MINIMO, JERARQUIA FROM SI_USR_CONFIG WHERE CLA_USUARIO = ".$anUserID." AND CLA_CONCEPTO = ".$aCubeID;
 		
 		$aRS = $aRepository->ADOConnection->Execute($sql);
 		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USR_CONFIG ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$strFilter = $aRS->fields["filtro"];
			$strFilter = trim($strFilter);
			
			$strLevels = $aRS->fields["niveles"];
			$strLevels = trim($strLevels);
			
			$arrayFilter[0] = $strFilter;
			$arrayFilter[1] = $strLevels;
			$arrayFilter[2] = 1;	//Indica que el filtro está asignado en forma directa al usuario
			$arrayFilter[3] = (string) $aRS->fields["minimo"];
			$arrayFilter[4] = (int) $aRS->fields["jerarquia"];
		}
		
		BITAMGlobalInstance::AddUserFilter($anUserID, $aCubeID, $arrayFilter);

		return $arrayFilter;
 	}
 	
 	static function getSecurityFilterByUserAndCubeIDFromRoles($aRepository, $anUserID, $aCubeID)
 	{
 		
 		$arrayFilter = array();
 		//0 - Cadena de Filtro
 		//1 - Cadena de los consecutivos mostrados de acuerdo a como se encuentran en la cadena de filtro
 		//@JAPR 2008-09-19: Agregado un índice mas al arreglo de filtro devuelto, se supone que no debe afectar porque solo se pide
 		//actualmente el índice 0 y el 1
 		//2 - Indicación de donde obtuvo el filtro (usado para saber si se hace INSERT o UPDATE): 0 = No hay filtro, 1 = Directo, 2 = Roles
 		//3 - Valor del campo MINIMO
 		//4 - Valor del campo JERARQUIA
 		
 		$strFilter = "";
 		$strLevels = "";
 		
 		$arrayFilter[0] = $strFilter;
 		$arrayFilter[1] = $strLevels;
		$arrayFilter[2] = 0;
		$arrayFilter[3] = '';
		$arrayFilter[4] = 0;
 		
		$theInfoUsrFilter=null;
		$theInfoUsrFilter =& BITAMGlobalInstance::GetUsrGpsFilter($anUserID, $aCubeID);
		if(!is_null($theInfoUsrFilter))
		{
			return $theInfoUsrFilter;
		}
 		
 		$sql = "SELECT A.FILTRO, A.NIVELES, A.MINIMO, A.JERARQUIA FROM SI_ROL_CONFIG A, SI_ROL_USUARIO B 
 				WHERE A.CLA_ROL = B.CLA_ROL AND B.CLA_USUARIO = ".$anUserID." AND A.CLA_CONCEPTO = ".$aCubeID." ORDER BY A.PRIORIDAD";
 		
 		$aRS = $aRepository->ADOConnection->Execute($sql);
 		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_ROL_CONFIG ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$strFilter = $aRS->fields["filtro"];
			$strFilter = trim($strFilter);
			
			$strLevels = $aRS->fields["niveles"];
			$strLevels = trim($strLevels);
			
			$arrayFilter[0] = $strFilter;
			$arrayFilter[1] = $strLevels;
			$arrayFilter[2] = 2;	//Indica que el filtro está asignado mediante los roles a los que pertenece el usuario
			$arrayFilter[3] = (string) $aRS->fields["minimo"];
			$arrayFilter[4] = (int) $aRS->fields["jerarquia"];
		}
		
		BITAMGlobalInstance::AddUserGpsFilter($anUserID, $aCubeID, $arrayFilter);
		
		return $arrayFilter;
 		
 	}
 	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//Se verificó esta clase y aparentemente no se utiliza nunca como parte del framework para desplegarse, así que se removió el parámetro
	//$iSecurityLevel para permitir que sea compatible con la definición de object
 	function canRemove($aUser)	//, $iSecurityLevel = 0
	{
		$iSecurityLevel = 0;
		//@JAPR 2008-05-19: Modificado para que funcione como el Administrator Web
		//No se permite borrar usuarios de Artus desde Ektos ya que estos usuarios pueden estarse usando en otros productos
		//return false;
		/* ====================================================================================
			Garantizamos la integridad del repositorio
		   ==================================================================================== */			
		$RepositoryType = $_SESSION["PABITAM_RepositoryType"];
		if (is_null($RepositoryType))
		{
			$RepositoryType = 2;
		}
		
		//Si no es de desarrollo, no podemos crear/editar/borrar
		if ($RepositoryType > 1 && ($RepositoryType != 2 || (int) $_SESSION["PABITAM_ImplementationSecurity"] != 1))
		{
			return false;
		}
		
		if ($this->UserID == 0)
		{
			// No se puede borrar al supervisor
			return false;
		}
		
		//Si el usuario logeado no es el dueño y no es SUPERVISOR, no se puede continuar
		if ($aUser->UserID != $this->OwnerID && $aUser->UserID != 0)
		{
			return false;
		}
		
		//Verificamos si se tiene que comprobar la integridad de mas productos
		if ($iSecurityLevel > 0)
		{
			$sql = "SELECT COUNT(*) AS CountStages FROM SI_ESCENARIO WHERE CLA_USUARIO = ".$this->UserID;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_ESCENARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$ncount = $aRS->fields["countstages"];
			if ($ncount > 0)
			{
				return false;
			}
			
			$sql = "SELECT COUNT(*) AS CountCubes FROM SI_CONCEPTO WHERE CLA_OWNER = ".$this->UserID;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_CONCEPTO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$ncount = $aRS->fields["countcubes"];
			if ($ncount > 0)
			{
				return false;
			}
		}
		return true;
		//@JAPR

		/*
		if($this->UserID==$_SESSION["PAtheUserID"])
		{
			return false;
		}
		else 
		{
			return true;
		}
		*/
	}
	
	function canEdit($aUser)
	{
		/* ====================================================================================
			Garantizamos la integridad del repositorio
		   ==================================================================================== */			
		$RepositoryType = $_SESSION["PABITAM_RepositoryType"];
		if (is_null($RepositoryType))
		{
			$RepositoryType = 2;
		}
		
		//Si no es de desarrollo, no podemos crear/editar/borrar
		if ($RepositoryType > 1 && ($RepositoryType != 2 || (int) $_SESSION["PABITAM_ImplementationSecurity"] != 1))
		{
			return false;
		}
		
		//Si el usuario logeado no es el dueño y no es SUPERVISOR, no se puede continuar
		if ($aUser->UserID != $this->OwnerID && $aUser->UserID != 0)
		{
			return false;
		}		
		
		return true;
	}
	
	function canAdd($aUser)
	{
		/* ====================================================================================
			Garantizamos la integridad del repositorio
		   ==================================================================================== */			
		$RepositoryType = $_SESSION["PABITAM_RepositoryType"];
		if (is_null($RepositoryType))
		{
			$RepositoryType = 2;
		}
		
		//Si no es de desarrollo, no podemos crear/editar/borrar
		if ($RepositoryType > 1 && ($RepositoryType != 2 || (int) $_SESSION["PABITAM_ImplementationSecurity"] != 1))
		{
			return false;
		}
		
		return true;
	}	
	
	static function  GetEktAdmins($aRepository)
	{
		$sql="SELECT CLA_USUARIO AS UserID, NOM_LARGO AS FullName, NOM_CORTO AS UserName FROM SI_USUARIO WHERE EKTOS_ADMIN >= 1";
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$array=array();
		
		$array[-1]=translate('None');
		
		while (!$aRS->EOF)
		{
			$array[(int) $aRS->fields['userid']]=$aRS->fields['fullname'].' ('.$aRS->fields['username'].')';
			$aRS->MoveNext();
		}
		return $array;
	}
	
	static function GetUsers($aRepository, $includeNone=true)
	{
		$users = array();
		
		$whereNotIn = "";
		
		if($_SESSION["PAFBM_Mode"]==1 && $_SESSION["PAtheUserID"]!=0)
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$whereNotIn = " WHERE CLA_USUARIO > 0 ";
			//@JAPR
		}

		$sql="SELECT CLA_USUARIO AS UserID, NOM_CORTO AS UserName, NOM_LARGO AS FullName FROM SI_USUARIO ".$whereNotIn." ORDER BY FullName";

		$aRS = $aRepository->ADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if($includeNone)
		{
			$users[-99999] = translate("None");
		}
		
		while (!$aRS->EOF)
		{
			$userID=(int)$aRS->fields["userid"];
			$name=$aRS->fields["fullname"];
			if(trim($name)=="")
			{
				$name=$aRS->fields["username"];
			}
			$users[$userID]=$name;
			$aRS->MoveNext();
		}
		
		return $users;
	}
	
	public function loadRolesFromDB($aConnection)
	{
		$resultRoles = array();
		
		$aSQL = "select A.CLA_ROL, A.NOM_ROL, "."\n".
				"B.CLA_USUARIO, B.NOM_LARGO, B.NOM_CORTO, B.CUENTA_CORREO, B.PASSWORD"."\n".
				"from SI_ROL A, SI_USUARIO B, SI_ROL_USUARIO C"."\n".
				"where A.CLA_ROL = C.CLA_ROL and C.CLA_USUARIO = B.CLA_USUARIO and B.CLA_USUARIO in ({$this->cla_usuario})";
		$aSQL .= "\n"." order by A.CLA_ROL, B.CLA_USUARIO";

		$recordSet = $aConnection->Execute($aSQL);

		while (!$recordSet->EOF)
		{
			$resultRoles[$recordSet->fields[0]] = array("cla_rol"=>$recordSet->fields[0],"nom_rol"=>$recordSet->fields[1]);
			$recordSet->MoveNext();
		}

		$recordSet->close();
		
		return($resultRoles);
	}
}

//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
class BITAMeFormsUserCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}

	static function NewInstance($aRepository, $anArrayOfUserIDs = null)
	{
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$anInstance = new BITAMeFormsUserCollection($aRepository);
		$where = "";
		if (!is_null($anArrayOfUserIDs))
		{
			switch (count($anArrayOfUserIDs))
			{
				case 0:
					break;
				case 1:
					$where = "AND A.CLA_USUARIO = ".((int) $anArrayOfUserIDs[0]);
					break;
				default:
					foreach ($anArrayOfUserIDs as $aUserID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $aUserID; 
					}
					if ($where != "")
					{
						$where = "AND A.CLA_USUARIO IN (".$where.")";
					}
					break;
			}
		}
		
		
		$whereNotIn = "";
		
		if($_SESSION["PAFBM_Mode"]==1 && $_SESSION["PAtheUserID"]!=0)
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$whereNotIn = " AND A.CLA_USUARIO > 0 ";
			//@JAPR
		}

		$sql = "SELECT A.CLA_USUARIO AS UserID ".
				", A.NOM_CORTO AS UserName ".
				", A.PASSWORD AS Password ".
				", A.NOM_LARGO AS FullName ".
				", A.PUESTO AS Position ".
				", A.CUENTA_CORREO AS EMail ".
				", A.WHTMLANG AS UserLanguageID ".
				", A.CONFIG_SECURITY AS ConfigSecurity ".
				", A.CLA_OWNER AS OwnerID ".
				", A.LDAP_PATH AS LdapPath ".
				", B.NOM_CORTO AS OwnerName ".
			"FROM SI_USUARIO A, SI_USUARIO B ".
			"WHERE A.CLA_OWNER = B.CLA_USUARIO ".$where." ".$whereNotIn." ORDER BY A.NOM_CORTO";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		while (!$aRS->EOF)
		{
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$anInstance->Collection[] = BITAMeFormsUser::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		return BITAMeFormsUserCollection::NewInstance($aRepository);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{
		return translate("Users");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=UserCollection";
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_SECTION=User";
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=User";
	}
	
	function get_PathArray()
	{
		//$parent = $this->get_Parent();
		//$pathArray = $parent->get_PathArray();
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'UserID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");

		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserName";
		$aField->Title = translate("User Name");
		$aField->Type = "String";
		$aField->Size = 15;
		$myFields[] = $aField;


		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FullName";
		$aField->Title = translate("Full Name");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "EMail";
		$aField->Title = translate("E-Mail");
		$aField->Type = "String";
		$aField->Size = 250;
		$myFields[] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Position";
		$aField->Title = translate("Job Position");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "OwnerName";
		$aField->Title = translate("Owner");
		$aField->Type = "String";
		$aField->Size = 15;
		$aField->IsDisplayOnly = true;
		$myFields[] = $aField;
		
		/*
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IsArtusAdministrator";
		$aField->Title = "Is Artus Administrator";
		$aField->Type = "Boolean";
		$myFields[] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IsArtusDesigner";
		$aField->Title = "Is Artus Designer";
		$aField->Type = "Boolean";
		$myFields[] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IsArtusUser";
		$aField->Title = "Is Artus User";
		$aField->Type = "Boolean";
		$myFields[] = $aField;
*/
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
 	function canRemove($aUser)
	{
		//No se permite borrar usuarios de Artus desde Ektos ya que estos usuarios pueden estarse usando en otros productos
		return false;
	}
	
	function canAdd($aUser)
	{
		/* ====================================================================================
			Garantizamos la integridad del repositorio
		   ==================================================================================== */			
		$RepositoryType = $_SESSION["PABITAM_RepositoryType"];
		if (is_null($RepositoryType))
		{
			$RepositoryType = 2;
		}
		
		//Si no es de desarrollo, no podemos crear/editar/borrar
		if ($RepositoryType > 1 && ($RepositoryType != 2 || (int) $_SESSION["PABITAM_ImplementationSecurity"] != 1))
		{
			return false;
		}
		
		return true;
	}	
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
		
		if($_SESSION["PAchangeLanguage"]==true)
		{	
			$_SESSION["PAvMenu"] = 1;
?>			<script language="JavaScript">
			//Refrescar el menú principal para q tome el nuevo idioma		
			window.parent.frames['header'].location.reload();
			</script>
<?
		}
	}
}
?>