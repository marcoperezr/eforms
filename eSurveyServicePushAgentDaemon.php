<?php
session_start();

//@JAPR 2015-10-23: Agregado esta línea por instrucción de LRoux para intentar prevenir que llamadas al agente desde la tarea de windows se pudieran quedar en el limbo por problemas
//de timeout, esto a raiz de un caso que coincidió con la desconexión de KPI7 cuando el agente de KPI4 intentaba conectarse para obtener tareas, pero pese a que KPI7 ya estaba online
//el agente de nunca salió del intento de conexión y ninguna otra tarea fue procesada por él
set_time_limit(0);

require_once('config.php');
require_once('cubeClasses.php');
require_once('eFormsAgent.inc.php');
require_once('eFormsAgentConfig.inc.php');
require_once('pushMsg.inc.php');

//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Variable para indicar que el proceso se está ejecutando desde un agente de eForms en general, sea cual sea dicho agente
global $gbIsAneFormsAgent;
$gbIsAneFormsAgent = true;
//Variable que indica si el proceso se está o no ejecutando desde un agente de eForms, originalmente utilizado por el agente de Push
global $gbIsPushAgent;
$gbIsPushAgent = true;
global $gblShowErrorSurvey;
$gblShowErrorSurvey = false;
global $intRequestNumber;
$intRequestNumber = 0;

//@JAPR 2018-02-23: Agregado el parámetro para condicionar al agente a procesar un único repositorio (#LP9ZSN)
$strAgentRepositoryName = '';
//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Punto de entrada de un agente, deberá asignar el AgentName así como enviarlo a la función GeteFormsLogPath para que se registre en las llamadas subsecuentes
global $strAgentName;
//@JAPR
$strAgentName = '';
if (isset($argv) && is_array($argv) && count($argv) > 2) {
	//Configuraciones por línea de comando
	foreach ($argv as $intParamNum => $strParamVal) {
		switch ($intParamNum) {
			case 0:		//Script name
				//Es irrelevante, es el nombre del archivo php del Agente
				break;
			case 1:		//Repository
				//No se procesa en este punto, así que será ignorado
				$strAgentRepositoryName = (string) $strParamVal;
				break;
			case 2:
				$strAgentName = (string) $strParamVal;
				break;
		}
	}
}
else {
	$strAgentRepositoryName = getParamValue('Repository', 'both', '(string)');
	$strAgentName = getParamValue('AgentName', 'both', '(string)');
}
$strAgentRepositoryName = strtoupper((string) $strAgentRepositoryName);

//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Integradas las funciones comunes para almacenamiento de log de eForms
$strLogPath = GeteFormsLogPath($strAgentRepositoryName, $strAgentName);
//@JAPR

$strAgentParams = "Agent parameters: Repository=".$strAgentRepositoryName."; strAgentName=".$strAgentName;
if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	ECHOString($strAgentParams, 1, 1, "color:blue;");
}

//@JAPR 2015-10-14: Corregido un bug, el identificador único sólo contenía el día y la hora incluyendo segundos, pero en un caso dos agentes si lograron entrar al mismos segundo así que
//se llegó a duplicar. En circunstancias normales eso no habría importado porque generalmente nunca dos agentes apuntarán al mismo repositorio, sin embargo hubo un error (ya corregido)
//donde el repositorio no se tomaba en cuenta para un SELECT, sino sólo el AgentID, así que se confundieron y dos agentes procesaron la misma tarea, con este uniqid + un prefijo enviado
//como parámetro en la URL de la tarea de Windows, se evitará repetir nuevamente este caso incluso si se deciden activar múltiples agentes para el mismo repositorio (en ese caso, el
//agentName sería obligatorio para asegurar que nunca se repetirá el ID, en cualqueir otro caso es opcional) (#NQ995V)
//$strAgentName = getParamValue('agentName', 'both', '(string)');
$strSessionID = "eFormsPushAgent_".date("YmdHis")."_".uniqid($strAgentName);

//Activar si se desea que los envíos de mensajes utilicen el modo de depuración, el cual arrojará muchos mas datos de las salidas del proceso
//de notificaciones enviando el parámetro DebugBreak 
$blnDebugAgentMode = false;
//Nombre default del archivo de log de cada sesión del agente (es diferente en cada ejecución) y usado para todas las salidas a menos que en
//cierto proceso explícitamente se genere un archivo distinto (como en el resultado de un envío de notificación push)
//@JAPR 2013-06-28: Modificado para que el log de procesamiento sea diario, mientras que el log de detalle de error si incluya el TimeStampt
//@JAPR 2016-10-20: Modificado el nombre de archivo de proceso del Agente a doble "__" para que pueda ser eliminado fácilmente cada cierto tiempo por un proceso de limpieza de KPIOnline
$strDefaultLogStringFile = "eFormsPushAgent__{$strAgentName}".date("Ymd").'.log';
$strDefaultDetailLogStringFile = $strSessionID.'.log';

//@JAPR 2018-02-27: Agregado el parámetro para condicionar al agente a procesar un único repositorio (#LP9ZSN)
//Sobreescribe las variables que identifican a la sesión y al archivo a utilizar en caso de tratarse de un agente personalizado
if ( $strAgentRepositoryName ) {
	$strDefaultLogStringFile = "eFormsPushAgent_{$strAgentRepositoryName}_{$strAgentName}".date("Ymd").'.log';
}
//@JAPR

//Activar si se quiere registrar el archivo de Log cada vez que entra en ejecución el Agente de eForms, de lo contrario desactivada sólo generará
//dicho archivo en caso de error
$blnSaveLogStringFile = true;

if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	ECHOString("eSurveyServicePushAgentDaemon.php", 1, 1, "color:purple;");
}

logString(str_repeat('*', 80));
logString('Starting eForms Push agent');
if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	echo("<br>\r\n Push agent session ID: {$strSessionID}<br>\r\n");
}

//@JAPR 2018-02-27: Agregado el parámetro para condicionar al agente a procesar un único repositorio (#LP9ZSN)
logString($strAgentParams);
//@JAPR

clearstatcache();

//Fecha y hora de ingreso de la tarea del Agente
$dteAgentStartDate = new DateTime(date("Y-m-d H:i:s"));
//$dteAgentStartDate = new DateTime("2016-10-17 19:40:30");

//@JAPR 2019-10-16: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Corregido un bug, no se estaba incluyendo esta clase antes de usarla para obtener el nombre de archivo, por lo que fallaba la ejecución del agente
@require_once("bpaemailconfiguration.inc.php");
//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Válvula de seguridad para abortar el agente en caso de que exista este archivo
$strStopFileName = $strLogPath.BPAEmailConfiguration::stopPushFile;

//Este agente se encuentra en un ciclo infinito, ejecutándose hasta que se encuentra el archivo de seguridad en la ruta indicada, o bien alguien mate el proceso que está corriendo
//Si se ejecuta en modo de Debug entonces se mata inmediatamente al terminar la primer iteración, lo cual simplemente sirve para permitir que se procesen mensajes pendientes a manera
//de prueba
while (true) {
	clearstatcache();
	//@JAPR 2016-11-15: Agregada validación para que el archivo que detiene el proceso no funcione si es modo depuración, de esa manera se puede detener el agente real para permitir
	//que con la URL de depuración se puedan revisar casos de error
	if (file_exists($strStopFileName) && !getParamValue('DebugBreak', 'both', '(int)', true)) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n Push agent process was aborted by user action");
		}
		logString("Push agent process was aborted by user action");
		BITAMPushMsg::CloseAllAPNSConnections();
		die();
	}
	
	//Lee la configuración que utilizará este agente para procesar las tareas
	$aConfiguration = @BPAEmailConfiguration::readConfiguration();
	if (is_null($aConfiguration)) {
		logString('Unable to read eForms configuration file');
		exit;
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n eForms Push agent configuration:");
		PrintMultiArray($aConfiguration);
	}

	$SMTPServer = (string) @$aConfiguration->fbm_email_server;	
	$SMTPPort = (string) @$aConfiguration->fbm_email_port;
	$SMTPUserName = (string) @$aConfiguration->fbm_email_username;
	$SMTPPassword = (string) @$aConfiguration->fbm_email_pwd;
	$SMTPEmailSource = (string) @$aConfiguration->fbm_email_source;
	$anInterval = $aConfiguration->retry_time;
	$maxProcess = 1;
	$processRunning = 0;

	$activeAgents = array();
	//@JAPR 2016-10-21: Agregadas configuraciones para depuración del Agente
	if ($aConfiguration->push_agent_ignore_tasks && !getParamValue('DebugBreak', 'both', '(int)', true)) {
		logString("Agent entered debug mode, skipping push notifications process. Deactivate 'push_agent_ignore_tasks' configuration to resume processing push notifications");
	}
	else {
		$activeAgents = getFBMAgents($aConfiguration);
	}
	//@JAPR
	$intMaxAgents = count($activeAgents);
	
	//Si hay algunas tareas por ejecutar, recorre uno a uno los repositorios que tenían tareas pendientes y las procesa
	$currentAgentIndex = 0;
	try {
		$aBITAMConnection = null;
		//Recorre una a una las bases de datos identificadas
		while ($currentAgentIndex < $intMaxAgents) {
			//Recorre mientras aun existan procesos libres que puedan tomar nuevas tareas
			do {
				logString("Number of process running: ". $processRunning .", Max number of process allowed: ". $maxProcess);
				
				// if we can run a process
				if ($processRunning < $maxProcess)
				{
					// get the agent
					$eachAgent = @$activeAgents[$currentAgentIndex];
					
					// execute the task if required
					if ($eachAgent) {
						logString("Check repository '{$eachAgent->repository}', server '{$eachAgent->server}'");
						//Busca tareas hasta que se confirme que no hay una sola libre en este repositorio
						
						//En este agente sólo debe existir un único registro en la tabla de tareas, así que no es necesario obtener un "TaskID", mas bien sólo se debe verificar si ya se
						//encontraba proceando esta tarea/repositorio verificando el campo AgentID, para lo cual realiza un UPDATE para apartarlo y así saber si debe o no procesarlo, en caso
						//que un Agente diferente (otra tarea de Windows) que está corriendo en paralelo fuera quien intentara procesar el repositorio
						
						//Primero realiza la conexión al Agente (tabla FBM000) configurado
						ob_start();
						try {
							//Cierra la conexión anterior si era de un server diferente
							if (!is_null($aBITAMConnection) && $aBITAMConnection->ADODBServer != $eachAgent->server) {
								@$aBITAMConnection->close();
								$aBITAMConnection = null;
							}
							
							if (is_null($aBITAMConnection)) {
								//@JAPR 2013-11-06: Modificado para reutilizar los archivos del Model Manager, los cuales no requieren el password desencriptado
								//Este password ya llega desencriptado en este punto
								//Ahora este password llega encriptado, pero así es como lo requiere BITAMConnection cuando se usa desde el
								//Model Manager
								$pass = $eachAgent->password;
								//@JAPR
								$aBITAMConnection = BITAMConnection::NewInstance($eachAgent->server, $eachAgent->username, $pass, "fbm000", 'mysql');
								if (is_null($aBITAMConnection) || is_null($aBITAMConnection->ADOConnection)) {
									$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
									ob_end_clean();
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("Unable to connect to '{$aConfiguration->fbm_db_server}': {$strErrorMsg}", 1, 1, "color:red;");
									}
									logString("Unable to connect to '{$aConfiguration->fbm_db_server}': {$strErrorMsg}", '');
									continue;
								}
							}
						} catch (Exception $e){
							ob_end_clean();
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("Unable to connect to '{$aConfiguration->fbm_db_server}'", 1, 1, "color:red;");
							}
							logString("Unable to connect to '{$aConfiguration->fbm_db_server}'", '');
							$currentAgentIndex++;
							//Brinca al siguiente agente de la lista
							continue;
						}
						ob_end_clean();
						
						//Verifica si el repositorio especificado ya ha sido tomado por otro agente, si es así simplemente lo descarta y continua con el siguiente repositorio
						$intPushTaskID = getNextTaskKeyToProcess($aBITAMConnection, $eachAgent);
						
						//Si hay una tarea, invoca al proceso que la ejecuta
						if ($intPushTaskID > 0) {
							logString("Processing push task {$intPushTaskID}");
							try {
								$blnStatus = executePushTask($aBITAMConnection, $intPushTaskID);
							} catch (Exception $e) {
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("executePushTask exception: ".$e->getMessage(), 1, 1, "color:red;");
								}
								
								logString($e->getMessage(), '', true, true);
							}
						}
						else {
							logString("There were no pending push tasks found");
						}
					}
					
					// wait a second, to avoid log file overwritting
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						//Si está en modo de depuración no es necesario que el proceso se quede en stand by
					}
					else {
						sleep(2);
					}
					
					// go to the next agent on the list
					$currentAgentIndex++;
				}
			} while ($processRunning < $maxProcess && $currentAgentIndex < count($activeAgents));
			
			//Espera el tiempo configurado antes de verificar por nuevas tareas que se puedan lanzar en otro proceso
			//El Agente de Push permanece corriendo todo el tiempo, así que no es necesario realizar esta acción, si se desea implementar como el Agente de eForms, entonces copiar el codigo de
			//nuevo tal como se encuentra en dicho agente
			//En este caso no hay necesidad de esperar este tiempo porque no pudo haber otro proceso de agente ejecutándose al mismo tiempo ya que teóricamente este agente es único, pero
			//aunque si estuviera otro agente corriendo que hubiera tomado los mismos repositorios, la validación del ciclo externo es la misma que el interno así que se va a salir
			//inmediatamente y este tiempo muerto habría sido innecesario, sólo se ocupaba si hubiera llegado a este punto porque ya estaban todos los procesos tomados, que como ya se indicó
			//no es posible con este agente dada la arquitectura diferente al agente de tareas de capturas con la cual se diseñó
			/*$checkTime = time();
			$currentTime = time();
			while (($currentTime - $checkTime) < $anInterval) {
				sleep(2);
				clearstatcache();
				
				$currentTime = time();
			}*/
		}
	} catch (Exception $e) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("eForms Push agent unhandled exception: ".$e->getMessage(), 1, 1, "color:red;");
		}
		
		logString("eForms Push agent unhandled exception: ".$e->getMessage(), '', true, true);
	}
	
	logString("Finishing eForms Push agent");
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		//En este caso como se encuentra en modo de depuración, automáticamente terminará el proceso del agente después de la primer iteración, en caso contrario se debe
		//ir a dormir la cantidad de segundos configurados
		BITAMPushMsg::CloseAllAPNSConnections();
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n Push agent process was aborted because it was a debugging session");
		}
		logString("Push agent process was aborted because it was a debugging session");
		die();
	}
	
	//Determina el tiempo que debe estar inactivo el servicio, si no se especifica o es un valor fuera del rango permitido por código, se resetea al valor default de 20 segundos
	$intSecondsToSleep = (int) @$aConfiguration->push_agent_sleep;
	if ($intSecondsToSleep <= 0 || $intSecondsToSleep > 300) {
		$intSecondsToSleep = 20;
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		//Si está en modo de depuración no es necesario que el proceso se quede en stand by
	}
	else {
		sleep($intSecondsToSleep);
	}
	
	//Verifica si el agente debe seguir ejecutándose según el tiempo transcurrido hasta este momento
	$dteAgentCurrentDate = new DateTime(date("Y-m-d H:i:s"));
	//$dteAgentCurrentDate = new DateTime("2016-10-17 19:50:20");
	$interval = $dteAgentCurrentDate->diff($dteAgentStartDate);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n dteAgentStartDate == ".$dteAgentStartDate->format("Y-m-d H:i:s"));
		echo("<br>\r\n dteAgentCurrentDate == ".$dteAgentCurrentDate->format("Y-m-d H:i:s"));
		echo("<br>\r\n Días transcurridos == ".$interval->format('%d días'));
		echo("<br>\r\n Horas transcurridos == ".$interval->format('%h horas'));
		echo("<br>\r\n Minutos transcurridos == ".$interval->format('%i minutos'));
		echo("<br>\r\n Segundos transcurridos == ".$interval->format('%s segundos'));
	}

	$intDaysDiff = (int) $interval->format('%d');
	if ($intDaysDiff > 0) {
		//Fin forzado de la tarea, no debe exceder a un día ejecutándose
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n Push agent process was aborted, execution time exceed 1 day");
		}
		logString("Push agent process was aborted, execution time exceed 1 day");
		BITAMPushMsg::CloseAllAPNSConnections();
		die();
	}
	$intHoursDiff = (int) $interval->format('%h');
	if ($intHoursDiff > 12) {
		//Fin forzado de la tarea, no debe exceder a medio día ejecutándose
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n Push agent process was aborted, execution time exceed 12 hours");
		}
		logString("Push agent process was aborted, execution time exceed 12 hours");
		BITAMPushMsg::CloseAllAPNSConnections();
		die();
	}
	$intMinutesDiff = (int) $interval->format('%i');
	$intMinutesDiff += $intHoursDiff * 60;
	$intMaxMinutesRunning = (int) @$aConfiguration->push_agent_running_time;	
	if ($intMaxMinutesRunning <= 0) {
		$intMaxMinutesRunning = 60;
	}
	if ($intMinutesDiff > $intMaxMinutesRunning) {
		//Fin forzado de la tarea, no debe exceder al tiempo de ejecución configurado
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n Push agent process was aborted, maximum execution time reached");
		}
		logString("Push agent process was aborted, maximum execution time reached");
		BITAMPushMsg::CloseAllAPNSConnections();
		die();
	}
}

/* Obtiene la lista de bases de datos que contienen por lo menos alguna tarea pendiente por ejecutar, incluyendo aquellas de servidores diferentes
al default que este agente procesa
//@JAPRChecked
*/
function getFBMAgents($aConfiguration)
{
	global $arrDBsEnabledForAgent;
	global $arrDBsExcludedFromAgent;
	global $BITAMRepositories;
	global $argv;
	//@JAPR 2016-10-26: Adaptado el agente de eForms para que se pueda forzar al procesamiento de tareas de repositorios fijos pre-generando una tarea después de haber confirmado que
	//existe por lo menos un mensaje sin procesar en el repositorio directamente, pero sólo para los que se encuentran en el array de repositorios especiapes para dicho trato
	//Originalmente utilizado para Petrotech, donde MLopez vendió la idea de llenar desde una forma de eBavel la tabla de mensajes Push de eForms, pero no había manera de que se generara
	//la tarea de este agente, así que había que forzar la revisión directa de la tabla de mensajes de eForms cada vez que el agente entraba a su proceso
	//@JAPR
	global $arrPushAgentDBsWithAutoTasks;
	
	//@JAPR 2016-01-14: Modificado para que ahora el Agente sólo procese aquellas tareas que pertenecen exclusivamente a repositorios que están configurados en este
	//servicio donde está corriendo el archivo php del Agente, independientemente de si dicho repositorio existe en el array $arrDBsEnabledForAgent, dicho array sólo
	//servirá a partir de ahora para limitar las BDs que sean las únicas que se deban procesar con el Agente, lo cual en v6 es completamente irrelevante ya que todo
	//repositorio debería ser procesado precisamente por el agente, así que con este cambio se podrá dejar $arrDBsEnabledForAgent completamente vacío, sólo llenar
	//$arrDBsExcludedFromAgent con repositorios que por alguna extraña razón se quisieran poner en Stand By (es decir, no procesar sus capturas) y simplemente va a
	//procesar todos los repositorios que se encuentren en el archivo .ini (#WUH8DI)
	//Para facilitar las cosas, simplemente se sobreescribirá internamente el array $arrDBsEnabledForAgent con los repositorios del archivo EKT_cfg.ini, de tal manera
	//que lo configurado manualmente en el archivo eFormsAgentConfig.inc.php será completamente irrelevante, así todos los repositorios del archivo .ini se considerarán
	//válidos para procesar, excepto si están configurados en el array $arrDBsExcludedFromAgent
	logString("Loading Push agents list from EKT_cfg.ini file");
	//Primero resetea cualquier posible configuración que hubiera estado indicada en el archivo eFormsAgentConfig.inc.php, ya que son obsoletas
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		if (isset($arrDBsEnabledForAgent)) {
			echo("<br>\r\n arrDBsEnabledForAgent original setting:");
			PrintMultiArray($arrDBsEnabledForAgent);
		}
	}
	
	$arrDBsEnabledForAgent = array();
	
	//Se debe invocar de nuevo a la lectura del archivo .ini de repositorios, ya que este agente se encuentra corriendo por tiempo indefinido, de esta manera se podrán agregar nuevos
	//repositorios sin necesidad de eliminar la tarea del agente que se encuentra corriendo
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n Loading repositories from ini file");
	}
	$BITAMRepositories = BITAMEKTRepository::loadBITAMRepositories();
	
	//En caso de haberse leído por lo menos un repositorio del archivo .ini, los agrega como repositoriso válidos tal como hubieran estado en el archivo eFormsAgentConfig.inc.php
	if (isset($BITAMRepositories) && is_array($BITAMRepositories)) {
		//@JAPR 2018-02-23: Agregado el parámetro para condicionar al agente a procesar un único repositorio (#LP9ZSN)
		$strAgentRepositoryName = '';
		//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		//La asignación de esta variable en este punto ya no tiene consecuencias, ya que no es una referencia a la variable global y ni siquiera se utiliza, probablemente solo se
		//dejó por haber extraído el mismo código de la identificación de parámetros original de este servicio, pero en todo caso habría sido mejor heredar como global lo que se necesitara
		$strAgentName = '';
		//@JAPR
		logString('Arguments #: '.count($argv));
		if (isset($argv) && is_array($argv) && count($argv) >= 1) {
			logString('Arguments read');
			//Configuraciones por línea de comando
			foreach ($argv as $intParamNum => $strParamVal) {
				switch ($intParamNum) {
					case 0:		//Script name
						//Es irrelevante, es el nombre del archivo php del Agente
						break;
					case 1:		//Repository
						//No se procesa en este punto, así que será ignorado
						$strAgentRepositoryName = (string) $strParamVal;
						break;
					case 2:
						$strAgentName = (string) $strParamVal;
						break;
				}
			}
		}
		else {
			logString('Checking Get params');
			$strAgentRepositoryName = getParamValue('Repository', 'both', '(string)');
			$strAgentName = getParamValue('AgentName', 'both', '(string)');
		}
		$strAgentRepositoryName = strtoupper((string) $strAgentRepositoryName);
		
		if ( $strAgentRepositoryName ) {
			logString('Starting eForms agent in single repository mode');
			
			//En este caso se debe omitir del array de exclusiones a este repositorio dado a que se está forzando su ejecución directa, esto es así para permitir que algunos repositorios
			//tengan un agente particular configurandolos en el array de exclusiones de eFormsAgentConfig.inc.php y forzando a un agente mediante este parámetro nuevo, mientras que el resto 
			//de los repositorios que no están excluídos serían procesados por el agente default
			if (isset($arrDBsExcludedFromAgent) && is_array($arrDBsExcludedFromAgent) && isset($arrDBsExcludedFromAgent[$strAgentRepositoryName])) {
				unset($arrDBsExcludedFromAgent[$strAgentRepositoryName]);
			}
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			PrintMultiArray($BITAMRepositories);
		}
		//@JAPR
		foreach ($BITAMRepositories as $aRepositoryName => $aRepository) {
			//@JAPR 2018-02-23: Agregado el parámetro para condicionar al agente a procesar un único repositorio (#LP9ZSN)
			//Ignora todos los repositorios leídos de la configuración que no coincidan con el especificado en el parámetro para que no sean procesados, al concluir
			//este ciclo $strAgentRepositoryName tendría exclusivamente el repositorio proporcionado como parámetro si es que hubo alguno, de lo contrario traería
			//el último repositorio de la lista, así que la variable no es confiable excepto en este ciclo
			if ( $strAgentRepositoryName && $strAgentRepositoryName != strtoupper((string) $aRepositoryName) ) {
				continue;
			}
			//@JAPR
			
			//@JAPR 2018-03-01: Corregido un bug, con la intruducción del parámetro Repository, se sobreescribió la variable $strAgentRepositoryName y ahora sólo el
			//primer repositorio leído del .ini era el que estaba siendo procesado (#LP9ZSN)
			$strAgentRepositoryNameTmp = strtoupper((string) $aRepositoryName);
			$arrDBsEnabledForAgent[$strAgentRepositoryNameTmp] = 1;
			//@JAPR
		}
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		if (isset($arrDBsEnabledForAgent)) {
			echo("<br>\r\n arrDBsEnabledForAgent from ini file:");
			PrintMultiArray($arrDBsEnabledForAgent);
		}
	}
	//@JAPR
	
	logString("Retrieving configured eForms Push agents list");
	$activeAgents = array();
	
	ob_start();
	try {
		//@JAPR 2013-11-06: Modificado para reutilizar los archivos del Model Manager, los cuales no requieren el password desencriptado
		//@JAPR 2015-01-30: Es indispensable que los archivos cconnection.inc.php y ccreatequery.inc.php NO se encuentren en la carpeta del
		//servicio de eForms si va a estar ejecutandose el agente, de lo contrario dado al cambio mencionado arriba, fallará la conexión pues
		//dichos archivos si requerían el password desencriptado, al eliminarlos se forzará a reutilizar los de Model Manager y funcionará bien
		//$pass = BITAMDecryptPassword($aConfiguration->fbm_db_pwd);
		$pass = $aConfiguration->fbm_db_pwd;
		//@JAPR
		$aBITAMConnection = BITAMConnection::NewInstance($aConfiguration->fbm_db_server, $aConfiguration->fbm_db_user, $pass, "fbm000", 'mysql');
		if (is_null($aBITAMConnection) || is_null($aBITAMConnection->ADOConnection)) {
			$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
			ob_end_clean();
			logString("Unable to connect to '{$aConfiguration->fbm_db_server}': {$strErrorMsg}", '', true, true);
			return false;
		}
	} catch (Exception $e){
		ob_end_clean();
		logString("Unable to connect to '{$aConfiguration->fbm_db_server}'", '', true, true);
		return false;
	}
	ob_end_clean();
	
	//Tabla que contendrá una notificación de Push pendiente para que el agente la tome en la siguiente oportunidad, la PRIMARY KEY es el auto-incremental, el cual se define para que
	//en caso de agregar nuevos Push mientras el agente ya estaba procesando ese repositorio, el repositorio se inserte al final de la lista para que el agente pueda procesar primero
	//otros repositorios que notificaron antes push pendientes. La llave única sin embargo es el propio repositorio, ya que sólo debe existir una petición por repositorio y el Agente
	//procesará todos los push pendientes del mismo eliminando dicha llave al terminar el proceso
	$sqlCREATE = "CREATE TABLE SI_SV_PushTasks (
			PushTaskID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
			Repository Varchar(50) NOT NULL,
			CreationDateID DATETIME NOT NULL,
			FormsPath VARCHAR(255) NULL,
			TaskStatus INTEGER NULL,
			TaskStartDate DATETIME NULL,
			AgentID VARCHAR(50) NULL,
			PRIMARY KEY (PushTaskID),
			UNIQUE KEY (Repository)
		) AUTO_INCREMENT=1";
	
	//Tabla que contiene el registro del status de procesamiento de un repositorio específico tomado por el agente debido a un Push enviado, aquí se registrará la hora de inicio así
	//como los IDs de los mensajes (PushMsgIDs) separados por "|" para poder buscar alguno específico mediante un LIKE, y registrará la referencia al archivo .log de procesamiento en
	//caso de haber ocurrido algún tipo de error en el proceso
	//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
	$sqlCREATELog = "CREATE TABLE SI_SV_PushTasksLog (
			PushTaskLogID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
			PushTaskID BIGINT UNSIGNED NOT NULL,
			Repository Varchar(50) NOT NULL,
			CreationDateID DATETIME NOT NULL,
			FormsPath VARCHAR(255) NULL,
			TaskStatus INTEGER NULL,
			TaskStartDate DATETIME NULL,
			AgentID VARCHAR(50) NULL,
			LogFileName VARCHAR(255) NULL,
			PushMsgIDs LONGTEXT NULL,
			PRIMARY KEY (PushTaskLogID)
		) AUTO_INCREMENT=1";
	
	//@JAPR 2016-10-26: Adaptado el agente de eForms para que se pueda forzar al procesamiento de tareas de repositorios fijos pre-generando una tarea después de haber confirmado que
	//existe por lo menos un mensaje sin procesar en el repositorio directamente, pero sólo para los que se encuentran en el array de repositorios especiapes para dicho trato
	//Originalmente utilizado para Petrotech, donde MLopez vendió la idea de llenar desde una forma de eBavel la tabla de mensajes Push de eForms, pero no había manera de que se generara
	//la tarea de este agente, así que había que forzar la revisión directa de la tabla de mensajes de eForms cada vez que el agente entraba a su proceso
	if (!isset($arrDBsExcludedFromAgent) || !is_array($arrDBsExcludedFromAgent)) {
		$arrDBsExcludedFromAgent = array();
	}
	
	if (!isset($arrDBsEnabledForAgent) || !is_array($arrDBsEnabledForAgent)) {
		$arrDBsEnabledForAgent = array();
	}
	
	if (isset($arrPushAgentDBsWithAutoTasks) && is_array($arrPushAgentDBsWithAutoTasks)) {
		//Este proceso se realizará únicamente sobre el agente principal de eForms configurado en el archivo .xml, aunque a la fecha de implementación, ese es el único servicio configurado
		//en todos los agentes, ya que ningún agente procesa las tareas impersonando a otro servidor de KPIOnline, lo que varían son los repositorios que cada agente puede procesar
		
		logString("Checking auto-generated tasks");
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n arrPushAgentDBsWithAutoTasks:");
			PrintMultiArray($arrPushAgentDBsWithAutoTasks);
		}
		
		foreach ($arrPushAgentDBsWithAutoTasks as $strAgentRepositoryName => $intActivated) {
			//Verifica por cada uno de estos repositorios si existen o no nuevos mensajes sin procesar, si es así, generará automáticamente una tarea del Agente de push
			$blnAddAgent = true;
			if (isset($arrDBsExcludedFromAgent[$strAgentRepositoryName])) {
				//Se debe excluir este repositorio
				$blnAddAgent = false;
			}
			
			if ($blnAddAgent) {
				if (count($arrDBsEnabledForAgent) > 0) {
					if (!isset($arrDBsEnabledForAgent[$strAgentRepositoryName])) {
						$blnAddAgent = false;
					}
				}
				else {
					//Ahora en caso de no existir repositorios configurados, NO se van a procesar con este agente las tareas
					$blnAddAgent = false;
				}
			}
			
			//Si es válido el repositorio, crea la conexión para verificar si hay o no tareas pendientes
			if ($blnAddAgent) {
				logString("Checking auto-generated tasks for repository: {$strAgentRepositoryName}");
				$intPendingMsgs = 0;
				$theRepository = $BITAMRepositories[strtolower($strAgentRepositoryName)];
				if ($theRepository->open()) {
					$sql = "SELECT COUNT(*) AS PendingMessages 
						FROM SI_SV_PushMsg 
						WHERE State = ".pushMsgPending;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n sql: {$sql}");
					}
					$aRS = $theRepository->DataADOConnection->Execute($sql);
					if ($aRS && !$aRS->EOF) {
						$intPendingMsgs = (int) @$aRS->fields["pendingmessages"];
					}
				}
				
				//Si hay al menos un mensaje sin procesar, entonces genera una tarea para que el proceso del agente pueda tomarla y mandar las notificaciones
				if ($intPendingMsgs > 0) {
					logString("Pending push notifications found ({$intPendingMsgs}), auto-generating push task");
					$currDate = date('Y-m-d H:i:s');
					$sql = "INSERT INTO SI_SV_PushTasks (CreationDateID, Repository, TaskStatus, FormsPath) 
						VALUES (".
							$aBITAMConnection->ADOConnection->DBTimeStamp($currDate).", ".
							$aBITAMConnection->ADOConnection->Quote(strtolower($strAgentRepositoryName)).", ".pushMsgPending.", ".
							$aBITAMConnection->ADOConnection->Quote('Auto-generated').")";
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n sql: {$sql}");
					}
					@$aBITAMConnection->ADOConnection->Execute($sql);
				}
			}
		}
	}
	//@JAPR
	
	//checamos las demas conexiones de FBM000 en otros servers que están configuradas como agentes en el archivo de configuraciones del servicio
	//(si no hubiera ningún agente configurado, significaría que este servicio sólo procesaría sus propias tareas, esto permitiría colocar 2 o mas
	//servicios, uno en cada servidor, y así cada servidor podría procesar sus propias tareas optimizando el proceso, ya que de lo contrario las
	//peticiones realmente se estaban realizando sincronamente y un sólo servicio hubiera tardado mucho mas en terminar de resolver las tareas de
	//todos los servidores)
	foreach ($aConfiguration->agents as $eachAgent) {
		$aBITAMConnection = null;
		ob_start();
    	try {
			//@JAPR 2013-11-06: Modificado para reutilizar los archivos del Model Manager, los cuales no requieren el password desencriptado
			//$pass = BITAMDecryptPassword($eachAgent->password);
			$pass = $eachAgent->password;
			//@JAPR
			$aBITAMConnection = BITAMConnection::NewInstance($eachAgent->server, $eachAgent->username, $pass, "fbm000", 'mysql');
			if (is_null($aBITAMConnection) || is_null($aBITAMConnection->ADOConnection)) {
				$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
				ob_end_clean();
				logString("Unable to connect to '{$eachAgent->server}': {$strErrorMsg}", '', true, true);
				return false;
			}
		} catch (Exception $e){
			ob_end_clean();
			logString("Unable to connect to '{$eachAgent->server}'", '', true, true);
      		$aBITAMConnection = NULL;
			return false;
		}
		ob_end_clean();
    	
		if ($aBITAMConnection) {
			logString("Reading pending task from server '{$eachAgent->server}'");
			//@JAPR 2015-01-30: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
			$sql = "SELECT DISTINCT Repository 
				FROM SI_SV_PushTasks 
				WHERE (TaskStatus IS NULL OR TaskStatus <> ".stsActive.") AND 
					(AgentID IS NULL OR AgentID = ".$aBITAMConnection->ADOConnection->Quote('').") 
				ORDER BY PushTaskID";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			//$aBITAMConnection->close();
			if ($aRS === false) {
				logString("Error executing query in server '{$eachAgent->server}': {$sql}. Error: ".@$aBITAMConnection->ADOConnection->ErrorMsg().", query: {$sql}", '', true, true);
				//En caso de error no terminará la ejecución, simplemente creará la tabla de tareas, pero obviamente no hay tareas en este server
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n sql: {$sqlCREATE}");
				}
				$aBITAMConnection->ADOConnection->Execute($sqlCREATE);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n sql: {$sqlCREATELog}");
				}
				$aBITAMConnection->ADOConnection->Execute($sqlCREATELog);
			}
			
			//@JAPR 2015-02-03: Independientemente de si hubo o no error en la consulta, se intenta actualizar la metadata por si le hiciera
			//falta, mas abajo se validará si puede o no continuar con el proceso normal
			//@JAPRWarning: Agregar en este punto futuros campos que se requieran para que el proceso del agente actualice el mismo su metadata
			//@JAPR
			
			if ($aRS !== false) {
				//@JAPR 2013-07-11: Ahora reutilizará las variables globales de configuración para determinar que repositorios debe procesar
				//este Agente, por si se configuran múltiples agentes en diferentes server, así cada server procesará sólo las cuentas que
				//si graban sus datos en él
				//@JAPR 2015-09-07: Agregado el procesamiento de DataSources al agente, ahora independientemente de si tiene o no tareas que procesar, deberá incluir el repositorio para
				//procesar los DataSources
				$arrAgentsIncluded = array();
				//@JAPR
				
				while (!$aRS->EOF) {
					//@JAPR 2013-07-11: Ahora reutilizará las variables globales de configuración para determinar que repositorios debe procesar
					//este Agente, por si se configuran múltiples agentes en diferentes server, así cada server procesará sólo las cuentas que
					//si graban sus datos en él
					$blnAddAgent = true;
					$strAgentRepositoryName = strtoupper((string) @$aRS->fields["repository"]);
					if (isset($arrDBsExcludedFromAgent[$strAgentRepositoryName])) {
						//Se debe excluir este repositorio
						$blnAddAgent = false;
					}
					
					//@JAPR 2016-01-14: Modificado para que ahora el Agente sólo procese aquellas tareas que pertenecen exclusivamente a repositorios que están configurados en este
					//servicio donde está corriendo el archivo php del Agente, independientemente de si dicho repositorio existe en el array $arrDBsEnabledForAgent, dicho array sólo
					//servirá a partir de ahora para limitar las BDs que sean las únicas que se deban procesar con el Agente, lo cual en v6 es completamente irrelevante ya que todo
					//repositorio debería ser procesado precisamente por el agente, así que con este cambio se podrá dejar $arrDBsEnabledForAgent completamente vacío, sólo llenar
					//$arrDBsExcludedFromAgent con repositorios que por alguna extraña razón se quisieran poner en Stand By (es decir, no procesar sus capturas) y simplemente va a
					//procesar todos los repositorios que se encuentren en el archivo .ini (#WUH8DI)
					//Para facilitar las cosas, simplemente se sobreescribirá internamente el array $arrDBsEnabledForAgent con los repositorios del archivo EKT_cfg.ini, de tal manera
					//que lo configurado manualmente en el archivo eFormsAgentConfig.inc.php será completamente irrelevante, así todos los repositorios del archivo .ini se considerarán
					//válidos para procesar, excepto si están configurados en el array $arrDBsExcludedFromAgent
					if ($blnAddAgent) {
						if (count($arrDBsEnabledForAgent) > 0) {
							if (!isset($arrDBsEnabledForAgent[$strAgentRepositoryName])) {
								$blnAddAgent = false;
							}
						}
						else {
							//Ahora en caso de no existir repositorios configurados, NO se van a procesar con este agente las tareas
							$blnAddAgent = false;
						}
					}
					//@JAPR
					
					//Si no se debe agregar este agente, incrementa el recordset y salta este repositorio
					if (!$blnAddAgent) {
						$aRS->MoveNext();
						continue;
					}
					//@JAPR
					
					$anArray = array($eachAgent->server, 80, $eachAgent->username, $pass, 1, $aRS->fields["repository"], 1);
					$activeAgents[] = eFormsAgent::fromArray($anArray);
					//@JAPR 2015-09-07: Agregado el procesamiento de DataSources al agente, ahora independientemente de si tiene o no tareas que procesar, deberá incluir el repositorio para
					//procesar los DataSources
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Adding Push agent processed {$strAgentRepositoryName}", 1, 1, "color:blue;");
					}
					$arrAgentsIncluded[$strAgentRepositoryName] = $strAgentRepositoryName;
					//@JAPR
					$aRS->MoveNext();
				}
				
				if ($aRS) {
					$aRS->close();
				}
				
				//@JAPR 2015-09-07: Agregado el procesamiento de DataSources al agente, ahora independientemente de si tiene o no tareas que procesar, deberá incluir el repositorio para
				//procesar los DataSources
				//Si no se había agregado este repositorio por sus tareas, lo agrega por sus DataSources
				foreach ($arrDBsEnabledForAgent as $strAgentRepositoryName => $intActive) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Checking Push agent processed {$strAgentRepositoryName}", 1, 1, "color:blue;");
					}
					if (!isset($arrAgentsIncluded[$strAgentRepositoryName])) {
						$anArray = array($eachAgent->server, 80, $eachAgent->username, $pass, 1, strtolower($strAgentRepositoryName), 1);
						$activeAgents[] = eFormsAgent::fromArray($anArray);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Adding new Push agent to process {$strAgentRepositoryName}", 1, 1, "color:blue;");
						}
					}
				}
				//@JAPR
			}
		}
	}
	
	if (count($activeAgents) == 0) {
		logString("There were no pending push tasks found");
	}
	else {
		logString("There were ".count($activeAgents)." repositories found with pending push tasks to process");
	}
	
	return $activeAgents;
}

/* Dada una conexión a una base de datos FBM000 y los datos de un agente, obtiene la siguiente clave de tarea aun no tomada en dicho repositorio,
para lo cual primero hace un UPDATE de la tabla utilizando el ID de sesión de este agente para permitirle reutilizar sólo tareas libres apartandola
para que algún otro agente no la tome al mismo tiempo
//@JAPRChecked
*/
function getNextTaskKeyToProcess($aBITAMConnection, $anAgent) {
	global $strSessionID;
	
	if (is_null($anAgent) || !is_object($anAgent)) {
		return 0;
	}
	
	$intNumChecks = 0;
	$intMaxFreeTasksCheck = 10;
	$blnContinueCheck = true;
	$intPushTaskID = 0;
	$strAgentID = $aBITAMConnection->ADOConnection->Quote($strSessionID);
	$strRepository = $aBITAMConnection->ADOConnection->Quote($anAgent->repository);
	logString("Adquiring a push task to process in server '{$anAgent->server}' => '{$strRepository}'");
	
	//Primero aparta una tarea para este agente, si no hubo registros afectados quiere decir que no hay tareas pendientes que no estén siendo
	//procesadas por lo que ya no es necesario
	while ($blnContinueCheck) {
		$sql = "UPDATE SI_SV_PushTasks 
  				SET AgentID = {$strAgentID} 
			WHERE Repository = {$strRepository} AND 
				(TaskStatus IS NULL OR TaskStatus <> ".stsActive.") AND 
				(AgentID IS NULL OR AgentID = ".$aBITAMConnection->ADOConnection->Quote('').") 
			LIMIT 1";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: {$sql}");
		}
		if ($aBITAMConnection->ADOConnection->Execute($sql)) {
			$intAffectedRows = (int) @$aBITAMConnection->ADOConnection->_affectedrows();
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n Affected rows: {$intAffectedRows}");
			}
			
			if ($intAffectedRows > 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n Verifying task");
				}
				
				//Se logró apartar la tarea, así que ahora verifica que finalmente sea este el agente que la apartó, ya que si en el mismo
				//instante 2 o mas agentes hubieran hecho un UPDATE a este registro, sólo uno de ellos finalmente tendría asignado el AgentID y
				//debe verificar que sea este antes de usar esta tarea, si no hubiera sido este AgentID quien tomó la tarea, entonces se debe repetir el proceso
				$sql = "SELECT PushTaskID 
					FROM SI_SV_PushTasks 
					WHERE Repository = {$strRepository} AND AgentID = {$strAgentID}";
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n sql: {$sql}");
				}
				$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
				if ($aRS) {
					if (!$aRS->EOF) {
						//Si encontró la tarea que este servicio actualizó, así que regresa su ID para procesarla
						//Sólo debería haber un registro en este query, aunque si hubiera mas de uno de todas formas toma el primero
						$intPushTaskID = (int) @$aRS->fields["pushtaskid"];
						$blnContinueCheck = false;
					}
					else {
						//Si no se hubiera podido recuperar ningún registro, entonces quiere decir que alguien mas tomó la tarea que teóricamente
						//se había apartado, así que repite el proceso hasta "n" veces
						//En este caso el agente de push procesa las tareas por repositorio como una entidad, así que si el repositorio ya estaba tomado, no tiene sentido continuar
						//pues no habría otra tarea del mismo repositorio hasta que el otro agente la hubiera liberado
						/*$intNumChecks++;
						if ($intNumChecks > $intMaxFreeTasksCheck) {
							$blnContinueCheck = false;
							break;
						}*/
						$blnContinueCheck = false;
					}
				}
				else {
					$blnContinueCheck = false;
					logString("Error adquiring a push task to process in server '{$anAgent->server}': ".@$aBITAMConnection->ADOConnection->ErrorMsg(), '', true, true);
				}
			}
			else {
				//En este caso no se pudo actualizar ningún registro, así que no hay tareas libres por lo que ya no continua identificando alguna,
				//sin embargo verifica si hubiera algún tipo de error y previamente había quedado alguna tarea apartada sin terminar, para en 
				//dicho caso procesar esa tarea
				//@JAPRWarning: Por el momento no se ve como este caso sería posible, ya que los agentes usan como Sesión un TimeStampt así que
				//un agente que interrumpió abruptamente una carga en otra sesión no podría continuar en una sesión posterior
				$blnContinueCheck = false;
			}
		}
		else {
			//Si no se pudo ejecutar el UPDATE, entonces cancela el proceso de ejecución
			$blnContinueCheck = false;
			logString("Error adquiring a push task to process: ".@$aBITAMConnection->ADOConnection->ErrorMsg(), '', true, true);
		}
	}
	
	if ($intPushTaskID > 0) {
		logString("Push task ID : {$intPushTaskID}");
	}
	return $intPushTaskID;
}

/* Obtiene los datos de la tarea especificada y hace una llamada a eForms para que cargue los datos de la captura asociada a ella
*/
function executePushTask($aBITAMConnection, $aPushTaskID) {
	if (getParamValue('DebugBreak', 'both', '(int)', true)){
		ECHOString("executePushTask {$aPushTaskID}", 1, 1, "color:purple;");
	}
	
	global $BITAMRepositories;
	global $aConfiguration;
	global $intRequestNumber;
	global $strDefaultLogStringFile;
	global $strDefaultDetailLogStringFile;
	global $blnDebugAgentMode;
	
	$currDate = date('Y-m-d H:i:s');
	$intRequestNumber++;
	$strDebugFileName = '';
	if (((string) @$strDefaultDetailLogStringFile) != '') {
		$strDebugFileName = $strDefaultDetailLogStringFile;
	}
	if (trim($strDebugFileName) == '') {
		$strDebugFileName = "eFormsPushAgent.log";
	}
	$strDebugFileName = str_ireplace('.log', '_'.$intRequestNumber.'.log', $strDebugFileName);
	$blnPushErrors = false;
	$strAllPushErrors = "";
	
	$intConnectionTimeOut = $intRequestTimeOut = (int) $aConfiguration->execution_timeout;
	if ($intConnectionTimeOut < 0) {
		$intConnectionTimeOut = 0;
	}
	if ($intRequestTimeOut <= 0) {
		$intRequestTimeOut = 600;
	}
	
	//Crea un ciclo sólo para poder salir con break y así poder marcar la tareas como fallida en caso de errores de conexión o algún tipo que no permita continuar con el resto del proceso
	do {
		//Dentro de toda esta función se duplicarán las llamadas a logString variando el archivo en la segunda llamada para que se genere el archivo de depuración específico, el cual
		//además contendrá cualquier salida del buffer que hubiera ocurrido a consecuencia de llamadas de las clases utilizadas por este proceso, las cuales no estuvieran validadas y que
		//no están preparadas para mandar su salida al log del proceso, de esa manera en caso de error se dejará registrado en el log del proceso todos los incidentes, sólo si se hubiera
		//procesado todo ok, al final de esta función se eliminará el archivo de depuración generado
		logString("Executing push task ID: {$aPushTaskID}");
		logString("Executing push task ID: {$aPushTaskID}", $strDebugFileName, true, false, false);
		$sql = "SELECT Repository 
			FROM SI_SV_PushTasks 
			WHERE PushTaskID = {$aPushTaskID}";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: {$sql}");
		}
		$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
		if ($aRS === false) {
			logString("Error reading the push task definition in server '{$aConfiguration->fbm_db_server}': {$sql}. Error: ".@$aBITAMConnection->ADOConnection->ErrorMsg().", query: {$sql}", '', true, true);
			logString("Error reading the push task definition in server '{$aConfiguration->fbm_db_server}': {$sql}. Error: ".@$aBITAMConnection->ADOConnection->ErrorMsg().", query: {$sql}", $strDebugFileName, true, true, false);
			break;
		}
		elseif ($aRS->EOF) {
			logString("The specified push task no longer exists '{$aPushTaskID}'", '', true, true);
			logString("The specified push task no longer exists '{$aPushTaskID}'", $strDebugFileName, true, true, false);
			break;
		}
		
		//Si llega aqui entonces está listo para ejecutar la tarea, pero antes la marca en estatus de ejecutando por si ocurre algún problema que
		//interrumpiera el proceso antes de terminarlo
		$sql = "UPDATE SI_SV_PushTasks SET 
				TaskStatus = ".stsActive.", 
				TaskStartDate = ".$aBITAMConnection->ADOConnection->DBTimeStamp($currDate)." 
			WHERE PushTaskID = {$aPushTaskID}";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: {$sql}");
		}
		$aBITAMConnection->ADOConnection->Execute($sql);
		
		//A diferencia del Agente de grabado de capturas, este Agente debe vivir en un único procesos ya que se realiza una conexión a servicios externos (Apple, Google) la cual debe
		//permanecer abierta mediante un identificador de sesión para no correr el riesgo de ser marcados como Spam, por lo tanto es este mismo proceso de PhP el que deberá conectarse
		//al repositorio y obtener todo lo necesario para reutilizar las clases de PushMsg.inc.php, sin embargo no es un ambiente normal así que no se debería esperar que $_SESSION tenga
		//información adecuada, y si se va a depender de ella entonces es en este punto donde se debe sobreescribir manualmente cada valor para que las clases utilizadas por el proceso
		//puedan utilizarla
		$theRepositoryName = (string) @$aRS->fields["repository"];
		logString("Connecting to the eForms repository '{$theRepositoryName}'", '');
		logString("Connecting to the eForms repository '{$theRepositoryName}'", $strDebugFileName, true, false, false);
		$theRepository = $BITAMRepositories[$theRepositoryName];
		if (!$theRepository->open()) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n Can not connect to Repository, please contact your System Administrator '{$theRepositoryName}'");
			}
			logString("Can not connect to Repository, please contact your System Administrator ('{$theRepositoryName}')", '', true, true);
			logString("Can not connect to Repository, please contact your System Administrator ('{$theRepositoryName}')", $strDebugFileName, true, true, false);
			break;
		}	
		
		//Aunque este agente permanece todo el tiempo corriendo, es necesario asignar esta variable de sesión para permitir que las clases que hacen uso de ella estén funcionen correctamente,
		//de cualquier manera sólo existe un repositorio siendo procesado por el agente
		$_SESSION["PAFBM_Mode"] = 1;
		$_SESSION["PABITAM_RepositoryName"] = $theRepository->RepositoryName;
		
		//Carga la colección de mensajes de Push a procesar
		logString("Loading pending push messages");
		logString("Loading pending push messages", $strDebugFileName, true, false, false);
		$objPushMsgCollection = BITAMPushMsgCollection::NewInstance($theRepository, '', '', '', -1, -1, '', 100, false, array(pushMsgPending, pushMsgReprocess));
		if (is_null($objPushMsgCollection)) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n There was an error loading the pending push messages");
			}
			logString("There was an error loading the pending push messages", '', true, true);
			logString("There was an error loading the pending push messages", $strDebugFileName, true, true, false);
			break;
		}
		
		//Ejecutar este INSERT en caso de falla (stsFailed) en la carga de la encuesta o si se completó (stsCompleted) correctamente
		$sqlINSERT = "INSERT INTO SI_SV_PushTasksLog (PushTaskID, CreationDateID, Repository, TaskStatus, TaskStartDate, FormsPath, AgentID, LogFileName) 
			SELECT PushTaskID, CreationDateID, Repository, %s, TaskStartDate, FormsPath, AgentID, ".
				$aBITAMConnection->ADOConnection->Quote($strDebugFileName)." 
			FROM SI_SV_PushTasks 
			WHERE PushTaskID = {$aPushTaskID}";
		
		//Ejecutar este DELETE si la tarea termina ok
		$sqlDELETE = "DELETE FROM SI_SV_PushTasks WHERE PushTaskID = {$aPushTaskID}";
		if (getParamValue('DebugBreak', 'both', '(int)', true)){
			ECHOString("Termina de armar insert y delete", 1, 1, "color:purple;");
		}
		
		$arrPushMessagesSent = array();
		$arrPushMessagesFailed = array();
		foreach ($objPushMsgCollection->Collection as $objPushMsg) {
			try {
				//Atrapa cualquier salida en el buffer para poder reportarlo como parte del log de depuración
				logString("Processing push message {$objPushMsg->MessageID}");
				logString("Processing push message {$objPushMsg->MessageID}", $strDebugFileName, true, false, false);
				ob_start();
				$strErrorDesc = $objPushMsg->sendNotifications();
				$strProcessOutput = (string) @ob_get_contents();
				ob_end_clean();
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n {$strProcessOutput}<br>\r\n");
				}
				if ($strProcessOutput != '') {
					logString($strProcessOutput, $strDebugFileName, true, false, false);
				}
				
				$blnPushErrors = $blnPushErrors || ($strErrorDesc != '');
				if ($strErrorDesc) {
					$strAllPushErrors .= "\r\n Push message ID: {$objPushMsg->MessageID}\r\n. Error detail: ".$strErrorDesc.".\r\n Output buffer: {$strProcessOutput}";
					logString("Error executing the push message {$objPushMsg->MessageID}: {$strErrorDesc}", '', true, true);
					logString("Error executing the push message {$objPushMsg->MessageID}: {$strErrorDesc}", $strDebugFileName, true, true, false);
					continue;
				}
				
				logString("Push message was sent successfully");
				logString("Push message was sent successfully", $strDebugFileName, true, false, false);
			} catch (Exception $e) {
				$strProcessOutput = (string) @ob_get_contents();
				ob_end_clean();
				
				if ($strProcessOutput != '') {
					logString($strProcessOutput, $strDebugFileName, true, false, false);
				}
				logString("Error executing the push message {$objPushMsg->MessageID}. Exception detail: ".$e->getMessage(), '', true, true);
				logString("Error executing the push message {$objPushMsg->MessageID}. Exception detail: ".$e->getMessage(), $strDebugFileName, true, true, false);
				continue;
			}
		}
		break;
	} while (false);	//Condición siempre falsa para forzar a salir después de una iteración, el ciclo sólo existe para permitir el uso de break en lugar de return y llegar al código siguiente
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n Terminó de procesar los mensajes de la tarea<br>\r\n");
	}
	
	//******************************************************************************************************************************************
	//******************************************************************************************************************************************
	//Elimina el archivo de depuración en caso de no haber existido ningún error
	//******************************************************************************************************************************************
	//******************************************************************************************************************************************
	if ($blnPushErrors) {
		//Debe marcar la tarea como fallida y eliminarla de la tabla de pendientes
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: ".str_ireplace('%s', stsFailed, $sqlINSERT));
		}
		if ($aBITAMConnection->ADOConnection->Execute(str_ireplace('%s', stsFailed, $sqlINSERT))) {
			//Si la pudo insertar en la tabla de log, entonces si la elimina de la tabla de pendientes
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sqlDELETE}");
			}
			$aBITAMConnection->ADOConnection->Execute($sqlDELETE);
			
			//@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
			//Después de mover la tarea a la tabla de log, crea la nueva tarea recalendarizada siempre y cuando aún no llegue al máximo de
			//intentos por procesarla
			//$objPushTask->TaskStatus = stsFailed;
			//$objPushTask->reschedule($aBITAMConnection);
			//@JAPR
		}
		else {
			//De lo contrario simplemente actualiza la tabla de pendientes para no perder la información
			$sql = "UPDATE SI_SV_PushTasks SET 
					TaskStatus = ".stsFailed." 
				WHERE PushTaskID = {$aPushTaskID}";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
		}
		
		//Este tipo de errores deben ser notificados ya que el proceso de envío de la notificación push ni siquiera inició, así que no hay ninguna otra notificación
		//vía EMail que se hubiera enviado
		reportPushTaskError($aBITAMConnection, $strAllPushErrors, null);
	}
	else {
		//Todo estuvo bien, debe marcar la tarea como completada y eliminarla de la tabla de pendientes
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: ".str_ireplace('%s', stsCompleted, $sqlINSERT));
		}
		if ($aBITAMConnection->ADOConnection->Execute(str_ireplace('%s', stsCompleted, $sqlINSERT))) {
			//Si la pudo insertar en la tabla de log, entonces si la elimina de la tabla de pendientes
			
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("If ejecucion correcta de insert", 1, 1, "color:purple;");
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sqlDELETE}");
			}
			$aBITAMConnection->ADOConnection->Execute($sqlDELETE);
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Ejecución Incorrecta de insert se ejecutará UPDATE", 1, 1, "color:purple;");
			}
			//De lo contrario simplemente actualiza la tabla de pendientes para no perder la información
			$sql = "UPDATE SI_SV_PushTasks SET 
					TaskStatus = ".stsCompleted." 
				WHERE PushTaskID = {$aPushTaskID}";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
		}
		
		//@JAPR 2016-10-21: Agregadas configuraciones para depuración del Agente
		if (!$aConfiguration->push_agent_debug_mode) {
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			@unlink(GeteFormsLogPath().$strDebugFileName);
			//@JAPR
		}
		//@JAPR
		return false;
	}
	
	return true;
}

//@JAPR 2013-02-28: Agregada la notificación vía eMail de errores de captura
/* Envía un eMail al personal especializado en resolver errores de envío de mensajes push de eForms cuando el servicio recibe una solicitud pero por alguna
causa no puede hacer el envío correcto. Adicionalmente se enviará un eMail al sistema de soporte, aunque en ese caso el EMail que se envía
sólo ocurre una vez por día para no saturar de tickets de soporte al sistema
*/
function reportPushTaskError($aRepository, $strEntryError, $theUser = null, $aPushTask = null) {
	//Por el momento no habrá reporte de error
	return;
	if (is_null($aPushTask)) {
		return false;
	}
	
	@require_once("bpaemailconfiguration.inc.php");
	
	$anInstanceEmailConf = @BPAEmailConfiguration::readConfiguration();
	if(is_null($anInstanceEmailConf)) {
		return false;
	}
	
	$SMTPServer = (string) @$anInstanceEmailConf->fbm_email_server;	
	$SMTPPort = (string) @$anInstanceEmailConf->fbm_email_port;
	$SMTPUserName = (string) @$anInstanceEmailConf->fbm_email_username;
	$SMTPPassword = (string) @$anInstanceEmailConf->fbm_email_pwd;
	$SMTPEmailSource = (string) @$anInstanceEmailConf->fbm_email_source;
	if (strlen($SMTPServer) == 0 && strlen($SMTPPort) == 0 && strlen($SMTPUserName) == 0) {
		return false;
	}
	
	//@JAPR 2012-10-10: Validada la integración con eBavel, como eBavel mandará eMails, incluye primero la clase PhPMailer así que se debe validar
	//que sólo incluya eForms su versión si no lo hizo ya eBavel
	if (!class_exists("PHPMailer")) {
		@require_once("mail/class.phpmailer.php");
		
		if (!class_exists("PHPMailer")) {
			return false;
		}
	}
	//@JAPR
	
	//Indentifica las cuentas a las que enviará los EMails basado en el tipo de cuenta maestra de la que se trata (si ocurre algún error, se
	//asumirá que se debe notificar a soporte interno por lo menos para que se verifique por qué sucede este error)
	$blnSendToSupport = false;
	$blnSendToQA = false;
	$blnSendToMasterAcc = false;
	$blnSendToBitamSupport = false;
	$blnSendToUserAcc = false;
	$blnSendToKPIAdmin = false;
	$blnSendToDevelopMent = true;
	//@JAPR 2015-12-10: Corregido un bug, esta es una variable global, así que no se debe asignar aquí sino hacer referencia a ella nada mas
	global $blnTestingServer;
	//$blnTestingServer = false;
	//@JAPR
	
	//Null significa que no se pudo determinar el CustomerType, por lo tanto se asumirá que se debe notificar solo a desarrollo para que verifique
	//por qué no se pudo determinar. Es diferente a que la tabla tenga un Null en cuyo caso se va a asumir que es Free Trial
	$intCustomerType = null;
	/* Customer Types:
		-9	- NA 
		1 	- Customer 
		2	- Free Trial 
		3	- Internal 
		4	- Partner 
		5	- POC 
		6	- Partner Prospect 
		7	- Free Trial Contactado 
		8	- Customer Dead 
		9	- Partner Dead 
		10	- Delete 
		11	- Demo 
		12	- Testing
	*/
	
	$strMasterAccount = '';
	$strRepositoryName = $aPushTask->RepositoryName;
	$gstrOutboxFileName = $aPushTask->FileName;
	$sql = "SELECT b.EMail, a.CustomerType 
		FROM saas_databases a, saas_users b 
		WHERE a.UserID = b.UserID AND a.Repository = ".$aRepository->ADOConnection->Quote($strRepositoryName);
	$aRS = $aRepository->ADOConnection->Execute($sql);
    if ($aRS && !$aRS->EOF) {
	    $strMasterAccount = (string) @$aRS->fields["email"];
	    $intCustomerType = @$aRS->fields["customertype"];
	    if (is_null($intCustomerType)) {
	    	//Se tiene que revisar por qué del error, así que envía a desarrollo
	    	$intCustomerType = cmrtFreeTrial;
	    }
	    else {
	    	//Se convierte a numérico y se desactiva el envío a desarrollo
	    	$intCustomerType = (int) $intCustomerType;
			$blnSendToDevelopMent = false;
	    }
    }
    
    //Según el tipo de Customer habilita el envío de EMail a diferentes cuentas
    switch ($intCustomerType) {
    	case cmrtInternal:
    		//Las cuentas Internal deben ser dirigidas al responsable de la cuenta que generalmente es el Administrador y a soporte de Bitam, 
    		//ya que no son tan prioritarias como los clientes así que no generarán ticket de KPIOnline (ellos lo podrían generar automáticamente, 
    		//pero no se considera prioritario como para hacerlo automático)
    		$blnSendToMasterAcc = true;
    		$blnSendToBitamSupport = true;
    		break;
		
    	case cmrtCustomerDead:
    	case cmrtPartnerDead:
    		//Las cuentas de Customer o Partner Dead teoricamente ya no importan, sin embargo se notificará a los encargados de KPIOnline por si
    		//se tratara de algún tipo de error ya que continua recibiendo peticiones de eForms y tal vez se necesite cambiar el Status o darla
    		//de baja en definitiva
    		$blnSendToKPIAdmin = true;
    		break;	
    		
    	case cmrtDelete:
    		//Las cuentas eliminadas no tienen por qué reportar, así que termina el proceso
    		return false;
    		break;
    		
    	case cmrtTesting:
    		//Las cuentas de testing se deben notificar exclusivamente al equipo de Control de Calidad. Generalmente la cuenta Administradora no
    		//importa, pero si la cuenta específica del tester
    		$blnSendToQA = true;
    		$blnSendToUserAcc = true;
    		break;
    		
    	//Se asumirá cualquier tipo no identificado como si fuera de máxima prioridad, esto para no cometer errores e ignorar a cuentas importantes
    	//que pudieran estar estrenando un CustomerType recientemente definido. Estas cuentas no notifican al usuario que captura, solo al
    	//responsable que es la cuenta Administradora (Generan un ticket de KPIOnline automáticamente)
    	default:
			$blnSendToSupport = true;
			$blnSendToMasterAcc = true;
    		break;
    }
    
    if ($blnTestingServer) {
		//$blnSendToSupport = false;
		$blnSendToQA = true;
		//$blnSendToMasterAcc = false;
		//$blnSendToBitamSupport = false;
		//$blnSendToUserAcc = false;
		//$blnSendToDevelopMent = false;
		//$blnSendToKPIAdmin = false;
		//$blnSendToDevelopMent = false;
    }
    
	//Obtiene los datos específicos del error. Para haber llegado aqui es que se trata de un error que pudo ser atrapado por eForms y que generó
	//un log y respaldó los archivos para restaurarlo, así que tenemos los datos exactamente de que archivos son y el detalle del error
	$streFormsService = $aPushTask->FormsPath;
	$strUserAccount = $aPushTask->User;
	$strPushDate = $aPushTask->PushDate;
	$intPushID = $aPushTask->PushID;
	$intAgendaID = $aPushTask->AgendaID;
	$strEFormsVersion = ESURVEY_SERVICE_VERSION;
	$strNav = $aPushTask->UserAgent;
	$strLogFileName = $aPushTask->LogFileName;
	$strPushName = $intPushID;
	$strAgendaName = $intAgendaID;
	$appVersion = (float) $aPushTask->FormsVersion;
	
	$mail = new PHPMailer();
	$mail->Mailer = "smtp";
	$mail->Host = $SMTPServer;
	$mail->Port = $SMTPPort;
	$mail->SMTPAuth = TRUE;
	//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
	//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
	$mail->SMTPAutoTLS = false;
	//$mail->SMTPDebug = false;
	//$mail->Debugoutput = 'echo';
	//$mail->SMTPSecure = '';
	//$mail->CharSet = 'UTF-8';
	//@JAPR
	$mail->Username = $SMTPUserName;
	$mail->Password = $SMTPPassword;
	$mail->From = $SMTPEmailSource;
	$mail->IsHTML(TRUE);
	//@JAPR 2013-03-26: Reemplazados los <enters> porque en el EMail no se veía bien
	$strEntryError = str_ireplace("\r\n", "\r\n<br><br>", $strEntryError);
	//@JAPR 2015-11-23: Agregado el nombre del producto que reporta
	$strProductName = (($gbIsGeoControl)?"GeoControl":"eForms");
	//@JAPR
	$mail->Subject = $strProductName.": ".str_ireplace("{var2}", $strUserAccount, str_ireplace("{var1}", $strMasterAccount, translate("An error has been produced while saving a form for the {var1} account with the {var2} user")));
	//OMMC 2015-10-29: se añade decode al subject y htmlentities al body para corregir acentos
	$mail->Subject = utf8_decode($mail->Subject);
	$mail->Body = "
<h2>".translate("Error made on the following form during saving time").".</h2>
<b>".translate("Service release").": </b>{$strEFormsVersion}
<br>
<b>".translate("App version").": </b>{$appVersion}
<br>
<b>".translate("Mobile device").": </b>{$strNav}
<br>
<b>".translate("Account").": </b>{$strMasterAccount}
<br>
<b>".translate("User").": </b>{$strUserAccount}
<br>
<b>".translate("Repository").": </b>{$strRepositoryName}
<br>
<b>".translate("Push").": </b>{$strPushName}
<br>
<b>".translate("Agenda").": </b>{$strAgendaName}
<br>
<b>".translate("Entry date").": </b>{$strPushDate}
<br>
<b>".translate("Service used").": </b>{$streFormsService}
<br>
<br>
<b>".translate("Error Detail").": </b>
<br>
{$strEntryError}
<br>
";
$mail->Body = utf8_decode($mail->Body);

	//Agrega el archivo log de errores
	//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strFullLogFileName = GeteFormsLogPath().$strLogFileName;
	//@JAPR
	clearstatcache();
	if (file_exists($strFullLogFileName)) {
		$mail->AddAttachment($strFullLogFileName, $strLogFileName);
	}
	
	//@JAPR 2015-12-10: Removidas las cuentas del equipo de QA a petición de Claudia, sólo recibirán notificaciones si la cuenta es de testing
	//@$mail->AddAddress("jpuente@bitam.com", "Juan Puente");
	@$mail->AddAddress("psanchez@bitam.com", "Pedro Sanchez");
	//@$mail->AddAddress("cgil@bitam.com", "Claudia Gil");
	@$mail->AddAddress("jgonzalez@bitam.com", "J Gonzalez");
	@$mail->AddAddress("glopez@bitam.com", "Gustavo López");
	@$mail->AddAddress("aceballos@bitam.com", "Adrian Ceballos");
	//@$mail->AddAddress("jagonzalez@bitam.com", "Jose A Gonzalez");
	//@$mail->AddAddress("jcuellar@bitam.com", "J Cuellar");
	//@$mail->AddAddress("csanchez@bitam.com", "Carlos Sanchez");
	
	//Agrega las direcciones de las personas encargadas de resolver problemas de eForms
	if ($blnSendToDevelopMent) {
		@$mail->AddAddress(eFormsDeveloper1, "{$strProductName} Developer 1");
		@$mail->AddAddress(eFormsDeveloper2, "{$strProductName} Developer 2");
		@$mail->AddAddress(eFormsDeveloper3, "{$strProductName} Developer 3");
		//@JAPR 2015-12-08: Agregada la lista de distribución para desarrollo
		@$mail->AddAddress(eFormsDevelopmentTeam, "{$strProductName} Development team");
	}
	if ($blnSendToKPIAdmin) {
		@$mail->AddAddress(KPIAdmin1, "KPI Admin 1");
		@$mail->AddAddress(KPIAdmin2, "KPI Admin 2");
		@$mail->AddAddress(KPIOnlineAdminTeam, "{$strProductName} Administrators");
	}
	if ($blnSendToQA) {
		@$mail->AddAddress(QAStaff1, "QA Staff 1");
		@$mail->AddAddress(QAStaff2, "QA Staff 2");
		@$mail->AddAddress(QAStaff3, "QA Staff 3");
		@$mail->AddAddress(QAStaff4, "QA Staff 4");
		@$mail->AddAddress(QAStaff5, "QA Staff 5");
		//@JAPR 2015-12-08: Agregada la lista de distribución para calidad
		@$mail->AddAddress(eFormsQualityTeam, "Quality Staff");
	}
	if ($blnSendToBitamSupport) {
		@$mail->AddAddress(SupportStaff1, "Support Staff 1");
		@$mail->AddAddress(SupportStaff2, "Support Staff 2");
	}
	//@JAPR 2016-01-04: Deshabilitado el reporte de errores a las cuentas de los clientes, sólo se enviará a soporte o el resto de destinos según Customer Type (#UM9ZJU)
	//Este cambio se pidió a raiz que los clientes no desean recibir notificaciones de error, de manera permanente se notificará al equipo de soporte así que
	//eso cubrirá todo posible problema
	/*if (trim($strUserAccount) != '' && strpos($strUserAccount, '@') !== false) {
		if ($blnSendToUserAcc) {
			@$mail->AddAddress($strUserAccount, "{$strProductName} User");
		}
	}
	if (trim($strMasterAccount) != '' && strpos($strMasterAccount, '@') !== false) {
		if ($blnSendToMasterAcc) {
			@$mail->AddAddress($strMasterAccount, "{$strProductName} Account Admin");
		}
	}*/
	if ($blnSendToSupport) {
		//@JAPR 2015-11-19: Esta cuenta ya no está funcional, se remueve este reporte por ahora
		//@JAPR 2015-12-07: Modificada la cuenta de soporte ya que la original fue dada de baja (#NVL2WM)
		@$mail->AddAddress(KPIOnlineSupportTeam, "KPI Online Support Team");
		@$mail->AddAddress(SupportStaff1, "Support Staff 1");
		@$mail->AddAddress(SupportStaff2, "Support Staff 2");
	}
	//Siempre se manda a internal para que quede registro del evento
	@$mail->AddAddress(InternalEForms, "{$strProductName}");
	//@JAPR 2014-02-20: Agregado el reporte de error en las notificaciones vía EMail
	//@JAPR 2016-01-04: Deshabilitado el reporte de errores a las cuentas de los clientes, sólo se enviará a soporte o el resto de destinos según Customer Type (#UM9ZJU)
	//Este cambio se pidió a raiz que los clientes no desean recibir notificaciones de error, de manera permanente se notificará al equipo de soporte así que
	//eso cubrirá todo posible problema
	//AddCuentaCorreoAltToMail($theUser, $mail, $appVersion);
	//@JAPR
	
	//@$mail->Send();
	if (!@$mail->Send()) {
		//Enviamos al log el error generado
		global $SendMailLogFile;
		
		$aLogString = "\r\n\r\n Error sending report: ".date('Y-m-d H:i:s');
		$aLogString .= "\r\n".@$mail->ErrorInfo;
		$aLogString .= "\r\n Capture detail: \r\n".@$mail->Body."\r\n";
		//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		//No se estaba grabando el log de errores de envíos de correo
		@error_log($aLogString, 3, GeteFormsLogPath().((string) @$SendMailLogFile));
		//@JAPR
	}
	//@JAPR
	
	return true;
}
?>