<?php
/* Este archivo contendrá definiciones de funciones que utilizando sólo PhP, realizan cálculos de fechas y horas, los cuales tradicionalmente se habían hecho con funciones propietarias
programadas en utils.inc.php u otros archivos desde el framework original en php 5.2
*/

/* Dada una fecha inicial y final como rango (incluyendo horas si así se desea), así como un patrón de repetición como intervalo, obtiene el conjunto de fechas en el formato indicado
que component todos los instantes entre ese rango brincado en las porciones definidas por el intervalo
Se puede indicar el intervalo por un valor Entero > 0 en el parámetro $iInterval que corresponderá con el tipo de indicador de periodo $sPeriodType, o bien utilizar la sintaxis extendida
en´el parámetro $sIntervalExt para especificar exactamente el patrón de saltos según la documentación. Los tipos de indicadores de periodo base son:
Y	años
M	meses
D	días
W	semanas; estas se convierten a días, por lo que no se puede combinar con D.
H	horas
N	minutos
S	segundos
http://php.net/manual/es/class.dateperiod.php
http://php.net/manual/es/dateinterval.construct.php
El valor resultante es el array de todos los puntos en el tiempo que fueron generados dados el intervalo indicado en el rango de fechas
*/
function GetDateTimeIntervalArrBetweenDates($aStartDate, $anEndDate, $iInterval = null, $sPeriodType = null, $sIntervalExt = null, $sFormat = null) {
	$arrDates = array();
	try {
		if ((is_null($iInterval) || is_null($sPeriodType)) && is_null($sIntervalExt)) {
			return $arrDates;
		}
		
		if (is_null($sFormat)) {
			$sFormat = 'Y-m-d';
		}
		
		$iInterval = (int) $iInterval;
		
		//Genera el intervalo del tipo especificado:
		if (is_null($sIntervalExt)) {
			$sPeriodType = strtoupper($sPeriodType);
			switch ($sPeriodType) {
				case 'Y':
				case 'M':	//Para permitir diferenciar entre minutos o meses, se usará internamente "M" para meses
				case 'D':
					//Estos intervalos representan fechas, van directos
					$sIntervalExt = "P".$iInterval.$sPeriodType;
					break;
				
				case 'H':
				case 'N':	//Para permitir diferenciar entre minutos o meses, se usará internamente "N" para minutos
				case 'S':
					//Estos intervalos representan tiempo, así que se agrega un modificador mas
					$sIntervalExt = "PT".$iInterval.(($sPeriodType == 'N')?'M':$sPeriodType);
					break;
				
				default:
					//No es un intervalo válido, no podrá continuar
					$sIntervalExt = null;
					break;
			}
		}
		
		if (is_null($sIntervalExt)) {
			return $arrDates;
		}
		
		$dteStart = new DateTime($aStartDate);
		$dteEnd = new DateTime($anEndDate);
		$dteInterval = new DateInterval($sIntervalExt);
		$arrDateRange = new DatePeriod($dteStart, $dteInterval, $dteEnd);
		
		if (is_object($arrDateRange)) {
			foreach ($arrDateRange as $objDate) {
				$strDate = $objDate->format($sFormat);
				$arrDates[] = $strDate;
			}
		}
		
	}
	catch (Exception $e) {
	}
	
	return $arrDates;
}

//@JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
/* Dada una fecha inicial y una fecha final (que tentativamente tienen horas), obtiene la diferencia entre ambas fechas, regresando el objeto interval correspondiente o bien el valor
acumulado según el tipo de intervalo específico que se necesite, en cuyo caso regresará un valor numérico únicamente. Regresa Null en caso de algún error en el proceso, ya sea una fecha
inválida o un cálculo erróneo. Las fechas deben venir en formato universal YYYY-MM-DD HH:MM:SS
Los tipos de indicadores de periodo base son:
Y	años
M	meses; //Para permitir diferenciar entre minutos o meses, se usará internamente "M" para meses
D	días
H	horas
N	minutos; //Para permitir diferenciar entre minutos o meses, se usará internamente "N" para minutos, por compatibilidad con PhP, se soportará "i" como identificador de minutos
S	segundos
*/
function DateDiff($aDate1, $aDate2, $sInterval = null) {
	try {
		$dteDate1 = new DateTime($aDate1);
		$dteDate2 = new DateTime($aDate2);
		$objInterval = $dteDate1->diff($dteDate2);
		
		if (!is_null($sInterval)) {
			$dblResult = null;
			switch (strtoupper($sInterval)) {
				case 'Y':
					$dblResult = (int) @$objInterval->y;
					break;
				case 'M':
					$dblResult = ((int) @$objInterval->y) * 12 + (int) @$objInterval->m;
					break;
				case 'D':
					$dblResult = (int) @$objInterval->days;
					break;
				case 'H':
					$dblResult = ((int) @$objInterval->days) * 24 + (int) @$objInterval->h;
					break;
				case 'N':
				case 'I':
					$dblHours = ((int) @$objInterval->days) * 24 + (int) @$objInterval->h;
					$dblResult = $dblHours * 60 + (int) @$objInterval->i;
					break;
				case 'S':
					$dblHours = ((int) @$objInterval->days) * 24 + (int) @$objInterval->h;
					$dblMinutes = $dblHours * 60 + (int) @$objInterval->i;
					$dblResult = $dblMinutes * 60 + (int) @$objInterval->s;
					break;
			}
			
			if (!is_null($dblResult)) {
				return $dblResult;
			}
		}
	}
	catch (Exception $e) {
		return null;
	}
	
	//En caso de llegar a este punto, significaría que no se especificó un intervalo a regresar, así que regresa el objeto completo
	return $objInterval;
}
?>