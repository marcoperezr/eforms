<?php

require_once('cubeClasses.php');
require_once("dimensionTableField.inc.php");

class BITAMDimensionValue extends BITAMObject
{
	public $DimensionValueID;
	public $OldDimensionValueID;	//Valor anterior de la Clave de este miembro de dimensión (utilizado sólo si la dimensión tiene el mismo campo Clave que Descriptor y se está editando el valor)
	public $Description;
	public $ParentValueID;			// (Descontinuado)
	public $cla_concepto;
	public $consecutivo;
	//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
	//public $cla_descrip;
	//@JAPR
	public $nom_logico;
	public $nom_tabla;
	public $nom_fisicok;
	public $nom_fisicok_bd;
	public $nom_fisicok_join_bd;
	public $nom_fisico;
	public $nom_fisico_bd;
	public $tipo_dato;
	public $tipo_datok;
	public $ParentKeyName;			// (Descontinuado)
	public $ParentKeyDataType;		// (Descontinuado)
	public $ParentKeyJoin;			// Nombre del campo llave de la dimensión Padre del SnowFlake tal como se encuentra definido en la tabla de la Dimensión Hija (se obtiene de SI_DESCRIP_KEYS) (Descontinuado)
	//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
	//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
	public $ParentKeyJoinDataType;	// Tipo de dato del Join hacia el Padre (puede ser el mismo que ParentKeyDataType pero eso implica que el Join es el mismo que el ID del padre y no necesariamente es así)
	//@JAPR
	public $ParentsInfo;			// Arreglo con la información de los múltiples padres de una dimensión
	public $AttributesInfo;			// Arreglo con la información de los atributos de una dimensión
	public $created_by;				// 0 = Artus Administrator, 1 = Ektos, 2 = Artus Wizard
	public $newObject;
	public $ForParentAssignment;
	public $IsEmpty;				//Indica si tanto la llave como el descriptor vienen vacios
	public $DimensionValuesArray;
	/* Array multidimensional para la asociación de padres
		array[DimensionValueID][0] = DimensionValueID;
		array[DimensionValueID][1] = DimensionValueDescription;
		array[DimensionValueID][2] = DimensionValueParentID;
		array[DimensionValueID][parentFieldName] = DimensionValueParentID;
	*/
	public $FieldsValues;			// Colección con los valores capturados en los campos adicionales seleccionados por dimensión
	public $FromCaptureTable;		// Indica que se está capturando un nuevo valor desde la ventana del presupuesto
	public $IncludeAdditionalFields;// Indica si al capturar el valor, se pedirán también los campos adicionales asignados a la dimensión
	public $IsDialog;
	// Campos ocultos para reenviar la información del POST para grabar valores desde el presupuesto (solo si FromCaptureTable = 1)
	// y solo son asignados por el PerformHTTPRequest
	public $POSTDimensions;
	public $POSTCombinations;
	public $POSTDates;
	public $POSTMembersFrom;
	public $POSTPeriodID;
	public $POSTStartDateDim;
	public $POSTEndDateDim;
	public $HasAutoKeyGeneration;
	
	function __construct($aRepository, $aDimensionID, $aCubeID, $featuresDimension = null) //, $aDescripKey = -1)
	{
		BITAMObject::__construct($aRepository);
		$this->DimensionValueID = null;
		$this->OldDimensionValueID = null;
		$this->Description = '';
		$this->ParentValueID = null;
		$this->consecutivo = $aDimensionID;
		$this->cla_concepto = $aCubeID;
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		//$this->cla_descrip = $aDescripKey;
		//@JAPR
		$this->newObject = true;
		$this->ParentKeyDataType =7;
		$this->ParentKeyName = '';
		$this->ParentKeyJoin = '';
		//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
		//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
		$this->ParentKeyJoinDataType = tINT;
		//@JAPR
		$this->ParentsInfo = array();
		$this->created_by = 0;
		$this->DimensionValuesArray = array();
		$this->FieldsValues = array();
		$this->ForParentAssignment = false;
		$this->FromCaptureTable = false;
		$this->IncludeAdditionalFields = false;
		$this->POSTDimensions = '';
		$this->POSTCombinations = '';
		$this->POSTDates = '';
		$this->POSTMembersFrom = '1';
		$this->POSTPeriodID = '4';
		$this->POSTStartDateDim = '';
		$this->POSTEndDateDim = '';
		$this->IsDialog = false;
		$this->HasAutoKeyGeneration = false;
		$this->IsEmpty = false;
		
		if(!is_null($featuresDimension))
		{
			$this->nom_logico = $featuresDimension["nom_logico"];
			$this->nom_tabla = $featuresDimension["nom_tabla"];
			$this->nom_fisicok_bd = $featuresDimension["nom_fisicok_bd"];
			$this->nom_fisicok = $featuresDimension["nom_fisicok"];
			$this->nom_fisicok_join_bd = $featuresDimension["nom_fisicok_join_bd"];
			$this->nom_fisico_bd = $featuresDimension["nom_fisico_bd"];
			$this->nom_fisico = $featuresDimension["nom_fisico"];
			$this->tipo_datok = $featuresDimension["tipo_datok"];
			$this->tipo_dato = $featuresDimension["tipo_dato"];
			$this->ParentKeyDataType = $featuresDimension["parent_tipo_datok"];
			//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
			//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
			$this->ParentKeyJoinDataType = $featuresDimension["parent_tipo_datok_join"];
			//@JAPR
			$this->ParentKeyName = $featuresDimension["parent_nom_fisicok_bd"];
			$this->ParentKeyJoin = $featuresDimension["parent_nom_fisicok_join"];
			$this->ParentsInfo = &$featuresDimension["parents"];
			$this->AttributesInfo = &$featuresDimension["attributes"];
			$this->created_by = $featuresDimension["created_by"];
		}
		else
		{
			$featuresDimension = BITAMDimension::getFeaturesDimension($aRepository, $aDimensionID, $aCubeID);
			if (!is_null($featuresDimension))
			{
				$this->nom_logico = $featuresDimension["nom_logico"];
				$this->nom_tabla = $featuresDimension["nom_tabla"];
				$this->nom_fisicok_bd = $featuresDimension["nom_fisicok_bd"];
				$this->nom_fisicok = $featuresDimension["nom_fisicok"];
				$this->nom_fisicok_join_bd = $featuresDimension["nom_fisicok_join_bd"];
				$this->nom_fisico_bd = $featuresDimension["nom_fisico_bd"];
				$this->nom_fisico = $featuresDimension["nom_fisico"];
				$this->tipo_datok = $featuresDimension["tipo_datok"];
				$this->tipo_dato = $featuresDimension["tipo_dato"];
				$this->ParentKeyDataType = $featuresDimension["parent_tipo_datok"];
				//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
				//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
				$this->ParentKeyJoinDataType = $featuresDimension["parent_tipo_datok_join"];
				//@JAPR
				$this->ParentKeyName = $featuresDimension["parent_nom_fisicok_bd"];
				$this->ParentKeyJoin = $featuresDimension["parent_nom_fisicok_join"];
				$this->ParentsInfo = &$featuresDimension["parents"];
				$this->AttributesInfo = &$featuresDimension["attributes"];
				$this->created_by = $featuresDimension["created_by"];
			}
			else 
			{
				$this->nom_logico = '';
				$this->nom_tabla = '';
				$this->nom_fisicok_bd = '';
				$this->nom_fisicok = '';
				$this->nom_fisicok_join_bd = '';
				$this->nom_fisico_bd = '';
				$this->nom_fisico = '';
				$this->tipo_datok = -1;
				$this->tipo_dato = -1;
			}
		}
	}
	
	/*El parámetro $bLoadAdditionalFields indica que se deben crear propiedades para almacenar los campos del catálogo que se deben
	capturar adicionalmente con la dimensión. El parámetro $bLoadParentFields indica que se deben crear dichas propiedades además
	para tantos padres de SnowFlake como existan en la dimensión
	*/
	static function NewInstance($aRepository, $aDimensionID, $aCubeID, $featuresDimension = null, $bLoadAdditionalFields = true, $bLoadParentFields = true, $bLoadAttributeFields = true)
	{
		global $appSettings;
		
		$anInstance = new BITAMDimensionValue($aRepository, $aDimensionID, $aCubeID, $featuresDimension);
		if ($bLoadParentFields)
		{
			if (count($anInstance->ParentsInfo) > 0)
			{
				foreach ($anInstance->ParentsInfo as $aParentConsecutive => $aParentInfo)
				{
					eval ('$anInstance->ParentValueID'.$aParentConsecutive.' = null;');
				}
			}
		}
		
		//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
		if ($bLoadAttributeFields && $appSettings["DimensionAttributes"] == 1)
		{
			if (count($anInstance->AttributesInfo) > 0)
			{
				foreach ($anInstance->AttributesInfo as $anAttributeConsecutive => $anAttributeInfo)
				{
					eval ('$anInstance->AttributeValueID'.$anAttributeConsecutive.' = null;');
				}
			}
		}
		//@JAPR
		
		if ($bLoadAdditionalFields && $appSettings["DimensionFields"] == 1)
		{
			$anInstance->IncludeAdditionalFields = true;
			// Carga los campos adicionales que se deben capturar para esta dimensión, asignando un null por default
			$aDimensionTableFieldCollection = BITAMDimensionTableFieldCollection::NewInstance($aRepository, $aDimensionID, $aCubeID);
			foreach ($aDimensionTableFieldCollection as $aTableField)
			{
				//@JAPRWarning: $strEscapedFieldName = chr(39).addcslashes($aTableField->Name, "\0..\37\\\"\'\177..\377").chr(39);
				//@JAPRWarning: $strEscapedFieldName = chr(39).htmlspecialchars($aTableField->Name, ENT_QUOTES).chr(39);
				$strEscapedFieldName = chr(39).PartAWSAdd($aTableField->Name).chr(39);
				$anInstance->FieldsValues[$strEscapedFieldName] = null;
				eval ('$anInstance->'.$aTableField->Name.' = null;');
			}
		}
		return $anInstance;
	}

	// Si se especifica el parámetro $bLoadAdditionalFields == true, se cargarán los campos adicionales asignados a la dimensión
	static function NewInstanceWithID($aRepository, $aDimensionValueID, $aDimensionID, $aCubeID, $bLoadAdditionalFields = true)
	{
		global $appSettings;
		global $workingMode;
		
		$anInstance = null;
		
		//@JAPR 2011-06-23: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if (((int) $aDimensionID) < 0 || (((int) $aCubeID) < 0 && $appSettings["UseGlobalDimensions"] == 0))
		{
			return $anInstance;
		}
		//@JAPR

		$anInstance =& BITAMGlobalInstance::GetDimensionValueInstanceWithID($aCubeID, $aDimensionID, $aDimensionValueID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		//@JAPR 2010-03-26: Agregado el modo de trabajo BITAM para reutilizar los proyectos de Artus Web G6
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if ($appSettings["UseGlobalDimensions"] == 0)
		{
			if ($workingMode == wrkmBITAM)
			{
				$aCube = BITAMCube::NewInstanceWithID($aRepository, $aCubeID);
			}
		}
		//@JAPR
		
		$aDimensionTableFieldCollection = null;
		$featuresDimension = null;
		$featuresDimension = BITAMDimension::getFeaturesDimension($aRepository, $aDimensionID, $aCubeID);
		if (!is_null($featuresDimension))
		{
			// Carga los campos adicionales asociados con esta dimensión
			$strAdditionalFields = '';
			if ($bLoadAdditionalFields && $appSettings["DimensionFields"] == 1)
			{
				//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
				/*
				if ($appSettings["UseGlobalDimensions"] == 1)
				{
					$aDimensionTableFieldCollection = BITAMDimensionTableFieldCollection::NewInstanceWithDescripID($aRepository, $aDimensionID["cla_descrip"], $aCubeID);
				}
				else
				{
				*/
					$aDimensionTableFieldCollection = BITAMDimensionTableFieldCollection::NewInstance($aRepository, $aDimensionID, $aCubeID);
				//}
				//@JAPR
				foreach ($aDimensionTableFieldCollection as $aTableField)
				{
					$strAdditionalFields .= ', '.$aTableField->Name;
				}
			}
			
			$strParentField = '';
			$strAttributeField = '';
			/*
			if (array_key_exists('parent_nom_fisicok_bd', $featuresDimension))
			{
				$strParentField = $featuresDimension["parent_nom_fisicok_bd"];
				if (trim($strParentField) != '')
				{
					$strParentField = ', '.$strParentField.' AS ParentValueID ';
				}
				else 
				{
					$strParentField = '';
				}
			}
			*/
			
			//@JAPR 2008-05-21: Agregada la asignación de multiples padres
			/*
			if (array_key_exists('parent_nom_fisicok_join', $featuresDimension))
			{
				$strParentField = $featuresDimension["parent_nom_fisicok_join"];
				if (trim($strParentField) != '')
				{
					$strParentField = ', '.$strParentField.' AS ParentValueID ';
				}
				else 
				{
					$strParentField = '';
				}
			}
			*/
			//Agrega el campo para cada uno de los padres de esta dimensión
			if (array_key_exists('parents', $featuresDimension))
			{
				foreach ($featuresDimension['parents'] as $aParentConsecutive => $aParentInfo)
				{
					$strParentFieldName = $aParentInfo["parent_nom_fisicok_join"];
					if (trim($strParentFieldName) != '')
					{
						$strParentField .= ', '.$strParentFieldName.' AS ParentValueID'.$aParentConsecutive.' ';
					}
				}
			}
			
			//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
			if (array_key_exists('attributes', $featuresDimension))
			{
				foreach ($featuresDimension['attributes'] as $anAttributeConsecutive => $anAttributeInfo)
				{
					//Se asume que las dimensiones - atributo siempre son dimensiones sin Llave, puesto que una llave alternativa a la de la 
					//dimensión primaria (o a la Subrogada en todo caso) es innecesaria realmente, por ende sólo agregamos este campo
					$strAttributeFieldName = $anAttributeInfo["attribute_nom_fisicok_bd"];
					if (trim($strAttributeFieldName) != '')
					{
						$strAttributeField .= ', '.$strAttributeFieldName.' AS AttributeValueID'.$anAttributeConsecutive.' ';
					}
				}
			}
			//@JAPR
			
			//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
			$strDimensionValueID = formatValueForSQL($aRepository, $aDimensionValueID, $featuresDimension["tipo_datok"]);
			//$strDimensionValueID = ( ( (int)$featuresDimension["tipo_datok"] === 7 || (int)$featuresDimension["tipo_datok"] === 8) ? $aDimensionValueID : $aRepository->DataADOConnection->Quote($aDimensionValueID));
			//@JAPR
			if (trim($strDimensionValueID) == '')
			{
				$strDimensionValueID = 0;
			}
			$sql = 'SELECT '.$featuresDimension["nom_fisicok_bd"].' AS DimensionValueID, '.$featuresDimension["nom_fisico_bd"].' AS DimensionValueDescription '.$strParentField.$strAttributeField.$strAdditionalFields.
				' FROM '.$featuresDimension["nom_tabla"].' '.
				'WHERE  '.$featuresDimension["nom_fisicok_bd"].' = '.$strDimensionValueID.' '.
				'ORDER BY 2';
			global $queriesLogFile;
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMDimensionValue->NewInstanceWithID: \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$featuresDimension["nom_tabla"]." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			if (!$aRS->EOF)
			{
				$anInstance = BITAMDimensionValue::NewInstanceFromRS($aRepository, $aRS, $featuresDimension, $aDimensionTableFieldCollection);
			}
		}
		
		return $anInstance;
	}

	static function NewInstanceForParentAssignment($aRepository, $anArrayOfDimensionValuesIDs = null, $aDimensionID, $aCubeID, $in)
	{
		global $maxDimensionRecords;
		global $dimValuesFilter;
		global $appSettings;
		
		$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
		$aParentDimension = null;
		if (!is_null($aDimension))
		{
			if (count($aDimension->FatherParDescriptor) > 0)
			{
				$aParentDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimension->FatherParDescriptor[0], $aCubeID, false);
			}
			
			if (!is_null($aParentDimension))
			{
				//@JAPRWarning: 2009-08-09: No recuerdo por qué se puso esta validación, quitandola todos los padres no creados por Ektos (claves variables) se cargarían
				/*
				if ($aParentDimension->created_by != 1)
				{
					$aParentDimension = null;
				}
				*/
				//@JAPR
			}
		}

		// Si no hay dimensión padre, no continua
		//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
		// Modificado para considerar si tiene dimensiones atributo
		if (is_null($aParentDimension) && !$aDimension->hasAttributes)
		{
			return BITAMDimensionValueCollection::NewInstance($aRepository, null, $aDimensionID, $aCubeID, true, true, false);
		}
		
		$anInstance = new BITAMDimensionValue($aRepository, $aDimensionID, $aCubeID);
		$anInstance->ForParentAssignment = true;
		$featuresDimension = BITAMDimension::getFeaturesDimension($aRepository, $aDimensionID, $aCubeID);
		if (!is_null($featuresDimension))
		{
			$anInstance->tipo_dato = $featuresDimension["tipo_dato"];
			$anInstance->tipo_datok = $featuresDimension["tipo_datok"];
			
			$where = "";
			if (!is_null($anArrayOfDimensionValuesIDs))
			{
				switch (count($anArrayOfDimensionValuesIDs))
				{
					case 0:
						break;
					case 1:
						//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
						$strDimensionValueID = formatValueForSQL($aRepository, $anArrayOfDimensionValuesIDs[0], $featuresDimension["tipo_datok"]);
						if($in==true)
						{
							$where = " WHERE ".$featuresDimension["nom_fisicok_bd"]." = ".$strDimensionValueID;
							//$where = " WHERE ".$featuresDimension["nom_fisicok_bd"]." = ".(((int)$featuresDimension["tipo_datok"] === 7 || (int)$featuresDimension["tipo_datok"] === 8) ? $anArrayOfDimensionValuesIDs[0] : $aRepository->DataADOConnection->Quote($anArrayOfDimensionValuesIDs[0]));
						}
						else 
						{
							$where = " WHERE ".$featuresDimension["nom_fisicok_bd"]." <> ".$strDimensionValueID;
							//$where = " WHERE ".$featuresDimension["nom_fisicok_bd"]." <> ".(((int)$featuresDimension["tipo_datok"] === 7 || (int)$featuresDimension["tipo_datok"] === 8 ) ? $anArrayOfDimensionValuesIDs[0] : $aRepository->DataADOConnection->Quote($anArrayOfDimensionValuesIDs[0]));
						}
						//@JAPR
						break;
					default:
						foreach ($anArrayOfDimensionValuesIDs as $aDimensionValueID)
						{
							if ($where != "")
							{
								$where .= ", ";
							}
							//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
							$strDimensionValueID = formatValueForSQL($aRepository, $aDimensionValueID, $featuresDimension["tipo_datok"]);
							$where .= $strDimensionValueID; 
							//$where .= (((int) $featuresDimension["tipo_datok"] === 7 || (int) $featuresDimension["tipo_datok"] === 8 ) ? $aDimensionValueID : $aRepository->DataADOConnection->Quote($aDimensionValueID)); 
							//@JAPR
						}
						if ($where != "")
						{
							if($in==true)
							{
								$where = " WHERE ".$featuresDimension["nom_fisicok_bd"]." IN (".$where.")";
							}
							else 
							{
								$where = " WHERE ".$featuresDimension["nom_fisicok_bd"]." NOT IN (".$where.")";
							}
						}
						break;
				}
			}

			$strParentField = '';
			/*
			if (array_key_exists('parent_nom_fisicok_bd', $featuresDimension))
			{
				$strParentField = $featuresDimension["parent_nom_fisicok_bd"];
				if (trim($strParentField) != '')
				{
					$strParentField = ', '.$strParentField.' AS ParentValueID ';
				}
				else 
				{
					$strParentField = '';
				}
			}
			*/
			
			//@JAPR 2008-05-21: Agregada la asignación de multiples padres
			if (array_key_exists('parent_nom_fisicok_join', $featuresDimension))
			{
				$strParentField = $featuresDimension["parent_nom_fisicok_join"];
				if (trim($strParentField) != '')
				{
					$strParentField = ', '.$strParentField.' AS ParentValueID ';
				}
				else 
				{
					$strParentField = '';
				}
			}
			
			if (array_key_exists('parents', $featuresDimension))
			{
				foreach ($featuresDimension['parents'] as $aParentConsecutive => $aParentInfo)
				{
					$strParentFieldName = $aParentInfo["parent_nom_fisicok_join"];
					if (trim($strParentFieldName) != '')
					{
						$strParentField .= ', '.$strParentFieldName.' AS ParentValueID'.$aParentConsecutive.' ';
					}
				}
			}
			
			//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
			if ($appSettings["DimensionAttributes"] == 1 && array_key_exists('attributes', $featuresDimension))
			{
				foreach ($featuresDimension['attributes'] as $anAttributeConsecutive => $anAttributeInfo)
				{
					$strAttributeFieldName = $anAttributeInfo["attribute_nom_fisicok_bd"];
					if (trim($strAttributeFieldName) != '')
					{
						$strParentField .= ', '.$strAttributeFieldName.' AS AttributeValueID'.$anAttributeConsecutive.' ';
					}
				}
			}
			//@JAPR
			
			//@JAPR 2011-06-23: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			$intClaBD = MYSQL;
			$sTopSQL = '';
			$sTopMySQL = '';
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				//Si se carga en forma global, sólo se puede asumir que es un proyecto ligado al Model Manager por lo que busca la definición
				//de conexión default para éste y utiliza el tipo de BD especificado ahí
				$sql = "SELECT CLA_BD FROM SI_CONEXION_BD WHERE CLA_CONEXION = ".DEFAULT_MODEL_MANAGER_CONNECTION;
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_CONEXION_BD ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				if (!$aRS->EOF)
				{
					$intClaBD = $aRS->fields["cla_bd"];
				}
			}
			else
			{
				//Si se carga asociado a un cubo, se utiliza el tipo de base de datos especificado para este
				$aCube = BITAMCube::NewInstanceWithID($aRepository, $aCubeID);
				$intClaBD = $aCube->cla_bd;
			}
			if($maxDimensionRecords > 0)
			{
				//@JAPRWarning: Se pudo utilizar el método SelectLimit de la conexión, pero noté que para SQL Server carga el total del query y en
				//memoria hace el conteo de registros, así que preferí enviar la cláusula correspondiente desde el propio estatuto SQL
				switch ($intClaBD)
				{
					case SQL_SERVER:
						$sTopSQL = ' TOP '.$maxDimensionRecords.' ';
						break;
					case MYSQL:
						$sTopMySQL = ' LIMIT '.$maxDimensionRecords.' ';
						break;
				}
			}
			//@JAPR
			
			if(trim($dimValuesFilter) != '' && trim($where) == 0)
			{
				$where = ' WHERE '.$featuresDimension["nom_fisico_bd"].' LIKE '.$anInstance->Repository->DataADOConnection->Quote('%'.$dimValuesFilter.'%');
			}
			
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			$sql = "SELECT DISTINCT ".$sTopSQL.$featuresDimension["nom_fisicok_bd"]." AS DimensionValueID, ";
			if ($aDimension->isCatalog)
			{
				$sql .= $featuresDimension["nom_fisicok_bd"]." AS DimensionValueDescription ";
			}
			else
			{
				$sql .= $featuresDimension["nom_fisico_bd"]." AS DimensionValueDescription ";
			}
			$sql .= $strParentField.
				" FROM 	".$featuresDimension["nom_tabla"].$where." ORDER BY 2".$sTopMySQL;
			global $queriesLogFile;
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMDimensionValue->NewInstanceForParentAssignment: \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$featuresDimension["nom_tabla"]." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			while (!$aRS->EOF)
			{
				$strDimensionValueID = $aRS->fields["dimensionvalueid"];
				$strDimensionValueDesc = $aRS->fields["dimensionvaluedescription"];
				//$strEscapedDimensionValueID = "'".addcslashes((string)$strDimensionValueID, "\0..\37\\\"\'\177..\377")."'";
				//@JAPRWarning: $strEscapedDimensionValueID = chr(39).addcslashes((string)$strDimensionValueID, "\0..\37\\\"\'\177..\377").chr(39);
				//@JAPRWarning: $strEscapedDimensionValueID = chr(39).htmlspecialchars((string)$strDimensionValueID, ENT_QUOTES).chr(39);
				$strEscapedDimensionValueID = chr(39).PartAWSAdd((string)$strDimensionValueID).chr(39);
				$anInstance->DimensionValuesArray[$strEscapedDimensionValueID][0] = $strDimensionValueID;
				$anInstance->DimensionValuesArray[$strEscapedDimensionValueID][1] = $strDimensionValueDesc;
				if (!is_null(@$aRS->fields["parentvalueid"]))
				{
					$strDimensionValueParentID = $aRS->fields["parentvalueid"];
					$anInstance->DimensionValuesArray[$strEscapedDimensionValueID][2] = $strDimensionValueParentID;
				}
				else 
				{
					$anInstance->DimensionValuesArray[$strEscapedDimensionValueID][2] = "";
				}
				foreach ($featuresDimension['parents'] as $aParentConsecutive => $aParentInfo)
				{
					$strParentFieldNameOrig = 'ParentValueID'.$aParentConsecutive;
					//$strParentFieldName = 'ParentValueID'.$aParentConsecutive;
					$strParentFieldName = chr(39).'ParentValueID'.$aParentConsecutive.chr(39);
					$anInstance->DimensionValuesArray[$strEscapedDimensionValueID][$strParentFieldName] = $aRS->fields[strtolower($strParentFieldNameOrig)];
				}
				//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
				foreach ($featuresDimension['attributes'] as $anAttributeConsecutive => $anAttributeInfo)
				{
					$strAttributeFieldNameOrig = 'AttributeValueID'.$anAttributeConsecutive;
					//$strAttributeFieldName = 'AttributeValueID'.$anAttributeConsecutive;
					$strAttributeFieldName = chr(39).'AttributeValueID'.$anAttributeConsecutive.chr(39);
					$anInstance->DimensionValuesArray[$strEscapedDimensionValueID][$strAttributeFieldName] = $aRS->fields[strtolower($strAttributeFieldNameOrig)];
				}
				//@JAPR
				$aRS->MoveNext();
			}
		}

		return $anInstance;
	}
	
	// Crea un nuevo objeto BITAMDimensionValue preparado con los campos adicionales seleccionados para su captura desde la ventana del presupuesto
	static function NewInstanceFromCaptureTable($aRepository, $aDimensionID, $aCubeID)
	{
		global $appSettings;
		
		$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
		$aParentDimension = null;
		//@JAPR 2008-05-21: Agregada la asignación de multiples padres
		//Esta validación nunca fue implementada, como que pretendía validar solo los padres que fueron creados con Ektos
		/*
		if (!is_null($aDimension))
		{
			if (count($aDimension->FatherParDescriptor) > 0)
			{
				$aParentDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimension->FatherParDescriptor[0], $aCubeID, false);
			}
			
			if (!is_null($aParentDimension))
			{
				if ($aParentDimension->created_by != 1)
				{
					$aParentDimension = null;
				}
			}
		}
		*/

		//@JAPR 2008-05-21: Agregada la asignación de multiples padres
		$anInstance = new BITAMDimensionValue($aRepository, $aDimensionID, $aCubeID);
		//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
		if (count($anInstance->AttributesInfo) > 0)
		{
			foreach ($anInstance->AttributesInfo as $anAttributeConsecutive => $anAttributeInfo)
			{
				eval ('$anInstance->AttributeValueID'.$anAttributeConsecutive.' = null;');
			}
		}
		//@JAPR

		if (count($anInstance->ParentsInfo) > 0)
		{
			foreach ($anInstance->ParentsInfo as $aParentConsecutive => $aParentInfo)
			{
				eval ('$anInstance->ParentValueID'.$aParentConsecutive.' = null;');
			}
		}
		
		$anInstance->FromCaptureTable = true;
		$anInstance->IsDialog = true;
		$featuresDimension = BITAMDimension::getFeaturesDimension($aRepository, $aDimensionID, $aCubeID);
		if (!is_null($featuresDimension))
		{
			$anInstance->tipo_dato = $featuresDimension["tipo_dato"];
			$anInstance->tipo_datok = $featuresDimension["tipo_datok"];
		}
		
		// Carga los campos adicionales que se deben capturar para esta dimensión, asignando un null por default
		if($appSettings["DimensionFields"] == 1)
		{
			$aDimensionTableFieldCollection = BITAMDimensionTableFieldCollection::NewInstance($aRepository, $aDimensionID, $aCubeID);
			foreach ($aDimensionTableFieldCollection as $aTableField)
			{
				//@JAPRWarning: $strEscapedFieldName = chr(39).addcslashes($aTableField->Name, "\0..\37\\\"\'\177..\377").chr(39);
				//@JAPRWarning: $strEscapedFieldName = chr(39).htmlspecialchars($aTableField->Name, ENT_QUOTES).chr(39);
				$strEscapedFieldName = chr(39).PartAWSAdd($aTableField->Name).chr(39);
				$anInstance->FieldsValues[$strEscapedFieldName] = null;
				eval ('$anInstance->'.$aTableField->Name.' = null;');
			}
		}

		return $anInstance;
	}
	
	// El parámetro $aAdditionalFields es la lista de campos adicionales que fueron cargados (null si solo es el valor)
	static function NewInstanceFromRS($aRepository, $aRS, $featuresDimension, $aDimensionTableFieldCollection = null)
	{
		global $appSettings;
		
		$aCubeID = -1;
	 	if(array_key_exists('cla_concepto',$featuresDimension))
	 	{
			$aCubeID=(int) $featuresDimension["cla_concepto"];
	 	}
	 	$aDimensionID = -1;
	 	if(array_key_exists('consecutivo',$featuresDimension))
	 	{
			$aDimensionID = (int) $featuresDimension["consecutivo"];
	 	}
	 	$aDimensionValueID = null;
	 	if(array_key_exists('dimensionvalueid',$aRS->fields))
	 	{
	 		$aDimensionValueID = $aRS->fields["dimensionvalueid"];
	 	}
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		$blnIsCatalog = false;
	 	if(array_key_exists('is_catalog',$featuresDimension))
	 	{
	 		$blnIsCatalog = $featuresDimension["is_catalog"];
	 	}
		//@JAPR
	 	
		$anInstance = null;
		//@JAPR 2011-06-23: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
	 	if (($aCubeID != -1 || $appSettings["UseGlobalDimensions"] == 1) && $aDimensionID != -1 && !is_null($aDimensionValueID))
	 	{
			$anInstance =& BITAMGlobalInstance::GetDimensionValueInstanceWithID($aCubeID, $aDimensionID, $aDimensionValueID);
	 	}
		if(is_null($anInstance))
		{
			$anInstance = BITAMDimensionValue::NewInstance($aRepository, $aDimensionID, $aCubeID, $featuresDimension);
			$anInstance->DimensionValueID = $aRS->fields["dimensionvalueid"];
			$anInstance->OldDimensionValueID = $anInstance->DimensionValueID;
			$anInstance->Description = $aRS->fields["dimensionvaluedescription"];
			//@JAPR 2010-01-28: Validado que registros en NULL o vacios no se puedan regresar pues no hay forma de manipularlos
			//Generalmente esto pasa cuando se trata de una dimensión Atributo, si el registro fué agregado sólo para la Dimensión Base sin llegar
			//el resto de los campos
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			if (trim((string) $anInstance->DimensionValueID == '' ) && trim((string) $anInstance->Description == '') && !$blnIsCatalog)
			{
				$anInstance->IsEmpty = true;
			}
			//@JAPR
			if (!is_null($aDimensionTableFieldCollection))
			{
				$anInstance->IncludeAdditionalFields = true;
				foreach ($aDimensionTableFieldCollection as $aTableField)
				{
					//@JAPRWarning: $strEscapedFieldName = chr(39).addcslashes($aTableField->Name, "\0..\37\\\"\'\177..\377").chr(39);
					//@JAPRWarning: $strEscapedFieldName = chr(39).htmlspecialchars($aTableField->Name, ENT_QUOTES).chr(39);
					$strEscapedFieldName = chr(39).PartAWSAdd($aTableField->Name).chr(39);
					$aTableFieldValue = $aRS->fields[strtolower($aTableField->Name)];
					$anInstance->FieldsValues[$strEscapedFieldName] = $aTableFieldValue;
					eval ('$anInstance->'.$aTableField->Name.' = $aTableFieldValue;');
				}
			}

			//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
			//Recorre cada uno de los atributos configurados buscando el valor que tiene actualmente asignado
			foreach ($anInstance->AttributesInfo as $anAttributeConsecutive => $anAttributeInfo)
			{
				$anAttributeFieldName = 'AttributeValueID'.$anAttributeConsecutive;
				$anAttributeFieldValue = @$aRS->fields[strtolower($anAttributeFieldName)];
				eval ('$anInstance->'.$anAttributeFieldName.' = $anAttributeFieldValue;');
				//@JAPR
			}
			
			//@JAPR 2008-05-21: Agregada la asignación de multiples padres
			/*
			if (array_key_exists('parentvalueid', $aRS->fields))
			{
				$anInstance->ParentValueID = $aRS->fields["parentvalueid"];
			}
			*/
			//Recorre cada uno de los padres configurados buscando el valor que tiene actualmente asignado
			foreach ($anInstance->ParentsInfo as $aParentConsecutive => $aParentInfo)
			{
				$aParentFieldName = 'ParentValueID'.$aParentConsecutive;
				$aParentFieldValue = @$aRS->fields[strtolower($aParentFieldName)];
				eval ('$anInstance->'.$aParentFieldName.' = $aParentFieldValue;');
				//@JAPR 2009-07-08: Agregadas las columnas con las descripciones de los Padres de cada Valor
				$aParentFieldName = 'ParentValueDesc'.$aParentConsecutive;
				$aParentFieldValue = @$aRS->fields[strtolower($aParentFieldName)];
				eval ('$anInstance->'.$aParentFieldName.' = $aParentFieldValue;');
				//@JAPR
			}
			
			//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
			if ($appSettings["DimensionAttributes"] == 1)
			{
				foreach ($anInstance->AttributesInfo as $anAttributeConsecutive => $anAttributeInfo)
				{
					$anAttributeFieldName = 'AttributeValueID'.$anAttributeConsecutive;
					$anAttributeFieldValue = @$aRS->fields[strtolower($anAttributeFieldName)];
					eval ('$anInstance->'.$anAttributeFieldName.' = $anAttributeFieldValue;');
				}
			}
			//@JAPR
			$anInstance->newObject = false;
		 	BITAMGlobalInstance::AddDimensionValueInstanceWithID($aCubeID, $aDimensionID, $anInstance->DimensionValueID, $anInstance);
		}
		else 
		{
			$anInstance->newObject = false;
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		global $appSettings;
		global $strVersionFieldName;
		
		$aDimensionID = -1;
		$aCubeID = -1;
		if (array_key_exists("DimensionID", $aHTTPRequest->GET) && array_key_exists("CubeID", $aHTTPRequest->GET))
		{
			$aDimensionID = (int)$aHTTPRequest->GET["DimensionID"];
			$aCubeID = (int)$aHTTPRequest->GET["CubeID"];
		}
		else 
		{	
			if (array_key_exists("DimensionID", $aHTTPRequest->POST) && array_key_exists("CubeID", $aHTTPRequest->POST))
			{
				$aDimensionID = (int)$aHTTPRequest->POST["DimensionID"];
				$aCubeID = (int)$aHTTPRequest->POST["CubeID"];
			}
		}
		
		// Verifica si se está invocando desde la tabla de captura del presupuesto
		if (array_key_exists("FromCaptureTable", $aHTTPRequest->GET) && (int)$aHTTPRequest->GET["FromCaptureTable"] == 1)
		{
			$strAction = "New";
			if (array_key_exists("Action", $aHTTPRequest->GET))
			{
				$strAction = $aHTTPRequest->GET["Action"];
			}
			
			$aDimensionValueID = '';
			if (array_key_exists("DimensionValueID", $aHTTPRequest->GET))
			{
				$aDimensionValueID = urldecode($aHTTPRequest->GET["DimensionValueID"]);
			}
			if (array_key_exists("DimensionValueID", $aHTTPRequest->POST))
			{
				$aDimensionValueID = $aHTTPRequest->POST["DimensionValueID"];
			}
			if (($strAction == 'Edit' || $strAction == 'Update') && $aDimensionValueID != '')
			{
				// Si se va a editar un valor y se envió la clave, se carga el valor actual (isNewObject() == false)
				$anInstance = BITAMDimensionValue::NewInstanceWithID($aRepository,$aDimensionValueID,$aDimensionID,$aCubeID,true);
				$anInstance->FromCaptureTable = true;
				$anInstance->IsDialog = true;
			}
			else
			{
				// En este caso se trata de la captura de un nuevo valor o no llegaron todos los datos correctamente
				$anInstance = BITAMDimensionValue::NewInstanceFromCaptureTable($aRepository,$aDimensionID,$aCubeID);
			}
			
			if (($strAction == 'Save' || $strAction == 'Update') && array_key_exists("DimensionValueID", $aHTTPRequest->POST))
			{
				$aDimensionValueID = $aHTTPRequest->POST["DimensionValueID"];
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				
				/*
				$aFieldValuesArray = null;
				if (array_key_exists("FieldsValues", $aHTTPRequest->POST))
				{
					$aFieldValuesArray = $aHTTPRequest->POST["FieldsValues"];
				}
				if (!is_null($aFieldValuesArray))
				{
					foreach ($aFieldValuesArray as $strFieldName => $aFieldValue)
					{
						$aFieldName = stripcslashes($strFieldName);
						if (array_key_exists($aFieldName, $anInstance->FieldsValues))
						{
							$anInstance->FieldsValues[$aFieldName] = $aFieldValue;
						}
					}
				}
				*/
				// Recorre todos los elementos del POST y verifica si alguno de ellos es uno de los
				// campos adicionales para asignar su valor en el Arreglo de campos
				foreach ($aHTTPRequest->POST as $aFieldName => $aFieldValue)
				{
					//@JAPRWarning: $strEscapedFieldName = chr(39).addcslashes($aFieldName, "\0..\37\\\"\'\177..\377").chr(39);
					//@JAPRWarning: $strEscapedFieldName = chr(39).htmlspecialchars($aFieldName, ENT_QUOTES).chr(39);
					$strEscapedFieldName = chr(39).PartAWSAdd($aFieldName).chr(39);
					if (array_key_exists($strEscapedFieldName, $anInstance->FieldsValues))
					{
						$anInstance->FieldsValues[$strEscapedFieldName] = $aFieldValue;
					}
				}
				
				//@JAPR 2008-05-21: Agregada la asignación de multiples padres (agregada la condición false para que no entre a esta sección
				// Si existe un padre, verificamos el tipo de dato para asignarlo a este valor
				if (false && !is_null($anInstance->ParentValueID))
				{
					$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
					$aParentDimension = null;
					if (count($aDimension->FatherParDescriptor) > 0)
					{
						$aParentDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimension->FatherParDescriptor[0], $aCubeID, false);
					}
					
					if (!is_null($aParentDimension))
					{
						/*
						$featuresDimension = BITAMDimension::getFeaturesDimension($aRepository, $aDimensionID, $aCubeID);
						if (!is_null($featuresDimension))
						{
							//NO tenemos referencia al tipo de dato del Join, asumiremos que es el mismo de la llave pero no siempre es así
							//$anInstance->ParentKeyDataType = $featuresDimension["parent_tipo_datok"];
							$anInstance->ParentKeyName = $featuresDimension["parent_nom_fisicok_join"];
						}
						*/
						
						$anInstance->ParentKeyName = $anInstance->ParentKeyJoin;
						$anInstance->ParentKeyDataType = $aParentDimension->tipo_datok;
						//$anInstance->ParentKeyName = $aParentDimension->nom_fisicok_bd;
					}
				}

				/*@JAPR 2008-05-21: Agregada la asignación de multiples padres. Asigna todos los padres que se mandaran como parte del POST, los cuales se identifican
				porque sus nombres empiezan con ParentValueID
				*/
				foreach ($aHTTPRequest->POST as $aFieldName => $aFieldValue)
				{
					//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
					//Si el campo inicia con 'AttributeValueID' y tiene mas caracteres que dicho nombre, entonces es un campo Padre de la dimensión
					//Si el campo inicia con 'ParentValueID' y tiene mas caracteres que dicho nombre, entonces es un campo Padre de la dimensión
					if ((substr($aFieldName,0,16) == 'AttributeValueID' && strlen($aFieldName) > 16) ||
						(substr($aFieldName,0,13) == 'ParentValueID' && strlen($aFieldName) > 13))
					{
						eval ('$anInstance->'.$aFieldName.' = $aFieldValue;');
					}
				}
				
				$anInstance->save();
				// Agrega el nuevo elemento a las combinaciones especificadas
				$strPOSTDimensions = '';
				if (array_key_exists("POSTDimensions", $aHTTPRequest->POST))
				{
					$strPOSTDimensions = $aHTTPRequest->POST["POSTDimensions"];
				}
				$strPOSTCombinations = '';
				if (array_key_exists("POSTCombinations", $aHTTPRequest->POST))
				{
					$strPOSTCombinations = $aHTTPRequest->POST["POSTCombinations"];
				}
				$strPOSTDates = '';
				if (array_key_exists("POSTDates", $aHTTPRequest->POST))
				{
					$strPOSTDates = $aHTTPRequest->POST["POSTDates"];
				}

				$strPOSTMembersFrom = '';
				if (array_key_exists("POSTMembersFrom", $aHTTPRequest->POST))
				{
					$strPOSTMembersFrom = $aHTTPRequest->POST["POSTMembersFrom"];
				}
				$strPOSTPeriodID = '4';
				if (array_key_exists("POSTPeriodID", $aHTTPRequest->POST))
				{
					$strPOSTPeriodID = $aHTTPRequest->POST["POSTPeriodID"];
				}
				$strPOSTStartDateDim = '';
				if (array_key_exists("POSTStartDateDim", $aHTTPRequest->POST))
				{
					$strPOSTStartDateDim = $aHTTPRequest->POST["POSTStartDateDim"];
				}
				$strPOSTEndDateDim = '';
				if (array_key_exists("POSTEndDateDim", $aHTTPRequest->POST))
				{
					$strPOSTEndDateDim = $aHTTPRequest->POST["POSTEndDateDim"];
				}
				
				$anInstance->POSTDimensions = $strPOSTDimensions;
				$anInstance->POSTCombinations = $strPOSTCombinations;
				$anInstance->POSTDates = $strPOSTDates;
				$anInstance->POSTMembersFrom = $strPOSTMembersFrom;
				$anInstance->POSTPeriodID = (int)$strPOSTPeriodID;
				$anInstance->POSTStartDateDim = $strPOSTStartDateDim;
				$anInstance->POSTEndDateDim = $strPOSTEndDateDim;
				
				// Grabamos información adicional al miembro de la dimensión recien agregado
				if ($strAction == 'Save' && $anInstance->FromCaptureTable == 1)
				{
					// Verificamos si el valor grabado es un padre dentro de un SnowFlake, y en caso de serlo, se asignan las 
					// referencias al mismo para los elementos hijos del SnowFlake hasta llegar a la tabla de hechos, suponiendo
					// que no tuvieran previamente grabado un hijo, ya que en caso de tenerlo, no se puede sobreescribir
					$blnInsertFactTable = $anInstance->updateParentReference();
					
					// Solo si se obtienen combinaciones de la tabla de hechos y no es un padre de SnowFlake, se tiene que 
					// grabar una combinación con el nuevo valor capturado para que pueda ser leída
					if ((int)$anInstance->POSTMembersFrom !== 0 && $blnInsertFactTable)
					{
						$anInstance->insertFactTable();
					}
				}

				if ($strAction == 'Save')
				{
					// Después de grabar el valor anterior, se genera un nuevo valor vacio para grabar múltiples datos
					$anInstance = BITAMDimensionValue::NewInstanceFromCaptureTable($aRepository,$aDimensionID,$aCubeID);

					// Se asignan de nuevo estas propiedades porque con el NewInstanceFromCaptureTable fueron borradas
					$anInstance->POSTDimensions = $strPOSTDimensions;
					$anInstance->POSTCombinations = $strPOSTCombinations;
					$anInstance->POSTDates = $strPOSTDates;
					$anInstance->POSTMembersFrom = $strPOSTMembersFrom;
					$anInstance->POSTPeriodID = (int)$strPOSTPeriodID;
					$anInstance->POSTStartDateDim = $strPOSTStartDateDim;
					$anInstance->POSTEndDateDim = $strPOSTEndDateDim;
				}
			}
			else 
			{
				$strPOSTDimensions = '';
				if (array_key_exists("Dimensions", $aHTTPRequest->POST))
				{
					$strPOSTDimensions = $aHTTPRequest->POST["Dimensions"];
				}
				$strPOSTCombinations = '';
				if (array_key_exists("Combinations", $aHTTPRequest->POST))
				{
					$strPOSTCombinations = $aHTTPRequest->POST["Combinations"];
				}
				$strPOSTDates = '';
				if (array_key_exists("Dates", $aHTTPRequest->POST))
				{
					$strPOSTDates = $aHTTPRequest->POST["Dates"];
				}

				$strPOSTMembersFrom = '';
				if (array_key_exists("MembersFrom", $aHTTPRequest->POST))
				{
					$strPOSTMembersFrom = $aHTTPRequest->POST["MembersFrom"];
				}
				$strPOSTPeriodID = '4';
				if (array_key_exists("PeriodID", $aHTTPRequest->POST))
				{
					$strPOSTPeriodID = $aHTTPRequest->POST["PeriodID"];
				}
				$strPOSTStartDateDim = '';
				if (array_key_exists("StartDateDim", $aHTTPRequest->POST))
				{
					$strPOSTStartDateDim = $aHTTPRequest->POST["StartDateDim"];
				}
				$strPOSTEndDateDim = '';
				if (array_key_exists("EndDateDim", $aHTTPRequest->POST))
				{
					$strPOSTEndDateDim = $aHTTPRequest->POST["EndDateDim"];
				}

				$anInstance->POSTDimensions = $strPOSTDimensions;
				$anInstance->POSTCombinations = $strPOSTCombinations;
				$anInstance->POSTDates = $strPOSTDates;
				$anInstance->POSTMembersFrom = $strPOSTMembersFrom;
				$anInstance->POSTPeriodID = (int)$strPOSTPeriodID;
				$anInstance->POSTStartDateDim = $strPOSTStartDateDim;
				$anInstance->POSTEndDateDim = $strPOSTEndDateDim;
			}
			
			return $anInstance;
		}
		
		if (array_key_exists("SetParentValuesIDs", $aHTTPRequest->GET) && (int)$aHTTPRequest->GET["SetParentValuesIDs"] == 1)
		{
			return BITAMDimensionValue::NewInstanceForParentAssignment($aRepository,null,$aDimensionID,$aCubeID,true);
		}
		
		$blnIncludeAdditionalFields = false;
		if (array_key_exists('IncludeAdditionalFields', $aHTTPRequest->GET) && (int)$aHTTPRequest->GET['IncludeAdditionalFields'] == 1)
		{
			$blnIncludeAdditionalFields = true;
		}
		
		// Verifica si se está invocando desde la asignación de padres a la colección de valores
		if (array_key_exists("DimensionValuesArray", $aHTTPRequest->POST))
		{
			$anInstance = BITAMDimensionValue::NewInstance($aRepository, $aDimensionID, $aCubeID);
			$aModifiedValuesArray = $aHTTPRequest->POST["DimensionValuesArray"];
			if (!is_null($aModifiedValuesArray))
			{
				$aCollection = BITAMDimensionValue::NewInstanceForParentAssignment($aRepository, null, $aDimensionID, $aCubeID, true);
				if (!is_null($aCollection))
				{
					//@JAPR 2012-03-06: Agregado el indicador de versión por registro para descarga inteligente de catálogos
					//$intVersionNum = null;
					//$blnIsCatalog = false;
					$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
					$intVersionNum = $aDimension->getCatalogVersion();
					$intVersionNum = ($intVersionNum >= 0)?$intVersionNum+1:null;
					$strVersionFieldName = $aDimension->VersionFieldName;
			 		//@JAPR
					
					// Recorre todos los elementos existentes buscando la clave en el arreglo del POST, si se detecta que el padre
					// cambió, se debe ejecutar un UPDATE usando el objeto BITAMDimensionValue dummy
					foreach ($aCollection->DimensionValuesArray as $strDimensionValueID => $aDimensionValueDataArray)
					{
						if (array_key_exists($strDimensionValueID, $aModifiedValuesArray))
						{
							$blnUpdated = false;
							foreach ($anInstance->ParentsInfo as $aParentConsecutive => $aParentInfo)
							{
								$aParentFieldName = 'ParentValueID'.$aParentConsecutive;
								//Primero limpiamos el valor de este padre para que no se arrastre de actualizaciones anteriores
								eval ('$anInstance->'.$aParentFieldName.' = NULL;');
								//@JAPRWarning: $strParentFieldName = chr(39).addcslashes($aParentFieldName, "\0..\37\\\"\'\177..\377").chr(39);
								$strParentFieldName = chr(39).$aParentFieldName.chr(39);
								/*Por alguna muy extraña razón, al hacer el POST de los campos con nombre de arreglo del tipo:
									name="DimensionValuesArray['1']['ParentValueID6']"
								al llegar al Server como parte de los datos leídos con $_POST, están llegando substituidas las ' por \' para el segundo elemento del arreglo
								de esta forma:
									name="DimensionValuesArray['1'][\'ParentValueID6\']"
								pero no así para el primer elemento, por lo tanto manejar los nombres dentro de PhP ya no es posible en forma directa que sea igual a los
								nombres de los datos del POST, por tanto temporalmente en lo que se identifica por qué, reemplazaremos para el segundo índice las comillas
								en $strQuotedParentFieldName y se quedarán normales para el arreglo en PhP con $strParentFieldName
								*/
								/*@JAPR 2009-07-14: Se identificó que el comentario anterior aplica para la versión 5.0.5 de PhP, con la versión 5.2.5
								el segundo índice si llega correctamente tal como lo hace el primero, por lo que resulta necesario meter una validación
								para asegurarse que independientemente de la versión, se obtendrá el valor correcto del arreglo recibido en el $_POST
								*/
								$strQuotedParentFieldName = '\\\''.$aParentFieldName.'\\\'';
								$aParentFieldValue = @$aDimensionValueDataArray[$strParentFieldName];
								//$strNewParentValueID = @$aModifiedValuesArray[$strDimensionValueID][$strQuotedParentFieldName];	//Leer comentario de arriba referente a $strParentFieldName
								$anArrayOfModifiedValues = $aModifiedValuesArray[$strDimensionValueID];
								if(isset($anArrayOfModifiedValues[$strParentFieldName]))
								{
									$strNewParentValueID = @$anArrayOfModifiedValues[$strParentFieldName];	//Leer comentario de arriba referente a $strParentFieldName
								}
								else
								{
									$strNewParentValueID = @$anArrayOfModifiedValues[$strQuotedParentFieldName];	//Leer comentario de arriba referente a $strParentFieldName
								}
								
								if ($strNewParentValueID != $aParentFieldValue)
								{
									eval ('$anInstance->'.$aParentFieldName.' = $strNewParentValueID;');
									$blnUpdated = true;
								}
							}
							
							//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
							foreach ($anInstance->AttributesInfo as $anAttributeConsecutive => $anAttributeInfo)
							{
								$anAttributeFieldName = 'AttributeValueID'.$anAttributeConsecutive;
								//Primero limpiamos el valor de este padre para que no se arrastre de actualizaciones anteriores
								eval ('$anInstance->'.$anAttributeFieldName.' = NULL;');
								//@JAPRWarning: $strAttributeFieldName = chr(39).addcslashes($anAttributeFieldName, "\0..\37\\\"\'\177..\377").chr(39);
								$strAttributeFieldName = chr(39).$anAttributeFieldName.chr(39);
								/*Por alguna muy extraña razón, al hacer el POST de los campos con nombre de arreglo del tipo:
									name="DimensionValuesArray['1']['AttributeValueID6']"
								al llegar al Server como parte de los datos leídos con $_POST, están llegando substituidas las ' por \' para el segundo elemento del arreglo
								de esta forma:
									name="DimensionValuesArray['1'][\'AttributeValueID6\']"
								pero no así para el primer elemento, por lo tanto manejar los nombres dentro de PhP ya no es posible en forma directa que sea igual a los
								nombres de los datos del POST, por tanto temporalmente en lo que se identifica por qué, reemplazaremos para el segundo índice las comillas
								en $strQuotedAttributeFieldName y se quedarán normales para el arreglo en PhP con $strAttributeFieldName
								*/
								/*@JAPR 2009-07-14: Se identificó que el comentario anterior aplica para la versión 5.0.5 de PhP, con la versión 5.2.5
								el segundo índice si llega correctamente tal como lo hace el primero, por lo que resulta necesario meter una validación
								para asegurarse que independientemente de la versión, se obtendrá el valor correcto del arreglo recibido en el $_POST
								*/
								$strQuotedAttributeFieldName = '\\\''.$anAttributeFieldName.'\\\'';
								$anAttributeFieldValue = @$aDimensionValueDataArray[$strAttributeFieldName];
								//$strNewAttributeValueID = @$aModifiedValuesArray[$strDimensionValueID][$strQuotedAttributeFieldName];	//Leer comentario de arriba referente a $strAttributeFieldName
								$anArrayOfModifiedValues = $aModifiedValuesArray[$strDimensionValueID];
								if(isset($anArrayOfModifiedValues[$strAttributeFieldName]))
								{
									$strNewAttributeValueID = @$anArrayOfModifiedValues[$strAttributeFieldName];	//Leer comentario de arriba referente a $strAttributeFieldName
								}
								else
								{
									$strNewAttributeValueID = @$anArrayOfModifiedValues[$strQuotedAttributeFieldName];	//Leer comentario de arriba referente a $strAttributeFieldName
								}
								
								if ($strNewAttributeValueID != $anAttributeFieldValue)
								{
									eval ('$anInstance->'.$anAttributeFieldName.' = $strNewAttributeValueID;');
									$blnUpdated = true;
								}
							}
							//@JAPR
							
							/*Si hubo algún cambio en cualquiera de los padres, se graba el valor pero primero asignamos el resto de
							la información como la referencia a la clave del valor y la descripción del mismo (los padres ya fueron
							asignados en el ciclo de arriba, donde también se identificó si hubo o no cambio de padres
							*/
							if ($blnUpdated)
							{
								$anInstance->newObject = false;
								$anInstance->VersionID = $intVersionNum;
								$anInstance->OldDimensionValueID = $aDimensionValueDataArray[0];
								$anInstance->DimensionValueID = $aDimensionValueDataArray[0];
								$anInstance->Description = $aDimensionValueDataArray[1];
								$anInstance->save(false);
							}
						}
					}
				}
			}

			if (array_key_exists("go", $_POST))
			{
				$go = $_POST["go"];
			}
			else
			{
				$go = "self";
			}
			if ($go == "parent")
			{
				$anInstance = $anInstance->get_Parent();
			}
			else
			{
				if ($go == "new")
				{
					$anInstance = BITAMDimensionValue::NewInstance($aRepository, $aDimensionID, $aCubeID);
				}
				else 
				{
					//@JAPR 2009-07-13: Corregido el direccionamiento para que en el Self se quede en la asignación de Padres
					//$anInstance = $anInstance->get_Parent();
					return BITAMDimensionValue::NewInstanceForParentAssignment($aRepository,null,$aDimensionID,$aCubeID,true);
				}
			}
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		
		if (array_key_exists("DimensionValueID", $aHTTPRequest->POST))
		{
			$aDimensionValueID = $aHTTPRequest->POST["DimensionValueID"];
			
			if (is_array($aDimensionValueID))
			{
				$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
				$aCollection = BITAMDimensionValueCollection::NewInstance($aRepository, $aDimensionValueID, $aDimensionID, $aCubeID, true, false, false);

				//@JAPR 2012-03-07: Agregado el indicador de versión por registro para descarga inteligente de catálogos
				//Al eliminar valores del catálogo también se incrementa la versión ya que el catálogo cambió, así que necesita descargar
				//el total de los registros existentes
				$intVersionNum = $aDimension->getCatalogVersion();
				$intVersionNum = ($intVersionNum >= 0)?$intVersionNum+1:null;
				$strVersionFieldName = $aDimension->VersionFieldName;
				$intCatalogID = $aDimension->CatalogID;
				//@JAPR
				
				require_once("catalog.inc.php");
				$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $intCatalogID);
				if (!is_null($aCatalog))
				{
					$aCatalog->save();
				}
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				
				$aHTTPRequest->RedirectTo = $aDimension;
				
				//@JAPR 2012-03-07: Agregado el indicador de versión por registro para descarga inteligente de catálogos
				if (!is_null($intVersionNum))
				{
					$sql = "UPDATE ".$aDimension->nom_tabla." SET ".$strVersionFieldName." = ".$intVersionNum;
					$aRS = $aRepository->DataADOConnection->Execute($sql);
					//@JAPR 2012-03-27: Actualizadas las encuestas cuando los catálogos que usan cambian
					if ($aRS)
					{
						require_once("survey.inc.php");
						
						//Realizamos actualizacion de LastDate en las encuestas relacionadas con este catalogos
						//@JAPR 2012-06-20: Corregido un bug, se estaba usando $this para accesar al repositorio
						//@JAPRDescontinuada en v6
						//A partir de v6 esta función quedó descontinuada, se dejará la referencia por si en algún proceso de compatibilidad se pensara reutilizar el servicio para definir formas viejas
						//La actualización de formas la realizará el DataSource directamente
						//BITAMSurvey::updateAllSurveyLastUserAndDateFromCatalog($aRepository, $intCatalogID);
						//@JAPR
					}
					//@JAPR
				}
				//@JAPR
			}
			else
			{
				// Grabado normal de un valor
				$anInstance = BITAMDimensionValue::NewInstanceWithID($aRepository, $aDimensionValueID, $aDimensionID, $aCubeID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMDimensionValue::NewInstance($aRepository, $aDimensionID, $aCubeID);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				//@JAPR 2008-05-21: Agregada la asignación de multiples padres (agregada la condición false para que no entre a esta sección
				// Si existe un padre, verificamos el tipo de dato para asignarlo a este valor
				if (false && !is_null($anInstance->ParentValueID))
				{
					$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
					$aParentDimension = null;
					if (count($aDimension->FatherParDescriptor) > 0)
					{
						$aParentDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimension->FatherParDescriptor[0], $aCubeID, false);
					}
					
					if (!is_null($aParentDimension))
					{
						/*
						$featuresDimension = BITAMDimension::getFeaturesDimension($aRepository, $aDimensionID, $aCubeID);
						if (!is_null($featuresDimension))
						{
							//NO tenemos referencia al tipo de dato del Join, asumiremos que es el mismo de la llave pero no siempre es así
							//$anInstance->ParentKeyDataType = $featuresDimension["parent_tipo_datok"];
							$anInstance->ParentKeyName = $featuresDimension["parent_nom_fisicok_join"];
						}
						*/
						
						$anInstance->ParentKeyName = $anInstance->ParentKeyJoin;
						$anInstance->ParentKeyDataType = $aParentDimension->tipo_datok;
						//$anInstance->ParentKeyName = $aParentDimension->nom_fisicok_bd;
					}
				}
				/*@JAPR 2008-05-21: Agregada la asignación de multiples padres. Asigna todos los padres que se mandaran como parte del POST, los cuales se identifican
				porque sus nombres empiezan con ParentValueID
				*/
				foreach ($aHTTPRequest->POST as $aFieldName => $aFieldValue)
				{
					//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
					//Si el campo inicia con 'AttributeValueID' y tiene mas caracteres que dicho nombre, entonces es un campo Padre de la dimensión
					//Si el campo inicia con 'ParentValueID' y tiene mas caracteres que dicho nombre, entonces es un campo Padre de la dimensión
					if ((substr($aFieldName,0,16) == 'AttributeValueID' && strlen($aFieldName) > 16) ||
						(substr($aFieldName,0,13) == 'ParentValueID' && strlen($aFieldName) > 13))
					{
						eval ('$anInstance->'.$aFieldName.' = $aFieldValue;');
					}
				}
				
				if ($blnIncludeAdditionalFields)
				{
					$anInstance->IncludeAdditionalFields = $blnIncludeAdditionalFields;
					// Recorre todos los elementos del POST y verifica si alguno de ellos es uno de los
					// campos adicionales para asignar su valor en el Arreglo de campos
					foreach ($aHTTPRequest->POST as $aFieldName => $aFieldValue)
					{
						//@JAPRWarning: $strEscapedFieldName = chr(39).addcslashes($aFieldName, "\0..\37\\\"\'\177..\377").chr(39);
						//@JAPRWarning: $strEscapedFieldName = chr(39).htmlspecialchars($aFieldName, ENT_QUOTES).chr(39);
						$strEscapedFieldName = chr(39).PartAWSAdd($aFieldName).chr(39);
						if (array_key_exists($strEscapedFieldName, $anInstance->FieldsValues))
						{
							$anInstance->FieldsValues[$strEscapedFieldName] = $aFieldValue;
						}
					}
				}
				
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMDimensionValue::NewInstance($aRepository, $aDimensionID, $aCubeID);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}

		$anInstance = null;
		if(array_key_exists("DimensionValueID", $aHTTPRequest->GET))
		{
			//Si llega hasta aquí, entonces solo es la carga normal de un valor existente
			$aDimensionValueID = $aHTTPRequest->GET["DimensionValueID"];
			$anInstance = BITAMDimensionValue::NewInstanceWithID($aRepository,$aDimensionValueID,$aDimensionID,$aCubeID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMDimensionValue::NewInstance($aRepository, $aDimensionID, $aCubeID);
			}
		}
		else
		{
			//Si llega hasta aquí, entonces solo estamos creando un nuevo valor normal
			$anInstance = BITAMDimensionValue::NewInstance($aRepository, $aDimensionID, $aCubeID);
		}
		return $anInstance;
	}
	
	function save($bSaveNULLsOnUpdate = true)
	{
		global $appSettings;
		global $strVersionFieldName;
		global $queriesLogFile;
		
		//@JAPR 2008-05-21: Agregada la asignación de multiples padres
		/*
		if (!is_null($this->ParentValueID) && $this->ParentKeyName != '')
		{
			$strParentKeyField = ', '.$this->ParentKeyName;
			$strUpdatedParentValueID = $strParentKeyField.' = ';
			if (trim($this->ParentValueID) === '')
			{
				$strParentKeyValueID = 'NULL';
			}
			else 
			{
				switch ($this->ParentKeyDataType)
				{
					case 1:
					case 2:
						$strParentKeyValueID = $this->Repository->DataADOConnection->Quote($this->ParentValueID);
						break;
					case 8:
					case 21:
					case 23:
					case 24:
						$strParentKeyValueID = (float) $this->ParentValueID;
						break;
					case 12:
					case 22:
						$strParentKeyValueID = $this->Repository->DataADOConnection->DBTimeStamp($this->ParentValueID);
						break;
					default:
						$strParentKeyValueID = (int) $this->ParentValueID;
						break;
				}
			}
			$strUpdatedParentValueID .= $strParentKeyValueID;
			$strParentKeyValueID = ', '.$strParentKeyValueID;
		}
		*/
		
		//Agrega los campos padre que fueron modificados para hacer el UPDATE
		$strParentKeyField = '';
		$strParentKeyValueID = '';
		$strUpdatedParentValueID = '';
		foreach ($this->ParentsInfo as $aParentConsecutive => $aParentInfo)
		{
			$strParentFieldName = $aParentInfo["parent_nom_fisicok_join"];
			eval ('$strParentValueID = @$this->ParentValueID'.$aParentConsecutive.';');
			if (!is_null($strParentValueID) && $strParentFieldName != '')
			{
				$strParentKeyField .= ', '.$strParentFieldName;
				$strUpdatedParentValueID .= ', '.$strParentFieldName.' = ';
				if (trim($strParentValueID) === '')
				{
					$strParentFieldValue = 'NULL';
				}
				else 
				{
					//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
					//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
					switch ($aParentInfo["parent_tipo_datok_join"])
					{
						case 1:
						case 2:
							$strParentFieldValue = $this->Repository->DataADOConnection->Quote($strParentValueID);
							break;
						case 8:
						case 21:
						case 23:
						case 24:
							$strParentFieldValue = (float) $strParentValueID;
							break;
						case 12:
						case 22:
							$strParentFieldValue = $this->Repository->DataADOConnection->DBTimeStamp($strParentValueID);
							break;
						default:
							$strParentFieldValue = (int) $strParentValueID;
							break;
					}
				}
				$strUpdatedParentValueID .= $strParentFieldValue;
				$strParentKeyValueID .= ', '.$strParentFieldValue;
			}
		}
		
		//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
		//Agrega los campos de dimensiones - atributo que fueron modificados para hacer el UPDATE
		$strAttributeKeyField = '';
		$strAttributeKeyValueID = '';
		$strUpdatedAttributeValueID = '';
		
		//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
		$arrCatMembersByDescripID = array();
		$arrImagesToDownload = array();
		if (getMDVersion() >= esvCatalogAttributeImage) {
			//Obtiene la colección de atributos de este catálogo que están marcados como imagen, ya que esos serán procesados al momento de
			//grabar el valor para descargar su imagen actualizada
			$sql = "SELECT CatalogID FROM SI_SV_Catalog WHERE ParentID = ".$this->cla_descrip;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS!==false && !$aRS->EOF) {
				$svCatalogID = (int)$aRS->fields["catalogid"];
				if ($svCatalogID > 0) {
					require_once("catalog.inc.php");
					$aCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $svCatalogID);
					if (!is_null($aCatalog)) {
						$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $svCatalogID); 
						if (!is_null($objCatalogMembersColl)) {
							foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
								if ($objCatalogMember->IsImage) {
									$arrCatMembersByDescripID[$objCatalogMember->ClaDescrip] = $objCatalogMember;
								}
							}
						}
					}
				}
			}
		}
		//@JAPR
		
		foreach ($this->AttributesInfo as $anAttributeConsecutive => $anAttributeInfo)
		{
			$strAttributeFieldName = $anAttributeInfo["attribute_nom_fisicok_bd"];
			eval ('$strAttributeValueID = @$this->AttributeValueID'.$anAttributeConsecutive.';');
			if (!is_null($strAttributeValueID) && $strAttributeFieldName != '')
			{
				$strAttributeKeyField .= ', '.$strAttributeFieldName;
				$strUpdatedAttributeValueID .= ', '.$strAttributeFieldName.' = ';
				if (trim($strAttributeValueID) === '')
				{
					$strAttributeFieldValue = 'NULL';
				}
				else 
				{
					//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
					//Si el atributo está marcado como imagen y es la versión que lo soporta, agrega la ruta de la imagen a la lista de
					//imagenes que se deben descargar, además de convertir dicha ruta a la que finalmente se usará para descargar la imagen si
					//es que se trataba de una ruta absoluta http, de lo contrario se considera que la imagen ya es relativa al servicio y se
					//deja igual como se reciba
					$strAttributeFieldValue = $strAttributeValueID;
					$intClaDescrip = (int) @$anAttributeInfo["attribute_cla_descrip"];
					if ($intClaDescrip > 0 && isset($arrCatMembersByDescripID[$intClaDescrip])) {
						$strAttributeFieldValue = str_replace("\\", "/", $strAttributeFieldValue);
						$arrURLData = @IdentifyAndAdjustAttributeImageToDownload($strAttributeFieldValue);
						if (!is_null($arrURLData)) {
							//En este caso la URL necesita algún ajuste y posiblemente se debe descargar la imagen, según el objeto regresado
							if (is_array($arrURLData)) {
								//Se debe descargar la imagen, además de actualizar el valor a grabar
								$strAttributeFieldValue = (string) @$arrURLData['path'];
								$arrImagesToDownload[(string) @$arrURLData['name']] = $arrURLData;
							}
							else {
								//En este caso simplemente se ajusta la URL a grabar
								$strAttributeFieldValue = $arrURLData;
							}
						}
					}
					
					switch ($anAttributeInfo["attribute_tipo_datok"])
					{
						case 1:
						case 2:
							$strAttributeFieldValue = $this->Repository->DataADOConnection->Quote($strAttributeFieldValue);
							break;
						case 8:
						case 21:
						case 23:
						case 24:
							$strAttributeFieldValue = (float) $strAttributeFieldValue;
							break;
						case 12:
						case 22:
							$strAttributeFieldValue = $this->Repository->DataADOConnection->DBTimeStamp($strAttributeFieldValue);
							break;
						default:
							$strAttributeFieldValue = (int) $strAttributeFieldValue;
							break;
					}
					//@JAPR
				}
				$strUpdatedAttributeValueID .= $strAttributeFieldValue;
				$strAttributeKeyValueID .= ', '.$strAttributeFieldValue;
			}
		}
		//@JAPR
		
		// Genera el string para grabar los campos adicionales para este miembro de la dimensión
		$strAdditionalFields = '';
		$strAdditionalFieldsValues = '';
		$strUpdatedAdditionalFieldsValues = '';
		if (($this->FromCaptureTable || $this->IncludeAdditionalFields) && $appSettings["DimensionFields"] == 1)
		{
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			/*
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				$aDimensionTableFieldCollection = BITAMDimensionTableFieldCollection::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto);
			}
			else
			{
			*/
				$aDimensionTableFieldCollection = BITAMDimensionTableFieldCollection::NewInstance($this->Repository, $this->consecutivo, $this->cla_concepto);
			//}
			//@JAPR
			foreach ($aDimensionTableFieldCollection as $aTableField)
			{
				//Solo procesa los campos adicionales que no tienen fórmula que calcular
				if (trim($aTableField->Formula) == '')
				{
					//@JAPRWarning: $strEscapedFieldName = chr(39).addcslashes($aTableField->Name, "\0..\37\\\"\'\177..\377").chr(39);
					//@JAPRWarning: $strEscapedFieldName = chr(39).htmlspecialchars($aTableField->Name, ENT_QUOTES).chr(39);
					$strEscapedFieldName = chr(39).PartAWSAdd($aTableField->Name).chr(39);
					$blnNullValue = is_null($this->FieldsValues[$strEscapedFieldName]);
					if (!$blnNullValue && $aTableField->isADate())
					{
						$blnNullValue = ($this->FieldsValues[$strEscapedFieldName] == '0000-00-00 00:00:00');
					}
					if (!$aTableField->isADate() && !$aTableField->mustBeQuoted())
					{
						if ((trim($this->FieldsValues[$strEscapedFieldName]) == '') || is_nan($this->FieldsValues[$strEscapedFieldName]))
						{
							$blnNullValue = true;
						}
					}
					
					if ($blnNullValue)
					{
						if ($bSaveNULLsOnUpdate)
						{
							//Si es un valor NULL, por lo menos lo ponemos en el UPDATE para asignar el NULL a la tabla
							$strUpdatedAdditionalFieldsValues .= ', '.$aTableField->Name.' = NULL';
						}
					}
					else
					{
						//Si no es NULL, hay que ajustar de acuerdo al tipo de dato
						$strAdditionalFields .= ', '.$aTableField->Name;
						$strUpdatedAdditionalFieldsValues .= ', '.$aTableField->Name.' = ';
						$strFieldValue = $this->FieldsValues[$strEscapedFieldName];
						if ($aTableField->mustBeQuoted())
						{
							$strFieldValue = $this->Repository->DataADOConnection->Quote($strFieldValue);
						}
						elseif($aTableField->isADate())
						{
							$strFieldValue = $this->Repository->DataADOConnection->DBTimeStamp($strFieldValue);
						}
						$strAdditionalFieldsValues .= ', '.$strFieldValue;
						$strUpdatedAdditionalFieldsValues .= $strFieldValue;
					}
				}
			}
		}
		
		$aDimensionValueID = $this->DimensionValueID;
		//@JAPR 2009-07-09: Validado que si el campo clave y descriptor son los mismos, no intente capturar la clave
		$bSameKeyDescField = strtolower($this->nom_fisico) == (strtolower($this->nom_fisicok));
		if($bSameKeyDescField)
		{
			$aDimensionValueID = $this->Description;
			$this->DimensionValueID = $aDimensionValueID;
		}

		$featuresDimension = BITAMDimension::getFeaturesDimension($this->Repository, $this->consecutivo, $this->cla_concepto);
		//@JAPR 2012-03-06: Agregado el indicador de versión por registro para descarga inteligente de catálogos
		//Ahora se verificará si es o no un catálogo para saber si puede o no agregar el campo versión y que valor asignarle
		$blnIsCatalog = false;
		$svCatalogID = 0;
		$intVersionNum = null;
	 	if(array_key_exists('is_catalog',$featuresDimension))
	 	{
	 		$blnIsCatalog = $featuresDimension["is_catalog"];
	 		if ($blnIsCatalog)
	 		{
	 			//Verifica si el propio objeto ya trae el dato con el número de versión, si es así ya no se necesita volver a obtener
	 			//Esto debería de asignarse cuando se encuentra en la ventana de captura de padres, ya que ese por ser un proceso masivo
	 			//debe asignar a todos los valores editados el mismo número de versión
	 			eval ('$intVersionNum = @$this->VersionID;');
	 			if (is_null($intVersionNum))
	 			{
					//Obtenemos el CatalogID a partir del cla_descrip que se recibio
					$sql = "SELECT CatalogID FROM SI_SV_Catalog WHERE ParentID = ".$this->cla_descrip;
					$aRS = $this->Repository->DataADOConnection->Execute($sql);
					if($aRS!==false && !$aRS->EOF)
					{
						$svCatalogID = (int)$aRS->fields["catalogid"];
						if ($svCatalogID > 0)
						{
							require_once("catalog.inc.php");
							$aCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $svCatalogID);
							if (!is_null($aCatalog))
							{
								$intVersionIndID = $aCatalog->saveVersionIndicator();
								$aCatalog->save();
								if ($intVersionIndID > 0)
								{
									$sql = 'select A.NOM_FISICO from SI_CPTO_ATRIB A, SI_INDICADOR B where A.CLA_CONCEPTO = B.CLA_CONCEPTO AND A.CONSECUTIVO = B.ID_CPTO_ATRIB AND A.CLA_CONCEPTO = '.$aCatalog->ModelID.' and B.CLA_INDICADOR = '.$intVersionIndID;
									$aRS = $this->Repository->ADOConnection->Execute($sql);
									if($aRS!==false && !$aRS->EOF)
									{
										$strVersionFieldName = trim((string) $aRS->fields['nom_fisico']);
										if ($strVersionFieldName != '')
										{
											$sql = 'SELECT '.$this->Repository->DataADOConnection->IfNull('MAX('.$strVersionFieldName.')', '0').' + 1 AS MaxVersion FROM '.$this->nom_tabla;
											$aRS = $this->Repository->DataADOConnection->Execute($sql);
											if($aRS!==false && !$aRS->EOF)
											{
												//Cada vez que se modifica un valor se actualiza el número de versión para que al descargar la
												//aplicación se obtengan los catálogos cuando por lo menos tengan un valor de una versión posterior
												//a la del dispositivo que está sincronizando
												$intVersionNum = (int) $aRS->fields['maxversion'];
											}
										}
									}
								}
							}
						}
					}
	 			}
	 			
	 			if (!is_null($intVersionNum) && isset($strVersionFieldName))
	 			{
					$strAdditionalFields .= ', '.$strVersionFieldName;
					$strAdditionalFieldsValues .= ', '.$intVersionNum;
					$strUpdatedAdditionalFieldsValues .= ', '.$strVersionFieldName.' = '.$intVersionNum;
	 			}
	 		}
	 	}
		//@JAPR
		
	 	if ($this->isNewObject())
		{
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			/*
			$blnIsCatalog = false;
			//$blnUseSubrrogateKey = ($featuresDimension["nom_fisicok_bd"] == $featuresDimension["nom_fisicok_join_bd"]);
		 	if(array_key_exists('is_catalog',$featuresDimension))
		 	{
		 		$blnIsCatalog = $featuresDimension["is_catalog"];
		 	}
		 	*/
		 	//@JAPR
			if (trim($aDimensionValueID) == '' && $this->created_by == 1 && !$bSameKeyDescField && !$blnIsCatalog)
			{
				$sql = 'SELECT '.
							$this->Repository->DataADOConnection->IfNull('MAX('.$this->nom_fisicok_bd.')', '0').' + 1 AS DimensionValueID FROM '.$this->nom_tabla;
				$aLogString="\r\n--------------------------------------------------------------------------------------";
				$aLogString.="\r\nBITAMDimensionValue->Save (Max): \r\n";
				$aLogString.=$sql;
				@updateQueriesLogFile($queriesLogFile,$aLogString);
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$this->nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$aDimensionValueID = (int)$aRS->fields["dimensionvalueid"];
				// Validamos que el primer elemento siempre sea 1
				if ($aDimensionValueID < 0)
				{
					$aDimensionValueID = 1;
				}
				$this->DimensionValueID = $aDimensionValueID;
			}
			//else if($this->nom_fisicok == $this->nom_fisico)
			//{
			//	$aDimensionValueID = $this->Description;
			//	$this->DimensionValueID = $aDimensionValueID;
			//}
			
			//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
			$strDimensionValueID = formatValueForSQL($this->Repository, $aDimensionValueID, $this->tipo_datok);
			$strDimensionValueDesc = formatValueForSQL($this->Repository, $this->Description, $this->tipo_dato);
			$strDimFields = '';
			$strDimValues = '';
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			if(!$bSameKeyDescField && !$blnIsCatalog)
			{
				$strDimFields .= $this->nom_fisicok_bd.", ";
				$strDimValues .= $strDimensionValueID.", ";
			}
			$strDimFields .= $this->nom_fisico_bd;
			$strDimValues .= $strDimensionValueDesc;
			
			//if($this->nom_fisicok == $this->nom_fisico)
			//{
			//	$sql = "INSERT INTO ".$this->nom_tabla." (".$this->nom_fisicok_bd.$strParentKeyField.$strAdditionalFields.") 
			//			VALUES (".$strDimensionValueID.$strParentKeyValueID.$strAdditionalFieldsValues.")";
			//}
			//else 
			//{
			//	$sql = "INSERT INTO ".$this->nom_tabla." (".$this->nom_fisicok_bd.", ".$this->nom_fisico_bd.$strParentKeyField.$strAdditionalFields.") 
			//			VALUES (".$strDimensionValueID.", ".$strDimensionValueDesc.$strParentKeyValueID.$strAdditionalFieldsValues.")";
			//}
			$sql = "INSERT INTO ".$this->nom_tabla." (".$strDimFields.$strParentKeyField.$strAttributeKeyField.$strAdditionalFields.") 
				VALUES (".$strDimValues.$strParentKeyValueID.$strAttributeKeyValueID.$strAdditionalFieldsValues.")";
			//@JAPR
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMDimensionValue->Save (Insert): \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			if($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$this->nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Aqui actualizamos el registro en el campo dimension cuando se trata de un catalogo de FORMS
			//para que tenga el mismo valor que la llave o ID
			if($blnIsCatalog)
			{
				//Despues de esta carga realizamos el update de la dimension dummy o agrupadora con el valor del ID(consecutivo)
				$SQLUpdate = "UPDATE ".$this->nom_tabla." SET ".$strDimFields." = ".$this->nom_tabla."KEY WHERE ".$this->nom_tabla."KEY <> 1";
				if($this->Repository->DataADOConnection->Execute($SQLUpdate) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$this->nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$SQLUpdate);
				}
				
				//@JAPR 2014-03-25: Corregido un bug, no estaba actualizando la versión de las encuestas que usan este catalogo
				if($svCatalogID > 0) {
					require_once("survey.inc.php");
					//Realizamos actualizacion de LastDate en las encuestas relacionadas con este catalogos
					//@JAPRDescontinuada en v6
					//A partir de v6 esta función quedó descontinuada, se dejará la referencia por si en algún proceso de compatibilidad se pensara reutilizar el servicio para definir formas viejas
					//La actualización de formas la realizará el DataSource directamente
					//BITAMSurvey::updateAllSurveyLastUserAndDateFromCatalog($this->Repository, $svCatalogID);
					//@JAPR
				}
				//@JAPR
			}
		}
		else
		{
			//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
			if($bSameKeyDescField)
			{
				$strDimensionValueID = formatValueForSQL($this->Repository, $this->OldDimensionValueID, $this->tipo_datok);
			}
			else
			{
				$strDimensionValueID = formatValueForSQL($this->Repository, $aDimensionValueID, $this->tipo_datok);
			}
			$strDimensionValueDesc = formatValueForSQL($this->Repository, $this->Description, $this->tipo_dato);

			$sql = "UPDATE ".$this->nom_tabla." SET ".
					$this->nom_fisico_bd." = ".$strDimensionValueDesc.$strUpdatedParentValueID.$strUpdatedAttributeValueID.$strUpdatedAdditionalFieldsValues.
				" WHERE ".$this->nom_fisicok_bd." = ".$strDimensionValueID;

			//@JAPR
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMDimensionValue->Save (Update): \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$this->nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//@JAPR 2010-03-16: Modificado para que se actualice el valor de NoAplica en la definición de la dimensión, si es que se trata de una
			//dimensión sin llave
			if($bSameKeyDescField)
			{
				//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
				/*
				if ($appSettings["UseGlobalDimensions"] == 1)
				{
					$aDimension = BITAMDimension::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto, false);
				}
				else
				{
				*/
					$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
				//}
				//@JAPR
				if(!is_null($aDimension))
				{
					if($this->OldDimensionValueID == $aDimension->NoApplyValue)
					{
						$sql = 'UPDATE SI_DESCRIP_ENC SET DEFAULT_KEY = '.$this->Repository->ADOConnection->Quote($this->Description).' '.
							'WHERE CLA_DESCRIP = '.$aDimension->cla_descrip;
						$aRS = $this->Repository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}
			}
		}
		
		if($blnIsCatalog && $svCatalogID > 0) {
			require_once("survey.inc.php");
			//Realizamos actualizacion de LastDate en las encuestas relacionadas con este catalogos
			//@JAPRDescontinuada en v6
			//A partir de v6 esta función quedó descontinuada, se dejará la referencia por si en algún proceso de compatibilidad se pensara reutilizar el servicio para definir formas viejas
			//La actualización de formas la realizará el DataSource directamente
			//BITAMSurvey::updateAllSurveyLastUserAndDateFromCatalog($this->Repository, $svCatalogID);
			//@JAPR
		}
		
		//Calcula la fórmula de los campos adicionales que pidieron ser guardados
		if ($this->FromCaptureTable || $this->IncludeAdditionalFields)
		{
			//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
			$formattedDimensionValueID = formatValueForSQL($this->Repository, $aDimensionValueID, $this->tipo_datok);
			//$formattedDimensionValueID = ((int)$this->tipo_datok === 7 || (int)$this->tipo_datok === 8) ? $aDimensionValueID : $this->Repository->DataADOConnection->Quote($this->DimensionValueID);
			//@JAPR
			//$FormulaInfo contendrá los datos necesarios para obtener los valores involucrados en la fórmula 
			//@JAPRWarning: Funcionalidad faltante
			if(false)
			{
				$FormulaInfo=BITAMFormulaInfo::NewInstance($this->Repository);
				//Dimensione que contiene los campos a calcular (como es la única, no hay combinaciones ni nada de eso)
				$FormulaInfo->DimensionsIDs=array($this->consecutivo);
				$FormulaInfo->Dimensions=array(BITAMDimension::NewInstanceWithID($this->Repository,$this->consecutivo,$this->cla_concepto,false));
				$FormulaInfo->DisplayedDimensionID=$this->consecutivo;
				//Valores seleccionados de las dimensiones
				$FormulaInfo->StrDimensionSelectedValues=$this->DimensionValueID;
				$FormulaInfo->DimensionCombinations=array($this->DimensionValueID);
				//JP:Crea un arreglo adicional para búsquedas por combinación
				$numDimensionCombinations=count($FormulaInfo->DimensionCombinations);
				for($i=0;$i<$numDimensionCombinations;$i++)
				{
					$FormulaInfo->DimensionCombinationsKeys[$FormulaInfo->DimensionCombinations[$i]]=$i;
				}
				
				//El periodo siempre se considera Mensual cuando existen modificadores, cuando no entonces es el acumulado total
				$periodID = 4;
				$FormulaInfo->PeriodID=$periodID;
				$FormulaInfo->StartDate=date("Y-m-d");
				$FormulaInfo->EndDate=date("Y-m-d");
				
				//Clave del Indicador del cual se grabó el presupuesto
				$FormulaInfo->IndicatorID=-1;
				//Cubo del Indicador del cual se grabó el presupuesto
				$FormulaInfo->CubeID=$this->cla_concepto;
				//Fechas
				$FormulaInfo->Dates=BITAMFormulaInfo::getDates($this->Repository,$FormulaInfo->StartDate, $FormulaInfo->EndDate, $this->cla_concepto, $periodID,false);
				$FormulaInfo->FormatedDates=BITAMFormulaInfo::getDates($this->Repository,$FormulaInfo->StartDate, $FormulaInfo->EndDate, $this->cla_concepto, $periodID,true);
				$FormulaInfo->setFormatedDatesKeys();
				$FormulaInfo->SharedDimIDs = $FormulaInfo->DimensionsIDs;
				$FormulaInfo->SharedDimensions = $FormulaInfo->DimensionCombinations;
			}
			//@JAPR
			
			foreach ($aDimensionTableFieldCollection as $aTableField)
			{
				//Solo procesa los campos adicionales que tienen fórmula que calcular
				if (trim($aTableField->Formula) != '')
				{
					//Tipo de Fórmula = 8 es la fórmula de los campos adicionales para dimensiones (no requiere combinaciones
					//de otras dimensiones ni BudgetID, así que solo generamos un arreglo de una combinación y una fecha)
					$elementIDs=array('descripID'=>$aTableField->cla_descrip,'fieldName'=>$aTableField->Name);
					//@JAPRWarning: Funcionalidad faltante
					if(false)
					{
						$fieldFormulaInstance = BITAMFormula::NewInstanceWithStrFormula($this->Repository,$aTableField->FormulaBD,true,-1,8,$elementIDs);
						$FormulaInfo->Formula = $fieldFormulaInstance;
						
						$FormulaResult=BITAMFormulaResult::NewInstance($this->Repository);
						$arrayOfFormulaResult=array();
						//Obtener los arreglos de valores para cada término de la fórmula del campo de dimensión
						$FormulaResult->getValuesArraysOfFormula($FormulaInfo);
						//Obtener el arreglo de valores ($FormulaResult->ResultFormulaArray) para el campo de dimensión
						$FormulaResult->evaluateFormula();
						$arrayOfFormulaResult[]=$FormulaResult;
						
						//Graba el valor del campo calculado en la tabla de dimensión
						$aFieldValue = @$FormulaResult->ResultFormulaArray[0][0];
					}
					$aFieldValue = null;
					//@JAPR
					if (!is_null($aFieldValue))
					{
						if ($aTableField->mustBeQuoted())
						{
							$aFieldValue = $this->Repository->DataADOConnection->Quote($aFieldValue);
						}
						elseif($aTableField->isADate())
						{
							$aFieldValue = $this->Repository->DataADOConnection->DBTimeStamp($aFieldValue);
						}
						
						$sql = 'UPDATE '.$this->nom_tabla.' SET '.$aTableField->Name.' = '.$aFieldValue.' '.
							'WHERE '.$this->nom_fisicok_bd.' = '.$formattedDimensionValueID;
						$aLogString="\r\n--------------------------------------------------------------------------------------";
						$aLogString.="\r\nBITAMDimensionValue->Save (Update field): \r\n";
						$aLogString.=$sql;
						@updateQueriesLogFile($queriesLogFile,$aLogString);
						$aRS = $this->Repository->DataADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die("(".__METHOD__.") ".translate("Error updateing")." ".$this->nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}
			}
		}
		
		//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
		//Se descargan todas las imagenes que fueran definidas en este valor, no importa si estaba actualizandolo o insertándolo (ya que no
		//podemos determinar si ha cambiado o no la imagen de la URL especificada, si fuera una imagen del propio server entonces no vendría
		//como parte de este array)
		$strErrors = '';
		foreach ($arrImagesToDownload as $fileName => $objFileData) {
			//Antes de hacer la copia, primero crea la ruta destino
			$path = str_ireplace($fileName, '', $objFileData['path']);
			if (!file_exists($path)) {
				@mkdir($path, null, true);
			}
			
			if (!@copy($objFileData['url'], $objFileData['path'])) {
				$errors = error_get_last();
				$strErrors .= "\r\n({$fileName}) ".$errors['message'];
			}
		}
		
		if ($strErrors != '') {
			die("(".__METHOD__.") ".$strErrors);
		}
		//@JAPR
		
		$this->newObject = false;
		return $this;
	}

	// Graba un nuevo valor en la combinación de dimensiones especificada para las fechas definidas en el presupuesto,
	// de forma que puedan ser obtenidas al consultarlo el cubo nuevamente
	function insertFactTable()
	{
		global $queriesLogFile;
		
		$aRepository = $this->Repository;
		$aCube = BITAMCube::NewInstanceWithID($aRepository, $this->cla_concepto);
		$existDateField=BITAMGlobalInstance::GetExistsFieldOnTable($aCube->nom_tabla,$aCube->fecha);
		if (is_null($existDateField))
		{
			//Verificar si existe el campo fecha en la tabla de hechos
			$sql = "SELECT " .$aCube->fecha. " FROM " .$aCube->nom_tabla. " WHERE 1=2";
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMDimensionValue->InsertFactTable: \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				//El campo fecha no exite
				$existDateField=false;
			}
			else
			{
				$existDateField=true;
			}
			BITAMGlobalInstance::AddExistsFieldOnTable($aCube->nom_tabla,$aCube->fecha,$existDateField);
		}

		// Solo carga las dimensiones usadas en el presupuesto
		$dimensionIDs = explode('|', $this->POSTDimensions);
		$dimensions = BITAMDimensionCollection::NewInstance($aRepository, $dimensionIDs, $this->cla_concepto, true, false, false);
		$numDimensions = count($dimensionIDs);
		$noSelectedDimensions = BITAMDimensionCollection::NewInstance($aRepository, $dimensionIDs, $this->cla_concepto, false, false, false);
		$numNoSelectedDimensions=count($noSelectedDimensions->Collection);
		$descriptKeysCollection=BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository,$this->cla_concepto);
		
		//ISUAREZ 26-OCTUBRE-2009
		//Obtención de las ultimas posiciones de cada jerarquia presente en la colección de dimensiones
		//si es que existe una jerarquía y si coincide el nombre de tabla dentro de los elementos de
		//la jerarquía, es decir, que las dimensiones de la jerarquia tenga el mismo nombre de tabla
		//como es el caso del modelo financiero para las dimensiones nivel1, nivel2, ......, cuentaAfectable
		//tienen el mismo nombre de tabla SI_EKT_MasterAccountsLevels, si estamos presupuestando 
		//Nivel1, Nivel2 y Nivel3 se sabe que el valor se guardará en FinancialAccountID de la 
		//FactAccounts pero no se sabe que valor de Nivel1, Nivel2 y Nivel3 se va a guardar,
		//y como esta actualmente EktosBS es que procesa la primera dimensión Nivel1 y guarda el valor de
		//esta cuando lo correcto en el modelo financiero es que se guarde el último nivel que se 
		//esta presupuestando en este caso Nivel3, hasta ahora este caso solamente se ha presentado en el
		//modelo financiero para la presupuestación de cuentas maestras.
		
		$hierarchyLastPosition = array();
		
		for ($i=0;$i<$numDimensions;$i++) 
		{
			if($dimensions->Collection[$i]->HasHierarchy==true)
			{
				$hierarchyTable = $dimensions->Collection[$i]->nom_tabla;
				$hierarchyID = (int)$dimensions->Collection[$i]->HierarchyID;
				$hierarchyConsecutive = (int)$dimensions->Collection[$i]->ConsecutivoJerarquia;
				
				if(!isset($hierarchyLastPosition[$hierarchyID]))
				{
					$hierarchyLastPosition[$hierarchyID] = array();
				}
				
				if(!isset($hierarchyLastPosition[$hierarchyID][$hierarchyTable]))
				{
					$hierarchyLastPosition[$hierarchyID][$hierarchyTable] = $hierarchyConsecutive;
				}
				else 
				{
					$tempHierarchyConsecutive = $hierarchyLastPosition[$hierarchyID][$hierarchyTable];
					
					if($hierarchyConsecutive>$tempHierarchyConsecutive)
					{
						$hierarchyLastPosition[$hierarchyID][$hierarchyTable] = $hierarchyConsecutive;
					}
				}
			}
		}

		//$metaColumns=$aRepository->DataADOConnection->MetaColumns($aCube->nom_tabla);
		$metaColumns=$aRepository->GetTableSchema($aCube->nom_tabla,true);
		$dateKeyField=getJoinField($aCube->dim_periodo,"",$descriptKeysCollection, TRUE);
		$dateFieldType=$metaColumns[strtoupper($dateKeyField)]->type;
	
		//Nombre de los campos de las dimensiones seleccionadas
		$dimsString="";
		$fieldsInSQL = array();
		//Valores de las claves de las dimensiones a insertar en la tabla de hechos
		//$dimensionValues: se obtienen de la cadena separadas por | (ej. "4|1|1")
		$dimensionValues = explode('|', $this->POSTCombinations);
		//Contiene la posición dentro de $dimensionValues de cada dimensión, ya que se indexa por su ID
		$dimensionPositions = array_flip($dimensionIDs);
		//Reemplazamos el valor actual de la combinación por el valor recien capturado
		$dimensionValues[$dimensionPositions[$this->consecutivo]] = $this->DimensionValueID;
		$strDimensionValues="";
		for ($i=0;$i<$numDimensions;$i++) 
		{
			$blnInserValue = true;
			
			$aDimension = $dimensions->Collection[$i];
			$strDimKeyValue = $dimensionValues[$i];
			if($aDimension->tipo_datok==2 || $aDimension->tipo_datok==1)
			{
				$strDimValue = $aRepository->DataADOConnection->Quote($strDimKeyValue);
			}
			else
			{
				$strDimValue = $strDimKeyValue;
			}

			if($aDimension->dependsOnDimension==0)
			{
				//ISUAREZ 26-OCTUBRE-2009
				//Se verifica si la dimension tiene jerarquia y se relaciona con otra dimension de la coleccion
				//por medio de la misma jerarquia y de la misma tabla de catalogo
				//si se da este caso (como en los niveles de las cuentas maestras) se descartan aquellas 
				//dimensiones que no estén en el nivel más bajo de la jerarquía
				if($aDimension->HasHierarchy==true)
				{
					$hierarchyTable = $aDimension->nom_tabla;
					$hierarchyID = (int)$aDimension->HierarchyID;
					$hierarchyConsecutive = (int)$aDimension->ConsecutivoJerarquia;
				
					if($hierarchyLastPosition[$hierarchyID][$hierarchyTable]!=$hierarchyConsecutive)
					{
						$blnInserValue = false;
					}
				}
				
				if($blnInserValue)
				{
					$joinFieldName=getJoinFieldByTableName($aDimension->nom_tabla, $aDimension->nom_fisicok_bd, $descriptKeysCollection, TRUE, $keyEqualToCatJoin);
					if($joinFieldName!==null && in_array($joinFieldName, $fieldsInSQL)==false)
					{
						if($dimsString!="")
						{
							$dimsString.= ",";
						}
						$dimsString.=$joinFieldName;
	
						$fieldsInSQL[]=$joinFieldName;
	
						if ($strDimensionValues!="")
						{
							$strDimensionValues.=" , ";
						}
						
						if($keyEqualToCatJoin==true)
						{
							$strDimensionValues.=$strDimValue." ";
						}
						else
						{
							//Si el campo clave de la dimensión no es igual al campo join de su catálogo, entonces se busca
							//el valor correspondiente al campo join con base en el valor de la clave
							$keyValue = $strDimKeyValue;
							$dimJoinValue = BITAMDimension::getDimJoinValueByKeyValue($aRepository, $descriptKeysCollection, $aDimension->nom_tabla, $aDimension->nom_fisicok_bd, $aDimension->tipo_datok, $keyValue);
							
							$strDimensionValues.=$dimJoinValue." ";
						}
					}
				}
			}
		}	
		
		//Nombre de los campos de las dimensiones no seleccionadas a las q se les dará valor de No Aplica
		for ($i=0;$i<$numNoSelectedDimensions;$i++) 
		{
			$aDimension = $noSelectedDimensions->Collection[$i];

			if($aDimension->tipo_datok==2 || $aDimension->tipo_datok==1)
			{
				$strDimValue=$aRepository->DataADOConnection->Quote($aDimension->NoApplyValue);
			}
			else
			{
				$strDimValue=$aDimension->NoApplyValue;			
			}

			if($aDimension->dependsOnDimension==0)
			{
				$joinFieldName=getJoinFieldByTableName($aDimension->nom_tabla, $aDimension->nom_fisicok_bd, $descriptKeysCollection, TRUE, $keyEqualToCatJoin);
				if($joinFieldName!==null && in_array($joinFieldName, $fieldsInSQL)==false)
				{
					if($dimsString!="")
					{
						$dimsString.= ",";
					}
					$dimsString.=$joinFieldName;

					$fieldsInSQL[]=$joinFieldName;

					if ($strDimensionValues!="")
					{
						$strDimensionValues.=" , ";
					}
					
					if($keyEqualToCatJoin==true)
					{
						$strDimensionValues.=" ".$strDimValue." ";
					}
					else
					{
						//Si el campo clave de la dimensión no es igual al campo join de su catálogo, entonces se busca
						//el valor correspondiente al campo join con base en el valor de la clave
						$keyValue = $aDimension->NoApplyValue;
						$dimJoinValue = BITAMDimension::getDimJoinValueByKeyValue($aRepository, $descriptKeysCollection, $aDimension->nom_tabla, $aDimension->nom_fisicok_bd, $aDimension->tipo_datok, $keyValue);
						
						$strDimensionValues.=$dimJoinValue." ";
					}
				}
			}
		}
		
		//Verifica la tabla correspondiente a dim_periodo, si ya existe la fecha retorna su clave,
		//si no existe la fecha la agrega y retorna su clave
		$joinDimPeriodo=getJoinFieldByTableName($aCube->dim_periodo, "", $descriptKeysCollection, TRUE);
		$strNoUsedIndicatorsNames = '';
		$strNoUsedIndicatorsValues = '';
		$sql = 'SELECT NOM_FISICO, VALOR_DEFAULT FROM SI_CPTO_ATRIB WHERE CLA_CONCEPTO = '.$this->cla_concepto;
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			//No considero que valga la pena detener la ejecución por este error, esto es un plus, no es requerido para grabar
		}
		else 
		{
			while (!$aRS->EOF)
			{
				$strIndicatorField = $aRS->fields['nom_fisico'];
				$indicatorValue = $aRS->fields['valor_default'];
				if (!is_null($indicatorValue))
				{
					// Este es un indicador Base que debe grabarse pues tiene un default
					$strNoUsedIndicatorsNames .= ', '.$strIndicatorField;
					$strNoUsedIndicatorsValues .= ', '.$indicatorValue;
				}
				$aRS->MoveNext();
			}
		}
		
		$arrDates = explode('|', $this->POSTDates);
		$numDates = count($arrDates);
		for ($intCont=0; $intCont < $numDates; $intCont++)
		{
			$date = $arrDates[$intCont];
			$dateKey="";
			$dateKey=$this->checkTimeTable($date);
			
			$sqlInsert="INSERT INTO ".$aCube->nom_tabla. " ( "; 
			$sqlInsert.=$dimsString;
			
			if ($existDateField)
			{
				if($joinDimPeriodo!=$aCube->fecha)
				{
					$sqlInsert.=" , ". $aCube->fecha;
				}
			}
		
			//Campo clave de la tabla de periodos (especificada en dim_periodo)
			$sqlInsert.= " , " . $joinDimPeriodo;
			
			//No se insertan valores para los indicadores, solo el default para aquellos que lo tengan
			/*
			foreach ($indicators as $anIndicator)
			{
				$sqlInsert.= " , ". $anIndicator->nom_campo_bd;
			}
			*/
			$sqlInsert.= $strNoUsedIndicatorsNames." ) VALUES (";
			$sqlInsert.= $strDimensionValues;
			
			if ($existDateField)
			{	
				if($joinDimPeriodo!=$aCube->fecha)
				{
					$sqlInsert.=", ". $aRepository->DataADOConnection->DBTimeStamp($date);
				}
			}
			
			if($dateFieldType=='T' || $dateFieldType=='D')
			{
				$sqlInsert.=" , ".$aRepository->DataADOConnection->DBTimeStamp($dateKey);
			}
			else
			{
				$sqlInsert.=" , ".$dateKey;
			} 
			
			//No se insertan valores para los indicadores, solo el default para aquellos que lo tengan
			/*
			foreach ($indicators as $anIndicator)
			{
				$indicatorValue = $arrAllCombinations[$sCombination]['ind'.$anIndicator->cla_indicador][$aFormattedDate]['TOSAVE'];
				if (is_null($indicatorValue))
				{
					$sqlInsert.=" , NULL";
				}
				else 
				{
					$sqlInsert.=" , ".$indicatorValue;
				}
			}
			*/
			
			$sqlInsert .= $strNoUsedIndicatorsValues.')';
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMDimensionValue->InsertFactTable (Insert): \r\n";
			$aLogString.=$sqlInsert;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			if ($aRepository->DataADOConnection->Execute($sqlInsert) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlInsert);
			}
		}
		
		return;
	}

	// A partir de este valor, verifica si la dimensión es un padre de SnowFlake y en caso de serlo, actualiza las refencias
	// de las dimensiones de las que depende hasta llegar a la tabla de hechos para que la jerarquía quede completa. 
	// El valor de regreso puede ser True en caso de que el valor capturado sea de una dimensión directamente en la tabla de
	// hechos (lo cual significa que se pudieran generar combinaciones a partir de este nuevo elemento), o False si pertenecía
	// a un padre dentro de un SnowFlake, por lo cual grabar este nuevo valor no afecta en nada ya que la combinación debió
	// existir previamente por lo menos para las dimensiones mas cercanas a la tabla de hechos o no hubiera llegado aqui 
	// La referencia solo puede ser actualizada en las dimensión hija inmediata de esta en el SnowFlake, ya que si no existiera
	// el resto de la jerarquía hasta llegar a la tabla de hechos no hubiera llegado a este punto
	function updateParentReference()
	{
		global $appSettings;
		
		$dimensionIDs = explode('|', $this->POSTDimensions);
		$dimensions = BITAMDimensionCollection::NewInstance($this->Repository, $dimensionIDs, $this->cla_concepto, true, false, false);
		$numDimensions = count($dimensions->Collection);
		$displayedDimension = null;
		for ($i=0; $i < $numDimensions; $i++)
		{
			if ($dimensions->Collection[$i]->consecutivo == $this->consecutivo)
			{
				$displayedDimension = $dimensions->Collection[$i];
				break;
			}
		}
		
		// No se encontró la dimensión capturada (no debería ser posible)
		if (is_null($displayedDimension))
		{
			return false;
		}
		
		// No tiene un hijo de SnowFlake, así que regresamos True
		if ($displayedDimension->dependsOnDimension == 0)
		{
			return true;
		}
		
		// En este caso si hay una dimensión de la que se depende para llegar a la tabla de hechos, así que verificamos si
		// su valor ya tiene un padre asignado
		$childDimension = null;
		for ($i=0; $i < $numDimensions; $i++)
		{
			if ($dimensions->Collection[$i]->consecutivo == $displayedDimension->dependsOnDimension)
			{
				$childDimension = $dimensions->Collection[$i];
				break;
			}
		}
		
		// No se encontró la dimensión hija (no debería ser posible)
		if (is_null($childDimension))
		{
			return false;
		}
		
		// Identificamos el valor enviado para esta dimensión
		$dimensionValues = explode('|', $this->POSTCombinations);
		//Contiene la posición dentro de $dimensionValues de cada dimensión, ya que se indexa por su ID
		$dimensionPositions = array_flip($dimensionIDs);
		$aChildValue = @$dimensionValues[$dimensionPositions[$childDimension->consecutivo]];
		// No se encontró el valor de la dimensión hija dentro de la combinación enviada, simplemente salimos
		if (is_null($aChildValue))
		{
			return false;
		}
		
		
		$aDimensionValueInstance = BITAMDimensionValue::NewInstanceWithID($this->Repository, $aChildValue, $childDimension->consecutivo, $childDimension->cla_concepto);
		if (is_null($aDimensionValueInstance))
		{
			return false;
		}
		
		// Varificamos si el miembro de la dimensión hija ya tiene un padre, si es así nuevo elemento no puede ser asignado como padre o
		// se sobreescribiría al actual, por tanto salimos sin actualizar nada
		$aDimensionValueInstance->ParentValueID = null;
		foreach ($aDimensionValueInstance->ParentsInfo as $aParentConsecutive => $aParentInfo)
		{
			if ($this->consecutivo == $aParentConsecutive)
			{
				//Leemos el padre actualmente asignado para el valor de esa dimensión hija y al terminar salimos del ciclo
				$strParentValueID = null;
				eval ('$strParentValueID = @$aDimensionValueInstance->ParentValueID'.$aParentConsecutive.';');
				$aDimensionValueInstance->ParentValueID = $strParentValueID;
				break;
			}
		}
		
		if (!is_null($aDimensionValueInstance->ParentValueID))
		{
			return false;
		}
		
		// En este punto, ya tenemos identificado el valor sobre el cual asignaremos el nuevo padre, simplemente actualizamos
		// la información de la instancia y la grabamos
		/*
		$aDimensionValueInstance->ParentKeyDataType = $this->tipo_datok;
		$aDimensionValueInstance->ParentKeyName = $this->nom_fisicok_bd;
		
		$featuresDimension = BITAMDimension::getFeaturesDimension($this->Repository, $childDimension->consecutivo, $childDimension->cla_concepto);
		if (!is_null($featuresDimension))
		{
			//NO tenemos referencia al tipo de dato del Join, asumiremos que es el mismo de la llave pero no siempre es así
			//$anInstance->ParentKeyDataType = $featuresDimension["parent_tipo_datok"];
			$aDimensionValueInstance->ParentKeyName = $featuresDimension["parent_nom_fisicok_join"];
		}

		$aDimensionValueInstance->ParentValueID = $this->DimensionValueID;
		*/
		eval ('$aDimensionValueInstance->ParentValueID'.$this->consecutivo.' = '.$this->DimensionValueID);
		
		$aDimensionValueInstance->save();
	}
	
	function checkTimeTable($date)
	{
		global $queriesLogFile;
		
		$dateKey=BITAMGlobalInstance::GetDateIDFromCube($this->cla_concepto,$date);
		if(!is_null($dateKey))
		{
			return $dateKey;
		}

		$aRepository = $this->Repository;
		$descriptKeysCollection=BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository,$this->cla_concepto);
		$periodDims=BITAMDimensionCollection::NewInstancePeriodDims($aRepository, $this->cla_concepto, false);
		$aCube = BITAMCube::NewInstanceWithID($this->Repository, $this->cla_concepto);
		$numPeriodDims = count($periodDims->Collection);
		
		$formatedDate = unformatDateTime($date, $aCube->fmt_fecha);
		$dateTimeStamp=$aRepository->DataADOConnection->DBTimeStamp($formatedDate);
		
		if($_SESSION["PAFBM_Mode"]==1 && strtoupper($aCube->dim_periodo)==strtoupper("DimTime"))
		{
			$processingDate = trim(substr($formatedDate, 0, 10));
			$processingDate = str_replace("-", "", $processingDate);
			$processingDate = str_replace("/", "", $processingDate);
		
			$intProcessingDate = (int)$processingDate;
		}
		
		$dateKey=-1;
		$dateKeyField=getJoinField($aCube->dim_periodo,"",$descriptKeysCollection, FALSE);
		
		$sql = "SELECT ".$dateKeyField." FROM ".$aCube->dim_periodo." WHERE ".$aCube->fecha." = ".$aRepository->DataADOConnection->DBTimeStamp($date);
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMDimensionValue->CheckTimeTable: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{	
			//Ya existe esa fecha en la tabla especificada en dim_periodo, se retorna su clave
			$dateKey =  $aRS->fields[strtolower($dateKeyField)];
			BITAMGlobalInstance::AddDateIDFromCube($aCube->cla_concepto,$date,$dateKey);
			return $dateKey;
		}
		else 
		{ 
			//No existe esa fecha en la tabla especificada en dim_periodo, se agrega y se retorna su clave
			
			//$metaColumns=$aRepository->DataADOConnection->MetaColumns($aCube->dim_periodo);
			$metaColumns=$aRepository->GetTableSchema($aCube->dim_periodo,true);
			$dateFieldType=$metaColumns[strtoupper($dateKeyField)]->type;
			
			if($dateFieldType=='T' || $dateFieldType=='D')
			{
				//formato universal
				//"{ts '2003-01-01 00:00:00'}"
			
				$sql = "INSERT INTO ".$aCube->dim_periodo."(".$aCube->fecha;
							
				for($i=0;$i<$numPeriodDims;$i++)
				{
					$sql.= ", ".$periodDims->Collection[$i]->nom_fisicok_bd;
				}

				if($_SESSION["PAFBM_Mode"]==1 && strtoupper($aCube->dim_periodo)==strtoupper("DimTime"))
				{
					$sql.= ", DateKey";
				}
				
				$sql.= ") VALUES (".$dateTimeStamp;
				
				for($i=0;$i<$numPeriodDims;$i++)
				{	
					$formatedDateField=formatADateTime($formatedDate, $periodDims->Collection[$i]->fmt_periodo);
					$formatedDateField=$aRepository->DataADOConnection->Quote($formatedDateField);
					$sql.= ", " .$formatedDateField;
				}
				
				if($_SESSION["PAFBM_Mode"]==1 && strtoupper($aCube->dim_periodo)==strtoupper("DimTime"))
				{
					$sql.= ", ".$intProcessingDate;
				}

				$sql.=")";
				$aLogString="\r\n--------------------------------------------------------------------------------------";
				$aLogString.="\r\nBITAMDimensionValue->CheckTimeTable (Insert Date): \r\n";
				$aLogString.=$sql;
				@updateQueriesLogFile($queriesLogFile,$aLogString);
				if ($aRepository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				BITAMGlobalInstance::AddDateIDFromCube($aCube->cla_concepto, $date, $formatedDate);
				
				return($formatedDate);
			}
			else 
			{
				$sql =  "SELECT ".$aRepository->DataADOConnection->IfNull("MAX(".$dateKeyField.")", "0")." + 1 AS DateKey"." FROM ".$aCube->dim_periodo;
	
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
	
				$dateKey = $aRS->fields["datekey"];
				
				//formato universal
				//"{ts '2003-01-01 00:00:00'}"
			
				$sql = "INSERT INTO ".$aCube->dim_periodo."(".$dateKeyField.",".$aCube->fecha;
							
				for($i=0;$i<$numPeriodDims;$i++)
				{
					$sql.= ", ".$periodDims->Collection[$i]->nom_fisicok_bd;
				}
				
				$sql.= ") VALUES (".$dateKey.", ".$aRepository->DataADOConnection->DBTimeStamp($date);
				
				for($i=0;$i<$numPeriodDims;$i++)
				{	
					$formatedDateField=formatADateTime($formatedDate, $periodDims->Collection[$i]->fmt_periodo);
					$formatedDateField=$aRepository->DataADOConnection->Quote($formatedDateField);
					$sql.= ", ".$formatedDateField;
				}
				
				$sql.=")";
				$aLogString="\r\n--------------------------------------------------------------------------------------";
				$aLogString.="\r\nBITAMDimensionValue->CheckTimeTable (Insert DateID): \r\n";
				$aLogString.=$sql;
				@updateQueriesLogFile($queriesLogFile,$aLogString);
				if ($aRepository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				BITAMGlobalInstance::AddDateIDFromCube($aCube->cla_concepto, $date, $dateKey);
				
				return($dateKey);
			}
		}
	}
	
	function remove()
	{
		global $appSettings;
		
		//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
		$strDimensionValueID = formatValueForSQL($this->Repository, $this->DimensionValueID, $this->tipo_datok);
		$sql = "DELETE FROM ".$this->nom_tabla." 
			WHERE ".$this->nom_fisicok_bd." = ".$strDimensionValueID;
		/*
		$sql = "DELETE FROM ".$this->nom_tabla." 
			WHERE ".$this->nom_fisicok_bd." = ".( ((int)$this->tipo_datok === 7 || (int)$this->tipo_datok === 8) ? $this->DimensionValueID : $this->Repository->DataADOConnection->Quote($this->DimensionValueID) );
		*/
		//@JAPR
		global $queriesLogFile;
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMDimensionValue->Remove: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$this->nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		//En caso de que se esté procesando en forma global, no se puede saber que hijos existen así que termina después de eliminar el valor
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			return $this;
		}
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$aDimension = BITAMDimension::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto, false);
		}
		else
		{
		*/
			$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
		//}
		//@JAPR
		if(!is_null($aDimension))
		{
			if(count($aDimension->ChildParDescriptor) > 0)
			{
				$aChildDimension = BITAMDimension::NewInstanceWithID($this->Repository, $aDimension->ChildParDescriptor[0], $this->cla_concepto, false);
				if(!is_null($aChildDimension))
				{
					$strDimValue = formatValueForSQL($this->Repository, $aDimension->NoApplyValue, $aDimension->tipo_datok);
					$descriptKeysCollection = BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($this->Repository,$this->cla_concepto);
					$joinFieldName = getJoinFieldByTableName($aDimension->nom_tabla, $aDimension->nom_fisicok_bd, $descriptKeysCollection, TRUE);
					$sql = "UPDATE ".$aChildDimension->nom_tabla." SET ".$joinFieldName." = ".$strDimValue." WHERE ".$joinFieldName." = ".$strDimensionValueID;
					$aLogString="\r\n--------------------------------------------------------------------------------------";
					$aLogString.="\r\nBITAMDimensionValue->Remove (Update parents): \r\n";
					$aLogString.=$sql;
					@updateQueriesLogFile($queriesLogFile,$aLogString);
					$aRS = $this->Repository->DataADOConnection->Execute($sql);
				}
			}
		}
		
		return $this;
	}
	
	function get_FormIDFieldName()
	{
		return 'DimensionValueID';
	}
	
	function get_QueryString()
	{
		$strFromCaptureTable = '';
		if ($this->FromCaptureTable)
		{
			$strFromCaptureTable .= '&FromCaptureTable=1';
			if ($this->isNewObject())
			{
				// Si es un objeto nuevo, significa que se deben grabar en secuencia nuevos objetos así que la acción es Save
				$strFromCaptureTable .= '&Action=Save';
			}
			else
			{
				// Si es desde la ventana de captura pero no es un objeto nuevo, significa que estamos editando un valor,
				// así que se tiene que cambiar la acción a Update
				$strFromCaptureTable .= '&Action=Update';
			}
		}
		if ($this->IncludeAdditionalFields)
		{
			$strFromCaptureTable .= '&IncludeAdditionalFields=1';
		}
		
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=DimensionValue&CubeID=".$this->cla_concepto.'&DimensionID='.$this->consecutivo.$strFromCaptureTable;
		}
		else
		{
			//@JAPR 2010-02-19: Agregada la corrección para Ids tipo texto
			$strDimensionValueID = $this->DimensionValueID;
			if ($this->tipo_datok == 1 || $this->tipo_datok == 2)
			{
				$strDimensionValueID = urlencode($this->DimensionValueID);
			}
			return "BITAM_PAGE=DimensionValue&CubeID=".$this->cla_concepto.'&DimensionID='.$this->consecutivo.'&DimensionValueID='.$strDimensionValueID.$strFromCaptureTable;
		}
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			if ($this->ForParentAssignment)
			{
				return translate("Tabular entry");
			}
			else
			{
				$strDimensionTitle = translate("Value");
				if ($this->FromCaptureTable)
				{
					$strDimensionTitle = $this->nom_logico;
				}
				return translate("New")." ".$strDimensionTitle;
			}
		}
		else
		{  
			return translate("Value");
		}
	}

	function isNewObject()
	{
		return (is_null($this->DimensionValueID) || $this->newObject);
	}

	function get_Parent()
	{
		global $appSettings;
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			if ($this->cla_descrip > 0)
			{
				return BITAMDimension::NewInstanceWithDescripID($this->Repository,$this->cla_descrip,$this->cla_concepto,false);
			}
			else 
			{
				return $this->Repository;
			}
		}
		else
		{
		*/
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			if (($this->cla_concepto > 0 || $appSettings["UseGlobalDimensions"] == 1) && $this->consecutivo > 0)
			{
				return BITAMDimension::NewInstanceWithID($this->Repository,$this->consecutivo,$this->cla_concepto,false);
			}
			else 
			{
				return $this->Repository;
			}
		//}
		//@JAPR
	}
	
	function canRemove($aUser)
	{
		global $appSettings;
		
		//@JAPR 2010-07-13: Modificada temporalmente la validación para NO permitir remover valores de dimensiones mientras no se determine como
		//actuar en la tabla de hechos del cubo para no causar inconsistencias
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
		if (!is_null($aDimension))
		{
			//De una dimensión catálogo si se puede eliminar un registro, excepto si este es el de No Aplica con Id == 1
			if ($aDimension->isCatalog && (int) $this->DimensionValueID != 1)
			{
				return true;
			}
		}
		return false;
		
		//@JAPR 2010-01-27: Modificada la restricción para remover valores a la siguiente regla:
		// - Si la dimensión es un Atributo, NO se pueden remover valores ya que hay múltiples registros que los contienen y esto afectaría 
		//		a la dimensión primaria del catálogo
		// - Si la dimensión está ligada directamente al cubo y tiene llave Subrogada, ya que posiblemente tenga una Foreign Key y no nos dejaría
		//		de todas formas
		// - No recuerdo por qué desde la tabla  de captura no se pueden remover valores, posiblemente porque regresaría a una ventana donde la
		//		combinación mostrada ya no sería válida
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$aDimension = BITAMDimension::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto, false);
		}
		else
		{
		*/
			$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
		//}
		//@JAPR
		if ($this->FromCaptureTable || is_null($aDimension) || $aDimension->IsAttribute || ($aDimension->dependsOnDimension == 0 && ($aDimension->nomfisicok_descripkeys != '' && $aDimension->nom_fisicok_bd != $aDimension->nomfisicok_descripkeys)))
		{
			return false;
		}
		
		//@JAPR 2010-03-09: Agregada una validación mas, si se trata del valor de No aplica, no se puede remover
		if ($aDimension->NoApplyValue == $this->DimensionValueID)
		{
			return false;
		}
		return true;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		global $appSettings;
		
		//Verificar si el campo clave de la dimensión es igual 
		//al campo de la descripcion de la dimensión
		//$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
		//
		//if($aDimension->nom_fisicok != $aDimension->nom_fisico)
		//{
		//	return true;
		//}
		//else 
		//{
		//	return false;
		//}
		//@JAPR 2010-01-27: Modificada la restricción de edición de valores a la siguiente regla:
		// - Si la dimensión Es un Atributo, se pueden editar porque no afectan a la tabla de hechos
		// - Si la dimensión tiene Llave diferente del Descriptor, se puede editar sin problema
		// - Si la dimensión comparte Llave con el Descriptor y NO es un Atributo, NO se pueden editar a menos que tengan una llave Subrogada
		// - El caso bloqueado es una Dimensión cuya Llave y Descriptor sean el mismo, sin tener llave Subrogada
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$aDimension = BITAMDimension::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto, false);
		}
		else
		{
		*/
			$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
		//}
		//@JAPR
		if (is_null($aDimension))
		{
			return false;
		}
		
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		if ($aDimension->isCatalog && (int) $this->DimensionValueID == 1)
		{
			return false;
		}
		
		//@JAPR 2010-07-13: Modificada temporalmente la validación para NO permitir remover valores de dimensiones mientras no se determine como
		//actuar en la tabla de hechos del cubo para no causar inconsistencias
		//@JAPR 2010-07-13: Modificada la validación para que en caso de tener llave diferente al descriptor SIN subrrogada, pero que la llave 
		//sea editable, NO se pueda editar el valor pues eso alteraría la integridad del cubo
		if ($aDimension->dependsOnDimension == 0 && $aDimension->nom_fisicok != $aDimension->nom_fisico && $aDimension->created_by == 1 && (trim($aDimension->nomfisicok_descripkeys) == '' || $aDimension->nom_fisicok_bd == $aDimension->nomfisicok_descripkeys))
		{
			return false;
		}
		// @JAPR
		
		if ($aDimension->IsAttribute || ($aDimension->nom_fisicok != $aDimension->nom_fisico) || ($aDimension->nomfisicok_descripkeys != '' && $aDimension->nom_fisicok_bd != $aDimension->nomfisicok_descripkeys))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function get_PathArray()
	{
		if ($this->FromCaptureTable)
		{
			return array();
		}
		else
		{
			$parent = $this->get_Parent();
			$pathArray = $parent->get_PathArray();
			$pathArray[] = array($this->get_Title(), $this->get_QueryString());
			return $pathArray;
		}
	}

	function get_FormFields($aUser)
	{
		global $appSettings;
		global $workingMode;
		
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		if ($this->ForParentAssignment)
		{
			/*Esta sección es para la asignación de padres simultáneamente a todos los valores de la dimensión especificada
			*/
			switch ($this->tipo_dato)
			{
				case 1:
				case 2:
					$strDataType = "String";
					break;
				default:
					$strDataType = "Integer";
					break;
			}

			$aParentValuesArray = array();
			$aParentValuesArray[''] = '['.translate('None').']';
			$aParentDimension = null;
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			/*
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				$aDimension = BITAMDimension::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto, false);
			}
			else 
			{
			*/
				$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
			//}
			//@JAPR
			/*
			if (!is_null($aDimension))
			{
				if (count($aDimension->FatherParDescriptor) > 0)
				{
					$aParentDimension = BITAMDimension::NewInstanceWithID($this->Repository, $aDimension->FatherParDescriptor[0], $this->cla_concepto, true);
				}
			}
			
			if (!is_null($aParentDimension))
			{
				if (count($aParentDimension->values) > 0)
				{
					$aParentValuesArray = array_combine($aParentDimension->valuesID, $aParentDimension->values);
					$aParentValuesArray[''] = '['.translate('None').']';
				}
			}
			*/

			/*Agrega un FormField oculto por cada dimensión padre existente, de forma que se pueda leer posteriormente para copiar los 
			valores a los FormsFields de cada miembro de la dimensión capturada la primera vez que se expandan
			*/
			if (count($this->ParentsInfo) > 0)
			{
				foreach ($this->ParentsInfo as $aParentConsecutive => $aParentInfo)
				{
					$aParentValuesArray = array();
					$aParentValuesArray[''] = '['.translate('None').']';
					$aParentDimension = BITAMDimension::NewInstanceWithID($this->Repository, $aParentConsecutive, $this->cla_concepto, true, true, true);
					if (!is_null($aParentDimension))
					{
						if (count($aParentDimension->values) > 0)
						{
							//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
							//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
							//Si el padre tiene ID diferente del Join, debe procesar la lista de valores del Join en lugar de la de IDs
							$blnUseParentInfo = false;
							if (trim($aParentInfo["parent_nom_fisicok_join"]) != '' && $aParentInfo["parent_nom_fisicok_bd"] != $aParentInfo["parent_nom_fisicok_join"])
							{
								$blnUseParentInfo = true;
							}
							
							if ($blnUseParentInfo)
							{
								$arrJoinIDs = array();
								foreach ($aParentDimension->valuesID as $aValueID)
								{
									$arrJoinIDs[] = @$aParentDimension->subrrogateID[$aValueID];
								}
								$aParentValuesArray = array_combine($arrJoinIDs, $aParentDimension->values);
							}
							else 
							{
								$aParentValuesArray = array_combine($aParentDimension->valuesID, $aParentDimension->values);
							}
							//@JAPR
							$aParentValuesArray[''] = '['.translate('None').']';
						}
					}
					if ($aParentConsecutive == $aDimension->FatherParDescriptor[0])
					{
						$aParentDimensionGlob = $aParentDimension;
					}
					eval ('$this->ParentValueArray'.$aParentConsecutive.' = null;');
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "ParentValueArray".$aParentConsecutive;
					$aField->Title = $aField->Name;
					$aField->Type = "Object";
					$aField->VisualComponent = "ComboBox";
					$aField->Options = $aParentValuesArray;
					$aField->Size = 64;
					$aField->InTitle = true;
					$aField->IsVisible = false;
					$aField->BackgroundColor = '#FFFFFF';
					$myFields[$aField->Name] = $aField;
				}
			}
			//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
			//Si no hay padres, agrega un FormField oculto como referencia para saber donde agregar el Header con los títulos de los campos
			//el nombre de este campo será fijo ya que realmente no se requiere para una función fuera del posicionamiento de los Rows
			else 
			{
					$this->AttributeValueArray0 = 0;
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "AttributeValueArray0";
					$aField->Title = $aField->Name;
					$aField->Type = "Object";
					$aField->VisualComponent = "ComboBox";
					$aField->Options = array();
					$aField->Size = 64;
					$aField->InTitle = true;
					$aField->IsVisible = false;
					$aField->BackgroundColor = '#FFFFFF';
					$myFields[$aField->Name] = $aField;
			}
			//@JAPR
			
			foreach ($this->DimensionValuesArray as $strDimensionValueID => $aDimensionValueDataArray)
			{
				//$strEscapedDimensionValueID = addcslashes((string)$strDimensionValueID, "\0..\37\\\"\'\177..\377");
				/*
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "DimensionValuesArray[".$strDimensionValueID."][0]";
				$aField->Title = $aDimensionValueDataArray[0];
				$aField->Type = $strDataType;
				if ($strDataType == 'String')
				{
					$aField->Size = 255;
				}
				else 
				{
					$aField->Size = 25;
				}
				$aField->IsDisplayOnly = true;
				$aField->CloseCell = false;
				$myFields[$aField->Name] = $aField;
				*/
				/*
					MUY FUNCIONAL
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "DimensionValuesArray[".$strDimensionValueID."][2]";
					$aField->Title = $aDimensionValueDataArray[1];
					$aField->Type = "Object";
					$aField->VisualComponent = "ComboBox";
					//$aField->Options = $aParentValuesArray;
					$aField->Options = array();
					$aField->Options[$aDimensionValueDataArray[2]] = @$aParentDimensionGlob->values[$aDimensionValueDataArray[2]];
					$aField->Size = 64;
					$aField->BackgroundColor = '#FFFFFF';
					$myFields[$aField->Name] = $aField;
					$intNumParent++;
				*/
				$intNumParent = 0;
				$intCountParents = count($this->ParentsInfo);
				//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
				$intTotalColumns = $intCountParents;
				if ($appSettings["DimensionAttributes"] == 1)
				{
					$intTotalColumns += count($this->AttributesInfo);
				}
				//@JAPR
				foreach ($this->ParentsInfo as $aParentConsecutive => $aParentInfo)
				{
					$intNumParent++;
					$aParentDimension = BITAMDimension::NewInstanceWithID($this->Repository, $aParentConsecutive, $this->cla_concepto, true, true, true);
					//$strParentFieldName = "ParentValueID".$aParentConsecutive;
					//@JAPRWarning: $strParentFieldName = chr(39).addcslashes('ParentValueID'.$aParentConsecutive, "\0..\37\\\"\'\177..\377").chr(39);
					$strParentFieldName = chr(39).'ParentValueID'.$aParentConsecutive.chr(39);
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "DimensionValuesArray[".$strDimensionValueID."][".$strParentFieldName."]";
					$aField->Title = $aDimensionValueDataArray[1];
					$aField->Type = "Object";
					$aField->VisualComponent = "ComboBox";
					//$aField->Options = $aParentValuesArray;
					$aField->Options = array();
					if (!is_null($aParentDimension))
					{
						//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
						//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
						//Si el padre tiene ID diferente del Join, debe procesar la lista de valores del Join en lugar de la de IDs
						//@JAPRWarning: Optimizar esto, se podría dejar este Boolean como una propiedad
						$blnUseParentInfo = false;
						if (trim($aParentInfo["parent_nom_fisicok_join"]) != '' && $aParentInfo["parent_nom_fisicok_bd"] != $aParentInfo["parent_nom_fisicok_join"])
						{
							$blnUseParentInfo = true;
						}
						
						if ($blnUseParentInfo)
						{
							$aField->Options[$aDimensionValueDataArray[$strParentFieldName]] = @$aParentDimension->values[array_search($aDimensionValueDataArray[$strParentFieldName], $aParentDimension->subrrogateID)];
						}
						else 
						{
							$aField->Options[$aDimensionValueDataArray[$strParentFieldName]] = @$aParentDimension->values[$aDimensionValueDataArray[$strParentFieldName]];
						}
						//@JAPR
					}
					$aField->Size = 64;
					//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
					$aField->CloseCell = ($intNumParent == $intTotalColumns);
					$aField->FixedWidth = (int)(80 / $intTotalColumns)."%";
					//@JAPR
					$aField->BackgroundColor = '#FFFFFF';
					$myFields[$aField->Name] = $aField;
				}
				
				//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
				if ($appSettings["DimensionAttributes"] == 1)
				{
					$intNumAttr = 0;
					$intCountAttrs = count($this->AttributesInfo);
					foreach ($this->AttributesInfo as $anAttributeConsecutive => $anAttributeInfo)
					{
						$intNumAttr++;
						$anAttributeDimension = BITAMDimension::NewInstanceWithID($this->Repository, $anAttributeConsecutive, $this->cla_concepto, false, true, true);
						$strAttributeFieldName = chr(39).'AttributeValueID'.$anAttributeConsecutive.chr(39);
						$intSize = 1;
						switch ($anAttributeDimension->tipo_dato)
						{
							case 1:
							case 2:
								$strDataType = "String";
								//@JAPR 2008-12-19: Ahora ya tomará en cuenta el tipo de dato definido para los campos de la dimensión al momento de mostrar los componentes de captura
								$intSize = $anAttributeDimension->long_dato;
								break;
							default:
								$strDataType = "Integer";
								break;
						}
						$aField = BITAMFormField::NewFormField();
						$aField->Name = "DimensionValuesArray[".$strDimensionValueID."][".$strAttributeFieldName."]";
						$aField->Title = $aDimensionValueDataArray[1];
						$aField->Type = $strDataType;
						if ($strDataType == 'String')
						{
							$aField->Size = $intSize;
						}
						else 
						{
							$aField->Size = 25;
						}
						$aField->CloseCell = ($intNumParent + $intNumAttr == $intTotalColumns);
						$aField->FixedWidth = (((int) (100 / $intTotalColumns))-0.5)."%";
						$aField->BackgroundColor = '#FFFFFF';
						$myFields[$aField->Name] = $aField;
					}
				}
				//@JAPR
			}
		}
		else 
		{
			/*Esta sección es para la captura de un valor de dimensión permitiendo la asignación de padre(s) y de campo(s) adicional(es)
			*/
			//@JAPR 2008-12-19: Ahora ya tomará en cuenta el tipo de dato definido para los campos de la dimensión al momento de mostrar los componentes de captura
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			/*
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				$aDimension = BITAMDimension::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto, false);
			}
			else
			{
			*/
				$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
			//}
			//@JAPR
			$intSize = 1;
			switch ($this->tipo_datok)
			{
				case 1:
				case 2:
					$strDataType = "String";
					$intSize = $aDimension->long_datok;
					break;
				case 8:
				case 21:
				case 23:
				case 24:
					$strDataType = "Float";
					break;
				case 12:
				case 22:
					$strDataType = "DateTime";
					break;
				default:
					$strDataType = "Integer";
					break;
			}
	
			if ($this->FromCaptureTable)
			{
				/*Esta sección es para la captura/edición de valores desde la tabla de captura del presupuesto, con lo cual además de
				grabar el valor de dimensión, automáticamente asocia el valor a la combinación de dimensiones seleccionada
				*/
				$blnDebugData = false;
				// Agrega los datos que deben ser enviados como POST en campos LargeString para que no existan problemas
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "POSTDimensions";
				$aField->Title = translate("Dimensions");
				$aField->Type = "LargeString";
				$aField->InTitle = !$blnDebugData;
				$aField->IsVisible = $blnDebugData;
				$myFields[$aField->Name] = $aField;

				$aField = BITAMFormField::NewFormField();
				$aField->Name = "POSTCombinations";
				$aField->Title = translate("Combinations");
				$aField->Type = "LargeString";
				$aField->InTitle = !$blnDebugData;
				$aField->IsVisible = $blnDebugData;
				$myFields[$aField->Name] = $aField;

				$aField = BITAMFormField::NewFormField();
				$aField->Name = "POSTDates";
				$aField->Title = translate("Dates");
				$aField->Type = "LargeString";
				$aField->InTitle = !$blnDebugData;
				$aField->IsVisible = $blnDebugData;
				$myFields[$aField->Name] = $aField;

				$aField = BITAMFormField::NewFormField();
				$aField->Name = "POSTMembersFrom";
				$aField->Title = translate("Members from");
				$aField->Type = "Integer";
				$aField->InTitle = !$blnDebugData;
				$aField->IsVisible = $blnDebugData;
				$myFields[$aField->Name] = $aField;

				$aField = BITAMFormField::NewFormField();
				$aField->Name = "POSTPeriodID";
				$aField->Title = translate("Period ID");
				$aField->Type = "Integer";
				$aField->InTitle = !$blnDebugData;
				$aField->IsVisible = $blnDebugData;
				$myFields[$aField->Name] = $aField;

				$aField = BITAMFormField::NewFormField();
				$aField->Name = "POSTStartDateDim";
				$aField->Title = translate("Start date dim");
				$aField->Type = "String";
				$aField->Size = 10;
				$aField->InTitle = !$blnDebugData;
				$aField->IsVisible = $blnDebugData;
				$myFields[$aField->Name] = $aField;

				$aField = BITAMFormField::NewFormField();
				$aField->Name = "POSTEndDateDim";
				$aField->Title = translate("End date dim");
				$aField->Type = "String";
				$aField->Size = 10;
				$aField->InTitle = !$blnDebugData;
				$aField->IsVisible = $blnDebugData;
				$myFields[$aField->Name] = $aField;
			}

			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			$blnIsCatalog = $aDimension->isCatalog;
			//@JAPR 2009-07-09: Validado que si el campo clave y descriptor son los mismos, no intente capturar la clave
			$bSameKeyDescField = strtolower($aDimension->nom_fisico) == (strtolower($aDimension->nom_fisicok));
			$bIsOwnedByEktos = false;
			if (!is_null($aDimension))
			{
				$bIsOwnedByEktos = ($aDimension->created_by == 1);
			}

			//Verificar si el campo clave de la dimensión es igual 
			//al campo de la descripcion de la dimensión
			$isSameFieldKeyDescrip = false;

			if($aDimension->nom_fisicok == $aDimension->nom_fisico)
			{
				$isSameFieldKeyDescrip = true;
			}
			
			//Esta propiedad indica que la ventana no se cierre cuando no hay valor para la clave de la dimensión
			//$this->HasAutoKeyGeneration = $bIsOwnedByEktos;
			if($isSameFieldKeyDescrip==true)
			{
				$this->HasAutoKeyGeneration = true;
			}
			else 
			{
				$this->HasAutoKeyGeneration = $bIsOwnedByEktos;
			}

			$aField = BITAMFormField::NewFormField();
			$aField->Name = "DimensionValueID";
			$aField->Title = translate("ID");
			$aField->Type = $strDataType;
			if ($strDataType === 'DateTime')
			{
				$aField->FormatMask = "yyyy-MM-dd";
			}
			if ($strDataType == 'String')
			{
				//@JAPR 2008-12-19: Ahora ya tomará en cuenta el tipo de dato definido para los campos de la dimensión al momento de mostrar los componentes de captura
				$aField->Size = $intSize;
			}
			else 
			{
				$aField->Size = 25;
			}
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			if ($bIsOwnedByEktos || (!$this->isNewObject()) || $bSameKeyDescField || $blnIsCatalog)	//$this->FromCaptureTable && 
			{
				$aField->InTitle = true;
				$aField->IsVisible = false;
			}
			$myFields[$aField->Name] = $aField;

			//@JAPR 2009-07-09: Validado que si el campo clave y descriptor son los mismos, no intente capturar la clave
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "OldDimensionValueID";
			$aField->Title = translate("OldID");
			$aField->Type = $strDataType;
			if ($strDataType === 'DateTime')
			{
				$aField->FormatMask = "yyyy-MM-dd";
			}
			if ($strDataType == 'String')
			{
				//@JAPR 2008-12-19: Ahora ya tomará en cuenta el tipo de dato definido para los campos de la dimensión al momento de mostrar los componentes de captura
				$aField->Size = $intSize;
			}
			else 
			{
				$aField->Size = 25;
			}
			$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
			//@JAPR
			
			$intSize = 1;

			switch ($this->tipo_dato)
			{
				case 1:
				case 2:
					$strDataType = "String";
					//@JAPR 2008-12-19: Ahora ya tomará en cuenta el tipo de dato definido para los campos de la dimensión al momento de mostrar los componentes de captura
					$intSize = $aDimension->long_dato;
					break;
				default:
					$strDataType = "Integer";
					break;
			}
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Description";
			$aField->Title = translate("Description");
			$aField->Type = $strDataType;
			//@JAPR 2010-03-19: Validado que la dimensión Company no permita la captura de caracteres que causen problemas al usarse como nombres
			//de archivos en los procesos de carga
			//if (strtolower($aDimension->nom_fisico) == 'company' && $_SESSION["PAFBM_Mode"] == 1)
			//{
			//	$aField->FormatMask = "fF";
			//}
			//@JAPR
			if ($strDataType == 'String')
			{
				//@JAPR 2008-12-19: Ahora ya tomará en cuenta el tipo de dato definido para los campos de la dimensión al momento de mostrar los componentes de captura
				$aField->Size = $intSize;
			}
			else 
			{
				$aField->Size = 25;
			}
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			if ($blnIsCatalog)
			{
				$aField->InTitle = true;
				$aField->IsVisible = false;
			}
			//@JAPR
			$myFields[$aField->Name] = $aField;

			//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
			/*Crea un FormField por cada Atributo de la dimensión, utilizando el nombre genérico AttributeValueID concatenado con
			el CONSECUTIVO de cada dimensión - atributo, de esta forma no hay conflicto de nombres ni caracteres extraños
			*/
			if (count($this->AttributesInfo) > 0 && $appSettings["DimensionAttributes"] == 1)
			{
				foreach ($this->AttributesInfo as $anAttributeConsecutive => $anAttributeInfo)
				{
					switch ($anAttributeInfo["attribute_tipo_datok"])
					{
						case 1:
						case 2:
							$strDataType = "String";
							//@JAPR 2008-12-19: Ahora ya tomará en cuenta el tipo de dato definido para los campos de la dimensión al momento de mostrar los componentes de captura
							$intSize = (int) $anAttributeInfo["attribute_long_datok"];
							break;
						default:
							$strDataType = "Integer";
							break;
					}
					
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "AttributeValueID".$anAttributeConsecutive;
					$aField->Title = $anAttributeInfo["attribute_nom_logico"];
					$aField->Type = $strDataType;
					if ($strDataType == 'String')
					{
						//@JAPR 2008-12-19: Ahora ya tomará en cuenta el tipo de dato definido para los campos de la dimensión al momento de mostrar los componentes de captura
						$aField->Size = $intSize;
					}
					else 
					{
						$aField->Size = 25;
					}
					$myFields[$aField->Name] = $aField;
				}
			}
			//@JAPR
			
			$aParentDimension = null;
			$aParentDimensionCollection = null;
			if (!is_null($aDimension))
			{
				/*@JAPRWarning: En este punto, validar si la dimensión contiene mas de un Padre de SnowFlake, si es así, entonces se debe generar un FormField por cada uno
				pero no podemos usar directamente FatherParDescriptor pues no podemos obtener cuales son los padres directos a partir de ella
				*/
				/*
				if (count($aDimension->FatherParDescriptor) > 0)
				{
					$aParentDimension = BITAMDimension::NewInstanceWithID($this->Repository, $aDimension->FatherParDescriptor[0], $this->cla_concepto, true);
				}
				*/
				if (count($aDimension->InmmediateFatherParDescriptor) > 0)
				{
					//@JAPR 2010-07-19: Agregado el modo de trabajo BITAM para reutilizar los proyectos de Artus Web G6
					if ($workingMode == wrkmBITAM)
					{
						$aCube = BITAMCube::NewInstanceWithID($this->Repository, $this->cla_concepto);
					}
					//@JAPR

					$aParentDimensionCollection = BITAMDimensionCollection::NewInstance($this->Repository, $aDimension->InmmediateFatherParDescriptor, $this->cla_concepto, true, false, true, false, null, true);
				}
			}
			
			/*
			if (!is_null($aParentDimension))
			{
				$aParentValuesArray = array();
				$aParentValuesArray[''] = '['.translate('None').']';
				if (count($aParentDimension->values) > 0)
				{
					$aParentValuesArray = array_combine($aParentDimension->valuesID, $aParentDimension->values);
					$aParentValuesArray[''] = '['.translate('None').']';
				}
				
				//Hay que poner un FormField por cada campo Padre de SnowFlake de la dimensión
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "ParentValueID";
				$aField->Title = translate("Parent");
				$aField->Type = "Object";
				$aField->VisualComponent = "ComboBox";
				$aField->Options = $aParentValuesArray;
				$aField->Size = 64;
				$myFields[$aField->Name] = $aField;
			}
			*/
			/*Crea un FormField por cada Padre de SnowFlake de la dimensión, utilizando el nombre genérico ParentValueID concatenado con
			el CONSECUTIVO de cada dimensión padre, de esta forma no hay conflicto de nombres ni caracteres extraños
			*/
			if (!is_null($aParentDimensionCollection))
			{
				foreach ($aParentDimensionCollection as $aParentDimension)
				{
					$aParentValuesArray = array();
					$aParentValuesArray[''] = '['.translate('None').']';
					if (count($aParentDimension->values) > 0)
					{
						//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
						//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
						//Si el padre tiene ID diferente del Join, debe procesar la lista de valores del Join en lugar de la de IDs
						$blnUseParentInfo = false;
						$aParentInfo = null;
						$aParentInfo = @$this->ParentsInfo[$aParentDimension->consecutivo];
						if (!is_null($aParentInfo))
						{
							if (trim($aParentInfo["parent_nom_fisicok_join"]) != '' && $aParentInfo["parent_nom_fisicok_bd"] != $aParentInfo["parent_nom_fisicok_join"])
							{
								$blnUseParentInfo = true;
							}
						}
						
						if ($blnUseParentInfo)
						{
							$arrJoinIDs = array();
							foreach ($aParentDimension->valuesID as $aValueID)
							{
								$arrJoinIDs[] = @$aParentDimension->subrrogateID[$aValueID];
							}
							$aParentValuesArray = array_combine($arrJoinIDs, $aParentDimension->values);
						}
						else 
						{
							$aParentValuesArray = array_combine($aParentDimension->valuesID, $aParentDimension->values);
						}
						//@JAPR
						$aParentValuesArray[''] = '['.translate('None').']';
					}
					
					//Hay que poner un FormField por cada campo Padre de SnowFlake de la dimensión
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "ParentValueID".$aParentDimension->consecutivo;
					$aField->Title = $aParentDimension->nom_logico;
					$aField->Type = "Object";
					$aField->VisualComponent = "ComboBox";
					$aField->Options = $aParentValuesArray;
					$aField->Size = 64;
					$myFields[$aField->Name] = $aField;
				}
			}
			
			// Crea FormFields por cada campo adicional que se debe capturar con los valores de esta dimensión
			if (($this->FromCaptureTable || $this->IncludeAdditionalFields) && $appSettings["DimensionFields"] == 1)
			{
				//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
				/*
				if ($appSettings["UseGlobalDimensions"] == 1)
				{
					$aDimensionTableFieldCollection = BITAMDimensionTableFieldCollection::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto);
				}
				else
				{
				*/
					$aDimensionTableFieldCollection = BITAMDimensionTableFieldCollection::NewInstance($this->Repository, $this->consecutivo, $this->cla_concepto);
				//}
				//@JAPR
				foreach ($this->FieldsValues as $strFieldName => $aFieldValue)
				{
					$aDimensionTableField = $aDimensionTableFieldCollection->getItemByProperty(str_replace('\'', '',$strFieldName), 'Name', false);
					if (!is_null($aDimensionTableField))
					{
						$aField = BITAMFormField::NewFormField();
						//$aField->Name = "FieldsValues[".$strFieldName."]";
						$aField->Name = str_replace('\'', '',$strFieldName);
						$aField->Title = $aDimensionTableField->Name;
						$aField->Type = $aDimensionTableField->FrameWorkDataTypeName();
						$aField->FormatMask = $aDimensionTableField->FrameWorkFormatMask(7, $aDimensionTableField->FormatMask, $aField->FormatMask);
						$aField->FormatMask = $aDimensionTableField->FrameWorkFormatMask(8, $aDimensionTableField->FormatMask, $aField->FormatMask);
						$aField->FormatMask = $aDimensionTableField->FrameWorkFormatMask(12, 'yyyy-MM-dd', $aField->FormatMask);
						if ($aDimensionTableFieldCollection->hasLength($aDimensionTableField->Name))
						{
							$aField->Size = $aDimensionTableField->MaxLength;
						}
						//Si el campo tiene fórmula asociada, entonces será de solo lectura
						if (trim($aDimensionTableField->Formula) != '')
						{
							$aField->IsDisplayOnly = true;
						}
						$myFields[$aField->Name] = $aField;
					}
				}
			}
		}
		
		return $myFields;
	}

	function updateFromArray($anArray)
	{
		global $appSettings;
		
 		if (array_key_exists("OldDimensionValueID", $anArray))
		{
			$this->OldDimensionValueID = $anArray["OldDimensionValueID"];
		}

 		if (array_key_exists("DimensionValueID", $anArray))
		{
			$this->DimensionValueID = $anArray["DimensionValueID"];
		}

		if (array_key_exists("Description", $anArray))
		{
			$this->Description = $anArray["Description"];
		}

		if (array_key_exists("ParentValueID", $anArray))
		{
			$this->ParentValueID = $anArray["ParentValueID"];
		}

		if (array_key_exists("DimensionID", $anArray))
		{
			$this->consecutivo = $anArray["DimensionID"];
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				$this->cla_descrip = $this->consecutivo;
			}
			//@JAPR
		}

		if (array_key_exists("CubeID", $anArray))
		{
			$this->cla_concepto = $anArray["CubeID"];
		}

		return $this;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{	
		global $appSettings;
		
?>
 		<script language="JavaScript">
 		function canSave(target)
 		{
 			strBlanksField = "";
<?
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$aDimension = BITAMDimension::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto, false);
		}
		else
		{
		*/
			$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
		//}
		//@JAPR
		$bSameKeyDescField = strtolower($aDimension->nom_fisico) == (strtolower($aDimension->nom_fisicok));
		$bIsOwnedByEktos = false;
		if (!is_null($aDimension))
		{
			$bIsOwnedByEktos = ($aDimension->created_by == 1);
		}

		if ($this->ForParentAssignment)
		{
?>
			BITAMDimensionValue_Ok(target);
			return false;
<?
		}
		
		$isSameFieldKeyDescrip = false;
		
		if($aDimension->nom_fisicok == $aDimension->nom_fisico)
		{
			$isSameFieldKeyDescrip = true;
		}
		
		if ($this->isNewObject() && !$bIsOwnedByEktos && !$bSameKeyDescField)
		{
?>
			var dimensionValue = BITAMDimensionValue_SaveForm.DimensionValueID.value;
			
			if (Trim(dimensionValue) == "")
			{
 <?
				//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
				//Las dimensiones catálogo se pueden grabar con descripción vacía ya que internamente si no contienen Llave, utilizarán la
				//llave subrrogada
 				if (!$aDimension->isCatalog)
 				{
 ?>
				strBlanksField += '\n- ' + '<?=translate("The value id must be filled in")?>';
 <?
 				}
 ?>
			}
			else
			{
				switch (<?=$this->tipo_datok?>)
				{
					case 1:
					case 2:
						break;
					case 8:
					case 21:
					case 23:
					case 24:
						vValue = parseFloat(dimensionValue);
						if (isNaN(vValue))
						{
							alert ('<?=translate('Invalid data format, only floating-point numbers are allowed')?>. <?=translate('Field')?>: \'<?=translate('ID')?>\'');
							return false;
						}
						
						vValue = String(vValue);
						if (Trim(vValue) != Trim(dimensionValue))
						{
							alert ('<?=translate('Invalid data format, only floating-point numbers are allowed')?>. <?=translate('Field')?>: \'<?=translate('ID')?>\'');
							return false;
						}
						break;
					case 12:
					case 22:
						break;
					default:
						vValue = parseInt(dimensionValue);
						if (isNaN(vValue))
						{
							alert ('<?=translate('Invalid data format, only integer numbers are allowed')?>. <?=translate('Field')?>: \'<?=translate('ID')?>\'');
							return false;
						}
						
						vValue = String(vValue);
						if (Trim(vValue) != Trim(dimensionValue))
						{
							alert ('<?=translate('Invalid data format, only integer numbers are allowed')?>. <?=translate('Field')?>: \'<?=translate('ID')?>\'');
							return false;
						}
						break;
				}
				
				
				var xmlObj = getXMLObject();
				if (xmlObj == null)
				{
					alert('<?=translate('XML Object not supported')?>');
					return(new Array);
				}
				
				xmlObj.open('GET', 'existDimensionValue.php?ObjectID='+dimensionValue+'&DimensionID=<?=$this->consecutivo?>&CubeID=<?=$this->cla_concepto?>', false);
		 		xmlObj.send(null);
				if(xmlObj.status == 404)
				{
					alert('<?=translate('Unable to launch validation process')?>');
					return false;
				}
				
		 		strResponseData = Trim(unescape(xmlObj.responseText));
		 		arrResponseData = strResponseData.split('<SEP>');
		 		if(arrResponseData.length == 0)
		 		{
		 			alert('<?=translate('Error found during data validation')?>: <?=translate('404 - File not found')?>');
					return false;
		 		}
		 		
		 		if(arrResponseData[0]!=0)
		 		{
					strErrMsg = arrResponseData[0];
					if(arrResponseData.length > 1)
					{
						strErrMsg = arrResponseData[1];
					}
					
	  				strBlanksField += '\n- ' + '<?=translate('Error found during data validation')?>: ' + strErrMsg;
		 		}
		 		
			}
<?
		}
?>	 		
			description = Trim(BITAMDimensionValue_SaveForm.Description.value);
 			
 			if (description == "" )
 			{
 <?
				//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
				//Las dimensiones catálogo se pueden grabar con descripción vacía ya que internamente si no contienen Llave, utilizarán la
				//llave subrrogada
 				if (!$aDimension->isCatalog)
 				{
 ?>
 				strBlanksField += '\n- ' + '<?=translate("The value description must be filled in")?>';
 <?
 				}
 ?>
 			}
 			else
 			{
				switch (<?=$this->tipo_dato?>)
				{
					case 1:
					case 2:
						break;
					default:
						vValue = parseInt(description);
						if (isNaN(vValue))
						{
							alert ('<?=translate('Invalid data format, only integer numbers are allowed')?>. <?=translate('Field')?>: \'<?=translate('Description')?>\'');
							return false;
						}
						
						vValue = String(vValue);
						if (Trim(vValue) != Trim(description))
						{
							alert ('<?=translate('Invalid data format, only integer numbers are allowed')?>. <?=translate('Field')?>: \'<?=translate('Description')?>\'');
							return false;
						}
						break;
				}

<?
		//@JAPR 2010-02-19: Valida que en caso de usar el mismo campo para descripción y llave, no se puedan duplicar descripciones
		if ($bSameKeyDescField)
		{
			$strSourceDimensionValueIDParam = '';
			if (!$this->isNewObject())
			{
				$strSourceDimensionValueIDParam = '&DimensionValueID='.urlencode($this->DimensionValueID);
			}
?>			
				var xmlObj = getXMLObject();
				if (xmlObj == null)
				{
					alert('<?=translate('XML Object not supported')?>');
					return(new Array);
				}
				
				xmlObj.open('GET', 'existDimensionValue.php?ObjectID='+escape(description).replace(new RegExp( "\\+", "g" ), "%2B")+'&DimensionID=<?=$this->consecutivo?>&CubeID=<?=$this->cla_concepto?><?=$strSourceDimensionValueIDParam?>', false);
		 		xmlObj.send(null);
				if(xmlObj.status == 404)
				{
					alert('<?=translate('Unable to launch validation process')?>');
					return false;
				}
				
		 		strResponseData = Trim(unescape(xmlObj.responseText));
		 		arrResponseData = strResponseData.split('<SEP>');
		 		if(arrResponseData.length == 0)
		 		{
		 			alert('<?=translate('Error found during data validation')?>: <?=translate('404 - File not found')?>');
					return false;
		 		}
		 		
		 		if(arrResponseData[0]!=0)
		 		{
					strErrMsg = arrResponseData[0];
					if(arrResponseData.length > 1)
					{
						strErrMsg = arrResponseData[1];
					}
					
	  				strBlanksField += '\n- ' + '<?=translate('Error found during data validation')?>: ' + strErrMsg;
		 		}
		 		
<?
		}
?> 			
			}
 			
 <?
 		/* Validación que los campos adicionales para captura siempre tengan un valor.
 		En caso de que el campo sea tipo fecha, se valida que no sea la máscara default (0000-00-00)
 		*/
 		if ($this->FromCaptureTable && $appSettings["DimensionFields"] == 1)
 		{
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			/*
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				$aDimensionTableFieldCollection = BITAMDimensionTableFieldCollection::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto);
			}
			else 
			{
			*/
				$aDimensionTableFieldCollection = BITAMDimensionTableFieldCollection::NewInstance($this->Repository, $this->consecutivo, $this->cla_concepto);
			//}
			//@JAPR
			if (!is_null($aDimensionTableFieldCollection))
			{
				foreach ($aDimensionTableFieldCollection as $aTableField)
				{
					if ($aTableField->isADate())
					{
						// Es un campo fecha, no debe ir vacio ni ser la máscara de 0s
?>
			strJSFieldValue = Trim(BITAMDimensionValue_SaveForm.<?=$aTableField->Name?>.value);
 			if (strJSFieldValue == "" || strJSFieldValue == "0000-00-00")
 			{
 				strBlanksField += '\n- ' + '<?=str_replace('{var1}',$aTableField->Name,translate("The value in the field {var1} must be filled in"))?>';
 			}
<?
					}
					elseif (!$aTableField->mustBeQuoted())
					{
						// Es un campo numérico, no debe ir vacio
?>
			strJSFieldValue = Trim(BITAMDimensionValue_SaveForm.<?=$aTableField->Name?>.value);
 			if (strJSFieldValue == "")
 			{
 				strBlanksField += '\n- ' + '<?=str_replace('{var1}',$aTableField->Name,translate("The value in the field {var1} must be filled in"))?>';
 			}
<?
					}
				}
			}
 		}
 ?>
  			if (strBlanksField != "")
  			{
				alert('<?=translate("Please verify")?>:' + strBlanksField );
  			}
  			else
  			{
  				BITAMDimensionValue_Ok(target);
  			}
 		}
 		</script>
 		
 		<table class="object_table" width="100%" id="Formfields_table_Header" style="display:none">
 			<tr id="Formfields_row_Header">
 				<td class="object_field_title"><b><?=$this->nom_logico?></b>
 				</td>
<?
		$intNumParents = count($this->ParentsInfo);
		//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
		$intNumAttrs = 0;
		if ($appSettings["DimensionAttributes"] == 1)
		{
			$intNumAttrs = count($this->AttributesInfo);
		}
		$intTotalColumns = $intNumParents + $intNumAttrs;
?>
				<td colspan="<?= $intTotalColumns * 2 - 1 ?>" class="object_field_value_<?= $intTotalColumns * 2 - 1 ?>">
					<table width="100%">
						<tr>
<?
				foreach ($this->ParentsInfo as $aParentConsecutive => $aParentInfo)
				{
?>
					<td style="text-align:center;width:<?=(100 /$intTotalColumns)?>%;"><b><?=$aParentInfo["parent_nom_logico"]?></b></td>
<?
				}
?>
<?
				if ($appSettings["DimensionAttributes"] == 1)
				{
					foreach ($this->AttributesInfo as $anAttributeConsecutive => $anAttributeInfo)
					{
?>
					<td style="text-align:center;width:<?=(100 /$intTotalColumns)?>%;"><b><?=$anAttributeInfo["attribute_nom_logico"]?></b></td>
<?
					}
				}
?>
						</tr>
					</table>
				</td>
 			</tr>
 		</table>
<?
 	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
 	{
 		global $appSettings;
 		
 		if ($this->FromCaptureTable)
 		{
?>
	 	<script language="JavaScript">
 			objOkSelfButton = document.getElementById("BITAMDimensionValue_OkSelfButton");
 			objOkSelfButton.onclick=new Function("canSave('self');");
 			
 			BITAMDimensionValue_CancelButton.style.display = 'none';

		</script> 		
<?
 		}
 		else
 		{
?>
	 	<script language="JavaScript">
 			objOkSelfButton = document.getElementById("BITAMDimensionValue_OkSelfButton");
 			objOkParentButton = document.getElementById("BITAMDimensionValue_OkParentButton");
 			
 			objOkSelfButton.onclick=new Function("canSave('self');");
 			objOkParentButton.onclick=new Function("canSave('parent');");
<?
			if($this->isNewObject())
	 		{
?>
			BITAMDimensionValue_SaveForm.DimensionValueID.value = "";
 			objOkNewButton = document.getElementById("BITAMDimensionValue_OkNewButton");
 			objOkNewButton.onclick=new Function("canSave('new');");
<?
			}
			
			if ($this->ForParentAssignment)
			{
?>
	 		function LoadFullParentValues(anElement)
	 		{
	 			var objSourceCombo, objTargetCombo, objOption, objSourceOption, objFirstRow, objNewRow, objSourceRow, objNewCell
	 			var intIndex, intMaxElements, intDimID, strSelectedValue
	 			objTargetCombo = BITAMDimensionValue_SaveForm.elements.namedItem(anElement.name);
	 			if (objTargetCombo == null)
	 			{
	 				return;
	 			}
	 			intDimID = objTargetCombo.getAttribute("DimID")
	 			objTargetCombo.onclick = function(){return;};
<?
				foreach ($this->ParentsInfo as $aParentConsecutive => $aParentInfo)
				{
?>
					if (intDimID == <?=$aParentConsecutive?>)
					{
						objSourceCombo = BITAMDimensionValue_SaveForm.elements.namedItem("ParentValueArray<?=$aParentConsecutive?>");
						if (objSourceCombo != null)
						{
							intMaxElements = objSourceCombo.options.length;
							strSelectedValue = objTargetCombo.options.item(objTargetCombo.options.selectedIndex).value;
							if (intMaxElements > 0)
							{
								objTargetCombo.options.length = 0;
							}
							for(intIndex=0; intIndex < intMaxElements; intIndex++)
							{
								objSourceOption = objSourceCombo.options.item(intIndex);
								objOption = document.createElement("OPTION");
								objTargetCombo.options.add(objOption);
								objOption.value = objSourceOption.value;
								objOption.text = objSourceOption.text;
								if (objOption.value == strSelectedValue)
								{
									objTargetCombo.selectedIndex = intIndex;
								}
							}
						}
					}
<?
				}
?>
	 		}
<?
?>
<?
				foreach ($this->DimensionValuesArray as $strDimensionValueID => $aDimensionValueDataArray)
				{
					foreach ($this->ParentsInfo as $aParentConsecutive => $aParentInfo)
					{
						//$strParentFieldName = "ParentValueID".$aParentConsecutive;
						//@JAPRWarning: $strParentFieldName = chr(39).addcslashes('ParentValueID'.$aParentConsecutive, "\0..\37\\\"\'\177..\377").chr(39);
						$strParentFieldName = chr(39).'ParentValueID'.$aParentConsecutive.chr(39);
						$strParentDimensionCompName = "DimensionValuesArray[".$strDimensionValueID."][".$strParentFieldName."]";
			//BITAMDimensionValue_SaveForm.elements.namedItem("<=$strParentDimensionCompName>").onclick = LoadFullParentValues;									//Works in IE only
			//BITAMDimensionValue_SaveForm.elements.namedItem("<=$strParentDimensionCompName>").onclick = function(){LoadFullParentValues(this);};		//Works in IE, FireFox, Safari
?>
			BITAMDimensionValue_SaveForm.elements.namedItem("<?=$strParentDimensionCompName?>").onclick = function(){LoadFullParentValues(this);};
			BITAMDimensionValue_SaveForm.elements.namedItem("<?=$strParentDimensionCompName?>").setAttribute("DimID", <?=$aParentConsecutive?>);
<?
					}
				}
				
				//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
				$intNumParents = count($this->ParentsInfo);
				$intNumAttrs = 0;
				if ($appSettings["DimensionAttributes"] == 1)
				{
					$intNumAttrs = count($this->AttributesInfo);
				}
				//@JAPR
				
				foreach ($this->ParentsInfo as $aParentConsecutive => $aParentInfo)
				{
				//objFirstRow.insertAdjacentElement("beforeBegin", objNewRow);		//Works in IE only
?>
			objFirstRow = document.getElementById("<?="Row_ParentValueArray".$aParentConsecutive?>");
			objSourceRow = document.getElementById("Formfields_row_Header");
			objNewRow = document.createElement("TR");
			objNewRow.id = "title_row_header"
			objFirstRow.parentNode.insertBefore(objNewRow, objFirstRow);
			intMaxElements = objSourceRow.cells.length;
			for (intIndex = 0; intIndex < intMaxElements; intIndex++)
			{
				objNewCell = objNewRow.insertCell(-1);
				objNewCell.className = objSourceRow.cells.item(intIndex).className;
				objNewCell.innerHTML = objSourceRow.cells.item(intIndex).innerHTML;
			}
<?
					break;
				}
				
				//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
				//Si había padres, el proceso anterior ya incluyó las etiquetas de los atributos ya que forman parte de la misma Row, así que
				//ya no necesita agregar nada
				if ($appSettings["DimensionAttributes"] == 1 && $intNumParents == 0 && $intNumAttrs > 0)
				{
					//Si no hay padres, se tiene que agregar la Row de los títulos pero ahora se busca por el nombre fijo de la 
					//Row dummy para posicionamiento de Atributos, el resto del proceso es igual que con los padres
?>
			objFirstRow = document.getElementById("Row_AttributeValueArray0");
			objSourceRow = document.getElementById("Formfields_row_Header");
			objNewRow = document.createElement("TR");
			objNewRow.id = "title_row_header"
			objFirstRow.parentNode.insertBefore(objNewRow, objFirstRow);
			intMaxElements = objSourceRow.cells.length;
			for (intIndex = 0; intIndex < intMaxElements; intIndex++)
			{
				objNewCell = objNewRow.insertCell(-1);
				objNewCell.className = objSourceRow.cells.item(intIndex).className;
				objNewCell.innerHTML = objSourceRow.cells.item(intIndex).innerHTML;
			}
<?
				}
				//@JAPR
			}
?>
		</script>
<?
 		}
	}
}

/***********************************************************************************************/

class BITAMDimensionValueCollection extends BITAMCollection
{
	public $cla_concepto;
	public $consecutivo;
	//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
	//public $cla_descrip;
	//@JAPR
	public $tipo_dato;
	public $tipo_datok;
	public $ForParentAssignment;
	//@JAPR 2009-07-08: Agregados los filtros para la carga de valores de dimensiones
	public $dimensionValuesFilter;	//Filtro (temporal, se pierde al termina la sesión) a aplicar para la carga de valores
	public $maxDimensionValueToShow;//Cantidad de valores máxima a mostrar en el listado de valores (configurable en forma global dentro de SI_CONFIGURA)
	public $showNullParentValues;	//Indica si al consultar la lista de valores se deberían o no ver todos los valores incluso con padres que no estén asignados
	//@JAPR	
	
	function __construct($aRepository, $aDimensionID = -1, $aCubeID = -1, $bApplyFilters = false, $sLikeFilter = '', $iTopValues = -1, $bShowNullParVal = null) //, $aDescripKey = -1)
	{
		global $maxDimensionRecords;
		global $dimValuesFilter;
		global $bShowNullParentValues;
		
		BITAMCollection::__construct($aRepository);
		$this->cla_concepto = $aCubeID;
		$this->consecutivo = $aDimensionID;
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		//$this->cla_descrip = $aDescripKey;
		//@JAPR
		$this->tipo_dato = 2;
		$this->tipo_datok = 7;
		$this->ForParentAssignment = false;
		$this->dimensionValuesFilter = '';
		$this->showNullParentValues = false;
		$this->maxDimensionValueToShow = DEFAULT_MAX_DIM_RECORDS;
		//@JAPR 2009-07-08: Agregados los filtros para la carga de valores de dimensiones
		if($bApplyFilters)
		{
			if($sLikeFilter != '')
			{
				$dimValuesFilter = $sLikeFilter;
			}
			$this->dimensionValuesFilter = $dimValuesFilter;
			if(!is_null($bShowNullParVal))
			{
				$bShowNullParentValues = $bShowNullParVal;
			}
			$this->showNullParentValues = $bShowNullParentValues;
			if($iTopValues < 0)
			{
				$this->maxDimensionValueToShow = $maxDimensionRecords;
			}
			else
			{
				$this->maxDimensionValueToShow = $iTopValues;
			}
		}
		//@JAPR
	}
	
	static function NewInstance($aRepository, $anArrayOfDimensionValuesIDs = null, $aDimensionID, $aCubeID, $in, $bUseParentsJoin=false, $bApplyFilters=false, $sLikeFilter = '', $iTopValues = -1, $bIncludeNullParents = false)
	{
		global $appSettings;
		
		$anInstance = new BITAMDimensionValueCollection($aRepository, $aDimensionID, $aCubeID, $bApplyFilters, $sLikeFilter, $iTopValues);
		$featuresDimension = BITAMDimension::getFeaturesDimension($aRepository, $aDimensionID, $aCubeID);
		if (!is_null($featuresDimension))
		{
			$anInstance->tipo_dato = $featuresDimension["tipo_dato"];
			$anInstance->tipo_datok = $featuresDimension["tipo_datok"];
			
			$where = "";
			$strFilter = "";
			if (!is_null($anArrayOfDimensionValuesIDs))
			{
				switch (count($anArrayOfDimensionValuesIDs))
				{
					case 0:
						break;
					case 1:
						//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
						$strDimensionValueID = formatValueForSQL($aRepository, $anArrayOfDimensionValuesIDs[0], $featuresDimension["tipo_datok"]);
						if($in==true)
						{
							$where = " WHERE tb.".$featuresDimension["nom_fisicok_bd"]." = ".$strDimensionValueID;
							//$where = " WHERE ".$featuresDimension["nom_fisicok_bd"]." = ".(((int)$featuresDimension["tipo_datok"] === 7 || (int)$featuresDimension["tipo_datok"] === 8) ? $anArrayOfDimensionValuesIDs[0] : $aRepository->DataADOConnection->Quote($anArrayOfDimensionValuesIDs[0]));
							$strFilter = "[".$featuresDimension["nom_tabla"]."].[".$featuresDimension["nom_fisicok_bd"]."_".$aDimensionID."].[".$anArrayOfDimensionValuesIDs[0]."]";
						}
						else 
						{
							$where = " WHERE tb.".$featuresDimension["nom_fisicok_bd"]." <> ".$strDimensionValueID;
							$strFilter = "[EXCEPT] "."[".$featuresDimension["nom_tabla"]."].[".$featuresDimension["nom_fisicok_bd"]."_".$aDimensionID."].[".$anArrayOfDimensionValuesIDs[0]."]";
							//$where = " WHERE ".$featuresDimension["nom_fisicok_bd"]." <> ".(((int)$featuresDimension["tipo_datok"] === 7 || (int)$featuresDimension["tipo_datok"] === 8 ) ? $anArrayOfDimensionValuesIDs[0] : $aRepository->DataADOConnection->Quote($anArrayOfDimensionValuesIDs[0]));
						}
						//@JAPR
						break;
					default:
						foreach ($anArrayOfDimensionValuesIDs as $aDimensionValueID)
						{
							if ($where != "")
							{
								$where .= ", ";
							}
							//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
							$strDimensionValueID = formatValueForSQL($aRepository, $aDimensionValueID, $featuresDimension["tipo_datok"]);
							$where .= $strDimensionValueID; 
							//$where .= (((int) $featuresDimension["tipo_datok"] === 7 || (int) $featuresDimension["tipo_datok"] === 8 ) ? $aDimensionValueID : $aRepository->DataADOConnection->Quote($aDimensionValueID)); 
							//@JAPR
							$strFilter .= ((strlen($strFilter) > 0)?" OR ":"")."[".$featuresDimension["nom_tabla"]."].[".$featuresDimension["nom_fisicok_bd"]."_".$aDimensionID."].[".$aDimensionValueID."]";
						}
						if ($where != "")
						{
							if($in==true)
							{
								$where = " WHERE tb.".$featuresDimension["nom_fisicok_bd"]." IN (".$where.")";
							}
							else 
							{
								$where = " WHERE tb.".$featuresDimension["nom_fisicok_bd"]." NOT IN (".$where.")";
								$strFilter = "[EXCEPT] ".$strFilter;
							}
						}
						break;
				}
			}

			$strParentField = '';
			/*
			if (array_key_exists('parent_nom_fisicok_bd', $featuresDimension))
			{
				$strParentField = $featuresDimension["parent_nom_fisicok_bd"];
				if (trim($strParentField) != '')
				{
					$strParentField = ', '.$strParentField.' AS ParentValueID ';
				}
				else 
				{
					$strParentField = '';
				}
			}
			*/
			
			//@JAPR 2008-05-21: Agregada la asignación de multiples padres
			/*
			if (array_key_exists('parent_nom_fisicok_join', $featuresDimension))
			{
				$strParentField = $featuresDimension["parent_nom_fisicok_join"];
				if (trim($strParentField) != '')
				{
					$strParentField = ', '.$strParentField.' AS ParentValueID ';
				}
				else 
				{
					$strParentField = '';
				}
			}
			*/

			//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
			$strAttributeField = '';
			if ($appSettings['DimensionAttributes'] == 1)
			{
				//Genera la lista de campos Atributo que se deberán cargar
				if (array_key_exists('attributes', $featuresDimension))
				{
					foreach ($featuresDimension['attributes'] as $anAttributeConsecutive => $anAttributeInfo)
					{
						//Se asume que las dimensiones - atributo siempre son dimensiones sin Llave, puesto que una llave alternativa a la de la 
						//dimensión primaria (o a la Subrogada en todo caso) es innecesaria realmente, por ende sólo agregamos este campo
						$strAttributeFieldName = $anAttributeInfo["attribute_nom_fisicok_bd"];
						if (trim($strAttributeFieldName) != '')
						{
							$strAttributeField .= ', tb.'.$strAttributeFieldName.' AS AttributeValueID'.$anAttributeConsecutive.' ';
						}
					}
				}
			}
			//@JAPR
			
			//@JAPR 2011-06-23: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			$intClaBD = MYSQL;
			$sTopSQL = '';
			$sTopMySQL = '';
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				//Si se carga en forma global, sólo se puede asumir que es un proyecto ligado al Model Manager por lo que busca la definición
				//de conexión default para éste y utiliza el tipo de BD especificado ahí
				$sql = "SELECT CLA_BD FROM SI_CONEXION_BD WHERE CLA_CONEXION = ".DEFAULT_MODEL_MANAGER_CONNECTION;
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_CONEXION_BD ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				if (!$aRS->EOF)
				{
					$intClaBD = $aRS->fields["cla_bd"];
				}
			}
			else
			{
				//Si se carga asociado a un cubo, se utiliza el tipo de base de datos especificado para este
				$aCube = BITAMCube::NewInstanceWithID($aRepository, $aCubeID);
				$intClaBD = $aCube->cla_bd;
			}
			if($bApplyFilters && $anInstance->maxDimensionValueToShow > 0)
			{
				//@JAPRWarning: Se pudo utilizar el método SelectLimit de la conexión, pero noté que para SQL Server carga el total del query y en
				//memoria hace el conteo de registros, así que preferí enviar la cláusula correspondiente desde el propio estatuto SQL
				switch ($intClaBD)
				{
					case SQL_SERVER:
						$sTopSQL = ' TOP '.$anInstance->maxDimensionValueToShow.' ';
						break;
					case MYSQL:
						$sTopMySQL = ' LIMIT '.$anInstance->maxDimensionValueToShow.' ';
						break;
				}
			}
			//@JAPR
			
			//@JAPR 2009-07-10: Agregado el código directo con la librería de queries para que se hagan los Joins necesarios entre dimensiones
			//cuando existe por lo menos un padre (esto servirá para traer la descripción de la Dimensión). Se requiere una referencia a un cubo
			//para proceder de esta manera
			if ($bUseParentsJoin && array_key_exists('parents', $featuresDimension) && count($featuresDimension['parents']) > 0 && $aCubeID > 0)
			{
				// Obtenemos el query para este SnowFlake
				//$thePeriods = BITAMCInfoPeriodCollection::NewInstance($aRepository);
				//@JAPR 2010-07-19: Agregado el parámetro $bCreateQuery para indicar que la instancia de rolap sea generada automáticamente
				$theCube = BITAMCInfoCube::NewInstance($aRepository, $aCubeID, false, true);
				
				// Estamos listos para usar la funcionalidad de ROLAP
				$theCubeRolap = $theCube->rolap;
				
				// Initialize
				$theCubeRolap->Initialize();
				
				//@JAPR 2009-07-16: Agregada la generación de datos para formación de LEFT OUTER JOIN u omitir JOINs innecesarios
				$theCubeRolap->bInfoArrays = true;
				
				//Se establece temporalmente que el cubo no use agregaciones para asegurar que las combinaciones de dimensiones se obtengan de la Tabla de Hechos
				$aux_use_agregation = $theCubeRolap->use_agregation;
				$theCubeRolap->use_agregation = false;
		
				// Como se trata de un SnowFlake, el query no debe utilizar la tabla de hechos
				$theCubeRolap->QuerySinTablaDeHechos = true;
				
				$theCubeRolap->borderbykey = true;
				
				$arrDimTables = array();
				//Dimensión de la cual se obtienen los valores
				$strTableName = $theCube->a_dimensions[$aDimensionID]->physical_table;
				$arrDimTables[$strTableName] = $aDimensionID;
				$theCubeRolap->InsertDimension('['.$strTableName.'].['.	$theCube->a_dimensions[$aDimensionID]->physical_name_descrip.']');
				
				$numDimensions = 1;				
				//Lista de dimensiones padre de SnowFlake de la dimensión de la cual se obtienen los valores
				$arrParentsIDs = array();
				foreach ($featuresDimension['parents'] as $aParentConsecutive => $aParentInfo)
				{
					$strTableName = $theCube->a_dimensions[$aParentConsecutive]->physical_table;
					$arrDimTables[$strTableName] = $aParentConsecutive;
					$theCubeRolap->InsertDimension('['.$strTableName.'].['.	$theCube->a_dimensions[$aParentConsecutive]->physical_name_descrip.']');
					$arrParentsIDs[] = $aParentConsecutive;
					$numDimensions++;
				}
				
				$theCubeRolap->Filter($strFilter);
				$sql = $theCubeRolap->QueryText();
				//use_agregation se restaura a su valor original
				$theCubeRolap->use_agregation = $aux_use_agregation;
				
				$arrQuery = array('SELECT' => '', 'FROM' => '', 'WHERE' => '', 'GROUP' => '', 'HAVING' => '', 'ORDER' => '', 'INSERT' => '');
				$arrQueryText = explode('<BR>', $sql);
				// SELECT (Tantos campos como dimensiones se pidieron agregar * 2 para excluir los padres filtrados si es que hay)
				$elements = explode(',', strpbrk(ltrim($arrQueryText[0]), ' '));
				for ($i = 0; $i < $numDimensions * 2; $i++)
				{
					if($i == 0)
					{
						$arrQuery['SELECT'] .= ', '.$elements[$i].' AS DimensionValueID ';
					}
					elseif($i == 1)
					{
						$arrQuery['SELECT'] .= ', '.$elements[$i].' AS DimensionValueDescription ';
					}
					else
					{
						if(($i % 2) == 0)
						{
							$arrQuery['SELECT'] .= ', '.$elements[$i].' AS ParentValueID'.$arrParentsIDs[($i / 2) - 1].' ';
						}
						else
						{
							$arrQuery['SELECT'] .= ', '.$elements[$i].' AS ParentValueDesc'.$arrParentsIDs[($i / 2) - 1].' ';
						}
					}
				}
				
				$arrQuery['FROM'] = strpbrk(ltrim($arrQueryText[1]), ' ');
				if (substr(strtoupper(trim($arrQueryText[2])),0,6) == 'WHERE ')
				{
					$arrQuery['WHERE'] = strpbrk(ltrim($arrQueryText[2]), ' ');
				}
				
				//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
				//Se debe reemplazar el alias tb ya que como se usó el armador de Queries ROLAP, usa un alias diferente
				if ($strAttributeField != '')
				{
					$strAttributeField = str_replace('tb.', 't'.$featuresDimension["nom_tabla"].'.', $strAttributeField);
				}
				//@JAPR
				
				//@JAPR 2009-07-10: Se prepara el estatuto como un OUTER JOIN para incluir los valores Nulls en los padres también
				if($bIncludeNullParents)
				{
					if(false)		//Método original, funciona para LEFT OUTER JOIN contra un solo padre
					{
						$strFromOrig = substr($arrQuery['FROM'],2,strlen($arrQuery['FROM']));
						$iPos = stripos($strFromOrig, ",", 0);
						$strFrom = substr($strFromOrig, 0, $iPos);
						$strFromParents = strpbrk($strFromOrig, ",");
						$strFromParents = substr($strFromParents,1,strlen($strFromParents));
						$strFrom .= " LEFT OUTER JOIN ".$strFromParents;
						$sql = "SELECT DISTINCT ".$sTopSQL." ".substr($arrQuery['SELECT'],2, strlen($arrQuery['SELECT']))." ".
							"FROM ".$strFrom." ";
						if (trim($arrQuery['WHERE']) != "")
						{
							$sql .= "ON ".$arrQuery['WHERE']." ";
						}
						if($bApplyFilters && trim($anInstance->dimensionValuesFilter) != '' && trim($where) == 0)
						{
							$sql .= ' WHERE t'.$featuresDimension["nom_tabla"].'.'.$featuresDimension["nom_fisico_bd"].' LIKE '.$anInstance->Repository->DataADOConnection->Quote('%'.$anInstance->dimensionValuesFilter.'%');
						}
						
						//Verificamos si estamos modificando un catalogo de ESurvey
					 	if(array_key_exists('is_catalog',$featuresDimension))
					 	{
					 		$blnIsCatalog = $featuresDimension["is_catalog"];
					 		
					 		if($blnIsCatalog)
					 		{
					 			$sql .= " ORDER BY 1";
					 		}
					 		else 
					 		{
					 			$sql .= " ORDER BY 2, 1";
					 		}
					 	}
					 	else 
					 	{
					 		$sql .= " ORDER BY 2, 1";
					 	}

						$sql .= $sTopMySQL;
					}
					else
					{
						//Método nuevo, genera LEFT OUTER JOIN contra tantos padres como se pidieran en el query, agrupando por tabla de dimensión
						$blnFirstTable = true;
						$strFrom = '';
						foreach ($theCubeRolap->arrayTables as $aPosID => $aDimTableName)
						{
							//Sólo se consideran dimensiones que formen parte de las Insertadas, asumiendo que se inserta la jerarquía completa
							//por lo cual no habría forma de dejar huecos, cualquier dimensión regresada en el Generador de queries que no esté en
							//el arreglo del Insert Dimensions se debería de tratar de dimensiones hijas para llegar a la tabla de hechos mas no
							//fueron solicitadas y no se deben procesar
							if(isset($arrDimTables[$aDimTableName]))
							{
								if($blnFirstTable)
								{
									$strFrom = $aDimTableName.' t'.$aDimTableName;
									$blnFirstTable = false;
								}
								else
								{
									$strTableJoins = "";
									if(isset($theCubeRolap->arrayJoins[$aPosID -1]))
									{
										$arrTableJoins = $theCubeRolap->arrayJoins[$aPosID -1];
										if(is_array($arrTableJoins))
										{
											foreach ($arrTableJoins as $aTableJoin)
											{
												$strTableJoins .= "AND ".$aTableJoin;
												//Los joins ya procesados se reemplazan por una constante fácil de ignorar para dejar solo filtros
												$arrQuery['WHERE'] = str_ireplace($aTableJoin, "(1 = 1)", $arrQuery['WHERE']);
											}
											$aTableJoin = strpbrk($aTableJoin, " ");
										}
										else
										{
											$strTableJoins = $arrTableJoins;
											$arrQuery['WHERE'] = str_ireplace($arrTableJoins, "(1 = 1)", $arrQuery['WHERE']);
										}
									}
									//Las tablas posteriores ya se generan como LEFT OUTER JOIN
									$strFrom .= "\r\nLEFT OUTER JOIN ".$aDimTableName." t".$aDimTableName." ON (".$strTableJoins.") ";
								}
							}
						}
						
						//Obtiene sólo los filtros, ignorando los Joins ya que estos fueron agregados como parte del LEFT OUTER JOIN
						$strWhere = '';
						//Elimina los paréntesis agrupadores del WHERE comlpeto para permitir hacer el Split
						$arrQuery['WHERE'] = trim($arrQuery['WHERE']);
						$intStringLen = strlen($arrQuery['WHERE']);
						if(substr($arrQuery['WHERE'],0, 1) == '(')
						{
							$arrQuery['WHERE'] = substr($arrQuery['WHERE'], 1, $intStringLen);
							$intStringLen--;
						}
						if(substr($arrQuery['WHERE'],$intStringLen-1, 1) == ')')
						{
							$arrQuery['WHERE'] = substr($arrQuery['WHERE'], 0, $intStringLen-1);
						}
						
						$arrFilters = spliti(' AND ', ' '.$arrQuery['WHERE']);
						foreach ($arrFilters as $strDimFilter)
						{
							//En caso de que sea un filtro válido, lo agrega al WHERE del estatuto SQL
							if(trim($strDimFilter) != '' && strpos($strDimFilter, "(1 = 1)") === false && strpos($strDimFilter, " = ") === false)
							{
								$strWhere .= 'AND '.$strDimFilter;
							}
						}
						if(trim($strWhere) != '')
						{
							$strWhere = strpbrk($strWhere, ' ');
						}
						
						//Crea el estatuto SQL con los LEFT OUTER JOINs
						if($bApplyFilters && trim($anInstance->dimensionValuesFilter) != '' && trim($where) == 0)
						{
							$strWhere = ' WHERE t'.$featuresDimension["nom_tabla"].'.'.$featuresDimension["nom_fisico_bd"].' LIKE '.$anInstance->Repository->DataADOConnection->Quote('%'.$anInstance->dimensionValuesFilter.'%');
						}
						$sql = "SELECT DISTINCT ".$sTopSQL." ".substr($arrQuery['SELECT'],2, strlen($arrQuery['SELECT'])).$strAttributeField." ".
							"FROM ".$strFrom." ".$strWhere;
							
						//Verificamos si estamos modificando un catalogo de ESurvey
					 	if(array_key_exists('is_catalog',$featuresDimension))
					 	{
					 		$blnIsCatalog = $featuresDimension["is_catalog"];
					 		
					 		if($blnIsCatalog)
					 		{
					 			$sql .= " ORDER BY 1";
					 		}
					 		else 
					 		{
					 			$sql .= " ORDER BY 2, 1";
					 		}
					 	}
					 	else 
					 	{
					 		$sql .= " ORDER BY 2, 1";
					 	}

						$sql .= $sTopMySQL;
					}
				}
				else
				{
					$sql = "SELECT DISTINCT ".$sTopSQL." ".substr($arrQuery['SELECT'],2, strlen($arrQuery['SELECT'])).$strAttributeField." ".
						"FROM ".substr($arrQuery['FROM'],2,strlen($arrQuery['FROM']))." ";
					if($bApplyFilters && trim($anInstance->dimensionValuesFilter) != '' && trim($where) == 0)
					{
						if (trim($arrQuery['WHERE']) != "")
						{
							$arrQuery['WHERE'] .= ' AND ';
						}
						$arrQuery['WHERE'] .= 't'.$featuresDimension["nom_tabla"].'.'.$featuresDimension["nom_fisico_bd"].' LIKE '.$anInstance->Repository->DataADOConnection->Quote('%'.$anInstance->dimensionValuesFilter.'%');
					}
					if (trim($arrQuery['WHERE']) != "")
					{
						$sql .= "WHERE ".$arrQuery['WHERE']." ";
					}
					
					//Verificamos si estamos modificando un catalogo de ESurvey
				 	if(array_key_exists('is_catalog',$featuresDimension))
				 	{
				 		$blnIsCatalog = $featuresDimension["is_catalog"];
				 		
				 		if($blnIsCatalog)
				 		{
				 			$sql .= " ORDER BY 1";
				 		}
				 		else 
				 		{
				 			$sql .= " ORDER BY 2, 1";
				 		}
				 	}
				 	else 
				 	{
				 		$sql .= " ORDER BY 2, 1";
				 	}

					$sql .= $sTopMySQL;
				}
			}
			else
			{
				if (array_key_exists('parents', $featuresDimension))
				{
					foreach ($featuresDimension['parents'] as $aParentConsecutive => $aParentInfo)
					{
						$strParentFieldName = "tb.".$aParentInfo["parent_nom_fisicok_join"];
						if (trim($strParentFieldName) != '')
						{
							$strParentField .= ', '.$strParentFieldName.' AS ParentValueID'.$aParentConsecutive.' ';
						}
					}
				}
			
				if($bApplyFilters && trim($anInstance->dimensionValuesFilter) != '' && trim($where) == 0)
				{
					$where = ' WHERE tb.'.$featuresDimension["nom_fisico_bd"].' LIKE '.$anInstance->Repository->DataADOConnection->Quote('%'.$anInstance->dimensionValuesFilter.'%');
				}
			
				$sql = "SELECT DISTINCT ".$sTopSQL." tb.".$featuresDimension["nom_fisicok_bd"]." AS DimensionValueID, tb.".$featuresDimension["nom_fisico_bd"]." AS DimensionValueDescription ".$strParentField.$strAttributeField.
				" FROM ".$featuresDimension["nom_tabla"]." tb".$where;
				
				//Verificamos si estamos modificando un catalogo de ESurvey
			 	if(array_key_exists('is_catalog',$featuresDimension))
			 	{
			 		$blnIsCatalog = $featuresDimension["is_catalog"];
			 		
			 		if($blnIsCatalog)
			 		{
			 			$sql.=" ORDER BY 1";
			 		}
			 		else 
			 		{
			 			$sql.=" ORDER BY 2";
			 		}
			 	}
			 	else 
			 	{
		 			$sql.=" ORDER BY 2";
			 	}
			 	
			 	$sql.=$sTopMySQL;
			}
			global $queriesLogFile;
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMDimensionValueCollection->NewInstance: \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$featuresDimension["nom_tabla"]." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$blnIsCatalog = false;
		 	if(array_key_exists('is_catalog',$featuresDimension))
		 	{
		 		$blnIsCatalog = $featuresDimension["is_catalog"];
		 	}

			while (!$aRS->EOF)
			{
				//obtener el valor de DimensionValueID
				$tempDimValID = (int)$aRS->fields["dimensionvalueid"];
				
				$anObject = BITAMDimensionValue::NewInstanceFromRS($anInstance->Repository, $aRS, $featuresDimension);
				if (!$anObject->IsEmpty)
				{
					if($blnIsCatalog==false || ($blnIsCatalog==true && $tempDimValID!=1))
					{
						$anInstance->Collection[] = $anObject;
					}
				}
				$aRS->MoveNext();
			}
		}

		return $anInstance;
	}

	static function NewInstanceEmpty($aRepository)
	{
		$anInstance = new BITAMDimensionValueCollection($aRepository);
		
		return $anInstance;
	}
	
	function getIndexByDimValueID($dimValID)
	{	
		$index=null;
		
		if($dimValID==null)
		{
			return $index;
		}
	
		$sizeCollection=count($this->Collection);
		
		for($i=0;$i<$sizeCollection;$i++)
		{
			if($this->Collection[$i]->DimensionValueID == $dimValID)
			{
			 	$index=$i;
			 	break;
			}
		}
		
		return $index;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2009-07-08: Agregados los filtros para la carga de valores de dimensiones
		if(array_key_exists("txtDefDimRec", $aHTTPRequest->GET))
		{
			$this->maxDimensionValueToShow = (int) $aHTTPRequest->GET[txtDefDimRec];
		}
		if(array_key_exists("txtDimFilter", $aHTTPRequest->GET))
		{
			$this->dimensionValuesFilter = $aHTTPRequest->GET["txtDimFilter"];
		}
		if(array_key_exists("chkShowNullParentValues", $aHTTPRequest->GET))
		{
			$this->showNullParentValues = (int) $aHTTPRequest->GET["chkShowNullParentValues"];
		}
		if(array_key_exists("txtDefDimRec", $aHTTPRequest->POST))
		{
			$this->maxDimensionValueToShow = (int) $aHTTPRequest->POST[txtDefDimRec];
		}
		if(array_key_exists("txtDimFilter", $aHTTPRequest->POST))
		{
			$this->dimensionValuesFilter = $aHTTPRequest->POST["txtDimFilter"];
		}
		if(array_key_exists("chkShowNullParentValues", $aHTTPRequest->POST))
		{
			$this->showNullParentValues = (int) $aHTTPRequest->POST["chkShowNullParentValues"];
		}
		//@JAPR
		
		if (array_key_exists("DimensionID", $aHTTPRequest->GET) && array_key_exists("CubeID", $aHTTPRequest->GET))
		{
			$aDimensionID = (int) $aHTTPRequest->GET["DimensionID"];
			$aCubeID = (int) $aHTTPRequest->GET["CubeID"];
		}
		else
		{
			$aDimensionID = 0;
			$aCubeID = 0;
		}

		$anInstance = BITAMDimensionValueCollection::NewInstance($aRepository,null,$aDimensionID,$aCubeID,true, true, true, $this->dimensionValuesFilter, $this->maxDimensionValueToShow, $this->showNullParentValues);
		if (array_key_exists("SetParentValuesIDs", $aHTTPRequest->GET) && (int)$aHTTPRequest->GET["SetParentValuesIDs"] == 1)
		{
			$anInstance->ForParentAssignment = true;
		}
		
		return $anInstance;
	}
	
	function get_Parent()
	{
		global $appSettings;
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			if ($this->cla_descrip > 0)
			{
				return BITAMDimension::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto, false);
			}
			else 
			{
				return $this->Repository;
			}
		}
		else
		{
		*/
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			if (($this->cla_concepto > 0 || $appSettings["UseGlobalDimensions"] == 1) && $this->consecutivo > 0)
			{
				return BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
			}
			else 
			{
				return $this->Repository;
			}
		//}
		//@JAPR
	}
	
	function get_Title()
	{	
		$title = translate("Values");
		
		return $title;
	}

	function get_AddRemoveQueryString()
	{
		global $appSettings;
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$anInstance = BITAMDimension::NewInstanceWithDescripID($this->Repository,$this->cla_descrip,$this->cla_concepto);
		}
		else
		{
		*/
			$anInstance = BITAMDimension::NewInstanceWithID($this->Repository,$this->consecutivo,$this->cla_concepto);
		//}
		//@JAPR
		if (is_null($anInstance))
		{
			return $this->Repository;
		}
		
		//Si se trata de una Dimensión - Atributo, no se pueden agregar valores directamente a ella así que se invoca a la dimensión a quien
		//pertenece la instancia en este cubo
		if ($anInstance->IsAttribute)
		{
			$anInstance = BITAMDimension::NewInstanceWithDescripID($this->Repository, $anInstance->BaseDimensionKey, $this->cla_concepto, false);
			if (!is_null($anInstance))
			{
				return "BITAM_PAGE=DimensionValue&CubeID=".$anInstance->cla_concepto.'&DimensionID='.$anInstance->consecutivo;
			}
			else 
			{
				return $this->Repository;
			}
		}
		else
		{
			return "BITAM_PAGE=DimensionValue&CubeID=".$this->cla_concepto.'&DimensionID='.$this->consecutivo;
		}
		//return "BITAM_PAGE=DimensionValue&CubeID=".$this->cla_concepto.'&DimensionID='.$this->consecutivo;
	}
	
	function get_QueryString()
	{
		return "BITAM_SECTION=DimensionValueCollection";
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_SECTION=DimensionValue";
	}

	function get_FormIDFieldName()
	{
		return 'DimensionValueID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
		$bSameKeyDescField = strtolower($aDimension->nom_fisico) == (strtolower($aDimension->nom_fisicok));
		$intSize = 1;
		switch ($this->tipo_datok)
		{
			case 1:
			case 2:
				$strDataType = "String";
				$intSize = $aDimension->long_datok;
				break;
			case 8:
			case 21:
			case 23:
			case 24:
				$strDataType = "Float";
				break;
			case 12:
			case 22:
				$strDataType = "DateTime";
				break;
			default:
				$strDataType = "Integer";
				break;
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DimensionValueID";
		$aField->Title = translate("ID");
		$aField->Type = $strDataType;
		if ($strDataType == 'String')
		{
			$aField->Size = $intSize;
		}
		else 
		{
			$aField->Size = 25;
		}
		//@JAPR 2010-02-19: Validado que sólo aparezca el campo ID si es diferente del Descriptor
		if (!$bSameKeyDescField)
		{
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		$intSize = 1;
		switch ($this->tipo_dato)
		{
			case 1:
			case 2:
				$strDataType = "String";
				$intSize = $aDimension->long_dato;
				break;
			default:
				$strDataType = "Integer";
				break;
		}
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Description";
		$aField->Title = translate("Description");
		$aField->Type = $strDataType;
		if ($strDataType == 'String')
		{
			$aField->Size = $intSize;
		}
		else 
		{
			$aField->Size = 25;
		}
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		if (!$aDimension->isCatalog)
		{
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		$featuresDimension = BITAMDimension::getFeaturesDimension($this->Repository, $this->consecutivo, $this->cla_concepto);
		//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
		if (array_key_exists('attributes', $featuresDimension))
		{
			foreach ($featuresDimension['attributes'] as $anAttributeConsecutive => $anAttributeInfo)
			{
				$intSize = 1;
				switch ($anAttributeInfo["attribute_tipo_datok"])
				{
					case 1:
					case 2:
						$strDataType = "String";
						$intSize = 60;
						break;
					default:
						$strDataType = "Integer";
						break;
				}
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "AttributeValueID".$anAttributeConsecutive;
				$aField->Title = $anAttributeInfo["attribute_nom_logico"];
				$aField->Type = $strDataType;
				if ($strDataType == 'String')
				{
					$aField->Size = $intSize;
				}
				else 
				{
					$aField->Size = 25;
				}
				$myFields[$aField->Name] = $aField;
			}
		}
		//@JAPR
		
		if (array_key_exists('parents', $featuresDimension))
		{
			foreach ($featuresDimension['parents'] as $aParentConsecutive => $aParentInfo)
			{
				$intSize = 1;
				switch ($featuresDimension["tipo_dato"])
				{
					case 1:
					case 2:
						$strDataType = "String";
						$intSize = 60;
						break;
					default:
						$strDataType = "Integer";
						break;
				}
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "ParentValueDesc".$aParentConsecutive;
				$aField->Title = $aParentInfo["parent_nom_logico"];
				$aField->Type = $strDataType;
				if ($strDataType == 'String')
				{
					$aField->Size = $intSize;
				}
				else 
				{
					$aField->Size = 25;
				}
				$myFields[$aField->Name] = $aField;
			}
		}
		
		return $myFields;
	}
	
	//@JAPR 2008-11-24: Agregada esta propiedad para casos de dimensiones tipo SmallMoney o Flotantes que no pueden utilizar sus claves
	//en llamadas a métodos como array_key_exists que no aceptan dichos tipos de dato como parámetro
	function mustForceStringKeys()
	{
		$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
		if (strpos('|'.tDATE.'|'.tSMALLMONEY.'|'.tREAL.'|'.tDECIMAL.'|', '|'.$aDimension->tipo_datok.'|'))
		{
			return true;
		}
		return false;
	}
	//@JAPR
	
 	function addButtons($aUser)
	{
		global $maxDimensionRecords;
		global $dimValuesFilter;
		global $bShowNullParentValues;
		global $appSettings;
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if (!$this->ForParentAssignment && ($this->cla_concepto > 0 || $appSettings["UseGlobalDimensions"] == 1) && $this->consecutivo > 0)
		{
			$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
			if (!is_null($aDimension))
			{
				//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
				//Se agregó la condición para mostrar esta opción si se trata de una dimensión con Atributos
				if (count($aDimension->FatherParDescriptor) > 0 || $aDimension->hasAttributes)
				{
	?>
		<!--<a href="#" class="alinkescfav" onclick="setParentValuesIDs();"><img src="images/parentchild5.gif"> <?=translate("Tabular entry")?> </a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp-->
		<button id="btnSetParentValuesIDs" class="alinkescfav" onclick="setParentValuesIDs();"><img src="images/parentchild5.gif" alt="<?=translate('Tabular entry')?>" title="<?=translate('Tabular entry')?>" displayMe="1" /> <?=translate("Tabular entry")?></button>
	<?
				}
				//@JAPR
	?>
		<?=translate("Max records")?>:&nbsp;&nbsp;<input type="text" id="txtDefDimRecCapt" name="txtDefDimRecCapt" size="5" maxlength="5" value="<?=$maxDimensionRecords?>"></input>
	<?
				//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
				if (!$aDimension->isCatalog)
				{
	?>
		<?=translate("Filter")?>:&nbsp;&nbsp;
	<?
				}
				//@JAPR
	?>
		<input type="text" id="txtDimFilterCapt" name="txtDimFilterCapt" size="50" value="<?=$dimValuesFilter?>" <?=($aDimension->isCatalog)?" style=\"display:none\"":""?>></input>
	<?
				//@JAPR
	?>
		<!--<a href="#" class="alinkescfav" onclick="saveDimensionSettings();"><img src="images/refresh.gif"> <?=translate("Apply filter")?> </a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp-->
		<button id="btnSaveDimSet" class="alinkescfav" onclick="saveDimensionSettings();"><img src="images/refresh.gif" alt="<?=translate('Apply filter')?>" title="<?=translate('Apply filter')?>" displayMe="1" /> <?=translate("Apply filter")?></button>
	<?
				//@JAPR 2012-07-12: Se ocultará este botón para asignar Padres Nulos porque este día se encontró que faltaba el archivo
				//requerido para cargar esa ventana, así que se delegará la captura de Padres al Dimension Tools que al final es el único que
				//los podría crear a la fecha desde KPIOnline
				if (count($aDimension->FatherParDescriptor) > 0)
				{
		/*<a href="#" class="alinkescfav" onclick="setNullParentValues();"><img src="images/associate.png"> <?=translate("Set null parents")?> </a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		*/
	?>
		&nbsp;&nbsp;<input type="checkbox" id="chkShowNullParentValuesCapt" name="chkShowNullParentValuesCapt" " onclick="saveDimensionSettings();" <?=(($bShowNullParentValues)?"checked":"")?>><?=translate("Show null parents")?></input>
	<?
				}
			}
		}
	}	
	
	function canRemove($aUser)
	{
		global $appSettings;
		
		//@JAPR 2010-07-13: Modificada temporalmente la validación para NO permitir remover valores de dimensiones mientras no se determine como
		//actuar en la tabla de hechos del cubo para no causar inconsistencias
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
			if (!is_null($aDimension))
			{
				//De una dimensión catálogo si se puede eliminar un registro
				if ($aDimension->isCatalog)
				{
					return true;
				}
			}
		}
		return false;
		
		//@JAPR 2010-01-27: Modificada la restricción para remover valores a la siguiente regla:
		// - Si la dimensión es un Atributo, NO se pueden remover valores ya que hay múltiples registros que los contienen y esto afectaría 
		//		a la dimensión primaria del catálogo
		// - Si la dimensión está ligada directamente al cubo y tiene llave Subrogada, ya que posiblemente tenga una Foreign Key y no nos dejaría
		//		de todas formas
		$aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
		if (is_null($aDimension) || $aDimension->IsAttribute || ($aDimension->dependsOnDimension == 0 && ($aDimension->nomfisicok_descripkeys != '' && $aDimension->nom_fisicok_bd != $aDimension->nomfisicok_descripkeys)))
		{
			return false;
		}
		return true;
	}
	 
	function canAdd($aUser)
	{
		return true;
	}
	
	function canEdit($aUser)
	{
		return true;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

    function generateBeforeFormCode($aUser)
    {
?>
		<script language="javascript" src="js/dialogs.js"></script>
	 	<script language="JavaScript">

		function setParentValuesIDs()
		{
			/*
			var answer = confirm('Do you want to close all selected Budget Periods?');
			if (answer)
			{
			*/
			frmSetParentValuesIDs.submit();
			/*
			}
			*/
		}
	 	
		function saveDimensionSettings()
		{
			var aObject = document.getElementById("chkShowNullParentValuesCapt");
			
			frmSaveDimenSettings.txtDimFilter.value = document.getElementById("txtDimFilterCapt").value;
			frmSaveDimenSettings.txtDefDimRec.value = document.getElementById("txtDefDimRecCapt").value;
			if(aObject != null && aObject != undefined)
			{
				if(aObject.checked)
				{
					frmSaveDimenSettings.chkShowNullParentValues.value = "1";
				}
				else
				{
					frmSaveDimenSettings.chkShowNullParentValues.value = "0"
				}
			}
			frmSaveDimenSettings.submit();
		}
		
		function setNullParentValues()
		{
			//var ok = showModalDialog('dialog.html', '<?='main.php?action=select&BITAM_PAGE=DimensionParents&CubeID='.$this->cla_concepto.'&DimensionID='.$this->consecutivo?>', 'dialogHeight:465px; dialogWidth:720px; help: no; status: no; scroll: yes; resizable: yes;')
			openWindowDialog('dialog.html', '<?='main.php?action=select&BITAM_PAGE=DimensionParents&CubeID='.$this->cla_concepto.'&DimensionID='.$this->consecutivo?>', [], 720, 465, setNullParentValuesDone, 'setNullParentValuesDone');
			/*
			if (ok === 1)
			{
				document.location.reload();
			}
			*/
		}
		
		function setNullParentValuesDone(sValue, sParams)
		{
			var ok = sValue;

			if (ok === 1)
			{
				document.location.reload();
			}
		}
		</script> 		
<?
    }

    function generateAfterFormCode($aUser)
    {
?>
		<form name="frmSetParentValuesIDs" action="main.php?BITAM_PAGE=DimensionValue&SetParentValuesIDs=1&CubeID=<?=$this->cla_concepto?>&DimensionID=<?=$this->consecutivo?>" method="post" target="body">
		<input type="hidden" name="BITAM_PAGE" value="DimensionValueCollection">
		</form>
		
		<form name="frmSaveDimenSettings" action="main.php?BITAM_PAGE=Dimension&CubeID=<?=$this->cla_concepto?>&consecutivo=<?=$this->consecutivo?>" method="post" target="body">
		<input type="hidden" name="BITAM_PAGE" value="DimensionValueCollection">
		<input type="hidden" name="txtDimFilter">
		<input type="hidden" name="txtDefDimRec">
		<input type="hidden" name="chkShowNullParentValues" value="0">
		</form>
<?
    }

}

function PartAWSAdd($sText)
{
	$strReplaced=$sText;
	$strReplaced=str_replace('\'','#AWECom#',$strReplaced);
	$strReplaced=str_replace('"','#AWEDCom#',$strReplaced);
	//$strReplaced=str_replace(chr(10),AWSc10,$strReplaced);
	//$strReplaced=str_replace(chr(13),AWSc13,$strReplaced);
	//$strReplaced=str_replace('Æ',AWSDAE,$strReplaced);
	return $strReplaced;
}

function PartAWSRemove($sText)
{
	$strReplaced=$sText;
	$strReplaced=str_replace('#AWECom#','\'',$strReplaced);
	$strReplaced=str_replace('#AWEDCom#','"',$strReplaced);
	//$strReplaced=str_replace(chr(10),AWSc10,$strReplaced);
	//$strReplaced=str_replace(chr(13),AWSc13,$strReplaced);
	//$strReplaced=str_replace('Æ',AWSDAE,$strReplaced);
	return $strReplaced;
}

?>