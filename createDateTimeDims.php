<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

$strOriginalWD = getcwd();

//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("BITAM_UserID");
//session_register("BITAM_UserName");
//session_register("BITAM_RepositoryName");
$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

require_once("../model_manager/filescreator.inc.php");
require_once("../model_manager/modeldimension.inc.php");
require_once("../model_manager/modeldata.inc.php");
global $generateXLS;
$generateXLS = false;
foreach ($surveyCollection->Collection as $surveyInstance) 
{
	echo("<BR>***** "."Survey: ".$surveyInstance->SurveyName);
	//Crear dimensión Start Date
	if(is_null($surveyInstance->StartDateDimID) || $surveyInstance->StartDateDimID<=0)
	{
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		$dimensionName = "Start Date";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//Verificamos si hay dimension Start Date existente
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($aRepository, "StartDateDimID", $dimensionName);
		
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		$aStartDateDimID = 0;
		if($cla_descrip!=-1)
		{
			echo("<BR>"."Dimensión Start Date ya existía, se reutilizará");
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aStartDateDimID = $cla_descrip;
			}
			else {
				echo("<BR>".translate("Error adding Start Date dimension").": ".$strMMError);
			}
		}
		else 
		{
			echo("<BR>"."Crear Dimensión Start Date");
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aStartDateDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				echo("<BR>".translate("Error creating Start Date dimension").": ".$strMMError);
			}
		}
		
		$sql = "UPDATE SI_SV_Survey SET StartDateDimID = ".$aStartDateDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		echo("<BR>"."sql:"."<BR>".$sql);
		
		$surveyInstance->StartDateDimID = $aStartDateDimID;
	}

	//Crear dimensión End Date
	if(is_null($surveyInstance->EndDateDimID) || $surveyInstance->EndDateDimID<=0)
	{
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		//Crear la dimension EndDate
		$dimensionName = "End Date";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//Verificamos si hay dimension End Date existente
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($aRepository, "EndDateDimID", $dimensionName);
		
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		$anEndDateDimID = 0;
		if($cla_descrip!=-1)
		{
			echo("<BR>"."Dimensión End Date ya existía, se reutilizará");
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$anEndDateDimID = $cla_descrip;
			}
			else {
				echo("<BR>".translate("Error adding End Date dimension").": ".$strMMError);
			}
		}
		else 
		{
			echo("<BR>"."Crear Dimensión End Date");
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$anEndDateDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				echo("<BR>".translate("Error creating End Date dimension").": ".$strMMError);
			}
		}
		
		$sql = "UPDATE SI_SV_Survey SET EndDateDimID = ".$anEndDateDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		echo("<BR>"."sql:"."<BR>".$sql);
		
		$surveyInstance->EndDateDimID = $anEndDateDimID;
	}
	
	//Crear dimensión Start Time
	if(is_null($surveyInstance->StartTimeDimID) || $surveyInstance->StartTimeDimID<=0)
	{	
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		//Crear la dimension StartTime
		$dimensionName = "Start Time";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//Verificamos si hay dimension Start Time existente
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($aRepository, "StartTimeDimID", $dimensionName);
		
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		$aStartTimeDimID = 0;
		if($cla_descrip!=-1)
		{
			echo("<BR>"."Dimensión Start Time ya existía, se reutilizará");
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aStartTimeDimID = $cla_descrip;
			}
			else {
				echo("<BR>".translate("Error adding Start Time dimension").": ".$strMMError);
			}
		}
		else 
		{
			echo("<BR>"."Crear Dimensión Start Time");
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aStartTimeDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				echo("<BR>".translate("Error creating Start Time dimension").": ".$strMMError);
			}
		}
		
		$sql = "UPDATE SI_SV_Survey SET StartTimeDimID = ".$aStartTimeDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		echo("<BR>"."sql:"."<BR>".$sql);
		
		$surveyInstance->StartTimeDimID = $aStartTimeDimID;
	}
		
	//Crear dimensión End Time
	if(is_null($surveyInstance->EndTimeDimID) || $surveyInstance->EndTimeDimID<=0)
	{		
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		$dimensionName = "End Time";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//Verificamos si hay dimension End Time existente
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($aRepository, "EndTimeDimID", $dimensionName);
		
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		$anEndTimeDimID = 0;
		if($cla_descrip!=-1)
		{
			echo("<BR>"."Dimensión End Time ya existía, se reutilizará");
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$anEndTimeDimID = $cla_descrip;
			}
			else {
				echo("<BR>".translate("Error adding End Time dimension").": ".$strMMError);
			}
		}
		else 
		{
			echo("<BR>"."Crear Dimensión End Time");
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$anEndTimeDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				echo("<BR>".translate("Error creating End Time dimension").": ".$strMMError);
			}
		}
		
		$sql = "UPDATE SI_SV_Survey SET EndTimeDimID = ".$anEndTimeDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		echo("<BR>"."sql:"."<BR>".$sql);
		
		$surveyInstance->EndTimeDimID = $anEndTimeDimID;
	}
	
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	//Crear dimensión Sync Date
	if(is_null($surveyInstance->SyncDateDimID) || $surveyInstance->SyncDateDimID<=0)
	{
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		$dimensionName = "Sync Date";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//Verificamos si hay dimension Sync Date existente
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($aRepository, "SyncDateDimID", $dimensionName, true);
		
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		$aSyncDateDimID = 0;
		if($cla_descrip!=-1)
		{
			echo("<BR>"."Dimensión Sync Date ya existía, se reutilizará");
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aSyncDateDimID = $cla_descrip;
			}
			else {
				echo("<BR>".translate("Error adding Sync Date dimension").": ".$strMMError);
			}
		}
		else 
		{
			echo("<BR>"."Crear Dimensión Sync Date");
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aSyncDateDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				echo("<BR>".translate("Error creating Sync Date dimension").": ".$strMMError);
			}
		}
		
		$sql = "UPDATE SI_SV_Survey SET SyncDateDimID = ".$aSyncDateDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		echo("<BR>"."sql:"."<BR>".$sql);
		
		$surveyInstance->SyncDateDimID = $aSyncDateDimID;
	}

	//Crea la columna en la tabla paralela
	$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD SyncDate VARCHAR(64)";
	@$aRepository->DataADOConnection->Execute($sql);
	echo("<BR>"."sql:"."<BR>".$sql);
	
	//Crear dimensión Sync Time
	if(is_null($surveyInstance->SyncTimeDimID) || $surveyInstance->SyncTimeDimID<=0)
	{	
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		$dimensionName = "Sync Time";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//Verificamos si hay dimension Sync Time existente
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($aRepository, "SyncTimeDimID", $dimensionName, true);
		
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		$aSyncTimeDimID = 0;
		if($cla_descrip!=-1)
		{
			echo("<BR>"."Dimensión Sync Time ya existía, se reutilizará");
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aSyncTimeDimID = $cla_descrip;
			}
			else {
				echo("<BR>".translate("Error adding Sync Time dimension").": ".$strMMError);
			}
		}
		else 
		{
			echo("<BR>"."Crear Dimensión Sync Time");
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aSyncTimeDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				echo("<BR>".translate("Error creating Sync Time dimension").": ".$strMMError);
			}
		}
		
		$sql = "UPDATE SI_SV_Survey SET SyncTimeDimID = ".$aSyncTimeDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		echo("<BR>"."sql:"."<BR>".$sql);
		
		$surveyInstance->SyncTimeDimID = $aSyncTimeDimID;
	}
	
	//Crea la columna en la tabla paralela
	$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD SyncTime VARCHAR(64)";
	@$aRepository->DataADOConnection->Execute($sql);
	echo("<BR>"."sql:"."<BR>".$sql);
	
	//Estas dimensiones NO deben actualizar datos en las tablas de cubos porque no sabemos en realidad cuando fueron sincronizadas,
	//se dejarán NULLs por ahora para indicar este hecho
	//@JAPR
	
	//Insertar elementos faltantes a los catálogos de las nuevas dimensiones Start Date, End Date, Start Time, End Time
	
	//Insertar elementos faltantes al catálogo de la dimensión Start Date
	
	echo("<BR>Insertar elementos faltantes al catálogo de la dimensión Start Date:<BR>");
	
	$startDateCatalog = "RIDIM_".$surveyInstance->StartDateDimID;
	$startDateDescField = "DSC_".$surveyInstance->StartDateDimID;
	$startDateDimField = $startDateCatalog."KEY";
	
	$sqlInsert = "INSERT INTO ".$startDateCatalog." (".$startDateDescField.")
					SELECT DISTINCT SUBSTRING(DateID, 1, 10)
					FROM ".$surveyInstance->SurveyTable."
					WHERE SUBSTRING(DateID, 1, 10) NOT IN (SELECT ".$startDateDescField." FROM ".$startDateCatalog.")";
	
	echo("<BR>".$sqlInsert);
	
	if ($aRepository->DataADOConnection->Execute($sqlInsert) === false)
	{
		die( translate("Error accessing")." ".$startDateCatalog." OR ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlInsert);
	}
	
	//Insertar elementos faltantes al catálogo de la dimensión End Date
	
	echo("<BR>Insertar elementos faltantes al catálogo de la dimensión End Date:<BR>");
	
	$endDateCatalog = "RIDIM_".$surveyInstance->EndDateDimID;
	$endDateDescField = "DSC_".$surveyInstance->EndDateDimID;	
	$endDateDimField = $endDateCatalog."KEY";

	$sqlInsert = "INSERT INTO ".$endDateCatalog." (".$endDateDescField.")
					SELECT DISTINCT SUBSTRING(DateID, 1, 10)
					FROM ".$surveyInstance->SurveyTable."
					WHERE SUBSTRING(DateID, 1, 10) NOT IN (SELECT ".$endDateDescField." FROM ".$endDateCatalog.")";
	
	echo("<BR>".$sqlInsert);
	
	if ($aRepository->DataADOConnection->Execute($sqlInsert) === false)
	{
		die( translate("Error accessing")." ".$endDateCatalog." OR ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlInsert);
	}

	//Insertar elementos faltantes al catálogo de la dimensión Start Time
	
	echo("<BR>Insertar elementos faltantes al catálogo de la dimensión Start Time:<BR>");
	
	$startTimeCatalog = "RIDIM_".$surveyInstance->StartTimeDimID;
	$startTimeDescField = "DSC_".$surveyInstance->StartTimeDimID;
	$startTimeDimField = $startTimeCatalog."KEY";

	$sqlInsert = "INSERT INTO ".$startTimeCatalog." (".$startTimeDescField.")
					SELECT DISTINCT StartTime
					FROM ".$surveyInstance->SurveyTable."
					WHERE StartTime NOT IN (SELECT ".$startTimeDescField." FROM ".$startTimeCatalog.")";
	
	echo("<BR>".$sqlInsert);
	
	if ($aRepository->DataADOConnection->Execute($sqlInsert) === false)
	{
		die( translate("Error accessing")." ".$startTimeCatalog." OR ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlInsert);
	}
	
	//Insertar elementos faltantes al catálogo de la dimensión End Time	
	
	echo("<BR>Insertar elementos faltantes al catálogo de la dimensión End Time:<BR>");
	
	$endTimeCatalog = "RIDIM_".$surveyInstance->EndTimeDimID;
	$endTimeDescField =  "DSC_".$surveyInstance->EndTimeDimID;
	$endTimeDimField = $endTimeCatalog."KEY";

	$sqlInsert = "INSERT INTO ".$endTimeCatalog." (".$endTimeDescField.")
					SELECT EndTime
					FROM ".$surveyInstance->SurveyTable. "
					WHERE EndTime NOT IN (SELECT ".$endTimeDescField." FROM ".$endTimeCatalog.")";
	
	echo("<BR>".$sqlInsert);
	
	if ($aRepository->DataADOConnection->Execute($sqlInsert) === false)
	{
		die( translate("Error accessing")." ".$endTimeCatalog." OR ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlInsert);
	}

	//Asignar valores a las dimensiones Start Date, End Date, Start Time, End Time en la tabla de hechos de la encuesta
	
	echo("<BR>Asignar valores a las dimensiones Start Date, End Date, Start Time, End Time en la tabla de hechos de la encuesta:");
	
	$factSurvey = "RIFACT_".$surveyInstance->ModelID; //Tabla de hechos de la encuesta
	
	$sqlUpdate = "UPDATE ".$factSurvey." FCT,
					".$surveyInstance->SurveyTable." SV,
					".$startDateCatalog." SD,
					".$endDateCatalog." ED,
					".$startTimeCatalog." ST,
					".$endTimeCatalog." ET
					SET FCT.".$startDateDimField." = SD.".$startDateDimField.",
					FCT.".$endDateDimField." = ED.".$endDateDimField.",
					FCT.".$startTimeDimField." = ST.".$startTimeDimField.",
					FCT.".$endTimeDimField." = ET.".$endTimeDimField."
					WHERE FCT.FactKey = SV.FactKey AND
					SUBSTRING(SV.DateID,1,10) = SD.".$startDateDescField." AND
					SUBSTRING(SV.DateID,1,10) = ED.".$endDateDescField." AND
					SV.StartTime = ST.".$startTimeDescField." AND
					SV.EndTime = ET.".$endTimeDescField."
					AND (FCT.".$startDateDimField." IS NULL OR
					FCT.".$endDateDimField." IS NULL OR 
					FCT.".$startTimeDimField." IS NULL OR
					FCT.".$endTimeDimField." IS NULL)";
	
	echo("<BR>".$sqlUpdate);
	
	if ($aRepository->DataADOConnection->Execute($sqlUpdate) === false)
	{
		die( translate("Error accessing")." ".$factSurvey." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpdate);
	}
}
?>