<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("question.inc.php");
require_once("paginator.inc.php");

$svStartDate=null;
$svEndDate=null;
$svUserID=null;
$svIpp=null;

class BITAMPushMessage extends BITAMObject
{
	public $MessageID;
	public $UserID;
	public $UserIDs;				//Array temporal usado sólo en el grabado si se elige como usuario Customized
	public $UserName;
	public $CreationUserID;
	public $CreationDateID;
	public $Title;
	public $PushMessage;
	public $State;
	public $StateOld;
	public $DeviceIDs;
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->MessageID = -1;
		$this->UserID = -1;
		$this->UserIDs = array();
		$this->UserName = "";
		$this->CreationUserID = -1;
		$this->CreationDateID = "";
		$this->Title = '';
		$this->PushMessage = "";
		$this->State = 0;
		$this->StateOld = 0;
		$this->DeviceIDs = array();
		$this->MsgConfirmRemove = "Do you wish to delete this entry?";
	}

	static function NewInstance($aRepository)
	{
		return new BITAMPushMessage($aRepository);
	}

	static function NewInstanceWithID($aRepository, $aMessageID, $bGetValues = true)
	{
		$anInstance = null;
		if ((int)$aMessageID < 0)
		{
			return $anInstance;
		}
		$anInstance = BITAMPushMessage::NewInstance($aRepository);
		
		//Obtener los datos de la dimension Usuario
		$aSurvey = BITAMSurvey::NewInstance($aRepository);
		if (is_null($aSurvey)) {
			return null;
		}
		$aSurvey->readGblSurveyModel();
		if ($aSurvey->GblUserDimID <= 0) {
			return null;
		}
		
		$fieldEmailKey = "RIDIM_".$aSurvey->GblUserDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->GblUserDimID;
		
		$sql = "SELECT A.MessageID, A.UserID, C.{$fieldEmailDesc} AS UserName, A.CreationUserID, A.CreationDateID, A.Title, A.PushMessage, A.State ";
		$where = '';
		$sql .= " FROM SI_SV_PushMessage A, RIDIM_{$aSurvey->GblUserDimID} C ";
		//Ligamos las Tablas para lo de Email
		$where.=($where!=""?" AND ":"")."A.UserID = C.{$fieldEmailKey} AND A.MessageID = {$aMessageID}";
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMessage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMPushMessage::NewInstanceFromRS($aRepository, $aRS, $bGetValues);
		}
		
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS, $bGetValues = true)
	{
		$anInstance = BITAMPushMessage::NewInstance($aRepository);
		$anInstance->MessageID = (int) @$aRS->fields["messageid"];
		$anInstance->CreationDateID = (string) @$aRS->fields["creationdateid"];
		$anInstance->CreationUserID = (int) @$aRS->fields["creationuserid"];
		$anInstance->UserID = (int) @$aRS->fields["userid"];
		$anInstance->UserName = (int) @$aRS->fields["username"];
		$anInstance->Title = (string) @$aRS->fields["title"];
		$anInstance->PushMessage = (string) @$aRS->fields["pushmessage"];
		$anInstance->State = (int) @$aRS->fields["state"];
		$anInstance->StateOld = $anInstance->State;
        
		if ($bGetValues) {
			$anInstance->getDevices();
		}
		
		return $anInstance;
	}
	
	//Obtiene la colección de dispositivos a los que se envió este mensaje
	function getDevices() {
		$this->DeviceIDs = array();
		
		$sql = "SELECT DeviceID FROM SI_SV_PushMessageDevices WHERE MessageID = {$this->MessageID}";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_PushMessageDevices ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF) {
			$intDeviceID = (int) @$aRS->fields["deviceid"];
			if ($intDeviceID > 0) {
				$this->DeviceIDs[] = $intDeviceID;
			}
			$aRS->MoveNext();
		}
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		global $svStartDate;
		if(array_key_exists("startDate", $aHTTPRequest->GET))
		{
			$svStartDate = $aHTTPRequest->GET["startDate"];
		}

		global $svEndDate;
		if(array_key_exists("endDate", $aHTTPRequest->GET))
		{
			$svEndDate = $aHTTPRequest->GET["endDate"];
		}

		global $svUserID;
		if(array_key_exists("userID", $aHTTPRequest->GET))
		{
			$svUserID = $aHTTPRequest->GET["userID"];
		}
		
		global $svIpp;
		if(array_key_exists("ipp", $aHTTPRequest->GET))
		{
			$svIpp = $aHTTPRequest->GET["ipp"];
		}

		if (array_key_exists("MessageID", $aHTTPRequest->POST))
		{
			//@JAPR 2013-11-05: Agregado el borrado incremental de agregaciones (cancela los valores actuales)
			$arrDates = array();
			$aMessageID = $aHTTPRequest->POST["MessageID"];
			if (is_array($aMessageID))
			{
				if(array_key_exists("startDate", $aHTTPRequest->POST))
				{
					$svStartDate = $aHTTPRequest->POST["startDate"];
				}
				
				if(array_key_exists("endDate", $aHTTPRequest->POST))
				{
					$svEndDate = $aHTTPRequest->POST["endDate"];
				}
				
				if(array_key_exists("userID", $aHTTPRequest->POST))
				{
					$svUserID = $aHTTPRequest->POST["userID"];
				}
				
				if(array_key_exists("ipp", $aHTTPRequest->POST))
				{
					$svIpp = $aHTTPRequest->POST["ipp"];
				}
				
				$aCollection = BITAMPushMessageCollection::NewInstanceForRemove($aRepository, $aMessageID, $svStartDate, $svEndDate, $svUserID, $svIpp);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$strDateID = trim((string) @$anInstanceToRemove->DateID);
					if ($strDateID != '') {
						$strDateID = (string) @substr($strDateID, 0, 10);
						$arrDates[$strDateID] = $strDateID;
					}
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMPushMessage::NewInstanceWithID($aRepository, (int) $aMessageID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMPushMessage::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMPushMessage::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("MessageID", $aHTTPRequest->GET))
		{
			$aMessageID = (int) @$aHTTPRequest->GET["MessageID"];
			$anInstance = BITAMPushMessage::NewInstanceWithID($aRepository, $aMessageID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMPushMessage::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMPushMessage::NewInstance($aRepository);
		}

		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("UserID", $anArray))
		{
			$this->UserID = (int)$anArray["UserID"];
		}
		
		if (array_key_exists("UserIDs", $anArray))
		{
			$this->UserIDs = $anArray["UserIDs"];
		}
		
		if (array_key_exists("DeviceIDs", $anArray))
		{
			$this->DeviceIDs = $anArray["DeviceIDs"];
		}
		
		if (array_key_exists("CreationUserID", $anArray))
		{
			$this->CreationUserID = (int)$anArray["CreationUserID"];
		}
		
		if (array_key_exists("CreationDateID", $anArray))
		{
			$this->CreationDateID = (string)$anArray["CreationDateID"];
		}
		
		if (array_key_exists("Title", $anArray))
		{
			$this->Title = (string)$anArray["Title"];
		}
		
		if (array_key_exists("PushMessage", $anArray))
		{
			$this->PushMessage = (string)$anArray["PushMessage"];
		}
		
		if (array_key_exists("State", $anArray))
		{
			$this->State = (int)$anArray["State"];
		}
	}
	
	function save()
	{
		$aHTTPRequest = BITAMHTTPRequest::NewHTTPRequest();
		if ($this->isNewObject() && count($this->UserIDs) > 0) {
			//En este caso que sólo puede tratarse de un mensaje con destinos personalizados, se genera un mensaje por cada usuario seleccionado
			foreach ($this->UserIDs as $intUserID) {
				$anInstance = BITAMPushMessage::NewInstance($this->Repository);
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->UserIDs = array();
				$anInstance->UserID = $intUserID;
				//Se limpia la colección de dispositivos para forzar a que se envíe el mensaje a todos los que tenga disponibles cada usuario
				$anInstance->DeviceIDs = array();
				$anInstance->save();
				
				//Para que redireccione al último de los mensajes si es que se envió a múltiples usuarios
				$this->MessageID = $anInstance->MessageID;
			}
			return;
		}
		
		$blnSendPush = false;
		$blnNewObject = false;
	 	if ($this->isNewObject())
		{
			$blnNewObject = true;
			$blnSendPush = true;
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(MessageID)", "0")." + 1 AS MessageID".
						" FROM SI_SV_PushMessage";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die( translate("Error accessing")." SI_SV_PushMessage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->CreationDateID = date("Y-m-d H:i:s");
			$this->MessageID = (int) @$aRS->fields["messageid"];
			$sql = "INSERT INTO SI_SV_PushMessage (".
						" MessageID".
			            ", CreationDateID".
			            ", CreationUserID".
			            ", UserID".
						", State".
						", Title".
						", PushMessage".
			            ") VALUES (".
			            $this->MessageID.
			            ",".$this->Repository->DataADOConnection->DBTimeStamp($this->CreationDateID).
						",".$_SESSION["PABITAM_UserID"].
						",".$this->UserID.
						",".$this->State.
						",".$this->Repository->DataADOConnection->Quote($this->Title).
			            ",".$this->Repository->DataADOConnection->Quote($this->PushMessage).
					")";

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_PushMessage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
        } else {
        	$blnSendPush = ($this->State && !$this->StateOld);
            $sql = "UPDATE SI_SV_PushMessage SET ".
					"Title = ".$this->Repository->DataADOConnection->Quote($this->Title).
					", PushMessage = ".$this->Repository->DataADOConnection->Quote($this->PushMessage).
					", State = ".$this->State.
					" WHERE MessageID = ".$this->MessageID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_PushMessage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
        }
        
        //Graba la colección de dispositivos a los que se envió el mensaje, si no había ninguno asignado entonces carga todos los disponibles
        if (count($this->DeviceIDs) == 0) {
        	$sql = "SELECT DeviceID, RegID FROM SI_SV_RegID WHERE UserID = {$this->UserID}";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die( translate("Error accessing")." SI_SV_RegID ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF) {
				$intDeviceID = (int) @$aRS->fields["deviceid"];
				$strRegID = (string) @$aRS->fields["regid"];
				if ($intDeviceID > 0 && $strRegID != '') {
					$this->DeviceIDs[] = $intDeviceID;
				}
				$aRS->MoveNext();
			}
        }
        
        if (!$blnNewObject) {
        	$sql = "DELETE FROM SI_SV_PushMessageDevices WHERE MessageID = {$this->MessageID}";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_PushMessageDevices ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
        }
        foreach ($this->DeviceIDs as $intDeviceID) {
        	$sql = "INSERT INTO SI_SV_PushMessageDevices (MessageID, DeviceID) 
        		VALUES ({$this->MessageID}, {$intDeviceID})";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_PushMessageDevices ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
        }
        
        //Envía la notificación si se activó y se está grabando con un estado de no enviado
        if ($blnSendPush) {
        	$this->sendMessage();
        }
	}
	
	//Envía nuevamente el mensaje a todos los dispositivos del destinatario
	function reSend() {
		$this->State = 1;
		$this->StateOld = 0;
		$this->save();
	}
	
	//Realiza la invocación del mensaje hacia el dispositivo móvil, si logra hacerlo sin error, actualiza el Status del mensaje
	function sendMessage() {
		$strErrorDesc = '';
		try {
		    $theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($this->Repository, $this->UserID);
		    if (is_null($theAppUser)) {
		    	return;
		    }
		    $theAppUser->getMobileData();
			
			$apiKey = "AIzaSyB2flGIjUeX0c_pBhBTxSZ2GiDeaSo6dio";    //api key de google que se genera por APLICACIÓN
			$arrAndroidPhones = array();
			$arrIOsPhones = array();
	    	//@JAPR 2014-11-05: Corregido un bug, sólo se debe enviar a los dispositivos especificados en el mensaje
			$arrDeviceIDs = array_flip(array_keys($this->DeviceIDs));
			//@JAPR
		    foreach ($theAppUser->MobileData as $strRegID => $arrMobileData) {
		    	//@JAPR 2014-11-05: Corregido un bug, sólo se debe enviar a los dispositivos especificados en el mensaje
		    	$intDeviceID = (int) @$arrMobileData['deviceID'];
		    	if (!isset($arrDeviceIDs[$intDeviceID])) {
		    		continue;
		    	}
		    	//@JAPR
		    	
		    	$strType = trim((string) @$arrMobileData['type']);
		    	if ($strRegID == '') {
		    		continue;
		    	}
		    	
		    	if ($strType == 'IOS') {
		    		$arrIOsPhones[] = $strRegID;
		    	}
		    	else {
		    		$arrAndroidPhones[] = $strRegID;
		    	}
		    }
		    
			//Mandar mensaje a los dispositivos Android configurados
			if (count($arrAndroidPhones) > 0) {
				$url = 'https://android.googleapis.com/gcm/send';
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
				$fields = array(
					'registration_ids' => $arrAndroidPhones,
					'data' => array( "message" => $this->PushMessage, "title" => ($this->Title != '')?$this->Title:'KPIOnline'),
				);
				$headers = array(
					'Authorization: key=' . $apiKey,
					'Content-Type: application/json'
				);
				
				// Open connection
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HEADER, 1);
				curl_setopt($ch, CURLOPT_NOBODY, 0);
				curl_setopt($ch, CURLOPT_VERBOSE, 0);
				
				// Set the url, number of POST vars, POST data
				curl_setopt( $ch, CURLOPT_URL, $url );
				curl_setopt( $ch, CURLOPT_POST, true );
				curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));
				curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				
				// Execute post
				$result = curl_exec($ch);
				if ($result === false){
					$strErrorDesc = curl_error($ch);
				}
				
				// Close connection
				curl_close($ch);
			}
			$pemFiles = array(
					0 => 'ck.pem',
					1 => 'ckformsstore.pem',
					2 => 'ckmovilidad.pem',
					1 => 'ck_forms.pem',
					3 => 'FormsV6PushCK.pem',
					4 => 'GeocontrolPushCK.pem'
					);
			//Mandar mensaje a los dispositivos IOs configurados
			if (count($arrIOsPhones) > 0) {
				foreach ($arrIOsPhones as $strRegID) {
					//hay q ejecutar por cada pem este codigo
					//si es q existe ese pem
					foreach ($pemFiles as $pem) {
						if(file_exists($pem)){
					// Create a stream to the server
					$streamContext = stream_context_create();
							stream_context_set_option($streamContext, 'ssl', 'local_cert', $pem);//'ck.pem'
					stream_context_set_option($streamContext, 'ssl', 'passphrase', 'b1t4m1nc');
					$apns = stream_socket_client('ssl://gateway.push.apple.com:2195', $error, $errorString, 60,
					    STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $streamContext);
					if (!$apns) {
						$strErrorDesc = "Failed to connect: {$err} {$errstr}";
					    break;
					}
					
					// Now we need to create JSON which can be sent to APNS
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					$load = array('aps' => array(
					                'alert' => $this->PushMessage,
					                'badge' => 1,
					                'sound' => 'default')
					        );
					$payload = json_encode($load);
					
					// The payload needs to be packed before it can be sent
					$apnsMessage = chr(0) . chr(0) . chr(32);
					$apnsMessage .= pack('H*', str_replace(' ', '', $strRegID));
					$apnsMessage .= chr(0) . chr(strlen($payload)) . $payload;
					
					// Write the payload to the APNS
					fwrite($apns, $apnsMessage);
					// Close the connection
					fclose($apns);
				}
			}
				}
			}
		} catch (Exception $e) {
			$strErrorDesc = $e->getMessage();
		}
		
		if ($strErrorDesc != '') {
			$sql = "UPDATE SI_SV_PushMessage SET State = 0 WHERE MessageID = {$this->MessageID}";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$strErrorDesc = translate("Error sending the mobile message").": ".curl_error($ch);
			die("(".__METHOD__.") ".$strErrorDesc);
		}
		else {
			$sql = "UPDATE SI_SV_PushMessage SET State = 1 WHERE MessageID = {$this->MessageID}";
			$this->Repository->DataADOConnection->Execute($sql);
		}
	}
	
	function remove()
	{
		$sql = "DELETE FROM SI_SV_PushMessageDevices WHERE MessageID = {$this->MessageID}";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMessage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_PushMessage WHERE MessageID = {$this->MessageID}";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMessage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
    
	function isNewObject()
	{
		return ($this->MessageID < 0);
	}

	function get_Title()
	{
		return translate("Mobile notification");
	}

	function get_QueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		
		$strParameters = "";
		
		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}
		
		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}
		
		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}
		
		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}
		
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=PushMessage".$strParameters;;
		}
		else
		{
			return "BITAM_PAGE=PushMessage"."&MessageID=".$this->MessageID.$strParameters;
		}
	}

	function get_Parent()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		return BITAMPushMessageCollection::NewInstance($this->Repository, $svStartDate, $svEndDate, $svUserID, $svIpp);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'MessageID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		$myFields = array();
		$arrayUsers = array();
		$arrayUsersOnly = array();
		if ($this->isNewObject()) {
			$arrayUsers[0] = "(".translate("Select user").")";
			$arrayUsers[-1] = "(".translate("Customized").")";
		}
		
		$sql = "SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B WHERE A.CLA_USUARIO = B.CLA_USUARIO ";
		if($_SESSION["PAFBM_Mode"]==1)
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$sql.=" AND B.CLA_USUARIO > 0 ";
			//@JAPR
		}
		$sql.=" ORDER BY NOM_LARGO ";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users, SI_USUARIO ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$cla_usuario = (int)@$aRS->fields["cla_usuario"];
			$nom_largo = (string) @$aRS->fields["nom_largo"]; 
			$arrayUsers[$cla_usuario] = $nom_largo;
			$arrayUsersOnly[$cla_usuario] = $nom_largo;
			$aRS->MoveNext();
		}
		
		if ($this->UserID < 0) {
			$this->UserID = 0;
		}
		if (!isset($arrayUsers[$this->UserID])) {
			$this->UserID = -1;
			$arrayUsers[-1] = "(".translate("Deleted user").")";
		}
		
		$strUserButtons = '<span id="spnUserButtons" name="spnUserButtons" style="display:none;"><img title="'.translate("Marcar todos").'" src="images/selectall.png" onclick="checkAllItems(\'UserIDs[]\')" style="cursor:pointer;"><img title="'.translate("Desmarcar todos").'" src="images/unselect.png" onclick="uncheckAllItems(\'UserIDs[]\')" style="cursor:pointer;"></span>';
		$userField = BITAMFormField::NewFormField();
		$userField->Name = "UserID";
		$userField->Title = translate("User Name");
		$userField->ToolTip = translate("User Name");
		$userField->Type = "Object";
		$userField->VisualComponent = "Combobox";
		$userField->Options = $arrayUsers;
		$userField->OnChange = "checkCustomizedUsers()";
		$userField->Size = 255;
		if (!$this->isNewObject()) {
			$userField->IsDisplayOnly = true;
		}
		$myFields[$userField->Name] = $userField;
		
		if ($this->isNewObject()) {
			$surveyField = BITAMFormField::NewFormField();
			$surveyField->Name = "UserIDs";
			$surveyField->Title = translate("Users");
			$surveyField->Type = "Array";
			$surveyField->Options = $arrayUsersOnly;
			$surveyField->AfterMessage = $strUserButtons;
			$myFields[$surveyField->Name] = $surveyField;
		}
		
		$arrDevicesByUser = BITAMAppUser::GetMobileDevicesByUser($this->Repository);
		if (is_null($arrDevicesByUser) || !is_array($arrDevicesByUser)) {
			$arrDevicesByUser = array();
		}
		$arrDevicesByUser[0] = array();
		$arrDevicesByUser[-1] = array();
		
		$strDeviceButtons = '<span id="spnDeviceButtons" name="spnDeviceButtons" style="display:none;"><img title="'.translate("Marcar todos").'" src="images/selectall.png" onclick="checkAllItems(\'DeviceIDs[]\')" style="cursor:pointer;"><img title="'.translate("Desmarcar todos").'" src="images/unselect.png" onclick="uncheckAllItems(\'DeviceIDs[]\')" style="cursor:pointer;"></span>';
		$devicesField = BITAMFormField::NewFormField();
		$devicesField->Name = "DeviceIDs";
		$devicesField->Title = translate("Mobile devices");
		$devicesField->Type = "Array";
		$devicesField->Options = $arrDevicesByUser;
		$devicesField->AfterMessage = $strDeviceButtons;
		$myFields[$devicesField->Name] = $devicesField;
		$devicesField->Parent = $userField;
		$userField->Children[] = $devicesField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Title";
		$aField->Title = translate("Title");
		$aField->ToolTip = translate("Title");
		$aField->Type = "String";
		$aField->Size = 100;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "PushMessage";
		$aField->Title = translate("Message");
		$aField->ToolTip = translate("Message");
		$aField->Type = "LargeString";
		$aField->Size = 500;
		$myFields[$aField->Name] = $aField;
		
		if (!$this->isNewObject()) {
			$arrayStatus = array(0 => translate('Not sent'), 1 => translate('Sent'));
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "State";
			$aField->Title = translate("Status");
			$aField->ToolTip = translate("Status");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrayStatus;
			$aField->Size = 255;
			if ($this->State > 0) {
				$aField->IsDisplayOnly = true;
			}
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
?>
 		<script language="JavaScript">
		function verifyFieldsAndSave(target)
 		{
 			strBlanksField = "";
 			
			if(Trim(BITAMPushMessage_SaveForm.Title.value)=="")
			{
				strBlanksField += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Title"))?>';
			}
 			
			selectedIdx = parseInt(BITAMPushMessage_SaveForm.UserID.selectedIndex);
			if(selectedIdx<0 || BITAMPushMessage_SaveForm.UserID.value == 0)
			{
				strBlanksField+= '\n'+'<?=translate("You must have selected a valid user.")?>';
			}
			
			if(strBlanksField!="")
  			{
				alert('<?=translate("Please verify")?>:' + strBlanksField );
  			}
  			else {
  				BITAMPushMessage_Ok(target);
  			}
 		}
 		
 		function checkCustomizedUsers()
 		{
 			if (cancel==true || true)
 			{
 				/*
	 			if (initialValuesSet != undefined && initialValuesSet == true)
	 			{
					BITAMSurveyAgenda_FilterText = new Array();
	 			}
	 			*/
 			}
 			//cancel=false;
	 		var rowUserIDs = document.getElementById("Row_UserIDs");
	 		if (rowUserIDs) {
	 			rowUserIDs.style.display = (BITAMPushMessage_SaveForm.UserID.value != -1)?"none":((bIsIE)?"inline":"table-row");
	 		}
	 		
	 		var intUserID = 0;
	 		var spanUserButtons = document.getElementById("spnUserButtons");
	 		if (spanUserButtons) {
	 			intUserID = BITAMPushMessage_SaveForm.UserID.value;
				spanUserButtons.style.display = (intUserID != -1)?'none':'inline';
			}
	 		
	 		var rowDeviceIDs = document.getElementById("Row_DeviceIDs");
	 		if (rowDeviceIDs) {
	 			rowDeviceIDs.style.display = (intUserID > 0)?((bIsIE)?"inline":"table-row"):"none";
		 		var spanDeviceButtons = document.getElementById("spnDeviceButtons");
		 		if (spanDeviceButtons) {
					spanDeviceButtons.style.display = rowDeviceIDs.style.display;
				}
	 			
	 			var arrObjs = document.getElementsByName("DeviceIDs[]");
	 			if (arrObjs && arrObjs.length) {
	 				for (var i in arrObjs) {
	 					arrObjs[i].checked = true;
	 				}
	 			}
	 		}
 		}
 		
 		function checkAllItems(sElementName) {
 			var arrObjs = document.getElementsByName(sElementName);
 			if (arrObjs && arrObjs.length) {
 				for (var i in arrObjs) {
 					arrObjs[i].checked = true;
 				}
 			}
 		}
 		
 		function uncheckAllItems(sElementName) {
 			var arrObjs = document.getElementsByName(sElementName);
 			if (arrObjs && arrObjs.length) {
 				for (var i in arrObjs) {
 					arrObjs[i].checked = false;
 				}
 			}
 		}
 		</script>
<?
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return (!$this->isNewObject() && !$this->State);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return (!$this->isNewObject() && !$this->State);
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideRemoveButton($aUser)
	{
		return false;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideBackButton($aUser)
	{
		return true;
	}

	function getImages($questionID)
	{
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
?>
	 	<script language="JavaScript">
		objOkSelfButton = document.getElementById("BITAMPushMessage_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMPushMessage_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
<?
		if($this->isNewObject())
 		{
?>
			objOkNewButton = document.getElementById("BITAMPushMessage_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
		</script>
<?
	}
	
}

class BITAMPushMessageCollection extends BITAMCollection
{
	public $StartDate;
	public $EndDate;
	public $UserID;
	public $ipp;
	public $Pages;
	
	function __construct($aRepository, $aUserID)
	{
		BITAMCollection::__construct($aRepository);

		$this->UserID = $aUserID;
		$this->MsgConfirmRemove = "Do you wish to delete the selected entries?";
		$this->MsgNoSelectedRemove = "Please select the entries you wish to delete.";
        $this->MsgConfirmSendPDF = "Do you wish to Send PDF of the selected entries?";
		$this->MsgNoSelectedSendPDF = "Please select the entries you wish to Send PDF.";
        $this->MsgCancelSendPDF = "Send PDF of selected entries cancelled.";
		
		$this->StartDate = "";
		$this->EndDate = "";
		$this->UserID = -2;
		$this->ipp = 100;
		$this->Pages = null;
	}

	static function NewInstance($aRepository, $startDate="", $endDate="", $userID=-2, $ipp=100, $bGetValues = false)
	{
		$anInstance = new BITAMPushMessageCollection($aRepository, $userID);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$svIpp = $ipp;
		
		//Obtener los datos de la dimension Usuario
		$aSurvey = BITAMSurvey::NewInstance($aRepository);
		if (is_null($aSurvey)) {
			return null;
		}
		$aSurvey->readGblSurveyModel();
		if ($aSurvey->GblUserDimID <= 0) {
			return null;
		}
		
		$fieldEmailKey = "RIDIM_".$aSurvey->GblUserDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->GblUserDimID;
		
		$sql = "SELECT A.MessageID, A.UserID, C.{$fieldEmailDesc} AS UserName, A.CreationUserID, A.CreationDateID, A.Title, A.PushMessage, A.State ";
		$where = '';
		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" A.CreationDateID >= ".$aRepository->DataADOConnection->DBTimeStamp($startDate);
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" A.CreationDateID <= ".$aRepository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID!=-2 && $userID!=0)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" A.UserID = ".$userID;
		}
		
		$sql .= " FROM SI_SV_PushMessage A, RIDIM_{$aSurvey->GblUserDimID} C";
		//Ligamos las Tablas para lo de Email
		$where.=($where!=""?" AND ":"")."A.UserID = C.{$fieldEmailKey}";
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}
		$sql .= " ORDER BY A.CreationDateID, C.{$fieldEmailDesc}";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMessage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Total de Registros
		$numRows = $aRS->RecordCount();
		
		$_GET["ipp"] = $ipp;
		
		if(!isset($_GET["page"]))
		{
			$_GET["page"] = 1;
		}
		
		if($numRows>0)
		{
			//Creamos el objeto de paginacion
			$anInstance->Pages = new Paginator();
			$anInstance->Pages->items_total = $numRows;
			//$anInstance->Pages->items_per_page = 100;
			//$anInstance->Pages->items_per_page = $_GET["ipp"];
			$anInstance->Pages->items_per_page = $ipp;
			$anInstance->Pages->mid_range = 9;
			$anInstance->Pages->paginate();
			
			//Agregamos el elemento LIMIT al SELECT para obtener los registros que se requieren
			$sql .= $anInstance->Pages->limit;
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMessage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$anInstance->Pages->current_num_items = $aRS->RecordCount();
			}
		}
		
		while (!$aRS->EOF)
		{
			//@JAPR 2013-05-24: Validado que no trate de obtener todos los valores en la colección de reportes para ciertas cuentas (temporal)
			$anInstance->Collection[] = BITAMPushMessage::NewInstanceFromRS($aRepository, $aRS, $bGetValues);
			$aRS->MoveNext();
		}
		
		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}
		
		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		if ($ipp!=100)
		{
			$anInstance->ipp = $ipp;
		}
		
		return $anInstance;
	}

	static function NewInstanceForRemove($aRepository, $anArrayOfMessageIDs=null, $startDate="", $endDate="", $userID=-2, $ipp=100, $bGetValues = false)
	{
		$anInstance = new BITAMPushMessageCollection($aRepository, $userID);
		
		//Obtener los datos de la dimension Usuario
		$aSurvey = BITAMSurvey::NewInstance($aRepository);
		if (is_null($aSurvey)) {
			return null;
		}
		$aSurvey->readGblSurveyModel();
		if ($aSurvey->GblUserDimID <= 0) {
			return null;
		}
		
		$fieldEmailKey = "RIDIM_".$aSurvey->GblUserDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->GblUserDimID;
		
		$whereFilter = "";
		if (!is_null($anArrayOfMessageIDs))
		{
			switch (count($anArrayOfMessageIDs))
			{
				case 0:
					break;
				case 1:
					$whereFilter = " AND A.MessageID = ".((int)$anArrayOfMessageIDs[0]);
					break;
				default:
					foreach ($anArrayOfMessageIDs as $aMessageID)
					{
						if ($whereFilter != "")
						{
							$whereFilter .= ", ";
						}
						
						$whereFilter .= (int)$aMessageID; 
					}
					
					if ($whereFilter != "")
					{
						$whereFilter = " AND A.MessageID IN (".$whereFilter.")";
					}
					break;
			}
		}
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$svIpp = $ipp;
		
		$sql = "SELECT A.MessageID, A.UserID, C.{$fieldEmailDesc} AS UserName, A.CreationUserID, A.CreationDateID, A.Title, A.PushMessage, A.State ";
		$where = '';
		$sql .= " FROM SI_SV_PushMessage A, RIDIM_{$aSurvey->GblUserDimID} C";
		//Ligamos las Tablas para lo de Email
		$where.=($where!=""?" AND ":"")."A.UserID = C.{$fieldEmailKey}";
		$sql .= " WHERE ".$where.$whereFilter;
		$sql .= " ORDER BY A.CreationDateID, C.{$fieldEmailDesc}";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMessage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMPushMessage::NewInstanceFromRS($aRepository, $aRS, $bGetValues);
			$aRS->MoveNext();
		}
		
		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}
		
		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		if ($ipp!=100)
		{
			$anInstance->ipp = $ipp;
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$startDate="";
		if (array_key_exists("startDate", $aHTTPRequest->GET))
		{
			if(trim($aHTTPRequest->GET["startDate"])!="" && substr($aHTTPRequest->GET["startDate"],0,10)!="0000-00-00")
			{
				$startDate = trim($aHTTPRequest->GET["startDate"]);
			}
		}
		
		$endDate="";
		if (array_key_exists("endDate", $aHTTPRequest->GET))
		{
			if(trim($aHTTPRequest->GET["endDate"])!="" && substr($aHTTPRequest->GET["endDate"],0,10)!="0000-00-00")
			{
				$endDate = trim($aHTTPRequest->GET["endDate"]);
			}
		}
		
		$userID=-2;
		if (array_key_exists("userID", $aHTTPRequest->GET))
		{
			$userID = (int)$aHTTPRequest->GET["userID"];
		}
		
		$ipp=100;
		if (array_key_exists("ipp", $aHTTPRequest->GET))
		{
			$ipp = (int)$aHTTPRequest->GET["ipp"];
		}
		
		return BITAMPushMessageCollection::NewInstance($aRepository, $startDate, $endDate, $userID, $ipp);
	}

	function get_Parent()
	{
		return $this->Repository;
	}

	function get_Title()
	{
		return translate("Mobile notifications");
	}

	function get_QueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;

		$strParameters = "";

		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}

		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}
		
		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}

		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}

		return "BITAM_SECTION=PushMessageCollection".$strParameters;
	}

	function get_ChildQueryString()
	{
		return "BITAM_SECTION=PushMessage";
	}

	function get_AddRemoveQueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;

		$strParameters = "";

		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}

		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}
		
		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}

		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}

		return "BITAM_PAGE=PushMessage".$strParameters;
	}

	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'MessageID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");

		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CreationDateID";
		$aField->Title = translate("Date");
		$aField->ToolTip = translate("Date");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Title";
		$aField->Title = translate("Title");
		$aField->ToolTip = translate("Title");
		$aField->Type = "String";
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;
		
		$arrayUsers = array();
		if ($this->isNewObject()) {
			$arrayUsers[0] = translate("Select a user");
		}
		
		$sql = "SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B WHERE A.CLA_USUARIO = B.CLA_USUARIO ";
		if($_SESSION["PAFBM_Mode"]==1)
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$sql.=" AND B.CLA_USUARIO > 0 ";
			//@JAPR
		}
		$sql.=" ORDER BY NOM_LARGO ";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users, SI_USUARIO ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$cla_usuario = (int)@$aRS->fields["cla_usuario"];
			$nom_largo = (string) @$aRS->fields["nom_largo"]; 
			$arrayUsers[$cla_usuario] = $nom_largo;
			$aRS->MoveNext();
		}
		
		if ($this->UserID < 0) {
			$this->UserID = 0;
		}
		if (!isset($arrayUsers[$this->UserID])) {
			$this->UserID = -1;
			$arrayUsers[-1] = "(".translate("Deleted user").")";
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserID";
		$aField->Title = translate("User Name");
		$aField->ToolTip = translate("User Name");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrayUsers;
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "PushMessage";
		$aField->Title = translate("Message");
		$aField->ToolTip = translate("Message");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$arrayStatus = array(0 => translate('Not sent'), 1 => translate('Sent'));
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "State";
		$aField->Title = translate("Status");
		$aField->ToolTip = translate("Status");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrayStatus;
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return true;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideRemoveButton($aUser)
	{
		return false;
	}
	
	function generateBeforeFormCode($aUser)
	{
?>		<script language="JavaScript">
 		
		function applyFilterAttrib()
		{
			var objStartDate = document.getElementById("CaptureStartDate");
			frmAttribFilter.startDate.value = objStartDate.value;

			var objEndDate = document.getElementById("CaptureEndDate");
			frmAttribFilter.endDate.value = objEndDate.value;
			
			var objUserID = document.getElementById("UserID");
			frmAttribFilter.userID.value = objUserID.value;
			
			var objIpp = document.getElementById("ipp");
			frmAttribFilter.ipp.value = objIpp.value;

			frmAttribFilter.submit();
		}
		
		function clearFilter()
		{
<?
			if($this->UserID!=-2 || $this->StartDate!="" || $this->EndDate!="" || $this->ipp!=100)
			{
?>
			var objStartDate = document.getElementById("CaptureStartDate");
			frmAttribFilter.startDate.value = "";

			var objEndDate = document.getElementById("CaptureEndDate");
			frmAttribFilter.endDate.value = "";
			
			var objUserID = document.getElementById("UserID");
			frmAttribFilter.userID.value = -2;
			
			var objIpp = document.getElementById("ipp");
			frmAttribFilter.ipp.value = 100;
			
			frmAttribFilter.submit();
<?
			}
?>
		}
		
		var ctrlEditAudit;
		ctrlEditAudit = -1;
		
		function editAudit()
		{
			ctrlEditAudit = ctrlEditAudit*(-1);
			var obDivTitle = document.getElementById("collection_title_div");
			var objBtnBack = document.getElementById("btnBack");
			var objBtnEditLog = document.getElementById("btnEditLog");
			var objBtnDelEntries = document.getElementById("btnDelEntries");
			var arrayTrCollection = document.getElementsByName("tr_collection_checkbox");
			var i;
			
			if(ctrlEditAudit==1)
			{
				objBtnBack.style.display = 'inline';
				objBtnEditLog.style.display = 'none';
                objBtnDelEntries.style.display = 'inline';
				//obDivTitle.innerHTML = 'Edit Log<hr>';
				
    			if(arrayTrCollection!=undefined && arrayTrCollection!=null)
    			{
      				for(i=0; i<arrayTrCollection.length; i++)
    				{
    					objTD = arrayTrCollection[i].firstChild;
    					objTD.style.display = ((bIsIE)?"inline":"table-cell");
    				}
    			}
			}
			else
			{
				objBtnBack.style.display = 'none';
				objBtnEditLog.style.display = 'inline';
				objBtnDelEntries.style.display = 'none';
				//obDivTitle.innerHTML = 'Log: Data Captures<hr>';

    			if(arrayTrCollection!=undefined && arrayTrCollection!=null)
    			{
    				for(i=0; i<arrayTrCollection.length; i++)
    				{
    					objTD = arrayTrCollection[i].firstChild;
    					objTD.style.display = 'none';
    				}
    			}		
			}
		}
		</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
?>
	
	<form name="frmAttribFilter" action="main.php" method="GET" target="body">
		<input type="hidden" name="BITAM_SECTION" value="PushMessageCollection">
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="ipp" id="ipp" value="">
	</form>
<?
	//Vamos a desplegar el pie de pagina, es decir, la paginacion
		if(!is_null($this->Pages))
		{
?>
	<style type="text/css">
	.paginate 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
	}

	a.paginate 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		color: #000080;
		text-decoration: none;
	}

	a.paginate:hover 
	{
		background-color: #000080;
		color: #FFF;
		text-decoration: underline;
	}
	
	.currentlink 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
		font-weight: bold;
	}

	a.currentlink 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		cursor: default;
		background:#000080;
		color: #FFF;
		text-decoration: none;
	}
	
	.showingInfo 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 11px;
	}
	</style>
	<br/>
	<?=$this->Pages->display_pages(true)?>
	<br/>
<?
		}
	}

	function displayCheckBox($aUser)
	{
		return true;
	}
	
	function afterTitleCode($aUser)
	{
		$surveyCollection = BITAMSurveyCollection::NewInstance($this->Repository);
		$numSurveys = count($surveyCollection->Collection);
		
		$numRows = ceil(0/3);
		$countCell = 0;
		$maxCell = 4;
		//@JAPR 2012-04-04: Modificado el combo de encuestas para que aparezca mas grande y no corte nombres de encuestas largos
		$numColSpanForm = 4; // 2;
		//@JAPR
		$totalColSpanTable = 1 + ($maxCell*2);
		
		if($this->UserID!=-2 || $this->StartDate!="" || $this->EndDate!="" || $this->ipp!=100)
		{
			$imgSrcClear = "images/sub_cancelarfiltro.gif";
			$lblApply = "Reapply";
		}
		else 
		{
			$imgSrcClear = "images/sub_cancelarfiltro_disabled.gif";
			$lblApply = "Apply";
		}
		
		$arrayFilters = array();
		$applyFilter = '<a href="javascript:applyFilterAttrib();"><img src="images/sub_aplicarfiltro.gif" alt="'.translate("Apply Filters").'" style="cursor:hand">&nbsp;<span id="lblApply">'.$lblApply.'</span></a>&nbsp;&nbsp;';
		$clearFilter = '<a href="javascript:clearFilter();"><img src="'.$imgSrcClear.'" alt="'.translate("Clear Filters").'" style="cursor:hand">&nbsp;<span id="lblClear">Clear</span></a>&nbsp;&nbsp;';
		
		$arrayFilters[0]=$applyFilter;
		$arrayFilters[1]=$clearFilter;
		
		$arrayButtons = array();		
		$btnEditAudit='<div class="collection_buttons" style="border:0px; background:transparent"><button id="btnEditLog" style="display:none" onclick="javascript:editAudit();"><img src="images/edit.png" alt="'.translate("Edit log").'" title="'.translate("Edit log").'" displayMe="1" />'.translate("Edit log").'</button></div>';
		
		$arrayButtons[0]=$btnEditAudit;
		$btnDelEntries = '<span id="btnDelEntries" style="display:none" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:BITAMPushMessageCollection_Remove();"><img src="images/delete.png">&nbsp;'.translate("Delete entries").'</span>';
		
		//Validamos si nos faltaron campos de filtros o botones que no se hayan logrado colocar
		$countFilters = count($arrayFilters);
		$countButtons = count($arrayButtons)+1;
		
		$strFrom =
		"<input type=\"text\" id=\"CaptureStartDate\" name=\"CaptureStartDate\" size=\"22\" maxlength=\"19\" value=\"".$this->StartDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0) {year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureStartDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureStartDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureStartDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureStartDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureStartDate = new calendar('calendar_BITAMSurvey_CaptureStartDate', 'calendarCallback_BITAMSurvey_CaptureStartDate');
					</script>";
		
		$strTo=
		"<input type=\"text\" id=\"CaptureEndDate\" name=\"CaptureEndDate\" size=\"22\" maxlength=\"19\" value=\"".$this->EndDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0)	{year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureEndDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureEndDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureEndDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureEndDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureEndDate = new calendar('calendar_BITAMSurvey_CaptureEndDate', 'calendarCallback_BITAMSurvey_CaptureEndDate');
					</script>";

?>
	<span id="btnBack" style="display:none" class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:editAudit();"><img src="images/home.png">&nbsp;<?="Back"?></span>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td colspan="<?=$totalColSpanTable?>">
				&nbsp;
			</td>
			
		</tr>
<?

		$arrayUsers = array();
		$arrayUsers[-2] = translate("All");
		$arrayUsers[-1] = "NA";
		
		$sql = "SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B WHERE A.CLA_USUARIO = B.CLA_USUARIO ";
		if($_SESSION["PAFBM_Mode"]==1)
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$sql.=" AND B.CLA_USUARIO > 0 ";
			//@JAPR
		}
		$sql.=" ORDER BY NOM_LARGO ";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users, SI_USUARIO ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$cla_usuario = (int)$aRS->fields["cla_usuario"];
			$nom_largo = $aRS->fields["nom_largo"]; 
			$arrayUsers[$cla_usuario] = $nom_largo;
			
			$aRS->MoveNext();
		}
		
		$countCell = 0;
		$numRows = 0;
		$positionDates = 2;
		$positionFilter = 3;
		$positionButtons = 4;
		$position = 1;
		$maxRows = $countButtons;

		$selected = "";
?>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td>
			<?="Filters"?>&nbsp;
			</td>
			<td style="text-align:right;" class="styleBorderFilterLeftTop">
				&nbsp;&nbsp;&nbsp;<span><?=translate("User")?></span>
			</td>
			<td style="text-align:right;" class="styleBorderFilterTop">
				<select id="UserID" name="UserID" style="width:150px; font-size:11px">
<?		
		foreach ($arrayUsers as $keyUser=>$nameUser)
		{
			$selected="";

			if($this->UserID == $keyUser)
			{
				$selected = "selected";
			}
?>
					<option value="<?=$keyUser?>" <?=$selected?>><?=$nameUser?></option>
<?
		}
?>
				</select>
			</td>
<?
		$countCell=1;
		
		$loadCat = true;
		$strAtribFilter = "";
		
		if(true)
		{
			if(($countCell%$maxCell)==0)
			{
				$numRows++;
				$position = 1;
?>
		</tr>
<?
				if(($numRows+1)<$countButtons)
				{
?>
		<tr>
			<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
<?
					}
?>
			</td>

			<td>
				&nbsp;
			</td>
<?
				}
			}
			else 
			{
				$position++;
			}
			
			if(($numRows+1)<=$countButtons)
			{
				while(($numRows+1)<=$countButtons)
				{
					if($position==1)
					{
						$classBorderLeft = ' class="styleBorderFilterLeft"';
						$classBorderBottom = '';
						if($maxRows==($numRows+1))
						{
							$classBorderLeft = ' class="styleBorderFilterLeftBottom"';
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}
	
	?>
				<td <?=$classBorderLeft?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						$position++;
					}
					
					if($position==$positionDates)
					{
						if($numRows==0)
						{
	?>
				<td style="text-align:right;" class="styleBorderFilterTop">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("From")?></span>
				</td>
				<td class="styleBorderFilterTop">
					<?=$strFrom?>
				</td>
	<?
						}
						else if($numRows==1)
						{
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
	?>
				<td style="text-align:right;" <?=$classBorderBottom?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("To")?></span>
				</td>
				<td <?=$classBorderBottom?>>
					<?=$strTo?>
				</td>
	<?
						}
						else if($numRows==2)
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
	?>
				<td style="text-align:right;" <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?="Max Records"?></span>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
					
							}
	?>
				</td>
				<td <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
				
					<?=$this->Pages->display_items_per_page_report()?>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
							}
	?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
				//@JAPR 2012-01-10: Corregido un bug, los nombres previos de estas variables no existían, además, deberían ser $classBorderBottom
				//que que se trata de celdas vacías debajo de los filtros de fecha y Max records, así que mientras no se agreguen mas opciones
				//fijas esto debería no tener border para cada filtro intermedio, y debería ser border inferior para el último filtro
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						$classBorderTop = "";
						$classBorderRightTop = "";
						if($numRows==0)
						{
							$classBorderTop = ' class="styleBorderFilterTop"';
							$classBorderRightTop = ' class="styleBorderFilterRightTop"';
						}
						elseif ($maxRows == ($numRows+1)) {
							$classBorderTop = ' class="styleBorderFilterBottom"';
							$classBorderRightTop = ' class="styleBorderFilterRightBottom"';
						}
						else 
						{
							$classBorderTop = '';
							$classBorderRightTop = ' class="styleBorderFilterRight"';
						}
						
						if(isset($arrayFilters[$numRows]))
						{
	?>
				<td <?=$classBorderTop?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightTop?>>
					<?=$arrayFilters[$numRows]?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = "";
							$classBorderRightBottom = "";
							if($maxRows == ($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
								$classBorderRightBottom = ' class="styleBorderFilterRightBottom"';
							}
							else 
							{
								$classBorderBottom = '';
								$classBorderRightBottom = ' class="styleBorderFilterRight"';
							}
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						if(isset($arrayButtons[$numRows]))
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td style="padding-top:5px;padding-bottom:5px;">
					<?=$arrayButtons[$numRows]?>
				</td>
	<?
						}
						else 
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
	<?
						}
						
						$countCell++;
					}
					
						$numRows++;
						$position = 1;
	?>
				</tr>
	<?
						if(($numRows+1)<=$countButtons)
						{
	?>
				<tr>
				<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
<?
					}
?>
				</td>
	
					<td>
						&nbsp;
					</td>
	<?
						}
				}
			}
			else 
			{
				while(($countCell%$maxCell)!=0)
				{
					//Si es el primero
					if(($countCell+1)%$maxCell==1)
					{
						$classBorderCell01 = ' class="styleBorderFilterLeftBottom"';
						$classBorderCell02 = ' class="styleBorderFilterBottom"';
					}
					else 
					{
						//Si es el penultimo par de tds
						if(($countCell+1)%$maxCell==3)
						{
							$classBorderCell01 = ' class="styleBorderFilterBottom"';
							$classBorderCell02 = ' class="styleBorderFilterRightBottom"';
						}
						else
						{
							//Si es intermedio
							if(($countCell+1)%$maxCell==2)
							{
								$classBorderCell01 = ' class="styleBorderFilterBottom"';
								$classBorderCell02 = ' class="styleBorderFilterBottom"';
							}
							else 
							{	//Si es el ultimo(el cuarto)
								$classBorderCell01 = '';
								$classBorderCell02 = '';
							}
						}
					}
	?>
				<td <?=$classBorderCell01?>>
					&nbsp;
				</td>
				<td <?=$classBorderCell02?>>
					&nbsp;
				</td>
	<?
					$countCell++;
				}
			}
		}
?>
	</table>
	<br>
<?
	}
	
	function generateInsideFormCode($aUser)
	{
?>
		<input type="hidden" id="startDate" name="startDate" value="<?=$this->StartDate?>">
		<input type="hidden" id="endDate" name="endDate" value="<?=$this->EndDate?>">
		<input type="hidden" id="userID" name="userID" value="<?=$this->UserID?>">
		<input type="hidden" id="ipp" name="ipp" value="<?=$this->ipp?>">
<?
	}
}
?>