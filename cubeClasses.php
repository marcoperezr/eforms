<?php

require_once("initialize.php");
//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
//Ahora se utilizará directamente el cinfocube.inc.php del Model Manager para poder utilizar los métodos de agregaciones
//require_once("cinfocube.inc.php");
require_once("../model_manager/cinfocube.inc.php");
//@JAPR
require_once("cube.inc.php");
require_once("dimension.inc.php");
require_once("attribute.inc.php");

$arrInvalidChars = array('á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú', 'ñ', 'Ñ');
$arrValidChars = array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', 'n', 'N');

/***********************************************************************************************/

class BITAM_SI_DESCRIPT_KEYS extends BITAMObject
{
	public $cla_concepto;
	public $nom_tabla;
	public $consecutivo;
	public $nom_fisicok;
	public $nom_fisicok_join;

	function __construct($aRepository)
	{	
		BITAMObject::__construct($aRepository);
	
		$this->cla_concepto=-1;
		$this->nom_tabla="";
		$this->consecutivo=-1;
		$this->nom_fisicok="";
		$this->nom_fisicok_join="";
	}
	
	static function NewInstance($aRepository)
	{
		return new BITAM_SI_DESCRIPT_KEYS($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aCubeID)
	{
		$anInstance = null;
		
		if (((int) $aCubeID) < 0)
		{
			return $anInstance;
		}
		$sql = 
	"SELECT CLA_CONCEPTO, NOM_TABLA, CONSECUTIVO, NOM_FISICOK, NOM_FISICOK_JOIN FROM SI_DESCRIP_KEYS
	WHERE CLA_CONCEPTO =".$aCubeID. " ORDER BY CLA_CONCEPTO, NOM_TABLA, CONSECUTIVO";

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			//die("Error accessing SI_DESCRIP_KEYS table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			die( translate("Error accessing")." SI_DESCRIP_KEYSs ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAM_SI_DESCRIPT_KEYS::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAM_SI_DESCRIPT_KEYS::NewInstance($aRepository);
		
		$anInstance->consecutivo=  (int) $aRS->fields["consecutivo"];
		$anInstance->nom_fisicok =  $aRS->fields["nom_fisicok"];
		$anInstance->nom_tabla =    $aRS->fields["nom_tabla"];
	 	$anInstance->cla_concepto= (int) $aRS->fields["cla_concepto"];
	 	$anInstance->nom_fisicok_join =  $aRS->fields["nom_fisicok_join"];
		return $anInstance;
	}
}

/***********************************************************************************************/

class BITAM_SI_DESCRIPT_KEYSCollection extends BITAMCollection
{
	public $cla_concepto;
	
	function __construct($aRepository, $cla_concepto=-1)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->cla_concepto = $cla_concepto;
	}
	
	static function NewInstance($aRepository, $aCubeID)
	{
		$anInstance = null;

		$anInstance =& BITAMGlobalInstance::GetDescriptKeysCollection($aCubeID);
		
		if(is_null($anInstance))
		{
			$anInstance = new BITAM_SI_DESCRIPT_KEYSCollection($aRepository, $aCubeID);
			
			$sql = "SELECT CLA_CONCEPTO, NOM_TABLA, CONSECUTIVO, NOM_FISICOK, NOM_FISICOK_JOIN FROM SI_DESCRIP_KEYS
					WHERE CLA_CONCEPTO =".$aCubeID. " ORDER BY CLA_CONCEPTO, NOM_TABLA, CONSECUTIVO";
	
			$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die(translate("Error accessing")." SI_DESCRIP_KEYS ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			while (!$aRS->EOF)
			{
				$anInstance->Collection[] = BITAM_SI_DESCRIPT_KEYS::NewInstanceFromRS($anInstance->Repository, $aRS);
				$aRS->MoveNext();
			}
						
			BITAMGlobalInstance::AddDescriptKeysCollection($aCubeID, $anInstance);
		}
		
		return $anInstance;
	}
	
	static function NewInstanceEmpty($aRepository)
	{
		$anInstance = new BITAM_SI_DESCRIPT_KEYSCollection($aRepository);
		
		return $anInstance;
	}
	
}

class BITAMPeriod extends BITAMObject
{
	public $PeriodID;
	public $PeriodName;
	public $PeriodType;
	public $PeriodBasis;
	public $PeriodFormat;
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		
		$this->PeriodID = -1;
		$this->PeriodName = "";
		$this->PeriodType = 0;
		$this->PeriodBasis = 0;
		$this->PeriodFormat = "";
	}

	static function NewInstance($aRepository)
	{
		return new BITAMPeriod($aRepository);
	}

	static function NewInstanceWithID($aRepository, $aPeriodID)
	{
		$anInstance = null;
		
		$theInfoBITAMPeriod=null;
		$theInfoBITAMPeriod =& BITAMGlobalInstance::GetBITAMPeriod($aPeriodID);
		if(!is_null($theInfoBITAMPeriod))
		{
			return $theInfoBITAMPeriod;
		}
		
		if (((int) $aPeriodID) < 0)
		{
			return $anInstance;
		}
		
		$sql ="SELECT t1.CLA_PERIODO AS PeriodID,t1.NOM_PERIODO AS PeriodName,t1.TIPO_PERIODO AS PeriodType,
       t1.BASE_PERIODO AS PeriodBasis,t1.FMT_PERIODO AS PeriodFormat FROM SI_PERIODO t1 WHERE t1.CLA_PERIODO = ".((int) $aPeriodID);
 
 		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_PERIODO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMPeriod::NewInstanceFromRS($aRepository, $aRS);
		}
		
		BITAMGlobalInstance::AddBITAMPeriod($aPeriodID, $anInstance);
		
		return $anInstance;
	}
	
	static function WithPeriodName($aRepository, $aPeriodName)
	{
		$anInstance = null;
		if ($aPeriodName == "")
		{
			return $anInstance;
		}
		
		$sql ="SELECT t1.CLA_PERIODO AS PeriodID,t1.NOM_PERIODO AS PeriodName,t1.TIPO_PERIODO AS PeriodType,
       t1.BASE_PERIODO AS PeriodBasis,t1.FMT_PERIODO AS PeriodFormat FROM SI_PERIODO t1 WHERE t1.NOM_PERIODO = ".$aRepository->ADOConnection->Quote($aPeriodName);
		 
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			//die("Error accessing SI_PERIODO table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			die( translate("Error accessing")." SI_PERIODO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMPeriod::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMPeriod::NewInstance($aRepository);
		$anInstance->PeriodID = (int) $aRS->fields["periodid"];
		$anInstance->PeriodName = rtrim($aRS->fields["periodname"]);
		$anInstance->PeriodType = (int) $aRS->fields["periodtype"];
		$anInstance->PeriodBasis = (int) $aRS->fields["periodbasis"];
		$anInstance->PeriodFormat = rtrim($aRS->fields["periodformat"]);
		return $anInstance;
	}

	function periodDateTime($aDateTime, $aPeriodsToAdd)
	{
		$periodDateTime = null;
		$periodDateTime = BITAMGlobalInstance::GetPeriodDateTime($aDateTime,$aPeriodsToAdd,$this->PeriodType);
		if (!is_null($periodDateTime))
		{
			return $periodDateTime;
		}
		
		switch ($this->PeriodType)
		{
			case 1:
				$firstDate = make_date(year($aDateTime), 1, 1, 0, 0, 0);
				$days = ((int) (yday($aDateTime) / $this->PeriodBasis)) * $this->PeriodBasis;
				$periodDateTime = days_add($firstDate, $days);
				$periodDateTime = days_add($periodDateTime, $aPeriodsToAdd);
				break;
			case 2:
				$firstDate = make_date(year($aDateTime), 1, 1, 0, 0, 0);
				$firstDate = days_add($firstDate, 7 - wday($firstDate));
				$days = days_diff($firstDate, $aDateTime);
				$days = ((int) ($days / (7 * $this->PeriodBasis))) * (7 * $this->PeriodBasis);
				$periodDateTime = days_add($firstDate, $days);
				$periodDateTime = weeks_add($periodDateTime, $aPeriodsToAdd);
				break;
			case 4:
				$firstDate = make_date(year($aDateTime), 1, 1, 0, 0, 0);
				$months = ((int) ((mon($aDateTime) - 1) / $this->PeriodBasis)) * $this->PeriodBasis;
				$periodDateTime = months_add($firstDate, $months);
				$periodDateTime = months_add($periodDateTime, $aPeriodsToAdd);
				break;
			case 5:
				$years = ((int) (year($aDateTime) / $this->PeriodBasis)) * $this->PeriodBasis;
				$periodDateTime = make_date($years, 1, 1, 0, 0, 0);
				$periodDateTime = years_add($periodDateTime, $aPeriodsToAdd);
				break;
		}
		BITAMGlobalInstance::AddPeriodDateTime($aDateTime,$aPeriodsToAdd,$this->PeriodType,$periodDateTime);
		return $periodDateTime;
	}
	
	function getDateRangeByStartDate($startDate, $relativePeriodStart=0, $relativePeriodEnd=0)
	{
		//$dates: arreglo que contendrá dos elementos uno con clave "startDate" y otro con clave "endDate"
		$dates = array();

		$aDate=$this->periodDateTime($startDate, $relativePeriodStart*$this->PeriodBasis);
		$dates["startDate"]=formatADateTime($aDate, 'yyyy-mm-dd');
		
		//A $relativePeriodEnd se le suma 1 y a esa fecha se le resta 1 día para obtener el fin del último periodo solicitado
		$aDate=$this->periodDateTime($startDate, ($relativePeriodEnd+1)*$this->PeriodBasis);
		$aDate = days_add($aDate, -1);
		$dates["endDate"]=formatADateTime($aDate, 'yyyy-mm-dd');
		
		return $dates;
	}
}

class BITAMPeriodCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository, $anArrayOfPeriodIDs = null)
	{
		$anInstance = new BITAMPeriodCollection($aRepository);
		
		$where = "";
		
		if (!is_null($anArrayOfPeriodIDs))
		{
			switch (count($anArrayOfPeriodIDs))
			{
				case 0:
					break;
				
				case 1: 
					$where = " AND t1.CLA_PERIODO = ".((int) $anArrayOfPeriodIDs[0]);
					break;	
					
				default:
				
					foreach ($anArrayOfPeriodIDs as $aPeriodID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $aPeriodID; 
					}
					if ($where != "")
					{
						$where = " AND t1.CLA_PERIODO IN (".$where.")";
								 
					}
					break;
			}
		}

		$sql ="SELECT t1.CLA_PERIODO AS PeriodID,t1.NOM_PERIODO AS PeriodName,t1.TIPO_PERIODO AS PeriodType,
       			t1.BASE_PERIODO AS PeriodBasis,t1.FMT_PERIODO AS PeriodFormat 
       			FROM SI_PERIODO t1 WHERE t1.TIPO_PERIODO IN (1, 2, 4, 5)";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_PERIODO ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMPeriod::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

}

/***********************************************************************************************/

class BITAMTable extends BITAMObject
{
	public $tableID;
	public $tableContent;

	function __construct($aRepository)
	{	
		BITAMObject::__construct($aRepository);
	
		$this->tableID="";
		$this->tableContent=array();
	}
	
	static function NewInstance($aRepository)
	{
		return new BITAMTable($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aTableID,$filter="")
	{
		$anInstance = null;
		
		if( trim($aTableID)=="" )
		{
			return $anInstance;
		}
		
		$anInstance =& BITAMGlobalInstance::GetTableContent(strtolower($aTableID),$filter);
		
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		//$metaColumns = $aRepository->DataADOConnection->MetaColumns($aTableID);
		$metaColumns=$aRepository->GetTableSchema($aTableID,true);
		
		$aTableContent = array();
		
		$fields = "";
		
		foreach ($metaColumns as $keyColumn => $objectColumn)
		{
			$aTableContent[strtolower($keyColumn)] = array();
			
			if(trim($fields)!="")
			{
				$fields.=", ";
			}
			
			$fields.=$keyColumn;
		}
		
		$sql = "SELECT ".$fields." FROM ".$aTableID;
		
		if($filter!=="")
		{
			$sql.=" WHERE ".$filter;
		}
		
		global $queriesLogFile;
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nQuery DLookByRange: ";
		$aLogString.=$sql;
		updateQueriesLogFile($queriesLogFile,$aLogString);
				
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." $aTableID ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$i=0;
		
		while(!$aRS->EOF)
		{
			foreach ($metaColumns as $keyColumn => $objectColumn)
			{
				$aTableContent[strtolower($keyColumn)][$i] = $aRS->fields[strtolower($keyColumn)];
			}
			
			$i++;
			
			$aRS->MoveNext();
		}
		
		$anInstance = BITAMTable::NewInstanceFromElements($aRepository, strtolower($aTableID), $aTableContent);
		
		BITAMGlobalInstance::AddTableContent(strtolower($aTableID), $anInstance, $filter);

		return $anInstance;
	}
	
	static function NewInstanceFromElements($aRepository, $aTableID, $aTableContent)
	{
		$anInstance = BITAMTable::NewInstance($aRepository);
		
		$anInstance->tableID = $aTableID;
		$anInstance->tableContent = $aTableContent;

		return $anInstance;
	}
}

/***********************************************************************************************/

function formatADateTime($aDateTime, $aFormat)
{
	$formatedDateTime=null;
	$formatedDateTime=BITAMGlobalInstance::GetFormattedDateTime($aDateTime,$aFormat);
	if(!is_null($formatedDateTime))
	{
		return $formatedDateTime;
	}

	$dateTimeArray = privateUnformatDateTime($aDateTime, '');
  	if ($aFormat == '')
	{
		$formatedDateTime=$dateTimeArray[0].'-'.$dateTimeArray[1].'-'.$dateTimeArray[2].' '.$dateTimeArray[3].':'.$dateTimeArray[4].':'.$dateTimeArray[5];
		BITAMGlobalInstance::AddFormattedDateTime($aDateTime,$aFormat,$formatedDateTime);
		return $formatedDateTime;
	}
	$formatedDateTime = '';
  	$formatArray = array(null, null, null, null, null, null, null, null);
 	for ($i = 0; $i < count($formatArray); $i++)
	{
		$formatArray[$i] = 0;
	}
	
	/* @JAPR 2008-11-20: Agregado el soporte para cadenas de texto dentro de los formatos, las cuales se delimitan entre los caracteres " "
	Lo que se hará es un Split del formato para separar por el caracter " y cada dos secciones se formatearán, mientras que las intermedias se dejarán tal cual
	*/
	$blnApply = true;
	$strOriginalFormat = $aFormat;
	//@JAPR 2015-02-06: Agregado soporte para php 5.6
	$arrSubFormats = explode('"', $aFormat);
	$intCount = count($arrSubFormats);
	for ($intCont = 0; $intCont < $intCount; $intCont++)
	{
		$aFormat = $arrSubFormats[$intCont];
		if ($blnApply)
		{
			$inDate = true;
			$lastCh = '';
			for ($i = 0; $i < strlen($aFormat); $i++)
			{
				$ch = $aFormat[$i];
				if ($ch == '\\')
				{
					$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
					$i++;
					if ($i < strlen($aFormat))
					{
						$ch = $aFormat[i];
						$formatedDateTime .= $ch;
					}
				}
				else
				{
					switch ($ch)
					{
						case 'y':
						case 'Y':
							$inDate = true;
							if (($lastCh != '' && $lastCh != 'y' && $lastCh != 'Y') || $formatArray[0] == 4)
							{
								$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
							}
							$formatArray[0]++;
							break;
						case 'm':
						case 'M':
							if ($inDate)
							{
								if (($lastCh != '' && $lastCh != 'm' && $lastCh != 'M') || $formatArray[1] == 4)
								{
									$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
								}
								$formatArray[1]++;
							}
							else
							{
								if (($lastCh != '' && $lastCh != 'm' && $lastCh != 'M') || $formatArray[4] == 2)
								{
									$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
								}
								$formatArray[4]++;
							}
							break;
						//@JAPR 2008-11-20: Agregado el formato de Minutos en inglés (VB lo puede utilizar, así que es válido)
						case 'n':
						case 'N':
								if (($lastCh != '' && $lastCh != 'n' && $lastCh != 'N') || $formatArray[4] == 2)
								{
									$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
								}
								$formatArray[4]++;
								break;
						//@JAPR
						case 'd':
						case 'D':
							$inDate = true;
							//@JAPR 2008-11-20: Agregado el formato 'ddd'
							if (($lastCh != '' && $lastCh != 'd' && $lastCh != 'D') || $formatArray[2] == 4)
							{
								$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
							}
							$formatArray[2]++;
							break;
						case 'h':
						case 'H':
							$inDate = false;
							if (($lastCh != '' && $lastCh != 'h' && $lastCh != 'H') || $formatArray[3] == 2)
							{
								$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
							}
							$formatArray[3]++;
							break;
						case 's':
						case 'S':
							$inDate = false;
							if (($lastCh != '' && $lastCh != 's' && $lastCh != 'S') || $formatArray[5] == 2)
							{
								$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
							}
							$formatArray[5]++;
							break;
						case 'q':
						case 'Q':
							$inDate = true;
							if (($lastCh != '' && $lastCh != 'q' && $lastCh != 'Q') || $formatArray[6] == 1)
							{
								$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
							}
							$formatArray[6]++;
							break;
						case 'w':
						case 'W':
							$inDate = true;
							if (($lastCh != '' && $lastCh != 'w' && $lastCh != 'W') || $formatArray[7] == 2)
							{
								$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
							}
							$formatArray[7]++;
							break;
						default:
							$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
							$formatedDateTime .= $ch;
							break;
					}
				}
				$lastCh = $ch;
			}
		}
		else 
		{
			$formatedDateTime .= $aFormat;
		}
		//Se omite cada sección intermedia pues corresponde con una cadena fija de texto que se debe dejar como se escribió
		$blnApply = !$blnApply;
	}
	$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
	$aFormat = $strOriginalFormat;
	BITAMGlobalInstance::AddFormattedDateTime($aDateTime,$aFormat,$formatedDateTime);
	return $formatedDateTime;
}

/***********************************************************************************************/

function getJoinName ($nom_tabla, $nom_fisicok, $aDESCRIPT_KEYSCollection, $bCubeFiled)
{
	$str="";
	
	$exist=false;
	
	for ($i=0; $i<count($aDESCRIPT_KEYSCollection->Collection); $i++)
	{
		if ($aDESCRIPT_KEYSCollection->Collection[$i]->nom_tabla==$nom_tabla)
		{
			if ($str!="")
			{
				$str.=", ";
			}
			if ($bCubeFiled==true)
			{	//nom_fisicok_join para hacer join con la tabla de hechos
				$str.="B.".$aDESCRIPT_KEYSCollection->Collection[$i]->nom_fisicok_join;
			}
			else 
			{
				//nom_fisicok para hacer join con la tabla de catálagos
				$str.="A.".$aDESCRIPT_KEYSCollection->Collection[$i]->nom_fisicok;
			}
			
			$exist=true;
			
			break;
		}

	}

	if( $exist==false )
	{
		if($bCubeFiled==true)
		{
			$str.="B.".$nom_fisicok;
		}
		else 
		{
			$str.="A.".$nom_fisicok;
		}
	}
	return $str;

}

/***********************************************************************************************/

function getJoinField ($nom_tabla, $nom_fisicok, $aDESCRIPT_KEYSCollection, $cubeFiled)
{	$str="";

	$exist=false;
	
	for ($i=0; $i<count($aDESCRIPT_KEYSCollection->Collection); $i++)
	{
		//if ($aDESCRIPT_KEYSCollection->Collection[$i]->nom_tabla==$nom_tabla && ($nom_fisicok=="" || $aDESCRIPT_KEYSCollection->Collection[$i]->nom_fisicok==$nom_fisicok))
		if ($aDESCRIPT_KEYSCollection->Collection[$i]->nom_tabla==$nom_tabla)
		{
			if ($cubeFiled==true)
			{	//nom_fisicok_join para hacer join con la tabla de hechos
				$str=$aDESCRIPT_KEYSCollection->Collection[$i]->nom_fisicok_join;
			}
			else 
			{
				//nom_fisicok para hacer join con la tabla de catálagos
				$str=$aDESCRIPT_KEYSCollection->Collection[$i]->nom_fisicok;
			}
			$exist=true;
			break;
		}

	}	
	if( $exist==false )
	{
		$str=$nom_fisicok;
	}

	return $str;
}

/***********************************************************************************************/
/*
	getJoinFieldByTableName:
-	Si el nombre de catálogo sí existe en el campo SI_DESCRIP_KEYS.NOM_TABLA y el campo clave de la dimensión
	SÍ es igual al campo join del catálogo, entonces se retorna el join solicitado (catálogo o tabla de hechos)
-	Si el nombre de catálogo sí existe en el campo SI_DESCRIP_KEYS.NOM_TABLA pero el campo clave de la dimensión
	NO es igual al campo join del catálogo, entonces se verifica si sólo existe una vez el nombre de la tabla en SI_DESCRIP_KEYS
	y se toma el join correspondiente a dicha tabla.
-	Si no existe el nombre del catálogo en SI_DESCRIP_KEYS.NOM_TABLA, se considera que el campo join del catálogo y de la tabla de hechos
	se llaman igual que la clave de la dimensión.
*/

function getJoinFieldByTableName ($nom_tabla, $nom_fisicok, $aDESCRIPT_KEYSCollection, $cubeFiled, &$keyEqualToCatJoin=null)
{	
	$str=null;
	$strAux=null;
	$extNomTabla=false;
	$countTbl = 0;
	$keyEqualToCatJoin = false;
	
	for ($i=0; $i<count($aDESCRIPT_KEYSCollection->Collection); $i++)
	{
		if ($aDESCRIPT_KEYSCollection->Collection[$i]->nom_tabla==$nom_tabla)
		{
			$extNomTabla=true; 
			if ($nom_fisicok=="" || $aDESCRIPT_KEYSCollection->Collection[$i]->nom_fisicok==$nom_fisicok)
			{
				//Si el nombre de catálogo sí existe en el campo SI_DESCRIP_KEYS.NOM_TABLA y el campo clave de la dimensión
				//SÍ es igual al campo join del catálogo, entonces se retorna el join solicitado (catálogo o tabla de hechos)
				$keyEqualToCatJoin = true;
				if ($cubeFiled==true)
				{	//nom_fisicok_join para hacer join con la tabla de hechos
					$str=$aDESCRIPT_KEYSCollection->Collection[$i]->nom_fisicok_join;
				}
				else 
				{
					//nom_fisicok para hacer join con la tabla de catálagos
					$str=$aDESCRIPT_KEYSCollection->Collection[$i]->nom_fisicok;
				}
				break;
			}
			else
			{
				$countTbl++;
				if ($cubeFiled==true)
				{	//nom_fisicok_join para hacer join con la tabla de hechos
					$strAux=$aDESCRIPT_KEYSCollection->Collection[$i]->nom_fisicok_join;
				}
				else 
				{
					//nom_fisicok para hacer join con la tabla de catálagos
					$strAux=$aDESCRIPT_KEYSCollection->Collection[$i]->nom_fisicok;
				}
			}
		}
	}
		
	//Si no existe el nombre del catálogo en SI_DESCRIP_KEYS.NOM_TABLA, se considera que el campo join del catálogo y de la tabla de hechos
	//se llaman igual que la clave de la dimensión.
	if($extNomTabla==false)
	{
		$str=$nom_fisicok;
	}
	else
	{
		//Si el nombre de catálogo sí existe en el campo SI_DESCRIP_KEYS.NOM_TABLA pero el campo clave de la dimensión
		//NO es igual al campo join del catálogo, entonces se verifica si sólo existe una vez el nombre de la tabla en SI_DESCRIP_KEYS
		//y se toma el join correspondiente a dicha tabla.
		if($keyEqualToCatJoin==false && $countTbl==1)
		{
			$str = $strAux;
		}
	}
	
	return $str;
}

/***********************************************************************************************/

function getTimeAsString()
{	
	$aTimeString="";
	$aTimeArray=array();
	
	$aTimeArray = localtime(time(), 1) ; 

	$hour = $aTimeArray["tm_hour"]; 
	$minutes = $aTimeArray["tm_min"] ; 
	$seconds = $aTimeArray["tm_sec"]; 
	
	$aTimeString=$hour.":".$minutes.":".$seconds;
	return $aTimeString;
}

function updateTestLogFile($aLogString)
{           
	global $queriesLogFile;
	error_log($aLogString, 3, $queriesLogFile);
}

function getValidFieldNameFromStr($aName, $iLength = 18)
{
	global $arrInvalidChars, $arrValidChars;
	
	$strValidCharsPattern = '[^A-Za-z0-9_]';
	$aName = str_replace($arrInvalidChars, $arrValidChars, $aName);
	//@JAPR 2015-02-06: Agregado soporte para php 5.6
	$arrValidCharsFromStr = explode($strValidCharsPattern, $aName);
	$aFieldName = implode($arrValidCharsFromStr, '');
	$aFieldName = substr($aFieldName, 0, $iLength);
	return $aFieldName;
}
?>