<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

require_once("survey.inc.php");

$arraySurveyIDs = array();
if(array_key_exists("strSurveyIDs",$_GET))
{
	$strSurveyIDs= trim($_GET["strSurveyIDs"]);
	$arraySurveyIDs = explode("|", $strSurveyIDs);
}

$showMessage = true;

ob_start();
if($showMessage)
{
?>
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<title><?=translate("Copy Surveys")?></title>
			<link rel="stylesheet" type="text/css" href="css/main.css">
		</head>
	<body>
	<table id="InfoCopySurveys" name="InfoCopySurveys">
	<tr>
	<td>
	</td>
	</tr>
	</table>
	<script language="JavaScript"> 
	infoCopySurveyTable=document.getElementById("InfoCopySurveys");
	var strInfo='';
	</script> 
<?
}

if($showMessage)
{
	echo("<script language='JavaScript'>");
	echo("strInfo='(".date("H:i:s").") - ".translate("Begin of process")."';\n");
	echo("newRow = infoCopySurveyTable.insertRow(0);");
	echo("newCell = newRow.insertCell();");
	echo("newCell.innerHTML=\"<span class='infoGen'>\"+strInfo+\"</span>\";");
	echo(str_repeat(" ",4096));
	echo("</script>");
	ob_flush();
	flush();
}

$numSurveys = count($arraySurveyIDs);
for($i=0; $i<$numSurveys; $i++)
{
	BITAMSurvey::copySurvey($theRepository, $arraySurveyIDs[$i], $showMessage);
}

if($showMessage)
{
	echo("<script language='JavaScript'>");
	echo("strInfo='(".date("H:i:s").") - ".translate("End of process")."';\n");
	echo("newRow = infoCopySurveyTable.insertRow(0);");
	echo("newCell = newRow.insertCell();");
	echo("newCell.innerHTML=\"<span class='infoGen'>\"+strInfo+\"</span>\";");
	echo(str_repeat(" ",4096));
	echo("</script>");
	ob_flush();
	flush();
}

if($showMessage)
{
?>
	</body>
	</html>
<?
}
?>