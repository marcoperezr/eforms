<?php
/* Este archivo se puede invocar directamente para iniciar una sesión de exportación de imagenes (Fotos y Firmas) según los parámetros, 
sin necesidad de que previamente se esté logeado al eForms
	El proceso recibe como parámetros las encuestas, rangos de fechas y usuarios (preferentemente sólo una encuesta) que deberán ser procesados,
buscará en la tabla de imagenes de eForms aquellas que cumplan con los filtros indicados y simplemente copiará a la carpeta de eForms configurada
en los settings los archivos de imagenes anteponiendo como prefijo un valor fijo configurado en este archivo php, dentro de la carpeta de eBavel
que se formará también utilizando valores fijos para la aplicación y Forma de la que se trate (se podría intentar obtener esa información directamente
de la encuesta, sin embargo GNC no utilizó las configuraciones correspondientes a eBavel a nivel de encuesta, por lo que no hay seguridad que estén
dichos valores, en lugar de eso se utilizará un array indexado por el ID de la encuesta de la que se trate)
*/

//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
session_start();
$UserName = '';
$Password = '';
$theUserName = '';
$theRepositoryName = '';
$theRepository = null;
$theUser = null;
$arrSurveyIDs = array();
$arrUserID = array();
$strFormat = '';
//Datos de los filtros
$strStartDate = '';
$strEndDate = '';
//Arrays para procesamiento en eBavel por cada encuesta de eForms (se debe indexar por el SurveyID, si no se encuentra el SurveyID entonces se usará
//el primer elemento del array, si no hay elementos simplemente no se puede continuar)
$arreBavelApps = array('BAPP10005');
$arreBavelForms = array('FRM_10117');
$arreBavelPrefix = array('bft9999.');

require_once("initialize.php");
require_once("utils.inc.php");
require_once("config.php");
require_once("survey.inc.php");
require_once("report.inc.php");
require_once('settingsvariable.inc.php');
global $BITAMRepositories;

$EncryptedPassword = '';
$UserName = getParamValue('UserName', 'both', '(string)');
$Password = getParamValue('Password', 'both', '(string)');
$EncryptedPassword = getParamValue('EncryptedPassword', 'both', '(string)');
$theRepositoryName = getParamValue('Repository', 'both', '(string)');
$strFormat = getParamValue('Format', 'both', '(string)');
$arrSurveyIDs = getParamValue('SurveyID', 'both', '(array)');
$arrUserID = getParamValue('UserID', 'both', '(array)');
$strStartDate = getParamValue('StartDate', 'both', '(string)', true);
if (is_null($strStartDate) || trim($strStartDate) == '') {
	$strStartDate = date('Y-m-d').' 00:00:00';
	$strStartDate = days_add($strStartDate, -1);
}
$strEndDate = getParamValue('EndDate', 'both', '(string)', true);
if (is_null($strEndDate) || trim($strEndDate) == '') {
	$strEndDate = date('Y-m-d').' 23:59:59';
	$strEndDate = days_add($strEndDate, -1);
}

$UserName = trim($UserName);
$theRepositoryName = trim($theRepositoryName);
$strFormat = trim($strFormat);

//Valida la información de los arrays de eBavel, obteniendo el primero de cada uno, el cual será usado en caso de no encontrar uno indexado por
//encuesta (tentativamente el primero tendrá el índice 0, pero depende realmente de como configuren el array)
if (count($arreBavelApps) == 0 || count($arreBavelForms) == 0 || count($arreBavelPrefix) == 0) {
	die('errorDesc='.translate('Some eBavel data is missing, unable to continue'));
}
$strFirsteBavelApp = '';
$strFirsteBavelForm = '';
$strFirsteBavelPrefix = '';
foreach ($arreBavelApps as $anId => $aData) {
	$strFirsteBavelApp = $aData;
	break;
}
foreach ($arreBavelForms as $anId => $aData) {
	$strFirsteBavelForm = $aData;
	break;
}
foreach ($arreBavelPrefix as $anId => $aData) {
	$strFirsteBavelPrefix = $aData;
	break;
}

//@JAPR 2013-12-18: Agregado el parámetro para permitir recibir passwords encriptados
if (trim($EncryptedPassword) != '') {
	$Password = BITAMDecryptPassword($EncryptedPassword);
}

//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
$blnIncludePhotos = getParamValue('AddPhotos', 'both', '(int)');
//@JAPR

$blnNewExcelMethod = getParamValue('newExcelMethod', 'both', '(int)');

//Inicia la conexión con el repositorio según los datos especificados
if ($theRepositoryName == '' || !array_key_exists($theRepositoryName, $BITAMRepositories))
{
	//header("Location: index.php?error=1");
	//exit();
	die('errorDesc='.translate('The specified repository is not registered in this service'));
}
$theRepository = $BITAMRepositories[$theRepositoryName];
if (!$theRepository->open())
{
	//header("Location: index.php?error=2");
	//exit();
	die('errorDesc='.translate('Can not connect to Repository, please contact your System Administrator'));
}

//Obtiene el Path hacia el eBavel
$objSetting = BITAMSettingsVariable::NewInstanceWithName($theRepository, 'EBAVELURL');
if (is_null($objSetting) || trim($objSetting->SettingValue == '')) {
	die("errorDesc=No eBavel URL");
}
$arrURL = parse_url($objSetting->SettingValue);
$streBavelPath = str_replace("/", "\\", $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']));

//Obtiene el Path hacia el eForms
$script = $_SERVER["REQUEST_URI"];
$position = strrpos($script, "/");
$streFormsPath = str_replace("/", "\\", $_SERVER["DOCUMENT_ROOT"].substr($script, 0, $position)."\\")."surveyimages\\".$theRepositoryName."\\";

if (is_null($arrSurveyIDs) || !is_array($arrSurveyIDs) || count($arrSurveyIDs) == 0) {
	$arrSurveyIDs = array();
	//Obtiene todas las encuestas del repositorio
	$objSurveyColl = BITAMSurveyCollection::NewInstance($theRepository);
	if (!is_null($objSurveyColl)) {
		foreach ($objSurveyColl as $surveyInstance) {
			$arrSurveyIDs[] = $surveyInstance->SurveyID;
		}
	}
}

if ($UserName == '') {
	die('errorDesc='.translate('User account not specified'));
}

$theUserName = $UserName;
//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
$theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
if (is_null($theUser))
{
	//header("Location: index.php?error=3");
	//exit();
	die('errorDesc='.translate('You must enter a valid UserID and Password to access this resource').' ('.$theUserName.')');
}

//@JAPR 2009-02-10: Corregido el login desde enlaces externos para usuarios Bitam, falta integrar el cambio para usuarios LDAP pero desconozco como llegan los
//parámetros para esos casos
if (BITAMDecryptPassword($theUser->Password) != $Password)
{
	setcookie("PABITAM_RepositoryName", $theRepositoryName);
    setcookie("PABITAM_UserName", $theUserName);
	die('errorDesc='.translate('You must enter a valid UserID and Password to access this resource').' ('.$theUserName.')');
	//header("Location: index.php?error=3");
}
else
{	
	$_SESSION["PABITAM_RepositoryName"]=$theRepositoryName;
	//@JAPR 2013-05-08: Corregido un bug, debido al cambio para Agrega el Score al reporte (#28772), se necesitaba este valor y no estaba
	//asignandose en este punto de entrada para exportaciones independientes del login de eForms
    $_SESSION["PABITAM_UserID"] = $theUser->UserID;
	//@JAPR
	$_SESSION["PABITAM_UserName"]=$theUserName;
	$_SESSION["PABITAM_Password"]=$Password;
	$_SESSION["PAdisplayAllFrames"]=1;
}

//Recorre todas las encuestas especificadas y procesa las fotos de cada una según los parámetros
$strUserIDs = '';
if (count($arrUserID) > 0) {
	$strUserIDs = implode(',', $arrUserID);
}
$strStartDate = $theRepository->DataADOConnection->DBTimeStamp($strStartDate);
$strEndDate = $theRepository->DataADOConnection->DBTimeStamp($strEndDate);

foreach ($arrSurveyIDs as $intSurveyID) {
	$surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $intSurveyID);
	if (is_null($surveyInstance)) {
		echo('errorDesc='.translate('Survey not found').' ('.$intSurveyID.')');
		continue;
	}
	
	//Obtiene la lista de imagenes a procesar
	$sql = "SELECT B.UserID, B.DateID, A.PathImage 
		FROM SI_SV_SurveyAnswerImage A, {$surveyInstance->SurveyTable} B 
		WHERE A.MainFactKey = B.FactKey AND A.SurveyID = {$intSurveyID} 
     		AND B.DateID BETWEEN {$strStartDate} AND {$strEndDate}";
	if ($strUserIDs != '') {
		$sql .= " AND B.UserID IN ({$strUserIDs}) ";
	}
	echo("<br>\r\n $surveyInstance->SurveyName");
	echo("<br>\r\n $sql");
    $aRS = $theRepository->DataADOConnection->Execute($sql);
    if ($aRS === false) {
		echo("<br>\r\n (".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg());
		continue;
    }
    
    //Asigna para esta encuesta los valores de objetos de eBavel a utilizar
    $streBavelApp = @$arreBavelApps[$intSurveyID];
    if (is_null($streBavelApp)) {
    	$streBavelApp = $strFirsteBavelApp;
    }
    $streBavelForm = @$arreBavelForms[$intSurveyID];
    if (is_null($streBavelForm)) {
    	$streBavelForm = $strFirsteBavelForm;
    }
    $streBavelPrefix = @$arreBavelPrefix[$intSurveyID];
    if (is_null($streBavelPrefix)) {
    	$streBavelPrefix = $strFirsteBavelPrefix;
    }
    
    //Empieza a mover la lista de imagenes obtenidas, para lo cual quita la referencia de foldes de eForms y agrega el prefijo a utilizar para eBavel
    while (!$aRS->EOF) {
    	$strImagePath = trim((string) @$aRS->fields['pathimage']);
    	if ($strImagePath != '') {
    		$arrPath = explode('/', $strImagePath);
    		$strImageName = trim($arrPath[count($arrPath) -1]);
    		if ($strImageName != '') {
    			$streBavelImageName = $streBavelPrefix.$strImageName;
    			$strCompleteSourcePath = str_replace("/", "\\", $streFormsPath.$strImagePath);
    			$strCompletePath = str_replace("/", "\\", $streBavelPath."uploads\\".$streBavelApp."\\".$streBavelForm."\\".$streBavelImageName);
    			$status = 0;
    			$status = @copy($strCompleteSourcePath, $strCompletePath);
    			echo("<br>\r\n Saving eBavel image from {$strCompleteSourcePath} to {$strCompletePath}, Status == {$status}");
    		}
    	}
    	$aRS->MoveNext();
    }
}

die("<br>\r\n Process finished");

?>
