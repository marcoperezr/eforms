<?php
//@JAPRDescontinuada: Se reemplazó por invocación a report.inc.php con la nueva propiedad exportToPDF == true
//Antes de eliminar este archivo, se deben copiar los dos métodos fuera de la clase ya que son invocados ahora por report.inc.php directamente
require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("question.inc.php");
require_once("report.inc.php");

class BITAMExportSurvey extends BITAMObject
{
	public $FactKey;
	public $SurveyID;
	public $SurveyName;
	public $UserID;
	public $UserName;
	public $DateID;
	public $QuestionFields;
	public $NumQuestionFields;
	public $Questions;
	public $QuestionText;
	public $QuestionIDs;
	public $QuestionIDsFlip;
	public $QComments;
	public $QImages;
	public $Signature;

	public $Survey;
	public $FactKeyDimVal;
	public $HasDynamicSection;
	public $DynamicSectionID;
	public $DynamicSection;
	public $DynQuestionCollection;
	public $Catalog;
	public $ChildDynamicAttribID;
	public $ArrayDynSectionValues;
	
	public $doc;
	public $fileName = "";
	public $filePath = "";
	public $orientation;
	public $unidad;
	public $format;
	public $displayMode;
	public $aListFontsAdded = array();

	function __construct($aRepository, $aSurveyID)
	{
		BITAMObject::__construct($aRepository);
		$this->FactKey = -1;
		$this->SurveyID = $aSurveyID;
		$this->Survey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		$this->SurveyName = "";
		$this->UserID = -1;
		$this->UserName = "";
		$this->DateID = "";
		$this->FactKeyDimVal = -1;

		$this->QuestionFields = array();
		$this->FieldTypes = array();
		$this->QuestionText = array();
		$this->QuestionIDs = array();
		$this->QuestionIDsFlip = array();
		$this->QComments = array();
		$this->QImages = array();
		$this->Signature = "";
		
		$this->HasDynamicSection = false;
		$this->DynamicSectionID = -1;
		$this->DynamicSection = null;
		$this->DynQuestionCollection = null;
		$this->Catalog = null;
		$this->ChildDynamicAttribID = -1;
		$this->ArrayDynSectionValues = array();
		
		$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $aSurveyID);
		
		if($dynamicSectionID>0)
		{
			$this->HasDynamicSection = true;
			$this->DynamicSectionID = $dynamicSectionID;
			$this->DynamicSection = BITAMSection::NewInstanceWithID($aRepository, $this->DynamicSectionID);
			
			//Obtenemos el ChildDynamicAttribID
			$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($aRepository, $this->DynamicSection->CatMemberID);
			
			//Obtener el siguiente CatMemberID
			$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->DynamicSection->CatalogID." AND MemberOrder > ".$catMemberInstance->MemberOrder." ORDER BY MemberOrder ASC";
			
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
	
			if(!$aRS->EOF)
			{
				//Asignamos el atributo hijo
				$this->ChildDynamicAttribID = (int)$aRS->fields["parentid"];
				
				//Obtenemos coleccion de las preguntas de la seccion dinamica
				$this->DynQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->DynamicSectionID);
				
				//Obtenemos el catalogo
				$this->Catalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->DynamicSection->CatalogID);
			}
			else 
			{
				//Se vuelven a quedar los valores como si no hubiera seccion dinamica
				$this->HasDynamicSection = false;
				$this->DynamicSectionID = -1;
				$this->DynamicSection = null;
			}
		}

		$questions = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID, null, null);
		$this->Questions = $questions;
		$numFields = count($questions->Collection);

		for($i=0; $i<$numFields; $i++)
		{
			$fieldName = $questions->Collection[$i]->SurveyField;
			$fieldType = $questions->Collection[$i]->QTypeID;

			if($fieldType==1)
			{
				$this->$fieldName = 0;
			}
			else
			{
				$this->$fieldName = "";
			}

			$this->QuestionFields[$i] = $fieldName;
			$this->FieldTypes[$i] = $fieldType;
			
			//@JAPR 2012-11-29: Corregido un bug durante la exportación a PDF
			//Aunque no se encontraron datos, se deben agregar los campos tipo jerarquía para que no falle la generación del PDF
			if($questions->Collection[$i]->QTypeID==qtpSingle && $questions->Collection[$i]->UseCatalog==true)
			{
				$fieldNameHierarchy = $fieldName."_Hierarchy";
				$this->$fieldNameHierarchy = "";
			}
			//@JAPR
			
			//15Abril2013: Que se despliegue la descripción de la pregunta
			if (trim($questions->Collection[$i]->QuestionText)=="") {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$questionText = $questions->Collection[$i]->getAttributeName();
			}
			else {
				$questionText = $questions->Collection[$i]->QuestionText;
			}

			$this->QuestionText[$i] = $questions->Collection[$i]->QuestionNumber.". ".$questionText;
			$this->QuestionIDs[$i] = $questions->Collection[$i]->QuestionID;
			$this->QComments[$i] = "";
			$this->QImages[$i] = "";
		}
        
		$this->NumQuestionFields = $numFields;
		$this->QuestionIDsFlip = array_flip($this->QuestionIDs);
	}

	static function NewInstance($aRepository, $aSurveyID)
	{
		return new BITAMExportSurvey($aRepository, $aSurveyID);
	}

	static function NewInstanceWithID($aRepository, $aSurveyID, $aFactKey)
	{
        
        $anInstance = null;

		if (((int)$aFactKey) < 0 || ((int)$aSurveyID) < 0 )
		{
			return $anInstance;
		}

		$aSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);

		$anInstance = BITAMExportSurvey::NewInstance($aRepository, $aSurveyID);

		$sql = "SELECT ".$aSurvey->SurveyTable.". FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal ";

		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = " AND ";
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//Conchita se agrego para las tipo numericas
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric) 
			{
				//@JAPR 2012-11-23: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSketch:
					//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case qtpSketchPlus:
					//@JAPR
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					$memberParentID = $catParentID;
					$catTable = "RIDIM_".$catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
						$strAnd = " AND ";
					}
				}
				else {
					$catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
	
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					
					$sql.=", ".$catTable.".".$attribField;
	
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en elFROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
	
					$arrayTables[$catTable] = $catTable;
	
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.=" AND ".$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
				}
				//@JAPR
			}
		}
		
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat;
		$sql .= " WHERE ".$aSurvey->SurveyTable.".FactKey = ".$aFactKey." ".$joinCat;
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMExportSurvey::NewInstanceFromRS($aRepository, $aSurveyID, $aRS);
		}

		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aSurveyID, $aRS)
	{
		$anInstance = BITAMExportSurvey::NewInstance($aRepository, $aSurveyID);
		$aSurveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		$anInstance->SurveyName = $aSurveyInstance->SurveyName;
		$anInstance->FactKey = (int) $aRS->fields["factkey"];
        
		$anInstance->UserID = (int) $aRS->fields["userid"];
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$anInstance->UserName = BITAMeFormsUser::GetUserName($aRepository, $anInstance->UserID);
		$strHour = $aRS->fields["starttime"];
		$anInstance->DateID = substr($aRS->fields["dateid"], 0, 10)." ".substr($strHour, 0, 8);
		$anInstance->FactKeyDimVal = (int)$aRS->fields["factkeydimval"];
        
		//Obtenemos el arreglo $this->ArrayDynSectionValues si es que existe seccion dinamica
		if($anInstance->HasDynamicSection==true)
		{
			$anInstance->getDynamicSectionValues();
		}
		
		for ($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//El primer IF es para identificar si las preguntas son dinamicas y adicionalmente que la propiedad si lo indique
			//que tengan seccion dinamica
			if($anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true)
			{
				//@JAPR 2013-01-08: Corregido un bug, hay varios tipos de pregunta que no deben tener campo en la paralela
				$blnValid = true;
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpShowValue:
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSketch:
					//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case qtpSketchPlus:
					//@JAPR
					case qtpSignature:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValid = false;
						break;
				}
				
				if (!$blnValid) {
					continue;
				}
				//@JAPR
				
				$fieldName =$anInstance->QuestionFields[$i];
				$strValue = "";
				//Obtenemos el valor de esta pregunta a partir del arrreglo $this->ArrayDynSectionValues
				//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
				//porque todavia no se sabe exactamente como se va a comportar
				if($anInstance->Questions[$i]->QTypeID!=qtpMulti)
				{
					foreach ($anInstance->ArrayDynSectionValues as $element)
					{
						$idxName = $anInstance->QuestionFields[$i];
						
						if($strValue!="")
						{
							$strValue.="; ";
						}
						
						$strValue.=$element["desc"]."=".$element[$idxName];
					}
				}
				else 
				{
					//Si es entrada de tipo Checkbox, el valor seran
					//todo los valores seleccionados concatenados con ;
					if($anInstance->Questions[$i]->MCInputType==0)
					{
						foreach ($anInstance->ArrayDynSectionValues as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							$valueInteger = (int)$element[$idxName];
							
							if($valueInteger!=0)
							{
								if($strValue!="")
								{
									$strValue.=";";
								}
								
								$strValue.=$element["desc"];
							}
						}
					}
					else 
					{
						foreach ($anInstance->ArrayDynSectionValues as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							
							if($strValue!="")
							{
								$strValue.="; ";
							}
							
							$strValue.=$element["desc"]."=".$element[$idxName];
						}
					}
				}
				
				$anInstance->$fieldName = $strValue;
			}
			else if($anInstance->Questions[$i]->QTypeID==qtpMulti && $anInstance->Questions[$i]->QDisplayMode==dspVertical && $anInstance->Questions[$i]->MCInputType==1 && $anInstance->Questions[$i]->IsMultiDimension==0 && $anInstance->Questions[$i]->UseCategoryDimChoice!=0)
			{
				$fieldName =$anInstance->QuestionFields[$i];
				$fieldType =$anInstance->FieldTypes[$i];

				//En el caso de las preguntas con dimension categoria vamos a obtener la informacion desde una funcion q realice 
				//en una cadena con enters las respuestas q fueron capturadas
				$LN = "; ";
				$strCatDimValAnswers = BITAMReportCollection::getStrCatDimValAnswers($aRepository, $anInstance->Survey, $anInstance->Questions[$i], $anInstance->FactKeyDimVal, $LN, false);

				$anInstance->$fieldName = $strCatDimValAnswers;
				
				if(trim($anInstance->$fieldName)=="")
				{
					$anInstance->$fieldName = "NA";
				}
			}
			else 
			{
				if ($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true)
				{
					$fieldName =$anInstance->QuestionFields[$i];
					//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($anInstance->Survey->DissociateCatDimens) {
						$memberParentID = $objQuestion->CatIndDimID;
						$attribField = "DSC_".$memberParentID;
						if(array_key_exists(strtolower($attribField),$aRS->fields))
						{
							$anInstance->$fieldName = trim($aRS->fields[strtolower($attribField)]);
							
							//Obtiene la jerarquía del catálogo pero utilizando los datos grabados en la tabla paralela
							$strCatDimValAnswers = trim(@$aRS->fields[strtolower($objQuestion->SurveyCatField)]);
							$arrayCatValues = array();
							if ($strCatDimValAnswers != '') {
								$arrayCatPairValues = explode('_SVElem_', $strCatDimValAnswers);
								$arrayCatClaDescrip = array();
								$arrayTemp = array();
								$arrayInfo = array();
								foreach($arrayCatPairValues as $element)
								{
									$arrayInfo = explode('_SVSep_', $element);
									//Obtener el cla_descrip y almacenar (key_1178)
									$arrayTemp = explode('_', $arrayInfo[0]);
									$arrayCatClaDescrip[] = $arrayTemp[1];
									//Obtener el valor del catalogo
									$arrayCatValues[] = $arrayInfo[1];
								}
							}
							
							//22-Ago-2012: Agregar un campo que contenga la respuesta pero con la jerarquía concatenada
							$fieldNameHierarchy = $fieldName."_Hierarchy";
							$anInstance->$fieldNameHierarchy = implode(', ', $arrayCatValues);
						}
						
						if(trim($anInstance->$fieldName)=="")
						{
							$anInstance->$fieldName = "NA";
						}
					}
					else {
						$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
						$attribField = "DSC_".$memberParentID;
						
						if(array_key_exists(strtolower($attribField),$aRS->fields))
						{
							$anInstance->$fieldName = trim($aRS->fields[strtolower($attribField)]);
							
							//22-Ago-2012: Agregar un campo que contenga la respuesta pero con la jerarquía concatenada
							$fieldNameHierarchy = $fieldName."_Hierarchy";
							$catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
							$anInstance->$fieldNameHierarchy = BITAMReport::getCatHierarchyByMemberValue($aRepository, $anInstance->Questions[$i]->CatalogID, $anInstance->Questions[$i]->CatMemberID, $attribField, $aRS->fields[strtolower("RIDIM_".$catParentID."KEY")], $aRS->fields[strtolower($attribField)]);
							
							if(trim($anInstance->$fieldName)=="")
							{
								$anInstance->$fieldName = "NA";
							}
						}
						else
						{
							if(trim($anInstance->$fieldName)=="")
							{
								$anInstance->$fieldName = "NA";
							}
						}
					}
					//@JAPR
				}
				else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
				{
					$fieldName =$anInstance->QuestionFields[$i];
					$fieldType =$anInstance->FieldTypes[$i];
					
					//En el caso de las matrices de simple choice vamos a obtener la informacion desde una funcion q realice 
					//en una cadena con enters las respuestas q fueron capturadas
					$strAnswersSCMatrix = $anInstance->getStrAnswersSCMatrix($anInstance->Questions[$i]);
					
					$anInstance->$fieldName = $strAnswersSCMatrix;
					
					if(trim($anInstance->$fieldName)=="")
					{
						$anInstance->$fieldName = "NA";
					}
				}
				else
				{
					$fieldName =$anInstance->QuestionFields[$i];
					$fieldType =$anInstance->FieldTypes[$i];
	
					if(array_key_exists(strtolower($fieldName),$aRS->fields))
					{
						if($fieldType==1)
						{
							$anInstance->$fieldName = (float) $aRS->fields[strtolower($fieldName)];
						}
						else
						{
							if($fieldType == qtpMulti)
							{
								$anInstance->$fieldName = trim($aRS->fields[strtolower($fieldName)]);
								
								if(trim($anInstance->$fieldName)=="")
								{
									$anInstance->$fieldName = "NA";
								}
							}
							else 
							{
								$anInstance->$fieldName = trim($aRS->fields[strtolower($fieldName)]);
		
								if(trim($anInstance->$fieldName)=="")
								{
									$anInstance->$fieldName = "NA";
								}
							}
						}
					}
					else
					{
						if($fieldType==1)
						{
							$anInstance->$fieldName = 0;
						}
						else
						{
							if(trim($anInstance->$fieldName)=="")
							{
								$anInstance->$fieldName = "NA";
							}
						}
					}
				}
			}
		}
		
		//Obtenemos los comentarios 
		$anInstance->getAllQuestionComments();
		
		//Obtenemos las imagenes
		$anInstance->getAllQuestionImages();
        
        if($anInstance->Survey->HasSignature) {
            $anInstance->getSignature();
        }
        
		return $anInstance;
	}
	
	function getStrAnswersSCMatrix($questionInstance)
	{
		$strAnswersSCMatrix = "";
		$LN = chr(13).chr(10);
		$LN = "; ";
		
		$tableMatrixData = "SVSurveyMatrixData_".$this->Survey->ModelID;
		
		if($questionInstance->OneChoicePer==0)
		{
			$tableName = "SI_SV_QAnswers";
		}
		else 
		{
			$tableName = "SI_SV_QAnswersCol";
		}

		$sql = "SELECT A.DisplayText, b.dim_value FROM ".$tableName." A LEFT JOIN ".$tableMatrixData." B 
				ON A.QuestionID = B.QuestionID AND A.ConsecutiveID = B.ConsecutiveID 
				AND A.QuestionID = ".$questionInstance->QuestionID." 
				WHERE B.FactKey = ".$this->FactKey." AND B.FactKeyDimVal = ".$this->FactKeyDimVal." 
				ORDER BY A.SortOrder";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$tableMatrixData." ".translate("tables").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$strDisplayText = $aRS->fields["displaytext"];
			$strAnswerText = $aRS->fields["dim_value"];
			
			if(trim($strAnswerText)=="")
			{
				$strAnswerText = "NA";
			}
			
			if($strAnswersSCMatrix!="")
			{
				$strAnswersSCMatrix.=$LN;
			}
			
			$strAnswersSCMatrix.=$strDisplayText." = ".$strAnswerText;

			$aRS->MoveNext();
		}
		
		return $strAnswersSCMatrix;
	}

	function getDynamicSectionValues()
	{
		//Obtenemos coleccion de las preguntas de la seccion dinamica
		$dynQuestionCollection = $this->DynQuestionCollection;
		//Obtenemos la instancia de la seccion dinamica
		$dynSectionInstance = $this->DynamicSection;
		//Obtenemos la instancia de Survey		
		$aSurvey = $this->Survey;
		//Obtenemos la instancia del catalogo
		$catalogInstance = $this->Catalog;
		//Obtenemos la instancia del atributo del catálogo que genera las secciones dinámicas
		$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($this->Repository, $this->DynamicSection->CatMemberID);
		
		//Obtener la primera pregunta de seleccion simple de catalogo para obtener los valores del hijo del atributo 
		//de la seccion dinamica
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." 
				AND QTypeID = ".qtpSingle." AND UseCatalog = 1 AND CatalogID = ".$dynSectionInstance->CatalogID." 
				ORDER BY QuestionNumber";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS || $aRS->EOF)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//El key subrrogado del catálogo es irrelevante cuando se usa DissociateCatDimens, sin embargo se dejará sólo como referencia para no cambiar
		//la manera en como se generaba el Array y por compatibilidad con encuestas que aun no han sido migradas a esta característica
		$questionID = (int)$aRS->fields["questionid"];
		$dimCatFieldID = "dim_".$questionID;
		
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		if ($aSurvey->DissociateCatDimens) {
			//En este caso son las dimensiones usadas como atributo en la sección dinámica y la del siguiente atributo (en caso de tener preguntas
			//multiple choice, de lo contrario, se repite la propia dimensión de la dinámica) las que hacen join, ya que la dimensión del catálogo
			//se separó de la tabla paralela aunque internamente sigue una referencia a sus keys subrrogados
			if ($dynSectionInstance->ChildCatMemberID > 0) {
				$childCatMemberInstance = BITAMCatalogMember::NewInstanceWithID($this->Repository, $dynSectionInstance->ChildCatMemberID);
			}
			else {
				$childCatMemberInstance = $catMemberInstance;
			}
			
			//Atributo de la sección dinámica
			$catParentID = $catMemberInstance->IndDimID;
			$catTable = "RIDIM_".$catParentID;
			$fromCat.=", ".$catTable;
			$joinField = "RIDIM_".$catParentID."KEY";
			$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
			$arrayTables[$catTable] = $catTable;
			$dimCatParentDsc = $catTable.".DSC_".$catMemberInstance->IndDimID;
			
			//Atributo de las preguntas múltiple choice (si existen, si no entonces es el mismo de arriba)
			$catParentID = $childCatMemberInstance->IndDimID;
			$catTable = "RIDIM_".$catParentID;
			if(!isset($arrayTables[$catTable])) {
				$fromCat.=", ".$catTable;
				$joinField = "RIDIM_".$catParentID."KEY";
				$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
			}
			
			$dimCatFieldDsc = $catTable.".DSC_".$childCatMemberInstance->IndDimID;
		}
		else {
			$dimCatFieldDsc = "B.DSC_".$this->ChildDynamicAttribID;
			$dimCatFieldJoin = "RIDIM_".$catalogInstance->ParentID."KEY";
			$dimCatTable = "RIDIM_".$catalogInstance->ParentID;
			$dimCatParentDsc = "B.DSC_".$catMemberInstance->ClaDescrip;
		}
		
		$sql = "SELECT A.".$dimCatFieldID." AS id, ".$dimCatFieldDsc." AS description ";
		
		foreach($dynQuestionCollection as $aQuestion)
		{
			//@JAPR 2013-01-08: Corregido un bug, hay varios tipos de pregunta que no deben tener campo en la paralela
			switch ($aQuestion->QTypeID) {
				case qtpShowValue:
				case qtpPhoto:
				case qtpDocument:
				case qtpAudio:
				case qtpVideo:
				case qtpSketch:
				//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				case qtpSketchPlus:
				//@JAPR
				case qtpSignature:
				case qtpMessage:
				case qtpSkipSection:
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				case qtpSync:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					break;
				
				default:
					$sql .= ", dim_".$aQuestion->QuestionID;
					break;
			}
			//@JAPR
		}
		
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		if ($this->Survey->UseDynamicPageField) {
			$sql .= ", A.DynamicPageDSC";
		}
		
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if ($aSurvey->DissociateCatDimens) {
			$sql .= ", A.DynamicOptionDSC, A.DynamicValue";
			$sql .= " FROM ".$aSurvey->SurveyTable." A, ".$aSurvey->FactTable." B ".$fromCat;
			$sql .= " WHERE A.FactKey = B.FactKey ".$joinCat." AND A.FactKeyDimVal = ".$this->FactKeyDimVal;
		}
		else {
			$sql .= " FROM ".$aSurvey->SurveyTable." A, ".$dimCatTable." B ";
			$sql .= " WHERE A.".$dimCatFieldID." = B.".$dimCatFieldJoin." AND A.FactKeyDimVal = ".$this->FactKeyDimVal;
		}
		
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		if ($this->Survey->UseStdSectionSingleRec) {
			$sql .= " AND A.EntrySectionID = ".$dynSectionInstance->SectionID;
		}
		//@JAPR
		$sql .= " ORDER BY A.FactKey ";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2013-02-06: Agregada la opción para que las preguntas estándar se graben en un registro único
		//No debe marcar error si no hay datos, por lo menos no a partir de la versión donde existen registros únicos
		if (!$aRS || ($aRS->EOF && !$this->Survey->UseStdSectionSingleRec))
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$count = 0;
		while(!$aRS->EOF)
		{
			$this->ArrayDynSectionValues[$count] = array();
			$this->ArrayDynSectionValues[$count]["id"] = (int)$aRS->fields["id"];
			$this->ArrayDynSectionValues[$count]["desc"] = $aRS->fields["description"];
			
			foreach($dynQuestionCollection as $aQuestion)
			{
				//@JAPR 2013-01-08: Corregido un bug, hay varios tipos de pregunta que no deben tener campo en la paralela
				$blnValid = true;
				switch ($aQuestion->QTypeID) {
					case qtpShowValue:
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSketch:
					//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case qtpSketchPlus:
					//@JAPR
					case qtpSignature:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValid = false;
						break;
				}
				
				if (!$blnValid) {
					continue;
				}
				//@JAPR
				
				$fieldName = "dim_".$aQuestion->QuestionID;
				if($aQuestion->QTypeID!=qtpMulti)
				{
					$this->ArrayDynSectionValues[$count][$fieldName] = $aRS->fields[$fieldName];
				}
				else 
				{
					//Si es de tipo Multiple Choice Numerica le realizamos un float
					if($aQuestion->MCInputType==1)
					{
						$this->ArrayDynSectionValues[$count][$fieldName] = (float)$aRS->fields[$fieldName];
					}
					else 
					{
						$this->ArrayDynSectionValues[$count][$fieldName] = $aRS->fields[$fieldName];
					}
				}
			}

			$count++;
			$aRS->MoveNext();
		}
	}
	
	function getAllQuestionComments()
	{
		$sql = "SELECT QuestionID, StrComment FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS !== false)
		{
			while(!$aRS->EOF)
			{
				$strComment = trim($aRS->fields["strcomment"]);
				$questionID = (int) $aRS->fields["questionid"];
				$index = $this->QuestionIDsFlip[$questionID];
				
				if(!is_null($strComment) && $strComment!="")
				{
					$this->QComments[$index] = $strComment;
				}

				$aRS->MoveNext();
			}
		}
	}
	
	function getAllQuestionImages()
	{
		$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
		$sql = "SELECT QuestionID, PathImage FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS !== false)
		{
			while(!$aRS->EOF)
			{
				$strImage = $aRS->fields["pathimage"];
				$questionID = (int) $aRS->fields["questionid"];
				$index = $this->QuestionIDsFlip[$questionID];
				
				if(!is_null($strImage) && $strImage!="")
				{
					$this->QImages[$index] = "surveyimages/".$theRepositoryName."/".$strImage;
				}

				$aRS->MoveNext();
			}
		}
	}
	
	function getSignature()
	{
		$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
		$sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKey;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if($aRS !== false && !$aRS->EOF)
		{
			$strSignature = $aRS->fields["pathimage"];
			
			if($strSignature!="")
			{
				$this->Signature = "surveysignature/".$theRepositoryName."/".$strSignature;
			}
		}
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("SurveyID", $aHTTPRequest->GET))
		{
			$aSurveyID = (int) $aHTTPRequest->GET["SurveyID"];
		}
		else
		{
			$aSurveyID = 0;
		}

		if (array_key_exists("FactKey", $aHTTPRequest->POST))
		{
			$aFactKey = $aHTTPRequest->POST["FactKey"];

			if (is_array($aFactKey))
			{

				$aCollection = BITAMExportSurveyCollection::NewInstance($aRepository, $aSurveyID, $aFactKey);

				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMExportSurvey::NewInstanceWithID($aRepository, (int)$aSurveyID, $aFactKey);
				if (is_null($anInstance))
				{
					$anInstance = BITAMExportSurvey::NewInstance($aRepository, $aSurveyID);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMExportSurvey::NewInstance($aRepository, $aSurveyID);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("FactKey", $aHTTPRequest->GET))
		{
			$aFactKey = $aHTTPRequest->GET["FactKey"];

			$anInstance = BITAMExportSurvey::NewInstanceWithID($aRepository, $aSurveyID, (int)$aFactKey);

			if (is_null($anInstance))
			{
				$anInstance = BITAMExportSurvey::NewInstance($aRepository, $aSurveyID);
			}
		}
		else
		{
			$anInstance = BITAMExportSurvey::NewInstance($aRepository, $aSurveyID);
		}

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
	}

	function save()
	{
	}

	function remove()
	{
	}

	function isNewObject()
	{
		return ($this->FactKey < 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Export Survey");
		}
		else
		{
			return "Export Survey";
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=ExportSurvey&SurveyID=".$this->SurveyID;
		}
		else
		{
			return "BITAM_PAGE=ExportSurvey&SurveyID=".$this->SurveyID."&FactKey=".$this->FactKey;
		}
	}

	function get_Parent()
	{
		return BITAMExportSurveyCollection::NewInstance($this->Repository, $this->SurveyID, null);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'FactKey';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");

		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserName";
		$aField->Title = translate("User Name");
		$aField->ToolTip = translate("User Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DateID";
		$aField->Title = translate("Date");
		$aField->ToolTip = translate("Date");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		for ($i=0; $i<$this->NumQuestionFields; $i++)
		{
			$fieldName = $this->QuestionFields[$i];
			$fieldType = $this->FieldTypes[$i];
			$toolTip = $this->QuestionText[$i];

			//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
			if ($this->Questions[$i]->getAttributeName() != "") {
				$questionText = $this->Questions[$i]->getAttributeName();
			}
			else {
				$questionText = $this->QuestionText[$i];
			}

			$aField = BITAMFormField::NewFormField();
			$aField->Name = $fieldName;
			$aField->Title = $questionText;
			$aField->ToolTip = $toolTip;

			if($fieldType==1)
			{
				$aField->Type = "Float";
				$aField->FormatMask = "#,##0";
			}
			else
			{
				$aField->Type = "String";
				$aField->Size = 255;
			}

			$myFields[$aField->Name] = $aField;
		}

		return $myFields;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return false;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return false;
	}
	
	function Header()
	{
		$longitudLine = 190;
		$heightFont = 6;
		$countY = 5;
		$heightSpace = 5;

		$this->doc->SetFont('arial', 'B', 15);
		$title = "KPIOnline eSurvey";
		$widthText = $this->doc->GetStringWidth($title);
		$numLines = ceil($widthText / $longitudLine);
		$heightText = $numLines * $heightFont;
		$this->doc->SetXY(10,$countY);
		$this->doc->Cell($longitudLine, 6, $title, 0, 0, 'C');
		$countY = $this->validateOrdenadaY($countY, $heightText + $heightSpace);

		//Imprimir datos de la encuesta
		$heightFont = 5;
		$this->doc->SetFont('arial', 'B', 12);
		$surveyLine = "Survey: ".$this->SurveyName;
		$widthText = $this->doc->GetStringWidth($surveyLine);
		$numLines = ceil($widthText / $longitudLine);
		$heightText = $numLines * $heightFont;
		$this->doc->SetXY(10,$countY);
		$this->doc->Cell($longitudLine, $heightFont, $surveyLine, 0, 0, 'J');
		$countY = $this->validateOrdenadaY($countY, $heightText);

		$userLine = "User: ".$this->UserName;
		$widthText = $this->doc->GetStringWidth($userLine);
		$numLines = ceil($widthText / $longitudLine);
		$heightText = $numLines * $heightFont;
		$this->doc->SetXY(10,$countY);
		$this->doc->Cell($longitudLine, $heightFont, $userLine, 0, 0, 'J');
		$countY = $this->validateOrdenadaY($countY, $heightText);

		$dateLine = "Date of application: ".date('F j, Y H:i:s', strtotime($this->DateID));
		$widthText = $this->doc->GetStringWidth($dateLine);
		$numLines = ceil($widthText / $longitudLine);
		$heightText = $numLines * $heightFont;
		$this->doc->SetXY(10,$countY);
		$this->doc->Cell($longitudLine, $heightFont, $dateLine, 0, 0, 'J');
		$countY = $this->validateOrdenadaY($countY, $heightText);
		
		return $countY;
	}
	
	function Body($lastY=25)
	{
		$heightSpace = 10;
		$longitudLine = 190;
		$countY = $lastY + $heightSpace;
		$heightFont = 5;
		$heightSpace = 5;
		
		//Imprimir el cuerpo de la encuesta
		for ($i=0; $i<$this->NumQuestionFields; $i++)
		{
			$fieldName = $this->QuestionFields[$i];
			$questionText = $this->QuestionText[$i];
			
			//22-Ago-2018: Si es pregunta de selección sencilla (combo) y utiliza catálogo
			//la respuesta se leerá del campo que incluye una cadena con la jerarquía del catálogo
			//correspondiente a la respuesta (este campo se llena en report.inc.php
			if($this->Questions[$i]->QTypeID==qtpSingle && $this->Questions[$i]->UseCatalog==true)
			{
				$fieldName=$fieldName."_Hierarchy";
			}
			
			$answerText = $this->$fieldName;
			
			//Imprimir la pregunta
			$questionLine = $questionText;
			$widthText = $this->doc->GetStringWidth($questionLine);
			$numLines = ceil($widthText / $longitudLine);
			$heightText = $numLines * $heightFont;
			$this->doc->SetFont('arial', 'B', 12);
			$this->doc->SetXY(10,$countY);
			$this->doc->MultiCell($longitudLine, $heightFont, $questionLine, 0, 'J');
			$countY = $this->validateOrdenadaY($countY, $heightText);
			
            if($this->FieldTypes[$i] != 8 && $this->FieldTypes[$i] != 10) {
                //Imprimir la respuesta
                $answerText = str_replace(chr(13).chr(10), " ", $answerText);
                $answerLine = $answerText;
                $widthText = $this->doc->GetStringWidth($answerLine);
                $countEnter = substr_count($answerLine, chr(10));

                $numLines = ceil($widthText / $longitudLine) + $countEnter;
                $heightText = $numLines * $heightFont;
                $this->doc->SetFont('arial', 'I', 12);
                $this->doc->SetXY(10,$countY);
                $this->doc->MultiCell($longitudLine, $heightFont, $answerLine, 0, 'J');
            }
			
			//Si la pregunta tiene comentarios entonces procedemos a insertarlo tambien
			//omitimos comentarios de preguntas de secciones dinamicas
			if($this->QComments[$i]!="" && $this->Questions[$i]->SectionID!=$this->DynamicSectionID)
			{
				$countY = $this->validateOrdenadaY($countY, $heightText);
				$commentLine = "Comments: ".$this->QComments[$i];
				$commentLine = str_replace(chr(13).chr(10), " ", $commentLine);
				$widthText = $this->doc->GetStringWidth($commentLine);
				$countEnter = substr_count($commentLine, chr(10));
				
				$numLines = ceil($widthText / $longitudLine) + $countEnter;
				$heightText = $numLines * $heightFont;
				$this->doc->SetFont('arial', '', 12);
				$this->doc->SetXY(10,$countY);
				$this->doc->MultiCell($longitudLine, $heightFont, $commentLine, 0, 'J');
			}
			
			$width = 0;
			$height = 0;
			//Si la pregunta tiene imagen entonces procedemos a insertarla tambien
			//omitimos imagenes de preguntas de secciones dinamicas
			if($this->QImages[$i]!="" && $this->Questions[$i]->SectionID!=$this->DynamicSectionID)
			{
				$imagePath = $this->QImages[$i];
				$validPhoto = false;
				
				if (($img_info = @getimagesize($imagePath)) === false)
				{
					$imagePath = rtrim($imagePath);
					if (($img_info = @getimagesize($imagePath)) === false)
					{
						$imagePath = str_replace(' ', '%20', $imagePath);
				    	if (($img_info = @getimagesize($imagePath)) === true)
				    	{
				    		$validPhoto = true;
						}
					}
					else 
					{
						$validPhoto = true;
					}
				}
				else 
				{
					$validPhoto = true;
				}
                
				//Validar si es una imagen valida, si no lo es entonces no se debera 
				//indicar que existe foto
				if($validPhoto)
				{
					$countY = $this->validateOrdenadaY($countY, $heightText);
					
                    if($this->FieldTypes[$i] == 10) {
                        $imageLine = "Signature: ";
                    } else {
                        $imageLine = "Photo: ";
                    }
					$widthText = $this->doc->GetStringWidth($imageLine);
					$numLines = ceil($widthText / $longitudLine);
					$heightText = $numLines * $heightFont;
					$this->doc->SetXY(10,$countY);
					$this->doc->SetFont('arial', '', 12);
					$this->doc->MultiCell($longitudLine, $heightFont, $imageLine, 0, 'J');

					$tipo = 'jpg';
					//@JAPR 2015-02-06: Agregado soporte para php 5.6
			        $W_H_img = explode(' ',$img_info[3]);
			        $w = str_replace("width=",'',$W_H_img[0]);
			        $w = $this->ajustarUnidad((float)str_replace('"','',$w));
			        $w_img = $w;
			        $h = str_replace("height=",'',$W_H_img[1]);
			        $h = $this->ajustarUnidad((float)str_replace('"','',$h));
			        $h_img = $h;
			        
			        if($w_img>$longitudLine || $h_img>100)
			        {
			        	$arrayMeasures = $this->adjustPhotoMeasures($w_img, $h_img);
			        	$width = $arrayMeasures["width"];
			        	$height = $arrayMeasures["height"];
			        }
			        else 
			        {
			        	$width = $w_img;
			        	$height = $h_img;
			        }
	
	   				$countY = $this->validateOrdenadaY($countY, $heightText);
					
					//Ahora validaremos si la imagen puede caber en la hoja sino
					//para que se mueva a la siguiente hoja
					if( ($countY + $height)>285) 
					{
						$this->doc->AddPage();
						$countY = 5;
					}
					$this->doc->SetXY(10,$countY);
					//@JAPR 2012-08-10: Corregido un bug, a partir de cierta versión v3 los archivos de firmas realmente comenzaron a ser archivos
					//.jpg, antes de eso la exportación a PDF hubiera fallado porque eran archivos .png (aunque disfrazados como .jpg)
					
					//Identifica el tipo de archivo antes de intentar usarlo, ya que si no es correcto generará un Fatal Error
					$strJPGHead = chr(255).chr(216);
					$readFile = fopen($imagePath, "rb");
					$strFileHead = '';
					if (!feof($readFile)) {
						$strFileHead = fread($readFile, 2);
					}
					fclose($readFile);
					
					//Si no corresponde con una cabecera de archivo JPG, asumirá que era de una versión previa y usará PNG
					$strTempFileName = '';
					if ($strFileHead != $strJPGHead) {
						//@JAPR 2012-08-10: Por alguna razón el método imagecreatefrompng estaba generando una excepción de memoria al intentar
						//procesar la imagen, así que simplemente se ignorarán estas firmas cuando son archivos .png
						$tipo = '';
						/*
						$tipo = 'png';
						$strTempFileName = tempnam(".\log", "JPG");
						if ($strTempFileName !== false) {
    						$image = @imagecreatefrompng($imagePath);
    						if ($image) {
	    						imagejpeg($image, $strTempFileName, 100);
	    						imagedestroy($image);
    						}
						}
						*/
					}
					
					if ($tipo != '') {
						$this->doc->Image($imagePath, 10, $countY, $width, $height, $tipo);
					}
					if ($strTempFileName != '') {
						@unlink($strTempFileName);
					}
					//@JAPR
				}
			}
			
			if($height==0)
			{
				$height = $heightText;
			}

			$countY = $this->validateOrdenadaY($countY, $height + $heightSpace);
		}
        
        if($this->Survey->HasSignature) {
            $questionLine = "Survey signature";
			$widthText = $this->doc->GetStringWidth($questionLine);
			$numLines = ceil($widthText / $longitudLine);
			$heightText = $numLines * $heightFont;
			$this->doc->SetFont('arial', 'B', 12);
			$this->doc->SetXY(10,$countY);
			$this->doc->MultiCell($longitudLine, $heightFont, $questionLine, 0, 'J');
			$countY = $this->validateOrdenadaY($countY, $heightText);
            
            if($this->Signature != "") {
                $imagePath = $this->Signature;
				$validPhoto = false;
				
				if (($img_info = @getimagesize($imagePath)) === false)
				{
					$imagePath = rtrim($imagePath);
					if (($img_info = @getimagesize($imagePath)) === false)
					{
						$imagePath = str_replace(' ', '%20', $imagePath);
				    	if (($img_info = @getimagesize($imagePath)) === true)
				    	{
				    		$validPhoto = true;
						}
					}
					else 
					{
						$validPhoto = true;
					}
				}
				else 
				{
					$validPhoto = true;
				}
                
				//Validar si es una imagen valida, si no lo es entonces no se debera 
				//indicar que existe foto
				if($validPhoto)
				{
                    
					$tipo = 'jpg';
					//@JAPR 2015-02-06: Agregado soporte para php 5.6
			        $W_H_img = explode(' ',$img_info[3]);
			        $w = str_replace("width=",'',$W_H_img[0]);
			        $w = $this->ajustarUnidad((float)str_replace('"','',$w));
			        $w_img = $w;
			        $h = str_replace("height=",'',$W_H_img[1]);
			        $h = $this->ajustarUnidad((float)str_replace('"','',$h));
			        $h_img = $h;
			        
			        if($w_img>$longitudLine || $h_img>100)
			        {
			        	$arrayMeasures = $this->adjustPhotoMeasures($w_img, $h_img);
			        	$width = $arrayMeasures["width"];
			        	$height = $arrayMeasures["height"];
			        }
			        else 
			        {
			        	$width = $w_img;
			        	$height = $h_img;
			        }
	
	   				$countY = $this->validateOrdenadaY($countY, $heightText);
					
					//Ahora validaremos si la imagen puede caber en la hoja sino
					//para que se mueva a la siguiente hoja
					if( ($countY + $height)>285) 
					{
						$this->doc->AddPage();
						$countY = 5;
					}
					$this->doc->SetXY(10,$countY);
					//@JAPR 2012-08-10: Corregido un bug, a partir de cierta versión v3 los archivos de firmas realmente comenzaron a ser archivos
					//.jpg, antes de eso la exportación a PDF hubiera fallado porque eran archivos .png (aunque disfrazados como .jpg)
					
					//Identifica el tipo de archivo antes de intentar usarlo, ya que si no es correcto generará un Fatal Error
					$strJPGHead = chr(255).chr(216);
					$readFile = fopen($imagePath, "rb");
					$strFileHead = '';
					if (!feof($readFile)) {
						$strFileHead = fread($readFile, 2);
					}
					fclose($readFile);
					
					//Si no corresponde con una cabecera de archivo JPG, asumirá que era de una versión previa y usará PNG
					$strTempFileName = '';
					if ($strFileHead != $strJPGHead) {
						//@JAPR 2012-08-10: Por alguna razón el método imagecreatefrompng estaba generando una excepción de memoria al intentar
						//procesar la imagen, así que simplemente se ignorarán estas firmas cuando son archivos .png
						$tipo = '';
						/*
						$tipo = 'png';
						$strTempFileName = tempnam(".\log", "JPG");
						if ($strTempFileName !== false) {
    						$image = @imagecreatefrompng($imagePath);
    						if ($image) {
	    						imagejpeg($image, $strTempFileName, 100);
	    						imagedestroy($image);
    						}
						}
						*/
					}
					
					if ($tipo != '') {
						$this->doc->Image($imagePath, 10, $countY, $width, $height, $tipo);
					}
					if ($strTempFileName != '') {
						@unlink($strTempFileName);
					}
					//@JAPR
				}
            }
        }
	}
	
	function validateOrdenadaY($currentPosition, $height)
	{
		if( ($currentPosition + $height)>285) 
		{
			$this->doc->AddPage();
			$currentPosition = 5;
		}
		else 
		{
			$currentPosition = $currentPosition + $height;
		}
		
		return $currentPosition;
	}
	
	function adjustPhotoMeasures($width, $height)
	{
		$longitudLine = 190;
		$arrayMeasures = array();
		$arrayMeasures["width"] = $width;
		$arrayMeasures["height"] = $height;
		
		if($width>$longitudLine)
		{
			$porcentaje = ($longitudLine*100)/$width;
			$newWidth = $width*($porcentaje/100);
			$newHeight = $height*($porcentaje/100);
			
			if($newWidth>$longitudLine || $newHeight>100)
			{
				$arrayMeasures = $this->adjustPhotoMeasures($newWidth, $newHeight);
				return $arrayMeasures;
			}
			else 
			{
				$arrayMeasures["width"] = $newWidth;
				$arrayMeasures["height"] = $newHeight;
				return $arrayMeasures;
			}
		}
		
		if($height>100)
		{
			$porcentaje = (100*100)/$height;
			$newHeight = $height*($porcentaje/100);
			$newWidth = $width*($porcentaje/100);
			
			if($newWidth>$longitudLine || $newHeight>100)
			{
				$arrayMeasures = $this->adjustPhotoMeasures($newWidth, $newHeight);
				return $arrayMeasures;
			}
			else 
			{
				$arrayMeasures["width"] = $newWidth;
				$arrayMeasures["height"] = $newHeight;
				return $arrayMeasures;
			}
		}
		
		return $arrayMeasures;
	}
	
	function exportSurveyToPDF($sendToFile=true)
	{
		require_once ('pdf/fpdf.php');
		$this->orientation = "P";
		$this->unidad = "mm";
		$this->format = "A4";
		$this->displayMode = "fullpage";
		$title = "KPIOnline eSurvey";
		
		$this->doc = new FPDF($this->orientation, $this->unidad, $this->format);
		$this->doc->SetDisplayMode($this->displayMode);
		$this->doc->AddPage();
		$this->doc->SetTitle($title);
		$this->doc->SetFillColor(255,255,255);
		$this->AddFont("arial", "");
		$this->AddFont("arial", "B");
		$this->AddFont("arial", "I");
		
		$countY = $this->Header();
		$this->Body($countY);
		
		//Generamos el nombre del pdf
	    $this->fileName = 'Survey_'.$this->SurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.pdf';
	    //@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	    $this->filePath = GeteFormsLogPath().$this->fileName;
	    //@JAPR
	    if($sendToFile==true)
	    {
			$this->doc->Output($this->filePath);
	    }
	    else 
	    {
	    	$this->doc->Output();
	    }
	}
	
	function sendNotification()
	{
		if(!is_null($this->fileName) && trim($this->fileName)!="")
		{
			NotifyNewSurveyCapturedByMail($this);
		}
	}
	
    function AddFont($family, $style='')
    {
    	$family = strtolower($family);
    	$style = strtolower($style);

    	if (!isset($this->doc->aSupportedFonts[$family]))
    	{
    		$family = 'calibri';
    	}

    	if (strpos($style, 'u') !== false)
    	{
    		$style = str_replace('u', '', $style);
    	}

    	if($style=='ib') $style='bi';
    	$keyFont = $family.$style;

  		if (!isset($this->aListFontsAdded[$keyFont]))
		{
			$this->doc->AddFont($family,$style,$keyFont.'.php');
			$this->aListFontsAdded[$keyFont] = $keyFont;
		}
    }
    
	function ajustarUnidad($valor)
    {
        switch ($this->unidad) {
        	case 'pt':
				$valor = $valor/1;
				break;
        	case 'mm':
        		$valor = $valor/(72/25.4);
				break;
        	case 'cm':
				$valor = $valor/(72/2.54);
				break;
        	case 'in':
				$valor = $valor/72;
				break;
        }
        return $valor;
    }
}

function NotifyNewSurveyCapturedByMail($objExpSurvey)
{
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	global $arrAdditionalEMails;
	//@JAPR 2014-04-09: Agregada la variable para identificar el envío de reportes desde la captura o desde la opción para forzarlo en la lista de reportes
	global $blnSendPDFEntries;
	//@JAPR
	
	require_once("appuser.inc.php");
	$aRepository = $objExpSurvey->Repository;

	$objResource = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $objExpSurvey->UserID);
	
	$strCreatorStatusEmail = $objResource->Email;
	$strCreatorStatusUserName = $objResource->Email;
	
	require_once("bpaemailconfiguration.inc.php");
	
	$anInstanceEmailConf = BPAEmailConfiguration::readConfiguration();
	
	if(is_null($anInstanceEmailConf))
	{
		return false;
	}
  
	$SMTPServer = $anInstanceEmailConf->fbm_email_server;
	
	if (strlen($SMTPServer) == 0) 
	{
		return false;
	}

	$SMTPPort = $anInstanceEmailConf->fbm_email_port;
	
	if (strlen($SMTPPort) == 0) 
	{
		return false;
	}
	
	$SMTPUserName = $anInstanceEmailConf->fbm_email_username;
	
	if (strlen($SMTPUserName) == 0)
	{
		return false;
	}

	$SMTPPassword = $anInstanceEmailConf->fbm_email_pwd;
	
	//@JAPR 2012-10-10: Validada la integración con eBavel, como eBavel mandará eMails, incluye primero la clase PhPMailer así que se debe validar
	//que sólo incluya eForms su versión si no lo hizo ya eBavel
	if(!class_exists("PHPMailer")){
		require_once("mail/class.phpmailer.php");
	}	
	//@JAPR
	
	$mail = new PHPMailer();
	$mail->Mailer = "smtp";
	$mail->Host = $SMTPServer;
	$mail->Port = $SMTPPort;
	$mail->SMTPAuth = TRUE;
	//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
	//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
	$mail->SMTPAutoTLS = false;
	//$mail->SMTPDebug = false;
	//$mail->Debugoutput = 'echo';
	//$mail->SMTPSecure = '';
	//$mail->CharSet = 'UTF-8';
	//@JAPR
	$mail->Username = $SMTPUserName;
	$mail->Password = $SMTPPassword;

	//Se modifico el envio de correos para que el usuario que se loguea al servidor de correos
	//sea el mismo que vaya en el FROM, pero en el momento en que se contesta el correo entonces
	//el Reply To va dirigido a la persona q realmente si creo el correo
	//$mail->From = $strCreatorStatusEmail;
	//$mail->FromName = $strCreatorStatusUserName;
	$mail->From = $anInstanceEmailConf->fbm_email_source;
	$mail->FromName = $strCreatorStatusUserName;
	$mail->AddReplyTo($strCreatorStatusEmail, $strCreatorStatusUserName);

	$mail->IsHTML(TRUE);
	
	$maillanguage = $_SESSION["PAuserLanguage"];
	$mail->SetLanguage(strtolower($maillanguage),"mail/language/"); //set the language
	
	$mail->Subject = translate("eSurvey - Survey Captured: ").$objExpSurvey->SurveyName;
	$existRecipients = false;

	$tempEmail = $strCreatorStatusEmail;
	//$tempEmail = "iris.suarez@gmail.com";
	$tempUserName = $strCreatorStatusUserName;
	
	$blnCheckMail = BPAEmailConfiguration::check_email_address($tempEmail);
	if ($blnCheckMail)
	{
		$mail->AddAddress($tempEmail, $tempUserName);
		$existRecipients = true;
	}
	
	//@JAPR 2012-10-09: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
	$tempEmailCC = @$objResource->AltEmail;
	if (trim($tempEmailCC) != '') {
		$blnCheckMail = BPAEmailConfiguration::check_email_address($tempEmailCC);
		if ($blnCheckMail)
		{
			$mail->AddCC($tempEmailCC, $tempUserName);
			$existRecipients = true;
		}
	}
	
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	//Agrega las cuentas de correo adicionales configuradas para la encuesta, en este punto se asume que ya están validadas por lo menos para que
	//se vean como un EMail, si están bien armadas o no son reales, ya se sale del alcance y se dejará al server de correo que lo resuelva
	if (isset($arrAdditionalEMails) && is_array($arrAdditionalEMails) && count($arrAdditionalEMails) > 0) {
		foreach ($arrAdditionalEMails as $strEMail) {
			@$mail->AddCC($strEMail, "");
			$existRecipients = true;
		}
	}
	//@JAPR
	
	//Enviaremos copias ocultas a los correos que se encuentran en la variable global
	//$KPAMailTesting definida en el config con la finalidad de monitorear el envio de correos
	global $KPAMailTesting;
	
	foreach ($KPAMailTesting as $emailTest)
	{
		$blnCheckMail = BPAEmailConfiguration::check_email_address($emailTest);
		if ($blnCheckMail)
		{
			$mail->AddBCC($emailTest, "");
			$existRecipients = true;
		}
	}
	
	$mail->Body = CreateNewSurveyCapturedMailBody($objExpSurvey);
	
	if (!is_null($objExpSurvey->fileName) && trim($objExpSurvey->fileName)!="")
	{
		$mail->AddAttachment($objExpSurvey->filePath, $objExpSurvey->fileName);
	}
	
	if($existRecipients)
	{
		AddCuentaCorreoAltToMail(@$objResource, $mail);
		if (!@$mail->Send())
		{
			//Enviamos al log el error generado
			global $SendMailLogFile;
			
			$aLogString = "\r\n\r\neSurvey - Survey Captured: ".$objExpSurvey->SurveyName.": ".date('Y-m-d H:i:s');
			$aLogString.="\r\n";
			$aLogString.="\r\n\r\n".$mail->ErrorInfo;
			$aLogString.="\r\n";

			//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log($aLogString, 3, GeteFormsLogPath().$SendMailLogFile);
			//@JAPR
		}
	}
	
	unset($mail);
	return true;
}

function CreateNewSurveyCapturedMailBody($objExpSurvey)
{
	$aRepository = $objExpSurvey->Repository;
	
	$strMailTitle = translate("Survey Captured Notification - ".$objExpSurvey->SurveyName);
	
	$strProjectName = translate("Survey").": ".$objExpSurvey->SurveyName;
	
	$strProjectManager=translate("Survey Captured by")." ".$objExpSurvey->UserName;
	
	$strProjectManager.=" ".translate("on")." ".date('F j, Y H:i:s', strtotime($objExpSurvey->DateID));
	
	$strAdditionalNotes = "Review attachment....";
	
	$strtemp = "<html>"."\n"
	."<style type=text/css>"."\n"
	."  A:active { color: #00008b }"."\n"
	."  A:visited { color: #708090 }"."\n"
	."  A:link { color: #00008b }"."\n"
	."  A:hover { color: #4169e1 }"."\n"
	."  A { font-family: verdana; font-size: 8pt }"."\n"
	."</style>"."\n"
	."<body>"."\n"
	."<div style=\"font-family:verdana;color:#336699;font-size:13pt\"><b>".$strMailTitle."</b></div>"."\n"
	."<table border=0 cellPadding=5 cellSpacing=1 style=\"font-family:verdana;font-weight:normal;font-size:10pt\">"."\n"
	."  <tr>"."\n"
	."    <td colspan=2><hr noshade width=100% size=1></td>"."\n"
	."  </tr>"."\n"
	."  <tr>"."\n"
	."    <td>&nbsp;</td>"."\n"
	."  </tr>"."\n"	
	."  <tr>"."\n"
	."    <td>".$strProjectName."</td>"."\n"
	."  </tr>"."\n"
	."  <tr>"."\n"
	."    <td>".$strProjectManager."</td>"."\n"
	."  </tr>"."\n"
	."  <tr>"."\n"
	."    <td>".$strAdditionalNotes."</td>"."\n"
	."  </tr>"."\n"
	
	."</table>"."\n"
	."<br>"."\n";

	$strtemp .= "</body>"."\n"
	."</html>"."\n";

	return $strtemp;
}