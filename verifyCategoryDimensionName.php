<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Obtener el SurveyID
$SurveyID = -1;
if (array_key_exists("SurveyID", $_POST))
{
	$SurveyID = (int)$_POST["SurveyID"];
}

//Obtener el CategoryDimName
$CategoryDimName = "";
if (array_key_exists("CategoryDimName", $_POST))
{
	$CategoryDimName = $_POST["CategoryDimName"];
}

//Obtener el ElementDimName
$ElementDimName = "";
if (array_key_exists("ElementDimName", $_POST))
{
	$ElementDimName = $_POST["ElementDimName"];
}

//Obtener el ElementIndName
$ElementIndName = "";
if (array_key_exists("ElementIndName", $_POST))
{
	$ElementIndName = $_POST["ElementIndName"];
}

require_once("survey.inc.php");

$surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $SurveyID);

$exist = false;
$existElemDim = false;
$existElemInd = false;

$response = "ERROR";

$sql = "SELECT A.CLA_DESCRIP FROM SI_DESCRIP_ENC A, SI_CPTO_LLAVE B 
		WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.NOM_LOGICO = ".$theRepository->ADOConnection->Quote($CategoryDimName);

$aRS = $theRepository->ADOConnection->Execute($sql);

if ($aRS === false)
{
	die(translate("Error accessing")." SI_DESCRIP_ENC, SI_CPTO_LLAVE ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}

if(!$aRS->EOF)
{
	$exist = true;
}

$sql = "SELECT A.CLA_DESCRIP FROM SI_DESCRIP_ENC A, SI_CPTO_LLAVE B 
		WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.NOM_LOGICO = ".$theRepository->ADOConnection->Quote($ElementDimName);

$aRS = $theRepository->ADOConnection->Execute($sql);

if ($aRS === false)
{
	die(translate("Error accessing")." SI_DESCRIP_ENC, SI_CPTO_LLAVE ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}

if(!$aRS->EOF)
{
	$existElemDim = true;
}

$sql = "SELECT A.CLA_INDICADOR FROM SI_INDICADOR A
		WHERE A.CLA_CONCEPTO = ".$surveyInstance->ModelID." 
		AND A.NOM_INDICADOR = ".$theRepository->ADOConnection->Quote($ElementIndName);

$aRS = $theRepository->ADOConnection->Execute($sql);

if ($aRS === false)
{
	die(translate("Error accessing")." SI_INDICADOR ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}

if(!$aRS->EOF)
{
	$existElemInd = true;
}

if(!$exist && !$existElemDim && !$existElemInd)
{
	$response = "OK";
}
else 
{
	if($exist)
	{
		$response = "ERROR<SVSEP>Can not add this question because there is another dimension with the same name as the category dimension name";
	}
	else if($existElemDim)
	{
		$response = "ERROR<SVSEP>Can not add this question because there is another dimension with the same name as the element dimension name";
	}
	else
	{
		$response = "ERROR<SVSEP>Can not add this question because there is another indicator in the survey with the same name as the element indicator name";
	}
}

header('Content-Type: text/plain; charset=utf-8');
echo $response;
?>