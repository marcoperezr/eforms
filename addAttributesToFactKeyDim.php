<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

$strOriginalWD = getcwd();

//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("BITAM_UserID");
//session_register("BITAM_UserName");
//session_register("BITAM_RepositoryName");
$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

require_once("../model_manager/filescreator.inc.php");
require_once("../model_manager/attribdimension.inc.php");
require_once("../model_manager/modeldimension.inc.php");
require_once("../model_manager/modeldata.inc.php");

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	//Crear atributo Latitud en la dimension FactKey
	if(is_null($surveyInstance->LatitudeAttribID) || $surveyInstance->LatitudeAttribID<=0)
	{
		//Despues de crear la dimension FactKey se debe crear el atributo Latitud
		$AttribName = "Latitude";
		$cla_descrip = -1;
		$cla_descrip_parent = $surveyInstance->FactKeyDimID;
		$cla_concepto = $surveyInstance->ModelID;
		
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Dimensión atributo, no debe ser reutilizada de otro cubo
		$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($aRepository, $cla_concepto, $cla_descrip_parent);
		
		$anInstanceAttrib->Dimension->DimensionName = $AttribName;
		$anInstanceAttrib->Dimension->ParentDimensionID = $cla_descrip_parent;
		$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
		$anInstanceAttrib->Dimension->DimensionID = $cla_descrip;
		$anInstanceAttrib->save();

		chdir($strOriginalWD);
		
		$aLatitudeAttribID = $anInstanceAttrib->Dimension->DimensionClaDescrip;
		$surveyInstance->LatitudeAttribID = $aLatitudeAttribID;
		
		$sql = "UPDATE SI_SV_Survey SET LatitudeAttribID = ".$aLatitudeAttribID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	//Crear atributo Longitud en la dimension FactKey
	if(is_null($surveyInstance->LongitudeAttribID) || $surveyInstance->LongitudeAttribID<=0)
	{
		//Despues de crear el atributo Latitud se debe crear el atributo Longitud
		$AttribName = "Longitude";
		$cla_descrip = -1;
		$cla_descrip_parent = $surveyInstance->FactKeyDimID;
		$cla_concepto = $surveyInstance->ModelID;
		
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Dimensión atributo, no debe ser reutilizada de otro cubo
		$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($aRepository, $cla_concepto, $cla_descrip_parent);
		
		$anInstanceAttrib->Dimension->DimensionName = $AttribName;
		$anInstanceAttrib->Dimension->ParentDimensionID = $cla_descrip_parent;
		$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
		$anInstanceAttrib->Dimension->DimensionID = $cla_descrip;
		$anInstanceAttrib->save();

		chdir($strOriginalWD);
		
		$aLongitudeAttribID = $anInstanceAttrib->Dimension->DimensionClaDescrip;
		$surveyInstance->LongitudeAttribID = $aLongitudeAttribID;
		
		$sql = "UPDATE SI_SV_Survey SET LongitudeAttribID = ".$aLongitudeAttribID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}

	//Actualizamos el valor de No Aplica ya que en esta dimension contienen atributos
	//de Longitud y Latitud y los establecemos a NA
	global $gblEFormsNA;
	$factTable = "RIFACT_".$surveyInstance->ModelID;
	$tableName = "RIDIM_".$surveyInstance->FactKeyDimID;
	$fieldSubrogateKey = $tableName."KEY";
	$fieldAttrLatitude = "DSC_".$surveyInstance->LatitudeAttribID;
	$fieldAttrLongitude = "DSC_".$surveyInstance->LongitudeAttribID;

	$sql = "UPDATE ".$tableName." SET ".$fieldAttrLatitude." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA).", 
			".$fieldAttrLongitude." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldSubrogateKey." = -1";

	if ($aRepository->DataADOConnection->Execute($sql) === false)
	{
		die(translate("Error accessing")." $tableName ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	//Procedemos a actualizar la tabla de dimension Encuesta ID para que tenga los valores 
	//de la longitud y latitud que se hayan capturado
	$sql = "UPDATE ".$tableName." A, ".$factTable." B 
			SET A.".$fieldAttrLatitude." = CAST( B.IND_".$surveyInstance->LatitudeIndID." AS CHAR ), 
			A.".$fieldAttrLongitude." = CAST( B.IND_".$surveyInstance->LongitudeIndID." AS CHAR ) 
			WHERE A.".$fieldSubrogateKey." = B.".$fieldSubrogateKey;

	if ($aRepository->DataADOConnection->Execute($sql) === false)
	{
		die(translate("Error accessing")." $tableName ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
}
?>