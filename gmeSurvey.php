<?php
$bDebug=true;

//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
$strLogPath = GeteFormsLogPath();
$strLogFilename = $strLogPath.'gmeSurvey.log';
if($bDebug) error_log("\n\n".'Start at '.date("D M j G:i:s T Y").' gmeSurvey.php', 3, $strLogFilename);
//@JAPR
//Obtener los Datos del Reporte
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("report.inc.php");

if($bDebug) error_log("\n".'Loading lenguage', 3, $strLogFilename);
//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else
{	//English
	InitializeLocale(2);
	LoadLanguageWithName("EN");
}

//Separadores
$sep      ='_AWSep_';
$sepMarker='_AWSepMkr_';

if($bDebug) error_log("\n".'Getting Params', 3, $strLogFilename);
//Parametros del Reporte
$wTitle     =(array_key_exists("WinTitle", $_GET) ? $_GET["WinTitle"]   : 'Bitam ' . translate('Survey') . ' - '. translate('Map'));
$aSurveyID  =(array_key_exists("surveyID", $_GET) ? $_GET["surveyID"]   : -1);
$aUserID  =(array_key_exists("userID", $_GET) ? $_GET["userID"]   : -2);

$startDate  =(array_key_exists("startDate",$_GET) ?	$_GET["startDate"]  : '');
$endDate    =(array_key_exists("endDate",$_GET)   ?	$_GET["endDate"]    : '');
$AttribIDs  =(array_key_exists("AttribIDs",$_GET) ?	$_GET["AttribIDs"]  : '');
$AttribVals =(array_key_exists("AttribVals",$_GET)? $_GET["AttribVals"] : '');

$aReportID = (int)(array_key_exists("reportID", $_GET) ? $_GET["reportID"] : -1);

if($bDebug) error_log("\n".'aSurveyID:'.$aSurveyID.'aUserID:'.$aUserID.', startDate:'.$startDate.', endDate:'.$endDate.', AttribIDs:'.$AttribIDs.', AttribVals:'.$AttribVals, 3, $strLogFilename);

//Armar el arreglo de atributtos
$AttribIDs =explode($sep, $AttribIDs);
$AttribVals=explode($sep, $AttribVals);
$attribSelected = array();
for($i=0; $i<count($AttribIDs); $i++)
{
	if($AttribIDs[$i]!='' && $AttribVals[$i]!='')
	{
		$attribSelected[$AttribIDs[$i]]=$AttribVals[$i];
	}
}
if($bDebug) error_log("\n".'attribSelected:'.print_r($attribSelected, true), 3, $strLogFilename);

//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
$anInstance = new BITAMReportCollection($theRepository, $aSurveyID);
$aSurvey=BITAMSurvey::NewInstanceWithID($theRepository, $aSurveyID);

$afile = array();
$line = '"User Name","Date"';
for($i=0; $i<$anInstance->NumQuestionFields; $i++)
{
	//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
	if ($anInstance->Questions[$i]->getAttributeName() != "") {
		$strText = $anInstance->Questions[$i]->getAttributeName();
	}
	else {
		$strText = $anInstance->Questions[$i]->QuestionText;
	}
	$line.=',"'.str_replace('"', '""', $strText).'"';
}
$sDataDesc=$line;
if($bDebug) error_log("\n".'sDataDesc:'.$sDataDesc, 3, $strLogFilename);

global $svStartDate;
global $svEndDate;
$startDate=($anInstance->StartDate!=''?$anInstance->StartDate:$startDate);
$endDate  =($anInstance->EndDate!=''  ?$anInstance->EndDate  :$endDate);

$sql = "SELECT ".$aSurvey->SurveyTable.".SurveyKey AS 'FolioID', ".$aSurvey->SurveyTable.".SurveyKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".Latitude latitud, ".$aSurvey->SurveyTable.".Longitude longitud, '".$aSurvey->SurveyTable.".FactKeyDimVal' ";

//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
$arrayTables = array();
$fromCat = "";
$joinCat = "";
$strAnd = "";
for($i=0; $i<$anInstance->NumQuestionFields; $i++)
{
	$objQuestion = $anInstance->Questions[$i];
	//@JAPR 2012-09-14: Corregido un bug, las preguntas Open numéricas aunque pueden usar catálogo, no graban un ID subrrogado así que fallaba el join
	if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric || true)
	{
		//@JAPR 2012-11-23: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
		switch ($anInstance->Questions[$i]->QTypeID) {
			case qtpPhoto:
			case qtpSignature:
			case qtpShowValue:
			case qtpMessage:
			case qtpSkipSection:
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			case qtpSync:
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			case qtpPassword:
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			case qtpSection:
			//@JAPR 2015-06-15: Corregido un bug, estos tipos de pregunta no contienen campo en la tabla paralela
			case qtpAudio:
			case qtpVideo:
			case qtpSketch:
			case qtpDocument:
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			case qtpExit:
			//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
			case qtpUpdateDest:
				break;
				
			default:
//				$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
				break;
		}
	}
	else
	{
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if ($aSurvey->DissociateCatDimens) {
			//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
			$catParentID = $objQuestion->CatIndDimID;
			$memberParentID = $catParentID;
			$catTable = "RIDIM_".$catParentID;
			$attribField = "DSC_".$memberParentID;
			$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
			$sql.=", ".$catTable.".".$attribField;
			$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
			
			//Si se vuelve a repetir el catalogo, con este arreglo
			//se evita que se vuelva a dar de alta en el FROM dicha tabla
			if(!isset($arrayTables[$catTable]))
			{
				$fromCat.=", ".$catTable;
				$joinField = "RIDIM_".$catParentID."KEY";
				$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
				$arrayTables[$catTable] = $catTable;
				$strAnd = " AND ";
			}
		}
		else {
			//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
			$catParentID = BITAMReportCollection::getCatParentID($theRepository, $anInstance->Questions[$i]->CatalogID);
			$catTable = "RIDIM_".$catParentID;
			$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($theRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
			$attribField = "DSC_".$memberParentID;
			
			$sql.=", ".$catTable.".".$attribField;
			
			//Si se vuelve a repetir el catalogo, con este arreglo
			//se evita que se vuelva a dar de alta en elFROM dicha tabla
			if(!isset($arrayTables[$catTable]))
			{
				$fromCat.=", ".$catTable;
			}
			
			$arrayTables[$catTable] = $catTable;
			
			if($joinCat!="")
			{
				$joinCat.=" AND ";
			}
			$joinField = "RIDIM_".$catParentID."KEY";
			$joinCat.=$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
			$strAnd = " AND ";
		}
		//@JAPR
	}
}
$where = $joinCat;
$filterJoin = "";
//Filtro de combos de atributos (si es que existen)
if($attribSelected!=null)
{
	//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	$anInstance->AttribSelected=$attribSelected;
	$isFiltered = false;
	foreach($anInstance->AttribSelected as $idx => $value)
	{
		if($value!="sv_ALL_sv")
		{
			$isFiltered = true;
			break;
		}
	}

	if($isFiltered && false)
	{
		if ($aSurvey->DissociateCatDimens) {
			//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
			foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
			{
				if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
				{
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
						$strAnd = " AND ";
					}
					
					$where .= $strAnd.$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
					$strAnd = " AND ";
				}
			}
		}
		else {
			$key = $anInstance->getCatalogKey();
			if($where!="")
			{
				$where.=" AND ";
			}
	
			if($key!==null)
			{
				$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
			}
			else
			{
				$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
			}
		}
		//@JAPR
	}
}

if($aUserID != -2) {
	if($where!="")
	{
		$where.=" AND ";
	}
	$where.=" ".$aSurvey->SurveyTable.".UserID = ".$aUserID;
}

if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
{
	if($where!="")
	{
		$where.=" AND ";
	}
	$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
}

if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
{
	if($where!="")
	{
		$where.=" AND ";
	}
	$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
}

//@JAPR 2015-06-15: Este filtro no es necesario bajo el nuevo esquema de obtención de datos de reportes a partir de la tabla si_sv_surveylog, además que realmente
//sobraba porque la combinación de usuario + fecha y agrupado por FactKeyDimVal aseguraban obtener un registro por captura
/*
if(!is_null($aReportID) && $aReportID>0)
{
	if($where!="")
	{
		$where.=" AND ";
	}
	$where.=" ".$aSurvey->SurveyTable.".FactKey = ".$aReportID;
}
*/

//Se agrego el JOIN a la tabla RIFACT_+ModelID para obtener longitud y latitud
$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat;
//Ligamos las Tablas para lo de Longitud/Latitud
if($where!="")
{
	$sql .= " WHERE ".$where;
}

$sql .= " GROUP BY ".$aSurvey->SurveyTable.".SurveyKey";
$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";
$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
if ($aRS === false)
{
	die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}

$longitud = array();
$latitud  = array();
while (!$aRS->EOF)
{
	$arrayDynSectionValues = array();
	$factKeyDimVal = (int)$aRS->fields["surveykey"];
	
	//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
	//valores de la seccion dinamica
	if($anInstance->HasDynamicSection==true)
	{
		//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
		$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
	}
	
	$userID    = $aRS->fields["userid"];
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
	$userName  = str_replace('"', '""', BITAMeFormsUser::GetUserName($anInstance->Repository, $userID));
	$dateValue = str_replace('"', '""', $aRS->fields["dateid"]);
	$latitud[] = $aRS->fields["latitud"];
	$longitud[]= $aRS->fields["longitud"];
	
	$line='"'.$userName.'","'.$dateValue.'"';
	
	for($i=0; $i<$anInstance->NumQuestionFields; $i++)
	{
		$objQuestion = $anInstance->Questions[$i];
		//El primer IF es para identificar si las preguntas son dinamicas y adicionalmente que la propiedad si lo indique
		//que tengan seccion dinamica
		if($anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true)
		{
			//@JAPR 2013-02-20: Corregido un bug, hay varios tipos de pregunta que no deben tener campo en la paralela
			$blnValid = true;
			switch ($anInstance->Questions[$i]->QTypeID) {
				case qtpShowValue:
				case qtpPhoto:
				case qtpDocument:
				case qtpAudio:
				case qtpVideo:
				case qtpSignature:
				case qtpMessage:
				case qtpSkipSection:
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				case qtpSync:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValid = false;
					break;
			}
			
			if (!$blnValid) {
				continue;
			}
			//@JAPR
			
			$fieldName =$anInstance->QuestionFields[$i];
			$strValue = "";
			//Obtenemos el valor de esta pregunta a partir del arrreglo $arrayDynSectionValues
			//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
			//porque todavia no se sabe exactamente como se va a comportar
			if($anInstance->Questions[$i]->QTypeID!=qtpMulti)
			{
				foreach ($arrayDynSectionValues as $element)
				{
					$idxName = $anInstance->QuestionFields[$i];
					
					if($strValue!="")
					{
						$strValue.="; ";
					}
					
					$strValue.=$element["desc"]."=".$element[$idxName];
				}
			}
			else 
			{
				//Si es entrada de tipo Checkbox, el valor seran
				//todo los valores seleccionados concatenados con ;
				if($anInstance->Questions[$i]->MCInputType==0)
				{
					foreach ($arrayDynSectionValues as $element)
					{
						$idxName = $anInstance->QuestionFields[$i];
						$valueInteger = (int)$element[$idxName];
						
						if($valueInteger!=0)
						{
							if($strValue!="")
							{
								$strValue.=";";
							}
							
							$strValue.=$element["desc"];
						}
					}
				}
				else 
				{
					foreach ($arrayDynSectionValues as $element)
					{
						$idxName = $anInstance->QuestionFields[$i];
						
						if($strValue!="")
						{
							$strValue.="; ";
						}
						
						$strValue.=$element["desc"]."=".$element[$idxName];
					}
				}
			}
			
			$line.=',"'.str_replace('"', '""', $strValue).'"';
		}
		else if($anInstance->Questions[$i]->QTypeID==qtpMulti && $anInstance->Questions[$i]->QDisplayMode==dspVertical && $anInstance->Questions[$i]->MCInputType==1 && $anInstance->Questions[$i]->IsMultiDimension==0 && $anInstance->Questions[$i]->UseCategoryDimChoice!=0)
		{
			//En el caso de las preguntas con dimension categoria vamos a obtener la informacion desde una funcion q realice 
			//en una cadena con enters las respuestas q fueron capturadas
			$LN = "; ";
			//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
			$strCatDimValAnswers = BITAMReportCollection::getStrCatDimValAnswers($theRepository, $anInstance->Survey, $anInstance->Questions[$i], $factKeyDimVal, $LN, false);
			
			if(trim($strCatDimValAnswers)=="")
			{
				$strCatDimValAnswers = "NA";
			}

			$line.=',"'.str_replace('"', '""', $strCatDimValAnswers).'"';
		}
		else 
		{
			if ($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true)
			{
				//@JAPR 2013-02-07: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($anInstance->Survey->DissociateCatDimens) {
					$memberParentID = $objQuestion->CatIndDimID;
					$attribField = "DSC_".$memberParentID;
					$strFieldValue = '';
					if(array_key_exists(strtolower($attribField),$aRS->fields))
					{
						$strFieldValue = trim($aRS->fields[strtolower($attribField)]);
					}
					
					if($anInstance->FieldTypes[$i]==1)
					{
						$line.=','.$strFieldValue;
					}
					else
					{
						$line.=',"'.str_replace('"', '""', $strFieldValue).'"';
					}
				}
				else {
					//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
					$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($theRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
		
					$lowerFieldName = strtolower($attribField);
		
					if($anInstance->FieldTypes[$i]==1)
					{
						$line.=','.$aRS->fields[$lowerFieldName];
					}
					else
					{
						$line.=',"'.str_replace('"', '""', $aRS->fields[$lowerFieldName]).'"';
					}
				}
				//@JAPR
			}
			else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
			{
				$factKey = (int)$aRS->fields["factkey"];
				//En el caso de las matrices de simple choice vamos a obtener la informacion desde una funcion q realice 
				//en una cadena con enters las respuestas q fueron capturadas
				$strAnswersSCMatrix = $anInstance->getStrAnswersSCMatrix($anInstance->Questions[$i], $factKeyDimVal, $factKey);
				
				if(trim($strAnswersSCMatrix)=="")
				{
					$strAnswersSCMatrix = "NA";
				}

				$line.=',"'.str_replace('"', '""', $strAnswersSCMatrix).'"';
			}
			else 
			{
				$lowerFieldName = strtolower($anInstance->QuestionFields[$i]);
				if($anInstance->FieldTypes[$i]==1)
				{
					$line.=','.@$aRS->fields[$lowerFieldName];
				}
				else
				{
					$validValue = @$aRS->fields[$lowerFieldName];
					if($anInstance->FieldTypes[$i]==3)
					{
						if(trim($validValue)=="")
						{
							$validValue = "NA";
						}
					}
					else
					{
						if(trim($validValue)=="")
						{
							$validValue = "NA";
						}
					}
					
					$line.=',"'.str_replace('"', '""', $validValue).'"';
				}
			}
		}
	}
	array_push($afile, $line);
	$aRS->MoveNext();
}

if($bDebug)
{
	//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
	error_log("\n".'RepositoryName:'.$theRepository->RepositoryName.', surveyID='.$aSurveyID.', ModelID='.$aSurvey->ModelID.', anInstance->NumQuestionFields='.$anInstance->NumQuestionFields, 3, $strLogFilename);
	error_log("\n".'svStartDate:'.$startDate.', svEndDate:'.$endDate, 3, $strLogFilename);
	error_log("\n".'sql:'.$sql, 3, $strLogFilename);
	error_log("\n".$sDataDesc, 3, $strLogFilename);
	error_log("\n".(implode((chr(13).chr(10)), $afile)), 3, $strLogFilename);
}

//Parsear los Datos a Markers
class Marker
{
	public $desc='';
	public $long=0;
	public $lat =0;

	public function __construct($desc, $long, $lat)
	{
       $this->desc	= $desc;
       $this->long	= $long;
       $this->lat   = $lat;
	}
}

$bFijo=false;
if($bFijo)
{
	//Crear Markers Dummy
	$i=0;
	$aMarker[$i++]=new Marker('Bitam, Tampico, Mex', 22.249183, -97.868183);
	$aMarker[$i++]=new Marker('Monterrey, Mex', 25.648954,-100.302429);
	$aMarker[$i++]=new Marker('Ciudad de Mexico, Mex', 19.139979,-99.255981);
	$aMarker[$i++]=new Marker('Las Vegas, US', 36.155618,-115.172424);
	$aMarker[$i++]=new Marker('Houston, US', 29.847791,-95.369568);
	$aMarker[$i++]=new Marker('Dallas, US', 32.175612,-96.789551);
	$aMarker[$i++]=new Marker('San Antonio, US',29.113775,-98.22876);

	//Valores y Descripciones de los Markers
	for($iMaxDatas=5, $i=1, $sDataVals='', $sDataDesc=''; $i<$iMaxDatas; $i++)
	{
		$sDataDesc .='Data'.$i.(($i+1)<$iMaxDatas?$sep:'');
		$sDataVals .=$i.(($i+1)<$iMaxDatas?',':'');
	}
	//Formar los Markers en Cadena
	for($i=0, $sMarkerInf=''; $i<count($aMarker); $i++)
	{
		$sMarkerInf.=$aMarker[$i]->desc.$sep.$aMarker[$i]->long.$sep.$aMarker[$i]->lat.$sep.$sDataVals.$sepMarker;
	}
}
else
{
	$sDataDesc=str_ireplace('","', '"'.$sep.'"', $sDataDesc);
	for($i=0, $sMarkerInf=''; $i<count($afile); $i++)
	{   //Dividir los resultados de cada encuesta (viene sep por comas)
		$markerInf=explode(',', $afile[$i]);
		//Si tiene Longitud y Latitud Validos tomamos el punto
		if($bDebug) error_log("\n".'Latitud:'.$latitud[$i].', longitud='.$longitud[$i].', Data='.$afile[$i], 3, $strLogFilename);
		if(is_numeric($latitud[$i]) && is_numeric($longitud[$i]) && $latitud[$i]!=0 && $longitud[$i]!=0)
		{
			//Crear Marker
			$aMarker[]=new Marker($markerInf[0], $latitud[$i], $longitud[$i]);
			//Generar MD de todos los Markers
			$j=count($aMarker)-1;
			$sMarkerInf.=$aMarker[$j]->desc.$sep.$aMarker[$j]->long.$sep.$aMarker[$j]->lat.$sep.$afile[$i].$sepMarker;
		}
	}
	if($bDebug) error_log("\n*******************************", 3, $strLogFilename);
}
$sDataDesc =str_replace('"', '', $sDataDesc);
$sMarkerInf=str_replace('"', '', $sMarkerInf);
$sMarkerInf=str_replace("\n", ' ', $sMarkerInf);
$sMarkerInf=str_replace("\r", ' ', $sMarkerInf);
$sDataDesc =htmlspecialchars($sDataDesc, ENT_QUOTES);
$sMarkerInf=htmlspecialchars($sMarkerInf, ENT_QUOTES);

if($bDebug) error_log("\n".'DataDesc :'.$sDataDesc, 3, $strLogFilename);
if($bDebug) error_log("\n".'MarkerInf:'.$sMarkerInf, 3, $strLogFilename);
if($bDebug) error_log("\n*******************************", 3, $strLogFilename);
if($bDebug) error_log("\n*******************************", 3, $strLogFilename);
if($bDebug) error_log("\n*******************************", 3, $strLogFilename);
?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$wTitle?></title>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css">
		html { height: 100% }
		body { height: 100%; margin: 0; padding: 0 }
		#map_canvas { height: 100% }
	</style>
<?php
echo "   <script language='JavaScript'>\n";
echo "      var sDataDesc ='".$sDataDesc."';\n";
echo "      var sMarkerInf='".$sMarkerInf."';\n";
echo "      var sep       ='".$sep."';\n";
echo "      var sepMarker ='".$sepMarker."';\n";
echo "   </script>\n";
?>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script> -->
	<script type="text/javascript" src="https://maps-api-ssl.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/gmeSurvey.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/gmeSurvey.css"> -->
	<script type="text/javascript">
	</script>
</head>
<body onload="initialize()">
	<div id="map_canvas" style="width:100%; height:100%"></div>
</body>
</html>