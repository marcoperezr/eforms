<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once('dataSource.inc.php');

$showAllFrames = true;

if(isset($_SESSION["PAdisplayAllFrames"]))
{
	if($_SESSION["PAdisplayAllFrames"]==0)
	{
		$showAllFrames = false;
	}
}

if($showAllFrames)
{
	$strMainFrameset='4%,96%';
}
else 
{
	$strMainFrameset='0%,100%';
}

$strSaveFrameSize = '100%,*';

$_SESSION["PABITAM_UserID"];
require_once('appuser.inc.php');

$AppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
$strUserPassword = BITAMAppUser::getUserPassword($theRepository, $AppUser->CLA_USUARIO);
$UserMail=$AppUser->Email;

if (array_key_exists("menuType", $_GET))
{
	$menuType = (int) $_GET["menuType"];
}
else 
{
	$menuType = 0;
}
$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
$strScriptPath = $path_parts["dirname"];
require_once("eFormsAgentConfig.inc.php");
global $blnIsAgentEnabled;
//21Ene2013: Validar que si no existe la tabla SI_SV_SurveyTasks no se muestre el menú de Tasks
$displayTasks = true;
$sqlTasks = "SELECT SurveyTaskID FROM SI_SV_SurveyTasks";
$aRSTasks = $theRepository->DataADOConnection->Execute($sqlTasks);
if ($aRSTasks === false)
{
	//La tabla no existe, por lo tanto no se despliega la opción Tasks
	$displayTasks = false;
}
$displayTasks = true;
//@JAPR 2014-04-01: Agregada la ventana para permitir enviar mensajes vía Push
$displayPush = false;
$arrCatalogid = array();
$arrCatalogname = array();
$arrFilterId = array();
$arrFilterName = array();
//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
$arrFilterFormula = array();
$arrSurveID = array();
$status = array();
/*Obtengo todos los catalagos*/
$sqlTasks = "SELECT DISTINCT a.catalogid, b.datasourcename, IFNULL(c.filterid,-1) AS filterid, IFNULL(c.filtername,'') AS filtername
	FROM si_sv_section a
	INNER JOIN si_sv_dataSource b 
		ON a.catalogid = b.DataSourceID
	LEFT JOIN si_sv_datasourcefilter c
		ON b.DataSourceID = c.catalogid
	INNER JOIN si_sv_survey d
		ON a.surveyID = d.surveyID
	WHERE a.catalogid > 0
	AND d.surveyID in (" . implode(", ", $arrSurveID) . ")
UNION
SELECT DISTINCT a.catalogid, b.datasourcename, IFNULL(c.filterid,-1) AS filterid, IFNULL(c.filtername,'') AS filtername
	FROM si_sv_question a
	INNER JOIN si_sv_dataSource b 
		ON a.catalogid = b.DataSourceID
	LEFT JOIN si_sv_datasourcefilter c
		ON b.DataSourceID = c.catalogid
	INNER JOIN si_sv_survey d
		ON a.surveyID = d.surveyID
	WHERE a.catalogid > 0
	AND d.surveyID in (" . implode(", ", $arrSurveID) . ")
ORDER BY 2";
$UserID = isset($_GET["UserID"]) ? $_GET["UserID"] : NULL;
$UserGroupID = isset($_GET["UserGroupID"]) ? $_GET["UserGroupID"] : NULL;

//GCRUZ 2016-05-24. Agregado campo para fórmula escrita
$strAddFieldsUserID = '';
$strAddFieldsUserGroupID = '';
if (getMDVersion() >= esvDataSourceFilterFormula) {
	$strAddFieldsUserID = ", IFNULL((SELECT T1.filterformula FROM si_sv_datasourcefilter T1 INNER JOIN si_sv_datasourcefilterUSER T2 ON T2.filterID = T1.FilterID WHERE T2.UserID = " . $UserID . " AND T1.CatalogID = b.DataSourceID), '') AS filterformula";
	$strAddFieldsUserGroupID = ", IFNULL((SELECT T1.filterformula FROM si_sv_datasourcefilter T1 INNER JOIN si_sv_datasourcefilterrol T2 ON T2.filterID = T1.FilterID WHERE T2.RolID = " . $UserGroupID . " AND T1.CatalogID = b.DataSourceID), '') AS filterformula";
}

if ($UserID) {
	$sqlTasks = "SELECT DISTINCT b.DataSourceID, b.datasourcename, IFNULL((SELECT T1.filterID FROM si_sv_datasourcefilterUSER T1 INNER JOIN si_sv_datasourcefilter T2 ON T2.filterID = T1.filterID WHERE T1.UserID = " . $UserID . " AND T2.CatalogID = b.DataSourceID),-1) AS filterid,
					IFNULL((SELECT T1.filtername FROM si_sv_datasourcefilter T1 INNER JOIN si_sv_datasourcefilterUSER T2 ON T2.filterID = T1.FilterID WHERE T2.UserID = " . $UserID . " AND T1.CatalogID = b.DataSourceID), '') AS filtername, 
					IFNULL((SELECT T1.status FROM si_sv_datasourcefilter T1 INNER JOIN si_sv_datasourcefilterUSER T2 ON T2.filterID = T1.FilterID WHERE T2.UserID = " . $UserID . " AND T1.CatalogID = b.DataSourceID), 0) AS status {$strAddFieldsUserID} 
					FROM si_sv_dataSource b 
					WHERE b.DataSourceID > 0 order by b.datasourcename";
}else {
	$sqlTasks = "SELECT DISTINCT b.DataSourceID, b.datasourcename, IFNULL((SELECT T1.filterID FROM si_sv_datasourcefilterrol T1 INNER JOIN si_sv_datasourcefilter T2 ON T2.filterID = T1.filterID WHERE T1.RolID = " . $UserGroupID . " AND T2.CatalogID = b.DataSourceID),-1) AS filterid,
					IFNULL((SELECT T1.filtername FROM si_sv_datasourcefilter T1 INNER JOIN si_sv_datasourcefilterrol T2 ON T2.filterID = T1.FilterID WHERE T2.RolID = " . $UserGroupID . " AND T1.CatalogID = b.DataSourceID), '') AS filtername, 
					IFNULL((SELECT T1.status FROM si_sv_datasourcefilter T1 INNER JOIN si_sv_datasourcefilterrol T2 ON T2.filterID = T1.FilterID WHERE T2.RolID = " . $UserGroupID . " AND T1.CatalogID = b.DataSourceID), 0) AS status {$strAddFieldsUserGroupID} 
					FROM si_sv_dataSource b 
					WHERE b.DataSourceID > 0 order by b.datasourcename";
}

$rs2 = $theRepository->DataADOConnection->Execute($sqlTasks);
while ($rs2 && !$rs2->EOF){
	$arrCatalogid[]=$rs2->fields['datasourceid'];
	$arrCatalogname[]=$rs2->fields['datasourcename'];
	$arrFilterId[] = $rs2->fields['filterid'];
	$arrFilterName[] = $rs2->fields['filtername'];
	$arrFilterFormula[] = BITAMDataSource::ReplaceMemberIDsByNames($theRepository, $rs2->fields['datasourceid'], @$rs2->fields['filterformula']);
	$status[]=$rs2->fields['status'];
	$rs2->MoveNext();
}
if (getMDVersion() >= esvPasswordLangAndRedesign) {
	$sql = "SELECT COUNT(*) AS Regs FROM SI_SV_REGID";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$displayPush = ((int) @$aRS->fields["regs"]) > 0;
	}
}
$numSyncs = 0;

$arrDashboards = array();
$sql = "SELECT CLA_ESCENARIO, NOM_ESCENARIO 
	FROM SI_ESCENARIO 
	WHERE CLA_ESCENARIO > 0 AND CLA_USUARIO >= 0 AND (CLA_ROL >= 0 OR CLA_ROL IS NULL) AND CLA_GPO_ESC > 0";
$aRS = $theRepository->ADOConnection->Execute($sql);
if ($aRS) {
	while (!$aRS->EOF) {
		$intDashboardID = (int) @$aRS->fields["cla_escenario"];
		$strDashboardName = (string) @$aRS->fields["nom_escenario"];
		if ($intDashboardID > 0) {
			$arrDashboards[$intDashboardID] = $strDashboardName;
			if (count($arrDashboards) >= 8) {
				break;
			}
		}
		$aRS->MoveNext();
	}
}

/*$Cad = '[{"id":"208","data":["cesaralexlopez@gmail.com"]},{"id":"209","data":["22"]},{"id":"210","data":["15 a 29"]},{"id":"211","data":["2015-05-06 00:00:00"]},{"id":"212","data":[""]},{"id":"213","data":[""]},{"id":"214","data":["1505031715107030290","1504302110107020249","1505061901107030406"]},{"id":"215","data":[""]},{"id":"216","data":["OJO DE AGUA","CIUDAD AZTECA"]},{"id":"217","data":["CHD1504"]},{"id":"218","data":["Sabritas Todas las presentaciones","Glade Todos los aromatizantes en aerosol"]}]';
$data = json_decode($Cad, true);
foreach ($data as $key => $value) {
	var_dump($value["id"]); // Contiene el Id
	foreach ($value["data"] as $key => $val) {
		var_dump($val);  //$Val contiene los n valores del subArray
	}
}
//PrintMultiArray($data);
die();*/

?>
<html>
<link href='favicon.ico' rel='shortcut icon' type='image/x-icon'/>
<link href='favicon.ico' rel='icon' type='image/x-icon'/>
<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
<link href="css/font/roboto/robotofont.css" rel="stylesheet" type="text/css">
<!--<script src="js/codebase/dhtmlx.js"></script>-->
<script src="js/codebase/dhtmlxFmtd.js"></script>
<script type="text/javascript" src="js/json2.min.js"></script>
<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>
<style>
/* it's important to set width/height to 100% for full-screen init */
html, body {
	width: 100%;
	height: 100%;
	margin: 0px;
	overflow: hidden;
}
.dhx_dataview {
	overflow-y:auto !important;
}
.dhxdataview_placeholder{
	  height: 100% !important;
}
table.hdr td {
	cursor:pointer !important;
}
div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
	  text-transform: none !important;
}
</style>
<script language="JavaScript">
	var objFormsDSFilter;
	var objStatsLayOut;
	var arrvalueMembers;
	var objSecurityLayOut;
	var objFormsGrid;
	var objDataSrcsGrid;
	var objDailyEntriesDataView;
	var menuType = <?=$menuType?>;
	var UserRole = <?=(int) @$_SESSION["PABITAM_UserRole"]?>;
	var eFormsMDVersion = <?=getMDVersion()?>;
	var esvSurveyMenu = <?=esvSurveyMenu?>;
	var esvAdminWYSIWYG = <?=esvAdminWYSIWYG?>;
	var esvAgendaScheduler = <?=esvAgendaScheduler?>;
	var esveFormsAgent = <?=esveFormsAgent?>;
	var blnIsAgentEnabled = <?=(int) ($blnIsAgentEnabled && $displayTasks)?>;
	var gbIsGeoControl = <?=(int) $gbIsGeoControl?>;
	var displayPush = <?=(int) $displayPush?>;
	var dhxUsers = "a";
	var objWindows;
	var objDialog;
	var filterid;
	var filterName;
	var otyAttribute = 7; //Identificador en el processRequest.php
	var UserID = <?=(int) $UserID?>;
	var UserGroupID = <?=(int) $UserGroupID?>;
	var typesave = (UserID>0?1:2);
	var t = -1;

	function onLoad() {
		var rows = [];
		var data = {};
		objFormsDSFilter = new dhtmlXLayoutObject({
			parent: document.body,
			pattern: "1C" /*4F genera una platilla de 3 renglones con el primer renglon dividido en 2 secciones*/
		});
		objFormsDSFilter.cells(dhxUsers).hideHeader();
		/*Coloco fijas todas las columnas*/
		objFormsDSFilter.cells(dhxUsers).fixSize(true, true);

		objFormsGrid = objFormsDSFilter.cells(dhxUsers).attachGrid();
		objFormsGrid.setIconsPath("<?=$strScriptPath?>/images/admin/");
		objFormsGrid.setHeader("<?=translate("Catalog")?>,<?=translate("Activate filter")?>,<?=translate("Filters")?>, id");//La cabecera de las columnas
        //objFormsGrid.setInitWidths("100%");          //el ancho de las columna s 
		objFormsGrid.setInitWidthsP("15,10,75,10");
        objFormsGrid.setColAlign("left,left,left,left");       //la alineacion de las columnas  
        objFormsGrid.setColTypes("ro,ch,ro,ro");                //el tipo "ro"(solo lectura)
        objFormsGrid.setStyle("background-color:rgb(254,204,98);font-family:Arial;font-weight:bold;font-size:10pt;", "background-color:rgb(240,240,240);color:rgb(51,51,51);font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);color:rgb(240,240,240);font-family:Arial;font-size:9pt;");
      	objFormsGrid.init();
		objFormsGrid.setColumnHidden(3,true);
<?
		for($i=0; $i<count($arrCatalogid); $i++){
			if (trim($arrFilterFormula[$i]) != '')
			{
			//JAPR 2015-11-30: Agregado escape de comillas para los nombres de grupos con comillas
				//GCRUZ 2016-07-21. Eliminar <ENTER> de las fórmulas
				$arrFilterFormula[$i] = str_replace(PHP_EOL, ' ', $arrFilterFormula[$i]);
				$arrFilterFormula[$i] = str_replace("\r", ' ', $arrFilterFormula[$i]);
				$arrFilterFormula[$i] = str_replace("\n", ' ', $arrFilterFormula[$i]);
				//@JAPR 2018-11-13: Corregido el escape de algunos caracteres en strings (#KGW0CU)
?>
		rows.push({id:<?= $arrCatalogid[$i] ?>, data: ["<?= GetValidJScriptText($arrCatalogname[$i]) ?>","<?= $status[$i] ?>","<?= GetValidJScriptText($arrFilterFormula[$i]) ?>","<?= $arrFilterId[$i] ?>"] });
<?
			}
			else
			{
			//JAPR 2015-11-30: Agregado escape de comillas para los nombres de grupos con comillas
			//@JAPR 2018-11-13: Corregido el escape de algunos caracteres en strings (#KGW0CU)
?>
		rows.push({id:<?= $arrCatalogid[$i] ?>, data: ["<?= GetValidJScriptText($arrCatalogname[$i]) ?>","<?= $status[$i] ?>","<?= GetValidJScriptText($arrFilterName[$i]) ?>","<?= $arrFilterId[$i] ?>"] });
<?
			}
	}
?>
		data.rows = rows;
		objFormsGrid.parse(data,"json");
		objFormsGrid.forEachRow(function(id) {
			this.setCellTextStyle(id,2,"cursor:pointer;");
		});
		objFormsGrid.attachEvent("onRowSelect", function(rId,cInd) {	//onRowDblClicked
			var filterid = this.cells(rId,3).getValue();
			var filterName = this.cells(rId,0).getValue();
			//2015-10-20@JRPP No se permiten los # en la cadena de modo que si encuentro uno lo remplazo por una N mayuscula
			while (filterName.indexOf("#")>=0){
				filterName = filterName.replace("#", "N");
			}
			var status =  this.cells(rId,1).getValue();
			strURL= "main.php?BITAM_PAGE=DataSourceFilter&CatalogID=" + rId + "&FilterID=" + filterid  + "&typesave=" + typesave + "&UserID=" + UserID + "&UserGroupID=" + UserGroupID + "&Status=" + status + "&FilterName=" + filterName;
			
			objWindows = new dhtmlXWindows({
				image_path:"images/"
			});
	
			objDialog = objWindows.createWindow({
				id:"newObject",
				left:0,
				top:0,
				width:700,
				height:450,
				center:true,
				modal:true
			});
			objDialog.setText("<?=translate("Filters")?>");
			objDialog.denyPark();
			objDialog.denyResize();
			objDialog.setIconCss("without_icon");
			//Al cerrar realizará el cambio de sección de la pregunta
			objDialog.attachEvent("onClose", function() {
				return true;
			});
			objDialog.progressOn();
			objDialog.attachURL(strURL);
		});
		objFormsGrid.attachEvent("onCheck", function(rId,cInd,state){
		    var filterid = this.cells(rId,3).getValue();
		    if (filterid>0){
		    	var objData = {};
		    	objData.filterid = filterid;
		    	objData.stateValue = (state==true?1:0);
		    	var data = JSON.stringify(objData);
				var urlPath = 'loadexcel/';
				$.post(urlPath +'action.php?action=UpdateFilterState', {"data":data}).done(function( r ) {
					//Se termino la peticion
				});
		    }
		});
	}
	function openFilterWindow(idcatalog) {
		//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
		objWindows = new dhtmlXWindows({
			image_path:"images/"
		});
	
		
		objDialog = objWindows.createWindow({
			id:"newObject",
			left:0,
			top:0,
			width:700,
			height:350,
			center:true,
			modal:true
		});
		objDialog.setText("<?=translate("Filters")?>");
		objDialog.denyPark();
		objDialog.denyResize();
		objDialog.setIconCss("without_icon");
		//Al cerrar realizará el cambio de sección de la pregunta
		objDialog.attachEvent("onClose", function() {
			return true;
		});

		var Layout = objDialog.attachLayout("2E");
		Layout.cells("a").hideHeader();
		Layout.cells("a").fixSize(true, true); //Bloque el Rezise del Content a)
		Layout.cells("b").hideHeader();
		Layout.cells("b").setHeight(45);
		Layout.cells("b").fixSize(true, true); //Bloque el Rezise del Content b)

		var objGridValues = Layout.cells("a").attachGrid();
		objGridValues.setHeader("<?=translate("Member")?>,<?=translate("Values")?>");//La cabecera de las columnas
        //objGridValues.setInitWidths("100%");          //el ancho de las columna s 
		objGridValues.setInitWidthsP("30,70");
        objGridValues.setColAlign("left,left");       //la alineacion de las columnas  
        objGridValues.setColTypes("ro,ro"); 
        objGridValues.setStyle("background-color:rgb(254,204,98);font-family:Arial;font-weight:bold;font-size:10pt;", "background-color:rgb(240,240,240);color:rgb(51,51,51);font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);color:rgb(240,240,240);font-family:Arial;font-size:9pt;");
		objGridValues.enableLightMouseNavigation(true);
		objGridValues.init();
		getMembers(idcatalog, objGridValues);
		objGridValues.attachEvent("onEditCell", function(stage,id,index) {
			switch (stage) {
				case 0: //Before start
					//Antes de iniciar, se debe llenar la columna con la lista de opciones corresponiente utilizando sólo las descripciones, ya que este
					//componente no soporta uso de ids
					this.registerCList(1, arrvalueMembers[id]);
					break;
				case 1:
					//No es necesario
					break;

				case 2: //No es necesario
					break;
			}
			//@AAL 28/07/2015: Agregado para ajustar el Height del componente MiltipleList, cuado hay una lista grande, ésta desbordaba.
			if ($(".dhx_clist").height() > 80) {
				$(".dhx_clist").css({"height": "100px", "overflow-y": "scroll"});
			}

			//Return true permite la edición, false la cancela
			return true;
		});

		var objFormData = [
				{type:"block", blockOffset:240, offsetLeft:250, list:[
					{type:"button", name: "btnOk", value:"<?=translate("OK")?>",width:200},
					{type:"newcolumn"},
					{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>",width:200}
				]}
			];

		//Removemos la clase para no mostrar los bordes
		Layout.cells("b").cell.childNodes[1].removeAttribute("class");
		var objForm = Layout.cells("b").attachForm(objFormData);
		objForm.attachEvent("onButtonClick", function(name) {
			switch (name) {
				case "btnCancel":
					setTimeout(function () {
						doUnloadDialog();
					}, 100);
					break;
					
				case "btnOk":
				    var data = [];
			    	objGridValues.forEachRow(function(id) {
			    		//De esta forma no es conveniente por que deja espacios vacios en el Arrays y los llena con null
			    		//data[id] = this.cells(id, 1).getValue().split(',');
			    		data.push({id:id, data:this.cells(id, 1).getValue().split(',')});
					});
					//JAPR 2018-11-01: Corregido un bug con el escape de caracteres en requests (#S0SRC1)
					data = encodeURIComponent(JSON.stringify(data));
					//JAPR
					var strParams = "CatalogID=" + idcatalog + "&FilterID=" + filterid + "&Process=Add&RequestType=1&ObjectType=" + otyAttribute + "&data=" + data;
					
					setTimeout(function() {
						//@AAL Agregado para mandar a guardar los datos del usuario
						window.dhx4.ajax.post("processRequest.php", strParams, doSaveConfirmation);
					}, 100);
					break;
			}
		});
	}

	//Descarga la instancia de la forma de la memoria
	function doUnloadDialog() {
		if (objDialog) {
			var objForm = objDialog.getAttachedObject();
			if (objForm && objForm.unload) {
				objForm.unload();
				objForm = null;
			}
		}
		
		if (objWindows) {
			if (objWindows.unload) {
				objWindows.unload();
				objWindows = null;
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
			}
		}
		
		objDialog = null;
		return true;
	}

	function getMembers(idcatalog,objGrid) {
		var rows = [];
		var data = {};
		var urlPath = 'loadexcel/';
		$.get(urlPath +'action.php?action=getmembers&idcatalog=' + idcatalog).done(function( r ) {
			arrvalueMembers = r.arrValueMember;
			if (r.arrMembers){
				for (var i = 0; i<r.arrMembers.length;i++){
					rows.push({id:r.fieldName[i], data: [r.arrMembers[i],""]});
				}
				data.rows = rows;
				objGrid.parse(data,"json");
				
				objGrid.forEachRow(function(id) {
					this.setCellExcellType(id, 1, "clist");
				});
			}
		});
	}
	
	/* Procesa la respuesta de un request de actualización de datos asíncrona y presenta el error en pantalla en caso de existir alguno
	Esta función está pensada como un callback de un request Ajax que regresará un contenido en JSon con infomación adicional de error
	*/
	function doSaveConfirmation(loader) {
		console.log('doSaveConfirmation');
		
		if (!loader || !loader.xmlDoc) {
			return;
		}
		
		if (loader.xmlDoc && loader.xmlDoc.status != 200) {
			alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
			return;
		}
		
		//Si llega a este punto es que se recibió una respuesta correctamente
		var response = loader.xmlDoc.responseText;
		try {
			var objResponse = JSON.parse(response);
		} catch(e) {
			alert(e + "\r\n" + response.substr(0, 1000));
			return;
		}
		
		if (objResponse.error) {
			alert(objResponse.error.desc);
			return;
		}
		else {
			if (objResponse.warning) {
				console.log(objResponse.warning.desc);
			}
		}
		
		//La respuesta no tiene error, debió haber terminado de grabar correctamente
		//alert('Todo actualizado ok: ' + response);
	}
	
</script>
</head>
<body onload="onLoad();"  style="width: 100%; height: 100%;">
	<div id="__menu__" style="overflow:auto;"></div>
</body>
</html>