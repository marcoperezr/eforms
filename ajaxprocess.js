function makeAjaxProcess (success, error) {
    
    if(!WEB_MODE && device.phonegap == '0.9.5') {
        navigator.network.isReachable('kpionline.com', function reachableCallback(reachability) {
            var networkState = reachability.code || reachability;
            var states = {};
            states[NetworkStatus.NOT_REACHABLE]                      = 'No network connection';
            states[NetworkStatus.REACHABLE_VIA_CARRIER_DATA_NETWORK] = 'Carrier data connection';
            states[NetworkStatus.REACHABLE_VIA_WIFI_NETWORK]         = 'WiFi connection';
            if(states[networkState] != 'No network connection') {
                success();
            } else {
                error();
            }
        }, function() {});
    } else {
        if(isOnline) {
            success();
        } else {
            error();
        }
    }
}