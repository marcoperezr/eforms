<?php

require_once("dimensionValue.inc.php");
require_once("dimensionTableField.inc.php");

class BITAMDimension extends BITAMObject
{
	public $consecutivo;
	public $nom_logico;
	public $nom_fisico;
	public $nom_fisico_bd;
	public $tipo_dato;
	public $long_dato;
	public $nom_fisicok;
	public $nom_fisicok_bd;
	public $tipo_datok;
	public $long_datok;
	public $nom_tabla;
	public $cla_concepto;
	public $cla_descrip;
	public $values;
	public $valuesID;
	//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
	//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
	public $subrrogateID;		// Colección con los valores correspondientes a la llave Subrrogada de cada miembro de la dimensión (se carga sólo mediante un parámetro)
	public $parentJoinsIDs;		//Array multidimensional, la primer dimensión es el ID del miembro de dimensión correspondiente, a partir de la 2da dimensión es el campo de padre con el nombre usado como índice
	//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
	public $isCatalog;
	//@JAPR
	public $numValues;
	public $count; 
	public $numberOfTimes;
	
	public $HasHierarchy;
	public $HierarchyID;
	public $HierarchyDimIDs;
	
	//sólo aplica a los periodos, es el formato en el q se graban los tiempos en la tabla de periodos
	//indicada en el campo dim_periodo de la tabla si_concepto
	public $fmt_periodo;
	//Si $cla_periodo = NULL o -1 se trata de una dimensión, si es >= 0 es una dimensión de tiempo o periodo
	public $cla_periodo;
	public $parameters;
	public $dependsOnDimension;
	//@JAPR 2010-01-27: Agregado el soporte para Atributos de cubos del SAAS RI
	//Con la introducción de SAAS - FB y el SAAS RI, ahora se hace oficial que las dimensiones adicionales a una tabla de Catálogo (practicamente siempre son
	//dimensiones sin Llave) se llamarán "Atributos", y se identifican por tener registros en SI_DESCRIP_DET pero NO tener nada especial en SI_CPTO_LLAVE.PAR_DESCRIPTOR
	public $IsAttribute;			//(True) indica si la dimensión es una adicional a la tabla del mismo catálogo, (False) significa que la dimensión es la Primaria del catálogo y por ende la que se liga a la tabla de hechos (aunque pudiera tener un campo diferente como Join)
	public $BaseDimensionKey;		//Si la dimensión IsAttribute, esto indicará el cla_descrip de la dimensión Primaria de su catálogo
	//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
	public $hasAttributes;			// Indica si la dimensión tiene Atributos hijos (True) que deberían ser capturados junto a sus valores (si se habilita dicha funcionalidad)
	//@JAPR
	public $ChildrenHierarchyDimIDs;
	
	public $NoApplyValue;
	public $ChildParDescriptor;
	public $FatherParDescriptor;
	public $InmmediateFatherParDescriptor;
	// Para crear columnas directamente en la tabla de hechos
	public $cla_conexion;
	public $created_by;				// 0 = Artus Administrator, 1 = Ektos, 2 = Artus Wizard
	public $ChildDescripKey;		// Clave del hijo por el que pasa para llegar a la tabla de hechos (solo se asigna si se está creando una dimensión agrupadora)
	//@JAPR 2009-07-08: Agregados los filtros para la carga de valores de dimensiones
	public $dimensionValuesFilter;	//Filtro (temporal, se pierde al termina la sesión) a aplicar para la carga de valores
	public $maxDimensionValueToShow;//Cantidad de valores máxima a mostrar en el listado de valores (configurable en forma global dentro de SI_CONFIGURA)
	public $showNullParentValues;	//Indica si al consultar la lista de valores se deberían o no ver todos los valores incluso con padres que no estén asignados
	//@JAPR 2012-03-08: Agregado el indicador de versión por registro para descarga inteligente de catálogos
	public $VersionFieldName;		//Nombre del campo en la tabla del catálogo que corresponde al indicador con la versión de cada registro
	public $MaxVersionNum;			//Máximo número de versión grabado en el campo Versión de la tabla de catálogo (se debe incrementar manualmente una vez se actualice el catálogo, o bien volver a invocar a la función getCatalogVersion)
	public $VersionIndID;			//ID del indicador que contiene la versión de cada registro del catálogo
	//@JAPR 2012-03-27: Actualizadas las encuestas cuando los catálogos que usan cambian
	public $CatalogID;				//ID del catálogo de ESurvey (se debe invocar a la función getCatalogVersion para asignar este valor)
	//@JAPR	
	
	public $ConsecutivoJerarquia;	//Si esta asociado a una jerarquia se guarda el valor consecutivo que ocupa en la jerarquía
	
	public $nomfisicok_descripkeys; //Es el campo join en la tabla del catalogo de X dimensión registrado en la tabla SI_DESCRIP_KEYS
	
	function __construct($aRepository, $aCubeID = -1)
	{	
		global $maxDimensionRecords;
		global $dimValuesFilter;
		global $bShowNullParentValues;
		
		BITAMObject::__construct($aRepository);
		$this->consecutivo=-1;
		$this->nom_logico="";
		$this->nom_fisico="";
		$this->nom_fisico_bd="";
		$this->tipo_dato="";
		$this->long_dato=255;
		$this->nom_fisicok="";
		$this->nom_fisicok_bd="";
		$this->tipo_datok="";
		$this->long_datok=0;
		$this->nom_tabla="";
		$this->cla_concepto=$aCubeID;
		$this->cla_descrip=-1;
		$this->values= array();
		$this->valuesID= array();
		//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
		//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
		$this->subrrogateID = array();
		$this->parentJoinsIDs = array();
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		$this->isCatalog = false;
		//@JAPR
		$this->numValues=-1;
		$this->count=0;
		//Número de veces q se repite consecutivamente cada valor
		$this->numberOfTimes=-1;
		$this->fmt_periodo="";
		$this->cla_periodo=-1;
	
		$this->HasHierarchy = false;
		$this->HierarchyID=-1;
		$this->HierarchyDimIDs = array();
		$this->parameters='';
		$this->dependsOnDimension=0;
		//@JAPR 2010-01-27: Agregado el soporte para Atributos de cubos del SAAS RI
		$this->IsAttribute = false;
		$this->BaseDimensionKey = 0;
		//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
		$this->hasAttributes = false;
		//@JAPR
		$this->ChildrenHierarchyDimIDs = array();
		//Equivalente a DEFAULT_KEY
		$this->NoApplyValue = -99999;
		
		$this->ChildParDescriptor = array();
		$this->FatherParDescriptor = array();
		$this->InmmediateFatherParDescriptor = array();
		
		$this->cla_conexion=-1;
		$this->created_by = 0;
		$this->ChildDescripKey = null;
		//@JAPR 2009-07-08: Agregados los filtros para la carga de valores de dimensiones
		$this->dimensionValuesFilter = $dimValuesFilter;
		$this->maxDimensionValueToShow = $maxDimensionRecords;
		$this->showNullParentValues = $bShowNullParentValues;
		//@JAPR 2012-03-08: Agregado el indicador de versión por registro para descarga inteligente de catálogos
		$this->VersionFieldName = '';
		$this->MaxVersionNum = -1;
		$this->VersionIndID = 0;
		//@JAPR 2012-03-27: Actualizadas las encuestas cuando los catálogos que usan cambian
		$this->CatalogID = 0;
		//@JAPR
		
		$this->ConsecutivoJerarquia = -1;
		
		$this->nomfisicok_descripkeys = "";
	}
	
	static function NewInstance($aRepository, $aCubeID = -1)
	{
		return new BITAMDimension($aRepository, $aCubeID);
	}
	
	//@JAPR 2010-07-20: Agregados los parámetros $bGetMySubrrogateID y $bGetMyParentsID para cargar las nuevas colecciones de valores de la 
	//llave Subrrogada y de los padres (subrrogados si es el caso) de la dimensión que está siendo instanciada con este método
	static function NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, $aGetValues = true, $useGlobalInstance = true, $bGetMySubrrogateID = false, $bGetMyParentsID = false)
	{
		global $appSettings;
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			return BITAMDimension::NewInstanceWithDescripID($aRepository, $aDimensionID, $aCubeID, $aGetValues, $useGlobalInstance, $bGetMySubrrogateID, $bGetMyParentsID);
		}
		//@JAPR
		$anInstance = null;
		
		if (((int) $aDimensionID) < 0 || ((int) $aCubeID) < 0)
		{
			return $anInstance;
		}

		if($useGlobalInstance)
		{
			$anInstance =& BITAMGlobalInstance::GetDimensionInstanceWithID($aCubeID, $aDimensionID);
		}
		
		if(!is_null($anInstance))
		{
			if($aGetValues)
			{
				$anInstance->getDimensionValues(null, $bGetMySubrrogateID, $bGetMyParentsID);
			}
			return $anInstance;
		}

		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		$sql = "SELECT A.CLA_CONCEPTO, A.CONSECUTIVO, A.FMT_PERIODO, A.CLA_PERIODO, A.NOM_LOGICO, B.NOM_FISICO, B.TIPO_DATO, B.LONG_DATO, B.NOM_FISICOK, B.TIPO_DATOK, B.LONG_DATOK, B.NOM_TABLA, A.PAR_DESCRIPTOR, B.DEFAULT_KEY, A.CLA_DESCRIP, B.CLA_CONEXION, B.CREATED_BY, B.IS_CATALOG ".
			"FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B ".
			"WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.CLA_CONCEPTO = ".$aCubeID." AND A.CONSECUTIVO = ".$aDimensionID;

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE or SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMDimension::NewInstanceFromRS($aRepository, $aRS, $aGetValues, null, $useGlobalInstance, $bGetMySubrrogateID, $bGetMyParentsID);
		}
		return $anInstance;
	}
	
	//@JAPR 2010-07-20: Agregados los parámetros $bGetMySubrrogateID y $bGetMyParentsID para cargar las nuevas colecciones de valores de la 
	//llave Subrrogada y de los padres (subrrogados si es el caso) de la dimensión que está siendo instanciada con este método
	static function NewInstanceWithDescripID($aRepository, $aDescripKey, $aCubeID, $aGetValues = true, $useGlobalInstance = true, $bGetMySubrrogateID = false, $bGetMyParentsID = false)
	{
		global $appSettings;
		
		$anInstance = null;
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			if (((int) $aDescripKey) < 0)
			{
				return $anInstance;
			}
		}
		else
		{
			if (((int) $aDescripKey) < 0 || ((int) $aCubeID) < 0)
			{
				return $anInstance;
			}
		}
		//@JAPR

		//$anInstance =& BITAMGlobalInstance::GetDimensionInstanceWithID($aCubeID, $aDescripKey);
		if(!is_null($anInstance))
		{
			if($aGetValues)
			{
				$anInstance->getDimensionValues(null, $bGetMySubrrogateID, $bGetMyParentsID);
			}
			return $anInstance;
		}
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$sql = "SELECT -1 AS CLA_CONCEPTO, -1 AS CONSECUTIVO, '' AS fMT_PERIODO, -1 AS CLA_PERIODO, B.NOM_LOGICO, B.NOM_FISICO, B.TIPO_DATO, B.LONG_DATO, B.NOM_FISICOK, B.TIPO_DATOK, B.LONG_DATOK, B.NOM_TABLA, '00' AS PAR_DESCRIPTOR, B.DEFAULT_KEY, B.CLA_DESCRIP, B.CLA_CONEXION, B.CREATED_BY, B.IS_CATALOG ".
				"FROM SI_DESCRIP_ENC B ".
				"WHERE B.CLA_DESCRIP = ".$aDescripKey;
		}
		else
		{
			$sql = "SELECT A.CLA_CONCEPTO, A.CONSECUTIVO, A.FMT_PERIODO, A.CLA_PERIODO, A.NOM_LOGICO, B.NOM_FISICO, B.TIPO_DATO, B.LONG_DATO, B.NOM_FISICOK, B.TIPO_DATOK, B.LONG_DATOK, B.NOM_TABLA, A.PAR_DESCRIPTOR, B.DEFAULT_KEY, A.CLA_DESCRIP, B.CLA_CONEXION, B.CREATED_BY, B.IS_CATALOG ".
				"FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B ".
				"WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.CLA_CONCEPTO = ".$aCubeID." AND B.CLA_DESCRIP = ".$aDescripKey;
		}
		//@JAPR

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE or SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMDimension::NewInstanceFromRS($aRepository, $aRS, $aGetValues, null, $useGlobalInstance, $bGetMySubrrogateID, $bGetMyParentsID);
		}
		return $anInstance;
	}
	
	function getDimensionIDByName($aRepository, $aDimensionName, $aCubeID)
	{
		$dimensionID=null;
		
		$anInstance = null;
		
		if (trim($aDimensionName)=="" || ((int) $aCubeID) < 0)
		{
			return $anInstance;
		}

		$dimensionID =& BITAMGlobalInstance::GetDimensionIDByName($aCubeID, $aDimensionName);
		if(!is_null($dimensionID))
		{
			return $dimensionID;
		}

		
		$sql = "SELECT A.CONSECUTIVO FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B, SI_CONCEPTO C ".
		" WHERE A.CLA_CONCEPTO=C.CLA_CONCEPTO AND A.CLA_DESCRIP=B.CLA_DESCRIP AND A.CLA_CONCEPTO = ".$aCubeID.
		" AND A.NOM_LOGICO = ".$aRepository->ADOConnection->Quote($aDimensionName);
				
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE A, SI_DESCRIP_ENC B, SI_CONCEPTO C ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$dimensionID = (int) $aRS->fields["consecutivo"];
		}

		BITAMGlobalInstance::AddDimensionIDByName($aCubeID, $aDimensionName, $dimensionID);
		
		return $dimensionID;
	}
	
	//@JAPR 2010-07-20: Agregados los parámetros $bGetMySubrrogateID y $bGetMyParentsID para cargar las nuevas colecciones de valores de la 
	//llave Subrrogada y de los padres (subrrogados si es el caso) de la dimensión que está siendo instanciada con este método
	static function NewInstanceFromRS($aRepository, $aRS, $aGetValues = true, $budgetID=null, $useGlobalInstance = true, $bGetMySubrrogateID = false, $bGetMyParentsID = false)
	{
		global $appSettings;
		
		$aCubeID=(int) $aRS->fields["cla_concepto"];
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$aDimensionID = (int) $aRS->fields["cla_descrip"];
		}
		else 
		{
			$aDimensionID = (int) $aRS->fields["consecutivo"];
		}
		//@JAPR
		$anInstance = null;
		
		if($useGlobalInstance)
		{
			$anInstance =& BITAMGlobalInstance::GetDimensionInstanceWithID($aCubeID, $aDimensionID);
		}
		
		if(is_null($anInstance))
		{
			$anInstance = BITAMDimension::NewInstance($aRepository);
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				//Para efecto de las colecciones, el consecutivo se igualará al Id de dimensión global para que sea único en lugar del 0 del Recordset
				$anInstance->consecutivo=  (int) $aRS->fields["cla_descrip"];
			}
			else
			{
				$anInstance->consecutivo=  (int) $aRS->fields["consecutivo"];
			}
			$anInstance->nom_logico =  $aRS->fields["nom_logico"];
			$anInstance->nom_fisico =  strtolower($aRS->fields["nom_fisico"]);
			$anInstance->nom_fisico_bd =  $aRS->fields["nom_fisico"];
			$anInstance->tipo_dato =   (int) $aRS->fields["tipo_dato"];
			$anInstance->long_dato =   (int) $aRS->fields["long_dato"];
			$anInstance->nom_fisicok =  strtolower($aRS->fields["nom_fisicok"]);
			$anInstance->nom_fisicok_bd =  $aRS->fields["nom_fisicok"];		
			$anInstance->tipo_datok =  (int) $aRS->fields["tipo_datok"];
			$anInstance->long_datok=   (int) $aRS->fields["long_datok"];
			$anInstance->nom_tabla =    $aRS->fields["nom_tabla"];
		 	$anInstance->cla_concepto= (int) $aRS->fields["cla_concepto"];
		 	$anInstance->cla_descrip= (int) $aRS->fields["cla_descrip"];
		 	$anInstance->fmt_periodo= $aRS->fields["fmt_periodo"];
		 	//@JAPR 2008-06-05: Validado que los NULLs no se conviertan a 0 (Periodo hora)
		 	if (is_null($aRS->fields["cla_periodo"]))
		 	{
		 		$anInstance->cla_periodo=-1;
		 	}
		 	else
		 	{
		 		$anInstance->cla_periodo= (int)$aRS->fields["cla_periodo"];
		 	}
		 	$anInstance->NoApplyValue= $aRS->fields["default_key"];

		 	if(array_key_exists('created_by',$aRS->fields))
		 	{
				$anInstance->created_by= (int)$aRS->fields["created_by"];
		 	}
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			//Sólo existen dimensiones catálogo si fueron creadas por el Model Manager
			//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			//Ahora las dimensiones pueden ser creadas por eForms, así que esas también se consideran de catálogo
		 	if(($anInstance->created_by == CREATED_BY_RAPID_WEB || $anInstance->created_by == CREATED_BY_EFORMS) && array_key_exists('is_catalog',$aRS->fields))
		 	{
				if (!is_null($aRS->fields["is_catalog"]))
				{
					$anInstance->isCatalog = (bool) $aRS->fields["is_catalog"];
				}
		 	}
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			//Las dimensiones catálogo se pueden grabar con descripción vacía ya que internamente si no contienen Llave, utilizarán la
			//llave subrrogada
			if ($anInstance->isCatalog)
			{
				//Las dimensiones catálogo se pueden grabar con descripción vacía ya que internamente si no contienen Llave, utilizarán la
				//llave subrrogada, así que sobreescribimos la información de la llave subrrogada para que funcione como la llave de la dimensión
				//siempre y cuando no exista ya una llave para esta
				if ($anInstance->nom_fisicok_bd == $anInstance->nom_fisico_bd)
				{
					//Como la llave y el descriptor son el mismo, se debe utilizar la llave subrrogada como llave de dimensión
					//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					//Ahora las dimensiones pueden ser creadas por eForms, así que esas también se consideran de catálogo
					if ($anInstance->created_by == CREATED_BY_RAPID_WEB || $anInstance->created_by == CREATED_BY_EFORMS)
					{
						$anInstance->nom_fisicok_bd = $anInstance->nom_tabla.'KEY';
						$anInstance->nom_fisicok = strtolower($anInstance->nom_fisicok_bd);
					}
				}
			}
		 	//@JAPR
		 	if(array_key_exists('cla_conexion',$aRS->fields))
		 	{
				$anInstance->cla_conexion= (int)$aRS->fields["cla_conexion"];
		 	}

		 	if($anInstance->NoApplyValue===null || ( trim($anInstance->NoApplyValue)=="" && strlen($anInstance->NoApplyValue)<=1 ) )
		 	{
		 		switch($anInstance->tipo_datok)
		 		{
		 			case 1:
		 				if($anInstance->long_datok===null)
		 				{
		 					$anInstance->long_datok=0;
		 				}
		 				if($anInstance->long_datok>=3)
		 				{
		 					$anInstance->NoApplyValue='*ND'.str_repeat(' ',$anInstance->long_datok - 3);	
		 				}
		 				else 
		 				{
		 					$anInstance->NoApplyValue='*ND';
		 				}
		 				break;
		 			case 2:
		 				$anInstance->NoApplyValue='*ND';
		 				break;
		 			case 6:
		 			case 7:
		 			case 8:
		 			case 21:
		 			case 23:
		 			case 24:
		 				$anInstance->NoApplyValue=-99999;
		 				break;
		 			default:	// 12, 22 (DateTime, SmallDateTime)
		 				$anInstance->NoApplyValue='';
		 				break;
		 		}
		 	}
			
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			if ($appSettings["UseGlobalDimensions"] == 0)
			{
			 	$anInstance->getHierarchy();
		
			 	$arrayChildParDescriptor = array();
			 	
			 	if(array_key_exists('par_descriptor',$aRS->fields))
			 	{
			 		$strParameters=trim($aRS->fields['par_descriptor']);
			 		if($strParameters!='')
			 		{
			 			$anInstance->parameters=$strParameters;
			 			$arrParameters=explode('|',$strParameters);
			 			if(count($arrParameters)>1)
			 			{
			 				$anInstance->dependsOnDimension=(int)$arrParameters[1];
			 				//BITAMDimension::getChildParDescriptor($aRepository, (int)$arrParameters[1], $anInstance->cla_concepto, $arrayChildParDescriptor);
			 				BITAMDimension::getChildParDescriptor($aRepository, $anInstance->consecutivo, $anInstance->cla_concepto, $arrayChildParDescriptor);
			 				
			 			}
			 		}
			 	}
		 		
			 	$intNumElements = count($arrayChildParDescriptor);
			 	if($intNumElements>0)
			 	{
			 		//@JAPR 2008-06-19: Validación vs referencias circulares
			 		if ($arrayChildParDescriptor[$intNumElements-1] == $anInstance->consecutivo)
			 		{
			 			array_pop($arrayChildParDescriptor);
			 		}
				 	$anInstance->ChildParDescriptor = array_values( array_unique( $arrayChildParDescriptor ) );
			 	}
			 	
			 	$arrayFatherParDescriptor = array();
			 	BITAMDimension::getFatherParDescriptor($aRepository, $anInstance->consecutivo, $anInstance->cla_concepto, $arrayFatherParDescriptor, $anInstance);
			 	
			 	if(count($arrayFatherParDescriptor)>0)
			 	{
				 	$anInstance->FatherParDescriptor = array_values( array_unique( $arrayFatherParDescriptor ) );
			 	}

			 	//Obtener el valor del campo nomfisicok de la tabla SI_DESCRIP_KEYS
			 	$anInstance->nomfisicok_descripkeys = BITAMDimension::getNomFisicoKFromDescripKeys($aRepository, $anInstance->cla_concepto, $anInstance->nom_tabla);
			}
			//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			//Ahora las dimensiones pueden ser creadas por eForms, así que esas también se consideran de catálogo
			elseif ($anInstance->created_by == CREATED_BY_RAPID_WEB || $anInstance->created_by == CREATED_BY_EFORMS)
			{
				//Si la dimensión fué creada por el Model Manager, se asume que siempre contiene una llave Subrrogada que funciona como Join
				//entre esta tabla hacia la que se aproxime a la de hechos, así que el nombre del campo sigue cierta nomenclatura fácilmente deducible
				$anInstance->nomfisicok_descripkeys = $anInstance->nom_tabla.'KEY';
			}
			//@JAPR
		 	
			//@JAPR 2010-01-27: Agregado el soporte para Atributos de cubos del SAAS RI
		 	//Obtiene la información de Atributo de la dimensión
		 	$anInstance->getAttributeData();
		 	//@JAPR
		 	
		 	if($useGlobalInstance)
		 	{
		 		BITAMGlobalInstance::AddDimensionInstanceWithID($aCubeID, $aDimensionID, $anInstance);
		 	}
		}
		
	 	if ($aGetValues)
	 	{
		 	$anInstance->getDimensionValues($budgetID, $bGetMySubrrogateID, $bGetMyParentsID);
	 	}
		return $anInstance;
	}
	
	static function getNomFisicoKFromDescripKeys($aRepository, $cubeID, $nomTabla)
	{
		$nomfisicok_descrip_keys = "";
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		$sql = "SELECT NOM_FISICOK FROM SI_DESCRIP_KEYS ";
		if ($cubeID > 0)
		{
			$sql .= "WHERE CLA_CONCEPTO = ".$cubeID." AND ";
		}
		else 
		{
			$sql .= "WHERE ";
		}
		$sql .= "NOM_TABLA = ".$aRepository->ADOConnection->Quote($nomTabla);
		//@JAPR
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_KEYS ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$nomfisicok_descrip_keys = $aRS->fields["nom_fisicok"];
		}
		
		return $nomfisicok_descrip_keys;
	}

	function default_key()
	{
		return $this->NoApplyValue;
	}
	
	/*@JAPR 2008-05-21: El parámetro $aDimInstance le indica al método que grabe en dicha instancia todas las referencias a sus Padres de
	SnowFlake inmediatos, ya que el arreglo ->FatherParDescriptor contiene los padres en todos los niveles superiores pero sin identificar
	de que rama son cada uno 
	*/
	static function getFatherParDescriptor($aRepository, $fatherParDescriptor, $cubeID, &$arrayFatherParDescriptor, $aDimInstance = null, &$arrayProcessedChildParDescriptor = null)
	{
		global $checkDisabledCubes;
		
		$intNumElements = 0;
		if (is_null($arrayProcessedChildParDescriptor))
		{
			$arrayProcessedChildParDescriptor = array();
		}
		$intNumElements = count($arrayProcessedChildParDescriptor);
		if($fatherParDescriptor<10)
		{
			$strFatherParDescriptor = "0".$fatherParDescriptor;
		}
		else 
		{
			$strFatherParDescriptor = "".$fatherParDescriptor."";
		}
		
		//@JAPR 2010-01-29: Corregida la validación del LIKE para incluir IDs de dimensión de mas de 2 caracteres
		//@JAPR 2010-03-18: Validado que sólo se carguen dimensiones visibles
		$sql = "SELECT PAR_DESCRIPTOR FROM SI_CPTO_LLAVE 
				WHERE PAR_DESCRIPTOR LIKE ".$aRepository->ADOConnection->Quote("%__|".$strFatherParDescriptor)." AND CLA_CONCEPTO = ".$cubeID." AND ESTATUS = 1";
		if ($intNumElements > 0)
		{
			$sql .= ' AND CONSECUTIVO NOT IN ('.implode(',',$arrayProcessedChildParDescriptor).')';
		}
		//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
		if ($checkDisabledCubes)
		{
			$sql .= ' AND CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
		}
		//@JAPR
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$arrayProcessedChildParDescriptor[] = $fatherParDescriptor;
		while (!$aRS->EOF)
		{
	 		$strParDecriptor=trim($aRS->fields['par_descriptor']);
	 		
	 		if($strParDecriptor!='')
	 		{
	 			$arrayElements=explode('|',$strParDecriptor);
	 			
	 			if(count($arrayElements)>1)
	 			{
	 				$aParDecriptor=(int)$arrayElements[0];
	 				
	 				$arrayFatherParDescriptor[] = $aParDecriptor;
	 				if (!is_null($aDimInstance))
	 				{
	 					$aDimInstance->InmmediateFatherParDescriptor[] = $aParDecriptor;
	 				}
	 				
	 				BITAMDimension::getFatherParDescriptor($aRepository, $aParDecriptor, $cubeID, $arrayFatherParDescriptor,null,$arrayProcessedChildParDescriptor);
	 			}
	 		}
	 		
	 		$aRS->MoveNext();
		}
		
	}

	static function getFeaturesDimension($aRepository, $aDimensionID, $aCubeID)
	{
		global $appSettings;
		global $checkDisabledCubes;
		
		//@JAPR 2010-03-18: Validado que sólo se carguen dimensiones visibles
		//@JAPR 2011-06-23: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$sql = "SELECT B.CLA_DESCRIP, B.NOM_FISICOK, B.NOM_FISICO, B.NOM_LOGICO, B.NOM_TABLA, B.DEFAULT_KEY, B.TIPO_DATO, B.TIPO_DATOK, B.LONG_DATOK, B.CREATED_BY, B.IS_CATALOG ".
				"FROM SI_DESCRIP_ENC B ".
				"WHERE B.CLA_DESCRIP = ".$aDimensionID;
		}
		else
		{
			$sql = 'SELECT B.CLA_DESCRIP, B.NOM_FISICOK, B.NOM_FISICO, B.NOM_LOGICO, B.NOM_TABLA, B.DEFAULT_KEY, B.TIPO_DATO, B.TIPO_DATOK, B.LONG_DATOK, B.CREATED_BY, B.IS_CATALOG ".
				"FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B ".
				"WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.CLA_CONCEPTO = '.$aCubeID.' AND A.CONSECUTIVO = '.$aDimensionID.' AND A.ESTATUS = 1';
			//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
			if ($checkDisabledCubes)
			{
				$sql .= ' AND CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
			}
		}
		//@JAPR
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE, SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$featuresDimension = null;
		
		if (!$aRS->EOF)
		{
			$featuresDimension = array();
			$featuresDimension["nom_fisicok_bd"] = $aRS->fields["nom_fisicok"];
			$featuresDimension["nom_fisicok"] = strtolower($aRS->fields["nom_fisicok"]);
			//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
			//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
			$featuresDimension["nom_fisicok_join_bd"] = $featuresDimension["nom_fisicok_bd"];
			//@JAPR
			$featuresDimension["nom_fisico_bd"] = $aRS->fields["nom_fisico"];
			$featuresDimension["nom_fisico"] = strtolower($aRS->fields["nom_fisico"]);
			$featuresDimension["nom_logico"] = $aRS->fields["nom_logico"];
			$featuresDimension["nom_tabla"] = $aRS->fields["nom_tabla"];
			$featuresDimension["default_key"] = $aRS->fields["default_key"];
			$featuresDimension["tipo_dato"] = (int)$aRS->fields["tipo_dato"];
			$featuresDimension["tipo_datok"] = (int)$aRS->fields["tipo_datok"];
			$featuresDimension["consecutivo"] = $aDimensionID;
			$featuresDimension["cla_descrip"] = (int)$aRS->fields["cla_descrip"];
			$featuresDimension["cla_concepto"] = $aCubeID;
			$featuresDimension["parent_nom_fisicok_bd"] = '';
			$featuresDimension["parent_nom_fisicok"] = '';
			$featuresDimension["parent_tipo_datok"] = 7;
			$featuresDimension["parent_nom_fisicok_join"]= '';
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			$featuresDimension["is_catalog"] = (bool) (int) $aRS->fields["is_catalog"];
			//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
			//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
			$featuresDimension["parent_tipo_datok_join"]= tINT;
			// @JAPR
			$featuresDimension["created_by"] = (int)$aRS->fields["created_by"];
			/*@JAPR 2008-05-21: Agregada la asignación de multiples padres, esto se consigue con un arreglo multidimensional
			indexado por la clave de consecutivo de cada padre dentro del elemento 'parents', el cual contendrá internamente
			un arreglo con la información de los elementos 'parent_xxx'
			*/
			$featuresDimension["parents"] = array();
			/*@JAPR 2008-05-21: Agregada la asignación de atributos, esto se consigue con un arreglo multidimensional
			indexado por la clave de consecutivo de cada atributo dentro del elemento 'attributes', el cual contendrá internamente
			un arreglo con la información de los elementos 'attribute_xxx'
			*/
			$featuresDimension["attributes"] = array();
			$aParentDimensionID = -1;
			$aParentDimensionArray = array();
			
			//@JAPR 2011-06-23: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			if ($appSettings["UseGlobalDimensions"] == 0)
			{
				if ((int) $aDimensionID < 10)
				{
					$strFatherParDescriptor = '0'.$aDimensionID;
				}
				else 
				{
					$strFatherParDescriptor = ''.$aDimensionID.'';
				}
				
				//@JAPR 2010-03-18: Validado que sólo se carguen dimensiones visibles
				$sql = "SELECT PAR_DESCRIPTOR FROM SI_CPTO_LLAVE 
					WHERE PAR_DESCRIPTOR LIKE ".$aRepository->ADOConnection->Quote("%|".$strFatherParDescriptor)." AND CLA_CONCEPTO = ".$aCubeID.' AND ESTATUS = 1';
				//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
				if ($checkDisabledCubes)
				{
					$sql .= ' AND CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
				}
				//@JAPR
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while (!$aRS->EOF)
				{
			 		$strParDecriptor = trim($aRS->fields['par_descriptor']);
			 		if ($strParDecriptor != '')
			 		{
			 			$arrayElements = explode('|',$strParDecriptor);
			 			if (count($arrayElements) > 1)
			 			{
			 				/*@JAPR 2008-05-21: Para soporte de versiones anteriores, la variable $aParentDimensionID contendrá todavía el
			 				primer padre encontrado según el query, así mismo los elementos del arreglo que hacían referencia a él seguirán
			 				tratandose igual, pero se agregarán nuevos elementos para contener la info de todos los padres encontrados
			 				*/
			 				if ($aParentDimensionID <= 0)
			 				{
			 					$aParentDimensionID = (int) $arrayElements[0];
			 				}
			 				//Almacena la info de todos los padres encontrados, indexados por el consecutivo
			 				$aParentDimensionArray[] = (int) $arrayElements[0];
			 			}
			 		}
			 		//@JAPR 2008-05-21: Removido el break para poder cargar la info de múltiples padres
			 		//break;
			 		$aRS->MoveNext();
				}
			}
			//@JAPR
			
			if ($aParentDimensionID > 0)
			{
				/*Modificado para leer todos los posibles padres simultáneamente, y asignar la información para cada uno de ellos en
				nuevos elementos, mientras se deja la información del original en los elementos que inicialmente se usaban para el único
				padre leído en versiones anteriores
				*/
				$strParentDimensionIDs = implode(",", $aParentDimensionArray);
				//@JAPR 2010-03-18: Validado que sólo se carguen dimensiones visibles
				$sql = 'SELECT B.CLA_DESCRIP, B.NOM_FISICOK, B.NOM_FISICO, B.NOM_LOGICO, B.NOM_TABLA, B.DEFAULT_KEY, B.TIPO_DATO, B.TIPO_DATOK, B.LONG_DATOK, A.CONSECUTIVO '.
					'FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B '.
					'WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.CLA_CONCEPTO = '.$aCubeID.' AND A.CONSECUTIVO IN ('.$strParentDimensionIDs.') AND A.ESTATUS = 1';
				//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
				if ($checkDisabledCubes)
				{
					$sql .= ' AND A.CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
				}
				//@JAPR
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE, SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while (!$aRS->EOF)
				{
					$aConsecutive = $aRS->fields["consecutivo"];
					//Código para obtener el nom_fisicok_join de la dim $aParentDimensionID
					$nom_tabla=$aRS->fields["nom_tabla"];
					$sql = "SELECT NOM_FISICOK_JOIN FROM SI_DESCRIP_KEYS WHERE NOM_TABLA=".$aRepository->ADOConnection->Quote($nom_tabla);
					$aRSJoin = $aRepository->ADOConnection->Execute($sql);
					if ($aRSJoin === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					//@JAPR 2005-08-21: Asignada la información del padre original
					if ($aConsecutive == $aParentDimensionID)
					{
						$featuresDimension["parent_nom_fisicok_bd"] = $aRS->fields["nom_fisicok"];
						$featuresDimension["parent_nom_fisicok"] = strtolower($aRS->fields["nom_fisicok"]);
						$featuresDimension["parent_tipo_datok"] = (int)$aRS->fields["tipo_datok"];
						if (!$aRSJoin->EOF)
						{	
							$featuresDimension["parent_nom_fisicok_join"]=$aRSJoin->fields["nom_fisicok_join"];
							//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
							//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
							if (trim($featuresDimension["parent_nom_fisicok_join"]) != '' && $featuresDimension["parent_nom_fisicok_bd"] != $featuresDimension["parent_nom_fisicok_join"])
							{
								//@JAPRWarning:
								//Se asume por ahora que si el Join no es el mismo que el ID del padre, es una llave subrrogada y debe ser Integer
								$featuresDimension["parent_tipo_datok_join"] = tINT;
							}
							else 
							{
								//Utiliza el mismo tipo de dato del ID, pues el join es el mismo (o no existe Join, en cuyo caso se usa el ID)
								$featuresDimension["parent_tipo_datok_join"] = $featuresDimension["parent_tipo_datok"];
							}
							//JAPR
						}
					}
					
					//@JAPR 2008-05-21: Agregada la asignación de multiples padres
					$featuresDimension["parents"][$aConsecutive] = array();
					$featuresDimension["parents"][$aConsecutive]["parent_consecutivo"] = (int) $aRS->fields["consecutivo"];
					$featuresDimension["parents"][$aConsecutive]["parent_cla_descrip"] = (int) $aRS->fields["cla_descrip"];
					$featuresDimension["parents"][$aConsecutive]["parent_nom_fisicok_bd"] = $aRS->fields["nom_fisicok"];
					$featuresDimension["parents"][$aConsecutive]["parent_nom_fisicok"] = strtolower($aRS->fields["nom_fisicok"]);
					$featuresDimension["parents"][$aConsecutive]["parent_tipo_datok"] = (int)$aRS->fields["tipo_datok"];
					$featuresDimension["parents"][$aConsecutive]["parent_long_datok"] = (int)$aRS->fields["long_datok"];
					if (!$aRSJoin->EOF)
					{	
						$featuresDimension["parents"][$aConsecutive]["parent_nom_fisicok_join"]=$aRSJoin->fields["nom_fisicok_join"];
					}
					$featuresDimension["parents"][$aConsecutive]["parent_nom_logico"]=$aRS->fields["nom_logico"];
					//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
					//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
					if (trim($featuresDimension["parents"][$aConsecutive]["parent_nom_fisicok_join"]) != '' && $featuresDimension["parents"][$aConsecutive]["parent_nom_fisicok_bd"] != $featuresDimension["parents"][$aConsecutive]["parent_nom_fisicok_join"])
					{
						//@JAPRWarning:
						//Se asume por ahora que si el Join no es el mismo que el ID del padre, es una llave subrrogada y debe ser Integer
						$featuresDimension["parents"][$aConsecutive]["parent_tipo_datok_join"] = tINT;
					}
					else 
					{
						//Utiliza el mismo tipo de dato del ID, pues el join es el mismo (o no existe Join, en cuyo caso se usa el ID)
						$featuresDimension["parents"][$aConsecutive]["parent_tipo_datok_join"] = $featuresDimension["parents"][$aConsecutive]["parent_tipo_datok"];
					}
					//JAPR
			 		$aRS->MoveNext();
				}
			}
			
			//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
			//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
			//Código para obtener el nom_fisicok_join de la dim que está siendo cargada (usado para mostrar los padres de dimensiones)
			//JAPRWarning: Posible bug, no está aplicando el Id del cubo, así que el join regresado pudiera no corresponder para este caso
			//@JAPR 2011-06-23: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				//Ahora las dimensiones pueden ser creadas por eForms, así que esas también se consideran de catálogo
				if ($featuresDimension["created_by"] == CREATED_BY_RAPID_WEB || $featuresDimension["created_by"] == CREATED_BY_EFORMS)
				{
					$featuresDimension["nom_fisicok_join_bd"] = $featuresDimension["nom_tabla"].'KEY';
				}
			}
			else
			{
				$sql = "SELECT NOM_FISICOK, NOM_FISICOK_JOIN FROM SI_DESCRIP_KEYS WHERE NOM_TABLA=".$aRepository->ADOConnection->Quote($featuresDimension["nom_tabla"]);
				$aRSJoin = $aRepository->ADOConnection->Execute($sql);
				if ($aRSJoin === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				if (!$aRSJoin->EOF)
				{	
					$featuresDimension["nom_fisicok_join_bd"] = $aRSJoin->fields["nom_fisicok"];
				}
			}
			
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			if ($featuresDimension["is_catalog"])
			{
				//Las dimensiones catálogo se pueden grabar con descripción vacía ya que internamente si no contienen Llave, utilizarán la
				//llave subrrogada, así que sobreescribimos la información de la llave subrrogada para que funcione como la llave de la dimensión
				//siempre y cuando no exista ya una llave para esta
				if ($featuresDimension["nom_fisicok_bd"] == $featuresDimension["nom_fisico_bd"])
				{
					//Como la llave y el descriptor son el mismo, se debe utilizar la llave subrrogada como llave de dimensión
					//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					//Ahora las dimensiones pueden ser creadas por eForms, así que esas también se consideran de catálogo
					if ($featuresDimension["created_by"] == CREATED_BY_RAPID_WEB || $featuresDimension["created_by"] == CREATED_BY_EFORMS)
					{
						$featuresDimension["nom_fisicok_bd"] = $featuresDimension["nom_fisicok_join_bd"];
						$featuresDimension["nom_fisicok"] = strtolower($featuresDimension["nom_fisicok_bd"]);
					}
				}
			}
			//@JAPR
			
			//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
			if ($appSettings['DimensionAttributes'] == 1)
			{
				//@JAPR 2010-03-18: Validado que sólo se carguen dimensiones visibles
				//@JAPR 2011-06-23: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
				if ($appSettings["UseGlobalDimensions"] == 1)
				{
					$sql = 'SELECT B.CLA_DESCRIP, B.NOM_FISICOK, B.NOM_FISICO, B.NOM_LOGICO, B.NOM_TABLA, B.DEFAULT_KEY, B.TIPO_DATO, B.TIPO_DATOK, B.LONG_DATOK, B.CLA_DESCRIP AS CONSECUTIVO '.
						'FROM SI_DESCRIP_ENC B, SI_DESCRIP_DET C '.
						'WHERE B.CLA_DESCRIP = C.CLA_DESCRIP AND C.CLA_PADRE = '.$featuresDimension["cla_descrip"];
				}
				else
				{
					$sql = 'SELECT B.CLA_DESCRIP, B.NOM_FISICOK, B.NOM_FISICO, B.NOM_LOGICO, B.NOM_TABLA, B.DEFAULT_KEY, B.TIPO_DATO, B.TIPO_DATOK, B.LONG_DATOK, A.CONSECUTIVO '.
						'FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B, SI_DESCRIP_DET C '.
						'WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND B.CLA_DESCRIP = C.CLA_DESCRIP AND A.CLA_CONCEPTO = '.$aCubeID.' AND C.CLA_PADRE = '.$featuresDimension["cla_descrip"].' AND A.ESTATUS = 1';
					//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
					if ($checkDisabledCubes)
					{
						$sql .= ' AND A.CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
					}
				}
				//@JAPR
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE, SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while (!$aRS->EOF)
				{
					$aConsecutive = $aRS->fields["consecutivo"];
					//Verifica que el campo del consecutivo no sea realmente un ID de Dimensión Padre, de forma que no duplique campos
					if (!isset($featuresDimension["parents"][$aConsecutive]))
					{
						$featuresDimension["attributes"][$aConsecutive] = array();
						$featuresDimension["attributes"][$aConsecutive]["attribute_consecutivo"] = (int) $aRS->fields["consecutivo"];
						$featuresDimension["attributes"][$aConsecutive]["attribute_cla_descrip"] = (int) $aRS->fields["cla_descrip"];
						$featuresDimension["attributes"][$aConsecutive]["attribute_nom_fisicok_bd"] = $aRS->fields["nom_fisicok"];
						$featuresDimension["attributes"][$aConsecutive]["attribute_nom_fisicok"] = strtolower($aRS->fields["nom_fisicok"]);
						$featuresDimension["attributes"][$aConsecutive]["attribute_tipo_datok"] = (int)$aRS->fields["tipo_datok"];
						$featuresDimension["attributes"][$aConsecutive]["attribute_long_datok"] = (int)$aRS->fields["long_datok"];
						//$featuresDimension["attributes"][$aConsecutive]["attribute_nom_fisicok_join"]=$aRSJoin->fields["nom_fisicok_join"];
						$featuresDimension["attributes"][$aConsecutive]["attribute_nom_logico"]=$aRS->fields["nom_logico"];
					}
			 		$aRS->MoveNext();
				}
			}
		}
		
		return $featuresDimension;
	}
	
	static function getChildParDescriptor($aRepository, $childParDescriptor, $cubeID, &$arrayChildParDescriptor, &$arrayProcessedChildParDescriptor = null)
	{
		global $checkDisabledCubes;
		
		$intNumElements = 0;
		if (is_null($arrayProcessedChildParDescriptor))
		{
			$arrayProcessedChildParDescriptor = array();
		}
		$intNumElements = count($arrayProcessedChildParDescriptor);
		//@JAPR 2010-03-18: Validado que sólo se carguen dimensiones visibles
		$sql = "SELECT PAR_DESCRIPTOR FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = ".$cubeID." AND CONSECUTIVO = ".$childParDescriptor." AND ESTATUS = 1";
		if ($intNumElements > 0)
		{
			$sql .= ' AND CONSECUTIVO NOT IN ('.implode(',',$arrayProcessedChildParDescriptor).')';
		}
		//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
		if ($checkDisabledCubes)
		{
			$sql .= ' AND CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
		}
		//@JAPR
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
	 		$strParDecriptor=trim($aRS->fields['par_descriptor']);
	 		
	 		if($strParDecriptor!='')
	 		{
	 			$arrayElements=explode('|',$strParDecriptor);
	 			
	 			if(count($arrayElements)>1)
	 			{
	 				$aParDecriptor=(int)$arrayElements[1];
	 				
	 				$arrayChildParDescriptor[] = $aParDecriptor;
	 				$arrayProcessedChildParDescriptor[] = $childParDescriptor;
	 				BITAMDimension::getChildParDescriptor($aRepository, $aParDecriptor, $cubeID, $arrayChildParDescriptor, $arrayProcessedChildParDescriptor);
	 			}
	 		}
		}
	}
	
	//@JAPR 2010-01-27: Agregado el soporte para Atributos de cubos del SAAS RI
	//Verifica si la dimensión es o no un Atributo, y obtiene el ID del Atributo padre si lo es
	//Se trata de un atributo si la dimensión comparte la misma tabla que otra, y tiene una dependencia a ella en SI_DESCRIP_DET
	function getAttributeData()
	{
		$this->IsAttribute = false;
		//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
		$this->hasAttributes = false;
		$this->BaseDimensionKey = 0;
		$sql = 'SELECT B.CLA_PADRE, A.CLA_DESCRIP, A.NOM_TABLA '.
			'FROM SI_DESCRIP_ENC A, SI_DESCRIP_DET B '.
			'WHERE A.CLA_DESCRIP = B.CLA_PADRE AND (B.CLA_DESCRIP = '.$this->cla_descrip.' OR B.CLA_PADRE = '.$this->cla_descrip.')';
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC, SI_DESCRIP_DET ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$strTableName = $aRS->fields['nom_tabla'];
			if ($strTableName == $this->nom_tabla)
			{
				//@JAPR 2011-06-21: Agregada la captura de las dimensiones Atributo en forma de columna
				if ($this->cla_descrip == (int) $aRS->fields['cla_padre'])
				{
					$this->hasAttributes = true;
				}
				else 
				{
					$this->IsAttribute = true;
				}
				$this->BaseDimensionKey = (int) $aRS->fields['cla_descrip'];
			}
		}
	}
	//@JAPR
	
	function getHierarchy()
	{
		
		$sql = "SELECT A.CLA_JERARQUIA, A.CONSECUTIVO FROM SI_JERARQUIA A WHERE A.CLA_CONCEPTO = ".$this->cla_concepto." AND A.DIM_PADRE = ".$this->consecutivo."
				ORDER BY A.CLA_JERARQUIA, A.CONSECUTIVO";
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$this->HierarchyID = (int) $aRS->fields["cla_jerarquia"];
			$this->ConsecutivoJerarquia = (int) $aRS->fields["consecutivo"];
			
			$theInfoFatherHierarchy=null;
			$theInfoFatherHierarchy =& BITAMGlobalInstance::GetFatherHierarchy($this->cla_concepto, $this->consecutivo, $this->HierarchyID);
			if(!is_null($theInfoFatherHierarchy))
			{
				$this->HierarchyDimIDs = $theInfoFatherHierarchy;
			}
			else 
			{
				$sqlFather = 	"SELECT B.DIM_PADRE AS DIM_PADRE
								FROM SI_JERARQUIA A, SI_JERARQUIA B 
								WHERE A.DIM_PADRE = ".$this->consecutivo." 
								AND A.CLA_CONCEPTO = ".$this->cla_concepto." 
								AND A.CLA_JERARQUIA = ".$this->HierarchyID."
								AND A.CLA_CONCEPTO = B.CLA_CONCEPTO 
								AND A.CLA_JERARQUIA = B.CLA_JERARQUIA
								AND B.CONSECUTIVO < A.CONSECUTIVO
								ORDER BY B.CONSECUTIVO DESC";
				
				$aRSFather = $this->Repository->ADOConnection->Execute($sqlFather);
				
				if ($aRSFather === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlFather);
				}
				
				while (!$aRSFather->EOF)
				{
					$this->HierarchyDimIDs[] = (int) $aRSFather->fields["dim_padre"];
					
					$aRSFather->MoveNext();
				}
				
				BITAMGlobalInstance::AddFatherHierarchy($this->cla_concepto, $this->consecutivo, $this->HierarchyID, $this->HierarchyDimIDs);
			}
			
			$theInfoChildHierarchy=null;
			$theInfoChildHierarchy =& BITAMGlobalInstance::GetChildHierarchy($this->cla_concepto, $this->consecutivo, $this->HierarchyID);
			if(!is_null($theInfoChildHierarchy))
			{
				$this->ChildrenHierarchyDimIDs = $theInfoChildHierarchy;
			}
			else 
			{
				$sqlFather = 	"SELECT B.DIM_PADRE AS DIM_PADRE
								FROM SI_JERARQUIA A, SI_JERARQUIA B 
								WHERE A.DIM_PADRE = ".$this->consecutivo." 
								AND A.CLA_CONCEPTO = ".$this->cla_concepto." 
								AND A.CLA_JERARQUIA = ".$this->HierarchyID."
								AND A.CLA_CONCEPTO = B.CLA_CONCEPTO 
								AND A.CLA_JERARQUIA = B.CLA_JERARQUIA
								AND B.CONSECUTIVO > A.CONSECUTIVO
								ORDER BY B.CONSECUTIVO ASC";
				
				$aRSFather = $this->Repository->ADOConnection->Execute($sqlFather);
				
				if ($aRSFather === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlFather);
				}
				
				while (!$aRSFather->EOF)
				{
					$this->ChildrenHierarchyDimIDs[] = (int) $aRSFather->fields["dim_padre"];
					
					$aRSFather->MoveNext();
				}
				
				BITAMGlobalInstance::AddChildHierarchy($this->cla_concepto, $this->consecutivo, $this->HierarchyID, $this->ChildrenHierarchyDimIDs);				
			}
			
			$this->HasHierarchy = true;
		}
		else
		{
			$this->HasHierarchy = false;
		}
	
	}
	
	//@JAPR 2010-07-20: Agregados los parámetros $bGetMySubrrogateID y $bGetMyParentsID para cargar las nuevas colecciones de valores de la 
	//llave Subrrogada y de los padres (subrrogados si es el caso) de la dimensión que está siendo instanciada con este método
	function getDimensionValues($forBudgetID=null, $bGetMySubrrogateID = false, $bGetMyParentsID = false)
	{
		if(is_null($forBudgetID))
		{
			$arrKeys=null;
			$arrKeys =& BITAMGlobalInstance::GetDimensionKeysWithFilter($this->cla_concepto,$this->consecutivo,'');
			if(!is_null($arrKeys))
			{
				$this->valuesID =& BITAMGlobalInstance::GetDimensionKeysWithFilter($this->cla_concepto,$this->consecutivo,'');
				$this->values =& BITAMGlobalInstance::GetDimensionValuesWithFilter($this->cla_concepto,$this->consecutivo,'');
				//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
				//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
				$this->subrrogateID =& BITAMGlobalInstance::GetDimensionSubrrKeysWithFilter($this->cla_concepto,$this->consecutivo,'');
				$this->parentJoinsIDs =& BITAMGlobalInstance::GetDimensionParentsFldsWithFilter($this->cla_concepto,$this->consecutivo,'');
				//@JAPR
				$this->numValues=count($arrKeys);
				return;
			}
	
			//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
			//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
			$strParentField = '';
			$arrParents = array();
			$featuresDimension = BITAMDimension::getFeaturesDimension($this->Repository, $this->consecutivo, $this->cla_concepto);
			if (!is_null($featuresDimension) && array_key_exists('parents', $featuresDimension))
			{
				foreach ($featuresDimension['parents'] as $aParentConsecutive => $aParentInfo)
				{
					$strParentFieldName = $aParentInfo["parent_nom_fisicok_join"];
					if (trim($strParentFieldName) != '')
					{
						$strParentField .= ','.$strParentFieldName;
					}
				}
				if (trim($strParentField) != '')
				{
					$arrParents = explode(',', substr($strParentField, 1));
				}
				else 
				{
					$bGetMyParentsID = false;
				}
			}
			
			//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
			//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
			$strMySubrrogateField = '';
			//Si se solicitaron los valores incluyendo la información de la llave Subrrogada, se agrega
			//dicho campo a la consulta para generar el nuevo Array de datos para ella
			if ($bGetMySubrrogateID && trim($this->nomfisicok_descripkeys) != '' && $this->nom_fisicok_bd != $this->nomfisicok_descripkeys)
			{
				$strMySubrrogateField = ', '.$this->nomfisicok_descripkeys;
			}
			else
			{
				$bGetMySubrrogateID = false;
			}
			//@JAPR 2010-03-22: Validado que si el campo llave y descriptor sean iguales, el ORDER BY sea por posición
			$bSameKeyDescField = strtolower($this->nom_fisico) == (strtolower($this->nom_fisicok));
			$sql = "SELECT ".$this->nom_fisicok_bd." , ".$this->nom_fisico_bd.' '.$strParentField.$strMySubrrogateField.
				" FROM ".$this->nom_tabla. " ORDER BY ".(($bSameKeyDescField)?'2':$this->nom_fisico_bd);
			//@JAPR
			global $queriesLogFile;
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMDimension->GetDimensionValues: \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error")." ".translate("in")." getDimensionValues() ".translate("Function").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$count=0;
			while (!$aRS->EOF)
			{
	   		    if(in_array($aRS->fields[$this->nom_fisicok],$this->valuesID)===false)
	 		    {
					$this->valuesID[$count]=$aRS->fields[$this->nom_fisicok];
					$this->values[$aRS->fields[$this->nom_fisicok]] = $aRS->fields[$this->nom_fisico];
					//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
					//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
					if ($bGetMySubrrogateID)
					{
						$this->subrrogateID[$aRS->fields[$this->nom_fisicok]] = $aRS->fields[strtolower($this->nomfisicok_descripkeys)];
					}
					if ($bGetMyParentsID)
					{
						$this->parentJoinsIDs[$aRS->fields[$this->nom_fisicok]] = array();
						foreach ($arrParents as $strParentField)
						{
							$this->parentJoinsIDs[$aRS->fields[$this->nom_fisicok]][$strParentField] = @$aRS->fields[strtolower($strParentField)];
						}
					}
					//@JAPR
		 		    $count++;
	 		    }
	 		    $aRS->MoveNext();
			}
			$this->numValues=$count;
			BITAMGlobalInstance::AddDimensionKeysWithFilter($this->cla_concepto,$this->consecutivo,'',$this->valuesID);
			BITAMGlobalInstance::AddDimensionValuesWithFilter($this->cla_concepto,$this->consecutivo,'',$this->values);
			//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
			//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
			BITAMGlobalInstance::AddDimensionSubrrKeysWithFilter($this->cla_concepto,$this->consecutivo,'',$this->subrrogateID);
			BITAMGlobalInstance::AddDimensionParentsFldsWithFilter($this->cla_concepto,$this->consecutivo,'',$this->parentJoinsIDs);
			//@JAPR
		}
		else 
		{
			// Obtenemos la informacion del cubo
			//@JAPR 2010-07-19: Agregado el parámetro $bCreateQuery para indicar que la instancia de rolap sea generada automáticamente
			$theCube = BITAMCInfoCube::NewInstance($this->Repository, $this->cla_concepto, false, true);
	
			// Estamos listos para usar la funcionalidad de ROLAP
			$theCubeRolap = $theCube->rolap;
	
			// Initialize
			$theCubeRolap->Initialize();
			
			//Se establece temporalmente que el cubo no use agregaciones para asegurar que las combinaciones de dimensiones se obtengan de la Tabla de Hechos
			$aux_use_agregation = $theCubeRolap->use_agregation;
			$theCubeRolap->use_agregation = 0;
			
			$sql="SELECT PeriodID, DimensionMembersFrom, StartDateDim, EndDateDim, OrderByKey FROM SI_EKT_BudgetIndicator WHERE BudgetID=".$forBudgetID;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_EKT_BudgetIndicator ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if (!$aRS->EOF)
			{
				$dimensionMembersFrom = $aRS->fields["dimensionmembersfrom"];
				$startDateDim = $aRS->fields["startdatedim"];
				$endDateDim = $aRS->fields["enddatedim"];
				$periodID = $aRS->fields["periodid"];
				$orderByKey = $aRS->fields["orderbykey"];
			}
			
			//Si $dimensionMembersFrom=0 se obtienen las combinaciones del catálogo
			//Si $dimensionMembersFrom=1 se obtienen las combinaciones de la tabla de hechos
			//Si $dimensionMembersFrom=null se obtienen las combinaciones de la tabla de hechos
			if(is_null($dimensionMembersFrom))
			{
				$querySinTablaDeHechos=false;
			}
			else
			{
				if($dimensionMembersFrom==0)
				{
					$querySinTablaDeHechos=true;
				}
				else
				{
					$querySinTablaDeHechos=false;
				}
			}
			
			if($orderByKey==0)
			{
				$dimensionsOrderByKey = false;
			}
			else 
			{
				$dimensionsOrderByKey = true;
			}
			
			$aux_brange_date=$theCubeRolap->brange_date;
			if($querySinTablaDeHechos==false)
			{	
				require_once("artusmetadata.php");
				
				if($startDateDim!=="" && $startDateDim!==null && $endDateDim!=="" && $endDateDim!==null)
				{
					$accumulated=true;
					$rangeDate = getRange($this->Repository, $startDateDim, $endDateDim, $periodID);
					$theCubeRolap->InsertPeriod($periodID, $startDateDim, $endDateDim, true, $accumulated, $rangeDate, '', false, '' );
				}
				else 
				{
					$accumulated=true;
					$rangeDate = "";
					$theCubeRolap->brange_date=true;
					$rangeDate="";
					$periodStartDate=$_SESSION["PAperiodStartDate"];
					$dateElements=explode('-',$periodStartDate);
					$yearDate=$dateElements[0];
					$endDateAux=($yearDate+10)."-12-31";
					$theCubeRolap->InsertPeriod($periodID, '1950-01-01', $endDateAux, true, $accumulated, $rangeDate, '', false, '' );
				}
			}
			
			$theCubeRolap->QuerySinTablaDeHechos = $querySinTablaDeHechos;
			$theCubeRolap->borderbykey = $dimensionsOrderByKey;
	
			// Filter
			$theCubeRolap->Filter("");
			
			// InsertDimension
			$theCubeRolap->InsertDimension('['.$theCube->a_dimensions[$this->consecutivo]->physical_table.'].['.$theCube->a_dimensions[$this->consecutivo]->physical_name_descrip.']');
			
			// RowSet
			//print '<BR>QueryText()<BR>==================================<BR>'.$theCubeRolap->QueryText().'<BR>';
			
			$aRS = $theCubeRolap->RowSet($theCube->connection->ADOConnection);
			
			//use_agregation se restaura a su valor original
			$theCubeRolap->use_agregation = $aux_use_agregation;
			
			$theCubeRolap->brange_date=$aux_brange_date;
			
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\n".date('Y-M-d H:i:s');
			$aLogString.="\r\nQuery to get Values Of the Dimension ".$this->consecutivo.":";
			$aLogString.="\r\n";
			$aLogString.=str_replace('<BR>'," ",$theCubeRolap->QueryText());
			global $queriesLogFile;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			
			$this->getFilteredDimensionValues($aRS, $bGetMySubrrogateID, $bGetMyParentsID);
			
			BITAMGlobalInstance::AddDimensionKeysWithFilter($this->cla_concepto,$this->consecutivo,'',$this->valuesID);
			BITAMGlobalInstance::AddDimensionValuesWithFilter($this->cla_concepto,$this->consecutivo,'',$this->values);
			//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
			//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
			BITAMGlobalInstance::AddDimensionSubrrKeysWithFilter($this->cla_concepto,$this->consecutivo,'',$this->subrrogateID);
			BITAMGlobalInstance::AddDimensionParentsFldsWithFilter($this->cla_concepto,$this->consecutivo,'',$this->parentJoinsIDs);
			//@JAPR
		}
	}
	
	//@JAPR 2012-03-12: Agregado el parámetro $sGroupBy para permitir aplicar funciones de agrupación en los campos (utilizado originalmente
	//para obtener los valores distintos junto a la llave Subrrogada de una dimensión - atributo, forzando manualmente a cambiar el nom_fisicok
	//para que apuntara a la subrrogada, sólo que se necesitaba agrupar por la descripción, se hizo en este método ya que nadie lo utilizaba
	//y realmente no varía tanto la funcionalida del mismo con este cambio)
	function getDimensionValuesExt($CompanyFilter = "", $sGroupBy = "")
	{
		//@JAPR 2010-03-22: Validado que si el campo llave y descriptor sean iguales, el ORDER BY sea por posición
		$bSameKeyDescField = strtolower($this->nom_fisico) == (strtolower($this->nom_fisicok));
		$sql = "SELECT DISTINCT ".$this->nom_fisicok_bd." , ".$this->nom_fisico_bd.
			" FROM ".$this->nom_tabla.(strlen($CompanyFilter) == 0 ? "" : $CompanyFilter).
			($sGroupBy != ''?" GROUP BY ".$sGroupBy:"").
		    " ORDER BY ".(($bSameKeyDescField)?'2':$this->nom_fisico_bd);
		//@JAPR
		global $queriesLogFile;
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMDimension->GetDimensionValuesExt: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error")." ".translate("in")." getDimensionValues() ".translate("Function").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		$count=0;
		while (!$aRS->EOF)
		{
   		    if(in_array($aRS->fields[$this->nom_fisicok],$this->valuesID)===false)
 		    {
				$this->valuesID[$count]=$aRS->fields[$this->nom_fisicok];
				$this->values[$aRS->fields[$this->nom_fisicok]] = $aRS->fields[$this->nom_fisico];
	 		    $count++;
 		    }
 		    $aRS->MoveNext();
		}
		$this->numValues=$count;
	}
	
	static function NewInstanceByPeriodID($aRepository, $aCubeID, $aPeriodID, $aGetValues = true)
	{
		$theInfoPeriodDim=null;
		$withValues=($aGetValues==true)?'true':'false';
		$theInfoPeriodDim =& BITAMGlobalInstance::GetPeriodDimension($aCubeID, $aPeriodID, $withValues);
		if(!is_null($theInfoPeriodDim))
		{
			return $theInfoPeriodDim;
		}
		
		$anInstance=null;
		
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		$sql = "SELECT A.CLA_CONCEPTO, A.CONSECUTIVO, A.FMT_PERIODO, A.CLA_PERIODO,B.NOM_LOGICO,B.NOM_FISICO, B.TIPO_DATO, B.LONG_DATO, B.NOM_FISICOK, B.TIPO_DATOK, B.LONG_DATOK, B.NOM_TABLA, A.PAR_DESCRIPTOR, B.DEFAULT_KEY, A.CLA_DESCRIP, B.CLA_CONEXION, B.CREATED_BY, B.IS_CATALOG ".
			"FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B ".
			"WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.CLA_CONCEPTO = ".$aCubeID." AND CLA_PERIODO=".$aPeriodID ." ORDER BY A.CONSECUTIVO";
	
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE or SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$anInstance = BITAMDimension::NewInstanceFromRS($aRepository, $aRS, $aGetValues);
		}
		
		BITAMGlobalInstance::AddPeriodDimension($aCubeID, $aPeriodID, $withValues, $anInstance);

		return $anInstance;
	}
	
	static function getDimensionIDsFromCubeIDDimID($aCubeIDDimIDArray)	
	{
		$dimAux = $aCubeIDDimIDArray;
		$sizeDimensionIDs=count($dimAux);

		$dimensionIDs=array();
		if ($sizeDimensionIDs>0)
		{
			for ($i=0; $i<$sizeDimensionIDs; $i++)
			{
				$cubeIDAux = (int) ($dimAux[$i]/ 1000);
				$dimensionIDs[$i]=$dimAux[$i] - ($cubeIDAux * 1000);
			}

			sort($dimensionIDs);
		}
		
		return $dimensionIDs;
	}
	
	//getSecurityFilter -> setValuesBySecurityFilter
	function setValuesBySecurityFilter($UserFilterCollection, $HierarchyFilterCollection, $SelectedValuesFilterCollection)
	{
		$ResultFilterCollection = BITAMFilteredDimensionCollection::NewInstance($this->Repository);
		
		$abortProcess = false;
		
		if( is_null($HierarchyFilterCollection) || count($HierarchyFilterCollection->Collection)==0 )
		{
			if(is_null($UserFilterCollection)==false && count($UserFilterCollection->Collection)>0)
			{
			
				$sql = "SELECT B.DIM_PADRE FROM SI_JERARQUIA A, SI_JERARQUIA B 
						WHERE A.DIM_PADRE = ".$this->consecutivo." AND A.CLA_CONCEPTO = ".$this->cla_concepto." 
						AND A.CLA_CONCEPTO = B.CLA_CONCEPTO 
						AND A.CLA_JERARQUIA = B.CLA_JERARQUIA
						AND B.CONSECUTIVO <= A.CONSECUTIVO
						ORDER BY B.CONSECUTIVO DESC";
				
				
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}			
				
				$numElements = 0;
				
				if($aRS->EOF)
				{
					$consecutivo = $this->consecutivo;
					
					for($i=0; $i<count($UserFilterCollection->Collection); $i++)
					{
						if( $consecutivo == $UserFilterCollection->Collection[$i]->consecutivo )
						{
							$ResultFilterCollection->Collection[$numElements] = clone( $UserFilterCollection->Collection[$i] );
							$numElements = $numElements + 1;
							break;
						}
					}
				}
				else 
				{
				
					while(!$aRS->EOF)
					{
						$dimensionPadre = (int)$aRS->fields["dim_padre"];
						
						for($i=0; $i<count($UserFilterCollection->Collection); $i++)
						{
							if( $dimensionPadre == $UserFilterCollection->Collection[$i]->consecutivo )
							{
								$ResultFilterCollection->Collection[$numElements] = clone( $UserFilterCollection->Collection[$i] );
								$numElements = $numElements + 1;
								break;
							}
						}
						
						$aRS->MoveNext();
					}
				}
			}
		}
		else 
		{
			if($this->isValidFilterCollection($HierarchyFilterCollection)==true)
			{
			
				$hierarchyDimensionsIDs = array();
				
				$strHierarchyDimensionsIDsWithComma = "";
				
				$numElements = 0;
	
				for($i=0; $i<count($HierarchyFilterCollection->Collection); $i++)
				{
					if($strHierarchyDimensionsIDsWithComma!="")
					{
						$strHierarchyDimensionsIDsWithComma.=", ";
					}
					
					$ResultFilterCollection->Collection[$numElements] = clone ( $HierarchyFilterCollection->Collection[$i] );
							
					$strHierarchyDimensionsIDsWithComma.= $HierarchyFilterCollection->Collection[$i]->consecutivo;
					
					$numElements = $numElements + 1;
				}
	
				if(is_null($UserFilterCollection)==false && count($UserFilterCollection->Collection)>0)
				{
				
					$sql = "SELECT B.DIM_PADRE FROM SI_JERARQUIA A, SI_JERARQUIA B 
							WHERE A.DIM_PADRE = ".$this->consecutivo." AND A.CLA_CONCEPTO = ".$this->cla_concepto." 
							AND A.CLA_CONCEPTO = B.CLA_CONCEPTO 
							AND A.CLA_JERARQUIA = B.CLA_JERARQUIA
							AND B.DIM_PADRE NOT IN (".$strHierarchyDimensionsIDsWithComma.")
							AND B.CONSECUTIVO <= A.CONSECUTIVO
							ORDER BY B.CONSECUTIVO DESC";
					
					
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}			
					
					while(!$aRS->EOF)
					{
						$dimensionPadre = (int)$aRS->fields["dim_padre"];
						
						for($i=0; $i<count($UserFilterCollection->Collection); $i++)
						{
							if( $dimensionPadre == $UserFilterCollection->Collection[$i]->consecutivo )
							{
								$ResultFilterCollection->Collection[$numElements] = clone( $UserFilterCollection->Collection[$i] );
								
								$numElements = $numElements + 1;
							}
						}
						
						$aRS->MoveNext();
					}
				}
			}
			else 
			{
				$this->valuesID = array();
				$this->values = array();
				//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
				//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
				$this->subrrogateID = array();
				$this->parentJoinsIDs = array();
				//@JAPR
				$this->numValues = 0;
				
				$abortProcess = true;
			}		
		}
		
		if($abortProcess==false)
		{
		
			$emptyIntersection = false;
			
			//La coleccion ResultFilterCollection sino esta vacía o nula entonces se procede
			//a realizar una intersección con los valores establecidos en el filtro personalizado
			//o sea en el $SelectedValuesFilterCollection
			if( is_null($ResultFilterCollection) == false && count($ResultFilterCollection->Collection)>0)
			{
				if($SelectedValuesFilterCollection!==null&&count($SelectedValuesFilterCollection->Collection)>0)
				{
					$emptyIntersection = $this->intersectionBetweenFilterCollections($ResultFilterCollection, $SelectedValuesFilterCollection);
					
					$this->copySelectedFilterCollectionToHierarchy($ResultFilterCollection, $SelectedValuesFilterCollection);
					
					$arrayConsecutivos = $ResultFilterCollection->GetArrayConsecutivos();
					
					if(in_array($this->consecutivo,$arrayConsecutivos)===false)
					{
						$this->copyFilterCollection($ResultFilterCollection, $SelectedValuesFilterCollection);
					}
				}
			}
			else 
			{
				//En este else se aplica cuando ResultFilterCollection si se encuentra vacío entonces se procede a copiar
				//la coleccion $SelectedValuesFilterCollection
				if($SelectedValuesFilterCollection!==null&&count($SelectedValuesFilterCollection->Collection)>0)
				{
					$this->copySelectedFilterCollectionToHierarchy($ResultFilterCollection, $SelectedValuesFilterCollection);
					$this->copyFilterCollection($ResultFilterCollection, $SelectedValuesFilterCollection);
				}
			}
	
			if($emptyIntersection==false)
			{
				$ResultFilterCollection->GenerateStrFilterByDimensionCollection();
				$strFilter=$ResultFilterCollection->Filter;
				$arrKeys=null;
				$arrKeys =& BITAMGlobalInstance::GetDimensionKeysWithFilter($this->cla_concepto,$this->consecutivo,$strFilter);
				if(!is_null($arrKeys))
				{
					$this->valuesID =& BITAMGlobalInstance::GetDimensionKeysWithFilter($this->cla_concepto,$this->consecutivo,$strFilter);
					$this->values =& BITAMGlobalInstance::GetDimensionValuesWithFilter($this->cla_concepto,$this->consecutivo,$strFilter);
					//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
					//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
					$this->subrrogateID =& BITAMGlobalInstance::GetDimensionSubrrKeysWithFilter($this->cla_concepto,$this->consecutivo,$strFilter);
					$this->parentJoinsIDs =& BITAMGlobalInstance::GetDimensionParentsFldsWithFilter($this->cla_concepto,$this->consecutivo,$strFilter);
					//@JAPR
					$this->numValues=count($arrKeys);
					return;
				}

				$aRSDimensionValues = $this->getRSDimensionValuesWithArtusMetaData($this->Repository, $ResultFilterCollection);
				
				$this->getFilteredDimensionValues($aRSDimensionValues);

				BITAMGlobalInstance::AddDimensionKeysWithFilter($this->cla_concepto,$this->consecutivo,$strFilter,$this->valuesID);
				BITAMGlobalInstance::AddDimensionValuesWithFilter($this->cla_concepto,$this->consecutivo,$strFilter,$this->values);
				//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
				//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
				BITAMGlobalInstance::AddDimensionSubrrKeysWithFilter($this->cla_concepto,$this->consecutivo,$strFilter,$this->subrrogateID);
				BITAMGlobalInstance::AddDimensionParentsFldsWithFilter($this->cla_concepto,$this->consecutivo,$strFilter,$this->parentJoinsIDs);
				//@JAPR
			}
			else 
			{
				$this->valuesID = array();
				$this->values = array();
				//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
				//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
				$this->subrrogateID = array();
				$this->parentJoinsIDs = array();
				//@JAPR
				$this->numValues = 0;
			}
		}
		
	}
	
	function isValidFilterCollection(&$FilterCollection)
	{
		$isValid = true;
		
		for($i=0; $i<count($FilterCollection->Collection); $i++)
		{
			if($FilterCollection->Collection[$i]->values[0]===null)
			{
				$isValid = false;
				break;
			}
		}
		
		return $isValid;
	}
	
	function copySelectedFilterCollectionToHierarchy(&$ResultFilterCollection, &$SelectedValuesFilterCollection)
	{
		$arraySelectedConsecutivos = $SelectedValuesFilterCollection->GetArrayConsecutivos();
		
		$arrayResultConsecutivos = $ResultFilterCollection->GetArrayConsecutivos();
		
		$arrayTopHierarchy = $this->HierarchyDimIDs;
		
		foreach ($arrayTopHierarchy as $anElement)
		{
			if( in_array($anElement, $arrayResultConsecutivos)===false )
			{
				$position = array_search($anElement, $arraySelectedConsecutivos);
				
				if($position!==false)
				{
					$newPosition = count($ResultFilterCollection->Collection);
					$ResultFilterCollection->Collection[$newPosition] = clone($SelectedValuesFilterCollection->Collection[$position]);
				}
			}
		}
	}
	
	function copyFilterCollection(&$ResultFilterCollection, &$SelectedValuesFilterCollection)
	{
		if(is_null($ResultFilterCollection)==true)
		{
			$numElements = 0;
		}
		else 
		{
			$numElements = count($ResultFilterCollection->Collection);
		}
		
		$tempDimensionID = $this->consecutivo;
		
		for($j=0; $j<count($SelectedValuesFilterCollection->Collection); $j++)
		{
			if($tempDimensionID == $SelectedValuesFilterCollection->Collection[$j]->consecutivo)
			{
				//$selectedDimensionValues = $SelectedValuesFilterCollection->Collection[$j]->values;
				//$ResultFilterCollection->Collection[$numElements]->values = $selectedDimensionValues;
				$ResultFilterCollection->Collection[$numElements] = clone( $SelectedValuesFilterCollection->Collection[$j] );
				$numElements = $numElements + 1;
				break;		
			}
		}
	}

	
	function intersectionBetweenFilterCollections ( &$ResultFilterCollection, &$SelectedValuesFilterCollection )
	{
		$emptyIntersection=false;
		
		for($i=0; $i<count($ResultFilterCollection->Collection); $i++)
		{
			$selectedDimensionValues = array();
			$resultDimensionValues = array();
			$isFound = false;

			$tempDimensionID = $ResultFilterCollection->Collection[$i]->consecutivo;
			$valueExcept = false;
			//echo('<BR>intersectionBetweenFilterCollections='.$SelectedValuesFilterCollection->Collection[0]->consecutivo.'<BR>');
			for($j=0; $j<count($SelectedValuesFilterCollection->Collection); $j++)
			{
				if($tempDimensionID == $SelectedValuesFilterCollection->Collection[$j]->consecutivo)
				{
					$selectedDimensionValues = $SelectedValuesFilterCollection->Collection[$j]->values;
					$resultDimensionValues = $ResultFilterCollection->Collection[$i]->values;
					$valueExcept = $SelectedValuesFilterCollection->Collection[$j]->except;
					$isFound = true;
					
					break;		
				}
			}
			
			if($isFound==true)
			{
				if($valueExcept==false)
				{
					$valueBoolean = true;
				}
				else 
				{
					$valueBoolean = false;
				}
				
				$tempDimensionValues = array();
				
				$numElements = 0;
				
				for($m=0; $m<count($resultDimensionValues); $m++)
				{
						if(in_array($resultDimensionValues[$m],$selectedDimensionValues)==$valueBoolean)
						{
							$tempDimensionValues[$numElements] = $resultDimensionValues[$m];
							
							$numElements = $numElements + 1;
						}
				}
				
				$ResultFilterCollection->Collection[$i]->values = array();
				
				$ResultFilterCollection->Collection[$i]->values = $tempDimensionValues;
				
				if(count($tempDimensionValues)==0)
				{
					$emptyIntersection=true;
				}
			}
		}
		
		return $emptyIntersection;
	}
	
	static function getIntersectionBetweenFilterCollections ( &$ResultFilterCollection, &$SelectedValuesFilterCollection,$addDimIDs=array())
	{
		$emptyIntersection=false;
		
		//IDs de dimensiones incluídas en el filtro del presupueto
		$tempFilterDimensions=array();
		
		for($i=0; $i<count($ResultFilterCollection->Collection); $i++)
		{
			$selectedDimensionValues = array();
			$resultDimensionValues = array();
			$isFound = false;

			$tempDimensionID = $ResultFilterCollection->Collection[$i]->consecutivo;
			//IDs de dimensiones incluídas en el filtro del presupueto
			$tempFilterDimensions[]=$tempDimensionID;
			//echo('<BR>intersectionBetweenFilterCollections='.$SelectedValuesFilterCollection->Collection[0]->consecutivo.'<BR>');
			for($j=0; $j<count($SelectedValuesFilterCollection->Collection); $j++)
			{
				if($tempDimensionID == $SelectedValuesFilterCollection->Collection[$j]->consecutivo)
				{
					$selectedDimensionValues = $SelectedValuesFilterCollection->Collection[$j]->values;
					$resultDimensionValues = $ResultFilterCollection->Collection[$i]->values;
					$isFound = true;
					$idxResultFilter = $i;
					$idxSelectedFilter = $j;
					break;		
				}
			}
			
			if($isFound==true)
			{
				if($ResultFilterCollection->Collection[$idxResultFilter]->except==false && $SelectedValuesFilterCollection->Collection[$idxSelectedFilter]->except==false)
				{
					$tempDimensionValues = array();
					
					$numElements = 0;
					
					for($m=0; $m<count($resultDimensionValues); $m++)
					{
						if(in_array($resultDimensionValues[$m],$selectedDimensionValues)==true)
						{
							$tempDimensionValues[$numElements] = $resultDimensionValues[$m];
							
							$numElements = $numElements + 1;
						}
					}
					
					$ResultFilterCollection->Collection[$i]->values = array();
					$ResultFilterCollection->Collection[$i]->values = $tempDimensionValues;
					$ResultFilterCollection->Collection[$i]->except = false;
				}
				else if($ResultFilterCollection->Collection[$idxResultFilter]->except==true && $SelectedValuesFilterCollection->Collection[$idxSelectedFilter]->except==true)
				{
					$tempDimensionValues = array();
					
					$firstArray = $ResultFilterCollection->Collection[$idxResultFilter]->values;
					$secondArray = $SelectedValuesFilterCollection->Collection[$idxSelectedFilter]->values;
						
					$tempDimensionValues = array_values(array_unique(array_merge($firstArray, $secondArray)));

					$ResultFilterCollection->Collection[$i]->values = array();
					$ResultFilterCollection->Collection[$i]->values = $tempDimensionValues;
					$ResultFilterCollection->Collection[$i]->except = true;
				}
				else 
				{
					if($ResultFilterCollection->Collection[$idxResultFilter]->except==false && $SelectedValuesFilterCollection->Collection[$idxSelectedFilter]->except==true)
					{
						$firstArray = $ResultFilterCollection->Collection[$idxResultFilter]->values;
						$secondArray = $SelectedValuesFilterCollection->Collection[$idxSelectedFilter]->values;
					}
					else 
					{
						$firstArray = $SelectedValuesFilterCollection->Collection[$idxSelectedFilter]->values;
						$secondArray = $ResultFilterCollection->Collection[$idxResultFilter]->values;
					}
					
					$tempDimensionValues = array();
					
					$numElements = 0;
					
					for($m=0; $m<count($firstArray); $m++)
					{
						if(in_array($firstArray[$m],$secondArray)==false)
						{
							$tempDimensionValues[$numElements] = $firstArray[$m];
							
							$numElements = $numElements + 1;
						}
					}
					
					$ResultFilterCollection->Collection[$i]->values = array();
					$ResultFilterCollection->Collection[$i]->values = $tempDimensionValues;
					$ResultFilterCollection->Collection[$i]->except = false;
				}

				if(count($tempDimensionValues)==0)
				{
					$emptyIntersection=true;
					
					$aLogString="\r\n--------------------------------------------------------------------------------------";
					$aLogString.="\r\n".date('Y-M-d H:i:s');
					$aLogString.="\r\n".translate("Error: The user does not have permission to consult the values selected for the dimension ").$ResultFilterCollection->Collection[$i]->consecutivo;
					global $queriesLogFile;
					@updateQueriesLogFile($queriesLogFile,$aLogString);
?>
					<SCRIPT language="JavaScript">
					alert ('<?=translate("Error: The user does not have permission to consult the values selected for the dimension ").$ResultFilterCollection->Collection[$i]->consecutivo?>');
					</SCRIPT>
<?
					die("(".__METHOD__.") ".translate("Error: The user does not have permission to consult the values selected for the dimension ").$ResultFilterCollection->Collection[$i]->consecutivo);

				}
			}
		}
		
		for($i=0;$i<count($SelectedValuesFilterCollection->Collection);$i++)
		{
			$consecutivo=$SelectedValuesFilterCollection->Collection[$i]->consecutivo;
			if(in_array($consecutivo,$addDimIDs) && !in_array($consecutivo,$tempFilterDimensions))
			{
				$ResultFilterCollection->Collection[]=$SelectedValuesFilterCollection->Collection[$i];
			}
		}
		
		return $emptyIntersection;
	}
	
	static function getFilterIntersection($aRepository, $strFilter1,$strFilter2,$dimIDs)
	{	
		require_once("filter.inc.php");
		
		$filterCollection1 = null;
		$filterCollection2 = null;
		$resultFilterCollection=null;
		$resultFilterArray=array();
		$resultFilterArray["filter"]="";
		$resultFilterArray["levels"]="";
		
		//Obtener la colección de dimensiones filtradas correspondientes a strFilter1
		if($strFilter1!=null && trim($strFilter1)!="")
		{
			$arrayFilterInfo1=BITAMDimension::getInfoFromStrFilter($strFilter1);
			
			$strDimFilter1=$arrayFilterInfo1["filter"];
			$strLevels1=$arrayFilterInfo1["levels"];
			$anIndicatorID=$arrayFilterInfo1["indicatorID"];
			$aCubeID=BITAMIndicator::getCubeIDByIndID($aRepository, $anIndicatorID);
			
			if($strDimFilter1 !== false)
			{
				$filterCollection1=BITAMFilteredDimensionCollection::NewInstanceByStrFilter($aRepository, $strDimFilter1, $strLevels1, $aCubeID);
			}
		}
		else 
		{
			$filterCollection1 = null;
		}
		
		//Obtener la colección de dimensiones filtradas correspondientes a strFilter2
		if($strFilter2!=null && trim($strFilter2)!="")
		{
			$arrayFilterInfo2=BITAMDimension::getInfoFromStrFilter($strFilter2);
			
			$strDimFilter2=$arrayFilterInfo2["filter"];
			$strLevels2=$arrayFilterInfo2["levels"];
			$anIndicatorID=$arrayFilterInfo2["indicatorID"];
			$aCubeID=BITAMIndicator::getCubeIDByIndID($aRepository, $anIndicatorID);
			
			if($strDimFilter2 !== false)
			{
				$filterCollection2=BITAMFilteredDimensionCollection::NewInstanceByStrFilter($aRepository, $strDimFilter2, $strLevels2, $aCubeID);
			}
		}
		else 
		{
			$filterCollection2 = null;
		}
				
		//Obtener $resultFilterCollection y $resultFilterArray
		if( $filterCollection1!==null && count($filterCollection1->Collection)>0 && $filterCollection2!==null && count($filterCollection2->Collection)>0)
		{
			$emptyIntersection = BITAMDimension::getIntersectionBetweenFilterCollections($filterCollection1,$filterCollection2,$dimIDs);
			$resultFilterCollection=$filterCollection1;
		}
		else
		{
			if($filterCollection1!==null && count($filterCollection1->Collection)>0)
			{
				$resultFilterCollection=$filterCollection1;
			}
			else
			{
				//Considerar sólo las dim incluídas en dimIDs
				//A partir de una colección de dim filtradas retorna otra colección que incluya sólo las de $includedDims
				if($filterCollection2!==null && count($filterCollection2->Collection)>0)
				{
					$resultFilterCollection=$filterCollection2->getPartialFilterCollection($dimIDs);
				}
			}
		}
		
		if($resultFilterCollection!==null)
		{
			$resultFilterCollection->GenerateStrFilterByDimensionCollection();
			$resultFilterArray["filter"]=$resultFilterCollection->Filter;
			$resultFilterArray["levels"]=$resultFilterCollection->Levels;
		}
		
		return $resultFilterArray;
	}
	
	
	static function getFilterIntersectionByCollection($aRepository, $filterCollection1, $filterCollection2, $dimIDs)
	{	
		require_once("filter.inc.php");
		
		$resultFilterCollection=null;
		$resultFilterArray=array();
		$resultFilterArray["filter"]="";
		$resultFilterArray["levels"]="";
		
		//Obtener $resultFilterCollection y $resultFilterArray
		if( $filterCollection1!==null && count($filterCollection1->Collection)>0 && $filterCollection2!==null && count($filterCollection2->Collection)>0)
		{
			$emptyIntersection = BITAMDimension::getIntersectionBetweenFilterCollections($filterCollection1,$filterCollection2,$dimIDs);
			$resultFilterCollection=$filterCollection1;
		}
		else
		{
			if($filterCollection1!==null && count($filterCollection1->Collection)>0)
			{
				$resultFilterCollection=$filterCollection1;
			}
			else
			{
				//Considerar sólo las dim incluídas en dimIDs
				//A partir de una colección de dim filtradas retorna otra colección que incluya sólo las de $includedDims
				if($filterCollection2!==null && count($filterCollection2->Collection)>0)
				{
					$resultFilterCollection=$filterCollection2->getPartialFilterCollection($dimIDs);
				}
			}
		}
		
		if($resultFilterCollection!==null)
		{
			$resultFilterCollection->GenerateStrFilterByDimensionCollection();
			$resultFilterArray["filter"]=$resultFilterCollection->Filter;
			$resultFilterArray["levels"]=$resultFilterCollection->Levels;
		}
		
		return $resultFilterArray;
	}
	
	static function getInfoFromStrFilter($strFilter)
	{
		$arrayInfo=array();
		$arrayInfo["filter"]="";
		$arrayInfo["levels"]="";
		$arrayInfo["indicadorID"]=null;
		
		if(trim($strFilter)!="")
		{
			$count=0;
			
			$elements = explode ("|", $strFilter); 
			
			$anIndicatorID=(int)$elements[0];
			
			$arrayTempFilteredDimensionIDs = explode (",", $elements[2]);
			
			$selectedFilteredDimensionIDs=array();
			
			for ($i=0;$i<count($arrayTempFilteredDimensionIDs);$i++)
			{
				if ($arrayTempFilteredDimensionIDs[$i]>0)
				{
					$selectedFilteredDimensionIDs[$count]=$arrayTempFilteredDimensionIDs[$i];
					$count++;
				}
			}
			
			$numSelectedFilteredDimensions=$count;
		
			$strSelectedFilteredDimLevels = "";
			
			for ($i=0;$i<$numSelectedFilteredDimensions;$i++)
			{
				$strSelectedFilteredDimLevels .= "|".$selectedFilteredDimensionIDs[$i];
			}
			
			$strSelectedFilterDim = strstr($elements[3],'[');

			$arrayInfo["filter"]=$strSelectedFilterDim;
			$arrayInfo["levels"]=$strSelectedFilteredDimLevels;
			$arrayInfo["indicatorID"]=$anIndicatorID;
		}
		
		return $arrayInfo;
	}
	
	//@JAPR 2010-07-20: Agregados los parámetros $bGetMySubrrogateID y $bGetMyParentsID para cargar las nuevas colecciones de valores de la 
	//llave Subrrogada y de los padres (subrrogados si es el caso) de la dimensión que está siendo instanciada con este método
	function getFilteredDimensionValues($aRS, $bGetMySubrrogateID = false, $bGetMyParentsID = false)
	{
		if ($aRS === false || $aRS === null)
		{
			die("(".__METHOD__.") ".translate("Error")." ".translate("in")." getRSDimensionValuesWithArtusMetaData() ".translate("Function"));
		}
		else 
		{
			$this->valuesID = array();
			$this->values = array();
			//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
			//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
			$this->subrrogateID = array();
			$this->parentJoinsIDs = array();
			//@JAPR
			$this->numValues = 0;
		}
		
		$arrayNoApplyDesc = array();
		
		$sqlNoApplyDesc = "SELECT REF_CONFIGURA FROM SI_CONFIGURA WHERE CLA_CONFIGURA = 534";
		
		$aRSNoApplyDesc = $this->Repository->ADOConnection->Execute($sqlNoApplyDesc);
		
		if ($aRSNoApplyDesc === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CONFIGURA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlNoApplyDesc);
		}
		
		if (!$aRSNoApplyDesc->EOF)
		{
			$strNoApplyDesc = ''.$aRSNoApplyDesc->fields['REF_CONFIGURA'].'';
			$arrayNoApplyDesc[$strNoApplyDesc] = 1;
		}
		else 
		{
			$arrayNoApplyDesc['*ND'] = 1;
			$arrayNoApplyDesc['*NA'] = 1;
		}
		
		$count=0;
		
		//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
		//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
		$blnGetParentValues = false;
		$strParentField = '';
		$arrParents = array();
		$featuresDimension = BITAMDimension::getFeaturesDimension($this->Repository, $this->consecutivo, $this->cla_concepto);
		if (!is_null($featuresDimension) && array_key_exists('parents', $featuresDimension))
		{
			foreach ($featuresDimension['parents'] as $aParentConsecutive => $aParentInfo)
			{
				$strParentFieldName = $aParentInfo["parent_nom_fisicok_join"];
				if (trim($strParentFieldName) != '')
				{
					$strParentField .= ','.$strParentFieldName;
				}
			}
			if (trim($strParentField) != '')
			{
				$arrParents = explode(',', substr($strParentField, 1));
			}
			else 
			{
				$bGetMyParentsID = false;
			}
			
			//Sólo si la cantidad de registros retornada es exactamente la cantidad de padres + el Id + la Descripción y opcionalmente la Subrrogada,
			//pues en ese caso si se puede hacer la correspondencia del nombre de campo del padre con sus valores
			if (count($arrParents) == (count($aRS->fields) -2 - (($bGetMySubrrogateID)?1:0)))
			{
				$blnGetParentValues = $bGetMyParentsID;
			}
		}
		//@JAPR
		
		//Si sólo existe un miembro de dimensión y éste es No Aplica entonces 
		//Sí se despliega con descripción vacía para que el combo tenga al menos un elemento
		if($aRS->RecordCount()==1)
		{
			//if(strtoupper(trim($aRS->fields[1]))=="*ND")
			//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
			//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
			if(isset($arrayNoApplyDesc[trim($aRS->fields[1])]))
			{
				$this->valuesID[$count]=$aRS->fields[0];
				$this->values[$aRS->fields[0]] = "";
				$this->subrrogateID[$aRS->fields[0]] = "";
				$this->parentJoinsIDs[$aRS->fields[0]] = array();
	 		    $count++;
			}
			else
			{	
	 		    $this->valuesID[$count]=$aRS->fields[0];
				$this->values[$aRS->fields[0]] = $aRS->fields[1];
				if ($bGetMySubrrogateID)
				{
					$this->subrrogateID[$aRS->fields[0]] = $aRS->fields[count($aRS->fields) -1];
				}
				if ($blnGetParentValues)
				{
					$this->parentJoinsIDs[$aRS->fields[0]] = array();
					for ($intIndex = 2, $intFieldsCount = (count($aRS->fields) - (($bGetMySubrrogateID)?1:0)); $intIndex < $intFieldsCount; $intIndex++)
					{
						$this->parentJoinsIDs[$aRS->fields[0]][$arrParents[$intIndex -2]] = @$aRS->fields[$intIndex];
					}
				}
	 		    $count++;
			}
			//@JAPR
		}
		else
		{	
			while (!$aRS->EOF)
			{
				//if(strtoupper(trim($aRS->fields[1]))!=="*ND" && strtoupper(trim($aRS->fields[1]))!=="*NA")
				if(!isset($arrayNoApplyDesc[trim($aRS->fields[1])]))
				{
		 		    $this->valuesID[$count]=$aRS->fields[0];
					$this->values[$aRS->fields[0]] = $aRS->fields[1];
					if ($bGetMySubrrogateID)
					{
						$this->subrrogateID[$aRS->fields[0]] = $aRS->fields[count($aRS->fields) -1];
					}
					if ($blnGetParentValues)
					{
						$this->parentJoinsIDs[$aRS->fields[0]] = array();
						for ($intIndex = 2, $intFieldsCount = count($aRS->fields); $intIndex < $intFieldsCount; $intIndex++)
						{
							$this->parentJoinsIDs[$aRS->fields[0]][$intIndex -2] = @$aRS->fields[$intIndex];
						}
					}
		 		    $count++;
				}
	 		    $aRS->MoveNext();
			}
		}
		
		$this->numValues=$count;
	}
	
	function getRSDimensionValuesWithArtusMetaData($theRepository, $ResultFilterCollection)
	{
		// Conseguimos una instancia de tipo Connection
															//server, userDB, passwordDB, DB;
		//$theRepository = BITAMConnection::NewInstance('iris-sql', 'sa', 'bitam', 'DemoEktos03');
		if (!is_null($theRepository))
		{
			// Obtenemos la informacion del cubo		
			//@JAPR 2010-07-19: Agregado el parámetro $bCreateQuery para indicar que la instancia de rolap sea generada automáticamente
			$theCube = BITAMCInfoCube::NewInstance($theRepository, $this->cla_concepto, false, true);
	
			// Estamos listos para usar la funcionalidad de ROLAP
			$theCubeRolap = $theCube->rolap;
	
			// Initialize
			$theCubeRolap->Initialize();
			
			//Se establece temporalmente que el cubo no use agregaciones para asegurar que las combinaciones de dimensiones se obtengan de la Tabla de Hechos
			$aux_use_agregation = $theCubeRolap->use_agregation;
			$theCubeRolap->use_agregation = 0;
			
			$sql="SELECT PeriodID, DimensionMembersFrom, StartDateDim, EndDateDim, OrderByKey FROM SI_EKT_BudgetIndicator WHERE BudgetID=".$_SESSION["PABudgetID"];
			$aRS = $theRepository->ADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_EKT_BudgetIndicator ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if (!$aRS->EOF)
			{
				$dimensionMembersFrom = $aRS->fields["dimensionmembersfrom"];
				$startDateDim = $aRS->fields["startdatedim"];
				$endDateDim = $aRS->fields["enddatedim"];
				$periodID = $aRS->fields["periodid"];
				$orderByKey = $aRS->fields["orderbykey"];
			}
			
			//Si $dimensionMembersFrom=0 se obtienen las combinaciones del catálogo
			//Si $dimensionMembersFrom=1 se obtienen las combinaciones de la tabla de hechos
			//Si $dimensionMembersFrom=null se obtienen las combinaciones de la tabla de hechos
			
			if(is_null($dimensionMembersFrom))
			{
				$querySinTablaDeHechos=false;
			}
			else
			{
				if($dimensionMembersFrom==0)
				{
					$querySinTablaDeHechos=true;
				}
				else
				{
					$querySinTablaDeHechos=false;
				}
			}
			
			if($orderByKey==0)
			{
				$dimensionsOrderByKey = false;
			}
			else 
			{
				$dimensionsOrderByKey = true;
			}
			
			$aux_brange_date=$theCubeRolap->brange_date;
			if($querySinTablaDeHechos==false)
			{	
				require_once("artusmetadata.php");
				
				if($startDateDim!=="" && $startDateDim!==null && $endDateDim!=="" && $endDateDim!==null)
				{
					$accumulated=true;
					$rangeDate = getRange($theRepository, $startDateDim, $endDateDim, $periodID);
					$theCubeRolap->InsertPeriod($periodID, $startDateDim, $endDateDim, true, $accumulated, $rangeDate, '', false, '' );
				}
				else 
				{
					$accumulated=true;
					$rangeDate = "";
					$theCubeRolap->brange_date=true;
					//$rangeDate = getRange($theRepository, '2000-01-01', '2010-12-31', $periodID);
					$rangeDate="";
					$periodStartDate=$_SESSION["PAperiodStartDate"];
					//echo('<BR>$periodStartDate='.$periodStartDate);
					$dateElements=explode('-',$periodStartDate);
					$yearDate=$dateElements[0];
					//echo('<BR>$yearDate='.$yearDate);
					$endDateAux=($yearDate+10)."-12-31";
					//echo('<BR>$endDateAux='.$endDateAux);
					$theCubeRolap->InsertPeriod($periodID, '1950-01-01', $endDateAux, true, $accumulated, $rangeDate, '', false, '' );
				}
			}
			
			$theCubeRolap->QuerySinTablaDeHechos = $querySinTablaDeHechos;
			$theCubeRolap->borderbykey = $dimensionsOrderByKey;
	
			// Filter
			$theCubeRolap->Filter($ResultFilterCollection->Filter);
			
			//echo('<BR>$ResultFilterCollection->Filter='.$ResultFilterCollection->Filter.'<BR>');
	
			// InsertDimension
			$numDimensionIDs = $ResultFilterCollection->NumFilteredDimensions;
			
			$theCubeRolap->InsertDimension('['.$theCube->a_dimensions[$this->consecutivo]->physical_table.'].['.	$theCube->a_dimensions[$this->consecutivo]->physical_name_descrip.']');
			
			
			// RowSet
			//print '<BR>QueryText()<BR>==================================<BR>'.$theCubeRolap->QueryText().'<BR>';
			
			$aRS = $theCubeRolap->RowSet($theCube->connection->ADOConnection);
			
			//use_agregation se restaura a su valor original
			$theCubeRolap->use_agregation = $aux_use_agregation;
			
			$theCubeRolap->brange_date=$aux_brange_date;
			
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\n".date('Y-M-d H:i:s')." ".$theRepository->RepositoryName;
			$aLogString.="\r\nQuery to get Values Of the Dimension ".$this->consecutivo.":";
			$aLogString.="\r\n";
			$aLogString.=str_replace('<BR>'," ",$theCubeRolap->QueryText());
			global $queriesLogFile;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			
			return ($aRS);
		}
		else 
		{
			return null;
		}
	}
	
	function getHierarchyFilter($dimensionIDs,$dimensions,$strDimensionSelectedValues,$displayedDimensionID)
	{	
		$fatherValue=null;
		$dimIDFather=null;
		
		$nothingValue = false;
		
		if($this->HasHierarchy==false)
		{	
			//echo("<br>Retorna nulo<br>");
			return null;
		}

		
		$aRepository=$this->Repository;
		$cubeID=$this->cla_concepto;		
		
		$hierarchyDimIDs=$this->HierarchyDimIDs;
		$hierarchySize=count($hierarchyDimIDs);
		
		$indexFilteredDim=0;
		
		$filteredDimensions=BITAMFilteredDimensionCollection::NewInstance($this->Repository,"","",$cubeID);		
		
		if($strDimensionSelectedValues!="")
		{	
			//echo("<br>strDimensionSelectedValues no esta vacia<br>");
			
			$dimensionSelectedValues=explode("|",$strDimensionSelectedValues);
			//echo('<BR>##########Checar: $strDimensionSelectedValues='.$strDimensionSelectedValues);
			//echo('<BR>##########Checar: $dimensionSelectedValues=');
			//var_dump($dimensionSelectedValues);
			
			/*Obtener los valores de las dims. de la jerarquía que se despliegan como combos*/
			for($i=0;$i<$hierarchySize;$i++)
			{	
				$dimIDFather=$hierarchyDimIDs[$i];
				//echo('<BR>##########Checar: $dimIDFather='.$dimIDFather);
				$index=array_search($dimIDFather,$dimensionIDs);
				//echo('<BR>##########Checar: $index='.$index);
				if($index!==false && $dimIDFather!=$displayedDimensionID)
				{
					if (array_key_exists($index, $dimensionSelectedValues))
					{
						$fatherValue=$dimensionSelectedValues[$index];
					}
					else
					{
						$fatherValue=null;
					}

					$fatherValuesIDs=$dimensions->Collection[$index]->valuesID;
					
					if(count($fatherValuesIDs)>0)
					{
						if(in_array($fatherValue,$fatherValuesIDs)===false)
						{
							$fatherValue = $dimensions->Collection[$index]->valuesID[0];
						}
						//echo('<BR>##########Checar: $fatherValue='.$fatherValue);
					}
					else 
					{
						$this->valuesID = array();
						$this->values = array();
						$this->numValues = 0;
						
						$fatherValue = null;
						$nothingValue = true;
						
						//echo('<BR>##########Checar: $fatherValue='.$fatherValue);
					}

					//if($fatherValue!=null && $dimIDFather!=null)
					//{
						//echo('<BR>##########$fatherValue es diferente de nulo');
			
						$filteredDimensions->Collection[$indexFilteredDim]=BITAMFilteredDimension::NewInstance($aRepository);
						$filteredDimensions->Collection[$indexFilteredDim]->consecutivo=$dimIDFather;
						$filteredDimensions->Collection[$indexFilteredDim]->nom_tabla=$dimensions->Collection[$index]->nom_tabla;
						$filteredDimensions->Collection[$indexFilteredDim]->nom_fisicok=$dimensions->Collection[$index]->nom_fisicok;
						$filteredDimensions->Collection[$indexFilteredDim]->values[0]=$fatherValue;
						$indexFilteredDim++;
						//echo("en getHierarchyFilter");
						//echo('<BR>$dimIDFather='.$filteredDimensions->Collection[0]->consecutivo);
						//echo('<BR>$fatherValue='.$filteredDimensions->Collection[0]->values[0]);
					//}
				}
			}
		}
		else
		{
			//echo("<br>strDimensionSelectedValues si esta vacia<br>");
			
			/*Obtener los valores de las dims. de la jerarquía que se despliegan como combos*/
			for($i=0;$i<$hierarchySize;$i++)
			{	
				$dimIDFather=$hierarchyDimIDs[$i];
				//echo('<BR>##########Checar: $dimIDFather='.$dimIDFather);
				$index=array_search($dimIDFather,$dimensionIDs);
				//echo('<BR>##########Checar: $index='.$index);
				if($index!==false && $dimIDFather!=$displayedDimensionID)
				{
					if(count($dimensions->Collection[$index]->valuesID)>0)
					{
						$fatherValue=$dimensions->Collection[$index]->valuesID[0];
						//echo('<BR>##########Checar: $fatherValue='.$fatherValue);
					}
					else 
					{
						$this->valuesID = array();
						$this->values = array();
						$this->numValues = 0;
						
						$fatherValue = null;
						$nothingValue = true;
						//echo('<BR>##########Checar: $fatherValue='.$fatherValue);
					}

					//if($fatherValue!=null && $dimIDFather!=null)
					//{
						//echo('<BR>##########$fatherValue es diferente de nulo');
			
						$filteredDimensions->Collection[$indexFilteredDim]=BITAMFilteredDimension::NewInstance($aRepository);
						$filteredDimensions->Collection[$indexFilteredDim]->consecutivo=$dimIDFather;
						$filteredDimensions->Collection[$indexFilteredDim]->nom_tabla=$dimensions->Collection[$index]->nom_tabla;
						$filteredDimensions->Collection[$indexFilteredDim]->nom_fisicok=$dimensions->Collection[$index]->nom_fisicok;
						$filteredDimensions->Collection[$indexFilteredDim]->values[0]=$fatherValue;
						$indexFilteredDim++;
						//echo("en getHierarchyFilter");
						//echo('<BR>$dimIDFather='.$filteredDimensions->Collection[0]->consecutivo);
						//echo('<BR>$fatherValue='.$filteredDimensions->Collection[0]->values[0]);
					//}
					
				}
			}
		}
		
			
		return $filteredDimensions;
	}
	
	function addNearChildHierarchyFilter($dimensionIDs,$dimensions,$strDimensionSelectedValues,&$hierarchyFilterCollectionDisplayedDimensionID)
	{	
		$childValue=null;
		$dimIDChild=null;
		
		if($this->HasHierarchy==false)
		{	
			//echo("<br>Retorna nulo<br>");
			return;
		}

		
		$aRepository=$this->Repository;
		$cubeID=$this->cla_concepto;		
		
		$hierarchyDimIDs=$this->ChildrenHierarchyDimIDs;
		$hierarchySize=count($hierarchyDimIDs);
		
		$indexFilteredDim = count($hierarchyFilterCollectionDisplayedDimensionID->Collection);
		
			
		$dimensionSelectedValues=explode("|",$strDimensionSelectedValues);
		
		//echo('<BR>##########Checar: $strDimensionSelectedValues='.$strDimensionSelectedValues);
		//echo('<BR>##########Checar: $dimensionSelectedValues=');
		//var_dump($dimensionSelectedValues);
		
		/*Obtener los valores de las dims. de la jerarquía que se despliegan como combos*/
		for($i=0;$i<$hierarchySize;$i++)
		{	
			$dimIDChild=$hierarchyDimIDs[$i];
			//echo('<BR>##########Checar: $dimIDChild='.$dimIDChild);
			$index=array_search($dimIDChild,$dimensionIDs);
			//echo('<BR>##########Checar: $index='.$index);
			if($index!==false)
			{
				$childValue=$dimensionSelectedValues[$index];
		
				$hierarchyFilterCollectionDisplayedDimensionID->Collection[$indexFilteredDim]=BITAMFilteredDimension::NewInstance($aRepository);
				$hierarchyFilterCollectionDisplayedDimensionID->Collection[$indexFilteredDim]->consecutivo=$dimIDChild;
				$hierarchyFilterCollectionDisplayedDimensionID->Collection[$indexFilteredDim]->nom_tabla=$dimensions->Collection[$index]->nom_tabla;
				$hierarchyFilterCollectionDisplayedDimensionID->Collection[$indexFilteredDim]->nom_fisicok=$dimensions->Collection[$index]->nom_fisicok;
				$hierarchyFilterCollectionDisplayedDimensionID->Collection[$indexFilteredDim]->values[0]=$childValue;
				$indexFilteredDim++;
		
			}
		}
	}
	
	
	function getDimensionIDsBetweenCubes($aRepository, $dimensionIDs, $aSourceCubeID, $aTargetCubeID)
	{
		global $checkDisabledCubes;
		
		if ($aSourceCubeID == $aTargetCubeID)
		{
			return $dimensionIDs;
		}
		
		$numDimensions = count($dimensionIDs);
		if ($numDimensions == 0)
		{
			return array();
		}
		$strDimensionsWithComma=implode(',',$dimensionIDs);
		$newDimensionIDs = BITAMGlobalInstance::GetSharedDimensionIDsBetweenCubes($strDimensionsWithComma,$aSourceCubeID,$aTargetCubeID);
		if (!is_null($newDimensionIDs))
		{
			return $newDimensionIDs;
		}
		/*
		$strDimensionsWithComma="";
		for($i=0; $i<$numDimensions; $i++)
		{
			if($strDimensionsWithComma!="")
			{
				$strDimensionsWithComma.=", ";
			}
			
			$strDimensionsWithComma.= $dimensionIDs[$i];
		}
		*/
		
		if($strDimensionsWithComma!="")
		{
			//@JAPR 2010-02-16: Validado que sólo se carguen dimensiones visibles
			$sql="SELECT B.CONSECUTIVO FROM SI_CPTO_LLAVE A, SI_CPTO_LLAVE B 
			WHERE A.CLA_CONCEPTO=".$aSourceCubeID." AND A.CONSECUTIVO IN (".$strDimensionsWithComma.
					") AND A.CLA_DESCRIP=B.CLA_DESCRIP AND B.CLA_CONCEPTO=".$aTargetCubeID." AND A.ESTATUS = 1 AND B.ESTATUS = 1";
			//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
			if ($checkDisabledCubes)
			{
				$sql .= " AND A.CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB) 
					AND B.CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)";
			}
			$sql .= " ORDER BY A.CONSECUTIVO";
			//@JAPR

			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ". $aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$newDimensionIDs = array();
		
			while (!$aRS->EOF)
			{
				$newDimensionIDs[] = (int)$aRS->fields["consecutivo"];
				
				$aRS->MoveNext();
			}
		
		}
		
		BITAMGlobalInstance::AddSharedDimensionIDsBetweenCubes($strDimensionsWithComma,$aSourceCubeID,$aTargetCubeID,$newDimensionIDs);
		return $newDimensionIDs;
	
	}
	
	function getDimensionIDsInTargetCube($aRepository, $dimensionIDs, $aSourceCubeID, $aTargetCubeID)
	{
		if ($aSourceCubeID == $aTargetCubeID)
		{
			return $dimensionIDs;
		}
		
		$numDimensions = count($dimensionIDs);
		if ($numDimensions == 0)
		{
			return array();
		}
		$strDimensionsWithComma=implode(',',$dimensionIDs);
		
		if($strDimensionsWithComma!="")
		{
		
			$sql = "SELECT A.CONSECUTIVO FROM SI_CPTO_LLAVE A, SI_CPTO_LLAVE B 
					WHERE A.CLA_CONCEPTO=".$aSourceCubeID." AND A.CONSECUTIVO IN (".$strDimensionsWithComma.") 
					AND A.CLA_DESCRIP=B.CLA_DESCRIP AND B.CLA_CONCEPTO=".$aTargetCubeID." ORDER BY A.CONSECUTIVO";

			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ". $aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$newDimensionIDs = array();
		
			while (!$aRS->EOF)
			{
				$newDimensionIDs[] = (int)$aRS->fields["consecutivo"];
				
				$aRS->MoveNext();
			}
		}
		
		return $newDimensionIDs;
	}

	static function getLogicalNameByDimID($aRepository, $aCubeID, $dimID)
	{
		$logicalName = "";
		
		$sql = "SELECT NOM_LOGICO FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = ".$aCubeID." AND CONSECUTIVO = ".$dimID." ORDER BY CONSECUTIVO";
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ". $aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$logicalName = $aRS->fields["nom_logico"];
			$aRS->MoveNext();
		}
		
		return $logicalName;
	}
	
	
	static function getLogicalNameByDimIDs($aRepository, $aCubeID, $dimIDs)
	{
		$logicalNames=array();
		if(count($dimIDs) == 0) 
		{
			return $logicalNames;
		}
		$sql = "SELECT CONSECUTIVO, NOM_LOGICO FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = ".$aCubeID." AND CONSECUTIVO IN (".implode(",",$dimIDs).") ORDER BY CONSECUTIVO";
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ". $aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$logicalNames[(int)$aRS->fields["consecutivo"]] = $aRS->fields["nom_logico"];
			$aRS->MoveNext();
		}
		
		return $logicalNames;
	}
	
	//Obtener el valor de no aplica de la dimensión
	function getNoApplyValue()
	{
		$defaultKey=null;
		$cla_descrip=$this->cla_descrip;
		
		$sql="SELECT DEFAULT_KEY FROM SI_DESCRIP_ENC WHERE CLA_DESCRIP =".$cla_descrip;
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$defaultKey = $aRS->fields["default_key"];
		}
	
		return $defaultKey;
	}
	
	//Obtener del catálogo de la dimensión la clave mínima que sea diferente al valor de no aplica
	function getMinIDDifferentFromNoApplyValue($fatherValueID)
	{
		global $descriptKeysCollection;
		$minID=null;
		
		$defaultKey=$this->getNoApplyValue();
		$nom_tabla=$this->nom_tabla;
		$nom_fisicok_bd=$this->nom_fisicok_bd;
		$nom_fisicok=$this->nom_fisicok;
		$cubeID=$this->cla_concepto;
		$fatherDimID=$this->FatherParDescriptor[0];
		
		$fatherDim=BITAMDimension::NewInstanceWithID($this->Repository, $fatherDimID,$cubeID,false);
		//campo que es el join entre fatherDim y la dimensión ($this)
		$joinFieldName=getJoinFieldByTableName($fatherDim->nom_tabla, $fatherDim->nom_fisicok_bd,$descriptKeysCollection,TRUE);
		
		if($fatherDim->tipo_datok==2)
		{
			$fatherValueID=$this->Repository->DataADOConnection->Quote($fatherValueID);
		}
		
		$sql="SELECT MIN(".$nom_fisicok_bd.") AS MinValueID FROM ".$nom_tabla . " WHERE ".$joinFieldName."=".$fatherValueID;
		
		if($defaultKey!=null)
		{
			$sql.=" WHERE ".$nom_fisicok_bd." NOT IN (".$defaultKey.")";
		}
	
		//global $queriesLogFile;
		//$aLogString="\r\n--------------------------------------------------------------------------------------";
		//$aLogString.="\r\nBITAMDimension->GetMinIDDifferentFromNoApplyValue: \r\n";
		//$aLogString.=$sql;
		//@updateQueriesLogFile($queriesLogFile,$aLogString);
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$minID = $aRS->fields["minvalueid"];
		}
	
		return $minID;
	}
	
	//Obtener del catálogo de la dimensión la clave mínima
	function getMinID()
	{
		$minID=null;
	
		$nom_tabla=$this->nom_tabla;
		$nom_fisicok_bd=$this->nom_fisicok_bd;
		
		$sql="SELECT MIN(".$nom_fisicok_bd.") AS MinValueID FROM ".$nom_tabla;
		//global $queriesLogFile;
		//$aLogString="\r\n--------------------------------------------------------------------------------------";
		//$aLogString.="\r\nBITAMDimension->GetMinID: \r\n";
		//$aLogString.=$sql;
		//@updateQueriesLogFile($queriesLogFile,$aLogString);
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$minID = $aRS->fields["minvalueid"];
		}
	
		return $minID;
	}
	
	function getDummyValue($fatherValueID)
	{
		$dummyValue=null;
		
		$minID=$this->getMinIDDifferentFromNoApplyValue($fatherValueID);
		
		if($minID<0)
		{	//Si tiene valor Dummy, por que existe un id negativo q no es el valor de no aplica	
			$dummyValue=$minID;	
		}
		
		return $dummyValue;
	}
	
	function generateDummyElement()
	{
		$dummyInfo=array();
		
		$minID=$this->getMinID();
		
		//El valor dummy siempre será negativo para las claves enteras
		if ($minID>=0)
		{	
			$dummyID=-1;
		}
		else 
		{
			$dummyID=$minID-1;	
		}
	
		$dummyDescription="Dummy".$dummyID;
	
		$dummyInfo['dummyID']=$dummyID;
		$dummyInfo['dummyDescription']=$dummyDescription;
		
		return $dummyInfo;
	}
	
	function insertDummyElement($aDummyID, $aDummyDescription,$fatherValueID)
	{
		global $descriptKeysCollection;
		$nom_tabla=$this->nom_tabla;
		$nom_fisicok_bd=$this->nom_fisicok_bd;
		$nom_fisicok=$this->nom_fisicok;
		$nom_fisico_bd=$this->nom_fisico_bd;
		$cubeID=$this->cla_concepto;
		//Info de la dimensión padre		
		$fatherDimID=$this->FatherParDescriptor[0];
		$fatherDim=BITAMDimension::NewInstanceWithID($this->Repository, $fatherDimID,$cubeID,false);
		//campo que es el join entre fatherDim y la dimensión ($this)
		$joinFieldName=getJoinFieldByTableName($fatherDim->nom_tabla, $fatherDim->nom_fisicok_bd,$descriptKeysCollection,TRUE);
		
		if($fatherDim->tipo_datok==2)
		{
			$fatherValueID=$this->Repository->DataADOConnection->Quote($fatherValueID);
		}
		
		$sql="INSERT INTO ". $nom_tabla. " (".$nom_fisicok_bd. ",".$nom_fisico_bd.",".$joinFieldName.") VALUES (".$aDummyID.",".$this->Repository->DataADOConnection->Quote($aDummyDescription).",".$fatherValueID.")";
		//global $queriesLogFile;
		//$aLogString="\r\n--------------------------------------------------------------------------------------";
		//$aLogString.="\r\nBITAMDimension->InsertDummyElement: \r\n";
		//$aLogString.=$sql;
		//@updateQueriesLogFile($queriesLogFile,$aLogString);
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ". $nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	//Obtiene la descripción de un elemento del catalógo de la dimensión
	function getDescriptionValueByID($elementID)
	{
		$nom_tabla=$this->nom_tabla;
		$nom_fisicok_bd=$this->nom_fisicok_bd;
		$nom_fisico=$this->nom_fisico;
		$nom_fisico_bd=$this->nom_fisico_bd;
		
		
		$sql="SELECT ".$nom_fisico_bd." FROM ".$nom_tabla. " WHERE ".$nom_fisicok_bd."=".$elementID;
		//global $queriesLogFile;
		//$aLogString="\r\n--------------------------------------------------------------------------------------";
		//$aLogString.="\r\nBITAMDimension->GetDescriptionValueByID: \r\n";
		//$aLogString.=$sql;
		//@updateQueriesLogFile($queriesLogFile,$aLogString);
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$description = $aRS->fields[$nom_fisico];
		}
	
		return $description;
	}
	
	//Asignar el valor dummy a la dimensión
	function setDummyValue($fatherValueID)
	{
		$dummyID=$this->getDummyValue($fatherValueID);
	
		if ($dummyID==null)
		{	//Generar un valor dummy
			$newDummyInfo=$this->generateDummyElement();	
			$newDummyID=$newDummyInfo['dummyID'];
			$newDummyDescription=$newDummyInfo['dummyDescription'];
			$this->insertDummyElement($newDummyID, $newDummyDescription,$fatherValueID);
			$dummyID=$newDummyID;
			$dummyDescription=$newDummyDescription;
		}
		else
		{
			$dummyDescription=$this->getDescriptionValueByID($dummyID);
		}
		$this->values=array();
		$this->values[$dummyID]=$dummyDescription;
		$this->valuesID=array();
		$this->valuesID[0]=$dummyID;
		//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
		//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
		$this->subrrogateID=array();
		$this->subrrogateID[0]="";
		$this->parentJoinsIDs=array();
		$this->parentJoinsIDs[0]=array();
		//@JAPR
		$this->numValues=1;
	}
	
	//Se recibe un arreglo en el formato cubeID*1000+DimID y se retorna un arreglo
	//con los IDs de las dimensiones
	function getDimensionsIDsByCubeDimID($aCubeDimArray)
	{
		$numElements=count($aCubeDimArray);
		$dimensionIDs=array();
		for ($i=0; $i<$numElements; $i++)
		{
			$dimensionIDs[$i]=$aCubeDimArray[$i] % 1000;
		}
		
		return $dimensionIDs;
	}
	
	static function getDimensionsNotInFactTable($noSelectedDimensions, $descriptKeysCollection)
	{
        $arrDimensionsToSkipNoApply = array();
        $numNoSelectedDimensions = count($noSelectedDimensions->Collection);
        for ($i=0;$i<$numNoSelectedDimensions;$i++) 
        {
	        if($noSelectedDimensions->Collection[$i]->dependsOnDimension==0)
            {
     	       $joinFieldName=getJoinFieldByTableName($noSelectedDimensions->Collection[$i]->nom_tabla, $noSelectedDimensions->Collection[$i]->nom_fisicok_bd, $descriptKeysCollection, TRUE);

                if(is_null($joinFieldName))
                {
	                $arrDimensionsToSkipNoApply[$noSelectedDimensions->Collection[$i]->consecutivo] = $noSelectedDimensions->Collection[$i]->consecutivo;                            
                }
            }
            else
            {
    	        $arrDimensionsToSkipNoApply[$noSelectedDimensions->Collection[$i]->consecutivo] = $noSelectedDimensions->Collection[$i]->consecutivo;
            }
        }
        
        return $arrDimensionsToSkipNoApply;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		global $maxDimensionRecords;
		global $dimValuesFilter;
		global $bShowNullParentValues;
		
		//@JAPR 2009-07-08: Agregados los filtros para la carga de valores de dimensiones
		if(array_key_exists("txtDefDimRec", $aHTTPRequest->POST))
		{
			$maxDimensionRecords = (int) $aHTTPRequest->POST["txtDefDimRec"];
			if($maxDimensionRecords < 0)
			{
				$maxDimensionRecords = DEFAULT_MAX_DIM_RECORDS;
			}
			$_SESSION["PAMaxDimRecords"] = $maxDimensionRecords;
		}
		if(array_key_exists("txtDimFilter", $aHTTPRequest->POST))
		{
			$dimValuesFilter = $aHTTPRequest->POST["txtDimFilter"];
			$_SESSION["PADefDimValuesFilter"] = $dimValuesFilter;
		}
		if(array_key_exists("chkShowNullParentValues", $aHTTPRequest->POST))
		{
			$bShowNullParentValues = $aHTTPRequest->POST["chkShowNullParentValues"];
			$_SESSION["PAShowNullParentValues"] = $bShowNullParentValues;
		}
		//@JAPR
		
		if (array_key_exists("CubeID", $aHTTPRequest->GET))
		{
			$aCubeID = (int) $aHTTPRequest->GET["CubeID"];
		}
		else
		{
			$aCubeID = -1;
		}
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if (array_key_exists("consecutivo", $aHTTPRequest->POST)) // || array_key_exists("DescripID", $aHTTPRequest->POST))
		{
			$anInstance = null;
			/*
			if (array_key_exists("DescripID", $aHTTPRequest->POST))
			{
				$aDescripKey = $aHTTPRequest->POST["DescripID"];
				if (!is_array($aDescripKey))
				{
					$anInstance = BITAMDimension::NewInstanceWithDescripID($aRepository, (int) $aDescripKey, $aCubeID, false);
					if (is_null($anInstance))
					{
						$anInstance = BITAMDimension::NewInstance($aRepository, $aCubeID);
					}
				}
			}
			else
			{
			*/
				$aConsecutiveID = $aHTTPRequest->POST["consecutivo"];
				if (!is_array($aConsecutiveID))
				{
					$anInstance = BITAMDimension::NewInstanceWithID($aRepository, (int) $aConsecutiveID, $aCubeID, false);
					if (is_null($anInstance))
					{
						$anInstance = BITAMDimension::NewInstance($aRepository, $aCubeID);
					}
				}
			//}
			//$aCubeID = $aHTTPRequest->POST["CubeID"];
			
			if (!is_null($anInstance))
			{
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMDimension::NewInstance($aRepository, $aCubeID);
					}
				}
				$anInstance->maxDimensionValueToShow = $maxDimensionRecords;
				$anInstance->dimensionValuesFilter = $dimValuesFilter;
				$anInstance->showNullParentValues = $bShowNullParentValues;
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		//@JAPR
		
		$anInstance = null;
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if(array_key_exists("DescripID", $aHTTPRequest->GET))
		{
			$aDescripKey = $aHTTPRequest->GET["DescripID"];
			$anInstance = BITAMDimension::NewInstanceWithDescripID($aRepository, $aDescripKey, $aCubeID,false);
			if (is_null($anInstance))
			{
				$anInstance = BITAMDimension::NewInstance($aRepository, $aCubeID);
			}
		}
		else*/if (array_key_exists("consecutivo", $aHTTPRequest->GET))
		{
			$aConsecutiveID = $aHTTPRequest->GET["consecutivo"];
			$anInstance = BITAMDimension::NewInstanceWithID($aRepository,$aConsecutiveID,$aCubeID,false);
			if (is_null($anInstance))
			{
				$anInstance = BITAMDimension::NewInstance($aRepository, $aCubeID);
			}
		}
		//@JAPR
		else
		{
			$anInstance = BITAMDimension::NewInstance($aRepository, $aCubeID);
			if (array_key_exists("ChildDescripKey", $aHTTPRequest->GET))
			{
				$anInstance->ChildDescripKey = (int) $aHTTPRequest->GET["ChildDescripKey"];
			}
		}
		$anInstance->maxDimensionValueToShow = $maxDimensionRecords;
		$anInstance->dimensionValuesFilter = $dimValuesFilter;
		$anInstance->showNullParentValues = $bShowNullParentValues;
		return $anInstance;
	}

	/*@JAPR 2010-07-09: Agregado el recalculo de agregaciones a partir de la lista de valores editados/borrados (si se trata de edición, sólo
	aplica si la dimensión no tiene ni llave ni subrrogada, pues de lo contrario la descripción no afecta en nada al cubo)
		Este método identificará cual es el rango de fechas en el cubo que contengan a cualquiera de estos valores, y a partir de dicho rango es que
	se hará el recalculo de las agregaciones donde la dimensión se encuentre involucrada (pero el recalculo se hace en otro método). Se regresa un
	array indexado por el Id del cubo, conteniendo un array con la fecha inicial y fecha final donde hay datos para cualquiera de los valores, si
	no se hubieran encontrado datos o hubiera ocurrido un error en su obtención, se regresaría un valor null en el índice de dicho cubo
		Al día de hoy no se ha definido aun si se borrará el registro completo que contenga a este valor de dimensión, o si simplemente se
	actualizará dicho valor al de No aplica, pero eso es determinante para saber que es lo que se recalculará
		@JAPRWarning: Este código no fué habilitado (ni probado) porque se decidió temporalmente deshailitar el borrado de elementos de dimensión
	*/
	function refreshAggregationsByDatesRange($aValuesArr)
	{
		$arrCubeDate = array();
		$arrCubes = array();

		//Si la dimensión es un padre de SnowFlake, no hay necesidad de recalcular agregados
		if ($aDimension->dependsOnDimension > 0)
		{
			return $arrCubeDate;
		}
		
		//Obtiene todas las instancias de la dimensión en los cubos donde se usa
		$aChildDimensionCollection = BITAMDimensionCollection::NewInstanceWithDescripID($this->Repository, array($this->ChildDescripKey), -1, true, false, false);
		if (is_null($aChildDimensionCollection))
		{
			return $arrCubeDate;
		}
		
		//Recorre cada cubo encontrado y obtiene el MIN y MAX de fecha donde se encuentren los valores especificados
		foreach ($aChildDimensionCollection as $aChildDimension)
		{
			$arrCubeDate[$aChildDimension->cla_concepto] = null;
			
			//Obtiene el rango de fechas usando las librerías de conectividad a cubos
			//@JAPR 2010-07-19: Agregado el parámetro $bCreateQuery para indicar que la instancia de rolap sea generada automáticamente
			$theCube = BITAMCInfoCube::NewInstance($this->Repository, $aChildDimension->cla_concepto, false, true);
			$arrCubes[$aChildDimension->cla_concepto] = $theCube;
			
			//Obtiene el nombre del campo fecha y le asigna el Alias de la tabla de tiempo (t2) pues ahi es seguro que si está
			$strFieldDate = 't2.'.$theCube->field_date;
			
			//Genera el filtro de valores de la dimensión (este filtro es dependiente del consecutivo de la dimensión en este cubo)
			$strFilter = '';
			foreach ($aValuesArr as $strValueID)
			{
				$strDimensionValueID = formatValueForSQL($this->Repository, $strValueID, $aChildDimension->tipo_datok);
				$strFilter .= ' OR '."[".$aChildDimension->nom_tabla."].[".$aChildDimension->nom_fisicok_bd."_".$aChildDimension->consecutivo."].[".$strDimensionValueID."]";
			}
			
			// Estamos listos para usar la funcionalidad de ROLAP
			$theCubeRolap = $theCube->rolap;
			
			// Initialize
			$theCubeRolap->Initialize();
			
			//Se establece temporalmente que el cubo no use agregaciones para asegurar que las combinaciones de dimensiones se obtengan de la Tabla de Hechos
			$aux_use_agregation = $theCubeRolap->use_agregation;
			$theCubeRolap->use_agregation = false;
			
			//No se debe insertar la dimensión para que no agrupe (aunque si sólo se inserta un valor en el filtro entonces si agrupa, pero 
			//en este caso para un sólo valor la agrupación es irrelevante)
			//$theCubeRolap->InsertDimension('['.$theCube->a_dimensions[$aChildDimension->consecutivo]->physical_table.'].['.$theCube->a_dimensions[$aChildDimension->consecutivo]->physical_name_descrip.']');
			
			//Inserta los dos indicadores Dummy para obtener las fechas
			$theCubeRolap->InsertIndicator('min('.$strFieldDate.') AS MinDate',false, 0, -1, -1);
			$theCubeRolap->InsertIndicator('max('.$strFieldDate.') AS MaxDate',false, 0, -1, -1);
			
			$theCubeRolap->Filter($strFilter);
			
			$aRS = $theCubeRolap->RowSet($theCube->connection->ADOConnection);
			if ($aRS)
			{
				if (!$aRS->EOF)
				{
					$aDateIni = @$aRS->fields[0];
					$aDateFin = @$aRS->fields[1];
					if (!is_null($aDateIni) && !is_null($aDateFin))
					{
						$arrCubeDate[$aChildDimension->cla_concepto] = array('IniD' => $aDateIni, 'FinD' => $aDateFin);
					}
				}
			}
		}

		/*Invoca al recalculo de agregados según el array de cubos/fechas proporcionado como parámetro. Si el valor correspondiente a cada cubo
		es un null, se asume que se desea recalcular la totalidad del agregado, de lo contrario usa las fecha inicial y final enviadas 
			Por cada cubo especificado, verifica sus fechas para obtener el rango a recalcular (si no se enviaron, se asume el total del cubo)
		*/
		foreach ($aChildDimensionCollection as $aChildDimension)
		{
			$arrDates = $aCubesDataArr[$aChildDimension->cla_concepto];
			$theCube = @$arrCubes[$aChildDimension->cla_concepto];
			if (!is_null($arrDates) && !is_null($theCube))
			{
				$aDateIni = @$arrDates['IniD'];
				$aDateFin = @$arrDates['FinD'];
				
				//Recalcula el agregado de este cubo
				$theCube->RefreshAgregationsForDimension($aDateIni, $aDateFin, $aChildDimension->consecutivo);
			}
		}
		
		return $arrCubeDate;
	}
	
	function save()
	{
		global $queriesLogFile;
		global $appSettings;
		
	 	if ($this->isNewObject())
		{
			$aCube = BitamCube::NewInstanceWithID($this->Repository, $this->cla_concepto); 
			$aCubeID = $aCube->cla_concepto;

			$sql = 'SELECT TIPO_DATO, NOM_DATO FROM SI_BD_TIPOS 
				WHERE CLA_BD = '.$aCube->cla_bd.' AND TIPO_DATO = '.$this->tipo_dato;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_BD_TIPOS ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$strDataType = '';
			if (!$aRS->EOF)
			{
				$strDataType = $aRS->fields["nom_dato"];
				switch ($this->tipo_dato)
				{
		 			case 1:
		 			case 2:
		 				$strDataType .= '('.$this->long_dato.')';
		 				break;
				}
			}

			$strDataTypeK = $strDataType;
			if ($this->tipo_datok != $this->tipo_dato || $this->long_datok != $this->long_dato)
			{
				$sql = 'SELECT TIPO_DATO, NOM_DATO FROM SI_BD_TIPOS 
					WHERE CLA_BD = '.$aCube->cla_bd.' AND TIPO_DATO = '.$this->tipo_datok;
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_BD_TIPOS ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				if (!$aRS->EOF)
				{
					$strDataTypeK = $aRS->fields["nom_dato"];
					switch ($this->tipo_datok)
					{
			 			case 1:
			 			case 2:
			 				$strDataTypeK .= '('.$this->long_datok.')';
			 				break;
					}
				}
			}
			
			$initialKey = GetBitamInitialKey($this->Repository);

			// Asigna un nombre default si no fueron capturados por el usuario
			$sql = 'SELECT '.
						$this->Repository->ADOConnection->IfNull('MAX(CLA_DESCRIP)', "".$initialKey."").' + 1 AS CLA_DESCRIP FROM SI_DESCRIP_ENC';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$temporalID = (int)$aRS->fields["cla_descrip"];
			
			if($temporalID<($initialKey+1))
			{
				$temporalID = $initialKey+1;
			}

			$this->cla_descrip = $temporalID;

			$strFieldName = getValidFieldNameFromStr($this->nom_logico, 12);
			if (trim($this->nom_tabla) == '')
			{
				$this->nom_tabla = 'T'.$strFieldName.sprintf('%05d', $this->cla_descrip);
			}
			if (trim($this->nom_fisicok_bd) == '')
			{
				$this->nom_fisicok_bd = 'K'.$strFieldName.sprintf('%05d', $this->cla_descrip);
			}
			if (trim($this->nom_fisico_bd) == '')
			{
				$this->nom_fisico_bd = 'D'.$strFieldName.sprintf('%05d', $this->cla_descrip);
			}
			
			$strNewFieldDefinition = $this->nom_fisico_bd.' '.$strDataType;
			$strNewKeyFieldDefinition = $this->nom_fisicok_bd.' '.$strDataTypeK;
			// Agrega la tabla de la dimensión
			$sql =  "CREATE TABLE ".$this->nom_tabla.' ('.
				$strNewKeyFieldDefinition.', '.
				$strNewFieldDefinition.', '.
				'PRIMARY KEY ('.$this->nom_fisicok_bd.'))';
			//$aLogString="\r\n--------------------------------------------------------------------------------------";
			//$aLogString.="\r\nBITAMDimension->Save (Create table): \r\n";
			//$aLogString.=$sql;
			//@updateQueriesLogFile($queriesLogFile,$aLogString);
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error creating")." ".$this->nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			// Agrega el valor de No Aplica
			$sql = "INSERT INTO ".$this->nom_tabla." (".$this->nom_fisicok_bd.", ".$this->nom_fisico_bd.") 
				VALUES (".$this->NoApplyValue.', '.$this->Repository->DataADOConnection->Quote('Without '.$this->nom_logico).')';
			//$aLogString="\r\n--------------------------------------------------------------------------------------";
			//$aLogString.="\r\nBITAMDimension->Save (Insert): \r\n";
			//$aLogString.=$sql;
			//@updateQueriesLogFile($queriesLogFile,$aLogString);
			if($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$this->nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			// Agrega la columna de la llave a la tabla de la dimensión hija
			$aConnectionID = -1;
			if (!is_null($this->ChildDescripKey) && $this->ChildDescripKey > 0)
			{
				$aChildDimension = BITAMDimension::NewInstanceWithDescripID($this->Repository,$this->ChildDescripKey,$aCubeID,false);
			}
			elseif ($this->dependsOnDimension > 0)
			{
				$aChildDimension = BITAMDimension::NewInstanceWithID($this->Repository,$this->dependsOnDimension,$aCubeID,false);
			}

			if (!is_null($aChildDimension))
			{
				//@JAPR 2011-06-07: Agregada la actualización del valor del campo join al registro de no aplica del padre (#25004)
				$strDefaultValue = '';
				if ($aCube->cla_bd == MYSQL || $aCube->cla_bd == SQL_SERVER || $aCube->cla_bd == ORACLE)
				{
					$strDefaultValue = ' DEFAULT '.$this->NoApplyValue;
				}
				$sql =  "ALTER TABLE ".$aChildDimension->nom_tabla.' ADD '.$strNewKeyFieldDefinition.$strDefaultValue;
				//@JAPR
				//$aLogString="\r\n--------------------------------------------------------------------------------------";
				//$aLogString.="\r\nBITAMDimension->Save (Alter): \r\n";
				//$aLogString.=$sql;
				//@updateQueriesLogFile($queriesLogFile,$aLogString);
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error modifying")." ".$aChildDimension->nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				//@JAPR 2011-06-07: Agregada la actualización del valor del campo join al registro de no aplica del padre (#25004)
				else
				{
					$sql = 'UPDATE '.$aChildDimension->nom_tabla.' SET '.$this->nom_fisicok_bd.' = '.$this->NoApplyValue.
						' WHERE '.$this->nom_fisicok_bd.' IS NULL';
					$aRS = $this->Repository->DataADOConnection->Execute($sql);
				}
				//@JAPR
				$aConnectionID = $aChildDimension->cla_conexion;
			}
			
			// Agrega la nueva definición de dimensión global
			$sql = 'INSERT INTO SI_DESCRIP_ENC (CLA_DESCRIP, CLA_CONEXION, NOM_LOGICO, NOM_FISICO, SQL_SELECT, 
					TIPO_DATO, LONG_DATO, NOM_FISICOK, TIPO_DATOK, LONG_DATOK, 
					AGRUPADOR, NOM_TABLA, ORDEN_DEL_SERVER, CLA_OWNER, TIPO_DIMENSION, DEFAULT_KEY, CREATED_BY)  
			    VALUES ('.$this->cla_descrip.', '.$aConnectionID.', '.
					$this->Repository->ADOConnection->Quote($this->nom_logico).', '.
					$this->Repository->ADOConnection->Quote($this->nom_fisico_bd).', '.
					$this->Repository->ADOConnection->Quote('').', '.
					$this->tipo_dato.', '.$this->long_dato.', '.
					$this->Repository->ADOConnection->Quote($this->nom_fisicok_bd).', '.
					$this->tipo_datok.', '.$this->long_datok.', '.
					$this->Repository->ADOConnection->Quote('').', '.
					$this->Repository->ADOConnection->Quote($this->nom_tabla).', '.
					'0, '.$_SESSION["PAtheUserID"].', 3, '.
					$this->Repository->ADOConnection->Quote($this->NoApplyValue).
					', 1)';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error inserting ")." SI_DESCRIP_ENC ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			// Agrega la relación global entre las dimensiones padre e hija
		   $sql = 'INSERT INTO SI_DESCRIP_DET (CLA_DESCRIP, CLA_PADRE)  
		       VALUES ('.$this->cla_descrip.', '.$this->ChildDescripKey.')';
		   $aRS = $this->Repository->ADOConnection->Execute($sql);
		   if ($aRS === false)
		   {
		    	die("(".__METHOD__.") ".translate("Error inserting ")." SI_DESCRIP_DET ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		   }

			// Obtenemos todas las instancias de la dimensión hija en los cubos donde se usa para agregar también esta nueva
			// dimensión en dichos cubos
			$aChildDimensionCollection = BITAMDimensionCollection::NewInstanceWithDescripID($this->Repository, array($this->ChildDescripKey), -1, true, false, false);
			if (is_null($aChildDimensionCollection))
			{
				return $this;
			}
			
			$arrCubesToExport = array();
			//@JAPR 2011-06-23: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			if ($appSettings["UseGlobalDimensions"] == 0)
			{
				//Estos registros sólo aplican si existe un cubo asociado a la dimensión cargada
				//@JAPRWarning: Validar que se puede hacer si no hay un cubo cargado, porque si se graba sin esta información pero la dimensión
				//si existe aplicada a un cubo (por ejemplo, se graba desde el Survey pero en Model Manager se había usado para ligar a un cubo)
				//entonces en los cubos donde exista la dimensión quedará la información incompleta
				foreach ($aChildDimensionCollection as $aChildDimension)
				{
					$this->dependsOnDimension = $aChildDimension->consecutivo;
					// Agrega la nueva Dimensión a la definición del Cubo
					$sql = 'SELECT '.
									$this->Repository->ADOConnection->IfNull('MAX(CONSECUTIVO)', '0').' + 1 AS CONSECUTIVO FROM SI_CPTO_LLAVE 
								WHERE CLA_CONCEPTO = '.$aChildDimension->cla_concepto;
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
	
					$this->consecutivo = (int)$aRS->fields["consecutivo"];
	
					$strParameters = '';
	/*				
					if ($this->dependsOnDimension > 0)
					{
						if ($aChildDimension->consecutivo < 10)
						{
							$strParameters = $this->consecutivo.'|0'.$aChildDimension->consecutivo;
						}
						else 
						{
							$strParameters = $this->consecutivo.'|'.$aChildDimension->consecutivo;
						}
					}
					else 
					{
						$strParameters = $this->consecutivo;
					}
	*/
					if ($this->consecutivo < 10)
					{
						$strParameters = '0'.$this->consecutivo;
					}
					else 
					{
						$strParameters = $this->consecutivo;
					}
					if ($this->dependsOnDimension > 0)
					{
						if ($aChildDimension->consecutivo < 10)
						{
							$strParameters .= '|0'.$aChildDimension->consecutivo;
						}
						else 
						{
							$strParameters .= '|'.$aChildDimension->consecutivo;
						}
						/*
						if ($aChildDimension->consecutivo < 10)
						{
							$strParameters = $this->consecutivo.'|0'.$aChildDimension->consecutivo;
						}
						else 
						{
							$strParameters = $this->consecutivo.'|'.$aChildDimension->consecutivo;
						}
						*/
					}
					else 
					{
						/*
						$strParameters = $this->consecutivo;
						*/
					}				
		
					//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
					//Se agregaron los valores EDIT_OUTLINE y ORDINALLEVEL en -1 para evitar que se consideren en los LayOuts del SAAS
					//@JAPR 2010-07-13: Modificado el valor default del campo ORDINALLEVEL a -2 por petición de EArteaga
					$sql = 'INSERT INTO SI_CPTO_LLAVE (NIVEL, CLA_CONCEPTO, CLA_DESCRIP, NOM_LOGICO, NOM_FISICO, 
							TIPO_DATO, NOM_DESCRIPTOR, CONSECUTIVO, LONG_DATO, PAR_DESCRIPTOR, ESTATUS, EDIT_OUTLINE, AGRUPADOR, ORDINALLEVEL) 
					    VALUES ('.$this->consecutivo.', '.$aChildDimension->cla_concepto.', '.$this->cla_descrip.', '.
							$this->Repository->ADOConnection->Quote($this->nom_logico).', '.
							$this->Repository->ADOConnection->Quote($this->nom_fisicok_bd).', '.
							$this->tipo_datok.', '.
							$this->Repository->ADOConnection->Quote($this->nom_fisico_bd).', '.
							$this->consecutivo.', '.$this->long_datok.', '.
							$this->Repository->ADOConnection->Quote($strParameters).', '.
							'1, -1, '.
							$this->Repository->ADOConnection->Quote('').', -2)';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error inserting ")." SI_CPTO_LLAVE ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					// Agrega el nuevo Join a la dimensión hija
					if ($this->dependsOnDimension > 0)
					{
						$sql = 'INSERT INTO SI_DESCRIP_KEYS (CLA_CONCEPTO, NOM_TABLA, CONSECUTIVO, NOM_FISICOK, NOM_FISICOK_JOIN) 
							VALUES ('.$aChildDimension->cla_concepto.', '.
								$this->Repository->ADOConnection->Quote($this->nom_tabla).', '.
								'1, '.
								$this->Repository->ADOConnection->Quote($this->nom_fisicok_bd).', '.
								$this->Repository->ADOConnection->Quote($this->nom_fisicok_bd).')';
						$aRS = $this->Repository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_KEYS ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
		
					// Agrega la dimensión a las jerarquías definidas
					$aHierarchyID = -1;		// -1 = No existe, -2 = Inválida, > 0 ID de jerarquía actual donde no se es hija
					$aHierarchyConsecutive = 1;
					$sql = 'SELECT DISTINCT A.CLA_JERARQUIA 
						FROM SI_JERARQUIA A 
						WHERE A.CLA_CONCEPTO = '.$aChildDimension->cla_concepto.' AND 
							A.DIM_PADRE = '.$aChildDimension->consecutivo;
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error inserting")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
	
					if ($aRS->EOF)
					{
						// No existe jerarquía donde esta dimensión hija sea un padre, pero eso no significa que se pueda
						// crear una jerarquía nueva ya que si la dimensión hija ya pertenece a otra jerarquía como hija, sería un 
						// error pues solo se soporta una jerarquía por dimensión
						$sql = 'SELECT DISTINCT A.CLA_JERARQUIA 
							FROM SI_JERARQUIA A 
							WHERE A.CLA_CONCEPTO = '.$aChildDimension->cla_concepto.' AND 
								A.DIM_HIJA = '.$aChildDimension->consecutivo;
						$aRS = $this->Repository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die("(".__METHOD__.") ".translate("Error inserting")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						if (!$aRS->EOF)
						{
							// Esta dimensión ya es hija en una jerarquía, la jerarquía nueva es inválida
							$aHierarchyID = -2;
						}
					}
					else 
					{
						$aHierarchyID = $aRS->fields["cla_jerarquia"];
						// Existe una jerarquía donde la dimensión que será hija de la que estamos creando es un padre, solo verificamos
						// que no sea también hija simultáneamente o sería un error pues solo se soporta una jerarquía por dimensión
						$sql = 'SELECT DISTINCT A.CLA_JERARQUIA 
							FROM SI_JERARQUIA A 
							WHERE A.CLA_CONCEPTO = '.$aChildDimension->cla_concepto.' AND 
								A.CLA_JERARQUIA = '.$aHierarchyID.' AND 
								A.DIM_HIJA = '.$aChildDimension->consecutivo;
						$aRS = $this->Repository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die("(".__METHOD__.") ".translate("Error inserting")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						if (!$aRS->EOF)
						{
							// Esta dimensión ya es hija en una jerarquía, la jerarquía nueva es inválida
							$aHierarchyID = -2;
						}
					}
					
					if ($aHierarchyID == -1)
					{
						// Es una jerarquía nueva, simplemente la agregamos incluso al catálogo
						$sql = 'SELECT '.
									$this->Repository->ADOConnection->IfNull('MAX(CLA_JERARQUIA)', "".$initialKey."").' + 1 AS CLA_JERARQUIA FROM SI_JERARQUIA_CAT';
						$aRS = $this->Repository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA_CAT ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						
						$temporalID = (int)$aRS->fields["cla_jerarquia"];
						
						if($temporalID<($initialKey+1))
						{
							$temporalID = $initialKey+1;
						}
						
						$aHierarchyID = $temporalID;
						
						$sql = 'INSERT INTO SI_JERARQUIA_CAT (CLA_JERARQUIA, CLA_CONCEPTO, NOM_JERARQUIA) 
							VALUES ('.$aHierarchyID.', '.$aChildDimension->cla_concepto.', '.
							$this->Repository->ADOConnection->Quote($aChildDimension->nom_logico).')';
						$aRS = $this->Repository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die("(".__METHOD__.") ".translate("Error inserting")." SI_JERARQUIA_CAT ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
	
						$sql = 'INSERT INTO SI_JERARQUIA (CLA_CONCEPTO, CLA_JERARQUIA, CONSECUTIVO, DIM_PADRE, DIM_HIJA) 
							VALUES ('.$aChildDimension->cla_concepto.', '.$aHierarchyID.', '.$aHierarchyConsecutive++.', '.
							$this->consecutivo.', '.$aChildDimension->consecutivo.')';
						$aRS = $this->Repository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die("(".__METHOD__.") ".translate("Error inserting")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						
						$sql = 'INSERT INTO SI_JERARQUIA (CLA_CONCEPTO, CLA_JERARQUIA, CONSECUTIVO, DIM_PADRE, DIM_HIJA) 
							VALUES ('.$aChildDimension->cla_concepto.', '.$aHierarchyID.', '.$aHierarchyConsecutive++.', '.
							$aChildDimension->consecutivo.', 0)';
						$aRS = $this->Repository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die("(".__METHOD__.") ".translate("Error inserting")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
					elseif ($aHierarchyID > 0) 
					{
						// Es una jerarquía existente donde la dimensión que seleccionamos como hija es la cima de la jerarquía
						// simplemente recorremos los consecutivos una posición y agregamos el nuevo registro
						if($aCube->cla_bd == MYSQL)
						{
							//@JAPR 2009-07-16: Agregado el soporte para MySQL
							//En MySQL el UPDATE del Else no funciona pues activa la validación de Primary Key, por lo que se hace un paso adicional
							$sql = 'SELECT '.$this->Repository->ADOConnection->IfNull('MAX(CONSECUTIVO)', '0').' + 10 AS CONSECUTIVO 
									FROM SI_JERARQUIA 
									WHERE CLA_CONCEPTO = '.$aChildDimension->cla_concepto.' AND 
										CLA_JERARQUIA = '.$aHierarchyID;
							$aRS = $this->Repository->ADOConnection->Execute($sql);
							if ($aRS === false)
							{
								die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							
							//Se incrementan los consecutivos a un valor suficientemente alto para asegurar que no se repitan
							$nextConsecutive = $aRS->fields["consecutivo"];
							$sql = 'UPDATE SI_JERARQUIA SET CONSECUTIVO = CONSECUTIVO + '.$nextConsecutive.'  
								WHERE CLA_CONCEPTO = '.$aChildDimension->cla_concepto.' AND 
									CLA_JERARQUIA = '.$aHierarchyID;
							$aRS = $this->Repository->ADOConnection->Execute($sql);
							if ($aRS === false)
							{
								die("(".__METHOD__.") ".translate("Error updating")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							
							//Se inserta el nuevo elemento precisamente con ese valor máximo de consecutivo usado para incrementar
							$sql = 'INSERT INTO SI_JERARQUIA (CLA_CONCEPTO, CLA_JERARQUIA, CONSECUTIVO, DIM_PADRE, DIM_HIJA) 
								VALUES ('.$aChildDimension->cla_concepto.', '.$aHierarchyID.', '.$nextConsecutive.', '.
								$this->consecutivo.', '.$aChildDimension->consecutivo.')';
							$aRS = $this->Repository->ADOConnection->Execute($sql);
							if ($aRS === false)
							{
								die("(".__METHOD__.") ".translate("Error inserting")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							
							//Finalmente se regresan los consecutivos a la secuencia original
							$nextConsecutive--;
							$sql = 'UPDATE SI_JERARQUIA SET CONSECUTIVO = CONSECUTIVO - '.$nextConsecutive.'  
								WHERE CLA_CONCEPTO = '.$aChildDimension->cla_concepto.' AND 
									CLA_JERARQUIA = '.$aHierarchyID;
							$aRS = $this->Repository->ADOConnection->Execute($sql);
							if ($aRS === false)
							{
								die("(".__METHOD__.") ".translate("Error updating")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
						}
						else
						{
							$sql = 'UPDATE SI_JERARQUIA SET CONSECUTIVO = CONSECUTIVO + 1 
								WHERE CLA_CONCEPTO = '.$aChildDimension->cla_concepto.' AND 
									CLA_JERARQUIA = '.$aHierarchyID;
							$aRS = $this->Repository->ADOConnection->Execute($sql);
							if ($aRS === false)
							{
								die("(".__METHOD__.") ".translate("Error updating")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							
							$sql = 'INSERT INTO SI_JERARQUIA (CLA_CONCEPTO, CLA_JERARQUIA, CONSECUTIVO, DIM_PADRE, DIM_HIJA) 
								VALUES ('.$aChildDimension->cla_concepto.', '.$aHierarchyID.', '.$aHierarchyConsecutive++.', '.
								$this->consecutivo.', '.$aChildDimension->consecutivo.')';
							$aRS = $this->Repository->ADOConnection->Execute($sql);
							if ($aRS === false)
							{
								die("(".__METHOD__.") ".translate("Error inserting")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
						}
					}
					
					// Actualiza el Cache de Artus
					$sql = 'DELETE FROM SI_CACHE_METADATA 
						WHERE CLA_CACHE IN (2) AND CLA_CONCEPTO = '.$aChildDimension->cla_concepto;
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error deleting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
		
					$sql = 'DELETE FROM SI_CACHE_METADATA 
						WHERE CLA_CACHE IN (7)';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error deleting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					$strCacheDate = formatADateTime(date('Y-m-d H:i:s'),'yyyy-mm-dd hh:mm:ss');
					$sql = 'INSERT INTO SI_CACHE_METADATA (CLA_CACHE, CLA_CONCEPTO, FECHA) 
						VALUES (2, '.$aChildDimension->cla_concepto.', '.
						$this->Repository->ADOConnection->Quote($strCacheDate).')';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error inserting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					$sql = 'INSERT INTO SI_CACHE_METADATA (CLA_CACHE, CLA_CONCEPTO, FECHA) 
						VALUES (7, 0, '.
						$this->Repository->ADOConnection->Quote($strCacheDate).')';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error inserting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					/*
					// Agrega la columna de la Dimensión a todas las agregaciones (solo si el cubo usa Agregados)
					if ($aCube->usa_agregacion)
					{
						$strNewFieldDefinition = $this->nom_fisico_bd.$this->consecutivo.' '.$strDataType;
						$strNewKeyFieldDefinition = $this->nom_fisicok_bd.$this->consecutivo.' '.$strDataTypeK;
						$aPeriodDimensionArray = array();
						$aAggregationArray = array();
						//$aAggregationArray[] = array(0, $aCube->catalogo_olap);
						$blnUseNewAggregations = $aCube->ag_new;
						$aAggregationCollection = BITAMAggregationCollection::NewInstance($this->Repository, null, $aChildDimension->cla_concepto, true);
						if (!is_null($aAggregationCollection))
						{
							foreach ($aAggregationCollection as $aAggregation)
							{
								$aAggregationArray[] = array($aAggregation->consecutivo, $aAggregation->nom_fisico_bd);
							}
						}
						
						$aPeriodDimensionCollection = BITAMDimensionCollection::NewInstancePeriodDims($this->Repository, $this->cla_concepto, false);
						if (!is_null($aPeriodDimensionCollection))
						{
							foreach ($aPeriodDimensionCollection as $aPeriodDimension)
							{
								$aPeriodDimensionArray[] = $aPeriodDimension->cla_periodo;
							}
						}
						
						// Recorre cada Id de agregación
						$arrTableArray = array();
						foreach ($aAggregationArray as $aAggregation)
						{
							if (trim($aAggregation[1]) != '')
							{
								// Este es un agregado personalizado del usuario, solo hay una tabla sin importar la cantidad de periodos
								$arrTableArray[] = $aAggregation[1];
							}
							else
							{
								// Recorre cada periodo para terminar de formar el nombre de tabla, ya que se trata de agregados propietarios
								foreach ($aPeriodDimensionArray as $aPeriodDimension)
								{
									if ($blnUseNewAggregations)
									{
										$arrTableArray[] = 'AN_'.$aChildDimension->cla_concepto.'_'.$aPeriodDimension.'_'.$aAggregation[0];
									}
									else 
									{
										$arrTableArray[] = 'A_'.$aChildDimension->cla_concepto.'_'.$aPeriodDimension.'_'.$aAggregation[0];
									}
								}
							}
						}
		
						// Inserta el campo en las tablas de los agregados
						if (count($arrTableArray) > 0)
						{
							foreach ($arrTableArray as $strTableName)
							{
								$sql =  "ALTER TABLE ".$strTableName.' ADD '.$strNewKeyFieldDefinition;
								$aRS = $this->Repository->DataADOConnection->Execute($sql);
								if ($aRS === false)
								{
									die("(".__METHOD__.") ".translate("Error modifying")." ".$strTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
								}
		
								$sql =  "ALTER TABLE ".$strTableName.' ADD '.$strNewFieldDefinition;
								$aRS = $this->Repository->DataADOConnection->Execute($sql);
								if ($aRS === false)
								{
									die("(".__METHOD__.") ".translate("Error modifying")." ".$strTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
								}
							}
						}
					}
					*/
					
					$this->Repository->addPhPCacheRequest(MDOBJECT_Cube, $aChildDimension->cla_concepto, MDOBJECTDATA_All);
					//Agrega el cubo a la lista de los que se requiere reprocesar debido a que se agregó una dimensión
					$arrCubesToExport[] = $aChildDimension->cla_concepto;
				}
			}
			//@JAPR
			
			//@JAPRWarning: Funcionalidad faltante
			//@JAPR 2009-07-15: Agregada la exportación automática de Metadata de Cubos a PhP al agregar dimensiones
			//if(count($arrCubesToExport) > 0)
			//{
			//	require_once('cgenmetadata.inc.php');
			//	
			//	$theGenMetadata = cgenmetadata::NewInstance($this->Repository->ADOConnection);
			//	foreach ($arrCubesToExport as $aCubeID)
			//	{
			//		$theGenMetadata->ExportCube($aCubeID);
			//	}
			//}
		}
		else
		{
				
			/*
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_ATRIB ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			*/
		}
		return $this;
	}
	
	function get_FormIDFieldName()
	{
		return 'consecutivo';
	}
	
	function get_QueryString()
	{
		global $appSettings;
		
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Dimension&CubeID=".$this->cla_concepto;
		}
		else
		{
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			/*
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				return "BITAM_PAGE=Dimension&CubeID=".$this->cla_concepto.'&DescripID='.$this->cla_descrip;
			}
			else
			{
			*/
				return "BITAM_PAGE=Dimension&CubeID=".$this->cla_concepto.'&consecutivo='.$this->consecutivo;
			//}
			//@JAPR
		}
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Dimension");
		}
		else
		{  
			//@JAPR 2012-06-20: Agregado el enlace al catálogo desde los valores de la dimensión
			if ($this->isCatalog)
			{
				return $this->nom_logico.' ('.translate('Values').')';
			}
			else 
			{
				return $this->nom_logico;
			}
		}
	}

	function isNewObject()
	{
		global $appSettings;
		
		return ($this->consecutivo < 0);
	}

	function get_Parent()
	{
		global $appSettings;
		
		if ($appSettings["Cubes"] == 1)
		{
			if ($this->cla_concepto > 0)
			{
				return BITAMCube::NewInstanceWithID($this->Repository,$this->cla_concepto);
			}
			else 
			{
				return $this->Repository;
			}
		}
		else
		{
			//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
			if ($this->isCatalog)
			{
				//@JAPR 2012-06-20: Agregado el enlace al catálogo desde los valores de la dimensión
				require_once('catalog.inc.php');
				
				$aParentInstance = BITAMCatalog::NewInstanceWithParentID($this->Repository, $this->cla_descrip);
				if (!is_null($aParentInstance))
				{
					return $aParentInstance;
				}
				//@JAPR
				
				return $this->Repository;
			}
			else
			{
				return BITAMDimensionCollection::NewInstance($this->Repository,null,0,true,false,false,true);
			}
			//@JAPR
		}
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	
 	function addButtons($aUser)
	{
		//@JAPR 2009-07-09: Validado que si el campo clave y descriptor son los mismos, no permita crearles dimensiones Padre
		$bSameKeyDescField = strtolower($this->nom_fisico) == (strtolower($this->nom_fisicok));
		//@JAPR 2010-01-27: Validado que a las dimensiones Atributo no se les permita crear dimensiones Padre
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		if (!$this->isNewObject() && !$this->IsAttribute && !$this->isCatalog) // && !$bSameKeyDescField)
		{
	?>
			<!--<a href="#" class="alinkescfav" onclick="createParentDimension();"><img src="images/parentchild1.gif" displayMe=1> <?=translate("Create Parent Dimension")?> </a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp-->
			<button id="btnParentDimension" class="alinkescfav" onclick="createParentDimension();"><img src="images/parentchild1.gif" alt="<?=translate('Create Parent Dimension')?>" title="<?=translate('Create Parent Dimension')?>" displayMe="1" /> <?=translate("Create Parent Dimension")?></button>
	<?
		}
	}

	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		return false;
	}

	function get_PathArray()
	{
		//@JAPR 2012-06-20: Agregado el enlace al catálogo desde los valores de la dimensión
		if ($this->cla_concepto > 0 || $this->isCatalog)
		{
			$parent = $this->get_Parent();
			$pathArray = $parent->get_PathArray();
		}
		else 
		{
			$pathArray = array();
		}
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		if ($this->isNewObject())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "nom_logico";
			$aField->Title = translate("Name");
			$aField->Type = "String";
			$aField->Size = 60;
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "nom_tabla";
			$aField->Title = translate("Table");
			$aField->Type = "String";
			$aField->Size = 60;
			$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
			
			$aCube = BitamCube::NewInstanceWithID($this->Repository, $this->cla_concepto);
			$aDataTypesArray = array();
			if (!is_null($aCube))
			{
				$sql = 'SELECT TIPO_DATO, NOM_DATO FROM SI_BD_TIPOS 
					WHERE CLA_BD = '.$aCube->cla_bd.' AND TIPO_DATO IN (7, 8)';
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_BD_TIPOS ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				while (!$aRS->EOF)
				{
					$aDataTypesArray[(int) $aRS->fields["tipo_dato"]] = $aRS->fields["nom_dato"];
					$aRS->MoveNext();
				}
			}
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "nom_fisicok_bd";
			$aField->Title = translate("Key Name");
			$aField->Type = "String";
			$aField->Size = 60;
			$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "tipo_datok";
			$aField->Title = translate("Key Data Type");
			$aField->Type = "Object";
			$aField->OnChange = "showLengthField(1)";
			$aField->VisualComponent = "ComboBox";
			$aField->Options = $aDataTypesArray;
			$aField->Size = 20;
			$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "long_datok";
			$aField->Title = translate("Key Length");
			$aField->Type = "String";
			$aField->Size = 10;
			$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
			
			$aDataTypesArray = array();
			if (!is_null($aCube))
			{
				$sql = 'SELECT TIPO_DATO, NOM_DATO FROM SI_BD_TIPOS 
					WHERE CLA_BD = '.$aCube->cla_bd.' AND TIPO_DATO IN (2)';
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_BD_TIPOS ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				while (!$aRS->EOF)
				{
					$aDataTypesArray[(int) $aRS->fields["tipo_dato"]] = $aRS->fields["nom_dato"];
					$aRS->MoveNext();
				}
			}
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "nom_fisico_bd";
			$aField->Title = translate("Descriptor Name");
			$aField->Type = "String";
			$aField->Size = 60;
			$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "tipo_dato";
			$aField->Title = translate("Descriptor Data Type");
			$aField->Type = "Object";
			$aField->OnChange = "showLengthField(2)";
			$aField->VisualComponent = "ComboBox";
			$aField->Options = $aDataTypesArray;
			$aField->Size = 20;
			$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
	
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "long_dato";
			$aField->Title = translate("Descriptor Length");
			$aField->Type = "String";
			$aField->Size = 10;
			$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "default_key";
			$aField->Title = translate("No Apply Value");
			$aField->Type = "String";
			$aField->Size = 60;
			$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
			
			if ($this->isNewObject())
			{
				if (is_null($this->ChildDescripKey))
				{
					$aDimensionArray = array();
					$aDimensionCollection = BITAMDimensionCollection::NewInstance($this->Repository,null,$this->cla_concepto,true,false,false);
					if (!is_null($aDimensionCollection))
					{
						foreach ($aDimensionCollection as $aDimension)
						{
							if (count($aDimension->FatherParDescriptor) == 0)
							{
								$aDimensionArray[(int) $aDimension->consecutivo] = $aDimension->nom_logico;
							}
						}
					}
					
					if (count($aDimensionArray) > 0)
					{
						$aField = BITAMFormField::NewFormField();
						$aField->Name = "dependsOnDimension";
						$aField->Title = translate("Child dimension");
						$aField->Type = "Object";
						$aField->VisualComponent = "ComboBox";
						$aField->Options = $aDimensionArray;
						$aField->Size = 60;
						$myFields[$aField->Name] = $aField;
					}
				}
				else 
				{
					$aDimensionArray = array();
					$aChildDimension = BITAMDimension::NewInstanceWithDescripID($this->Repository,$this->ChildDescripKey,$this->cla_concepto,false);
					if (!is_null($aChildDimension))
					{
						$aDimensionArray[(int) $aChildDimension->cla_descrip] = $aChildDimension->nom_logico;
					}
					if (count($aDimensionArray) > 0)
					{
						$aField = BITAMFormField::NewFormField();
						$aField->Name = "ChildDescripKey";
						$aField->Title = translate("Child dimension");
						$aField->Type = "Object";
						$aField->VisualComponent = "ComboBox";
						$aField->Options = $aDimensionArray;
						$aField->Size = 60;
						$myFields[$aField->Name] = $aField;
					}
				}
			}
		}
		
		return $myFields;
	}

	function updateFromArray($anArray)
	{
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if ($appSettings["UseGlobalDimensions"] == 0)
		{
			if (array_key_exists("consecutivo", $anArray))
			{
				$this->consecutivo = (int) $anArray["consecutivo"];
			}
		}
		//@JAPR

		if (array_key_exists("cla_descrip", $anArray))
		{
			$this->cla_descrip = (int) $anArray["cla_descrip"];
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				$this->consecutivo = (int) $anArray["cla_descrip"];
			}
			//@JAPR
		}

	 	if (array_key_exists("nom_logico", $anArray))
		{
			$this->nom_logico = $anArray["nom_logico"];
		}
		
	 	if (array_key_exists("nom_tabla", $anArray))
		{
			$this->nom_tabla = $anArray["nom_tabla"];
		}
		
	 	if (array_key_exists("nom_fisico_bd", $anArray))
		{
			$this->nom_fisico_bd = $anArray["nom_fisico_bd"];
			$this->nom_fisico = strtolower($anArray["nom_fisico_bd"]);
		}
		
		if (array_key_exists("tipo_dato", $anArray))
		{
			$this->tipo_dato = (int) $anArray["tipo_dato"];
		}
		
	 	if (array_key_exists("long_dato", $anArray))
		{
			$this->long_dato = (int) $anArray["long_dato"];
		}
		
	 	if (array_key_exists("nom_fisicok_bd", $anArray))
		{
			$this->nom_fisicok_bd = $anArray["nom_fisicok_bd"];
			$this->nom_fisicok = strtolower($anArray["nom_fisicok_bd"]);
		}
		
		if (array_key_exists("tipo_datok", $anArray))
		{
			$this->tipo_datok = (int) $anArray["tipo_datok"];
		}
		
	 	if (array_key_exists("long_datok", $anArray))
		{
			$this->long_datok = (int) $anArray["long_datok"];
		}

		if (array_key_exists("default_key", $anArray))
		{
			$this->NoApplyValue = $anArray["default_key"];
		}

		if (array_key_exists("dependsOnDimension", $anArray))
		{
			$this->dependsOnDimension = (int) $anArray["dependsOnDimension"];
		}
		
		if (array_key_exists("ChildDescripKey", $anArray))
		{
			$this->ChildDescripKey = (int) $anArray["ChildDescripKey"];
		}
		
		return $this;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{	
?>
	 	<script language="JavaScript">

	 	function showLengthField(iFieldID)
	 	{
	 		arrDataTypes = new Array();
	 		switch (iFieldID)
	 		{
	 			case 2:
			 		objDataTypeCmb = BITAMDimension_SaveForm.tipo_dato;
			 		objRowDataLength = document.getElementById("Row_long_dato");
			 		objDataField = BITAMDimension_SaveForm.long_dato;
			 		arrDataTypes[1] = 1;
			 		arrDataTypes[2] = 1;
			 		break;
			 	default:
			 		objDataTypeCmb = BITAMDimension_SaveForm.tipo_datok;
			 		objRowDataLength = document.getElementById("Row_long_datok");
			 		objDataField = BITAMDimension_SaveForm.long_datok;
			 		arrDataTypes[1] = 1;
			 		arrDataTypes[2] = 1;
			 		//arrDataTypes[21] = 1;
			 		//arrDataTypes[24] = 1;
			 		break;
	 		}
	 		intDataType = parseInt(objDataTypeCmb.options[objDataTypeCmb.selectedIndex].value);
 			if (objRowDataLength != null)
 			{
		 		if (intDataType in arrDataTypes)
		 		{
					objRowDataLength.style.display = 'inline';
		 		}
		 		else
		 		{
	 				objRowDataLength.style.display = 'none';
		 		}
 			}
	 	}
	 	
	 	
	 	function canSave(target)
	 	{
	 		if(Trim(BITAMDimension_SaveForm.nom_logico.value)=='')
			{
				BITAMDimension_SaveForm.nom_logico.focus();
				alert('<?=translate('Data in required field is missing')?>. <?=translate('Field')?>: \'<?=translate('Name')?>\'');
				return false;
			}
			
			/*
			if(Trim(BITAMDimension_SaveForm.nom_tabla.value)=='')
			{
				BITAMDimension_SaveForm.nom_tabla.focus();
				alert('<?=translate('Data in required field is missing')?>. <?=translate('Field')?>: \'<?=translate('Table Name')?>\'');
				return false;
			}
	 		
			if(Trim(BITAMDimension_SaveForm.nom_fisicok_bd.value)=='')
			{
				BITAMDimension_SaveForm.nom_fisicok_bd.focus();
				alert('<?=translate('Data in required field is missing')?>. <?=translate('Field')?>: \'<?=translate('Key Name')?>\'');
				return false;
			}
	 		
			if(Trim(BITAMDimension_SaveForm.nom_fisico_bd.value)=='')
			{
				BITAMDimension_SaveForm.nom_fisico_bd.focus();
				alert('<?=translate('Data in required field is missing')?>. <?=translate('Field')?>: \'<?=translate('Descriptor Name')?>\'');
				return false;
			}
			*/
	 		
			objRegExp = /[\[{|}\]]/;
			arrMatches = objRegExp.exec(BITAMDimension_SaveForm.nom_logico.value);
			anArrayLength = 0;
			if (arrMatches != null)
			{
				anArrayLength = arrMatches.length;
			}
			strMatches = '';
			for (i=0; i<anArrayLength; i++)
			{
				strMatches += arrMatches[i];
			}
			if (strMatches.length > 0)
			{
				alert('<?=translate('Invalid characters were found on a required field')?>: \''+strMatches+'\'. <?=translate('Field')?>: \'<?=translate('Name')?>\'');
				BITAMDimension_SaveForm.nom_logico.focus();
				return false;
			}
			
			/*
			objRegExp = /\W/;
			arrMatches = objRegExp.exec(BITAMDimension_SaveForm.nom_tabla.value);
			anArrayLength = 0;
			if (arrMatches != null)
			{
				anArrayLength = arrMatches.length;
			}
			strMatches = '';
			for (i=0; i<anArrayLength; i++)
			{
				strMatches += arrMatches[i];
			}
			if (strMatches.length > 0)
			{
				BITAMDimension_SaveForm.nom_tabla.focus();
				alert('<?=translate('Invalid characters were found on a required field')?>: \''+strMatches+'\'. <?=translate('Field')?>: \'<?=translate('Table Name')?>\'');
				return false;
			}

			objRegExp = /\W/;
			arrMatches = objRegExp.exec(BITAMDimension_SaveForm.nom_fisicok_bd.value);
			anArrayLength = 0;
			if (arrMatches != null)
			{
				anArrayLength = arrMatches.length;
			}
			strMatches = '';
			for (i=0; i<anArrayLength; i++)
			{
				strMatches += arrMatches[i];
			}
			if (strMatches.length > 0)
			{
				BITAMDimension_SaveForm.nom_fisicok_bd.focus();
				alert('<?=translate('Invalid characters were found on a required field')?>: \''+strMatches+'\'. <?=translate('Field')?>: \'<?=translate('Key Name')?>\'');
				return false;
			}

			objRegExp = /\W/;
			arrMatches = objRegExp.exec(BITAMDimension_SaveForm.nom_fisico_bd.value);
			anArrayLength = 0;
			if (arrMatches != null)
			{
				anArrayLength = arrMatches.length;
			}
			strMatches = '';
			for (i=0; i<anArrayLength; i++)
			{
				strMatches += arrMatches[i];
			}
			if (strMatches.length > 0)
			{
				BITAMDimension_SaveForm.nom_fisico_bd.focus();
				alert('<?=translate('Invalid characters were found on a required field')?>: \''+strMatches+'\'. <?=translate('Field')?>: \'<?=translate('Descriptor Name')?>\'');
				return false;
			}
			*/
			
	 		objDataTypeCmb = BITAMDimension_SaveForm.tipo_dato;
	 		intDataType = parseInt(objDataTypeCmb.options[objDataTypeCmb.selectedIndex].value);
	 		switch (intDataType)
	 		{
				case 1:
				case 2:
			 		intLength = parseInt(BITAMDimension_SaveForm.long_dato.value);
			 		if (isNaN(intLength))
			 		{
						alert('<?=translate('Invalid value')?>. <?=translate('Field')?>: \'<?=translate('Descriptor Length')?>\'');
						return false;
			 		}
			 		else
			 		{
			 			if (intLength <= 0)
			 			{
						alert('<?=translate('Invalid value')?>. <?=translate('Field')?>: \'<?=translate('Descriptor Length')?>\'');
						return false;
			 			}
			 		}
					break;
	 			case 7:
	 				BITAMDimension_SaveForm.long_dato.value = '';
	 				break;
	 			default:
	 				break;
	 		}
	 		objDataTypeCmb = BITAMDimension_SaveForm.tipo_datok;
	 		intDataTypeK = parseInt(objDataTypeCmb.options[objDataTypeCmb.selectedIndex].value);
	 		switch (intDataTypeK)
	 		{
				case 1:
				case 2:
			 		intLength = parseInt(BITAMDimension_SaveForm.long_datok.value);
			 		if (isNaN(intLength))
			 		{
						alert('<?=translate('Invalid value')?>. <?=translate('Field')?>: \'<?=translate('Key Length')?>\'');
						return false;
			 		}
			 		else
			 		{
			 			if (intLength <= 0)
			 			{
						alert('<?=translate('Invalid value')?>. <?=translate('Field')?>: \'<?=translate('Key Length')?>\'');
						return false;
			 			}
			 		}
					break;
	 			//case 21:
	 			//case 24:
	 				//break;
	 			default:
	 				BITAMDimension_SaveForm.long_datok.value = '';
	 				break;
	 		}
	 		
			if (Trim(BITAMDimension_SaveForm.long_datok.value) != '')
			{
				//objRegExp = /[^\d|\,]/;
				objRegExp = /[^\d]/;
				arrMatches = objRegExp.exec(BITAMDimension_SaveForm.long_datok.value);
				anArrayLength = 0;
				if (arrMatches != null)
				{
					anArrayLength = arrMatches.length;
				}
				strMatches = '';
				for (i=0; i<anArrayLength; i++)
				{
					strMatches += arrMatches[i];
				}
				if (strMatches.length > 0)
				{
					BITAMDimension_SaveForm.long_datok.focus();
					alert('<?=translate('Invalid characters were found on a required field')?>: \''+strMatches+'\'. <?=translate('Field')?>: \'<?=translate('Key Length')?>\'');
					return false;
				}
			}

			if (Trim(BITAMDimension_SaveForm.long_dato.value) != '')
			{
				//objRegExp = /[^\d|\,]/;
				objRegExp = /[^\d]/;
				arrMatches = objRegExp.exec(BITAMDimension_SaveForm.long_dato.value);
				anArrayLength = 0;
				if (arrMatches != null)
				{
					anArrayLength = arrMatches.length;
				}
				strMatches = '';
				for (i=0; i<anArrayLength; i++)
				{
					strMatches += arrMatches[i];
				}
				if (strMatches.length > 0)
				{
					BITAMDimension_SaveForm.long_dato.focus();
					alert('<?=translate('Invalid characters were found on a required field')?>: \''+strMatches+'\'. <?=translate('Field')?>: \'<?=translate('Descriptor Length')?>\'');
					return false;
				}
			}
			intDependsOnDimension = 0;
			intChildDescripKey = 0;
		<?
		if (is_null($this->ChildDescripKey))
		{
		?>
	 		objDimensionCmb = BITAMDimension_SaveForm.dependsOnDimension;
	 		intDependsOnDimension = parseInt(objDimensionCmb.selectedIndex);
	 		if (intDependsOnDimension < 0)
	 		{
				alert('<?=translate('You must specify a valid child dimension to create a hierarchy')?>');
				return false;
	 		}
	 		else
	 		{
		 		intDependsOnDimension = parseInt(objDimensionCmb.options[objDimensionCmb.selectedIndex].value);
	 		}
	 	<?
		}
		else 
		{
		?>
	 		objDimensionCmb = BITAMDimension_SaveForm.ChildDescripKey;
	 		intChildDescripKey = parseInt(objDimensionCmb.selectedIndex);
	 		if (intChildDescripKey < 0)
	 		{
				alert('<?=translate('You must specify a valid child dimension to create a hierarchy')?>');
				return false;
	 		}
	 		else
	 		{
		 		intChildDescripKey = parseInt(objDimensionCmb.options[objDimensionCmb.selectedIndex].value);
	 		}
	 	<?
		}
	 	?>
			
	 		var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('<?=translate('XML Object not supported')?>');
				return false;
			}
			
			xmlObj.open('GET', 'validateDimension.php?CubeID=<?=$this->cla_concepto?>&DataType='+intDataType+'&DataTypeK='+intDataTypeK+'&DependsOnDimension='+intDependsOnDimension+'&ChildDescripKey='+intChildDescripKey+'&TableName='+escape(BITAMDimension_SaveForm.nom_tabla.value)+'&KeyName='+escape(BITAMDimension_SaveForm.nom_fisicok_bd.value)+'&DescName='+escape(BITAMDimension_SaveForm.nom_fisico_bd.value)+'&Label='+escape(BITAMDimension_SaveForm.nom_logico.value), false);
	 		xmlObj.send(null);
			if(xmlObj.status == 404)
			{
				alert('<?=translate('Unable to launch validation process')?>');
				return false;
			}
			
	 		strResponseData = Trim(unescape(xmlObj.responseText));
	 		arrResponseData = strResponseData.split('<SEP>');
	 		if(arrResponseData.length == 0)
	 		{
	 			alert('<?=translate('Error found during data validation')?>: <?=translate('404 - File not found')?>');
				return false;
	 		}
	 		
	 		if(arrResponseData[0]!=0)
	 		{
				strErrMsg = arrResponseData[0];
				if(arrResponseData.length > 1)
				{
					strErrMsg = arrResponseData[1];
				}
				
	 			alert('<?=translate('Error found during data validation')?>: ' + strErrMsg);
	 		}
	 		else
	 		{	
	 			BITAMDimension_Ok(target);
	 		}
	 	}
	 	
		function createParentDimension()
		{
			/*
			var answer = confirm('Do you want to close all selected Budget Periods?');
			if (answer)
			{
			*/
			frmCreateParent.ChildDescripKey.value=<?=$this->cla_descrip?>;
			frmCreateParent.submit();
			/*
			}
			*/
		}
	 	
		</script> 		
<?
 	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
 	{
?>
	 	<script language="JavaScript">
 			objOkSelfButton = document.getElementById("BITAMDimension_OkSelfButton");
 			objOkParentButton = document.getElementById("BITAMDimension_OkParentButton");
 			objOkSelfButton.onclick=new Function("canSave('self');");
 			objOkParentButton.onclick=new Function("canSave('parent');");
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMDimension_OkNewButton");
 			objOkNewButton.onclick=new Function("canSave('new');");
<?
		}
?>
		</script> 	
		<form name="frmCreateParent" action="main.php?BITAM_PAGE=Dimension&CubeID=<?=$this->cla_concepto?>&ChildDescripKey=<?=$this->cla_descrip?>" method="post" target="body">
		<input type="hidden" name="BITAM_SECTION" value="Dimension">
		<input type="hidden" name="ChildDescripKey" value="-1">
		</form>
	
<?
	}	

	function get_Children($aUser)
	{
		global $appSettings;
		
		$myChildren = array();
		if (!$this->isNewObject())
		{
			$myChildren[] = BITAMDimensionValueCollection::NewInstance($this->Repository,null,$this->consecutivo,$this->cla_concepto,true,true,true,$this->dimensionValuesFilter,$this->maxDimensionValueToShow, $this->showNullParentValues);
			if($appSettings["DimensionFields"] == 1)
			{
				$myChildren[] = BITAMDimensionTableFieldCollection::NewInstance($this->Repository, $this->consecutivo, $this->cla_concepto);
			}
		}
		return $myChildren;
	}
	
	static function getClaDescripFromDimensionIDsAndCubeID($aRepository, $dimensionIDs, $aCubeID)
	{
		$arrayDimensions = array();
		$strDimensionsWithComma="";
		$numDimensions = count($dimensionIDs);
		
		for($i=0; $i<$numDimensions; $i++)
		{
			if($strDimensionsWithComma!="")
			{
				$strDimensionsWithComma.=", ";
			}
			
			$strDimensionsWithComma.= $dimensionIDs[$i];
		}
		
		if($strDimensionsWithComma!="")
		{
		
			$sql = "SELECT CLA_DESCRIP FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = ".$aCubeID." AND CONSECUTIVO IN ( ".$strDimensionsWithComma." ) ORDER BY CONSECUTIVO";
			
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ". $aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		
			while (!$aRS->EOF)
			{
				$arrayDimensions[] = (int) $aRS->fields["cla_descrip"];
				$aRS->MoveNext();
			}
		
		}
		
		return $arrayDimensions;
	}
	
	static function getClaDescripIndexByDimFromDimIDsAndCubeID($aRepository, $dimensionIDs, $aCubeID)
	{
		$arrayDimensions = array();
		$strDimensionsWithComma="";
		$numDimensions = count($dimensionIDs);
		
		for($i=0; $i<$numDimensions; $i++)
		{
			if($strDimensionsWithComma!="")
			{
				$strDimensionsWithComma.=", ";
			}
			
			$strDimensionsWithComma.= $dimensionIDs[$i];
		}
		
		if($strDimensionsWithComma!="")
		{
			$sql = "SELECT CLA_DESCRIP, CONSECUTIVO FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = ".$aCubeID." AND CONSECUTIVO IN ( ".$strDimensionsWithComma." ) ORDER BY CONSECUTIVO";
			
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ". $aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		
			while (!$aRS->EOF)
			{
				$intDimID = (int) $aRS->fields["consecutivo"];
				$arrayDimensions[$intDimID] = (int) $aRS->fields["cla_descrip"];
				$aRS->MoveNext();
			}
		}

		return $arrayDimensions;
	}
	
	static function getAllClaDescripIndexByDimFromCubeID($aRepository, $aCubeID)
	{
		$arrayDimensions = array();
		
		$sql = "SELECT CLA_DESCRIP, CONSECUTIVO FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = ".$aCubeID." ORDER BY CONSECUTIVO";
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ". $aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	
		while (!$aRS->EOF)
		{
			$intDimID = (int) $aRS->fields["consecutivo"];
			$arrayDimensions[$intDimID] = (int) $aRS->fields["cla_descrip"];
			$aRS->MoveNext();
		}

		return $arrayDimensions;
	}
	
	static function getDimIDByClaDescrip($aRepository, $claDescrip, $aCubeID)
	{
		$consecutivo = null;
		
		if($claDescrip!==null)
		{
			$sql = "SELECT CLA_DESCRIP, CONSECUTIVO FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = ".$aCubeID." AND CLA_DESCRIP =".$claDescrip;
			
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ". $aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		
			if (!$aRS->EOF)
			{
				$consecutivo = (int) $aRS->fields["consecutivo"];
			}
		}
		
		return $consecutivo;
	}
	
	function getDimensionsWithOutDependence($aRepository, $aUser)
	{
		$allValues = array();
		
		$sql = 
"
SELECT
	t1.CLA_CONCEPTO *1000 + t1.CONSECUTIVO AS CubeDimensionID,
	t1.NOM_LOGICO AS DimensionName

FROM 	
	SI_CPTO_LLAVE t1,
	SI_CONCEPTO t2,
	SI_INDICADOR t3,
	SI_GPO_IND_DET t4, 
	SI_USR_IND t5
WHERE	
	t1.CLA_CONCEPTO = t2.CLA_CONCEPTO
AND	t2.CLA_CONCEPTO = t3.CLA_CONCEPTO
AND	t3.CLA_INDICADOR = t4.CLA_INDICADOR
AND	t4.CLA_GPO_IND = t5.CLA_GPO_IND
AND	t2.ESTATUS = 1
AND (t1.CLA_PERIODO<0 OR (".$aRepository->ADOConnection->IfNull("t1.CLA_PERIODO", "-1")."=-1))
AND	t5.CLA_USUARIO = ".((int) $aUser->UserID)."
AND t1.PAR_DESCRIPTOR NOT LIKE ('%|%')
UNION
SELECT

	t1.CLA_CONCEPTO *1000 + t1.CONSECUTIVO AS CubeDimensionID,
	t1.NOM_LOGICO AS DimensionName

FROM 	
	SI_CPTO_LLAVE t1,
	SI_CONCEPTO t2,
	SI_INDICADOR t3, 
	SI_GPO_IND_DET t4, 
	SI_ROL_IND t5,
	SI_ROL_USUARIO t6
WHERE	
	t1.CLA_CONCEPTO = t2.CLA_CONCEPTO
AND	t2.CLA_CONCEPTO = t3.CLA_CONCEPTO
AND	t3.CLA_INDICADOR = t4.CLA_INDICADOR
AND	t4.CLA_GPO_IND = t5.CLA_GPO_IND
AND	t5.CLA_ROL = t6.CLA_ROL
AND	t2.ESTATUS = 1
AND (t1.CLA_PERIODO<0 OR (".$aRepository->ADOConnection->IfNull("t1.CLA_PERIODO", "-1")."=-1))
AND	t6.CLA_USUARIO = ".((int) $aUser->UserID)."
AND t1.PAR_DESCRIPTOR NOT LIKE ('%|%')
ORDER BY 1,2";

			$aRS = $aRepository->ADOConnection->Execute($sql);
		
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error")." ".translate("in")." getSelectedDimensions() ".translate("Function").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				$allValues[$aRS->fields["cubedimensionid"]] = $aRS->fields["dimensionname"];
				$aRS->MoveNext();
			}
		
		return $allValues;
		
	}

	//Retorna el valor del campo join del catálogo con base en el valor de la clave de la dimensión
	static function getDimJoinValueByKeyValue($aRepository, $descriptKeysCollection, $nom_tabla, $nom_fisicok_bd, $tipo_datok, $keyValue)
	{
		//ISUAREZ 21-OCTUBRE-2009
		//El modelo financiero tiene un caso especial, en el cual aunque se acceda a las dimensiones
		//de los niveles de las cuentas del catalogo maestro y te devuelva correctamente el campo join
		//que en este caso es la cuenta afectable "AffectableAccount" porque ya en EktosBS se soportan 
		//llaves subrogadas, al momento de hacer el join con el catalogo de niveles se forma el siguiente
		//query para obtener la clave correcta: 
		//SELECT AffectableAccount FROM SI_EKT_MasterAccountsLevels WHERE Level_3 = 'F1200'
		//Pero como sabemos en el modelo financiero el campo Level_3 puede tener muchos registros
		//repetidos con el valor 'F1200' ya que la cuenta 'F1200' puede tener varias cuenta hijas, 
		//por lo tanto en la consulta puede regresar varios registros con diferentes valores
		//en el campo AffectableAccount los cuales pueden ser como la misma cuenta procesada 'F1200'
		//así como cuenta hijas 'F1210', 'F1220', 'F1230' y el orden en como los regrese puede ser
		//aleatorio y en este caso puede tomar como llave alguna cuenta hija cuando en realidad no
		//es la que se esta procesando en el presupuesto
		
		//Validacion con el modelo financiero - catalogo de cuentas maestro
		$temporalTable = strtoupper($nom_tabla);
		$temporalNomFisicokBD = strtoupper($nom_fisicok_bd);
		$masterAccounstTable = "SI_EKT_MASTERACCOUNTSLEVELS";
		$joinMasterAccountsTable = "AFFECTABLEACCOUNT";
		
		if($temporalTable==$masterAccounstTable && $temporalNomFisicokBD!=$joinMasterAccountsTable)
		{
			//Como se ha venido manejando en el modelo financiero se manejan en los
			//niveles claves varchar
			$dimJoinValue = $aRepository->DataADOConnection->Quote($keyValue);
			
			return $dimJoinValue;
		}
		
		$dimJoinValue = null;
		$cubeID = $descriptKeysCollection->cla_concepto;
		
		$dimJoinValue = BITAMGlobalInstance::GetDimJoinValueByKeyValue($cubeID, $nom_tabla, $nom_fisicok_bd, $tipo_datok, $keyValue);
		
		if (!is_null($dimJoinValue))
		{
			return $dimJoinValue;
		}

		//Obtener campo join del catálogo
		$joinCatalog = getJoinFieldByTableName($nom_tabla, $nom_fisicok_bd, $descriptKeysCollection, FALSE, $keyEqualToCatJoin);

		if($tipo_datok==2 || $tipo_datok==1)
		{
			$selectKeyValue = $aRepository->DataADOConnection->Quote($keyValue);
		}
		else 
		{
			$selectKeyValue = $keyValue;
		}
		
		//Obtener el valor del campo join
		$sql = "SELECT ".$joinCatalog." FROM ".$nom_tabla." WHERE ".$nom_fisicok_bd." = ".$selectKeyValue;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." function getDimJoinValueByKeyValue() ".$nom_tabla." ".translate("table").": ". $aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	
		if (!$aRS->EOF)
		{
			$dimJoinValue = $aRS->fields[strtolower($joinCatalog)];
					
			//Obtener el tipo de dato del campo join en el catálogo
			$metaColumns = $aRepository->GetTableSchema($nom_tabla, true);
			$fieldType = $metaColumns[strtoupper($joinCatalog)]->type;
			
			if($fieldType=="C")
			{
				$dimJoinValue = $aRepository->DataADOConnection->Quote($dimJoinValue);
			}
		}
		
		BITAMGlobalInstance::AddDimJoinValueByKeyValue($cubeID, $nom_tabla, $nom_fisicok_bd, $tipo_datok, $keyValue, $dimJoinValue);
		
		return $dimJoinValue;
	}
	
	//@JAPR 2012-03-08: Agregado el indicador de versión por registro para descarga inteligente de catálogos
	//Si la dimensión representa un catálogo, verifica que tenga la columna para la versión de sus registros y actualiza en la instancia
	//el valor de la última versión y el nombre de campo del indicador Versión utilizado en la tabla
	//La función regresa el número máximo de versión actualmente grabado, si la dimensión no es catálogo entonces regresa -1
	function getCatalogVersion()
	{
		$this->VersionFieldName = '';
		$this->MaxVersionNum = -1;
		$this->VersionIndID = 0;
		//@JAPR 2012-03-27: Actualizadas las encuestas cuando los catálogos que usan cambian
		$this->CatalogID = 0;
		//@JAPR
		if (!$this->isCatalog)
		{
			return -1;
		}
		
		//Obtenemos el CatalogID a partir del cla_descrip que se recibio
		$sql = "SELECT CatalogID FROM SI_SV_Catalog WHERE ParentID = ".$this->cla_descrip;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if($aRS!==false && !$aRS->EOF)
		{
			$svCatalogID = (int)$aRS->fields["catalogid"];
			if ($svCatalogID > 0)
			{
				require_once("catalog.inc.php");
				$aCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $svCatalogID);
				if (!is_null($aCatalog))
				{
					$aCatalog->getCatalogVersion();
					$this->VersionFieldName = $aCatalog->VersionFieldName;
					$this->MaxVersionNum = $aCatalog->MaxVersionNum;
					$this->VersionIndID = $aCatalog->VersionIndID;
					//@JAPR 2012-03-27: Actualizadas las encuestas cuando los catálogos que usan cambian
					$this->CatalogID = $svCatalogID;
					//@JAPR
				}
			}
		}
		
		return $this->MaxVersionNum;
	}
}

/***********************************************************************************************/

class BITAMDimensionCollection extends BITAMCollection
{
	public $cla_concepto;
	public $ExcludeDuplicated;
	
	function __construct($aRepository, $bExcludeDuplicated = false)
	{
		BITAMCollection::__construct($aRepository);
		$this->cla_concepto = -1;
		$this->ExcludeDuplicated = $bExcludeDuplicated;
	}
	
	
	//@JAPR 2010-07-20: Agregados los parámetros $bGetMySubrrogateID y $bGetMyParentsID para cargar las nuevas colecciones de valores de la 
	//llave Subrrogada y de los padres (subrrogados si es el caso) de la dimensión que está siendo instanciada con este método
	static function NewInstance($aRepository, $anArrayOfDimensionIDs = null, $aCubeID = -1, $in, $includePeriods, $aGetValues = true, $bExcludeDuplicated = false, $budgetID=null, $bGetMySubrrogateID = false, $bGetMyParentsID = false)
	{
		global $checkDisabledCubes;
		global $appSettings;
		
		$anInstance = BITAMGlobalInstance::GetDimensionCollections($anArrayOfDimensionIDs,$aCubeID,$in,$includePeriods,$bExcludeDuplicated);
		if (!is_null($anInstance))
		{
			return $anInstance;
		}
		
		$anInstance = new BITAMDimensionCollection($aRepository, $bExcludeDuplicated);
		$where = "";
		
		if (!is_null($anArrayOfDimensionIDs))
		{
			switch (count($anArrayOfDimensionIDs))
			{
				case 0:
					break;
				
				case 1:
				
					if($in==true)
					{
						$where = " AND A.CONSECUTIVO = ".((int) $anArrayOfDimensionIDs[0]);
					}
					else
					{
						$where = " AND A.CONSECUTIVO <> ".((int) $anArrayOfDimensionIDs[0]);
					}
					break;	
					
				default:
				
					foreach ($anArrayOfDimensionIDs as $aDimensionID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $aDimensionID; 
					}
					if ($where != "")
					{
						if($in==true)
						{
							$where = " AND A.CONSECUTIVO IN (".$where.")";
						}
						else
						{
							$where = " AND A.CONSECUTIVO NOT IN (".$where.")";
						}
					}
					break;
			}
		}
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$where = str_replace('A.CONSECUTIVO ', 'B.CLA_DESCRIP', $where);
		}
		//@JAPR

		// Si $period==true también se incluyen también las dimensiones de tipo periodo 
		//aquellas cuyo campo cla_periodo en la tabla si_cpto_llave es >= 0 siempre y cuando si 
		//$anArrayOfDimensionIDs tiene valores, las incluya
		$strCubeJoin = '';
		$strFromForCubes = '';
		$strJoinForCubes = '';
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			//Si no se usan joins a los cubos, el ordenamiento es siempre por nombre lógico global
			$strOrderBy = 'ORDER BY B.NOM_LOGICO';
		}
		else
		{
			if ($aCubeID > 0)
			{
				$strCubeJoin = 'AND A.CLA_CONCEPTO = '.$aCubeID;
				$strOrderBy = 'ORDER BY A.CONSECUTIVO';
			}
			else 
			{	$strFromForCubes = ', SI_CONCEPTO C ';
				$strJoinForCubes = ' AND A.CLA_CONCEPTO = C.CLA_CONCEPTO AND C.TIPO_CUBO = 3 ';
				$strOrderBy = 'ORDER BY A.NOM_LOGICO';
			}
		}
		//@JAPR

		$strLogicNameTable = 'A.';
		if ($bExcludeDuplicated)
		{
			$strLogicNameTable = 'B.';
		}

		//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if ($appSettings["UseGlobalDimensions"] == 0)
		{
			if ($checkDisabledCubes)
			{
				$where .= ' AND A.CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
			}
		}
		//@JAPR
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			//En este caso se utilizan datos Dummy para cualquier referencia a la tabla de dimensiones para cubos (SI_CPTO_LLAVE)
			$sql = "SELECT -1 AS CLA_CONCEPTO, -1 AS CONSECUTIVO, '' AS FMT_PERIODO, -1 AS CLA_PERIODO, B.NOM_LOGICO, B.NOM_FISICO, B.TIPO_DATO, B.LONG_DATO, B.NOM_FISICOK, B.TIPO_DATOK, B.LONG_DATOK, B.NOM_TABLA, '00' AS PAR_DESCRIPTOR, B.DEFAULT_KEY, B.CLA_DESCRIP, B.CLA_CONEXION, B.CREATED_BY, B.IS_CATALOG ".
				"FROM SI_DESCRIP_ENC B ".
				"WHERE B.CLA_DESCRIP > 0";
			if (trim($where) != '')
			{
				$sql .= $where;
			}
		}
		else 
		{
			//En este caso se usa el query original con join de dimensiones por cubo
			$sql = "SELECT A.CLA_CONCEPTO, A.CONSECUTIVO, A.FMT_PERIODO, A.CLA_PERIODO,".$strLogicNameTable."NOM_LOGICO, B.NOM_FISICO, B.TIPO_DATO, B.LONG_DATO, B.NOM_FISICOK, B.TIPO_DATOK, B.LONG_DATOK, B.NOM_TABLA, A.PAR_DESCRIPTOR, B.DEFAULT_KEY, A.CLA_DESCRIP, B.CLA_CONEXION, B.CREATED_BY, B.IS_CATALOG ".
				"FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B ".$strFromForCubes.
				" WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.ESTATUS = 1 ".$strJoinForCubes." ".$strCubeJoin." ".$where;
			if($includePeriods==true)
			{	
				//@JAPR 2010-02-16: Validado que sólo se carguen dimensiones visibles
			}
			// Si $period==false no se incluyen en la colección las dimensiones de tipo periodo 
			//aquellas cuyo campo cla_periodo en la tabla si_cpto_llave es >= 0 se filtran de la consulta
			else 
			{
				//@JAPR 2010-02-16: Validado que sólo se carguen dimensiones visibles
				$sql .= " AND (A.CLA_PERIODO<0 OR A.CLA_PERIODO IS NULL) ";
			}
		}
		$sql .= " ".$strOrderBy;
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE or SI_DESCRIP_ENC ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		$arrDimensionsDescripts = array();
		while (!$aRS->EOF)
		{
			$bIncludeThisDimension = true;
			if ($bExcludeDuplicated)
			{
				$aClaDescrip = (int)$aRS->fields["cla_descrip"];
				if (array_key_exists($aClaDescrip, $arrDimensionsDescripts))
				{
					$bIncludeThisDimension = false;
				}
				else 
				{
					$arrDimensionsDescripts[$aClaDescrip] = $aClaDescrip;
				}
			}
			
			if ($bIncludeThisDimension)
			{
				$anInstance->Collection[] = BITAMDimension::NewInstanceFromRS($anInstance->Repository, $aRS, $aGetValues, $budgetID, true, $bGetMySubrrogateID, $bGetMyParentsID);
			}
			$aRS->MoveNext();
		}
		
		BITAMGlobalInstance::AddDimensionCollections($anArrayOfDimensionIDs,$aCubeID,$in,$includePeriods,$bExcludeDuplicated,$anInstance);
		$anInstance->cla_concepto=$aCubeID;
		$anInstance->getNumberOfTimes();
		return $anInstance;
	}
	
	static function NewInstanceWithDescripID($aRepository, $anArrayOfDescripKeys = null, $aCubeID = -1,$in, $includePeriods, $aGetValues = true, $bExcludeDuplicated = false)
	{
		global $checkDisabledCubes;
		
		$anInstance = new BITAMDimensionCollection($aRepository);
		
		$where = "";
		
		if (!is_null($anArrayOfDescripKeys))
		{
			switch (count($anArrayOfDescripKeys))
			{
				case 0:
					break;
				
				case 1:
				
					if($in==true)
					{
						$where = " AND B.CLA_DESCRIP = ".((int) $anArrayOfDescripKeys[0]);
					}
					else
					{
						$where = " AND B.CLA_DESCRIP <> ".((int) $anArrayOfDescripKeys[0]);
					}
					break;	
					
				default:
				
					foreach ($anArrayOfDescripKeys as $aDimensionID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $aDimensionID; 
					}
					if ($where != "")
					{
						if($in==true)
						{
							$where = " AND B.CLA_DESCRIP IN (".$where.")";
						}
						else
						{
							$where = " AND B.CLA_DESCRIP NOT IN (".$where.")";
						}
					}
					break;
			}
		}

		// Si $period==true también se incluyen también las dimensiones de tipo periodo 
		//aquellas cuyo campo cla_periodo en la tabla si_cpto_llave es >= 0 siempre y cuando si 
		//$anArrayOfDescripKeys tiene valores, las incluya
		$strCubeJoin = '';
		$strFromForCubes = '';
		$strJoinForCubes = '';
		if ($aCubeID > 0)
		{
			$strCubeJoin = 'AND A.CLA_CONCEPTO = '.$aCubeID;
		}
		else
		{
			$strFromForCubes = ', SI_CONCEPTO C ';
			$strJoinForCubes = ' AND A.CLA_CONCEPTO = C.CLA_CONCEPTO AND C.TIPO_CUBO = 3 ';
		}

		$strLogicNameTable = 'A.';
		if ($bExcludeDuplicated)
		{
			$strLogicNameTable = 'B.';
		}
		
		//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
		if ($checkDisabledCubes)
		{
			$where .= ' AND A.CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
		}
		//@JAPR
		
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		if($includePeriods==true)
		{	
			//@JAPR 2010-02-16: Validado que sólo se carguen dimensiones visibles
			$sql = "SELECT A.CLA_CONCEPTO, A.CONSECUTIVO, A.FMT_PERIODO, A.CLA_PERIODO,".$strLogicNameTable."NOM_LOGICO,B.NOM_FISICO, B.TIPO_DATO, B.LONG_DATO, B.NOM_FISICOK, B.TIPO_DATOK, B.LONG_DATOK, B.NOM_TABLA, A.PAR_DESCRIPTOR, B.DEFAULT_KEY, A.CLA_DESCRIP, B.CLA_CONEXION, B.CREATED_BY, B.IS_CATALOG ".
				"FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B ".$strFromForCubes.
				" WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.ESTATUS = 1 ".$strJoinForCubes." ".$strCubeJoin." ".$where. " ORDER BY A.CONSECUTIVO";
		}
		// Si $period==false no se incluyen en la colección las dimensiones de tipo periodo 
		//aquellas cuyo campo cla_periodo en la tabla si_cpto_llave es >= 0 se filtran de la consulta
		else 
		{
			//@JAPR 2010-02-16: Validado que sólo se carguen dimensiones visibles
			$sql = "SELECT A.CLA_CONCEPTO, A.CONSECUTIVO, A.FMT_PERIODO, A.CLA_PERIODO,".$strLogicNameTable."NOM_LOGICO,B.NOM_FISICO, B.TIPO_DATO, B.LONG_DATO, B.NOM_FISICOK, B.TIPO_DATOK, B.LONG_DATOK, B.NOM_TABLA, A.PAR_DESCRIPTOR, B.DEFAULT_KEY, A.CLA_DESCRIP, B.CLA_CONEXION, B.CREATED_BY, B.IS_CATALOG ".
				"FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B ".$strFromForCubes.
				" WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.ESTATUS = 1 ".$strJoinForCubes." ".$strCubeJoin." ".$where. " AND (A.CLA_PERIODO<0 OR A.CLA_PERIODO IS NULL) ORDER BY A.CONSECUTIVO";
		}
	
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE or SI_DESCRIP_ENC ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		$arrDimensionsDescripts = array();
		while (!$aRS->EOF)
		{
			$bIncludeThisDimension = true;
			if ($bExcludeDuplicated)
			{
				$aClaDescrip = (int)$aRS->fields["cla_descrip"];
				if (array_key_exists($aClaDescrip, $arrDimensionsDescripts))
				{
					$bIncludeThisDimension = false;
				}
				else 
				{
					$arrDimensionsDescripts[$aClaDescrip] = $aClaDescrip;
				}
			}
			
			if ($bIncludeThisDimension)
			{
				$anInstance->Collection[] = BITAMDimension::NewInstanceFromRS($anInstance->Repository, $aRS, $aGetValues);
			}
			$aRS->MoveNext();
		}
		
		$anInstance->cla_concepto=$aCubeID;
		$anInstance->getNumberOfTimes();
		return $anInstance;
	}

	static function NewInstanceEmpty($aRepository)
	{
		$anInstance = new BITAMDimensionCollection($aRepository);
		
		return $anInstance;
	}
	
	//Retorna sólo las dimensiones con cla_periodo>0
	/***********************************************/
	static function NewInstancePeriodDims($aRepository, $aCubeID, $aGetValues = true)
	{
		global $checkDisabledCubes;
		
		$anInstance = BITAMGlobalInstance::GetPeriodDimensionCollections($aCubeID);
		if (!is_null($anInstance))
		{
			return $anInstance;
		}
		
		$anInstance = new BITAMDimensionCollection($aRepository);
		
		//@JAPR 2010-02-16: Validado que sólo se carguen dimensiones visibles
		//@JAPR 2011-06-23: Agregada la opción de dimensiones - catálogo cuya descripción puede ser vacia
		$sql = "SELECT A.CLA_CONCEPTO, A.CONSECUTIVO, A.FMT_PERIODO, A.CLA_PERIODO,B.NOM_LOGICO,B.NOM_FISICO, B.TIPO_DATO, B.LONG_DATO, B.NOM_FISICOK, B.TIPO_DATOK, B.LONG_DATOK, B.NOM_TABLA, A.PAR_DESCRIPTOR, B.DEFAULT_KEY, A.CLA_DESCRIP, B.CLA_CONEXION, B.CREATED_BY, B.IS_CATALOG ".
			"FROM SI_CPTO_LLAVE A, SI_DESCRIP_ENC B ".
			"WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND A.CLA_CONCEPTO = ".$aCubeID." AND CLA_PERIODO>=0 AND A.ESTATUS = 1";
		//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
		if ($checkDisabledCubes)
		{
			$sql .= ' AND A.CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
		}
		$sql .= " ORDER BY A.CONSECUTIVO";
		//@JAPR
	
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE or SI_DESCRIP_ENC ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMDimension::NewInstanceFromRS($anInstance->Repository, $aRS, $aGetValues);
			$aRS->MoveNext();
		}
		
		$anInstance->cla_concepto=$aCubeID;
		$anInstance->getNumberOfTimes();

		BITAMGlobalInstance::AddPeriodDimensionCollections($aCubeID,$anInstance);
		return $anInstance;
	}
	
	
	//Obtiene el número de veces q se debe repetir cada valor de una dimensión considerando
	//q se combinan todas las dimensiones de la colección, y q se muestran en la tabla de combinaciones
	//en el orden en el q se encuentran en el arreglo Collection[]
	
	function getNumberOfTimes()
	{
		for ($i=0; $i<count($this->Collection); $i++)
		{
			$numberOfTimes=1;
			for ($j=$i+1;$j<count($this->Collection);$j++)
			{
				$numberOfTimes=$this->Collection[$j]->numValues*$numberOfTimes;
				
			}
			$this->Collection[$i]->numberOfTimes=$numberOfTimes;
		}
	}

	function getIndexByDimID($dimID)
	{	
		$index=null;
		
		if($dimID==null)
		{
			return $index;
		}
	
		$sizeCollection=count($this->Collection);
		
		for($i=0;$i<$sizeCollection;$i++)
		{
			if($this->Collection[$i]->consecutivo==$dimID)
			{
			 	$index=$i;
			 	break;
			}
		}
		
		return $index;
	}
	
	static function OrderDimensionIDsByHierarchy($aRepository, $aCubeID, $dimensionIDs)
	{	
		$strDimensionsWithComma="";
		$arrayDimensionsIDsOrderByHierarchy = array();
		$numDimensions = count($dimensionIDs);
		
		for($i=0; $i<$numDimensions; $i++)
		{
			if($strDimensionsWithComma!="")
			{
				$strDimensionsWithComma.=", ";
			}
			
			$strDimensionsWithComma.= $dimensionIDs[$i];
		}
		
		//Obtenemos jerarquías
		if($strDimensionsWithComma != "")
		{
			
			$sql = "SELECT A.DIM_PADRE
					FROM SI_JERARQUIA A
					WHERE A.CLA_CONCEPTO = ".$aCubeID."
					AND A.DIM_PADRE IN ( ".$strDimensionsWithComma." )
					ORDER BY A.CLA_JERARQUIA, A.CONSECUTIVO";		
			
			$aRS = $aRepository->ADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}			
			
			$numDimensionsHierarchyElements = 0;
			
			
			
			while(!$aRS->EOF)
			{
				$arrayDimensionsIDsOrderByHierarchy[$numDimensionsHierarchyElements] = (int)$aRS->fields["dim_padre"];
				$numDimensionsHierarchyElements = $numDimensionsHierarchyElements + 1;
				
				$aRS->MoveNext();
			}
			
			$arrayDimensionIDsWithoutHierarchy = array();
			
			if($numDimensionsHierarchyElements>0)
			{
				$arrayDimensionIDsWithoutHierarchy = array_diff($dimensionIDs, $arrayDimensionsIDsOrderByHierarchy);
			}
			else 
			{
				$arrayDimensionIDsWithoutHierarchy=$dimensionIDs;
			}
			
			foreach($arrayDimensionIDsWithoutHierarchy as $ElementOfArray )
			{
			
				$arrayDimensionsIDsOrderByHierarchy[$numDimensionsHierarchyElements] = $ElementOfArray;
				
				$numDimensionsHierarchyElements = $numDimensionsHierarchyElements + 1;
			}
		}
		//echo('<BR>Orden de dims por jerarquía:   ');
		//var_dump($arrayDimensionsIDsOrderByHierarchy);
		return $arrayDimensionsIDsOrderByHierarchy;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("CubeID", $aHTTPRequest->GET))
		{
			$aCubeID = (int) $aHTTPRequest->GET["CubeID"];
		}
		else
		{
			$aCubeID = 0;
		}
		
		$bExcludeDuplicated = false;
		if (array_key_exists("ExcludeDuplicated", $aHTTPRequest->GET))
		{
			$bExcludeDuplicated = (bool) $aHTTPRequest->GET["ExcludeDuplicated"];
		}

		return BITAMDimensionCollection::NewInstance($aRepository,null,$aCubeID,true,false,false,$bExcludeDuplicated);

	}
	
	function get_Parent()
	{
		global $appSettings;
		
		if($appSettings["Cubes"] == 1)
		{
			if ($this->cla_concepto > 0)
			{
				return BITAMCube::NewInstanceWithID($this->Repository,$this->cla_concepto);
			}
			else 
			{
				return BITAMCubeCollection::NewInstance($this->Repository,null,3);
			}
		}
		else
		{
			return BITAMDimensionCollection::NewInstance($this->Repository,null,0,true,false,false,true);
		}
	}
	
	function get_Title()
	{	
		$title = translate("Dimension Definitions");
		
		return $title;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=Dimension&CubeID=".$this->cla_concepto;
	}
	
	function get_QueryString()
	{
		if ($this->ExcludeDuplicated)
		{
			return "BITAM_SECTION=DimensionCollection&ExcludeDuplicated=1";
		}
		else
		{
			return "BITAM_SECTION=DimensionCollection";
		}
	}
	
	function get_ChildQueryString()
	{
		if ($this->ExcludeDuplicated)
		{
			return "BITAM_SECTION=Dimension";
		}
		else 
		{
			return "BITAM_SECTION=Dimension";
		}
	}

	function get_FormIDFieldName()
	{
		return 'consecutivo';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		global $appSettings;
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "nom_logico";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		if($appSettings["DimensionValuesCols"]["Table"] == 1)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "nom_tabla";
			$aField->Title = translate("Table");
			$aField->Type = "String";
			$myFields[$aField->Name] = $aField;
		}
		
		if($appSettings["DimensionValuesCols"]["Key"] == 1)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "nom_fisicok_bd";
			$aField->Title = translate("Key Name");
			$aField->Type = "String";
			$myFields[$aField->Name] = $aField;
		}
		
		if($appSettings["DimensionValuesCols"]["Desc"] == 1)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "nom_fisico_bd";
			$aField->Title = translate("Descriptor Name");
			$aField->Type = "String";
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}

	function canAdd($aUser)
	{
		if ($this->cla_concepto > 0)
		{
			$aCube = BitamCube::NewInstanceWithID($this->Repository, $this->cla_concepto);
			if (!is_null($aCube))
			{
				if ($aCube->show_alias)
				{
					return true;
				}
				else 
				{
					return false;
				}
			}
			else 
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	
	function canEdit($aUser)
	{
		return false;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		if(get_class($this) == get_class($parent))
		{
			$pathArray = $this->Repository->get_PathArray();
		}
		else
		{
			$pathArray = $parent->get_PathArray();
		}
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
}
?>