<?php
chdir('../../../');
require 'checkCurrentSession.inc.php';

?><form role="form">
	<style>
		.httpRight{
			float: right;
    		width: 45%;
    		border-style: solid;
		    border-width: 2px;
		    border-color: #4285f4;
		    padding: 5px;
		}
		.httpLeft{
			float: left;
    		width: 45%;
    		border-style: solid;
		    border-width: 2px;
		    border-color: #4285f4;
		    padding: 5px;
		}
		.fa {
		    display: inline-block;
		    font: normal normal normal 14px/1 FontAwesome;
		    font-size: inherit;
		    text-rendering: auto;
		    margin: 3px;
		}
		.typedestinations label > input {
			visibility: hidden;
			position: absolute;
		}
		.typedestinations label > input + img {
			cursor:pointer;
			border:2px solid transparent;
			padding: 2px;
		}
		.typedestinations label > input:checked + img {
			border: 2px solid #ffcd61;
			border-radius: 4px;
		}
		.modal > .modal-dialog > .modal-content .wizard > .steps a, .wizard > .steps a:hover, .wizard > .steps a:active {
    		padding: .5em 4px !important;
		}
		.wizard > .steps > ul > li {
			width: 20%;
		}
		.fa-folder-o:before {
	    	content: "\f114";
		}
		.fa-file:before {
			content: "\f15b";
		}
		.fa-file-excel-o:before {
		    content: "\f1c3";
		}
		.selectedTree > div {
			overflow: auto;
			float: left;
			height: 230px;
			width: 100%;
			border: 1px solid #FFFFFF;
			border-radius: 3px;
			margin-right: 10px !important;
		}
		.selectedTree > div > ul {			
			padding: 10px !important;
			margin-right: 10px !important;
		}
		.metisFolder .active > a > .icon-expand-alt:before {
			content: "\f117";
		}

		.metisFolder a, .selectedTree > div > .selectedList {
			.overflow: auto;	
		}

		.metisFolder a, .selectedTree > div > .selectedList > div {
			cursor: pointer;
		}

		.selectedTree > div > .selectedList > div {
			padding: 4px 10px 4px 10px;
			border-bottom: 1px solid #FFFFFF;
		}

		.selectedTree > div > .selectedList > div.old {
			background-color: rgb(229, 229, 229);
		}

		.selectedTree > div > .selectedList > div:hover {
			background-color: rgba(0, 136, 204, 0.66);
			color: #FFFFFF;
		}
		.object_table {
			display: inline-block;
		    float: left;
		    margin-left: 5px;
		    margin-right: 5px;
		}

		.object_table table {
			font-size: 12px;
		    border-style: solid;
		    border-width: 1px;
		    border-collapse: separate;
		    background-color: #FFFFFF;
		}

		.object_table td {
		    padding: 3px 5px;
		    border: solid #ebebeb;
		    border-width: 0 1px 1px 0;
		}

		.object_table td.selected {
			background-color: rgb(186, 210, 224);
		}			
		.field {
		  width: 150px;
		  border: 1px solid #E0E0E0;
		  padding: 4px;
		  float: left;
		  margin-right: 8px;
		  margin-bottom: 8px;
		  cursor: pointer;
		}
		.field div {
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
			width: 142px;
			float: left;
		}
		.field i {
			display: none;
			float: right;
			margin-top: 4px;
		}
		.field:hover {
			background-color: rgba(0, 0, 0, 0.56);
			color: #FFFFFF;
		}
		.field:hover div {
			width: 127px;
		}
		.field:hover i.icon-check-empty {
			display: block;
		}
		.field.selected {
			background-color: #128888;
			color: #FFFFFF;
		}
		.field.selected div {
			width: 127px;
		}
		.field.selected i.icon-check {
			display: block;
		}
		.field.selected i.icon-check-empty {
			display: none;
		}
		.mappingfield {
			margin-bottom: 10px;
		}
		.alert.alert-danger {
			display: none;
		}
		
		.modal > .modal-dialog > .modal-content .wizard > .content {
			min-height: 310px;
		}
		.selectize-input {
			min-height: 28px;
			padding: 3px 10px;
		}
		.fields .form-group {
			margin-bottom: 10px;
			width: 50%;
			float: left;
		}
		.modal.material .modal-dialog .modal-content .modal-body .form-group>label {
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
		.modal.material form {
			overflow: inherit;
		}
		.modal .fieldsnew {
			clear: both;
			padding-top: 5px;
			border-top: 1px solid #FFFFFF;
			margin-top: 12px;
		}
		.modal .fieldsnew h3 {
			margin-bottom: 12px;
		}
		.modal .errorinput {
			border-color: red !important;
    		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    		box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    		border-width: 1px;
    		border-style: solid;
		}

	</style>
	<div style="position: relative;  top: -59px;  left: 0px;height: 0px;"><div class="alert alert-danger" style="">Error!</div></div>
	<div id = "vacio">
		<h1 class="scheduler"><?= translate("Scheduler") ?></h1>
	</div>
    <h1 class="Connection"><?= translate("Connection") ?></h1>
    <section data-section="Connection" class="Connection">
   		<div class = "dpxdiv">
	    	<p><?= translate('Select an Dropbox account or create a new connection') ?></p>
	    	<select name="dpxaccount"></select>
	    	<br>
	    	<div class="loadingDiv" style="display: none;">
	    		<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
			</div>
    	</div>
    	<div class = "gddiv">
    		<p><?= translate('Select an Google account') ?></p>
	    	<select name="gaccount"></select>
	    	<br>
	    	<div class="loadingDiv" style="display: none;">
	    		<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
			</div>
    	</div>
    	<div class = "httpdiv">
	    	<p><?= translate('Select an HTTP account') ?></p>
    		<select name="httpaccount"></select>
	    	<br>
			<div style="display: none" class="httpnew">
				<p><?= translate('Type a HTTP server, user name and password') ?></p>
<? 			if (getMDVersion()>=esveServicesConnectionName) { ?>
				<div class="form-group">
					<label for="BDName">* <?= translate('Name') ?></label>
					<input type="text" autocomplete="off"  required requiredisnull="ftpaccount" class="form-control" id="httpName" name="httpName">
				</div>
<? 			} ?>
		    	<div class="form-group">
					<label for="ftpServer">* <?= translate('Server') ?></label>
				    <input type="text" pattern="^(ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$" required requiredisnull="ftpaccount" class="form-control" id="httpServer" name="httpServer">
				</div>
		    	<div class="form-group">
					<label for="ftpUsername"><?= translate('User Name') ?></label>
				    <input type="text" autocomplete="off" class="form-control" id="httpUsername" name="httpUsername">
				</div>
				<div class="form-group">
					<label for="ftpPassword"><?= translate('Password') ?></label>
				    <input type="password" autocomplete="off" class="form-control" id="httpPassword" name="httpPassword">
				</div>
				<div class="form-group">
					<label for="ftpParameters"><?= translate('Parameters') ?></label>
				    <input type="text" autocomplete="off" class="form-control" id="httpParameters" name="httpParameters">
				</div>	    	
			</div>
		    <div class="loadingDiv" style="display: none;">
		    	<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
			</div>
    	</div>
    	<div class = "ftpdiv">
			<p><?= translate('Select an FTP account') ?></p>
			<select name="ftpaccount"></select>
			<br>
			<div style="display: none" class="ftpnew">
				<p><?= translate('Type a FTP server, user name and password') ?></p>
<?	 		if (getMDVersion()>=esveServicesConnectionName) { ?>
				<div class="form-group">
					<label for="ftpName">* <?= translate('Name') ?></label>
					<input type="text" autocomplete="off"  required requiredisnull="ftpaccount" class="form-control" id="ftpName" name="ftpName">
				</div>
<? 			} ?>
				<div class="form-group">
					<label for="ftpServer">* <?= translate('FTP Server') ?></label>
					<input type="text" autocomplete="off" pattern="^(ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$" required requiredisnull="ftpaccount" class="form-control" id="ftpServer" name="ftpServer">
				</div>
				<div class="form-group">
					<label for="ftpUsername">* <?= translate('User Name') ?></label>
					<input type="text" autocomplete="off" required requiredisnull="ftpaccount" class="form-control" id="ftpUsername" name="ftpUsername">
				</div>
				<div class="form-group">
					<label for="ftpPassword"><?= translate('Password') ?></label>
					<input type="password" autocomplete="off" class="form-control" id="ftpPassword" name="ftpPassword">
				</div>
				<div class="loadingDiv" style="display: none;">
	    			<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
				</div>
			</div>
    	</div>
    </section>
	<h1 class="File"><?php echo translate( 'File' ) ?></h1>
    <section class="File">
		<p><?= translate('Destination path') ?></p>
		<input type="text" class="form-control" required name="path" id="path" maxlength="250">
		<input type="text" class="form-control" required name="file" id="file" maxlength="250" style="display: none">
		<label>* <?= translate('Folder tree') ?></label>
		<div class="selectedTree">
			<div><ul class="metisFolder" data-text="<?= translate('Building folder tree') ?>...", data-textnoopen="<?= translate('You must select a dimension or indicator before opening the next level') ?>"><?= translate( 'Building folder tree' ) ?>...</ul></div>
		</div>
		<div class="loadingDiv" style="display: none; float: right;">
			<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
		</div>
	</section>
    <h1 class="Sheet"><?php echo translate( 'Sheet' ) ?></h1>
    <section class="Sheet">
    	<div style="margin-top: 70px;width: 370px;margin-left: auto;margin-right: auto; " class = "divSheetselect">
			<label>* <?php echo translate( 'Select sheet' ) ?></label>
			<select id="sheets" name="sheets"></select>
		</div>
		<div class="loadingDiv" style=" margin-top: 125px; display: none; float: right;">
			<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
		</div>
	</section>

    <h1 class="Attributes" title="<?php echo translate( 'Attributes' ) ?>"><?php echo translate( 'Attributes' ) ?></h1>
    <section class="finishstep Attributes">
    	<div class="mappingselect">
	    	<div>
	    		<div class="catalogname form-group">
	    			<label for="catalogName"><?php echo translate( 'Catalog name' ) ?></label>
	    			<input type="text" class="form-control" id="catalogName" name="catalogName" placeholder="<?php echo translate( 'Name' ) ?>">
	  			</div>
	  		</div>
	    	<div class="fields"></div>
	    	<div class="fieldsnew">
	    		<h3><?php echo translate( 'Add new attributes' ) ?></h3>
	    		<div class="listnewfields"></div>
	    	</div>
    	</div>
    	<div class="creatingtables" style="display: none">
    		<img src="loadexcel/images/loading1.gif" style="vertical-align: sub;"><?php echo translate( 'Creating tables and loading data...' ) ?>
    	</div>
    </section>
    <div style="display: none" id="tLoadExcel"><?php echo translate( 'Schedule it' ) ?></div>
    <div style="display: none" id="tPrevious"><?php echo translate( 'Previous' ) ?></div>
    <div style="display: none" id="tFinish"><?php echo translate( 'Finish' ) ?></div>
    <div style="display: none" id="tNext"><?php echo translate( 'Next' ) ?></div>
    <div style="display: none" id="tSelectAttribute"><?php echo translate( 'Select Attribute' ) ?></div>
    <div style="display: none" id="tCreatingTables"><?php echo translate( 'Creating tables and loading data' ) ?></div>
</form>
