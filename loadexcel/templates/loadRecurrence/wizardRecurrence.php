<?php
chdir('../../../');
require 'checkCurrentSession.inc.php';

?><form role="form">
	<style>
		.fa {
		    display: inline-block;
		    font: normal normal normal 14px/1 FontAwesome;
		    font-size: inherit;
		    text-rendering: auto;
		    margin: 3px;
		}
		.fa-folder-o:before {
	    	content: "\f114";
		}
		.fa-file:before {
			content: "\f15b";
		}
		.fa-file-excel-o:before {
		    content: "\f1c3";
		}
		.selectedTree > div {
			overflow: auto;
			float: left;
			height: 230px;
			width: 700px;
			border: 1px solid #FFFFFF;
			border-radius: 3px;
			margin-right: 10px !important;
		}
		.selectedTree > div > ul {			
			padding: 10px !important;
			margin-right: 10px !important;
		}
		.metisFolder .active > a > .icon-expand-alt:before {
			content: "\f117";
		}

		.metisFolder a, .selectedTree > div > .selectedList {
			.overflow: auto;	
		}

		.metisFolder a, .selectedTree > div > .selectedList > div {
			cursor: pointer;
		}

		.selectedTree > div > .selectedList > div {
			padding: 4px 10px 4px 10px;
			border-bottom: 1px solid #FFFFFF;
		}

		.selectedTree > div > .selectedList > div.old {
			background-color: rgb(229, 229, 229);
		}

		.selectedTree > div > .selectedList > div:hover {
			background-color: rgba(0, 136, 204, 0.66);
			color: #FFFFFF;
		}			
		.field {
		  width: 150px;
		  border: 1px solid #E0E0E0;
		  padding: 4px;
		  float: left;
		  margin-right: 8px;
		  margin-bottom: 8px;
		  cursor: pointer;
		}
		.field div {
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
			width: 142px;
			float: left;
		}
		.field i {
			display: none;
			float: right;
			margin-top: 4px;
		}
		.field:hover {
			background-color: rgba(0, 0, 0, 0.56);
			color: #FFFFFF;
		}
		.field:hover div {
			width: 127px;
		}
		.field:hover i.icon-check-empty {
			display: block;
		}
		.field.selected {
			background-color: #128888;
			color: #FFFFFF;
		}
		.field.selected div {
			width: 127px;
		}
		.field.selected i.icon-check {
			display: block;
		}
		.field.selected i.icon-check-empty {
			display: none;
		}
		.mappingfield {
			margin-bottom: 10px;
		}
		.alert.alert-danger {
			display: none;
		}
		
		.modal > .modal-dialog > .modal-content .wizard > .content {
			min-height: 310px;
		}
		.selectize-input {
			min-height: 28px;
			padding: 3px 10px;
		}
		.fields .form-group {
			margin-bottom: 10px;
			width: 50%;
			float: left;
		}
		.modal.material .modal-dialog .modal-content .modal-body .form-group>label {
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
		.modal.material form {
			overflow: inherit;
		}
		.modal .fieldsnew {
			clear: both;
			padding-top: 5px;
			border-top: 1px solid #FFFFFF;
			margin-top: 12px;
		}
		.modal .fieldsnew h3 {
			margin-bottom: 12px;
		}

	</style>
	<div style="position: relative;  top: -59px;  left: 0px;height: 0px;"><div class="alert alert-danger" style="">Error!</div></div>
	<h1><?= translate("Basic info") ?></h1>
	<section data-section="basicinfo">
		<div class="form-group">
			<label for="type">* <?= translate('Type') ?></label>
		    <div class="typedestinations">
				<label>
				  	<input required type="radio" id="type" name="type" value="3" checked="checked"/>
					<img src="images/datadestinations/drive.png" title="Google Drive" >
				</label>
				<label>
				  	<input required type="radio" id="type" name="type" value="2" />
					<img src="images/datadestinations/dropbox.png" title="Dropbox" >
				</label>
				<label>
				  	<input required type="radio" id="type" name="type" value="5" />
					<img src="images/datadestinations/ftp.png" title="FTP" >
				</label>
			</div>
		</div>
    </section>
    <h1><?= translate("Dropbox") ?></h1>
    <section data-section="dropbox">
    	<p><?= translate('Select an Dropbox account or create a new connection') ?></p>
    	<select name="dpxaccount"></select>
    	<br>
    	<input type="hidden" name="access_token">
    </section>
    <h1><?= translate("Google Drive") ?></h1>
    <section data-section="gdrive">
    	<p><?= translate('Selecciona una conexion con Google del catalogo o crea una nueva conexion') ?></p>
    	<select name="gaccount"></select>
    	<input type="hidden" name="access_token">
    </section>
    <h1><?= translate("FTP") ?></h1>
    <section data-section="ftpstep">
    	<p><?= translate('Select an FTP account') ?></p>
    	<select name="ftpaccount"></select>
    	<br>
    	<div style="display: none" class="ftpnew">
	    	<p><?= translate('Type a FTP server, user name and password') ?></p>
	    	<div class="form-group">
				<label for="ftpServer">* <?= translate('FTP Server') ?></label>
			    <input type="text" pattern="^(ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$" required requiredisnull="ftpaccount" class="form-control" id="ftpServer" name="ftpServer">
			</div>
	    	<div class="form-group">
				<label for="ftpUsername">* <?= translate('User Name') ?></label>
			    <input type="text" autocomplete="off" required requiredisnull="ftpaccount" class="form-control" id="ftpUsername" name="ftpUsername">
			</div>
			<div class="form-group">
				<label for="ftpPassword"><?= translate('Password') ?></label>
			    <input type="password" autocomplete="off" class="form-control" id="ftpPassword" name="ftpPassword">
			</div>
		</div>
    </section>
	<h1><?php echo translate( 'App' ) ?></h1>
    <section>
    	<div style="margin-top: 70px;width: 370px;margin-left: auto;margin-right: auto;">
			<label>* <?php echo translate( 'Select App' ) ?></label>
			<select id="apps" name="Apps"></select>
		</div>
    </section>

    <h1><?php echo translate( 'Load file' ) ?></h1>
    <section>
		<p><?= translate('Destination path') ?></p>
		<input type="text" class="form-control" required name="path" id="path" maxlength="250">
		<input type="text" class="form-control" required name="file" id="file" maxlength="250" style="display: none">
		<label>* <?= translate('Folder tree') ?></label>
		<div class="selectedTree">
			<div><ul class="metisFolder" data-text="<?= translate('Building folder tree') ?>...", data-textnoopen="<?= translate('You must select a dimension or indicator before opening the next level') ?>"><?= translate( 'Building folder tree' ) ?>...</ul></div>
		</div>
    </section>
    <h1><?php echo translate( 'Form' ) ?></h1>
    <section>
    	<div style="margin-top: 70px;width: 370px;margin-left: auto;margin-right: auto;">
			<label>* <?php echo translate( 'Select Form' ) ?></label>
			<select id="catalogs" name="Catalogs"></select>
		</div>
    </section>
    <h1><?php echo translate( 'Select sheet' ) ?></h1>
    <section>
    	<div style="margin-top: 70px;width: 370px;margin-left: auto;margin-right: auto;">
			<label>* <?php echo translate( 'Select sheet' ) ?></label>
			<select id="sheets" name="sheets"></select>
		</div>
    </section>

    <h1 title="<?php echo translate( 'Select fields' ) ?>"><?php echo translate( 'Select fields' ) ?></h1>
    <section class="finishstep">
    	<div class="mappingselect">
	    	<div>
	    		<div class="catalogname form-group">
	    			<label for="catalogName"><?php echo translate( 'Catalog name' ) ?></label>
	    			<input type="text" class="form-control" id="catalogName" name="catalogName"  placeholder="<?php echo translate( 'Name' ) ?>">
	  			</div>
	  		</div>
	    	<div class="fields"></div>
	    	<div class="fieldsnew">
	    		<h3><?php echo translate( 'Add new attributes' ) ?></h3>
	    		<div class="listnewfields"></div>
	    	</div>
    	</div>
    	<div class="creatingtables" style="display: none">
    		<img src="loadexcel/images/loading1.gif" style="vertical-align: sub;"><?php echo translate( 'Creating tables and loading data...' ) ?>
    	</div>
    </section>
    <div style="display: none" id="tLoadExcel"><?php echo translate( 'Load Excel' ) ?></div>
    <div style="display: none" id="tPrevious"><?php echo translate( 'Previous' ) ?></div>
    <div style="display: none" id="tFinish"><?php echo translate( 'Finish' ) ?></div>
    <div style="display: none" id="tNext"><?php echo translate( 'Next' ) ?></div>
    <div style="display: none" id="tSelectAttribute"><?php echo translate( 'Select Attribute' ) ?></div>
    <div style="display: none" id="tCreatingTables"><?php echo translate( 'Creating tables and loading data' ) ?></div>
    <h1><?= translate("Scheduler") ?></h1>
</form>
