<?php
chdir('../../../');
require 'checkCurrentSession.inc.php';

?><form role="form">
	<style>
			
		.field {
		  width: 150px;
		  border: 1px solid #E0E0E0;
		  padding: 4px;
		  float: left;
		  margin-right: 8px;
		  margin-bottom: 8px;
		  cursor: pointer;
		}
		.field div {
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
			width: 142px;
			float: left;
		}
		.field i {
			display: none;
			float: right;
			margin-top: 4px;
		}
		.field:hover {
			background-color: rgba(0, 0, 0, 0.56);
			color: #FFFFFF;
		}
		.field:hover div {
			width: 127px;
		}
		.field:hover i.icon-check-empty {
			display: block;
		}
		.field.selected {
			background-color: #128888;
			color: #FFFFFF;
		}
		.field.selected div {
			width: 127px;
		}
		.field.selected i.icon-check {
			display: block;
		}
		.field.selected i.icon-check-empty {
			display: none;
		}
		.mappingfield {
			margin-bottom: 10px;
		}
		.alert.alert-danger {
			display: none;
		}
		.wizard > .steps > ul > li {
			width: 33.3%;
		}
		.modal > .modal-dialog > .modal-content .wizard > .content {
			min-height: 310px;
		}
		.selectize-input {
			min-height: 28px;
			padding: 3px 10px;
		}
		.fields .form-group {
			margin-bottom: 10px;
			width: 50%;
			float: left;
		}
		.modal.material .modal-dialog .modal-content .modal-body .form-group>label {
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
		.modal.material form {
			overflow: inherit;
		}
		.modal .fieldsnew {
			clear: both;
			padding-top: 5px;
			border-top: 1px solid #FFFFFF;
			margin-top: 12px;
		}
		.modal .fieldsnew h3 {
			margin-bottom: 12px;
		}

	</style>
	<div style="position: relative;  top: -59px;  left: 0px;height: 0px;"><div class="alert alert-danger" style="">Error!</div></div>
    <h1><?php echo translate( 'Database' ) ?></h1>
    <section>
		<p><?= translate('Server') ?></p>
    	<select name="BDaccount" id="bdselector"></select>
    	<br>
    	<div style="display: none" class="ftpnew">
    		<div class="form-group">
	    		<p>* <?= translate('Database Handler') ?></p>
	    	    <select required name="typeDatabase" id ="typeDatabase">
		    		<option value="0" selected><?= translate('MySQL') ?></option>
		    		<option value="1"><?= translate('SQL Server') ?></option>
	    		</select>
    		</div>
    	   	<p><?= translate('Type a Database server, user name and password') ?></p>
<? 		if (getMDVersion()>=esveServicesConnectionName) { ?>
			<div class="form-group">
				<label for="BDName">* <?= translate('Name') ?></label>
				<input type="text" autocomplete="off"  required requiredisnull="BDaccount" class="form-control" id="BDName" name="BDName">
			</div>
<? 		} ?>
	    	<div class="form-group">
				<label for="ftpServer">* <?= translate('Server') ?></label>
			    <input type="text" pattern="^(ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$" required requiredisnull="ftpaccount" class="form-control" id="httpServer" name="httpServer">
			</div>
	    	<div class="form-group">
				<label for="ftpUsername">* <?= translate('User Name') ?></label>
			    <input type="text" autocomplete="off" class="form-control" id="httpUsername" name="httpUsername">
			</div>
			<div class="form-group">
				<label for="ftpPassword">* <?= translate('Password') ?></label>
			    <input type="password" autocomplete="off" class="form-control" id="httpPassword" name="httpPassword">
			</div>
			<div class="form-group">
				<label for="ftpParameters">* <?= translate('Database') ?></label>
			    <input type="text" autocomplete="off" class="form-control" id="httpParameters" name="httpParameters">
			</div>
		</div>
		<div class="loadingDiv" style="display: none;">
	   		<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
		</div>
    </section>

    <h1><?php echo translate( 'Table' ) ?></h1>
    <section>
    	<div style="margin-top: 70px;width: 370px;margin-left: auto;margin-right: auto;">
			<label>* <?php echo translate( 'Select Table' ) ?></label>
			<select id="tables" name="Tables"></select>
		</div>
    </section>

    <h1 title="<?php echo translate( 'Attributes' ) ?>"><?php echo translate( 'Attributes' ) ?></h1>
    <section class="finishstep">
    	<div class="mappingselect">
	    	<div>
	    		<div class="catalogname form-group">
	    			<label for="catalogName"><?php echo translate( 'Catalog name' ) ?></label>
	    			<input type="text" class="form-control" id="catalogName" name="catalogName"  placeholder="<?php echo translate( 'Name' ) ?>">
	  			</div>
	  		</div>
	    	<div class="fields"></div>
	    	<div class="fieldsnew">
	    		<h3><?php echo translate( 'Add new attributes' ) ?></h3>
	    		<div class="listnewfields"></div>
	    	</div>
    	</div>
    	<div class="creatingtables" style="display: none">
    		<img src="loadexcel/images/loading1.gif" style="vertical-align: sub;"><?php echo translate( 'Creating tables and loading data...' ) ?>
    	</div>
    </section>
    <div style="display: none" id="tLoadExcel"><?php echo translate( 'Load from ODBC' ) ?></div>
    <div style="display: none" id="tPrevious"><?php echo translate( 'Previous' ) ?></div>
    <div style="display: none" id="tFinish"><?php echo translate( 'Finish' ) ?></div>
    <div style="display: none" id="tNext"><?php echo translate( 'Next' ) ?></div>
    <div style="display: none" id="tSelectAttribute"><?php echo translate( 'Select Attribute' ) ?></div>
    <div style="display: none" id="tCreatingTables"><?php echo translate( 'Creating tables and loading data' ) ?></div>
    <div style="display: none" id="tInvalidname"><?php echo translate( 'Invalid catalog name' ) ?></div>
    <div style="display: none" id="tInvalidfields"><?php echo translate( 'Invalid fields' ) ?></div>
</form>
