<?php

class ExcelUpload
{
	public $id;
	public $path;
	
	public $lastErrorMsg;
	
	public $excel;
	//@JAPR 2016-06-23: Intento de estandarizar el código a la manera de programar de eForms
	public $Repository;						//Instancia del repositorio como en todas las clases creadas para eForms con el Framework original
	//@JAPR
	
	//@JAPR 2016-06-23: Intento de estandarizar el código a la manera de programar de eForms
	//Agregado el parámetro $aRepository
	function __construct($Path, $id, $aRepository = null)
	{
		$this->path = $Path;
		
		$this->id = $id;
		//@JAPR 2016-06-23: Intento de estandarizar el código a la manera de programar de eForms
		$this->Repository = $aRepository;
	}
	
	function getColumns( $sheet )
	{
		$res = true;
		
		if( !$this->_isOpen() )
			$res = $this->_open();
		
		if( !$res ) return false;
		
		$excelColumns = $this->excel->getColumns($sheet, $error);
		if($error)
		{
			$this->lastErrorMsg = "Could not get the excel columns";
			return false;
		}
		//$this->excel->close();
		return $excelColumns;
	}
	
	function getDefinitionColumns( $aConnection )
	{
		$arrMembers = array();
		
		if( $aConnection )
		{
			$rs = $aConnection->Execute( "SELECT MemberName, MemberID, fieldName FROM SI_SV_DataSourceMember WHERE DataSourceID = " . $this->id );
		
			while($rs && !$rs->EOF)
			{
				$arrMembers[] = array("value" => $rs->fields['fieldName'], "text" => $rs->fields['MemberName']);
				$rs->MoveNext();
			}
		}
		
		return $arrMembers;
	}

	function uploadCatalog($fields, $sheet, $catalogName, $aConnection, $theUser, $theRepository, $DataSourcetype = 2, $document = "", $path = "", $accountID = "", $idGoogleDrive = "")
	{
		//@JRPP 2015-09-09: Se agrega el update al catalago de DataSource
		$aConnection->Execute("UPDATE SI_SV_DataSource SET RecurActive = 1, id_ServiceConnection = " . $accountID . ", SheetName = " . $aConnection->quote($sheet) . ", Path = " . $aConnection->quote($path) . ", Document = ". $aConnection->quote($document) .", Type = " . $DataSourcetype . ($idGoogleDrive!=""?", IdGoogleDrive = " .  $aConnection->quote($idGoogleDrive): "") . " WHERE DataSourceID = " . $this->id);
		/** Buscamos la tabla */
		$rs = $aConnection->Execute("SELECT TableName FROM SI_SV_DataSource WHERE DataSourceID = " . $this->id);
		//@JRPP 2015-10-22: Cuando cargamos un nuevo documento es necesario actualizar los posibles campos que hayan cambiado
		foreach ($fields as $key => $value) {
			if ($value && $value["columnexcel"] != "" && $value["columntable"] != "") {
				$sql = "UPDATE SI_SV_datasourcemember SET MemberName = " . $aConnection->quote($value["columnexcel"]) . " WHERE fieldname = " . $aConnection->quote($value["columntable"]) ;
				$aConnection->Execute($sql);
			}
		}
		if( $rs )
		{
			$table = $rs->fields['TableName'];

			if( $this->_insertData( $fields, $table, $sheet, $aConnection ) === false ) {
				return false;
			}
			//@JRPP 2015-09-07: Se agrega el save() al actualizar el Datasource
			$aDataSource = BITAMDataSource::NewInstanceWithID($theRepository,$this->id);
			if (!is_null($aDataSource)) {
				$aDataSource->updateAllCatalogDefinitions(false, true, true);
			}
			
			//@JAPR 2015-08-02: Agregada la actualización de versión de las definiciones
			BITAMDataSource::updateLastUserAndDateInDataSource($theRepository, $this->id);
			//@JAPR		
		}

		return true;

	}

	function _truncateTable( $table, $aConnection )
	{
		
		$aConnection->Execute("TRUNCATE TABLE {$table}");
	}
	
	function createCatalog($fields, $sheet, $catalogName, $aConnection, $theUser, $theRepository, $DataSourcetype = 2, $document = "", $path = "", $accountID = "", $idGoogleDrive = "")
	{

		/** Creamos la tabla donde se cargaran los valores del excel*/
		if( ( $infoTable = $this->_createTable($fields, $aConnection ) ) === false )
			return false;
			
		/** Con la tabla creada, ahora cargamos los datos del excel a la tabla de la base de datos*/
		if( $this->_insertData( $infoTable['columns'], $infoTable['table'], $sheet, $aConnection ) === false ) return false;

		/** La tabla ha sido creada y los registros cargados, agregamos la informacion a la metadata */
		/** Obtenemos el id maximo de la tabla de catalogos */
		$maxId = 1;
		$rs = $aConnection->Execute('SELECT max( DataSourceID ) as MAX FROM SI_SV_DataSource');
		if( $rs && $rs->RecordCount() != 0 ) $maxId = $rs->fields['MAX'] + 1;

		$this->id = $maxId;

		/** Insertamos la informacion del catalogo */
		//@JAPR 2015-09-18: Corregido un bug, no se estaba reportando el error exacto, así que se agrega la descripción del error de ADODB
		//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
		$currentDate = date("Y-m-d H:i:s");
		$strAdditionalFields = ', LastModUserID, LastModDate';
		$strAdditionalValues = ', '.(int) @$_SESSION["PABITAM_UserID"].
			', '.$theRepository->DataADOConnection->DBTimeStamp($currentDate);
		//@JAPR
		if ($document ==""){
			//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
			if( $aConnection->Execute( "INSERT INTO SI_SV_DataSource (DataSourceID, DataSourceName, TableName, VersionNum, CreationUserID, CreationDate, Type, LastSyncDate, RecurActive {$strAdditionalFields}) VALUES ({$maxId}, ". $aConnection->quote($catalogName) .",". $aConnection->quote($infoTable['table']) .",0,{$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .", " . $DataSourcetype .", " . $aConnection->quote( date("Y-m-d H:i:s") ) . ", 1 {$strAdditionalValues})" ) === false )
			{
				/** Ahora insertamos los miembros del catalogo */
				$this->lastErrorMsg = "Error insert metadata: ".$aConnection->ErrorMsg();
				return false;
			}
		}else {
			//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
			if( $aConnection->Execute( "INSERT INTO SI_SV_DataSource (DataSourceID, DataSourceName, TableName, VersionNum, CreationUserID, CreationDate, Type, Document, SheetName, Path, id_ServiceConnection, IdGoogleDrive, RecurActive {$strAdditionalFields}) VALUES ({$maxId}, ". $aConnection->quote($catalogName) .",". $aConnection->quote($infoTable['table']) .",0,{$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .", " . $DataSourcetype . ", " . $aConnection->quote($document) . ", " . $aConnection->quote($sheet) . ", " . $aConnection->quote($path) .", " . $aConnection->quote($accountID) . ", " . $aConnection->quote($idGoogleDrive) .", 1 {$strAdditionalValues})" ) === false )
			{
				/** Ahora insertamos los miembros del catalogo */
				$this->lastErrorMsg = "Error insert metadata: ".$aConnection->ErrorMsg();
				return false;
			}
		}


		/** Obtenemos el id maximo de la tabla de catalogos */
		$maxMemberID = 1;
		$rs = $aConnection->Execute('SELECT max( MemberID ) as MAX FROM SI_SV_DataSourceMember');
		if( $rs && $rs->RecordCount() != 0 ) $maxMemberID = $rs->fields['MAX'] + 1;

		foreach ($infoTable['columns'] as $key => $value) {
			$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate) VALUES ({$maxId}, {$maxMemberID},". $aConnection->quote($value["columnexcel"]) .", ". $aConnection->quote($value["columntable"]) .", ". ($key + 1) .", 6, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .")" );
			$maxMemberID += 1;
		
		}
		
		$aDataSource = BITAMDataSource::NewInstanceWithID($theRepository,$maxId);
		if (!is_null($aDataSource)){
			$aDataSource->updateAllCatalogDefinitions(false, true, true);
		}

		return true;
	}
	
	/**
	* Inserta en la base de datos la informacion contenida en la hoja de excel
	* @param  array 	$columns	Un array que contiene el nombre de la columna en el excel (columnexcel) y el nombre de la columna equivalente en la base de datos (columntable)
	* @param  string 	$table		Nombre de la tabla en la base de datos
	*/
	function _insertData($columns, $table, $sheet, $aConnection)
	{
		$res = true;
		if( !$this->_isOpen() )
			$res = $this->_open();
		
		if( !$res ) return false;
		
		$excelColumns = array();
		//@JAPR 2015-11-20: Agregado un array exclusivo para responder mensajes de error, porque en ese caso no debe estar decodificado el texto o generará error en los Inserts
		//hacia las tablas del Metadata (originalmente en SI_SV_DataSource durante el reporte de errores de sincronización)
		$queryColumns = array();
		//@JAPR
		$tableColumns = array();
		$arrValuesQuery = array();
		$sQueryInsert = "INSERT INTO {$table} ";
		//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
		//Se genera el array con los campos que son tipo imagen para realizar el proceso de ajuste y descarga de la imagen
		$arrImageFieldNamesColl = array();
		//JAPR 2016-06-27: Corregido un bug, cuando es una carga de catálogo durante la creación, no viene asignado el id así que no puede utilizar esta función (#R3Q9JQ)
		if ($this->id) {
			$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->id);
			if (!is_null($objDataSourceMembersColl)) {
				foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
					if ($objDataSourceMember->IsImage) {
						$arrImageFieldNamesColl[$objDataSourceMember->FieldName] = $objDataSourceMember->FieldName;
					}
				}
			}
		}
		//@JAPR
		
		foreach ($columns as $value)
		{
			if( trim( $value['columnexcel'] ) == '' ) 
			{
				//@JAPR 2015-11-20: Agregado un array exclusivo para responder mensajes de error, porque en ese caso no debe estar decodificado el texto o generará error en los Inserts
				//hacia las tablas del Metadata (originalmente en SI_SV_DataSource durante el reporte de errores de sincronización)
				$queryColumns[] = 'null';
				//@JAPR
				$excelColumns[] = 'null';
				$tableColumns[] = $value['columntable'];	
			} else
			{
				//@JAPR 2015-11-20: Agregado un array exclusivo para responder mensajes de error, porque en ese caso no debe estar decodificado el texto o generará error en los Inserts
				//hacia las tablas del Metadata (originalmente en SI_SV_DataSource durante el reporte de errores de sincronización)
				$queryColumns[] = '[' .$value['columnexcel'] . '] as ['. $value['columntable'] .']';
				//@JAPR
				$excelColumns[] = '[' .utf8_decode($value['columnexcel']) . '] as ['. $value['columntable'] .']';
				$tableColumns[] = $value['columntable'];
			}
		}
		
		$sQueryInsert .= "( ". implode(',', $tableColumns) ." ) VALUES (". implode(',', array_fill(0, count($tableColumns), '%s')) .")";
		
		$sQuery = "SELECT " . implode(',', $excelColumns ) . " FROM [%s]";
		//@JAPR 2015-11-20: Agregado un array exclusivo para responder mensajes de error, porque en ese caso no debe estar decodificado el texto o generará error en los Inserts
		//hacia las tablas del Metadata (originalmente en SI_SV_DataSource durante el reporte de errores de sincronización)
		$sQueryErr = "SELECT " . implode(',', $queryColumns ) . " FROM [%s]";
		//@JAPR
		
		/** Realizamos el query a la hoja de excel */
		$rs = $this->excel->execute($sQuery, $sheet, $error);
		
		if($error)
		{
			//@JAPR 2015-11-20: Agregado un array exclusivo para responder mensajes de error, porque en ese caso no debe estar decodificado el texto o generará error en los Inserts
			//hacia las tablas del Metadata (originalmente en SI_SV_DataSource durante el reporte de errores de sincronización)
			$this->lastErrorMsg = "Invalid query: '{$sQueryErr}'";
			//@JAPR
			$this->excel->close();
			return false;
		}
		
		/** Si el query es valido y hay datos, truncamos la tabla antes de eliminar los datos */
		if( ($rs != false && !$rs->EOF) )
			$this->_truncateTable( $table, $aConnection );

		while($rs != false && !$rs->EOF)
		{
			$arrValuesQuery = array();
			//@MAPR 2016-07-29: Se creo una variable boolean para verificar que todas las celdas estaban vacías. Issue #79OEVC
			$flag = false;
			foreach ($columns as $i => $value)
			{

				//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
				$strFieldValue = utf8_encode( $rs->Fields($i)->value );
				$intIsImage = isset($arrImageFieldNamesColl[(string) @$value['columntable']]);
				$strErrorMsg = "";
				if ($intIsImage) {
					//En este caso se forzará a regresar un nivel, ya que el php invocado está corriendo desde una subcarpeta (loadexcel/action.php)
					$strFieldValue = GetRelativeUserImgFileNameFromURL($strFieldValue, '../', $strErrorMsg);
				}
				$fieldValue = trim( $strFieldValue );
				//@MAPR 2016-07-29: Se valida que el valor del campo sea diferente de vacío. Issue #79OEVC

				if ( $fieldValue != '' ) {
					$flag = true;
				}
				$arrValuesQuery[] = $aConnection->quote( $strFieldValue );
				//@JAPR
			}
			//@MAPR 2016-07-29: Si pasa la verificación se escribirá en la tabla. Issue #79OEVC
			if ( $flag ) {
			$sQuery = call_user_func_array('sprintf', array_merge( array( $sQueryInsert ), $arrValuesQuery ));
			
			$aConnection->Execute( $sQuery );
			}
			
			$rs->MoveNext();
		}
		$this->excel->close();
		return true;
	}

	function addAttrs($attrs, $aConnection, $theUser)
	{
		/** Obtenemos el id maximo de la tabla de catalogos */
		$maxMemberID = 1;
		$rs = $aConnection->Execute('SELECT max( MemberID ) as MAX FROM SI_SV_DataSourceMember');
		if( $rs && $rs->RecordCount() != 0 ) $maxMemberID = $rs->fields['MAX'] + 1;

		$maxMemberOrder = 1;
		$rs = $aConnection->Execute('SELECT MAX(MemberOrder) AS maxMemberOrder FROM SI_SV_DataSourceMember WHERE DataSourceID = ' . $this->id);
		if( $rs && $rs->RecordCount() != 0 ) $maxMemberOrder = $rs->fields['maxMemberOrder'] + 1;		

		foreach($attrs as $eachField)
		{
			$columnName = 'F' . $this->_getNextKey();
			$arrColumns[] = $columnName . ' varchar(512)';
			$arrColumnsName[] = array('columnexcel' => $eachField['name'], 'columntable' => $columnName);

			$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate) VALUES ({$this->id}, {$maxMemberID},". $aConnection->quote( $eachField['name'] ) .", ". $aConnection->quote( $columnName ) .", ". $maxMemberOrder .", 6, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .")" );

			$maxMemberID += 1;

			$maxMemberOrder += 1;
		}

		/** Buscamos la tabla */
		$rs = $aConnection->Execute("SELECT TableName FROM SI_SV_DataSource WHERE DataSourceID = " . $this->id);

		if( $rs )
		{
			$table = $rs->fields['TableName'];

			$aConnection->Execute( "ALTER TABLE {$table} ADD (". implode(',', $arrColumns) .")" );
		}

		return $arrColumnsName;
	}
	
	function _createTable($fields, $aConnection )
	{
		$arrColumns = array();
		
		$table = 'SI_SV_CAT' . $this->_getNextKey();

		foreach($fields as $eachField)
		{
			$columnName = 'F' . $this->_getNextKey();
			$arrColumns[] = $columnName . ' varchar(512)';
			$arrColumnsName[] = array('columnexcel' => $eachField['name'], 'columntable' => $columnName);
		}

		if( $aConnection->Execute( 'CREATE TABLE ' . $table . '(id int NOT NULL AUTO_INCREMENT, '. implode(',', $arrColumns) .', PRIMARY KEY (id) )' ) === false ) return false;
		
		return array('columns' => $arrColumnsName, 'table' => $table);

	}
	
	function _open()
	{
		if( !is_file( $this->path ) )
		{
			$this->lastErrorMsg = "File not found";
			return false;
		}
			
		$excel = new Excel( $this->path );
		
		$result = $excel->open();	
		if(is_array($result) && $result["success"] == false)
		{
			$this->lastErrorMsg = $result["error"];
			return false;
		}
		
		$this->excel = $excel;
		
		return true;
	}
	
	function _isOpen()
	{
		return !is_null( $this->excel );
	}
	
	function getErrorMsg()
	{
		return $this->lastErrorMsg;
	}
	
	function _getNextKey()
	{
		$nextKey = uniqid(rand(), false);
		
		return strtoupper($nextKey);
	}
	
}

?>
