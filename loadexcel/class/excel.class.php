<?php

class Excel
{
	public $path = '';
	public $conn = null;
	
	public $sheets = null;
	public $columns = null;
	
	function __construct($Path)
	{
		$this->path = $Path;
	}
	
	function open()
	{
		//$ext = substr($this->path,-4);
		//@JAPR 2017-01-17: (ARamirez) Corregido un bug, se debe hacer conversión a minúsculas para que pase la validación correctamente
		$ext = strtolower(pathinfo($this->path, PATHINFO_EXTENSION));
		//@JAPR
		//var_dump($this->path);
		
		if ($ext == "xls")
			$connStr = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='. $this->path .';Extended Properties="Excel 8.0;HDR=YES;IMEX=1;";';
		elseif ($ext == "xlsx")
			$connStr = 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='. $this->path .';Extended Properties="Excel 12.0;HDR=YES;IMEX=1;";';
		elseif ($ext == "xlsb")
			$connStr = 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='. $this->path .';Extended Properties="Excel 12.0;HDR=YES;IMEX=1;";';
		elseif ($ext == "xlsm")
			$connStr = 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='. $this->path .';Extended Properties="Excel 12.0 Macro;HDR=YES;IMEX=1;";';
			
		//echo $connStr;
		
		try
		{
			$conn = new COM("ADODB.Connection");
			$conn->Open($connStr);		
		}
		catch(Exception  $e)
		{
			return array("success" => false, "error"=>$e->getMessage());
		}
		
		$this->conn = $conn;
		
		return $conn;
	}
	
	function close()
	{
		$this->conn->Close();
		$this->conn = null;	
	}
	
	function getSheets(&$error)
	{
		$rsSheets = null;
		$tables = array();
		
		if ($this->sheets == null)
		{			
			try
			{
				$rsSheets = $this->conn->OpenSchema(20);
				$this->sheets = $rsSheets;
			}
			catch(Exception  $e)
			{
				if(class_exists("PHPExcel_Reader_Excel5"))
				{
					$ext = pathinfo($this->path, PATHINFO_EXTENSION);
					if($ext == 'xls')
						$objReader = new PHPExcel_Reader_Excel5();
					if($ext == 'xlsx')
						$objReader = new PHPExcel_Reader_Excel2007();
						
					$objReader->setReadDataOnly(true);
					$objPHPExcel = $objReader->load( $this->path );
					
					$tables = $objPHPExcel->getSheetNames();
					foreach ($tables as $key => $value)
					{
						$tables[$key] = utf8_encode( $value ) . "$";
					}
					
					return $tables;
					//exit();
				}
				else 
				{
					$this->sheets = null;
					$error = $e->getMessage();
					return false;
				}
			}
		}
		else 
		{
			$rsSheets = $this->sheets;
		}
		
		
		while (!$rsSheets->EOF)
		{
			$tables[] = str_replace("'", "", utf8_encode( $rsSheets->Fields('TABLE_NAME')->value ));
			$rsSheets->MoveNext();
		}
		
		return $tables;
	}

	function getColumns($sheet, &$error)
	{
		$rsColumns = null;
		$columns = array();
		
		$ext = pathinfo($this->path, PATHINFO_EXTENSION);
		
		//var_dump($this->path);
		
		if ($this->columns == null)
		{
			//if ($ext == "xlsx")
			//siempre usamos el metodo de XLSX para optener las columnas por que el OpenSchema me regresa a veces el siguiente error:
			//Object or provider is not capable of performing requested operation.
			if(true)
			{
				$columnsFn = array();
				for($i=1; $i<=255; $i++)
				{
					$columnsFn[] = 'F'.$i;
				}
				
				
				$aQuery = "SELECT  * FROM [%sA:IU]";
				$rs = $this->execute($aQuery, $sheet, $error);
				if (trim($error))
				{
					//echo $error;
					return false;
				}
				$num_columns = $rs->Fields->Count();
				for ($i=0; $i<$num_columns; $i++)
				{
					if (!in_array($rs->Fields($i)->name, $columnsFn))
						$columns[] = utf8_encode( $rs->Fields($i)->name );
				}
				
				$this->columns = $columns;
			}
			else 
			{
				try
				{
					$rsColumns = $this->conn->OpenSchema(4);
				}
				catch(Exception  $e)
				{
					$this->columns = null;
					$error = $e;
					return false;
				}
	
				
				while (!$rsColumns->EOF)
				{
					$table_name = str_replace("'", "", $rsColumns->Fields('TABLE_NAME')->value);
					
					if ($table_name == $sheet)
					{
						//OMMC 2015-11-05: Este UTF8 estaba provocando que los catálogos que son de excel al tener atributos con acentos, no se mostraran en el formulaEditor.
						$columns[] = $rsColumns->Fields('COLUMN_NAME')->value;
						//$columns[] = utf8_encode( $rsColumns->Fields('COLUMN_NAME')->value );
					}
					$rsColumns->MoveNext();
				}
	
				$this->columns = $columns;
			}
		}
		
		return $this->columns;
		
		
		/*	
		if ($this->columns == null)
		{			
			try
			{
				$rsColumns = $this->conn->OpenSchema(4);
			}
			catch(Exception  $e)
			{
				$this->columns = null;
				$error = $e;
				return false;
			}

			
			while (!$rsColumns->EOF)
			{
				$table_name = str_replace("'", "", $rsColumns->Fields('TABLE_NAME')->value);
				
				if ($table_name == $sheet)
				{
					$columns[] = $rsColumns->Fields('COLUMN_NAME')->value;
				}
				$rsColumns->MoveNext();
			}

			$this->columns = $columns;
			
		}
		else 
		{
			$columns = $this->columns;
		}
		
		return $columns;*/
	}
	
	// busca en todas las hojas de excel el query valido. Regresa el RS del primero que encuentre.
	
	function execute($aSQL, $sheet, &$error)
	{
		$rs = null;
		
		$table = $sheet;
		//echo '<br>'.'$table= "' . $table.'"'.'<br>';
		//$aSQL_tmp = sprintf($aSQL, $table);
		$aSQL_tmp = str_replace("[%s", "[".$table."", $aSQL);
				
		try{
			$rs = $this->conn->Execute($aSQL_tmp);    // Recordset
			$found = true;
		}
		catch (Exception $e)
		{
			$found = false;
			$error = $e->getMessage();
			$error .= "\n\nQuery: " . $aSQL_tmp;
		}
		
		return $rs;
	}
	
	function execute_s($aSQL, &$error)
	{
		$rsSheets = null;
				
		if ($this->sheets == null)
		{			
			try
			{
				$rsSheets = $this->conn->OpenSchema(20);
				$this->sheets = $rsSheets;
			}
			catch(Exception  $e)
			{
				$this->sheets = null;
				$error = $e;
				return false;
			}
		}
		else 
		{
			$rsSheets = $this->sheets;
		}
		
				
		$found = false;
		
		while (!$rsSheets->EOF)
		{
			$field = $rsSheets->Fields('TABLE_NAME')->value;
			//echo '$field= "' . $field.'"';
			if (!$found)
			{
				if (substr($field, -1) == '$' || substr($field, -2) == "$'")
				{
					$table = $field;
					//echo '<br>'.'$table= "' . $table.'"'.'<br>';
					//$aSQL_tmp = sprintf($aSQL, $table);
					$aSQL_tmp = str_replace("[%s", "[".$table."", $aSQL);
					
					try{
						$rs = $this->conn->Execute($aSQL_tmp);    // Recordset
						$found = true;
					}
					catch (Exception $e)
					{
						$found = false;
						$error = $e;
						$error .= '<br><br><strong>Query:</strong> ' . $aSQL_tmp;
					}
				}
			}
			$rsSheets->MoveNext();
		}
		
		if ($found)
		{
			return $rs;
		}
		else
		{ 
			return false;
		}
		
	}
}


?>
