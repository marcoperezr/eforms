﻿<?php
header("Content-Type:application/json; charset=UTF-8");
$action = $_REQUEST['action'];
$selectedSheet = @$_REQUEST['sheet'];
if( is_string($selectedSheet) ) $selectedSheet = utf8_decode( $selectedSheet );
/*@MAPR 2016-08-02: En la cadena esta entrando un valor ("%23") que representa a un simbolo ("#") que una dirección no podría reconocer 
En caso de que contenga el valor se devuelve el simbolo que se removio en javascript.*/
if ( strpos($selectedSheet, "%23") !== false ) {
	$selectedSheet = $selectedSheet.replace("%23", "#");
}
$id = @$_REQUEST['id'];
$upload_dir = 'uploads/';

chdir('../');
require_once('cubeClasses.php');
require_once 'checkCurrentSession.inc.php';
require_once("dataSource.inc.php");
chdir('loadexcel');
require_once 'class/excel.class.php';
require_once "class/excelupload.class.php";


/** Valores testing*/
$aConnection = $theRepository->DataADOConnection;
$aConectionBMD = $theRepository->ADOConnection;
if( $action == 'uploadCatalog' )
{
	require('class/Uploader.class.php');
	
	$valid_extensions = array('xls', 'xlsx');
	$type = @$_REQUEST['type'];
	$filename = @$_REQUEST['filename'];
	if ($filename){
		switch ($type) {
		    case "5":
		        $upload_dir = 'uploads/Datasources/FTP/';
		        break;
		    case "3":
		        $upload_dir = "uploads/Datasources/googledrive/";
		        break;
		    case "2":
		        $upload_dir = "uploads/Datasources/dropbox/";
		        break;
		}
	}else{
		$Upload = new FileUpload('uploadfile');

		if( !is_dir( $upload_dir ))
		{
			mkdir($upload_dir, 0777, true);
		}

		$result = $Upload->handleUpload($upload_dir, $valid_extensions);
		//@JAPR 2018-01-12: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
		//Corregido un bug, no estaba validando en caso de haber resportado algún error el proceso de upload del archivo
		if ( !$result ) {
			echo json_encode( array("success" => false, "msg" => $Upload->getErrorMsg()) );
			exit();
		}
		//@JAPR
		$filename =  $Upload->getFileName();
	}
	$excel = new Excel( $upload_dir . '/' . $filename );
	
	$result = $excel->open();	
	if(is_array($result) && $result["success"] == false)
	{
		echo json_encode( array("success" => false, "msg" => $result["error"]) );
		exit();
	}
	
	$sheets = $excel->getSheets($error);	
	if($error)
	{
		echo json_encode( array("success" => false, "msg" => "Could not get the excel sheets") );
		exit();
	}
	
	if (!$result) {
		$excel->close();
		echo json_encode(array('success' => false, 'msg' =>$filename));   
	} else {
		
		echo json_encode(array('success' => true, 'file' => $filename, 'sheets' => $sheets));
	}
	exit();
}

if( $action == 'loadApps' ) {
	$appId = array();
	$appName = array();
	$rs = $aConectionBMD->Execute("SELECT id_app, applicationname FROM SI_FORMS_APPS WHERE id_app NOT IN (1,10000)");
	while ($rs && !$rs->EOF){
		$appId[] = $rs->fields["id_app"];
		$appName[] = $rs->fields["applicationname"];
		$rs->MoveNext();
	}
	$noRecords = translate("No records");
	echo json_encode(array('success' => true, 'appId' => $appId, 'appName' => $appName, 'noRecords' => $noRecords));

	exit();
}

if( $action == 'changeRecurActive' ) {
	$dataSourceID = $_REQUEST['dataSourceID'];
	$appId = array();
	$appName = array();
	if ($dataSourceID) {
		$aConnection->Execute("UPDATE SI_SV_DATASOURCE SET RecurActive = 0 WHERE DataSourceID = " . $dataSourceID);
		BITAMDataSource::updateLastUserAndDateInDataSource($theRepository, $dataSourceID);
	}
	echo json_encode(array('success' => true, 'dataSourceID' => $dataSourceID));

	exit();
}

if( $action == 'loadFTPConfiguration' ) {
	$ftpId = array();
	$ftpName = array();
	$user = array();
	$pass = array();
	$ftp = array();
	$rs = $aConnection->Execute("SELECT id,name FROM si_sv_servicesconnection WHERE TYPE = 5");
	while ($rs && !$rs->EOF){
		$ftpId[] = $rs->fields["id"];
		$ftpName[] = $rs->fields["name"];
		$rs->MoveNext();
	}
	echo json_encode(array('success' => true, 'ftpId' => $ftpId, 'ftpName' => $ftpName));
	exit();
}

if( $action == 'getUserAndPass' ) {
	$id = $_REQUEST['idFTP'];
	$user = "";
	$pass = "";
	$ftp = "";
	$rs = $aConnection->Execute("SELECT user,pass,ftp FROM si_sv_servicesconnection WHERE id = " . $id);
	if($rs && !$rs->EOF){
		$user= $rs->fields["user"];
		$pass= $rs->fields["pass"];
		$ftp= $rs->fields["ftp"];
	}
	echo json_encode(array('success' => true, 'user' => $user, 'pass' => $pass, 'ftp'=> $ftp));
	exit();
}

if( $action == 'loadCatalogs' ) {
	$appId = $_REQUEST['Appid'];
	$sectionId = array();
	$sectionName = array();
	$rs = $aConectionBMD->Execute("SELECT id_section, sectionname FROM SI_FORMS_SECTIONS WHERE appid = " . $appId);
	while ($rs && !$rs->EOF){
		$sectionId[] = $rs->fields["id_section"];
		$sectionName[] = $rs->fields["sectionname"];
		$rs->MoveNext();
	}
		
	echo json_encode(array('success' => true, 'sectionId' => $sectionId, 'sectionName' => $sectionName));

	exit();
}

if( $action == 'getConexion' ) {
	$idConnection = @$_REQUEST['idConnection'];
	$ftp ="";
	$user = "";
	$pass = "";
	$attributes = 'ftp, user, pass ';
	if (getMDVersion()>=esveServicesConnectionName) {
		$attributes .= ', servicename '; 
	}
	$rs = $aConnection->Execute("SELECT {$attributes} FROM SI_SV_Servicesconnection  WHERE type = 5 AND id = " . $idConnection);
	if ($rs && !$rs->EOF){
		$arrReturn['success'] = true;
		$arrReturn['ftp'] = @$rs->fields["ftp"];
		$arrReturn['user'] = @$rs->fields["user"];
		$arrReturn['pass'] = @$rs->fields["pass"];
		if (getMDVersion()>=esveServicesConnectionName) {
			$arrReturn['servicename'] = @$rs->fields["servicename"];
		}
		echo json_encode($arrReturn);
		exit();
	}
		echo json_encode(array('success' => false));
	exit();
}

if( $action == 'getHttpConexion' ) {
	$idConnection = @$_REQUEST['idConnection'];
	$http ="";
	$user = "";
	$pass = "";
	$attributes = 'name, user, pass ';
	if (getMDVersion()>=esveServicesConnectionName) {
		$attributes .= ', servicename '; 
	}
	$rs = $aConnection->Execute("SELECT {$attributes} FROM SI_SV_Servicesconnection  WHERE type = 6 AND id = " . $idConnection);
	if ($rs && !$rs->EOF){
		$arrReturn['success'] = true;
		$arrReturn['http'] = @$rs->fields["name"];
		$arrReturn['user'] = @$rs->fields["user"];
		$arrReturn['pass'] = @$rs->fields["pass"];
		if (getMDVersion()>=esveServicesConnectionName) {
			$arrReturn['servicename'] = @$rs->fields["servicename"];
		}
		echo json_encode($arrReturn);
		exit();
	}
		echo json_encode(array('success' => false, 'http' => $http, 'user' => $user,'pass' => $pass));
	exit();
}
if( $action == 'getTableConexion' ) {
	$idConnection = @$_REQUEST['idConnection'];
	$http ="";
	$user = "";
	$pass = "";
	if (getMDVersion()>=esvTypeOfDatabase) {
		$sql = "SELECT name, user, pass, servicedbtype, servicename FROM SI_SV_Servicesconnection  WHERE type = 7 AND id = " . $idConnection;
	}else {
		$sql = "SELECT name, user, pass FROM SI_SV_Servicesconnection  WHERE type = 7 AND id = " . $idConnection;	
	}
	$rs = $aConnection->Execute($sql);
	if ($rs && !$rs->EOF){
		$http = $rs->fields["name"];
		$user = $rs->fields["user"];
		$pass = BITAMDecryptPassword($rs->fields["pass"]);
		if (getMDVersion()>=esvTypeOfDatabase) {
			$servicedbtype = $rs->fields["servicedbtype"];
			$servicename = $rs->fields["servicename"];
			echo json_encode(array('success' => true, 'http' => $http, 'user' => $user,'pass' => $pass, 'servicedbtype' => $servicedbtype, 'servicename'=> $servicename));
			exit();
		}else {
			echo json_encode(array('success' => true, 'http' => $http, 'user' => $user,'pass' => $pass));
			exit();
		}
		
	}
		echo json_encode(array('success' => false));
	exit();
}
if($action == 'validateBDconnetion'){
	$BDServer = @$_REQUEST['BDServer'];
	$BDUsername = @$_REQUEST['BDUsername'];
	$BDPassword = @$_REQUEST['BDPassword'];
	$Database = @$_REQUEST['Database'];
	$TypeOfDatabase = @$_REQUEST['TypeOfDatabase'];
	/*
	ob_start();
	$aBITAMConnection = BITAMConnection::NewInstance($BDServer, $BDUsername, $BDPassword, $Database, 'mysql');
	if (is_null($aBITAMConnection) || is_null($aBITAMConnection->ADOConnection)) {
		$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
		ob_end_clean();
		echo json_encode(array('success' => false, 'msg' => "Unable to connect to : {$strErrorMsg}"));
		exit();
		//return "Unable to connect to : {$strErrorMsg}";
	}else {
		ob_end_clean();
		echo"DESPUES ";
		var_dump($aBITAMConnection);
	}*/
	if (getMDVersion()>=esvTypeOfDatabase) {
		if ($TypeOfDatabase == 1){
			ob_start();
			try {
				$conn = odbc_connect("Driver={SQL Server};Server=$BDServer;Database=$Database;", $BDUsername, $BDPassword);
				if ($conn) {
					//@JAPR 2018-10-30: Agregado el soporte de vistas a los DataSources tipo ODBC (#JIF4Q9)
					$result=odbc_exec($conn,"select name from sysobjects where type='U' or type='V';");
					//@JAPR
					if ($result){
						while(odbc_fetch_row($result)){
							for($i=1;$i<=odbc_num_fields($result);$i++){
								$arrtables[]= odbc_result($result,$i);
							}
						}
						ob_end_clean();
						echo json_encode(array('success' => true, 'msg' => "Connect Success:", 'arrtables' => $arrtables ));
						exit();
					} else {
						ob_end_clean();
						echo json_encode(array('success' => false, 'msg' => "Failed: " . odbc_error()));
						exit();
					}
				} else{
				    throw new Exception("Connection could not be established.");
				}

			} catch (Exception $e) {
				ob_end_clean();
				echo json_encode(array('success' => false, 'msg' => "Connect failed: " . $e->getMessage()));
				exit();
			}

		}else{
			ob_start();
			try {
				$mysqli = new mysqli($BDServer,$BDUsername,$BDPassword,$Database);
				if ($mysqli->connect_error) {
					throw new Exception($mysqli->connect_error);
				}else {
					//@JAPR 2018-02-06: Corregido un bug, no estaba delimitando el nombre de la tabla en la instrucción, así que nombres con ciertos caracteres o espacios
					//provocaban un error en la consulta
					//@JAPR 2018-10-30: Agregado el soporte de vistas a los DataSources tipo ODBC (#JIF4Q9)
					$query = "SHOW FULL TABLES FROM `" . $Database . "` WHERE table_type = 'BASE TABLE' OR TABLE_TYPE LIKE 'VIEW'";
					//@JAPR
					if (($resultado = $mysqli->query($query))===false) {
						ob_end_clean();
						echo json_encode(array('success' => false, 'msg' => "Failed: " . $query));
						exit();
					}else {
						$arrtables = array();
						while ($myrow = $resultado->fetch_array())
						{
							$arrtables[]= $myrow[0];
						}
						ob_end_clean();
						echo json_encode(array('success' => true, 'msg' => "Connect Success:", 'arrtables' => $arrtables ));
						exit();
					}
					
				}
			} catch (Exception $e) {
				ob_end_clean();
				//@MAPR 2016-07-26: El mensaje de errror entra en una clasificación para saber que tipo de error es.
				$msg = "";
				if ($e->getMessage()) {
					if (strpos($e->getMessage(), 'host') !== false) {
						$msg = 'host';
					}elseif (strpos($e->getMessage(), 'database') !== false ) {
						$msg =  'database';
					}else {
						$msg ='user';
					}
				}
				echo json_encode(array('success' => false, 'msg' => $msg));
				exit();
			}
		}
	}else {
		ob_start();
		try {
			$mysqli = new mysqli($BDServer,$BDUsername,$BDPassword,$Database);
			if ($mysqli->connect_error) {
				throw new Exception($mysqli->connect_error);
			}else {
				//@JAPR 2018-02-06: Corregido un bug, no estaba delimitando el nombre de la tabla en la instrucción, así que nombres con ciertos caracteres o espacios
				//provocaban un error en la consulta
				$query = "SHOW FULL TABLES FROM `" . $Database . "` WHERE table_type = 'BASE TABLE'";
				//@JAPR
				if (($resultado = $mysqli->query($query))===false) {
					ob_end_clean();
					echo json_encode(array('success' => false, 'msg' => "Failed: " . $query));
					exit();
				}else {
					$arrtables = array();
					while ($myrow = $resultado->fetch_array())
					{
						$arrtables[]= $myrow[0];
					}
					ob_end_clean();
					echo json_encode(array('success' => true, 'msg' => "Connect Success:", 'arrtables' => $arrtables ));
					exit();
				}
				
			}
		} catch (Exception $e) {
			ob_end_clean();
			echo json_encode(array('success' => false, 'msg' => "Connect failed: " . $e->getMessage()));
			exit();
		}
	}
}
if($action == 'getColumnsFromTable'){
	$BDServer = @$_REQUEST['BDServer'];
	$BDUsername = @$_REQUEST['BDUsername'];
	$BDPassword = @$_REQUEST['BDPassword'];
	$Database = @$_REQUEST['Database'];
	$Table = @$_REQUEST['Table'];
	$TypeOfDatabase = @$_REQUEST['TypeOfDatabase'];
	$id = @$_REQUEST['id'];
	$definition= array();
	$arrdefinition= array();
	$arrExcelColumns= array();
	if (getMDVersion()>=esvTypeOfDatabase && $TypeOfDatabase == 1) {
		ob_start();
		try {
			$conn = odbc_connect("Driver={SQL Server};Server=$BDServer;Database=$Database;", $BDUsername, $BDPassword);
			if ($conn) {
				$result=odbc_columns($conn, $Database, "", $Table, "%");
				if ($result){
					$arrColumns = array();
					while(odbc_fetch_row($result)){
							$arrColumns[]= odbc_result($result, "COLUMN_NAME");
					}
					if( is_numeric( $id ) )
					{
						$sql = 'SELECT membername, fieldname FROM SI_SV_datasourceMember WHERE datasourceid = ' . $id;
						$rs = $aConnection->Execute($sql);
						while( $rs && !$rs->EOF ) {
							$definition['text']= $rs->fields['membername'];
							$definition['value']=$rs->fields['fieldname'];
							$arrdefinition[] = $definition;
							$rs->MoveNext();
						}
						/** Buscamos que columnas del Excel no existen actualmente en la definicion del catalogo */
						for($i = 0; $i < count( $arrColumns ); $i++)
						{
							$bFound = false;
							for($x = 0; $x < count( $arrdefinition ); $x++)
							{
								if( $arrdefinition[$x]['text'] == $arrColumns[$i] )
								{
									$bFound = true; break;
								}
							}

							if(!$bFound)
							{
								$arrExcelColumns[] = $arrColumns[$i];
							}
						}
					}
//					ob_end_clean();
					echo json_encode(array('success' => true, 'msg' => "Connect Success:", 'arrColumns' => $arrColumns, 'definition' => $arrdefinition, 'excelColumns' => $arrExcelColumns));
					exit();
				} else {
//					ob_end_clean();
					echo json_encode(array('success' => false, 'msg' => "Failed: " . odbc_error()));
					exit();
				}
			} else{
			    throw new Exception("Connection could not be established.");
			}

		} catch (Exception $e) {
//			ob_end_clean();
			echo json_encode(array('success' => false, 'msg' => "Connect failed: " . $e->getMessage()));
			exit();
		}
	}else {
		ob_start();
		try {
			$mysqli = new mysqli($BDServer,$BDUsername,$BDPassword,$Database);
			if ($mysqli->connect_error) {
				throw new Exception($mysqli->connect_error);
			}else {
				$query = "SHOW COLUMNS FROM " . $Table ;
				if (($resultado = $mysqli->query($query))===false) {
					ob_end_clean();
					echo json_encode(array('success' => false, 'msg' => "Failed: " . $query));
					exit();
				}
				$arrColumns = array();
				while ($myrow = $resultado->fetch_array())
				{
					$arrColumns[]= $myrow[0];
				}
				if( is_numeric( $id ) )
				{
					$sql = 'SELECT membername, fieldname FROM SI_SV_datasourceMember WHERE datasourceid = ' . $id;
					$rs = $aConnection->Execute($sql);
					while( $rs && !$rs->EOF ) {
						$definition['text']= $rs->fields['membername'];
						$definition['value']=$rs->fields['fieldname'];
						$arrdefinition[] = $definition;
						$rs->MoveNext();
					}
					/** Buscamos que columnas del Excel no existen actualmente en la definicion del catalogo */
					for($i = 0; $i < count( $arrColumns ); $i++)
					{
						$bFound = false;
						for($x = 0; $x < count( $arrdefinition ); $x++)
						{
							if( $arrdefinition[$x]['text'] == $arrColumns[$i] )
							{
								$bFound = true; break;
							}
						}

						if(!$bFound)
						{
							$arrExcelColumns[] = $arrColumns[$i];
						}
					}
				}
				ob_end_clean();
				echo json_encode(array('success' => true, 'msg' => "Connect Success:", 'arrColumns' => $arrColumns, 'definition' => $arrdefinition, 'excelColumns' => $arrExcelColumns));
				exit();
			}
		} catch (Exception $e) {
			ob_end_clean();
			echo json_encode(array('success' => false, 'msg' => "Connect failed: " . $e->getMessage()));
			exit();
		}
	}
}

if( $action == 'loadeBavelFields' ) {
	$definition  = array();
	$arrExcelColumns = array();
	$excelColumns = array();
	$excelid = array();
	$arrTagname = array();
	$sectionId = $_REQUEST['sectionId'];
	$appID = $_REQUEST['appID'];
	$arrEBavelFormsFields = array();
	$arrEBavelFormsFields = GetMappedEBavelFieldsForms($theRepository, true, $appID, array($sectionId), null, false, '');
	foreach ($arrEBavelFormsFields as $type => $arrfield) {
		foreach ($arrfield as $idfield => $field) {
			$excelColumns[] = $field["label"];
			$excelid[] = $idfield;
			$arrTagname[] = $field["tagname"];
		}
	}		
	echo json_encode(array('success' => true, 'excelColumns' => $excelColumns, 'definition' => $definition, 'arrExcelColumns' => $arrExcelColumns, 'excelID'=>$excelid, 'tagname'=>$arrTagname));

	exit();
}

if( $action == 'loadFields' )
{
	$type = @$_REQUEST['type'];
	if ($type){
		switch ($type) {
		    case "5":
		        $upload_dir = 'uploads/Datasources/FTP/';
		        break;
		    case "3":
		        $upload_dir = "uploads/Datasources/googledrive/";
		        break;
		    case "2":
		        $upload_dir = "uploads/Datasources/dropbox/";
		        break;
		}
	}
	$path = $upload_dir . $_REQUEST['file'];

	if( is_file($path) )
	{
		$anExcelUpload = new ExcelUpload( $path, $id );
		
		$definition  = array();

		$arrExcelColumns = array();
		
		$excelColumns = $anExcelUpload->getColumns( $selectedSheet, $aConnection );
		
		if( $excelColumns === false )
		{
			echo json_encode(array('success' => false, 'msg' => translate($anExcelUpload->getErrorMsg()))); 
			exit();
		}
		
		if( is_numeric( $id ) )
		{
			$definition = $anExcelUpload->getDefinitionColumns( $aConnection );

			/** Buscamos que columnas del Excel no existen actualmente en la definicion del catalogo */
			for($i = 0; $i < count( $excelColumns ); $i++)
			{
				$bFound = false;
				for($x = 0; $x < count( $definition ); $x++)
				{
					if( $definition[$x]['text'] == $excelColumns[$i] )
					{
						$bFound = true; break;
					}
				}

				if(!$bFound)
				{
					$arrExcelColumns[] = $excelColumns[$i];
				}
			}

			//if( count( $arrExcelColumns ) == count( $excelColumns ) ) $arrExcelColumns = array();
		}
		
		echo json_encode(array('success' => true, 'excelColumns' => $excelColumns, 'definition' => $definition, 'arrExcelColumns' => $arrExcelColumns));
	}
	
	exit();
}

if($action == 'UpdateFilterState') {
	$data = json_decode($_REQUEST["data"], true);

	$sql= 'UPDATE si_sv_datasourcefilter SET STATUS = ' .$data["stateValue"] . '
				WHERE FilterID = ' . $data["filterid"];
	$rs = $aConnection->Execute($sql);
	//@JAPR 2015-09-03: Corregido un bug, no estaba actualizando la versión de los catálogos que dependen de este DataSource modificado (#K8ZJZJ)
	if ($rs) {
		$sql = "SELECT CatalogID 
			FROM SI_SV_DataSourceFilter 
			WHERE FilterID = ".$data["filterid"];
		$rs = $aConnection->Execute($sql);
		if ($rs && !$rs->EOF) {
			$intDataSourceID = (int) @$rs->fields["catalogid"];
			if ($intDataSourceID > 0) {
				BITAMDataSource::updateLastUserAndDateInDataSource($theRepository, $intDataSourceID);
			}
		}
	}
	//@JAPR
	
	echo json_encode(array('success' => true));
	exit();
}

if($action == 'createDatasource') {
	$data = json_decode($_REQUEST["data"], true);
	$type = $_REQUEST["type"];
	$CatName = $data["SurveyName"];
	if ($type==1){
		//@JAPR 2016-07-29: Corregido un bug, no debe reemplazar el <enter> por otros caracteres imprimibles (#LSJYAB)
		$members = explode("\n",$data["Members"]);
		//@JAPR
	} else {
		$members = $data["Members"];
	}
	
	function getNextKey()
	{
		$nextKey = uniqid(rand(), false);
		
		return strtoupper($nextKey);
	}
	$idnewCatalog = -1;
	function createCatalog($fields,$catalogName, $data,$aConnection, $theUser, $type,$theRepository) {
		/** Creamos la tabla donde se cargaran los valores del excel*/
		if ($type == 1){
			if( ( $infoTable = createTable($fields, $aConnection ) ) === false )
				return false;
		}else if ($type == 3){
			if( ( $infoTable = createTableeBavel($fields, $aConnection ) ) === false )
				return false;
		}

		
		/** Con la tabla creada, ahora cargamos los datos del excel a la tabla de la base de datos*/
		/** La tabla ha sido creada y los registros cargados, agregamos la informacion a la metadata */
		/** Obtenemos el id maximo de la tabla de catalogos */
		$maxId = 1;
		$rs = $aConnection->Execute('SELECT max( DataSourceID ) as MAX FROM SI_SV_DataSource');
		if( $rs && $rs->RecordCount() != 0 ) $maxId = $rs->fields['MAX'] + 1;

		$idnewCatalog = $maxId;
		/** Insertamos la informacion del catalogo */
		//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
		$currentDate = date("Y-m-d H:i:s");
		$strAdditionalFields = ', LastModUserID, LastModDate';
		$strAdditionalValues = ', '.(int) @$_SESSION["PABITAM_UserID"].
			', '.$theRepository->DataADOConnection->DBTimeStamp($currentDate);
		//@JAPR
		if ($type == 1) {
			//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
			if( $aConnection->Execute( "INSERT INTO SI_SV_DataSource (DataSourceID, DataSourceName, TableName, VersionNum, CreationUserID, CreationDate, Type, RecurActive {$strAdditionalFields}) VALUES ({$maxId}, ". $aConnection->quote($catalogName) .",". $aConnection->quote($infoTable['table']) .",0,{$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .", {$type}, 1 {$strAdditionalValues})" ) === false )
			{
				/** Ahora insertamos los miembros del catalogo */

			}
			
			/** Obtenemos el id maximo de la tabla de catalogos */
			$maxMemberID = 1;
			$rs = $aConnection->Execute('SELECT max( MemberID ) as MAX FROM SI_SV_DataSourceMember');
			if( $rs && $rs->RecordCount() != 0 ) $maxMemberID = $rs->fields['MAX'] + 1;
			
			foreach ($infoTable['columns'] as $key => $value) {
				$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate) VALUES ({$maxId}, {$maxMemberID},". $aConnection->quote($value["columnexcel"]) .", ". $aConnection->quote($value["columntable"]) .", ". ($key + 1) .", 6, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .")" );
				$maxMemberID += 1;
			}
		} else if ($type == 3) {
			//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
			if( $aConnection->Execute( "INSERT INTO SI_SV_DataSource (DataSourceID, DataSourceName, TableName, VersionNum, CreationUserID, CreationDate, Type, eBavelFormID, eBavelAppID, eBavelFormType {$strAdditionalFields}) VALUES ({$maxId}, ". $aConnection->quote($catalogName) .",". $aConnection->quote($infoTable['table']) .",0,{$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .", {$type}, {$data["idCatalogeBavel"]}, {$data["idAppeBavel"]}, 1 {$strAdditionalValues})" ) === false )
			{
				/** Ahora insertamos los miembros del catalogo */

			}
			
			/** Obtenemos el id maximo de la tabla de catalogos */
			$maxMemberID = 1;
			$order = 1;
			$rs = $aConnection->Execute('SELECT max( MemberID ) as MAX FROM SI_SV_DataSourceMember');
			if( $rs && $rs->RecordCount() != 0 ) $maxMemberID = $rs->fields['MAX'] + 1;

			foreach ($infoTable['columns'] as $key => $value) {
				switch ($value["tagname"]) {
					/*Si verificamos que la carga es invocada por un FTP,Google Drive ó Dropbox se cambia el path y el tipo de datasource*/
				    case "widget_geolocation":
						$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID ,eBavelFieldDataID) VALUES ({$maxId}, {$maxMemberID},". $aConnection->quote($value["columnexcel"] . "_LATI") .", ". $aConnection->quote($value["columntable"]. "_LATI") .", ". ($order) .", -2, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$value["idfieldform"]} ,1)" );
						$maxMemberID += 1;
						$order += 1;
						$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID ,eBavelFieldDataID) VALUES ({$maxId}, {$maxMemberID},". $aConnection->quote($value["columnexcel"] . "_LONG") .", ". $aConnection->quote($value["columntable"]. "_LONG") .", ". ($order) .", -2, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$value["idfieldform"]} ,2)" );
						$maxMemberID += 1;
						$order += 1;
				        break;
				    case "widget_qr":
				    	$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID , IsImage) VALUES ({$maxId}, {$maxMemberID},". $aConnection->quote($value["columnexcel"]) .", ". $aConnection->quote($value["columntable"]) .", ". ($order) .", 20, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$value["idfieldform"]}, 1 )" );
						$maxMemberID += 1;
						$order += 1;
				        break;
				    case "widget_photo":
						$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID , IsImage) VALUES ({$maxId}, {$maxMemberID},". $aConnection->quote($value["columnexcel"]) .", ". $aConnection->quote($value["columntable"]) .", ". ($order) .", 8, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$value["idfieldform"]}, 1 )" );
						$maxMemberID += 1;
						$order += 1;
				        break;
				    case "widget_signature":
						$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID, IsImage ) VALUES ({$maxId}, {$maxMemberID},". $aConnection->quote($value["columnexcel"]) .", ". $aConnection->quote($value["columntable"]) .", ". ($order) .", 10, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$value["idfieldform"]}, 1 )" );
						$maxMemberID += 1;
						$order += 1;
				        break;
				    default:
						$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID ) VALUES ({$maxId}, {$maxMemberID},". $aConnection->quote($value["columnexcel"]) .", ". $aConnection->quote($value["columntable"]) .", ". ($order) .", 6, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$value["idfieldform"]} )" );
						$maxMemberID += 1;
						$order += 1;
				        break;
				}
			}
		}


		$aDataSource = BITAMDataSource::NewInstanceWithID($theRepository,$maxId);
		if (!is_null($aDataSource)) {
			$aDataSource->updateAllCatalogDefinitions(false, true, true);
		}

		return $idnewCatalog;
	}

	function createTable($fields, $aConnection ) {
		$arrColumns = array();
		
		$table = 'SI_SV_CAT' . getNextKey();

		foreach($fields as $eachField)
		{
			$columnName = 'F' . getNextKey();
			$arrColumns[] = $columnName . ' varchar(512)';
			$arrColumnsName[] = array('columnexcel' => $eachField, 'columntable' => $columnName);
		}

		if( $aConnection->Execute( 'CREATE TABLE ' . $table . '(id int NOT NULL AUTO_INCREMENT, '. implode(',', $arrColumns) .', PRIMARY KEY (id) )' ) === false ) return false;
		
		return array('columns' => $arrColumnsName, 'table' => $table);

	}
	function createTableeBavel($fields, $aConnection ) {
		$arrColumns = array();
		
		$table = 'SI_SV_CAT' . getNextKey();

		foreach($fields as $eachField)
		{
			$columnName = 'F' . getNextKey();
			$arrColumns[] = $columnName . ' varchar(512)';
			$arrColumnsName[] = array('columnexcel' => $eachField['name'], 'columntable' => $columnName, 'idfieldform' => $eachField['idfieldform'], "tagname" => $eachField["tagname"]);
		}
		if( $aConnection->Execute( 'CREATE TABLE ' . $table . '(id int NOT NULL AUTO_INCREMENT, '. implode(',', $arrColumns) .', PRIMARY KEY (id) )' ) === false ) return false;
		
		return array('columns' => $arrColumnsName, 'table' => $table);

	}
	$idnewCatalog=createCatalog($members, $CatName, $data, $aConnection, $theUser, $type, $theRepository);
	echo json_encode(array('success' => true,  'idNewCatalog' => $idnewCatalog, "id" => $idnewCatalog));
	exit();
}

if($action == 'getmembers') {
	$members = array();
	$arrfieldname = array();
	$valuemember = array();
	$idCatalog = $_REQUEST['idcatalog'];
	//@JAPR 2018-07-27: Modificado el request para permitir filtrar por atributo y así optimizar la carga por demanda (#2PB40B)
	$strMemberFilter = '';
	$idMember = (int) @$_REQUEST['idattrib'];
	if ( $idMember > 0 ) {
		$strMemberFilter = " AND MemberID = {$idMember}";
	}
	$blnNoValues = (bool) @$_REQUEST['novalues'];
	//@JAPR 2019-02-27: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
	$strSearchText = (string) @$_REQUEST['searchtext'];
	//@JAPR
	$aDataSource = $aDataSource = BITAMDataSource::NewInstanceWithID($theRepository,$idCatalog);
	if ($aDataSource) {
		//@JAPR 2018-07-27: Modificado el request para permitir filtrar por atributo y así optimizar la carga por demanda (#2PB40B)
		$rs = $aConnection->Execute('SELECT MemberName, MemberID, FieldName, eBavelFieldID FROM SI_SV_DataSourceMember where DataSourceID = '. $idCatalog.$strMemberFilter);
		//@JAPR
		while ($rs && !$rs->EOF){
			$value = array();
			$members[] = $rs->fields['membername'];
			$memberid = $rs->fields['memberid'];
			$arrfieldname[] = $memberid;
			
			//@JAPR 2018-07-27: Modificado el request para permitir filtrar por atributo y así optimizar la carga por demanda (#2PB40B)
			//En caso de que no se necesiten los valores, avanza el recordset pero no realiza los queries para obtener los datos de los atributos
			if ( $blnNoValues ) {
				$rs->MoveNext();
				continue;
			}
			//@JAPR
			
			$fieldname = $rs->fields['fieldname'];
			$eBavelFieldID = $rs->fields['ebavelfieldid'];
			if ($aDataSource->eBavelAppID > 0 && $aDataSource->eBavelFormID > 0) {
				$sqlid = 'select id from SI_FORMS_FIELDSFORMS where id_fieldform = ' . $eBavelFieldID ;
				$rs0 = $aConectionBMD->Execute($sqlid);
				if ($rs0 && !$rs0->EOF){
					$fieldFormID = $rs0->fields["id"];
					//@JAPR 2018-07-26: Corregido un bug, al obtener los datos de los atributos no estaba aplicando un DISTINCT y estaba pidiendo los campos de sistema
					//los cuales ni siquiera tienen uso en este contexto, por lo tanto en formas con demasiados registros era lentísimo este proceso (#2PB40B)
					$bError = false;
					//@JAPR 2019-02-27: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
					$strWhere = '';
					if ( $strSearchText ) {
						//Los filtros de eBavel automáticamente agregarán la porción de WHERE, así que no es necesario incluirla en esta parte
						$strWhere = /*' WHERE '.*/$fieldFormID.' LIKE '.$aConectionBMD->quote('%'.$strSearchText.'%');
					}
					$objeBavel = $aDataSource->getCatalogValues([$memberid], -1, $strWhere, true, true, false, -1, $bError, null, false);
					//@JAPR
					if ($objeBavel) {
						$aRS = $objeBavel['rs'];
						$fieldKey = $objeBavel['id'];
					}else {
						$aRS = array();
						$fieldKey = -1;
					}

					$value = array();
					while($aRS && !$aRS->EOF) {

						if($aRS->fields[$fieldFormID]!="" && !in_array($aRS->fields[$fieldFormID], $value)){
							$value[] = $aRS->fields[$fieldFormID];
						}
						$aRS->MoveNext();
					}
					if(count($value)>0){
						$valuemember[$memberid] = $value;
					}
				}
				
			}if ($aDataSource->Type == dstyTable){
				//@JAPR 2016-06-20: Corregido un bug, se estaba enviando como array de atributos sólo el ID del deseado, siendo que esa función espera un objeto definición de atributo (#012P4U)
				$objDataSourceMember = BITAMDataSourceMember::NewInstanceWithID($theRepository, $memberid);
				if (!is_null($objDataSourceMember)) {
					$arrayAttribIDs = array();
					$anObjectMember = array();
					$anObjectMember["DataSourceID"]= $objDataSourceMember->DataSourceID;
					$anObjectMember["sourceMemberID"]= $objDataSourceMember->MemberID;
					$anObjectMember["MemberName"]= $objDataSourceMember->MemberName;
					$anObjectMember["FieldName"]= $objDataSourceMember->FieldName;
					$anObjectMember["eBavelFieldID"]= $objDataSourceMember->eBavelFieldID;
					$anObjectMember["eBavelFieldDataID"]= $objDataSourceMember->eBavelFieldDataID;
					$arrayAttribIDs[] = $anObjectMember;
					
					//@JAPR 2019-02-27: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
					$strWhere = '';
					if ( $strSearchText ) {
						//@JAPRWarning: En este contexto NO se tiene creada la conexión, así que por el momento se usará la conexión al DWH para el Quote, bajo el entendido que esto está mal
						//y lo correcto sería abrir la conexión en este punto y reutilizarla en la propia función getExternalCatalogValues
						$strWhere = ' WHERE '.$objDataSourceMember->FieldName.' LIKE '.$aConnection->quote('%'.$strSearchText.'%');
					}
					
					$rs2 = $aDataSource->getExternalCatalogValues($arrayAttribIDs, -1, $strWhere, true, false, false, null, false, false);
					//@JAPR
					while ($rs2 && !$rs2->EOF){
						if ($rs2->fields[$fieldname]!=""){
							$value[] = $rs2->fields[$fieldname];
						}
						$rs2->MoveNext();
					}
				}
				//@JAPR
				if(count($value)>0){
					$valuemember[$memberid] = $value;
				}
			}else {
				//@JAPR 2019-02-27: Modificada la ventana de filtros para utilizar una combo dinámica mediante auto-complete (#L6ADN8)
				$strWhere = '';
				if ( $strSearchText ) {
					$strWhere = ' WHERE '.$fieldname.' LIKE '.$aConnection->quote('%'.$strSearchText.'%');
				}
				
				$sql = 'SELECT DISTINCT ' . $fieldname . ' FROM ' . $aDataSource->TableName . $strWhere;
				$rs2 = $aConnection->Execute($sql);
				//@JAPR
				while ($rs2 && !$rs2->EOF){
					if ($rs2->fields[$fieldname]!=""){
						$value[] = $rs2->fields[$fieldname];
					}
					$rs2->MoveNext();
				}
				if(count($value)>0){
					$valuemember[$memberid] = $value;
				}
			}
			$rs->MoveNext();
		}
	}else {
		echo json_encode(array('success' => false));
		exit();
	}
	echo json_encode(array('success' => true,  'arrMembers' => $members, 'fieldName' => $arrfieldname, 'arrValueMember'=>$valuemember));
	exit();
}

if($action == 'getvalueofMembers') {
	$members = array();
	$fieldname = array();
	$idCatalog = $_REQUEST['idcatalog'];
	$fieldName = $_REQUEST['fieldName'];
	$filter = $_REQUEST['filter'];
	$aDataSource = $aDataSource = BITAMDataSource::NewInstanceWithID($theRepository,$idCatalog);
	$rs = $aConnection->Execute('SELECT DISTINCT ' . $fieldName . ' FROM ' . $aDataSource->TableName);
	while ($rs && !$rs->EOF){
		if ($rs->fields[$fieldName]!=""){
			$members[] = $rs->fields[$fieldName];
		}
		$rs->MoveNext();
	}

	echo json_encode(array('success' => true,  'arrMembers' => $members));
	exit();
}

if($action == 'createMember') {
	function getNextKey()
	{
		$nextKey = uniqid(rand(), false);
		
		return strtoupper($nextKey);
	}
	$Members = $_REQUEST['Members'];
	$catalogId = $_REQUEST['catalogId'];
	$TableName = $_REQUEST['TableName'];
	$type =  @$_REQUEST['type'];
	$maxMemberID = 1;
	$sql= 'SELECT max( MemberID ) as MAX FROM SI_SV_DataSourceMember';
	$rs = $aConnection->Execute($sql);
	if( $rs && $rs->RecordCount() != 0 ) $maxMemberID = $rs->fields['MAX'] + 1;
	$Order = 1;
	$sql2 = 'SELECT max( MemberOrder ) as MAX FROM SI_SV_DataSourceMember WHERE DataSourceID = '. $catalogId ;
	$rs = $aConnection->Execute($sql2);
	if( $rs && $rs->RecordCount() != 0 ) $Order = $rs->fields['MAX'] + 1;
	$fieldName = 'F' . getNextKey();
	if ($type == "3") {
		$id_fieldform = @$_REQUEST['idFieldForm'];
		$tagname = @$_REQUEST['tagname'];
		//@JAPRWarning: Estandarizar todo este código, debería de utilizar las Clases creadas para esto, no generar INSERTs directos a la metadata
		switch ($tagname) {
			/*Si verificamos que la carga es invocada por un FTP,Google Drive ó Dropbox se cambia el path y el tipo de datasource*/
			case "widget_geolocation":
				$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID ,eBavelFieldDataID) VALUES ({$catalogId}, {$maxMemberID},". $aConnection->quote($Members . "_LATI") .", ". $aConnection->quote($fieldName . "_LATI") .", ". ($Order) .", -2, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$id_fieldform} ,1)" );
				$maxMemberID += 1;
				$Order += 1;
				$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID ,eBavelFieldDataID) VALUES ({$catalogId}, {$maxMemberID},". $aConnection->quote($Members . "_LONG") .", ". $aConnection->quote($fieldName . "_LONG") .", ". ($Order) .", -2, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$id_fieldform} ,2)" );
				$sql4 = "ALTER TABLE " . $TableName . " ADD " . $fieldName . " VARCHAR (512) NULL";
				$aConnection->Execute($sql4);
				break;
			case "widget_photo":
				$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID , IsImage) VALUES ({$catalogId}, {$maxMemberID},". $aConnection->quote($Members) .", ". $aConnection->quote($fieldName) .", ". ($Order) .", 8, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$id_fieldform}, 1 )" );
				$sql4 = "ALTER TABLE " . $TableName . " ADD " . $fieldName . " VARCHAR (512) NULL";
				$aConnection->Execute($sql4);
				break;
			case "widget_signature":
				$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID , IsImage ) VALUES ({$catalogId}, {$maxMemberID},". $aConnection->quote($Members) .", ". $aConnection->quote($fieldName) .", ". ($Order) .", 10, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$id_fieldform}, 1 )" );
				$sql4 = "ALTER TABLE " . $TableName . " ADD " . $fieldName . " VARCHAR (512) NULL";
				$aConnection->Execute($sql4);
				break;
			default:
				$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, eBavelFieldID ) VALUES ({$catalogId}, {$maxMemberID},". $aConnection->quote($Members) .", ". $aConnection->quote($fieldName) .", ". ($Order) .", 6, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s")).", {$id_fieldform} )" );
				$sql4 = "ALTER TABLE " . $TableName . " ADD " . $fieldName . " VARCHAR (512) NULL";
				$aConnection->Execute($sql4);
				break;
		}
	}else {
		//@JAPR 2016-06-21: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
		$intIsImage = (int) @$_REQUEST['IsImage'];
		$sql3 = "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate, IsImage) VALUES ({$catalogId}, {$maxMemberID},". $aConnection->quote($Members) .", ". $aConnection->quote($fieldName) .", ". $Order .", 6, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .", {$intIsImage})" ;
		//@JAPR
		$aConnection->Execute($sql3);
		$sql4 = "ALTER TABLE " . $TableName . " ADD " . $fieldName . " VARCHAR (512) NULL";
		$aConnection->Execute($sql4);
	}
	
	//@JAPR 2015-07-26: Agregada la eliminación de los catálogos internos de las preguntas cuando se elimina la pregunta o el DataSource
	//Invoca a la actualización de definiciones en cualquier punto donde se requiera, en este caso sincronizar los nombres y agrega/elimina atributos
	require_once("..\\dataSource.inc.php");
	$objDataSource = BITAMDataSource::NewInstanceWithID($theRepository, $catalogId);
	if (!is_null($objDataSource)) {
		$objDataSource->updateAllCatalogDefinitions(false, true, true);
	}
	//@JAPR
	
	echo json_encode(array('success' => true,  'Members' => $Members));
	exit();
}

if($action == 'newCatalogValue') {
	$fields = explode(",",$_REQUEST['fields']);
	$values = explode(",",$_REQUEST['values']);
	$TableName = $_REQUEST['TableName'];
	for ($i=0;$i<count($values);$i++){
		$values[$i] = $aConnection->quote($values[$i]);
	}
	$id = 1;
	$sql= 'SELECT max( id ) as MAX FROM '.$TableName;
	$rs = $aConnection->Execute($sql);
	if( $rs && $rs->RecordCount() != 0 ) $id = $rs->fields['MAX'] + 1;
	
	$sql3 = "INSERT INTO " . $TableName . " (id, " . implode(",",$fields) . ") VALUES ({$id}, " . implode(",",$values) . " )" ;
	$aConnection->Execute($sql3);

	echo json_encode(array('success' => true,  'TableName' => $TableName));
	exit();
}

if( $action == 'processExcel' )
{
	function createCatalogHTTPS($fields,$catalogName, $aConnection, $theUser, $type, $accountID, $parameters, $Database, $Table, $theRepository) {
		/** Creamos la tabla donde se cargaran los valores del excel*/
		if ($type == 7 || 8){
			if( ( $infoTable = createTableHTTP($fields, $aConnection ) ) === false )
				return false;
		}		
		/** Con la tabla creada, ahora cargamos los datos del excel a la tabla de la base de datos*/
		/** La tabla ha sido creada y los registros cargados, agregamos la informacion a la metadata */
		/** Obtenemos el id maximo de la tabla de catalogos */
		$maxId = 1;
		$rs = $aConnection->Execute('SELECT max( DataSourceID ) as MAX FROM SI_SV_DataSource');
		if( $rs && $rs->RecordCount() != 0 ) $maxId = $rs->fields['MAX'] + 1;

		$idnewCatalog = $maxId;
		/** Insertamos la informacion del catalogo */
		//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
		$currentDate = date("Y-m-d H:i:s");
		$strAdditionalFields = ', LastModUserID, LastModDate';
		$strAdditionalValues = ', '.(int) @$_SESSION["PABITAM_UserID"].
			', '.$theRepository->DataADOConnection->DBTimeStamp($currentDate);
		//@JAPR
		if ($type == 7 || 8){
			//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
			if( $aConnection->Execute( "INSERT INTO SI_SV_DataSource (DataSourceID, DataSourceName, TableName, VersionNum, CreationUserID, CreationDate, Type, id_ServiceConnection, parameters, servicedbname, servicetablename {$strAdditionalFields}) VALUES ({$maxId}, ". $aConnection->quote($catalogName) .",". $aConnection->quote($infoTable['table']) .",0,{$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .", {$type}, {$accountID}, " .  $aConnection->quote($parameters) . ", " .  $aConnection->quote($Database) . ", " .  $aConnection->quote($Table) . " {$strAdditionalValues})" ) === false )
			{
				/** Ahora insertamos los miembros del catalogo */

			}

			/** Obtenemos el id maximo de la tabla de catalogos */
			$maxMemberID = 1;
			$rs = $aConnection->Execute('SELECT max( MemberID ) as MAX FROM SI_SV_DataSourceMember');
			if( $rs && $rs->RecordCount() != 0 ) $maxMemberID = $rs->fields['MAX'] + 1;

			foreach ($infoTable['columns'] as $key => $value) {
				$aConnection->Execute( "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate) VALUES ({$maxId}, {$maxMemberID},". $aConnection->quote($value["columnexcel"]) .", ". $aConnection->quote($value["columntable"]) .", ". ($key + 1) .", 6, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .")" );
				$maxMemberID += 1;
			}
		}
		$aDataSource = BITAMDataSource::NewInstanceWithID($theRepository,$maxId);
		if (!is_null($aDataSource)) {
			$aDataSource->updateAllCatalogDefinitions(false, true, true);
		}

		return $idnewCatalog;
	}
	function getNextKeyHTTP()
	{
		$nextKey = uniqid(rand(), false);
		
		return strtoupper($nextKey);
	}
	function createTableHTTP($fields, $aConnection ) {
		$arrColumns = array();
		
		$table = 'SI_SV_CAT' . getNextKeyHTTP();

		foreach($fields as $eachField)
		{
			$columnName = 'F' . getNextKeyHTTP();
			$arrColumns[] = $columnName . ' varchar(512)';
			$arrColumnsName[] = array('columnexcel' => $eachField['name'], 'columntable' => $columnName);
		}

		if( $aConnection->Execute( 'CREATE TABLE ' . $table . '(id int NOT NULL AUTO_INCREMENT, '. implode(',', $arrColumns) .', PRIMARY KEY (id) )' ) === false ) return false;
		
		return array('columns' => $arrColumnsName, 'table' => $table);

	}

	/*Esta funcion Solo se utilizaba cuando se cargaba de excel por dicho motivo el default es 2*/
	$dataSourceType = 2;
	$arrFields = @$_REQUEST['fields'];

	$arrNewFields = @$_REQUEST['arrNewFields'];
	
	$catalogName = @$_REQUEST['catalogName'];
	
	$type = @$_REQUEST['type'];
	
	$pathSource = @$_REQUEST['Path'];

	$fileName = @$_REQUEST['filename'];

	$accountID = @$_REQUEST['Account'];

	$idGoogleDrive = @$_REQUEST['idGoogleDrive'];
	$attributesValues = @$_REQUEST['attributesValues'];
	$error = "";
	
	if ($type){
		switch ($type) {
			/*Si verificamos que la carga es invocada por un FTP,Google Drive ó Dropbox se cambia el path y el tipo de datasource*/
			case "5":
				$upload_dir = 'uploads/Datasources/FTP/';
				$dataSourceType = 6;
				$FTPname = @$_REQUEST['FTPName'];
				$name = @$_REQUEST['FTPServer'];
				$user = @$_REQUEST['FTPUser'];
				$pass = @$_REQUEST['FTPPass'];
				if (getMDVersion()>=esveServicesConnectionName) {
					$where = 'where servicename = '. $aConnection->quote($FTPname);
				}else {
					$where = 'where name = ' .  $aConnection->quote($name);
				}
				$sql = 'SELECT id FROM si_sv_servicesconnection ' . $where;
				$rs = $aConnection->Execute($sql);
				if ($rs && !$rs->EOF){
					$accountID = $rs->fields['id'];
					$sqlUpdate='UPDATE si_sv_servicesconnection set `name` = '.  $aConnection->quote($name) . ', ' . 
											(getMDVersion()>=esveServicesConnectionName?'`servicename` = '.  $aConnection->quote($FTPname) . ', ':'').
											'`pass` = '.  $aConnection->quote($pass) . ', ' .
											'`user` = '.  $aConnection->quote($user) . 
											' where id = '. $accountID;
					$aConnection->Execute($sqlUpdate);
				}else {
					$sql= 'INSERT INTO `si_sv_servicesconnection`
							( `name`, `access_token`, `user`, `pass`, `ftp`, `type`, `pass_parameter`, `user_parameter`, `type_request`'.(getMDVersion()>=esveServicesConnectionName? ', `servicename`': ''). ')
							VALUES
							(' . $aConnection->quote($name) . ',"",' . $aConnection->quote($user) . ' ,' . $aConnection->quote($pass) . ' , ' . $aConnection->quote($name) . ',5 ,NULL ,NULL , NULL ' . (getMDVersion()>=esveServicesConnectionName? ', '. $aConnection->quote($FTPname) : '') . ');';
					$rs = $aConnection->Execute($sql);
					$accountID = $aConnection->_insertid();
				}
				break;
			case "3":$upload_dir = "uploads/Datasources/googledrive/";
				$dataSourceType = 5;
				break;
				
			case "2":
				$upload_dir = "uploads/Datasources/dropbox/";
				$dataSourceType = 4;
				break;
			case "6":
				$dataSourceType = 7;
				$httpName = @$_REQUEST['httpName'];
				$name = @$_REQUEST['httpServer'];
				$user = @$_REQUEST['httpUsername'];
				$pass = @$_REQUEST['httpPassword'];
				$parameters = @$_REQUEST['httpParameters'];
				$Database ='';
				$Table ='';
				$TypeOfDatabase = null;
				if (getMDVersion()>=esveServicesConnectionName) {
					$where = 'where servicename = '. $aConnection->quote($httpName);
				}else {
					$where = 'where name = ' .  $aConnection->quote($name);
				}
				$sql = 'SELECT id FROM si_sv_servicesconnection ' . $where;
				$rs = $aConnection->Execute($sql);
				if ($rs && !$rs->EOF){
					$accountID = $rs->fields['id'];
					$sqlUpdate='UPDATE si_sv_servicesconnection set `name` = '.  $aConnection->quote($name) . ', ' . 
											(getMDVersion()>=esveServicesConnectionName?'`servicename` = '.  $aConnection->quote($httpName) . ', ':'').
											'`pass` = '.  $aConnection->quote($pass) . ', ' .
											'`user` = '.  $aConnection->quote($user) . 
											' where id = '. $accountID;
					$aConnection->Execute($sqlUpdate);
				}else {
					$sql= 'INSERT INTO `si_sv_servicesconnection`
							( `name`, `access_token`, `user`, `pass`, `ftp`, `type`, `pass_parameter`, `user_parameter`, `type_request`'.(getMDVersion()>=esveServicesConnectionName? ', `servicename`': ''). ')
							VALUES
							(' . $aConnection->quote($name) . ',"",' . $aConnection->quote($user) . ' ,' . $aConnection->quote($pass) . ' , ' . $aConnection->quote($name) . ',6 ,NULL ,NULL , NULL ' . (getMDVersion()>=esveServicesConnectionName? ', '. $aConnection->quote($httpName) : '') . ');';
					$rs = $aConnection->Execute($sql);
					$accountID = $aConnection->_insertid();
				}
				break;
		    case "7":
				$dataSourceType = 8;
				$BDName = @$_REQUEST['BDName'];
				$name = @$_REQUEST['BDServer'];
				$user = @$_REQUEST['BDUsername'];
				$pass = BITAMEncryptPassword(@$_REQUEST['BDPassword']);
				$Database = @$_REQUEST['Database'];
				$Table = @$_REQUEST['Table'];
				$TypeOfDatabase = @$_REQUEST['TypeOfDatabase'];
				$parameters = '';
				if (getMDVersion()>=esveServicesConnectionName) {
					$where = 'where servicename = '. $aConnection->quote($BDName);
				}else {
					$where = 'where name = ' .  $aConnection->quote($name);
				}
				$sql = 'SELECT id FROM si_sv_servicesconnection ' . $where;
				$rs = $aConnection->Execute($sql);
				if ($rs && !$rs->EOF){
					$accountID = $rs->fields['id'];
					if (getMDVersion()>=esvTypeOfDatabase) {
						$sqlUpdate='UPDATE si_sv_servicesconnection set `name` = '.  $aConnection->quote($name) . ', ' . 
																	'`servicedbtype` = '.  $TypeOfDatabase . ', ' .
																	'`servicename` = '.  $aConnection->quote($BDName) . ', ' .
																	'`pass` = '.  $aConnection->quote($pass) . ', ' .
																	'`user` = '.  $aConnection->quote($user) . 
																	' where id = '. $accountID;
					}else {
						$sqlUpdate='UPDATE si_sv_servicesconnection set `name` = '.  $aConnection->quote($name) . ', ' . 
																	'`pass` = '.  $aConnection->quote($pass) . ', ' .
																	'`user` = '.  $aConnection->quote($user) . 
																	' where id = '. $accountID;
					}
					
					$aConnection->Execute($sqlUpdate);
				}else {
					if (getMDVersion()>=esvTypeOfDatabase) {
						$sql= 'INSERT INTO `si_sv_servicesconnection`
						( `name`, `access_token`, `user`, `pass`, `ftp`, `type`, `pass_parameter`, `user_parameter`, `type_request`, `servicedbtype`, `servicename`)
						VALUES
						(' . $aConnection->quote($name) . ',"",' . $aConnection->quote($user) . ' ,' . $aConnection->quote($pass) . ' , ' . $aConnection->quote($name) . ',7 ,NULL ,NULL , NULL, ' . $TypeOfDatabase . ', ' . $aConnection->quote($BDName) . ');';
					}else {
						$sql= 'INSERT INTO `si_sv_servicesconnection`
						( `name`, `access_token`, `user`, `pass`, `ftp`, `type`, `pass_parameter`, `user_parameter`, `type_request`)
						VALUES
						(' . $aConnection->quote($name) . ',"",' . $aConnection->quote($user) . ' ,' . $aConnection->quote($pass) . ' , ' . $aConnection->quote($name) . ',7 ,NULL ,NULL , NULL);';
					}
					$rs = $aConnection->Execute($sql);
					$accountID = $aConnection->_insertid();
				}
				break;
		}
	}
	if ($type=="6" || $type=="7" ){
		if( !is_numeric($id) ) {
			$catalogId = createCatalogHTTPS($arrFields, $catalogName, $aConnection, $theUser, $dataSourceType, $accountID, $parameters, $Database, $Table, $theRepository);
			if ($type=="7") {
				echo json_encode(array('success' => true, 'id' =>$catalogId));
				exit();
			}
			if($catalogId && $type=="6"){
				$sql = 'SELECT tableName FROM SI_SV_datasource WHERE datasourceid = ' . $catalogId;
				$rs = $aConnection->Execute($sql);
				//@JAPR 2015-09-03: Corregido un bug, no estaba actualizando la versión de los catálogos que dependen de este DataSource modificado (#K8ZJZJ)
				if ($rs && !$rs->EOF) {
					$tablename = @$rs->fields["tableName"];
					$sql = 'SELECT membername, fieldname FROM SI_SV_datasourceMember WHERE datasourceid = ' . $catalogId;
					$arrdefinitionfields = array();
					$arrnamefields = array();
					$rs2 = $aConnection->Execute($sql);
					while ($rs2 && !$rs2->EOF) {
						$arrdefinitionfields[] = @$rs2->fields["fieldname"];
						$arrnamefields[] = @$rs2->fields["membername"];
						$rs2->MoveNext();
					}
				}
				foreach ($attributesValues as $key => $value) {
					$arrinsertfields = array();
					$arrinsertvalues = array();
					foreach ($value as $key2 => $value2) {
						$arrinsertfields[] = $key2;
						$arrinsertvalues[] = $aConnection->quote($value2);
					}
					//En el siguiente ciclo elimino los que no se usan en caso de que usuaio no haya seleccionado todos los fields
					$countArrInsertFields = count($arrinsertfields);
					for($i = 0; $i <$countArrInsertFields; $i++) {
						if (!in_array($arrinsertfields[$i], $arrnamefields)){
							unset($arrinsertfields[$i]);
							unset($arrinsertvalues[$i]);
						}
					}
					$fields = implode(",", $arrinsertfields);
					//Una vez deburado obtengo los nombres verdaderos del campo en la tabla para poder armar los insert
					$fields = str_replace($arrnamefields, $arrdefinitionfields, $fields);
					$sql = 'INSERT INTO ' . $tablename . '(' . $fields . ') VALUES (' . implode(",", $arrinsertvalues) . ')';
					$aConnection->Execute($sql);
				}
			echo json_encode(array('success' => true, 'id' =>$catalogId));
			exit();
			}
		}else {
			//Primero actualizamos todos los fields
			foreach ($arrFields as $key => $value) {
				if ($value && $value["columnexcel"] != "" && $value["columntable"] != "") {
					$sql = "UPDATE SI_SV_datasourcemember SET MemberName = " . $aConnection->quote($value["columnexcel"]) . " WHERE fieldname = " . $aConnection->quote($value["columntable"]) ;
					$aConnection->Execute($sql);
				}
			}
			//Actualizamos el Datasource
			if ($type=="7") {
				$aConnection->Execute("UPDATE SI_SV_DataSource SET RecurActive = 0, id_ServiceConnection = " . $accountID . ", servicedbname = " . $aConnection->quote($Database) . ", servicetablename = " . $aConnection->quote($Table) . ", type = " . $dataSourceType . " WHERE DataSourceID = " . $id);
			}else {
				$aConnection->Execute("UPDATE SI_SV_DataSource SET RecurActive = 1, id_ServiceConnection = " . $accountID . ", parameters = " . $aConnection->quote($parameters) . ", type = " . $dataSourceType . " WHERE DataSourceID = " . $id);	
			}
			
			$maxMemberID = 1;
			$sql= 'SELECT max( MemberID ) as MAX FROM SI_SV_DataSourceMember';
			$rs = $aConnection->Execute($sql);
			if( $rs && $rs->RecordCount() != 0 ) $maxMemberID = $rs->fields['MAX'] + 1;
			$sql = 'SELECT tableName FROM SI_SV_datasource WHERE datasourceid = ' . $id;
			$rs = $aConnection->Execute($sql);
			if( $rs && $rs->RecordCount() != 0 ) $TableName = $rs->fields['tableName'];
			$Order = 1;
			$sql2 = 'SELECT max( MemberOrder ) as MAX FROM SI_SV_DataSourceMember WHERE DataSourceID = '. $id ;
			$rs = $aConnection->Execute($sql2);
			if( $rs && $rs->RecordCount() != 0 ) $Order = $rs->fields['MAX'] + 1;
			if ($arrNewFields && count($arrNewFields)>0) {
				foreach ($arrNewFields as $keynewfield => $valuenewfield) {
					$fieldName = 'F' . getNextKeyHTTP();
					$sql3 = "INSERT INTO SI_SV_DataSourceMember (DataSourceID, MemberID, MemberName, fieldName, MemberOrder, MemberType, CreationUserID, CreationDate) VALUES ({$id}, {$maxMemberID},". $aConnection->quote($valuenewfield["name"]) .", ". $aConnection->quote($fieldName) .", ". $Order .", 6, {$theUser->UserID},". $aConnection->quote( date("Y-m-d H:i:s") ) .")" ;
					$aConnection->Execute($sql3);
					$sql4 = "ALTER TABLE " . $TableName . " ADD " . $fieldName . " VARCHAR (512) NULL";
					$aConnection->Execute($sql4);
					$maxMemberID++;
					$Order++;
				}
			}
			if ($type=="7") {
				require_once("..\\dataSource.inc.php");
				$objDataSource = BITAMDataSource::NewInstanceWithID($theRepository, $id);
				if (!is_null($objDataSource)) {
					$objDataSource->updateAllCatalogDefinitions(false, true, true);
				}
				echo json_encode(array('success' => true, 'id' =>$id));
				exit();
			}
			//Una vez creados limio la tabla 
			$sqlDelete = 'DELETE FROM ' . $TableName;
			$aConnection->Execute($sqlDelete);
			//y vuelvo a insertar todos los valores del catalago
			$sql = 'SELECT membername, fieldname FROM SI_SV_datasourceMember WHERE datasourceid = ' . $id;
			$arrdefinitionfields = array();
			$arrnamefields = array();
			$rs2 = $aConnection->Execute($sql);
			while ($rs2 && !$rs2->EOF) {
				$arrdefinitionfields[] = @$rs2->fields["fieldname"];
				$arrnamefields[] = @$rs2->fields["membername"];
				$rs2->MoveNext();
			}

			foreach ($attributesValues as $key => $value) {
				$arrinsertfields = array();
				$arrinsertvalues = array();
				foreach ($value as $key2 => $value2) {
					$arrinsertfields[] = $key2;
					$arrinsertvalues[] = $aConnection->quote($value2);
				}
				//En el siguiente ciclo elimino los que no se usan en caso de que usuaio no haya seleccionado todos los fields
				$countArrInsertFields = count($arrinsertfields);
				for($i = 0; $i < $countArrInsertFields; $i++) {
					if (!in_array($arrinsertfields[$i], $arrnamefields)){
						unset($arrinsertfields[$i]);
						unset($arrinsertvalues[$i]);
					}
				}
				$fields = implode(",", $arrinsertfields);
				//Una vez deburado obtengo los nombres verdaderos del campo en la tabla para poder armar los insert
				$fields = str_replace($arrnamefields, $arrdefinitionfields, $fields);
				$sql = 'INSERT INTO ' . $TableName . '(' . $fields . ') VALUES (' . implode(",", $arrinsertvalues) . ')';
				$aConnection->Execute($sql);
			}
			require_once("..\\dataSource.inc.php");
			$objDataSource = BITAMDataSource::NewInstanceWithID($theRepository, $id);
			if (!is_null($objDataSource)) {
				$objDataSource->updateAllCatalogDefinitions(false, true, true);
			}
			echo json_encode(array('success' => true, 'id' =>$id));
			exit();
			
		}

	}
	$path = $upload_dir . $_REQUEST['file'];
	//$sheetselected = str_replace("$","",utf8_encode($selectedSheet));
	if( is_file( $path ) )
	{
		//@JAPR 2016-06-23: Intento de estandarizar el código a la manera de programar de eForms
		//Agregado el parámetro $aRepository
		$anExcelUpload = new ExcelUpload( $path, $id, $theRepository );
		//@JAPR
		if( !is_numeric($id) )
		{
			$res = $anExcelUpload->createCatalog($arrFields, $selectedSheet, $catalogName, $aConnection, $theUser , $theRepository, $dataSourceType, $fileName, $pathSource, $accountID, $idGoogleDrive);				
		}	
		else if ( is_numeric($id) )	
		{

			/** Si hay nuevos campos que agregar al catalogo, los agregamos antes de hacer el insert */
			if( count( $arrNewFields ) )
			{
				$resNewAttrs = $anExcelUpload->addAttrs( $arrNewFields, $aConnection, $theUser );
				$arrFields = array_merge($arrFields, $resNewAttrs);
			}
			$excelColumns = $anExcelUpload->getColumns( $selectedSheet, $aConnection );
			$bIsValid = false;
			$aIsValid = true;
			//2015-11-05 @JRPP: Si una de los registros marcados como columna nueva no se encuentra en el excel 
			//quiere decir que se mapeo otro diferente excel y no es posible obtener ese campo
			foreach ($arrFields as $key => $value) {
				if (!in_array($value['columnexcel'], $excelColumns)) {
					$aIsValid = false;
					$error = "The Excel file has changed. The columns must be re-linked in order to complete the importation";
				}
			}
			/** validamos que almenos una columna este mapeada */
			foreach ($arrFields as $key => $value) {
				if( $value['columnexcel'] != '' )
				{
					$bIsValid = true;
				}
			}
			if( !$bIsValid  || !$aIsValid)
			{
				echo json_encode(array('success' => false, 'msg' => translate('Error found during data validation') . ". " . translate($error) ) );
				exit();			
			}
			$res = $anExcelUpload->uploadCatalog($arrFields, $selectedSheet, $catalogName, $aConnection, $theUser , $theRepository, $dataSourceType, $fileName, $pathSource, $accountID, $idGoogleDrive);
		}
	}
	
	if( $res )
	{
		echo json_encode(array('success' => true, 'id' => $anExcelUpload->id));
	} else
	{
		echo json_encode(array('success' => false, 'msg' => utf8_encode($anExcelUpload->getErrorMsg())));   
	}
	exit();
}

if( $action == 'saveScheduler' )
{
	$aDataSourceID = getParamValue('id', 'both', '(int)', true);

	$anInstance = BITAMDataSource::NewInstanceWithID($theRepository, $aDataSourceID);

	if( is_object($anInstance) )
	{
		$dataUpdate = getParamValue('data', 'both', '(array)', true);

		$anInstance->updateFromArray( $dataUpdate );

		$anInstance->save();
	}

	exit();
}

if( $action == 'getInstance' )
{
	$aDataSourceID = getParamValue('id', 'both', '(int)', true);

	$anArray = [];

	$anInstance = BITAMDataSource::NewInstanceWithID($theRepository, $aDataSourceID);

	if( is_object( $anInstance ) )
	{
		$anArray = $anInstance->getJSonDefinition(false);
	}

	echo json_encode($anArray);

	exit();
}

if($action == 'loadDashboar') {
	$addDashboar = array();
	$idSurvey = $_REQUEST['idSurvey'];
	$aReporter = BITAMArtusReporter::NewInstance($theRepository,'',$idSurvey);
	echo json_encode(array('success' => true,  'arrDashboards' => $aReporter->onlydashboard, 'cladimension' => $aReporter->cla_dimension ));
	exit();
}

if($action == 'loadArs') {
	$addDashboar = array();
	$idSurvey = $_REQUEST['idSurvey'];
	$aReporter = BITAMArtusReporter::NewInstance($theRepository,'',$idSurvey);
	echo json_encode(array('success' => true,  'arrARS' => $aReporter->onlyreports, 'cladimension' => $aReporter->cla_dimension ));
	exit();
}
?>