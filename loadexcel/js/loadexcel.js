require.config({
	//2013-10-22@ARMZ: Agregado el parametro bust para evitar cache de los archivos, esto por mala configuracion del server
	urlArgs: "bust=" +  (new Date()).getTime(),
	waitSeconds: 200,
	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: ["underscore"/*, "jquery"*/],
			exports: "Backbone"
		}
	},
	paths: {
		/*jquery: 'libs/jquery/jquery.min',*/
		underscore: 'libs/underscore/underscore-min',
		lodash: 'libs/lodash/lodash.min',
		bootstrap: 'libs/bootstrap/bootstrap.min',
		backbone: 'libs/backbone/backbone-min',		
		// Require.js plugins
		text: 'libs/require/text',		
		json: 'libs/require/json',
		css: 'libs/require/css',
		// Just a short cut so we can put our html outside the js dir
		// When you have HTML/CSS designers this aids in keeping them out of the js directory
		templates: '../templates',
		templates2: '../../templates',
		javascript: '../../js'
	}	
});

var bitamApp =  bitamApp || { session: function() {}, 
	/** Obtiene el path de la aplicacion */
	getURLServer: function() {
		return 'http://localhost/models';
	},
	getJSONRequest: function() {

		return $.ajax.apply({}, arguments);

	},
	isMobile: {
		isApp: function() { return false}
	} };
