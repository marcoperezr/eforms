﻿<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);

	header("Content-Type:text/javascript; charset=UTF-8");

?>define([
	'lodash',
	'backbone',
	'views/modal/basicModal',
	'text!templates2/agendas/newscheduler.php',
	'javascript/views/agendas/schedulerlogic',
	'javascript/eBavel/date',
	'libs/selectize.js/dist/js/standalone/selectize',
	'bootstrap',
	'css!libs/selectize.js/dist/css/selectize.bootstrap3.css',
	'css!../../../../../../css/materialModal.css'
], function(_, Backbone, BasicModal, Template2, ScheduleFunction) {

	var serializeJSON = function( form ) {
		var resData = {};
		
		var json = {};
		
		jQuery.map(form.serializeArray(), function(n, i) {
			if(json[n['name']]) {
				if($.isArray(json[n['name']])) {
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = new Array(json[n['name']]);
					json[n['name']].push(n['value']);
				}
			}
			else {
				if($('#' + n['name']).attr('multiple') == 'multiple') {
					json[n['name']] = new Array();
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = n['value'];
				}
			}
		});

		resData = json;

		return resData;	
	}	
	
	return function( id ) {
		
		var wizard, aModal, urlPath = 'loadexcel/', $scheduler = $(_.template( Template2 )({})), $wizard;

		$scheduler.find('section.scheduler').wrap('<form class="scheduler"/>');

		$scheduler = $scheduler.find('form.scheduler, style');

		$wizard = $scheduler.filter('form.scheduler');
		
		aModal = new BasicModal({
			modalTitle: '<?= translate('Change frequency') ?>',
			className: 'material',
			labels: {
				close: '<?= translate('Close') ?>'
			}
		});

		aModal.addButton({
				'label': '<?= translate("Save") ?>',
				'callback': function() {
					var anEvent = $.Event(), data = serializeJSON( $scheduler.filter('form.scheduler') );
					
					ScheduleFunction.validScheduler( $scheduler.filter('form.scheduler'), anEvent, 0 );

					if( anEvent.isPropagationStopped() ) return;

					if( data['RecurPatternType'] ) {
						aModal.remove();

						$.post(urlPath + 'action.php', {action: 'saveScheduler', id: id, data: data}).done(function(r){

						});
					} else {
						$scheduler.find('[name="RecurPatternType"] + div.selectize-control .selectize-input').addClass('errorinput');
					}
				}
			})

		require(['selectize', 'json!../action.php?action=getInstance&id=' + id + '&r=' + (new Date()).getTime()], function(s, data) {

			//$scheduler.find('section.scheduler').prepend('<div class="form-group"><label><?= translate('Recurrence pattern') ?></label><div><label class="radio-inline"><input type="radio" id="RecurActive" name="RecurActive" value="1"> <?= translate('Active') ?></label><label class="radio-inline"><input type="radio" id="RecurActive" name="RecurActive" value="0"> <?= translate('Inactive') ?></label></div></div>');
			
			$scheduler.find('div.range_of_recurrence').append('<div style="clear: both;margin-top: 18px;"><label class="pull-left"><?= translate('Minutes') ?>&nbsp;</label><input name="Interval" class="form-control" style="width: 70px;font-size: 13px;" type="number"></input></div>')

			$scheduler.find('<?php if (getMDVersion() >= esvDataSourceAgentHours) echo ".form-group.generate_next_day"; else echo ".form-group.capture_start_time, .form-group.generate_next_day"; ?>').remove();

			aModal.setContent( $scheduler );

			$wizard.find('[name="RecurActive"][value="'+ data.RecurActive +'"]').prop('checked', true);

			delete data.RecurActive;

			ScheduleFunction.schedulerLogic( $wizard, data );

			aModal.$el.find('select').selectize();
		});

		
		
		$('body').append(aModal.width(880).render());

	};

});
