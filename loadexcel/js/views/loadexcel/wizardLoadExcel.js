﻿define([
	'lodash',
	'backbone',
	'text!templates/loadexcel/wizard.php',
	'views/modal/wizardModal',
	'libs/misc/SimpleAjaxUploader.min',
	'libs/selectize.js/dist/js/standalone/selectize',
	'bootstrap'
], function(_, Backbone, Template, WizardModal){
	
	var wizard, aModal, $select, urlPath = 'loadexcel/';

	/** Recibe el ID de la definicion del modelo de Artus a cargar, si esta vacio crea uno nuevo */
	return function( id ) {
		var file, selectedSheet, dfd = $.Deferred(), bFinish = false, $tpl = $(_.template(Template)( {} ));
		
		/** Testing*/
		//id = 1;
		
		aModal = new WizardModal({
			modalTitle: (id ? $tpl.find('#tLoadExcel').html() : $tpl.find('#tLoadExcel').html()),
			className: 'material'
		});

		$('body').append(aModal.render().width(780).$el);		

		aModal.$el.find('.modal-body').html($.t('Loading') + '...');
		
		/** Agregamos el contenido para la ventana modal tipo wizard */
		aModal.setContent( $tpl );

		aModal.$el.find('a[href="#previous"]').html( $tpl.find('#tPrevious').html() );
		aModal.$el.find('a[href="#next"]').html( $tpl.find('#tNext').html() );
		aModal.$el.find('a[href="#finish"]').html( $tpl.find('#tFinish').html() );
		
		/** Al terminar mandamos la informacion de la hoja de excel cargada y los campos seleccionados */
		aModal.on('onFinished', function(res) {
			var $fields = aModal.$el.find('div.fields div.field[data-selected="true"]'), arrFields = [], arrNewFields = [], catalogName = aModal.$el.find('input[name="catalogName"]').val();

			if( id == undefined && $fields.length == 0 || $.trim(catalogName) == '' ) {
				res.result = false;

				if( $.trim(catalogName) == '' ) {
					aModal.showError( $tpl.find('#tInvalidname').html() );
				} else {
					aModal.showError( $tpl.find('#tInvalidfields').html() );
				}
				
				return;
			} else if ( $fields.length == 0 ) {
				aModal.$el.find('div.fields select').each(function(i, el) {
					arrFields.push({ columntable: el.getAttribute('data-value'), columnexcel: el.value });
				});

				$fields = aModal.$el.find('div.fieldsnew div.field[data-selected="true"]');
			}

			$fields.each(function(i, el) {
				
				if( id == undefined )
					arrFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
				else
					arrNewFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
			});

			if( $fields.length ) {
				arrFields && arrFields.sort(function(a, b) {
					return a.i - b.i;
				});

				arrNewFields && arrNewFields.sort(function(a, b) {
					return a.i - b.i;
				});
			}

			aModal.$el.find('section.finishstep .mappingselect').hide();

			aModal.$el.find('section.finishstep .creatingtables').show();
			
			!bFinish && $.post(urlPath + 'action.php?action=processExcel', {fields: arrFields, 'arrNewFields': arrNewFields, file: file, id: id, sheet: selectedSheet, catalogName: catalogName}).done(function( r ) {
				
				aModal.hideError();

				bFinish = false;
				if( r.success ) {
					dfd.resolve( r.id, catalogName );
					aModal.remove();
				}
				else {
					aModal.$el.find('section.finishstep .creatingtables').hide();
					aModal.$el.find('section.finishstep .mappingselect').show();
					aModal.showError( r.msg );
					//dfd.reject();
				}
			});

			bFinish = true;
			
		});
		
		aModal.on('onStepChanging', function(o, a, b) {

			aModal.hideError();

			if( b != 0 ) {
				$('div input[name="uploadfile"]').parent().hide();
			} else {
				$('div input[name="uploadfile"]').parent().show();
			}
			if( a == 1 && aModal.$el.find('#sheets').val() == '' ) {
				o.result = false;
				return;
			}
		});
		
		$select = aModal.$el.find('select#sheets');

		uploader = new ss.SimpleUpload({
			  button: aModal.$el.find('.btn.excelfileupload')[0],
			  url: urlPath + 'action.php?action=uploadCatalog',
			  name: 'uploadfile',
			  allowedExtensions: ['xls', 'xlsx'],
			  onChange: function( filename, extension, uploadBtn ) {
			  	aModal.$el.find('.filename').html( filename );
			  },
			  onProgress: function(pct){
			  	aModal.$el.find('.pctBox').html( pct + '%' );
			  },
			  onsubmit: function() {
				aModal.$el.find('.error-msg').html('');
			  },
			  onComplete: function (filename, response, uploadBtn ) {
					response = JSON.parse(response);
					if( response.success ) {
						file = response.file;
						aModal.$el.find('form').steps('next');
						aModal.$el.find('ul[role="menu"][aria-label="Pagination"]').show();
						$select[0].selectize.clearOptions();
						$select.append('<option value="">---</option>')
						for(var i = 0; i < response.sheets.length; i++) {
							$select[0].selectize.addOption({value: response.sheets[i], text: response.sheets[i]});
						}
						
						$select[0].selectize.refreshItems();

					} else {
						aModal.$el.find('.error-msg').html('Error: ' + response.msg);  
					}				  
			  },
			  onExtError: function( filename, extension ) {
				  aModal.$el.find('.error-msg').html('Invalid file');
			  },
			  onError: function( filename, errorType, status, statusText, response, uploadBtn ) {
				  aModal.$el.find('.error-msg').html('Error: ' + statusText);
			  }
		});
		aModal.$el.find('ul[role="menu"][aria-label="Pagination"]').hide();
		
		require(['selectize'], function(a) {
			$select.selectize().on('change', function() {
				/*@MAPR 2016-08-02: Existían problemas cuando el nombre de una hoja de cálculo contenía el signo "#".  
				Se creo la variable para poder manipular los valores y cambiar el simbolo por otro conjunto de valores que representan su valor.
				Issue #79OEVC
				*/
				var str = $select.val();
				if ( $select.val().includes("#")) {
					str = str.replace("#", "%23");
				}
				$select.val() != '' && $.get(urlPath +'action.php?action=loadFields&sheet=' + str + '&file=' + file + '&id=' + id).done(function( r ) {
					if( r.success ) {

						selectedSheet = $select.val();

						aModal.$el.find('input[name="catalogName"]').val( selectedSheet.replace(/\$$/, '') );

						aModal.$el.find('div.fieldsnew').hide();

						var $contenfFields = aModal.$el.find('div.fields').empty(), i = 0, options = [], shtml = '', sfhtml = '', $fieldsselectd;
						
						if( r.definition.length ) {

							aModal.$el.find('.finishstep div.catalogname').hide();

							shtml += '<form class="form-horizontal">';
							
							options.push('<option value="">---</option>');

							for( i = 0; i < r.excelColumns.length; i++ ) {
								options.push('<option value="'+ r.excelColumns[i] +'">'+ r.excelColumns[i] +'</option>');
							}
							
							for( i = 0; i < r.definition.length; i++ ) {
								shtml += '<div class="form-group"><label title="'+ r.definition[i].text +'" for="'+ r.definition[i].text +'" class="col-sm-4 control-label">'+ r.definition[i].text +'</label><div class="col-sm-8"><select placeholder="'+ $tpl.find('#tSelectAttribute').html() +'" data-value="'+ r.definition[i].value +'" class="input-sm" name="'+ r.definition[i].text +'">'+ options.join('') +'</select></div></div>'
							}

							if( r.arrExcelColumns && r.arrExcelColumns.length ) {

								for( i = 0; i < r.arrExcelColumns.length; i++ ) {
									
									sfhtml += '<div class="field" data-fieldname="'+ r.arrExcelColumns[i] +'"><div>'+ r.arrExcelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>';

								}

								aModal.$el.find('div.fieldsnew').show();

								aModal.$el.find('div.listnewfields').append( sfhtml );

								$fieldsselectd = aModal.$el.find('div.listnewfields div.field');
							}
							
							$contenfFields.append( shtml + '<div style="clear: both;"></div>' );

							$contenfFields.find('select').each(function(i, el) {
								var $el = $(el);

								$el.find('option[value="'+ $el.attr('name') +'"]').attr('selected', true);
							})
							
							$contenfFields.find('select').selectize({onDropdownClose: function() {
								/*if( this.getValue() == '' )
									this.setValue( Object.keys(this.options)[0] );*/
							}});
							
						} else {
							for( var i = 0; i < r.excelColumns.length; i++ ) {
								$contenfFields.append('<div class="field" data-fieldname="'+ r.excelColumns[i] +'"><div>'+ r.excelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>')
							}

							/***/
							$fieldsselectd = $contenfFields.find('div.field');
						}

						$fieldsselectd && $fieldsselectd.on('click', function() {
							if( this.getAttribute('data-selected') == undefined ) {
								this.setAttribute('data-selected', 'true');
								this.setAttribute('data-i', i++);
								this.classList.add('selected');
							} else {
								this.classList.remove('selected');
								this.removeAttribute('data-selected');
								this.removeAttribute('data-i');
							}
						});						
					} else if( r.msg != '' ) {
						alert( r.msg );
					}
				});
			});
		});

		return dfd.promise();
		
	};

});
