﻿<?php
	$chdirCurrent = getcwd ();
	chdir('../../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);
	/** Leemos la informacion del cliente de Google */
	$sJson = file_get_contents('../../../../datadestinations/libs/Google/client_secret_gDrive.json');
	$gInfoClient = json_decode( $sJson );
?>define([
	'lodash',
	'backbone',
	'text!templates/loadDatasource/wizardDatasource.php',
	'views/modal/wizardModal',
	'text!templates2/agendas/newscheduler.php',
	'javascript/views/agendas/schedulerlogic',
	'javascript/eBavel/date',
	'libs/misc/SimpleAjaxUploader.min',
	'libs/selectize.js/dist/js/standalone/selectize',
	'bootstrap',
	'libs/metisMenu/src/metisMenu'
], function(_, Backbone, Template, WizardModal, Template2, ScheduleFunction){
	
	var wizard, aModal, $selectSheet, urlPath = 'loadexcel/', $selectTypeDatabase;
	attributesValues = [];
	first = true;
	var serializeJSON = function( form ) {
		var resData = {};
		
		var json = {};
		
		jQuery.map(form.serializeArray(), function(n, i) {
			if(json[n['name']]) {
				if($.isArray(json[n['name']])) {
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = new Array(json[n['name']]);
					json[n['name']].push(n['value']);
				}
			}
			else {
				if($('#' + n['name']).attr('multiple') == 'multiple') {
					json[n['name']] = new Array();
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = n['value'];
				}
			}
		});

		resData = json;

		return resData;	
	}
	/** Recibe el ID de la definicion del modelo de Artus a cargar, si esta vacio crea uno nuevo */
	return function( id, dataSourceType) {
		var file, selectedSheet, dfd = $.Deferred(), bFinish = false, $tpl = $(_.template(Template)( {} )), $scheduler = $(_.template( Template2 )({}));
		/** Testing*/
		//id = 1;

		aModal = new WizardModal({
			modalTitle: (id ? $tpl.find('#tLoadExcel').html() : $tpl.find('#tLoadExcel').html()),
			className: 'material'
		});

		$('body').append(aModal.render().$el);

		aModal.width(1000);	

		aModal.$el.find('.modal-body').html($.t('Loading') + '...');
		$scheduler.find('section.scheduler');
		$scheduler = $scheduler.find('section.scheduler');
		$scheduler.find('div.range_of_recurrence').append('<div style="clear: both;margin-top: 18px;"><label class="pull-left"><?= translate('Minutes') ?>&nbsp;</label><input required name="Interval" min="0" class="form-control" style="width: 70px;font-size: 13px;" type="number"></input></div>');
		$scheduler.find('<?php if (getMDVersion() >= esvDataSourceAgentHours) echo ".form-group.generate_next_day"; else echo ".form-group.capture_start_time, .form-group.generate_next_day"; ?>').remove();
		$scheduler.appendTo($tpl.find('#vacio'));
		var $stepscheduler = $tpl.find('.scheduler').detach();
		$tpl.find('#vacio').replaceWith($stepscheduler);
		/** Agregamos el contenido para la ventana modal tipo wizard */
		wizard = $tpl;
		aModal.setContent(wizard);

		<? //@JAPR 2018-06-07: Corregido un bug, se estaba ejecutando esta creación de evento cada vez que ingresaba a la ventana de archivos (#VLMNXW)
			//Movido este código para que sólo se ejecute una vez la creación del evento, así no se agregará cada vez que se muestren carpetas/archivos ?>
		wizard.find('.metisFolder').on('beforeopen', function(e, anObject) {
			wizard.find('[name="path"]').val($(anObject.el).text()+"/");
			wizard.find('[name="file"]').val("");
			wizard.find('[name="path"]').data("idFolderDrive",anObject.el.parentElement.id);
			if($(anObject.el).parent().attr('aria-expanded')==undefined || $(anObject.el).parent().attr('aria-expanded')=="false"){
				$(anObject.el).parent().attr("aria-expanded","true");
				$(anObject.el).parent().parent().attr("aria-expanded","true");
				createTreeFolder($(anObject.el).parent().attr('id'));
			}else{
				$(anObject.el).parent().attr("aria-expanded","false");
				$(anObject.el).parent().parent().attr("aria-expanded","false");
			}
		});
		<? //@JAPR ?>
		
		//2015-09-17@JRPP: Se agrega un formulario en la seccion final de scheduler
		aModal.$el.find('section.scheduler').append("<form class = 'schedulerform'></form>");
		aModal.$el.find('form.schedulerform').append( aModal.$el.find('section.scheduler > div').detach() );

		//aModal.$el.find('.steps.clearfix').hide();
		aModal.$el.find('a[href="#previous"]').html( $tpl.find('#tPrevious').html() );
		aModal.$el.find('a[href="#next"]').html( $tpl.find('#tNext').html() );
		aModal.$el.find('a[href="#finish"]').html( $tpl.find('#tFinish').html() );

		/** Al terminar mandamos la informacion de la hoja de excel cargada y los campos seleccionados */
		aModal.on('onFinished', function(anEvent, nStep) {
			var $fields = aModal.$el.find('div.fields div.field[data-selected="true"]'), arrFields = [], arrNewFields = [], catalogName = aModal.$el.find('input[name="catalogName"]').val();

			if( nStep == 1 ) {
				/** Se agrego la funcion para validar los datos del scheduler */
				ScheduleFunction.validScheduler( aModal.$el.find('form.schedulerform'), anEvent, 0 );

				if( anEvent.isPropagationStopped() ) return;
			}
			var typedownload = dataSourceType;
			var account = "";
			var filename = "";
			var downloadPath = "";
			var idGoogleDrive = "";
			var FTPServer = aModal.$el.find('[name="ftpServer"]').val();
			var FTPUser = aModal.$el.find('[name="ftpUsername"]').val();
			var FTPPass = aModal.$el.find('[name="ftpPassword"]').val();
			var httpServer = wizard.find('[name="httpServer"]').val();
			var httpUsername = wizard.find('[name="httpUsername"]').val();
			var	httpPassword = wizard.find('[name="httpPassword"]').val();
			var httpParameters = wizard.find('[name="httpParameters"]').val();
			var BDServer = wizard.find('[name="BDServer"]').val();
			var BDUsername = wizard.find('[name="BDUsername"]').val();
			var BDPassword = wizard.find('[name="BDPassword"]').val();
			var Database = wizard.find('[name="Database"]').val();
			/*@JRPP 2016-01-20 Se agregan todas los divs donde se guarda el valor del nombre de la conexion*/
			var httpName = wizard.find('#httpName').val();
			var ftpName = wizard.find('#ftpName').val();
			var BDName = wizard.find('#BDName').val();
			var Table = '';
			var TypeOfDatabase = $selectTypeDatabase.val();
			var dataScheduler = serializeJSON(aModal.$el.find('section.scheduler form').filter('form.schedulerform'));
			filename = aModal.$el.find('[name="file"]').val();
			downloadPath = aModal.$el.find('[name="path"]').val();

			if (typedownload=="3") {
				account = aModal.$el.find('[name="gaccount"]').val();
				idGoogleDrive = aModal.$el.find('[name="path"]').data("idFolderDrive");
			}else if (typedownload=="5") {
				account = aModal.$el.find('[name="ftpaccount"]').val();
			}else if (typedownload=="2") {
				account = aModal.$el.find('[name="dpxaccount"]').val();
			}if(typedownload=="6"){
				catalogName=dataSourcename;
			}

			if( id == undefined && $fields.length == 0 || $.trim(catalogName) == '' ) {
				anEvent.stopPropagation();
				aModal.showError( );
				return;
			} else if ( $fields.length == 0 ) {
				aModal.$el.find('div.fields select').each(function(i, el) {
					arrFields.push({ columntable: el.getAttribute('data-value'), columnexcel: el.value });
				});

				$fields = aModal.$el.find('div.fieldsnew div.field[data-selected="true"]');
			}

			$fields.each(function(i, el) {
				
				if( id == undefined )
					arrFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
				else
					arrNewFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
			});

			if( $fields.length ) {
				arrFields && arrFields.sort(function(a, b) {
					return a.i - b.i;
				});

				arrNewFields && arrNewFields.sort(function(a, b) {
					return a.i - b.i;
				});
			}

			aModal.$el.find('section.finishstep .mappingselect').hide();

			aModal.$el.find('section.finishstep .creatingtables').show();
			if( dataScheduler['RecurPatternType'] ) {
				!bFinish && $.post(urlPath + 'action.php?action=processExcel', {'fields': arrFields, 'arrNewFields': arrNewFields, 'file': file, 'id': id, 'sheet': selectedSheet, 'catalogName': catalogName, 'type': typedownload, 'filename' : filename , 'Path' : downloadPath, 'Account' : account, 'idGoogleDrive': idGoogleDrive, 'FTPServer': FTPServer, 'FTPUser': FTPUser, 'FTPPass': FTPPass, 'attributesValues': attributesValues, 'httpServer': httpServer, 'httpUsername': httpUsername, 'httpPassword': httpPassword, 'httpParameters': httpParameters, 'BDServer': BDServer, 'BDUsername': BDUsername,'BDPassword': BDPassword, 'Database' : Database, 'Table' : Table, 'TypeOfDatabase': TypeOfDatabase, 'httpName': httpName, 'FTPName': ftpName, 'BDName': BDName}).done(function( r ) {
					aModal.hideError();
					bFinish = false;
					if( r.success ) {
						$.post(urlPath + 'action.php', {action: 'saveScheduler', id: r.id, data: dataScheduler}).done(function( r ) {
						});
						dfd.resolve( r.id, catalogName );
						aModal.remove();
					}
					else {
						aModal.$el.find('section.finishstep .creatingtables').hide();
						aModal.$el.find('section.finishstep .mappingselect').show();
						aModal.showError( r.msg );
						//dfd.reject();
					}
				});			
			} else {
				!bFinish && $.post(urlPath + 'action.php?action=processExcel', {'fields': arrFields, 'arrNewFields': arrNewFields, 'file': file, 'id': id, 'sheet': selectedSheet, 'catalogName': catalogName, 'type': typedownload, 'filename' : filename , 'Path' : downloadPath, 'Account' : account, 'idGoogleDrive': idGoogleDrive, 'FTPServer': FTPServer, 'FTPUser': FTPUser, 'FTPPass': FTPPass, 'attributesValues': attributesValues, 'BDServer': BDServer, 'BDUsername': BDUsername,'BDPassword': BDPassword, 'Database' : Database, 'Table' : Table, 'TypeOfDatabase': TypeOfDatabase, 'httpName': httpName, 'FTPName': ftpName, 'BDName': BDName}).done(function( r ) {
					aModal.hideError();

					bFinish = false;
					if( r.success ) {
						dfd.resolve( r.id, catalogName );
						aModal.remove();
					}
					else {
						aModal.$el.find('section.finishstep .creatingtables').hide();
						aModal.$el.find('section.finishstep .mappingselect').show();
						aModal.showError( r.msg );
					}
				}); 
			}
			bFinish = true;
			
		});
		var gdrive = function() {
			(function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				po.src = 'https://apis.google.com/js/platform.js?onload=renderButtonsGoogle';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			})();	
		}
		var ftpstep = function() {
			loadConnections( 'ftpaccount' );
		}
		var dropbox=function() {
			loadConnections( 'dpxaccount' );
		}
		var httpstep=function() {
			loadConnections( 'httpaccount' );
		}
		var BDstep=function() {
			loadConnections( 'BDaccount' );
		}
		var loadConnections = function( sEl ) {
			var type = dataSourceType;
			$.get('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'getInfoSrvConnection', type: type}).done(function( r ) {
				wizard.find('[name="'+ sEl +'"]').selectize({
					//options:(type==3? r :[ {text: '<?= translate( '(New connection)' ) ?>', value: 0} ].concat( r )) ,
					options:[ {text: '<?= translate( '(New connection)' ) ?>', value: 0} ].concat( r ),
					render: {
						option: function(item, escape) {
							var label = item.text
								var caption = item.value == 0 ? ( type == 2 ? '<?= translate('Creates a new connection to') ?> DropBox' : 
								( type == 3 ? '<?= translate('Creates a new connection to') ?> Google Drive' :
									( type == 5 ? '<?= translate('Creates a new connection to') ?> FTP' : 
										( type == 6 ? '<?= translate('Creates a new connection to') ?> HTTP':
											( type == 7 ? '<?= translate('Creates a new connection to') ?> DB':null)
										)
								 	) 
								)
								) : null;

							return '<div>' +
								'<span class="label">' + escape(label) + '</span>' +
								(caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
								'</div>';
						}
					},
					onItemAdd: function( v ) {
						if( v == 0 ) {
							
							if( type == 3 ) {
								var windowValidate = window.open("main.php?BITAM_SECTION=eFormsDataDestinationsCollection&action=validateGoogle", 'authGoogle', "width=800,height=436");
									
								window.fnSendCode=function( data ){
									windowValidate.close();

									var aSelectize = aModal.$el.find('[name="gaccount"]')[0].selectize;

									if( aSelectize ) {
										aSelectize.addOption([{ text: data.email, value: data.id }]);
										aSelectize.refreshOptions();
										aSelectize.addItem( data.id );
										aSelectize.refreshItems();
									}

								};
								
								<?//@JAPR 2018-05-22: Corregido un bug con las conexiones a Google Drive y Dropbox, no terminaba el proceso por bloqueo de cross-origin (#VAHJXY) ?>
								window.fnGoogleDriveCallback = function(event) {
									<?// IMPORTANT: Check the origin of the data! 
									/*Si el origen del mensaje no es el mismo servicio, no se debe continuar. Esto previene el error por Same-origin security policy
									que impedía que se agregara y refrescara el DataSource recien agregado debido al bloqueo de la ejecución de la función fnSendCode
									ejecutada por el código de callback de la llamada de GoogleDrive/DropBox, con este método siempre y cuando quien origina dicho
									código y quien lo reciben sean el mismo server (KPIOnline en este caso) se puede ejecutar sin problema
									*/
									?>
									if (~event.origin.indexOf('<?='http' . ( $_SERVER['SERVER_NAME'] == 'localhost' ? '' : 's' ) . '://' . $_SERVER['SERVER_NAME']?>')) {
										<?// The data has been sent from your site
										// The data sent with postMessage is stored in event.data ?>
										if ( event && event.data && event.data.email && event.data.id ) {
											fnSendCode(event.data);
										}
									} else {
										<?// The data hasn't been sent from your site!
										// Be careful! Do not use it. ?>
										console.log("Be careful! cross-origin data received");
									}
									
									window.removeEventListener('message', window.fnGoogleDriveCallback);
								}
								
								window.addEventListener('message', window.fnGoogleDriveCallback);
								<?//@JAPR ?>
							}
							if(type==2){
								var windowValidate=window.open("main.php?BITAM_SECTION=eFormsDataDestinationsCollection&action=validateDropBox", 'authDropBox', "width=800,height=436");
								window.fnSendCode=function(codeValidate){
									$.post( 'main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'validateDropBox', 'code': codeValidate} ).done(function(r) {
										var aSelectize = wizard.find('[name="dpxaccount"]')[0].selectize;

										if( aSelectize ) {
											var eMail = r;

											aSelectize.addOption([{text: eMail.email, value: eMail.id}]);
											aSelectize.refreshOptions();
											aSelectize.addItem(eMail.id);
											aSelectize.refreshItems();
										}
										windowValidate.close();
									});
								};
								
								<?//@JAPR 2018-05-29: Corregido un bug con las conexiones a Google Drive y Dropbox, no terminaba el proceso por bloqueo de cross-origin (#VAHJXY) ?>
								window.fnDropboxCallback = function(event) {
									<?// IMPORTANT: Check the origin of the data! 
									/*Si el origen del mensaje no es el mismo servicio, no se debe continuar. Esto previene el error por Same-origin security policy
									que impedía que se agregara y refrescara el DataSource recien agregado debido al bloqueo de la ejecución de la función fnSendCode
									ejecutada por el código de callback de la llamada de GoogleDrive/DropBox, con este método siempre y cuando quien origina dicho
									código y quien lo reciben sean el mismo server (KPIOnline en este caso) se puede ejecutar sin problema
									*/
									?>
									if (~event.origin.indexOf('<?='http' . ( $_SERVER['SERVER_NAME'] == 'localhost' ? '' : 's' ) . '://' . $_SERVER['SERVER_NAME']?>')) {
										<?// The data has been sent from your site
										// The data sent with postMessage is stored in event.data ?>
										if ( event && event.data ) {
											fnSendCode(event.data);
										}
									} else {
										<?// The data hasn't been sent from your site!
										// Be careful! Do not use it. ?>
										console.log("Be careful! cross-origin data received");
									}
									
									window.removeEventListener('message', window.fnDropboxCallback);
								}
								
								window.addEventListener('message', window.fnDropboxCallback);
								<?//@JAPR ?>
							}
							if( type == 5 ) {
								wizard.find('div.ftpnew').show();
								wizard.find('#ftpName').val("");
								wizard.find('#ftpServer').val("");
								wizard.find('#ftpUsername').val("");
								wizard.find('#ftpPassword').val("");
								if (wizard.find('#ftpName').val()==""){
									wizard.find('#ftpName').focus();
								}else {
									wizard.find('[name="ftpServer"]').focus();
								}
								//@MAPR 2016-07-26: Línea que se encarga de borrar el texto en Selectize
								//wizard.find('[name="'+ sEl +'"]')[0].selectize.clear( true );

							}
							if( type == 6 ) {
								wizard.find('div.httpnew').show();
								wizard.find('#httpName').val("");
								wizard.find('#httpServer').val("");
								wizard.find('#httpUsername').val("");
								wizard.find('#httpPassword').val("");
								if (wizard.find('#httpName').val()==""){
									wizard.find('#httpName').focus();
								}else {
									wizard.find('[name="httpServer"]').focus();
								}
								wizard.find('[name="'+ sEl +'"]')[0].selectize.clear( true );
							}
							if( type == 7 ) {
								wizard.find('div.BDnew').show();
								wizard.find('#BDName').val("");
								wizard.find('#BDServer').val("");
								wizard.find('#BDUsername').val("");
								wizard.find('#BDPassword').val("");
								if (wizard.find('#BDName').val()==""){
									wizard.find('#BDName').focus();
								}else {
									wizard.find('[name="BDServer"]').focus();
								}
								wizard.find('[name="'+ sEl +'"]')[0].selectize.clear( true );
							}
							//@MAPR 2016-07-26: Línea que se encarga de borrar el texto en Selectize
							//	wizard.find('[name="'+ sEl +'"]')[0].selectize.clear( true );
						}
					}
				});

				if( type == 5 ) {
					wizard.find('[name="'+ sEl +'"]').selectize().on('change', function() {
						if(wizard.find('[name="'+ sEl +'"]').val()!= "") {
							$.get(urlPath +'action.php?action=getConexion&idConnection=' + wizard.find('[name="'+ sEl +'"]').val()).done(function( response ) {
								if (response.success) {
									wizard.find('div.ftpnew').show();
									wizard.find('#ftpServer').val(response.ftp);
									wizard.find('#ftpUsername').val(response.user);
									wizard.find('#ftpPassword').val(response.pass);
									if(response.servicename){
										wizard.find('#ftpName').val(response.servicename);
									}
								}
							});
						}
					});
				}
				if( type == 6 ) {
					wizard.find('[name="'+ sEl +'"]').selectize().on('change', function() {
						if(wizard.find('[name="'+ sEl +'"]').val()!= "") {
							$.get(urlPath +'action.php?action=getHttpConexion&idConnection=' + wizard.find('[name="'+ sEl +'"]').val()).done(function( response ) {
								if (response.success) {
									wizard.find('div.httpnew').show();
									wizard.find('#httpServer').val(response.http);
									wizard.find('#httpUsername').val(response.user);
									wizard.find('#httpPassword').val(response.pass);
									wizard.find('#httpParameters').val(Parameters);
									if(response.servicename){
										wizard.find('#httpName').val(response.servicename);
									}
								}
							});
						}
					});
				}
				if( type == 7 ) {
					wizard.find('[name="'+ sEl +'"]').selectize().on('change', function() {
						if(wizard.find('[name="'+ sEl +'"]').val()!= "") {
							$.get(urlPath +'action.php?action=getTableConexion&idConnection=' + wizard.find('[name="'+ sEl +'"]').val()).done(function( response ) {
								if (response.success) {
									wizard.find('div.BDnew').show();
									wizard.find('#BDServer').val(response.http);
									wizard.find('#BDUsername').val(response.user);
									wizard.find('#BDPassword').val(response.pass);
									wizard.find('#Database').val(SourceDatabase);
									if (response.servicedbtype){
										$selectTypeDatabase[0].selectize.setValue(response.servicedbtype);
									}
									if(response.servicename){
										wizard.find('#BDName').val(response.servicename);
									}
								}
							});
						}
					});
				}
			});

		}
		var getFile = function(){
			aModal.$el.find('.error-msg').hide();
			var data = { 
					'gaccount': wizard.find('[name="gaccount"]').val(),
					'action': 'downloadFile',
					'type': dataSourceType,
					'ftpServer': wizard.find('[name="ftpServer"]').val(),
					'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
					'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
					'file': wizard.find('[name="file"]').val() ,
					'path':  (dataSourceType=="3"?wizard.find('[name="path"]').data("idFolderDrive") : wizard.find('[name="path"]').val()),
					'ftpaccount': wizard.find('[name="ftpaccount"]').val(),
					'dpxaccount': wizard.find('[name="dpxaccount"]').val()
				};
			$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
				if( r.success ) {
					$.get(urlPath +'action.php?action=uploadCatalog&filename=' + r.file + '&type=' + dataSourceType).done(function(response) {
						if( response.success ) {
							file = response.file;
							aModal.$el.find('ul[role="menu"][aria-label="Pagination"]').show();
							$selectSheet[0].selectize.clearOptions();
							$selectSheet.append('<option value="">---</option>')
							for(var i = 0; i < response.sheets.length; i++) {
								$selectSheet[0].selectize.addOption({value: response.sheets[i], text: response.sheets[i]});
							}
							wizard.find(".loadingDiv").hide();
							$selectSheet[0].selectize.refreshItems();
						} else {
							aModal.$el.find('.error-msg').html('Error: ' + response.msg);
							aModal.$el.find('.error-msg').show();
						}		
					});	
				}
			});
		}

		var createTreeFolder = function(idFolder) {
			if(idFolder==null){
				wizard.find('[name="path"]').val("/");
				var data = {
					'gaccount': wizard.find('[name="gaccount"]').val(),
					'action': 'getTreeFolder',
					'type': dataSourceType,
					'ftpServer': wizard.find('[name="ftpServer"]').val(),
					'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
					'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
					'ftpaccount': wizard.find('[name="ftpaccount"]').val(),
					'showFiles': true,
					'dpxaccount':wizard.find('[name="dpxaccount"]').val()
				};
				$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
					if (typeof r == "object") {
						wizard.find(".loadingDiv").hide();
						wizard.find('.nextclass').attr("href",'#next');
						wizard.find('a[href="#next"]').removeClass("nextclass");
						aModal.showError( r.msg );
					}else {
						wizard.find(".loadingDiv").hide();
						first = false;
						wizard.find(".selectedTree").find("ul").empty();
						wizard.find(".selectedTree").find("ul").append(r);
						wizard.find('.fileclass').on( "click", function(e) {
							//wizard.find('[name="path"]').data("idFolderDrive",this.id);
							var liElement=$(this);
							wizard.find('[name="path"]').val(liElement.find('a')[0].text);
							wizard.find('[name="file"]').val(liElement.find('a')[0].text);
							//2015-09-24@JRPP: Si se hace clic en algun documento se considera que ya cambio la información
							// motivo por el cual se esconde el boton y se cambia la variable
						});
						wizard.find(".selectedTree").find("ul").metisMenu({ toggle: false });
						wizard.steps('next');

					}

				});
				<? //@JAPR 2018-06-07: Corregido un bug, se estaba ejecutando esta creación de evento cada vez que ingresaba a la ventana de archivos (#VLMNXW)
					//Movido este código para que sólo se ejecute una vez la creación del evento, así no se agregará cada vez que se muestren carpetas/archivos ?>
				<? //@JAPR ?>
			}else{
				var data = {
					'gaccount': wizard.find('[name="gaccount"]').val(),
					'action': 'getTreeFolder',
					'idFolder':idFolder,
					'type': dataSourceType,
					'ftpServer': wizard.find('[name="ftpServer"]').val(),
					'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
					'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
					'ftpaccount': wizard.find('[name="ftpaccount"]').val(),
					'showFiles': true,
					'dpxaccount':wizard.find('[name="dpxaccount"]').val()
				};
				$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
					wizard.find(".selectedTree [name='"+idFolder+"'] ul").remove();
					wizard.find(".selectedTree [name='"+idFolder+"']").append("<ul class='collapse in' aria-expanded='false'>"+r+"</ul>");
					//wizard.find(".selectedTree").find(".metisFolder").metisMenu({ toggle: false });
					wizard.find(".selectedTree [name='"+idFolder+"'] li").on( "click", function(e) {
						e.stopPropagation();
						if ($(this).find('.fa-file').length>0){
							if(this.className!="fileclass")
								wizard.find('[name="path"]').data("idFolderDrive",this.id);
							
							wizard.find('[name="path"]').val("");
							var liElement=$(this);
							wizard.find('[name="file"]').val(liElement.find('a')[0].text);
							var index = 0;
							while (liElement[0].tagName!="DIV") {
								var pathFolder=wizard.find('[name="path"]').val();
								if (index==0) {
									wizard.find('[name="path"]').val(liElement.find('a')[0].text+pathFolder);
								}else {
									wizard.find('[name="path"]').val(liElement.find('a')[0].text+"/"+pathFolder);
								}
								liElement=liElement.parent().parent();
								index++;
							}
							wizard.find('ul[role="menu"][aria-label="Pagination"]').show();
							return;
						}
						if(this.id.length > 0){
							wizard.find('[name="path"]').data("idFolderDrive",this.id);
							wizard.find('[name="path"]').val("");
							var liElement=$(this);
							while (liElement[0].tagName!="DIV") {
								var pathFolder=wizard.find('[name="path"]').val();
								wizard.find('[name="path"]').val(liElement.find('a')[0].text+"/"+pathFolder);
								liElement=liElement.parent().parent();
							}
							if($(this).attr('class')==undefined || $(this).attr('class').indexOf('active')< 0){
								if(this.id.length>0){
									$(this).addClass('active');
									if(dataSourceType==5){
										$(this).attr('name',wizard.find('[name="path"]').val())
										createTreeFolder(wizard.find('[name="path"]').val());
									}else{
										createTreeFolder(this.id);
									}
								}
							}else{
								$(this).removeClass('active');
								$(this).find('ul').removeClass('in');
							}
						}
					});
					
				});
			}
		}
		var searchHttpAttributes = function() {
			wizard.find('[name="path"]').val("/");
			var data = {
				'action': 'downloadHttpAttributes',
				'type': dataSourceType,
				'httpServer': wizard.find('[name="httpServer"]').val(),
				'httpUsername': wizard.find('[name="httpUsername"]').val(),
				'httpPassword': wizard.find('[name="httpPassword"]').val(),
				'httpParameters': wizard.find('[name="httpParameters"]').val(),
				'catalogId' : id
			};
			$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
				if (r.success){
					attributesValues = r.multiaRRay;
					aModal.$el.find('div.listnewfields').empty();
					var $contenfFields = aModal.$el.find('div.fields').empty(), i = 0, options = [], shtml = '', sfhtml = '', $fieldsselectd;
					aModal.$el.find('.finishstep div.catalogname').hide();

					shtml += '<form class="form-horizontal">';

					options.push('<option value="">---</option>');

					for( i = 0; i < r.arrAtributesname.length; i++ ) {
						options.push('<option value="'+ r.arrAtributesname[i] +'">'+ r.arrAtributesname[i] +'</option>');
					}

					for( i = 0; i < r.arrdefinition.length; i++ ) {
						shtml += '<div class="form-group"><label title="'+ r.arrdefinition[i].text +'" for="'+ r.arrdefinition[i].text +'" class="col-sm-4 control-label">'+ r.arrdefinition[i].text +'</label><div class="col-sm-8"><select placeholder="'+ $tpl.find('#tSelectAttribute').html() +'" data-value="'+ r.arrdefinition[i].value +'" class="input-sm" name="'+ r.arrdefinition[i].text +'">'+ options.join('') +'</select></div></div>'
					}

					if( r.arrExcelColumns && r.arrExcelColumns.length ) {

						for( i = 0; i < r.arrExcelColumns.length; i++ ) {
							
							sfhtml += '<div class="field" data-fieldname="'+ r.arrExcelColumns[i] +'"><div>'+ r.arrExcelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>';

						}

						aModal.$el.find('div.fieldsnew').show();

						aModal.$el.find('div.listnewfields').append( sfhtml );

						$fieldsselectd = aModal.$el.find('div.listnewfields div.field');
					}

					$contenfFields.append( shtml + '<div style="clear: both;"></div>' );

					$contenfFields.find('select').each(function(i, el) {
						var $el = $(el);

						$el.find('option[value="'+ $el.attr('name') +'"]').attr('selected', true);
					})

					$contenfFields.find('select').selectize({onDropdownClose: function() {
						/*if( this.getValue() == '' )
							this.setValue( Object.keys(this.options)[0] );*/
					}});

					$fieldsselectd && $fieldsselectd.on('click', function() {
							if( this.getAttribute('data-selected') == undefined ) {
								this.setAttribute('data-selected', 'true');
								this.setAttribute('data-i', i++);
								this.classList.add('selected');
							} else {
								this.classList.remove('selected');
								this.removeAttribute('data-selected');
								this.removeAttribute('data-i');
							}
					});
					wizard.steps('goto', 5);
				}else {
					aModal.showError(r.msg);
					aModal.$el.find(".loadingDiv").hide();
				}
			});

		}
		aModal.on('onStepChanged', function( e, currentIndex, newIndex ) {
			var type = dataSourceType;
			aModal.$el.find(".loadingDiv").hide();
			aModal.hideError();
			if (currentIndex==2) {
				first = true;
				aModal.$el.find('.nextclass').attr("href",'#next');
				aModal.$el.find('a[href="#next"]').removeClass("nextclass");
			}
  		});
		aModal.on('onStepChanging', function(e, currentIndex, newIndex ) {

			var type = dataSourceType, numMapping = 0, newStep, fn;
			var bValid = true;
			var error = '<?= translate('In order to continue the information must be fully completed') ?>';
			aModal.$el.find('[id$="-p-'+ currentIndex +'"]').find('select[required], input[required]').each(function() {
				var iValid = true;
				if( this.getAttribute('requiredisnull') && aModal.$el.find('[name="'+ this.getAttribute('requiredisnull') +'"]').val() ) {
					return;
				}
				if(!$(this).val()) {
					//@MAPR 2016-07-21: Se crea una una variable y una condición para el caso base donde la conexión sea nueva. Issue ID #MGLJPL
					var isNew = currentIndex == 1 ? true : false;
					if (type == "5" && currentIndex == 1) {
						if (!isNew) {	
						iValid = false;
						error = '<?= translate('The connection cannot be successful. There is some missing information at the FTP service') ?>';
						}
					}
				} else if( this.getAttribute('pattern')  ) {
					iValid = new RegExp(this.getAttribute('pattern')).test( this.value );
					if (!iValid && type == "5" && currentIndex == 2) {
						error = '<?= translate('Invalid FTP link service(ftp:\/\/your_FTP.domain_extension)') ?>';
					}
				}

				if( !iValid ) {
					bValid = false;
					if( this.tagName == 'SELECT' && this.selectize ) {
						this.selectize.$control.addClass('errorinput');
					} else if( this.tagName == 'INPUT' ) {
						this.classList.add('errorinput');
					}
				}

			});
			if (newIndex == 2 && currentIndex == 4 && type == '6'){
				wizard.steps('goto', 2);
				e.stopPropagation();
			}
			if( !bValid && currentIndex < newIndex &&  type =="5" && newIndex == 2) {
				e.stopPropagation();
				aModal.showError(error);
				return false;
			}
			if (currentIndex == 0) {
				first = true;
			}
			if( newIndex != 0 && type != "7") {
				/** Se agrego la funcion para validar los datos del scheduler */
				ScheduleFunction.validScheduler( aModal.$el.find('form.schedulerform'), e, 0 );

				if( e.isPropagationStopped() ) {
					aModal.showError( error );
					return;
				}
			}
			if (newIndex == 1 && type =="3"){ // Primer paso selecciono la opcion de Google Drive
				/*2015-09-14@JRPP: Invoco funcion que carga el combobox*/
				aModal.$el.find(".httpdiv").hide(); //Se esconden las opciones de HTTP
				aModal.$el.find(".dpxdiv").hide(); //Se esconden las opciones de Dropbox
				aModal.$el.find(".ftpdiv").hide();//Se esconden las opciones de FTP
				aModal.$el.find(".BDdiv").hide();//Se esconden las opciones de BD
				aModal.$el.find(".gddiv").show(); //Se muestran las opcionde de GoogleDrive
				loadConnections( 'gaccount' );
			}else if (newIndex == 1 && type =="5") { //Primer paso selecciono la opcion de FTP
				ftpstep();
				aModal.$el.find(".httpdiv").hide(); //Se esconden las opciones de HTTP
				aModal.$el.find(".dpxdiv").hide(); //Se esconden las opciones de Dropbox
				aModal.$el.find(".gddiv").hide();//Se esconden las opciones de GoogleDrive
				aModal.$el.find(".BDdiv").hide();//Se esconden las opciones de BD
				aModal.$el.find(".ftpdiv").show(); //Se muestran las opcionde de FTP
			}else if (newIndex == 1 && type =="2") {
				aModal.$el.find(".httpdiv").hide(); //Se esconden las opciones de HTTP
				aModal.$el.find(".ftpdiv").hide(); //Se esconden las opciones de FTP
				aModal.$el.find(".gddiv").hide();//Se esconden las opciones de GoogleDrive
				aModal.$el.find(".BDdiv").hide();//Se esconden las opciones de BD
				aModal.$el.find(".dpxdiv").show(); //Se muestran las opcionde de Dropbox				
				dropbox();
			}else if (newIndex == 1 && type =="6"){
				aModal.$el.find(".dpxdiv").hide(); //Se esconden las opciones de Dropbox
				aModal.$el.find(".ftpdiv").hide(); //Se esconden las opciones de FTP
				aModal.$el.find(".gddiv").hide();//Se esconden las opciones de GoogleDrive
				aModal.$el.find(".BDdiv").hide();//Se esconden las opciones de BD
				aModal.$el.find(".httpdiv").show(); //Se muestran las opcionde de HTTP	
				httpstep();
			}else if (newIndex == 1 && type =="7"){
				aModal.$el.find(".dpxdiv").hide(); //Se esconden las opciones de Dropbox
				aModal.$el.find(".ftpdiv").hide(); //Se esconden las opciones de FTP
				aModal.$el.find(".gddiv").hide();//Se esconden las opciones de GoogleDrive
				aModal.$el.find(".httpdiv").hide(); //Se esconden las opcionde de HTTP
				aModal.$el.find(".BDdiv").show();//Se muestran las opciones de BD
				BDstep();
			}
			if(currentIndex == 1 && newIndex==2 && first && (type == "3" || type =="5" || type =="2")) {
				aModal.$el.find('a[href="#next"]').addClass("nextclass");
				aModal.$el.find('a[href="#next"]').removeAttr("href");
				aModal.$el.find(".selectedTree").find("ul").empty();
				aModal.$el.find(".loadingDiv").show();
				createTreeFolder(null);
				e.stopPropagation();

			}
			if (currentIndex == 1 && newIndex==2 && type == "6"){
				searchHttpAttributes();
				e.stopPropagation();
			}
			if (newIndex==3 && type != "7"){
				if (aModal.$el.find('[name="path"]').val() == "/" || aModal.$el.find('[name="file"]').val() == "") {
					aModal.showError(error);
					e.stopPropagation();
					return false;
				}
				$selectSheet[0].selectize.clearOptions();
				aModal.$el.find(".loadingDiv").show();
				getFile();
			}
			if (newIndex==4) {
				if ($selectSheet[0].selectize.getValue() == ""  && type !="6" && type !="7"){
					aModal.showError(error);
					e.stopPropagation();
					return false;
				}
			}
		});
		$selectSheet = aModal.$el.find('select#sheets');
		$selectTypeDatabase = aModal.$el.find('select#typeDatabase');

		require(['selectize'], function(a) {
			window.renderButtonsGoogle = function() {
				gapi.load('auth2', function() {
					gapi.auth2.getAuthInstance() || gapi.auth2.init({
						client_id: '<?= $gInfoClient->web->client_id; ?>',
						scope: 'https://www.googleapis.com/auth/drive'
					});
				});
				loadConnections( 'gaccount' );
			}
			var data = {};
			ScheduleFunction.schedulerLogic( aModal.$el.find('section.scheduler'), data );
			$selectSheet.selectize().on('change', function() {
				<?//@JAPR 2016-07-19: Corregido un bug, no se estaban pasando correctamente los parámetros al utilizar nombres con ciertos caracteres (#EIK630) ?>	
				$selectSheet.val() != '' && $.get(urlPath +'action.php?action=loadFields&sheet=' + encodeURIComponent($selectSheet.val()) + '&file=' + encodeURIComponent(file) + '&id=' + id + '&type=' + dataSourceType).done(function( r ) {
					aModal.$el.find('div.listnewfields').empty();
					if( r.success ) {
						selectedSheet = $selectSheet.val();

						aModal.$el.find('input[name="catalogName"]').val( selectedSheet.replace(/\$$/, '') );

						aModal.$el.find('div.fieldsnew').hide();

						var $contenfFields = aModal.$el.find('div.fields').empty(), i = 0, options = [], shtml = '', sfhtml = '', $fieldsselectd;
						
						if( r.definition.length ) {

							aModal.$el.find('.finishstep div.catalogname').hide();

							shtml += '<form class="form-horizontal">';
							
							options.push('<option value="">---</option>');

							for( i = 0; i < r.excelColumns.length; i++ ) {
								options.push('<option value="'+ r.excelColumns[i] +'">'+ r.excelColumns[i] +'</option>');
							}
							
							for( i = 0; i < r.definition.length; i++ ) {
								shtml += '<div class="form-group"><label title="'+ r.definition[i].text +'" for="'+ r.definition[i].text +'" class="col-sm-4 control-label">'+ r.definition[i].text +'</label><div class="col-sm-8"><select placeholder="'+ $tpl.find('#tSelectAttribute').html() +'" data-value="'+ r.definition[i].value +'" class="input-sm" name="'+ r.definition[i].text +'">'+ options.join('') +'</select></div></div>'
							}

							if( r.arrExcelColumns && r.arrExcelColumns.length ) {

								for( i = 0; i < r.arrExcelColumns.length; i++ ) {
									
									sfhtml += '<div class="field" data-fieldname="'+ r.arrExcelColumns[i] +'"><div>'+ r.arrExcelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>';

								}

								aModal.$el.find('div.fieldsnew').show();

								aModal.$el.find('div.listnewfields').append( sfhtml );

								$fieldsselectd = aModal.$el.find('div.listnewfields div.field');
							}
							
							$contenfFields.append( shtml + '<div style="clear: both;"></div>' );

							$contenfFields.find('select').each(function(i, el) {
								var $el = $(el);

								$el.find('option[value="'+ $el.attr('name') +'"]').attr('selected', true);
							})
							
							$contenfFields.find('select').selectize({onDropdownClose: function() {
								/*if( this.getValue() == '' )
									this.setValue( Object.keys(this.options)[0] );*/
							}});
							
						} else {
							for( var i = 0; i < r.excelColumns.length; i++ ) {
								$contenfFields.append('<div class="field" data-fieldname="'+ r.excelColumns[i] +'"><div>'+ r.excelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>')
							}

							/***/
							$fieldsselectd = $contenfFields.find('div.field');
						}

						$fieldsselectd && $fieldsselectd.on('click', function() {
							if( this.getAttribute('data-selected') == undefined ) {
								this.setAttribute('data-selected', 'true');
								this.setAttribute('data-i', i++);
								this.classList.add('selected');
							} else {
								this.classList.remove('selected');
								this.removeAttribute('data-selected');
								this.removeAttribute('data-i');
							}
						});						
					} else if( r.msg != '' ) {
						alert( r.msg );
					}
				});
			});
			//@JRPP 2015-12-28 Se agrega funcion para el combo de tipo table
			$selectTypeDatabase.selectize().on('change', function() {
				debugger;
			});
		});

		return dfd.promise();
		
	};

});
