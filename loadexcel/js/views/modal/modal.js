//es necesario que los formularios contengan el campo 'name'
define([], function(){
		var $ = jQuery;
		var Modal = function(params) {
			params = params || {};
			return {
				options: params.options || {},
				dfd: null,
				defaultWidth: 560,
				open: function() {
					var self = this;
					self.dfd = $.Deferred();
					self.render();
					return self.dfd.promise();
				},
				render: function() {
					var myWindowModal = $("#windowModal"), self = this,
					labelOk = self.options.labelOk || $.t('Ok'), labelCancel = $.t('Cancel');
					if(myWindowModal.length > 0) {
						myWindowModal.remove();
					}
					myWindowModal = $("<div/>", {"id":"windowModal", "class":"ui-body-c", 'data-enhance':'false'});
					self.$el = myWindowModal;
					myWindowModal.append(self.options.content || '');
					$('body').append(myWindowModal);
					width = (options.width ? this.options.width : this.defaultWidth);
					optionsDialog = {
						title: self.options.title, 
						minHeight: 200, 
						minWidth: width, 
						modal: true
					}
					optionsDialog.buttons = {};
					optionsDialog.buttons[labelOk] = $.proxy(self.functionFormOk, self);
					optionsDialog.buttons[labelCancel] = $.proxy(self.functionCancel, self);
					$.mobile.ignoreContentEnabled = true;
					myWindowModal.dialogWidget(optionsDialog);
					$.mobile.ignoreContentEnabled = false;
				},
				//es necesario que los formularios contengan el campo 'name'
				functionFormOk: function() {
					var self = this, resData = {};
					resData = self.$el.find('form').serializeJSON();
					self.dfd.resolve({
						data:resData,
						close: function(){
							self.$el.dialogWidget( "close" );
							self.$el.dialogWidget( "destroy" );
							self.$el.remove();
						}
					});
				},
				functionCancel: function() {
					var self = this;
					self.$el.dialogWidget( "close" );
					self.$el.dialogWidget( "destroy" );
					self.$el.remove();
					self.dfd.reject({
						data:{}
					})
				}
			}
		}
		return Modal;
	}
);