define([
  'underscore',
  'backbone',
  'text!templates/modal/basicModal.html',
  'bootstrap'
], function(_, Backbone, Template){
  var ModalPage = Backbone.View.extend({
    tagName: 'div',
    initialize: function(opt) {
      var self = this;
      this.options = this.options || opt;
      this.options.buttons = this.options.buttons || [];

      self.setElement($(_.template(Template)( {modalTitle: self.options.modalTitle, modalBody: '', bsModalSize:'', labelClose: this.options.labels.close} )));

      self.$el.find('.modal-body').append(self.options.modalBody);

      if(self.options.className) {
        self.$el.addClass(self.options.className);
      }
    },

    options: {
      labels: {
        close: 'Close'
      }
    },

    events: {
      
    },
    setContent: function(content) {
      this.$el.find('.modal-body').empty().append(content);

      this._resizeBody();

      return this;
    },
    setTitle: function(title) {
      this.$el.find('.modal-header .modal-title').html(title);
      return this;
    },
    width: function(w) {
      this.$el.find('> .modal-dialog').width(w);
      return this;
    },
    height: function(w) {
      this.$el.find('> .modal-dialog > .modal-content > .modal-body').height(w);
      return this;
    },
    addButton: function(aButton) {
      this.options.buttons.push(aButton);
      this.$el.find('.modal-footer').append($('<button type="button" class="btn btn-primary">'+ aButton.label +'</button>').on('click', aButton.callback));      
      return this;
    },
    render: function (opts) {
      var self = this;
      opts = opts || {};
      self.$el = self.$el.modal(opts).on('hidden.bs.modal', function (e) {
      if( self.options.removeContent !== false )
        $(this).detach();
        self.trigger('hidden');
      }).on('shown.bs.modal', function() {
        self._resizeBody();
        //Se lanza cuando la ventana modal sea visible para el usuario (despues de la animacion)
        self.trigger('shown');
      });
      return self.$el;
    },
    _resizeBody: function() {
      var h = $(window).height();

      if( h < this.$el.find('.modal-body').height() ) {

        this.$el.find('.modal-body').css( { overflow: 'auto', height: (h - 200) + 'px'} );

      }
    },
    remove: function() {
      var self = this;
      self.$el.modal('hide');
      return this;
    },
    disabled: function(aBoolean) {
      if( aBoolean ) {
        this.$el.find('.modal-footer').css('opacity', 0);
        this.$el.find('.modal-header button.close').hide();
      }
      else {
        this.$el.find('.modal-footer').css('opacity', 1);
        this.$el.find('.modal-header button.close').show();
      }
      return this;
    }
  });
  return ModalPage;
});