﻿<?php
	$chdirCurrent = getcwd ();
	chdir('../../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);	
?>
define([
	'lodash',
	'backbone',
	'text!templates/loadHTML/wizardHTML.php',
	'views/modal/wizardModal',
	'text!templates2/agendas/newscheduler.php',
	'javascript/views/agendas/schedulerlogic',
	'javascript/eBavel/date',
	'libs/misc/SimpleAjaxUploader.min',
	'libs/selectize.js/dist/js/standalone/selectize',
	'bootstrap',
	'libs/metisMenu/src/metisMenu'
], function(_, Backbone, Template, WizardModal, Template2, ScheduleFunction){
	
	var wizard, aModal, $select, urlPath = 'loadexcel/';
	attributesValues = [];
	first = true;
	/** Recibe el ID de la definicion del modelo de Artus a cargar, si esta vacio crea uno nuevo */
	var serializeJSON = function( form ) {
		var resData = {};
		
		var json = {};
		
		jQuery.map(form.serializeArray(), function(n, i) {
			if(json[n['name']]) {
				if($.isArray(json[n['name']])) {
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = new Array(json[n['name']]);
					json[n['name']].push(n['value']);
				}
			}
			else {
				if($('#' + n['name']).attr('multiple') == 'multiple') {
					json[n['name']] = new Array();
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = n['value'];
				}
			}
		});

		resData = json;

		return resData;	
	}
	return function( id ) {
		var file, selectedSheet, dfd = $.Deferred(), bFinish = false, $tpl = $(_.template(Template)( {} )),$scheduler = $(_.template( Template2 )({}));
		
		/** Testing*/
		//id = 1;
		
		aModal = new WizardModal({
			modalTitle: (id ? $tpl.find('#tLoadExcel').html() : $tpl.find('#tLoadExcel').html()),
			className: 'material'
		});

		$('body').append(aModal.render().$el);		
		aModal.width(700);	
		aModal.$el.find('.modal-body').html($.t('Loading') + '...');
		$scheduler.find('section.scheduler');
		$scheduler = $scheduler.find('section.scheduler');
		$scheduler.find('div.range_of_recurrence').append('<div style="clear: both;margin-top: 18px;"><label class="pull-left"><?= translate('Minutes') ?>&nbsp;</label><input required name="Interval" min="0" class="form-control" style="width: 70px;font-size: 13px;" type="number"></input></div>');
		$scheduler.find('<?php if (getMDVersion() >= esvDataSourceAgentHours) echo ".form-group.generate_next_day"; else echo ".form-group.capture_start_time, .form-group.generate_next_day"; ?>').remove();
		$scheduler.appendTo($tpl.find('#vacio'));
		var $stepscheduler = $tpl.find('.scheduler').detach();
		$tpl.find('#vacio').replaceWith($stepscheduler);
		/** Agregamos el contenido para la ventana modal tipo wizard */
		wizard = $tpl;
		aModal.setContent(wizard);
		//2015-09-17@JRPP: Se agrega un formulario en la seccion final de scheduler
		aModal.$el.find('section.scheduler').append("<form class = 'schedulerform'></form>");
		aModal.$el.find('form.schedulerform').append( aModal.$el.find('section.scheduler > div').detach() );

		aModal.$el.find('a[href="#previous"]').html( $tpl.find('#tPrevious').html() );
		aModal.$el.find('a[href="#next"]').html( $tpl.find('#tNext').html() );
		aModal.$el.find('a[href="#finish"]').html( $tpl.find('#tFinish').html() );
		
		/** Al terminar mandamos la informacion de la hoja de excel cargada y los campos seleccionados */
		aModal.on('onFinished', function(res) {
			var $fields = aModal.$el.find('div.fields div.field[data-selected="true"]'), arrFields = [], arrNewFields = [], catalogName = aModal.$el.find('input[name="catalogName"]').val();
			var dataScheduler = serializeJSON(aModal.$el.find('section.scheduler form').filter('form.schedulerform'));
			if( id == undefined && $fields.length == 0 || $.trim(catalogName) == '' ) {
				res.result = false;
				
				if( $.trim(catalogName) == '' ) {
					aModal.showError( $tpl.find('#tInvalidname').html() );
				} else {
					aModal.showError( $tpl.find('#tInvalidfields').html() );
				}

				return;
			} else if ( $fields.length == 0 ) {
				aModal.$el.find('div.fields select').each(function(i, el) {
					arrFields.push({ columntable: el.getAttribute('data-value'), columnexcel: el.value });
				});

				$fields = aModal.$el.find('div.fieldsnew div.field[data-selected="true"]');
			}

			$fields.each(function(i, el) {
				
				if( id == undefined )
					arrFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
				else
					arrNewFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
			});

			if( $fields.length ) {
				arrFields && arrFields.sort(function(a, b) {
					return a.i - b.i;
				});

				arrNewFields && arrNewFields.sort(function(a, b) {
					return a.i - b.i;
				});
			}

			aModal.$el.find('section.finishstep .mappingselect').hide();

			aModal.$el.find('section.finishstep .creatingtables').show();
			var httpName = wizard.find('#httpName').val();
			var httpServer = wizard.find('[name="httpServer"]').val();
			var httpUsername = wizard.find('[name="httpUsername"]').val();
			var httpPassword = wizard.find('[name="httpPassword"]').val();
			var httpParameters = wizard.find('[name="httpParameters"]').val();

			!bFinish && $.post(urlPath + 'action.php?action=processExcel', {fields: arrFields, 'arrNewFields': arrNewFields, id: id, sheet: selectedSheet, catalogName: catalogName, 'type': "6", 'httpServer': httpServer, 'httpUsername': httpUsername, 'httpPassword': httpPassword, 'httpParameters': httpParameters, 'attributesValues': attributesValues, 'RecurActive': 1, 'httpName': httpName}).done(function( r ) {
				aModal.hideError();
				bFinish = false;
				if( r.success ) {
					dataScheduler.RecurActive = 1;
					$.post(urlPath + 'action.php', {action: 'saveScheduler', id: r.id, data: dataScheduler}).done(function( r ) {

					});
					dfd.resolve( r.id, catalogName );
					aModal.remove();
				}
				else {
					aModal.$el.find('section.finishstep .creatingtables').hide();
					aModal.$el.find('section.finishstep .mappingselect').show();
					aModal.showError( r.msg );
					//dfd.reject();
				}
			});

			bFinish = true;
			
		});
		aModal.on('onStepChanged', function(o, a, b) {
			aModal.hideError();
			aModal.$el.find(".loadingDiv").hide();
			if (a==2 && b == 1) {
				aModal.$el.find('a[href="#next"]').hide();
				first = true;
			}else {
				aModal.$el.find('a[href="#next"]').show();
			}
		});
		aModal.on('onStepChanging', function(o, a, b) {
			var error = '<?= translate('In order to continue the information must be fully completed') ?>';
			var bValid = true;
			aModal.$el.find('[id$="-p-'+ a +'"]').find('select[required], input[required]').each(function() {
				var iValid = true;

				if( this.getAttribute('requiredisnull') && aModal.$el.find('[name="'+ this.getAttribute('requiredisnull') +'"]').val() ) {
					return;
				}
				if( !iValid ) {
					bValid = false;
					if( this.tagName == 'SELECT' && this.selectize ) {
						this.selectize.$control.addClass('errorinput');
					} else if( this.tagName == 'INPUT' ) {
						this.classList.add('errorinput');
					}
				}

			});
			if( !bValid && a < b) {
				o.stopPropagation();
				aModal.showError( error );
				return false;
			}
			if( b != 0 ) {
				/** Se agrego la funcion para validar los datos del scheduler */
				ScheduleFunction.validScheduler( aModal.$el.find('form.schedulerform'), o, 0 );

				if( o.isPropagationStopped() ) {
					aModal.showError( error );
					return;
				}
			}
			if (a == 0) {
				first = true;
			}
			if( b == 0 ) {
				aModal.$el.find('a[href="#next"]').parent().show();
			}
			if(b==4 && a == 3 && aModal.$el.find('#sheets').val() == '') {
				aModal.showError(error);
				o.stopPropagation();
				return false;
			}
			if(b==2 && a==1 && first){
				aModal.$el.find(".loadingDiv").show();
				searchHttpAttributes();
				o.stopPropagation();
			}
		});
		
		$select = aModal.$el.find('select#sheets');
		$selectFTP = aModal.$el.find('select#ftpselector');

		require(['selectize'], function(a) {
			var data = {};
			ScheduleFunction.schedulerLogic( aModal.$el.find('section.scheduler'), data );
			$select.selectize().on('change', function() {
				<?//@JAPR 2016-07-19: Corregido un bug, no se estaban pasando correctamente los parámetros al utilizar nombres con ciertos caracteres (#EIK630) ?>
				$select.val() != '' && $.get(urlPath +'action.php?action=loadFields&sheet=' + encodeURIComponent($select.val()) + '&file=' + encodeURIComponent(file) + '&id=' + id + '&type=6').done(function( r ) {
					if( r.success ) {

						selectedSheet = $select.val();

						aModal.$el.find('input[name="catalogName"]').val( selectedSheet.replace(/\$$/, '') );

						aModal.$el.find('div.fieldsnew').hide();

						var $contenfFields = aModal.$el.find('div.fields').empty(), i = 0, options = [], shtml = '', sfhtml = '', $fieldsselectd;
						
						if( r.definition.length ) {

							aModal.$el.find('.finishstep div.catalogname').hide();

							shtml += '<form class="form-horizontal">';
							
							options.push('<option value="">---</option>');

							for( i = 0; i < r.excelColumns.length; i++ ) {
								options.push('<option value="'+ r.excelColumns[i] +'">'+ r.excelColumns[i] +'</option>');
							}
							
							for( i = 0; i < r.definition.length; i++ ) {
								shtml += '<div class="form-group"><label title="'+ r.definition[i].text +'" for="'+ r.definition[i].text +'" class="col-sm-4 control-label">'+ r.definition[i].text +'</label><div class="col-sm-8"><select placeholder="'+ $tpl.find('#tSelectAttribute').html() +'" data-value="'+ r.definition[i].value +'" class="input-sm" name="'+ r.definition[i].text +'">'+ options.join('') +'</select></div></div>'
							}

							if( r.arrExcelColumns && r.arrExcelColumns.length ) {

								for( i = 0; i < r.arrExcelColumns.length; i++ ) {
									
									sfhtml += '<div class="field" data-fieldname="'+ r.arrExcelColumns[i] +'"><div>'+ r.arrExcelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>';

								}

								aModal.$el.find('div.fieldsnew').show();

								aModal.$el.find('div.listnewfields').append( sfhtml );

								$fieldsselectd = aModal.$el.find('div.listnewfields div.field');
							}
							
							$contenfFields.append( shtml + '<div style="clear: both;"></div>' );

							$contenfFields.find('select').each(function(i, el) {
								var $el = $(el);

								$el.find('option[value="'+ $el.attr('name') +'"]').attr('selected', true);
							})
							
							$contenfFields.find('select').selectize({onDropdownClose: function() {
								/*if( this.getValue() == '' )
									this.setValue( Object.keys(this.options)[0] );*/
							}});
							
						} else {
							for( var i = 0; i < r.excelColumns.length; i++ ) {
								$contenfFields.append('<div class="field" data-fieldname="'+ r.excelColumns[i] +'"><div>'+ r.excelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>')
							}

							/***/
							$fieldsselectd = $contenfFields.find('div.field');
						}

						$fieldsselectd && $fieldsselectd.on('click', function() {
							if( this.getAttribute('data-selected') == undefined ) {
								this.setAttribute('data-selected', 'true');
								this.setAttribute('data-i', i++);
								this.classList.add('selected');
							} else {
								this.classList.remove('selected');
								this.removeAttribute('data-selected');
								this.removeAttribute('data-i');
							}
						});						
					} else if( r.msg != '' ) {
						alert( r.msg );
					}
				});
			});
			var ftpstep = function() {		
				loadConnections( 'ftpaccount' );
			}
			var loadConnections = function( sEl ) {
				var type = 6;
				$.get('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'getInfoSrvConnection', type: type}).done(function( r ) {
					aModal.$el.find('[name="'+ sEl +'"]').selectize({
						options: [ {text: '<?= translate( '(New connection)' ) ?>', value: 0} ].concat( r ),
						render: {
							option: function(item, escape) {
								var label = item.text
								var caption = item.value == 0 ? ( type == 2 ? '<?= translate('Creates a new connection to') ?> DropBox' : 
								( type == 3 ? '<?= translate('Creates a new connection to') ?> Google Drive' :
									( type == 5 ? '<?= translate('Creates a new connection to') ?> FTP' : ( type == 6 ? '<?= translate('Creates a new connection to') ?> HTTP':null )
								 ) )) : null;

								return '<div>' +
									'<span class="label">' + escape(label) + '</span>' +
									(caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
									'</div>';
							}
						},
						onItemAdd: function( v ) {
							if( v == 0 ) {
								wizard.find('div.ftpnew').show();
								wizard.find('#httpName').val("");
								wizard.find('#ftpServer').val("");
								wizard.find('#ftpUsername').val("");
								wizard.find('#ftpPassword').val("");
								if (wizard.find('#httpName').val()==""){
									wizard.find('#httpName').focus();
								}else {
									wizard.find('[name="ftpServer"]').focus();
								}
								wizard.find('[name="'+ sEl +'"]')[0].selectize.clear( true );
							}
						}
					});
					aModal.$el.find('[name="'+ sEl +'"]').selectize().on('change', function() {
						if(aModal.$el.find('[name="'+ sEl +'"]').val()!= "") {
							$.get(urlPath +'action.php?action=getHttpConexion&idConnection=' + aModal.$el.find('[name="'+ sEl +'"]').val()).done(function( response ) {
								if (response.success) {
									debugger;
									wizard.find('div.ftpnew').show();
									wizard.find('#httpServer').val(response.http);
									wizard.find('#httpUsername').val(response.user);
									wizard.find('#httpPassword').val(response.pass);
									if(response.servicename){
										wizard.find('#httpName').val(response.servicename);
									}
								}
							});
						}
					});
				});
			}
			ftpstep();
		});
		var searchHttpAttributes = function(idFolder) {
			wizard.find('[name="path"]').val("/");
			var data = {
				'action': 'downloadHttpAttributes',
				'type': '6',
				'httpName': wizard.find('[name="httpName"]').val(),
				'httpServer': wizard.find('[name="httpServer"]').val(),
				'httpUsername': wizard.find('[name="httpUsername"]').val(),
				'httpPassword': wizard.find('[name="httpPassword"]').val(),
				'httpParameters': wizard.find('[name="httpParameters"]').val(),
				'catalogId' : '-1'
			};
			$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
				if (r){
					if (r.success){
						attributesValues = r.multiaRRay;
						first = false;
						aModal.$el.find('div.fieldsnew').hide();
						var $contenfFields = aModal.$el.find('div.fields').empty(), i = 0, options = [], shtml = '', sfhtml = '', $fieldsselectd;

						for( var i = 0; i < r.arrAtributesname.length; i++ ) {
									$contenfFields.append('<div class="field" data-fieldname="'+ r.arrAtributesname[i] +'"><div>'+ r.arrAtributesname[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>')
						}
						$fieldsselectd = $contenfFields.find('div.field');
						$fieldsselectd && $fieldsselectd.on('click', function() {
								if( this.getAttribute('data-selected') == undefined ) {
									this.setAttribute('data-selected', 'true');
									this.setAttribute('data-i', i++);
									this.classList.add('selected');
								} else {
									this.classList.remove('selected');
									this.removeAttribute('data-selected');
									this.removeAttribute('data-i');
								}
						});
					wizard.steps('goto', 2);
					}else {
						aModal.showError(r.msg);
						aModal.$el.find(".loadingDiv").hide();
					}
				}else {
					var error = '<?= translate('Invalid link service') ?>';
					aModal.showError(error);
					aModal.$el.find(".loadingDiv").hide();
				}
			});
		}
		return dfd.promise();
	};

});
