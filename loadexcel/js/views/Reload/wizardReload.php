﻿<?php
	$chdirCurrent = getcwd ();
	chdir('../../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);
	/** Leemos la informacion del cliente de Google */
	$sJson = file_get_contents('../../../../datadestinations/libs/Google/client_secret_gDrive.json');
	$gInfoClient = json_decode( $sJson );
?>define([
	'lodash',
	'backbone',
	'text!templates/loadReload/wizardReload.php',
	'views/modal/wizardModal',
	'text!templates2/agendas/newscheduler.php',
	'javascript/views/agendas/schedulerlogic',
	'javascript/eBavel/date',
	'libs/misc/SimpleAjaxUploader.min',
	'libs/selectize.js/dist/js/standalone/selectize',
	'bootstrap',
	'libs/metisMenu/src/metisMenu'
], function(_, Backbone, Template, WizardModal, Template2, ScheduleFunction){
	
	var wizard, aModal, $selectSheet, urlPath = 'loadexcel/', $selectTable, $selectTypeDatabase;
	attributesValues = [];
	first = true;
	var serializeJSON = function( form ) {
		var resData = {};
		
		var json = {};
		
		jQuery.map(form.serializeArray(), function(n, i) {
			if(json[n['name']]) {
				if($.isArray(json[n['name']])) {
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = new Array(json[n['name']]);
					json[n['name']].push(n['value']);
				}
			}
			else {
				if($('#' + n['name']).attr('multiple') == 'multiple') {
					json[n['name']] = new Array();
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = n['value'];
				}
			}
		});

		resData = json;

		return resData;	
	}
	/** Recibe el ID de la definicion del modelo de Artus a cargar, si esta vacio crea uno nuevo */
	return function( id, dataSourceType, dataSourceDocument, dataSourceSheet, dataSourcePath, dataSourceServiceConnectionID, GoogleDriveID, RecurActive, membersDatasource, dataSourcename,	RecurPatternNum, RecurPatternOpt,RecurPatternType,RecurRangeOpt, Interval, CaptureStartTime_Hours, CaptureStartTime_Minutes, Parameters, SourceDatabase, SourceTable) {
		var file, selectedSheet, dfd = $.Deferred(), bFinish = false, $tpl = $(_.template(Template)( {} )), $scheduler = $(_.template( Template2 )({})), isEditable = false;
		/** Testing*/
		//id = 1;

		if (dataSourceType == "4" ||dataSourceType == "5" ||dataSourceType == "6" ) {
			isEditable = true;
		}
		aModal = new WizardModal({
			modalTitle: (id ? $tpl.find('#tLoadExcel').html() : $tpl.find('#tLoadExcel').html()),
			className: 'material'
		});

		$('body').append(aModal.render().$el);

		aModal.width(1000);	

		aModal.$el.find('.modal-body').html($.t('Loading') + '...');
		$scheduler.find('section.scheduler');
		$scheduler = $scheduler.find('section.scheduler');
		$scheduler.find('div.range_of_recurrence').append('<div style="clear: both;margin-top: 18px;"><label class="pull-left"><?= translate('Minutes') ?>&nbsp;</label><input required name="Interval" min="0" class="form-control" style="width: 70px;font-size: 13px;" type="number"></input></div>');
		$scheduler.find('<?php if (getMDVersion() >= esvDataSourceAgentHours) echo ".form-group.generate_next_day"; else echo ".form-group.capture_start_time, .form-group.generate_next_day"; ?>').remove();
		$scheduler.appendTo($tpl.find('#vacio'));
		var $stepscheduler = $tpl.find('.scheduler').detach();
		$tpl.find('#vacio').replaceWith($stepscheduler);
		/** Agregamos el contenido para la ventana modal tipo wizard */
		wizard = $tpl;
		aModal.setContent(wizard);

		//2015-09-17@JRPP: Se agrega un formulario en la seccion final de scheduler
		aModal.$el.find('section.scheduler').append("<form class = 'schedulerform'></form>");
		aModal.$el.find('form.schedulerform').append( aModal.$el.find('section.scheduler > div').detach() );

		//2015-09-04@JRPP: Se esconden los botones de la navegacion para evitar que el usuario navege incorrectamente por los steps
		if (dataSourceType) {
			switch (dataSourceType) {
				case "4":
					aModal.$el.find('input:radio[name="type"]').filter('[value="2"]').attr('checked', true);
					break; 
				case "5":
					aModal.$el.find('input:radio[name="type"]').filter('[value="3"]').attr('checked', true);
					break; 
				case "6":
					aModal.$el.find('input:radio[name="type"]').filter('[value="5"]').attr('checked', true);
					break;
				case "7":
					aModal.$el.find('input:radio[name="type"]').filter('[value="6"]').attr('checked', true);
					break; 
				case "8":
					aModal.$el.find('input:radio[name="type"]').filter('[value="7"]').attr('checked', true);
					break; 
			}
		}
		aModal.$el.find('input:radio[name="type"]').click(function() {
			isEditable = false;
			wizard.find('a[href="#finish"]').parent().hide();
			var valueType;
			switch (dataSourceType) {
				case "4":
					valueType = "2";
					break; 
				case "5":
					valueType = "3";
					break; 
				case "6":
					valueType = "5";
					break; 
				case "7":
					valueType = "6";
					break; 
				case "8":
					valueType = "7";
					break; 
			}
			switch ($(this).val()) {
				case "3":
				case "2":
				case "5":
					aModal.$el.find( ".number" ).each(function() {
						$(this).parent().parent().show();
					});
					aModal.$el.find('div.checkbox').show();
					aModal.$el.find('div.divSheetselect').show();
					aModal.$el.find('div.divTableselect').hide();
					break; 
				case "6":
					aModal.$el.find( ".number" ).each(function() {
						if ($(this).html() == '4.' || $(this).html() == '5.' ) {
							 $(this).parent().parent().hide();
						}else {
							 $(this).parent().parent().show();
						}
					});
					aModal.$el.find('div.checkbox').show();
					aModal.$el.find('div.divSheetselect').hide();
					aModal.$el.find('div.divTableselect').hide();
					break; 
				case "7":
					aModal.$el.find( ".number" ).each(function() {
						if ($(this).html() == '2.' || $(this).html() == '4.' ) {
							 $(this).parent().parent().hide();
						}else {
							 $(this).parent().parent().show();
						}
					});
					aModal.$el.find('div.checkbox').hide();
					aModal.$el.find('div.divSheetselect').hide();
					aModal.$el.find('div.divTableselect').show();
					break; 
			}
		});
		//aModal.$el.find('.steps.clearfix').hide();
		aModal.$el.find('a[href="#previous"]').html( $tpl.find('#tPrevious').html() );
		aModal.$el.find('a[href="#next"]').html( $tpl.find('#tNext').html() );
		aModal.$el.find('a[href="#finish"]').html( $tpl.find('#tFinish').html() );
		//2015-09-18@JRPP: Si no esta habilitada por default la opcion oculto todos los pasos siguientes y cambio
		// el texto del boton siguiente por terminar
		if (dataSourceType == "7") {
			aModal.$el.find( ".number" ).each(function() {
				if ($(this).html() == '4.' || $(this).html() == '5.' ) {
					 $(this).parent().parent().hide();
				}
			});
		}
		if (RecurActive == "0"  && dataSourceType != "8") {
			aModal.$el.find('#activeCheck').prop("checked", "");
			aModal.$el.find('a[href="#next"]').html($tpl.find('#tFinish').html());
			aModal.$el.find('li.disabled').hide();
		}else if (isEditable  && dataSourceType != "8") {
			aModal.$el.find('a[href="#finish"]').parent().show();
		}
		if (dataSourceType == "8"){
			aModal.$el.find('a[href="#next"]').html($tpl.find('#tNext').html());
			aModal.$el.find('li.disabled').show();
			aModal.$el.find('div.checkbox').hide();
			aModal.$el.find( ".number" ).each(function() {
				if ($(this).html() == '2.' || $(this).html() == '4.' ) {
					 $(this).parent().parent().hide();
				}
			});
			aModal.$el.find('div.divSheetselect').hide();
			aModal.$el.find('div.divTableselect').show();
		}else {
			aModal.$el.find('div.divSheetselect').show();
			aModal.$el.find('div.divTableselect').hide();	
		}
		aModal.$el.find('#activeCheck').click(function() {
			var type = aModal.$el.find('[name="type"]:checked').val()
			// access properties using this keyword
			if ( this.checked ) {
				aModal.$el.find('a[href="#next"]').html($tpl.find('#tNext').html());
				aModal.$el.find('li.disabled').show();
				aModal.$el.find('li.done').show();
				if(isEditable){
					aModal.$el.find('a[href="#finish"]').parent().show();
				}
				if (type == "6"){
					aModal.$el.find( ".number" ).each(function() {
						if ($(this).html() == '4.' || $(this).html() == '5.' ) {
							 $(this).parent().parent().hide();
						}
					});
				}
			} else {
				aModal.$el.find('a[href="#next"]').html($tpl.find('#tFinish').html());
				aModal.$el.find('li.disabled').hide();
				aModal.$el.find('li.done').hide();
				if(isEditable){
					aModal.$el.find('a[href="#finish"]').parent().hide();
				}
			}
		});
		/** Al terminar mandamos la informacion de la hoja de excel cargada y los campos seleccionados */
		aModal.on('onFinished', function(anEvent, nStep) {
			var $fields = aModal.$el.find('div.fields div.field[data-selected="true"]'), arrFields = [], arrNewFields = [], catalogName = aModal.$el.find('input[name="catalogName"]').val();

			if( nStep == 1 ) {
				/** Se agrego la funcion para validar los datos del scheduler */
				ScheduleFunction.validScheduler( aModal.$el.find('form.schedulerform'), anEvent, 0 );

				if( anEvent.isPropagationStopped() ) return;
			}

			/*Si es editable quiere decir que puede finalizar en cualquier step*/
			var typedownload = aModal.$el.find('[name="type"]:checked').val();
			var account = "";
			var filename = "";
			var downloadPath = "";
			var idGoogleDrive = "";
			var FTPServer = aModal.$el.find('[name="ftpServer"]').val();
			var FTPUser = aModal.$el.find('[name="ftpUsername"]').val();
			var FTPPass = aModal.$el.find('[name="ftpPassword"]').val();
			var httpServer = wizard.find('[name="httpServer"]').val();
			var httpUsername = wizard.find('[name="httpUsername"]').val();
			var	httpPassword = wizard.find('[name="httpPassword"]').val();
			var httpParameters = wizard.find('[name="httpParameters"]').val();
			var BDServer = wizard.find('[name="BDServer"]').val();
			var BDUsername = wizard.find('[name="BDUsername"]').val();
			var BDPassword = wizard.find('[name="BDPassword"]').val();
			var Database = wizard.find('[name="Database"]').val();
			/*@JRPP 2016-01-20 Se agregan todas los divs donde se guarda el valor del nombre de la conexion*/
			var httpName = wizard.find('#httpName').val();
			var ftpName = wizard.find('#ftpName').val();
			var BDName = wizard.find('#BDName').val();
			var Table = $selectTable.val();
			var TypeOfDatabase = $selectTypeDatabase.val();
			
			var dataScheduler = serializeJSON(aModal.$el.find('section.scheduler form').filter('form.schedulerform'));
			filename = aModal.$el.find('[name="file"]').val();
			downloadPath = aModal.$el.find('[name="path"]').val();

			if (typedownload=="3") {
				account = aModal.$el.find('[name="gaccount"]').val();
				idGoogleDrive = aModal.$el.find('[name="path"]').data("idFolderDrive");
			}else if (typedownload=="5") {
				account = aModal.$el.find('[name="ftpaccount"]').val();
			}else if (typedownload=="2") {
				account = aModal.$el.find('[name="dpxaccount"]').val();
			}if(typedownload=="6"){
				catalogName=dataSourcename;
			}
			if (isEditable && nStep < 5){
				var notEditedFields = membersDatasource;
				file = dataSourceDocument;
				selectedSheet = dataSourceSheet;
				filename = dataSourceDocument;
				downloadPath = dataSourcePath;
				account = dataSourceServiceConnectionID;
				idGoogleDrive = GoogleDriveID;
				catalogName=dataSourcename;

				switch (dataSourceType) {
					case "4":
						typedownload = "2";
						break; 
					case "5":
						typedownload = "3";
						break; 
					case "6":
						typedownload = "5";
						break;
					case "7":
						typedownload = "6";
						break; 
					case "8":
						typedownload = "7";
						break; 
				}
				if( dataScheduler['RecurPatternType'] ) {
					!bFinish && $.when( $.post(urlPath + 'action.php?action=processExcel', {'fields': notEditedFields, 'arrNewFields': arrNewFields, 'file': file, 'id': id, 'sheet': selectedSheet, 'catalogName': catalogName, 'type': typedownload, 'filename' : filename , 'Path' : downloadPath, 'Account' : account, 'idGoogleDrive': idGoogleDrive, 'FTPServer': FTPServer, 'FTPUser': FTPUser, 'FTPPass': FTPPass, 'attributesValues': attributesValues, 'httpName': httpName, 'FTPName': ftpName, 'BDName': BDName})
					, $.post(urlPath + 'action.php', {action: 'saveScheduler', id: id, data: dataScheduler})).done(function( a1, a2 ) {
						var  r =  a1[0];
						aModal.hideError();

						bFinish = false;
						if( r.success ) {
							dfd.resolve( r.id, catalogName );
							aModal.remove();
						}
						else {
							aModal.$el.find('section.finishstep .creatingtables').hide();
							aModal.$el.find('section.finishstep .mappingselect').show();
							aModal.showError( r.msg );
						}

					});			
				} else {
					!bFinish && $.post(urlPath + 'action.php?action=processExcel', {'fields': notEditedFields, 'arrNewFields': arrNewFields, 'file': file, 'id': id, 'sheet': selectedSheet, 'catalogName': catalogName, 'type': typedownload, 'filename' : filename , 'Path' : downloadPath, 'Account' : account, 'idGoogleDrive': idGoogleDrive, 'FTPServer': FTPServer, 'FTPUser': FTPUser, 'FTPPass': FTPPass, 'attributesValues': attributesValues, 'httpName': httpName, 'FTPName': ftpName, 'BDName': BDName}).done(function( r ) {
						
						aModal.hideError();

						bFinish = false;
						if( r.success ) {
							dfd.resolve( r.id, catalogName );
							aModal.remove();
						}
						else {
							aModal.$el.find('section.finishstep .creatingtables').hide();
							aModal.$el.find('section.finishstep .mappingselect').show();
							aModal.showError( r.msg );
						}
					}); 
				}
				return;
			}
			if( id == undefined && $fields.length == 0 || $.trim(catalogName) == '' ) {
				anEvent.stopPropagation();
				aModal.showError( );
				return;
			} else if ( $fields.length == 0 ) {
				aModal.$el.find('div.fields select').each(function(i, el) {
					arrFields.push({ columntable: el.getAttribute('data-value'), columnexcel: el.value });
				});

				$fields = aModal.$el.find('div.fieldsnew div.field[data-selected="true"]');
			}

			$fields.each(function(i, el) {
				
				if( id == undefined )
					arrFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
				else
					arrNewFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
			});

			if( $fields.length ) {
				arrFields && arrFields.sort(function(a, b) {
					return a.i - b.i;
				});

				arrNewFields && arrNewFields.sort(function(a, b) {
					return a.i - b.i;
				});
			}

			aModal.$el.find('section.finishstep .mappingselect').hide();

			aModal.$el.find('section.finishstep .creatingtables').show();
			
			if( dataScheduler['RecurPatternType'] ) {
				!bFinish && $.when( $.post(urlPath + 'action.php?action=processExcel', {'fields': arrFields, 'arrNewFields': arrNewFields, 'file': file, 'id': id, 'sheet': selectedSheet, 'catalogName': catalogName, 'type': typedownload, 'filename' : filename , 'Path' : downloadPath, 'Account' : account, 'idGoogleDrive': idGoogleDrive, 'FTPServer': FTPServer, 'FTPUser': FTPUser, 'FTPPass': FTPPass, 'attributesValues': attributesValues, 'httpServer': httpServer, 'httpUsername': httpUsername, 'httpPassword': httpPassword, 'httpParameters': httpParameters, 'BDServer': BDServer, 'BDUsername': BDUsername,'BDPassword': BDPassword, 'Database' : Database, 'Table' : Table, 'TypeOfDatabase': TypeOfDatabase, 'httpName': httpName, 'FTPName': ftpName, 'BDName': BDName})
				, $.post(urlPath + 'action.php', {action: 'saveScheduler', id: id, data: dataScheduler})).done(function( a1, a2 ) {
					var  r =  a1[0];
					aModal.hideError();

					bFinish = false;
					if( r.success ) {
						dfd.resolve( r.id, catalogName );
						aModal.remove();
					}
					else {
						aModal.$el.find('section.finishstep .creatingtables').hide();
						aModal.$el.find('section.finishstep .mappingselect').show();
						aModal.showError( r.msg );
					}

				});			
			} else {
				!bFinish && $.post(urlPath + 'action.php?action=processExcel', {'fields': arrFields, 'arrNewFields': arrNewFields, 'file': file, 'id': id, 'sheet': selectedSheet, 'catalogName': catalogName, 'type': typedownload, 'filename' : filename , 'Path' : downloadPath, 'Account' : account, 'idGoogleDrive': idGoogleDrive, 'FTPServer': FTPServer, 'FTPUser': FTPUser, 'FTPPass': FTPPass, 'attributesValues': attributesValues, 'BDServer': BDServer, 'BDUsername': BDUsername,'BDPassword': BDPassword, 'Database' : Database, 'Table' : Table, 'TypeOfDatabase': TypeOfDatabase, 'httpName': httpName, 'FTPName': ftpName, 'BDName': BDName}).done(function( r ) {
					
					aModal.hideError();

					bFinish = false;
					if( r.success ) {
						dfd.resolve( r.id, catalogName );
						aModal.remove();
					}
					else {
						aModal.$el.find('section.finishstep .creatingtables').hide();
						aModal.$el.find('section.finishstep .mappingselect').show();
						aModal.showError( r.msg );
					}
				}); 
			}
			bFinish = true;
			
		});
		var gdrive = function() {
			(function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				po.src = 'https://apis.google.com/js/platform.js?onload=renderButtonsGoogle';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			})();	
		}
		var ftpstep = function() {
			loadConnections( 'ftpaccount' );
		}
		var dropbox=function() {
			loadConnections( 'dpxaccount' );
		}
		var httpstep=function() {
			loadConnections( 'httpaccount' );
		}
		var BDstep=function() {
			loadConnections( 'BDaccount' );
		}
		var changeRecurActive = function() {
			var catalogName = aModal.$el.find('input[name="catalogName"]').val();
			$.post(urlPath + 'action.php?action=changeRecurActive', {'dataSourceID':id , 'RecurActive': 0}).done(function( r ) {
				dfd.resolve( id, catalogName );
				aModal.remove();
			});
		}
		var loadConnections = function( sEl ) {
			var type = wizard.find('[name="type"]:checked').val();
			$.get('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'getInfoSrvConnection', type: type}).done(function( r ) {
				wizard.find('[name="'+ sEl +'"]').selectize({
					//options:(type==3? r :[ {text: '<?= translate( '(New connection)' ) ?>', value: 0} ].concat( r )) ,
					options:[ {text: '<?= translate( '(New connection)' ) ?>', value: 0} ].concat( r ),
					render: {
						option: function(item, escape) {
							var label = item.text
								var caption = item.value == 0 ? ( type == 2 ? '<?= translate('Creates a new connection to') ?> DropBox' : 
								( type == 3 ? '<?= translate('Creates a new connection to') ?> Google Drive' :
									( type == 5 ? '<?= translate('Creates a new connection to') ?> FTP' : 
										( type == 6 ? '<?= translate('Creates a new connection to') ?> HTTP':
											( type == 7 ? '<?= translate('Creates a new connection to') ?> DB':null)
										)
								 	) 
								)
								) : null;

							return '<div>' +
								'<span class="label">' + escape(label) + '</span>' +
								(caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
								'</div>';
						}
					},
					onItemAdd: function( v ) {
						if( v == 0 ) {
							
							if( type == 3 ) {
								var windowValidate = window.open("main.php?BITAM_SECTION=eFormsDataDestinationsCollection&action=validateGoogle", 'authGoogle', "width=800,height=436");
									
								window.fnSendCode=function( data ){
									windowValidate.close();

									var aSelectize = aModal.$el.find('[name="gaccount"]')[0].selectize;

									if( aSelectize ) {
										aSelectize.addOption([{ text: data.email, value: data.id }]);
										aSelectize.refreshOptions();
										aSelectize.addItem( data.id );
										aSelectize.refreshItems();
									}

								};
							}
							if(type==2){
								var windowValidate=window.open("main.php?BITAM_SECTION=eFormsDataDestinationsCollection&action=validateDropBox", 'authDropBox', "width=800,height=436");
								window.fnSendCode=function(codeValidate){
									$.post( 'main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'validateDropBox', 'code': codeValidate} ).done(function(r) {
										var aSelectize = wizard.find('[name="dpxaccount"]')[0].selectize;

										if( aSelectize ) {
											var eMail = r;

											aSelectize.addOption([{text: eMail.email, value: eMail.id}]);
											aSelectize.refreshOptions();
											aSelectize.addItem(eMail.id);
											aSelectize.refreshItems();
										}
										windowValidate.close();
									});
								};
							}
							if( type == 5 ) {
								wizard.find('div.ftpnew').show();
								wizard.find('#ftpName').val("");
								wizard.find('#ftpServer').val("");
								wizard.find('#ftpUsername').val("");
								wizard.find('#ftpPassword').val("");
								if (wizard.find('#ftpName').val()==""){
									wizard.find('#ftpName').focus();
								}else {
									wizard.find('[name="ftpServer"]').focus();
								}
							}
							if( type == 6 ) {
								wizard.find('div.httpnew').show();
								wizard.find('#httpName').val("");
								wizard.find('#httpServer').val("");
								wizard.find('#httpUsername').val("");
								wizard.find('#httpPassword').val("");
								if (wizard.find('#httpName').val()==""){
									wizard.find('#httpName').focus();
								}else {
									wizard.find('[name="httpServer"]').focus();
								}
							}
							if( type == 7 ) {
								wizard.find('div.BDnew').show();
								wizard.find('#BDName').val("");
								wizard.find('#BDServer').val("");
								wizard.find('#BDUsername').val("");
								wizard.find('#BDPassword').val("");
								wizard.find('#Database').val("");
								if (wizard.find('#BDName').val()==""){
									wizard.find('#BDName').focus();
								}else {
									wizard.find('[name="BDServer"]').focus();
								}
							}
							
							wizard.find('[name="'+ sEl +'"]')[0].selectize.clear( true );
						}
					}
				});

				if( type == 5 ) {
					wizard.find('[name="'+ sEl +'"]').selectize().on('change', function() {
						if(wizard.find('[name="'+ sEl +'"]').val()!= "") {
							$.get(urlPath +'action.php?action=getConexion&idConnection=' + wizard.find('[name="'+ sEl +'"]').val()).done(function( response ) {
								if (response.success) {
									wizard.find('div.ftpnew').show();
									wizard.find('#ftpServer').val(response.ftp);
									wizard.find('#ftpUsername').val(response.user);
									wizard.find('#ftpPassword').val(response.pass);
									if(response.servicename){
										wizard.find('#ftpName').val(response.servicename);
									}
								}
							});
						}
					});
				}
				if( type == 6 ) {
					wizard.find('[name="'+ sEl +'"]').selectize().on('change', function() {
						if(wizard.find('[name="'+ sEl +'"]').val()!= "") {
							$.get(urlPath +'action.php?action=getHttpConexion&idConnection=' + wizard.find('[name="'+ sEl +'"]').val()).done(function( response ) {
								if (response.success) {
									wizard.find('div.httpnew').show();
									wizard.find('#httpServer').val(response.http);
									wizard.find('#httpUsername').val(response.user);
									wizard.find('#httpPassword').val(response.pass);
									wizard.find('#httpParameters').val(Parameters);
									if(response.servicename){
										wizard.find('#httpName').val(response.servicename);
									}
								}
							});
						}
					});
				}
				if( type == 7 ) {
					wizard.find('[name="'+ sEl +'"]').selectize().on('change', function() {
						if(wizard.find('[name="'+ sEl +'"]').val()!= "") {
							$.get(urlPath +'action.php?action=getTableConexion&idConnection=' + wizard.find('[name="'+ sEl +'"]').val()).done(function( response ) {
								if (response.success) {
									wizard.find('div.BDnew').show();
									wizard.find('#BDServer').val(response.http);
									wizard.find('#BDUsername').val(response.user);
									wizard.find('#BDPassword').val(response.pass);
									if (wizard.find('[name="'+ sEl +'"]').val()==dataSourceServiceConnectionID){
										wizard.find('#Database').val(SourceDatabase);
									}else {
										wizard.find('#Database').val("");
									}
									if (response.servicedbtype){
										$selectTypeDatabase[0].selectize.setValue(response.servicedbtype);
									}
									if(response.servicename){
										wizard.find('#BDName').val(response.servicename);
									}
								}
							});
						}
					});
				}

				wizard.find('[name="'+ sEl +'"]').selectize().on('change', function() {
					if (wizard.find('[name="'+ sEl +'"]').val() != '' && wizard.find('[name="'+ sEl +'"]').val() != dataSourceServiceConnectionID && isEditable) {
						isEditable = false;
						wizard.find('a[href="#finish"]').parent().hide();
					}
				});
				if (dataSourceServiceConnectionID && dataSourceServiceConnectionID != "0" && isEditable ) {
					wizard.find('[name="'+ sEl +'"]')[0].selectize.setValue(dataSourceServiceConnectionID);
					wizard.find('a[href="#finish"]').parent().show();
				}else if (type == 6 || type == 7){
					wizard.find('[name="'+ sEl +'"]')[0].selectize.setValue(dataSourceServiceConnectionID);
				}
			});

		}
		var getFile = function(){
			aModal.$el.find('.error-msg').hide();
			var data = { 
					'gaccount': wizard.find('[name="gaccount"]').val(),
					'action': 'downloadFile',
					'type': wizard.find('[name="type"]:checked').val(),
					'ftpServer': wizard.find('[name="ftpServer"]').val(),
					'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
					'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
					'file': wizard.find('[name="file"]').val() ,
					'path':  (wizard.find('[name="type"]:checked').val()=="3"?wizard.find('[name="path"]').data("idFolderDrive") : wizard.find('[name="path"]').val()),
					'ftpaccount': wizard.find('[name="ftpaccount"]').val(),
					'dpxaccount': wizard.find('[name="dpxaccount"]').val()
				};
			$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
				if( r.success ) {
					$.get(urlPath +'action.php?action=uploadCatalog&filename=' + r.file + '&type=' + wizard.find('[name="type"]:checked').val()).done(function( response ) {
						if( response.success ) {
							if (dataSourceType == "7"){
								aModal.$el.find('div.divSheetselect').hide();
								aModal.$el.find('div.divTableselect').show();		
							}else {
								aModal.$el.find('div.divSheetselect').show();
								aModal.$el.find('div.divTableselect').hide();			
							}

							file = response.file;
							aModal.$el.find('ul[role="menu"][aria-label="Pagination"]').show();
							$selectSheet[0].selectize.clearOptions();
							$selectSheet.append('<option value="">---</option>')
							for(var i = 0; i < response.sheets.length; i++) {
								$selectSheet[0].selectize.addOption({value: response.sheets[i], text: response.sheets[i]});
							}
							wizard.find(".loadingDiv").hide();
							$selectSheet[0].selectize.refreshItems();
							if (dataSourceSheet && dataSourceSheet != "" && isEditable) {
								$selectSheet[0].selectize.setValue(dataSourceSheet);
								wizard.find('a[href="#finish"]').parent().show();

							}
						} else {
							aModal.$el.find('.error-msg').html('Error: ' + response.msg);
							aModal.$el.find('.error-msg').show();
						}		
					});	
				}
			});
		}

		var createTreeFolder = function(idFolder) {
			if(idFolder==null){
				wizard.find('[name="path"]').val("/");
				var data = {
					'gaccount': wizard.find('[name="gaccount"]').val(),
					'action': 'getTreeFolder',
					'type': wizard.find('[name="type"]:checked').val(),
					'ftpServer': wizard.find('[name="ftpServer"]').val(),
					'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
					'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
					'ftpaccount': wizard.find('[name="ftpaccount"]').val(),
					'showFiles': true,
					'dpxaccount':wizard.find('[name="dpxaccount"]').val()
				};
				$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
					if (typeof r == "object") {
						wizard.find(".loadingDiv").hide();
						wizard.find('.nextclass').attr("href",'#next');
						wizard.find('a[href="#next"]').removeClass("nextclass");
						aModal.showError( r.msg );
					}else {
						wizard.find(".loadingDiv").hide();
						first = false;
						wizard.find(".selectedTree").find("ul").empty();
						wizard.find(".selectedTree").find("ul").append(r);
						wizard.find('.fileclass').on( "click", function(e) {
							//wizard.find('[name="path"]').data("idFolderDrive",this.id);
							var liElement=$(this);
							wizard.find('[name="path"]').val(liElement.find('a')[0].text);
							wizard.find('[name="file"]').val(liElement.find('a')[0].text);
							//2015-09-24@JRPP: Si se hace clic en algun documento se considera que ya cambio la información
							// motivo por el cual se esconde el boton y se cambia la variable
							if (isEditable) {
								isEditable = false;
								wizard.find('a[href="#finish"]').parent().hide();
							}
						});
						if (dataSourcePath && dataSourcePath != "" && dataSourceDocument && dataSourceDocument != "" && isEditable) {
							wizard.find('[name="path"]').val(dataSourcePath);
							wizard.find('[name="path"]').data("idFolderDrive",GoogleDriveID);
							wizard.find('[name="file"]').val(dataSourceDocument);
							wizard.find('a[href="#finish"]').parent().show();
						}				
						wizard.find(".selectedTree").find("ul").metisMenu({ toggle: false });
						wizard.steps('next');

					}

				});
				wizard.find('.metisFolder').on('beforeopen', function(e, anObject) {
					wizard.find('[name="path"]').val($(anObject.el).text()+"/");
					wizard.find('[name="file"]').val("");
					wizard.find('[name="path"]').data("idFolderDrive",anObject.el.parentElement.id);
					if($(anObject.el).parent().attr('aria-expanded')==undefined || $(anObject.el).parent().attr('aria-expanded')=="false"){
						$(anObject.el).parent().attr("aria-expanded","true");
						$(anObject.el).parent().parent().attr("aria-expanded","true");
						createTreeFolder($(anObject.el).parent().attr('id'));
					}else{
						$(anObject.el).parent().attr("aria-expanded","false");
						$(anObject.el).parent().parent().attr("aria-expanded","false");
					}
					//2015-09-24@JRPP: Si se hace clic en algun documento se considera que ya cambio la información
					// motivo por el cual se esconde el boton y se cambia la variable
					if (isEditable) {
						isEditable = false;
						wizard.find('a[href="#finish"]').parent().hide();
					}
		      	});
			}else{
				var data = {
					'gaccount': wizard.find('[name="gaccount"]').val(),
					'action': 'getTreeFolder',
					'idFolder':idFolder,
					'type': wizard.find('[name="type"]:checked').val(),
					'ftpServer': wizard.find('[name="ftpServer"]').val(),
					'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
					'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
					'ftpaccount': wizard.find('[name="ftpaccount"]').val(),
					'showFiles': true,
					'dpxaccount':wizard.find('[name="dpxaccount"]').val()
				};
				$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
					wizard.find(".selectedTree [name='"+idFolder+"'] ul").remove();
					wizard.find(".selectedTree [name='"+idFolder+"']").append("<ul class='collapse in' aria-expanded='false'>"+r+"</ul>");
					//wizard.find(".selectedTree").find(".metisFolder").metisMenu({ toggle: false });
					wizard.find(".selectedTree [name='"+idFolder+"'] li").on( "click", function(e) {
						e.stopPropagation();
						if ($(this).find('.fa-file').length>0){
							if(this.className!="fileclass")
								wizard.find('[name="path"]').data("idFolderDrive",this.id);
							
							wizard.find('[name="path"]').val("");
							var liElement=$(this);
							wizard.find('[name="file"]').val(liElement.find('a')[0].text);
							var index = 0;
							while (liElement[0].tagName!="DIV") {
								var pathFolder=wizard.find('[name="path"]').val();
								if (index==0) {
									wizard.find('[name="path"]').val(liElement.find('a')[0].text+pathFolder);
								}else {
									wizard.find('[name="path"]').val(liElement.find('a')[0].text+"/"+pathFolder);
								}
								liElement=liElement.parent().parent();
								index++;
							}
							wizard.find('ul[role="menu"][aria-label="Pagination"]').show();
							return;
						}
						if(this.id.length > 0){
							wizard.find('[name="path"]').data("idFolderDrive",this.id);
							wizard.find('[name="path"]').val("");
							var liElement=$(this);
							while (liElement[0].tagName!="DIV") {
								var pathFolder=wizard.find('[name="path"]').val();
								wizard.find('[name="path"]').val(liElement.find('a')[0].text+"/"+pathFolder);
								liElement=liElement.parent().parent();
							}
							if($(this).attr('class')==undefined || $(this).attr('class').indexOf('active')< 0){
								if(this.id.length>0){
									$(this).addClass('active');
									if(wizard.find('[name="type"]:checked').val()==5){
										$(this).attr('name',wizard.find('[name="path"]').val())
										createTreeFolder(wizard.find('[name="path"]').val());
									}else{
										createTreeFolder(this.id);
									}
								}
							}else{
								$(this).removeClass('active');
								$(this).find('ul').removeClass('in');
							}
						}
					});
					
				});
			}
		}
		var searchHttpAttributes = function() {
			wizard.find('[name="path"]').val("/");
			var data = {
				'action': 'downloadHttpAttributes',
				'type': wizard.find('[name="type"]:checked').val(),
				'httpServer': wizard.find('[name="httpServer"]').val(),
				'httpUsername': wizard.find('[name="httpUsername"]').val(),
				'httpPassword': wizard.find('[name="httpPassword"]').val(),
				'httpParameters': wizard.find('[name="httpParameters"]').val(),
				'catalogId' : id
			};
			$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
				if (r.success){
					attributesValues = r.multiaRRay;
					aModal.$el.find('div.listnewfields').empty();
					var $contenfFields = aModal.$el.find('div.fields').empty(), i = 0, options = [], shtml = '', sfhtml = '', $fieldsselectd;
					aModal.$el.find('.finishstep div.catalogname').hide();

					shtml += '<form class="form-horizontal">';

					options.push('<option value="">---</option>');

					for( i = 0; i < r.arrAtributesname.length; i++ ) {
						options.push('<option value="'+ r.arrAtributesname[i] +'">'+ r.arrAtributesname[i] +'</option>');
					}

					for( i = 0; i < r.arrdefinition.length; i++ ) {
						shtml += '<div class="form-group"><label title="'+ r.arrdefinition[i].text +'" for="'+ r.arrdefinition[i].text +'" class="col-sm-4 control-label">'+ r.arrdefinition[i].text +'</label><div class="col-sm-8"><select placeholder="'+ $tpl.find('#tSelectAttribute').html() +'" data-value="'+ r.arrdefinition[i].value +'" class="input-sm" name="'+ r.arrdefinition[i].text +'">'+ options.join('') +'</select></div></div>'
					}

					if( r.arrExcelColumns && r.arrExcelColumns.length ) {

						for( i = 0; i < r.arrExcelColumns.length; i++ ) {
							
							sfhtml += '<div class="field" data-fieldname="'+ r.arrExcelColumns[i] +'"><div>'+ r.arrExcelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>';

						}

						aModal.$el.find('div.fieldsnew').show();

						aModal.$el.find('div.listnewfields').append( sfhtml );

						$fieldsselectd = aModal.$el.find('div.listnewfields div.field');
					}

					$contenfFields.append( shtml + '<div style="clear: both;"></div>' );

					$contenfFields.find('select').each(function(i, el) {
						var $el = $(el);

						$el.find('option[value="'+ $el.attr('name') +'"]').attr('selected', true);
					})

					$contenfFields.find('select').selectize({onDropdownClose: function() {
						/*if( this.getValue() == '' )
							this.setValue( Object.keys(this.options)[0] );*/
					}});

					$fieldsselectd && $fieldsselectd.on('click', function() {
							if( this.getAttribute('data-selected') == undefined ) {
								this.setAttribute('data-selected', 'true');
								this.setAttribute('data-i', i++);
								this.classList.add('selected');
							} else {
								this.classList.remove('selected');
								this.removeAttribute('data-selected');
								this.removeAttribute('data-i');
							}
					});
					wizard.steps('goto', 5);
				}else {
					aModal.showError(r.msg);
					aModal.$el.find(".loadingDiv").hide();
				}
			});

		}
		var validateBDconnetion = function(idFolder) {
			wizard.find('[name="path"]').val("/");
			var BDServer = wizard.find('[name="BDServer"]').val();
			var BDUsername = wizard.find('[name="BDUsername"]').val();
			var BDPassword = wizard.find('[name="BDPassword"]').val();
			var Database = wizard.find('[name="Database"]').val();
			var TypeOfDatabase = $selectTypeDatabase.val();
			$.post(urlPath + 'action.php?action=validateBDconnetion', {'BDServer': BDServer, 'BDUsername': BDUsername,'BDPassword': BDPassword, 'Database' : Database, 'TypeOfDatabase': TypeOfDatabase}).done(function( r ) {
				if (r){
					if (r.success){
						first = false;
						aModal.$el.find('ul[role="menu"][aria-label="Pagination"]').show();
						$selectTable[0].selectize.clearOptions();
						$selectTable.append('<option value="">---</option>')
						for(var i = 0; i < r.arrtables.length; i++) {
							$selectTable[0].selectize.addOption({value: r.arrtables[i], text: r.arrtables[i]});
						}
						
						$selectTable[0].selectize.refreshItems();
						$selectTable[0].selectize.setValue(SourceTable);
						wizard.steps('goto', 4);
					}else {
						aModal.showError(r.msg);
						aModal.$el.find(".loadingDiv").hide();
					}
				}else {
					var error = '<?= translate('Invalid link service') ?>';
					aModal.showError(error);
					aModal.$el.find(".loadingDiv").hide();
				}
			});
		}
		aModal.on('onStepChanged', function( e, currentIndex, newIndex ) {
			var type = aModal.$el.find('[name="type"]:checked').val();
			aModal.$el.find(".loadingDiv").hide();
			if (isEditable) {
				aModal.$el.find('a[href="#finish"]').parent().show();
			} else if (currentIndex != 5){
				aModal.$el.find('a[href="#finish"]').parent().hide();
			}
			aModal.hideError();
			if (currentIndex==3) {
				first = true;
				aModal.$el.find('.nextclass').attr("href",'#next');
				aModal.$el.find('a[href="#next"]').removeClass("nextclass");
			}
			if (type == "7") {
				first = true;
				for (var i = 0; i <= 6; i++) {
					aModal.$el.find('#steps-uid-' + i + '-t-' + newIndex).css( "background", "#9dc8e2" );
					aModal.$el.find('#steps-uid-' + i + '-t-' + currentIndex).css( "background", "#2184be" );
				}
			}
  		});
		aModal.on('onStepChanging', function(e, currentIndex, newIndex ) {

			var type = aModal.$el.find('[name="type"]:checked').val(), numMapping = 0, newStep, fn;
			var bValid = true;
			var error = '<?= translate('In order to continue the information must be fully completed') ?>';
			aModal.$el.find('[id$="-p-'+ currentIndex +'"]').find('select[required], input[required]').each(function() {
				var iValid = true;

				if( this.getAttribute('requiredisnull') && aModal.$el.find('[name="'+ this.getAttribute('requiredisnull') +'"]').val() ) {
					return;
				}
				if(!$(this).val()) {
					iValid = false;
					if (type == "5" && currentIndex == 2) {
						error = '<?= translate('The connection cannot be successful. There is some missing information at the FTP service') ?>';
					}
				} else if( this.getAttribute('pattern')  ) {
					iValid = new RegExp(this.getAttribute('pattern')).test( this.value );
					if (!iValid && type == "5" && currentIndex == 2) {
						error = '<?= translate('Invalid FTP link service(ftp:\/\/your_FTP.domain_extension)') ?>';
					}
				}

				if( !iValid ) {
					bValid = false;
					if( this.tagName == 'SELECT' && this.selectize ) {
						this.selectize.$control.addClass('errorinput');
					} else if( this.tagName == 'INPUT' ) {
						this.classList.add('errorinput');
					}
				}

			});
			if (newIndex == 4 && currentIndex == 5 && type == '6'){
				wizard.steps('goto', 2);
				e.stopPropagation();
			}
			if( !bValid && currentIndex < newIndex &&  type =="5" && newIndex == 3) {
				e.stopPropagation();
				aModal.showError(error);
				return false;
			}
			if (currentIndex == 0) {
				first = true;
			}
			if( newIndex != 1 && type != "7") {
				/** Se agrego la funcion para validar los datos del scheduler */
				ScheduleFunction.validScheduler( aModal.$el.find('form.schedulerform'), e, 0 );

				if( e.isPropagationStopped() ) {
					aModal.showError( error );
					return;
				}
			}
			if (newIndex == 1 && currentIndex == 0 && type != "7") {
				if(!(aModal.$el.find('#activeCheck').prop("checked"))){
					changeRecurActive();
					return false;
				}
			}
			if (newIndex == 1 && currentIndex == 0 && type != "7") {
				if(!(aModal.$el.find('#activeCheck').prop("checked"))){
					changeRecurActive();
					return false;
				}
			}
			//@JRPP 2015-09-14
			if (newIndex == 1 && currentIndex == 0 && type == "7"){
				wizard.steps('goto', 2);
				e.stopPropagation();
				return false;
			}
			if (newIndex == 1 && currentIndex == 2 && type == "7"){
				wizard.steps('goto', 0);
				e.stopPropagation();
				return false;
			}
			if (newIndex == 3 && currentIndex == 2 && type == "7"){
				wizard.steps('goto', 4);
				e.stopPropagation();
				return false;
			}
			if (newIndex == 3 && currentIndex == 4 && type == "7"){
				wizard.steps('goto', 2);
				e.stopPropagation();
				return false;
			}
			if (newIndex == 2 && type =="3"){ // Primer paso selecciono la opcion de Google Drive
				/*2015-09-14@JRPP: Invoco funcion que carga el combobox*/
				aModal.$el.find(".httpdiv").hide(); //Se esconden las opciones de HTTP
				aModal.$el.find(".dpxdiv").hide(); //Se esconden las opciones de Dropbox
				aModal.$el.find(".ftpdiv").hide();//Se esconden las opciones de FTP
				aModal.$el.find(".BDdiv").hide();//Se esconden las opciones de BD
				aModal.$el.find(".gddiv").show(); //Se muestran las opcionde de GoogleDrive
				loadConnections( 'gaccount' );
			}else if (newIndex == 2 && type =="5") { //Primer paso selecciono la opcion de FTP
				ftpstep();
				aModal.$el.find(".httpdiv").hide(); //Se esconden las opciones de HTTP
				aModal.$el.find(".dpxdiv").hide(); //Se esconden las opciones de Dropbox
				aModal.$el.find(".gddiv").hide();//Se esconden las opciones de GoogleDrive
				aModal.$el.find(".BDdiv").hide();//Se esconden las opciones de BD
				aModal.$el.find(".ftpdiv").show(); //Se muestran las opcionde de FTP
			}else if (newIndex == 2 && type =="2") {
				aModal.$el.find(".httpdiv").hide(); //Se esconden las opciones de HTTP
				aModal.$el.find(".ftpdiv").hide(); //Se esconden las opciones de FTP
				aModal.$el.find(".gddiv").hide();//Se esconden las opciones de GoogleDrive
				aModal.$el.find(".BDdiv").hide();//Se esconden las opciones de BD
				aModal.$el.find(".dpxdiv").show(); //Se muestran las opcionde de Dropbox				
				dropbox();
			}else if (newIndex == 2 && type =="6"){
				aModal.$el.find(".dpxdiv").hide(); //Se esconden las opciones de Dropbox
				aModal.$el.find(".ftpdiv").hide(); //Se esconden las opciones de FTP
				aModal.$el.find(".gddiv").hide();//Se esconden las opciones de GoogleDrive
				aModal.$el.find(".BDdiv").hide();//Se esconden las opciones de BD
				aModal.$el.find(".httpdiv").show(); //Se muestran las opcionde de HTTP	
				httpstep();
			}else if (newIndex == 2 && type =="7"){
				aModal.$el.find(".dpxdiv").hide(); //Se esconden las opciones de Dropbox
				aModal.$el.find(".ftpdiv").hide(); //Se esconden las opciones de FTP
				aModal.$el.find(".gddiv").hide();//Se esconden las opciones de GoogleDrive
				aModal.$el.find(".httpdiv").hide(); //Se esconden las opcionde de HTTP
				aModal.$el.find(".BDdiv").show();//Se muestran las opciones de BD
				BDstep();
			}
			if(currentIndex == 2 && newIndex==3 && first && (type == "3" || type =="5" || type =="2")) {
				aModal.$el.find('a[href="#next"]').addClass("nextclass");
				aModal.$el.find('a[href="#next"]').removeAttr("href");
				aModal.$el.find(".selectedTree").find("ul").empty();
				aModal.$el.find(".loadingDiv").show();
				createTreeFolder(null);
				e.stopPropagation();

			}
			if (currentIndex == 2 && newIndex==3 && type == "6"){
				searchHttpAttributes();
				e.stopPropagation();
			}
			if (newIndex==4 && type != "7"){
				if (aModal.$el.find('[name="path"]').val() == "/" || aModal.$el.find('[name="file"]').val() == "") {
					aModal.showError(error);
					e.stopPropagation();
					return false;
				}
				$selectSheet[0].selectize.clearOptions();
				aModal.$el.find(".loadingDiv").show();
				getFile();
			}
			if (newIndex==4 && type == "7" && first){
				if (wizard.find('#BDServer').val() == "" || wizard.find('#BDUsername').val() == "" || wizard.find('#BDPassword').val() == "" || wizard.find('#Database').val() == "") {
					aModal.showError(error);
					e.stopPropagation();
					return false;
				}
				aModal.$el.find(".loadingDiv").show();
				validateBDconnetion();
				e.stopPropagation();
				return false;
			}
			if (newIndex==5) {
				if ($selectSheet[0].selectize.getValue() == ""  && type !="6" && type !="7"){
					aModal.showError(error);
					e.stopPropagation();
					return false;
				}
			}
			if (newIndex==5 && type == "7") {
				if ($selectTable[0].selectize.getValue() == ""){
					aModal.showError(error);
					e.stopPropagation();
					return false;
				}
			}

		});
		$selectSheet = aModal.$el.find('select#sheets');
		$selectTable = aModal.$el.find('select#selectTable');
		$selectTypeDatabase = aModal.$el.find('select#typeDatabase');

		require(['selectize'], function(a) {
			window.renderButtonsGoogle = function() {
				gapi.load('auth2', function() {
					gapi.auth2.getAuthInstance() || gapi.auth2.init({
						client_id: '<?= $gInfoClient->web->client_id; ?>',
						scope: 'https://www.googleapis.com/auth/drive'
					});
				});
				loadConnections( 'gaccount' );
			}
			if (CaptureStartTime_Hours>=10) {
				CaptureStartTime_Hours = CaptureStartTime_Hours.toString();
			} else {
				CaptureStartTime_Hours = "0" + CaptureStartTime_Hours.toString();
			}
			if (CaptureStartTime_Minutes>=10) {
				CaptureStartTime_Minutes = CaptureStartTime_Minutes.toString();
			} else {
				CaptureStartTime_Minutes = "0" + CaptureStartTime_Minutes.toString();
			}

			var data = {'RecurPatternNum': parseInt(RecurPatternNum),'RecurPatternOpt': RecurPatternOpt,'RecurPatternType': parseInt(RecurPatternType), 'RecurRangeOpt': RecurRangeOpt, 'Interval': Interval, 'CaptureStartTime_Hours':CaptureStartTime_Hours,'CaptureStartTime_Minutes':CaptureStartTime_Minutes, 'captureStartTime':CaptureStartTime_Hours + ':' + CaptureStartTime_Minutes};
			ScheduleFunction.schedulerLogic( aModal.$el.find('section.scheduler'), data );
			$selectSheet.selectize().on('change', function() {
				if ($selectSheet.val() != '' && $selectSheet.val() != dataSourceSheet){
					isEditable = false;
					aModal.$el.find('a[href="#finish"]').parent().hide();
				}
				<?//@JAPR 2016-07-19: Corregido un bug, no se estaban pasando correctamente los parámetros al utilizar nombres con ciertos caracteres (#EIK630) ?>
				$selectSheet.val() != '' && $.get(urlPath +'action.php?action=loadFields&sheet=' + encodeURIComponent($selectSheet.val()) + '&file=' + encodeURIComponent(file) + '&id=' + id + '&type=' + aModal.$el.find('[name="type"]:checked').val()).done(function( r ) {
					aModal.$el.find('div.listnewfields').empty();
					if( r.success ) {
						selectedSheet = $selectSheet.val();

						aModal.$el.find('input[name="catalogName"]').val( selectedSheet.replace(/\$$/, '') );

						aModal.$el.find('div.fieldsnew').hide();

						var $contenfFields = aModal.$el.find('div.fields').empty(), i = 0, options = [], shtml = '', sfhtml = '', $fieldsselectd;
						
						if( r.definition.length ) {

							aModal.$el.find('.finishstep div.catalogname').hide();

							shtml += '<form class="form-horizontal">';
							
							options.push('<option value="">---</option>');

							for( i = 0; i < r.excelColumns.length; i++ ) {
								options.push('<option value="'+ r.excelColumns[i] +'">'+ r.excelColumns[i] +'</option>');
							}
							
							for( i = 0; i < r.definition.length; i++ ) {
								shtml += '<div class="form-group"><label title="'+ r.definition[i].text +'" for="'+ r.definition[i].text +'" class="col-sm-4 control-label">'+ r.definition[i].text +'</label><div class="col-sm-8"><select placeholder="'+ $tpl.find('#tSelectAttribute').html() +'" data-value="'+ r.definition[i].value +'" class="input-sm" name="'+ r.definition[i].text +'">'+ options.join('') +'</select></div></div>'
							}

							if( r.arrExcelColumns && r.arrExcelColumns.length ) {

								for( i = 0; i < r.arrExcelColumns.length; i++ ) {
									
									sfhtml += '<div class="field" data-fieldname="'+ r.arrExcelColumns[i] +'"><div>'+ r.arrExcelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>';

								}

								aModal.$el.find('div.fieldsnew').show();

								aModal.$el.find('div.listnewfields').append( sfhtml );

								$fieldsselectd = aModal.$el.find('div.listnewfields div.field');
							}
							
							$contenfFields.append( shtml + '<div style="clear: both;"></div>' );

							$contenfFields.find('select').each(function(i, el) {
								var $el = $(el);

								$el.find('option[value="'+ $el.attr('name') +'"]').attr('selected', true);
							})
							
							$contenfFields.find('select').selectize({onDropdownClose: function() {
								/*if( this.getValue() == '' )
									this.setValue( Object.keys(this.options)[0] );*/
							}});
							
						} else {
							for( var i = 0; i < r.excelColumns.length; i++ ) {
								$contenfFields.append('<div class="field" data-fieldname="'+ r.excelColumns[i] +'"><div>'+ r.excelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>')
							}

							/***/
							$fieldsselectd = $contenfFields.find('div.field');
						}

						$fieldsselectd && $fieldsselectd.on('click', function() {
							if( this.getAttribute('data-selected') == undefined ) {
								this.setAttribute('data-selected', 'true');
								this.setAttribute('data-i', i++);
								this.classList.add('selected');
							} else {
								this.classList.remove('selected');
								this.removeAttribute('data-selected');
								this.removeAttribute('data-i');
							}
						});						
					} else if( r.msg != '' ) {
						alert( r.msg );
					}
				});
			});
			//@JRPP 2015-12-28 Se agrega funcion para el combo de tipo table
			$selectTable.selectize().on('change', function() {
				var BDServer = wizard.find('[name="BDServer"]').val();
				var BDUsername = wizard.find('[name="BDUsername"]').val();
				var BDPassword = wizard.find('[name="BDPassword"]').val();
				var Database = wizard.find('[name="Database"]').val();
				var Table = $selectTable.val();
				var TypeOfDatabase = $selectTypeDatabase.val();
				$selectTable.val() != '' && $.post(urlPath + 'action.php?action=getColumnsFromTable', {'BDServer': BDServer, 'BDUsername': BDUsername,'BDPassword': BDPassword, 'Database' : Database, 'Table' : Table, 'id' : id , 'TypeOfDatabase': TypeOfDatabase}).done(function( r ) {
					aModal.$el.find('div.listnewfields').empty();
					if( r.success ) {
						selectedSheet = $selectTable.val();

						aModal.$el.find('input[name="catalogName"]').val( selectedSheet.replace(/\$$/, '') );

						aModal.$el.find('div.fieldsnew').hide();

						var $contenfFields = aModal.$el.find('div.fields').empty(), i = 0, options = [], shtml = '', sfhtml = '', $fieldsselectd;
						
						if( r.definition.length ) {

							aModal.$el.find('.finishstep div.catalogname').hide();

							shtml += '<form class="form-horizontal">';
							
							options.push('<option value="">---</option>');

							for( i = 0; i < r.arrColumns.length; i++ ) {
								options.push('<option value="'+ r.arrColumns[i] +'">'+ r.arrColumns[i] +'</option>');
							}
							
							for( i = 0; i < r.definition.length; i++ ) {
								shtml += '<div class="form-group"><label title="'+ r.definition[i].text +'" for="'+ r.definition[i].text +'" class="col-sm-4 control-label">'+ r.definition[i].text +'</label><div class="col-sm-8"><select placeholder="'+ $tpl.find('#tSelectAttribute').html() +'" data-value="'+ r.definition[i].value +'" class="input-sm" name="'+ r.definition[i].text +'">'+ options.join('') +'</select></div></div>'
							}

							if( r.excelColumns && r.excelColumns.length ) {

								for( i = 0; i < r.excelColumns.length; i++ ) {
									
									sfhtml += '<div class="field" data-fieldname="'+ r.excelColumns[i] +'"><div>'+ r.excelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>';

								}

								aModal.$el.find('div.fieldsnew').show();

								aModal.$el.find('div.listnewfields').append( sfhtml );

								$fieldsselectd = aModal.$el.find('div.listnewfields div.field');
							}
							
							$contenfFields.append( shtml + '<div style="clear: both;"></div>' );

							$contenfFields.find('select').each(function(i, el) {
								var $el = $(el);

								$el.find('option[value="'+ $el.attr('name') +'"]').attr('selected', true);
							})
							
							$contenfFields.find('select').selectize({onDropdownClose: function() {
								/*if( this.getValue() == '' )
									this.setValue( Object.keys(this.options)[0] );*/
							}});
							
						}

						$fieldsselectd && $fieldsselectd.on('click', function() {
							if( this.getAttribute('data-selected') == undefined ) {
								this.setAttribute('data-selected', 'true');
								this.setAttribute('data-i', i++);
								this.classList.add('selected');
							} else {
								this.classList.remove('selected');
								this.removeAttribute('data-selected');
								this.removeAttribute('data-i');
							}
						});						
					} else if( r.msg != '' ) {
						alert( r.msg );
					}
				});
			});
			$selectTypeDatabase.selectize().on('change', function() {
				//debugger;
			});
		});

		return dfd.promise();
		
	};

});
