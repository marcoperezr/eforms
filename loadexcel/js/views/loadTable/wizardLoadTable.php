﻿<?php
	$chdirCurrent = getcwd ();
	chdir('../../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);	
?>
define([
	'lodash',
	'backbone',
	'text!templates/loadTable/wizardLoadTable.php',
	'views/modal/wizardModal',
	'javascript/eBavel/date',
	'libs/misc/SimpleAjaxUploader.min',
	'libs/selectize.js/dist/js/standalone/selectize',
	'bootstrap',
	'libs/metisMenu/src/metisMenu'
], function(_, Backbone, Template, WizardModal){
	
	var wizard, aModal, $select, urlPath = 'loadexcel/';
	attributesValues = [];
	first = true;
	/** Recibe el ID de la definicion del modelo de Artus a cargar, si esta vacio crea uno nuevo */
	var serializeJSON = function( form ) {
		var resData = {};
		
		var json = {};
		
		jQuery.map(form.serializeArray(), function(n, i) {
			if(json[n['name']]) {
				if($.isArray(json[n['name']])) {
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = new Array(json[n['name']]);
					json[n['name']].push(n['value']);
				}
			}
			else {
				if($('#' + n['name']).attr('multiple') == 'multiple') {
					json[n['name']] = new Array();
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = n['value'];
				}
			}
		});

		resData = json;

		return resData;	
	}
	return function( id ) {
		var file, selectedSheet, dfd = $.Deferred(), bFinish = false, $tpl = $(_.template(Template)( {} ));
		
		/** Testing*/
		//id = 1;
		
		aModal = new WizardModal({
			modalTitle: (id ? $tpl.find('#tLoadExcel').html() : $tpl.find('#tLoadExcel').html()),
			className: 'material'
		});

		$('body').append(aModal.render().$el);		
		aModal.width(700);	
		aModal.$el.find('.modal-body').html($.t('Loading') + '...');
		/** Agregamos el contenido para la ventana modal tipo wizard */
		wizard = $tpl;
		aModal.setContent(wizard);

		aModal.$el.find('a[href="#previous"]').html( $tpl.find('#tPrevious').html() );
		aModal.$el.find('a[href="#next"]').html( $tpl.find('#tNext').html() );
		aModal.$el.find('a[href="#finish"]').html( $tpl.find('#tFinish').html() );
		
		/** Al terminar mandamos la informacion de la hoja de excel cargada y los campos seleccionados */
		aModal.on('onFinished', function(res) {
			var $fields = aModal.$el.find('div.fields div.field[data-selected="true"]'), arrFields = [], arrNewFields = [], catalogName = aModal.$el.find('input[name="catalogName"]').val();
			if( id == undefined && $fields.length == 0 || $.trim(catalogName) == '' ) {
				res.result = false;
				
				if( $.trim(catalogName) == '' ) {
					aModal.showError( $tpl.find('#tInvalidname').html() );
				} else {
					aModal.showError( $tpl.find('#tInvalidfields').html() );
				}

				return;
			} else if ( $fields.length == 0 ) {
				aModal.$el.find('div.fields select').each(function(i, el) {
					arrFields.push({ columntable: el.getAttribute('data-value'), columnexcel: el.value });
				});

				$fields = aModal.$el.find('div.fieldsnew div.field[data-selected="true"]');
			}

			$fields.each(function(i, el) {
				
				if( id == undefined )
					arrFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
				else
					arrNewFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
			});

			if( $fields.length ) {
				arrFields && arrFields.sort(function(a, b) {
					return a.i - b.i;
				});

				arrNewFields && arrNewFields.sort(function(a, b) {
					return a.i - b.i;
				});
			}

			aModal.$el.find('section.finishstep .mappingselect').hide();

			aModal.$el.find('section.finishstep .creatingtables').show();
			var BDName = wizard.find('[name="BDName"]').val();
			var BDServer = wizard.find('[name="httpServer"]').val();
			var BDUsername = wizard.find('[name="httpUsername"]').val();
			var BDPassword = wizard.find('[name="httpPassword"]').val();
			var Database = wizard.find('[name="httpParameters"]').val();
			var Table = $select.val();
			var TypeOfDatabase = $selectTypeDatabase.val();
			!bFinish && $.post(urlPath + 'action.php?action=processExcel', {'fields': arrFields, 'arrNewFields': arrNewFields, 'id': id, 'sheet': selectedSheet, 'catalogName': catalogName, 'type': "7", 'BDServer': BDServer, 'BDUsername': BDUsername, 'BDPassword': BDPassword, 'Database': Database, 'Table': Table, 'TypeOfDatabase': TypeOfDatabase, 'BDName': BDName}).done(function( r ) {
				aModal.hideError();
				bFinish = false;
				if( r.success ) {
					dfd.resolve( r.id, catalogName );
					aModal.remove();
				}
				else {
					aModal.$el.find('section.finishstep .creatingtables').hide();
					aModal.$el.find('section.finishstep .mappingselect').show();
					aModal.showError( r.msg );
					//dfd.reject();
				}
			});

			bFinish = true;
			
		});
		aModal.on('onStepChanged', function(o, a, b) {
			aModal.hideError();
			aModal.$el.find(".loadingDiv").hide();
			if (a==1 && b == 0) {
				first = true;
			}
		});
		aModal.on('onStepChanging', function(o, a, b) {
			var error = '<?= translate('In order to continue the information must be fully completed') ?>';
			var bValid = true;
			//@MAPR 2016-07-26: Se reinician los input para que no permanezcan marcados 
			wizard.find('[name="httpServer"]').removeClass('errorinput');
			wizard.find('[name="httpUsername"]').removeClass('errorinput');
			wizard.find('[name="httpPassword"]').removeClass('errorinput');
			wizard.find('[name="httpParameters"]').removeClass('errorinput');

			if( b == 0 ) {
				aModal.$el.find('a[href="#next"]').parent().show();
			}
			if(b==2 && a == 1 && aModal.$el.find('#tables').val() == '') {
				aModal.showError(error);
				o.stopPropagation();
				return false;
			}
			
			if(b==1 && a==0 && (wizard.find('[name="httpServer"]').val() == '' || wizard.find('[name="httpUsername"]').val() == '' || wizard.find('[name="httpPassword"]').val() == '' || wizard.find('[name="httpParameters"]').val() == '')) {
			   //@MAPR 2016-07-26: Si no hay nada en el input se marca como error
			   if (wizard.find('[name="httpServer"]').val() == ''){
				   	wizard.find('[name="httpServer"]').addClass('errorinput');
			   }
				aModal.showError(error);
				o.stopPropagation();
				return false;
			}else if(b==1 && a==0 && first){
				aModal.$el.find(".loadingDiv").show();
				validateBDconnetion();
				o.stopPropagation();
			}
		});
		
		$select = aModal.$el.find('select#tables');
		$selectBD = aModal.$el.find('select#bdselector');
		$selectTypeDatabase = aModal.$el.find('select#typeDatabase');

		
		/*
		$select = aModal.$el.find('select#sheets');
		$selectFTP = aModal.$el.find('select#ftpselector');
		*/
		require(['selectize'], function(a) {
			$select.selectize().on('change', function() {
				var BDName = wizard.find('[name="BDName"]').val();
				var BDServer = wizard.find('[name="httpServer"]').val();
				var BDUsername = wizard.find('[name="httpUsername"]').val();
				var BDPassword = wizard.find('[name="httpPassword"]').val();
				var Database = wizard.find('[name="httpParameters"]').val();
				var Table = $select.val();
				var TypeOfDatabase = $selectTypeDatabase.val();
				$select.val() != '' && $.post(urlPath + 'action.php?action=getColumnsFromTable', {'BDServer': BDServer, 'BDUsername': BDUsername,'BDPassword': BDPassword, 'Database' : Database, 'Table' : Table , 'TypeOfDatabase': TypeOfDatabase, 'BDName': BDName}).done(function( r ) {
					if( r.success ) {

						selectedSheet = $select.val();

						aModal.$el.find('input[name="catalogName"]').val( selectedSheet.replace(/\$$/, '') );

						aModal.$el.find('div.fieldsnew').hide();

						var $contenfFields = aModal.$el.find('div.fields').empty(), i = 0, options = [], shtml = '', sfhtml = '', $fieldsselectd;
						

						for( var i = 0; i < r.arrColumns.length; i++ ) {
							$contenfFields.append('<div class="field" data-fieldname="'+ r.arrColumns[i] +'"><div>'+ r.arrColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>')
						}

						$fieldsselectd = $contenfFields.find('div.field');

						$fieldsselectd && $fieldsselectd.on('click', function() {
							if( this.getAttribute('data-selected') == undefined ) {
								this.setAttribute('data-selected', 'true');
								this.setAttribute('data-i', i++);
								this.classList.add('selected');
							} else {
								this.classList.remove('selected');
								this.removeAttribute('data-selected');
								this.removeAttribute('data-i');
							}
						});						
					} else if( r.msg != '' ) {
						alert( r.msg );
					}
				});
			});
			$selectTypeDatabase.selectize().on('change', function() {
				
			});
			var bdstep = function() {		
				loadConnections( 'BDaccount' );
			}
			var loadConnections = function( sEl ) {
				var type = 7;
				$.get('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'getInfoSrvConnection', type: type}).done(function( r ) {
					aModal.$el.find('[name="'+ sEl +'"]').selectize({
						options: [ {text: '<?= translate( '(New connection)' ) ?>', value: 0} ].concat( r ),
						render: {
							option: function(item, escape) {
								var label = item.text
								var caption = item.value == 0 ? ( type == 2 ? '<?= translate('Creates a new connection to') ?> DropBox' : 
								( type == 3 ? '<?= translate('Creates a new connection to') ?> Google Drive' :
									( type == 5 ? '<?= translate('Creates a new connection to') ?> FTP' : 
										( type == 6 ? '<?= translate('Creates a new connection to') ?> HTTP':
											( type == 7 ? '<?= translate('Creates a new connection to') ?> DB':null)
										)
								 	) 
								)
								) : null;

								return '<div>' +
									'<span class="label">' + escape(label) + '</span>' +
									(caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
									'</div>';
							}
						},
						onItemAdd: function( v ) {
							if( v == 0 ) {
								aModal.$el.find('div.ftpnew').show();
								aModal.$el.find('#BDName').val("");
								aModal.$el.find('#httpServer').val("");
								aModal.$el.find('#httpUsername').val("");
								aModal.$el.find('#httpPassword').val("");
								if (wizard.find('#BDName').val()==""){
									wizard.find('#BDName').focus();
								}else {
									wizard.find('[name="httpServer"]').focus();
								}
								aModal.$el.find('[name="'+ sEl +'"]')[0].selectize.clear( true );
							}
						}
					});
					aModal.$el.find('[name="'+ sEl +'"]').selectize().on('change', function() {
						if(aModal.$el.find('[name="'+ sEl +'"]').val()!= "") {
							$.get(urlPath +'action.php?action=getTableConexion&idConnection=' + aModal.$el.find('[name="'+ sEl +'"]').val()).done(function( response ) {
								if (response.success) {
									aModal.$el.find('div.ftpnew').show();
									aModal.$el.find('#httpServer').val(response.http);
									aModal.$el.find('#httpUsername').val(response.user);
									aModal.$el.find('#httpPassword').val(response.pass);
									if (response.servicedbtype){
										$selectTypeDatabase[0].selectize.setValue(response.servicedbtype);
									}
									if(response.servicename){
										wizard.find('#BDName').val(response.servicename);
									}
								}
							});
						}
					});
				});
			}
			bdstep();
		});
		var validateBDconnetion = function(idFolder) {
			wizard.find('[name="path"]').val("/");
			var BDName = wizard.find('[name="BDName"]').val();
			var BDServer = wizard.find('[name="httpServer"]').val();
			var BDUsername = wizard.find('[name="httpUsername"]').val();
			var BDPassword = wizard.find('[name="httpPassword"]').val();
			var Database = wizard.find('[name="httpParameters"]').val();
			var TypeOfDatabase = $selectTypeDatabase.val();

			$.post(urlPath + 'action.php?action=validateBDconnetion', {'BDServer': BDServer, 'BDUsername': BDUsername,'BDPassword': BDPassword, 'Database' : Database, 'TypeOfDatabase': TypeOfDatabase, 'BDName': BDName}).done(function( r ) {
				if (r){
					if (r.success){
						first = false;
						aModal.$el.find('ul[role="menu"][aria-label="Pagination"]').show();
						$select[0].selectize.clearOptions();
						$select.append('<option value="">---</option>')
						for(var i = 0; i < r.arrtables.length; i++) {
							$select[0].selectize.addOption({value: r.arrtables[i], text: r.arrtables[i]});
						}
						
						$select[0].selectize.refreshItems();
						wizard.steps('goto', 1);
					}else {
						//@MAPR 2016-07-26: Se señala el input que sea necesario de acuerdo al error.
						switch(r.msg){
							case 'host':
								wizard.find('[name="httpServer"]').addClass('errorinput');
								break;
							case 'database':
								wizard.find('[name="httpParameters"]').addClass('errorinput');
								break;
							case 'user':
								wizard.find('[name="httpUsername"]').addClass('errorinput');
								wizard.find('[name="httpPassword"]').addClass('errorinput');
								break;
						}
						//@MAPR 2016-07-26: Mensaje de error genérico.
						var error = "<?=translate('Error found during data validation')?>";
						aModal.showError(error);
						aModal.$el.find(".loadingDiv").hide();
					}
				}else {
					var error = '<?= translate('Invalid link service') ?>';
					aModal.showError(error);
					aModal.$el.find(".loadingDiv").hide();
				}
			});
		}
		return dfd.promise();
	};

});
