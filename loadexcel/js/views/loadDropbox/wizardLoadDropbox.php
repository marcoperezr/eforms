﻿<?php
	$chdirCurrent = getcwd ();
	chdir('../../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);	
?>
define([
	'lodash',
	'backbone',
	'text!templates/loadDropbox/wizardDropbox.php',
	'views/modal/wizardModal',
	'text!templates2/agendas/newscheduler.php',
	'javascript/views/agendas/schedulerlogic',
	'javascript/eBavel/date',
	'libs/misc/SimpleAjaxUploader.min',
	'libs/selectize.js/dist/js/standalone/selectize',
	'bootstrap',
	'libs/metisMenu/src/metisMenu'
], function(_, Backbone, Template, WizardModal, Template2, ScheduleFunction){
	
	var wizard, aModal, $select, urlPath = 'loadexcel/';
	first = true;
	/** Recibe el ID de la definicion del modelo de Artus a cargar, si esta vacio crea uno nuevo */
	var serializeJSON = function( form ) {
		var resData = {};
		
		var json = {};
		
		jQuery.map(form.serializeArray(), function(n, i) {
			if(json[n['name']]) {
				if($.isArray(json[n['name']])) {
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = new Array(json[n['name']]);
					json[n['name']].push(n['value']);
				}
			}
			else {
				if($('#' + n['name']).attr('multiple') == 'multiple') {
					json[n['name']] = new Array();
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = n['value'];
				}
			}
		});

		resData = json;

		return resData;	
	}
	return function( id ) {
		var file, selectedSheet, dfd = $.Deferred(), bFinish = false, $tpl = $(_.template(Template)( {} )),$scheduler = $(_.template( Template2 )({}));
		
		/** Testing*/
		//id = 1;
		
		aModal = new WizardModal({
			modalTitle: (id ? $tpl.find('#tLoadExcel').html() : $tpl.find('#tLoadExcel').html()),
			className: 'material'
		});

		$('body').append(aModal.render().$el);		
		aModal.width(700);	
		aModal.$el.find('.modal-body').html($.t('Loading') + '...');
		$scheduler.find('section.scheduler');
		$scheduler = $scheduler.find('section.scheduler');
		$scheduler.find('div.range_of_recurrence').append('<div style="clear: both;margin-top: 18px;"><label class="pull-left"><?= translate('Minutes') ?>&nbsp;</label><input required name="Interval" min="0" class="form-control" style="width: 70px;font-size: 13px;" type="number"></input></div>');
		$scheduler.find('<?php if (getMDVersion() >= esvDataSourceAgentHours) echo ".form-group.generate_next_day"; else echo ".form-group.capture_start_time, .form-group.generate_next_day"; ?>').remove();
		$scheduler.appendTo($tpl.find('#vacio'));
		var $stepscheduler = $tpl.find('.scheduler').detach();
		$tpl.find('#vacio').replaceWith($stepscheduler);
		/** Agregamos el contenido para la ventana modal tipo wizard */
		wizard = $tpl;
		aModal.setContent(wizard);
		//2015-09-17@JRPP: Se agrega un formulario en la seccion final de scheduler
		aModal.$el.find('section.scheduler').append("<form class = 'schedulerform'></form>");
		aModal.$el.find('form.schedulerform').append( aModal.$el.find('section.scheduler > div').detach() );

		aModal.$el.find('a[href="#previous"]').html( $tpl.find('#tPrevious').html() );
		aModal.$el.find('a[href="#next"]').html( $tpl.find('#tNext').html() );
		aModal.$el.find('a[href="#finish"]').html( $tpl.find('#tFinish').html() );
		
		/** Al terminar mandamos la informacion de la hoja de excel cargada y los campos seleccionados */
		aModal.on('onFinished', function(res) {
			var $fields = aModal.$el.find('div.fields div.field[data-selected="true"]'), arrFields = [], arrNewFields = [], catalogName = aModal.$el.find('input[name="catalogName"]').val();
			var dataScheduler = serializeJSON(aModal.$el.find('section.scheduler form').filter('form.schedulerform'));
			if( id == undefined && $fields.length == 0 || $.trim(catalogName) == '' ) {
				res.result = false;
				
				if( $.trim(catalogName) == '' ) {
					aModal.showError( '<?= translate( 'Invalid catalog name' ) ?>' );
				} else {
					aModal.showError( '<?= translate( 'Invalid fields' ) ?>' );
				}

				return;
			} else if ( $fields.length == 0 ) {
				aModal.$el.find('div.fields select').each(function(i, el) {
					arrFields.push({ columntable: el.getAttribute('data-value'), columnexcel: el.value });
				});

				$fields = aModal.$el.find('div.fieldsnew div.field[data-selected="true"]');
			}

			$fields.each(function(i, el) {
				
				if( id == undefined )
					arrFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
				else
					arrNewFields.push({ name: el.getAttribute('data-fieldname'), i: el.getAttribute('data-i') });
			});

			if( $fields.length ) {
				arrFields && arrFields.sort(function(a, b) {
					return a.i - b.i;
				});

				arrNewFields && arrNewFields.sort(function(a, b) {
					return a.i - b.i;
				});
			}

			aModal.$el.find('section.finishstep .mappingselect').hide();

			aModal.$el.find('section.finishstep .creatingtables').show();
			var dpxAccount = aModal.$el.find('[name="dpxaccount"]').val();
			var filename = aModal.$el.find('[name="file"]').val();
			var dropboxpath = aModal.$el.find('[name="path"]').val();
			!bFinish && $.post(urlPath + 'action.php?action=processExcel', {'fields': arrFields, 'arrNewFields': 'arrNewFields', 'file': file, 'id': id, 'sheet': selectedSheet, 'catalogName': catalogName, 'type': "2", 'filename':filename, 'Path':dropboxpath, 'Account' : dpxAccount}).done(function( r ) {
				
				aModal.hideError();

				bFinish = false;
				if( r.success ) {
					$.post(urlPath + 'action.php', {action: 'saveScheduler', id: r.id, data: dataScheduler}).done(function( r ) {
						debugger;
					});
					dfd.resolve( r.id, catalogName );
					aModal.remove();
				}
				else {
					aModal.$el.find('section.finishstep .creatingtables').hide();
					aModal.$el.find('section.finishstep .mappingselect').show();
					aModal.showError( r.msg );
					//dfd.reject();
				}
			});

			bFinish = true;
			
		});
		aModal.on('onStepChanged', function(o, a, b) {
			aModal.hideError();
			if (a==2) {
				first = true;
				aModal.$el.find('.nextclass').attr("href",'#next');
				aModal.$el.find('a[href="#next"]').removeClass("nextclass");
			}
		});
		aModal.on('onStepChanging', function(o, a, b) {
			var error = '<?= translate('In order to continue the information must be fully completed') ?>';
			if( b != 0 ) {
				/** Se agrego la funcion para validar los datos del scheduler */
				debugger;
				ScheduleFunction.validScheduler( aModal.$el.find('form.schedulerform'), o, 0 );

				if( o.isPropagationStopped() ) {
					aModal.showError( error );
					return;
				}
			}
			if (a == 0) {
				first = true;
			}

			if( b == 0 ) {
				aModal.$el.find('a[href="#next"]').parent().show();
			}
			if(b==4 && a == 3 && aModal.$el.find('#sheets').val() == '') {
				aModal.showError(error);
				o.stopPropagation();
				return false;
			}
			if(b==2 && a==1 && first){
				aModal.$el.find('a[href="#next"]').addClass("nextclass");
				aModal.$el.find('a[href="#next"]').removeAttr("href");
				aModal.$el.find(".selectedTree").find("ul").empty();
				aModal.$el.find(".loadingDiv").show();
				createTreeFolder(null);
				o.stopPropagation();
			}
			if(b==3 && a == 2){
				if (aModal.$el.find('[name="path"]').val() == "/" || aModal.$el.find('[name="file"]').val() == "") {
					aModal.showError( error );
					o.stopPropagation();
					return false;
				}
				getFile();
			}
		});
		
		$select = aModal.$el.find('select#sheets');
		$selectFTP = aModal.$el.find('select#ftpselector');

		require(['selectize'], function(a) {
			var data = {};
			ScheduleFunction.schedulerLogic( aModal.$el.find('section.scheduler'), data );
			$select.selectize().on('change', function() {
				<?//@JAPR 2016-07-19: Corregido un bug, no se estaban pasando correctamente los parámetros al utilizar nombres con ciertos caracteres (#EIK630) ?>
				$select.val() != '' && $.get(urlPath +'action.php?action=loadFields&sheet=' + encodeURIComponent($select.val()) + '&file=' + encodeURIComponent(file) + '&id=' + id + '&type=2').done(function( r ) {
					if( r.success ) {

						selectedSheet = $select.val();

						aModal.$el.find('input[name="catalogName"]').val( selectedSheet.replace(/\$$/, '') );

						aModal.$el.find('div.fieldsnew').hide();

						var $contenfFields = aModal.$el.find('div.fields').empty(), i = 0, options = [], shtml = '', sfhtml = '', $fieldsselectd;
						
						if( r.definition.length ) {

							aModal.$el.find('.finishstep div.catalogname').hide();

							shtml += '<form class="form-horizontal">';
							
							options.push('<option value="">---</option>');

							for( i = 0; i < r.excelColumns.length; i++ ) {
								options.push('<option value="'+ r.excelColumns[i] +'">'+ r.excelColumns[i] +'</option>');
							}
							
							for( i = 0; i < r.definition.length; i++ ) {
								shtml += '<div class="form-group"><label title="'+ r.definition[i].text +'" for="'+ r.definition[i].text +'" class="col-sm-4 control-label">'+ r.definition[i].text +'</label><div class="col-sm-8"><select placeholder="'+ $tpl.find('#tSelectAttribute').html() +'" data-value="'+ r.definition[i].value +'" class="input-sm" name="'+ r.definition[i].text +'">'+ options.join('') +'</select></div></div>'
							}

							if( r.arrExcelColumns && r.arrExcelColumns.length ) {

								for( i = 0; i < r.arrExcelColumns.length; i++ ) {
									
									sfhtml += '<div class="field" data-fieldname="'+ r.arrExcelColumns[i] +'"><div>'+ r.arrExcelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>';

								}

								aModal.$el.find('div.fieldsnew').show();

								aModal.$el.find('div.listnewfields').append( sfhtml );

								$fieldsselectd = aModal.$el.find('div.listnewfields div.field');
							}
							
							$contenfFields.append( shtml + '<div style="clear: both;"></div>' );

							$contenfFields.find('select').each(function(i, el) {
								var $el = $(el);

								$el.find('option[value="'+ $el.attr('name') +'"]').attr('selected', true);
							})
							
							$contenfFields.find('select').selectize({onDropdownClose: function() {
								/*if( this.getValue() == '' )
									this.setValue( Object.keys(this.options)[0] );*/
							}});
							
						} else {
							for( var i = 0; i < r.excelColumns.length; i++ ) {
								$contenfFields.append('<div class="field" data-fieldname="'+ r.excelColumns[i] +'"><div>'+ r.excelColumns[i] +'</div><i class="icon-check"></i><i class="icon-check-empty"></i></div>')
							}

							/***/
							$fieldsselectd = $contenfFields.find('div.field');
						}

						$fieldsselectd && $fieldsselectd.on('click', function() {
							if( this.getAttribute('data-selected') == undefined ) {
								this.setAttribute('data-selected', 'true');
								this.setAttribute('data-i', i++);
								this.classList.add('selected');
							} else {
								this.classList.remove('selected');
								this.removeAttribute('data-selected');
								this.removeAttribute('data-i');
							}
						});						
					} else if( r.msg != '' ) {
						alert( r.msg );
					}
				});
			});
			var ftpstep = function() {		
				loadConnections( 'dpxaccount' );
			}
			var loadConnections = function( sEl ) {
				var type = 2;
				$.get('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'getInfoSrvConnection', type: type}).done(function( r ) {
					aModal.$el.find('[name="'+ sEl +'"]').selectize({
						options: [ {text: '<?= translate( '(New connection)' ) ?>', value: 0} ].concat( r ),
						render: {
							option: function(item, escape) {
								var label = item.text
								var caption = item.value == 0 ? ( type == 2 ? 'Crea una nueva conexion a DropBox' : 
									( type == 3 ? 'Crea una nueva conexion a Google Drive' :
										( type == 5 ? 'Crea una nueva conexion a FTP' : null )
									 ) ) : null;

								return '<div>' +
									'<span class="label">' + escape(label) + '</span>' +
									(caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
									'</div>';
							}
						},
						onItemAdd: function( v ) {
							if( v == 0 ) {
								var windowValidate=window.open("main.php?BITAM_SECTION=eFormsDataDestinationsCollection&action=validateDropBox", 'authDropBox', "width=800,height=436");
								window.fnSendCode=function(codeValidate){
									$.post( 'main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'validateDropBox', 'code': codeValidate} ).done(function(r) {
										var aSelectize = aModal.$el.find('[name="dpxaccount"]')[0].selectize;

										if( aSelectize ) {
											var eMail = r;

											aSelectize.addOption([{text: eMail.email, value: eMail.id}]);
											aSelectize.refreshOptions();
											aSelectize.addItem(eMail.id);
											aSelectize.refreshItems();
										}
										windowValidate.close();
									});
								};
								aModal.$el.find('[name="'+ sEl +'"]')[0].selectize.clear( true );
							}
						}
					});
/*
					if( infoDestination.connection ) {
						selectConnection( sEl, infoDestination.connection.name );
					}
*/
				});
			}
			ftpstep();
		});
		var getFile = function(){
			aModal.$el.find('.error-msg').hide();
			var data = {
					'gaccount': wizard.find('[name="gaccount"]').val(),
					'action': 'downloadFile',
					'type': 2,
					'ftpServer': $selectFTP.val(),
					'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
					'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
					'file':  wizard.find('[name="file"]').val(),
					'path':  wizard.find('[name="path"]').val(),
					'showFiles': true,
					'dpxaccount': wizard.find('[name="dpxaccount"]').val()
				};
			$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
				if( r.success ) {
					$.get(urlPath +'action.php?action=uploadCatalog&filename=' + r.file + '&type=2').done(function( response ) {
						if( response.success ) {
							file = response.file;
							$select[0].selectize.clearOptions();
							$select.append('<option value="">---</option>')
							for(var i = 0; i < response.sheets.length; i++) {
								$select[0].selectize.addOption({value: response.sheets[i], text: response.sheets[i]});
							}
							
							$select[0].selectize.refreshItems();

						} else {
							aModal.$el.find('.error-msg').html('Error: ' + response.msg);
							aModal.$el.find('.error-msg').show();
						}		
					});	
				}
			});
		}

		var createTreeFolder = function(idFolder) {
			if(idFolder==null){
				wizard.find('[name="path"]').val("/");
				var data = {
					'gaccount': wizard.find('[name="gaccount"]').val(),
					'action': 'getTreeFolder',
					'type': 2,
					'ftpServer': $selectFTP.val(),
					'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
					'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
					'showFiles': true,
					'dpxaccount': wizard.find('[name="dpxaccount"]').val()
				};
				$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
					if (typeof r == "object") {
						wizard.find(".loadingDiv").hide();
						wizard.find('.nextclass').attr("href",'#next');
						wizard.find('a[href="#next"]').removeClass("nextclass");
						aModal.showError( r.msg );
					} else {
						wizard.find(".loadingDiv").hide();
						first = false;
						wizard.find(".selectedTree").find("ul").empty();
						wizard.find(".selectedTree").find("ul").append(r);
						wizard.find('.fileclass').on( "click", function(e) {
							wizard.find('[name="path"]').data("idFolderDrive",this.id);
							var liElement=$(this);
							wizard.find('[name="path"]').val(liElement.find('a')[0].text);
							wizard.find('[name="file"]').val(liElement.find('a')[0].text);
						});
						wizard.find(".selectedTree").find("ul").metisMenu({ toggle: false });
						wizard.steps('next');
					}
				});
				wizard.find('.metisFolder').on('beforeopen', function(e, anObject) {
					/*@JRPP : Si se trata de una Carpeta escondo la seccion de siguiente*/
					wizard.find('[name="path"]').val($(anObject.el).text()+"/");
					wizard.find('[name="file"]').val("");
					wizard.find('[name="path"]').data("idFolderDrive",anObject.el.parentElement.id);
					if($(anObject.el).parent().attr('aria-expanded')==undefined || $(anObject.el).parent().attr('aria-expanded')=="false"){
						$(anObject.el).parent().attr("aria-expanded","true");
						$(anObject.el).parent().parent().attr("aria-expanded","true");
						createTreeFolder($(anObject.el).parent().attr('id'));
					}else{
						$(anObject.el).parent().attr("aria-expanded","false");
						$(anObject.el).parent().parent().attr("aria-expanded","false");
					}
		      	});
			}else{
				var data = {
					'gaccount': wizard.find('[name="gaccount"]').val(),
					'action': 'getTreeFolder',
					'idFolder':idFolder,
					'type': 2 ,
					'ftpServer':  $selectFTP.val(),
					'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
					'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
					'showFiles': true,
					'dpxaccount': wizard.find('[name="dpxaccount"]').val()
				};
				$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
					wizard.find(".selectedTree [name='"+idFolder+"'] ul").remove();
					wizard.find(".selectedTree [name='"+idFolder+"']").append("<ul class='collapse in' aria-expanded='false'>"+r+"</ul>");
					//wizard.find(".selectedTree").find(".metisFolder").metisMenu({ toggle: false });
					wizard.find(".selectedTree [name='"+idFolder+"'] li").on( "click", function(e) {
						e.stopPropagation();
						if ($(this).find('.fa-file').length>0){
							wizard.find('[name="path"]').data("idFolderDrive",this.id);
							wizard.find('[name="path"]').val("");
							var liElement=$(this);
							wizard.find('[name="file"]').val(liElement.find('a')[0].text);
							var index = 0;
							while (liElement[0].tagName!="DIV") {
								var pathFolder=wizard.find('[name="path"]').val();
								if (index==0) {
									wizard.find('[name="path"]').val(liElement.find('a')[0].text+pathFolder);
								}else {
									wizard.find('[name="path"]').val(liElement.find('a')[0].text+"/"+pathFolder);
								}
								liElement=liElement.parent().parent();
								index++;
							}
							return;
						}
						if(this.id.length > 0){
							wizard.find('[name="path"]').data("idFolderDrive",this.id);
							wizard.find('[name="path"]').val("");
							var liElement=$(this);
							while (liElement[0].tagName!="DIV") {
								var pathFolder=wizard.find('[name="path"]').val();
								wizard.find('[name="path"]').val(liElement.find('a')[0].text+"/"+pathFolder);
								liElement=liElement.parent().parent();
							}
							if($(this).attr('class')==undefined || $(this).attr('class').indexOf('active')< 0){
								if(this.id.length>0){
									$(this).addClass('active');
									if(wizard.find('[name="type"]:checked').val()==5){
										$(this).attr('name',wizard.find('[name="path"]').val())
										createTreeFolder(wizard.find('[name="path"]').val());
									}else{
										createTreeFolder(this.id);
									}
								}
							}else{
								$(this).removeClass('active');
								$(this).find('ul').removeClass('in');
							}
						}
					});
					
				});
			}
		}
		return dfd.promise();
	};

});
