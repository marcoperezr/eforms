<?php 
require_once("repository.inc.php");
require_once("initialize.php");
require_once("surveyProfile.inc.php");

class BITAMSurveyProfileRestriction extends BITAMObject
{
	public $RestrictionID;
	public $ProfileID;
	public $ProfileName;
	public $SectionType;
	public $QuestionType;
	public $CatalogID;
	public $Description;
	
	function __construct($aRepository, $aProfileID)
	{
		BITAMObject::__construct($aRepository);
		
		$this->ProfileID = $aProfileID;
		$this->ProfileName = '';
		$this->RestrictionID = -1;
		$this->Description = '';
		//-1 en las siguientes propiedades significa "cualquier elemento de ese tipo", así que no poner defaults que se ajusten a los defaults
		//de esos tipos de objeto (ejemplo, no poner SectionType == 0 sólo porque las secciones nacen como Estándar cuando se empiezan a definir)
		$this->SectionType = -1;
		$this->QuestionType = -1;
		$this->CatalogID = -1;
	}

	static function NewInstance($aRepository, $aProfileID)
	{
		return new BITAMSurveyProfileRestriction($aRepository, $aProfileID);
	}

	static function NewInstanceWithID($aRepository, $aRestrictionID)
	{
		$anInstance = null;
		
		if (((int) $aRestrictionID) <= 0)
		{
			return $anInstance;
		}
		
		$anInstance =& BITAMGlobalFormsInstance::GetProfileRestInstanceWithID($aRestrictionID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		$sql = "SELECT t1.RestrictionID, t1.ProfileID, t2.ProfileName, t1.Description, t1.SectionType, t1.QuestionType, t1.CatalogID 
				FROM SI_SV_SurveyRestriction t1, SI_SV_SurveyProfile t2 
				WHERE t1.ProfileID = t2.ProfileID AND t1.RestrictionID = ".$aRestrictionID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyRestriction, SI_SV_SurveyProfile ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$anInstance = BITAMSurveyProfileRestriction::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aProfileID = (int)$aRS->fields["ProfileID"];
		
		$anInstance = BITAMSurveyProfileRestriction::NewInstance($aRepository, $aProfileID);
		$anInstance->ProfileName = (string) @$aRS->fields["profilename"];
		$anInstance->RestrictionID = (int) @$aRS->fields["RestrictionID"];
		$anInstance->Description = (string) @$aRS->fields["description"];
		$anInstance->SectionType = (int) @$aRS->fields["sectiontype"];
		$anInstance->QuestionType = (int) @$aRS->fields["questiontype"];
		
		BITAMGlobalFormsInstance::AddProfileRestInstanceWithID($anInstance->RestrictionID, $anInstance);
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("ProfileID", $aHTTPRequest->GET))
		{
			$aProfileID = (int) $aHTTPRequest->GET["ProfileID"];
		}
		else
		{
			$aProfileID = 0;
		}
		
		if (array_key_exists("RestrictionID", $aHTTPRequest->POST))
		{
			$aRestrictionID = $aHTTPRequest->POST["RestrictionID"];
			
			if (is_array($aRestrictionID))
			{
				$aCollection = BITAMSurveyProfileRestrictionCollection::NewInstance($aRepository, $aProfileID, $aRestrictionID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				
				$anInstance = BITAMSurveyProfileRestriction::NewInstance($aRepository, $aProfileID);
				
				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				$anInstance = BITAMSurveyProfileRestriction::NewInstanceWithID($aRepository, (int)$aRestrictionID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMSurveyProfileRestriction::NewInstance($aRepository, $aProfileID);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMSurveyProfileRestriction::NewInstance($aRepository, $aProfileID);
					}
				}
				
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("RestrictionID", $aHTTPRequest->GET))
		{
			$aRestrictionID = $aHTTPRequest->GET["RestrictionID"];
			$anInstance = BITAMSurveyProfileRestriction::NewInstanceWithID($aRepository, (int)$aRestrictionID);
			
			if (is_null($anInstance))
			{
				$anInstance = BITAMSurveyProfileRestriction::NewInstance($aRepository, $aProfileID);
			}
		}
		else
		{
			$anInstance = BITAMSurveyProfileRestriction::NewInstance($aRepository, $aProfileID);
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("RestrictionID", $anArray))
		{
			$this->RestrictionID = (int)$anArray["RestrictionID"];
		}
		
		if (array_key_exists("ProfileID", $anArray))
		{
			$this->ProfileID = (int)$anArray["ProfileID"];
		}
		
 		if (array_key_exists("Description", $anArray))
		{
			$this->Description = $anArray["Description"];
		}
		
		if (array_key_exists("SectionType", $anArray))
		{
			$this->SectionType = (int) $anArray["SectionType"];
		}
		
		if (array_key_exists("QuestionType", $anArray))
		{
			$this->QuestionType = (int) $anArray["QuestionType"];
		}
		
		if (array_key_exists("CatalogID", $anArray))
		{
			$this->CatalogID = (int)$anArray["CatalogID"];
		}
		
		return $this;
	}
	
	function save()
	{
		if ($this->isNewObject())
		{
	    	$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(RestrictionID)", "0")." + 1 AS RestrictionID".
				" FROM SI_SV_SurveyRestriction";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyRestriction ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->RestrictionID = (int) $aRS->fields["RestrictionID"];
			unset ($aRS);
			
			$sql = "INSERT INTO SI_SV_SurveyRestriction (".
					"ProfileID".
					",RestrictionID".
					",Description".
					",SectionType".
					",QuestionType".
					",CatalogID".
					") VALUES (".
					$this->ProfileID.
					",".$this->RestrictionID.
					",".$this->Repository->DataADOConnection->Quote($this->Description).
					",".$this->SectionType.
					",".$this->QuestionType.
					",".$this->CatalogID.
					")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyRestriction ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else
		{
			$sql = "UPDATE SI_SV_SurveyRestriction SET 
					Description = ".$this->Repository->DataADOConnection->Quote($this->Description)." 
					, SectionType = ".$this->SectionType." 
					, QuestionType = ".$this->QuestionType." 
					, CatalogID = ".$this->CatalogID." 
				WHERE RestrictionID = ".$this->RestrictionID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyRestriction ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		return $this;
	}
	
	function remove()
	{
		$sql = "DELETE FROM SI_SV_SurveyRestriction WHERE RestrictionID = ".$this->RestrictionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyRestriction ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->RestrictionID <= 0);
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Rule");
		}
		else
		{
			return translate("Rule")." ".$this->MemberName;
		}
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SurveyProfileRestriction&ProfileID=".$this->ProfileID;
		}
		else
		{
			return "BITAM_PAGE=SurveyProfileRestriction&ProfileID=".$this->ProfileID."&RestrictionID=".$this->RestrictionID;
		}
	}
	
	function get_Parent()
	{
		return BITAMSurveyProfile::NewInstanceWithID($this->Repository, $this->ProfileID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'RestrictionID';
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->get_Parent()->canRemove($aUser);
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Description";
		$aField->Title = translate("Description");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SectionType";
		$aField->Title = translate("Section type");
		$aField->Type = "Integer";
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QuestionType";
		$aField->Title = translate("Question Type");
		$aField->Type = "Integer";
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogID";
		$aField->Title = translate("Catalog");
		$aField->Type = "Integer";
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
?>
 		<script language="JavaScript">
 		</script>
<?
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
?>
	 	<script language="JavaScript">
		objOkSelfButton = document.getElementById("BITAMSurveyProfileRestriction_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMSurveyProfileRestriction_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMSurveyProfileRestriction_OkNewButton");
 			//objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
		</script>
<?
	}
	
	/* Basado en la definición de esta regla, genera la porción WHERE del estatuto SQL correspondiente a la misma
	Estas claúsulas se basan en al parámetro $aTableAliasColl para verificar que alias deben utilizar para cada una de las tablas que se 
	asumirán existentes en el SELECT donde se aplicará (ya que las reglas son pensadas para los procesos que determinan la visibilidad de las
	encuestas, así que ya se tienen una serie de queries para tal fin con joins entre varias tablas), si no se especifica alguna tabla en
	este array, se asumirá que se debe utilizar el nombre de la misma como alias
	*/
	function getWhere($aTableAliasColl) {
		$strWhere = '';
		$strAnd = '';
		
		if (is_null($aTableAliasColl) || !is_array($aTableAliasColl)) {
			$aTableAliasColl = array();
		}
		$strQuestionTab = ($strQuestionTab = (string)@$aTableAliasColl['SI_SV_Question'])?$strQuestionTab:'SI_SV_Question';
		$strSectionTab = ($strSectionTab = (string)@$aTableAliasColl['SI_SV_Section'])?$strSectionTab:'SI_SV_Section';
		
		//Validación por tipo de sección
		$blnValidateQuestions = true;
		$blnValidateSectionCatalog = false;
		if ($this->SectionType > 0) {
			//Valida el tipo de sección específica
			$strWhere .= $strAnd." {$strSectionTab}.SectionType = {$this->SectionType}";
			$strAnd = ' AND ';
			
			switch ($this->SectionType) {
				case sectFormatted:
					//No pueden existir preguntas en secciones formateadas, así que en este caso no valida preguntas
					$blnValidateQuestions = false;
					break;
				case sectDynamic:
				case sectInline:
					//Este tipo de secciones permiten especificar un catálogo
					$blnValidateSectionCatalog = true;
					break;
			}
		} 
		else {
			//Cualquier otro caso no aplica validación
		}
		
		//Validación por tipo de pregunta
		$blnValidateQuestionCatalog = false;
		if ($blnValidateQuestions && $this->QuestionType > 0) {
			//Valida el tipo de pregunta específica
			$strWhere .= $strAnd." {$strQuestionTab}.QTypeID = {$this->QuestionType}";
			$strAnd = ' AND ';
			
			switch ($this->QuestionType) {
				case qtpSingle:
				case qtpOpenNumeric:
					//Estos tipos de pregunta soportan especificar un catálogo
					$blnValidateQuestionCatalog = true;
					break;
			}
		}
		else {
			//Cualquier otro caso no aplica
		}
		
		//Validación por catálogo (depende de si aplica para una pregunta o una sección, si ambas son aplicables, se le dará preferencia a la
		//pregunta y así se podrá crear una regla previa exclusiva para la sección). En este caso la validación incluirá el valor 0 para
		//comprobar si hay preguntas/secciones que no usen catálogo, lo cual sería una regla un tanto extraña pero posible, sólo un valor -1
		//indicaría que son válidas todos los catálogos
		if (($blnValidateQuestionCatalog || $blnValidateSectionCatalog) && $this->CatalogID >= 0) {
			if ($blnValidateQuestionCatalog) {
				//Le da preferencia a la pregunta
				$strWhere .= $strAnd." {$strQuestionTab}.CatalogID = {$this->CatalogID}";
				$strAnd = ' AND ';
			}
			else {
				//En cualquier otro caso se refiere a la sección
				$strWhere .= $strAnd." {$strSectionTab}.CatalogID = {$this->CatalogID}";
				$strAnd = ' AND ';
			}
		}
		
		return $strWhere;
	}
}

class BITAMSurveyProfileRestrictionCollection extends BITAMCollection
{
	public $ProfileID;
	
	function __construct($aRepository, $aProfileID)
	{
		BITAMCollection::__construct($aRepository);
		$this->ProfileID = $aProfileID;
	}
	
	static function NewInstance($aRepository, $aProfileID, $anArrayOfRestrictionIDs = null)
	{
		global $gblShowErrorSurvey;
		
		$anInstance = new BITAMSurveyProfileRestrictionCollection($aRepository, $aProfileID);
		
		$filter = "";
		
		if (!is_null($anArrayOfRestrictionIDs))
		{
			switch (count($anArrayOfRestrictionIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND t1.RestrictionID = ".((int)$anArrayOfRestrictionIDs[0]);
					break;
				default:
					foreach ($anArrayOfRestrictionIDs as $aRestrictionID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aRestrictionID; 
					}
					if ($filter != "")
					{
						$filter = " AND t1.RestrictionID IN (".$filter.")";
					}
					break;
			}
		}
		
		$sql = "SELECT t1.RestrictionID, t1.ProfileID, t2.ProfileName, t1.Description, t1.SectionType, t1.QuestionType, t1.CatalogID 
			FROM SI_SV_SurveyRestriction t1, SI_SV_SurveyProfile t2 
			WHERE t1.ProfileID = t2.ProfileID AND t1.ProfileID = ".$aProfileID.$filter;
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyRestriction, SI_SV_SurveyProfile ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyRestriction, SI_SV_SurveyProfile ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMSurveyProfileRestriction::NewInstanceFromRS($anInstance->Repository, $aRS);
			
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("ProfileID", $aHTTPRequest->GET))
		{
			$aProfileID = (int) $aHTTPRequest->GET["ProfileID"];
		}
		else
		{
			$aProfileID = 0;
		}
		
		return BITAMSurveyProfileRestrictionCollection::NewInstance($aRepository, $aProfileID);
	}
	
	function get_Parent()
	{
		return BITAMSurveyProfile::NewInstanceWithID($this->Repository, $this->ProfileID);
	}
	
	function get_Title()
	{
		return translate("Rules");
	}
	
	function get_QueryString()
	{
		return "BITAM_SECTION=SurveyProfileRestrictionCollection&ProfileID=".$this->ProfileID;
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=SurveyProfileRestriction&ProfileID=".$this->ProfileID;
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'RestrictionID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Description";
		$aField->Title = translate("Description");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "getRule";
		$aField->Title = translate("Rule");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->get_Parent()->canRemove($aUser);
	}
}
?>