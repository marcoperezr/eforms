<?php

define('CREATED_BY_EBAVEL', 9);
define('CREATED_BY_EFORMSV6', 4);
define('CREATED_BY_EFORMSV5', 8);
define('CREATED_BY_SCHEDULER', 5);
define('CREATED_BY_MODELMANAGER', 6);

define('PRE_DIM_MPC_TABLE', 'RIMDIM_');
define('PRE_DIM_WPC_TABLE', 'RIWDIM_');
define('PRE_DIM_SEC_TABLE', 'RISDIM_');
define('PRE_DIM_CAT_TABLE', 'RICDIM_');
define('PRE_DIM_SVY_TABLE', 'RIVDIM_');
define('PRE_DIM_MPC_FIELD', 'MPCQ_');
define('PRE_DIM_DSC_FIELD', 'DSCQ_');
define('PRE_DIM_DSC_FIELD_INLINE', 'DSCS_');
define('PRE_DIM_MEM_FIELD', 'DSCM_');
define('PRE_DIM_MEM_FIELD_INLINE', 'DSCI_');
define('PRE_IND_FIELD', 'INDQ_');
define('PRE_IND_SCORE', 'SCOREQ_');

include_once('error.class.php');

require_once("../model_manager/model.inc.php");
require_once("../model_manager/modeldimension.inc.php");
require_once("../model_manager/attribdimension.inc.php");
require_once("../model_manager/indicatorkpi.inc.php");
require_once("../model_manager/indicator.inc.php");
require_once("../model_manager/modeldata.inc.php");
require_once("../model_manager/filescreator.inc.php");

class ARTUSModelFunctionBasics
{
	public static function existRow($aConnection, $sql, &$consecutivo=-1)
	{
		$consecutivo = -1;
		
		$recordSet = $aConnection->Execute($sql);
		
//		print '<br>'.$sql;

		if($recordSet)
		{
			if (!$recordSet->EOF)	
			{
				$consecutivo = (int) $recordSet->fields['consecutivo'];
				return true;
			}
		}
		else
		{
			$ErrSQL = $aConnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}

		return false;
	}
	
	public static function insertAttribute(&$metaconnection, $cla_descrip_parent, $cla_descrip_child, $nom_dimension, $nom_tabla, $nom_fisico)
	{
// @EA 2016-01-12 Generar atributos para latitude, longitude y accuracy
		$ErrSQL = '';
		
		$sql =
		"INSERT INTO SI_ATRIBUTOS(CLA_DESCRIP, CONSECUTIVO, NOM_LOGICO, NOM_TABLA, NOM_FISICO, TIPO_DATO)
		 VALUES ({$cla_descrip_parent}, {$cla_descrip_child}, {$metaconnection->quote($nom_dimension)}, {$metaconnection->quote($nom_tabla)}, {$metaconnection->quote($nom_fisico)}, 2)";

		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
			
//			print '<br>'.$sql.' -> '.$ErrSQL;
		}		
		
		return $ErrSQL;		 
	}
	
	public static function insertGlobalDimension(&$metaconnection, $cla_descrip, $nom_dimension, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup)
	{
		$tipo_dimension = 3;
		$default_key = '\'NA\'';
		$ErrSQL = '';
		
		$sql  = 
		"INSERT INTO SI_DESCRIP_ENC
		(cla_descrip, cla_conexion, nom_logico, nom_fisico, tipo_dato,
			long_dato, nom_fisicok, tipo_datok, long_datok,
			nom_tabla, tipo_dimension, default_key, created_by, agrupador
			) VALUES (
			{$cla_descrip}, 1, {$metaconnection->quote($nom_dimension)},
			{$metaconnection->quote($nom_fisico)}, {$tipo_dato}, {$long_dato},
			{$metaconnection->quote($nom_fisico)}, {$tipo_dato}, {$long_dato},
			{$metaconnection->quote($nom_tabla)}, {$tipo_dimension}, {$default_key},
			".CREATED_BY_EFORMSV6.", {$metaconnection->quote($dimensionGroup)}
			)";
			
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}		
		
		return $ErrSQL;
	}
	
	public static function insertLocalDimension(&$metaconnection, $consecutivo, $nivel, $cla_descrip, $nom_dimension, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup, $cla_concepto, $par_descriptor = '')
	{
		$ErrSQL = '';
		
		if ($par_descriptor == '')
			$par_descriptor = $consecutivo;
			
		$sql = 
		"INSERT INTO si_cpto_llave
			(cla_concepto, consecutivo, nivel, cla_descrip, nom_descriptor, par_descriptor,
				nom_fisico, nom_logico, tipo_dato, long_dato, estatus, agrupador 
				) VALUES (
				{$cla_concepto}, {$consecutivo}, {$nivel}, {$cla_descrip},
				{$metaconnection->quote($nom_fisico)},
				{$metaconnection->quote($par_descriptor)},
				{$metaconnection->quote($nom_fisico)},
				{$metaconnection->quote($nom_dimension)},
				{$tipo_dato}, {$long_dato}, 1, {$metaconnection->quote($dimensionGroup)})";
				
//		print '<br>'.$sql;

		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}		
		return $ErrSQL;
	}
	
	public static function getMaxClaDescrip($aConnection)
	{
		$maxId = 1;
		
		$aQuery = 
		"SELECT MAX(CLA_DESCRIP) as cla_descrip 
		 FROM SI_DESCRIP_ENC ";
		 
		$recordSet = $aConnection->Execute($aQuery);
		if($recordSet)
			$maxId = $recordSet->fields['cla_descrip'] + 1;
		else
		{
			$ErrSQL = $aConnection->ErrorMsg();
			savelogfile($aQuery.' -> '.$ErrSQL);
		}
		
		return $maxId;
	}
	
	public static function getMaxConsecutivoNivel($cla_concepto, $aConnection)
	{
		$arrMax = array("consecutivo" => 1, "nivel" => 1);
		$aQuery = "SELECT MAX(consecutivo) as consecutivo, MAX(nivel) as nivel FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = {$cla_concepto}";
		$recordSet = $aConnection->Execute($aQuery);
		if($recordSet && !$recordSet->EOF)
		{
			$arrMax["consecutivo"] = $recordSet->fields['consecutivo'] + 1;
			$arrMax["nivel"] = $recordSet->fields['nivel'] + 1;
		}
		else
		{
			$ErrSQL = $aConnection->ErrorMsg();
			savelogfile($aQuery.' -> '.$ErrSQL);
		}
		
		return $arrMax;
	}
	
	public static function insertSI_DESCRIP_KEYS(&$metaconnection, $nom_tabla, $nom_fisicok, $nom_fisicok_join, $cla_concepto )
	{
		$sql =
		"select count(1) as nveces from si_descrip_keys where NOM_TABLA = '".$nom_tabla."' and CLA_CONCEPTO = {$cla_concepto}";
		
		$rst = $metaconnection->Execute($sql);
		
		if ($rst)
		{
			if ($rst->fields['nveces'] == 0)
			{
				$sql  = 
				"INSERT INTO si_descrip_keys
					(cla_concepto, nom_tabla, consecutivo, nom_fisicok, nom_fisicok_join
						) VALUES ( {$cla_concepto}, {$metaconnection->quote($nom_tabla)},
						1,
						{$metaconnection->quote($nom_fisicok)},
						{$metaconnection->quote($nom_fisicok_join)}
						)
				";
				if (!$metaconnection->Execute($sql))
				{
					$ErrSQL = $metaconnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}
			}
		}
	}
	
	public static function addModelIndicatorCalculated($Repository, $cla_concepto, $indicator_name, $formula_bd, $formato, $consecutivo=-1)
	{
		$strOriginalWD = getcwd();
		
		$anInstanceIndicator = BITAMIndicator::NewIndicator($Repository);
		$anInstanceIndicator->CubeID = $cla_concepto;
		$anInstanceIndicator->IndicatorName = $indicator_name;
		$anInstanceIndicator->formula_bd = $formula_bd;
		$anInstanceIndicator->formato = $formato;
		$anInstanceIndicator->is_custom = 1;
		$anInstanceIndicator->no_ejecutivo = 0;
		
		if ($consecutivo > 0)
			$anInstanceIndicator->dim_depend[$consecutivo] = $consecutivo;
		
		@$anInstanceIndicator->save();
		
		chdir($strOriginalWD);
		
		return $anInstanceIndicator->IndicatorID;
	}
	
	public static function deleteFromSI_INDICADOR(&$metaconnection, $cla_indicador)
	{
		$sql = "delete from SI_GPO_IND_DET where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_IND_LINEA where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_PROPxIND where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_IND_REPORTE where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_COMENTARIO where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_INDxLINEA where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_OPxIND where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_INDREL where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_REPORTExIND where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_IND_REPORTE6 where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_USR_CONFIG_IND where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_ROL_CONFIG_IND where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		$sql = "delete from SI_INDICADOR where CLA_INDICADOR = ".$cla_indicador;
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
	}

}

class ARTUSModelFromScheduler extends ARTUSModelFunctionBasics
{
	public $Repository;
	public $cla_concepto = -1;
	public $cla_descrip_user = -1;
	public $cla_descrip_agenda = -1;
	public $cla_descrip_puntodevisita = -1;
	
	public $cla_gpo_ind = -1;
	
	public $cla_indicator_visitasplaneadas = -1;
	public $cla_indicador_visitasreales = -1;
	public $cla_indicator_visitasdiferencia = -1;
	public $cla_indicator_visitasporcentaje = -1;
	
	public $cla_indicador_duracionplaneada = -1;
	public $cla_indicador_duracionreal = -1;
	public $cla_indicador_duraciondiferencia = -1;
	public $cla_indicador_duracionporcentaje = -1;
	
	public $cla_indicador_distanciadiferencia = -1;
	
	public $cla_indicador_tiempodetranslado = -1;

	function __destruct() {

	}
	
	static function NewInstance(&$Repository)
	{
		$aArtusModel = new ARTUSModelFromScheduler();
		$aArtusModel->Repository = $Repository;
		$aArtusModel->error = BITAMAppError::NewInstance();
		$aArtusModel->warning = BITAMAppError::NewInstance();

		return $aArtusModel;
	}

	function getDimClaDescrip($dimensionName, $CREATE_BY = -1)
	{
		if ($CREATE_BY == -1)
			$CREATE_BY = CREATED_BY_EFORMSV5;

		$sql = 'select CLA_DESCRIP as cla_descrip from SI_DESCRIP_ENC where CREATED_BY = '.$CREATE_BY.' and NOM_LOGICO = '.$this->Repository->ADOConnection->Quote($dimensionName);
		$rst = $this->Repository->ADOConnection->Execute($sql);

		if ($rst && !$rst->EOF)
			return $rst->fields['cla_descrip'];
		else
		{
			$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
			
		return -1;
	}

	function addModelDimension($dimensionName, $dimensionID, $dimensionGroup, &$consecutivo_dim_survey=-1, $CREATE_BY = -1)
	{
		$strOriginalWD = getcwd();
		
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->cla_concepto);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		
		if ($CREATE_BY == -1)
			$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMSV5;
		else
			$anInstanceModelDim->Dimension->Created_By = $CREATE_BY;
			
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->DimensionGroup = $dimensionGroup;
		$anInstanceModelDim->Dimension->UseKey = 0;
		$anInstanceModelDim->Dimension->DimensionClaDescrip = $dimensionID;
		
		$objMMAnswer = @$anInstanceModelDim->save();
		
		if ($CREATE_BY != -1 && $anInstanceModelDim->Dimension->DimensionClaDescrip != -1)
		{
			$sql = "update SI_DESCRIP_ENC set CREATED_BY = {$CREATE_BY} where CLA_DESCRIP = {$anInstanceModelDim->Dimension->DimensionClaDescrip}";
			$this->Repository->ADOConnection->Execute($sql);
		}
		
		$consecutivo_dim_survey = $anInstanceModelDim->Dimension->DimensionID;
		
		chdir($strOriginalWD);
		
		$strMMError = identifyModelManagerError($objMMAnswer);
		if ($strMMError == '') {
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
			return $anInstanceModelDim->Dimension->DimensionClaDescrip;
		}
		else {
			$sErr = "\r\n(".__METHOD__.") ".translate("Error adding {$dimensionName} dimension").": ".$strMMError.". ";
			savelogfile($sErr);
		}
		
		return -1;
	}

	function addBaseIndicator($cla_concepto, $nom_indicador, $nom_tabla, $nom_fisico, &$cla_indicador=-1, $consecutivo_dim=-1)
	{
// Consideramos el indicador de tipo score
		$formato = '#,##0';
		$formula_bd = 'SUM(t1.'.$nom_fisico.')';
	
		$sql = 
		"select cla_indicador as consecutivo from SI_INDICADOR where FORMULA_BD = '{$formula_bd}' and CLA_CONCEPTO = {$cla_concepto}";
		$ExistIndicator = $this->existRow($this->Repository->ADOConnection, $sql);

		if (!$ExistIndicator)
		{
			$dataconnection = $this->Repository->DataADOConnection;
			
			$sql = 
			"alter table {$nom_tabla} add {$nom_fisico} double null";
			
			if (!$dataconnection->Execute($sql))
			{
				$ErrSQL = $dataconnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}
			
			$cla_indicador = $this->addModelIndicatorCalculated($this->Repository, $cla_concepto, $nom_indicador, $formula_bd, $formato, $consecutivo_dim);
		}
//		
	}

	public function getSQLSchedulerFactTable($FactTable, $cla_descrip_user, $cla_descrip_puntodevisita, $cla_descrip_agenda)
	{
		$subrrogatekeyUser = "RIDIM_{$cla_descrip_user}KEY";
		$subrrogatekeyCheckPoint = "RIDIM_{$cla_descrip_puntodevisita}KEY";
		$subrrogatekeyScheduler = "RIDIM_{$cla_descrip_agenda}KEY";
		
		$userDSC = "DSC_{$cla_descrip_user}";
		$checkpointDSC = "DSC_{$cla_descrip_puntodevisita}";
		$schedulerDSC = "DSC_{$cla_descrip_agenda}";
		
		$tableUser = "RIDIM_{$cla_descrip_user}";
		$tableCheckPoint = "RIDIM_{$cla_descrip_puntodevisita}";
		$tableScheduler = "RIDIM_{$cla_descrip_agenda}";
		
		$sql =
		"
		
		/*== Sincronize FactTable ==*/
		
		delete from {$FactTable} where DateKey between DATE_FORMAT(vDateIni, '%Y%m%d') and DATE_FORMAT(vDateFin, '%Y%m%d');
		
		INSERT INTO {$FactTable} ({$subrrogatekeyUser}, {$subrrogatekeyCheckPoint}, {$subrrogatekeyScheduler}, DateKey, REALVISIT, PLANED_DURATION, REAL_DURATION, LOCATION_DIFERENCE, TIME_TRANSLADE)
		SELECT COALESCE(t2.{$subrrogatekeyUser}, 1), COALESCE(t3.{$subrrogatekeyCheckPoint}, 1), COALESCE(t4.{$subrrogatekeyScheduler}, 1), DATE_FORMAT(t1.FechaDeCaptura, '%Y%m%d'), 
			t1.VisitasReales, t1.DuracionPlaneada, t1.DuracionReal, t1.DistanciaVariacion, t1.TiempoDeTraslado
		FROM TempFactScheduler AS t1 
			LEFT JOIN $tableUser AS t2 ON (t2.{$userDSC} = t1.Usuario)
			LEFT JOIN $tableCheckPoint AS t3 ON (t3.{$checkpointDSC} = t1.PuntoDeVisita)
			LEFT JOIN $tableScheduler AS t4 ON (t4.{$schedulerDSC} = t1.Agenda)
		where t1.FechaDeCaptura between vDateIni and vDateFin;
		
		";
		
		return $sql;
	}

	public function getSQLSchedulerGlobalDimension($cla_descrip, $campoTMP)
	{
		$sql = '';
		if ($cla_descrip > 0)
		{
			$tabla = "RIDIM_{$cla_descrip}";
			$campo = "DSC_{$cla_descrip}";
		
			$sql =
			"
			/*== Sincronize Catalog Dim -> {$campoTMP} */
			
			CREATE TEMPORARY TABLE IF NOT EXISTS TEMP1{$tabla} (
                 {$campo} VARCHAR(255)  NOT NULL
    		)ENGINE=MyISAM;
    		
			delete from TEMP1{$tabla};
    		
 			INSERT INTO TEMP1{$tabla} ({$campo})
    		SELECT distinct COALESCE(src.{$campoTMP},'ND')
    		FROM  TempFactScheduler src  
    		WHERE src.FechaDeCaptura between vDateIni and vDateFin;
    		
			INSERT INTO {$tabla} ({$campo})
			SELECT COALESCE(src.{$campo}, 'ND')
    		FROM TEMP1{$tabla} src   LEFT JOIN {$tabla} dim  ON src.{$campo} =  dim.{$campo}
    		WHERE	 dim.{$campo} IS NULL
    		GROUP BY src.{$campo};
    		
    		DROP TABLE TEMP1{$tabla};

    		";
    	}
		return $sql;
	}
	
	public function createSchedulerProc()
	{
		if ($this->cla_concepto > 0)
		{
			$sql = "drop procedure IF EXISTS sp_load_scheduler";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$sql =
			"CREATE PROCEDURE sp_load_scheduler (IN vDateIni datetime, IN vDateFin datetime)
			BEGIN
			";
			
			$sql .= $this->getSQLSchedulerGlobalDimension($this->cla_descrip_user, 'Usuario');
			$sql .= $this->getSQLSchedulerGlobalDimension($this->cla_descrip_puntodevisita, 'PuntoDeVisita');
			$sql .= $this->getSQLSchedulerGlobalDimension($this->cla_descrip_agenda, 'Agenda');
			
			$FactTable = "RIFACT_{$this->cla_concepto}";
			
			$sql .= $this->getSQLSchedulerFactTable($FactTable, $this->cla_descrip_user, $this->cla_descrip_puntodevisita, $this->cla_descrip_agenda);
			
			$sql .= "
			END
			";

			if (!$this->Repository->DataADOConnection->Execute($sql))
			{
				$serr = $this->Repository->DataADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$serr);
//				print '<br>'.$sql.' -> '.$serr;
				
			}
			
		}
	}
	
	public function runSchedulerProc($fecha_ini, $fecha_fin, &$error)
	{
		$sql = "call sp_load_scheduler('{$fecha_ini}', '{$fecha_fin}')";
		
		$error = '';
		if (!$this->Repository->DataADOConnection->Execute($sql))
		{
			$serr = $this->Repository->DataADOConnection->ErrorMsg();
			$error = $sql.' -> '.$serr;
			savelogfile($error);
//			print '<br>'.$sql.' -> '.$serr;
		}
	}
	
	public function createIfNotExistsSchedulerModel()
	{
// @EA 2016-05-26 Checa si existe el modelo de agenda en la metadata y si no existe lo crea
		global $generateXLS, $generatePHP, $PHPSincronize;
		$generateXLS = false;
		$generatePHP = false;
		$PHPSincronize = false;

		$metaconnection = $this->Repository->ADOConnection;
		$dataconnection = $this->Repository->DataADOConnection;
		
		$sql = "select	CLA_CONCEPTO as modelid, LLAVES as cla_gpo_ind from SI_CONCEPTO where RI_DEFINITION = ".CREATED_BY_SCHEDULER;
		$rst = $metaconnection->Execute($sql);
		if ($rst)
		{
			if ($rst->EOF)
			{
				$sql = "ALTER TABLE si_sv_survey ADD PlannedDuration INT NULL";
				$dataconnection->Execute($sql);
				$sql = "ALTER TABLE si_sv_surveyagendadet ADD RealDuration DOUBLE NULL";
				$dataconnection->Execute($sql);
				$sql = "ALTER TABLE si_sv_surveyagendadet ADD RealLatitude DOUBLE NULL";
				$dataconnection->Execute($sql);
				$sql = "ALTER TABLE si_sv_surveyagendadet ADD RealLongitude DOUBLE NULL";
				$dataconnection->Execute($sql);
				$sql = "ALTER TABLE si_sv_surveyagenda ADD Latitude DOUBLE NULL";
				$dataconnection->Execute($sql);
				$sql = "ALTER TABLE si_sv_surveyagenda ADD Longitude DOUBLE NULL";
				$dataconnection->Execute($sql);
				$sql = "ALTER TABLE si_sv_surveyagenda ADD TrasladeTime DOUBLE NULL";
				$dataconnection->Execute($sql);
				$sql = "DROP VIEW IF EXISTS distinctScheduler";
				$dataconnection->Execute($sql);
				$sql = "CREATE VIEW distinctScheduler AS
					   SELECT CaptureStartDate, UserId, FilterText, MAX(AgendaID) AS AgendaID, COUNT(1) AS duplicidad
					   FROM   si_sv_surveyagenda
					   GROUP BY CaptureStartDate, UserId, FilterText";
				$dataconnection->Execute($sql);
				$sql = "DROP VIEW IF EXISTS TempFactScheduler";
				$dataconnection->Execute($sql);
				$sql = "CREATE VIEW TempFactScheduler
				AS
				SELECT t1.CaptureStartDate AS FechaDeCaptura,
                CONCAT(SUBSTRING(t1.CaptureStartDate,1,10), ' ', t1.CaptureStartTime, ':00') AS FechaHoraDeCaptura,
                t4.NOM_LARGO AS Usuario, IFNULL(t3.NAME, 'ND') AS PuntoDeVisita, t1.FilterText AS Agenda, 
                (SELECT MAX(t2.Status) FROM si_sv_surveyagendadet AS t2 WHERE t2.AgendaID = t1.AgendaID) AS VisitasReales,
                (SELECT IFNULL(SUM(t3.PlannedDuration),0) FROM si_sv_survey AS t3 INNER JOIN si_sv_surveyagendadet AS t4 ON (t4.SurveyID = t3.SurveyID) WHERE t4.AgendaID = t1.AgendaID) AS DuracionPlaneada,
                (SELECT IFNULL(SUM(t2.RealDuration), 0) FROM si_sv_surveyagendadet AS t2 WHERE t2.AgendaID = t1.AgendaID) AS DuracionReal,
                (SELECT MAX(Geolocalization(t1.Latitude, t1.Longitude, t2.RealLatitude, t2.RealLongitude))  FROM si_sv_surveyagendadet AS t2 WHERE t2.AgendaID = t1.AgendaID) AS DistanciaVariacion,
                IFNULL(t1.TrasladeTime, 0) AS TiempoDeTraslado
				FROM si_sv_surveyagenda AS t1 
                	INNER JOIN distinctScheduler AS t2 ON (t2.AgendaID = t1.AgendaID)
                	LEFT JOIN si_sv_checkpoint AS t3 ON (t3.id = t1.checkpointid)
                	INNER JOIN {$this->Repository->ADODBDatabase}.SI_USUARIO AS t4 ON (t4.CLA_USUARIO = t1.UserID)";
				$dataconnection->Execute($sql);

				$sql = "create index idxRI_DEFINITION on SI_CONCEPTO (RI_DEFINITION)";
				$metaconnection->Execute($sql);

				$strOriginalWD = getcwd();
				$anInstanceModel = BITAMModel::NewModel($this->Repository);
				$anInstanceModel->ModelName = 'Scheduler Model';
				$anInstanceModel->readPeriods();

				@$anInstanceModel->ri_definition = CREATED_BY_SCHEDULER;
				@$anInstanceModel->use_cache = false;

				$objMMAnswer = @$anInstanceModel->save();
				chdir($strOriginalWD);
				
				$fact_table = '';
				
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError != '') {
					$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding survey's model").": ".$strMMError.". ";
					savelogfile($gblModMngrErrorMessage);
				}
				else				
				{
					$this->cla_concepto = $anInstanceModel->ModelID;
					$this->cla_gpo_ind = (int) $anInstanceModel->model_options;
					$fact_table = $anInstanceModel->nom_tabla;
				}

				$dimensionGroup = '';
//=================================================================
				$dimensionName = 'User';
				$dimensionID = $this->getDimClaDescrip($dimensionName);
				$this->cla_descrip_user = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);
				
				$dimensionName = 'Scheduler';
				$dimensionID = $this->getDimClaDescrip($dimensionName, CREATED_BY_SCHEDULER);
				$this->cla_descrip_agenda = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup, $dimensionID, CREATED_BY_SCHEDULER);
				
				$dimensionName = 'Check Point';
				$dimensionID = $this->getDimClaDescrip($dimensionName, CREATED_BY_SCHEDULER);
				$this->cla_descrip_puntodevisita = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup, $dimensionID, CREATED_BY_SCHEDULER);
//=================================================================				
				$indicatorName = 'Planned Visit';
				$formula_bd = "COUNT(1)";
				$formato = '#,##0';
				$this->cla_indicator_visitasplaneadas = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);
				
				$indicatorName = 'Real Visit';
				$fieldName = "REALVISIT";
				$this->cla_indicador_visitasreales = -1;
				$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_visitasreales);
				
				$indicatorName = 'Visit Difference';
				$formula_bd = "SUM(t1.REALVISIT)-COUNT(1)";
				$formato = '#,##0';
				$this->cla_indicator_visitasdiferencia = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);

				$indicatorName = 'Visit Percentage';
				$formula_bd = "( SUM(t1.REALVISIT) / COUNT(1) ) * 100.0";
				$formato = '%0';
				$this->cla_indicator_visitasporcentaje = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);
//=================================================================
				$indicatorName = 'Planned Duration';
				$fieldName = "PLANED_DURATION";
				$this->cla_indicador_duracionplaneada = -1;
				$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_duracionplaneada);
				
				$indicatorName = 'Real Duration';
				$fieldName = "REAL_DURATION";
				$this->cla_indicador_duracionreal = -1;
				$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_duracionreal);
				
				$indicatorName = 'Duration Difference';
				$formula_bd = "SUM(t1.REAL_DURATION)-SUM(t1.PLANED_DURATION)";
				$formato = '#,##0';
				$this->cla_indicator_duraciondiferencia = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);

				$indicatorName = 'Duration Percentage';
				$formula_bd = "( SUM(t1.REAL_DURATION) / SUM(t1.PLANED_DURATION) ) * 100.0";
				$formato = '%0';
				$this->cla_indicator_duracionporcentaje = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);
//=================================================================
				$indicatorName = 'Location Diference';
				$fieldName = "LOCATION_DIFERENCE";
				$this->cla_indicador_distanciadiferencia = -1;
				$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_distanciadiferencia);
//=================================================================
				$indicatorName = 'Time Traslade';
				$fieldName = "TIME_TRANSLADE";
				$this->cla_indicador_tiempodetranslado = -1;
				$this->addBaseIndicator($this->cla_concepto, $indicatorName, $fact_table, $fieldName, $this->cla_indicador_tiempodetranslado);
//=================================================================
			}
			else
			{
				$this->cla_concepto = $rst->fields['modelid'];
				
				$this->cla_gpo_ind = (int) $rst->fields['cla_gpo_ind'];
				
				$dimensionName = 'User';
				$this->cla_descrip_user = $this->getDimClaDescrip($dimensionName);
				$dimensionName = 'Scheduler';
				$this->cla_descrip_agenda = $this->getDimClaDescrip($dimensionName, CREATED_BY_SCHEDULER);
				$dimensionName = 'Check Point';
				$this->cla_descrip_puntodevisita = $this->getDimClaDescrip($dimensionName, CREATED_BY_SCHEDULER);
				
			}
		}
		
		if ($this->cla_concepto > 0)
		{
			$this->createSchedulerProc();
			
			$generateXLS = false;
			$generatePHP = true;
			$PHPSincronize = true;
			@ExportToPHP($this->Repository, $this->cla_concepto);
		}
	}
}

class ARTUSModelFromSurvey extends ARTUSModelFunctionBasics
{
	public $Repository;
	public $error = null;
	public $warning = null;
	//si_sv_modelartus
	
	public $id_modelartus;
	public $SurveyID = -1;
	public $SurveyName = '';
	public $name;
	public $description;

	//si_concepto
	public $cla_concepto = -1;
	public $cla_descrip_user = -1;
	public $cla_descrip_sv = -1;
	public $cla_descrip_sv_latitude = -1;
	public $cla_descrip_sv_longitude = -1;
	public $cla_descrip_sv_accuracy = -1;
	public $cla_descrip_sv_date = -1;
	public $cla_descrip_sv_hour = -1;
	public $cla_descrip_sv_enddate = -1;
	public $cla_descrip_sv_endhour = -1;
	
	public $cla_descrip_version = -1;
	public $cla_descrip_country = -1;
	public $cla_descrip_state = -1;
	public $cla_descrip_city = -1;
	public $cla_descrip_zipcode = -1;
	public $cla_descrip_address = -1;
	
	public $cla_descrip_agenda = -1;
	
	public $cla_indicador_count = -1;
	public $cla_indicador_elapsed = -1;
	
	public $ri_definition = 0;
	//Detalles
	public $indicators = array();
	public $dimensions = array();
	// Mapeo con eBavel
	public $ebavelmap = array();
	public $ebaveldims = array();
	public $aquestions = array();
	
	public $list_questions = '';
	public $list_sections = '';
	public $list_globaldimensions = '';
	public $list_globalindicators = '';
	
	public $ExistsChanges = false;
	
	function __destruct() {

	}
	
	static function NewInstance(&$Repository, $id_modelartus, &$rs)
	{
		$aArtusModel = new ARTUSModelFromSurvey();
		$aArtusModel->Repository = $Repository;
		$aArtusModel->error = BITAMAppError::NewInstance();
		$aArtusModel->warning = BITAMAppError::NewInstance();
		$aArtusModel->id_modelartus = $id_modelartus;
		$aArtusModel->fillParameters($rs);
		return $aArtusModel;
	}

	public function fillParameters(&$rs)
	{
		$this->SurveyID = $rs->fields['surveyid'];
		$this->SurveyName = $rs->fields['surveyname'];
		$this->name = $rs->fields['modelname'];
		$this->description = $rs->fields['modeldescription'];
		//si_concepto
		$this->cla_concepto = $rs->fields['cla_concepto'];
		$this->nom_concepto = $this->name;
		
// Indicadores
		$this->cla_indicador_count = $rs->fields['cla_indicador_count'];
		$this->cla_indicador_elapsed = $rs->fields['cla_indicador_elapsed'];
//
		
		$this->fillParametersDimensions($rs);
	}
	
	public function fillParametersDimensions(&$rs)
	{
		$this->cla_descrip_user = $rs->fields['cla_descrip_user'];
		$this->cla_descrip_country = $rs->fields['cla_descrip_country'];
		$this->cla_descrip_state = $rs->fields['cla_descrip_state'];
		$this->cla_descrip_city = $rs->fields['cla_descrip_city'];
		$this->cla_descrip_zipcode = $rs->fields['cla_descrip_zipcode'];
		$this->cla_descrip_address = $rs->fields['cla_descrip_address'];
		$this->cla_descrip_version = $rs->fields['cla_descrip_version'];
// @EA 2015-08-31		
		$this->cla_descrip_agenda = $rs->fields['cla_descrip_agenda'];
//		
		
		$this->cla_descrip_sv = $rs->fields['cla_descrip_sv'];
// Atributos		
		$this->cla_descrip_sv_latitude = $rs->fields['cla_descrip_sv_latitude'];
		$this->cla_descrip_sv_longitude = $rs->fields['cla_descrip_sv_longitude'];
		$this->cla_descrip_sv_accuracy = $rs->fields['cla_descrip_sv_accuracy'];
		$this->cla_descrip_sv_date = $rs->fields['cla_descrip_sv_date'];
		$this->cla_descrip_sv_hour = $rs->fields['cla_descrip_sv_hour'];
		$this->cla_descrip_sv_enddate = $rs->fields['cla_descrip_sv_enddate'];
		$this->cla_descrip_sv_endhour = $rs->fields['cla_descrip_sv_endhour'];
	}
	
	public static function deleteDataModelFromSurvey(&$Repository, $id_survey, &$acapturesidtodelete, &$ErrDesc)
	{
// @EA 2016-06-05 Dado una encuesta, se reciben la lista de consecutivos a borrar
		if (!isset($id_survey))
		{
			$ErrDesc = '!isset($id_survey)';
			return false;			
		}

		if ($id_survey <= 0)
		{
			$ErrDesc = '$id_survey <= 0';
			return false;
		}
		
		if (!is_array($acapturesidtodelete))
		{
			$ErrDesc = '!is_array($acapturesidtodelete)';
			return false;	
		}
		$ncont = count($acapturesidtodelete);
		if ($ncont == 0)
		{
			$ErrDesc = 'count($acapturesidtodelete) == 0';
			return false;	
		}
		
		$sids = implode(',', $acapturesidtodelete);
			
		$dataconnection = $Repository->DataADOConnection;
		$metaconnection = $Repository->ADOConnection;
		
		$scubes = '';
		$acubes = array();

		$sql = "select	cla_concepto as cubekey from si_sv_modelartus where id_survey = {$id_survey} and cla_concepto > 0";
		$rs = $dataconnection->Execute($sql);
		$capturekey = PRE_DIM_SVY_TABLE.'KEY';
		if ($rs)
		{
			while (!$rs->EOF)
			{
				$ncubekey = $rs->fields['cubekey'];
				$acubes[] = $ncubekey;
				if ($scubes == '')
					$scubes = $ncubekey;
				else
					$scubes .= ', '.$ncubekey;
					
				$rs->MoveNext();
			}
		}
		if ($scubes != '')
		{
			foreach ($acubes as $ncubekey)
			{
				$sql = "delete from RIFACT_{$ncubekey} where {$capturekey} IN ({$sids})";
				$dataconnection->Execute($sql);
//				print '<br>'.$sql;
			}
			
			$sql =
			"SELECT DISTINCT t1.NOM_TABLA AS tabla
			 FROM SI_DESCRIP_ENC AS t1 INNER JOIN SI_CPTO_LLAVE AS t2 ON (t2.CLA_DESCRIP = t1.CLA_DESCRIP)
			 WHERE t2.CLA_CONCEPTO IN ({$scubes}) AND t2.CLA_PERIODO IS NULL AND NOT t1.NOM_TABLA LIKE 'RIDIM%' AND t1.NOM_TABLA LIKE 'RI%'";
		
			$rs = $metaconnection->Execute($sql);
			$adimtables = array();
			if ($rs)
			{
				$adimtables[] = $rs->fields['tabla'];
				$rs->MoveNext();
			}
			
			foreach ($adimtables as $dimtable)
			{
				$sql = "delete from {$dimtable} where {$capturekey} IN ({$sids})";
				$dataconnection->Execute($sql);
//				print '<br>'.$sql;
			}
		}
		return true;
	}
	
	public function setGlobalDimensions()
	{
		if ($this->cla_concepto == -1)
		{
// Checamos si existe al menos una encuesta que tenga modelo
			$sql = 
			"select	cla_descrip_user, cla_descrip_country, cla_descrip_state, cla_descrip_city, cla_descrip_zipcode,
				    cla_descrip_address, cla_descrip_version, cla_descrip_agenda, cla_descrip_sv, cla_descrip_sv_latitude, cla_descrip_sv_longitude,
				    cla_descrip_sv_accuracy, cla_descrip_sv_date, cla_descrip_sv_hour, cla_descrip_sv_enddate, cla_descrip_sv_endhour
			from	si_sv_modelartus
			where	id_survey = {$this->SurveyID} and id_modelartus <> {$this->id_modelartus} and (not cla_concepto is null and cla_concepto > 0)";
			
			$rs = $this->Repository->DataADOConnection->Execute($sql);
			
			if (!$rs)
			{
				$ErrMsg = $this->Repository->DataADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrMsg);
			}
			elseif (!$rs->EOF)
				$this->fillParametersDimensions($rs);
				
		}
		
		$this->list_globaldimensions =
		$this->cla_descrip_user.', '.$this->cla_descrip_country.', '.$this->cla_descrip_state.', '.$this->cla_descrip_city.', '.$this->cla_descrip_zipcode.', '.
		$this->cla_descrip_address.', '.$this->cla_descrip_version.', '.$this->cla_descrip_agenda.', '.$this->cla_descrip_sv.', '.$this->cla_descrip_sv_latitude.', '.$this->cla_descrip_sv_longitude.', '.
		$this->cla_descrip_sv_accuracy.', '.$this->cla_descrip_sv_date.', '.$this->cla_descrip_sv_hour.', '.$this->cla_descrip_sv_enddate.', '.$this->cla_descrip_sv_endhour;
			
	}
	
	public function setGlobalIndicators()
	{
		$this->list_globalindicators = $this->cla_indicador_count.', '.$this->cla_indicador_elapsed;
	}
	
	public static function buildARTUSModelWithKey(&$Repository, $id_modelartus)
	{
// @EA 2015-10-21 Insertar las preguntas mapeadas a eBavel, si no existen en el modelo
		$sql = 
		"INSERT INTO si_sv_modelartus_questions(id_modelartus, id_section, id_question, `path`)
		 SELECT t2.id_modelartus, t1.SectionID, t1.QuestionID, CONCAT(t1.QuestionID,'[DIM]') AS `path`
 		 FROM SI_SV_Question AS t1 INNER JOIN si_sv_modelartus_mapping AS t2 ON (t2.id_question = t1.QuestionID)
		 WHERE t2.id_modelartus = {$id_modelartus} AND NOT t1.QuestionID IN 
		 (SELECT id_question FROM si_sv_modelartus_questions WHERE id_modelartus = {$id_modelartus})";
		$Repository->DataADOConnection->Execute($sql);
//

		$aArtusModel = null;
		
		//Consultar los modelos de artus configurados en desarrollo
		$sql = "SELECT IFNULL(V.cla_concepto,-1) AS cla_concepto, V.`name` AS modelname, V.`description` AS modeldescription, S.SurveyID as surveyid,
				S.SurveyName as surveyname,
				ifnull(V.cla_descrip_user,-1) as cla_descrip_user,
				IFNULL(V.cla_descrip_country,-1) AS cla_descrip_country,
				IFNULL(V.cla_descrip_state,-1) AS cla_descrip_state,
				IFNULL(V.cla_descrip_city,-1) AS cla_descrip_city,
				IFNULL(V.cla_descrip_zipcode,-1) AS cla_descrip_zipcode,
				IFNULL(V.cla_descrip_address,-1) AS cla_descrip_address,
				IFNULL(V.cla_descrip_version,-1) AS cla_descrip_version,
				IFNULL(V.cla_descrip_agenda,-1) AS cla_descrip_agenda,
				IFNULL(V.cla_descrip_sv,-1) AS cla_descrip_sv,
				IFNULL(V.cla_descrip_sv_latitude,-1) AS cla_descrip_sv_latitude,
				IFNULL(V.cla_descrip_sv_longitude,-1) AS cla_descrip_sv_longitude,
				IFNULL(V.cla_descrip_sv_accuracy,-1) AS cla_descrip_sv_accuracy,
				IFNULL(V.cla_descrip_sv_date,-1) AS cla_descrip_sv_date,
				IFNULL(V.cla_descrip_sv_hour,-1) AS cla_descrip_sv_hour,
				IFNULL(V.cla_descrip_sv_enddate,-1) AS cla_descrip_sv_enddate,
				IFNULL(V.cla_descrip_sv_endhour,-1) AS cla_descrip_sv_endhour,
				IFNULL(V.cla_indicador_count,-1) AS cla_indicador_count,
				IFNULL(V.cla_indicador_elapsed,-1) AS cla_indicador_elapsed
				FROM `si_sv_modelartus` V 
				INNER JOIN SI_SV_Survey S ON (V.id_survey = S.SurveyID)
				WHERE S.CreationVersion >= 6.0 and V.id_modelartus = {$id_modelartus}";

		$rs = $Repository->DataADOConnection->Execute($sql);
		
		if (!$rs)
		{
			$ErrMsg = $Repository->DataADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrMsg);
		}

		if($rs && !$rs->EOF)
		{
			$cla_concepto = $rs->fields['cla_concepto'];
			
			$aArtusModel = ARTUSModelFromSurvey::NewInstance($Repository, $id_modelartus, $rs);
			
			$aArtusModel->setGlobalDimensions();
			
// @EA 2015-07-31 Borramos las preguntas que no se deben de mapear
// @EA 2016-05-05 Se quita de la lista de dimensiones no soportadas a la QTypeID = 18 (Geolocalizacion)
			$sql = 
			"DELETE FROM si_sv_modelartus_questions WHERE id_modelartus = {$id_modelartus} and id_question IN
			(SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = {$aArtusModel->SurveyID} AND QTypeID IN (7,11,16,19,20,24))";
			if (!$Repository->DataADOConnection->Execute($sql))
			{
				$ErrSQL = $Repository->DataADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);				
			}
			
// @EA 2015-08-28 Habria que considerar tambien las secciones tipo inline, que en la pregunta seria negativa con la clave de la sección
			$sql = 
			"DELETE FROM si_sv_modelartus_questions WHERE id_modelartus = {$id_modelartus} and id_question > 0 and not id_question IN
			(SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = {$aArtusModel->SurveyID})";
			if (!$Repository->DataADOConnection->Execute($sql))
			{
				$ErrSQL = $Repository->DataADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);				
			}
			
			$sql = 
			"DELETE FROM si_sv_modelartus_questions WHERE id_modelartus = {$id_modelartus} AND id_question < 0 AND NOT id_question*-1 IN
			(SELECT SectionID FROM SI_SV_Section WHERE SurveyID = {$aArtusModel->SurveyID})";
			if (!$Repository->DataADOConnection->Execute($sql))
			{
				$ErrSQL = $Repository->DataADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);				
			}
//
			
// Dimensiones de tipo catalogo			
			$sql =
			"SELECT t1.QuestionID as questionid, t1.MemberID as memberid, t2.MemberName as membername
			 FROM
			(SELECT	t1.id_question AS QuestionID, CAST( MID(t1.`path`, 1, INSTR(t1.`path`, '[CAT]')-1) AS UNSIGNED) AS MemberID
			 FROM	si_sv_modelartus_questions AS t1
			 WHERE t1.id_modelartus = {$id_modelartus} AND t1.`path` LIKE '%[CAT]%') AS t1
				INNER JOIN si_sv_datasourcemember AS t2 ON (t2.MemberID = t1.MemberID)";

			$aCatalogMembers = array();
			
			$rsFields = $Repository->DataADOConnection->Execute($sql);
			if ($rsFields)
				while (!$rsFields->EOF)
				{
					$questionID = (int) $rsFields->fields['questionid'];
					$memberID = (int) $rsFields->fields['memberid'];
					$memberName = trim($rsFields->fields['membername']);
					
					$aCatalogMembers[$questionID][$memberID] = $memberName;

					$rsFields->MoveNext();
				}
			else
			{
				$ErrSQL = $Repository->DataADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}			
			
// Dimensiones e Indicadores
			$sql = 
			"SELECT 
					V.id,
					B.SectionName as sectionname, 
					B.SectionType as sectiontype, 
					A.MCInputType as inputtypeid,
					A.QTypeID as qtypeid, 
					A.QuestionID as questionid,
					A.QuestionText as attributename, 
					A.CatalogID as catalogid, 
					A.SectionID as sectionid,
					A.ValuesSourceType as sourcetype,
					IFNULL(V.cla_descrip,-1) AS cla_descrip, 
					IFNULL(V.cla_descrip_atributo,-1) AS cla_descrip_atributo, 
					IFNULL(V.cla_indicador, -1) AS cla_indicador,
					`path` AS path_ebavel,
					`type` AS tipo,
					A.QLength AS longitudalfa
			 FROM 	`si_sv_modelartus_questions` V
			 INNER JOIN SI_SV_Question A ON (A.QuestionID = V.id_question)
			 INNER JOIN SI_SV_Section B ON (A.SectionID = B.SectionID)
			 WHERE V.id_modelartus = {$id_modelartus} and A.CreationVersion >= 6.0 and `type` in ('IND','DIM')
			 ORDER BY B.SectionType, B.SectionID, V.id";
			$rsFields = $Repository->DataADOConnection->Execute($sql);

			$arrKeys = array();
			$aArtusModel->indicators = array();
			$slist_questions = '';
			if ($rsFields)
			while(!$rsFields->EOF)
			{
//				print '<br> Entro 2';

				$qtypeID = (int) $rsFields->fields['qtypeid'];
				$questionID = (int) $rsFields->fields['questionid'];
				$SectionType = (int) $rsFields->fields['sectiontype'];
					
				$TipoIndDim = strtoupper(trim($rsFields->fields['tipo']));
				
				$slist_questions .= ','.$questionID;
				
				if ($TipoIndDim == 'DIM')
				{
					$aArtusDimension = ARTUSDimensionFromSurvey::NewInstance($Repository, $id_modelartus, $cla_concepto, $rsFields);
					
					if ($aArtusDimension->id_member > 0 && !$aArtusDimension->renamed)
					{
						if (isset($aCatalogMembers[$questionID][$aArtusDimension->id_member]))
							$aArtusDimension->nom_dimension = $aCatalogMembers[$questionID][$aArtusDimension->id_member]." ({$aArtusDimension->nom_dimension})";
					}
					
					if($aArtusDimension)
					{
						$aArtusModel->aquestions[$questionID] = $aArtusDimension->cla_descrip;
						$aArtusModel->dimensions[] = $aArtusDimension;
					}
				}


//				print '<br>Section Type ['.$SectionType.'] QuestionType ['.$qtypeID.']';

				if ($TipoIndDim == 'IND' && ($qtypeID == qtpOpenNumeric OR $qtypeID == qtpCalc))
				{
// Seccion Standard + Tipo de Pregunta abierta Numerica = Indicador Base
//					print '<br>Indicator -> ['.$rsFields->fields['attributename'].']';
					
					$aArtusIndicator = ARTUSIndicatorFromSurvey::NewInstance($Repository, $id_modelartus, $cla_concepto, $rsFields);
					if($aArtusIndicator)
						$aArtusModel->indicators[] = $aArtusIndicator;
				}

				$rsFields->MoveNext();
			}
			
// @EA 2015-09-02 Soporte de Secciones Inline
			$sql =
			"SELECT 
				V.id,
				B.SectionName AS sectionname, 
				B.SectionType AS sectiontype, 
				0 AS inputtypeid,
				5 AS qtypeid, 
				V.id_question AS questionid,
				B.SectionName AS attributename, 
				B.CatalogID AS catalogid, 
				B.SectionID AS sectionid,
				B.ValuesSourceType AS sourcetype,
				IFNULL(V.cla_descrip,-1) AS cla_descrip, 
				IFNULL(V.cla_descrip_atributo,-1) AS cla_descrip_atributo, 
				IFNULL(V.cla_indicador, -1) AS cla_indicador,
				`path` AS path_ebavel,
				`type` AS tipo,
				255 AS longitudalfa
			FROM 	`si_sv_modelartus_questions` V
			INNER JOIN SI_SV_Section B ON (V.id_question*-1 = B.SectionID)
			WHERE V.id_modelartus = {$id_modelartus} AND B.SectionType = ".sectInline." AND `type` = 'DIM'
			ORDER BY B.SectionID, V.id";
			
			$rsFields = $Repository->DataADOConnection->Execute($sql);
			$slist_sections = '';
			if ($rsFields)
			while(!$rsFields->EOF)
			{
				$questionID = (int) $rsFields->fields['questionid'];
				$slist_sections .= ','.($questionID * -1);
				
				$aArtusDimension = ARTUSDimensionFromSurvey::NewInstance($Repository, $id_modelartus, $cla_concepto, $rsFields);
				
				if ($aArtusDimension->id_member > 0 && !$aArtusDimension->renamed)
				{
					if (isset($aCatalogMembers[$questionID][$aArtusDimension->id_member]))
					{
						if ($questionID > 0)
							$aArtusDimension->nom_dimension = $aCatalogMembers[$questionID][$aArtusDimension->id_member];
						else
							$aArtusDimension->nom_dimension = "Section Value ({$aArtusDimension->section_name}) -> ".$aCatalogMembers[$questionID][$aArtusDimension->id_member];
					}
				}
				elseif (!$aArtusDimension->renamed)
					$aArtusDimension->nom_dimension = "Section Value ({$aArtusDimension->section_name})";
				
				if($aArtusDimension)
					$aArtusModel->dimensions[] = $aArtusDimension;
					
				$rsFields->MoveNext();
			}

			if ($slist_questions != '')
				$aArtusModel->list_questions = substr($slist_questions, 1);
			else
				$aArtusModel->list_questions = '-1';
				
			if ($slist_sections != '')
				$aArtusModel->list_sections = substr($slist_sections, 1);
			else
				$aArtusModel->list_sections = '-1';

			if($rsFields)
				$rsFields->close();
				
//			var_dump($aArtusModel);
		}
		if($rs){
			$rs->close();
		}
		
		
		$sql = 
		"SELECT id, id_question, id_field, ebavel_table, ebavel_form
		 FROM si_sv_modelartus_mapping 
		 WHERE id_modelartus = {$id_modelartus}";
		$rs = $Repository->DataADOConnection->Execute($sql);
		
		if (!$rs)
		{
			$ErrMsg = $Repository->DataADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrMsg);
		}

		if($rs && !$rs->EOF)
		{
			$questionID = (int) $rs->fields['id_question'];
			$theMAPeBavel = ARTUSQuestionFromMAPeBavel::NewInstance($Repository, $id_modelartus, $rs);
			
			if ($theMAPeBavel)
				$aArtusModel->ebavelmap[$questionID] = $theMAPeBavel;
				
			$rs->MoveNext();
		}
		
		$sql = 
		"SELECT 
				V.id,
				B.SectionName as sectionname, 
				B.SectionType as sectiontype, 
				A.MCInputType as inputtypeid,
				A.QTypeID as qtypeid, 
				A.QuestionID as questionid,
				A.QuestionText as attributename, 
				A.CatalogID as catalogid, 
				A.SectionID as sectionid,
				A.ValuesSourceType as sourcetype,
				IFNULL(V.cla_descrip,-1) AS cla_descrip, 
				IFNULL(V.cla_descrip_atributo,-1) AS cla_descrip_atributo, 
				IFNULL(V.cla_indicador, -1) AS cla_indicador,
				`path` AS path_ebavel,
				`type` AS tipo,
				A.QLength AS longitudalfa
		 FROM 	`si_sv_modelartus_questions` V
		 INNER JOIN SI_SV_Question A ON (A.QuestionID = V.id_question)
		 INNER JOIN SI_SV_Section B ON (A.SectionID = B.SectionID)
		 WHERE V.id_modelartus = {$id_modelartus} and A.CreationVersion >= 6.0 and `type` = ''
		 ORDER BY B.SectionType, B.SectionID, V.id";
		$rsFields = $Repository->DataADOConnection->Execute($sql);
		
		if ($rsFields)
		while(!$rsFields->EOF)
		{
			$questionID = (int) $rsFields->fields['questionid'];
			
			$aArtusDimension = ARTUSDimensionFromSurvey::NewInstance($Repository, $id_modelartus, $cla_concepto, $rsFields);
			if($aArtusDimension)
			{
				if (array_key_exists($questionID, $aArtusModel->ebavelmap))
				{
					$idForm = $aArtusModel->ebavelmap[$questionID]->ebavel_form;
					$nom_tabla = $aArtusModel->ebavelmap[$questionID]->ebavel_table;
					$path = $aArtusDimension->path_ebavel;
					
//					print '<br>path -> ['.$path.']';
//					print '<br>$idForm -> ['.$idForm.']';
//					print '<br>$nom_tabla -> ['.$nom_tabla.']';
					
					$aJoins = $aArtusModel->parseFieldPath($path, $idForm, $nom_tabla, $Repository->ADOConnection);

//					print '<br>';
					$label = $aJoins['field']['label'];
					if (!$aArtusDimension->renamed)
						$aArtusDimension->nom_dimension = $label;

/*						
					if (count($aJoins['joins']) > 0)
					{
						print '<br>SI Tiene JOINS';
					}
					else
						print '<br>NO Tiene JOINS';
*/						

//					print '<br>label -> ['.$label.']';
//					print '<br>';

					$aArtusDimension->aJoins = $aJoins;
					$aArtusModel->ebaveldims[] = $aArtusDimension;
				}
				else
					savelogfile("No exists QuestionID={$questionID} in si_sv_modelartus_mapping where id_modelartus={$id_modelartus}");
			}
			
			$rsFields->MoveNext();
		}

		return $aArtusModel;
	}

	public function renameModel()
	{
		if ($this->cla_concepto > 0)
		{
			$nom_modelo = $this->Repository->ADOConnection->quote($this->name);

			$sql = 
			"update	SI_CONCEPTO set 
				NOM_CONCEPTO = {$nom_modelo}
			 where  CLA_CONCEPTO = {$this->cla_concepto}";
			 
			if (!$this->Repository->ADOConnection->Execute($sql))
			{
				$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}
			
			$sql = 
			"select LLAVES as cla_gpo_ind
			 from	SI_CONCEPTO
			 where	CLA_CONCEPTO = {$this->cla_concepto}";
			
			$rst = $this->Repository->ADOConnection->Execute($sql);
			if (!$rst)
			{
				$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}
			else
			{
				$cla_gpo_ind = (int) $rst->fields['cla_gpo_ind'];
				$sql = 
				"update	SI_GPO_IND
				 set	NOM_GPO_IND = {$nom_modelo}
				 where	CLA_GPO_IND = {$cla_gpo_ind}";
				 
				if (!$this->Repository->ADOConnection->Execute($sql))
				{
					$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}
			}
		}		
	}
	
	public function checkAgendaDimension()
	{
// @EA 2015-08-31 Soporte de dimension agenda
		$sql = 
		"select IFNULL(cla_descrip_agenda, -1) as cla_descrip_agenda
		 from si_sv_modelartus 
		 where id_modelartus = {$this->id_modelartus}";
		$rst = $this->Repository->DataADOConnection->Execute($sql);
		if (!$rst)
		{
			$ErrSQL = $this->Repository->DataADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
		if (!$rst->EOF)
		{
			$this->cla_descrip_agenda = $rst->fields['cla_descrip_agenda'];
			
			if ($this->cla_descrip_agenda <= 0)
			{
				$sql = 
				"alter table sveForms_{$this->SurveyID} add AgendaID INT NULL";
				if (!$this->Repository->DataADOConnection->Execute($sql))
				{
					$ErrSQL = $this->Repository->DataADOConnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}

				$dimensionGroup = 'SurveyID';
				$dimensionName = 'Agenda';
				$dimensionID = $this->cla_descrip_agenda;
				$dimensionID = $this->getDimClaDescrip($dimensionName);
				
				$this->cla_descrip_agenda = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);
				
				$this->ExistsChanges = true;
				
				$sql = 
				"UPDATE si_sv_modelartus SET 
				cla_descrip_agenda = {$this->cla_descrip_agenda}
				WHERE id_modelartus = {$this->id_modelartus}";
				if($this->Repository->DataADOConnection->Execute($sql) === false){
					$serr = $this->Repository->DataADOConnection->ErrorMsg();
					savelogfile($serr);
				}
				
				$sql = 
				"select RIDIM_{$this->cla_descrip_agenda}KEY as agendakey
				 from	RIDIM_{$this->cla_descrip_agenda}
				 where  DSC_{$this->cla_descrip_agenda} = 'NA'";
				 
				$rst = $this->Repository->DataADOConnection->Execute($sql);
				if (!$rst)
				{
					$ErrSQL = $this->Repository->DataADOConnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}
				elseif (!$rst->EOF)
				{
					$agendakey = $rst->fields['agendakey'];
					$sql =
					"update RIFACT_{$this->cla_concepto} set RIDIM_{$this->cla_descrip_agenda}KEY = {$agendakey} where RIDIM_{$this->cla_descrip_agenda}KEY is null";
					if($this->Repository->DataADOConnection->Execute($sql) === false){
						$serr = $this->Repository->DataADOConnection->ErrorMsg();
						savelogfile($sql.' -> '.$serr);
					}
				}
			}
		}
//
	}
	
	public function save()
	{
		if (isset($_SESSION["PABITAM_UserID"]))
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		if (isset($_SESSION["PABITAM_UserName"]))
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		if (isset($_SESSION["PABITAM_RepositoryName"]))
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		global $generateXLS, $generatePHP, $PHPSincronize;
		$generateXLS = false;
		$generatePHP = false;
		$PHPSincronize = false;
		
		if($this->cla_concepto <= 0)
		{
// Crear el modelo de Artus

			$this->ExistsChanges = true;

			$strOriginalWD = getcwd();
			$anInstanceModel = BITAMModel::NewModel($this->Repository);
			$anInstanceModel->ModelName = $this->name;
			$anInstanceModel->readPeriods();

			@$anInstanceModel->ri_definition = CREATED_BY_EFORMSV6;
			@$anInstanceModel->use_cache = false;

			$objMMAnswer = @$anInstanceModel->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding survey's model").": ".$strMMError.". ";
				savelogfile($gblModMngrErrorMessage);
			}
			else				
			{
				$this->cla_concepto = $anInstanceModel->ModelID;
				$fact_table = $anInstanceModel->nom_tabla;
				
// Agregamos las dimensiones globales al modelo
				if ($this->addSystemDimensionsAndIndicators()) {
//Actualizar el campo cla_concepto de la metadata de ebavel donde se configuro el modelo de artus
					$sql = 
					"UPDATE si_sv_modelartus SET 
					cla_concepto = {$this->cla_concepto},
					cla_descrip_user = {$this->cla_descrip_user},
					cla_descrip_country = {$this->cla_descrip_country},
					cla_descrip_state = {$this->cla_descrip_state},
					cla_descrip_city = {$this->cla_descrip_city},
					cla_descrip_zipcode = {$this->cla_descrip_zipcode},
					cla_descrip_address = {$this->cla_descrip_address},
					cla_descrip_version = {$this->cla_descrip_version},
					cla_descrip_agenda = {$this->cla_descrip_agenda},
					cla_descrip_sv = {$this->cla_descrip_sv},
					cla_descrip_sv_latitude = {$this->cla_descrip_sv_latitude},
					cla_descrip_sv_longitude = {$this->cla_descrip_sv_longitude},
					cla_descrip_sv_accuracy = {$this->cla_descrip_sv_accuracy},
					cla_descrip_sv_date = {$this->cla_descrip_sv_date},
					cla_descrip_sv_hour = {$this->cla_descrip_sv_hour},
					cla_descrip_sv_enddate = {$this->cla_descrip_sv_enddate},
					cla_descrip_sv_endhour = {$this->cla_descrip_sv_endhour},
					cla_indicador_count = {$this->cla_indicador_count},
					cla_indicador_elapsed = {$this->cla_indicador_elapsed}
					WHERE id_modelartus = {$this->id_modelartus}";
					if($this->Repository->DataADOConnection->Execute($sql) === false){
						$serr = $this->Repository->DataADOConnection->ErrorMsg();
						savelogfile($serr);
					}
					$this->addQuestionsInSurvey(true);
				}
			}
		}
		else
		{
			$this->checkAgendaDimension();
			$this->addQuestionsInSurvey();
		}
		
//		$this->ExistsChanges = true;
		if ($this->ExistsChanges)
		{
// Generar dependencias correspondientes
			$this->generateDepends();
//			print '<br>ExistsChanges';
			$this->generateSP_Global();
//			print '<br>generateSP_Global..';
			$this->generateSP_ETL($this->Repository, $this->SurveyID);
//			print '<br>generateSP_ETL..';
		}

		$this->saveFromEBavel();
		
		if ($this->cla_concepto > 0)
		{
			$generateXLS = false;
			$generatePHP = true;
			$PHPSincronize = true;
			@ExportToPHP($this->Repository, $this->cla_concepto);
		}
	}

	public function saveFromEBavel()
	{
// @EA 2015-10-01 Mapeo de dimensiones hacia eBavel
		$tipo_dato = 2;
		$long_dato = 255;
		$cla_concepto = $this->cla_concepto;
		$metaconnection = $this->Repository->ADOConnection;
		$database = $this->Repository->ADODBDatabase;
		foreach ($this->ebaveldims as $adimension)
		{
//			print '<br><-----<br>';
//			print_r($adimension->aJoins);
			$id_question = $adimension->id_question;
			$cla_descrip_ebavel = $adimension->cla_descrip;
			
			$bValida = false;
			if (array_key_exists('joins', $adimension->aJoins) && array_key_exists('field', $adimension->aJoins))
				if (isset($adimension->aJoins['field']['table']) && isset($adimension->aJoins['field']['fieldValue'])
					&& isset($adimension->aJoins['root']['table']))
					$bValida = true;
// Es necesario que exista la preguna mapeada a eBavel como dimension del modelo			
			if (isset($this->aquestions[$id_question]) && $cla_descrip_ebavel <= 0 && $bValida)
			{
				$cla_descrip = $this->aquestions[$id_question];
				$njoins = count($adimension->aJoins['joins']);
				$dimensionGroup = 'eBavel';
				$dimensionName = $adimension->nom_dimension;
				$nom_tabla_ebavel = $database.'.'.$adimension->aJoins['field']['table'];
				$nom_fisico_ebavel = $adimension->aJoins['field']['fieldValue'];
				if ($njoins == 0)
				{
					$consecutivo_padre = -1;
					$nom_fisicok = '';
					if ($this->getJoinDimension($metaconnection, $cla_descrip, $cla_concepto, $consecutivo_padre, $nom_fisicok))
					{
						$this->existsEBavelDimension($metaconnection, $nom_tabla_ebavel, $nom_fisico_ebavel, $cla_descrip_ebavel);
						if ($cla_descrip_ebavel <= 0)
						{
							$cla_descrip_ebavel = $this->getMaxClaDescrip($metaconnection);
							$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip_ebavel, $dimensionName, $nom_tabla_ebavel, $nom_fisico_ebavel, $tipo_dato, $long_dato, $dimensionGroup);
						}
						
						if ($cla_descrip_ebavel > 0)
						{
							$arrMax = ARTUSDimensionFromSurvey::getMaxConsecutivoNivel($cla_concepto, $metaconnection);
							$consecutivo_hijo = $arrMax["consecutivo"];
							$nivel_hijo = $arrMax["nivel"];
							
							$par_descriptor	= $consecutivo_hijo.'|'.$consecutivo_padre;
							$ErrSQL = $this->insertLocalDimension($metaconnection, $consecutivo_hijo, $nivel_hijo, $cla_descrip_ebavel, $dimensionName, $nom_fisico_ebavel, $tipo_dato, $long_dato, $dimensionGroup, $cla_concepto, $par_descriptor);
							
							$adimension->cla_descrip = $cla_descrip_ebavel;
							$adimension->saveSincronize();
							
							$nom_fisico_ebavel = $this->ebavelmap[$id_question]->id_field;
							$this->insertSI_DESCRIP_KEYS($metaconnection, $nom_tabla_ebavel, $nom_fisico_ebavel, $nom_fisicok, $cla_concepto);
						}
					}
				}
				else
				{
// Solo hacemos el recorrido sobre el ultimo JOIN					
					for ($nvcc=($njoins-1); $nvcc < $njoins; $nvcc++)
					{
						if ($nvcc == 0)
							$masterTable = $database.'.'.$adimension->aJoins['root']['table'];
						else
							$masterTable = $database.'.'.$adimension->aJoins['joins'][$nvcc-1]['table'];

						$masterField = $adimension->aJoins['joins'][$nvcc]['masterField'];						
						$detailField = $adimension->aJoins['joins'][$nvcc]['detailField'];

/*
						print '<br>$masterTable ['.$masterTable.']';
						print '<br>$masterField ['.$masterField.']';
						print '<br>$detailField ['.$detailField.']';
*/						

						$sql =
						"SELECT MIN(t1.CONSECUTIVO) as consecutivo
						FROM	SI_CPTO_LLAVE AS t1 INNER JOIN SI_DESCRIP_ENC AS t2 ON (t2.CLA_DESCRIP = t1.CLA_DESCRIP)
						WHERE	t1.CLA_CONCEPTO = {$cla_concepto} AND NOM_TABLA = '{$masterTable}'";
						
						$rst = $metaconnection->Execute($sql);
						if (!$rst)
						{
							$ErrSQL = $metaconnection->ErrorMsg();
							savelogfile($sql.' -> '.$ErrSQL);
						}
						else
						{
							$consecutivo_padre = (int) $rst->fields['consecutivo'];
							$this->existsEBavelDimension($metaconnection, $nom_tabla_ebavel, $nom_fisico_ebavel, $cla_descrip_ebavel);
							if ($cla_descrip_ebavel <= 0)
							{
								$cla_descrip_ebavel = $this->getMaxClaDescrip($metaconnection);
								$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip_ebavel, $dimensionName, $nom_tabla_ebavel, $nom_fisico_ebavel, $tipo_dato, $long_dato, $dimensionGroup);
							}
							
							if ($cla_descrip_ebavel > 0)
							{
								$arrMax = ARTUSDimensionFromSurvey::getMaxConsecutivoNivel($cla_concepto, $metaconnection);
								$consecutivo_hijo = $arrMax["consecutivo"];
								$nivel_hijo = $arrMax["nivel"];
								
								$par_descriptor	= $consecutivo_hijo.'|'.$consecutivo_padre;
								$ErrSQL = $this->insertLocalDimension($metaconnection, $consecutivo_hijo, $nivel_hijo, $cla_descrip_ebavel, $dimensionName, $nom_fisico_ebavel, $tipo_dato, $long_dato, $dimensionGroup, $cla_concepto, $par_descriptor);
								
								$adimension->cla_descrip = $cla_descrip_ebavel;
								$adimension->saveSincronize();
								
								$this->insertSI_DESCRIP_KEYS($metaconnection, $nom_tabla_ebavel, $detailField, $masterField, $cla_concepto);
							}
						}
					}
				}
			}
			
		}
	}

	static function getJoinDimension(&$metaconnection, $cla_descrip, $cla_concepto, &$consecutivo, &$nom_fisicok)
	{
		$sql = 
		"select	t1.NOM_FISICOK as clave, t1.NOM_TABLA as tabla, t2.CONSECUTIVO as consecutivo 
		from	SI_DESCRIP_ENC as t1 inner join SI_CPTO_LLAVE as t2 on (t2.CLA_DESCRIP = t1.CLA_DESCRIP)
		where 	t1.CLA_DESCRIP = {$cla_descrip} and t2.CLA_CONCEPTO = {$cla_concepto}";
		$rst = $metaconnection->Execute($sql);
		if ($rst && !$rst->EOF)
		{
			$consecutivo = (int) $rst->fields['consecutivo'];
			$nom_fisicok = trim($rst->fields['clave']);
			return true;
		}
		return false;
	}
	
	static function existsEBavelDimension(&$metaconnection, $nom_tabla, $nom_fisico, &$cla_descrip)
	{
		$sql = "select	CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}' and NOM_FISICO = '{$nom_fisico}'";
		ARTUSModelFunctionBasics::existRow($metaconnection, $sql, $cla_descrip);
	}

	function dropModelAttribute($cla_descrip_parent, $cla_descrip)
	{
		$cla_concepto = $this->cla_concepto;
		$strOriginalWD = getcwd();
		
		$anInstanceAttrib = BITAMAttribDimension::NewAttribDimensionWithDimensionID($this->Repository, $cla_concepto, $cla_descrip_parent, -1, $cla_descrip);
		$anInstanceModelDim->Dimension->CheckStages = false;
		@$anInstanceModelDim->Dimension->remove();

		chdir($strOriginalWD);
		
		$strMMError = identifyModelManagerError($objMMAnswer);
		if ($strMMError != '') {
			$sErr = "\r\n(".__METHOD__.") ".translate("Error creating {$AttribName} dimension").": ".$strMMError.". ";
			savelogfile($sErr);
			return false;
		}
		
		return true;
	}

	public static function getSQLValuesGlobalDimension($cla_descrip, $nom_dimension)
	{
		$sql = '';
		if ($cla_descrip > 0)
			$sql =
			"SELECT RIDIM_{$cla_descrip}KEY INTO v{$nom_dimension}ID FROM RIDIM_{$cla_descrip} WHERE DSC_{$cla_descrip} = v{$nom_dimension};
			 IF ROW_COUNT() = 0 THEN
				INSERT INTO RIDIM_{$cla_descrip} (DSC_{$cla_descrip}) VALUES (v{$nom_dimension});
				SELECT RIDIM_{$cla_descrip}KEY INTO v{$nom_dimension}ID FROM RIDIM_{$cla_descrip} WHERE DSC_{$cla_descrip} = v{$nom_dimension};
			 END IF;
			 ";
		 return $sql;
	}

	public function generateSP_Global()
	{
		$sql = 
		"drop procedure IF EXISTS sp_sincronize_global_dimensions_esurveyV6";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql =
		"CREATE PROCEDURE sp_sincronize_global_dimensions_esurveyV6 (
		IN vUser VARCHAR(255), IN vCountry VARCHAR(255), IN vState VARCHAR(255), IN vCity VARCHAR(255), IN vZIP VARCHAR(255), IN vAddress VARCHAR(255), IN vVersion VARCHAR(255), IN vAgenda VARCHAR(255), 
		OUT vUserID INT, OUT vCountryID INT, OUT vStateID INT, OUT vCityID INT, OUT vZIPID INT, OUT vAddressID INT, OUT vVersionID INT, OUT vAgendaID INT)
		BEGIN
		";

		$sql .= $this->getSQLValuesGlobalDimension($this->cla_descrip_user, 'User');
		$sql .= $this->getSQLValuesGlobalDimension($this->cla_descrip_country, 'Country');
		$sql .= $this->getSQLValuesGlobalDimension($this->cla_descrip_state, 'State');
		$sql .= $this->getSQLValuesGlobalDimension($this->cla_descrip_city, 'City');
		$sql .= $this->getSQLValuesGlobalDimension($this->cla_descrip_zipcode, 'ZIP');
		$sql .= $this->getSQLValuesGlobalDimension($this->cla_descrip_address, 'Address');
		$sql .= $this->getSQLValuesGlobalDimension($this->cla_descrip_version, 'Version');
		$sql .= $this->getSQLValuesGlobalDimension($this->cla_descrip_agenda, 'Agenda');
		$sql .= "
		END;";
		
		if (!$this->Repository->DataADOConnection->Execute($sql))
		{
			$serr = $this->Repository->DataADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$serr);
		}
	}

	public static function makeSectionUpdate(&$aFrom, $aSet)
	{
		$sql = '';
		foreach ($aFrom as $sectionID => $innerJoin)
		{
			$nom_tabla = PRE_DIM_SEC_TABLE.$sectionID;
			
			$ssection_update = "
			update {$nom_tabla} as t1
			{$innerJoin}
			SET $aSet[$sectionID]
			WHERE t1.RIVDIM_KEY = pSurveyKey;
			";

			$sql .= $ssection_update;
		}
		return $sql;
	}

	public static function makeSectionInsertSelect(&$aInsert, &$aSelect, &$aFrom, &$aDefaults, $id_survey)
	{
		$sql = '';
		foreach ($aInsert as $sectionID => $fields)
		{
			$nom_tabla = PRE_DIM_SEC_TABLE.$sectionID;
			$ssection_insert = "
			DELETE FROM {$nom_tabla} WHERE RIVDIM_KEY = pSurveyKey;
			";
			$sinsert_defaults = "INSERT INTO {$nom_tabla} ( RIVDIM_KEY{$fields}) VALUES (pSurveyKey{$aDefaults[$sectionID]});";
			$ssection_insert .= "INSERT INTO {$nom_tabla} ( RIVDIM_KEY, RISDIM_KEY{$fields})";
			$values = $aSelect[$sectionID];
			$innerjoin = '';
			if (isset($aFrom[$sectionID]))
				$innerjoin = $aFrom[$sectionID];
			$ssection_select = "
			SELECT t1.SurveyKey, t1.SectionKey{$values}
			from sveforms_{$id_survey}_section_{$sectionID} as t1{$innerjoin}
			WHERE t1.SurveyKey = pSurveyKey;
			
			IF ROW_COUNT() = 0 THEN
				{$sinsert_defaults}
			END IF;			
			";
			$sql .= $ssection_insert.$ssection_select;
		}
		return $sql;
	}
	
	public static function makeCatalogInsertSelect(&$aInsert, &$aSelect, &$aDefaults, $id_survey)
	{
		$sql = '';
		foreach ($aInsert as $questionID => $fields)
		{
			$nom_tabla = PRE_DIM_CAT_TABLE.$questionID;
			$ssection_insert = "
			DELETE FROM {$nom_tabla} WHERE RIVDIM_KEY = pSurveyKey;
			";
			$sinsert_defaults = "INSERT INTO {$nom_tabla} ( RIVDIM_KEY{$fields}) VALUES (pSurveyKey{$aDefaults[$questionID]});";
			$ssection_insert .= "INSERT INTO {$nom_tabla} ( RIVDIM_KEY, RICDIM_KEY{$fields})";
			$values = $aSelect[$questionID];
			$ssection_select = "
			SELECT SurveyKey, QuestionKey{$values}
			from sveforms_{$id_survey}_questiondet_{$questionID}
			WHERE SurveyKey = pSurveyKey;
			
			IF ROW_COUNT() = 0 THEN
				{$sinsert_defaults}
			END IF;
			";
			$sql .= $ssection_insert.$ssection_select;
		}
		return $sql;
	}
	
	public static function IsImage($questionType)
	{
		//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		return ($questionType == qtpPhoto || $questionType == qtpSignature || $questionType == qtpSketch || $questionType == qtpSketchPlus);
		//@JAPR
	}
	
	public static function IsDoc($questionType)
	{
		return ($questionType == qtpDocument || $questionType == qtpAudio || $questionType == qtpVideo);
	}

	public static function makeDimensionsAndIndicatorsFromQuestions(&$Repository, $id_survey)
	{
// Obtener la carpeta de ESurvey para armado de paths (imagenes y documentos)
	    $path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
	    $strScriptPath = $path_parts["dirname"];
	    $aDir = explode('/',$strScriptPath);
	    $nindexDir = count($aDir)-1;
	    $folderSurvey = "";
	    if (isset($aDir[$nindexDir]))
	    	$folderSurvey = $aDir[$nindexDir];
	    else
	    	$folderSurvey = 'ESurveyV6';
//	    	

		$dataconn = $Repository->DataADOConnection;
		
		$nom_tabla_std = PRE_DIM_SVY_TABLE.$id_survey;
		$key_std = PRE_DIM_SVY_TABLE."KEY";
		
		$stdtar = '';
		$stdsrc = '';
		
		$sql_delete_std = 
		"
		delete from {$nom_tabla_std} where {$key_std} = pSurveyKey;
		";
		
		$sql_insert_std = 
		"INSERT INTO {$nom_tabla_std} ( {$key_std}, latitude_sv, longitude_sv, accuracy_sv, date_sv, hour_sv, dateend_sv, hourend_sv";
		$sql_values_std =
		"SELECT t1.SurveyKey, t1.Latitude, t1.Longitude, t1.Accuracy, t1.StartDate, t1.StartTime, t1.EndDate, t1.EndTime";
		
		$sql_from_std =
		"FROM 	SVeForms_{$id_survey} AS t1 LEFT JOIN SVeForms_{$id_survey}_SectionStd AS t2 ON (t2.SurveyKey = t1.SurveyKey)
		 WHERE 	t1.SurveyKey = pSurveyKey;";
		 
		 $sql_update_std = 
		 "
		 UPDATE	{$nom_tabla_std} as t1 ";
		 
		 $sql_update_std_innerjoin = array();
		 $sql_update_std_set = array();
		 
		 $sql_update_std_innerjoin[0] = "";
		 $sql_update_std_set[0] = "";
		 $ncont_std_innerjoin = 0;
		 $nblock50 = 0;
		 
		 $sql_update_std_where = "
		 where t1.RIVDIM_KEY = pSurveyKey;
		 ";
		 
		 $asql_mul = array();
		 $asql_mul_md = array();
		 $asql_sec_insert = array();
		 $asql_sec_select = array();
		 $asql_sec_defaults = array();
		 $asql_sec_from = array();
		 $asql_cat_insert = array();
		 $asql_cat_select = array();
		 $asql_cat_defaults = array();
		 
		 $asql_sec_update_sc = array();
		 $asql_sec_from_sc = array();
		
// Dimensiones normales
		$sql =
		"SELECT DISTINCT
					B.SectionType AS sectiontype, 
					A.MCInputType AS inputtypeid,
					A.QTypeID AS qtypeid, 
					A.QuestionID AS questionid,
					A.SectionID AS sectionid
			 FROM 	`si_sv_modelartus_questions` V
			 INNER JOIN si_sv_modelartus C ON (C.id_modelartus = V.id_modelartus)
			 INNER JOIN SI_SV_Question A ON (A.QuestionID = V.id_question)
			 INNER JOIN SI_SV_Section B ON (A.SectionID = B.SectionID)
			 WHERE A.SurveyID = {$id_survey} AND `type` IN ('DIM')
				AND C.cla_concepto > 0
				AND V.cla_descrip > 0
				AND NOT V.`path` LIKE '%[CAT]%'
			 ORDER BY B.SectionType, B.SectionID";
			 
		$rst = $dataconn->Execute($sql);
		
		if ($rst)
		{
			while (!$rst->EOF)
			{
				$sectionType = $rst->fields['sectiontype'];
				$sectionID = $rst->fields['sectionid'];
				$inputType = $rst->fields['inputtypeid'];
				$questionType = $rst->fields['qtypeid'];
				$questionID = $rst->fields['questionid'];

				if ($sectionType == sectNormal)
				{
					if ($questionType == qtpSingle)
					{
// @EA 2017-06-06 Soportar más de 50 joins en update, partiendolo en varios updates de ser necesario
						$ncont_std_innerjoin++;
						$nblock50 = floor($ncont_std_innerjoin / 51);
						
						if (!isset($sql_update_std_innerjoin[$nblock50]))
						{
							$sql_update_std_innerjoin[$nblock50] = '';
							$sql_update_std_set[$nblock50] = '';
						}

						$sql_update_std_innerjoin[$nblock50] .= 
						"LEFT JOIN sveforms_{$id_survey}_questiondet_{$questionID} as q{$questionID} on (q{$questionID}.SurveyKey = t1.RIVDIM_KEY)
						";
						
						if ($sql_update_std_set[$nblock50] != '') $coma = '
						,';
						else	$coma = '
						SET ';
						
						$sql_update_std_set[$nblock50] .= 
						"{$coma}t1.DSCQ_{$questionID} = IFNULL(NULLIF(TRIM(q{$questionID}.QuestionDesc_{$questionID}), ''), 'NA'), t1.SCOREQ_{$questionID} = q{$questionID}.QuestionPts_{$questionID}";
						
					}
/*					
					elseif ($questionType == qtpCallList)
					{
						$sql_update_std_innerjoin .= 
						"LEFT JOIN sveforms_{$id_survey}_questiondet_{$questionID} as q{$questionID} on (q{$questionID}.SurveyKey = t1.RIVDIM_KEY)
						";
						
						if ($sql_update_std_set != '') $coma = '
						,';
						else	$coma = '
						SET ';
						
						$sql_update_std_set .= 
						"{$coma}t1.DSCQ_{$questionID} = IFNULL(NULLIF(TRIM(q{$questionID}.QuestionDesc_{$questionID}), ''), 'NA')";
					}
*/					
					elseif ($questionType == qtpMulti)
					{
						$nom_tabla = PRE_DIM_MPC_TABLE.$questionID;
						$smulti_insert = "
						DELETE FROM {$nom_tabla} WHERE RIVDIM_KEY = pSurveyKey;
						";
						
						$smulti_insert .= "INSERT INTO {$nom_tabla} ( RIVDIM_KEY, RIMDIM_KEY, DSCQ_{$questionID}, SCOREQ_{$questionID}";
						
						$smulti_select = "SELECT SurveyKey, QuestionKey, QuestionDesc_{$questionID}, QuestionPts_{$questionID}";

						$smulti_insert_defaults = "INSERT INTO {$nom_tabla} ( RIVDIM_KEY, RIMDIM_KEY, DSCQ_{$questionID}";
						$smulti_select_defaults = " VALUES ( pSurveyKey, 1, 'NA'";
						
						if ($inputType == mpcNumeric)
							$sdefault = '0';
						else
							$sdefault = '\'NA\'';
						
						if ($inputType == mpcNumeric || $inputType == mpcText)
						{
							$smulti_insert .= ", MPCQ_{$questionID}";
							$smulti_select .= ", QuestionVal_{$questionID}";
							$smulti_insert_defaults .= ", MPCQ_{$questionID} )";
							$smulti_select_defaults .= ", {$sdefault} );";
						}
						else
						{
							$smulti_insert_defaults .= " )";
							$smulti_select_defaults .= " );";
						}
							
						$smulti_select .= "
						FROM sveforms_{$id_survey}_questiondet_{$questionID}
						WHERE SurveyKey = pSurveyKey;
						
						IF ROW_COUNT() = 0 THEN
							{$smulti_insert_defaults} {$smulti_select_defaults}
						END IF;
						";
						$asql_mul[] = $smulti_insert.')
						'.$smulti_select;
					}
					else
					{
// Todas estas preguntas van como atributos de la dimension principal de la encuesta
// (1)
						$field_target = PRE_DIM_DSC_FIELD.$questionID;

						if (ARTUSModelFromSurvey::isImage($questionType))
							$field_source = "concat('{$folderSurvey}/surveyimages/{$Repository->ADODBDatabase}/', t2.QuestionImg_{$questionID})";
						elseif (ARTUSModelFromSurvey::isDoc($questionType))
							$field_source = "concat('{$folderSurvey}/surveydocuments/{$Repository->ADODBDatabase}/', t2.QuestionDoc_{$questionID})";
						else
							$field_source = 't2.QuestionDesc_'.$questionID;
						
						if ($questionType == qtpGPS || $questionType == qtpMyLocation)
						{
							$stdtar .= ', '.$field_target.'LAT, '.$field_target.'LON';
							$stdsrc .= ', t2.QuestionLat_'.$questionID.', '.'t2.QuestionLong_'.$questionID;
						}
						else
						{
							$stdtar .= ', '.$field_target;	
							$stdsrc .= ", IFNULL(NULLIF(TRIM({$field_source}), ''), 'NA')";
						}
					}
				}
//				elseif ($sectionType != sectInline)
// @EA 2015-08-25 Quite la restriccion en secciones de tipo inline (equivale a una maestro detalle)
				else
				{
// Secciones Inline y Maestro Detalle tienen que compartir tablas de dimension x seccionID
					if ($questionType == qtpMulti)
					{
						$nom_tabla = PRE_DIM_WPC_TABLE.$questionID;
						$smulti_insert = "
						DELETE FROM {$nom_tabla} WHERE RIVDIM_KEY = pSurveyKey;
						";
						
						$smulti_insert .= "INSERT INTO {$nom_tabla} ( RIVDIM_KEY, RISDIM_KEY, RIWDIM_KEY, DSCQ_{$questionID}, SCOREQ_{$questionID}";
						
						$smulti_select = "SELECT t1.SurveyKey, t1.SectionKey, IFNULL(t2.QuestionKey, 1), IFNULL(NULLIF(TRIM(t2.QuestionDesc_{$questionID}), ''), 'NA'), QuestionPts_{$questionID}";

						if ($inputType == mpcNumeric)
							$sdefault = '0';
						else
							$sdefault = '\'NA\'';
						
						if ($inputType == mpcText)
						{
							$smulti_insert .= ", MPCQ_{$questionID}";
							$smulti_select .= ", IFNULL(NULLIF(TRIM(t2.QuestionVal_{$questionID}), ''), {$sdefault})";
						} 
						elseif ($inputType == mpcNumeric)
						{
							$smulti_insert .= ", MPCQ_{$questionID}";
							$smulti_select .= ", IFNULL(t2.QuestionVal_{$questionID}, {$sdefault})";
						}
							
						$smulti_select .= "
						FROM sveforms_{$id_survey}_section_{$sectionID} AS t1 LEFT JOIN sveforms_{$id_survey}_questiondet_{$questionID} AS t2 ON (t2.SectionKey = t1.SectionKey)
						WHERE t1.SurveyKey = pSurveyKey;
						
						";
						$asql_mul_md[] = $smulti_insert.')
						'.$smulti_select;						
					}
					else
					{
// Aqui la clave de la seccion formará parte del nombre de la tabla que compartira todas las dimensiones de esa seccion
// (3)
//							$adimension->cla_descrip = $adimension->saveSectionDimension($this->cla_concepto, $this->cla_descrip_sv, $cla_descrip, $dimensionGroup, $this->SurveyID);
// OJO AQUI CONSIDERAR CASOS DE SIMPLE CHOICE
						$field_target = PRE_DIM_DSC_FIELD.$questionID;
						$score_target = PRE_IND_SCORE.$questionID;

						if ($questionType == qtpSingle)
						{
							$field_source = 'QuestionDesc_'.$questionID;
							if (isset($asql_sec_from_sc[$sectionID]))
							{
								$asql_sec_from_sc[$sectionID] .= "
								LEFT JOIN sveforms_{$id_survey}_questiondet_{$questionID} AS q{$questionID} ON (q{$questionID}.SectionKey = t1.RISDIM_KEY)";
								$asql_sec_update_sc[$sectionID] .= "
								,t1.{$field_target} = IFNULL(NULLIF(TRIM(q{$questionID}.{$field_source}), ''), 'NA'), t1.{$score_target} = q{$questionID}.QuestionPts_{$questionID}";
							}
							else
							{
								$asql_sec_from_sc[$sectionID] = "
								LEFT JOIN sveforms_{$id_survey}_questiondet_{$questionID} AS q{$questionID} ON (q{$questionID}.SectionKey = t1.RISDIM_KEY)";
								$asql_sec_update_sc[$sectionID] = "
								t1.{$field_target} = IFNULL(NULLIF(TRIM(q{$questionID}.{$field_source}), ''), 'NA'), t1.{$score_target} = q{$questionID}.QuestionPts_{$questionID}";
							}
						}
/*						
						elseif ($questionType == qtpCallList)
						{
							$field_source = 'QuestionDesc_'.$questionID;
							if (isset($asql_sec_from_sc[$sectionID]))
							{
								$asql_sec_from_sc[$sectionID] .= "
								LEFT JOIN sveforms_{$id_survey}_questiondet_{$questionID} AS q{$questionID} ON (q{$questionID}.SectionKey = t1.RISDIM_KEY)";
								$asql_sec_update_sc[$sectionID] .= "
								,t1.{$field_target} = IFNULL(NULLIF(TRIM(q{$questionID}.{$field_source}), ''), 'NA')";
							}
							else
							{
								$asql_sec_from_sc[$sectionID] = "
								LEFT JOIN sveforms_{$id_survey}_questiondet_{$questionID} AS q{$questionID} ON (q{$questionID}.SectionKey = t1.RISDIM_KEY)";
								$asql_sec_update_sc[$sectionID] = "
								t1.{$field_target} = IFNULL(NULLIF(TRIM(q{$questionID}.{$field_source}), ''), 'NA')";
							}							
						}
*/						
						else
						{
							$defaults = ", 'NA'";
							
							if (ARTUSModelFromSurvey::isImage($questionType))
								$field_source = "concat('{$folderSurvey}/surveyimages/{$Repository->ADODBDatabase}/', t1.QuestionImg_{$questionID})";
							elseif (ARTUSModelFromSurvey::isDoc($questionType))
								$field_source = "concat('{$folderSurvey}/surveydocuments/{$Repository->ADODBDatabase}/', t1.QuestionDoc_{$questionID})";
							elseif ($questionType == qtpGPS || $questionType == qtpMyLocation)
							{
								$field_source = "IFNULL(NULLIF(TRIM(t1.QuestionLat_{$questionID}), ''), '0'), IFNULL(NULLIF(TRIM(t1.QuestionLong_{$questionID}), ''), '0')";
								$field_target = PRE_DIM_DSC_FIELD.$questionID.'LAT, '.PRE_DIM_DSC_FIELD.$questionID.'LON';
								$defaults = ", '0', '0'";
							}
							else
								$field_source = "IFNULL(NULLIF(TRIM(t1.QuestionDesc_{$questionID}), ''), 'NA')";

							if (isset($asql_sec_insert[$sectionID]))
							{
								$asql_sec_insert[$sectionID] .= ", {$field_target}";
								$asql_sec_select[$sectionID] .= ", {$field_source}";
								$asql_sec_defaults[$sectionID] .= $defaults;
							}
							else
							{
								$asql_sec_insert[$sectionID] = ", {$field_target}";
								$asql_sec_select[$sectionID] = ", {$field_source}";
								$asql_sec_defaults[$sectionID] = $defaults;
							}
						}

					}
				}
				
				$rst->MoveNext();
			}
		}
		else
		{
			$serr = $dataconn->ErrorMsg();
			savelogfile($serr);
		}
		
// @EA 2015-09-04 Soporte de secciones tipo INLINE normales
		$sql =
		"SELECT DISTINCT B.SectionID AS sectionid
		FROM `si_sv_modelartus_questions` V
		INNER JOIN si_sv_modelartus C ON (C.id_survey = {$id_survey} AND C.id_modelartus = V.id_modelartus)
		INNER JOIN SI_SV_Section B ON (B.SurveyID = {$id_survey} AND V.id_question = B.SectionID*-1)
		WHERE `type` = 'DIM'
			AND B.SectionType = ".sectInline."
		 	AND C.cla_concepto > 0
			AND V.cla_descrip > 0 
			AND NOT V.`path` LIKE '%[CAT]%'
		ORDER BY B.SectionID";
		$rst = $dataconn->Execute($sql);
		
		if ($rst)
		{
			while (!$rst->EOF)
			{
				$sectionID = $rst->fields['sectionid'];
				$field_target = PRE_DIM_DSC_FIELD_INLINE.$sectionID;
				$field_source = "IFNULL(NULLIF(TRIM(SectionDesc), ''), 'NA')";
				
				if (isset($asql_sec_insert[$sectionID]))
				{
					$asql_sec_insert[$sectionID] .= ", {$field_target}";
					$asql_sec_select[$sectionID] .= ", {$field_source}";
					$asql_sec_defaults[$sectionID] .= ", 'NA'";
				}
				else
				{
					$asql_sec_insert[$sectionID] = ", {$field_target}";
					$asql_sec_select[$sectionID] = ", {$field_source}";
					$asql_sec_defaults[$sectionID] = ", 'NA'";
				}
				
				$rst->MoveNext();
			}
		}
		else
		{
			$serr = $dataconn->ErrorMsg();
			savelogfile($serr);
		}
			 
// Dimensiones asociadas a catalogos
		$sql =
		"SELECT DISTINCT
					B.SectionType AS sectiontype, 
					A.MCInputType AS inputtypeid,
					A.QTypeID AS qtypeid, 
					A.QuestionID AS questionid,
					A.SectionID AS sectionid,
					CAST( MID(V.`path`, 1, INSTR(V.`path`, '[CAT]')-1) AS UNSIGNED) AS memberid
			 FROM 	`si_sv_modelartus_questions` V
			 INNER JOIN si_sv_modelartus C ON (C.id_modelartus = V.id_modelartus)
			 INNER JOIN SI_SV_Question A ON (A.QuestionID = V.id_question)
			 INNER JOIN SI_SV_Section B ON (A.SectionID = B.SectionID)
			 WHERE A.SurveyID = {$id_survey} AND `type` IN ('DIM')
				AND C.cla_concepto > 0
				AND V.cla_descrip > 0
				AND V.`path` LIKE '%[CAT]%'
			 ORDER BY B.SectionType, B.SectionID";
			 
		$rst = $dataconn->Execute($sql);
		
		if ($rst)
		{
			while (!$rst->EOF)
			{
				$sectionType = $rst->fields['sectiontype'];
				$sectionID = $rst->fields['sectionid'];
				$inputType = $rst->fields['inputtypeid'];
				$questionType = $rst->fields['qtypeid'];
				$questionID = $rst->fields['questionid'];
				$memberID = $rst->fields['memberid'];
				
				if ($sectionType == sectNormal)
				{
					if (isset($asql_cat_insert[$questionID]))
					{
						$asql_cat_insert[$questionID] .= ", DSCM_{$memberID}";
						$asql_cat_select[$questionID] .= ", IFNULL(NULLIF(TRIM(Attribute_{$memberID}), ''), 'NA')";
						$asql_cat_defaults[$questionID] .= ", 'NA'";
					}
					else
					{
						$asql_cat_insert[$questionID] = ", DSCM_{$memberID}";
						$asql_cat_select[$questionID] = ", IFNULL(NULLIF(TRIM(Attribute_{$memberID}), ''), 'NA')";
						$asql_cat_defaults[$questionID] = ", 'NA'";
					}
				}
				else
				{
					if ($questionType == qtpMulti)
					{
// No existe aun esta funcionalidad						
					}
					else
					{
						if (isset($asql_sec_insert[$sectionID]))
						{
							$asql_sec_insert[$sectionID] .= ", DSCM_{$memberID}";
							$asql_sec_select[$sectionID] .= ", IFNULL(NULLIF(TRIM(q{$questionID}.Attribute_{$memberID}), ''), 'NA')";
							$asql_sec_defaults[$sectionID] .= ", 'NA'";
						}
						else
						{
							$asql_sec_insert[$sectionID] = ", DSCM_{$memberID}";
							$asql_sec_select[$sectionID] = ", IFNULL(NULLIF(TRIM(q{$questionID}.Attribute_{$memberID}), ''), 'NA')";
							$asql_sec_defaults[$sectionID] = ", 'NA'";
						}
						if (isset($asql_sec_from[$sectionID]))
						{
							if (strpos($asql_sec_from[$sectionID], "AS q{$questionID} ON") === false)
								$asql_sec_from[$sectionID] .= "
								LEFT JOIN sveforms_{$id_survey}_questiondet_{$questionID} AS q{$questionID} ON (q{$questionID}.SectionKey = t1.SectionKey)";
						}
						else
							$asql_sec_from[$sectionID] = "
							LEFT JOIN sveforms_{$id_survey}_questiondet_{$questionID} AS q{$questionID} ON (q{$questionID}.SectionKey = t1.SectionKey)";
					}
				}

				$rst->MoveNext();
			}
		}
		else
		{
			$serr = $dataconn->ErrorMsg();
			savelogfile($serr);
		}
		
// @EA 2015-09-03 Soporte de secciones tipo INLINE asociadas a catalogos
		$sql =
		"SELECT DISTINCT B.SectionID AS sectionid, CAST( MID(V.`path`, 1, INSTR(V.`path`, '[CAT]')-1) AS UNSIGNED) AS memberid
		FROM `si_sv_modelartus_questions` V
		INNER JOIN si_sv_modelartus C ON (C.id_survey = {$id_survey} AND C.id_modelartus = V.id_modelartus)
		INNER JOIN SI_SV_Section B ON (B.SurveyID = {$id_survey} AND V.id_question = B.SectionID*-1)
		WHERE `type` = 'DIM'
			AND B.SectionType = ".sectInline."
		 	AND C.cla_concepto > 0
			AND V.cla_descrip > 0 
			AND V.`path` LIKE '%[CAT]%'
		ORDER BY B.SectionID";
		$rst = $dataconn->Execute($sql);
		
		if ($rst)
		{
			while (!$rst->EOF)
			{
				$sectionID = $rst->fields['sectionid'];
				$memberID = $rst->fields['memberid'];

				$field_target = PRE_DIM_MEM_FIELD_INLINE.$memberID;
				$field_source = "IFNULL(NULLIF(TRIM(t1.Attribute_{$memberID}), ''), 'NA')";
				
				if (isset($asql_sec_insert[$sectionID]))
				{
					$asql_sec_insert[$sectionID] .= ", {$field_target}";
					$asql_sec_select[$sectionID] .= ", {$field_source}";
					$asql_sec_defaults[$sectionID] .= ", 'NA'";
				}
				else
				{
					$asql_sec_insert[$sectionID] = ", {$field_target}";
					$asql_sec_select[$sectionID] = ", {$field_source}";
					$asql_sec_defaults[$sectionID] = ", 'NA'";
				}				

				$rst->MoveNext();
			}
		}
		else
		{
			$serr = $dataconn->ErrorMsg();
			savelogfile($serr);
		}
//
		
// Indicadores
		$sql = 
		"SELECT DISTINCT 
					B.SectionType AS sectiontype, 
					A.MCInputType AS inputtypeid,
					A.QTypeID AS qtypeid, 
					A.QuestionID AS questionid,
					A.SectionID AS sectionid
			 FROM 	`si_sv_modelartus_questions` V
			 INNER JOIN si_sv_modelartus C ON (C.id_modelartus = V.id_modelartus)
			 INNER JOIN SI_SV_Question A ON (A.QuestionID = V.id_question)
			 INNER JOIN SI_SV_Section B ON (A.SectionID = B.SectionID)
			 WHERE A.SurveyID = {$id_survey} AND `type` IN ('IND')
				AND C.cla_concepto > 0
				AND V.cla_indicador > 0
			 ORDER BY B.SectionType, B.SectionID";
			 
		$rst = $dataconn->Execute($sql);
		
		if ($rst)
		{
			while (!$rst->EOF)
			{
				$sectionType = $rst->fields['sectiontype'];
				$sectionID = $rst->fields['sectionid'];
				$inputType = $rst->fields['inputtypeid'];
				$questionType = $rst->fields['qtypeid'];
				$questionID = $rst->fields['questionid'];
				
				$field_target = PRE_IND_FIELD.$questionID;
				$field_source = 'QuestionVal_'.$questionID;
				
				if ($sectionType == sectNormal)
				{
					$stdtar .= ', '.$field_target;
					$stdsrc .= ', t2.'.$field_source;
				}
				else
				{
					if (isset($asql_sec_insert[$sectionID]))
					{
						$asql_sec_insert[$sectionID] .= ', '.$field_target;
						$asql_sec_select[$sectionID] .= ', t1.'.$field_source;
						$asql_sec_defaults[$sectionID] .= ", NULL";
					}
					else
					{
						$asql_sec_insert[$sectionID] = ', '.$field_target;
						$asql_sec_select[$sectionID] = ', t1.'.$field_source;
						$asql_sec_defaults[$sectionID] = ", NULL";
					}
				}

				$rst->MoveNext();
			}
		}
		else
		{
			$serr = $dataconn->ErrorMsg();
			savelogfile($serr);
		}
		
		if ($stdsrc == '')
		{
			$sql_from_std =
			"FROM 	SVeForms_{$id_survey} AS t1
			 WHERE 	t1.SurveyKey = pSurveyKey;";
		}
		
		$sql_insert_std .= $stdtar.')';
		$sql_values_std .= $stdsrc;
		
		$sql1 = $sql_delete_std.'
		'.$sql_insert_std.'
		'.$sql_values_std.'
		'.$sql_from_std;
		
// @EA 2017-06-06 Update en bloques de 50 joins		
		$sql2 = '';
		for ($nvccblock = 0; $nvccblock <= $nblock50; $nvccblock++)
		{
			if ($sql_update_std_innerjoin[$nvccblock] != '')
				$sql2 .= '
				'.$sql_update_std.$sql_update_std_innerjoin[$nvccblock].$sql_update_std_set[$nvccblock].$sql_update_std_where;
		}
//		

		
		$sql3 = ARTUSModelFromSurvey::makeSectionInsertSelect($asql_sec_insert, $asql_sec_select, $asql_sec_from, $asql_sec_defaults, $id_survey);
		
		$sql3 .= ARTUSModelFromSurvey::makeSectionUpdate($asql_sec_from_sc, $asql_sec_update_sc);
		
		$sql4 = ARTUSModelFromSurvey::makeCatalogInsertSelect($asql_cat_insert, $asql_cat_select, $asql_cat_defaults, $id_survey);
		
		return $sql1.$sql2.implode('
		', $asql_mul).$sql3.$sql4.implode('
		', $asql_mul_md);
	}

	public static function generateSP_ETL(&$Repository, $id_survey, $loadHistory=true, $TestSP='')
	{
// Actualizar el SP de carga de datos
		$sql = 
		"drop procedure IF EXISTS sp_load_facts_for_survey_{$id_survey}";
		if (!$Repository->DataADOConnection->Execute($sql))
		{
			$serr = $Repository->DataADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$serr);
		}

		$insertToFactTables = ARTUSModelFromSurvey::insertToFactTable($Repository, $id_survey);
		
		if ($insertToFactTables != '')
		{
			$sql =
			"CREATE PROCEDURE sp_load_facts_for_survey_{$TestSP}{$id_survey}(IN pSurveyKey INT)
			begin
			 DECLARE vUser, vCountry, vState, vCity, vZIP, vAddress, vVersion, vAgenda VARCHAR(255); 
	 		 DECLARE vUserID, vCountryID, vStateID, vCityID, vZIPID, vAddressID, vVersionID, vAgendaID, vDateKey INT; 
	 		 DECLARE vFactKey BIGINT;
	 		 
	 		 SELECT  UserEMail, Country, State, City, ZIPCode, Address, eFormsVersionNum, IFNULL(AgendaID, -1), DateKey
	 		 INTO    vUser, vCountry, vState, vCity, vZIP, vAddress, vVersion, vAgendaID, vDateKey
	 		 FROM 	SVeForms_{$id_survey} 
	 		 WHERE 	SurveyKey = pSurveyKey;
	 		 
	 		 set vAgenda = 'ND';
	 		 
	 		 SELECT FilterText 
	 		 INTO vAgenda
	 		 FROM si_sv_surveyagenda
			 WHERE AgendaID = vAgendaID;
			 
			 CALL sp_sincronize_global_dimensions_esurveyV6 
				(vUser,vCountry,vState,vCity, vZIP, vAddress, vVersion, vAgenda,
					vUserID, vCountryID, vStateID , vCityID, vZIPID, vAddressID, vVersionID, vAgendaID);
					
			 
			".$insertToFactTables;
			
			$sql .= '
			
			end';
			
			if (!$Repository->DataADOConnection->Execute($sql))
			{
				$serr = $Repository->DataADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$serr);
//				print '<br>'.$sql.' -> '.$serr;
			}
		}
		
		$sql = 
		"drop procedure IF EXISTS sp_load_questions_for_survey_{$TestSP}{$id_survey}";
		if (!$Repository->DataADOConnection->Execute($sql))
		{
			$serr = $Repository->DataADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$serr);
		}
		
		$insertToQuestions = ARTUSModelFromSurvey::makeDimensionsAndIndicatorsFromQuestions($Repository, $id_survey);
		
		if ($insertToFactTables != '')
		{
			$sql =
			"CREATE PROCEDURE sp_load_questions_for_survey_{$TestSP}{$id_survey}(IN pSurveyKey INT)
			begin

			".$insertToQuestions;
			
			$sql .= '
			
			end';
			
			if (!$Repository->DataADOConnection->Execute($sql))
			{
				$serr = $Repository->DataADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$serr);
//				print '<br>'.$sql.' -> '.$serr;
			}
			
			$sql = 
			"drop procedure IF EXISTS sp_load_model_for_survey_{$TestSP}{$id_survey}";
			if (!$Repository->DataADOConnection->Execute($sql))
			{
				$serr = $Repository->DataADOConnection->ErrorMsg();
	//			savelogfile($sql.' -> '.$serr);
			}
			
			$sp_load_model = 
			"CREATE PROCEDURE sp_load_model_for_survey_{$TestSP}{$id_survey}(IN pSurveyKey INT)
			begin
				call sp_load_facts_for_survey_{$TestSP}{$id_survey}(pSurveyKey);
				call sp_load_questions_for_survey_{$TestSP}{$id_survey}(pSurveyKey);
			end";
			if (!$Repository->DataADOConnection->Execute($sp_load_model))
			{
				$serr = $Repository->DataADOConnection->ErrorMsg();
				savelogfile($sp_load_model.' -> '.$serr);
	//			print '<br>'.$sp_load_model.' -> '.$serr;
			}
			
			$sql = 
			"drop procedure IF EXISTS sp_load_historical_model_for_survey_{$TestSP}{$id_survey}";
			if (!$Repository->DataADOConnection->Execute($sql))
			{
				$serr = $Repository->DataADOConnection->ErrorMsg();
	//			savelogfile($sql.' -> '.$serr);
			}
			
			$sp_load_model = 
			"CREATE PROCEDURE sp_load_historical_model_for_survey_{$TestSP}{$id_survey}()
			begin
			  DECLARE done INT DEFAULT FALSE;
			  DECLARE vSurveyKey INT;
			  
			  DECLARE cur1 CURSOR FOR SELECT SurveyKey FROM SVeForms_{$id_survey};
			  
			  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

			  OPEN cur1;

			  read_loop: LOOP
			    FETCH cur1 INTO vSurveyKey;

			    IF done THEN
			      LEAVE read_loop;
			    END IF;
				CALL sp_load_model_for_survey_{$TestSP}{$id_survey}(vSurveyKey);
			  END LOOP;

			  CLOSE cur1;
			  
			end";
			if (!$Repository->DataADOConnection->Execute($sp_load_model))
			{
				$serr = $Repository->DataADOConnection->ErrorMsg();
				savelogfile($sp_load_model.' -> '.$serr);
	//			print '<br>'.$sp_load_model.' -> '.$serr;
			}
			elseif ($loadHistory && $TestSP == '')
			{
// @EA 2016-06-09 #5RITR1 Primero intentamos ejecutar exitosamente las preguntas de la encuesta, para una captura inexistente, si no falla se puede cargar la historia
				$sql = "call sp_load_questions_for_survey_{$id_survey}(0)";
				if ($Repository->DataADOConnection->Execute($sql))
				{
					$sql = "call sp_load_historical_model_for_survey_{$id_survey}()";
					if (!$Repository->DataADOConnection->Execute($sql))
					{
						$serr = $Repository->DataADOConnection->ErrorMsg();
						savelogfile($sql.' -> '.$serr);
			//			print '<br>'.$sp_load_model.' -> '.$serr;
					}
				}
				else
				{
					$serr = $Repository->DataADOConnection->ErrorMsg();
					savelogfile($sql.' -> '.$serr);
		//			print '<br>'.$sp_load_model.' -> '.$serr;
				}
			}			
		}
		
	}
	
	public static function insertToFactTable(&$Repository, $id_survey)
	{
		$sql =
		"SELECT V.cla_concepto, 
				ifnull(V.cla_descrip_user,-1) as cla_descrip_user,
				IFNULL(V.cla_descrip_country,-1) AS cla_descrip_country,
				IFNULL(V.cla_descrip_state,-1) AS cla_descrip_state,
				IFNULL(V.cla_descrip_city,-1) AS cla_descrip_city,
				IFNULL(V.cla_descrip_zipcode,-1) AS cla_descrip_zipcode,
				IFNULL(V.cla_descrip_address,-1) AS cla_descrip_address,
				IFNULL(V.cla_descrip_version,-1) AS cla_descrip_version,
				IFNULL(V.cla_descrip_agenda,-1) AS cla_descrip_agenda
		 FROM `si_sv_modelartus` V 
		 INNER JOIN SI_SV_Survey S ON (V.id_survey = S.SurveyID)
		 WHERE S.SurveyID = {$id_survey} AND V.cla_concepto > 0";
		 
		$rst = $Repository->DataADOConnection->Execute($sql);
		
		$sql = '';		 
		
		if (!$rst)
		{
			$ErrMsg = $Repository->DataADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrMsg);
		}
		else
		{
			if (!$rst->EOF)
			{
				$user = $rst->fields['cla_descrip_user'];
				$country = $rst->fields['cla_descrip_country'];
				$state = $rst->fields['cla_descrip_state'];
				$city = $rst->fields['cla_descrip_city'];
				$zip = $rst->fields['cla_descrip_zipcode'];
				$address = $rst->fields['cla_descrip_address'];
				$version = $rst->fields['cla_descrip_version'];

				$insert = "
				insert into ";
			}
				
			while (!$rst->EOF)
			{
				$agenda = $rst->fields['cla_descrip_agenda'];
				
				$sfield_agenda = '';
				$svalue_agenda = '';
				if ($agenda > 0)
				{
					$sfield_agenda = ", RIDIM_{$agenda}KEY";
					$svalue_agenda = ", vAgendaID";
				}
				
				$values = 
				"(DateKey, ".PRE_DIM_SVY_TABLE."KEY, RIDIM_{$user}KEY, RIDIM_{$country}KEY, RIDIM_{$state}KEY, RIDIM_{$city}KEY, RIDIM_{$zip}KEY, RIDIM_{$address}KEY, RIDIM_{$version}KEY{$sfield_agenda} )
					VALUES (vDateKey, pSurveyKey, vUserID, vCountryID, vStateID , vCityID, vZIPID, vAddressID, vVersionID{$svalue_agenda} );
					";

				$cla_concepto = $rst->fields['cla_concepto'];
				$fact = 'RIFACT_'.$cla_concepto;
				$sql .= '
				SELECT FactKey into vFactKey FROM '.$fact.' where '.PRE_DIM_SVY_TABLE.'KEY = pSurveyKey LIMIT 1;
				IF ROW_COUNT() = 0 THEN
				'.$insert.$fact.' '.$values.'
				END IF;
				';
				$rst->MoveNext();
			}
		}
		
		return $sql; 
	}

	function getDimClaDescrip($dimensionName)
	{
		$sql = 'select CLA_DESCRIP as cla_descrip from SI_DESCRIP_ENC where CREATED_BY = '.CREATED_BY_EFORMSV5.' and NOM_LOGICO = '.$this->Repository->ADOConnection->Quote($dimensionName);
		$rst = $this->Repository->ADOConnection->Execute($sql);

		if ($rst && !$rst->EOF)
			return $rst->fields['cla_descrip'];
		else
		{
			$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}
			
		return -1;
	}

	function addModelDimension($dimensionName, $dimensionID, $dimensionGroup, &$consecutivo_dim_survey=-1)
	{
		$strOriginalWD = getcwd();
		
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->cla_concepto);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMSV5;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->DimensionGroup = $dimensionGroup;
		$anInstanceModelDim->Dimension->UseKey = 0;
		$anInstanceModelDim->Dimension->DimensionClaDescrip = $dimensionID;
		
		$objMMAnswer = @$anInstanceModelDim->save();
		
		$consecutivo_dim_survey = $anInstanceModelDim->Dimension->DimensionID;
		
		chdir($strOriginalWD);
		
		$strMMError = identifyModelManagerError($objMMAnswer);
		if ($strMMError == '') {
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
			return $anInstanceModelDim->Dimension->DimensionClaDescrip;
		}
		else {
			$sErr = "\r\n(".__METHOD__.") ".translate("Error adding {$dimensionName} dimension").": ".$strMMError.". ";
			savelogfile($sErr);
		}
		
		return -1;
	}
	
	function getIndicatorsRemovedFromSelection()
	{
		$aIndicators = array();
		if ($this->cla_concepto > 0)
		{
			$this->setGlobalIndicators();
			
			$sql =
			"select	CLA_INDICADOR as cla_indicador
			 from	SI_INDICADOR
			 WHERE	CLA_CONCEPTO = {$this->cla_concepto} 
			 		AND NOT CLA_INDICADOR in ({$this->list_globalindicators})
			 		AND NOT CLA_INDICADOR in (select cla_indicador from {$this->Repository->DataADODBDatabase}.si_sv_modelartus_questions
			 		where id_modelartus = {$this->id_modelartus} AND `type` = 'IND' and NOT cla_indicador IS NULL)
			 		AND NOT CLA_INDICADOR in (select cla_indicador_score from {$this->Repository->DataADODBDatabase}.si_sv_modelartus_questions
			 		where id_modelartus = {$this->id_modelartus} AND `type` = 'DIM' and NOT cla_indicador_score IS NULL)
			 		AND NOT CLA_INDICADOR in (select cla_indicador_atributo from {$this->Repository->DataADODBDatabase}.si_sv_modelartus_questions
			 		where id_modelartus = {$this->id_modelartus} AND `type` = 'DIM' and NOT cla_indicador_atributo IS NULL)";
			 		
//			savelogfile($sql);
			 		
			$rst = $this->Repository->ADOConnection->Execute($sql);

			if ($rst && !$rst->EOF)
			{
				while (!$rst->EOF)
				{
					$cla_indicador = (int) $rst->fields['cla_indicador'];
					$aIndicators[] = $cla_indicador;
					$rst->MoveNext();
				}
			}
			else
			{
				$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
				savelogfile($ErrSQL);
			}
		}
		return $aIndicators;
	}
	
	function getDimensionsRemovedEBavelFromSelection()
	{
		$aDimensions = array();
		if ($this->cla_concepto > 0)
		{
			$sql =
			"select	CLA_DESCRIP as cla_descrip
			 from	SI_CPTO_LLAVE
			 WHERE	CLA_CONCEPTO = {$this->cla_concepto} AND CLA_PERIODO is NULL 
				AND NOM_FISICO LIKE 'FFRMS%'
				AND NOT CLA_DESCRIP in 
				(select cla_descrip from {$this->Repository->DataADODBDatabase}.si_sv_modelartus_questions
				where id_modelartus = {$this->id_modelartus} AND NOT cla_descrip IS NULL AND `type` = '')";
				
			$rst = $this->Repository->ADOConnection->Execute($sql);

			if ($rst)
			{
				while (!$rst->EOF)
				{
					$cla_descrip = (int) $rst->fields['cla_descrip'];
					$aDimensions[] = $cla_descrip;
					$rst->MoveNext();
				}
			}
			else
			{
				$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
				savelogfile($ErrSQL);
			}
		}
		return $aDimensions;
	}
	
	function getDimensionsRemovedFromSelection()
	{
		$aDimensions = array();
		if ($this->cla_concepto > 0)
		{
			$this->setGlobalDimensions();
			
			$sql =
			"select	CLA_DESCRIP as cla_descrip
			 from	SI_CPTO_LLAVE
			 WHERE	CLA_CONCEPTO = {$this->cla_concepto} AND CLA_PERIODO is NULL 
			 		AND NOT CLA_DESCRIP in ({$this->list_globaldimensions})
			 		AND NOT CLA_DESCRIP in (select cla_descrip from {$this->Repository->DataADODBDatabase}.si_sv_modelartus_questions
			 		where id_modelartus = {$this->id_modelartus} AND NOT cla_descrip IS NULL )
			 		AND NOT CLA_DESCRIP in (select cla_descrip_atributo from {$this->Repository->DataADODBDatabase}.si_sv_modelartus_questions
			 		where id_modelartus = {$this->id_modelartus} AND NOT cla_descrip_atributo IS NULL )";
			 		
//			savelogfile($sql);
//			print '<br>'.$sql;
			 		
			$rst = $this->Repository->ADOConnection->Execute($sql);

			if ($rst)
			{
				while (!$rst->EOF)
				{
					$cla_descrip = (int) $rst->fields['cla_descrip'];
					$aDimensions[] = $cla_descrip;
					$rst->MoveNext();
				}
			}
			else
			{
				$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
				savelogfile($ErrSQL);
			}
		}
		return $aDimensions;
	}

	function deleteFromSI_CPTO_LLAVE($cla_descrip)
	{
		$sql = "delete from SI_CPTO_LLAVE where CLA_DESCRIP = {$cla_descrip} and CLA_CONCEPTO = {$this->cla_concepto}";
		if (!$this->Repository->ADOConnection->Execute($sql))
		{
			$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
			savelogfile($ErrSQL);
		}		
	}

	function deleteFromSI_DESCRIP_KEYS($nom_tabla)
	{
		$sql =
		"select count(1) as nveces from SI_DESCRIP_ENC where NOM_TABLA = '".$nom_tabla."' and CLA_DESCRIP in 
		(select CLA_DESCRIP from SI_CPTO_LLAVE where CLA_CONCEPTO = {$this->cla_concepto})";
		
		$rst = $this->Repository->ADOConnection->Execute($sql);
		
		if ($rst)
		{
			if ($rst->fields['nveces'] == 0)
			{
				$sql = 
				"delete from SI_DESCRIP_KEYS where NOM_TABLA = '".$nom_tabla."' and CLA_CONCEPTO = {$this->cla_concepto}";
				if (!$this->Repository->ADOConnection->Execute($sql))
				{
					$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
					savelogfile($ErrSQL);
				}
			}
		}	
	}

	static function deleteFromSI_DESCRIP_ENC(&$Repository,$cla_descrip, $nom_tabla='')
	{
		$sql = 
		"select	count(1) as nveces from SI_CPTO_LLAVE where CLA_DESCRIP = {$cla_descrip}";
		$rst = $Repository->ADOConnection->Execute($sql);
		
		if ($rst)
		{
			if ($rst->fields['nveces'] == 0)
			{
				if ($nom_tabla == '')
				{
					$sql = 
					"select	NOM_TABLA as tabla from SI_DESCRIP_ENC where CLA_DESCRIP = {$cla_descrip}";
					$rst = $Repository->ADOConnection->Execute($sql);
					if ($rst)
					{
						if (!$rst->EOF)
							$nom_tabla = trim($rst->fields['tabla']);
					}
				}

				$sql = 
				"delete from SI_DESCRIP_ENC where CLA_DESCRIP = {$cla_descrip}";
				if (!$Repository->ADOConnection->Execute($sql))
				{
					$ErrSQL = $Repository->ADOConnection->ErrorMsg();
					savelogfile($ErrSQL);
				}
				
				if ($nom_tabla != '')
				{
					$sql = 
					"select	count(1) as nveces from SI_DESCRIP_ENC where NOM_TABLA = '".$nom_tabla."'";
					$rst = $Repository->ADOConnection->Execute($sql);
					
					if ($rst->fields['nveces'] == 0)
					{
						$sql =
						"drop table {$nom_tabla}";
						if (!$Repository->DataADOConnection->Execute($sql))
						{
							$ErrSQL = $Repository->DataADOConnection->ErrorMsg();
							savelogfile($ErrSQL);
						}
					}
				}
			}
		}
		else
		{
			$ErrSQL = $Repository->ADOConnection->ErrorMsg();
			savelogfile($ErrSQL);
		}		
	}

	function removeDimensionOnDependSeccion($cla_descrip)
	{
		if ($cla_descrip > 0)
		{
			$seccion_estandar = PRE_DIM_SVY_TABLE.$this->cla_descrip_sv;
		
			$sql =
			"select	NOM_TABLA as nom_tabla, NOM_FISICO as nom_fisico from SI_DESCRIP_ENC where CLA_DESCRIP = {$cla_descrip}";
			$rst = $this->Repository->ADOConnection->Execute($sql);
			
			if ($rst)
			{
				if (!$rst->EOF)
				{
					$nom_tabla = strtoupper(trim($rst->fields['nom_tabla']));
					$nom_fisico = strtoupper(trim($rst->fields['nom_fisico']));
// @EA 2016-05-20 No borramos las dimensiones padre de las preguntas tipo GPS
					if ($nom_fisico != 'RISDIM_KEY')
					{
						if ($nom_tabla == $seccion_estandar)
							$this->deleteFromSI_CPTO_LLAVE($cla_descrip);
						else
						{
							$this->deleteFromSI_CPTO_LLAVE($cla_descrip);
							$this->deleteFromSI_DESCRIP_KEYS($nom_tabla);
						}
					}
				}
			}
			else
			{
				$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}
		}
	}

	function generateDepends()
	{
		$metaconnection = $this->Repository->ADOConnection;
		
		$sql = 
		"update SI_CPTO_LLAVE set PAR_DESCRIPTOR = CONSECUTIVO where CLA_CONCEPTO = {$this->cla_concepto} and NOT NOM_FISICO like 'FFRMS%'";
		if (!$metaconnection->Execute($sql))
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}

		$aconsecutive_parent = array();
		$aconsecutivo_childs = array();
		
		foreach ($this->dimensions as $adimension)
		{
//			if ($adimension->cla_descrip > 0 && $adimension->section_type != sectNormal && $adimension->section_type != sectInline && $adimension->id_qtype == qtpMulti)
// @EA 2015-08-25 Quite la restriccion en secciones de tipo inline (equivale a una maestro detalle)
			if ($adimension->cla_descrip > 0 && $adimension->section_type != sectNormal && $adimension->id_qtype == qtpMulti)
			{
				if (!isset($aconsecutive_parent[$adimension->id_section]))
				{
					$sql = 
					"SELECT t2.CONSECUTIVO as consecutivo
					 FROM SI_DESCRIP_ENC AS t1 INNER JOIN SI_CPTO_LLAVE AS t2 ON (t2.CLA_DESCRIP = t1.CLA_DESCRIP)
					 WHERE t2.CLA_CONCEPTO = {$this->cla_concepto} AND t1.NOM_TABLA = 'RISDIM_{$adimension->id_section}' LIMIT 1";
					$rst = $metaconnection->Execute($sql);
					if ($rst)
					{
						$aconsecutive_parent[$adimension->id_section] = (int) $rst->fields['consecutivo'];
						$aconsecutivo_childs[$adimension->id_section] = '';
					}
					else
					{
						$ErrSQL = $metaconnection->ErrorMsg();
						savelogfile($sql.' -> '.$ErrSQL);
					}
				}
				if (isset($aconsecutive_parent[$adimension->id_section]))
				{
					$aconsecutivo_childs[$adimension->id_section] .= ",{$adimension->cla_descrip}";
					if ($adimension->cla_descrip_atributo > 0)
						$aconsecutivo_childs[$adimension->id_section] .= ",{$adimension->cla_descrip_atributo}";
				}
			}
		}
		
		foreach ($aconsecutive_parent as $id_section => $consecutive_parent)
		{
			$list_childs = substr($aconsecutivo_childs[$id_section],1);
			$sql = 
			"UPDATE SI_CPTO_LLAVE SET PAR_DESCRIPTOR = CONCAT(CONSECUTIVO,'|',{$consecutive_parent})
			 WHERE CLA_CONCEPTO = {$this->cla_concepto} and CLA_DESCRIP IN ({$list_childs})";

			if (!$metaconnection->Execute($sql))
			{
				$ErrSQL = $metaconnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}
		}
	}

	function addQuestionsInSurvey($IsNewModel=false)
	{
		$dataconnection = $this->Repository->DataADOConnection;
		if (!$IsNewModel)
		{
// Primero garantizamos que si cambiaron de indicador a dimension o de dimension a indicador, 
// este reseteado el campo cla_indicador y cla_descrip correspondiente
			$sql =
			"UPDATE si_sv_modelartus_questions
				SET cla_descrip = NULL, 
    				cla_descrip_atributo = NULL, 
    				cla_descrip_foto = NULL, 
    				cla_descrip_documento = NULL, 
    				cla_descrip_comentario = NULL,
					cla_indicador_score = NULL,
					cla_indicador_atributo = NULL
			 WHERE id_modelartus = {$this->id_modelartus} AND TYPE = 'IND'";
			if($dataconnection->Execute($sql) === false)
			{
				$serr = $dataconnection->ErrorMsg();
				savelogfile($serr);
			}
			
			$sql =
			"UPDATE si_sv_modelartus_questions
				SET cla_indicador = NULL
			 WHERE id_modelartus = {$this->id_modelartus} AND TYPE = 'DIM'";
			if($dataconnection->Execute($sql) === false)
			{
				$serr = $dataconnection->ErrorMsg();
				savelogfile($serr);
			}

// Si el model ya existia, hay que borrar las preguntas que ya no estan en la lista de seleccion del modelo
			$aListDimensionsToRemove = $this->getDimensionsRemovedFromSelection();
			
			$strOriginalWD = getcwd();

			foreach ($aListDimensionsToRemove as $cla_descrip)
			{
// Aqui hay que considerar que el borrado de la dimension/atributo correspondiente
				$this->removeDimensionOnDependSeccion($cla_descrip);
				$this->ExistsChanges = true;
			}
			
			$aListDimensionsEBavelToRemove = $this->getDimensionsRemovedEBavelFromSelection();
			foreach ($aListDimensionsEBavelToRemove as $cla_descrip)
			{
// Aqui hay que considerar que el borrado de la dimension/atributo correspondiente
				$this->removeDimensionOnDependSeccion($cla_descrip);
			}
			
			$aListIndicatorsToRemove = $this->getIndicatorsRemovedFromSelection();
			foreach ($aListIndicatorsToRemove as $cla_indicador)
			{
//				print '<br>Remove Indicator ['.$cla_indicador.']';
				$this->deleteFromSI_INDICADOR($this->Repository->ADOConnection, $cla_indicador);
				$this->ExistsChanges = true;
			}
			chdir($strOriginalWD);
			
			$this->renameModel();
		}
		$adescripsShared = array();
		$adescripsShared_atributo = array();
		$adescripsShared_foto = array();
		$adescripsShared_documento = array();
		$adescripsShared_comentario = array();

		$metaconnection = $this->Repository->ADOConnection;

		$sql =
		"SELECT t1.CONSECUTIVO AS consecutivo
		FROM	SI_CPTO_LLAVE t1 
		WHERE	t1.CLA_CONCEPTO = {$this->cla_concepto} AND
		t1.CLA_DESCRIP = {$this->cla_descrip_sv}";
		
		$consecutivo_dim_survey = -1;
		$rst = $metaconnection->Execute($sql);
		
		if ($rst)
		{
			if (!$rst->EOF)
				$consecutivo_dim_survey = $rst->fields['consecutivo'];
		}
		else
		{
			$ErrSQL = $metaconnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}

// Dimensiones nuevas, Recorremos la lista de preguntas para determinar que dimensiones+indicadores AGREGAMOS (cla_descrip > 0)
		foreach ($this->dimensions as $adimension)
		{
			if ($adimension->cla_descrip <= 0)
			{
				$cla_indicador_score = -1;
				$cla_indicador_atributo = -1;
				
				$this->ExistsChanges = true;

				$cla_descrip = -1;
				$cla_descrip_atributo = -1;
				$path = $adimension->path_ebavel;
				
				if (isset($adescripsShared[$path][$adimension->id_question]))
				{
					$cla_descrip = $adescripsShared[$path][$adimension->id_question];
					$cla_descrip_atributo = $adescripsShared_atributo[$path][$adimension->id_question];
				}
					
				$dimensionName = $adimension->nom_dimension;
				$dimensionGroup = $adimension->section_name;
				
				if ($adimension->isCatalogDimension() && $adimension->section_type == sectNormal)
				{
					$adimension->cla_descrip = $adimension->saveCatalogDimension($this->cla_concepto, $cla_descrip, $dimensionGroup, $this->SurveyID, $cla_indicador_score);
					$adimension->cla_indicador_score = $cla_indicador_score;
				}	
				else
				{
					if ($adimension->section_type == sectNormal)
					{
						if ($adimension->id_qtype == qtpMulti)
						{
// Las preguntas multiple choice hay que crearlas como tablas de dimension que tengan una llave foranea que apunte a la captura de la encuesta
// (2)						
							$adimension->cla_descrip = $adimension->saveMultiDimension($this->cla_concepto, $cla_descrip, $cla_descrip_atributo, $dimensionGroup, $this->SurveyID, $cla_indicador_score,$cla_indicador_atributo);
							$adimension->cla_descrip_atributo = $cla_descrip_atributo;
							$adimension->cla_indicador_score = $cla_indicador_score;
							$adimension->cla_indicador_atributo = $cla_indicador_atributo;
						}
						else
						{
// Todas estas preguntas van como atributos de la dimension principal de la encuesta
// (1)
							$nom_tabla = PRE_DIM_SVY_TABLE.$this->SurveyID;
							$nom_fisico = PRE_DIM_DSC_FIELD.$adimension->id_question;

// @EA 2016-05-11 Soporte de preguntas tipo GPS
							if ($adimension->id_qtype == qtpGPS || $adimension->id_qtype == qtpMyLocation)
							{
								$dimensionName .= ' Latitude';
								$nom_fisico .= 'LAT';
							}
							
							$sql = "select	CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}' and NOM_FISICO = '{$nom_fisico}'";
							
							$this->existRow($metaconnection, $sql, $cla_descrip);
							
							if ($cla_descrip <= 0)
							{
								$tipo_dato = 2;
								$long_dato = $adimension->longfield;

// @EA 2017-06-06 Soporte de campos tipo LONGTEXT
								if ($long_dato > 0 && $long_dato <= 255)
									$str_tipo_dato = "varchar({$long_dato})";
								else
									$str_tipo_dato = "LONGTEXT";
//
								
								$cla_descrip = $this->getMaxClaDescrip($metaconnection);
//								$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup, true);
								$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);
								
								$sql =
								"alter table {$nom_tabla} add {$nom_fisico} {$str_tipo_dato} null";
								if (!$dataconnection->Execute($sql))
								{
									$ErrSQL = $dataconnection->ErrorMsg();
									savelogfile($sql.' -> '.$ErrSQL);
								}
							}

							$adimension->cla_descrip = $this->addModelAttribute($this->cla_descrip_sv, $cla_descrip, $dimensionName, $dimensionGroup, ($adimension->id_qtype == qtpGPS || $adimension->id_qtype == qtpMyLocation));
							$adimension->verifyQuestionType();
// @EA 2016-05-11 Soporte de preguntas tipo GPS							
							if ($adimension->id_qtype == qtpGPS || $adimension->id_qtype == qtpMyLocation)
							{
								if ($adimension->cla_descrip_atributo <= 0)
								{
									$tipo_dato = 2;
									$long_dato = 100;
									$cla_descrip = $this->getMaxClaDescrip($metaconnection);
	//								$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup, true);
									$dimensionName = $adimension->nom_dimension.' Longitude';
									$nom_fisico = PRE_DIM_DSC_FIELD.$adimension->id_question;
									$nom_fisico .= 'LON';
									$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);
									
									$sql =
									"alter table {$nom_tabla} add {$nom_fisico} varchar(100) null";
									if (!$dataconnection->Execute($sql))
									{
										$ErrSQL = $dataconnection->ErrorMsg();
										savelogfile($sql.' -> '.$ErrSQL);
									}
									$adimension->cla_descrip_atributo = $this->addModelAttribute($this->cla_descrip_sv, $cla_descrip, $dimensionName, $dimensionGroup, true);
								}
							}
							elseif ($adimension->id_qtype == qtpSingle)
							{
								$adimension->addScore($this->cla_concepto, $consecutivo_dim_survey, $nom_tabla, $cla_indicador_score);
								$adimension->cla_indicador_score = $cla_indicador_score;
							}
/*							elseif ($adimension->id_qtype == qtpCallList)
							{
								
							}
*/							
						}
					}
//					elseif ($adimension->section_type != sectInline)
// @EA 2015-08-25 Quite la restriccion en secciones de tipo inline (equivale a una maestro detalle)
					else
					{
// Secciones Inline y Maestro Detalle tienen que compartir tablas de dimension x seccionID
						if ($adimension->id_qtype == qtpMulti)
						{
							$adimension->cla_descrip = $adimension->saveMultiDimension($this->cla_concepto, $cla_descrip, $cla_descrip_atributo, $dimensionGroup, $this->SurveyID, $cla_indicador_score, $cla_indicador_atributo, true);
							$adimension->cla_indicador_score = $cla_indicador_score;
							$adimension->cla_indicador_atributo = $cla_indicador_atributo;
						}
						else
						{
// Aqui la clave de la seccion formará parte del nombre de la tabla que compartira todas las dimensiones de esa seccion
// (3)
							$adimension->cla_descrip = $adimension->saveSectionDimension($this->cla_concepto, $cla_descrip, $dimensionGroup, $this->SurveyID, $cla_indicador_score);
							$adimension->cla_indicador_score = $cla_indicador_score;
							$adimension->cla_indicador_atributo = $cla_indicador_atributo;
						}
					}
				}
				$adimension->saveSincronize();
				
				$this->aquestions[$adimension->id_question] = $adimension->cla_descrip;
			}
			else
			{
/*
				print '<br>';
				print '<br>renameDimension -> '.$adimension->cla_descrip;
				print '<br>renameDimension -> '.$adimension->nom_dimension;
				print '<br>renameSection -> '.$adimension->section_name;
*/
				$adimension->renameDimension();
			}

		}

// Indicadoress nuevos
//		print '<br>consecutivo_dim_survey = '.$consecutivo_dim_survey;
		foreach ($this->indicators as $aindicator)
		{
//			print '<br>cla_indicador = '.$aindicator->cla_indicador;
//			print '<br>indicatorName = '.$aindicator->nom_indicador;
//			print '<br>id_question = '.$aindicator->id_question;
			$indicatorName = $aindicator->nom_indicador;			
			if ($aindicator->cla_indicador <= 0)
			{
				$this->ExistsChanges = true;
				
				$formato = '#,##0';
				$agrupador = 'SUM';
// Seccion Multiple o Seccion Estandar ... aqui se tendria que generar una dependencia del indicador
				$aindicator->cla_indicador = $aindicator->saveSectionIndicator($this->cla_concepto, $formato, $consecutivo_dim_survey, $this->SurveyID);

//				print('<br>$aindicator->cla_indicador -> '.$aindicator->cla_indicador);
				$aindicator->saveSincronize();
			}
			else
				$aindicator->renameIndicator();
		}
//
	}

	function addModelAttribute($cla_descrip_parent, $cla_descrip, $AttribName, $dimensionGroup, $IsGPS=false)
	{
		$cla_concepto = $this->cla_concepto;
		$strOriginalWD = getcwd();
		
		$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
		$anInstanceAttrib->Dimension->DimensionName = $AttribName;
		$anInstanceAttrib->Dimension->DimensionGroup = $dimensionGroup;
		$anInstanceAttrib->Dimension->ParentDimensionID = $cla_descrip_parent;
		$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
		$anInstanceAttrib->Dimension->DimensionID = -1;
		//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
		$objMMAnswer = @$anInstanceAttrib->save();
		
		chdir($strOriginalWD);
		
		$strMMError = identifyModelManagerError($objMMAnswer);
		if ($strMMError != '') {
			$sErr = "\r\n(".__METHOD__.") ".translate("Error creating {$AttribName} dimension").": ".$strMMError.". ";
			savelogfile($sErr);
		}
		
		if ($IsGPS)
		{
			$cla_descrip = $anInstanceAttrib->Dimension->DimensionClaDescrip;
			$table_name = $anInstanceAttrib->Dimension->TableName;
			$field_key = $anInstanceAttrib->Dimension->FieldKey;
			
			$this->insertAttribute($this->Repository->ADOConnection, $cla_descrip_parent, $cla_descrip, $AttribName, $table_name, $field_key);
		}
		
		return $anInstanceAttrib->Dimension->DimensionClaDescrip;
	}
	
	function createFactKeyDimension(&$consecutivo_dim_survey)
	{
		$dimensionName = $this->SurveyName." Survey ID";
		$dimensionGroup = 'SurveyID';
		$gps = 'GPS';
		$dimensionID = $this->cla_descrip_sv;
		
		$nom_tabla = PRE_DIM_SVY_TABLE.$this->SurveyID;
		$cla_concepto = $this->cla_concepto;
		$key = PRE_DIM_SVY_TABLE.'KEY';
		$nom_fisico = $key;
		
		$metaconnection = $this->Repository->ADOConnection;
		$dataconnection = $this->Repository->DataADOConnection;
		
		$tipo_dimension = 3;
		$default_key = '\'NA\'';
		
		if ($dimensionID <= 0)
		{
			$sql = 
			"select	1 as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}'";
			
			$bExistTableFactKey = $this->existRow($metaconnection, $sql);
			
			if (!$bExistTableFactKey)
			{
				$sql =
				"create table {$nom_tabla} ({$key} int not null,
				 latitude_sv double null,
				 longitude_sv double null,
				 accuracy_sv double null,
				 date_sv varchar(10) null,
				 hour_sv varchar(8) null,
				 dateend_sv varchar(10) null,
				 hourend_sv varchar(8) null,
				 primary key ({$key}))";
				
				if (!$this->Repository->DataADOConnection->Execute($sql))
				{
					$ErrSQL = $this->Repository->DataADOConnection->ErrorMsg();
//					print '<br>$sql -> '.$sql.' Err -> '.$ErrSQL;
					savelogfile($sql.' -> '.$ErrSQL);
				}
				
				$tipo_dato = 7;
				$long_dato = 0;

				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);
				$this->cla_descrip_sv = $cla_descrip;
			
				$dimensionName = "Latitude Survey";
				$nom_fisico = 'latitude_sv';	
				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $gps);
				$this->cla_descrip_sv_latitude = $cla_descrip;
				$this->insertAttribute($metaconnection, $this->cla_descrip_sv, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico);
				
				$dimensionName = "Longitude Survey";
				$nom_fisico = 'longitude_sv';	
				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $gps);
				$this->cla_descrip_sv_longitude = $cla_descrip;
				$this->insertAttribute($metaconnection, $this->cla_descrip_sv, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico);
			
				$dimensionName = "Accuracy Survey";
				$nom_fisico = 'accuracy_sv';	
				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $gps);
				$this->cla_descrip_sv_accuracy = $cla_descrip;
				$this->insertAttribute($metaconnection, $this->cla_descrip_sv, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico);
				
				$dimensionName = "Date Survey";
				$tipo_dato = 2;
				$long_dato = 10;
				$nom_fisico = 'date_sv';
				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);
				$this->cla_descrip_sv_date = $cla_descrip;
				
				$dimensionName = "Hour Survey";
				$long_dato = 8;
				$nom_fisico = 'hour_sv';
				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);
				$this->cla_descrip_sv_hour = $cla_descrip;
				
				$dimensionName = "Date End Survey";
				$long_dato = 10;
				$nom_fisico = 'dateend_sv';
				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);
				$this->cla_descrip_sv_enddate = $cla_descrip;
				
				$dimensionName = "Hour End Survey";
				$long_dato = 8;
				$nom_fisico = 'hourend_sv';
				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $dimensionName, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);
				$this->cla_descrip_sv_endhour = $cla_descrip;
			}
		}

		$sfiltro = "select	CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}' and NOM_FISICO = ";

		if ($this->cla_descrip_sv <= 0)
		{
			$sql = $sfiltro."'{$key}'";
			
//			PRINT '<br>'.$sql;
			
			$this->existRow($metaconnection, $sql, $dimensionID);
			$this->cla_descrip_sv = $dimensionID;
		}
		
		if ($this->cla_descrip_sv > 0)
		{
			$sql = "alter table RIFACT_{$cla_concepto} add {$key} int not null";
			if (!$this->Repository->DataADOConnection->Execute($sql))
			{
				$ErrSQL = $this->Repository->DataADOConnection->ErrorMsg();
//					print '<br>$sql -> '.$sql.' Err -> '.$ErrSQL;
				savelogfile($sql.' -> '.$ErrSQL);
			}
			
			$sql = "create index idx{$key} on RIFACT_{$cla_concepto} ({$key}, DateKey)";
			if (!$this->Repository->DataADOConnection->Execute($sql))
			{
				$ErrSQL = $this->Repository->DataADOConnection->ErrorMsg();
//					print '<br>$sql -> '.$sql.' Err -> '.$ErrSQL;
				savelogfile($sql.' -> '.$ErrSQL);
			}


			$arrMax = ARTUSDimensionFromSurvey::getMaxConsecutivoNivel($cla_concepto, $metaconnection);
			$consecutivo = $arrMax["consecutivo"];
			$consecutivo_dim_survey = $consecutivo;
			$nivel = $arrMax["nivel"];
			
			$parent_join = $key;
			$nom_fisico = $key;
			$dimensionName = $this->SurveyName." Survey ID";
			$tipo_dato = 7;
			$long_dato = 0;
			
			$ErrSQL = $this->insertLocalDimension($metaconnection, $consecutivo, $nivel, $this->cla_descrip_sv, $dimensionName, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup, $cla_concepto);
			
			$this->insertSI_DESCRIP_KEYS($metaconnection, $nom_tabla, $parent_join, $parent_join, $cla_concepto);
		}
						
		$dimensionParentID = $this->cla_descrip_sv;

		$attribName = "Latitude Survey";
		$dimensionID = $this->cla_descrip_sv_latitude;
		
		if ($dimensionID <= 0)
		{
			$sql = $sfiltro."'latitude_sv'";
			$this->existRow($metaconnection, $sql, $dimensionID);
		}
		$this->cla_descrip_sv_latitude = $this->addModelAttribute($dimensionParentID, $dimensionID, $attribName, $gps);
		
		$attribName = "Longitude Survey";
		$dimensionID = $this->cla_descrip_sv_longitude;
		if ($dimensionID <= 0)
		{
			$sql = $sfiltro."'longitude_sv'";
			$this->existRow($metaconnection, $sql, $dimensionID);
		}
		$this->cla_descrip_sv_longitude = $this->addModelAttribute($dimensionParentID, $dimensionID, $attribName, $gps);
		
		$attribName = "Accuracy Survey";
		$dimensionID = $this->cla_descrip_sv_accuracy;
		if ($dimensionID <= 0)
		{
			$sql = $sfiltro."'accuracy_sv'";
			$this->existRow($metaconnection, $sql, $dimensionID);
		}
		$this->cla_descrip_sv_accuracy = $this->addModelAttribute($dimensionParentID, $dimensionID, $attribName, $gps);
		
		$attribName = "Date Survey";
		$dimensionID = $this->cla_descrip_sv_date;
		if ($dimensionID <= 0)
		{
			$sql = $sfiltro."'date_sv'";
			$this->existRow($metaconnection, $sql, $dimensionID);
		}
		$this->cla_descrip_sv_date = $this->addModelAttribute($dimensionParentID, $dimensionID, $attribName, $dimensionGroup);
		
		$attribName = "Hour Survey";
		$dimensionID = $this->cla_descrip_sv_hour;
		if ($dimensionID <= 0)
		{
			$sql = $sfiltro."'hour_sv'";
			$this->existRow($metaconnection, $sql, $dimensionID);
		}
		$this->cla_descrip_sv_hour = $this->addModelAttribute($dimensionParentID, $dimensionID, $attribName, $dimensionGroup);
		
		$attribName = "End Date Survey";
		$dimensionID = $this->cla_descrip_sv_enddate;
		if ($dimensionID <= 0)
		{
			$sql = $sfiltro."'dateend_sv'";
			$this->existRow($metaconnection, $sql, $dimensionID);
		}
		$this->cla_descrip_sv_enddate = $this->addModelAttribute($dimensionParentID, $dimensionID, $attribName, $dimensionGroup);
		
		$attribName = "End Hour Survey";
		$dimensionID = $this->cla_descrip_sv_endhour;
		if ($dimensionID <= 0)
		{
			$sql = $sfiltro."'hourend_sv'";
			$this->existRow($metaconnection, $sql, $dimensionID);
		}
		$this->cla_descrip_sv_endhour = $this->addModelAttribute($dimensionParentID, $dimensionID, $attribName, $dimensionGroup);
	}

	function addModelIndicatorBase($indicador_name, $formato, $agrupador)
	{
		$strOriginalWD = getcwd();
		
		$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
		$anInstanceIndicator->ModelID = $this->cla_concepto;
		$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->cla_concepto);
		$anInstanceIndicator->IndicatorName = $indicador_name;
		$anInstanceIndicator->formato = $formato;
		$anInstanceIndicator->agrupador = $agrupador;
		$anInstanceIndicator->sql_source = $agrupador;

		@$anInstanceIndicator->save();
		
		chdir($strOriginalWD);

		return $anInstanceIndicator->IndicatorID;
	}
	
	function addSystemDimensionsAndIndicators()
	{
// Dimensiones
		$dimensionName = 'User';
		$dimensionGroup = '';
		$gps = 'GPS';
		$dimensionID = $this->cla_descrip_user;
		if ($dimensionID <= 0)
			$dimensionID = $this->getDimClaDescrip($dimensionName);
			
		$this->cla_descrip_user = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);
		
		$dimensionName = 'Country';
		$dimensionID = $this->cla_descrip_country;
		if ($dimensionID <= 0)
			$dimensionID = $this->getDimClaDescrip($dimensionName);
		$this->cla_descrip_country = $this->addModelDimension($dimensionName, $dimensionID, $gps);
		
		$dimensionName = 'State';
		$dimensionID = $this->cla_descrip_state;
		if ($dimensionID <= 0)
			$dimensionID = $this->getDimClaDescrip($dimensionName);
		$this->cla_descrip_state = $this->addModelDimension($dimensionName, $dimensionID, $gps);

		$dimensionName = 'City';
		$dimensionID = $this->cla_descrip_city;
		if ($dimensionID <= 0)
			$dimensionID = $this->getDimClaDescrip($dimensionName);
		$this->cla_descrip_city = $this->addModelDimension($dimensionName, $dimensionID, $gps);
		
		$dimensionName = 'ZIP Code';
		$dimensionID = $this->cla_descrip_zipcode;
		if ($dimensionID <= 0)
			$dimensionID = $this->getDimClaDescrip($dimensionName);
		$this->cla_descrip_zipcode = $this->addModelDimension($dimensionName, $dimensionID, $gps);
		
		$dimensionName = 'Address';
		$dimensionID = $this->cla_descrip_address;
		if ($dimensionID <= 0)
			$dimensionID = $this->getDimClaDescrip($dimensionName);
		$this->cla_descrip_address = $this->addModelDimension($dimensionName, $dimensionID, $gps);
		
		$dimensionGroup = 'SurveyID';
		
// @EA 2015-08-31 Soporte de dimension agenda
		$sql = 
		"alter table sveForms_{$this->SurveyID} add AgendaID INT NULL";
		if (!$this->Repository->DataADOConnection->Execute($sql))
		{
			$ErrSQL = $this->Repository->DataADOConnection->ErrorMsg();
			savelogfile($sql.' -> '.$ErrSQL);
		}

		$dimensionName = 'Agenda';
		$dimensionID = $this->cla_descrip_agenda;
		if ($dimensionID <= 0)
			$dimensionID = $this->getDimClaDescrip($dimensionName);
		$this->cla_descrip_agenda = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);
//
		
		$dimensionName = 'Version';
		$dimensionID = $this->cla_descrip_version;
		if ($dimensionID <= 0)
			$dimensionID = $this->getDimClaDescrip($dimensionName);
		$this->cla_descrip_version = $this->addModelDimension($dimensionName, $dimensionID, $dimensionGroup);
		
		$consecutivo_dim_survey = -1;
		$this->createFactKeyDimension($consecutivo_dim_survey);

// Indicadores
		$indicatorName = 'Count';
		$fieldName = PRE_DIM_SVY_TABLE."KEY";
		$formula_bd = "COUNT(DISTINCT t1.".$fieldName.")";;
		$formato = '#,##0';
		$this->cla_indicador_count = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato);


		$indicatorName = 'Duration (minutes)';
		$formula_bd = "AVG(TIMESTAMPDIFF(MINUTE, CONCAT(date_sv,' ',hour_sv), CONCAT(dateend_sv,' ',hourend_sv)))";;
		$formato = '#,##0';

		$this->cla_indicador_elapsed = $this->addModelIndicatorCalculated($this->Repository, $this->cla_concepto, $indicatorName, $formula_bd, $formato, $consecutivo_dim_survey);

//		
		return true;
	}

	public static function delete(&$Repository, $id_modelartus){
//Este metodo eliminará la configuracion de los modelos de artus en ebavel y la metadata de dicho modelo en artus
		$aQuery = "SELECT cla_concepto, id_survey FROM si_sv_modelartus WHERE id_modelartus = {$id_modelartus} AND cla_concepto IS NOT NULL";
		$rs = $Repository->DataADOConnection->Execute($aQuery);
		$cla_concepto = 0;
		if($rs && !$rs->EOF) {
			$cla_concepto = $rs->fields['cla_concepto'];
			$id_survey = $rs->fields['id_survey'];
		}					
			
		if ($cla_concepto > 0) {

			$strOriginalWD = getcwd();
			$anInstanceModel = BITAMModel::NewModelWithModelID($Repository, $cla_concepto);
			
			if ($anInstanceModel && $anInstanceModel->permit_remove())
			{
// @EA 2015-07-22 Dejariamos las dimensiones globales ya que estan compartidas por Survey
				$sql = "delete from SI_CPTO_LLAVE where CLA_CONCEPTO = {$cla_concepto}";
				$Repository->ADOConnection->Execute($sql);
// @EA 2016-01-28 #FP09SS  Eliminar dimensiones globales si estas ya no se estan usando en algun otro modelo
				$sql = "SELECT cla_descrip AS dimkey FROM si_sv_modelartus_questions
						WHERE id_modelartus = {$id_modelartus} AND NOT cla_descrip IS NULL";
				$rs = $Repository->DataADOConnection->Execute($sql);
				if($rs) {
					$adimkeys = array();
					while (!$rs->EOF)
					{
						$adimkeys[] = $rs->fields['dimkey'];
						$rs->MoveNext();
					}
					
					foreach ($adimkeys as $dimkey)
					{
						ARTUSModelFromSurvey::deleteFromSI_DESCRIP_ENC($Repository, $dimkey);
					}
				}
//
				if ($anInstanceModel)
					$anInstanceModel->remove();

			}
			chdir($strOriginalWD);			
//Eliminar el periodo, dimensiones e indicadores de la metadata de eSurvey
			$aQuery = "UPDATE si_sv_modelartus
						SET cla_concepto = null,
						cla_descrip_user = null,
						cla_descrip_country = null, 
						cla_descrip_state = null, 
						cla_descrip_city = null, 
						cla_descrip_zipcode = null,
					    cla_descrip_address = null, 
					    cla_descrip_version = null, 
					    cla_descrip_agenda = null,
					    cla_descrip_sv = null, 
					    cla_descrip_sv_latitude = null, 
					    cla_descrip_sv_longitude = null,
					    cla_descrip_sv_accuracy = null, 
					    cla_descrip_sv_date = null, 
					    cla_descrip_sv_hour = null, 
					    cla_descrip_sv_enddate = null, 
					    cla_descrip_sv_endhour = null,
					    cla_indicador_count = null,
					    cla_indicador_elapsed = null
						WHERE id_modelartus = ".$id_modelartus;
			$Repository->DataADOConnection->Execute($aQuery);
			
			$aQuery = "UPDATE si_sv_modelartus_questions
						SET cla_descrip = null,
							cla_descrip_atributo = null,
							cla_descrip_foto = null,
							cla_descrip_documento = null,
							cla_descrip_comentario = null,
							cla_indicador = null,
							cla_indicador_score = null,
							cla_indicador_atributo = null
						WHERE id_modelartus = ".$id_modelartus;
			$Repository->DataADOConnection->Execute($aQuery);
			
			ARTUSModelFromSurvey::generateSP_ETL($Repository, $id_survey);
		}
	}

	public static function beforeDeleteCallback($id_modelartus){
		$aRepository = Metadata::getRepository();
		ARTUSModelFromSurvey::delete($aRepository, $id_modelartus);
	}

	public static function genMetadata($cla_concepto, $database_name, $cla_owner, $exportOnlyUser, $validateArtusAPI, $appCodeName, $aConnection){
		//ya guardado en metadata construimos los PHP's
		//incluimos el archivo de cgenmetadata.inc.php
		//buscamos la ruta de artus web del repositorio
		$artusPath = "";
		$path = "";

		/** Cambiamos la clave de configuracion a una de uso exclusivo que guardara la ruta de los PHPs de Artus en el servidor */
		$aQuery = "SELECT IFNULL(ref_configura, '') FROM si_configura WHERE cla_configura = 550";
		$rs = $aConnection->Execute($aQuery);
		if($rs && !$rs->EOF)
		{
			$path = trim($rs->fields[0]);
		}

		/** Si no se encontro la configuracion, buscamos en FBM */
		if( $path == "" )
		{
			/** Buscamos el usuario administrador del repositorio */
			$rs = $aConnection->Execute("SELECT cuenta_correo, password FROM si_usuario WHERE cla_usuario = 1");
			if($rs && !$rs->EOF)
			{
				$res = file_get_contents( "http://kpionline.bitam.com/fbm/getSaaSUserInfo.php?params=" . BITAMEncode( "user=". $rs->fields[0] ."&pwd=" . $rs->fields[1] ) );

				if( $res != '' )
				{
					$res = explode('SUCCESS=', $res);

					if( isset($res[1]) )
					{
						$res = explode('_AWSep_', BITAMDecode( $res[1] ));

						$pUrl = parse_url($res[8]);
						
						/** Tienen que pertenecer al mismo host, si no es asi, el proyecto de artus se encuentra en otro servidor */
						if( $pUrl['host'] == $_SERVER['HTTP_HOST'] )
							$path = realpath( $_SERVER['DOCUMENT_ROOT'] . $pUrl['path'] . '\\projects\\' );
					}
				}
			}
		}

		if($path != "")
		{
			//$path = parse_url($path);
			$artusURLPath = realpath($path . "/../");
			$pathFileCgenMetadata = $artusURLPath. "/model_manager/cgenmetadata.inc.php";
			if(is_file($pathFileCgenMetadata))
			{
				if (!function_exists('InModeSAAS'))
				{
					function InModeSAAS()
					{
						global $SAAS;
						return $SAAS;
					}
				}
				
				if(!function_exists("savelogfile"))
				{
					function savelogfile()
					{
					}
				}

				include_once($pathFileCgenMetadata);

				Utils::setFetchModeForConnection($aConnection, 2);
				//guardamos temporalmente la variable:
				$tmpRepositoryName = Session::get("BITAM_RepositoryName");
				Session::set("BITAM_RepositoryName", null);
				$aCGenMetadata = CGenMetadata::NewInstance($aConnection);
				Session::set("BITAM_RepositoryName", $tmpRepositoryName);
				$aCGenMetadata->sPath = $artusURLPath."/projects/".$database_name;
				
				if(!$exportOnlyUser){
  					//echo "Export Cube:" . intval($cla_concepto) . "<br>";
					$aCGenMetadata->ExportCube(intval($cla_concepto));
				}

        		//exportamos conexiones
				$aCGenMetadata->ExportConns();

				//echo "Export User:" . $cla_owner . "<br>";
				$aCGenMetadata->ExportUser($cla_owner);

				unset($aCGenMetadata);

				Utils::setFetchModeForConnection($aConnection);
			}elseif($validateArtusAPI){
				ARTUSModelFromSurvey::sendReport("No fue posible cargar el api (".$pathFileCgenMetadata.") de la configuracion 511 (".$path.") para generar los phps de los modelos de artus", $appCodeName);
			}
		}elseif($validateArtusAPI){
			ARTUSModelFromSurvey::sendReport("No fue posible obtener la configuracion 511 de si_configura para generar los phps de los modelos de artus", $appCodeName);
		}
	}

	function parseFieldPath($path, $idForm, $nom_tabla, $aConnection){
		
		$arrPath = array();

		//Patron para obtener la forma raiz o el campo en el ultimo nivel
		$rootPattern = "/(.*)\[(.*)\]({.*})?/";
		//Patron para obtener los joins
		$levelPattern = "/(.*)\[(.*):(.*)\]/";

		//Obtener las secciones a las que debe acceder para llegar al campo
		$arrForms = explode("->", $path);
		$nLevels = count($arrForms);

		if($nLevels > 0){
			//Forma raiz
			preg_match($rootPattern, $arrForms[0], $arrRoot);
			if(count($arrRoot) > 0){
				$aLastSection = array("form" => $idForm, "table" => $nom_tabla, "fieldKey" => "id_" . $idForm);
				$arrPath["root"] = array("form" => $arrRoot[1], "table" => $aLastSection["table"], "masterField" => "", "detailField" => $arrRoot[2]);

				//Obtener los joins necesarios para llegar al campo que esta en el ultimo nivel
				$arrPath["joins"] = array();
				for($i = 1; $i < ($nLevels-1); $i++){
					preg_match($levelPattern, $arrForms[$i], $arrLevel);
					if(count($arrLevel) > 0){
						//Join a la tabla de usuarios
						if(strtolower($arrLevel[1]) == "si_usuario"){
							//2015-03-10@HADR (#3JTIDT) Error de Query al consultar campo de un catalogo tipo repository user
							//Si la forma anterior a esta es de tipo repositorio de usuarios entonces el masterfield debe ser el id de la forma extendida

							$rs = $aConnection->Execute( "SELECT editType FROM si_forms_sections WHERE name = '". $aLastSection['form'] ."'" );

							if((stripos(strtolower($aLastSection["form"]), "frm_") !== false && $rs && !$rs->EOF) && $rs->fields['editType'] == "10"){
								$masterField = "id_".$aLastSection["form"];
							}else{
								$masterField = (stripos(strtolower($arrLevel[3]), "modifiedUser") !== false)?"modifiedUserKey":$arrLevel[3];
								$masterField = (stripos(strtolower($masterField), "createdUser") !== false)?"createdUserKey":$masterField;
							}
							$aLastSection = array("form" => "USUARIO", "table" => "si_usuario", "fieldKey" => "cla_usuario");
							$arrPath["joins"][] = array("form" => $aLastSection["form"], "table" => $aLastSection["table"], "masterField" => $masterField, "detailField" => $aLastSection["fieldKey"]);
						}elseif(strtolower($arrLevel[1]) == "si_forms_elementattrs"){
							//Detectar si es un campo de tipo catalogo con valores q1
							$aLastSection = array("form" => "elementattrs", "table" => "si_forms_elementattrs", "fieldKey" => "id_elementattr");
							$arrPath["joins"][] = array("form" => $aLastSection["form"], "table" => $aLastSection["table"], "masterField" => $arrLevel[3], "detailField" => $aLastSection["fieldKey"]);
						}else{
							
							$rs = $aConnection->Execute( "SELECT A.id, CONCAT('SI_FORMS_', B.codeName, '_', A.id) AS tablename FROM si_forms_sections A, si_forms_apps B WHERE id = '{$arrLevel[1]}' AND A.appId = B.id_app" );

							if($rs && !$rs->EOF){
								$aLastSection = array("form" => $rs->fields['id'], "table" => $rs->fields['tablename'], "fieldKey" => 'id_' . $rs->fields['id']);
								$arrPath["joins"][] = array("form" => $arrLevel[1], "table" => $aLastSection["table"], "masterField" => $arrLevel[3], "detailField" => $arrLevel[2]);
							}
						}
					}
				}

				//Obtener el campo que esta en el ultimo nivel
				preg_match($rootPattern, $arrForms[$nLevels-1], $arrField);
				if(count($arrField) > 0){

					$rs = $aConnection->Execute("SELECT label FROM SI_FORMS_FIELDSFORMS WHERE id = '". $arrField[1] ."'");

					$arrPath["field"] = array("form" => $aLastSection["form"], "table" => $aLastSection["table"], "fieldValue" => $arrField[1], "fieldKey" => $aLastSection["fieldKey"], "type" => $arrField[2], "label" => (isset($arrField[3]) && strlen($arrField[3]) > 2?substr($arrField[3], 1, strlen($arrField[3])-2): $rs && !$rs->EOF ? $rs->fields['label'] : '' ));
				}
			}
		}

		return $arrPath;
	}

/*
	public function parseFieldPath($path, $aSection, $nom_tabla, $aConnection){
		$arrPath = array();

		//Patron para obtener la forma raiz o el campo en el ultimo nivel
		$rootPattern = "/(.*)\[(.*)\]({.*})?/";
		//Patron para obtener los joins
		$levelPattern = "/(.*)\[(.*):(.*)\]/";

		//Obtener las secciones a las que debe acceder para llegar al campo
		$arrForms = explode("->", $path);
		$nLevels = count($arrForms);

		if($nLevels > 0){
			//Forma raiz
			preg_match($rootPattern, $arrForms[0], $arrRoot);
			if(count($arrRoot) > 0){
				$aLastSection = array("form" => $aSection->id, "table" => $nom_tabla, "fieldKey" => $aSection->getFieldKey());
				$arrPath["root"] = array("form" => $arrRoot[1], "table" => $aLastSection["table"], "masterField" => "", "detailField" => $arrRoot[2]);

				//Obtener los joins necesarios para llegar al campo que esta en el ultimo nivel
				$arrPath["joins"] = array();
				for($i = 1; $i < ($nLevels-1); $i++){
					preg_match($levelPattern, $arrForms[$i], $arrLevel);
					if(count($arrLevel) > 0){
						//Join a la tabla de usuarios
						if(strtolower($arrLevel[1]) == "si_usuario"){
							//2015-03-10@HADR (#3JTIDT) Error de Query al consultar campo de un catalogo tipo repository user
							//Si la forma anterior a esta es de tipo repositorio de usuarios entonces el masterfield debe ser el id de la forma extendida
							if((stripos(strtolower($aLastSection["form"]), "frm_") !== false && $aSectionExtendedForm = BITAMAppSection::withName($aLastSection["form"], $aConnection)) && $aSectionExtendedForm->editType == "10"){
								$masterField = "id_".$aLastSection["form"];
							}else{
								$masterField = (stripos(strtolower($arrLevel[3]), "modifiedUser") !== false)?"modifiedUserKey":$arrLevel[3];
								$masterField = (stripos(strtolower($masterField), "createdUser") !== false)?"createdUserKey":$masterField;
							}
							$aLastSection = array("form" => "USUARIO", "table" => "si_usuario", "fieldKey" => "cla_usuario");
							$arrPath["joins"][] = array("form" => $aLastSection["form"], "table" => $aLastSection["table"], "masterField" => $masterField, "detailField" => $aLastSection["fieldKey"]);
						}elseif(strtolower($arrLevel[1]) == "si_forms_elementattrs"){
							//Detectar si es un campo de tipo catalogo con valores q1
							$aLastSection = array("form" => "elementattrs", "table" => "si_forms_elementattrs", "fieldKey" => "id_elementattr");
							$arrPath["joins"][] = array("form" => $aLastSection["form"], "table" => $aLastSection["table"], "masterField" => $arrLevel[3], "detailField" => $aLastSection["fieldKey"]);
						}else{
							$aSectionLevel = BITAMAppSection::withName($arrLevel[1], $aConnection);
							if($aSectionLevel){
								$aLastSection = array("form" => $aSectionLevel->id, "table" => $aSectionLevel->getTableName(), "fieldKey" => $aSectionLevel->getFieldKey());
								$arrPath["joins"][] = array("form" => $arrLevel[1], "table" => $aLastSection["table"], "masterField" => $arrLevel[3], "detailField" => $arrLevel[2]);
							}
						}
					}
				}

				//Obtener el campo que esta en el ultimo nivel
				preg_match($rootPattern, $arrForms[$nLevels-1], $arrField);
				if(count($arrField) > 0){
					$arrPath["field"] = array("form" => $aLastSection["form"], "table" => $aLastSection["table"], "fieldValue" => $arrField[1], "fieldKey" => $aLastSection["fieldKey"], "type" => $arrField[2], "label" => (isset($arrField[3]) && strlen($arrField[3]) > 2?substr($arrField[3], 1, strlen($arrField[3])-2):""));
				}
			}
		}

		return $arrPath;
	}
*/

	public function setError($title, $description){
		$this->error->setError(array("ErrorMsg" => $title, "ErrorDescription" => $description));
	}

	public function hasError(){
		return $this->error->count()>0;
	}

	public function setWarning($title, $description){
		$this->warning->setError(array("ErrorMsg" => $title, "ErrorDescription" => $description));
	}

	public function hasWarning(){
		return $this->warning->count()>0;
	}
}

class ARTUSIndicatorFromSurvey extends ARTUSModelFunctionBasics
{
	public $Repository;
	public $id = -1;
	public $id_modelartus;
	public $id_question;
	public $id_section;
	public $section_type;
	//si_cpto_atrib
	public $cla_concepto = -1;
	//si_indicador
	public $cla_indicador = -1;
	public $nom_indicador = "";
	public $consecutivo = 0;
	public $path_ebavel = "";

	function __destruct() {

	}
	
	static function NewInstance(&$theRepository, $id_modelartus, $cla_concepto, &$rst)
	{
		$aArtusIndicator = new ARTUSIndicatorFromSurvey();
		$aArtusIndicator->Repository = $theRepository;
		$aArtusIndicator->id_modelartus = $id_modelartus;
		$aArtusIndicator->cla_concepto = $cla_concepto;
		$aArtusIndicator->fillParameters($rst);
		
		return $aArtusIndicator;
	}

	public function fillParameters(&$rst)
	{
		$this->id = (int) $rst->fields['id'];
		$this->id_question = (int) $rst->fields['questionid'];
		$this->cla_indicador = (int) $rst->fields['cla_indicador'];
		
		$this->id_section = (int) $rst->fields['sectionid'];
		$this->section_type = trim($rst->fields['sectiontype']);
		$this->path_ebavel = trim($rst->fields['path_ebavel']);
		
		$rename_attribute = '';
		$npos = stripos($this->path_ebavel, '{');
		if ($npos !== false)
			$rename_attribute = substr($this->path_ebavel, $npos+1, strlen($this->path_ebavel)-$npos-2);
		if ($rename_attribute == '')
			$this->nom_indicador = trim($rst->fields['attributename']);
		else
		{
			$this->nom_indicador = $rename_attribute;
//			print '<br>Rename Indicator -> {'.$rename_attribute.'}';
		}
		
	}

	function saveSectionIndicator($cla_concepto, $formato, $consecutivo_dim_survey, $id_survey)
	{
		$this->cla_concepto = $cla_concepto;
		$cla_indicador = -1;
		$nom_indicador = $this->nom_indicador;
		$str_tipo_dato = 'double';
		$metaconnection = $this->Repository->ADOConnection;
		$dataconnection = $this->Repository->DataADOConnection;
						
		if ($this->section_type == sectNormal)
		{
			$bExistSectionTable = true;
			$consecutivo = $consecutivo_dim_survey;
			$nom_tabla = PRE_DIM_SVY_TABLE.$id_survey;
		}
		else
		{
			$nom_tabla = PRE_DIM_SEC_TABLE.$this->id_section;
			
			$sql =
			"SELECT t1.CONSECUTIVO AS consecutivo
			FROM	SI_CPTO_LLAVE t1 INNER JOIN SI_DESCRIP_ENC t2 ON (t2.CLA_DESCRIP = t1.CLA_DESCRIP)
			WHERE	t1.CLA_CONCEPTO = {$cla_concepto} AND
			t2.NOM_TABLA = '{$nom_tabla}'";
			
			$consecutivo = -1;
			
			$bExistSectionTable = $this->existRow($metaconnection, $sql, $consecutivo);
			
			if ($consecutivo <= 0)
				savelogfile("QuestionID-> {$this->id_question} .. Para agregar indicadores, NO existe al menos una pregunta como dimension para la seccion -> {$this->id_section}");
		}
		
		if ($bExistSectionTable && $consecutivo > 0)
		{
//			print '<br>$consecutivo -> '.$consecutivo;
			$nom_fisico = PRE_IND_FIELD.$this->id_question;
			$formula_bd = "SUM( {$nom_fisico} )";
			$cla_indicador = $this->addModelIndicatorCalculated($this->Repository, $cla_concepto, $nom_indicador, $formula_bd, $formato, $consecutivo);
			
			if ($cla_indicador > 0)
			{
/*
				$sql = 
				"update SI_INDICADOR set FORMULA_BD = '{$formula_bd}' WHERE CLA_INDICADOR = {$cla_indicador}";
				if (!$metaconnection->Execute($sql))
				{
					$ErrSQL = $metaconnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}
*/				
				$sql =
				"alter table {$nom_tabla} add {$nom_fisico} {$str_tipo_dato} NULL";
				
				if (!$dataconnection->Execute($sql))
				{
					$ErrSQL = $dataconnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}
			}
		}
		
		return $cla_indicador;
	}

	public function renameIndicator()
	{
		if ($this->cla_indicador > 0)
		{
			$nom_indicador = $this->Repository->ADOConnection->quote($this->nom_indicador);
			$formula_usr = $this->Repository->ADOConnection->quote('{'.$this->nom_indicador.'}');

			$sql = 
			"update	SI_INDICADOR set 
				NOM_INDICADOR = {$nom_indicador},
				FORMULA_USR = {$formula_usr}
			 where  CLA_INDICADOR = {$this->cla_indicador}";
			 
			if (!$this->Repository->ADOConnection->Execute($sql))
			{
				$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}
		}
	}

	public function saveSincronize(){
		if ($this->cla_indicador > 0)
		{
			$sql = 
			"update si_sv_modelartus_questions 
			 set 	cla_indicador = {$this->cla_indicador}
			 where  id_modelartus = {$this->id_modelartus} and id_question = {$this->id_question}";

			if (!$this->Repository->DataADOConnection->Execute($sql))
			{
				$ErrSQL = $this->Repository->DataADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}
		}
	}

	public static function delete($cla_concepto, $cla_indicador, $aConnection){
		//si_cpto_atrib, si_indicador
	}
}

class ARTUSQuestionFromMAPeBavel
{
	public $Repository;
	public $id_modelartus;
	public $id_question;
	public $id_field;
	public $ebavel_table;
	public $ebavel_form;
	
	function __destruct() {

	}
	
	static function NewInstance(&$theRepository, $id_modelartus, $rst)
	{
		$aArtusMAP = new ARTUSQuestionFromMAPeBavel();
		$aArtusMAP->Repository = $theRepository;
		$aArtusMAP->id_modelartus = $id_modelartus;
		$aArtusMAP->fillParameters($rst);
		return $aArtusMAP;
	}	
	
	public function fillParameters($rsFields)
	{
		$this->id = (int) $rsFields->fields['id'];
		$this->id_question = (int) $rsFields->fields['id_question'];

		$this->id_field = trim($rsFields->fields['id_field']);
		$this->ebavel_table = trim($rsFields->fields['ebavel_table']);
		$this->ebavel_form = trim($rsFields->fields['ebavel_form']);
	}
}

class ARTUSDimensionFromSurvey extends ARTUSModelFunctionBasics
{
	public $Repository;
	public $id = -1;
	public $id_modelartus;
	public $id_inputtype;
	public $id_question;
	public $id_qtype;
	public $id_section;
	public $id_catalog;
	public $sourcetype; // Cuando es = 1, es de tipo Catalogo
	public $section_type;
	public $section_name;
	public $cla_concepto = -1;
	public $cla_descrip = -1;
	public $cla_descrip_atributo = -1;
	public $cla_indicador_score = -1;
	public $cla_indicador_atributo = -1;
	public $nom_dimension = "";
	public $path_ebavel;
	public $id_member = -1;
	public $renamed = false;
	public $aJoins = array();
	public $longfield = 255;

	function __destruct() {

	}
	
	static function NewInstance(&$theRepository, $id_modelartus, $cla_concepto, $rst)
	{
		$aArtusDimension = new ARTUSDimensionFromSurvey();
		$aArtusDimension->Repository = $theRepository;
		$aArtusDimension->id_modelartus = $id_modelartus;
		$aArtusDimension->cla_concepto = $cla_concepto;
		$aArtusDimension->fillParameters($rst);
		return $aArtusDimension;
	}

	public function fillParameters($rsFields)
	{
		$this->id = (int) $rsFields->fields['id'];
		$this->id_qtype = (int) $rsFields->fields['qtypeid'];
		$this->id_inputtype = (int) $rsFields->fields['inputtypeid'];
		$this->id_question = (int) $rsFields->fields['questionid'];
		$this->cla_descrip = (int) $rsFields->fields['cla_descrip'];
		
		$this->cla_descrip_atributo = (int) $rsFields->fields['cla_descrip_atributo'];
		
		$this->id_section = (int) $rsFields->fields['sectionid'];
		$this->id_catalog = (int) $rsFields->fields['catalogid'];
		$this->sourcetype = (int) $rsFields->fields['sourcetype'];
		$this->section_type = trim($rsFields->fields['sectiontype']);
		$this->section_name = trim($rsFields->fields['sectionname']);
		$this->path_ebavel = trim($rsFields->fields['path_ebavel']);
		
		$rename_attribute = '';
		$npos = stripos($this->path_ebavel, '{');
		if ($npos !== false)
			$rename_attribute = substr($this->path_ebavel, $npos+1, strlen($this->path_ebavel)-$npos-2);
			
		if ($this->isCatalogDimension())
		{
			$npos = stripos($this->path_ebavel, '[CAT]');
			if ($npos > 0)
				$this->id_member = (int) substr($this->path_ebavel, 0, $npos);
		}
		
		if ($rename_attribute == '')
			$this->nom_dimension = trim($rsFields->fields['attributename']);
		else
		{
			$this->nom_dimension = $rename_attribute;
			$this->renamed = true;
//			print '<br>Rename Dimension -> {'.$rename_attribute.'}';
		}
		
// @EA 2016-06-14 Heredamos la longitud dependiendo el tipo de pregunta
		$Longitud = 255;
		if ($this->id_qtype == qtpOpenAlpha)
			$Longitud = (int) $rsFields->fields['longitudalfa'];
		elseif ($this->id_qtype == qtpOpenDate)
			$Longitud = 20;
// @EA 2017-06-06 Soportar preguntas abiertas tipo text
		elseif ($this->id_qtype == qtpOpenString)
			$Longitud = 256;

		if ($Longitud <= 0)
			$Longitud = 255;
			
		$this->longfield = $Longitud;
		
//		print '<br>'.$this->nom_dimension.' ('.$this->longfield.')';
//		
	}

	public function isCatalogDimension()
	{
		return ($this->id_catalog > 0 && $this->sourcetype == 1);
	}

	public function renameDimension()
	{
//		print '<br>function renameDimension()';
		
		if ($this->cla_descrip > 0)
		{
			$nom_dimension = $this->nom_dimension;

			if ($this->id_qtype == qtpGPS || $this->id_qtype == qtpMyLocation)
				$nom_dimension .= ' Latitude';
				
			$nom_dimension = $this->Repository->ADOConnection->quote($nom_dimension);
			
// @EA 2016-01-13 Faltaba considerar el agrupador
			$agrupador = $this->Repository->ADOConnection->quote($this->section_name);

			$sql = 
			"update	SI_DESCRIP_ENC set 
				NOM_LOGICO = {$nom_dimension},
				AGRUPADOR = {$agrupador}
			 where  CLA_DESCRIP = {$this->cla_descrip}";
			 
//			print '<br>'.$sql;
			 
			if (!$this->Repository->ADOConnection->Execute($sql))
			{
				$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
//				print '<br>'.$sql.' -> '.$ErrSQL;
				savelogfile($sql.' -> '.$ErrSQL);
			}
			
			$sql = 
			"update	SI_CPTO_LLAVE set 
				NOM_LOGICO = {$nom_dimension},
				AGRUPADOR = {$agrupador}
			 where  CLA_DESCRIP = {$this->cla_descrip}";
			 
			if (!$this->Repository->ADOConnection->Execute($sql))
			{
				$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
//				print '<br>'.$sql.' -> '.$ErrSQL;
			}
		}
	}

	public function saveSincronize(){
		if ($this->cla_descrip > 0)
		{
			$sql = 
			"update si_sv_modelartus_questions 
			 set 	cla_descrip = {$this->cla_descrip}, 
			 		cla_indicador_score = {$this->cla_indicador_score}, 
			 		cla_descrip_atributo = {$this->cla_descrip_atributo},
			 		cla_indicador_atributo = {$this->cla_indicador_atributo}
			 where  id_modelartus = {$this->id_modelartus} and id = {$this->id}";

			if (!$this->Repository->DataADOConnection->Execute($sql))
			{
				$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}
		}
	}

	function saveCatalogDimension($cla_concepto, $cla_descrip, $dimensionGroup, $id_survey, &$cla_indicador=-1)
	{
		$this->cla_concepto = $cla_concepto;

		$tipo_dato = '2';
		$long_dato = '255';
		$str_tipo_dato = 'varchar(255)';
		
		$nom_dimension = $this->nom_dimension;
		$id_member = $this->id_member;
		
		if ($id_member <= 0)
			return -1;
		
		$ErrSQL = '';
		
		$nom_tabla = PRE_DIM_CAT_TABLE.$this->id_question;
		
		$parent_join = PRE_DIM_SVY_TABLE.'KEY';
		$key = PRE_DIM_CAT_TABLE.'KEY';
		
		$metaconnection = $this->Repository->ADOConnection;
		$dataconnection = $this->Repository->DataADOConnection;
		
		if ($cla_descrip <= 0)
		{
			$nom_fisico = PRE_DIM_MEM_FIELD.$id_member;
			
			$bExistCatalogTable = $this->existRow($metaconnection, "select CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}'");
			
			$this->existRow($metaconnection, "select CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}' and NOM_FISICO = '{$nom_fisico}'", $cla_descrip);
			
			if ($cla_descrip <= 0)
			{
				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				
				$tipo_dimension = 3;
				$default_key = '\'NA\'';

				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $nom_dimension, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);
				
				if ($bExistCatalogTable)
					$sql =
					"alter table {$nom_tabla} add {$nom_fisico} {$str_tipo_dato} NULL";
				else
					$sql = 
					"create table {$nom_tabla} ".
					"( {$key} int null,
					   {$parent_join} INT NOT NULL,
					   {$nom_fisico} varchar(255) NULL,
					   key ({$key}),
					   key ({$nom_fisico}, {$parent_join}),
					   key ({$parent_join}))
					   ";
				   
				if (!$dataconnection->Execute($sql))
				{
					$ErrSQL = $dataconnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}
			}
		}

		if ($cla_descrip > 0)
		{
			$nom_fisico = PRE_DIM_MEM_FIELD.$id_member;
			
			$arrMax = ARTUSDimensionFromSurvey::getMaxConsecutivoNivel($cla_concepto, $metaconnection);
			$consecutivo = $arrMax["consecutivo"];
			$nivel = $arrMax["nivel"];
			
			$ErrSQL = $this->insertLocalDimension($metaconnection, $consecutivo, $nivel, $cla_descrip, $nom_dimension, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup, $cla_concepto);
			
			$this->insertSI_DESCRIP_KEYS($metaconnection, $nom_tabla, $parent_join, $parent_join, $cla_concepto);

/* No existen por lo pronto los scores asociados a catalogos			
			if ($this->id_qtype == qtpSingle)
			{
				$this->addScore($cla_concepto, $consecutivo, $nom_tabla, $cla_indicador);
			}
*/			
			
		}

		return $cla_descrip;
	}

	function saveSectionDimension($cla_concepto, $cla_descrip, $dimensionGroup, $id_survey, &$cla_indicador=-1)
	{
		$this->cla_concepto = $cla_concepto;
		
		$tipo_dato = '2';
		$long_dato = $this->longfield;
		
// @EA 2017-06-06 Soporte de campos tipo LONGTEXT
		if ($long_dato > 0 && $long_dato <= 255)
			$str_tipo_dato = "varchar({$long_dato})";
		else
			$str_tipo_dato = "LONGTEXT";
//

		$nom_dimension = $this->nom_dimension;
		$ErrSQL = '';
		
		$parent_join = PRE_DIM_SVY_TABLE.'KEY';
		$this->cla_concepto = $cla_concepto;
		
		$nom_tabla = PRE_DIM_SEC_TABLE.$this->id_section;
		$key = PRE_DIM_SEC_TABLE.'KEY';
		
		$metaconnection = $this->Repository->ADOConnection;
		$dataconnection = $this->Repository->DataADOConnection;
		
		$cla_descrip_gps = -1;
		
		if ($cla_descrip <= 0)
		{
			$bExistSectionTable = $this->existRow($metaconnection, "select 1 as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}'");

			if ($this->isCatalogDimension())
			{
				if ($this->id_member <= 0)
					return -1;
					
				$nom_fisico = PRE_DIM_MEM_FIELD.$this->id_member;
				if ($this->id_question < 0)
					$nom_fisico = PRE_DIM_MEM_FIELD_INLINE.$this->id_member;
			}
			else
			{
				$nom_fisico = PRE_DIM_DSC_FIELD.$this->id_question;
				if ($this->id_question < 0)
					$nom_fisico = PRE_DIM_DSC_FIELD_INLINE.$this->id_section;
				else
				{
					if ($this->id_qtype == qtpGPS || $this->id_qtype == qtpMyLocation)
					{
						$nom_dimension .= ' Latitude';
						$nom_fisico .= 'LAT';

						$this->existRow($metaconnection, "select CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}' and NOM_FISICO = '{$key}'", $cla_descrip_gps);
// @EA 2016-05-19 Las dimensiones tipo GPS (Latitude y Longitude mecesitan una dimension padre para funcionar)
						if ($cla_descrip_gps <= 0)
						{
							$cla_descrip_gps = $this->getMaxClaDescrip($metaconnection);
							$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip_gps, $dimensionGroup, $nom_tabla, $key, 7, 0, $dimensionGroup);
						}

						$consecutivo = -1;
						$this->existRow($metaconnection, "select CONSECUTIVO as consecutivo from SI_CPTO_LLAVE where CLA_CONCEPTO = {$cla_concepto} AND CLA_DESCRIP = {$cla_descrip_gps}", $consecutivo);
						if ($consecutivo <= 0)
						{
							$arrMax = ARTUSDimensionFromSurvey::getMaxConsecutivoNivel($cla_concepto, $metaconnection);
							$consecutivo = $arrMax["consecutivo"];
							$nivel = $arrMax["nivel"];
							$ErrSQL = $this->insertLocalDimension($metaconnection, $consecutivo, $nivel, $cla_descrip_gps, $dimensionGroup, $key, 7, 0, $dimensionGroup, $cla_concepto);
						}
					}
				}
			}
				
			$this->existRow($metaconnection, "select CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}' and NOM_FISICO = '{$nom_fisico}'", $cla_descrip);
			
			if ($cla_descrip <= 0)
			{
				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				
				$tipo_dimension = 3;
				$default_key = '\'NA\'';

				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $nom_dimension, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);

				if ($bExistSectionTable)
					$sql =
					"alter table {$nom_tabla} add {$nom_fisico} {$str_tipo_dato} NULL";
				else
					$sql = 
					"create table {$nom_tabla} ".
					"( {$key} int null ,
					   {$parent_join} INT NOT NULL,
					   {$nom_fisico} varchar({$long_dato}) DEFAULT NULL,
					   key ({$key}),
					   key ({$parent_join}),
					   key ({$nom_fisico}, {$parent_join}))
					   ";
				   
				if (!$dataconnection->Execute($sql))
				{
					$ErrSQL = $dataconnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}
			}
		}

		if ($cla_descrip > 0)
		{
			if ($this->isCatalogDimension())
			{
				if ($this->id_member <= 0)
					return -1;
					
				$nom_fisico = PRE_DIM_MEM_FIELD.$this->id_member;
				if ($this->id_question < 0)
					$nom_fisico = PRE_DIM_MEM_FIELD_INLINE.$this->id_member;
			}
			elseif ($this->id_qtype == qtpGPS || $this->id_qtype == qtpMyLocation)
			{
				$nom_fisico = PRE_DIM_DSC_FIELD.$this->id_question;
				$nom_fisico .= 'LAT';
			}
			else
			{
				$nom_fisico = PRE_DIM_DSC_FIELD.$this->id_question;
				if ($this->id_question < 0)
					$nom_fisico = PRE_DIM_DSC_FIELD_INLINE.$this->id_section;
			}
			
			$arrMax = ARTUSDimensionFromSurvey::getMaxConsecutivoNivel($cla_concepto, $metaconnection);
			$consecutivo = $arrMax["consecutivo"];
			$nivel = $arrMax["nivel"];
			
			$ErrSQL = $this->insertLocalDimension($metaconnection, $consecutivo, $nivel, $cla_descrip, $nom_dimension, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup, $cla_concepto);
			
			if ($this->id_qtype == qtpGPS || $this->id_qtype == qtpMyLocation)
			{
				$this->insertAttribute($metaconnection, $cla_descrip_gps, $cla_descrip, $nom_dimension, $nom_tabla, $nom_fisico);

				$nom_dimension = $this->nom_dimension.' Longitude';
				$nom_fisico = PRE_DIM_DSC_FIELD.$this->id_question.'LON';

				$cla_descrip_lon = -1;
				$this->existRow($metaconnection, "select CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}' and NOM_FISICO = '{$nom_fisico}'", $cla_descrip_lon);
// @EA 2016-05-19 Las dimensiones tipo GPS (Latitude y Longitude mecesitan una dimension padre para funcionar)
				if ($cla_descrip_lon <= 0)
				{
					$cla_descrip_lon = $this->getMaxClaDescrip($metaconnection);
					$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip_lon, $nom_dimension, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);
					
					$sql = "alter table {$nom_tabla} add {$nom_fisico} {$str_tipo_dato} NULL";
					if (!$dataconnection->Execute($sql))
					{
						$ErrSQL = $dataconnection->ErrorMsg();
						savelogfile($sql.' -> '.$ErrSQL);
					}
				}
				$arrMax = ARTUSDimensionFromSurvey::getMaxConsecutivoNivel($cla_concepto, $metaconnection);
				$consecutivo = $arrMax["consecutivo"];
				$nivel = $arrMax["nivel"];
				$ErrSQL = $this->insertLocalDimension($metaconnection, $consecutivo, $nivel, $cla_descrip_lon, $nom_dimension, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup, $cla_concepto);
				$this->insertAttribute($metaconnection, $cla_descrip_gps, $cla_descrip_lon, $nom_dimension, $nom_tabla, $nom_fisico);
				
				$this->cla_descrip_atributo = $cla_descrip_lon;
			}		
			
			$this->insertSI_DESCRIP_KEYS($metaconnection, $nom_tabla, $parent_join, $parent_join, $cla_concepto);
			
			if ($this->id_qtype == qtpSingle)
			{
				$this->addScore($cla_concepto, $consecutivo, $nom_tabla, $cla_indicador);
			}
		}

		return $cla_descrip;
	}
	
	function addScore($cla_concepto, $consecutivo_dim, $nom_tabla, &$cla_indicador=-1)
	{
// Consideramos el indicador de tipo score
		$nom_indicador = $this->nom_dimension. ' (Score)';
		$formato = '#,##0';
		$nom_fisico_score = PRE_IND_SCORE.$this->id_question;
		$formula_bd = "SUM( {$nom_fisico_score} )";
		
		
		$sql = 
		"select cla_indicador as consecutivo from SI_INDICADOR where FORMULA_BD = '{$formula_bd}' and CLA_CONCEPTO = {$cla_concepto}";
		$ExistIndicator = $this->existRow($this->Repository->ADOConnection, $sql);

		if (!$ExistIndicator)
		{
			$dataconnection = $this->Repository->DataADOConnection;
			
			$sql = 
			"alter table {$nom_tabla} add $nom_fisico_score double null";
			
			if (!$dataconnection->Execute($sql))
			{
				$ErrSQL = $dataconnection->ErrorMsg();
				savelogfile($sql.' -> '.$ErrSQL);
			}
			
			$cla_indicador = $this->addModelIndicatorCalculated($this->Repository, $cla_concepto, $nom_indicador, $formula_bd, $formato, $consecutivo_dim);
		}
//		
	}
	
	function saveMultiDimension($cla_concepto, $cla_descrip, &$cla_descrip_atributo, $dimensionGroup, $id_survey, &$cla_indicador=-1, &$cla_indicador_atributo=-1, $masterDetail=false)
	{
		$this->cla_concepto = $cla_concepto;
	
		$tipo_dato = '2';
		$long_dato = $this->longfield;

// @EA 2017-06-06 Soporte de campos tipo LONGTEXT
		if ($long_dato > 0 && $long_dato <= 255)
			$str_tipo_dato = "varchar({$long_dato})";
		else
			$str_tipo_dato = "LONGTEXT";
//

		$nom_dimension_atributo = $this->nom_dimension.' (Value)';
		$nom_indicador = $this->nom_dimension. ' (Score)';
		$nom_dimension = $this->nom_dimension;
		$ErrSQL = '';

		$this->cla_concepto = $cla_concepto;
		
		$metaconnection = $this->Repository->ADOConnection;
		$dataconnection = $this->Repository->DataADOConnection;
		
		if ($masterDetail)
		{
			$parent_join = PRE_DIM_SEC_TABLE.'KEY';
			$nom_tabla = PRE_DIM_WPC_TABLE.$this->id_question;
			$key = PRE_DIM_WPC_TABLE.'KEY';
		}
		else
		{
			$parent_join = PRE_DIM_SVY_TABLE.'KEY';
			$nom_tabla = PRE_DIM_MPC_TABLE.$this->id_question;
			$key = PRE_DIM_MPC_TABLE.'KEY';
		}
		
		$nom_fisico = PRE_DIM_DSC_FIELD.$this->id_question;
		$nom_fisico_atributo = PRE_DIM_MPC_FIELD.$this->id_question;
		$nom_fisico_score = PRE_IND_SCORE.$this->id_question;
		
		if ($cla_descrip <= 0)
		{
			$bExistMultiTable = $this->existRow($metaconnection, "select 1 as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}'");

			$this->existRow($metaconnection, "select CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}' and NOM_FISICO = '{$nom_fisico}'", $cla_descrip);

			$cla_descrip_atributo = -1;

			if ($cla_descrip <= 0)
			{
				$cla_descrip = $this->getMaxClaDescrip($metaconnection);
				
				$tipo_dimension = 3;
				$default_key = '\'NA\'';
				
				$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip, $nom_dimension, $nom_tabla, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup);
			}				
				
			if ($this->id_inputtype == mpcText)
			{
				$this->existRow($metaconnection, "select CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}' and NOM_FISICO = '{$nom_fisico_atributo}'", $cla_descrip_atributo);
				
				$tipo_dato_atributo = '2';
				$long_dato_atributo = '255';
				
				if ($cla_descrip_atributo <= 0)
				{
					$cla_descrip_atributo = $this->getMaxClaDescrip($metaconnection);
					$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip_atributo, $nom_dimension_atributo, $nom_tabla, $nom_fisico_atributo, $tipo_dato_atributo, $long_dato_atributo, $dimensionGroup);
				}
				
			}
			elseif ($this->id_inputtype == mpcNumeric)
			{
				$tipo_dato_atributo = '7';
				$long_dato_atributo = '0';
				$str_tipo_dato = 'double';
				
				$nom_fisico_atributo = PRE_DIM_MPC_FIELD.$this->id_question;

				$this->existRow($metaconnection, "select CLA_DESCRIP as consecutivo from SI_DESCRIP_ENC where NOM_TABLA = '{$nom_tabla}' and NOM_FISICO = '{$nom_fisico_atributo}'", $cla_descrip_atributo);
				
				if ($cla_descrip_atributo <= 0)
				{
					$cla_descrip_atributo = $this->getMaxClaDescrip($metaconnection);
					$ErrSQL = $this->insertGlobalDimension($metaconnection, $cla_descrip_atributo, $nom_dimension_atributo, $nom_tabla, $nom_fisico_atributo, $tipo_dato_atributo, $long_dato_atributo, $dimensionGroup);
				}
			}
		
			if (!$bExistMultiTable)
			{
				$survey_key = "";
				$survey_key_index = "";
				if ($masterDetail)
				{
					$field_survey = PRE_DIM_SVY_TABLE.'KEY';
					$survey_key = "{$field_survey} INT NOT NULL,";
					$survey_key_index = "key ({$field_survey}),";
				}
				
				$sql = 
				"create table {$nom_tabla} ".
				"( {$key} INT NULL,
				   {$parent_join} INT NOT NULL,
				   {$survey_key}
				   {$nom_fisico} varchar({$long_dato}) NULL,
				   {$nom_fisico_atributo} {$str_tipo_dato} NULL,
				   {$nom_fisico_score} double NULL,
				   key ({$key}),
				   key ({$parent_join}, {$nom_fisico}),
				   {$survey_key_index}
				   key ({$nom_fisico}, {$parent_join}))
				   ";
				   
				if (!$dataconnection->Execute($sql))
				{
					$ErrSQL = $dataconnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}
			}
		}

		if ($cla_descrip > 0)
		{
			$nom_fisico = PRE_DIM_DSC_FIELD.$this->id_question;
			
			if ($masterDetail)
				$nom_tabla = PRE_DIM_WPC_TABLE.$this->id_question;
			else
				$nom_tabla = PRE_DIM_MPC_TABLE.$this->id_question;
			
			$arrMax = ARTUSDimensionFromSurvey::getMaxConsecutivoNivel($cla_concepto, $metaconnection);
			$consecutivo = $arrMax["consecutivo"];
			$nivel = $arrMax["nivel"];
			
			$ErrSQL = $this->insertLocalDimension($metaconnection, $consecutivo, $nivel, $cla_descrip, $nom_dimension, $nom_fisico, $tipo_dato, $long_dato, $dimensionGroup, $cla_concepto);

// Consideramos el indicador de tipo score
			$formato = '#,##0';
			$agrupador = 'SUM';
			$formula_bd = "SUM( {$nom_fisico_score} )";
			$cla_indicador = $this->addModelIndicatorCalculated($this->Repository, $cla_concepto, $nom_indicador, $formula_bd, $formato, $consecutivo);
//
			if ($this->id_inputtype != mpcCheckBox)
			{
				$arrMax = ARTUSDimensionFromSurvey::getMaxConsecutivoNivel($cla_concepto, $metaconnection);
				$consecutivo = $arrMax["consecutivo"];
				$nivel = $arrMax["nivel"];
				
				$ErrSQL = $this->insertLocalDimension($metaconnection, $consecutivo, $nivel, $cla_descrip_atributo, 
				$nom_dimension_atributo, $nom_fisico_atributo, $tipo_dato_atributo, $long_dato_atributo, $dimensionGroup, $cla_concepto);
				
				if ($this->id_inputtype == mpcNumeric)
				{
// Consideramos el indicador de tipo captura numerico
					$formula_bd = "SUM( {$nom_fisico_atributo} )";
					$nom_indicador = $this->nom_dimension. ' (Value)';
					$cla_indicador_atributo = $this->addModelIndicatorCalculated($this->Repository, $cla_concepto, $nom_indicador, $formula_bd, $formato, $consecutivo);
				}
			}
			
			$this->insertSI_DESCRIP_KEYS($metaconnection, $nom_tabla, $parent_join, $parent_join, $cla_concepto);
			
		}
		return $cla_descrip;
	}

	public function verifyQuestionType()
	{
		if ($this->cla_descrip > 0)
		{
			if ($this->id_qtype == qtpPhoto || $this->id_qtype == qtpSignature)
			{
				$sql = 
				"update SI_DESCRIP_ENC set NATURALKEY_SOURCE = 'IMAGE' where CLA_DESCRIP = {$this->cla_descrip}";
				if (!$this->Repository->ADOConnection->Execute($sql))
				{
					$ErrSQL = $this->Repository->ADOConnection->ErrorMsg();
					savelogfile($sql.' -> '.$ErrSQL);
				}
			}
		}
	}

	public function getParDescriptor($aConnection){
		$nom_tabla_padre = $this->arrJoins[count($this->arrJoins)-2]["table"];//Obtiene la tabla del padre inmediato
		$aQuery  = "
			SELECT L.cla_descrip, L.consecutivo 
			FROM SI_DESCRIP_ENC D 
				INNER JOIN SI_CPTO_LLAVE L ON 
					L.cla_descrip = D.cla_descrip AND
					L.CLA_CONCEPTO = {$this->cla_concepto}
			WHERE D.CLA_DESCRIP IN (SELECT CLA_DESCRIP FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = {$this->cla_concepto})
				AND D.nom_tabla = {$aConnection->quote($nom_tabla_padre)}
			ORDER BY L.consecutivo ASC LIMIT 1;
		";
		$rsDescrip = $aConnection->Execute($aQuery);
		if($rsDescrip && !$rsDescrip->EOF){
			$this->par_descriptor = (($this->consecutivo<10)?"0":"").$this->consecutivo;
			$this->par_descriptor.= "|".(($rsDescrip->fields[1]<10)?"0":"").$rsDescrip->fields[1];
			$this->des_cla_padre = $rsDescrip->fields[0];
		}
		if($rsDescrip){
			$rsDescrip->close();
		}
	}

	public static function delete($cla_concepto, $consecutivo, $cla_descrip, $nom_tabla, $aConnection){
		//Comprobar si no hay otra dimension que ocupe este registro en si_descrip_keys
		$aQuery  = "
			SELECT cla_descrip 
			FROM SI_DESCRIP_ENC 
			WHERE CLA_DESCRIP IN (SELECT CLA_DESCRIP FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = {$cla_concepto})
				AND CLA_DESCRIP <> {$cla_descrip} 
				AND nom_tabla = {$aConnection->quote($nom_tabla)}";
		$rsKeys = $aConnection->Execute($aQuery);
		if($rsKeys){
			if($rsKeys->EOF){
				//si_descrip_keys
				$aQuery  = "
					DELETE FROM si_descrip_keys
					WHERE
						cla_concepto = {$cla_concepto} AND
						nom_tabla = {$aConnection->quote($nom_tabla)}";
				$aConnection->Execute($aQuery);
			}
			$rsKeys->close();
		}

		//validar que la dimension global no es utilizada por otra dimension
		$aQuery  = "
				SELECT cla_concepto, consecutivo FROM SI_CPTO_LLAVE
				WHERE
					NOT (cla_concepto = {$cla_concepto} AND
					consecutivo = {$consecutivo}) AND
					cla_descrip = {$cla_descrip}";
		$rs = $aConnection->Execute($aQuery);
		if($rs && $rs->RecordCount() == 0){
			//Eliminar dicho campo como atributo de otras dimensiones globales
			$aQuery  = "
				DELETE A
				FROM SI_DESCRIP_ENC DE 
					INNER JOIN SI_ATRIBUTOS A ON 
						A.NOM_TABLA = DE.NOM_TABLA AND 
						A.NOM_FISICO = DE.NOM_FISICO
				WHERE DE.CLA_DESCRIP = {$cla_descrip}";
			$aConnection->Execute($aQuery);
			//SI_DESCRIP_ENC
			$aQuery  = "
				DELETE DE, DD, A
				FROM SI_DESCRIP_ENC DE
					LEFT JOIN si_descrip_det DD ON
						DD.cla_descrip = DE.cla_descrip
					LEFT JOIN si_atributos A ON
						A.cla_descrip = DE.cla_descrip
				WHERE
					DE.cla_descrip = {$cla_descrip}";
			$aConnection->Execute($aQuery);
		}

		if($rs){
			$rs->close();
		}
		
		//SI_CPTO_LLAVE
		$aQuery  = "
				DELETE FROM SI_CPTO_LLAVE
				WHERE
					cla_concepto = {$cla_concepto} AND
					consecutivo = {$consecutivo}";
		$aConnection->Execute($aQuery);
	}
}

?>