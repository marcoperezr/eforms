<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$theSchedulerID = 0;

if (array_key_exists("SchedulerID", $_GET))
{
	$theSchedulerID = $_GET["SchedulerID"];
}

	//Obtenemos todos los usuarios de la tabla de usuarios de ESurvey
	$arrUsers = array();

	$sql = "SELECT CLA_USUARIO, Email FROM SI_SV_Users";
	
	if($_SESSION["PAFBM_Mode"])
	{
		//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
		//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
		$sql.= " WHERE CLA_USUARIO > 0";	
		//@JAPR
	}

	$sql.=" ORDER BY Email";
	
	
	$aRS = $theRepository->ADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	while (!$aRS->EOF)
	{
		$intUserID = (int)$aRS->fields["cla_usuario"];
		$strUserName = $aRS->fields["email"];

		$arrUsers[$intUserID]["cla_usuario"] = $intUserID;
		$arrUsers[$intUserID]["name"] = $strUserName;
		$aRS->MoveNext();
	}
	
	//Obtenemos los usuarios seleccionados en el scheduler
	$arrSurveyUsers = array();

	$sql = "SELECT UserID FROM SI_SV_SurveySchedulerUser WHERE SchedulerID = ".$theSchedulerID;
	
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		die( translate("Error accessing")." SI_SV_SurveySchedulerUser ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	while (!$aRS->EOF)
	{
		$intUserID = (int)$aRS->fields["userid"];
		$strUserName = $arrUsers[$intUserID]["name"];

		$arrSurveyUsers[$intUserID]["cla_usuario"] = $intUserID;
		$arrSurveyUsers[$intUserID]["name"] = $strUserName;

		$aRS->MoveNext();
	}

	/*******************************************************************************************************************************************************/
	
	//Obtenemos todos los roles
	$arrRoles = array();
	$sql = "SELECT CLA_ROL, NOM_ROL FROM SI_ROL WHERE CLA_ROL <> -1 AND NOM_ROL NOT LIKE (".$theRepository->ADOConnection->Quote("dummy").")";

	$aRS = $theRepository->ADOConnection->Execute($sql);
	if ($aRS === false)
	{
		die( translate("Error accessing")." SI_ROL ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	while (!$aRS->EOF)
	{
		$intRolID = (int)$aRS->fields["cla_rol"];
		$strRolName = $aRS->fields["nom_rol"];

		$arrRoles[$intRolID]["cla_rol"] = $intRolID;
		$arrRoles[$intRolID]["name"] = $strRolName;
		$aRS->MoveNext();
	}
	
	$arrSurveyRoles = array();
	$sql = "SELECT RolID FROM SI_SV_SurveySchedulerRol WHERE SchedulerID = ".$theSchedulerID;

	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS === false)
	{
		die( translate("Error accessing")." SI_SV_SurveySchedulerRol ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	while (!$aRS->EOF)
	{
		$intRolID = (int)$aRS->fields["rolid"];
		$strRolName = $arrRoles[$intRolID]["name"];

		$arrSurveyRoles[$intRolID]["cla_rol"] = $intRolID;
		$arrSurveyRoles[$intRolID]["name"] = $strRolName;
		$aRS->MoveNext();
	}
	
	$strAllElements = "";
	
	foreach ($arrUsers as $anElement)
	{
		if($strAllElements!="")
		{
			$strAllElements.="_AWSepElement_";
		}
		
		$strAllElements.="User_".$anElement["cla_usuario"]."_AWSepName_".$anElement["name"];
	}
	
	foreach ($arrRoles as $anElement)
	{
		if($strAllElements!="")
		{
			$strAllElements.="_AWSepElement_";
		}
		
		$strAllElements.="Rol_".$anElement["cla_rol"]."_AWSepName_".$anElement["name"];
	}
	
	$strSelectedElements = "";

	foreach ($arrSurveyUsers as $anElement)
	{
		if($strSelectedElements!="")
		{
			$strSelectedElements.="_AWElem_";
		}
		
		$strSelectedElements.="User_".$anElement["cla_usuario"];
	}
	
	foreach ($arrSurveyRoles as $anElement)
	{
		if($strSelectedElements!="")
		{
			$strSelectedElements.="_AWElem_";
		}
		
		$strSelectedElements.="Rol_".$anElement["cla_rol"];
	}
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>eSurvey</title>
<script language='JavaScript' src='js/utils.js'></script>
<script language="javascript" src="js/EditorHTML/dialogs.js"></script>
<script language="JavaScript">
//var window_dialogArguments = window.dialogArguments;
var window_dialogArguments = getDialogArgument();
owner = window_dialogArguments[0];
owner.returnValue = null;

var strAllElements = '<?=$strAllElements?>';
var strSelectedElements = '<?=$strSelectedElements?>';

function displayUsers()
{
	document.getElementById('Columns').style.display = 'block';
	document.getElementById('Roles').style.display = 'none';
}

function displayRoles()
{
	document.getElementById('Roles').style.display = 'block';
	document.getElementById('Columns').style.display = 'none';
}
</script>
<script language='JavaScript' src='js/manageLists.js'></script>
<style type="text/css">@import url("css/fudge.css");</style>
</head>
<body onload="OnStart(strAllElements, strSelectedElements)" class="NBody">
<div id='divHS' class="HeaderStep">
	<img style="position:absolute; left:10px; top:5px; width:112px; height:49px;" src="images/lineasfondo.png">
	<table cellpadding="0" cellspacing="1" border="0" class="TBStep">
		<tr>
			<td width="20px"><img src="images/step_icon.gif"></td>
			<td width="270px" id="lbStep" class="Step"><?=translate("Assign Users")?></td>
		</tr>
	</table>
</div>
<div id="id_Step1" style="width:565; height:165px; overflow-y:auto; overflow-x:hidden;" class="NtabForm">
	<table width="100%" height="auto" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="3" style="height:10px; font-size:12px;">
				&nbsp;&nbsp;&nbsp;&nbsp;<?=translate("Assign Users")?>&nbsp;<input type="radio" id="typeUser" name="typeUser" value="0" onclick="displayUsers();" style="border:none" checked>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=translate("User Groups")?>&nbsp;<input type="radio" id="typeUser" name="typeUser" value="1" style="border:none" onclick="displayRoles();">
			</td>
		</tr>
		<tr>
			<td align="center" width="45%">
				<div id="Columns" style="width:95%; height:130px; overflow:auto; border:1px solid silver; background-color:white;display:block">
				</div>
				<div id="Roles" style="width:95%; height:130px; overflow:auto; border:1px solid silver; background-color:white;display:none">
				</div>
			</td>
			<td align="center" valign="middle" width="5%">
				<br>
				<img id="add_column" src="images/add_field.gif" style="cursor:pointer;" title="<?=translate("Add")?>" onclick="Add_to_Order();">
				<br>
				<br>
				<img id="del_column" src="images/del_field.gif" style="cursor:pointer;" title="<?=translate("Delete")?>" onclick="Del_to_Order();">
				<br>
			</td>
			<td align="center" width="50%">
				<table width="100%" cellpadding="0" cellspacing="0" border="0" height="100%">
					<tr>
						<td width="90%" align="center">
							<div id="SortColumns" style="width:95%; height:130px; overflow:auto; border:1px solid silver; background-color:white;">
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<br>
<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tbButtons" style="position:absolute; bottom:20px;">
	<tr>
		<td><h2 id="lbModule" align="left" style="display:none"></h2></td>
		<td align="right" style="font-family:verdana;font-size:9pt">
			<input id="botOK" class="Nbutton" type="button" name="OK" onclick="On_Ok();" value="<?=translate("OK")?>">
			<input id="botCancel" class="Nbutton" type="button" name="Cancel" onclick="cerrar()" value="<?=translate("Cancel")?>">
		</td>
	</tr>
</table>
</body>
</html>