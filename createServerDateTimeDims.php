<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

$strOriginalWD = getcwd();

//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("BITAM_UserID");
//session_register("BITAM_UserName");
//session_register("BITAM_RepositoryName");
$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

require_once("../model_manager/filescreator.inc.php");
require_once("../model_manager/modeldimension.inc.php");
require_once("../model_manager/modeldata.inc.php");
global $generateXLS;
$generateXLS = false;
foreach ($surveyCollection->Collection as $surveyInstance) 
{
	echo("<BR>***** "."Survey: ".$surveyInstance->SurveyName);
	//Crear dimensión Start Date
	if(is_null($surveyInstance->ServerStartDateDimID) || $surveyInstance->ServerStartDateDimID<=0)
	{
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		$dimensionName = "Server Start Date";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//Verificamos si hay dimension Start Date existente
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($aRepository, "ServerStartDateDimID", $dimensionName, true);
		
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		$aServerStartDateDimID = 0;
		if($cla_descrip!=-1)
		{
			echo("<BR>"."Dimensión Server Start Date ya existía, se reutilizará");
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aServerStartDateDimID = $cla_descrip;
			}
			else {
				echo("<BR>".translate("Error adding Server Start Date dimension").": ".$strMMError);
			}
		}
		else 
		{
			echo("<BR>"."Crear Dimensión Server Start Date");
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aServerStartDateDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				echo("<BR>".translate("Error creating Server Start Date dimension").": ".$strMMError);
			}
		}
		
		$sql = "UPDATE SI_SV_Survey SET ServerStartDateDimID = ".$aServerStartDateDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		echo("<BR>"."sql:"."<BR>".$sql);
		
		$surveyInstance->ServerStartDateDimID = $aServerStartDateDimID;
	}

	//Crea la columna en la tabla paralela
	$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD ServerStartDate VARCHAR(64)";
	@$aRepository->DataADOConnection->Execute($sql);
	echo("<BR>"."sql:"."<BR>".$sql);

	//Crear dimensión End Date
	if(is_null($surveyInstance->ServerEndDateDimID) || $surveyInstance->ServerEndDateDimID<=0)
	{
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		//Crear la dimension EndDate
		$dimensionName = "Server End Date";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//Verificamos si hay dimension Server End Date existente
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($aRepository, "ServerEndDateDimID", $dimensionName, true);
		
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		$aServerEndDateDimID = 0;
		if($cla_descrip!=-1)
		{
			echo("<BR>"."Dimensión Server End Date ya existía, se reutilizará");
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aServerEndDateDimID = $cla_descrip;
			}
			else {
				echo("<BR>".translate("Error adding Server End Date dimension").": ".$strMMError);
			}
		}
		else 
		{
			echo("<BR>"."Crear Dimensión End Date");
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aServerEndDateDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				echo("<BR>".translate("Error creating Server End Date dimension").": ".$strMMError);
			}
		}
		
		$sql = "UPDATE SI_SV_Survey SET ServerEndDateDimID = ".$aServerEndDateDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		echo("<BR>"."sql:"."<BR>".$sql);
		
		$surveyInstance->ServerEndDateDimID = $aServerEndDateDimID;
	}
	
	//Crea la columna en la tabla paralela
	$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD ServerEndDate VARCHAR(64)";
	@$aRepository->DataADOConnection->Execute($sql);
	echo("<BR>"."sql:"."<BR>".$sql);
	
	//Crear dimensión Start Time
	if(is_null($surveyInstance->ServerStartTimeDimID) || $surveyInstance->ServerStartTimeDimID<=0)
	{	
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		//Crear la dimension StartTime
		$dimensionName = "Server Start Time";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//Verificamos si hay dimension Start Time existente
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($aRepository, "ServerStartTimeDimID", $dimensionName, true);
		
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		$aServerStartTimeDimID = 0;
		if($cla_descrip!=-1)
		{
			echo("<BR>"."Dimensión Server Start Time ya existía, se reutilizará");
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aServerStartTimeDimID = $cla_descrip;
			}
			else {
				echo("<BR>".translate("Error adding Server Start Time dimension").": ".$strMMError);
			}
		}
		else 
		{
			echo("<BR>"."Crear Dimensión Server Start Time");
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aServerStartTimeDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				echo("<BR>".translate("Error creating Server Start Time dimension").": ".$strMMError);
			}
		}
		
		$sql = "UPDATE SI_SV_Survey SET ServerStartTimeDimID = ".$aServerStartTimeDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		echo("<BR>"."sql:"."<BR>".$sql);
		
		$surveyInstance->ServerStartTimeDimID = $aServerStartTimeDimID;
	}
		
	//Crea la columna en la tabla paralela
	$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD ServerStartTime VARCHAR(64)";
	@$aRepository->DataADOConnection->Execute($sql);
	echo("<BR>"."sql:"."<BR>".$sql);
		
	//Crear dimensión End Time
	if(is_null($surveyInstance->ServerEndTimeDimID) || $surveyInstance->ServerEndTimeDimID<=0)
	{		
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		$dimensionName = "Server End Time";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//Verificamos si hay dimension End Time existente
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($aRepository, "ServerEndTimeDimID", $dimensionName, true);
		
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		$aServerEndTimeDimID = 0;
		if($cla_descrip!=-1)
		{
			echo("<BR>"."Dimensión Server End Time ya existía, se reutilizará");
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aServerEndTimeDimID = $cla_descrip;
			}
			else {
				echo("<BR>".translate("Error adding Server End Time dimension").": ".$strMMError);
			}
		}
		else 
		{
			echo("<BR>"."Crear Dimensión Server End Time");
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
				$aServerEndTimeDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				echo("<BR>".translate("Error creating Server End Time dimension").": ".$strMMError);
			}
		}
		
		$sql = "UPDATE SI_SV_Survey SET ServerEndTimeDimID = ".$aServerEndTimeDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		echo("<BR>"."sql:"."<BR>".$sql);
		
		$surveyInstance->ServerEndTimeDimID = $aServerEndTimeDimID;
	}
	
	//Crea la columna en la tabla paralela
	$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD ServerEndTime VARCHAR(64)";
	@$aRepository->DataADOConnection->Execute($sql);
	echo("<BR>"."sql:"."<BR>".$sql);
}
?>