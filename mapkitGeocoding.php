<?php
	//phantomjs --web-security=no --ignore-ssl-errors=true --ssl-protocol=any mapkitGeocoding.js "-debug" "service=https://kpionline10.bitam.com/artus/gen9_test/ESurveyV602_test/" "timeout=15000" "latitude=22.249409" "longitude=-97.8683129"
	$latitude = ($_GET["latitude"] != "") ? $_GET["latitude"] : "";
	$longitude = ($_GET["longitude"] != "") ? $_GET["longitude"] : "";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<script src="https://cdn2.apple-mapkit.com/mk/5.x.x/mapkit.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
		<title>DEMO</title>
	</head>
	<body>
		<input type="hidden" class="mapkitResponse" id="mapkitResponse">
	</body>
	<script>
		$(document).ready(function() {
			console.log("Page is ready to work...");
			if (mapkit) {
				mapkit.init({ authorizationCallback: function(done) {	
					console.log("Trying to get token...");
					$.ajax({ type: 'POST', dataType: 'json', url: "https://kpionline.bitam.com/mapsService/mapKit/service.php", headers: { 'Product-Name' : 'KPIForms' },
						success: function(responseData, textStatus, jqXHR) {
							if(textStatus == "success") {
								if (responseData && responseData.token) {
									console.log("Token retrieved...");
									done(responseData.token);
								}
							} else {
								console.log("No token generated at the site");
							}
						},
						error: function(responseData, textStatus, error) {
							console.log("There was an error with the token request");
						}
					});
				}});

				mapkit.addEventListener("configuration-change", function(event) {
					switch(event.status) {
						case "Initialized":
							//console.log("MapKit initialized correctly...");
							var latitude = <?=$latitude?>;
							var longitude = <?=$longitude?>;

							if (latitude && longitude) {
								latitude = parseFloat(latitude, 10);
								longitude = parseFloat(longitude, 10);
							} else {
								return false;
							}
							var aGeocoder = new mapkit.Geocoder({
								language: "en-GB",
								getsUserLocation: false
							});
							aGeocoder.reverseLookup(new mapkit.Coordinate(latitude, longitude), function(error, data) {
								if (error != null) {
									//Add error if needed
								} else if(data && data.results && data.results.length) {
									console.log("MapKit geocoding found an address match...");
									//console.log(data.results[0].formattedAddress);
									console.log(JSON.stringify(data.results));
									$(".mapkitResponse").val(JSON.stringify(data.results));
								}
							});
						break;
						case "Refreshed":
							console.log("Mapkit configuration or token changed correctly.");
						break;
					}
				});
			}			
		});
	</script>
</html>