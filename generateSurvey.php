<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//@JAPR 2014-06-25: Agregada la posibilidad de hacer login directamente a la captura Web sin pasar por el login de eForms
global $captureVia;
//@JAPR
$GBLGenSurveyCatFilter = "";

$editMode = false;
$entryID = 0;
//@JAPR 2013-06-27: Agregada la integración con IExplorer desde edición y captura vía EMail
$strWebEntryURL = '';
//@JAPR

$surveyID = 0;
if(array_key_exists("surveyID", $_GET))
{
	$surveyID = $_GET["surveyID"];
}
$surveyID = (int) $surveyID;

//Verificar si existe el parametro CaptureVia viene en el GET
$captureVia = 0;
if(array_key_exists("CaptureVia", $_GET))
{
	$captureVia = (int)$_GET["CaptureVia"];
}

$captureEmail = "";
if(array_key_exists("CaptureEmail", $_GET))
{
	$captureEmail = $_GET["CaptureEmail"];
}

$schedulerID = 0;
$schedulerInstance = null;
if(array_key_exists("SchedulerID", $_GET))
{
	$schedulerID = (int)$_GET["SchedulerID"];
}

require_once("survey.inc.php");
require_once("section.inc.php");
require_once("question.inc.php");
require_once("surveyscheduler.inc.php");

//Se genera instancia de encuesta dado un SurveyID
$surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $surveyID);

//Verificar si el usuario logueado puede aplicar dicha encuesta
$artusUserID = $_SESSION["PABITAM_UserID"];		//@JAPRWarning: Este usuario debería ser el de la captura y no el logeado en este momento al Admin si es una edición

//@JAPR 2013-05-15: Corregido un bug, para verificar si puede o no aplicar la encuesta, se debe usar al usuario de la captura si es que aun
//existe, de lo contrario entonces si se debe usar al usuario logeado
require_once('user.inc.php');
global $intApplicatorID;
//@JAPR 2017-01-10: Agregada la posibilidad de editar capturas grabadas desde el Admin en la captura en línea, utilizando un draft pre-cargado como parámetro para simular
//que se estuviera cargando un draft de archivo pero en browser (#J4HPY4)
//Se modifica la lectura de parámetros para permitir POST o GET, verificar el resto de los comportamientos ya que en versiones previas a eForms V6 si era importante el cómo llegaran los
//parámetros, si se va a modificar algo debe ser gradual conscientemente de las implicaciones según el nuevo esquema de invocación de este archivo
$userID = getParamValue("ApplicatorID", 'both', '(int)');
$intApplicatorID = $userID;
//@JAPR
$applicatorUser = null;
if (!is_null($intApplicatorID) && $intApplicatorID > 0) {
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
	$applicatorUser = BITAMeFormsUser::NewInstanceWithID($theRepository, $intApplicatorID);
	if (is_null($applicatorUser)) {
		//Si ya no se pudo cargar la instancia del usuario, intentará hacer la edición como el usuario logeado
		$intApplicatorID = 0;
	}
}

//Si no se recibió un usuario válido, se reutilizará al usuario logeado
if ($intApplicatorID <= 0) {
	$intApplicatorID = $artusUserID;
}

//Si captureVia es cero entonces se verifica de la manera tradicional
if($captureVia==0)
{
	//@JAPR 2013-12-02: Agregado el enlace para captura Web desde la colección de encuestas
	//En caso de que no esté asignado el Survey, no realiza estas consultas pues se asume que se está cargando simplemente el App de captura Web
	$blnApplySurvey = false;
	if ($surveyID > 0) {
		//@JAPR 2013-05-15: Corregido un bug, para verificar si puede o no aplicar la encuesta, se debe usar al usuario de la captura si es que aun
		//existe, de lo contrario entonces si se debe usar al usuario logeado
		$blnApplySurvey = BITAMSurvey::canApplySurvey($theRepository, $surveyID, $intApplicatorID);
	}
	/*
	else {
		//En este caso como no se especificó una encuesta y no es captura vía EMail, se asume que simplemente se quiere entrar a la captura Web,
		//así que lo permite
		$blnApplySurvey = true;
	}
	*/
}
else 
{
	//En caso contrario se verifica si el correo contenido en CaptureEmail se encuentra dentro de la tabla SI_SV_SchedulerEmail
	if($captureVia==1 && $schedulerID>0 && trim($captureEmail)!="")
	{
		$blnApplySurvey = BITAMSurveyScheduler::existEmailInThisScheduler($theRepository, $schedulerID, $captureEmail);
		$schedulerInstance = BITAMSurveyScheduler::NewInstanceWithID($theRepository, $schedulerID);
	}
	else 
	{
		$blnApplySurvey = true;
	}
}

//@JAPR 2013-12-02: Agregado el enlace para captura Web desde la colección de encuestas
//$aCollectionSchedulers = null;
//$arrayAppliedSurveys = array();
//@JAPRDescontinuada en V6: Este proceso completo ya no se usa pues no existe edición de capturas vía EMail en esta versión por ahora
$isEnabledCaptureThisDay = false;
/*
if($captureVia==0)
{
	//@JAPR 2013-12-02: Agregado el enlace para captura Web desde la colección de encuestas
	//En caso de que no esté asignado el Survey, no realiza estas consultas pues se asume que se está cargando simplemente el App de captura Web
	if ($surveyID > 0) {
		//@JAPR 2013-05-15: Corregido un bug, para verificar si puede o no aplicar la encuesta, se debe usar al usuario de la captura si es que aun
		//existe, de lo contrario entonces si se debe usar al usuario logeado
		//Verificamos si la encuesta ya se ha aplicado en ocasiones anteriores
		$arrayAppliedSurveys = BITAMSurvey::getAppliedSurveys($theRepository, $surveyID, $intApplicatorID);
		
		//Obtenemos coleccion de schedulers para revisar si Frecuencia es No/Si y para verificar si es que 
		//tiene frecuencia si el dia de hoy se le permite capturar una encuesta
		$aCollectionSchedulers = BITAMSurveySchedulerCollection::NewInstanceBySurveyAndUser($theRepository, $surveyID, $intApplicatorID);
	}
	//@JAPR
}
else 
{
	//Verificamos si la encuesta ya se ha aplicado en ocasiones anteriores
	$arrayAppliedSurveys = BITAMSurvey::getAppliedSurveysByEmail($theRepository, $schedulerID, $captureEmail);
	
	//Obtenemos coleccion de schedulers para revisar si Frecuencia es No/Si y para verificar si es que 
	//tiene frecuencia si el dia de hoy se le permite capturar una encuesta
	$aCollectionSchedulers = BITAMSurveySchedulerCollection::NewInstance($theRepository, array($schedulerID));
}

$countAppliedSurveys = count($arrayAppliedSurveys);

//Si por alguna circunstancia no hay datos en la coleccion establecemos que no tiene frequencia, 
//que tiene una encuesta capturada al menos y que el dia de hoy no puede capturar, 
//ya que es ilogico q pueda capturar encuestas y no este asignado a un scheduler
if(is_null($aCollectionSchedulers) || is_null($aCollectionSchedulers->Collection) || count($aCollectionSchedulers->Collection)==0)
{
	$hasFrequency = false;
	$countAppliedSurveys = 1;
	$isEnabledCaptureThisDay = false;
}
else 
{
	//En caso contrario se verifica la frecuencia y si el dia de hoy es capturable dicha encuesta
	$hasFrequency = $aCollectionSchedulers->hasFrequency();
	$isEnabledCaptureThisDay = $aCollectionSchedulers->isEnabledCaptureThisDay();
}
*/

//@JAPR 2013-06-27: Agregada la integración con IExplorer desde edición y captura vía EMail
$strWebEntryURL = (string) @$_POST["WebEntryURL"];
//@JAPR

if($blnApplySurvey)
{
	//Cuando se edita una encuesta
	if(array_key_exists("EntryID", $_POST) && array_key_exists("DateID", $_POST) && array_key_exists("HourID", $_POST) && array_key_exists("ApplicatorID", $_POST)) 
	{
		$editMode = true;
		$entryID = (int)@$_POST["EntryID"];
		$userID = @$_POST["ApplicatorID"];
		$dateID = @$_POST["DateID"];
		$hourID = @$_POST["HourID"];
		$endDateID = @$_POST["DateID"];
		$endHourID = @$_POST["EndHourID"];
		$dblLatitude = @$_POST["Latitude"];
		$dblLongitude = @$_POST["Longitude"];
		//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
		$dblAccuracy = @$_POST["Accuracy"];
	}
	else
	{
		//Cuando se aplica nueva encuesta
		if($captureVia==0 || ($captureVia==1 && $isEnabledCaptureThisDay==true))
		{
		}
		else 
		{
			if($captureVia==0)
			{
				//$url = "surveyList.php";
				$url = "index.html";
			}
			else 
			{
				$strError = "You can not access the survey '".(string) @$surveyInstance->SurveyName."' because this survey has expired";
				$url = "genericErrorPage.php?StrError=".urlencode($strError);
			}
			exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=".$url."\">\n</head>\n<body>\n</body>\n</html>\n");
		}
	}
}

//@JAPR 2012-11-05: Ahora la edición se hará utilizando el código de v4.01000
/* Una vez identificado que según los parámetros es una petición válida de captura (EntryID > 0) o una petición nueva y que el usuario
indicado tiene acceso a esta encuesta, se procede a cargar las respuestas actualmente grabadas (en caso de edición) y posteriormente a generar
la petición al index.html para que entre en modo de Capturar única de encuesta.
*/

//Habilitar si se quiere debugear los valores que se regresarán para generar el JSON de edición en v4, ya que para generar el JSON se usa una
//instancia de reporte extendida
/*
$intReportID = 0;
$sql = "SELECT MIN(FactKey) AS FactKey FROM ".$surveyInstance->SurveyTable." WHERE FactKeyDimVal = ".$entryID;
$aRS = $theRepository->DataADOConnection->Execute($sql);
if ($aRS && !$aRS->EOF)
{
	$intReportID = $aRS->fields["factkey"];
}

require_once("reportExt.inc.php");
$aReportData = BITAMReportExt::NewInstanceWithID($theRepository, $intReportID, $surveyID);
////require_once("report.inc.php");
////$aReportData = BITAMReport::NewInstanceWithID($theRepository, $intReportID, $surveyID);
PrintMultiArray($aReportData);
die();
*/

global $strAppUserName;
$strAppUserName = '';
global $strAppUserPwd;
$strAppUserPwd = '';
global $strAppSurveyID;
$strAppSurveyID = $surveyID;
global $strAppEntryID;
$strAppEntryID = $entryID;
global $intApplicatorID;
$intApplicatorID = (int) @$userID;
global $strApplicationDate;
$strApplicationDate = (string) @$dateID;
global $strApplicationTime;
$strApplicationTime = (string) @$hourID;
global $strApplicationEndDate;
$strApplicationEndDate = (string) @$endDateID;
global $strApplicationEndTime;
$strApplicationEndTime = (string) @$endHourID;
global $strApplicationLatitude;
$strApplicationLatitude = (string) @$dblLatitude;
global $strApplicationLongitude;
$strApplicationLongitude = (string) @$dblLongitude;
//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
global $strApplicationAccuracy;
$strApplicationAccuracy = (string) @$dblAccuracy;
//@JAPR 2013-06-27: Agregada la integración con IExplorer desde edición y captura vía EMail
global $strApplicationWebEntryURL;
$strApplicationWebEntryURL = (string) @$strWebEntryURL;
//@JAPR 2015-11-24: Habilitada la captura vía Web invocada desde el Administrador (#NGA28W)
global $strApplicationDefaultSurveyID;
$strApplicationDefaultSurveyID = getParamValue("DefaultSurveyID", 'both', '(int)');
//@JAPR

//Obtiene la instancia del usuario que hará la petición para permitir capturar la encuesta
//require_once('user.inc.php');
if($captureVia==0) {
	//@JAPR 2012-11-28: Modificado para que ahora el login se haga con el mismo usuario original de la captura, sólo en caso de no poder instanciarlo
	//se haría con la cuenta actualmente logeada
	$applicatorUser = null;
	if (!is_null($intApplicatorID) && $intApplicatorID > 0) {
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$applicatorUser = BITAMeFormsUser::NewInstanceWithID($theRepository, $intApplicatorID);
	}
	if (is_null($applicatorUser)) {
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$applicatorUser = BITAMeFormsUser::NewInstanceWithID($theRepository, $artusUserID);
	}
	
	if (!is_null($applicatorUser)) {
		$strAppUserName = $applicatorUser->EMail;
		$strAppUserPwd = BITAMDecryptPassword($applicatorUser->Password);
	}
}
else {
	//No estaría soportado por ahora logearse con un EMail que no esté registrado como usuario de eForms
	//@JAPRWarning:
	//29Abril2013: Por el momento se maneja de la misma manera que $captureVia==0
	$applicatorUser = null;
	if (!is_null($intApplicatorID) && $intApplicatorID > 0) {
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$applicatorUser = BITAMeFormsUser::NewInstanceWithID($theRepository, $intApplicatorID);
	}
	if (is_null($applicatorUser)) {
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$applicatorUser = BITAMeFormsUser::NewInstanceWithID($theRepository, $artusUserID);
	}
	if (!is_null($applicatorUser)) {
		$strAppUserName = $applicatorUser->EMail;
		$strAppUserPwd = BITAMDecryptPassword($applicatorUser->Password);
		$intApplicatorID = $applicatorUser->UserID;
	}
}

//@JAPR 2017-01-10: Agregada la posibilidad de editar capturas grabadas desde el Admin en la captura en línea, utilizando un draft pre-cargado como parámetro para simular
//que se estuviera cargando un draft de archivo pero en browser (#J4HPY4)
//Si se recibió una fecha de captura, usuario y ID forma además del parámetro "edit", se asumirá que se intenta iniciar la edición de la captura especificada, para lo cual cargará el
//contenido del archivo .dat recibido y realizará los ajustes para convertir todas las posibles referencias de archivos de captura móvil (si es que era de ese tipo) a referencias desde
//captura browser, ya que la edición sucede precisamente desde un browser y no podrá reconocer las referencias a archivos de dispositivo
$strSurveyDate = getParamValue('SurveyDate', 'both', '(string)');
if (getParamValue('edit', 'both', '(int)', true)) {
	if ($intApplicatorID > 0 && $strSurveyDate && $surveyID > 0 && $strAppUserName) {
		$strOutboxFileName = '';
		
		//Esta variable tiene un nombre ligeramente diferente a $blnWebMode precisamente para no confundirlas, ya que esta se lee directamente del query para la captura que se quiere
		//editar, mientras que $blnWebMode se lee automáticamente de un parámetro del request para determinar si se invocó vía captura web o dispositivo (aunque este proceso nunca se
		//invocaría desde las Apps de captura, pero de esta manera evitamos confusiones)
		$blnIsWebMode = 0;
		//Obtiene el nombre del archivo de datos que se utilizó para generar la captura
		$sql = "SELECT A.WebMode, A.FileName 
			FROM SI_SV_SurveyLog A 
			WHERE A.Status = 1 AND A.Deleted = 0 AND A.SurveyID = {$surveyID} 
				AND A.UserID = ".$theRepository->DataADOConnection->Quote($strAppUserName)." 
				AND A.SurveyDate = ".$theRepository->DataADOConnection->DBTimeStamp($strSurveyDate);
		$aRS = $theRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$blnIsWebMode = (int) @$aRS->fields["webmode"];
			$strOutboxFileName = (string) @$aRS->fields["filename"];
		}
		
		//Si se encontró la referencia del archivo que realizó la carga, obtiene su contenido y lo procesa para adaptarlo a captura vía browser para así finalmente asignarlo
		//como parámetro par que el App de captura browser lo utilice en la generación de la edición del outbox
		if ($strOutboxFileName) {
			try {
				$path = getcwd()."\\"."syncdata\\".trim($_SESSION["PABITAM_RepositoryName"]);
				$strCompletePath = $path."\\".$strOutboxFileName;
				$strJSON = @file_get_contents($strCompletePath);
				//Si es una captura de dispositivo, se deben ajustar las rutas/nombres de archivos locales B64 para convertirlas en referencias de archivos ya procesados en el server
				if (!$blnIsWebMode) {
					//Primero verifica si existen grabadas referencias de imagenes que deban ser ajustadas, y si la pregunta utilizada aún es parte de la definición de la forma
					$arrPhotoQuestionsColl = array();
					$arrDocumentQuestionsColl = array();
					$arrAudioQuestionsColl = array();		//Sólo agregado para poder diferenciar fácilmente el tipo de pregunta, se usará $arrDocumentQuestionsColl para procesar
					$arrVideoQuestionsColl = array();		//Sólo agregado para poder diferenciar fácilmente el tipo de pregunta, se usará $arrDocumentQuestionsColl para procesar
					$objQuestionsColl = BITAMQuestionCollection::NewInstanceIncludeTheseTypes($theRepository, $surveyID, array(qtpPhoto, qtpDocument, qtpAudio, qtpVideo));
					if (!is_null($objQuestionsColl)) {
						foreach ($objQuestionsColl as $objQuestion) {
							switch ($objQuestion->QTypeID) {
								case qtpPhoto:
									$arrPhotoQuestionsColl[$objQuestion->QuestionID] = $objQuestion;
									break;
								case qtpDocument:
									$arrDocumentQuestionsColl[$objQuestion->QuestionID] = $objQuestion;
									break;
								case qtpAudio:
									$arrAudioQuestionsColl[$objQuestion->QuestionID] = $objQuestion;
									$arrDocumentQuestionsColl[$objQuestion->QuestionID] = $objQuestion;
									break;
								case qtpVideo:
									$arrVideoQuestionsColl[$objQuestion->QuestionID] = $objQuestion;
									$arrDocumentQuestionsColl[$objQuestion->QuestionID] = $objQuestion;
									break;
							}
						}
					}
					
					//Si hay por lo menos alguna pregunta que pudiera tener alguna referencia a archivo, entonces continua con la validación
					if (count($arrPhotoQuestionsColl) + count($arrDocumentQuestionsColl) + count($arrAudioQuestionsColl) + count($arrVideoQuestionsColl)) {
						$factKeyDimVal = 0;
						$sql = "SELECT SurveyKey FROM {$surveyInstance->SurveyTable} 
							WHERE DateID={$theRepository->DataADOConnection->DBTimeStamp($strSurveyDate)} AND UserID = {$intApplicatorID} ";
						$aRS = $theRepository->DataADOConnection->Execute($sql);
						if ($aRS && !$aRS->EOF) {
							$factKeyDimVal = (int) @$aRS->fields["surveykey"];
						}
						
						//Si se encontró la referencia del Key de la captura entonces continua con la validación para buscar registros de archivos en esta captura específica
						if ($factKeyDimVal > 0) {
							$blnRequireFilesAdjust = false;
							$jsonData = json_decode($strJSON, true);
							if ($jsonData && (is_array($jsonData) || is_object($jsonData)) && isset($jsonData["answers"])) {
								$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
								//Buscará en las tablas de archivos todas las referencias de preguntas de los tipos que deben ser ajustados
								//Primero verifica la tabla de imágenes para las respuestas de preguntas tipo Foto
								$sql = "SELECT QuestionID, FactKey, MainFactKey, PathImage 
									FROM SI_SV_SurveyAnswerImage 
									WHERE SurveyID = {$surveyID} AND FactKey = {$factKeyDimVal}";
								$aRS = $theRepository->DataADOConnection->Execute($sql);
								if ($aRS && !$aRS->EOF) {
									while (!$aRS->EOF) {
										$intQuestionID = (int) @$aRS->fields["questionid"];
										$intSectionKey = (int) @$aRS->fields["mainfactkey"];
										$strPathImage = (string) @$aRS->fields["pathimage"];
										if ($strPathImage && $intQuestionID > 0 && $intSectionKey > 0 && isset($arrPhotoQuestionsColl[$intQuestionID])) {
											//Debe cambiar la referencia de esta respuesta de la pregunta
											$objQuestion = $arrPhotoQuestionsColl[$intQuestionID];
											$blnIsMultiRecord = $objQuestion->SectionType != sectNormal;
											if (isset($jsonData["answers"][$intQuestionID])) {
												//En este punto dependiendo de si es o no una sección multi-registro, vendría un objeto directo o un array de registros, así que se
												//valida según la definición actual, que es lo único que se podría utilizar para reconstruir la captura
												if ($blnIsMultiRecord) {
													//Se busca a partir del número de registro donde se grabó
													if (isset($jsonData["answers"][$intQuestionID][$intSectionKey -1]["photo"])) {
														$jsonData["answers"][$intQuestionID][$intSectionKey -1]["photo"] = "surveyimages/".$theRepositoryName."/".$strPathImage;
														$blnRequireFilesAdjust = true;
													}
												}
												else {
													//Es un objeto directo
													if (isset($jsonData["answers"][$intQuestionID]["photo"])) {
														$jsonData["answers"][$intQuestionID]["photo"] = "surveyimages/".$theRepositoryName."/".$strPathImage;
														$blnRequireFilesAdjust = true;
													}
												}
											}
										}
										
										$aRS->MoveNext();
									}
								}
								
								//A continuación verifica la tabla de documentos
								$sql = "SELECT QuestionID, FactKey, MainFactKey, PathDocument 
									FROM SI_SV_SurveyAnswerDocument 
									WHERE SurveyID = {$surveyID} AND FactKey = {$factKeyDimVal}";
								$aRS = $theRepository->DataADOConnection->Execute($sql);
								if ($aRS && !$aRS->EOF) {
									while (!$aRS->EOF) {
										$intQuestionID = (int) @$aRS->fields["questionid"];
										$intSectionKey = (int) @$aRS->fields["mainfactkey"];
										$strPathDocument = (string) @$aRS->fields["pathdocument"];
										if ($strPathDocument && $intQuestionID > 0 && $intSectionKey > 0 && isset($arrPhotoQuestionsColl[$intQuestionID])) {
											//Debe cambiar la referencia de esta respuesta de la pregunta
											$objQuestion = $arrPhotoQuestionsColl[$intQuestionID];
											$blnIsMultiRecord = $objQuestion->SectionType != sectNormal;
											if (isset($jsonData["answers"][$intQuestionID])) {
												//En este punto dependiendo de si es o no una sección multi-registro, vendría un objeto directo o un array de registros, así que se
												//valida según la definición actual, que es lo único que se podría utilizar para reconstruir la captura
												if ($blnIsMultiRecord) {
													//Se busca a partir del número de registro donde se grabó
													if (isset($jsonData["answers"][$intQuestionID][$intSectionKey -1]["document"])) {
														$jsonData["answers"][$intQuestionID][$intSectionKey -1]["document"] = "surveydocuments/".$theRepositoryName."/".$strPathDocument;
														$blnRequireFilesAdjust = true;
													}
												}
												else {
													//Es un objeto directo
													if (isset($jsonData["answers"][$intQuestionID]["document"])) {
														$jsonData["answers"][$intQuestionID]["document"] = "surveydocuments/".$theRepositoryName."/".$strPathDocument;
														$blnRequireFilesAdjust = true;
													}
												}
											}
										}
										
										$aRS->MoveNext();
									}
								}
							}
							
							//Si requirió ajuste en el objeto
							if ($blnRequireFilesAdjust) {
								$strJSON = json_encode($jsonData);
							}
						}
					}
				}
				
				//Asigna el parámetro que se usará para cargar el outbox en la captura web desde indexHTML.inc.php
				if ($strJSON) {
					global $strApplicationDataDraftContent;
					//Dado a que el valor se asignará a un input de HTML, se deben escapar las comillas dobles o de lo contrario quedará dañado el JSON asignado
					$strApplicationDataDraftContent = str_replace('"', "&quot;", $strJSON);
					
					//Adicionalmente, si se va a editar el outbox con este método, se debe enviar la configuración de loginDefaultSurveyID ya que loginDataSurveyID era para el método
					//de edición de eForms en versiones previas, el cual realizaba un request al server para generar el array de respuestas con el cual se sobreescribía el outbox,
					//pero fue descontinuado con el rediseño de v6, así que ahora se utilizará este otro esquema mas sencillo
					$strApplicationDefaultSurveyID = $surveyID;
				}
			} catch (Exception $e) {
				//No se reportará el error, simplemente continuará con la carga de la captura vacía
			}
		}
	}
}
//@JAPR

//Invoca a este PHP que simplemente generará la página de login tal como lo haría index.html, pero asignando las variables de login y otros
//parámetros para que haga auto-login con dicha información y se genere como una captura única
require_once("indexHTML.inc.php");

//Termina el proceso de edición de v4.01000 o posterior
die();
?>