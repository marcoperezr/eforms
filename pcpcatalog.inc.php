<?php

require_once("catalog.inc.php");

class BITAMPCPCatalog extends BITAMCatalog
{
	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		$this->VersionNum = -1;
		return $this;
	}

	function getCatalogVersion()
	{
		return parent::getCatalogVersion();
	}

	function updateFromRS($aRS)
	{
		parent::updateFromRS($aRS);
		return $this;
	}

	function getAttributeValuesBasic($arrayAttribIDs = null, $aUserID = -1, $filter="") 
	{
		return parent::getAttributeValues($arrayAttribIDs, $aUserID, $filter);
	}

	function getAttributeValues($arrayAttribIDs = null, $aUserID = -1, $filter="") 
	{
		if ($this->CatalogName == 'Job Catalog')
		{
			global $theAppUser;
			if (!is_null($theAppUser)) 
			{
				global $BITAMPCPHostName;
				$pcpResponse = BITAMPCPCatalog::PCPGetJobs($BITAMPCPHostName, $theAppUser->PCPUserID, $theAppUser->PCPPassword);
				if (isset($pcpResponse['pcpCode']))
				{
					die(translate("Error accessing")." ".$BITAMPCPHostName."/GetJobs".": [".$pcpResponse['pcpCode']."] ".$pcpResponse['pcpError']);
				}
				else
				{
					if (isset($pcpResponse['papers']))
					{
						global $BITAMPCPJobCatalogTableInfo;
						$sql = sprintf("DELETE FROM ridim_10093temp WHERE `DSC_10110` = %s", $this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID));
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die( translate("Error accessing")." ridim_10093temp ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						$userJobs = $pcpResponse['papers'];
						foreach ($userJobs as $aJob)
						{
							if (isset($aJob['addrList']))
							{
								foreach ($aJob['addrList'] as $aJobAddress)
								{
									$sql = sprintf("INSERT INTO ridim_10093temp (`DSC_10094`, `DSC_10095`, `DSC_10096`, `DSC_10097`, `DSC_10098`, `DSC_10099`, `DSC_10100`, `DSC_10101`, `DSC_10102`, `DSC_10103`, `DSC_10104`, `DSC_10105`, `DSC_10106`, `DSC_10107`, `DSC_10108`, `DSC_10109`, `DSC_10110`) VALUES ( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", 
										$this->Repository->DataADOConnection->Quote($aJob['paperId'].': '.$aJob['servee']),
										$this->Repository->DataADOConnection->Quote($aJobAddress['address'].($aJobAddress['address2'] == '' ? '' : ' ').$aJobAddress['address2'].', '.$aJobAddress['city'].', '.$aJobAddress['state'].', '.$aJobAddress['zip']), 
										$this->Repository->DataADOConnection->Quote($aJob['paperId']), 
										$this->Repository->DataADOConnection->Quote($aJob['servee']), 
										$this->Repository->DataADOConnection->Quote($aJob['priority']), 
										$this->Repository->DataADOConnection->Quote($aJob['specialInstr']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['address']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['address2']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['city']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['state']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['zip']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['addressType']),
										$this->Repository->DataADOConnection->Quote($aJobAddress['county']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['phoneNumber']), 
										$this->Repository->DataADOConnection->Quote(sprintf("%.8F", $aJobAddress['latitude'])), 
										$this->Repository->DataADOConnection->Quote(sprintf("%.8F", $aJobAddress['longitude'])), 
										$this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID));

									if ($this->Repository->DataADOConnection->Execute($sql) === false)
									{
										die( translate("Error accessing")." ridim_10093temp ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
									}
								}
							}
						}
						$sql = sprintf("INSERT INTO ridim_10093 (`DSC_10093`, `IND_10022`, `DSC_10094`, `DSC_10095`, `DSC_10096`, `DSC_10097`, `DSC_10098`, `DSC_10099`, `DSC_10100`, `DSC_10101`, `DSC_10102`, `DSC_10103`, `DSC_10104`, `DSC_10105`, `DSC_10106`, `DSC_10107`, `DSC_10108`, `DSC_10109`, `DSC_10110`, `DSC_10111`) SELECT @rownum:=@rownum+1, 1, t.`DSC_10094`, t.`DSC_10095`, t.`DSC_10096`, t.`DSC_10097`, t.`DSC_10098`, t.`DSC_10099`, t.`DSC_10100`, t.`DSC_10101`, t.`DSC_10102`, t.`DSC_10103`, t.`DSC_10104`, t.`DSC_10105`, t.`DSC_10106`, t.`DSC_10107`, t.`DSC_10108`, t.`DSC_10109`, t.`DSC_10110`, '0' FROM ridim_10093temp t, (SELECT @rownum:=(SELECT IFNULL(MAX(ai.`RIDIM_10093KEY`), 0) FROM ridim_10093 ai)) rn WHERE t.`DSC_10110` = %s AND NOT EXISTS (SELECT * FROM ridim_10093 r WHERE r.`DSC_10094` = t.`DSC_10094` AND r.`DSC_10095` = t.`DSC_10095` AND r.`DSC_10110` = t.`DSC_10110`)", $this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID));
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die( translate("Error accessing")." ridim_10093 ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						$sql = sprintf("DELETE FROM ridim_10093 WHERE ridim_10093.`RIDIM_10093KEY` > 1 AND ridim_10093.`DSC_10110` = %s AND ridim_10093.`DSC_10111` = '0' AND NOT EXISTS (SELECT * FROM ridim_10093temp t WHERE t.`DSC_10094` = ridim_10093.`DSC_10094` AND t.`DSC_10095` = ridim_10093.`DSC_10095` AND t.`DSC_10110` = ridim_10093.`DSC_10110`)", $this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID));
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die(translate("Error accessing")." ridim_10093 ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}
			}
			return parent::getAttributeValues($arrayAttribIDs, $aUserID, ($filter == '' ? sprintf(' AND (DSC_10110 = %s AND DSC_10111 = "0")', $this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID)) : sprintf('%s AND (DSC_10110 = %s AND DSC_10111 = "0")', $filter, $this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID))));
		}
		if ($this->CatalogName == 'Codes')
		{
			global $theAppUser;
			if (!is_null($theAppUser)) 
			{
				global $BITAMPCPHostName;
				$pcpResponse = BITAMPCPCatalog::PCPGetCodes($BITAMPCPHostName);
				if (isset($pcpResponse['pcpCode']))
				{
					die(translate("Error accessing")." ".$BITAMPCPHostName."/GetCodes".": [".$pcpResponse['pcpCode']."] ".$pcpResponse['pcpError']);
				}
				else
				{
					if (isset($pcpResponse['codes']) && isset($pcpResponse['categories']))
					{
						global $BITAMPCPJobCatalogTableInfo;
						$sql = "DELETE FROM ridim_10015temp";
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die( translate("Error accessing")." ridim_10015temp ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						$categoryArray = $pcpResponse['categories'];
						$categories = array();
						foreach ($categoryArray as $aCategory)
						{
							$categories[$aCategory['categoryId']] = $aCategory['category'];
						}
						$codes = $pcpResponse['codes'];
						foreach ($codes as $aCode)
						{
							$sql = sprintf("INSERT INTO ridim_10015temp (`DSC_10016`, `DSC_10017`, `DSC_10018`, `DSC_10019`, `DSC_10020`, `DSC_10021`) VALUES (%s, %s, %s, %s, %s, %s)", 
										$this->Repository->DataADOConnection->Quote($aCode['category']), 
										$this->Repository->DataADOConnection->Quote($categories[$aCode['category']]),
										$this->Repository->DataADOConnection->Quote($aCode['code']), 
										$this->Repository->DataADOConnection->Quote($aCode['description']), 
										$this->Repository->DataADOConnection->Quote($aCode['requiresAddlInfo'] ? '1' : '0'), 
										$this->Repository->DataADOConnection->Quote($aCode['requiresPicture'] ? '1' : '0'));

							if ($this->Repository->DataADOConnection->Execute($sql) === false)
							{
								die( translate("Error accessing")." ridim_10015temp ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
						}
						$sql = "INSERT INTO ridim_10015 (`DSC_10015`, `IND_10003`, `DSC_10016`, `DSC_10017`, `DSC_10018`, `DSC_10019`, `DSC_10020`, `DSC_10021`) SELECT  @rownum:=@rownum+1, 1, t.`DSC_10016`, t.`DSC_10017`, t.`DSC_10018`, t.`DSC_10019`, t.`DSC_10020`, t.`DSC_10021` FROM ridim_10015temp t, (SELECT @rownum:=(SELECT IFNULL(MAX(ai.`RIDIM_10015KEY`), 0) FROM ridim_10015 ai)) rn WHERE NOT EXISTS (SELECT * FROM ridim_10015 r WHERE r.`DSC_10019` = t.`DSC_10019`)";

						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die( translate("Error accessing")." ridim_10015 ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						$sql = "DELETE FROM ridim_10015 WHERE ridim_10015.`RIDIM_10015KEY` > 1 AND NOT EXISTS (SELECT * FROM ridim_10015temp t WHERE t.`DSC_10019` = ridim_10015.`DSC_10019`)";
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die( translate("Error accessing")." ridim_10015 ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}
			}
			return parent::getAttributeValues($arrayAttribIDs, $aUserID, $filter);
		}
/*
		if ($this->CatalogName == 'PCP')
		{
			global $theAppUser;
			if (!is_null($theAppUser)) 
			{
				global $BITAMPCPHostName;
				$pcpResponse = BITAMPCPCatalog::PCPGetJobs($BITAMPCPHostName, $theAppUser->PCPUserID, $theAppUser->PCPPassword);
				if (isset($pcpResponse['pcpCode']))
				{
					die(translate("Error accessing")." ".$BITAMPCPHostName."/GetJobs".": [".$pcpResponse['pcpCode']."] ".$pcpResponse['pcpError']);
				}
				else
				{
					if (isset($pcpResponse['papers']))
					{
						global $BITAMPCPJobCatalogTableInfo;
						$sql = sprintf("DELETE FROM ridim_10112temp WHERE `DSC_10120` = %s", $this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID));
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die( translate("Error accessing")." ridim_10112temp ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						$userJobs = $pcpResponse['papers'];
						foreach ($userJobs as $aJob)
						{
							if (isset($aJob['addrList']))
							{
								foreach ($aJob['addrList'] as $aJobAddress)
								{
									$sql = sprintf("INSERT INTO ridim_10112temp (`DSC_10113`, `DSC_10114`, `DSC_10115`, `DSC_10116`, `DSC_10117`, `DSC_10118`, `DSC_10119`, `DSC_10120`) VALUES ( %s, %s, %s, %s, %s, %s, %s, %s)", 
										$this->Repository->DataADOConnection->Quote($aJob['servee']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['address']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['city']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['state']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['zip']), 
										$this->Repository->DataADOConnection->Quote($aJob['servee'].' '.$aJobAddress['address'].($aJobAddress['address2'] == '' ? '' : ' ').$aJobAddress['address2'].', '.$aJobAddress['city'].', '.$aJobAddress['state'].', '.$aJobAddress['zip']), 
										$this->Repository->DataADOConnection->Quote($aJobAddress['address']),
										$this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID));

									if ($this->Repository->DataADOConnection->Execute($sql) === false)
									{
										die( translate("Error accessing")." ridim_10112temp ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
									}
								}
							}
						}
						$sql = sprintf("INSERT INTO ridim_10112 (`DSC_10112`, `IND_10024`, `DSC_10113`, `DSC_10114`, `DSC_10115`, `DSC_10116`, `DSC_10117`, `DSC_10118`, `DSC_10119`, `DSC_10120`, `DSC_10121`) SELECT @rownum:=@rownum+1, 1, t.`DSC_10113`, t.`DSC_10114`, t.`DSC_10115`, t.`DSC_10116`, t.`DSC_10117`, t.`DSC_10118`, t.`DSC_10119`, t.`DSC_10120`, '0' FROM ridim_10112temp t, (SELECT @rownum:=(SELECT IFNULL(MAX(ai.`RIDIM_10112KEY`), 0) FROM ridim_10112 ai)) rn WHERE t.`DSC_10120` = %s AND NOT EXISTS (SELECT * FROM ridim_10112 r WHERE r.`DSC_10118` = t.`DSC_10118` AND r.`DSC_10120` = t.`DSC_10120`)", $this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID));
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die( translate("Error accessing")." ridim_10112 ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						$sql = sprintf("DELETE FROM ridim_10112 WHERE ridim_10112.`RIDIM_10112KEY` > 1 AND ridim_10112.`DSC_10120` = %s AND ridim_10112.`DSC_10121` = '0' AND NOT EXISTS (SELECT * FROM ridim_10112temp t WHERE t.`DSC_10118` = ridim_10112.`DSC_10118` AND t.`DSC_10120` = ridim_10112.`DSC_10120`)", $this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID));
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die(translate("Error accessing")." ridim_10112 ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}
			}
			return parent::getAttributeValues($arrayAttribIDs, $aUserID, ($filter == '' ? sprintf(' AND (DSC_10120 = %s AND DSC_10121 = "0")', $this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID)) : sprintf('%s AND (DSC_10120 = %s AND DSC_10121 = "0")', $filter, $this->Repository->DataADOConnection->Quote($theAppUser->PCPUserID))));
		}
*/
		return parent::getAttributeValues($arrayAttribIDs, $aUserID, $filter);
	}

	static function PCPLogin($aHostName, $aServerId, $aPassword, $aDeviceId)
	{
		$pcpUrl = sprintf("http://%s/pcpMobile/login?serverId=%d&password=%s&deviceId=%s", $aHostName, $aServerId, $aPassword, $aDeviceId);
		$ch = curl_init();
		if ($ch === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_init() failed.", $pcpUrl); 
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_URL, $pcpUrl) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_URL) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_RETURNTRANSFER) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$jsonResult = @curl_exec($ch); 
		if ($jsonResult === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_exec() failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		@curl_close($ch);      

		$pcpResult = json_decode($jsonResult, true);
		if (!isset($pcpResult['code']))
		{
			$pcpError = sprintf("Error accessing %s. Unexpected response: %s.", $pcpUrl, $jsonResult);
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$pcpCode = $pcpResult['code'];
		if ($pcpCode != 0)
		{
			return array('pcpCode' => $pcpCode, 'pcpError' => $pcpResult['message']);
		}
		if (count($pcpResult) > 1)
		{
			unset($pcpResult['code']);
		}
		else
		{
			$pcpResult = array();
		}
		return $pcpResult;
	}

	static function PCPUpdateLocation($aHostName, $aServerId, $aLatitude, $aLongitude, $aDeviceId)
	{
		$pcpUrl = sprintf("http://%s/pcpMobile/%d/device?longitude=%.8F&latitude=%.8F&deviceId=%s", $aHostName, $aServerId, $aLongitude, $aLatitude, $aDeviceId);
		$ch = curl_init();
		if ($ch === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_init() failed.", $pcpUrl); 
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_URL, $pcpUrl) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_URL) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT") === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_CUSTOMREQUEST) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_RETURNTRANSFER) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$jsonResult = @curl_exec($ch); 
		if ($jsonResult === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_exec() failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		@curl_close($ch);      

		$pcpResult = json_decode($jsonResult, true);
		if (!isset($pcpResult['code']))
		{
			$pcpError = sprintf("Error accessing %s. Unexpected response: %s.", $pcpUrl, $jsonResult);
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$pcpCode = $pcpResult['code'];
		if ($pcpCode != 0)
		{
			return array('pcpCode' => $pcpCode, 'pcpError' => $pcpResult['message']);
		}
		if (count($pcpResult) > 1)
		{
			unset($pcpResult['code']);
		}
		else
		{
			$pcpResult = array();
		}
		return $pcpResult;
	}

	static function PCPGetJobs($aHostName, $aServerId, $aPassword)
	{
		$pcpUrl = sprintf("http://%s/pcpMobile/%d/papers?password=%s", $aHostName, $aServerId, $aPassword);
		$ch = curl_init();
		if ($ch === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_init() failed.", $pcpUrl); 
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_URL, $pcpUrl) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_URL) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_RETURNTRANSFER) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$jsonResult = @curl_exec($ch); 
		if ($jsonResult === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_exec() failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		@curl_close($ch);      

		$pcpResult = json_decode($jsonResult, true);
		if (!isset($pcpResult['code']))
		{
			$pcpError = sprintf("Error accessing %s. Unexpected response: %s.", $pcpUrl, $jsonResult);
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$pcpCode = $pcpResult['code'];
		if ($pcpCode != 0)
		{
			return array('pcpCode' => $pcpCode, 'pcpError' => $pcpResult['message']);
		}
		if (count($pcpResult) > 1)
		{
			unset($pcpResult['code']);
		}
		else
		{
			$pcpResult = array();
		}
		return $pcpResult;
	}

	static function PCPGetCodes($aHostName)
	{
		$pcpUrl = sprintf("http://%s/pcpMobile/codes", $aHostName);
		$ch = curl_init();
		if ($ch === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_init() failed.", $pcpUrl); 
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_URL, $pcpUrl) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_URL) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_RETURNTRANSFER) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$jsonResult = @curl_exec($ch); 
		if ($jsonResult === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_exec() failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		@curl_close($ch);      

		$pcpResult = json_decode($jsonResult, true);
		if (!is_array($pcpResult))
		{
			$pcpError = sprintf("Error accessing %s. json_decode() failed to decode: %s.", $pcpUrl, $jsonResult);
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (!isset($pcpResult['code']))
		{
			$pcpError = sprintf("Error accessing %s. Unexpected response: %s.", $pcpUrl, $jsonResult);
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$pcpCode = $pcpResult['code'];
		if ($pcpCode != 0)
		{
			return array('pcpCode' => $pcpCode, 'pcpError' => $pcpResult['message']);
		}
		if (count($pcpResult) > 1)
		{
			unset($pcpResult['code']);
		}
		else
		{
			$pcpResult = array();
		}
		return $pcpResult;
	}

	static function PCPSendAttempt($aHostName, $aPaperId, $anAttemptQueryArray, $anAttemptBodyArray = null)
	{
		$pcpQueryString = http_build_query($anAttemptQueryArray);
		$pcpUrl = sprintf("http://%s/pcpMobile/%s/attempt?%s", $aHostName, $aPaperId, $pcpQueryString);
		if (is_null($anAttemptBodyArray))
		{
			$pcpJsonBodyString = null;
		}
		else
		{
			$pcpJsonBodyString = json_encode($anAttemptBodyArray);
		}

		$ch = curl_init();
		if ($ch === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_init() failed.", $pcpUrl); 
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_URL, $pcpUrl) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_URL) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST") === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_CUSTOMREQUEST) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (!is_null($pcpJsonBodyString))
		{
			if (@curl_setopt($ch, CURLOPT_POSTFIELDS, $pcpJsonBodyString) === false)
			{
				$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_POSTFIELDS) failed with error: %s.", $pcpUrl, @curl_error($ch));
				return array('pcpCode' => '999', 'pcpError' => $pcpError);
			}
			if (@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: '.strlen($pcpJsonBodyString))) === false)
			{
				$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_HTTPHEADER) failed with error: %s.", $pcpUrl, @curl_error($ch));
				return array('pcpCode' => '999', 'pcpError' => $pcpError);
			}
		}
		if (@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_RETURNTRANSFER) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$jsonResult = @curl_exec($ch); 
		if ($jsonResult === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_exec() failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		@curl_close($ch);      

		$pcpResult = json_decode($jsonResult, true);
		if (!isset($pcpResult['code']))
		{
			$pcpError = sprintf("Error accessing %s. Unexpected response: %s.", $pcpUrl, $jsonResult);
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$pcpCode = $pcpResult['code'];
		if ($pcpCode != 0)
		{
			return array('pcpCode' => $pcpCode, 'pcpError' => $pcpResult['message']);
		}
		if (count($pcpResult) > 1)
		{
			unset($pcpResult['code']);
		}
		else
		{
			$pcpResult = array();
		}
		return $pcpResult;
	}

	static function PCPSendService($aHostName, $aPaperId, $aServiceQueryArray, $aServiceBodyArray = null)
	{
		$pcpQueryString =  http_build_query($aServiceQueryArray);
		$pcpUrl = sprintf("http://%s/pcpMobile/%s/service?%s", $aHostName, $aPaperId, $pcpQueryString);
		if (is_null($aServiceBodyArray))
		{
			$pcpJsonBodyString = null;
		}
		else
		{
			$pcpJsonBodyString = json_encode($aServiceBodyArray);
		}
		$ch = curl_init();
		if ($ch === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_init() failed.", $pcpUrl); 
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_URL, $pcpUrl) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_URL) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (@curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST") === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_CUSTOMREQUEST) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		if (!is_null($pcpJsonBodyString))
		{
			if (@curl_setopt($ch, CURLOPT_POSTFIELDS, $pcpJsonBodyString) === false)
			{
				$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_POSTFIELDS) failed with error: %s.", $pcpUrl, @curl_error($ch));
				return array('pcpCode' => '999', 'pcpError' => $pcpError);
			}
			if (@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: '.strlen($pcpJsonBodyString))) === false)
			{
				$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_HTTPHEADER) failed with error: %s.", $pcpUrl, @curl_error($ch));
				return array('pcpCode' => '999', 'pcpError' => $pcpError);
			}
		}
		if (@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_setopt(CURLOPT_RETURNTRANSFER) failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$jsonResult = @curl_exec($ch); 
		if ($jsonResult === false)
		{
			$pcpError = sprintf("Error accessing %s. curl_exec() failed with error: %s.", $pcpUrl, @curl_error($ch));
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		@curl_close($ch);      

		$pcpResult = json_decode($jsonResult, true);
		if (!isset($pcpResult['code']))
		{
			$pcpError = sprintf("Error accessing %s. Unexpected response: %s.", $pcpUrl, $jsonResult);
			return array('pcpCode' => '999', 'pcpError' => $pcpError);
		}
		$pcpCode = $pcpResult['code'];
		if ($pcpCode != 0)
		{
			return array('pcpCode' => $pcpCode, 'pcpError' => $pcpResult['message']);
		}
		if (count($pcpResult) > 1)
		{
			unset($pcpResult['code']);
		}
		else
		{
			$pcpResult = array();
		}
		return $pcpResult;
	}
}

class BITAMPCPCatalogCollection extends BITAMCatalogCollection
{

}

function BITAMPCPSend_Jobs($aRepository, $aSurvey, $aJsonData, $anArrayOfQuestionsByID)
{
	global $theAppUser;
	if (is_null($theAppUser)) 
	{
		echo("Unknown user");
		return false;
	}
	if (!isset($aJsonData['surveyDate']) || !isset($aJsonData['surveyHour']))
	{
		echo("Unrecognized Request: date & hour missing");
		return false;
	}
	$aPCPTs = date("m/d/Y H:i", strtotime(sprintf("%s %s", $aJsonData['surveyDate'], $aJsonData['surveyHour'])));
	if (!isset($aJsonData['answers']))
	{
		echo("Unrecognized Request: answers missing");
		return false;
	}
	$anArrayOfAnswersById = $aJsonData['answers'];
	$anArrayOfQuestionsByName = array();
	$anArrayOfAnswersByName = array();
	foreach ($anArrayOfQuestionsByID as $aQuestionID => $aQuestion)
	{
		$anArrayOfQuestionsByName[$aQuestion->AttributeName] = $aQuestion;
		if (isset($anArrayOfAnswersById[$aQuestionID]))
		{
			$anArrayOfAnswersByName[$aQuestion->AttributeName] = $anArrayOfAnswersById[$aQuestionID];
		}
	}
	if (!isset($anArrayOfAnswersByName['JobServiceType']))
	{
		echo("Unrecognized Request: Service Type missing");
		return false;
	}
	$aPCPServiceType = $anArrayOfAnswersByName['JobServiceType']['answer'];
	if ($aPCPServiceType != 'Attempting' && $aPCPServiceType != 'Manner of Service')
	{
		echo(sprintf("Unrecognized Request: Service Type %s unknown", $aPCPServiceType));
		return false;
	}
	if ($aPCPServiceType == 'Attempting')
	{
		if (!isset($anArrayOfAnswersByName['serveeAddress']))
		{
			echo("Unrecognized Request: JobId missing");
			return false;
		}
		$aPCPJobIdKey = $anArrayOfAnswersByName['serveeAddress']['key'];

		$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $anArrayOfQuestionsByName['JobId']->CatalogID);
		if (is_null($aCatalog)) 
		{
			echo("Unrecognized Request: Job Catalog not found");
			return false;
		}
		$aCatalogMembersCollection = BITAMCatalogMemberCollection::NewInstance($aRepository, $anArrayOfQuestionsByName['JobId']->CatalogID);
		if (is_null($aCatalogMembersCollection))
		{
			echo("Unrecognized Request: Job Catalog Members not found");
			return false;
		}
		//Obtiene la lista de atributos de la pregunta (incluyendo todos los padres)
		$arrAttributeIDs = array();
		foreach ($aCatalogMembersCollection->Collection as $aCatalogMember)
		{
			$arrAttributeIDs[] = array('dimID' => $aCatalogMember->ClaDescrip, 'MemberName' => $aCatalogMember->MemberName);
		}
						
		if (count($arrAttributeIDs) == 0)
		{
			echo("Unrecognized Request: Job Catalog Members empty");
			return false;
		}
						
		$strFilter = sprintf('AND %sKEY = %d ', $aCatalog->TableName, $aPCPJobIdKey);

		$arrCatalogValuesHierarchy = $aCatalog->getAttributeValuesBasic($arrAttributeIDs, -1, $strFilter);

		$arrLevelValues = $arrCatalogValuesHierarchy;
		$intIndex = 0;
		$intMax = count($arrAttributeIDs);
		$aPCPJobCatalogValues = array();
		while (count($arrLevelValues) > 0 && $intIndex < $intMax)
		{
			if (isset($arrLevelValues[0]))
			{
				$aPCPJobCatalogValues[$arrAttributeIDs[$intIndex]['MemberName']] = $arrLevelValues[0]['value'];
				$arrLevelValues = $arrLevelValues[0]['children'];
			}
			$intIndex++;
		}
		if (count($aPCPJobCatalogValues) == 0)
		{
			echo(sprintf("Unrecognized Request: Job Catalog data not found for Job key = %d", $aPCPJobIdKey));
			return false;
		}

		if (!isset($anArrayOfAnswersByName['JobAttemptCode']))
		{
			echo("Unrecognized Request: Attempt Code missing");
			return false;
		}
		$aPCPJobAttemptCodeKey = $anArrayOfAnswersByName['JobAttemptCode']['key'];
		$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $anArrayOfQuestionsByName['JobAttemptCode']->CatalogID);
		if (is_null($aCatalog)) 
		{
			echo("Unrecognized Request: Code Catalog not found");
			return false;
		}
		$aCatalogMembersCollection = BITAMCatalogMemberCollection::NewInstance($aRepository, $anArrayOfQuestionsByName['JobAttemptCode']->CatalogID);
		if (is_null($aCatalogMembersCollection))
		{
			echo("Unrecognized Request: Code Catalog Members not found");
			return false;
		}
		//Obtiene la lista de atributos de la pregunta (incluyendo todos los padres)
		$arrAttributeIDs = array();
		foreach ($aCatalogMembersCollection->Collection as $aCatalogMember)
		{
			$arrAttributeIDs[] = array('dimID' => $aCatalogMember->ClaDescrip, 'MemberName' => $aCatalogMember->MemberName);
		}
						
		if (count($arrAttributeIDs) == 0)
		{
			echo("Unrecognized Request: Code Catalog Members empty");
			return false;
		}
						
		$strFilter = sprintf('AND %sKEY = %d ', $aCatalog->TableName, $aPCPJobAttemptCodeKey);

		$arrCatalogValuesHierarchy = $aCatalog->getAttributeValuesBasic($arrAttributeIDs, -1, $strFilter);

		$arrLevelValues = $arrCatalogValuesHierarchy;
		$intIndex = 0;
		$intMax = count($arrAttributeIDs);
		$aPCPAttemptCodeCatalogValues = array();
		while (count($arrLevelValues) > 0 && $intIndex < $intMax)
		{
			if (isset($arrLevelValues[0]))
			{
				$aPCPAttemptCodeCatalogValues[$arrAttributeIDs[$intIndex]['MemberName']] = $arrLevelValues[0]['value'];
				$arrLevelValues = $arrLevelValues[0]['children'];
			}
			$intIndex++;
		}
		if (count($aPCPAttemptCodeCatalogValues) == 0)
		{
			echo(sprintf("Unrecognized Request: Code Catalog data not found for Code key = %d", $aPCPJobAttemptCodeKey));
			return false;
		}

		if (!isset($aPCPJobCatalogValues['paperId']))
		{
			echo("Unrecognized Request: Job # missing");
			return false;
		}
		$aPCPPaperId = $aPCPJobCatalogValues['paperId'];
		$aPCPAttemptQueryArray = array();
		$aPCPAttemptQueryArray['serverId'] = $theAppUser->PCPUserID;
		$aPCPAttemptQueryArray['ts'] = $aPCPTs;
		$aPCPAttemptQueryArray['code'] = isset($aPCPAttemptCodeCatalogValues['code']) ? $aPCPAttemptCodeCatalogValues['code'] : '';
		$aPCPAttemptQueryArray['comment'] = isset($anArrayOfAnswersByName['JobAttemptAdditionalInformation']) ? $anArrayOfAnswersByName['JobAttemptAdditionalInformation']['answer'] : '';
		$aPCPAttemptQueryArray['address'] = isset($aPCPJobCatalogValues['address']) ? $aPCPJobCatalogValues['address'] : '';
		$aPCPAttemptQueryArray['addr2'] = isset($aPCPJobCatalogValues['address2']) ? $aPCPJobCatalogValues['address2'] : '';
		$aPCPAttemptQueryArray['city'] = isset($aPCPJobCatalogValues['city']) ? $aPCPJobCatalogValues['city'] : '';
		$aPCPAttemptQueryArray['state'] = isset($aPCPJobCatalogValues['state']) ? $aPCPJobCatalogValues['state'] : '';
		$aPCPAttemptQueryArray['zip'] = isset($aPCPJobCatalogValues['zip']) ? $aPCPJobCatalogValues['zip'] : '';
		$aPCPAttemptQueryArray['addressType'] = isset($aPCPJobCatalogValues['addressType']) ? $aPCPJobCatalogValues['addressType'] : '';
		$aPCPAttemptQueryArray['county'] = isset($aPCPJobCatalogValues['county']) ? $aPCPJobCatalogValues['county'] : '';
		$aPCPAttemptQueryArray['runPlates'] = isset($anArrayOfAnswersByName['JobAttemptRunPlates']) ? ($anArrayOfAnswersByName['JobAttemptRunPlates']['answer'] == 'Yes') : false;
		if ($aPCPAttemptQueryArray['runPlates'])
		{
			$aPCPAttemptQueryArray['comment'] = $aPCPAttemptQueryArray['comment']." LICENSE PLATE 1: ".(isset($anArrayOfAnswersByName['JobAttempLicensePlate1']) ? trim($anArrayOfAnswersByName['JobAttempLicensePlate1']['answer']) : '');
			if (isset($anArrayOfAnswersByName['JobAttempLicensePlate2']))
			{
				if (trim($anArrayOfAnswersByName['JobAttempLicensePlate2']['answer']) != '')
				{
					$aPCPAttemptQueryArray['comment'] = $aPCPAttemptQueryArray['comment']." LICENSE PLATE 2: ".trim($anArrayOfAnswersByName['JobAttempLicensePlate2']['answer']);
				}
			}
			if (isset($anArrayOfAnswersByName['JobAttempLicensePlate3']))
			{
				if (trim($anArrayOfAnswersByName['JobAttempLicensePlate3']['answer']) != '')
				{
					$aPCPAttemptQueryArray['comment'] = $aPCPAttemptQueryArray['comment']." LICENSE PLATE 3: ".trim($anArrayOfAnswersByName['JobAttempLicensePlate3']['answer']);
				}
			}
		}
		$aPCPAttemptQueryArray['req106'] = isset($anArrayOfAnswersByName['JobAttemptReq106']) ? ($anArrayOfAnswersByName['JobAttemptReq106']['answer'] == 'Yes') : false;
	
		$aPCPAttemptBodyArray = array();
		$aPCPAttemptBodyArray['pictures'] = array();

		global $blnWebMode;
		if (isset($anArrayOfAnswersByName['JobPicture1']))
		{
			$aPicturePath = $anArrayOfAnswersByName['JobPicture1']['photo'];
			if (trim($aPicturePath) != '')
			{
				if (!$blnWebMode)
				{
					$aPicturePath = sprintf('./syncdata/%s/%s.jpg', $aRepository->RepositoryName, $aPicturePath);
				}
				if (file_exists($aPicturePath))
				{
					$aPicture = file_get_contents($aPicturePath);
					$aBase64Picture = base64_encode($aPicture);
					$aPictureArray  = array();
					$aPictureArray['image'] = $aBase64Picture;
					$aPictureArray['format'] = 'JPG';
					$aPictureArray['latitude'] = isset($aJsonData['latitude']) ? (float) $aJsonData['latitude'] : 0.0;
					$aPictureArray['longitude'] = isset($aJsonData['longitude']) ? (float) $aJsonData['longitude'] : 0.0;
					$aPictureArray['ts'] = $aPCPTs;
					$aPCPAttemptBodyArray['pictures'][] = $aPictureArray;
				}
			}
		}
		if (isset($anArrayOfAnswersByName['JobPicture2']))
		{
			$aPicturePath = $anArrayOfAnswersByName['JobPicture2']['photo'];
			if (trim($aPicturePath) != '')
			{
				if (!$blnWebMode)
				{
					$aPicturePath = sprintf('./syncdata/%s/%s.jpg', $aRepository->RepositoryName, $aPicturePath);
				}
				if (file_exists($aPicturePath))
				{
					$aPicture = file_get_contents($aPicturePath);
					$aBase64Picture = base64_encode($aPicture);
					$aPictureArray  = array();
					$aPictureArray['image'] = $aBase64Picture;
					$aPictureArray['format'] = 'JPG';
					$aPictureArray['latitude'] = isset($aJsonData['latitude']) ? (float) $aJsonData['latitude'] : 0.0;
					$aPictureArray['longitude'] = isset($aJsonData['longitude']) ? (float) $aJsonData['longitude'] : 0.0;
					$aPictureArray['ts'] = sprintf("%s %s", $aJsonData['surveyDate'], $aJsonData['surveyHour']);
					$aPCPAttemptBodyArray['pictures'][] = $aPictureArray;
				}
			}
		}
		if (isset($anArrayOfAnswersByName['JobPicture3']))
		{
			$aPicturePath = $anArrayOfAnswersByName['JobPicture3']['photo'];
			if (trim($aPicturePath) != '')
			{
				if (!$blnWebMode)
				{
					$aPicturePath = sprintf('./syncdata/%s/%s.jpg', $aRepository->RepositoryName, $aPicturePath);
				}
				if (file_exists($aPicturePath))
				{
					$aPicture = file_get_contents($aPicturePath);
					$aBase64Picture = base64_encode($aPicture);
					$aPictureArray  = array();
					$aPictureArray['image'] = $aBase64Picture;
					$aPictureArray['format'] = 'JPG';
					$aPictureArray['latitude'] = isset($aJsonData['latitude']) ? (float) $aJsonData['latitude'] : 0.0;
					$aPictureArray['longitude'] = isset($aJsonData['longitude']) ? (float) $aJsonData['longitude'] : 0.0;
					$aPictureArray['ts'] = sprintf("%s %s", $aJsonData['surveyDate'], $aJsonData['surveyHour']);
					$aPCPAttemptBodyArray['pictures'][] = $aPictureArray;
				}
			}
		}
		if (isset($anArrayOfAnswersByName['JobPicture4']))
		{
			$aPicturePath = $anArrayOfAnswersByName['JobPicture4']['photo'];
			if (trim($aPicturePath) != '')
			{
				if (!$blnWebMode)
				{
					$aPicturePath = sprintf('./syncdata/%s/%s.jpg', $aRepository->RepositoryName, $aPicturePath);
				}
				if (file_exists($aPicturePath))
				{
					$aPicture = file_get_contents($aPicturePath);
					$aBase64Picture = base64_encode($aPicture);
					$aPictureArray  = array();
					$aPictureArray['image'] = $aBase64Picture;
					$aPictureArray['format'] = 'JPG';
					$aPictureArray['latitude'] = isset($aJsonData['latitude']) ? (float) $aJsonData['latitude'] : 0.0;
					$aPictureArray['longitude'] = isset($aJsonData['longitude']) ? (float) $aJsonData['longitude'] : 0.0;
					$aPictureArray['ts'] = sprintf("%s %s", $aJsonData['surveyDate'], $aJsonData['surveyHour']);
					$aPCPAttemptBodyArray['pictures'][] = $aPictureArray;
				}
			}
		}

		global $BITAMPCPHostName;
		//var_dump($BITAMPCPHostName);
		//var_dump($aPCPPaperId);
		//var_dump($aPCPAttemptQueryArray);
		//var_dump($aPCPAttemptBodyArray);
		//return false;
/*
		$pcpResponse = BITAMPCPCatalog::PCPSendAttempt($BITAMPCPHostName, $aPCPPaperId, $aPCPAttemptQueryArray, $aPCPAttemptBodyArray);
		if (isset($pcpResponse['pcpCode']))
		{
			//var_dump($BITAMPCPHostName);
			//var_dump($aPCPPaperId);
			//var_dump($aPCPAttemptQueryArray);
			//var_dump($aPCPAttemptBodyArray);
			echo(translate("Error accessing")." ".$BITAMPCPHostName."/attempt".": [".$pcpResponse['pcpCode']."] ".$pcpResponse['pcpError']);
			return false;
		}
*/
		return true;
	}
	else if ($aPCPServiceType == 'Manner of Service')
	{
		if (isset($anArrayOfAnswersByName['IsActualAddress']) ? ($anArrayOfAnswersByName['IsActualAddress']['answer'] != 'Yes') : false)
		{
			if (!isset($anArrayOfAnswersByName['JobId']))
			{
				echo("Unrecognized Request: JobId missing");
				return false;
			}
			$aPCPJobIdKey = $anArrayOfAnswersByName['JobId']['key'];
		}
		else
		{
			if (!isset($anArrayOfAnswersByName['serveeAddress']))
			{
				echo("Unrecognized Request: JobId missing");
				return false;
			}
			$aPCPJobIdKey = $anArrayOfAnswersByName['serveeAddress']['key'];
		}
		$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $anArrayOfQuestionsByName['JobId']->CatalogID);
		if (is_null($aCatalog)) 
		{
			echo("Unrecognized Request: Job Catalog not found");
			return false;
		}
		$aCatalogMembersCollection = BITAMCatalogMemberCollection::NewInstance($aRepository, $anArrayOfQuestionsByName['JobId']->CatalogID);
		if (is_null($aCatalogMembersCollection))
		{
			echo("Unrecognized Request: Job Catalog Members not found");
			return false;
		}
		//Obtiene la lista de atributos de la pregunta (incluyendo todos los padres)
		$arrAttributeIDs = array();
		foreach ($aCatalogMembersCollection->Collection as $aCatalogMember)
		{
			$arrAttributeIDs[] = array('dimID' => $aCatalogMember->ClaDescrip, 'MemberName' => $aCatalogMember->MemberName);
		}
						
		if (count($arrAttributeIDs) == 0)
		{
			echo("Unrecognized Request: Job Catalog Members empty");
			return false;
		}
						
		$strFilter = sprintf('AND %sKEY = %d ', $aCatalog->TableName, $aPCPJobIdKey);

		$arrCatalogValuesHierarchy = $aCatalog->getAttributeValuesBasic($arrAttributeIDs, -1, $strFilter);

		$arrLevelValues = $arrCatalogValuesHierarchy;
		$intIndex = 0;
		$intMax = count($arrAttributeIDs);
		$aPCPJobCatalogValues = array();
		while (count($arrLevelValues) > 0 && $intIndex < $intMax)
		{
			if (isset($arrLevelValues[0]))
			{
				$aPCPJobCatalogValues[$arrAttributeIDs[$intIndex]['MemberName']] = $arrLevelValues[0]['value'];
				$arrLevelValues = $arrLevelValues[0]['children'];
			}
			$intIndex++;
		}
		if (count($aPCPJobCatalogValues) == 0)
		{
			echo(sprintf("Unrecognized Request: Job Catalog data not found for Job key = %d", $aPCPJobIdKey));
			return false;
		}

		if (!isset($aPCPJobCatalogValues['paperId']))
		{
			echo("Unrecognized Request: Job # missing");
			return false;
		}
		$aPCPPaperId = $aPCPJobCatalogValues['paperId'];
		$aPCPServiceQueryArray = array();
		$aPCPServiceQueryArray['serverId'] = $theAppUser->PCPUserID;
		$aPCPServiceQueryArray['ts'] = $aPCPTs;
		$aPCPServiceQueryArray['newAddress'] = isset($anArrayOfAnswersByName['IsActualAddress']) ? ($anArrayOfAnswersByName['IsActualAddress']['answer'] != 'Yes') : false;
		if ($aPCPServiceQueryArray['newAddress'])
		{
			$aPCPServiceQueryArray['address'] = isset($anArrayOfAnswersByName['newServeeAddress']) ? $anArrayOfAnswersByName['newServeeAddress']['answer'] : '';
			$aPCPServiceQueryArray['addr2'] = isset($anArrayOfAnswersByName['newServeeAddress2']) ? $anArrayOfAnswersByName['newServeeAddress2']['answer'] : '';
			$aPCPServiceQueryArray['city'] = isset($anArrayOfAnswersByName['newServeeCity']) ? $anArrayOfAnswersByName['newServeeCity']['answer'] : '';
			$aPCPServiceQueryArray['state'] = isset($anArrayOfAnswersByName['newServeeState']) ? $anArrayOfAnswersByName['newServeeState']['answer'] : '';
			$aPCPServiceQueryArray['zip'] = isset($anArrayOfAnswersByName['newServeeZip']) ? $anArrayOfAnswersByName['newServeeZip']['answer'] : '';
			$aPCPServiceQueryArray['addressType'] = 'O';
			$aPCPServiceQueryArray['county'] = isset($anArrayOfAnswersByName['newServeeCounty']) ? $anArrayOfAnswersByName['newServeeCounty']['answer'] : '';
		}
		else
		{
			$aPCPServiceQueryArray['address'] = isset($aPCPJobCatalogValues['address']) ? $aPCPJobCatalogValues['address'] : '';
			$aPCPServiceQueryArray['addr2'] = isset($aPCPJobCatalogValues['address2']) ? $aPCPJobCatalogValues['address2'] : '';
			$aPCPServiceQueryArray['city'] = isset($aPCPJobCatalogValues['city']) ? $aPCPJobCatalogValues['city'] : '';
			$aPCPServiceQueryArray['state'] = isset($aPCPJobCatalogValues['state']) ? $aPCPJobCatalogValues['state'] : '';
			$aPCPServiceQueryArray['zip'] = isset($aPCPJobCatalogValues['zip']) ? $aPCPJobCatalogValues['zip'] : '';
			$aPCPServiceQueryArray['addressType'] = isset($aPCPJobCatalogValues['addressType']) ? $aPCPJobCatalogValues['addressType'] : '';
			$aPCPServiceQueryArray['county'] = isset($aPCPJobCatalogValues['county']) ? $aPCPJobCatalogValues['county'] : '';
		}

		$aPCPServiceQueryArray['comment'] = isset($anArrayOfAnswersByName['JobComments']) ? $anArrayOfAnswersByName['JobComments']['answer'] : '';
		$aPCPServiceManner = isset($anArrayOfAnswersByName['JobServiceManner']) ? $anArrayOfAnswersByName['JobServiceManner']['answer'] : '';
		
		if ($aPCPServiceManner == 'Personally' || $aPCPServiceManner == 'Posted')
		{
			$aPCPServiceQueryArray['servee'] = isset($aPCPJobCatalogValues['servee']) ? $aPCPJobCatalogValues['servee'] : '';
			$aPCPServiceQueryArray['serveeTitle'] = '';
		}
		else if ($aPCPServiceManner == 'Substitute')
		{
			$aPCPServiceQueryArray['servee'] = isset($anArrayOfAnswersByName['JobNewServee']) ? $anArrayOfAnswersByName['JobNewServee']['answer'] : '';
			$aPCPServiceQueryArray['serveeTitle'] = isset($anArrayOfAnswersByName['JobServeeRelationship']) ? $anArrayOfAnswersByName['JobServeeRelationship']['answer'] : '';
		}
		else if ($aPCPServiceManner == 'Corporation')
		{
			$aPCPServiceQueryArray['servee'] = isset($anArrayOfAnswersByName['JobNewServee']) ? $anArrayOfAnswersByName['JobNewServee']['answer'] : '';
			$aPCPIsStatutoryAgent = isset($anArrayOfAnswersByName['JobServeeIsStatutoryAgent']) ? ($anArrayOfAnswersByName['JobServeeIsStatutoryAgent']['answer'] != 'Yes') : false;
			if ($aPCPIsStatutoryAgent)
			{
				$aPCPServiceQueryArray['serveeTitle'] = 'agent';
			}
			else
			{
				$aPCPServiceQueryArray['serveeTitle'] = isset($anArrayOfAnswersByName['JobServeeTitle']) ? $anArrayOfAnswersByName['JobServeeTitle']['answer'] : '';
			}
		}
		else
		{
			echo(sprintf("Unrecognized Request: Unknown Service Manner: '%s'", $aPCPServiceManner));
			return false;
		}
	
		$aPCPServiceBodyArray = array();
		$aPCPServiceBodyArray['pictures'] = array();

		global $blnWebMode;
		if (isset($anArrayOfAnswersByName['JobPicture1']))
		{
			$aPicturePath = $anArrayOfAnswersByName['JobPicture1']['photo'];
			if (trim($aPicturePath) != '')
			{
				if (!$blnWebMode)
				{
					$aPicturePath = sprintf('./syncdata/%s/%s.jpg', $aRepository->RepositoryName, $aPicturePath);
				}
				if (file_exists($aPicturePath))
				{
					$aPicture = file_get_contents($aPicturePath);
					$aBase64Picture = base64_encode($aPicture);
					$aPictureArray  = array();
					$aPictureArray['image'] = $aBase64Picture;
					$aPictureArray['format'] = 'JPG';
					$aPictureArray['latitude'] = isset($aJsonData['latitude']) ? (float) $aJsonData['latitude'] : 0.0;
					$aPictureArray['longitude'] = isset($aJsonData['longitude']) ? (float) $aJsonData['longitude'] : 0.0;
					$aPictureArray['ts'] = $aPCPTs;
					$aPCPServiceBodyArray['pictures'][] = $aPictureArray;
				}
			}
		}
		if (isset($anArrayOfAnswersByName['JobPicture2']))
		{
			$aPicturePath = $anArrayOfAnswersByName['JobPicture2']['photo'];
			if (trim($aPicturePath) != '')
			{
				if (!$blnWebMode)
				{
					$aPicturePath = sprintf('./syncdata/%s/%s.jpg', $aRepository->RepositoryName, $aPicturePath);
				}
				if (file_exists($aPicturePath))
				{
					$aPicture = file_get_contents($aPicturePath);
					$aBase64Picture = base64_encode($aPicture);
					$aPictureArray  = array();
					$aPictureArray['image'] = $aBase64Picture;
					$aPictureArray['format'] = 'JPG';
					$aPictureArray['latitude'] = isset($aJsonData['latitude']) ? (float) $aJsonData['latitude'] : 0.0;
					$aPictureArray['longitude'] = isset($aJsonData['longitude']) ? (float) $aJsonData['longitude'] : 0.0;
					$aPictureArray['ts'] = sprintf("%s %s", $aJsonData['surveyDate'], $aJsonData['surveyHour']);
					$aPCPServiceBodyArray['pictures'][] = $aPictureArray;
				}
			}
		}
		if (isset($anArrayOfAnswersByName['JobPicture3']))
		{
			$aPicturePath = $anArrayOfAnswersByName['JobPicture3']['photo'];
			if (trim($aPicturePath) != '')
			{
				if (!$blnWebMode)
				{
					$aPicturePath = sprintf('./syncdata/%s/%s.jpg', $aRepository->RepositoryName, $aPicturePath);
				}
				if (file_exists($aPicturePath))
				{
					$aPicture = file_get_contents($aPicturePath);
					$aBase64Picture = base64_encode($aPicture);
					$aPictureArray  = array();
					$aPictureArray['image'] = $aBase64Picture;
					$aPictureArray['format'] = 'JPG';
					$aPictureArray['latitude'] = isset($aJsonData['latitude']) ? (float) $aJsonData['latitude'] : 0.0;
					$aPictureArray['longitude'] = isset($aJsonData['longitude']) ? (float) $aJsonData['longitude'] : 0.0;
					$aPictureArray['ts'] = sprintf("%s %s", $aJsonData['surveyDate'], $aJsonData['surveyHour']);
					$aPCPServiceBodyArray['pictures'][] = $aPictureArray;
				}
			}
		}
		if (isset($anArrayOfAnswersByName['JobPicture4']))
		{
			$aPicturePath = $anArrayOfAnswersByName['JobPicture4']['photo'];
			if (trim($aPicturePath) != '')
			{
				if (!$blnWebMode)
				{
					$aPicturePath = sprintf('./syncdata/%s/%s.jpg', $aRepository->RepositoryName, $aPicturePath);
				}
				if (file_exists($aPicturePath))
				{
					$aPicture = file_get_contents($aPicturePath);
					$aBase64Picture = base64_encode($aPicture);
					$aPictureArray  = array();
					$aPictureArray['image'] = $aBase64Picture;
					$aPictureArray['format'] = 'JPG';
					$aPictureArray['latitude'] = isset($aJsonData['latitude']) ? (float) $aJsonData['latitude'] : 0.0;
					$aPictureArray['longitude'] = isset($aJsonData['longitude']) ? (float) $aJsonData['longitude'] : 0.0;
					$aPictureArray['ts'] = sprintf("%s %s", $aJsonData['surveyDate'], $aJsonData['surveyHour']);
					$aPCPServiceBodyArray['pictures'][] = $aPictureArray;
				}
			}
		}

		global $BITAMPCPHostName;
		//var_dump($BITAMPCPHostName);
		//var_dump($aPCPPaperId);
		//var_dump($aPCPServiceQueryArray);
		//var_dump($aPCPServiceBodyArray);
		$pcpResponse = BITAMPCPCatalog::PCPSendService($BITAMPCPHostName, $aPCPPaperId, $aPCPServiceQueryArray, $aPCPServiceBodyArray);
		if (isset($pcpResponse['pcpCode']))
		{
			echo(translate("Error accessing")." ".$BITAMPCPHostName."/service".": [".$pcpResponse['pcpCode']."] ".$pcpResponse['pcpError']);
			return false;
		}
		$sql = sprintf("UPDATE FROM ridim_10093 SET `DSC_10111` = '1' WHERE `RIDIM_10093KEY` = %s", $aPCPJobIdKey);
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die(translate("Error accessing")." ridim_10093 ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return true;
	}
	else
	{	echo(sprintf("Unrecognized Request: Unknown Service Type: '%s'", $aPCPServiceManner));
		return false;
	}
	return true;	
}


function BITAMPCPStoreSurveyCapture($aRespository, $aSurvey, $aJsonData, $anArrayOfQuestionsByID)
{
	if ($aSurvey->SurveyName == 'Jobs')
	{
		return BITAMPCPSend_Jobs($aRespository, $aSurvey, $aJsonData, $anArrayOfQuestionsByID);
	}
	return false;
}


?>