<?
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
$theObject=$this;
if (!is_null($theObject))
{
?>
	<div class="path-div">
<?
	$pathArray = $theObject->get_PathArray();
	$pathIndex = 1;
	foreach ($pathArray as $anArray)
	{
		if ($pathIndex > 1 && $pathIndex < count($pathArray))
		{
?>
		&nbsp;/&nbsp;
<?
		}
		if ($pathIndex < count($pathArray))
		{
			list($objectName, $objectQueryString) = $anArray;
?>
		<a class="path-div" href="main.php<?= ($objectQueryString == "" ? "" : "?").$objectQueryString ?>"><?= $objectName ?></a>
<?
		}
		$pathIndex++;
	}
?>
	</div>
<?
}
?>