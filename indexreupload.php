<?
global $directory;
global $strUserName;
$strUserName = (string) @$_GET['UserName'];
$strUserPassword = (string) @$_GET['Password'];
$strRepository = (string) @$_GET['Repository'];
$intDebugBreak = (int) @$_GET['DebugBreak'];

function getDirectoryList ($directory) {
	global $strUserName;
	
	// create an array to hold directory list
	$results = array();
	// create a handler for the directory
	$handler = @opendir($directory);
	if ($handler === false) {
		return $results;
	}
	
	// open directory and walk through the filenames
	while ($file = @readdir($handler)) {
		// if file isn't this directory or its parent, add it to the results
		if ($file != "." && $file != "..") {
			//@JAPR 2013-02-27: Ajustada la funcionalidad de Reupload para v4
			$errorLogPrefix = "surveyanswers_";
			if(strpos($file, $errorLogPrefix) !== false && strpos($file, $strUserName) !== false) {
				$results[] = $file;
			}
		}
	}
	
	// tidy up: close the handler
	@closedir($handler);
	// done!
	return $results;
}


$directory = getcwd();
$directory .= "\\syncerrors\\".$strRepository;
$dirFiles = array();
$dirFiles = getDirectoryList($directory);

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <title>RECOVER UPLOAD ERROR</title>
		<script type="text/javascript" src="jquery-1.6.2.js"></script>
		<script type="text/javascript" charset="utf-8">
			function redoUpload() {
				$('#frmReloadUploadErrors').submit();
			}
		</script>
	</head>
    <body onload="onBodyLoad()">
		<form id="frmReloadUploadErrors" name="frmReloadUploadErrors" action="reloadUploadErrors.php?UserName=<?=$strUserName?>&Password=<?=$strUserPassword?>&Repository=<?=$strRepository?>&DebugBreak=<?=$intDebugBreak?>" method="post" target="body">
			<? foreach ($dirFiles as $idxError => $errorFile) { ?>
				<input type="checkbox" name="<?=$idxError?>" value="<?=$errorFile?>"/><?=$errorFile?><br>
			<? } ?>
			<br>
			<input type="button" onclick="javascript: redoUpload();" name="buttonUploadErrors" value="Redo Upload"/>
		<form>
	</body>
</html>