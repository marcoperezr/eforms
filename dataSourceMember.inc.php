<?php 
require_once("repository.inc.php");
require_once("initialize.php");
require_once("dataSource.inc.php");
require_once("survey.inc.php");

class BITAMDataSourceMember extends BITAMObject
{
	public $DataSourceID;
	public $DataSourceName;		//Referencia
	public $TableName;			//Referencia
	public $MemberID;
	public $MemberName;
	public $FieldName;
	public $MemberOrder;
	public $eBavelFieldID;
	public $eBavelFieldDataID;
	public $KeyOfAgenda;
	public $AgendaLatitude;
	public $AgendaLongitude;
	public $AgendaDisplay;
	public $IsImage;
	public $MemberType;	//SI_SV_Question.QTypeID (TIPOS DE DATO + Photo)
	
	function __construct($aRepository, $aDataSourceID)
	{
		BITAMObject::__construct($aRepository);
		
		$this->DataSourceID = $aDataSourceID;
		$this->DataSourceName= "";
		$this->TableName = "";
		$this->MemberID = -1;
		$this->MemberName = "";
		$this->FieldName = "";
		$this->eBavelFieldID = 0;
		$this->eBavelFieldDataID = ebfdValue;
		$this->KeyOfAgenda = 0;
		$this->AgendaLatitude = 0;
		$this->AgendaLongitude = 0;
		$this->AgendaDisplay = 0;
		$this->IsImage = 0;
		
		unset($aRS);
	}

	static function NewInstance($aRepository, $aDataSourceID)
	{
		return new BITAMDataSourceMember($aRepository, $aDataSourceID);
	}

	static function NewInstanceWithID($aRepository, $aMemberID)
	{
		$anInstance = null;
		
		if (((int) $aMemberID) <= 0)
		{
			return $anInstance;
		}
		
		$anInstance =& BITAMGlobalFormsInstance::GetDataMemberInstanceWithID($aMemberID);
		if (!is_null($anInstance)) {
			return $anInstance;
		}
		
		$strAdditionalFields = '';
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', t1.eBavelFieldID';
		}
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.KeyOfAgenda, t1.AgendaLatitude, t1.AgendaLongitude, t1.AgendaDisplay';
		}
		//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		if (getMDVersion() >= esveBavelGeolocationAttribs) {
			$strAdditionalFields .= ', t1.eBavelFieldDataID';
		}
		if (getMDVersion() >= esvCatalogAttributeImage) {
			//@JAPR 2015-12-15: Corregido un bug, habían deshabilitado la carga de este campo, así que la corrección de este Issue no había funcionado (#SS2EKF)
			$strAdditionalFields .= ', t1.IsImage';
		}
		$sql = "SELECT t1.DataSourceID, t2.DataSourceName, t2.TableName, t1.MemberID, t1.MemberName, t1.fieldName, t1.MemberOrder $strAdditionalFields 
				FROM SI_SV_DataSourceMember t1, SI_SV_DataSource t2 
				WHERE t1.DataSourceID = t2.DataSourceID AND t1.MemberID = ".$aMemberID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$anInstance = BITAMDataSourceMember::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aDataSourceID = (int)$aRS->fields["datasourceid"];
		
		$anInstance = BITAMDataSourceMember::NewInstance($aRepository, $aDataSourceID);
		$anInstance->DataSourceName = $aRS->fields["datasourcename"];
		$anInstance->TableName = $aRS->fields["tablename"];
		$anInstance->MemberID = (int)$aRS->fields["memberid"];
		$anInstance->MemberName = $aRS->fields["membername"];
		$anInstance->MemberOrder = (int) $aRS->fields["memberorder"];
		$anInstance->FieldName = $aRS->fields["fieldname"];
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		$anInstance->eBavelFieldID = (int) @$aRS->fields["ebavelfieldid"];
		//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		$anInstance->eBavelFieldDataID = (int) @$aRS->fields["ebavelfielddataid"];
		//@JAPR
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$anInstance->KeyOfAgenda = (int) @$aRS->fields["keyofagenda"];
			$anInstance->AgendaLatitude = (int) @$aRS->fields["agendalatitude"];
			$anInstance->AgendaLongitude = (int) @$aRS->fields["agendalongitude"];
			$anInstance->AgendaDisplay = (int) @$aRS->fields["agendadisplay"];
		}
		//@JAPR 2014-12-09: Agregado el soporte de imagenes en los atributos de los catálogo
		$anInstance->IsImage = (int) @$aRS->fields["isimage"];
		//@JAPR
		
		BITAMGlobalFormsInstance::AddDataMemberInstanceWithID($anInstance->MemberID, $anInstance);
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$aDataSourceID = getParamValue('DataSourceID', 'both', '(int)');
		if (array_key_exists("MemberID", $aHTTPRequest->POST))
		{
			$aMemberID = $aHTTPRequest->POST["MemberID"];
			
			if (is_array($aMemberID))
			{
				$aCollection = BITAMDataSourceMemberCollection::NewInstance($aRepository, $aDataSourceID, $aMemberID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove(true);
				}
				$aCollection = BITAMDataSourceMemberCollection::NewInstance($aRepository, $aDataSourceID);
				$newOrder = 1;
				foreach ($aCollection as $anInstanceMember) {
					$sql = "UPDATE SI_SV_DataSourceMember SET MemberOrder = ".$newOrder." WHERE MemberID = ".$anInstanceMember->MemberID." AND DataSourceID = ".$aDataSourceID;
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					$newOrder++;
				}

				$anInstance = BITAMDataSourceMember::NewInstance($aRepository, $aDataSourceID);

				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				$anInstance = BITAMDataSourceMember::NewInstanceWithID($aRepository, (int)$aMemberID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMDataSourceMember::NewInstance($aRepository, $aDataSourceID);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();

				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}

				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMDataSourceMember::NewInstance($aRepository, $aDataSourceID);
					}
				}

				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("MemberID", $aHTTPRequest->GET))
		{
			$aMemberID = $aHTTPRequest->GET["MemberID"];
			$anInstance = BITAMDataSourceMember::NewInstanceWithID($aRepository, (int)$aMemberID);

			if (is_null($anInstance))
			{
				$anInstance = BITAMDataSourceMember::NewInstance($aRepository, $aDataSourceID);
			}
		}
		else
		{
			$anInstance = BITAMDataSourceMember::NewInstance($aRepository, $aDataSourceID);
		}

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("MemberID", $anArray))
		{
			$this->MemberID = (int)$anArray["MemberID"];
		}
		
		if (array_key_exists("MemberOrder", $anArray))
		{
			$this->MemberOrder = (int)$anArray["MemberOrder"];
		}
		
		if (array_key_exists("DataSourceID", $anArray))
		{
			$this->DataSourceID = (int)$anArray["DataSourceID"];
		}

 		if (array_key_exists("MemberName", $anArray))
		{
			$this->MemberName = rtrim($anArray["MemberName"]);
		}

		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (array_key_exists("eBavelFieldID", $anArray))
		{
			$this->eBavelFieldID = (int) $anArray["eBavelFieldID"];
		}
		
		//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		if (array_key_exists("eBavelFieldDataID", $anArray))
		{
			$this->eBavelFieldDataID = (int) $anArray["eBavelFieldDataID"];
		}
		//@JAPR
		
		if (getMDVersion() >= esvAgendaRedesign) {
			if (array_key_exists("KeyOfAgenda", $anArray)) {
				$this->KeyOfAgenda = (int) $anArray["KeyOfAgenda"];
			}
			if (array_key_exists("AgendaLatitude", $anArray)) {
				$this->AgendaLatitude = (int) $anArray["AgendaLatitude"];
			}
			if (array_key_exists("AgendaLongitude", $anArray)) {
				$this->AgendaLongitude = (int) $anArray["AgendaLongitude"];
			}
			if (array_key_exists("AgendaDisplay", $anArray)) {
				$this->AgendaDisplay = (int) $anArray["AgendaDisplay"];
			}
		}
		
		if (array_key_exists("IsImage", $anArray)) {
			$this->IsImage = (int) $anArray["IsImage"];
		}
		
		return $this;
	}
	
	function save() {
		$blnNewObject = $this->isNewObject();
		if ($this->isNewObject()) {
			//Obtener el MemberID
	    	$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(MemberID)", "0")." + 1 AS MemberID".
						" FROM SI_SV_DataSourceMember";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$this->MemberID = (int) $aRS->fields["memberid"];
	    	$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(MemberOrder)", "0")." + 1 AS MemberOrder".
						" FROM SI_SV_DataSourceMember where DataSourceID = ".$this->DataSourceID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->MemberOrder = (int) $aRS->fields["memberorder"];
			unset ($aRS);
			
			//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
			if (getMDVersion() >= esveBavelCatalogs) {
				$strAdditionalFields .= ', eBavelFieldID';
				$strAdditionalValues .= ', '.$this->eBavelFieldID;
			}
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalFields .= ', KeyOfAgenda';
				$strAdditionalValues .= ', '.$this->KeyOfAgenda;
				$strAdditionalFields .= ', AgendaLatitude';
				$strAdditionalValues .= ', '.$this->AgendaLatitude;
				$strAdditionalFields .= ', AgendaLongitude';
				$strAdditionalValues .= ', '.$this->AgendaLongitude;
				$strAdditionalFields .= ', AgendaDisplay';
				$strAdditionalValues .= ', '.$this->AgendaDisplay;
			}
			//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			if (getMDVersion() >= esveBavelGeolocationAttribs) {
				$strAdditionalFields .= ', eBavelFieldDataID';
				$strAdditionalValues .= ', '.$this->eBavelFieldDataID;
			}
			if (getMDVersion() >= esvCatalogAttributeImage) {
				//@JAPR 2015-12-15: Corregido un bug, habían deshabilitado la carga de este campo, así que la corrección de este Issue no había funcionado (#SS2EKF)
				$strAdditionalFields .= ', IsImage';
				$strAdditionalValues .= ', '.$this->IsImage;
			}
			$sql = "INSERT INTO SI_SV_DataSourceMember (".
					"DataSourceID".
					",MemberOrder".
					",MemberID".
					",MemberName".
					$strAdditionalFields.
					") VALUES (".
					$this->DataSourceID.
					",".$this->MemberOrder.
					",".$this->MemberID.
					",".$this->Repository->DataADOConnection->Quote($this->MemberName).
					$strAdditionalValues.
					")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else {
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
			$strAdditionalFields = '';
			if (getMDVersion() >= esveBavelCatalogs) {
				$strAdditionalFields .= ", eBavelFieldID = ".$this->eBavelFieldID;
			}
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalFields .= ", KeyOfAgenda = ".$this->KeyOfAgenda;
				$strAdditionalFields .= ", AgendaLatitude = ".$this->AgendaLatitude;
				$strAdditionalFields .= ", AgendaLongitude = ".$this->AgendaLongitude;
				$strAdditionalFields .= ", AgendaDisplay = ".$this->AgendaDisplay;
			}
			//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			if (getMDVersion() >= esveBavelGeolocationAttribs) {
				$strAdditionalFields .= ", eBavelFieldDataID = ".$this->eBavelFieldDataID;
			}
			if (getMDVersion() >= esvCatalogAttributeImage) {
				//@JAPR 2015-12-15: Corregido un bug, habían deshabilitado la carga de este campo, así que la corrección de este Issue no había funcionado (#SS2EKF)
				$strAdditionalFields .= ", IsImage = ".$this->IsImage;
			}
			
			$sql = "UPDATE SI_SV_DataSourceMember SET 
					MemberName = ".$this->Repository->DataADOConnection->Quote($this->MemberName)." 
					, IsImage = {$this->IsImage} 
				WHERE MemberID = ".$this->MemberID." AND DataSourceID = ".$this->DataSourceID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//@JAPR 2015-07-26: Agregada la eliminación de los catálogos internos de las preguntas cuando se elimina la pregunta o el DataSource
		//Invoca a la actualización de definiciones en cualquier punto donde se requiera, en este caso sincronizar los nombres y agrega/elimina atributos
		$objDataSource = BITAMDataSource::NewInstanceWithID($this->Repository, $this->DataSourceID);
		if (!is_null($objDataSource)) {
			$objDataSource->updateAllCatalogDefinitions(!$blnNewObject, $blnNewObject, $blnNewObject);
		}
		
		//Realizamos actualizacion de LastDate en las encuestas relacionadas con este catalogo
		//@JAPR 2015-07-21: Se remueve de este punto porque al final se invocará a la nueva función para actualizar las dependencias
		//BITAMSurvey::updateAllSurveyLastUserAndDateFromCatalog($this->Repository, $this->DataSourceID);
		//Actualiza la versión del DataSource
		//@JAPRWarning: Cuando se haga correctamente, este proceso debe invocar al save del DataSource ya que ahí se actualizarán las instancias de catálogos
		//@JAPR 2016-06-22: Corregidos bugs, no habían programado bien esta clase pues realmente nunca la usaron para los catálogos de v6 sino que reprogramaron todos los acceso a metadata
		BITAMDataSource::updateLastUserAndDateInDataSource($this->Repository, $this->DataSourceID);
		
		return $this;
	}
	
	//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
	/* Crea/actualiza el atributo de catálogo indicado utilizando la definición de este objeto ya sea por ID o como instancia, si no se especifica uno entonces crea uno nuevo
	Regresa la instancia del atributo de catálogo creado/actualizado
	Si se especifica el parámetro $iNewPos, quiere decir que en lugar de utilizar la posición de este DataSourceMemer se usará dicho parámetro como posición
	*/
	function createUpdateCatalogMember($aCatalogID, $aCatMemberID = 0, $oCatMember = null, $iNewPos = -1) {
		$objCatalogMember = null;
		if ($aCatMemberID <= 0 && is_null($oCatMember)) {
			//En este caso crea un nuevo atributo de catálogo
			$objCatalogMember = BITAMCatalogMember::NewInstance($this->Repository, $aCatalogID);
			if (is_null($objCatalogMember)) {
				return $objCatalogMember;
			}
		}
		else {
			if (!is_null($oCatMember)) {
				$objCatalogMember = $oCatMember;
			}
			else {
				$objCatalogMember = BITAMCatalogMember::NewInstanceWithID($this->Repository, $aCatMemberID);
				if (is_null($objCatalogMember)) {
					return $objCatalogMember;
				}
			}
		}
		
		//Actualiza la definición del catálogo
		$objCatalogMember->DataSourceMemberID = $this->MemberID;
		$objCatalogMember->MemberName = $this->MemberName;
		$objCatalogMember->MemberOrder = ($iNewPos >= 0)?$iNewPos:$this->MemberOrder;
		$objCatalogMember->KeyOfAgenda = $this->KeyOfAgenda;
		$objCatalogMember->AgendaLatitude = $this->AgendaLatitude;
		$objCatalogMember->AgendaLongitude = $this->AgendaLongitude;
		$objCatalogMember->AgendaDisplay = $this->AgendaDisplay;
		//@JAPR 2015-11-05: Corregido un bug, al crear el Catalog a partir del DataSource, no estaba heredando la propiedad de si era o no atributo imagen (#SS2EKF)
		$objCatalogMember->IsImage = $this->IsImage;
		//@JAPR 2015-12-07: Corregido un bug, al crear el Catálogo a partir de un DataSource, no estaba asignando el mapeo hacia eBavel (#ASIPRG)
		$objCatalogMember->eBavelFieldID = $this->eBavelFieldID;
		$objCatalogMember->eBavelFieldDataID = $this->eBavelFieldDataID;
		//@JAPR
		if (!$objCatalogMember->save()) {
			return $null;
		}
		
		return $objCatalogMember;
	}
	
	function remove ($fromCollection = null) {
		//@JAPR 2015-07-26: Agregada la eliminación de los catálogos internos de las preguntas cuando se elimina la pregunta o el DataSource
		//Elimina en cascada todos los atributos de catálogos que son derivados de este DataSourceMember
		$arrCatMemberIDs = array();
		$sql = "SELECT MemberID 
			FROM SI_SV_CatalogMember 
			WHERE DataSourceMemberID = {$this->MemberID}";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while(!$aRS->EOF) {
				$intCatMemberID = (int) @$aRS->fields["memberid"];
				$arrCatMemberIDs[] = $intCatMemberID;
				$aRS->MoveNext();
			}
			
			foreach ($arrCatMemberIDs as $intCatMemberID) {
				$objCatalogMember = BITAMCatalogMember::NewInstanceWithID($this->Repository, $intCatMemberID);
				if (is_null($objCatalogMember) || $objCatalogMember->CreationAdminVersion < esveFormsv6) {
					continue;
				}
				
				$objCatalogMember->remove();
			}
		}
		
		//Al eliminar un catálogo se deben actualizar todas las preguntas que lo utilizan, cuando se trata de la versión 6+ este método se invoca desde el 
		//DataSource y en realidad sólo debería estar asociado a una única pregunta (con preguntas tipo GetData se podría enlazar con otras preguntas adicionales)
		//pero es necesario desligarlas en la metadata
		$sql = "UPDATE SI_SV_Section SET 
				DataSourceMemberID = 0 
			WHERE DataSourceMemberID = {$this->MemberID}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql = "UPDATE SI_SV_Question SET 
				DataSourceMemberID = 0 
			WHERE DataSourceMemberID = {$this->MemberID}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql = "UPDATE SI_SV_Question SET 
				DataMemberLatitudeID = 0 
			WHERE DataMemberLatitudeID = {$this->MemberID}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql = "UPDATE SI_SV_Question SET 
				DataMemberLongitudeID = 0 
			WHERE DataMemberLongitudeID = {$this->MemberID}";
		$this->Repository->DataADOConnection->Execute($sql);
		//@JAPR
		
		$sql = "ALTER TABLE {$this->TableName} DROP COLUMN {$this->FieldName}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$memberOrder = $this->MemberOrder;
		$intDataSourceID = $this->DataSourceID;
		$sql = "DELETE FROM SI_SV_DataSourceMember WHERE MemberID = ".$this->MemberID." AND DataSourceID = ".$this->DataSourceID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (is_null($fromCollection)) {
			$sql = "UPDATE SI_SV_DataSourceMember SET MemberOrder = MemberOrder - 1 WHERE MemberOrder > ".$memberOrder." AND DataSourceID = ".$intDataSourceID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false) {
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		return $this;
	}

	function isNewObject()
	{
		return ($this->MemberID <= 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Attribute");
		}
		else
		{
			return translate("Attribute")." ".$this->MemberName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=DataSourceMember&DataSourceID=".$this->DataSourceID;
		}
		else
		{
			return "BITAM_PAGE=DataSourceMember&DataSourceID=".$this->DataSourceID."&MemberID=".$this->MemberID;
		}
	}

	function get_Parent()
	{
		return BITAMDataSource::NewInstanceWithID($this->Repository, $this->DataSourceID);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'MemberID';
	}

	function get_FormFieldName() {
		return 'MemberName';
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return true;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->get_Parent()->canRemove($aUser);
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MemberName";
		$aField->Title = translate("Attribute");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->DataSourceID);
			
			require_once('eBavelIntegration.inc.php');

            /*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel */
            if (getMDVersion() >= esvRedesign) {
	            //Si hemos seleccionado una Forma entonces pedimos sus atributos
	            if($objCatalog->eBavelFormType == ebftForms)
	                $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Forms');        
	            else//Si hemos seleccionado una Vista entonces solicitamos sus atributos.
	                $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Views');

            } //@AAL
            else
            	$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');         
            
			$arrAllEBavelFormsFields = array(0 => '('.translate("None").')');
			//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			$arrAllEBavelFormsFieldsData = array(0 => array(0 => '('.translate("None").')'));
			//@JAPR
			foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
				foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
					if ($intFieldID > 0) {
						$arrAllEBavelFormsFields[$intFieldID] = $arrFieldData['label'];
						
						//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						$inteBavelQTypeID = convertEBavelToEFormsFieldType($arrFieldData['tagname']);
						$arrAllEBavelFormsFieldsData[$intFieldID] = array(ebfdValue => translate("Value"));
						switch ($inteBavelQTypeID) {
							case qtpLocation:
								//En los campos tipo Geolocalización, se agregarán las opciones para utilizar el valor, la latitud o la longitud
								$arrAllEBavelFormsFieldsData[$intFieldID][ebfdLatitude] = translate("Latitude");
								$arrAllEBavelFormsFieldsData[$intFieldID][ebfdLongitude] = translate("Longitude");
								break;
							default:
								//El resto de los campos sólo agrega el valor para forzar a que sea la única opción posible a usar
								break;
						}
						//@JAPR
					}
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "eBavelFieldID";
			$aField->Title = translate("eBavel form field");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrAllEBavelFormsFields;
			$aField->OnChange = "showHideOptions();";
			$myFields[$aField->Name] = $aField;
			$eBavelFieldsField = $aField;
			
			//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			if (getMDVersion() >= esveBavelGeolocationAttribs) {
				$eBavelFieldsField->CloseCell = false;
				
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "eBavelFieldDataID";
				$aField->Title = "DataID";
				$aField->Type = "Object";
				$aField->VisualComponent = "Combobox";
				$aField->Options = $arrAllEBavelFormsFieldsData;
				//Con el valor FixedWidth=0 se forzará a respetar el tamaño natural del combo box sin ajustarlo automáticamente a 200px
				//como lo hace el framework cuando es menor que dicho tamaño, esto porque este campo está oculto por default así que la manera de revisar
				//su tamaño provocaba que si se considerara menor que 0px debido a eso
				$aField->FixedWidth = "0";
				$myFields[$aField->Name] = $aField;
				$eBavelFieldDataIDField = $aField;
				
				$eBavelFieldDataIDField->Parent = $eBavelFieldsField;
				$eBavelFieldsField->Children[] = $eBavelFieldDataIDField;
			}
			//@JAPR
		}
		//@JAPR
		
		if(getMDVersion() >= esvAgendaRedesign) {
			$this->get_FormField_KeyOfAgenda($myFields);
			$this->get_FormField_AgendaLatitude($myFields);
			$this->get_FormField_AgendaLongitude($myFields);
			$this->get_FormField_AgendaDisplay($myFields);
		}
		
		if(getMDVersion() >= esvCatalogAttributeImage) {
			$this->get_FormField_IsImage($myFields);
		}
		
		return $myFields;
	}
	
	function get_Field ($name, $title, $type, $options = null) {
		$aField = BITAMFormField::NewFormField();
		$aField->Name = $name;
		$aField->Title = translate($title);
		switch($type) {
			case 'Checkbox':
				$aField->Type = "Boolean";
				$aField->VisualComponent = $type;
				break;
			case 'Combobox':
				$aField->Type = "Object";
				$aField->VisualComponent = $type;
				$aField->Options = ((isset($options['options']))? $options['options'] : array());
				break;
			case 'Integer':
				$aField->Type = $type;
				$aField->FormatMask = ((isset($options['formatMask']))? $options['formatMask'] : '');
				break;
			case 'String':
				$aField->Type = $type;
				$aField->Size = ((isset($options['size']))? $options['size'] : 255);
				break;
			default:
				$aField->Type = $type;
				$aField->Size = 255;
				break;
		}
		return $aField;
	}
		
	function get_FormField ($name, $title, $type, $deployableVersion) {
		$aField = null;
		if(deployable($deployableVersion)) {
			$aField = $this->get_Field($name, $title, $type);
		}
		return $aField;
	}
	
	function append_FormField (&$myFields, $aField) {
		if(!is_null($myFields)) {
			$myFields[$aField->Name] = $aField;
		}
	}
	
	function get_FormField_AgendaRedesign ($name, $title, $type, $deployableVersion, &$myFields = null) {
		$aField = $this->get_FormField($name, $title, $type, $deployableVersion);
		$aField->Options = $this->getRemainingCatalogAttributes(true);
		$this->append_FormField($myFields, $aField);
		return $aField;
	}
	
	function get_FormField_KeyOfAgenda (&$myFields = null) {
		$aField = $this->get_FormField_AgendaRedesign("KeyOfAgenda", "Key of Agenda", "Checkbox" , "esvAgendaRedesign", $myFields);
		//$aField->OnChange = "showHideChkValidation();";
		return $aField;
	}
	
	function get_FormField_AgendaLatitude (&$myFields = null) {
		$aField = $this->get_FormField_AgendaRedesign("AgendaLatitude", "Latitude of Agenda", "Checkbox" , "esvAgendaRedesign", $myFields);
		//$aField->OnChange = "showHideChkValidation();";
		return $aField;
	}
	
	function get_FormField_AgendaLongitude (&$myFields = null) {
		$aField = $this->get_FormField_AgendaRedesign("AgendaLongitude", "Longitude of Agenda", "Checkbox" , "esvAgendaRedesign", $myFields);
		//$aField->OnChange = "showHideChkValidation();";
		return $aField;
	}
	
	function get_FormField_AgendaDisplay (&$myFields = null) {
		$aField = $this->get_FormField_AgendaRedesign("AgendaDisplay", "Display of Agenda", "Checkbox" , "esvAgendaRedesign", $myFields);
		//$aField->OnChange = "showHideChkValidation();";
		return $aField;
	}
	
	function get_FormField_IsImage (&$myFields = null) {
		$aField = $this->get_FormField_AgendaRedesign("IsImage", "Is Image?", "Checkbox" , "esvCatalogAttributeImage", $myFields);
		//$aField->OnChange = "showHideChkValidation();";
		return $aField;
	}
	
	//@JAPR 2014-06-06: Agregado el refresh de catálogos dinámico
	//Regresa el nombre de variable asignado a este catálogo, el cual es un nombre entendible por el usuario basado en el nombre del atributo en sí
	//eliminando espacios en blanco, en caso de repetirse en el mismo catálogo, se agregará el número de atributo al nombre obtenido. Se usan los
	//primeros 10 caracteres o hasta encontrar el primer espacio en blanco. Estos nombres NO son con los que se graban, internamente se grabarán
	//los nombres usando el ID para permitir su reemplazo dinámicamente
	function getVariableName() {
		$strCatMemberVarName = trim($this->MemberName);
		$intPos = strpos($strCatMemberVarName, ' ');
		if ($intPos !== false) {
			//@JAPR 2014-08-28: Corregido un bug, se estaba dejando el espacio en blanco, ahora preferentemente agregará una segunda palabra cuando
			//se detecte un espacio en blanco, si es que hay un segundo espacio
			$intPos2 = strpos($strCatMemberVarName, ' ', $intPos +1);
			if ($intPos2 !== false) {
				$strCatMemberVarName = str_replace(' ', '', substr($strCatMemberVarName, 0, $intPos2));
			}
			else {
				$strCatMemberVarNamePrev = $strCatMemberVarName;
				$strCatMemberVarName = substr($strCatMemberVarName, 0, $intPos);
				//En este caso, agregará los siguientes 10 caracteres a partir del espacio
				$strCatMemberVarName .= trim(substr($strCatMemberVarNamePrev, $intPos +1, 10));
			}
			//@JAPR
		}
		else {
			$strCatMemberVarName = substr($strCatMemberVarName, 0, 10);
		}
		
		if ($strCatMemberVarName == '') {
			$strCatMemberVarName = 'Member'.$this->MemberOrder;
		}
		
		$strCatMemberVarName = "@Attr".str_replace("'", '', $strCatMemberVarName);
		
		return $strCatMemberVarName;
	}
	
	//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
	//Dado un ID de atributo, regresa el identificador interno con el que se graba el filterText de los filtros dinámicos por encuesta, que no es
	//el mismo nombre de variable que el usuario utilizará para definirlo
	function getAttribIDVariableName() {
		return '@DMID'.$this->MemberID;
	}
	
	//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas
	//Obtiene la lista de atributos del catálogo actual excluyendo al propio atributo de esta instancia
	//@JAPR 2012-08-06: Agregada la opción de ninguno con el parámetro $includeNone
	function getRemainingCatalogAttributes($includeNone = false)
	{
		$allValues = array();
		//@JAPR 2012-08-06: Agregada la opción de ninguno con el parámetro $includeNone
		if($includeNone==true)
		{
			$allValues[0] = translate("None");
		}
		//@JAPR
		
		$sql = "SELECT MemberID, MemberName 
			FROM SI_SV_DataSourceMember WHERE DataSourceID = ".$this->DataSourceID." AND MemberID <> ".$this->MemberID." 
			ORDER BY MemberOrder";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$memberID = (int)$aRS->fields["memberid"];
			$memberName = $aRS->fields["membername"];
			$allValues[$memberID] = $memberName;			
			$aRS->MoveNext();
		}
		
		return $allValues;
	}
	//@JAPR
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
?>
 		<script language="JavaScript">
		
		function getRemainingCatalogAtributes () {

		}		
		
		function verifyFieldsAndSave(target)
 		{
 			var strBlanksField = "";

			if(Trim(BITAMDataSourceMember_SaveForm.MemberName.value)=="")
			{
				strBlanksField += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Member Name"))?>';
			}
			
 			//En esta parte se verifica si el AttributeName ha sido modificado por valores validos
			var attributeNameOld = '<?=$this->MemberName?>';
			var memberID = <?=$this->MemberID?>;
			var attributeName = BITAMDataSourceMember_SaveForm.MemberName.value;

			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('<?=translate('XML Object not supported')?>');
				return(new Array);
			}

			xmlObj.open('POST', 'verifyAttributeName.php', false);
			xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
	 		xmlObj.send('ObjectID='+memberID+'&AttributeName='+encodeURIComponent(attributeName)+'&AttributeNameOld='+encodeURIComponent(attributeNameOld));

	 		if(xmlObj.status == 404)
			{
				alert('<?=translate('Unable to launch validation process')?>');
				return false;
			}

	 		strResponseData = Trim(unescape(xmlObj.responseText));

	 		arrResponseData = strResponseData.split('<SVSEP>');
	 		if(arrResponseData.length == 0)
	 		{
	 			alert('<?=translate('Error found during data validation')?>');
				return false;
	 		}

	 		if(arrResponseData[0]!="OK")
	 		{
				strErrMsg = arrResponseData[0];
				if(arrResponseData.length > 1)
				{
					strErrMsg = arrResponseData[1];
				}
				
  				strBlanksField += '\n- ' + '<?=translate('Error found during data validation')?>: ' + strErrMsg;
	 		}

			if(strBlanksField!="")
  			{
				alert('<?=translate("Please verify")?>:' + strBlanksField );
  			}
  			else
  			{
  				BITAMDataSourceMember_Ok(target);
  			}
 		}
 		
 		function showHideOptions() {
 			var intNumOptions = 0;
			var objCmb = BITAMDataSourceMember_SaveForm.eBavelFieldDataID;
			if (objCmb) {
	 			intNumOptions = parseInt(objCmb.options.length);
				objCmb.style.display = (intNumOptions && intNumOptions > 1)?((bIsIE)?"inline":"table-row"):"none";
			}
 		}
 		</script>
<?
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
?>
	 	<script language="JavaScript">
		objOkSelfButton = document.getElementById("BITAMDataSourceMember_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMDataSourceMember_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMDataSourceMember_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
		</script>
<?
	}
	
	//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
	//@JAPR 2014-10-28: Agregado el mapeo de Scores
	//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
	/* Regresa la colección de atributos indexada por su catálogo. Esta función se encuentra definida en múltiples clases, en el futuro se deberá
	optar por utilizar directamente esta versión ya que es lo correcto pues esta es la clase de Atributos que es lo que se está pidiendo cargar
	El parámetro $anArrayOfCatalogIDs representa la colección de catálogos que se deben filtrar, si no se recibe se asumirá que se obtendrán los atributos de
	todos los catálogos existentes
	*/
	static function GetCatMembersByCatalogID($aRepository, $anArrayOfCatalogIDs = null)
	{
		$filter = "";
		if (!is_null($anArrayOfCatalogIDs))
		{
			switch (count($anArrayOfCatalogIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " t1.DataSourceID = ".((int)$anArrayOfCatalogIDs[0]);
					break;
				default:
					foreach ($anArrayOfCatalogIDs as $aCatalogID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aCatalogID; 
					}
					if ($filter != "")
					{
						$filter = " t1.DataSourceID IN (".$filter.")";
					}
					break;
			}
		}
		if ($filter != '') {
			$filter = " WHERE ".$filter;
		}
		
		$sql = "SELECT t1.DataSourceID, t1.MemberID, t1.MemberName, t1.MemberOrder 
			FROM SI_SV_DataSourceMember t1 
			{$filter} 
			ORDER BY t1.DataSourceID, t1.MemberOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();
		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["datasourceid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["datasourceid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["datasourceid"];
					$aGroup = array();
				}
				
				$aGroup[$aRS->fields["memberid"]] = $aRS->fields["membername"];
  				$aRS->MoveNext();
			} while (!$aRS->EOF);
			$allValues[$lastID] = $aGroup;
		}
		
		return $allValues;
	}
	
	//@JAPR 2014-04-30: Agregado el reordenamiento de atributos de los catálogos
	//Obtiene la posición actual del atributo de catálogo especificado
	static function getCurrentOrderPos($aRepository, $aMemberID)
	{
		$sql = "SELECT MemberOrder FROM SI_SV_DataSourceMember WHERE MemberID = {$aMemberID}";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$orderPos = -1;
		if(!$aRS->EOF)
		{
			$orderPos = (int) @$aRS->fields["memberorder"];
		}
		
		return $orderPos;
	}
	//@JAPR
	
	//Necesaria para que el grabado con processRequest no genere error
	function getJSonDefinition()
	{
		$arrDef = array();
		$arrDef['id'] = $this->MemberID;
		$arrDef['name'] = $this->MemberName;
		//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
		$arrDef['isImage'] = $this->IsImage;
		return $arrDef;
	}
}

class BITAMDataSourceMemberCollection extends BITAMCollection
{
	public $DataSourceID;

	function __construct($aRepository, $aDataSourceID)
	{
		BITAMCollection::__construct($aRepository);
		$this->DataSourceID = $aDataSourceID;
		//@JAPR 2014-04-30: Agregado el reordenamiento de atributos de los catálogos
		//$this->OrderPosField = "MemberOrder";
		//@JAPR
	}
	
	static function NewInstanceEmpty($aRepository, $aDataSourceID)
	{
		return new BITAMDataSourceMemberCollection($aRepository, $aDataSourceID);
	}
	
	static function NewInstance($aRepository, $aDataSourceID, $anArrayOfMemberIDs = null)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		if (is_null($anArrayOfMemberIDs) || count($anArrayOfMemberIDs)<=0) {
			$anInstance =& BITAMGlobalFormsInstance::GetDataMembersCollByDataSourceID($aDataSourceID);
			if (!is_null($anInstance)) {
				return $anInstance;
			}
		}
		
		$anInstance = new BITAMDataSourceMemberCollection($aRepository, $aDataSourceID);

		$filter = "";

		if (!is_null($anArrayOfMemberIDs))
		{
			switch (count($anArrayOfMemberIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND t1.MemberID = ".((int)$anArrayOfMemberIDs[0]);
					break;
				default:
					foreach ($anArrayOfMemberIDs as $aMemberID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aMemberID; 
					}
					if ($filter != "")
					{
						$filter = " AND t1.MemberID IN (".$filter.")";
					}
					break;
			}
		}
		
		$strAdditionalFields = '';
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', t1.eBavelFieldID';
		}
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.KeyOfAgenda, t1.AgendaLatitude, t1.AgendaLongitude, t1.AgendaDisplay';
		}
		//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		if (getMDVersion() >= esveBavelGeolocationAttribs) {
			$strAdditionalFields .= ', t1.eBavelFieldDataID';
		}
		if (getMDVersion() >= esvCatalogAttributeImage) {
			//@JAPR 2015-12-15: Corregido un bug, habían deshabilitado la carga de este campo, así que la corrección de este Issue no había funcionado (#SS2EKF)
			$strAdditionalFields .= ', t1.IsImage';
		}
		$sql = "SELECT t1.DataSourceID, t2.DataSourceName, t2.TableName, t1.MemberID, t1.MemberName, t1.fieldName, t1.MemberOrder $strAdditionalFields 
				FROM SI_SV_DataSourceMember t1, SI_SV_DataSource t2 
				WHERE t1.DataSourceID = t2.DataSourceID AND t1.DataSourceID = ".$aDataSourceID.$filter." ORDER BY t1.MemberOrder";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMDataSourceMember::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}
		
		if (is_null($anArrayOfMemberIDs) || count($anArrayOfMemberIDs)<=0) {
			BITAMGlobalFormsInstance::AddDataMembersCollByDataSourceID($aDataSourceID, $anInstance);
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("DataSourceID", $aHTTPRequest->GET))
		{
			$aDataSourceID = (int) $aHTTPRequest->GET["DataSourceID"];
		}
		else
		{
			$aDataSourceID = 0;
		}
		
		//@JAPR 2014-04-30: Agregado el reordenamiento de atributos de los catálogos
		if(array_key_exists("MemberOrder", $aHTTPRequest->POST))
		{
			$orderPos = (int)$aHTTPRequest->POST["MemberOrder"];
			$aMemberID = (int)$aHTTPRequest->POST["MemberID"];
			BITAMDataSourceMemberCollection::Reorder($aRepository, $aDataSourceID, $aMemberID, $orderPos);
			$anInstance = BITAMDataSourceMember::NewInstance($aRepository, $aDataSourceID);
			$anInstance = $anInstance->get_Parent();
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		//@JAPR
		
		return BITAMDataSourceMemberCollection::NewInstance($aRepository, $aDataSourceID);
	}
		
	//@JAPR 2014-04-30: Agregado el reordenamiento de atributos de los catálogos
	static function Reorder($aRepository, $aDataSourceID, $aMemberID, $orderPos)
	{
 		$prevOrderPos = BITAMDataSourceMember::getCurrentOrderPos($aRepository, $aMemberID);
 		if ($orderPos > $prevOrderPos)
		{
			$sql = "UPDATE SI_SV_DataSourceMember SET MemberOrder = MemberOrder - 1 
					WHERE MemberOrder BETWEEN ".$prevOrderPos." AND ".$orderPos.' AND DataSourceID = '.$aDataSourceID;
			if ($aRepository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		if ($prevOrderPos > $orderPos)
		{
			$sql = "UPDATE SI_SV_DataSourceMember SET MemberOrder = MemberOrder + 1 
					WHERE MemberOrder BETWEEN ".$orderPos." AND ".$prevOrderPos.' AND DataSourceID = '.$aDataSourceID;
			if ($aRepository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		$sql = "UPDATE SI_SV_DataSourceMember SET MemberOrder = ".$orderPos." WHERE MemberID = ".$aMemberID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Realizamos actualizacion de LastDate en las encuestas relacionadas con este catalogo
		BITAMDataSource::updateLastUserAndDateInDataSource($aRepository, $aDataSourceID);
		
		return;
	}
	//@JAPR
	
	function get_Parent()
	{
		return BITAMDataSource::NewInstanceWithID($this->Repository, $this->DataSourceID);
	}

	function get_Title()
	{
		return translate("Attributes");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=DataSourceMemberCollection&DataSourceID=".$this->DataSourceID;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=DataSourceMember&DataSourceID=".$this->DataSourceID;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'MemberID';
	}
	
	function get_FormFieldName() {
		return 'MemberName';
	}

	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MemberName";
		$aField->Title = translate("Attribute");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-11-19: Agregados campos adicionales a la lista para facilitar la identificación de elementos
		$optionsGral = array();
		$optionsGral[0] = "";
		$optionsGral[1] = translate("Yes");
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "KeyOfAgenda";
		$aField->Title = translate("Key of Agenda");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsGral;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AgendaDisplay";
		$aField->Title = translate("Display of Agenda");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsGral;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
		if (getMDVersion() >= esvCatalogAttributeImage) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "IsImage";
			$aField->Title = translate("Image");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $optionsGral;
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->DataSourceID);
			
			require_once('eBavelIntegration.inc.php');
            /*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel */
      		if (getMDVersion() >= esvRedesign) {
	            //Si hemos seleccionado una Forma entonces pedimos sus atributos
	            if($objCatalog->eBavelFormType == ebftForms)
	                $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Forms');        
	            else//Si hemos seleccionado una Vista entonces solicitamos sus atributos.
	                $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Views');
      		}//@AAL
      		else
      			$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');           
            
			$arrAllEBavelFormsFields = array(0 => '('.translate("None").')');
			//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			$arrAllEBavelFormsFieldsData = array(0 => array(0 => '('.translate("None").')'));
			//@JAPR
			foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
				foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
					if ($intFieldID > 0) {
						$arrAllEBavelFormsFields[$intFieldID] = $arrFieldData['label'];
						
						//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						$inteBavelQTypeID = convertEBavelToEFormsFieldType($arrFieldData['tagname']);
						$arrAllEBavelFormsFieldsData[$intFieldID] = array(ebfdValue => translate("Value"));
						switch ($inteBavelQTypeID) {
							case qtpLocation:
								//En los campos tipo Geolocalización, se agregarán las opciones para utilizar el valor, la latitud o la longitud
								$arrAllEBavelFormsFieldsData[$intFieldID][ebfdLatitude] = translate("Latitude");
								$arrAllEBavelFormsFieldsData[$intFieldID][ebfdLongitude] = translate("Longitude");
								break;
							default:
								//El resto de los campos sólo agrega el valor para forzar a que sea la única opción posible a usar
								break;
						}
						//@JAPR
					}
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "eBavelFieldID";
			$aField->Title = translate("eBavel form field");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrAllEBavelFormsFields;
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->get_Parent()->canRemove($aUser);
	}
}
?>