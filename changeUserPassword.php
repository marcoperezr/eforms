<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);
	LoadLanguageWithName("EN");
}

$userID = -1;
if(array_key_exists("userID",$_GET))
{
	
	$userID = (int) $_GET["userID"];
}

//Otener el password actual del usuario
require_once("appuser.inc.php");
$passwordSaved = BITAMAppUser::getUserPassword($theRepository, $userID);
$titlePassword = translate("Password");
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?=$titlePassword?></title>
<link rel="stylesheet" type="text/css" href="css/default2.css">
<link rel="stylesheet" type="text/css" href="css/tree.css">
<link rel="stylesheet" type="text/css" href="css/main.css">

	<script language="JavaScript" type="text/javascript" src="js/browserSniffer.js"></script>
	<script language="JavaScript">
		var MONTH_NAMES = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
		var MONTH_ABBR = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		var DAY_NAMES = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
		var DAY_ABBR = new Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
		var MONTH_REGEXP = '(';
		for (var _i = MONTH_NAMES.length - 1; _i >= 0; _i--)
		{
			if (_i == MONTH_NAMES.length - 1)
			{
				MONTH_REGEXP += MONTH_NAMES[_i];
			}
			else
			{
				MONTH_REGEXP += ('|' + MONTH_NAMES[_i]);
			}
			MONTH_REGEXP += ('|' + MONTH_ABBR[_i]);
			if ((_i + 1) < 10)
			{
				MONTH_REGEXP += ('|0' + (_i + 1));
			}
			MONTH_REGEXP += ('|' + (_i + 1));
		}
		MONTH_REGEXP += ')?';
		
		var DECIMAL_SYMBOL = '.';
		var DIGIT_GROUPING_SYMBOL = ',';
	</script>
	<script language="JavaScript" type="text/javascript" src="js/calendar.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/main.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/autofill.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/common.js"></script>
	<script language="JavaScript" type="text/javascript" src="BITAMXMLObject.js"></script>

<script language="JavaScript">
function BITAMPassword__Ok()
{
<?
//La validación se realiza cuando el usuario ya existe y no cuando es usuario nuevo
if($userID!=-1)
{
?>
	var passwordSave = '<?=$passwordSaved?>'
	var currentPasswordStr = BITAMPassword_SaveForm.CurrentPassword._value;
	
	if(currentPasswordStr!=passwordSave)
	{
		alert('<?=translate("Either the user/password combination is incorrect or the account has been blocked")?>');
		return;
	}
<?
}
?>

	var newPasswordStr = BITAMPassword_SaveForm.NewPassword._value;
	var confirmationPasswordStr = BITAMPassword_SaveForm.ConfirmationPassword._value;

	if(newPasswordStr!=confirmationPasswordStr)
	{
		alert('<?=translate("The user password confirmation is invalid")?>');
		return;
	}
	
	window.returnValue = newPasswordStr;
	top.close();
}

function BITAMPassword__Cancel()
{
    window.returnValue = null;
    top.close();
}
</script>
</head>

<body style="background-color:#f5f5f5" >
<div class="collection_title"><?=$titlePassword?><hr></div>

<div class="object_buttons">
	<input type="image" title="Save" id="BITAMPassword_OkButton" class="object_okButton" src="images/ok.png" onclick="javascript: BITAMPassword__Ok();">
	<input type="image" title="Cancel" id="BITAMPassword__CancelButton" class="object_cancelButton" src="images/cancel.png" onclick="javascript: BITAMPassword__Cancel();">
</div>

<form id="BITAMPassword_SaveForm" >
<table class="object_table">
<?
if($userID!=-1)
{
?>
	<tr id="Row_CurrentPassword">
		<td class="object_field_title">
			<?=translate("Current")?>
		</td>
		<td colspan="3" class="object_field_value_3">
					<input type="password"
			        id=""
					name="CurrentPassword"
					size="16"
					maxlength="14"
					value=""
					_value=""
					onfocus="javascript:
								var v = formatString(this['_value'], 'N');
								this.value = v;
								if (this.createTextRange)
								{
									var r = this.createTextRange();
									r.moveEnd('character', v.length);
									r.select();
									}"
					onkeypress="javascript: event.returnValue = validateFormat(this, unformatStringRegExpStr('N'), event.keyCode);"
					onblur="javascript:
    							var v = unformatString(this.value, 'N');
    							this['_value'] = v;
								this.value = formatString(v, 'N');"
					>
		</td>
	</tr>
<?
}
?>	
	<tr id="Row_NewPassword">
		<td class="object_field_title">
			<?=translate("New")?>
		</td>
		<td colspan="3" class="object_field_value_3">
					<input type="password"
			        id=""
					name="NewPassword"
					size="16"
					maxlength="14"
					value=""
					_value=""
					onfocus="javascript:
								var v = formatString(this['_value'], 'N');
								this.value = v;
								if (this.createTextRange)
								{
									var r = this.createTextRange();
									r.moveEnd('character', v.length);
									r.select();
									}"
					onkeypress="javascript: event.returnValue = validateFormat(this, unformatStringRegExpStr('N'), event.keyCode);"
					onblur="javascript:
    							var v = unformatString(this.value, 'N');
    							this['_value'] = v;
								this.value = formatString(v, 'N');">
		</td>
	</tr>
	
	<tr id="Row_ConfirmationPassword">
		<td class="object_field_title">
			<?=translate("Confirmation")?>
		</td>
		<td colspan="3" class="object_field_value_3">
					<input type="password"
			        id=""
					name="ConfirmationPassword"
					size="16"
					maxlength="14"
					value=""
					_value=""
					onfocus="javascript:
								var v = formatString(this['_value'], 'N');
								this.value = v;
								if (this.createTextRange)
								{
									var r = this.createTextRange();
									r.moveEnd('character', v.length);
									r.select();
									}"
					onkeypress="javascript: event.returnValue = validateFormat(this, unformatStringRegExpStr('N'), event.keyCode);"
					onblur="javascript:
    							var v = unformatString(this.value, 'N');
    							this['_value'] = v;
								this.value = formatString(v, 'N');">
		</td>
	</tr>
</table>
</form>
</body>
</html>