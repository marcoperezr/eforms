<?
	
//var_dump($_POST);
//@JAPR 2013-02-27: Ajustada la funcionalidad de Reupload para v4
//Se puede enviar el usuario pero no el password, así que este será omitido en el PhP de v4 si viene el parámetro "recover"
$strUserName = (string) @$_GET['UserName'];
$strUserPassword = (string) @$_GET['Password'];
$strRepository = (string) @$_GET['Repository'];
$intDebugBreak = (int) @$_GET['DebugBreak'];

require_once('config.php');
require_once('utils.inc.php');

foreach ($_POST as $idxError => $errorFile) {
	global $directory;
	$directory = getcwd();
	$directory .= "\\syncerrors\\".$strRepository;
	//@JAPR 2013-02-27: Ajustada la funcionalidad de Reupload para v4
	echo("<br><b>".$errorFile."</b><br>");
	$strAjaxParams = "";
	/*
	$errorFileLines = file($directory."\\".$errorFile);
	//$first = true;
	foreach($errorFileLines as $idx => $line) {
		$arrayInfo = explode(" => ", $line);
		if(count($arrayInfo) == 2) {
			$key = substr($arrayInfo[0],5,(strlen($arrayInfo[0])-6));
			$value = substr($arrayInfo[1],0,(strlen($arrayInfo[1])-1));
			//echo("[".$key."]=".utf8_encode($value)."\n");
			$strAjaxParams = $strAjaxParams."&".$key."=".urlencode(utf8_encode($value));
			//$_POST[$key] = utf8_encode($value);
		}
	}
	*/

	global $blnReUpload;
	$blnReUpload = true;
	
	//Genera los parámetros para el restaurado, si no se puede identificar los datos del archivo o no se recibió usuario entonces no continua
	$arrFileData = unformatFileName($errorFile, $strUserName);
	if ($arrFileData === false || !is_array($arrFileData) || count($arrFileData) == 0) {
		continue;
	}
	
	$intAgendaID = (int) @$arrFileData['agendaID'];
	$intSurveyID = (int) @$arrFileData['surveyID'];
	$strSurveyDate = (string) @$arrFileData['fmtdate'];
	$strSurveyHour = (string) @$arrFileData['fmthour'];
	if ($intSurveyID <= 0 || trim($strSurveyDate) == '' || trim($strSurveyHour) == '') {
		continue;
	}
	$strAppVersion = ESURVEY_SERVICE_VERSION;
	
	//$_POST["debugsave"] = true;
	
	/*
	http://kpionline5.bitam.com/artus/genvi_test/ESurveyv4_test/eSurveyService.php?
	Ok	appVersion=4.02000
	Ok	&UserID=eformsv4@kpionline.com
	Ok	&Password=1001
	NA	&projectName=
	NA	&webMode=1
	Ok	&surveyID=3
	Ok	&agendaID=undefined
	Ok	&surveyDate=2013-02-25
	Ok	&surveyHour=18:38:06
	Ok	&action=uploadsyncdata
	Ok	&ftype=outbox
	Ok	&filename=surveyanswers_eformsv4@kpionline.com_20130225_183806_3_finished.dat
	Ok	&DateID=2013-02-25
	Ok	&HourID=18:38:06
	NA	&testurl=1
	NA	&DebugBreak=1
	*/
	//$strPost = "http://kpionline.bitam.com/artus/genvi_test/ESurveyV3/eSurveyService.php";
	$strPost = (((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/eSurveyService.php";
	$strPost = str_replace("\\", "/", $strPost);
	$strAjaxParams = "appVersion=$strAppVersion&UserID=$strUserName&Password=$strUserPassword&surveyID=$intSurveyID".
		"&surveyDate=$strSurveyDate&surveyHour=$strSurveyHour&action=uploadsyncdata&ftype=outbox".
		"&filename=$errorFile&DateID=$strSurveyDate&HourID=$strSurveyHour&testurl=1&recover=1";
	if ($intDebugBreak > 0) {
		$strAjaxParams .= "&DebugBreak=1";
	}
	if ($intAgendaID > 0) {
		$strAjaxParams .= "&agendaID=$intAgendaID";
	}
	//echo("<pre>");
	echo(str_repeat('*', 80)."<br>\r\n");
	echo("POST = ".$strPost."?".$strAjaxParams."<br>\r\n<br>\r\n");
	//echo("</pre><br>");
	
	$url = sprintf($strPost."?".$strAjaxParams);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	//Los parámetros en esta llamanda son GET
	//curl_setopt($ch, CURLOPT_POST,true);
	//curl_setopt($ch, CURLOPT_POSTFIELDS, $strAjaxParams);
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	$json_result = curl_exec($ch);
	curl_close($ch);
	if ($json_result[0] == '(') {
		$json_result = substr($json_result, 1);
	}
	$len = strlen($json_result);
	if ($json_result[$len - 1] == ';') {
		$json_result = substr($json_result, 0, $len - 1);
	}
	$len = strlen($json_result);
	if ($json_result[$len - 1] == ')') {
		$json_result = substr($json_result, 0, $len - 1);
	}
	//echo("<pre>");
	var_dump($json_result);
	$result = json_decode($json_result, true);
	echo("<br>\r\n");
	//echo("</pre>");
	
	if ($result == "Survey data was saved successfully.") {
		//Sólo agrega el _restored si no era un archivo ya previamente restaurado
		if (stripos($errorFile, "_restored.dat") === false) {
			$errorFileRename = str_replace(".dat", "_restored.dat", $errorFile);
			@rename($directory."\\".$errorFile, $directory."\\".$errorFileRename);
		}
	}
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <title>RECOVER UPLOAD ERROR</title>
		<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" charset="utf-8">
		</script>
	</head>
    <body onload="">
	</body>
</html>