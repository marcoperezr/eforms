<?php
//@JAPR 2014-08-07: Agregados los datos heredados desde eBavel para secciones Recap
/* Este archivo es el equivalente a getAttribValues, pero en lugar de aplicar a atributos de un catálogo, obtendrá los valores de campos
de eBavel directamente. Inicialmente sólo se necesitaba un distinct, pero se preparará para poder funcionar en base a prefiltrado de otros
campos ya que se basa en el otro script mencionado el cual si se comporta de esa manera
*/
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$eBavelFormID = 0;
if (array_key_exists("eBavelFormID", $_GET))
{
	$eBavelFormID = $_GET["eBavelFormID"];
}

//Este parámetro contiene la lista de campos que se desean regresar, puede ser un valor numérico o un Array si se quieren varios campos
$eBavelFieldID = 0;
if (array_key_exists("eBavelFieldID", $_GET))
{
	$eBavelFieldID = @$_GET["eBavelFieldID"];
	if (!is_null($eBavelFieldID) && !is_array($eBavelFieldID)) {
		$eBavelFieldID = (int) $eBavelFieldID;
	}
}
if (!is_array($eBavelFieldID)) {
	$eBavelFieldID = array($eBavelFieldID);
}

$changedFieldID = 0;
if (array_key_exists("ChangedFieldID", $_GET))
{
	$changedFieldID = $_GET["ChangedFieldID"];
}

//Este parámetro recibe los IDs de campos de eBavel que serán filtrados (valor de id_fieldform)
$strFieldIDs = "";
if (array_key_exists("strFieldIDs", $_GET))
{
	$strFieldIDs = (string) @$_GET["strFieldIDs"];
}

//Este parámetro recibe los valores aplicados a cada campo correspondiente a $strFieldIDs
$strValues = "";
if (array_key_exists("strValues", $_GET))
{
	$strValues = (string) @$_GET["strValues"];
}

//Con este parámetro se permitirá extraer siempre todos los valores del nivel pedido sin aplicar filtros en los padres, pero sin enviar sus 
//valores como 'sv_ALL_sv' sino simplemente no enviar nada de ellos
$intIgnoreFilters = 0;
if (array_key_exists("noFilters", $_GET))
{
	$intIgnoreFilters = (int) @$_GET["noFilters"];
}

$memberIDs = array();
$values = array();
$filterValues = array();
if (trim($strFieldIDs) != '') {
	$memberIDs = explode('_SVSEPSV_', $strFieldIDs);
	$values = explode('_SVSEPSV_', $strValues);
}

$numMemberIDs = count($memberIDs);
for($i=0; $i<$numMemberIDs; ++$i)
{
	$filterValues[$memberIDs[$i]] = $values[$i];
}

$intResultType = getParamValue('ResultType', 'both', '(int)');

require_once('eBavelIntegration.inc.php');
$strResult = GetEBavelFieldValues($theRepository, $eBavelFormID, $eBavelFieldID, $filterValues, $intResultType);
if ($intResultType == ebdtJSonForApps || $intResultType == ebdtJSonArrayForApps) {
	$appVersion = '';
	if (isset($_POST['appVersion'])) {
		$appVersion = (string) @$_POST['appVersion'];
	}
	elseif (isset($_GET['appVersion'])) {
		$appVersion = (string) @$_GET['appVersion'];
	}
	if (is_null($appVersion) || trim($appVersion) == '' || !is_numeric($appVersion)) {
		$appVersion = '1.0';
	}
	
	if ($intResultType == ebdtJSonArrayForApps) {
		PrintMultiArray($strResult);
	}
	else {
	    header("Content-Type: text/javascript");
		$bAddServiceFields = getParamValue('AddSrvFields', 'both', '(int)');
	    if ($bAddServiceFields) {
			$return['web_service_version'] = ESURVEY_SERVICE_VERSION;
			$return['request_version'] = $appVersion;
		    $return['error'] = false;
			$return['data'] = $strResult;
			$return = json_encode($return);
	    }
	    else {
	    	$return = $strResult;
	    }
	    
	    if (isset($_GET['jsonpCallback'])) {
	        echo($_GET['jsonpCallback'] . '(' . $return . ');');
	    } else {
			echo ($return);
	    }
	}
    return;
}
else {
	header('Content-Type: text/plain; charset=utf-8');
}

echo($strResult);

?>