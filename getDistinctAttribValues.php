<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$catalogID = "";

if (array_key_exists("CatalogID", $_GET))
{
	$catalogID = $_GET["CatalogID"];
}

$catMemberID = 0;

if (array_key_exists("CatMemberID", $_GET))
{
	$catMemberID = $_GET["CatMemberID"];
}

$changedMemberID = 0;

if (array_key_exists("ChangedMemberID", $_GET))
{
	$changedMemberID = $_GET["ChangedMemberID"];
}

$strMemberIDs="";
if (array_key_exists("strMemberIDs", $_GET))
{
	$strMemberIDs = $_GET["strMemberIDs"];
}

$strParentIDs="";
if (array_key_exists("strParentIDs", $_GET))
{
	$strParentIDs = $_GET["strParentIDs"];
}

$strValues="";
if (array_key_exists("strValues", $_GET))
{
	$strValues = $_GET["strValues"];
}

$aUserID = 0;
if (array_key_exists("userID", $_GET))
{
	$aUserID = (int) $_GET["userID"];
}
if (array_key_exists("userID", $_POST))
{
	$aUserID = (int) $_POST["userID"];
}

require_once("catalog.inc.php");
require_once("catalogmember.inc.php");
require_once("dimension.inc.php");
$aCatalogMember = BITAMCatalogMember::NewInstanceWithID($theRepository, $catMemberID);
$aDimension = null;
$strFilter = '';
if (!is_null($aCatalogMember))
{
	$aCatalog = BITAMCatalog::NewInstanceWithID($theRepository, $aCatalogMember->CatalogID);
	if (!is_null($aCatalog))
	{
		$strFilter = $aCatalog->generateResultFilterByUser($aUserID);
		//@JAPR 2012-05-30: Corregido un bug, no estaba enviando el filtro por usuario al método de carga de los valores del catálogo
		if (trim($strFilter) != '')
		{
			$strFilter = " WHERE ".$strFilter;
		}
		//@JAPR
	}
	$aDimension = BITAMDimension::NewInstanceWithDescripID($theRepository, $aCatalogMember->ClaDescrip, -1, false);
}

$attribValues = array();
$strResult="";
if (!is_null($aDimension))
{
	//Cambia el nombre de la llave para que apunte a la llave subrrogada del catálogo, ya que como es un DISTINCT para efectos de este caso
	//no importa cual valor de la subrrogada se espera, el que sea que regrese será capaz de obtener la descripción de esta dimensión
	//$aDimension->nom_fisicok = strtolower($aDimension->nom_tabla).'key';
	//$aDimension->nom_fisicok_bd = 'MAX('.$aDimension->nom_tabla.'KEY) AS '.$aDimension->nom_tabla.'KEY';
	//$aDimension->getDimensionValuesExt('', $aDimension->nom_fisico);
	//@JAPR 2012-05-30: Corregido un bug, no estaba enviando el filtro por usuario al método de carga de los valores del catálogo
	$aDimension->getDimensionValuesExt($strFilter);
	//@JAPR
	//Los valores están contenidos en el Array propio de la dimensión, así que eso es lo que se recorre
	$attribValues = $aDimension->values;
	if (isset($attribValues[$aDimension->NoApplyValue]))
	{
		unset($attribValues[$aDimension->NoApplyValue]);
	}
}

foreach ($attribValues as $valKey => $value) 
{
	if($strResult != "")
	{
		$strResult.= "_SV_VAL_";
	}
	
	$strResult .= $valKey."_SV_SEP_".$value;
}
//$strResult = implode("_SV_VAL_SV_", $attribValues);

header('Content-Type: text/plain; charset=utf-8');

echo($strResult);

?>