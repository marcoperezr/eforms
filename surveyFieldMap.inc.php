<?php

//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
/* Esta clase corresponde con el mapeo de acciones original, en el cual se tenían campos predefinidos y se configuraba a que campo de eBavel se
enviaría esa información, este tipo de mapeo ha quedado obsoleto y esta clase sólo se utiliza para obtener los nombres de los campos fijos, pero ahora
las nuevas clases (existentes en questionActionMap.inc.php, questionOptionActionMap.inc.php y surveyActionMap.inc.php) mapearán los campos de eBavel
de la forma seleccionada hacia preguntas o datos fijos de eForms
*/

require_once("repository.inc.php");
require_once("initialize.php");

class BITAMSurveyFieldMap extends BITAMObject
{
	public $FieldMapID;						//Identificador único de la tabla
	public $SurveyID;						//ID de la encuesta para la que se está configurando la columna
	public $SurveyFieldID;					//ID que identifica el tipo de columna de la que se trata (es el mismo ID entre todas las encuestas para el mismo tipo de columna, ej. DateID == 1, HourID == 2, etc.)
	public $SurveyFieldText;				//Etiqueta para desplegar durante la configuración con el tipo de columna 
	public $eBavelFieldID;					//ID de FieldForm de eBavel al que se mapea esta columna
	public $eBavelAppID;					//ID de la aplicación de eBavel de la que se extraerán los campos para usar en el mapeo
	public $eBavelFormID;					//ID de la forma de eBavel de la que se extraerán los campos para usar en el mapeo
	public $ArrayVariables;					//Contenido de la tabla cuando se genera la instancia, utilizado para crear dinámicamente las propiedades cuando se carga como instancia para la página de configuración
	public $ArrayVariablesKeys;
	public $ArrayVariableValues;
	public $ArrayVariablesExist;
	public $StyleCellTitle;
	
  	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->FieldMapID = -1;
		$this->SurveyID = -1;
		$this->SurveyFieldID = -1;
		$this->eBavelFieldID = -1;
		$this->eBavelAppID = -1;
		$this->eBavelFormID = -1;
		$this->ArrayVariables = array();
		$this->ArrayVariablesKeys = array();
		$this->ArrayVariableValues = array();
		$this->ArrayVariablesExist = array();
		$this->StyleCellTitle = 'style="width:40%"';
	}

	static function NewInstance($aRepository)
	{
		return new BITAMSurveyFieldMap($aRepository);
	}

	static function NewInstanceWithID($aRepository, $aFieldMapID)
	{
		$anInstance = null;
		
		if (((int) $aFieldMapID) < 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT FieldMapID, SurveyID, SurveyFieldID, eBavelFieldID FROM SI_SV_SurveyFieldsMap WHERE FieldMapID = ".((int) $aFieldMapID) ;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMSurveyFieldMap::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMSurveyFieldMap::NewInstance($aRepository);
		$anInstance->FieldMapID = (int) @$aRS->fields["fieldmapid"];
		$anInstance->SurveyID = (int) @$aRS->fields["surveyid"];
		$anInstance->SurveyFieldID = (int) @$aRS->fields["surveyfieldid"];
		$anInstance->eBavelFieldID = (int) @$aRS->fields["ebavelfieldid"];
		$anInstance->SurveyFieldText = BITAMSurveyFieldMap::GetSurveyColumnText($this->SurveyFieldID);
		return $anInstance;
	}
	
	//Regresa la etiqueta correspondiente a la columna indicada
	static function GetSurveyColumnText($aSurveyFieldID) {
		$aSurveyFieldID = (int) $aSurveyFieldID;
		
		$strSurveyFieldText = '';
		switch ($aSurveyFieldID) {
			case sfidStartDate:
				$strSurveyFieldText = translate('Start entry date in app');
				break;
			case sfidStartHour:
				$strSurveyFieldText = translate('Start entry time in app');
				break;
			case sfidEndDate:
				$strSurveyFieldText = translate('End entry date in app');
				break;
			case sfidEndHour:
				$strSurveyFieldText = translate('End entry time in app');
				break;
			case sfidLocation:
				$strSurveyFieldText = translate('GPS location');
				break;
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			case sfidLocationAcc:
				$strSurveyFieldText = translate('GPS accuracy');
				break;
			//@JAPR
			case sfidSyncDate:
				$strSurveyFieldText = translate('Synchronization date');
				break;
			case sfidSyncHour:
				$strSurveyFieldText = translate('Synchronization time');
				break;
			case sfidServerStartDate:
				$strSurveyFieldText = translate('Start entry date in server');
				break;
			case sfidServerStartHour:
				$strSurveyFieldText = translate('Start entry time in server');
				break;
			case sfidServerEndDate:
				$strSurveyFieldText = translate('End entry date in server');
				break;
			case sfidServerEndHour:
				$strSurveyFieldText = translate('End entry time in server');
				break;
			case sfidScheduler:
				$strSurveyFieldText = translate('Scheduler');
				break;
			case sfidEMail:
				$strSurveyFieldText = translate('Entry EMail (EMail surveys only)');
				break;
			case sfidDuration:
				$strSurveyFieldText = translate('Entry duration');
				break;
			case sfidAnsweredQuestions:
				$strSurveyFieldText = translate('Number of answered questions');
				break;
			case sfidEntrySection:
				$strSurveyFieldText = translate('Record section id');
				break;
			case sfidDynamicPage:
				$strSurveyFieldText = translate('Dynamic page description');
				break;
			case sfidDynamicOption:
				$strSurveyFieldText = translate('Dynamic checkbox description');
				break;
			case sfidFactKey:
				$strSurveyFieldText = translate('Unique record id');
				break;
			case sfidEntryID:
				$strSurveyFieldText = translate('Report id');
				break;
			case sfidUserID:
				$strSurveyFieldText = translate('User id');
				break;
			case sfidUserName:
				$strSurveyFieldText = translate('User Name');
				break;
			//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
			case sfidGPSCountry:
				$strSurveyFieldText = translate('GPS country');
				break;
			case sfidGPSState:
				$strSurveyFieldText = translate('GPS state');
				break;
			case sfidGPSCity:
				$strSurveyFieldText = translate('GPS city');
				break;
			case sfidGPSAddress:
				$strSurveyFieldText = translate('GPS address');
				break;
			case sfidGPSZipCode:
				$strSurveyFieldText = translate('GPS zip code');
				break;
			case sfidGPSFullAddress:
				$strSurveyFieldText = translate('GPS full address');
				break;
			//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
			case sfidFixedStrExpression:
				$strSurveyFieldText = translate('String constant value');
				break;
			case sfidFixedNumExpression:
				$strSurveyFieldText = translate('Numeric constant value');
				break;
			case sfidSurveyScore:
				$strSurveyFieldText = translate('Entry score');
				break;
			case sfidCatalogAttribute:
				$strSurveyFieldText = translate('Attribute');
				break;
			//@JAPR
			default:
				$strSurveyFieldText = translate('Unknown');
				break;
		}
		
		return $strSurveyFieldText;
	}
	
	//Regresa el nombre identificador de la columna para permitir la captura usando el Framework. Si regresa vacio entonces se trata de una columna
	//no soportada en esta versión
	static function GetSurveyColumnName($aSurveyFieldID) {
		$aSurveyFieldID = (int) $aSurveyFieldID;
		
		$strSurveyFieldText = '';
		switch ($aSurveyFieldID) {
			case sfidStartDate:
				$strSurveyFieldText = 'FldStartDate';
				break;
			case sfidStartHour:
				$strSurveyFieldText = 'FldStartHour';
				break;
			case sfidEndDate:
				$strSurveyFieldText = 'FldEndDate';
				break;
			case sfidEndHour:
				$strSurveyFieldText = 'FldEndHour';
				break;
			case sfidLocation:
				$strSurveyFieldText = 'FldLocation';
				break;
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			case sfidLocationAcc:
				$strSurveyFieldText = 'FldLocationAcc';
				break;
			//@JAPR
			case sfidSyncDate:
				$strSurveyFieldText = 'FldSyncDate';
				break;
			case sfidSyncHour:
				$strSurveyFieldText = 'FldSyncHour';
				break;
			case sfidServerStartDate:
				$strSurveyFieldText = 'FldServerStartDate';
				break;
			case sfidServerStartHour:
				$strSurveyFieldText = 'FldServerStartHour';
				break;
			case sfidServerEndDate:
				$strSurveyFieldText = 'FldServerEndDate';
				break;
			case sfidServerEndHour:
				$strSurveyFieldText = 'FldServerEndHour';
				break;
			case sfidScheduler:
				$strSurveyFieldText = 'FldScheduler';
				break;
			case sfidEMail:
				$strSurveyFieldText = 'FldEMail';
				break;
			case sfidDuration:
				$strSurveyFieldText = 'FldDuration';
				break;
			case sfidAnsweredQuestions:
				$strSurveyFieldText = 'FldAnsweredQuestions';
				break;
			case sfidEntrySection:
				$strSurveyFieldText = 'FldEntrySection';
				break;
			case sfidDynamicPage:
				$strSurveyFieldText = 'FldDynamicPage';
				break;
			case sfidDynamicOption:
				$strSurveyFieldText = 'FldDynamicOption';
				break;
			case sfidFactKey:
				$strSurveyFieldText = 'FldFactKey';
				break;
			case sfidEntryID:
				$strSurveyFieldText = 'FldEntryID';
				break;
			case sfidUserID:
				$strSurveyFieldText = 'FldUserID';
				break;
			case sfidUserName:
				$strSurveyFieldText = 'FldUserName';
				break;
			//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
			case sfidGPSCountry:
				$strSurveyFieldText = 'FldGPSCountry';
				break;
			case sfidGPSState:
				$strSurveyFieldText = 'FldGPSState';
				break;
			case sfidGPSCity:
				$strSurveyFieldText = 'FldGPSCity';
				break;
			case sfidGPSAddress:
				$strSurveyFieldText = 'FldGPSAddress';
				break;
			case sfidGPSZipCode:
				$strSurveyFieldText = 'FldGPSZipCode';
				break;
			case sfidGPSFullAddress:
				$strSurveyFieldText = 'FldGPSFullAddress';
				break;
			//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
			case sfidFixedStrExpression:
				$strSurveyFieldText = 'FldFixedStringVal';
				break;
			case sfidFixedNumExpression:
				$strSurveyFieldText = 'FldFixedNumericVal';
				break;
			case sfidSurveyScore:
				$strSurveyFieldText = 'FldEntryScore';
				break;
			case sfidCatalogAttribute:
				$strSurveyFieldText = 'FldAttribute';
				break;
			//@JAPR
			default:
				$strSurveyFieldText = '';
				break;
		}
		
		return $strSurveyFieldText;
	}

	//Regresa id de la columna a partir del nombre identificador
	static function GetSurveyColumnID($aSurveyFieldName) {
		$aSurveyFieldName = strtolower(trim((string) $aSurveyFieldName));
		
		$intSurveyFieldID = 0;
		switch ($aSurveyFieldName) {
			case 'fldstartdate':
				$intSurveyFieldID = sfidStartDate;
				break;
			case 'fldstarthour':
				$intSurveyFieldID = sfidStartHour;
				break;
			case 'fldenddate':
				$intSurveyFieldID = sfidEndDate;
				break;
			case 'fldendhour':
				$intSurveyFieldID = sfidEndHour;
				break;
			case 'fldlocation':
				$intSurveyFieldID = sfidLocation;
				break;
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			case 'fldlocationAcc':
				$intSurveyFieldID = sfidLocationAcc;
				break;
			//@JAPR
			case 'fldsyncdate':
				$intSurveyFieldID = sfidSyncDate;
				break;
			case 'fldsynchour':
				$intSurveyFieldID = sfidSyncHour;
				break;
			case 'fldserverstartdate':
				$intSurveyFieldID = sfidServerStartDate;
				break;
			case 'fldserverstarthour':
				$intSurveyFieldID = sfidServerStartHour;
				break;
			case 'fldserverenddate':
				$intSurveyFieldID = sfidServerEndDate;
				break;
			case 'fldserverendhour':
				$intSurveyFieldID = sfidServerEndHour;
				break;
			case 'fldscheduler':
				$intSurveyFieldID = sfidScheduler;
				break;
			case 'fldeMail':
				$intSurveyFieldID = sfidEMail;
				break;
			case 'fldduration':
				$intSurveyFieldID = sfidDuration;
				break;
			case 'fldansweredquestions':
				$intSurveyFieldID = sfidAnsweredQuestions;
				break;
			case 'fldentrysection':
				$intSurveyFieldID = sfidEntrySection;
				break;
			case 'flddynamicpage':
				$intSurveyFieldID = sfidDynamicPage;
				break;
			case 'flddynamicoption':
				$intSurveyFieldID = sfidDynamicOption;
				break;
			case 'fldfactkey':
				$intSurveyFieldID = sfidFactKey;
				break;
			case 'fldentryid':
				$intSurveyFieldID = sfidEntryID;
				break;
			case 'flduserid':
				$intSurveyFieldID = sfidUserID;
				break;
			case 'fldusername':
				$intSurveyFieldID = sfidUserName;
				break;
			//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
			case 'fldgpscountry':
				$intSurveyFieldID = sfidGPSCountry;
				break;
			case 'fldgpsstate':
				$intSurveyFieldID = sfidGPSState;
				break;
			case 'fldgpscity':
				$intSurveyFieldID = sfidGPSCity;
				break;
			case 'fldgpsaddress':
				$intSurveyFieldID = sfidGPSAddress;
				break;
			case 'fldgpszipcode':
				$intSurveyFieldID = sfidGPSZipCode;
				break;
			case 'fldgpsfulladdress':
				$intSurveyFieldID = sfidGPSFullAddress;
				break;
			//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
			case 'fldfixedstringval':
				$intSurveyFieldID = sfidFixedStrExpression;
				break;
			case 'fldfixednumericval':
				$intSurveyFieldID = sfidFixedNumExpression;
				break;
			case 'fldentryscore':
				$intSurveyFieldID = sfidSurveyScore;
				break;
			case 'fldattribute':
				$intSurveyFieldID = sfidCatalogAttribute;
				break;
			//@JAPR
			default:
				break;
		}
		
		return $intSurveyFieldID;
	}
	
	//Llena la instancia de campos de eBavel con los valores default
	function resetDefaultSettings()
	{
		$this->ArrayVariables = array();
		$this->ArrayVariablesKeys = array();
		$this->ArrayVariableValues = array();
		$this->ArrayVariablesExist = array();
		//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
		//Agregados mas elementos, sin embargo algunos se pueden controlar mediante condiciones de Metadata para que sólo estén disponibles en
		//ciertas versiones
		//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
		for ($intSurveyFieldID = sfidStartDate; $intSurveyFieldID <= safidTitle; $intSurveyFieldID++) {	//sfidGPSFullAddress, sfidLocationAcc
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if ($intSurveyFieldID == sfidLocationAcc && getMDVersion() < esvAccuracyValues) {
				continue;
			}
			
			//@JAPR 2014-05-20: Corregido un bug, se le había dado el mismo valor que safidDescription así que estaba regresando mal el valor
			//Por el momento los IDs entre safidDescription y safidSourceID son exclusivos de las preguntas, así que no aplican en esta función que
			//es para encuestas
			//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
			if (($intSurveyFieldID >= safidDescription && $intSurveyFieldID <= safidSourceID) || $intSurveyFieldID >= safidTitle) {
				continue;
			}
			//@JAPR
			
			$this->ArrayVariablesKeys[$intSurveyFieldID] = 0;
			$this->ArrayVariables[$intSurveyFieldID] = BITAMSurveyFieldMap::GetSurveyColumnName($intSurveyFieldID);
			$this->ArrayVariableValues[$intSurveyFieldID] = 0;
			$this->ArrayVariablesExist[$intSurveyFieldID] = false;
		}
	}
	
	//Carga la configuración completa para una encuesta (esta es la instancia que se deberá usar para desplegar la ventana de configuración)
	static function NewInstanceWithSurvey($aRepository, $aSurveyID)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$anInstanceTemp = null;
		$sql = "SELECT FieldMapID, SurveyID, SurveyFieldID, eBavelFieldID FROM SI_SV_SurveyFieldsMap WHERE SurveyID = $aSurveyID Order By FieldMapID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		$anInstanceTemp = BITAMSurveyFieldMap::NewInstance($aRepository);
		$anInstanceTemp->SurveyID = $aSurveyID;
		$anInstanceTemp->resetDefaultSettings();
		$ArrayVariables = &$anInstanceTemp->ArrayVariables;
		$ArrayVariablesKeys = &$anInstanceTemp->ArrayVariablesKeys;
		$ArrayVariableValues = &$anInstanceTemp->ArrayVariableValues;
		$ArrayVariablesExist = array();
		
		while(!$aRS->EOF)
		{
			$intFieldMapID = (int) @$aRS->fields["fieldmapid"];
			$intSurveyID = (int) @$aRS->fields["surveyid"];
			$intSurveyFieldID = (int) @$aRS->fields["surveyfieldid"];
			$inteBavelFieldID = $aRS->fields["ebavelfieldid"];
			$strVariableName = BITAMSurveyFieldMap::GetSurveyColumnName($intSurveyFieldID);
			if ($strVariableName != '') {
				$ArrayVariables[$intSurveyFieldID] = $strVariableName;
				$ArrayVariablesKeys[$intSurveyFieldID] = $intFieldMapID;
				$ArrayVariableValues[$intSurveyFieldID] = $inteBavelFieldID;
				$ArrayVariablesExist[$intSurveyFieldID] = true;
			}
			$aRS->MoveNext();
		}
		
		$strtemp = 			'if (!class_exists("BITAMSurveyFieldMapExtended"))'."\n";
		$strtemp = $strtemp.'{'."\n";
		$strtemp = $strtemp.'	class BITAMSurveyFieldMapExtended extends BITAMSurveyFieldMap'."\n";
		$strtemp = $strtemp.'	{'."\n";
		foreach($ArrayVariables as $intSurveyFieldID => $strVariableName)
		{   
			$intVariableValue = (int) @$ArrayVariableValues[$intSurveyFieldID];
			$strtemp = $strtemp.'public $'.$strVariableName.' = '.$intVariableValue.';'."\n";
			
		}
		$strtemp = $strtemp.'	}'."\n";
		$strtemp = $strtemp.'}'."\n";
		//@JAPR 2012-05-18: Validación para evitar que invocar múltiples veces al SaveData en la misma sesión provoque que esta clase dinámica
		//sea redeclarada, de todas formas, las configuraciones dificilmente cambiarán durante la ejecución de una llamada de grabado como para 
		//que esto pudiera ser un problema
		$strtemp = addcslashes( $strtemp, '\\');
		eval($strtemp);
		$strtemp = "";
		//@JAPR
		$strtemp = $strtemp.'$theobject = new BITAMSurveyFieldMapExtended($aRepository);'."\n";
		$strtemp = addcslashes( $strtemp, '\\');
		eval($strtemp);
		$theobject->SurveyID = $aSurveyID;
		$theobject->ArrayVariables = $ArrayVariables;
		$theobject->ArrayVariablesKeys = $ArrayVariablesKeys;
		$theobject->ArrayVariableValues = $ArrayVariableValues;
		$theobject->ArrayVariablesExist = $ArrayVariablesExist;
		$theobject->IsDialog = true;
		
		return($theobject);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$inteBavelAppID = (int) @$aHTTPRequest->GET["eBavelAppID"];
		$inteBavelFormID = (int) @$aHTTPRequest->GET["eBavelFormID"];
		$aSurveyID = (int) @$aHTTPRequest->POST["SurveyID"];
		if ($aSurveyID > 0)
		{
			$anInstance = BITAMSurveyFieldMap::NewInstanceWithSurvey($aRepository, $aSurveyID);
			$anInstance->updateFromArray($aHTTPRequest->POST);
			$aResult = $anInstance->save();
			
			if (array_key_exists("go", $_POST))
			{
				$go = $_POST["go"];
			}
			else
			{
				$go = "self";
			}
			
			if ($go == "parent")
			{
				$anInstance = $anInstance->get_Parent();
			}
			else
			{
				if ($go == "new")
				{
					$anInstance = BITAMSurveyFieldMap::NewInstance($aRepository);
				}
			}
			
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		else {
			$aSurveyID = (int) @$aHTTPRequest->GET["SurveyID"];
		}
		
		$anInstance = BITAMSurveyFieldMap::NewInstanceWithSurvey($aRepository, $aSurveyID);
		if (!is_null($anInstance)) {
			$anInstance->eBavelAppID = $inteBavelAppID;
			$anInstance->eBavelFormID = $inteBavelFormID;
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
		{
			if (array_key_exists($strVariableName, $anArray))
			{
				$this->$strVariableName = $anArray[$strVariableName];
				$this->ArrayVariableValues[$intSurveyFieldID] = $anArray[$strVariableName];
			}
		}
		return $this;
	}
	
	function save()
	{
		foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
		{
			$sql = "";
			$intVariableValue = (int) @$this->$strVariableName;
			if (isset($this->ArrayVariablesExist[$intSurveyFieldID]) && $this->ArrayVariablesExist[$intSurveyFieldID])
			{
				if ($intVariableValue > 0) {
					$sql = "UPDATE SI_SV_SurveyFieldsMap SET eBavelFieldID = ".$intVariableValue.
					" WHERE SurveyID = ".$this->SurveyID." AND SurveyFieldID = $intSurveyFieldID";
				}
				else {
					$sql = "DELETE FROM SI_SV_SurveyFieldsMap 
						WHERE SurveyID = ".$this->SurveyID." AND SurveyFieldID = $intSurveyFieldID";
				}
			}
			else 
			{
				$sql =  "SELECT ".
					$this->Repository->DataADOConnection->IfNull("MAX(FieldMapID)", "0")." + 1 AS FieldMapID".
					" FROM SI_SV_SurveyFieldsMap";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$intFieldMapID = (int) @$aRS->fields["fieldmapid"];
				
				if ($intVariableValue > 0) {
					$sql = "INSERT INTO SI_SV_SurveyFieldsMap (FieldMapID, SurveyID, SurveyFieldID, eBavelFieldID) VALUES (".
						$intFieldMapID.", ".$this->SurveyID.", ".$intSurveyFieldID.", ".$intVariableValue.")";
				}
				else {
					$sql = "";
				}
			}
			
			if ($sql != '' && $this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	
	function isNewObject()
	{
		return false;
	}
	
	function get_Title()
	{
		return translate("eBavel field mapping");
	}
	
	function get_QueryString()
	{
		return "BITAM_PAGE=SurveyFieldMap";
	}
	
	function get_Parent()
	{
		//return $this->Repository;
		return $this;
	}
	
	function get_PathArray()
	{
		//$parent = $this->get_Parent();
		//$pathArray = $parent->get_PathArray();
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'FieldMapID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		require_once('eBavelIntegration.inc.php');
		
		$myFields = array();
		$categoryCatalogField = null;
		
		//Agrega la encuesta ya que realmente la ventana graba en base a ella y no a un campo específico
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyID";
		$aField->Title = translate("Survey");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = array($this->SurveyID => 'Survey');
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		//Obtiene la lista de aplicaciones de eBavel creadas en el repositorio
		$arreBavelApps = array(0 => '('.translate('None').')');
		$arreBavelApps = GetEBavelCollectionByField(@GetEBavelApplications($this->Repository), 'applicationName', 'key', true);
		
		$aFieldApps = BITAMFormField::NewFormField();
		$aFieldApps->Name = "eBavelAppID";
		$aFieldApps->Title = translate("eBavel application");
		$aFieldApps->Type = "Object";
		$aFieldApps->VisualComponent = "Combobox";
		$aFieldApps->Options = $arreBavelApps;
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		$aFieldApps->InTitle = true;
		$aFieldApps->IsVisible = false;
		$myFields[$aFieldApps->Name] = $aFieldApps;
		
		//Obtiene la lista de formas de eBavel creadas en el repositorio
		$arreBavelForms = array('' => array('' => '('.translate('None').')'));
		$arreBavelForms = GetEBavelCollectionByField(@GetEBavelSections($this->Repository), 'sectionName', 'key', true, 'appId');
		
		$aFieldForms = BITAMFormField::NewFormField();
		$aFieldForms->Name = "eBavelFormID";
		$aFieldForms->Title = translate("eBavel form");
		$aFieldForms->Type = "Object";
		$aFieldForms->VisualComponent = "Combobox";
		$aFieldForms->Options = $arreBavelForms;
		$aFieldForms->InTitle = true;
		$aFieldForms->IsVisible = false;
		$myFields[$aFieldForms->Name] = $aFieldForms;
		$aFieldForms->Parent = $aFieldApps;
		$aFieldApps->Children[] = $aFieldForms;
		
		$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, null, null, null, true, '');
		//Array indexado por tipo de dato (de eForms) + EBavelSectionId + EBavelFieldID para obtener los arrays que se usarán al sincronizar
		//las combos de cada campo de mapeo de eForms
		$arrAllEBavelFormsFields = array();
		$arrEmptyeBavelForms = array('0' => array(0 => translate('None')));
		
		//Primero tiene que generar una serie de Arrays basados en los tipos de campos pero agrupados por FormID
		//ya que finalmente los campos se deben entrelazar conforme se van seleccionando las combos padre. La agrupación final será por
		//el tipo de campo para poder aplicar en cada combo la colección correcta
		foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
			if (!isset($arrAllEBavelFormsFields[$intQTypeID])) {
				$arrAllEBavelFormsFields[$intQTypeID] = array();
			}
			
			foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
				$intFormID = (int) @$arrFieldData['section_id'];
				if (!isset($arrAllEBavelFormsFields[$intQTypeID][$intFormID])) {
					$arrAllEBavelFormsFields[$intQTypeID][$intFormID] = array();
					$arrAllEBavelFormsFields[$intQTypeID][$intFormID][0] = translate('None');
				}
				
				if ($intFieldID > 0) {
					$arrAllEBavelFormsFields[$intQTypeID][$intFormID][$intFieldID] = $arrFieldData['label'];
				}
			}
		}
		
		$arreBavelAlphaFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpOpenAlpha])) {
			$arreBavelAlphaFieldsColl = $arrAllEBavelFormsFields[qtpOpenAlpha];
		}
		
		$arreBavelDateFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpOpenDate])) {
			$arreBavelDateFieldsColl = $arrAllEBavelFormsFields[qtpOpenDate];
		}
		
		$arreBavelTimeFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpOpenTime])) {
			$arreBavelTimeFieldsColl = $arrAllEBavelFormsFields[qtpOpenTime];
		}
		
		$arreBavelNumericFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpOpenNumeric])) {
			$arreBavelNumericFieldsColl = $arrAllEBavelFormsFields[qtpOpenNumeric];
		}
		
		$arreBavelLocationFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpLocation])) {
			$arreBavelLocationFieldsColl = $arrAllEBavelFormsFields[qtpLocation];
		}
		
		$arreBavelEmailFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpEMail])) {
			$arreBavelEmailFieldsColl = $arrAllEBavelFormsFields[qtpEMail];
		}
		
		//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
		$arreBavelTextFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpOpenString])) {
			$arreBavelTextFieldsColl = $arrAllEBavelFormsFields[qtpOpenString];
		}
		
		//Genera todos los campos de cada columna a configurar, todos podrán configurarse hacia un campo alfanumérico de eBavel + el campo que
		//corresponde con el tipo específico de cada columna
		foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = $strVariableName;
			$aField->Title = BITAMSurveyFieldMap::GetSurveyColumnText($intSurveyFieldID);
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			switch($intSurveyFieldID)
			{
				case sfidStartDate:
				case sfidEndDate:
				case sfidSyncDate:
				case sfidServerStartDate:
				case sfidServerEndDate:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelDateFieldsColl, $arreBavelAlphaFieldsColl);
					break;
				
				case sfidStartHour:
				case sfidEndHour:
				case sfidSyncHour:
				case sfidServerStartHour:
				case sfidServerEndHour:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelTimeFieldsColl, $arreBavelAlphaFieldsColl);
					break;
				
				case sfidLocation:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelLocationFieldsColl, $arreBavelAlphaFieldsColl);
					break;
				
				case sfidScheduler:
				case sfidDuration:
				case sfidAnsweredQuestions:
				case sfidEntrySection:
				case sfidFactKey:
				case sfidEntryID:
				case sfidUserID:
				//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
				case sfidGPSZipCode:
				//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
				case sfidLocationAcc:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelAlphaFieldsColl, $arreBavelNumericFieldsColl);
					break;
				
				case sfidEMail:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelEmailFieldsColl, $arreBavelAlphaFieldsColl);
					break;
				
				//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
				case sfidGPSCountry:
				case sfidGPSState:
				case sfidGPSCity:
				case sfidGPSAddress:
				case sfidGPSFullAddress:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelTextFieldsColl, $arreBavelAlphaFieldsColl);
					break;
					
				case sfidDynamicPage:
				case sfidDynamicOption:
				case sfidUserName:
				default:			//Por default solo se configura el campo hacia un alfanumérico
					$arrMyeBavelFormsFields = $arreBavelAlphaFieldsColl;
					break;
			}
			
			$aField->Options = $arrMyeBavelFormsFields;
			$myFields[$aField->Name] = $aField;
			$aField->Parent = $aFieldForms;
			$aFieldForms->Children[] = $aField;
		}
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{
 		$myFormName = get_class($this);
?>
	<script language="javascript" src="js/dialogs.js"></script>
 	<script language="JavaScript">
 		function returnFieldsData()
 		{
 			var objFieldNames = new Object();
<? 			
			foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
			{
?>
				objFieldNames['<?=$strVariableName?>'] = Trim(<?=$myFormName?>_SaveForm.<?=$strVariableName?>.value);
<?
			}
 ?>			
 			
 			var strAnd = '';
 			var sParamReturn = '';
 			var arrKeys = Object.keys(objFieldNames); 
 			var intLength = arrKeys.length;
			for (var intCont = intLength -1; intCont >= 0; intCont--) {
				var strFieldName = arrKeys[intCont];
				sParamReturn += strAnd + strFieldName + '=' + objFieldNames[strFieldName];
				strAnd = '|';
			}
 			
			try {
				if (window.setDialogReturnValue)
				{
					setDialogReturnValue(sParamReturn);
				}
			}
			catch (e) {
			}
			try {
				owner.returnValue = sParamReturn;
			}
			catch (e)
			{
			}
			
			//window.close();
			cerrar();
 		}
	</script>
<?
 	}
 	
	function generateAfterFormCode($aUser)
	{
		$myFormName = get_class($this);
		
?>
	<script language="JavaScript">
		objOkSelfButton = document.getElementById("<?=$myFormName?>_OkSelfButton");
		objOkParentButton = document.getElementById("<?=$myFormName?>_OkParentButton");
		
		if (objOkSelfButton != null) {
			objOkSelfButton.onclick = new Function("returnFieldsData();");
		}
		if (objOkParentButton != null) {
			objOkParentButton.style.display = 'none';
		}
		
		var streBavelFieldData = '';
		var objField = null;
		if (window.opener && window.opener.BITAMSurvey_SaveForm && window.opener.BITAMSurvey_SaveForm.eBavelFieldsData) {
			objField = window.opener.BITAMSurvey_SaveForm.eBavelFieldsData.value;
		}
		try {
			if (objField != null) {
				streBavelFieldData = window.opener.document.getElementsByName('eBavelFieldsData')[0].value;
			}
			
			if (streBavelFieldData) {
				var arrFieldData = streBavelFieldData.split('|');
				for (var intIndex in arrFieldData) {
					var strFieldInfo = Trim(arrFieldData[intIndex]);
					if (strFieldInfo) {
						var arrFieldInfo = strFieldInfo.split('=');
						if (arrFieldInfo.length >= 2) {
							var strFieldName = arrFieldInfo[0];
							var intFieldID = parseInt(arrFieldInfo[1]);
							if (intFieldID <= 0) {
								intFieldID = 0;
							}
							
							var objFieldColl = document.getElementsByName(strFieldName);
							if (objFieldColl.length) {
								objFieldColl[0].value = intFieldID;
							}
						}
					}
				}
			}
		}
		catch (e) {
		}
	</script>
<?
	}
}
?>