<?php
/**
 * PHPPowerPoint
 *
 * Copyright (c) 2009 - 2010 PHPPowerPoint
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPPowerPoint
 * @package    PHPPowerPoint_Writer_PowerPoint2007
 * @copyright  Copyright (c) 2009 - 2010 PHPPowerPoint (http://www.codeplex.com/PHPPowerPoint)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    0.1.0, 2009-04-27
 */


/** PHPPowerPoint */
require_once 'PHPPowerPoint.php';

/** PHPPowerPoint_Writer_PowerPoint2007 */
require_once 'PHPPowerPoint/Writer/PowerPoint2007.php';

/** PHPPowerPoint_Writer_PowerPoint2007_WriterPart */
require_once 'PHPPowerPoint/Writer/PowerPoint2007/WriterPart.php';

/** PHPPowerPoint_Slide */
require_once 'PHPPowerPoint/Slide.php';

/** PHPPowerPoint_Shared_XMLWriter */
require_once 'PHPPowerPoint/Shared/XMLWriter.php';


/**
 * PHPPowerPoint_Writer_PowerPoint2007_Workbook
 *
 * @category   PHPPowerPoint
 * @package    PHPPowerPoint_Writer_PowerPoint2007
 * @copyright  Copyright (c) 2009 - 2010 PHPPowerPoint (http://www.codeplex.com/PHPPowerPoint)
 */
class PHPPowerPoint_Writer_PowerPoint2007_Others extends PHPPowerPoint_Writer_PowerPoint2007_WriterPart
{
	/**
	 * Write presProps to XML format
	 *
	 * @param 	PHPPowerPoint	$pPHPPowerPoint
	 * @return 	string 		XML Output
	 * @throws 	Exception
	 */
	public function writepresProps(PHPPowerPoint $pPHPPowerPoint = null)
	{
		// Create XML writer
		$objWriter = null;
		if ($this->getParentWriter()->getUseDiskCaching()) {
			$objWriter = new PHPPowerPoint_Shared_XMLWriter(PHPPowerPoint_Shared_XMLWriter::STORAGE_DISK, $this->getParentWriter()->getDiskCachingDirectory());
		} else {
			$objWriter = new PHPPowerPoint_Shared_XMLWriter(PHPPowerPoint_Shared_XMLWriter::STORAGE_MEMORY);
		}

		// XML header
		$objWriter->startDocument('1.0','UTF-8','yes');

		// p:presentationPr
		$objWriter->startElement('p:presentationPr');
		
		$objWriter->writeAttribute('xmlns:a', 'http://schemas.openxmlformats.org/drawingml/2006/main');
		$objWriter->writeAttribute('xmlns:r', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships');
		$objWriter->writeAttribute('xmlns:p', 'http://schemas.openxmlformats.org/presentationml/2006/main');
			
			// p:normalViewPr
			$objWriter->startElement('p:normalViewPr');
			
				// p:restoredLeft
				$objWriter->startElement('p:restoredLeft');
				$objWriter->writeAttribute('sz', '15620');
				$objWriter->endElement();
				
				// p:restoredTop
				$objWriter->startElement('p:restoredTop');
				$objWriter->writeAttribute('sz', '94660');
				$objWriter->endElement();
			
			// p:normalViewPr
			$objWriter->endElement();
			
			// p:slideViewPr
			$objWriter->startElement('p:slideViewPr');
			
				// p:cSldViewPr
				$objWriter->startElement('p:cSldViewPr');
				
					// p:cViewPr
					$objWriter->startElement('p:cViewPr');
					$objWriter->writeAttribute('varScale', '1');
					
						// p:scale
						$objWriter->startElement('p:scale');
						
							// a:sx
							$objWriter->startElement('a:sx');
							$objWriter->writeAttribute('n', '65');
							$objWriter->writeAttribute('d', '100');
							$objWriter->endElement();
							
							// a:sy
							$objWriter->startElement('a:sy');
							$objWriter->writeAttribute('n', '65');
							$objWriter->writeAttribute('d', '100');
							$objWriter->endElement();
						
						// p:scale
						$objWriter->endElement();
						
						// p:origin
						$objWriter->startElement('p:origin');
						$objWriter->writeAttribute('x', '-1770');
						$objWriter->writeAttribute('y', '-96');
						$objWriter->endElement();
				
					// p:cViewPr
					$objWriter->endElement();
					
					// p:guideLst
					$objWriter->startElement('p:guideLst');
					
						// p:guide
						$objWriter->startElement('p:guide');
						$objWriter->writeAttribute('orient', 'horz');
						$objWriter->writeAttribute('pos', '2880');
						$objWriter->endElement();
						
						// p:guide
						$objWriter->startElement('p:guide');
						$objWriter->writeAttribute('pos', '2880');
						$objWriter->endElement();
					
					// p:guideLst
					$objWriter->endElement();
			
				// p:cSldViewPr
				$objWriter->endElement();
			
			// p:slideViewPr
			$objWriter->endElement();
			
		// p:presentationPr
		$objWriter->endElement();

		// Return
		return $objWriter->getData();
	}
	/**
	 * Write tableStyles to XML format
	 *
	 * @param 	PHPPowerPoint	$pPHPPowerPoint
	 * @return 	string 		XML Output
	 * @throws 	Exception
	 */
	public function writetableStyles(PHPPowerPoint $pPHPPowerPoint = null)
	{
		// Create XML writer
		$objWriter = null;
		if ($this->getParentWriter()->getUseDiskCaching()) {
			$objWriter = new PHPPowerPoint_Shared_XMLWriter(PHPPowerPoint_Shared_XMLWriter::STORAGE_DISK, $this->getParentWriter()->getDiskCachingDirectory());
		} else {
			$objWriter = new PHPPowerPoint_Shared_XMLWriter(PHPPowerPoint_Shared_XMLWriter::STORAGE_MEMORY);
		}

		// XML header
		$objWriter->startDocument('1.0','UTF-8','yes');

		// a:tblStyleLst
		$objWriter->startElement('a:tblStyleLst');
		
		$objWriter->writeAttribute('xmlns:a', 'http://schemas.openxmlformats.org/drawingml/2006/main');
		$objWriter->writeAttribute('def', '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}');

		$objWriter->endElement();

		// Return
		return $objWriter->getData();
	}
	/**
	 * Write viewProps to XML format
	 *
	 * @param 	PHPPowerPoint	$pPHPPowerPoint
	 * @return 	string 		XML Output
	 * @throws 	Exception
	 */
	public function writeviewProps(PHPPowerPoint $pPHPPowerPoint = null)
	{
		// Create XML writer
		$objWriter = null;
		if ($this->getParentWriter()->getUseDiskCaching()) {
			$objWriter = new PHPPowerPoint_Shared_XMLWriter(PHPPowerPoint_Shared_XMLWriter::STORAGE_DISK, $this->getParentWriter()->getDiskCachingDirectory());
		} else {
			$objWriter = new PHPPowerPoint_Shared_XMLWriter(PHPPowerPoint_Shared_XMLWriter::STORAGE_MEMORY);
		}

		// XML header
		$objWriter->startDocument('1.0','UTF-8','yes');

		// p:viewPr
		$objWriter->startElement('p:viewPr');
		
		$objWriter->writeAttribute('xmlns:a', 'http://schemas.openxmlformats.org/drawingml/2006/main');
		$objWriter->writeAttribute('xmlns:r', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships');
		$objWriter->writeAttribute('xmlns:p', 'http://schemas.openxmlformats.org/presentationml/2006/main');

		$objWriter->endElement();

		// Return
		return $objWriter->getData();
	}
}
