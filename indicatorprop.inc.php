<?php 

require_once("repository.inc.php");
//require_once("indicator.inc.php");
//require_once("prop.inc.php");

class BITAMIndicatorPropCollection extends BITAMCollection
{
	public $IndicatorID;
	
	function __construct($aRepository, $aIndicatorID)
	{
		BITAMCollection::__construct($aRepository);
		$this->IndicatorID = $aIndicatorID;
//		$this->OrderPosField = "Prioridad";
	}

	static function NewIndicatorPropCollection($aRepository, $aIndicatorID, $anArrayOfPropIDs = null)
	{
		$anInstance = new BITAMIndicatorPropCollection($aRepository, $aIndicatorID);

		$filter = "";
		if (!is_null($anArrayOfPropIDs))
		{
			switch (count($anArrayOfPropIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "AND t1.CLA_PROP = ".((int) $anArrayOfPropIDs[0]);
					break;
				default:
					foreach ($anArrayOfPropIDs as $aPropID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aPropID; 
					}
					if ($filter != "")
					{
						$filter = "AND t1.CLA_PROP IN (".$filter.")";
					}
					break;
			}
		}

		$sql = 
		"SELECT t2.CLA_INDICADOR AS IndicatorID ".
		"		,t3.NOM_INDICADOR AS IndicatorName ".
		"		,t1.CLA_PROP AS PropID ".
		"		,t2.PRIORIDAD AS Prioridad ".
		"		,t1.NOM_PROP AS PropName ".
		"		,t2.OPERADOR AS Operador ".
		"		,t2.VALOR AS FormulaUsr ".
		"		,t2.VALOR_BD AS FormulaBD ".
		"		,t2.TIPO_VALOR AS TipoValor ".
		"		,t2.CLA_INDICADOR_PROP AS IndicatorProp ".
		"FROM 	SI_PROPIEDAD t1, SI_PROPxIND t2, SI_INDICADOR t3 ".
		"WHERE	t2.CLA_PROP = t1.CLA_PROP ".
		"		AND t3.CLA_INDICADOR = t2.CLA_INDICADOR ".
		"	    AND t2.CLA_INDICADOR = ".$aIndicatorID." ".$filter.
		" ORDER BY t2.PRIORIDAD, t1.NOM_PROP";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMPropKeys::NewPropFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewIndicatorPropCollectionForAssociation($aRepository, $aIndicatorID)
	{
		$anInstance = new BITAMIndicatorPropCollection($aRepository, $aIndicatorID);

		$sql = 
		"SELECT DISTINCT ".$aIndicatorID." AS IndicatorID ".
		"		,'' AS IndicatorName ".
		"		,t1.CLA_PROP AS PropID ".
		"		,NULL AS Prioridad ".
		"		,t1.NOM_PROP AS PropName ".
		"		,t1.OPERADOR AS Operador ".
		"		,'' AS FormulaUsr ".
		"		,'' AS FormulaBD ".
		"		,1 AS TipoValor ".
		"		,-1 AS IndicatorProp ".
		"FROM 	SI_PROPIEDAD t1 ".
		"WHERE	t1.CLA_PROP > 0 ".
		" ORDER BY t1.NOM_PROP";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMPropKeys::NewPropFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewIndicatorPropCollectionForSaving($aRepository, $aIndicatorID, $anArrayOfPropIDs)
	{
		$anInstance = new BITAMIndicatorPropCollection($aRepository, $aIndicatorID);

		$filter = "";
		switch (count($anArrayOfPropIDs))
		{
			case 0:
				break;
			case 1:
				$filter = "AND t1.CLA_PROP = ".((int) $anArrayOfPropIDs[0]);
				break;
			default:
				foreach ($anArrayOfPropIDs as $aPropID)
				{
					if ($filter != "")
					{
						$filter .= ", ";
					}
					$filter .= (int) $aPropID; 
				}
				if ($filter != "")
				{
					$filter = "AND t1.CLA_PROP IN (".$filter.")";
				}
				break;
		}

		$sql = 
		"SELECT DISTINCT ".$aIndicatorID." AS IndicatorID ".
		"		,'' AS IndicatorName ".
		"		,t1.CLA_PROP AS PropID ".
		"		,0 AS Prioridad ".
		"		,t1.NOM_PROP AS PropName ".
		"		,t1.OPERADOR AS Operador ".
		"		,'' AS FormulaUsr ".
		"		,'' AS FormulaBD ".
		"		,1 AS TipoValor ".
		"		,-1 AS IndicatorProp ".
		"FROM 	SI_PROPIEDAD t1 ".
		"WHERE	t1.CLA_PROP > 0 ".
		$filter.
		" ORDER BY t1.NOM_PROP";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMIndicatorProp::NewIndicatorPropFromRS($anInstance->Repository, $aIndicatorID, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("IndicatorID", $aHTTPRequest->GET))
		{
			$action = array_key_exists("action", $aHTTPRequest->GET) ? $aHTTPRequest->GET["action"] : "";
			$aIndicatorID = $aHTTPRequest->GET["IndicatorID"];
			if (array_key_exists("PropID", $aHTTPRequest->POST))
			{
				$aPropID = $aHTTPRequest->POST["PropID"];
				if (is_array($aPropID))
				{
					$anArrayOfPropIDs = $aPropID;
					switch ($action)
					{
						case 'associate':
							$filter = "";
							switch (count($anArrayOfPropIDs))
							{
								case 0:
									break;
								case 1:
									$filter = " AND CLA_PROP <> ".((int) $anArrayOfPropIDs[0]);
									break;
								default:
									foreach ($anArrayOfPropIDs as $aPropID)
									{
										if ($filter != "")
										{
											$filter .= ", ";
										}
										$filter .= (int) $aPropID; 
									}
									if ($filter != "")
									{
										$filter = " AND CLA_PROP NOT IN (".$filter.")";
									}
									break;
							}
							/*
							$sql = "DELETE FROM SI_PROPxIND WHERE CLA_INDICADOR = ".((int) $aIndicatorID); //.$filter;
							if ($aRepository->ADOConnection->Execute($sql) === false)
							{
								die("Error: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
							}
							*/
							$aCollection = BITAMIndicatorPropCollection::NewIndicatorPropCollectionForSaving($aRepository, $aIndicatorID, $anArrayOfPropIDs);
							foreach ($aCollection as $anInstanceToAssociate)
							{
								$anInstanceToAssociate->saveAssociation();
							}
							$aHTTPRequest->RedirectTo = $aCollection;
							break;
						case 'dissociate':
							$filter = "";
							switch (count($anArrayOfPropIDs))
							{
								case 0:
									break;
								case 1:
									$filter = " AND CLA_PROP = ".((int) $anArrayOfPropIDs[0]);
									break;
								default:
									foreach ($anArrayOfPropIDs as $aPropID)
									{
										if ($filter != "")
										{
											$filter .= ", ";
										}
										$filter .= (int) $aPropID; 
									}
									if ($filter != "")
									{
										$filter = " AND CLA_PROP IN (".$filter.")";
									}
									break;
							}
							if ($filter != "")
							{
								$sql = "DELETE FROM SI_PROPxIND WHERE CLA_INDICADOR = ".((int) $aIndicatorID).$filter;
								if ($aRepository->ADOConnection->Execute($sql) === false)
								{
									die("Error: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
								}
							}

							$aCollection = BITAMIndicatorPropCollection::NewIndicatorPropCollection($aRepository, $aIndicatorID);
							$aHTTPRequest->RedirectTo = $aCollection;
							
							ExportIndicatorToPHP($aRepository, (int) $aIndicatorID, true, false);
							
							break;
					}
					return null;
				}
			}
			$anInstance = null;
			switch ($action)
			{
				case 'associate':
				case 'dissociate':
					$aHTTPRequest->RedirectTo = BITAMIndicatorPropCollection::NewIndicatorPropCollection($aRepository, $aIndicatorID);
					break;
				default:
					$anInstance = BITAMIndicatorPropCollection::NewIndicatorPropCollectionForAssociation($aRepository, $aIndicatorID);
					break;
			}
			return $anInstance;
		}
		return null;
	}
	
	function get_AssociatedIDs()
	{
		$anArray = array();

		$sql = 
		"SELECT t2.CLA_PROP AS PropID, t2.NOM_PROP AS PropName ".
		"FROM 	SI_PROPxIND t1, SI_PROPIEDAD t2 ".
		"WHERE	t2.CLA_PROP = t1.CLA_PROP ".
		" AND  t1.CLA_INDICADOR = ".((int) $this->IndicatorID).
		" ORDER BY 2";
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anArray[(int) $aRS->fields["propid"]] = strtoupper(rtrim($aRS->fields["propname"]));
			$aRS->MoveNext();
		}
		
		return $anArray;
	}
	
	function get_Parent()
	{
		return BITAMIndicator::NewIndicatorWithID($this->Repository, $this->IndicatorID);
	}
	
	function get_Title()
	{
		return translate("Alerts");
	}

	function get_QueryString()
	{
		return $this->get_Parent()->get_QueryString()."&BITAM_SECTION=IndicatorPropCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=IndicatorProp&IndicatorID=".$this->IndicatorID;
	}
	
	function get_AssociateDissociateQueryString()
	{
		return "BITAM_SECTION=IndicatorPropCollection&IndicatorID=".$this->IndicatorID;
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'PropID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Prioridad";
		$aField->Title = translate("Level");
		$aField->Type = "Integer";
		$myFields[] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "PropName";
		$aField->Title = translate("Alert Name");
		$aField->Type = "String";
		$aField->Size = 15;
		$myFields[] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Operador";
		$aField->Title = translate("Condition");
		$aField->Type = "String";
		$aField->Size = 2;
		$myFields[] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FormulaUsrImg";
		$aField->Title = translate("Formula");
		$aField->Type = "String";
		$aField->Size = 30;
		$aField->Image = "FormulaImg";
		$myFields[] = $aField;		

		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
}

class BITAMPropKeys extends BITAMObject
{
	public $IndicatorID;
	public $IndicatorName;
	public $PropID;	
	public $Prioridad;
	public $PropName;
	public $Operador;
	public $FormulaUsr;
	public $FormulaBD;
	public $TipoValor;
	public $IndicatorProp;


	function FormulaUsrImg()
	{
		//@JAPR 2010-06-25: Agregada la imagen para los Elementos de una Row (s�lo para BITAMCollection)
		//Ya no es necesario concatenar la imagen al Name, porque eso se pone en el id del Tag
		return $this->FormulaUsr;
		//@JAPR
		if ($this->TipoValor == 1)
		{
			if ($this->IndicatorProp > 0)
				$htmlString = '<img src="images/indicadores.gif"';
			else
				$htmlString = '<img src="images/EditFormula.gif"';
		}
		else
			$htmlString = '<img src="images/date.gif"';
			
		return $htmlString." title = '".$this->FormulaUsr."'>".$this->FormulaUsr;
	}

	//@JAPR 2010-06-25: Agregada la imagen para los Elementos de una Row (s�lo para BITAMCollection)
	function FormulaImg()
	{
		if ($this->TipoValor == 1)
		{
			if ($this->IndicatorProp > 0)
				$htmlString = '<img src="images/indicadores.gif"';
			else
				$htmlString = '<img src="images/EditFormula.gif"';
		}
		else
			$htmlString = '<img src="images/date.gif"';
			
		return $htmlString." title = '".htmlentities($this->FormulaUsr)."'>";
	}
	//@JAPR
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->IndicatorName = "";
		$this->Prioridad = 1;
		$this->PropID = -1;
		$this->PropName = "";
		$this->Operador = ">";
		$this->FormulaUsr = "";
		$this->FormulaBD = "";
		$this->TipoValor = 1;
		$this->IndicatorProp = -1;
	}

	static function NewProp($aRepository)
	{
		return new BITAMPropKeys($aRepository);
	}

	static function NewPropWithID($aRepository, $aPropID, $IndicatorID)
	{
		$anInstance = null;
		if (((int) $aPropID) < 0)
		{
			return $anInstance;
		}
		if (((int) $IndicatorID) < 0)
		{
			return $anInstance;
		}		
		$sql = 
		"SELECT t2.CLA_INDICADOR AS IndicatorID ".
		"		,t3.NOM_INDICADOR AS IndicatorName ".
		"		,t1.CLA_PROP AS PropID ".
		"		,t2.PRIORIDAD AS Prioridad ".
		"		,t1.NOM_PROP AS PropName ".
		"		,t2.OPERADOR AS Operador ".
		"		,t2.VALOR AS FormulaUsr ".
		"		,t2.VALOR_BD AS FormulaBD ".
		"		,t2.TIPO_VALOR AS TipoValor ".
		"		,t2.CLA_INDICADOR_PROP AS IndicatorProp ".		
		"FROM 	SI_PROPIEDAD t1, SI_PROPxIND t2, SI_INDICADOR t3 ".
		"WHERE	t1.CLA_PROP = ".((int) $aPropID).
		"		AND t2.CLA_PROP = t1.CLA_PROP ".
		"		AND t3.CLA_INDICADOR = t2.CLA_INDICADOR ".
		"		AND t2.CLA_INDICADOR = ".$IndicatorID;
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMPropKeys::NewPropFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewPropFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMPropKeys::NewProp($aRepository);
		$anInstance->IndicatorID = (int) $aRS->fields["indicatorid"];
		$anInstance->IndicatorName = rtrim($aRS->fields["indicatorname"]);
		$anInstance->PropID = (int) $aRS->fields["propid"];
		$anInstance->Prioridad = (int) $aRS->fields["prioridad"];
		$anInstance->PropName = rtrim($aRS->fields["propname"]);
		$anInstance->Operador = rtrim($aRS->fields["operador"]);
		$anInstance->FormulaUsr = rtrim($aRS->fields["formulausr"]);		
		$anInstance->FormulaBD = rtrim($aRS->fields["formulabd"]);
		$anInstance->TipoValor = (int) $aRS->fields["tipovalor"];
		$anInstance->IndicatorProp = (int) $aRS->fields["indicatorprop"];		
		
		if ($anInstance->IndicatorProp == -1)
			$anInstance->IndicatorProp = 0;

		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		$aIndicatorID = -1;
		if (array_key_exists("IndicatorID", $aHTTPRequest->POST))
		{
			$aIndicatorID = $aHTTPRequest->POST["IndicatorID"];
		}
		if (array_key_exists("PropID", $aHTTPRequest->POST))
		{
			$aPropID = $aHTTPRequest->POST["PropID"];
			if (is_array($aPropID))
			{
				$aCollection = BITAMPropCollection::NewPropCollection($aRepository, $aPropID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
					if (!($anInstanceToRemove->Message == ""))
					{
						$anInstanceToRemove->Message = "Can�t delete this propertie because was used in production indicators";
						return $anInstanceToRemove;
					}
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMPropKeys::NewPropWithID($aRepository, (int) $aPropID, (int) $IndicatorID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMPropKeys::NewProp($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("PropID", $aHTTPRequest->GET))
		{
			$aPropID = $aHTTPRequest->GET["PropID"];
			$anInstance = BITAMPropKeys::NewPropWithID($aRepository, (int) $aPropID, (int) $IndicatorID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMPropKeys::NewProp($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMPropKeys::NewProp($aRepository);
		}
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("IndicatorName", $anArray))
		{
			$this->IndicatorName = stripslashes($anArray["IndicatorName"]);
		}		
 		if (array_key_exists("Prioridad", $anArray))
		{
			$this->Prioridad = (int) $anArray["Prioridad"];
		}		
 		if (array_key_exists("PropID", $anArray))
		{
			$this->PropID = (int) $anArray["PropID"];
		}
 		if (array_key_exists("TipoValor", $anArray))
		{
			$this->TipoValor = (int) $anArray["TipoValor"];
		}		
 		if (array_key_exists("IndicatorProp", $anArray))
		{
			$this->IndicatorProp = (int) $anArray["IndicatorProp"];
		}
 		if (array_key_exists("PropName", $anArray))
		{
			$this->PropName = stripslashes($anArray["PropName"]);
		}
 		if (array_key_exists("Operador", $anArray))
		{
			$this->Operador = stripslashes($anArray["Operador"]);
		}
 		if (array_key_exists("FormulaUsr", $anArray))
		{
			$this->FormulaUsr = stripslashes($anArray["FormulaUsr"]);
		}		
 		if (array_key_exists("FormulaBD", $anArray))
		{
			$this->FormulaBD = stripslashes($anArray["FormulaBD"]);
		}				
		return $this;
	}

	function save()
	{
	 	if ($this->isNewObject())
		{
		}
		else
		{
			$str_ind = '-1';			
			if ($this->TipoValor == 1)
			{
				$bError = false;
				$aIndicators = $this->fill_Indicators();
				
				if ($this->IndicatorProp > 0)
				{
					$this->FormulaBD = $this->get_FormulaBD_for_advanced($bError, $aIndicators);
					$str_ind = $this->FormulaBD;
					$this->FormulaBD = '{'.$this->FormulaBD.'}';
				}
				else
					$this->FormulaBD = $this->get_FormulaBD($bError, $aIndicators);
			}
			else 
				$this->FormulaBD = $this->FormulaUsr;
				
			if (trim($this->FormulaBD) == '')
				$this->FormulaBD = '0';
				
			if (trim($this->FormulaUsr) == '')
				$this->FormulaUsr = '0';
				
			$sql = "UPDATE SI_PROPxIND SET ".
						"PRIORIDAD = ".$this->Prioridad.
						",VALOR = ".$this->Repository->ADOConnection->Quote($this->FormulaUsr).
						",VALOR_BD = ".$this->Repository->ADOConnection->Quote($this->FormulaBD).
						",OPERADOR = ".$this->Repository->ADOConnection->Quote($this->Operador).
						",CLA_INDICADOR_PROP = ".(int) $str_ind.
					" WHERE CLA_PROP = ".$this->PropID.
					"		AND CLA_INDICADOR = ".$this->IndicatorID;
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
			
			ExportIndicatorToPHP($this->Repository, $this->IndicatorID, true, false);
		}
		return $this;
	}

	function fill_Indicators()
	{
// Llenamos un arreglo con los nombres de indicadores + formulas de base de datos
		if ($this->IndicatorProp > 0)
			$sql = 
			"select t1.CLA_INDICADOR AS indicator_id ".
			"		,t1.NOM_INDICADOR AS indicator_name ".
			"		,t2.NOM_CONCEPTO as cube_name ".
			"		,t1.FORMULA_BD AS formula_bd ".
			"from   SI_INDICADOR t1, SI_CONCEPTO t2 ".
			"where  t2.CLA_CONCEPTO = t1.CLA_CONCEPTO and TIPO_CUBO in ( 3, 4 ) and TIPO_INDICADOR IN ( 1, 3 ) and (t1.NOTNUMERIC IS NULL OR t1.NOTNUMERIC = 0) ".
			"order by t2.NOM_CONCEPTO, t1.NOM_INDICADOR";
		else 
			$sql = 
			"select CLA_INDICADOR AS indicator_id ".
			"		,NOM_INDICADOR AS indicator_name ".
			"		,FORMULA_BD AS formula_bd ".
			"from	SI_INDICADOR ".
			"where	TIPO_INDICADOR IN ( 1, 3 ) ".
			"		and CLA_CONCEPTO in ".
			"(select DISTINCT CLA_CONCEPTO  ".
			"from   SI_INDICADOR ".
			"where  CLA_INDICADOR = ".$this->IndicatorID." ) ".
			" and (ACTUAL is NULL or ACTUAL = 0 ) ".
			" and (NIVELES is NULL or NIVELES = '' ) ".
			" and (ONLY_LAST_PERIOD is NULL or ONLY_LAST_PERIOD = 0 ) and (NOTNUMERIC IS NULL OR NOTNUMERIC = 0) ".
			"order by NOM_INDICADOR";
			
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		$aIndicators = array();
		while (!$aRS->EOF)
		{		
			if ($this->IndicatorProp > 0)
				$aIndicators["{".$aRS->fields["cube_name"]."}.{".$aRS->fields["indicator_name"]."}"] = $aRS->fields["indicator_id"];
			else		
				$aIndicators["{".$aRS->fields["indicator_name"]."}"] = $aRS->fields["formula_bd"];
				
			$aRS->MoveNext();
		}
		return $aIndicators;
	}
	
	function get_FormulaBD_for_advanced($bError, &$aIndicators)
	{
// @EA 2005-06-30 Obtiene la formula de base de datos correspondiente al indicador capturado	
		$nposini = 0;
		$nposfin = 0;
		$sindicador_usr = "";
		$sindicator_bd = "";
		$bError = false;
// Obtenemos la formula de usuario original		
		$sformula_usr = $this->FormulaUsr;
		$sformula_bd = $this->FormulaUsr;
// Buscamos la primera ocurrencia		
		$nposini = strpos($sformula_usr, "{");
		while (!($nposini === false))
		{
// Buscamos donde termina la ocurrencia			
			$nposfin = strpos($sformula_usr, "}", $nposini+1);
			if ( $nposfin === false )
			{
				break;
			}
			else
			{
				$nposfin = strpos($sformula_usr, "}", $nposfin+1);
				if ( $nposfin === false )
				{
					break;
				}
// Obtenemos el nombre del indicador sin llaves {}				
				$sindicador_usr = substr($sformula_usr, $nposini, $nposfin-$nposini+1 );
// Obtenemos la formula de base de datos del indicador correspondiente

				if (array_key_exists($sindicador_usr, $aIndicators))
				{
					$sindicator_bd = $aIndicators[$sindicador_usr];
					
					//error_log.$sindicator_bd();
					//echo("<li>".$sindicator_bd."</li>");
	
					$sformula_bd = str_replace($sindicador_usr, $sindicator_bd, $sformula_bd);
					//echo("<li>".$sformula_bd."</li>");
				}
				$nposini = strpos($sformula_usr, "{", $nposfin+1);
				if ($nposini === false) 
					break;
			}
		}
		return $sformula_bd;
	}

	
	function get_FormulaBD($bError, $aIndicators)
	{
// @EA 2005-06-30 Obtiene la formula de base de datos correspondiente al indicador capturado	
		$nposini = 0;
		$nposfin = 0;
		$sindicador_usr = "";
		$sindicator_bd = "";
		$bError = false;
// Obtenemos la formula de usuario original		
		$sformula_usr = $this->FormulaUsr;
		$sformula_bd = $this->FormulaUsr;
// Buscamos la primera ocurrencia		
		$nposini = strpos($sformula_usr, "{");
		while (!($nposini === false))
		{
// Buscamos donde termina la ocurrencia			
			$nposfin = strpos($sformula_usr, "}", $nposini+1);
			if ( $nposfin === false )
			{
				break;
			}
			else
			{
// Obtenemos el nombre del indicador sin llaves {}				
				$sindicador_usr = substr($sformula_usr, $nposini, $nposfin-$nposini+1 );
// Obtenemos la formula de base de datos del indicador correspondiente
				//echo("<li>".$sindicador_usr."</li>");
				$sindicator_bd = $aIndicators[$sindicador_usr];
				//echo("<li>".$sindicator_bd."</li>");
// Lo reemplazamos en la formula de usuario original
				if ($this->IsNecesaryParentesis($sindicator_bd, $bError))
				{
					if ($bError)
					{
						return $sformula_bd;
					}
					else
						$sindicator_bd = "(".$sindicator_bd.")";
				}
				$sformula_bd = str_replace($sindicador_usr, $sindicator_bd, $sformula_usr);
				$sformula_usr = $sformula_bd;
				//echo("<li>".$sformula_bd."</li>");
				$nposini = strpos($sformula_usr, "{");
				if ($nposini === false) 
					break;
			}
		}
		return $sformula_bd;
	}
	

	function IsNecesaryParentesis($sExpresion, $bError)
	{	
//Recibe una expresion con un anidamiento correcto de parentesis y
//verifica si es necesario agregar parentesis externos para poder utilizar
//dicha expresion en una mayor		

		if ($this->IndicatorProp > 0)
			return false;

		$i = 0;
		$bError = false;
		$sExpresionBack = "";
		$xChar= "";
    
    	$xChar = "�";
    	$sExpresionBack = $sExpresion;
    	while (!(strpos($sExpresion, "(") === false))
    	{
        $sExpresion = $this->ReplaceFirstParentesis($sExpresion, $bError);
        if ($bError) return false;
    	}
    	$sExpresion = trim(str_replace($xChar, "", $sExpresion));
    	return (!($sExpresion == ""));
	}

	function ReplaceFirstParentesis($sExpresion, $bError)
	{
//Elimina el anidamiento de parentesis mas interno en la Expresion dada
//sustituyendola por un caracter especial		
		$iLastParentesis = 0;
		$iFirstParentesis = 0;
		$i = 0;
		$sParentesis = "";
		$sStringBefore = "";
		$schar = "";
		$xChar = "";
	    $bError = false;
	    $xChar = "�";
	
	    $iLastParentesis = strpos($sExpresion, ")");
	    If (!($iLastParentesis >= 0))
	    {
	    	$bError = true;
	    	return "";
	    }
	    
	    $i = $iLastParentesis - 1;
	    $bSalir = false;
	    while ($i >= 0 and !$bSalir )
	    {
	        $schar = substr($sExpresion, $i, 1);
	        if ($schar == "(") 
	        	$bSalir = true;
	        else
	        	$i = $i - 1;
	    }        	
	    if ($i = -1) 
	    {
	    	$bError = true;
	    	return "";
	    }
	    
	    $iFirstParentesis = $i;
	    $sParentesis = substr($sExpresion, $iFirstParentesis, $iLastParentesis - $iFirstParentesis + 1);
	    
	    $sStringBefore = substr($sExpresion, 0, $iFirstParentesis);
	    $sExpresion = str_replace($sParentesis, $xChar, $sExpresion);
	    return $sStringBefore.$sExpresion;
	}

	
	function remove()
	{		
		/* ====================================================================================
			Garantizamos la integridad del repositorio
		   ==================================================================================== */
		// Finalmente los grupos de indicadores		
		$sql = "DELETE 	FROM SI_PROPxIND ".
			   "WHERE 	CLA_PROP = ".$this->PropID.
			   "		AND CLA_INDICADOR = ".$this->IndicatorID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->PropID < 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return "New Prop";
		}
		else
		{
			return $this->PropName;
		}
	}

	function get_QueryString()
	{
		if ($this->PropID < 0)
		{
			return "BITAM_PAGE=IndicatorProp&IndicatorID=".$this->IndicatorID;
		}
		else
		{
			return "BITAM_PAGE=IndicatorProp&IndicatorID=".$this->IndicatorID."&PropID=".$this->PropID;
		}
	}

	function get_Parent()
	{
		return BITAMIndicator::NewIndicatorWithID($this->Repository, $this->IndicatorID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'PropID';
	}

	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		$this->generateBeforeFormCode($aUser);
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IndicatorName";
		$aField->Title = translate("Indicator");
		$aField->Type = "String";
		$aField->Size = 100;
		$aField->IsDisplayOnly = True;
		$myFields[$aField->Name] = $aField;		

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Prioridad";
		$aField->Title = translate("Level");
		$aField->Type = "Integer";
		$aField->FormatMask = "0";				
		$myFields[$aField->Name] = $aField;		
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "PropName";
		$aField->Title = translate("Alert Name");
		$aField->Type = "String";
		$aField->Size = 100;
		$aField->IsDisplayOnly = True;
		$myFields[$aField->Name] = $aField;
		
		$aOperators = array();
		$aOperators[">"] = ">";
		$aOperators[">="] = ">=";
		$aOperators["="] = "=";
		$aOperators["<"] = "<";
		$aOperators["<="] = "<=";
		$aOperators["<>"] = "<>";
		
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Operador";
		$aField->Title = "Operator";
		$aField->Type = "Object";
		$aField->Options = $aOperators;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FormulaUsr";
		$aField->Title = translate("Formula");
		$aField->Type = "LargeString";
		$aField->IsDisplayOnly = true;
		
		if ($this->TipoValor == 1)
		{
			$FormName = "BITAMIndicatorProp";
/*			
			$aField->AfterMessage = 
			"<input type=\"button\" name=\"pbbuild\" value=\"Build...\" style=\"text-align:center;vertical-align:top;width:64px\" ".
			"onclick=\"javascript: ".$FormName."_SaveForm.".$aField->Name.".value = showModalDialog('build_indicator.php?IndicatorID=".$this->IndicatorID."&PropID=".$this->PropID."', new Array('', ".$FormName."_SaveForm.".$aField->Name.".value), 'help: no; status: no; scroll: no; resizable: yes;')\">"; 
*/
			$aField->AfterMessage = 
			"<input type=\"button\" name=\"pbbuild\" value=\"".translate("Build")."...\" style=\"text-align:center;vertical-align:top;width:64px\" ".
			"onclick=\"javascript:addPropToFormula(".$this->IndicatorID.",".$this->PropID.");\">";
		}		
		
		$myFields[$aField->Name] = $aField;
		
		if ($this->TipoValor == 1)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "IndicatorProp";
			$aField->Type = "Boolean";		
			$aField->VisualComponent = "Checkbox";
			$aField->AfterMessage = translate("Advanced");
			$aField->OnChange = 'CheckPropFormula()';
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
?>
<script language="javascript">
			function addPropToFormula(IndicatorID, PropID)
			{
				var cb = document.getElementsByName('IndicatorProp');
				for (i=0;i<cb.length;i++)
				{
					if (cb[i].value == 1)
					{
						var bchecked = cb[i].checked;
						var df = document.getElementsByName('FormulaUsr');
						if (bchecked)
							df[0].value = showModalDialog('build_indicator.php?IndicatorID=' + IndicatorID + '&PropID=' + PropID + '&OnlyOneIndicator=1', new Array('', df[0].value), 'help: no; status: no; scroll: no; resizable: yes;');
						else
							df[0].value = showModalDialog('build_indicator.php?IndicatorID=' + IndicatorID + '&PropID=' + PropID, new Array('', df[0].value), 'help: no; status: no; scroll: no; resizable: yes;');
						break;
					}
				}
			}
			
			function CheckPropFormula()
			{
				var cb = document.getElementsByName('IndicatorProp');
				for (i=0;i<cb.length;i++)
				{
					if (cb[i].value == 1)
					{
						var bchecked = cb[i].checked;
						var df = document.getElementsByName('FormulaUsr');
						var str_formula = df[0].value;						
						if (bchecked)
						{
							if (str_formula.indexOf('*') > 0 || str_formula.indexOf('/') > 0 || str_formula.indexOf('+') > 0 || str_formula.indexOf('-') > 0 || str_formula.indexOf('(') > 0)
								df[0].value = '';
							if (str_formula.indexOf('}.{') < 0)
								df[0].value = '';
						}
						else
							if (str_formula.indexOf('}.{') > 0)
							 	df[0].value = '';
					}
				}
			}
</script>
<?
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}		

}


class BITAMIndicatorProp extends BITAMObject
{
	public $IndicatorID;
	public $Prop;
	
	function __construct($aRepository, $aIndicatorID)
	{
		BITAMObject::__construct($aRepository);
		$this->IndicatorID = (int) $aIndicatorID;
		$this->Prop = BITAMPropKeys::NewProp($aRepository);
	}

	static function NewIndicatorProp($aRepository, $aIndicatorID)
	{
		return new BITAMIndicatorProp($aRepository, $aIndicatorID);
	}

	static function NewIndicatorPropWithPropID($aRepository, $aIndicatorID, $aPropID)
	{
		
		$anInstance = null;
		if (((int) $aIndicatorID) < 0)
		{
			return $anInstance;
		}
		if (((int) $aPropID) < 0)
		{
			return $anInstance;
		}
		$sql = 
		"SELECT t2.CLA_INDICADOR AS IndicatorID ".
		"		,t3.NOM_INDICADOR AS IndicatorName ".
		"		,t1.CLA_PROP AS PropID ".
		"		,t2.PRIORIDAD AS Prioridad ".
		"		,t1.NOM_PROP AS PropName ".
		"		,t2.OPERADOR AS Operador ".
		"		,t2.VALOR AS FormulaUsr ".
		"		,t2.VALOR_BD AS FormulaBD ".
		"		,t2.TIPO_VALOR AS TipoValor ".
		"		,t2.CLA_INDICADOR_PROP AS IndicatorProp ".
		"FROM 	SI_PROPIEDAD t1, SI_PROPxIND t2, SI_INDICADOR t3 ".
		"WHERE	t2.CLA_PROP = t1.CLA_PROP ".
		"  AND  t3.CLA_INDICADOR = t2.CLA_INDICADOR ".
		"  AND  t2.CLA_INDICADOR = ".((int) $aIndicatorID).
		"  AND  t2.CLA_PROP = ".((int) $aPropID);

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMIndicatorProp::NewIndicatorPropFromRS($aRepository, $aIndicatorID, $aRS);
		}
		return $anInstance;
	}

	static function NewIndicatorPropFromRS($aRepository, $aIndicatorID, $aRS)
	{
		$anInstance = BITAMIndicatorProp::NewIndicatorProp($aRepository, $aIndicatorID);
		$anInstance->Prop = BITAMPropKeys::NewPropFromRS($aRepository, $aRS);
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("IndicatorID", $aHTTPRequest->GET))
		{
			$aIndicatorID = (int) $aHTTPRequest->GET["IndicatorID"];
			if (array_key_exists("PropID", $aHTTPRequest->POST))
			{
				$aPropID = $aHTTPRequest->POST["PropID"];
				if (is_array($aPropID))
				{
					$aCollection = BITAMIndicatorPropCollection::NewIndicatorPropCollection($aRepository, $aIndicatorID, $aPropID);
					foreach ($aCollection as $anInstanceToRemove)
					{
						$anInstanceToRemove->remove();
					}
					$aHTTPRequest->RedirectTo = $aCollection;
				}
				else
				{
					$anInstance = BITAMIndicatorProp::NewIndicatorPropWithPropID($aRepository, $aIndicatorID, $aPropID);
					if (is_null($anInstance))
					{
						$anInstance = BITAMIndicatorProp::NewIndicatorProp($aRepository, $aIndicatorID);
					}
					$anInstance->updateFromArray($aHTTPRequest->GET);
					$anInstance->updateFromArray($aHTTPRequest->POST);
					$anInstance->save();
					if (array_key_exists("go", $_POST))
					{
						$go = $_POST["go"];
					}
					else
					{
						$go = "self";
					}
					if ($go == "parent")
					{
						$anInstance = $anInstance->get_Parent();
					}
					else
					{
						if ($go == "new")
						{
							$anInstance = BITAMIndicatorProp::NewIndicatorProp($aRepository, $aIndicatorID);
						}
					}
					$aHTTPRequest->RedirectTo = $anInstance;
				}
				return null;
			}
			$anInstance = null;
			if (array_key_exists("PropID", $aHTTPRequest->GET))
			{
				$aPropID = $aHTTPRequest->GET["PropID"];
				$anInstance = BITAMIndicatorProp::NewIndicatorPropWithPropID($aRepository, $aIndicatorID, $aPropID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMIndicatorProp::NewIndicatorProp($aRepository, $aIndicatorID);
				}
			}
			else
			{
				$anInstance = BITAMIndicatorProp::NewIndicatorProp($aRepository, $aIndicatorID);
			}
			return $anInstance;
		}
		return null;
	}

	function updateFromArray($anArray)
	{
 		if (array_key_exists("IndicatorID", $anArray))
		{
			$this->IndicatorID = (int) $anArray["IndicatorID"];
		}
		$this->Prop->updateFromArray($anArray);
		return $this;
	}

	function save()
	{
	 	if ($this->isNewObject())
		{
			$this->Prop->save();
			$this->saveAssociation();
		}
		else
		{
			$this->Prop->save();
		}
		return $this;
	}

	function saveAssociation()
	{
		$sql = 
		'select count(*) as count_props from SI_PROPxIND where '.
		'CLA_INDICADOR = '.(int) $this->IndicatorID.' and '.
		'CLA_PROP = '.(int) $this->Prop->PropID;
		
		$rst = $this->Repository->ADOConnection->Execute($sql);
		
		if ($rst)
		{
			if ($rst->fields["count_props"] == 0)
			{
				$sql = 
				'select max(PRIORIDAD)+1 as ultima_prioridad from SI_PROPxIND where '.
				'CLA_INDICADOR = '.(int) $this->IndicatorID;
				
				$rst = $this->Repository->ADOConnection->Execute($sql);
				
				$prioridad = 1;
				if ($rst)
				{
					if (!is_null($rst->fields["ultima_prioridad"]))
						$prioridad = $rst->fields["ultima_prioridad"];
				}
				
				unset ($rst);
				
				$sql = "INSERT INTO SI_PROPxIND (".
							"PRIORIDAD,CLA_ROL, CLA_PERIODO, TIPO_VALOR, OPERADOR, CLA_INDICADOR".
							",CLA_PROP, VALOR, VALOR_BD".
							") VALUES (".$prioridad.",-1, -1, 1, '>', ".
							((int) $this->IndicatorID).
							",".((int) $this->Prop->PropID).
							",'0','0')";
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die("Error: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
				}
			}
		}
		
		return $this;
	}

	function remove()
	{		
		$sql = "DELETE FROM SI_PROPxIND WHERE CLA_INDICADOR = ".((int) $this->IndicatorID)." AND CLA_PROP = ".((int) $this->Prop->PropID);
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		$this->Prop->remove();
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->Prop->PropID < 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return "New Propertie";
		}
		else
		{
			return $this->Prop->PropName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=IndicatorProp&IndicatorID=".$this->IndicatorID;
		}
		else
		{
			return "BITAM_PAGE=IndicatorProp&IndicatorID=".$this->IndicatorID."&PropID=".$this->Prop->PropID;
		}
	}

	function get_Parent()
	{
		return BITAMIndicator::NewIndicatorWithID($this->Repository, $this->IndicatorID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'PropID';
	}

	function get_FieldValueOf($aFieldName)
	{
		if ($aFieldName == 'IndicatorID')
    	{
    		$aFieldValue = $this->get_FieldValueOf($aFieldName);
    	}
    	else
    	{
    		$aFieldValue = $this->Prop->get_FieldValueOf($aFieldName);
    	}
		return $aFieldValue;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		return $this->Prop->get_FormFields($aUser);
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		// @EA 2005-06-22
		$RepositoryType = $_SESSION["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			$RepositoryType = $_COOKIE["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			// Produccion
			$RepositoryType = 2;		

		if ($RepositoryType == 1)
			return true;
		else 
			return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}	
//
	

}

?>