<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Obtener el CatalogID
$CatalogID = "";
if (array_key_exists("CatalogID", $_POST))
{
	$CatalogID = $_POST["CatalogID"];
}

//Obtener el AttributeID
$AttributeID = "";
if (array_key_exists("AttributeID", $_POST))
{
	$AttributeID = $_POST["AttributeID"];
}

require_once("catalog.inc.php");

$response = "";

//Se obtiene la tabla RIDIM_dummy para sacar los emails
$sql = "SELECT TableName FROM SI_SV_Catalog WHERE CatalogID = ".$CatalogID;
		
$aRS = $theRepository->DataADOConnection->Execute($sql);
if ($aRS === false)
{
    die(translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}
if (!$aRS->EOF) {
    $dimTable = $aRS->fields["tablename"];
}

//obtenemos el campo donde estan los emails
$sql = "SELECT MemberName, ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$CatalogID." AND MemberID = ".$AttributeID;

$aRS = $theRepository->DataADOConnection->Execute($sql);
if ($aRS === false)
{
    die(translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}
if (!$aRS->EOF) {
    $fieldEmails = $aRS->fields["parentid"];
    $memberName = $aRS->fields["membername"];
}

//obtenemos los emails del catalogo
$sql = "SELECT DISTINCT(DSC_".$fieldEmails.") Email FROM ".$dimTable. " WHERE ".$dimTable."KEY > 1";
		
$aRS = $theRepository->DataADOConnection->Execute($sql);
if ($aRS === false)
{
    die(translate("Error accessing")." ".$dimTable." ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}
while (!$aRS->EOF) {
    $aEmail = $aRS->fields["email"];
    if(!filter_var($aEmail, FILTER_VALIDATE_EMAIL)) {
        $response = '"'.$memberName.'"'." has blanks or includes values that are not email addresses. Please correct and try again.";
        break;
    }
    $aRS->MoveNext();
}

header('Content-Type: text/plain; charset=utf-8');
echo $response;
?>