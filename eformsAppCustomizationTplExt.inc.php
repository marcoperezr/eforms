<?php

require_once("eformsAppCustomizationTpl.inc.php");
require_once("object.trait.php");

class BITAMEFormsAppCustomizationTplExt extends BITAMeformsAppCustomizationTpl
{
	use BITAMObjectExt;
  	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
	}
	
	function hideBackButton($aUser) {
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=EFormsAppCustomizationTplExt";
		}
		else
		{
			return "BITAM_PAGE=EFormsAppCustomizationExt&TemplateID=".$this->TemplateID;
		}
	}
	
	function get_Image() {
		
		$strImage = "<img src=\"images/admin/template.png\" />";
		
		return $strImage;
	}
	function getJSonDefinition() {
		
		$arrDef = array();
		$arrDef['id'] = $this->TemplateID;
		$arrDef['TemplateID'] = $this->TemplateID;
		$arrDef['TemplateName'] = $this->TemplateName;
			
		return $arrDef;
	}
	
}

class BITAMEFormsAppCustomizationTplExtCollection extends BITAMEFormsAppCustomizationTplCollection
{
	use BITAMCollectionExt;
	public $ObjectType;
	
	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		$this->ObjectType = otyAppCustomization;
		$this->ContainerID = "colors";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=EFormsAppCustomizationTplExt";
	}
	
	function get_CustomButtons() {
		$arrButtons = array();
		$arrButtons[] = array("id" => "add", "url" => "", "onclick" => "addNewObject();", "isDefault" => 1);
		return $arrButtons;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		return parent::PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser);
	}
	
	function generateAfterFormCode($aUser) {
?>
	<script language="JavaScript">
		var objWindows;
		var objDialog;
		var intDialogWidth = 380;
		var intDialogHeight = 350;
		var reqAjax = 1;
		
		//Descarga la instancia de la forma de la memoria
		function doUnloadDialog() {
			if (objDialog) {
				var objForm = objDialog.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
			}
			
			if (objWindows) {
				if (objWindows.unload) {
					objWindows.unload();
					objWindows = null;
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
			}
			
			objDialog = null;
		}
		
		//Manda los datos al server para ser procesados
		function doSendData() {
			if (!objDialog) {
				return;
			}
			
			objDialog.progressOn();
			var objForm = objDialog.getAttachedObject();
			var objData = objForm.getFormData();
			objForm.lock();
			objForm.send("processRequest.php", "post", function(loader, response) {
				if (!loader || !response) {
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				setTimeout(function () {
					try {
						var objResponse = JSON.parse(response);
					} catch(e) {
						alert(e + "\r\n" + response.substr(0, 1000));
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					if (objResponse.error) {
						alert(objResponse.error.desc);
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					else {
						if (objResponse.warning) {
							alert(objResponse.warning.desc);
						}
					}
					
					//Sólo si todo sale Ok cierra el diálogo y continua cargando el diseño de la forma recien creada
					doUnloadDialog();
					var strURL = objResponse.url;
					if (strURL && parent.doExecuteURL) {
						parent.doExecuteURL("<?=$this->ContainerID?>", undefined, undefined, strURL);
					}
				}, 100);
			});
		}
		
		//Muestra el diálogo para crear una nueva forma
		function addNewObject() {
			if (!objWindows) {
				//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
			}
			
			objDialog = objWindows.createWindow({
				id:"newObject",
				left:0,
				top:0,
				width:intDialogWidth,
				height:intDialogHeight,
				center:true,
				modal:true
			});
			
			objDialog.setText("<?=translate("New Template")?>");
			objDialog.denyPark();
			objDialog.denyResize();
			objDialog.setIconCss("without_icon");
			
			//Al cerrar realizará el cambio de sección de la pregunta
			objDialog.attachEvent("onClose", function() {
				return true;
			});
			
			/* El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma */
			var objFormData = [
				{type:"settings"/*, offsetLeft:20*/},
				{type:"input", name:"TemplateName", label:"<?=translate("Name")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
				{type:"block", blockOffset:0, offsetLeft:100, list:[
					{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
					{type:"newcolumn"},
					{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
				]},
				{type:"input", name:"TemplateID", value:-1, hidden:true},
				{type:"input", name:"RequestType", value:reqAjax, hidden:true},
				{type:"input", name:"Process", value:"Add", hidden:true},
				{type:"input", name:"ObjectType", value:<?=otyAppCustomization?>, hidden:true}
			];
			
			var objForm = objDialog.attachForm(objFormData);
			objForm.adjustParentSize();
			objForm.setItemFocus("TemplateName");
			objForm.attachEvent("onBeforeValidate", function (id) {
				console.log('Before validating the form: id == ' + id);
			});
			
			objForm.attachEvent("onAfterValidate", function (status) {
				console.log('After validating the form: status == ' + status);
			});
			
			objForm.attachEvent("onValidateSuccess", function (name, value, result) {
				console.log('Validating rule ok: name == ' + name + ', value == ' + value + ', result == ' + result);
			});
			
			objForm.attachEvent("onValidateError", function (name, value, result) {
				console.log('Error validating the form: name == ' + name + ', value == ' + value + ', result == ' + result);
			});
			
			objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
				if (!name || !inp || !ev || !ev.keyCode) {
					return;
				}
				
				switch (ev.keyCode) {
					case 13:
						//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
						if(objDialog.bitProcessing === undefined){
							objDialog.bitProcessing = true;
							setTimeout(function() {
								objForm.callEvent("onButtonClick", ["btnOk"]);
								objDialog.bitProcesing = undefined;
							}, 100);
						}
						break;
					case 27:
						setTimeout(function() {
							objForm.callEvent("onButtonClick", ["btnCancel"])
						}, 100);
						break;
				}
			});
			
			objForm.attachEvent("onButtonClick", function(name) {
				switch (name) {
					case "btnCancel":
						setTimeout(function () {
							doUnloadDialog();
						}, 100);
						break;
						
					case "btnOk":
						setTimeout(function() {
							if (objForm.validate()) {
								doSendData();
							}
						}, 100);
						break;
				}
			});
		}
	</script>
<?
	}	
}
?>