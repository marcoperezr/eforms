<?php
if(str_replace('.','',PHP_VERSION) > 530) {     error_reporting(E_ALL & ~E_DEPRECATED); } else {     error_reporting(E_ALL);   }
$userkey = -1;
$strOriginalWD = getcwd();
//@JAPR 2015-03-25: Agregada la validación para saber si debe o no convertir a utf8
global $blnEncodeUTF8;
$blnEncodeUTF8 = true;
if (version_compare(PHP_VERSION, '5.6.0', '>=')) {
	$blnEncodeUTF8 = false;
}

//@JAPR 2016-11-02: Agregado el parámetro de locale en cada request
$gsLocale = trim((string) @$_REQUEST['locale']);
//El API de globalization de Cordova indica a la fecha de esta implementación que el texto recibido estaría en formato "id-ct" donde
//id = Idioma en 2 caracteres (minúsculas en algunas pruebas)
//ct = ¿País? en 2 caracteres (mayúsculas en algunas pruebas)
//ejemplo: en-US, es-ES
//Se cortará la parte del idioma del local recibido
$gsLocaleLang = "";
if ($gsLocale) {
	$arrLocale = explode('-', $gsLocale);
	$gsLocaleLang = trim(strtoupper((string) @$arrLocale[0]));
}

if (!$gsLocaleLang) {
	$gsLocaleLang = "EN";
}
//@JAPR

//@JAPR 2016-01-18: Agregado el UUID y RegID a la tabla de identificación de usuarios para controlar los logis por dispositivo (sólo vía móvil) (#QOKOHB)
define('dvcWeb', 0);
define('dvciPod', 1);
define('dvciPad', 2);
define('dvciPadMini', 3);
define('dvciPhone', 4);
define('dvcCel', 5);
define('dvcTablet', 6);

//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
//Configuraciones de KPIOnline de la tabla saas_settings
define('kpisMultiPasswordEnabled', 1);
//@JAPR

//@JAPR 2016-11-02: Agregado el parámetro de locale en cada request
/* Dado un String con un texto de error o bien un código de error (por posibles funciones de validaciones de KPIOnline) regresa el texto
de error ajustado según el idioma del local recibido
Esta función sólo se debe utilizar para los procesos de eForms, en caso de no estar asignado el locale o no ser una versión de App con soporte, 
del mismo entonces se regresará el texto tal como actualmente funciona por compatibilidad hacia atrás
Si se recibe el parámetro $iErrCode, se asumirá que $sErrText se debe adaptar al texto correspondiente a dicho código de error, de lo contrario
se espera recibir siempre $sErrText
*/
function TranslateErrorMsgWithLocale($sErrText, $iErrCode = null) {
	global $gsLocaleLang;
	
	if (!is_null($iErrCode)) {
		//Ajustar aquí $sErrText según el código de error recibido
	}
	
	$appVersion = (float) @$_REQUEST['appVersion'];
	if (is_null($appVersion) || trim($appVersion) == '' || !is_numeric($appVersion)) {
		$appVersion = 1.0;
	}
	
	if ($appVersion < 6.02008) {
		return $sErrText;
	}
	
	$strErrorDesc = "Unknown error";
	switch ($gsLocaleLang) {
		case "ES":
			switch ($sErrText) {
				case "Error, user email empty":
					$strErrorDesc = "Debe introducir una cuenta válida";
					break;
				case "Connection error!":
					$strErrorDesc = "No se logró contactar al servidor de KPIOnline";
					break;
				case "Error, email or password incorrect":
					$strErrorDesc = "Debe introducir una cuenta válida";
					break;
				case "Error, the user account is not active":
					$strErrorDesc = "Su cuenta ha sido deshabilitada";
					break;
				case "Error, incorrect user or password":
					$strErrorDesc = "Contraseña equivocada";
					break;
				case "Error, no repository access found":
					$strErrorDesc = "Esta cuenta no tiene acceso a ningún proyecto de KPIOnline";
					break;
				case "The user does not have enough permissions":
					$strErrorDesc = "Su cuenta ha sido deshabilitada";
					break;
				case "Error creating the app data path in the KPIOnline Server":
					$strErrorDesc = "No se logró contactar al servidor de KPIOnline";
					break;
				case "USER OR PASSWORD INVALID":
					$strErrorDesc = "El usuario o contraseña es incorrecto";
					break;
				//@JAPR 2017-01-13: Agregada la validación de permisos de eForms a nivel de usuario (#Q9YG45)
				case "Access denied. Please contact your subscription administrator to obtain the appropriate permissions":
					$strErrorDesc = "Acceso denegado. Comuníquese con el administrador de su suscripción para obtener los permisos correspondientes";
					break;
				//@JAPR
				default:
					if (stripos($sErrText, "Error, could not select") !== false) {
						$strErrorDesc = "No se logró contactar al servidor de KPIOnline";
					}
					break;
			}
			break;
		default:		//Inglés por default
			switch ($sErrText) {
				case "Error, user email empty":
					$strErrorDesc = "You must enter a valid user account";
					break;
				case "Connection error!":
					$strErrorDesc = "Unable to contact KPIOnline server";
					break;
				case "Error, email or password incorrect":
					$strErrorDesc = "You must enter a valid user account";
					break;
				case "Error, the user account is not active":
					$strErrorDesc = "Your account is disabled";
					break;
				case "Error, incorrect user or password":
					$strErrorDesc = "Wrong password";
					break;
				case "Error, no repository access found":
					$strErrorDesc = "This account do not has access to any KPIOnline project";
					break;
				case "The user does not have enough permissions":
					$strErrorDesc = "Your account is disabled";
					break;
				case "Error creating the app data path in the KPIOnline Server":
					$strErrorDesc = "Unable to contact KPIOnline server";
					break;
				case "USER OR PASSWORD INVALID":
					$strErrorDesc = "The username or password you have entered is incorrect";
					break;
				//@JAPR 2017-01-13: Agregada la validación de permisos de eForms a nivel de usuario (#Q9YG45)
				case "Access denied. Please contact your subscription administrator to obtain the appropriate permissions":
					$strErrorDesc = "Access denied. Please contact your subscription administrator to obtain the appropriate permissions";
					break;
				//@JAPR
				default:
					if (stripos($sErrText, "Error, could not select") !== false) {
						$strErrorDesc = "Unable to contact KPIOnline server";
					}
					break;
			}
			break;
	}
	
	return $strErrorDesc;
}
//@JAPR

function BITAMFormsHasInvalidChar($aString)
{
	$STRVALID = utf8_decode("!¡#$%&'()*+,-./0123456789:;<=>?¿@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]_abcdefghijklmnopqrstuvwxyz");
	$n = strlen($aString);
	for ($i = 0; $i < $n; $i++)
	{
		if (strpos($STRVALID, $aString[$i]) === false)
		{
			return $i;
		}
	}
	return false;
}

function BITAMFormsDecryptPassword($inString)
{
	$inString = utf8_decode($inString);
	$STRVALID = utf8_decode("!¡#$%&'()*+,-./0123456789:;<=>?¿@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]_abcdefghijklmnopqrstuvwxyz");
	$aPassPos = array();
	$aPassChar= array();
	$inString = trim($inString);
	if ($inString == "")
	{
		return "";
	}
	elseif (BITAMFormsHasInvalidChar($inString)!==false)
	{   //EOAC 16Abr09 Soportar caracteres invalidos
		$inString2=BITAMFormsLiberaInValidChars($inString, $aPassPos, $aPassChar);
		if($inString2=='') return $inString;
		$inString=$inString2;
	}

	$firstChar = $inString[0];
	$lastChar = $inString[strlen($inString) - 1];
	$firstPos = strpos($STRVALID, $firstChar) + 1;
	$lastPos = strpos($STRVALID, $lastChar) + 1;
	$movPos = $lastPos - $firstPos;

	$outString = "";
	$n = strlen($inString) - 1;
	for ($i = 0; $i < $n; $i++)
	{
		$currentChar = $inString[$i];
		$newPos = strpos($STRVALID, $currentChar) + 1 + $movPos;
		if ($newPos <= 0)
		{
			$newPos = strlen($STRVALID) - abs($newPos);
		}
		If ($newPos > strlen($STRVALID))
		{
			$newPos = $newPos - strlen($STRVALID);
		}

		$outString .= $STRVALID[$newPos - 1];
	}
	if(count($aPassChar)>0) //EOAC 16Abr09 Soportar caracteres invalidos
	{
		$outString=BITAMFormsReturnInValidChars($outString, $aPassPos, $aPassChar);
	}
	$outString = str_replace('_#ArtusSpace#_',' ', $outString );
	return ($outString);
}

function checkFormsSecutity($conn, $repository, $userID, $password, $bSessionVars=false) {
	$bPWDV = false;
	$ssql = " Select CLA_CONFIGURA, REF_CONFIGURA " .
	" from SI_CONFIGURA " .
	" Where CLA_USUARIO = -2 " .
	" 		AND CLA_CONFIGURA IN (505,539)";

	$recordset = $conn->ADOConnection->Execute($ssql);
	if ($recordset === false) {
		return '3) Error Getting Metadata (checkForms)';
	}

	global $nIphoneStageGroup;
	if ($recordset) {
		while (!$recordset->EOF) {
			switch (intval($recordset->fields['CLA_CONFIGURA'])) {
				case 505:
				if (intval($recordset->fields['REF_CONFIGURA']) == 1) {
					$bPWDV = true;
				}
				break;

				case 539:
				$nIphoneStageGroup = $recordset->fields['REF_CONFIGURA'];
				break;
			}

			$recordset->MoveNext();
		}

		unset($recordset);
	}

	$ssql = " Select CLA_CONFIGURA, CLA_USUARIO, REF_CONFIGURA " .
	" from SI_CONFIGURA " .
	" Where CLA_USUARIO <> -2 " .
	" 		AND CLA_CONFIGURA IN (539)";

	$recordset = $conn->ADOConnection->Execute($ssql);
	if ($recordset === false) {
		return '3.1) Error Getting Metadata Dashboard Groups';
	}

	global $aIphoneStageGroup;

	if ($recordset) {
		while (!$recordset->EOF) {
			$aIphoneStageGroup[$recordset->fields['CLA_USUARIO']] = '(t2.CLA_GPO_ESC in (' . $recordset->fields['REF_CONFIGURA'] .
			') and t2.CLA_USUARIO = ' . $recordset->fields['CLA_USUARIO'] . ')';

			$recordset->MoveNext();
		}

		unset($recordset);
	}

	$sPassField = "A.PASSWORD, A.ARTUS_DESKTOP";

	if ($bPWDV) {
		$sPassField .= ", A.PASSWORDV, A.LAST_PWD_UPDATED, A.LAST_DISABLED";
	}

	$ssql = "select A.CLA_USUARIO, " . $sPassField . ", B.TERMINADOR, B.CLA_IDIOMA, A.CONFIG_SECURITY " .
	"from	SI_USUARIO A, SI_IDIOMA B " .
	"Where A.CUENTA_CORREO = " . strtoupper($conn->ADOConnection->Quote($userID)) . " and A.WHTMLANG = B.CLA_IDIOMA";

	$recordset = $conn->ADOConnection->Execute($ssql);
	if ($recordset === false) {
		return '4) Error Getting User Metadata';
	}

	if (!$recordset | $recordset->EOF) {
		$ssql = "select A.CLA_USUARIO, " . $sPassField . ", '@@' AS TERMINADOR, 2 AS CLA_IDIOMA, A.CONFIG_SECURITY " .
		"from	SI_USUARIO A " .
		"Where  A.CUENTA_CORREO = " . $conn->ADOConnection->Quote($userID);
		
		var_dump($ssql);
		
		$recordset = $conn->ADOConnection->Execute($ssql);

		if ($recordset === false) {
			return '5) Error Getting User Metadata';
		}
	}

	if (!$recordset) {
		return '6) Error Getting User Metadata';
	} else {
		global $userkey;
		if (!$recordset->EOF) {
			$userkey = $recordset->fields['CLA_USUARIO'];
			$userlang = $recordset->fields['TERMINADOR'];
			$userlangkey = $recordset->fields['CLA_IDIOMA'];
			if ($bPWDV) {
				$dbpwd = $recordset->fields['PASSWORDV'];
			} else {
				$dbpwd = $recordset->fields['PASSWORD'];
			}

			if ($userlang == '@@') {
				$userlang = 'EN';
				$userlangkey = 2;
			}
		}

		unset($recordset);
	}

	$dbpwd = trim(BITAMFormsDecryptPassword($dbpwd));
	if ($password != $dbpwd) { // Password Valido
		return 'You must enter a valid UserID and Password to access this resource';
	}

	if ($bSessionVars)
	{
		$ssql = 'SELECT ID_REPOSITORY FROM SI_REPOSITORY';
		$recordset = $conn->ADOConnection->Execute($ssql);
		$nRepositoryID = 0;
		if ($recordset === false) {
			return '7) Error Getting Repository ID';
		} else {
			if (!$recordset->EOF) {
				$nRepositoryID = intval($recordset->fields['ID_REPOSITORY']);
			} else {
				return '8) Error Getting Repository ID';
			}
		}

		$ssql = 'SELECT CLA_CONFIGURA, REF_CONFIGURA FROM SI_CONFIGURA WHERE
	    		  CLA_CONFIGURA IN (3) AND CLA_USUARIO = '.$userkey;
		$recordset = $conn->ADOConnection->Execute($ssql);
		global $StageDefault;
		$StageDefault = -1;
		if ($recordset === false){
			return '17) Error Getting Stage Default';
		}
		else {
			if (!$recordset->EOF){
				$StageDefault = intval($recordset->fields['REF_CONFIGURA']);
			}
			else{
				return '18) Error Getting Stage Default';
			}
		}

		session_start();
		$_SESSION["repository"] = $repository;
		$_SESSION["username"] = $userID;
		$_SESSION["userkey"] = $userkey;
		$_SESSION["password"] = $dbpwd;
		$_SESSION["userlang"] = $userlang;
		$_SESSION["userlangkey"] = $userlangkey;
		$_SESSION["bPWDV"] = $bPWDV;
		$_SESSION["repository_id"] = $nRepositoryID;
		$_SESSION["StageDefault"] = $StageDefault;
		session_write_close();
	}

	return '';
}

function getFormsInfo($kpiUser, $kpiPassword)
{
	global $blnEncodeUTF8;
	
	require_once('../../fbm/conns.inc.php');
	
	if (strlen($kpiUser) == 0)
	{
        $return['error'] = true;
		//@JAPR 2015-03-25: Agregada la validación para saber si debe o no convertir a utf8
		//El archivo ya es utf8, así que no hay necesidad de codificar en ninguna versión que haga el request
		$return['errmsg'] = TranslateErrorMsgWithLocale('Error, user email empty');
		//@JAPR
        return $return;
		//return 'Error, user email empty';
	}

	$mysql_Connection = mysql_connect($server, $server_user, $server_pwd);

	if (!$mysql_Connection)
	{
		$return['error'] = true;
		//@JAPR 2015-03-25: Agregada la validación para saber si debe o no convertir a utf8
		//El archivo ya es utf8, así que no hay necesidad de codificar en ninguna versión que haga el request
		$return['errmsg'] = TranslateErrorMsgWithLocale('Connection error!');
		//@JAPR 2016-11-02: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		$return['callSupportMsg'] = true;
		$return['errDetail'] = 'Connection error!';
		//@JAPR
        return $return;
        //return 'Error, connection error! '.mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
		//return 'Connection error! ';
	}

	$db_selected = mysql_select_db($masterdbname, $mysql_Connection);

	if (!$db_selected)
	{
		//return 'Error, could not select '.$masterdbname.'!'."\n".mysql_error();
		//return 'Error, could not select '.$masterdbname;
		$return['error'] = true;
		//@JAPR 2015-03-25: Agregada la validación para saber si debe o no convertir a utf8
		//El archivo ya es utf8, así que no hay necesidad de codificar en ninguna versión que haga el request
		$return['errmsg'] = TranslateErrorMsgWithLocale('Error, could not select '.$masterdbname);
		//@JAPR 2016-11-02: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		$return['callSupportMsg'] = true;
		$return['errDetail'] = 'Error, could not select '.$masterdbname;
		//@JAPR
        return $return;
	}

    //@JAPR 2014-03-10: Agregada información extendida para regresar al App de eForms
    $arrInfo = array();
	//Obtener el UserID de la tabla de SAAS_Users
	//@JAPR 2016-12-28: Corregido un bug, al ser un like, estaba permitiendo entrar con cuentas ligeramente diferentes siempre y cuando tuvieran un patrón LIKE válido (#IF5J19)
	$aSQL = sprintf("SELECT userID, Email, AccPassword, ActiveAcc, FirstName, LastName FROM saas_users WHERE Email = '%s'", mysql_real_escape_string($kpiUser, $mysql_Connection));
	//@JAPR
	$result = mysql_query($aSQL, $mysql_Connection);
	if (!$result || mysql_num_rows($result) == 0)
	{
		//return 'Error, data not found: '.$aSQL."\n".mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
		//return 'Error, email or password incorrect';
		$return['error'] = true;
		//@JAPR 2015-03-25: Agregada la validación para saber si debe o no convertir a utf8
		//El archivo ya es utf8, así que no hay necesidad de codificar en ninguna versión que haga el request
		$return['errmsg'] = TranslateErrorMsgWithLocale('Error, email or password incorrect');
		$return['errDetail'] = 'Error, email or password incorrect';
		//@JAPR
		return $return;
	}
	
	//Obtener resultado de la consulta
	$row = mysql_fetch_assoc($result);
	$intUserID = $row["userID"];
	$strUserEmail = $row["Email"];
	//@JAPR 2018-11-15: Validado que no se pueda ingresar si el EMail contiene espacios antes o después, aplicando un TRIM directo en PhP, ya que MySQL omite comparaciones con espacios en
	//campos que son Varchar (#ZA1GUB)
	$strUserEmail = (string) $strUserEmail;
	$kpiUser = (string) $kpiUser;
	if ( strtolower($strUserEmail) != strtolower($kpiUser) ) {
		$return['error'] = true;
		$return['errmsg'] = TranslateErrorMsgWithLocale('Error, email or password incorrect');
		$return['errDetail'] = 'Error, email or password incorrect';
		return $return;
	}
	//@JAPR
	
	$intUserActive = intval($row["ActiveAcc"]);
	$strEncryptedPass = $row["AccPassword"];
    //@JAPR 2014-03-10: Agregada información extendida para regresar al App de eForms
    $strUserName = '';
    $strData = trim((string) @$row['FirstName']);
    if ($strData != '') {
    	$strUserName .= $strData;
    }
    $strData = trim((string) @$row['LastName']);
    if ($strData != '' && $strData != $strUserName) {
    	$strUserName .= (($strUserName != '')?' ':'').$strData;
    }
    if ($strUserName != '') {
    	$arrInfo['userName'] = $strUserName;
    }
    //@JAPR

	//Verificar que la cuenta se encuentre activa
	if ($intUserActive == 2)
	{
		//return 'Error, the user account is not active'."\n".mysql_error();
		//return 'Error, the user account is not active';
        $return['error'] = true;
		//@JAPR 2015-03-25: Agregada la validación para saber si debe o no convertir a utf8
		//El archivo ya es utf8, así que no hay necesidad de codificar en ninguna versión que haga el request
		$return['errmsg'] = TranslateErrorMsgWithLocale('Error, the user account is not active');
		//@JAPR 2016-11-02: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		$return['callSupportMsg'] = true;
		$return['errDetail'] = 'Error, the user account is not active';
		//@JAPR
        return $return;
	}

	//Verificar que el password coincida
	$strUserPass = BITAMFormsDecryptPassword($strEncryptedPass);

	// SBF_2016-11-14: checar si está ya activo el soporte de contraseña por repositorio
	global $blnMultiPasswordEnabled;
	$blnMultiPass = (isset($blnMultiPasswordEnabled) && $blnMultiPasswordEnabled);
	//@JAPR 2016-11-24: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Finalmente la configuración se leerá desde la metadata de KPIOnline para permitir que todos tengan acceso sin tener que estar fisicamente en el lugar del archivo conns.php
	$blnMultiPass = false;
	$blnCheckMultiPassRepo = true;
	$aSQL = "SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = -2 AND settingUserID = -2 AND settingValue > 0";
	//NO se puede recibir el repositorio, ya que este servicio precisamente determinará a que repositorio se puede ingresar
	//Si se recibió el EMail (que todo request siempre lo debe recibir) primero identifica el UserID correspondiente
	if ($strUserEmail) {
		$intSAASUserID = $intUserID;	//En este caso ya se había consultado este dato arriba, así que se puede reutilizar
		/*
		$sql = "SELECT UserID 
			FROM saas_users 
			WHERE Email = '{$strUserEmail}'";
		$result = @mysql_query($sql, $mysql_Connection);
		if ($result && @mysql_num_rows($result) > 0) {
			$row = @mysql_fetch_assoc($result);
			$intSAASUserID = (int) @$row["UserID"];
		}
		*/
		if ($intSAASUserID) {
			$aSQL .= "
				UNION 
				SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = -2 AND settingUserID = {$intSAASUserID} AND settingValue > 0";
		}
	}
	
	$result = @mysql_query($aSQL, $mysql_Connection);
	if ($result && @mysql_num_rows($result) > 0) {
		$row = @mysql_fetch_assoc($result);
		$blnMultiPass = (bool) @$row["settingValue"];
		if ($blnMultiPass) {
			$blnCheckMultiPassRepo = false;
		}
	}
	
	//Si hasta este punto el usuario no tuviera habilitado el soporte de múlti passwords o no estuviera habilitado de manera global, habrá que revisar base de datos por base de datos
	//la primera que si lo tenga habilitado por excepción
	$arrMultiPassEnabledDBs = array();
	if ($blnCheckMultiPassRepo) {
		$aSQL = "SELECT settingValue, settingDBID 
			FROM saas_settings 
			WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID <> -2 AND settingUserID = -2 AND settingValue > 0 
				AND settingDBID IN (
					select DataBaseID FROM saas_dbxusr WHere UserID = {$intUserID}
				)";
		$result = @mysql_query($aSQL, $mysql_Connection);
		if ($result && @mysql_num_rows($result) > 0) {
			$row = @mysql_fetch_assoc($result);
			do {
				$blnMultiPassRepo = (bool) @$row["settingValue"];
				if ($blnMultiPassRepo) {
					$intDBID = (int) @$row["settingDBID"];
					$arrMultiPassEnabledDBs[$intDBID] = $blnMultiPassRepo;
				}
				$row = mysql_fetch_assoc($result);
			} while($row);
		}
		
		//Si existe por lo menos un repositorio donde está asociado este usuario y que si tiene habilitado múltiple passwords, entonces se tiene que validar en base a ello mas
		//adelante en lugar de verificar el password sólo considerando a saas_users
		$blnMultiPass = count($arrMultiPassEnabledDBs) > 0;
	}
	//@JAPR
	
	// SBF_2016-11-14: agregada la validación de contraseña por repositorio
	//@JAPR 2016-11-24: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Finalmente la configuración se leerá desde la metadata de KPIOnline para permitir que todos tengan acceso sin tener que estar fisicamente en el lugar del archivo conns.php
	if ($kpiPassword !== $strUserPass && !$blnMultiPass)
	{
		//return 'Error, incorrect user or password'."\n".mysql_error();
		//return 'Error, incorrect user or password';
        $return['error'] = true;
		//@JAPR 2015-03-25: Agregada la validación para saber si debe o no convertir a utf8
		//El archivo ya es utf8, así que no hay necesidad de codificar en ninguna versión que haga el request
		$return['errmsg'] = TranslateErrorMsgWithLocale('Error, incorrect user or password');
		//@JAPR 2016-11-02: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		$return['errDetail'] = 'Error, incorrect user or password';
		//@JAPR
        return $return;
	}
	
	// @SBF 2016-08-31: checar si viene el $_REQUEST["appName"]; si viene, obtener con él el repositorio y de ahí el DatabaseID para armar un filtro
	// para el query posterior.. (qué pasa si se obtiene más de un resultado? es eso posible? checar con @JAPR)
	$strDBIDWhere = "";
	//@JAPR 2016-11-09: Corregido un bug, estaba mal la referencia a $_REQUEST así que no ejecutaba este código correctamente
	$strAppName = (isset($_REQUEST["appName"]) ? trim($_REQUEST["appName"]) : "");
	if (strlen($strAppName) > 0)
	{
		$aSQL = <<<EOS
SELECT c.DatabaseID
FROM saas_eforms_apps a, saas_users b, saas_databases c
WHERE a.appname = '$strAppName'
AND a.masterAccount = b.EMail
AND b.userID = c.UserID
EOS;
		$restemp = mysql_query($aSQL, $mysql_Connection);
		if ($restemp && mysql_num_rows($restemp) > 0)
		{
			$rowtemp = mysql_fetch_assoc($restemp);
			$intDBID = intval($rowtemp["DatabaseID"]);
			if ($intDBID > 0)
			{
				$strDBIDWhere = " AND A.DatabaseID = $intDBID ";
			}
		}
	}
/*
@error_log("\n\nDBID = $intDBID", 3, "_request.log");
*/

	//Obtener el nombre del repositorio de KPA al cual tiene acceso
    //@JAPR 2012-10-25: Validado que si la cuenta tiene acceso a múltiples proyectos, regrese los datos del primero que esté configurado
    //como de eForms
	//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar imagenes a otra ruta
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones (campo DatabaseID)
    $aSQL = sprintf("SELECT A.KPIServer, A.ArtusPath, A.FormsPath, A.FormsVersion, A.Repository, A.DatabaseID, A.TemplateID, A.KPIServer, A.DBServer, A.DBServerUsr, A.DBServerPwd, A.DataWarehouse 
                                        FROM saas_dbxusr B, saas_databases A 
					WHERE B.UserID = %d AND (B.ArtusLogin = 1 OR B.ArtusLogin = 2) 
					AND B.DatabaseID = A.DatabaseID AND A.Status = 1 $strDBIDWhere ORDER BY B.ArtusLogin", $intUserID);
  	//error_log("\r\nuserID: " . $intUserID, 3, ".\checkData.log");
	// SBF_2016-11-14: si ya está activa la funcionalidad de contraseña por repositorio, pedir en el query la contraseña de saas_dbxusr, si es que existe
	if ($blnMultiPass)
	{
		$aSQL = sprintf("SELECT A.KPIServer, A.ArtusPath, A.FormsPath, A.FormsVersion, A.Repository, A.DatabaseID, A.TemplateID, A.KPIServer, A.DBServer, A.DBServerUsr, A.DBServerPwd, A.DataWarehouse, COALESCE(B.AccPassword, C.AccPassword) AS PasswordEnc 
											FROM saas_dbxusr B, saas_databases A, saas_users C 
						WHERE B.UserID = %d AND (B.ArtusLogin = 1 OR B.ArtusLogin = 2) 
						AND B.UserID = C.UserID
						AND B.DatabaseID = A.DatabaseID AND A.Status = 1 $strDBIDWhere ORDER BY B.ArtusLogin", $intUserID);
	}
	$result2 = mysql_query($aSQL, $mysql_Connection);
	if (!$result2 || mysql_num_rows($result2) == 0)
	{
		$return['error'] = true;
		//@JAPR 2015-03-25: Agregada la validación para saber si debe o no convertir a utf8
		//El archivo ya es utf8, así que no hay necesidad de codificar en ninguna versión que haga el request
		$return['errmsg'] = TranslateErrorMsgWithLocale('Error, no repository access found');
		//@JAPR 2016-11-02: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		$return['callSupportMsg'] = true;
		$return['errDetail'] = 'Error, no repository access found';
		//@JAPR
		return $return;
	}
	
    $row2 = mysql_fetch_assoc($result2);
    //@JAPR 2012-10-25: Validado que si la cuenta tiene acceso a múltiples proyectos, regrese los datos del primero que esté configurado
    //como de eForms
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones (campo DatabaseID)
	$intFormsDatabaseID = 0;
	$strFormsRepository = "";
    do {
		$intTemplateID = (int) @$row2["TemplateID"];
		//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar imagenes a otra ruta
	    $strServer = (string) @$row2["DBServer"];
		$strServerUsr = (string) @$row2["DBServerUsr"];
		$strServerPwd = (string) @$row2["DBServerPwd"];
		$strDWH = (string) @$row2["DataWarehouse"];
		//@JAPR
		$strKPIServer = is_null($row2["KPIServer"])?'':$row2["KPIServer"];
		$strArtusPath = is_null($row2["ArtusPath"])?'':$row2["ArtusPath"];
		$strFormsPath = is_null($row2["FormsPath"])?'':$row2["FormsPath"];
		$strFormsVersion = is_null($row2["FormsVersion"])?'':$row2["FormsVersion"];
		$strFormsRepository = (string) @$row2["Repository"];
		//@JAPR 2015-03-02: Agregado el Monitor de Operaciones (campo DatabaseID)
		$intFormsDatabaseID = (int) @$row2["DatabaseID"];
	    //Se considera un template válido de eForms si pertenece al template propio de eForms, o bien si aunque no sea de dicho template está
	    //configurado hacia un path de eForms (en esos casos sólo pudo ser desde la v3 o posterior, por lo que es seguro que no es válido un
	    //path vacio para que utilice por default el de v2) 
	    $blnValideFormsAcc = ($intTemplateID == 5 || $strFormsPath != '');
		// SBF 2016-11-14: Checar si está activa la funcionalidad de contraseña por repositorio y hacer validaciones correspondientes
		if ($blnMultiPass)
		{
			$strUserPass = BITAMFormsDecryptPassword($row2["PasswordEnc"]);
			$blnValideFormsAcc = $blnValideFormsAcc && ($kpiPassword === $strUserPass);
		}
	    $row2 = mysql_fetch_assoc($result2);
    } while($row2 && !$blnValideFormsAcc);
    //@JAPR
	
	// SBF 2016-11-14: Checar si está activa la funcionalidad de contraseña por repositorio y hacer validaciones correspondientes
	if ($blnMultiPass && !$blnValideFormsAcc)
	{
		//return 'Error, incorrect user or password'."\n".mysql_error();
		//return 'Error, incorrect user or password';
        $return['error'] = true;
		//@JAPR 2015-03-25: Agregada la validación para saber si debe o no convertir a utf8
		//El archivo ya es utf8, así que no hay necesidad de codificar en ninguna versión que haga el request
		$return['errmsg'] = TranslateErrorMsgWithLocale('Error, incorrect user or password');
		//@JAPR 2016-11-02: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		$return['errDetail'] = 'Error, incorrect user or password';
		//@JAPR
        return $return;
	}
    
	//@JAPR 2016-12-22: Agregada la validación de expiración de cuentas de eForms (#LR9E84)
	//En este punto ya se tiene el usuario, password y repositorio al que se debería ingresar, ahora simplemente valida si no se encuentra expirada esa combinación o cualquier otro
	//supuesto que bloquee el acceso, en caso de ser así, deberá reportar el error al App
	$blnAccountActive = ValidateAccountExpiration($kpiUser, $kpiPassword, $strFormsRepository);
	if ($blnAccountActive !== true) {
        $return['error'] = true;
		$return['errmsg'] = (string) @$blnAccountActive['errDesc'];
		$return['errDetail'] = "Error code: ".((int) @$blnAccountActive['errCode']).", Repository: {$strFormsRepository}, Expiration date: ".((string) @$blnAccountActive['expirationDate']);
		if (isset($blnAccountActive['forceerrordesc']) && $blnAccountActive['forceerrordesc']) {
			$return['forceerrordesc'] = 1;
		}
		//@JAPR 2017-01-16: Corregido un bug, no estaba regresando inmediatamente después de detectar el error, así que realizaba procesos que no debería
		return $return;
	}
	//@JAPR
	
    if($strKPIServer == '' || $strKPIServer == 'NULL') {
        $strKPIServer = 'kpionline.bitam.com';
    }
    if($strArtusPath == '' || $strArtusPath == 'NULL') {
        $strArtusPath = 'artus/genvi';
    }
    if($strFormsPath == '' || $strFormsPath == 'NULL') {
        $strFormsPath = 'ESurveyTMP';
    }
    if($strFormsVersion == '' || $strFormsVersion == 'NULL') {
        $strFormsVersion = '1.1';
    }
    
	//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar imagenes a otra ruta
	//Servicio de eForms alternativo al que ciertas Apps (condicionado por versión) se conectarán en lugar del especificado en SAAS_Databases
	//Valida si existen para esta cuenta las tablas de configuración de eForms, si existen verifica si tienen las entradas que la forzarían
	//a redireccionar a un servicio alternativo y si la versión que hace este request es >= que la versión configurada para forzar a esta
	//redirección alterna (esto debido a que para probar con Wixo simultáneamente, quienes tenían v2, se requería probar a la par con una v3
	//pero como sólo hay una configuración de servicio no hubiera sido posible, por lo tanto para futuros casos similares se ofrecerá esta
	//alternativa de redirección controlada vía la versión de eForms que hace el request)
	$appVersion = '';
	if (isset($_POST['appVersion'])) {
		$appVersion = (string) @$_POST['appVersion'];
	}
	elseif (isset($_GET['appVersion'])) {
		$appVersion = (string) @$_GET['appVersion'];
	}
	if (is_null($appVersion) || trim($appVersion) == '' || !is_numeric($appVersion)) {
		$appVersion = '1.0';
		//$appVersion = '3.00016';
	}
  	//error_log("\r\nformslogsrv appVersion: " . $appVersion, 3, ".\checkData.log");
	//@JAPR 2018-10-11: Agregada la versión de actualización vía inyección de código (#CACG8Y)
	$appUpdateVersion = '';
	if (isset($_REQUEST['updateVersion'])) {
		$appUpdateVersion = (string) @$_REQUEST['updateVersion'];
	}
	
	//@JAPR 2017-01-13: Agregada la validación de permisos de eForms a nivel de usuario (#Q9YG45)
	//Movido este código antes de registrar en SI_SV_UserDevices para permitir comprobar todos los permisos antes de registrar en dicha tabla
	//Realiza la conexión al DWH
	$blnGetAlteFormsService = false;
	$mysql_Connection_DWH = null;
	if (strlen($strServer) > 0)
	{
  		//error_log("\r\nDWH connection to: " . $strServer, 3, ".\checkData.log");
		$mysql_Connection_DWH = mysql_connect($strServer, $strServerUsr, BITAMFormsDecryptPassword($strServerPwd));
		if ($mysql_Connection_DWH) 
		{
			$blnGetAlteFormsService = true;
		}
	}
	else
	{
  		//error_log("\r\nNo new DWH connection", 3, ".\checkData.log");
		$mysql_Connection_DWH = $mysql_Connection;
		$blnGetAlteFormsService = true;
	}
	
	//@JAPR 2017-01-16: Agregada la validación de permisos de eForms a nivel de usuario (#Q9YG45)
	//Verifica si el usuario tiene o no permisos de eForms activados, cualquiera de los dos es suficiente para que el App pueda ingresar
	$sql = "SELECT A.SV_Admin, A.SV_User 
		FROM ".$strFormsRepository.".SI_SV_Users A 
		WHERE A.EMail = '".mysql_real_escape_string($strUserEmail, $mysql_Connection_DWH)."'";
	$aRS = @mysql_query($sql, $mysql_Connection_DWH);
	if ($aRS && mysql_num_rows($aRS) != 0) {
		$aRow = mysql_fetch_assoc($aRS);
		$intSVAdmin = (int) @$aRow['SV_Admin'];
		$intSVUser = (int) @$aRow['SV_User'];
		if ($intSVAdmin == 0 && $intSVUser == 0) {
			$return['error'] = true;
			$return['errmsg'] = TranslateErrorMsgWithLocale("Access denied. Please contact your subscription administrator to obtain the appropriate permissions");
			$return['errDetail'] = TranslateErrorMsgWithLocale("Access denied. Please contact your subscription administrator to obtain the appropriate permissions");
			$return['forceerrordesc'] = 1;
			return $return;
		}
	}
	
	//@JAPR 2016-01-18: Agregado el UUID y RegID a la tabla de identificación de usuarios para controlar los logis por dispositivo (sólo vía móvil) (#QOKOHB)
	//Se registrará la información del dispositivo que hizo el login a eForms en la nueva tabla de dispositivos por cuenta, de tal manera que se pueda controlar con qué dispositivos
	//se puede conectar a KPIOnline para uso de eForms
	//A la fecha de esta implementación, agregaba cada nuevo dispositivo recibido con la fecha de su último uso, ya que no se tiene aún definido si se bloqueará el acceso de nuevos
	//dispositivos o si se bloqueará el uso simultáneo de varios dispositivos aunque pueda tener varios disponibles, toda esa lógica se puede realizar debajo de esta actualización o
	//modificando la manera en que se inserta en esta tabla, la cual por lo pronto sólo servirá como estadística de uso de los dispositivos por cuenta
	//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
	//@JAPR 2016-10-21: Agregado el campo IsActive para identificar el dispositivo que se encuentra activo en cada cuenta, ya que si dos o mas cuentas entran con el mismo dispositivo,
	//sólo la última debería quedar con dicho dispositivo asignado (IsActive = 1), por lo que se hará un Update a las demás cuentas de ese dispositivo para desactivarlas (IsActive = 0)
	//@JAPR 2018-10-11: Agregada la versión de actualización vía inyección de código (#CACG8Y)
	try {
		//@JAPR 2019-01-30: Debido a un incidente reciente, LRoux indicó que ya no deberían realizarse ALTER TABLEs continuos para evitar Table locks, así que se
		//comentarán todos los siguientes estatutos bajo el entendido que la única tabla de metadata de KPIOnline tendría siempre que estar al día manualmente,
		//de cualquier manera es buena idea seguir agregando los ALTERs en este bloque como un histórico de los cambios sufridos en el Agente de eForms (#BBGFMH)
		/*$sql = "CREATE TABLE SI_SV_UserDevices (
			UserDeviceID INTEGER NOT NULL AUTO_INCREMENT,
			UserID VARCHAR(255) NOT NULL,
			UUID VARCHAR(100) NOT NULL,
			DeviceID INTEGER NOT NULL,
			LastDateID DATETIME NOT NULL,
			RegID VARCHAR(255) NULL,
			DeviceName VARCHAR(100) NULL,
			PhoneNumber VARCHAR(50) NULL,
			UserAgent TEXT NULL,
			AppName VARCHAR(50) NOT NULL,
			FormsVersion VARCHAR (50) NOT NULL, 
			StoreApp INTEGER NULL,
			IsActive INTEGER NULL,
			PRIMARY KEY (UserDeviceID),
			INDEX `UniqueDeviceXUser` (`UserID`(100), `UUID`)
		)";
		$aRS = @mysql_query($sql, $mysql_Connection);
		
		//@JAPR 2016-10-14: Modificado para que los campos nuevos se agreguen programáticamente a la tabla en caso de no existir
		$sql = "ALTER TABLE SI_SV_UserDevices ADD AppName VARCHAR(50) NULL";
		$aRS = @mysql_query($sql, $mysql_Connection);
		
		$sql = "ALTER TABLE SI_SV_UserDevices ADD FormsVersion VARCHAR(50) NULL";
		$aRS = @mysql_query($sql, $mysql_Connection);
		
		//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
		$sql = "ALTER TABLE SI_SV_UserDevices ADD StoreApp INTEGER NULL";
		$aRS = @mysql_query($sql, $mysql_Connection);
		
		//@JAPR 2016-10-21: Agregado el campo IsActive para identificar el dispositivo que se encuentra activo en cada cuenta, ya que si dos o mas cuentas entran con el mismo dispositivo,
		//sólo la última debería quedar con dicho dispositivo asignado (IsActive = 1), por lo que se hará un Update a las demás cuentas de ese dispositivo para desactivarlas (IsActive = 0)
		$sql = "ALTER TABLE SI_SV_UserDevices ADD IsActive INTEGER NULL";
		$aRS = @mysql_query($sql, $mysql_Connection);
		
		//@JAPR 2018-10-11: Agregada la versión de actualización vía inyección de código (#CACG8Y)
		$sql = "ALTER TABLE SI_SV_UserDevices ADD FormsUpdate VARCHAR(50) NULL";
		$aRS = @mysql_query($sql, $mysql_Connection);*/
		//@JAPR
		
		$strUserAccount = mysql_real_escape_string($kpiUser, $mysql_Connection);
		$strUUID = trim((string) @$_REQUEST['uuid']);
		if ($strUUID != '') {
			$strUUID = mysql_real_escape_string($strUUID, $mysql_Connection);
			$phoneNumber ='';
			//Conchita agregado el phonenumber q se recibe como parametro
			if (isset($_REQUEST['phoneNumber'])) {
				$tmp = (string) @$_REQUEST['phoneNumber']; //temporal antes de ser limpiado
				//se limpia el numero de telefono
				for ($i=0; $i<strlen($tmp); $i++) {
					if (is_numeric($tmp[$i])) {
						$phoneNumber.=$tmp[$i];
					}
				}
			}
			$phoneNumber = mysql_real_escape_string(trim($phoneNumber), $mysql_Connection);
			
			$deviceName = '';
			if (isset($_REQUEST['deviceName'])) {
				$deviceName = (string) @$_REQUEST['deviceName'];
			}
			$deviceName = mysql_real_escape_string(trim($deviceName), $mysql_Connection);
			
			$regID = (string) @$_REQUEST['regID'];
			$regID = mysql_real_escape_string($regID, $mysql_Connection);
			$currDate = mysql_real_escape_string(date('Y-m-d H:i:s'), $mysql_Connection);
			$intDeviceID = (int) @identifyDeviceType();
			$strUserAgent = @(isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
			$strUserAgent = mysql_real_escape_string($strUserAgent, $mysql_Connection);

			//OMMC 2016-10-11: Agregada la variable del nombre de la aplicación
			$appName = '';
			if (isset($_REQUEST['appName'])) {
				$appName = (string) @$_REQUEST['appName'];
			}
			$appName = mysql_real_escape_string($appName, $mysql_Connection);
			
			//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
			//Se leerá un nuevo parámetro para identificar si se trata de un App de tienda (valor de 1 o NULL) o app de liga (valor de 0). En caso de ausencia del parámetro se asumirá
			//que se trata de app de Tienda para permitir que si se generó un App sin este parámetro y en efecto se subió a la tienda, de todas maneras funcione correctamente, y sólo las
			//apps de liga que son mas fáciles de generar deberán ser regeneraras específicamente con el valor de 0
			//@JAPR 2016-10-31: Corregido un bug, estaba leyendo el parámetro con el case incorrecto (#L9T2IU)
			$intStoreApp = @$_REQUEST['storeApp'];
			if (is_null($intStoreApp)) {
				//En este caso grabará el literal NULL (NO null de PhP) en lugar de un número
				$intStoreApp = "NULL";
			}
			else {
				//En este caso se convierte a integer por si se recibió basura
				$intStoreApp = (int) @$intStoreApp;
			}
			//@JAPR
			
			//Verifica si existe la combinación de Usuario + UUID, si es así, actualiza el RegID y la fecha de último acceso, de lo contrario crea un nuevo registro
			//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
			$sql = "SELECT UserDeviceID, UserID, UUID, DeviceID, LastDateID, RegID, AppName, FormsVersion, StoreApp 
				FROM SI_SV_UserDevices 
				WHERE UserID = '{$strUserAccount}' AND UUID = '{$strUUID}'";
			$aRS = @mysql_query($sql, $mysql_Connection);
			if ($aRS && mysql_num_rows($aRS) != 0) {
				//Ya existe el dispositivo para este usuario, sólo actualiza la información reelevante si es que hubiera recibido alguna diferente
				$aRow = mysql_fetch_assoc($aRS);
				$strRegID = trim((string) @$aRow['RegID']);
				$strRegID = mysql_real_escape_string($strRegID, $mysql_Connection);
				
				//No todos los campos deben ser actualizados, identifica cuales se recibieron con algún valor nuevo para hacer el ajuste (se asume que un valor vacío NO es un dato válido,
				//sino que no se envió como parte del request, para limpiar el valor se tendría que enviar el identificado NULL). El UUID es el único valor que no puede cambiar, ya que
				//es parte de la llave primaria para identificar a un dispositivo único
				if ($regID != '') {
					//Se actualiza el RegID con el valor recibido, este es el único campo fijo en el UPDATE
					$strRegID = $regID;
				}
				
				$strAdditionalFields = '';
				if ($phoneNumber != '') {
					$strAdditionalFields .= ", PhoneNumber = '{$phoneNumber}'";
				}
				if ($deviceName != '') {
					$strAdditionalFields .= ", DeviceName = '{$deviceName}'";
				}
				//OMMC 2016-10-11: Agregado el nombre y versión de aplicación  
				//@JAPR 2016-11-08: Corregido un bug, el App de eForms normal no envía el nombre de App, así que debe actualizar este campo aunque esté vacío
				//if ($appName != '') {
					$strAdditionalFields .= ", AppName = '{$appName}'";
				//}
				//@JAPR
				if ($appVersion != '') {
					//@JAPR 2016-10-17: Corregido un bug, estaba mal la asignación de este valor y provocaba errores al actualizar
					$strAdditionalFields .= ", FormsVersion = '{$appVersion}'";
					//@JAPR
				}
				//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
				$strAdditionalFields .= ", StoreApp = {$intStoreApp}";
				//@JAPR 2016-10-21: Agregado el campo IsActive para identificar el dispositivo que se encuentra activo en cada cuenta, ya que si dos o mas cuentas entran con el mismo dispositivo,
				//sólo la última debería quedar con dicho dispositivo asignado (IsActive = 1), por lo que se hará un Update a las demás cuentas de ese dispositivo para desactivarlas (IsActive = 0)
				$strAdditionalFields .= ", IsActive = 1";
				//@JAPR 2018-10-11: Agregada la versión de actualización vía inyección de código (#CACG8Y)
				if ($appUpdateVersion != '') {
					$strAdditionalFields .= ", FormsUpdate = '{$appUpdateVersion}'";
				}
				//@JAPR
				
				$sql = "UPDATE SI_SV_UserDevices SET 
						LastDateID = '{$currDate}', 
						RegID = '{$strRegID}', 
						DeviceID = {$intDeviceID}, 
						UserAgent = '{$strUserAgent}' 
						{$strAdditionalFields} 
					WHERE UserID = '{$strUserAccount}' AND UUID = '{$strUUID}'";
				$aRS = @mysql_query($sql, $mysql_Connection);
			}
			else {
				//OMMC 2016-10-11: Agregado el nombre y versión de aplicación  
				//No existía este dispositivo para esta cuenta, así que se inserta en la tabla de registro
				//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
				//@JAPR 2016-10-21: Agregado el campo IsActive para identificar el dispositivo que se encuentra activo en cada cuenta, ya que si dos o mas cuentas entran con el mismo dispositivo,
				//sólo la última debería quedar con dicho dispositivo asignado (IsActive = 1), por lo que se hará un Update a las demás cuentas de ese dispositivo para desactivarlas (IsActive = 0)
				//@JAPR 2018-10-11: Agregada la versión de actualización vía inyección de código (#CACG8Y)
				$sql = "INSERT INTO SI_SV_UserDevices (UserID, UUID, DeviceID, LastDateID, RegID, DeviceName, PhoneNumber, UserAgent, AppName, FormsVersion, 
						StoreApp, IsActive, FormsUpdate) 
					VALUES ('{$strUserAccount}', '{$strUUID}', {$intDeviceID}, '{$currDate}', '{$regID}', '{$deviceName}', '{$phoneNumber}', '{$strUserAgent}', '{$appName}', '{$appVersion}', 
						{$intStoreApp}, 1, '{$appUpdateVersion}')";
				$aRS = @mysql_query($sql, $mysql_Connection);
			}
			
			//@JAPR 2016-10-17: Corregido un bug, estaba mal la asignación de este valor y provocaba errores al actualizar
			//Agregado el estatuto permanentemente en modo de depuración y validado que sólo se muestre el error si realmente hay alguno
			if ((int) @$_REQUEST['DebugBreak'] == 1) {
				echo("<br>\r\n Updating SI_SV_UserDevices: ".$sql."<br>\r\n");
			}
			
			if (mysql_error($mysql_Connection)) {
				if ((int) @$_REQUEST['DebugBreak'] == 1) {
					echo("<br>\r\n Error executing the sql sentence {$sql}, error: ".mysql_error($mysql_Connection)."<br>\r\n");
				}
			}
			else {
				//@JAPR 2016-10-21: Agregado el campo IsActive para identificar el dispositivo que se encuentra activo en cada cuenta, ya que si dos o mas cuentas entran con el mismo dispositivo,
				//sólo la última debería quedar con dicho dispositivo asignado (IsActive = 1), por lo que se hará un Update a las demás cuentas de ese dispositivo para desactivarlas (IsActive = 0)
				//Ahora desactiva el resto de los dispositivos que siendo el mismo recibido en este request están asociados a una cuenta diferente
				//Adicionalmente desactiva el resto de los dispositivos que esta cuenta ha utilizado anteriormente
				if ($intDeviceID > 0) {
					$sql = "UPDATE SI_SV_UserDevices SET 
							IsActive = 0 
						WHERE (UserID <> '{$strUserAccount}' AND UUID = '{$strUUID}') OR (UserID = '{$strUserAccount}' AND UUID <> '{$strUUID}')";
					if ((int) @$_REQUEST['DebugBreak'] == 1) {
						echo("<br>\r\n Disabling this device for other accounts: ".$sql."<br>\r\n");
					}
					$aRS = @mysql_query($sql, $mysql_Connection);
				}
				//@JAPR
			}
			//@JAPR
		}
	}
	catch(Exception $e) {
		//NO se reportarán errores de este proceso
	}
	//@JAPR
	
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
	$intClaUsuario = 0;
	//Si se pudo realizar la conexión al server del DWH, obtiene las configuraciones de eForms
	//En este punto no importa hacer un mysql_select_db porque ya no se accesará nada mas de la FBM000
	$strAlternativeEFormsService = '';
	$strAlternativeEFormsServiceVersion = '';
	$strReqAppVersionForAltService = '3.00000';
	if ($blnGetAlteFormsService && !is_null($mysql_Connection_DWH) && trim($strDWH) != '') {
  		//error_log("\r\nIdentifying alternative eForms server", 3, ".\checkData.log");
		$blnGetAlteFormsService = mysql_select_db($strDWH, $mysql_Connection_DWH);
		if ($blnGetAlteFormsService) {
  			//error_log("\r\nConnection established", 3, ".\checkData.log");
		    $aSQL = "SELECT SettingID, SettingName, SettingValue 
		    	FROM SI_SV_Settings 
		    	WHERE SettingID IN (5, 6, 7)";
		    $aRS = mysql_query($aSQL, $mysql_Connection_DWH);
		    if ($aRS && mysql_num_rows($aRS) != 0) {
				$aRow = mysql_fetch_assoc($aRS);
		    	while($aRow) {
		    		$intSettingID = (int) @$aRow["SettingID"];
		    		$strSettingValue = (string) @$aRow["SettingValue"];
		    		switch ($intSettingID) {
		    			case 5:
		    				$strAlternativeEFormsService = $strSettingValue;
  							//error_log("\r\nformslogsrv Alt Service: " . $strSettingValue, 3, ".\checkData.log");
		    				break;
		    			case 6:
		    				$strAlternativeEFormsServiceVersion = $strSettingValue;
  							//error_log("\r\nformslogsrv Alt Service version: " . $strSettingValue, 3, ".\checkData.log");
		    				break;
		    			case 7:
		    				$strReqAppVersionForAltService = $strSettingValue;
  							//error_log("\r\nformslogsrv Req App Version: " . $strSettingValue, 3, ".\checkData.log");
		    				break;
		    		}
		    		
		    		$aRow = mysql_fetch_assoc($aRS);
		    	}
		    }
			else {
  				//Error en el query para obtener SI_SV_Settings
			}
		}
		
		//@JAPR 2014-06-27:
		try {
			//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
			//@JAPR 2016-07-01: Agregada la variable con el nombre corto del usuario para usar en escenarios
			//@JAPR 2016-12-28: Corregido un bug, al ser un like, estaba permitiendo entrar con cuentas ligeramente diferentes siempre y cuando tuvieran un patrón LIKE válido (#IF5J19)
			$sql = "SELECT A.WHTMLANG, B.TERMINADOR, A.CLA_USUARIO, A.NOM_CORTO 
				FROM ".$strFormsRepository.".SI_USUARIO A 
					LEFT OUTER JOIN ".$strFormsRepository.".SI_IDIOMA B ON A.WHTMLANG = B.CLA_IDIOMA 
				WHERE A.CUENTA_CORREO = '".mysql_real_escape_string($strUserEmail, $mysql_Connection_DWH)."'";
			$aRS = @mysql_query($sql, $mysql_Connection_DWH);
			if ($aRS && mysql_num_rows($aRS) != 0) {
				$aRow = mysql_fetch_assoc($aRS);
				$intLanguage = (int) @$aRow['WHTMLANG'];
				$strTerminator = strtolower(trim((string) @$aRow['TERMINADOR']));
				if ($intLanguage) {
					$arrInfo['language'] = $strTerminator;
				}
				
				//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
				$intClaUsuario = (int) @$aRow['CLA_USUARIO'];
				$strShortName = (string) @$aRow['NOM_CORTO'];
				if ($strShortName) {
					$arrInfo['userLogin'] = $strShortName;
				}
				//@JAPR
			}
			
			//@JAPR 2016-06-20: Agregada información extendida para regresar al App de eForms (#17AHU7)
			if ($intClaUsuario > 0) {
        		//SBF 2016-07-01: Pedir también el permiso en SI_SV_Users.sv_user
				$sql = "SELECT A.GeoFenceLat, A.GeoFenceLng, A.GeoFenceRadius, SV_User 
					FROM ".$strFormsRepository.".SI_SV_Users A 
					WHERE A.CLA_USUARIO = {$intClaUsuario}";
				$aRS = @mysql_query($sql, $mysql_Connection_DWH);
				if ($aRS && mysql_num_rows($aRS) != 0) {
					$aRow = mysql_fetch_assoc($aRS);
					$dblGFLat = (float) @$aRow['GeoFenceLat'];
					$dblGFLong = (float) @$aRow['GeoFenceLng'];
					$dblGFRad = (int) @$aRow['GeoFenceRadius'];
					$arrInfo['gfLatitude'] = $dblGFLat;
					$arrInfo['gfLongitude'] = $dblGFLong;
					$arrInfo['gfRadius'] = abs($dblGFRad);
					//@SBF 2016-07-01: Es usuario válido? No? Regresar error!
					//@JAPR 2016-07-01: Comentando con Luis Roux y José Ángel, este cambio podría afectar a GeoControl, por tanto
					//se dejará comentado para no aplicarlo hasta analizar como resolver esa situación
					/*$arrInfo['SV_User'] = (int) @$aRow['SV_User'];
					if ($intClaUsuario != 1 && $arrInfo['SV_User'] != 1)
					{
						$return['error'] = true;
						$return['errmsg'] = TranslateErrorMsgWithLocale('The user does not have enough permissions');
						//@JAPR 2016-11-02: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
						$return['callSupportMsg'] = true;
						$return['errDetail'] = 'The user does not have enough permissions';
						return $return;
					}*/
					//@JAPR
				}
			}
			//@JAPR
		}
		catch (Exception $e) {
			//No reportará este error, simplemente no regresará idioma
		}
		//@JAPR
	}
    
	//Si la versión del App del request es >= que la mínima requerida para validar servicios alternativos de eForms y si existe dicho servicio,
	//entonces lo regresa, de lo contrario regresará el servicio configurado originalmente
	if ((float) $appVersion >= (float) $strReqAppVersionForAltService && $strAlternativeEFormsService != '') {
	    $formsFullPath = $strAlternativeEFormsService;
	    $formsVersion = $strAlternativeEFormsServiceVersion;
	}
	else {
        $formsFullPath = $strKPIServer.'/'.$strArtusPath.'/'.$strFormsPath;
        $formsVersion = $strFormsVersion;
	}
  	//error_log("\r\nformslgsrv Forms full path: " . $formsFullPath, 3, ".\checkData.log");
    //@JAPR
        
	updateLoginData($strUserEmail,$intUserID,$strDWH,$mysql_Connection);//Rafael Vega-25/04/2013    
	//NO BORRARLO
	//Conchita 2013-05-16 agregado el regID si existe, para grabar en una tabla con el cla_usuario
	
	try{
		if (isset($_REQUEST['regID']) && trim($_REQUEST['regID']) != '')
		{
			$regID = $_REQUEST['regID'];
	  		//error_log("\r\nregID: " . $regID, 3, ".\checkData.log");
			//Conchita 2013-05-16 debe hacerse el insert $strFormsRepository
			//@JAPR 2016-12-28: Corregido un bug, al ser un like, estaba permitiendo entrar con cuentas ligeramente diferentes siempre y cuando tuvieran un patrón LIKE válido (#IF5J19)
			$sqlbmd = "SELECT CLA_USUARIO 
				FROM ".$strFormsRepository.".SI_USUARIO 
				WHERE CUENTA_CORREO = '".mysql_real_escape_string($strUserEmail, $mysql_Connection_DWH)."'";
			//ejecutar sql
			
			//$aRS = @mysql_query($sqlbmd, $mysql_Connection); //$mysql_Connection_DWH
			$aRS = @mysql_query($sqlbmd, $mysql_Connection_DWH);
			//guardar clausuario en $strClaUsuario
			if ($aRS && mysql_num_rows($aRS) != 0) 
			{
				$aRow = mysql_fetch_assoc($aRS);
				$strClaUsuario = (int) @$aRow['CLA_USUARIO'];
	  			//error_log("\r\nstrClaUsuario: " . $strClaUsuario, 3, ".\checkData.log");
				if($strClaUsuario > 0)
				{
					//agregado el phonenum
					//si hay clave de usuario hacemos el grabado en la tabla nueva, primero la creamos
					//@JAPR 2014-04-04: Cambiada de base de datos la tabla de registros móviles para que se haga en la metadata de eForms, además
					//se agregaron los campos DeviceID y DeviceName, se dejó el mismo PRIMARY KEY para forzar a diferentes RegIDs por usuario
					$sqlCreate = "CREATE TABLE SI_SV_RegID (
							DeviceID INT NOT NULL,
							UserID INT NOT NULL,
							RegID VARCHAR (255) NOT NULL,
							PhoneNumber VARCHAR (255) NULL,
							DeviceName VARCHAR(255) NULL,
							PRIMARY KEY (UserID, RegID)
						)";
	  					//error_log("\r\nsqlCreate: " . $sqlCreate, 3, ".\checkData.log");
						
					//ejecutamos el sqlCreate
					//$aRS = @mysql_query($sqlCreate, $mysql_Connection); //$mysql_Connection_DWH
					$aRS = @mysql_query($sqlCreate, $mysql_Connection_DWH); 
					//insert
					$phoneNumber ='';
					//Conchita agregado el phonenumber q se recibe como parametro
					if (isset($_REQUEST['phoneNumber'])) {
						$tmp = (string) @$_REQUEST['phoneNumber']; //temporal antes de ser limpiado
						//se limpia el numero de telefono
						for ($i=0; $i<strlen($tmp); $i++) {
							if (is_numeric($tmp[$i])) {
								$phoneNumber.=$tmp[$i];
							}
						}
					}
					//@error_log(var_dump($mysql_Connection),3,"conchitatest.log");
					
					$deviceName = '';
					if (isset($_REQUEST['deviceName'])) {
						$deviceName = (string) @$_REQUEST['deviceName'];
					}
					
					//agregado phonenumber al query de insercion
					//@JAPR 2014-04-04: Cambiada de base de datos la tabla de registros móviles para que se haga en la metadata de eForms, además
					//se agregaron los campos DeviceID y DeviceName, se dejó el mismo PRIMARY KEY para forzar a diferentes RegIDs por usuario
          
					//@RTORRES Cuando se inserta un nuevo regId se actualiza en el usuario correspondiente
					$sqlSelectUser = "SELECT DeviceID FROM SI_SV_RegID WHERE UserID = {$strClaUsuario}";
					$SelectUserRS = @mysql_query($sqlSelectUser, $mysql_Connection_DWH);
					if ($SelectUserRS && mysql_num_rows($SelectUserRS) != 0) {
						$SelectUserRow = mysql_fetch_assoc($SelectUserRS);
						$intDeviceID = (int) @$SelectUserRow['DeviceID'];
						if ($intDeviceID > 0) {
							$sqlUpdateRegID = "UPDATE SI_SV_RegID SET 
										PhoneNumber = '{$phoneNumber}' 
										, DeviceName = '{$deviceName}'
										, RegID = '{$regID}'    
									WHERE DeviceID = {$intDeviceID}";
								$aRS = @mysql_query($sqlUpdateRegID, $mysql_Connection_DWH);
						}
					} else
					{
						$sqlInsert = "INSERT INTO SI_SV_RegID (DeviceID, UserID, RegID, PhoneNumber, DeviceName) 
							SELECT IFNULL(MAX(DeviceID) +1, 1) AS DeviceID, {$strClaUsuario}, '{$regID}', '{$phoneNumber}', '{$deviceName}' 
							FROM SI_SV_RegID";
						$aRS = @mysql_query($sqlInsert, $mysql_Connection_DWH);
					}
					//////
					/*$sqlInsert = "INSERT INTO SI_SV_RegID (DeviceID, UserID, RegID, PhoneNumber, DeviceName) 
						SELECT IFNULL(MAX(DeviceID) +1, 1) AS DeviceID, {$strClaUsuario}, '{$regID}', '{$phoneNumber}', '{$deviceName}' 
						FROM SI_SV_RegID";
					//ejecutar
					//$aRS = @mysql_query($sqlInsert, $mysql_Connection); //$mysql_Connection_DWH
					$aRS = @mysql_query($sqlInsert, $mysql_Connection_DWH);
					if (!$aRS) {
						//En caso de error se asume que ya existía la combinación UserID y RegID, así que se obtiene el DeviceID y se actualiza
						//el resto de la información
						$sql = "SELECT DeviceID FROM SI_SV_RegID 
							WHERE UserID = {$strClaUsuario} AND RegID = '{$regID}'";
						$aRS = @mysql_query($sql, $mysql_Connection_DWH);
						if ($aRS && mysql_num_rows($aRS) != 0) {
							$aRow = mysql_fetch_assoc($aRS);
							$intDeviceID = (int) @$aRow['DeviceID'];
							if ($intDeviceID > 0) {
								$sqlUpdate = "UPDATE SI_SV_RegID SET 
										PhoneNumber = '{$phoneNumber}' 
										, DeviceName = '{$deviceName}' 
									WHERE DeviceID = {$intDeviceID}";
								$aRS = @mysql_query($sqlUpdate, $mysql_Connection_DWH);
							}
						}
					}*/
				}
			}		
		}
		//Conchita 2013-05-16 
	}
	catch(Exception $e){
	}
	
	unset($row);
	unset($mysql_Connection);

    $return[0] = $formsFullPath;
    $return[1] = $formsVersion;
    $return[2] = $strFormsRepository;
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones (campo DatabaseID)
	$return[3] = $intFormsDatabaseID;
    //@JAPR 2014-03-10: Agregada información extendida para regresar al App de eForms
    if (count($arrInfo)) {
    	$return['info'] = $arrInfo;
    }
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
	if ($intClaUsuario > 0) {
		$return["CLA_USUARIO"] = $intClaUsuario;
	}
	//@JAPR 2016-11-24: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	$return['MultiPass'] = $blnMultiPass;
    //@JAPR
	return $return;
}

//@JAPR 2016-12-22: Agregada la validación de expiración de cuentas de eForms (#LR9E84)
/* Dada la combinación de usuario, password y repositorio, verifica si la cuenta se encuentra expirada o cualquier otro supuesto que debiera bloquear el acceso, o si es válida, regresando
un true en caso de éxito o un objeto/array con errCode y errDesc en caso de error
*/
function ValidateAccountExpiration($sUserID, $sPassword, $sRepositoryName) {
	//JAPRWarning: Si se desea por alguna razón evitar la validación de expiración, simplemente habilitar el return siguiente
	//return true;
	
	if (!$sUserID || !$sPassword || !$sRepositoryName) {
		return array("forceerrordesc" => 1, "errCode" => -1, "errDesc" => TranslateErrorMsgWithLocale('Error, user email empty'));
	}
	
	try {
		ini_set('soap.wsdl_cache_enabled', '0');
		$serverUrl = 'https://kpionline.bitam.com/fbm/kpiLoginService.php?wsdl';
		$client = new SoapClient($serverUrl, array('exceptions' => 1, 'encoding' => 'ISO-8859-1', 'cache_wsdl' => 0));
		//@JAPR 2017-01-11: Agregado el productID para verificar la expiración de cuentas por producto (#LR9E84)
		$jsonRes = (string) $client->checkUserAccessInfo(array("repository" => $sRepositoryName, "email" => $sUserID, "password" => $sPassword, "productID" => 6));
		
		//El resultado es un objeto en formato JSON, así que primero valida que sea un string diferente de vacío y luego realiza la conversión a objeto
		if (!$jsonRes) {
			return array("forceerrordesc" => 1, "errCode" => -5, "errDesc" => TranslateErrorMsgWithLocale('Connection error!'));
		}
		$objValResponse = json_decode($jsonRes);
		$objValResponse = object_to_array($objValResponse);
		
		//Detecta si ocurrió algún error al intentar realizar la validación de expirtación
		if (!$objValResponse || !is_array($objValResponse)) {
			return array("forceerrordesc" => 1, "errCode" => -2, "errDesc" => TranslateErrorMsgWithLocale('Connection error!'));
		}
		
		//Verifica que la respuesta se encuentre correctamente integrada para continuar con la validación, pidiendo como índice el repositorio validado
		if (!isset($objValResponse['repositories']) || !isset($objValResponse['repositories'][$sRepositoryName]) || (isset($objValResponse['errCode']) && $objValResponse['errCode'])) {
			if (isset($objValResponse['errCode']) && $objValResponse['errCode']) {
				return array("forceerrordesc" => 1, "errCode" => (int) @$objValResponse['errCode'], "errDesc" => (string) @$objValResponse['errDesc']);
			}
			else {
				return array("forceerrordesc" => 1, "errCode" => -3, "errDesc" => TranslateErrorMsgWithLocale('Connection error!'));
			}
		}
		
		//Detecta si se cumplió algún tipo de validación de expiración
		$arrRepoRes = $objValResponse['repositories'][$sRepositoryName];
		if (!isset($arrRepoRes['canLogin']) || !$arrRepoRes['canLogin']) {
			if (isset($arrRepoRes['errCode']) && $arrRepoRes['errCode']) {
				return array("forceerrordesc" => 1, "errCode" => (int) @$arrRepoRes['errCode'], "errDesc" => (string) @$arrRepoRes['errDesc'], "expirationDate" => (string) @$arrRepoRes['expirationDate']);
			}
			else {
				return array("forceerrordesc" => 1, "errCode" => -4, "errDesc" => TranslateErrorMsgWithLocale('Error, the user account is not active'), "expirationDate" => (string) @$arrRepoRes['expirationDate']);
			}
		}
		
	} catch (Exception $e) {
		return array("forceerrordesc" => 1, "errCode" => -6, "errDesc" => TranslateErrorMsgWithLocale('Connection error!'));
	}
	
	//Si llega a este punto significa que todo estuvo bien y la combinación si puede hacer login en el repositorio indicado
	return true;
}
//@JAPR

/* serverVerifyMessage */
if (isset($_GET['serverVerifyMessage'])) 
{
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
	global $gbUseOperationsMonitor;
	global $giOperationID;
	
	$gbUseOperationsMonitor = false;
	try {
    //@JAPR 2015-10-19: Registro de requests
    /*if (true) {
      $requestDir = getcwd()."\\"."syncrequests";
      if (!file_exists($requestDir)) {
        @mkdir($requestDir);
      }
      $requestDir .= "\\Requests.log";
      $strUserAgent = @var_export(@$_SERVER['HTTP_USER_AGENT'], true);
        //@JAPR 2015-03-05: Agregada la fecha al log de requests
      @error_log(date('Y-m-d H:i:s')."- ".$strUserAgent."\r\n", 3, $requestDir);
      @error_log(date('Y-m-d H:i:s')."- serverVerifyMessage"."\r\n", 3, $requestDir);
      $strRequestVars = @var_export($_REQUEST, true);
      @error_log($strRequestVars."\r\n", 3, $requestDir);
    }*/
    //@JAPR
  
		$giOperationID = 0;
		clearstatcache();
		$strOperationsMonitorFile = '../../fbm/operationsMonitor.inc.php';
		if (file_exists($strOperationsMonitorFile)) {
			@require_once($strOperationsMonitorFile);
			if (function_exists("RegisterOperation")) {
				$gbUseOperationsMonitor = true;
			}
		}
		
		if ($gbUseOperationsMonitor) {
			global $arrEventParams;
			
			$strUserEMail = (string) @$_REQUEST["UserID"];
			$arrEventParams = array();
			$arrEventParams["ProductID"] = 6;	//eForms
			$arrEventParams["ProductVersion"] = (float) @$_REQUEST["appVersion"];
			$arrEventParams["StartDate"] = date('Y-m-d H:i:s');
			$arrEventParams["Email"] = $strUserEMail;
			$arrEventParams["ProcessID"] = 300; //procServiceHello;
			$arrEventParams["AplicationServer"] = $_SERVER["SERVER_NAME"];
			$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
			$strScriptPath = $path_parts["dirname"];
			$arrEventParams["Ruta"] = $strScriptPath;
			$arrEventParams["MemoryCon"] = memory_get_peak_usage(true);
			$arrEventParams["Description"] = "Service Hello for ".$strUserEMail;
			//Este request no se hace asociado a ninguna BD, de hecho ni tiene acceso a la misma, así que esta información no es necesario asignarla
			//pero se envía como NULL para que la propia función no intente cargarla
			$arrEventParams["UserID"] = null;
			$arrEventParams["DBid"] = null;
			$arrEventParams["Repository"] = null;
			//@JAPR 2015-03-25: Agregados los datos de geolocalización
			if (isset($_REQUEST["syncLat"]) && (int)$_REQUEST["syncLat"] > 0) {
				$arrEventParams["Latitude"] = $_REQUEST["syncLat"];
			}
			if (isset($_REQUEST["syncLong"]) && (int)$_REQUEST["syncLong"] > 0) {
				$arrEventParams["Longitude"] = $_REQUEST["syncLong"];
			}
			if (isset($_REQUEST["syncAcc"])) {
				$arrEventParams["Accuracy"] = $_REQUEST["syncAcc"];
			}
			//@JAPR
			$giOperationID = (int) @RegisterOperation($arrEventParams);
		}
	} catch (Exception $e) {
	}
	//@JAPR
	
	//$return = array();
	$return['hellomsg'] = 'Hello';
	
	//@MABH20121210
	$return['serverSurveyDate'] = date("Y-m-d");
	$return['serverSurveyHour'] = date("H:i:s");
	
	header("Content-Type: text/javascript");
	if (isset($_GET['jsonpCallback'])) 
	{
		echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
	} 
	else 
	{
		echo(json_encode($return));
	}
	
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
	try {
		if ($gbUseOperationsMonitor) {
			$arrEventParams["MemoryPico"] = memory_get_peak_usage(true);
			$arrEventParams["EndDate"] = date('Y-m-d H:i:s');
			@RegisterOperation($arrEventParams, $giOperationID);
		}
	} catch (Exception $e) {
	}
	//@JAPR
	
	exit();
}

//JAPR 2016-05-20: Agregada la configuración para envío de GPS constante desde el App (#WRWF3Y)
/* Este servicio registrará un archivo en la carpeta eFormsAppData por cada request enviado desde un App, usando un nombre que
permita identificar de manera única al App por usuario y fecha, bajo el entendido que sólo se puede hacer este request con un mínimo
de 60 segundos de diferencia entre uno y otro, así que ningún archivo podría colisionar en nombre, fecha y hora, pero se va a usar
de todas maneras el UUID del equipo para hacer diferencia por si estuvieran compartiendo cuentas entre diferentes dispositivos
*/
if (isset($_GET['updateAppData'])) {
	global $gbUseOperationsMonitor;
	global $giOperationID;
	global $blnEncodeUTF8;
	
	$return = array();
	$return['error'] = false;
	
	$blnError = false;
	clearstatcache();
	$strFormsAppDataPath = getcwd()."\\eFormsAppData";
	if (!file_exists($strFormsAppDataPath)) {
		if (!mkdir($strFormsAppDataPath, null, true)) {
			$blnError = true;
			$return['error'] = true;
			$return['errmsg'] = TranslateErrorMsgWithLocale('Error creating the app data path in the KPIOnline Server');
			//@JAPR 2016-11-07: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
			$return['callSupportMsg'] = true;
			$return['errDetail'] = 'Error creating the app data path in the KPIOnline Server';
			//@JAPR
		}
	}
	
	if (!$blnError) {
		$kpiUser = $_GET['UserID'];
		//@JAPR 2016-06-30: Almacenar el password encriptado (#VRNM0H)
		$appVersion = (float) @$_REQUEST['appVersion'];
		if (is_null($appVersion) || trim($appVersion) == '' || !is_numeric($appVersion)) {
			$appVersion = 1.0;
		}
		
		$build = (string) @$_REQUEST['build'];
		
		$kpiPass = $_GET['Password'];
		//Si el password recibido viene de una versión 6.03000+ y de un build >= 2016-07-02, entonces se asume que viene codificado con btoa y encodeURIComponent, así que se decodifica
		if ($appVersion >= 6.03000 && $build >= '20160702000000') {
			$kpiPass = rawurldecode(base64_decode($kpiPass));
		}
		//@JAPR
		$strUUID = $_REQUEST['uuid'];
		$strData = json_encode($_REQUEST);
		$return['serverSurveyDate'] = date("Y-m-d");
		$return['serverSurveyHour'] = date("H:i:s");
		//El nombre de archivo que se utilizará será la combinación de los datos que identifican al usuario en este punto del tiempo (según el server)
		$strFileName = $kpiUser."_".str_replace("-", "", $return['serverSurveyDate'])."_".str_replace(":", "", $return['serverSurveyHour'])."_".$strUUID;
		$intTries = 0;
		$strSuffix = '';
		$strExt = '.log';
		do {
			$intTries++;
			$blnFileExists = file_exists($strFormsAppDataPath."\\".$strFileName.$strSuffix.$strExt);
			if ($blnFileExists) {
				$strSuffix = '_'.$intTries;
			}
			else {
				@error_log($strData, 3, $strFormsAppDataPath."\\".$strFileName.$strSuffix.$strExt);
			}
		} while($blnFileExists && $intTries < 100);
	}
	
	header("Content-Type: text/javascript");
	if (isset($_GET['jsonpCallback'])) 
	{
		echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
	} 
	else 
	{
		echo(json_encode($return));
	}
}
//JAPR

if (isset($_GET['getPathVersion'])) 
{
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
	global $gbUseOperationsMonitor;
	global $giOperationID;
	global $blnEncodeUTF8;
	
	$gbUseOperationsMonitor = false;
	try {
    //@JAPR 2015-10-19: Registro de requests
    /*if (true) {
      $requestDir = getcwd()."\\"."syncrequests";
      if (!file_exists($requestDir)) {
        @mkdir($requestDir);
      }
      $requestDir .= "\\Requests.log";
      $strUserAgent = @var_export(@$_SERVER['HTTP_USER_AGENT'], true);
        //@JAPR 2015-03-05: Agregada la fecha al log de requests
      @error_log(date('Y-m-d H:i:s')."- ".$strUserAgent."\r\n", 3, $requestDir);
      @error_log(date('Y-m-d H:i:s')."- getPathVersion"."\r\n", 3, $requestDir);
      $strRequestVars = @var_export($_REQUEST, true);
      @error_log($strRequestVars."\r\n", 3, $requestDir);
    }*/
    //@JAPR
  
		$giOperationID = 0;
		clearstatcache();
		$strOperationsMonitorFile = '../../fbm/operationsMonitor.inc.php';
		if (file_exists($strOperationsMonitorFile)) {
			@require_once($strOperationsMonitorFile);
			if (function_exists("RegisterOperation")) {
				$gbUseOperationsMonitor = true;
			}
		}
		
		if ($gbUseOperationsMonitor) {
			global $arrEventParams;
			
			$strUserEMail = (string) @$_REQUEST["UserID"];
			$arrEventParams = array();
			$arrEventParams["ProductID"] = 6;	//eForms
			$arrEventParams["ProductVersion"] = (float) @$_REQUEST["appVersion"];
			$arrEventParams["StartDate"] = date('Y-m-d H:i:s');
			$arrEventParams["Email"] = $strUserEMail;
			$arrEventParams["ProcessID"] = 301; //procAuthenticateAcc;
			$arrEventParams["AplicationServer"] = $_SERVER["SERVER_NAME"];
			$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
			$strScriptPath = $path_parts["dirname"];
			$arrEventParams["Ruta"] = $strScriptPath;
			$arrEventParams["MemoryCon"] = memory_get_peak_usage(true);
			$arrEventParams["Description"] = "User authentication for ".$strUserEMail;
			//Este request no se hace asociado a ninguna BD, de hecho ni tiene acceso a la misma, así que esta información no es necesario asignarla
			//pero se envía como NULL para que la propia función no intente cargarla
			$arrEventParams["UserID"] = null;
			$arrEventParams["DBid"] = null;
			$arrEventParams["Repository"] = null;
			//@JAPR 2015-03-25: Agregados los datos de geolocalización
			if (isset($_REQUEST["syncLat"]) && (int)$_REQUEST["syncLat"] > 0) {
				$arrEventParams["Latitude"] = $_REQUEST["syncLat"];
			}
			if (isset($_REQUEST["syncLong"]) && (int)$_REQUEST["syncLong"] > 0) {
				$arrEventParams["Longitude"] = $_REQUEST["syncLong"];
			}
			if (isset($_REQUEST["syncAcc"])) {
				$arrEventParams["Accuracy"] = $_REQUEST["syncAcc"];
			}
			//@JAPR
			$giOperationID = (int) @RegisterOperation($arrEventParams);
		}
	} catch (Exception $e) {
	}
	//@JAPR
	
	if (!isset($_GET['UserID'])) {
		//@JAPR 2016-11-07: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		//@JAPR 2016-11-07: Corregido un bug, regresar este header provocaba un timeout en las Apps en lugar de mostrar el alert esperado, se cambió para estandarizar el reporte de errores
		/*header('HTTP/1.0 401 Unauthorized');
		echo 'You must enter a valid UserID to access this resource';*/
		$return['error'] = true;
		$return['errmsg'] = TranslateErrorMsgWithLocale('USER OR PASSWORD INVALID');
		$return['errDetail'] = 'USER OR PASSWORD INVALID';
		
		//@JAPR 2016-11-07: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		//Corregido un bug, faltaba regresar la respuesta como función, así que se estaba provocando un error al parsearla en el App
		header("Content-Type: text/javascript");
		if (isset($_GET['jsonpCallback'])) 
		{
			echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
		} 
		else 
		{
			echo(json_encode($return));
		}
		
		//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
		if ($gbUseOperationsMonitor) {
			closeOperationWithError($arrEventParams, 'USER OR PASSWORD INVALID');
		}
		//@JAPR
		exit();
	}
	$kpiUser = $_GET['UserID'];

	if (!isset($_GET['Password'])) {
		//@JAPR 2016-11-07: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		//@JAPR 2016-11-07: Corregido un bug, regresar este header provocaba un timeout en las Apps en lugar de mostrar el alert esperado, se cambió para estandarizar el reporte de errores
		/*header('HTTP/1.0 401 Unauthorized');
		echo 'You must enter a valid Password to access this resource';*/
		$return['error'] = true;
		$return['errmsg'] = TranslateErrorMsgWithLocale('USER OR PASSWORD INVALID');
		//@JAPR 2016-11-07: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		$return['errDetail'] = 'USER OR PASSWORD INVALID';
		
		//@JAPR 2016-11-07: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		//Corregido un bug, faltaba regresar la respuesta como función, así que se estaba provocando un error al parsearla en el App
		header("Content-Type: text/javascript");
		if (isset($_GET['jsonpCallback'])) 
		{
			echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
		} 
		else 
		{
			echo(json_encode($return));
		}
		
		//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
		if ($gbUseOperationsMonitor) {
			closeOperationWithError($arrEventParams, 'USER OR PASSWORD INVALID');
		}
		//@JAPR
		exit();
	}
	//@JAPR 2016-06-30: Almacenar el password encriptado (#VRNM0H)
	$appVersion = (float) @$_REQUEST['appVersion'];
	if (is_null($appVersion) || trim($appVersion) == '' || !is_numeric($appVersion)) {
		$appVersion = 1.0;
	}
	
	$build = (string) @$_REQUEST['build'];
	
	$kpiPass = $_GET['Password'];
	//Si el password recibido viene de una versión 6.03000+ y de un build >= 2016-07-02, entonces se asume que viene codificado con btoa y encodeURIComponent, así que se decodifica
	if ($appVersion >= 6.03000 && $build >= '20160702000000') {
		$kpiPass = rawurldecode(base64_decode($kpiPass));
	}
	//@JAPR
	
	//Se obtiene el formspath y formsversion
	$arrayFormsInfo = getFormsInfo($kpiUser, $kpiPass);
	if(isset($arrayFormsInfo['error']))
	{
		$return['error'] = true;
		
		//@JAPR 2016-11-02: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		if (isset($arrayFormsInfo["callSupportMsg"])) {
			$return["callSupportMsg"] = $arrayFormsInfo["callSupportMsg"];
		}
		if (isset($arrayFormsInfo["errDetail"])) {
			$return["errDetail"] = $arrayFormsInfo["errDetail"];
		}
		
		//@JAPR 2015-03-25: Agregada la validación para saber si debe o no convertir a utf8
		//El archivo ya es utf8, así que no hay necesidad de codificar en ninguna versión que haga el request
		//SBF 2016-07-01: Pedir también el permiso en SI_SV_Users.sv_user
		//@JAPR 2016-07-01: Comentando con Luis Roux y José Ángel, este cambio podría afectar a GeoControl, por tanto
		//se dejará comentado para no aplicarlo hasta analizar como resolver esa situación
		/*if (isset($arrayFormsInfo['errmsg'])) {
			$return['errmsg'] = $arrayFormsInfo['errmsg'];
		}
		else {*/
		//@JAPR 2016-12-22: Agregada la validación de expiración de cuentas de eForms (#LR9E84)
		//Sólo en caso de que se trate de un error de validación de expiración es como se mostrará el error exacto, cualquier otra situación regresará siempre el mismo error
		if (isset($arrayFormsInfo['forceerrordesc']) && $arrayFormsInfo['forceerrordesc']) {
			$return['errmsg'] = $arrayFormsInfo['errmsg'];
		}
		else {
			$return['errmsg'] = TranslateErrorMsgWithLocale('USER OR PASSWORD INVALID');
		}
		//@JAPR 2016-11-07: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		//$return['errDetail'] = 'USER OR PASSWORD INVALID';
		//@JAPR
		//}
		//@JAPR
		header("Content-Type: text/javascript");
		if (isset($_GET['jsonpCallback'])) 
		{
			echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
		} 
		else 
		{
			echo(json_encode($return));
		}
		//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
		if ($gbUseOperationsMonitor) {
			closeOperationWithError($arrEventParams, 'USER OR PASSWORD INVALID');
		}
		//@JAPR
		exit();
	}
	
	$strFullPath = $arrayFormsInfo[0];
	$strFormsVersion = $arrayFormsInfo[1];
	$strFormsRepository = (string) @$arrayFormsInfo[2];
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
	$intFormsDatabaseID = (int) @$arrayFormsInfo[3];
	$intClaUsuario = (int) @$arrayFormsInfo["CLA_USUARIO"];
	//@JAPR
	$strFormsPub = '';

	$return['error'] = false;
	$return['formsFullPath'] = $strFullPath;
	$return['formsVersion'] = $strFormsVersion;
	$return['repository'] = $strFormsRepository;
	$return['formsPub'] = $strFormsPub;
	//@JAPR 2014-03-10: Agregada información extendida para regresar al App de eForms
	if (isset($arrayFormsInfo['info'])) {
		$return['info'] = $arrayFormsInfo['info'];
		//@JAPR 2016-07-21: Agregada la variable con el password encriptado para que esté disponible en cualquier versión 6.0+ del App
		/*if ($appVersion >= 6.00000) {
			$return['info']['passwordEnc'] = BITAMFormsEncode(BITAMFormsEncryptPassword($kpiPass));
		}*/
		//@JAPR
	}
	//@JAPR 2016-05-20: Corregido el proceso que determina la fecha y hora del servidor (#LR773Y)
	$return['serverSurveyDate'] = date("Y-m-d");
	$return['serverSurveyHour'] = date("H:i:s");
	
	//@JAPR 2016-11-24: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	$return['MultiPass'] = (int) @$arrayFormsInfo['MultiPass'];
	//@JAPR
	
	header("Content-Type: text/javascript");
	if (isset($_GET['jsonpCallback'])) 
	{
		echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
	} 
	else 
	{
		echo(json_encode($return));
	}
	
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
	try {
		if ($gbUseOperationsMonitor) {
			//En este punto ya se puede sobreescribir la información del repositorio sobre el cual se pide la autentificación
			if (!is_null($intClaUsuario) && (int) $intClaUsuario > 0) {
				$arrEventParams["UserID"] = $intClaUsuario;
			}
			$arrEventParams["DBid"] = $intFormsDatabaseID;
			$arrEventParams["Repository"] = $strFormsRepository;
			$arrEventParams["MemoryPico"] = memory_get_peak_usage(true);
			$arrEventParams["EndDate"] = date('Y-m-d H:i:s');
			@RegisterOperation($arrEventParams, $giOperationID);
		}
	} catch (Exception $e) {
	}
	//@JAPR
	
	exit();
}

//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
//Cierra el registro del request debido a un error, no se registrarán detalles adicionales en el request, sólo valida si se logró asignar el UserID ya que en
//caso de no recibirlo deberá asignar otras propiedades para evitar que se carguen automáticamente (no habría elementos suficientes para cargar dicha info)
function closeOperationWithError($anEventParams, $sErrorDesc = '') {
	global $gbUseOperationsMonitor;
	global $giOperationID;
	
	try {
		if ($gbUseOperationsMonitor) {
			$anEventParams["MemoryPico"] = memory_get_peak_usage(true);
			$anEventParams["EndDate"] = date('Y-m-d H:i:s');
			if ($sErrorDesc != '') {
				$anEventParams["Description"] .= " ({$sErrorDesc})";
			}
			@RegisterOperation($anEventParams, $giOperationID);
		}
	} catch (Exception $e) {
	}
}
//@JAPR

//Rafael Vega funcion para agregar registro de login	
function updateLoginData($strUserEmail,$intUserID,$strDWH,$mysql_Connection)
{
	  global $masterdbname;	
      
	  $strLongitude=0;
	  $strLatitude=0;	  
	  $aSQL = sprintf("SELECT userid FROM $masterdbname.saas_databases WHERE DataWarehouse='%s'", $strDWH);
	  
	  $result= mysql_query($aSQL,$mysql_Connection);
	  	  
	  $row=mysql_fetch_assoc($result);
	  $OwnerUserID=$row["userid"];
	  
	  require_once ('../../fbm/globals.inc.php');
	  
	  $arrInfoLoc= getIPAddressInfoGeo($_SERVER['REMOTE_ADDR'],'eForms');
	  $strLongitude=$arrInfoLoc["longitude"];
	  $strLatitude=$arrInfoLoc["latitude"];     
	  	  	  
      $LoginSQL= "INSERT INTO $masterdbname.saas_login_data (UserloginID,UserLoginNickname,UserOwnerID,Product, Longitude, Latitude, DateLogin,TimeLogin) 
	  VALUES($intUserID,'$strUserEmail',$OwnerUserID,'eForms',$strLongitude,$strLatitude,CURDATE(), CURTIME())"; 
      if(mysql_query($LoginSQL, $mysql_Connection))
	   {
	       $LoadDate=date("d-m-Y H:i:s");
		   $lastloginID=mysql_insert_id($mysql_Connection);
		   
		    $logindata= array(
			"LoadDate"=> $LoadDate,
			"LoginID"=> $lastloginID,
			"UserLoginNickname" =>$strUserEmail,
			"UserloginID" => $intUserID,
			"UserOwnerID" => $OwnerUserID,
			"Product" => 'eForms',
			"latitude" => $strLatitude,
			"longitude" => $strLongitude
			);
			
		    include("../../fbm/LoadCube.inc.php");   
		    UpdateCube($logindata,$mysql_Connection);
		   			
	   }
	  else
	   {  
	      @error_log ( "...Fallo la insercion de registro de login en eforms, query:$LoginSQL \r\n",  3 ,  "ErrorLoginsStadistics.log" );
	   }
	   mysql_close($mysql_Connection);
}
//Rafael Vega

//@JAPR 2016-01-18: Agregado el UUID y RegID a la tabla de identificación de usuarios para controlar los logis por dispositivo (sólo vía móvil) (#QOKOHB)
//@JAPRWarning: Estas mismas funciones se encuentran en el archivo utils.inc.php de eForms, si se modifican aquí hay que replicar el cambio también allá y visceversa
function identifyDeviceType() {
	//Primero intenta identificar el dispositivo mediante el parámetro directo que podría usar las constantes de Android/IOs
	$intDeviceID = @$_REQUEST['deviceID'];
	if (!is_null($intDeviceID)) {
		//Si no es NULL entonces ya se estaba enviando el parámetro, así que no tiene necesidad de calcularlo
		return $intDeviceID;
	}
	//@JAPR
	
	$intDeviceID = dvcWeb;
	$strPlatform = strtolower(trim((string) @$_REQUEST['platform']));
    
    //2015.01.23 JCEM #YAE927 Se cambia la forma de identificar los dispositivos iOS
	switch ($strPlatform) {
        case 'iphone simulator':
            $intDeviceID = dvciPhone;
			break;
		case 'ipad simulator':
			$intDeviceID = dvciPad;
			break;
       	case 'ipod':
		case 'ipad':
		case 'iphone':
            //2015.01.23 JCEM siendo dispositivos ios hay que detectar las diferencias de los diversos modelos, 
            //2015.01.23 JCEM En un futuro se puede ocupar, determina el modelo exacto del dispositivo ios function iOSDeviceModelByMachineID($sMachineID)
            $strModel =  trim((string) @$_REQUEST['model']);
            //2015.01.28 JCEM si por alguna circunstancia del destino el model no llega 
            //se toma el camino anterior por platform
            if ($strModel != ''){
                $intDeviceID = iOSDeviceTypeByMachineID($strModel);
            }else{
                if ($strPlatform == 'ipod') {$intDeviceID = dvciPod;}
                if ($strPlatform == 'iphone') {$intDeviceID = dvciPhone;}
                if ($strPlatform == 'ipad') {$intDeviceID = dvciPad;}
            }
            
            break;
		case 'android':
			//En el caso de android no basta con el platform ya que todos los dispositivos dicen Android, así que se comparará el screenSize
			//para determinar la posibilidad de que se trate de un celular o una tablet, tomando como referencia la menor de las resoluciones
			//recibidas independientemente de la orientación en que fue enviada
			$strScreenSize = strtolower(trim((string) @$_REQUEST['screenSize']));
			$intDeviceID = identifyAndroidDeviceByWidth($strScreenSize);
			break;
			
		//@JAPR 2015-08-14: Corregido un bug, a partir de la última actualización de Cordova (eForms v6) ya no se recibe el platform con el tipo de dispositivos de IOs correcto
		//sino que se recibe sólo "iOS", así que hay que determinar el tipo de DeviceID de otra manera
		case 'ios':
			//A partir de la actualización inicial de Cordova en v6, hay que identificar el dispositivo por el Screen Size ya que el platform llega como iOs independientemente
			//del dispositivo específico
			$strScreenSize = strtolower(trim((string) @$_REQUEST['screenSize']));
			$intDeviceID = identifyIOsDeviceByWidth($strScreenSize);
			break;
			//@JAPR
			
		case 'browser':
			$intDeviceID = dvcWeb;
			break;
			
		default:
			//Por default se debería considerar Web para que mínimo regrese las propiedades originales, sin embargo se hará un último intento
			//buscando por palabras clave
			if (strpos($strPlatform, 'ipod') !== false) {
				$intDeviceID = dvciPod;
			}
			elseif (strpos($strPlatform, 'ipad') !== false) {
				$intDeviceID = dvciPad;
			}
			elseif (strpos($strPlatform, 'iphone') !== false) {
				$intDeviceID = dvciPhone;
			}
			elseif (strpos($strPlatform, 'android') !== false) {
				//En el caso de android no basta con el platform ya que todos los dispositivos dicen Android, así que se comparará el screenSize
				//para determinar la posibilidad de que se trate de un celular o una tablet, tomando como referencia la menor de las resoluciones
				//recibidas independientemente de la orientación en que fue enviada
				$strScreenSize = strtolower(trim((string) @$_REQUEST['screenSize']));
				$intDeviceID = identifyAndroidDeviceByWidth($strScreenSize);
			}
			break;
	}
	
	return $intDeviceID;
}

function iOSDeviceTypeByMachineID($sMachineID){
    //por compatibilidad
    $res = dvcWeb;
    
    switch ($sMachineID){
        case "iPhone1,1": 
        case "iPhone1,2":  
        case "iPhone2,1":  
        case "iPhone3,1":  
        case "iPhone3,2":  
        case "iPhone3,3":  
        case "iPhone4,1":
        case "iPhone5,1": 
        case "iPhone5,2": 
        case "iPhone5,3": 
        case "iPhone5,4": 
        case "iPhone6,1": 
        case "iPhone6,2": 
        case "iPhone7,1": 
        case "iPhone7,2": 
            $res = dvciPhone;
            break;
        case "iPod1,1": 
        case "iPod2,1": 
        case "iPod3,1": 
        case "iPod4,1": 
        case "iPod5,1": 
            $res = dvciPod;
            break;   
        case "iPad1,1":  
        case "iPad2,1":  
        case "iPad2,2":  
        case "iPad2,3":  
        case "iPad2,4":  
        case "iPad3,1":  
        case "iPad3,2":  
        case "iPad3,3":  
        case "iPad3,4":  
        case "iPad3,5":  
        case "iPad3,6":  
        case "iPad4,1":  
        case "iPad4,2":  
        case "iPad4,3":  
            $res = dvciPad;
            break;   
        case "iPad2,5":  
        case "iPad2,6":  
        case "iPad2,7":  
        case "iPad4,4":  
        case "iPad4,5":  
        case "iPad4,6":  
            $res = dvciPadMini;
            break;
        case "x86_64":  //simulador x64
        case "i386":    //simulador x86
        default:
           $res = dvcWeb;
           break;
    }
    return $res;
}

/* Basado en un string en formato width_height recibido desde un dispositivo previamente identificado como Android, determina la menor de las
resoluciones ya sea width o height independientemente de la orientación, para comparar contra el menos valor preestablecido que se consideraría
como un teléfono Android y determinar así si se trata de una Tablet o un teléfono
*/
function identifyAndroidDeviceByWidth($sScreenSize) {
	$intDeviceID = dvcTablet;
	
	if ($sScreenSize != '') {
		$arrSize = explode('_', $sScreenSize);
		$intWidth = (int) @$arrSize[0];
		$intHeigh = (int) @$arrSize[1];
		if ($intWidth > $intHeigh) {
			$intWidth = $intHeigh;
		}
		
		if ($intWidth < 500) {
			$intDeviceID = dvcCel;
		}
	}
	
	return $intDeviceID;
}

/* Basado en un string en formato width_height recibido desde un dispositivo previamente identificado como IOs, determina la menor de las
resoluciones ya sea width o height independientemente de la orientación, para comparar contra el menos valor preestablecido que se consideraría
como un teléfono Android y determinar así si se trata de una Tablet o un teléfono
*/
function identifyIOsDeviceByWidth($sScreenSize) {
	$intDeviceID = dvciPad;
	
	if ($sScreenSize != '') {
		$arrSize = explode('_', $sScreenSize);
		$intWidth = (int) @$arrSize[0];
		$intHeigh = (int) @$arrSize[1];
		if ($intWidth > $intHeigh) {
			$intWidth = $intHeigh;
		}
		
		if ($intWidth < 500) {
			$intDeviceID = dvciPhone;
		}
	}
	
	return $intDeviceID;
}

//JAPR 2016-06-30: Almacenar el password encriptado (#VRNM0H)
function BITAMFormsEncode($inString)
{

	if (is_null($inString) || strlen($inString) == 0)
	{
		return $inString;
	}

	$encryptTable = 'S7H25sy8XFqjJIwxZNp1CvdEe_9u/PiW63TtOkGQMlgRbVhnmzf4oaBLDUYA0cKr';
	$inPos = 0;
	$outPos = 0;
	$inLen = strlen($inString);
	$outLen = floor(($inLen + 2) / 3) * 4;
	$outArray = array();
	array_pad($outArray, $outLen, '');

	for ($i = 0; $i < floor($inLen / 3); $i++)
	{
		$c1 = ord($inString[$inPos]) & 0xFF;
		$inPos++;
		$c2 = ord($inString[$inPos]) & 0xFF;
		$inPos++;
		$c3 = ord($inString[$inPos]) & 0xFF;
		$inPos++;
		$outArray[$outPos] = $encryptTable[($c1 & 0xFC) >> 2];
		$outPos++;
		$outArray[$outPos] = $encryptTable[(($c1 & 0x03) << 4) | (($c2 & 0xF0) >> 4)];
		$outPos++;
		$outArray[$outPos] = $encryptTable[(($c2 & 0x0F) << 2) | (($c3 & 0xC0) >> 6)];
		$outPos++;
		$outArray[$outPos] = $encryptTable[$c3 & 0x3F];
		$outPos++;
	}
	switch($inLen % 3)
	{	case 1:
			$c1 = ord($inString[$inPos]) & 0xFF;
			$inPos++;
			$outArray[$outPos] = $encryptTable[($c1 & 0xFC) >> 2];
			$outPos++;
			$outArray[$outPos] = $encryptTable[($c1 & 0x03) << 4];
			$outPos++;
			$outArray[$outPos] = '|';
			$outPos++;
			$outArray[$outPos] = '|';
			$outPos++;
			break;
		case 2:
			$c1 = ord($inString[$inPos]) & 0xFF;
			$inPos++;
			$c2 = ord($inString[$inPos]) & 0xFF;
			$inPos++;
			$outArray[$outPos] = $encryptTable[($c1 & 0xFC) >> 2];
			$outPos++;
			$outArray[$outPos] = $encryptTable[(($c1 & 0x03) << 4) | (($c2 & 0xF0) >> 4)];
			$outPos++;
			$outArray[$outPos] = $encryptTable[($c2 & 0x0F) << 2];
			$outPos++;
			$outArray[$outPos] = '|';
			$outPos++;
			break;
	}
	$outString = '';
	for ($i = 0; $i < count($outArray); $i++)
	{
		$outString .= $outArray[$i];
	}
	return $outString;
}

function BITAMFormsDecode($inString)
{
	$decryptTable = "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\034".
					"\074\023\003\041\063\004\040\001\007\032\000\000\000\000\000\000".
					"\000\073\066\024\070\027\011\046\002\015\014\076\067\050\021\044".
					"\035\047\053\000\042\071\055\037\010\072\020\000\000\000\000\031".
					"\000\065\054\075\026\030\062\052\056\036\013\045\051\060\057\064".
					"\022\012\077\005\043\033\025\016\017\006\061\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000";				
	$inPos  = 0;
	$outPos = 0;
	$inLen = strlen($inString);
	$outLen = floor($inLen / 4) * 3;
	$outArray = array();
	array_pad($outArray, $outLen, 'X');

	for ($i = 0; $i < floor($inLen / 4); $i++) 
	{
		$c1 = ord($decryptTable[ord($inString[$inPos])]);
		$inPos++;
		$c2 = ord($decryptTable[ord($inString[$inPos])]);
		$inPos++;
		$c3 = ord($decryptTable[ord($inString[$inPos])]);
		$inPos++;
		$c4 = ord($decryptTable[ord($inString[$inPos])]);
		$inPos++;

		$outArray[$outPos] = chr((($c1 << 2) & 0xFC) | (($c2 >> 4) & 0x03));
		$outPos++;
		$outArray[$outPos] = chr((($c2 << 4) & 0xF0) | (($c3 >> 2) & 0x0F));
		$outPos++;
		$outArray[$outPos] = chr((($c3 << 6) & 0xC0) | ($c4 & 0x3F)); 
		$outPos++;
	}

	$outString = '';
	for ($i = 0; $i < count($outArray); $i++)
	{
		if (ord($outArray[$i]) !== 000)
		{
			$outString .= $outArray[$i];
		}
	}

	return $outString;
}

function BITAMFormsLiberaInValidChars($aString, &$aPassPos, &$aPassChar)
{
	while (BITAMFormsHasInvalidChar($aString) !==false)
	{
		$iPos  = BITAMFormsHasInvalidChar($aString);
		$sChar = substr($aString, $iPos, 1);

		$aPassPos[]   = $iPos;
		$aPassChar[]  = $sChar;

		$sS1     = BITAMGetSiguiente($aString, $sChar);
		$aString = $sS1.$aString;
	}
	return($aString);
}

function BITAMFormsReturnInValidChars($aString, $aPassPos, $aPassChar)
{
	$j = count($aPassPos);
	for($i=0; $i<$j; $i++)
	{
		$iPos  = $aPassPos[$i] + $i;
		$sChar = $aPassChar[$i];

		$sS1  = substr($aString, 0, $iPos);
		$sNew = $sS1.$sChar.substr($aString, $iPos);
		$aString = $sNew;
	}
	return($aString);
}

function BITAMFormsEncryptPassword($inString)
{
	$inString = utf8_decode($inString);
	$STRVALID     = utf8_decode("!¡#$%&'()*+,-./0123456789:;<=>?¿@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]_abcdefghijklmnopqrstuvwxyz");
	$aPassPos     = array();
	$aPassChar    = array();
	$inString0rig = $inString;
	$inString     = trim($inString);
	$inString = str_replace(' ','_#ArtusSpace#_', $inString );
	if ($inString == "")
	{
		return "";
	}
	elseif (BITAMFormsHasInvalidChar($inString)!==false)
	{   //EOAC 16Abr09 Soportar caracteres invalidos
		$inString=BITAMFormsLiberaInValidChars($inString, $aPassPos, $aPassChar);
		if($inString=='') return($inString0rig); //Passwords con puros caracteres invalidos se regresaran =
		//return "";
	}

	$movPos = rand(1, strlen($STRVALID));

	$outString = "";
	$n = strlen($inString);
	for ($i = 0; $i < $n; $i++)
	{
		$currentChar = $inString[$i];
		$newPos = strpos($STRVALID, $currentChar) + 1 + $movPos;
		If ($newPos > strlen($STRVALID))
		{
			$newPos = $newPos - strlen($STRVALID);
		}
		$outString .= $STRVALID[$newPos - 1];
	}
	if(strlen($inString)>0) $outString .= $inString[0];
	if(count($aPassChar)>0) //EOAC 16Abr09 Soportar caracteres invalidos
	{
		$outString=BITAMFormsReturnInValidChars($outString, $aPassPos, $aPassChar);
	}
	if(strlen($inString0rig)>2 && strpos(strtoupper($outString),  strtoupper($inString0rig))!==false)
	{
		$outString=BITAMFormsEncryptPassword($inString0rig);
	}
	return ($outString);
}

//@JAPR 2016-12-22: Agregada la validación de expiración de cuentas de eForms (#LR9E84)
/* Convierte un objeto a un array asociativo */
function object_to_array($data) {
	if (is_array($data) || is_object($data)) {
		$result = array();
		foreach ($data as $key => $value) {
			$result[$key] = object_to_array($value);
		}         
		return $result;
	}
	return $data; 
} 
//@JAPR
?>