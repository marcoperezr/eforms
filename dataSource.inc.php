<?php
require_once("repository.inc.php");
require_once("initialize.php");
require_once("catalog.inc.php");
require_once("catalogmember.inc.php");
require_once("catalogfilter.inc.php");
require_once("appuser.inc.php");
require_once("survey.inc.php");
require_once("object.trait.php");
require_once("dataSourceMember.inc.php");
require_once("dataSourceFilter.inc.php");

//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
require_once("customClasses.inc.php");
//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
require_once("eBavelCatalog.inc.php");
require_once('eBavelIntegration.inc.php');
//@JAPR 2015-12-22: Agregado el tipo de DataSource basado en Tabla
require_once("serviceConnection.inc.php");

class BITAMDataSource extends BITAMObject
{
	use BITAMObjectExt;

	//@JAPR 2016-12-12: Agregado el esquema de paginación para obtener valores (#U7J0XU)
	static $ArrDataSourceRowCount = array();		//Cantidad estática de valores en la tabla del DataSource para reutilizarla durante los cálculos de la paginación, sólo se asignará la primera vez que sea necesaria
	//@JAPR
	
	public $NoHeader = 1;

	//***public $ParentID;
	public $DataSourceID;
	public $DataSourceName;
	public $TableName;
	public $Type;
	//***public $ModelID;
	//@JAPR 2012-03-06: Agregado el indicador de versión por registro para descarga inteligente de catálogos
	//***public $VersionFieldName;		//Nombre del campo en la tabla del catálogo que corresponde al indicador con la versión de cada registro
	//***public $MaxVersionNum;			//Máximo número de versión grabado en el campo Versión de la tabla de catálogo (se debe incrementar manualmente una vez se actualice el catálogo, o bien volver a invocar a la función getCatalogVersion)
	//***public $VersionIndID;			//ID del indicador que contiene la versión de cada registro del catálogo
	//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo
	public $VersionNum;				//Número de versión del catálogo, ya sea porque se editó su estructura o se cambiaron sus datos (originalmente se tenía que pedir la versión máxima de los registros pero resultó muy lento)
	//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
	public $DataVersionDate;			//Fecha de última modificación de los datos del catálogo de tipo eBavel o cualquiera externo a eForms donde no se tiene control de cuándo cambian los datos. Corresponde a un campo DateTime (YYYY-MM-DD HH:MM:SS)
	public $DataVersionNum;				//Número de versión del catálogo (es lo mismo que DataVersionDate pero ajustado a valor numérico en formato YYYYMMDDHHMMSS)
	//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Esto NO será una opción configurable, sólo nuevos catálogos y nuevas encuestas nacerán con esta opción activada, solamente los catálogos preexistentes se actualizarán al ser usados por una encuesta nueva
	//***public $DissociateCatDimens;	//Indica si se manejarán dimensiones separadas para los atributos de los catálogos en esta encuesta
	//***public $HierarchyID;			//Indica el ID de la jerarquía de Artus que contendrá a las dimensiones del catálogo, de tal forma que se pueda reconstruir cuando se modifique el catálogo (esta jerarquía no debería ser modificada por ningún otro producto pues se regenera automáticamente)
	//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
	public $eBavelAppID;			//Id de la aplicación de eBavel asociada a este catálogo
    public $eBavelFormType; 		//@AAL 19/03/2015: Agregado para definir si se utilizará una forma (1), una vista (2) de eBavel o Ninguno (0)
	public $eBavelFormID;			//Id de la forma de eBavel asociado a este catálogo
	public $eBavelSectionName;		//Nombre de la sección que generó el catálogo 
	public $eBavelLastModDate;		//Fecha de modificación del catálogo de eBavel
	//@JAPR 2016-09-14: Corregido un bug, a partir de eBavel 6.0, las imágenes asociadas a los campos de sus formas ya no se almacenan físicamente en el mismo lugar que en
	//las versiones anteriores, por lo que se debe utilizar este proceso para obtener la ruta físcia real y así poder obtener los datos de fecha y hora (#P04H52)
	public $eBavelAppCodeName;		//Identificador de la aplicación de eBavel a la que pertenece la forma que utiliza este DataSource
	//@JAPR 2016-10-10: Corregido un bug, se había usado el nombre lógico en lugar del nombre físico para obtener la ruta de imagenes de eBavel (#P04H52)
	public $eBavelFormName;			//Campo "Id" auto-generado tipo texto como físicamente se llama la forma de eBavel (NO es equivalente a la propiedad eBavelFormID que es un auto-generado numérico)
	//@JAPR
	public $Members = null;
	public $Document;//@JRPP 2015-09-03: Nuevo campo para guardar el nombre del documento
	//@JAPR 2015-09-05: Agregados los parámetros de recurrencia al DataSource basado en el cambio implementado por JCEM para SurveyAgendaScheduler
    public $RecurPatternType;
    public $RecurPatternOpt;
    public $RecurPatternNum;
    public $RecurRangeOpt;
    public $Ocurrences;
	public $ServiceConnectionID;		//ID a la tabla de conexiones de destinos para utilizar como fuente de este DataSource (SI_SV_ServicesConnection)
	public $Path;						//Ruta y nombre del archivo en el origen externo (DropBox, etc.)
	public $SheetName;					//Nombre de la hoja del archivo de XLS que se utilizó para cargar los datos por última vez
    public $CreationDateID;
	public $LastSyncDate;				//Fecha de la última carga exitosa del archivo XLS sincronizado desde una fuente externa, si ya se cargó el día actual, se registra esta fecha y ya no vuelve a cargarse
	public $RecurActive = 0;			//Indica si esta activo o no la recurrencia
	public $IdGoogleDrive; 				//Id del archivo seleccionado en caso de Google Drive
	//@JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
	public $LastSyncErrorDate;			//Indica la fecha del último envío de reporte de error vía EMail, generalmente sólo se enviará un reporte por hora siempre y cuando el error previo sea igual que el último, si es diferente entonces se manda en cada ocurrencia (basado en una configuración)
	public $LastSyncError;				//Contiene la descripción del último reporte de error de sincronización del DataSource
	//@JAPR

	public $CaptureStartTime;			//Hora de inicio de captura en formato hh:mm (Varchar)
	public $CaptureStartTime_Hours;		//Requerido por el componente Time para el campo CaptureStartTime
	public $CaptureStartTime_Minutes;	//Requerido por el componente Time para el campo CaptureStartTime
	public $Interval;
	public $parameters; 				//este campo guarda los parametros de la conexion de tipo http
	//@JAPR 2015-12-22: Agregado el tipo de DataSource basado en Tabla
	public $SourceDatabase;				//Indica el nombre de la base de datos externa de la que obtiene sus valores este DataSource
	public $SourceTable;				//Indica el nombre de la tabla en la base de datos externa de la que obtiene sus valores este DataSource
	//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
	public $LastModDateID;				//Fecha de la última modificación del objeto
	public $LastModUserID;				//ID del último usuario que modificó el objeto
	//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)
	//Las siguientes variables únicamente se asignarán cuando se esté cargando la instancia para despliegue de sus valores en la ventana del propio DataSource
	public $EnablePagination;			//Indica si se deberá o no utilizar el esquema de paginación (desactivado era el default hasta antes de v6.02012, a partir de dicha versión habrá paginación)
	public $NumValuesPerPage;			//Cantidad de valores a mostrar por cada página desplegada en el catálogo (default = DEFAULT_DATASOURCE_VALUESPERPAGE) (Este valor es asignado al navegar entre las diferentes páginas y se recibe como parámetro)
	public $NumValuesPerPageOld;		//Valor anterior de rows por página para calcular el ajuste de página al cambiar la cantidad de páginas visibles cuando se varía el número de rows por cada una
	public $TotalValuesPagesNum;		//Cantidad total de páginas existentes en el catálogo según la propiedad $NumValuesPerPage (Este valor es automáticamente calculado al obtener los valores cuando hay paginación activada)
	public $CurrentValuesPage;			//Número de página actual en el rango desde 1 (el default) hasta $TotalValuesPagesNum (Este valor es asignado al navegar entre las diferentes páginas y se recibe como parámetro)
	//@JAPR
	//OMMC 2019-03-25: Pregunta AR #4HNJD9
	public $IsModel;					//Propiedad que define si un Datasource es un modelo para pregunta AR
	
	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		$this->DataSourceID = -1;
		$this->DataSourceName= "";
		$this->TableName = "";
		//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo
		$this->VersionNum = 0;
		//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
		$this->DataVersionDate = null;
		$this->DataVersionNum = -1;
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		$this->eBavelAppID = 0;
        //@AAL 19/03/2015: Agregado para definir si se utilizará una forma o una vista de eBavel o ninguna
        $this->eBavelFormType = ebftForms;
		$this->eBavelFormID = 0;
		$this->eBavelSectionName = "";
		$this->eBavelLastModDate = "";
		$this->Type = dstyManual;
		//@JAPR 2016-09-14: Corregido un bug, a partir de eBavel 6.0, las imágenes asociadas a los campos de sus formas ya no se almacenan físicamente en el mismo lugar que en
		//las versiones anteriores, por lo que se debe utilizar este proceso para obtener la ruta físcia real y así poder obtener los datos de fecha y hora (#P04H52)
		$this->eBavelAppCodeName = "";
		//@JAPR 2016-10-10: Corregido un bug, se había usado el nombre lógico en lugar del nombre físico para obtener la ruta de imagenes de eBavel (#P04H52)
		$this->eBavelFormName = "";
		//@JAPR 2015-09-05: Agregados los parámetros de recurrencia al DataSource basado en el cambio implementado por JCEM para SurveyAgendaScheduler
		$this->Document = '';
        $this->RecurPatternType = frecNone;
        $this->RecurPatternOpt = "1";
        $this->RecurPatternNum = 1 ;
        $this->RecurRangeOpt = date("Y-m-d").'|0|0';
        $this->Ocurrences = 0;
		$this->ServiceConnectionID = 0;
		$this->Path = '';
		$this->SheetName = '';
        $this->CreationDateID = date("Y-m-d");
		$this->LastSyncDate = '';
		$this->CaptureStartTime = array(0,0);
		$this->CaptureStartTime_Hours = 0;
		$this->CaptureStartTime_Minutes = 0;
		$this->Interval = 0;
		$this->IdGoogleDrive = '';
		$this->RecurActive = 0;
		//@JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
		$this->LastSyncErrorDate = '';
		$this->LastSyncError = '';
		//@JAPR
		$this->parameters = '';
		//@JAPR 2015-12-22: Agregado el tipo de DataSource basado en Tabla
		$this->SourceDatabase = '';
		$this->SourceTable = '';
		//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
		$this->LastModDateID = '';
		$this->LastModUserID = 0;
		//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)
		//Las siguientes variables únicamente se asignarán cuando se esté cargando la instancia para despliegue de sus valores en la ventana del propio DataSource
		$this->EnablePagination = true;
		$this->NumValuesPerPageOld = DEFAULT_DATASOURCE_VALUESPERPAGE;
		$this->NumValuesPerPage = DEFAULT_DATASOURCE_VALUESPERPAGE;
		$this->TotalValuesPagesNum = 0;
		$this->CurrentValuesPage = 1;
		//@JAPR
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		$this->IsModel = 0;
		
	}
	
	function get_AddRemoveQueryString()	{
		return "BITAM_PAGE=DataSource";
	}
	function get_CustomButtons() {
		$arrButtons = array();
		$arrButtons[] = array("id" => "Home", "label" => translate("Home"), "image" => "home.png", "url" => "", "onclick" => "javascript: document.location.href = 'main.php?BITAM_SECTION=DataSourceCollection';");
		$arrButtons[] = array("id" => "add", "label" => translate("Catalog values"), "image" => "add.png", "url" => "", "onclick" => "setParentValuesIDs();");
		$arrButtons[] = array("id" => "Import", "label" => translate("Import"), "image" => "catalogEntry.gif", "url" => "", "onclick" => "importObject();");
		
		return $arrButtons;
	}
	function get_members(){
		if (is_null($this->Members)) {
			$this->Members = array();
			//@JAPRWarning: Estandarizar todo este código, debería de utilizar las Clases creadas para esto, no generar INSERTs directos a la metadata
			//@JAPR 2016-06-21: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
			$query = 'SELECT MemberID, MemberName,CONCAT(fieldName , " AS " , "\'", MemberName, "\'") AS fieldName, fieldName as fieldName2, eBavelFieldDataID as eBavelFieldDataID, eBavelFieldID as eBavelFieldID, MemberType as MemberType, IsImage 
				FROM SI_SV_DataSourceMember WHERE DataSourceID = ' . $this->DataSourceID . ' ORDER BY MemberOrder';
			$rs = $this->Repository->DataADOConnection->Execute($query);
			while ($rs && !$rs->EOF){
				//@JAPR 2016-06-21: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
				$this->Members[]=array("MemberID" => $rs->fields['memberid'], 
					//ears 2017-01-13 validación de caracteres especiales	
					"MemberName" => GetValidJScriptText($rs->fields['membername'],array("fullhtmlspecialchars" => 1)), 
					"fieldName" => $rs->fields['fieldname'], 
					"fieldName2" => $rs->fields['fieldname2'], 
					"eBavelFieldDataID" => $rs->fields['ebavelfielddataid'], 
					"eBavelFieldID" => $rs->fields['ebavelfieldid'], 
					"MemberType" => $rs->fields['membertype'], 
					"IsImage" => (int) @$rs->fields['isimage']);
				$rs->MoveNext();
			}
		}
		if (count($this->Members) == 0){
			return false;
		}else{
			return true;
		}
	}
	function get_formsFromDatasource(){
		$arrFormsInDatasource = array();
		//@JAPR 2016-07-26: Corregido un bug, no estaba considerando a las preguntas múltiple choice para determinar los catálogos usados por formas (#HSRGGM)
		$query = "(SELECT DISTINCT a.surveyID, B.SurveyName 
						FROM si_sv_question a
							INNER JOIN SI_SV_Survey B
							ON B.SurveyID = a.surveyID
					WHERE (a.qtypeid = ".qtpSingle." OR a.qtypeid = ".qtpMulti.") AND a.valuessourcetype = 1  AND a.datasourceid = {$this->DataSourceID})
					UNION
					(SELECT DISTINCT a.surveyid, B.SurveyName 
						FROM si_sv_section a 
							INNER JOIN SI_SV_Survey B
							ON B.SurveyID = a.surveyID
					WHERE a.sectiontype = 4 AND a.valuessourcetype = 1  AND a.datasourceid = {$this->DataSourceID})
					ORDER BY surveyID ";
		$rs = $this->Repository->DataADOConnection->Execute($query);
		while ($rs && !$rs->EOF){
			$arrFormsInDatasource[]=array("SurveyID" => $rs->fields['surveyid'], "SurveyName" => $rs->fields['surveyname']);
			$rs->MoveNext();
		}
		return $arrFormsInDatasource;
	}
	function get_QueryMembers(){
		$fields = array();
		foreach ($this->Members as $key => $value) {
			$fields[] = $this->Members[$key]["fieldName"];
		}
		return "SELECT id, " . implode(', ', $fields) . " FROM ". $this->TableName;
	}
	
	//@JAPR 2015-12-15: Corregidos bugs, esta al ser una función de instancia, no requiere recibir estos parámetros pues ya los contiene
	function updateAttributesHTML() {
		$name = '';
	    $user = '';
	    $pass = '';
	    $parameters = $this->parameters;
		$sql = 'SELECT `name`,`user`,`pass` FROM  si_sv_servicesconnection where id = '. $this->ServiceConnectionID;
		$rs = $this->Repository->DataADOConnection->Execute($sql);
		if ($rs && !$rs->EOF){
			$name =$rs->fields['name'];
		    $user = $rs->fields['user'];
		    $pass = $rs->fields['pass'];
       	}
		
       	$nameCloud="HTTP";
		$accountCloud= array('httpServer' => $name,'httpUsername' => $user,'httpPassword' => $pass,'httpParameters' =>  $parameters, 'catalogId' => $this->DataSourceID);
		include_once('datadestinations/connectionCloud.class.php');
		$r = connectionCloud::downloadAttributes($nameCloud,$accountCloud,$this->Repository);
		if ($r && $r['success']) {
			$attributesValues = $r['multiaRRay'];
			//Si se conecto satisfactoriamente lo primero que hago es eliminar los registros previamente guardados
			$sqlDelete = 'DELETE FROM ' . $this->TableName;
			$this->Repository->DataADOConnection->Execute($sqlDelete);
			$tablename = $this->TableName;
			$sql = 'SELECT membername, fieldname FROM SI_SV_datasourceMember WHERE datasourceid = ' . $this->DataSourceID;
			$arrdefinitionfields = array();
			$arrnamefields = array();
			$rs2 = $this->Repository->DataADOConnection->Execute($sql);
			while ($rs2 && !$rs2->EOF) {
				$arrdefinitionfields[] = @$rs2->fields["fieldname"];
				//ears 2017-01-13 validación de caracteres especiales
				$arrnamefields[] = GetValidJScriptText(@$rs2->fields["membername"],array("fullhtmlspecialchars" => 1));
				$rs2->MoveNext();
			}

			foreach ($attributesValues as $key => $value) {
				$arrinsertfields = array();
				$arrinsertvalues = array();
				foreach ($value as $key2 => $value2) {
					$arrinsertfields[] = $key2;
					$arrinsertvalues[] = $this->Repository->DataADOConnection->quote($value2);
				}
				//En el siguiente ciclo elimino los que no se usan en caso de que usuaio no haya seleccionado todos los fields
				$countArrInsertFields = count($arrinsertfields);
				for($i = 0; $i < $countArrInsertFields; $i++) {
					if (!in_array($arrinsertfields[$i], $arrnamefields)){
						unset($arrinsertfields[$i]);
						unset($arrinsertvalues[$i]);
					}
				}
				$fields = implode(",", $arrinsertfields);
				//Una vez deburado obtengo los nombres verdaderos del campo en la tabla para poder armar los insert
				$fields = str_replace($arrnamefields, $arrdefinitionfields, $fields);
				$sql = 'INSERT INTO ' . $tablename . '(' . $fields . ') VALUES (' . implode(",", $arrinsertvalues) . ')';
				$this->Repository->DataADOConnection->Execute($sql);
			}
		}else if (!$r['success']){
			return $r;
		}else {
			return null;
		}
	}
	//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
	/* Genera el estatuto para actualizar el valor de un registro del catálogo (todos los tipos excepto eBavel u ODBC) regresando en un parámetro una indicación sobre si se modificó
	algún atributo tipo Imagen, en cuyo caso adicionalmente realizará el upload de la imagen configurada al servicio de eForms si es que es una imagen absoluta, ajustanto la ruta
	hacia las carpetas de imagenes de eForms (tanto de usuario como de imagenes globales)
	*/
	function get_UpdateMembers($anID, $anArrayOfValues, &$bHasImageAttrib = false) {
		if ($this->get_members()) {
			$fields = array();
			//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
			$strErrorMsg = '';
			foreach ($this->Members as $key => $arrAttributeDef) {
				if (isset($anArrayOfValues[$arrAttributeDef["fieldName2"]])) {
					$strAttributeFieldValue = $anArrayOfValues[$arrAttributeDef["fieldName2"]];
					$intIsImage = (int) @$arrAttributeDef["IsImage"];
					//Si el atributo está marcado como imagen, debe hacer el reprocesamiento del valor e iniciar el upload de la imagen al server de eForms (si es necesario)
					if ($intIsImage) {
						$strAttributeFieldValue = GetRelativeUserImgFileNameFromURL($strAttributeFieldValue, '', $strErrorMsg);
						/*$strAttributeFieldValue = str_replace("\\", "/", $strAttributeFieldValue);
						$arrURLData = @IdentifyAndAdjustAttributeImageToDownload($strAttributeFieldValue);
						if (!is_null($arrURLData)) {
							//En este caso la URL necesita algún ajuste y posiblemente se debe descargar la imagen, según el objeto regresado
							if (is_array($arrURLData)) {
								//Se debe descargar la imagen, además de actualizar el valor a grabar
								$strAttributeFieldValue = (string) @$arrURLData['path'];
								//$arrImagesToDownload[(string) @$arrURLData['name']] = $arrURLData;
								$fileName = (string) @$arrURLData['name'];
								//Antes de hacer la copia, primero crea la ruta destino
								$path = str_ireplace($fileName, '', $arrURLData['path']);
								if (!file_exists($path)) {
									@mkdir($path, null, true);
								}
								
								if (!@copy($arrURLData['url'], $arrURLData['path'])) {
									$errors = error_get_last();
									$strErrorMsg = "({$fileName}) ".$errors['message'];
								}
							}
							else {
								//En este caso simplemente se ajusta la URL a grabar
								$strAttributeFieldValue = $arrURLData;
							}
						}
						*/
					}
					
					$fields[] = $arrAttributeDef["fieldName2"]." = ". $this->Repository->DataADOConnection->Quote($strAttributeFieldValue);
					$bHasImageAttrib = $bHasImageAttrib || $intIsImage;
				}
			}
			//@JAPR
			
			if (count($fields) == 0) {
				return null;
			}
			return "UPDATE ". $this->TableName. " SET ". implode(', ', $fields)." WHERE id = ".$anID;
		}
		return null;
	}
	function get_InsertMembers($anArrayOfValues) {
		if ($this->get_members()) {
			$fields = array();
			$values = array();
			//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
			$strErrorMsg = '';
			foreach ($this->Members as $key => $arrAttributeDef) {
				$fields[] = $arrAttributeDef["fieldName2"];
				if (isset($anArrayOfValues[$arrAttributeDef["fieldName2"]])) {
					$strAttributeFieldValue = $anArrayOfValues[$arrAttributeDef["fieldName2"]];
					$intIsImage = (int) @$arrAttributeDef["IsImage"];
					//Si el atributo está marcado como imagen, debe hacer el reprocesamiento del valor e iniciar el upload de la imagen al server de eForms (si es necesario)
					if ($intIsImage) {
						$strAttributeFieldValue = GetRelativeUserImgFileNameFromURL($strAttributeFieldValue, '', $strErrorMsg);
						/*$strAttributeFieldValue = str_replace("\\", "/", $strAttributeFieldValue);
						$arrURLData = @IdentifyAndAdjustAttributeImageToDownload($strAttributeFieldValue);
						if (!is_null($arrURLData)) {
							//En este caso la URL necesita algún ajuste y posiblemente se debe descargar la imagen, según el objeto regresado
							if (is_array($arrURLData)) {
								//Se debe descargar la imagen, además de actualizar el valor a grabar
								$strAttributeFieldValue = (string) @$arrURLData['path'];
								//$arrImagesToDownload[(string) @$arrURLData['name']] = $arrURLData;
								$fileName = (string) @$arrURLData['name'];
								//Antes de hacer la copia, primero crea la ruta destino
								$path = str_ireplace($fileName, '', $arrURLData['path']);
								if (!file_exists($path)) {
									@mkdir($path, null, true);
								}
								
								if (!@copy($arrURLData['url'], $arrURLData['path'])) {
									$errors = error_get_last();
									$strErrorMsg = "({$fileName}) ".$errors['message'];
								}
							}
							else {
								//En este caso simplemente se ajusta la URL a grabar
								$strAttributeFieldValue = $arrURLData;
							}
						}
						*/
					}
					
					$values[] = $this->Repository->DataADOConnection->Quote($strAttributeFieldValue);
				} else {
					$values[] = $this->Repository->DataADOConnection->Quote("");
				}
			}
			if (count($fields) == 0) {
				return null;
			}
			return "INSERT INTO ". $this->TableName. " (". implode(', ', $fields). ') VALUES ('.implode(', ', $values).')';
		}
		return null;
	}
	function get_DeleteMembers($anID){
		return "DELETE FROM ". $this->TableName. " WHERE ID = ".$anID;
	}
	function get_AllData() {
		$fields = array();
		$data = array();
		if ($this->get_members()) {
			$fields[] = 'id';
			foreach ($this->Members as $key => $value) {;
				$fields[] = $this->Members[$key]["fieldName2"];
			}
			//$this->Repository->DataADOConnection->SetFetchMode(1);
			//$rs = $this->Repository->DataADOConnection->Execute($this->get_QueryMembers());
			//@JAPR 2015-08-23: Agregado el parámetro $iFetchMode para cambiar el modo de regresar los fields
			//@JAPR 2015-08-23: Corregido un bug, la ventana de valores del catálogo no debe aplicar el filtro del usuario logeado
			//@JAPR 2015-12-22: Se removió el parámetro #5 $attribPosition, ya que no se usaba, fue un mal intento de simular BITAMCatalogFilter
			$rs = $this->getAttributeValues(null, null, "", 3, true, true, true, ADODB_FETCH_NUM, false, true);
			$x = 0;
			while ($rs && !$rs->EOF) {
				$x = $x + 1;
				$dataRow = array();
				if ($this->eBavelAppID > 0 && $this->eBavelFormID > 0) {
					$columnIndex = 4;
					for ($i = 0 ; count($fields)>$i;$i++) {
						if ($GPSfromEbavel[$i]){
							$columnIndex++;
						}
						$dataRow[$fields[$i]] = is_null($rs->fields[$columnIndex]) ? '' : (string) $rs->fields[$columnIndex];

						$columnIndex++;
					}
					//$dataRow["id"] = $x;
				}else{
					foreach ($fields as $columnIndex => $fieldName) {
						$dataRow[$fieldName] = is_null($rs->fields[$columnIndex]) ? '' : (string) $rs->fields[$columnIndex];
					}
				}
				$data[] = $dataRow;
				$rs->MoveNext();
			}
			//$this->Repository->DataADOConnection->SetFetchMode(2);
		}
		return $data;
	}
	function get_MembersHeaders($type){
		$headers = array();
		$cont = 0 ;
		$width = array();
		$Align = array();
		$filter = array();

		foreach ($this->Members as $key => $value) {
			$headers[] = $this->Members[$key]["MemberName"];
			$width[] = "200";
			$Align[] = "left";
			$filter[] = "#text_filter";
			$cont++;
		}
		switch($type) {
			case "1":
					return implode(',', $headers);
				break;
			case "2" :
					return $cont;
				break;
			case "3" :
					return implode(',', $width);
				break;	
			case "4" :
				return implode(',', $Align);
			break;
			case "5" :
				return implode(',', $filter);
			break;	
		}
	}
	function get_Image() {
		$strImage = "<img src=\"images/admin/catalogs_64x64.png\" />";
		return $strImage;
	}
	function get_fieldFromeBavel($arrid) {
		$fieldsFromeBavel = array();
		//GCRUZ 2016-04-13. Validar que los IDs de $arrid no vengan vacíos
		foreach($arrid as $key => $value)
		{
			if (trim($value) == '')
				unset($arrid[$key]);
		}
		$query = 'SELECT id_fieldform, label, tagname FROM SI_FORMS_FIELDSFORMS WHERE section_id = ' . $this->eBavelFormID . ' AND id_fieldform NOT IN ('. implode(', ', $arrid) .');';
		$rs = $this->Repository->ADOConnection->Execute($query);
		//@JAPR 2016-12-28: Validado que no se puedan seleccionar ciertos tipos de campos de eBavel como mapeables (#HJ3L7N)
		$arrExcludedFields = array('widget_secheader');
		$arrExcludedFields = array_flip($arrExcludedFields);
		while ($rs && !$rs->EOF) {
			//@JAPR 2016-12-28: Validado que no se puedan seleccionar ciertos tipos de campos de eBavel como mapeables (#HJ3L7N)
			$strTagName = (string) @$rs->fields['tagname'];
			if (!isset($arrExcludedFields[$strTagName])) {
				$fieldsFromeBavel[$rs->fields['id_fieldform']]=array("id_fieldform" => $rs->fields['id_fieldform'], "label" => $rs->fields['label'], "tagname" => $strTagName);
			}
			$rs->MoveNext();
		}
		//@JAPR
		
		if (count($fieldsFromeBavel) == 0){
			return false;
		}else{
			return $fieldsFromeBavel;
		}
	}
	function generateForm($aUser) {
		$arrAppNames = array();
		$arrAppIds = array();
		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		$strScriptPath = $path_parts["dirname"];
		$myCatalogName = get_class($this);
		$query = "SELECT ID_APP, APPLICATIONNAME 
					FROM SI_FORMS_APPS 
					WHERE ID_APP IN (10002,10005,10006) ORDER BY 2";
		$rs = $this->Repository->DataADOConnection->Execute($query);
		while ($rs && !$rs->EOF){
			$arrAppIds[]=$rs->fields['id_app'];
			$arrAppNames[]=$rs->fields['applicationname'];
			$rs->MoveNext();
		}
?>
		<html>
			<head>
				<meta http-equiv="x-ua-compatible" content="IE=10" />
				<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
				<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
				<link type="text/css" href="loadexcel/css/bootstrap.min.css" rel="stylesheet"/>
				<link rel="stylesheet" type="text/css" href="js/libs/metisMenu/src/metisFolder.css"/>
				<link rel="stylesheet" type="text/css" href="js/libs/selectize.js/dist/css/selectize.bootstrap3.css"/>
				<link rel="stylesheet" type="text/css" href="css/materialModal.css"/>
			    <link rel="stylesheet" media="screen" type="text/css" href="js/handsontable/handsontable.full.css">
			    <script type="text/javascript" src="js/removeDiacritics.js"></script>
			    <script type="text/javascript" src="js/handsontable/handsontable.full.js"></script>
				<script src="js/codebase/dhtmlxFmtd.js"></script>
		      	<script type="text/javascript" src="js/json2.min.js"></script>
				<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
				<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
				<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
				<!--2015-06-26 JRPP Agrego librerias y css para el pintado de la importacion de catalagos-->
				<link type="text/css" href="loadexcel/css/font-awesome.min.css" rel="stylesheet"/>
				<script src="loadexcel/js/libs/bootstrap/bootstrap.min.js"></script>
				<script type="text/javascript" data-main="loadexcel/js/loadexcel.js" src="loadexcel/js/libs/require/require.js"></script>
				<style>
					/* it's important to set width/height to 100% for full-screen init */
					html, body {
						width: 100%;
						height: 100%;
						margin: 0px;
						overflow: hidden;
					}
					div.filter {
						display: none;
					}
					.handsontable td, .handsontable tr, .handsontable th {
						height: 25px; /*change to the desired height value*/
						overflow: hidden;
						text-overflow: ellipsis;
						white-space: nowrap !important;
					}
					.handsontable span.colHeader, .handsontable th {
						background-color: #FECC62; /* yellow */
					}
					.handsontable col.rowHeader {
						width: 100px;
					}
					* {
					    box-sizing: inherit;
					}
					.noSpcLab {
						padding: 0px;
						margin: 0px;
					}
					.noSpcBut {
						margin: 0px;
						padding-top: 0px;
						padding-bottom: 0px;
						border-top-width: 1px;
						border-bottom-width: 1px;
						float: left;
					}
					.noVisFile {
						opacity:0;
						width:0px;
						height:0px;
						visibility:hidden;
					}
					<?	//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)?>
					.dhxform_obj_dhx_terrace {
						overflow: hidden !important;
						top: -5px;
					}					
					.dtSrcLbls .dhxcombo_dhx_terrace, .dtSrcLbls .dhxform_textarea, .dtSrcLbls .dhxform_btn, .dtSrcLbls .pageCounterlbl, .dtSrcLbls .dhxform_txt_label2, .dtSrcLbls .dhxform_txt_label2.topmost {
						font-family: "Roboto Regular" !important;
						font-size: 13px !important;
						font-weight: normal !important;
					}
					.nobrdrCbo .dhxcombo_dhx_terrace, .nobrdrCbo .dhxform_textarea {
						border: 0px;
					}
					.leftChgPagBtn .dhxform_btn {
						float: right !important;
						background-image: url(images/admin/leftArrowFntAw.png) !important;
						background-repeat:no-repeat !important;
						width: 18px;
						height: 18px;
						cursor: pointer !important;
						background-color: #FFFFFF !important;
						border: 0px !important;
					}
					.rightChgPagBtn .dhxform_btn {
						float: right !important;
						background-image: url(images/admin/rightArrowFntAw.png) !important;
						background-repeat:no-repeat !important;
						width: 18px;
						height: 18px;
						cursor: pointer !important;
						background-color: #FFFFFF !important;
						border: 0px !important;
					}
					.pglnkStyle .dhxform_btn {
						height: 18px;
						cursor: pointer !important;
						background-color: #FFFFFF !important;
						border: 0px !important;
					}
					.pglnkStyle .dhxform_btn_txt {
						color: #264B83;
						margin: 1px !important;
						line-height: normal !important;
						text-decoration: underline;
					}
					<?	//@JAPR		?>
					.pageCounterlbl {
						font-style: italic;
					}
				</style>
			</head>
			<script>
<?	//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)?>
				var intNumValuesPerPageOld = <?=$this->NumValuesPerPage?>;
				var intNumValuesPerPage = <?=$this->NumValuesPerPage?>;
				var intCurrentValuesPage = <?=$this->CurrentValuesPage?>;
				var intTotalValuesPagesNum = <?=$this->TotalValuesPagesNum?>;
<?	//@JAPR	?>
				var objWindows;
				var objDialog;
				var objMainForm = undefined;
				var objCustomizedButtons = new Array();
				var objCustomBehaviour = new Object();
				var objCustomizedFns = new Object();		//Array con funciones personalizadas en algunos botones (si no se trata de una URL)
				var intButtons = 0;
				var blnCanAdd = 1;
				var blnCanRemove = 1;
				intButtons += (blnCanAdd)?1:0;
				intButtons += (blnCanRemove)?1:0;
				//JAPR 2016-07-19: Agregada la scrollbar horizontal a la tabla de valores (#CSOWSK)
				var dhxDataSourceName = "a";
				var dhxDataSourceNav = "a";
				var dhxDataSource = "b";
				var objToolbarAttr;
				var objFormsLayoutDataSource;
				var objFormsLayoutDataSourceTable;
				var objValuesPageNavForm;
				var objGridValues;
				//JAPR 2015-102015-10-25: Agregado el proceso de confirmación de borrado cuando existen referencias
				var goObjectToDelete = undefined;	//Contiene la información para ejecutar nuevamente un proceso de borrado de algún elemento
				//JAPR
				var data;
				var dataString;
				var catalogName =<?= $this->Repository->DataADOConnection->Quote(GetValidJScriptText($this->DataSourceName))?>;
				var eBavelAppID ='<?= $this->eBavelAppID ?>';
				var eBavelFormType ='<?= $this->eBavelFormType ?>';
				var eBavelFormID ='<?= $this->eBavelFormID ?>';
				var eBavelLastModDate = '<?= $this->eBavelLastModDate ?>';
				var dataSourceType ='<?= $this->Type ?>';
				//@JRPP 2015-09-03: Nuevo campo para guardar el nombre del documento
				var dataSourceDocument = <?= $this->Repository->DataADOConnection->Quote($this->Document) ?>;
				var dataSourceSheet = <?= $this->Repository->DataADOConnection->Quote($this->SheetName) ?>;
				var dataSourcePath = <?= $this->Repository->DataADOConnection->Quote($this->Path) ?>;
				//OMMC 2015-11-20: Mensaje de error del catálogo
				var errorDisplayMsg = <?= $this->Repository->DataADOConnection->Quote((string) $this->LastSyncError) ?>;
				var parameters = <?=$this->Repository->DataADOConnection->Quote((string) @$this->parameters) ?>;
				var SourceDatabase = <?=$this->Repository->DataADOConnection->Quote((string) @$this->SourceDatabase) ?>;
				var SourceTable = <?=$this->Repository->DataADOConnection->Quote((string) @$this->SourceTable) ?>;
				//OMMC 2015-09-25: Se analiza el tipo de dataSource y se le da un nombre para mostarse en los detalles
				//OMMC 2015-09-25: En base al tipo de dataSource, se mostrará la fuente del archivo. Si es 1 o 2; Captura de usuario. Si es 3; Nombre de la forma de eBavel. Si es 4, 5, o 6; se mostrará el nombre del archivo que se cargó.
				var dataSourceTypeName = '';
				var dataSourceSourceName = '';
				//@JAPR 2016-06-21: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
				var attributeIsImage = new Array();
				var colMemberIDs = new Array();
				//JAPR 2016-07-19: Agregada la scrollbar horizontal a la tabla de valores (#CSOWSK)
				var objGridValuesContainer = undefined;
				var isModel = <?= $this->IsModel ?>;

				formsFromDatasource =<?= json_encode($this->get_formsFromDatasource()) ?>;
				DatasourceFirst = true;

				//OMMC 2015-12-30: Agregado por si el nombre de la fuente es demasiado largo, se corta el nombre y se asigna un tooltip para mostrar el nombre completo.
				var sourceTooltip = '';

				switch(dataSourceType){
					case '<?=dstyManual?>':
					case '<?=dstyExcel?>':
						dataSourceTypeName = '<?= translate("MS Excel or Manual") ?>';
						dataSourceSourceName = '<?= translate("User input") ?>';
					break;
					case '<?=dstyeBavel?>':
						dataSourceTypeName = 'eBavel';
						dataSourceSourceName = '<?=$this->eBavelSectionName?>';
					break;
					case '<?=dstyDropBox?>':
						dataSourceTypeName = 'Dropbox';
						dataSourceSourceName = dataSourceDocument;
					break;
					case '<?=dstyGoogleDrive?>':
						dataSourceTypeName = 'Google Drive';
						dataSourceSourceName = dataSourceDocument;
					break;
					case '<?=dstyFTP?>':
						dataSourceTypeName = 'FTP';
						dataSourceSourceName = dataSourceDocument;
					break;
					case '<?=dstyHTTP?>':
						dataSourceTypeName = 'HTTP';
						dataSourceSourceName = dataSourceDocument;
					break;
					case '<?=dstyTable?>':
						dataSourceTypeName = '<?= translate("ODBC") ?>';
						dataSourceSourceName = SourceDatabase+"."+SourceTable;
					break;
					default:
						dataSourceTypeName = '';
						dataSourceSourceName = '';
				}
				//OMMC 2015-12-30: Agregado el nombre recortado de las fuentes de datos y el tooltip. Recortado a 25 caracteres
				if(dataSourceSourceName.length > 25){
					sourceTooltip = dataSourceSourceName;
					dataSourceSourceName = dataSourceSourceName.substr(0, 25) + '...';
				}

				//OMMC 2015-09-25: Si la fecha de sincronización no existe, se mostrará la fecha en la que se creó el catálogo
				var dataSourceSyncDate = '<?= ($this->eBavelFormID != NULL)?($this->eBavelLastModDate):(($this->LastSyncDate==NULL)?translate($this->CreationDateID):translate($this->LastSyncDate)) ?>';
				var dataSourceServiceConnectionID = '<?= translate($this->ServiceConnectionID) ?>';
				var IdGoogleDrive = '<?= translate($this->IdGoogleDrive) ?>';
				var RecurActive = '<?= translate($this->RecurActive) ?>';
				var RecurPatternType = '<?= translate($this->RecurPatternType) ?>';
				var RecurPatternOpt = '<?= translate($this->RecurPatternOpt) ?>';
				var RecurPatternNum = '<?= translate($this->RecurPatternNum) ?>';
				var RecurRangeOpt = '<?= translate($this->RecurRangeOpt) ?>';
				var Interval = '<?= translate($this->Interval) ?>';
				var CaptureStartTime_Hours = '<?= $this->CaptureStartTime_Hours ?>';
				var CaptureStartTime_Minutes = '<?= $this->CaptureStartTime_Minutes ?>';			

				/* Descarga las ventanas adicionales creadas así como cualquier otro componente */
				function doOnUnload() {
					console.log('doOnUnload');
					
					//if (objWindows && objWindows.unload) {
					//	objWindows.unload();
					//}
				}
				function doUnloadDialog() {
					if (objDialog) {
						var objForm = objDialog.getAttachedObject();
						if (objForm && objForm.unload) {
							objForm.unload();
							objForm = null;
						}
					}
					
					if (objWindows) {
						if (objWindows.unload) {
							objWindows.unload();
							objWindows = null;
							objWindows = new dhtmlXWindows({
								image_path:"images/"
							});
						}
					}
					
					objDialog = null;
				}
<?					$arrMemebertype = array();
					$headers = array();
					$arrIdeBavel = array();
					$widths = array();
					$fields = array();
					$colNames = array();
					$columns = array();
					$colMemberIDs = array();
					$GPSfromEbavel = array();
					$data = array();
					$fieldsForms = array();
					$membersDatasource = array();
					//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
					$attributeIsImage = array();
					//@JAPR 2015-10-30: Corregido un bug, se inicializaba la variable sólo bajo cierta condición pero se intentaba usar sin ninguna, generaba un error (#1UTZS0)
					$eBavelfieldforms = null;
					//@JAPR
					if ($this->get_members()){
						if ($this->eBavelAppID > 0 && $this->eBavelFormID > 0) {
							$fields[] = 'id';
							$GPSfromEbavel[] = FALSE;
							$arrMemebertype[] = '';
							$fieldsForms[] = '';
						}else{
							$fields[] = 'id';
							$GPSfromEbavel[] = FALSE;
						}
					
						foreach ($this->Members as $key => $value) {
							$headers[] = $this->Repository->DataADOConnection->Quote((string) $this->Members[$key]["MemberName"]);
							$widths[] = "200";
							$fields[] = $this->Members[$key]["fieldName2"];
							$colNames[] = $this->Repository->DataADOConnection->Quote((string) $this->Members[$key]["fieldName2"]);
							//@JAPR 2016-06-21: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
							$arrAttributeDef = array("data" => $this->Members[$key]["fieldName2"]);
							$intIsImage = (int) @$this->Members[$key]["IsImage"];
							$attributeIsImage[] = $intIsImage;
							if ($intIsImage && $this->Type != dstyeBavel && $this->Type != dstyTable) {
								$arrAttributeDef["renderer"] = "imageRenderer";
							}
							
							//@JAPR 2016-07-20: Corregido un bug, no deben ser editables los catálogos de tipo eBavel ni ODBC (#AZFTW7)
							if ($this->Type == dstyeBavel || $this->Type == dstyTable) {
								$arrAttributeDef["editor"] = false;
								$arrAttributeDef["readOnly"] = true;
							}
							$columns[] = $arrAttributeDef;
							//@JAPR
							$colMemberIDs[] = (int) $this->Members[$key]["MemberID"];
							$GPSfromEbavel[] = @$this->Members[$key]["eBavelFieldDataID"];
							$arrMemebertype[] = @$this->Members[$key]["MemberType"];
							if (@$this->Members[$key]["eBavelFieldID"] && $this->Members[$key]["eBavelFieldID"] != "") {
								$fieldsForms[] = $this->Members[$key]["eBavelFieldID"];
							}
							$membersDatasource[] = array("columnexcel" => $this->Members[$key]["MemberName"], "columntable" => $this->Members[$key]["fieldName2"]);
						}

						//$this->Repository->DataADOConnection->SetFetchMode(1);
						//$rs = $this->Repository->DataADOConnection->Execute($this->get_QueryMembers());
						//@JAPR 2015-08-23: Agregado el parámetro $iFetchMode para cambiar el modo de regresar los fields
						//@JAPR 2015-08-23: Corregido un bug, la ventana de valores del catálogo no debe aplicar el filtro del usuario logeado
						//@JAPR 2015-12-22: Se removió el parámetro #5 $attribPosition, ya que no se usaba, fue un mal intento de simular BITAMCatalogFilter
						if ($this->eBavelAppID > 0 && $this->eBavelFormID > 0) {
							for ($i = 0 ; count($fields)>$i;$i++) {
								$rs_id = $this->Repository->ADOConnection->Execute("SELECT id FROM SI_FORMS_FIELDSFORMS where id_fieldform = " . (string) $fieldsForms[$i]);
								if ($rs_id && !$rs_id->EOF) {
									$arrIdeBavel[] = $rs_id->fields["id"];
								}else {
									$arrIdeBavel[] = '';
								}
							}							
						}
						$rs = $this->getAttributeValues(null, null, "", 3, true, true, true, ADODB_FETCH_ASSOC, false, true);
						$x = 0;
						while ($rs && !$rs->EOF) {
							$x = $x + 1;
							$dataRow = array();
							if ($this->eBavelAppID > 0 && $this->eBavelFormID > 0) {
									for ($i = 0 ; count($fields)>$i;$i++) {
										 $id = $arrIdeBavel[$i];
										if ($id && $id!="") {
											if ($GPSfromEbavel[$i])
											{
												if( $GPSfromEbavel[$i] == 1 ) {
													if (array_key_exists($id . '_L', $rs->fields)) {
														$dataRow[$fields[$i]] = is_null($rs->fields[ $id . '_L' ]) ? '' : (string) $rs->fields[$id . '_L' ];
													}
												}
												else {
													if (array_key_exists($id . '_A', $rs->fields)) {
														$dataRow[$fields[$i]] = is_null($rs->fields[ $id . '_A' ]) ? '' : (string) $rs->fields[$id . '_A' ];
													}
												}
											} else 
											{
												if (array_key_exists($id, $rs->fields)) {
 													$dataRow[$fields[$i]] = is_null($rs->fields[@$id]) ? '' : (string) $rs->fields[@$id];
												}
											}
											//@JRPP 2015-11-27: Si es Un tipo de dato signature debemos de buscar en el campo que tiene la imagen 
											if ($arrMemebertype[$i] == 10){
												$dataRow[$fields[$i]] = is_null($rs->fields[ $id . '_U' ]) ? '' : (string) $rs->fields[$id . '_U' ];
											}
										}
									}

							}else{
								foreach ($fields as $columnIndex => $fieldName) {
									if (array_key_exists($fieldName, $rs->fields)) {
										$dataRow[$fieldName] = is_null($rs->fields[@$fieldName]) ? '' : (string) $rs->fields[$fieldName];
									}
								}
							}
							$data[] = $dataRow;
							$rs->MoveNext();
						}
						
						//@JAPR 2015-10-30: Corregido un bug, se inicializaba la variable sólo bajo cierta condición pero se intentaba usar sin ninguna, generaba un error (#1UTZS0)
						if ($this->Type==dstyeBavel) { // variable para los catalagos cargados de Ebavel
							$eBavelfieldforms = $this->get_fieldFromeBavel($fieldsForms);
						}
					}
?>
				function applyFilter() {
					var input = objToolbarAttr.getInput("Search1");
					if (input.value) {
						var objCols = [ <?= implode(', ', $colNames) ?> ];
						var search = [];
						var value = removeLowercaseSpanishDiacritics(input.value.toLowerCase());
						for (var row = 0, r_len = data.length; row < r_len; row++) {
							if (data[row]['id']) {
								for (var col = 0, c_len = objCols.length; col < c_len; col++) {
									if (data[row][objCols[col]]) {
										if ((removeLowercaseSpanishDiacritics('' + data[row][objCols[col]]).toLowerCase()).indexOf(value) > -1) {
											search.push(data[row]);
											break;
										}
									}
								}
							} else {
								search.push(data[row]);
							}
						}
						objGridValues.loadData(search);
    				} else {
						objGridValues.loadData(data);
					}
				}
				function buildRecurrenceWindow(){
					var dataSourceId ='<?= $this->DataSourceID ?>';
					objGridValues.unlisten();
					require(['loadexcel/js/views/loadRecurrence/wizardLoadRecurrence.php?'], function( Wizard ) {
						Wizard(dataSourceId);
					});
				}
				function buildReloadWindow(){
					DatasourceFirst = false;
					var dataSourceId ='<?= $this->DataSourceID ?>';
					var dataSourcename =<?= $this->Repository->DataADOConnection->Quote($this->DataSourceName) ?>;
					objGridValues.unlisten();
					require(['loadexcel/js/views/Reload/wizardReload.php?'], function( Wizard ) {
						$(parent.window.document.getElementById('fondo')).hide();
						/** Funcion para traducciones */
						$.t = function( s ) { return s; };
						Wizard(dataSourceId, dataSourceType, dataSourceDocument, dataSourceSheet, dataSourcePath, dataSourceServiceConnectionID, IdGoogleDrive, RecurActive, membersDatasource, dataSourcename,	RecurPatternNum, RecurPatternOpt, RecurPatternType, RecurRangeOpt, Interval, CaptureStartTime_Hours, CaptureStartTime_Minutes, parameters, SourceDatabase, SourceTable).done(function(idCatalog,label) {
							objGridValues.listen();
							var dhxList = "b";
							strURL = "<?= 'main.php?'.$this->get_QueryString() ?>";
							parent.objFormsLayoutMenu.cells(dhxList).progressOn();
							parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
						});
					});
				}
				//JAPR 2016-06-21: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
				/* Función utilizada para generar un componente IMG donde se mostrará la imagen configurada para atributos tipo imagen, dependiendo de si es una ruta absoluta,
				relativa a eForms (en caso de que sólo se escriba el nombre de imagen) o relativa al web service
				*/
				function imageRenderer(instance, td, row, col, prop, value, cellProperties) {
					var escaped = Handsontable.helper.stringify(value);
					var img;
					
					var strLowerCase = escaped.toLowerCase();
					//Primero agrega el botón de Upload manual de la imagen (reutilizado del componente uploadImg de otras ventanas)
					var allData = instance.getData();
					if (allData[row]) {
						var id = allData[row]['id'];
						if (id === null) {
							id = "null";
						}
						objDiv = document.createElement('DIV');
						$(objDiv).css('border', 'none');
						<?//@JAPR 2016-12-23: Corregido un bug, los IDs de tags estaban mal empleados, sólo hacían referencia a la row y no a la columna, así que todos los atributos
						//cambiaban siempre al primer atributo (#R3Q9JQ)?>
						var strHTML = ''+
							'<form method="POST" action="uploadUserImage.php?strId='+id+'_'+col+'&tabName='+col+'&strRowId='+row+'" enctype="multipart/form-data" style="display:inline" target="frameUploadFile">'+
								'<input type="file" id="file'+id+'_'+col+'" name="file'+id+'_'+col+'" value="" class="noVisFile" onchange="document.getElementById(\'btnsubmit'+id+'_'+col+'\').click();">'+
								'<button type="button" class="noSpcBut" onclick="document.getElementById(\'file'+id+'_'+col+'\').click();"><?=translate("...")?></button>'+
								'<input type="submit" id="btnsubmit'+id+'_'+col+'" name="btnsubmit'+id+'_'+col+'" value="<?=translate("Upload file")?>" style="display: none; visibility: hidden;">'+
							'</form>';
						$(objDiv).html(strHTML);
						Handsontable.Dom.empty(td);
						td.appendChild(objDiv);
					}
					
					//Luego agrega la referencia a la imagen si es que es un texto que se puede identificar como tal
					if (strLowerCase.indexOf('http://') === 0 || strLowerCase.indexOf('https://') === 0 || strLowerCase.indexOf('customerimages/fbm_bmd_') === 0 || strLowerCase.indexOf('images/') === 0 || strLowerCase.indexOf('/') === 0) {
						img = document.createElement('IMG');
						img.src = value;
						$(img).css('max-height', '24px');
						Handsontable.Dom.addEvent(img, 'mousedown', function (e) {
							e.preventDefault(); //prevent selection quirk
						});
						
						//Handsontable.Dom.empty(td);
						td.appendChild(img);
					}
					else {
						//En caso de ser un texto que no se reconoció como imagen, agrega un label simplemente
						var objLabel = document.createElement('LABEL');
						$(objLabel).addClass('noSpcLab');
						$(objLabel).text((value == null)?'':value);
						//Handsontable.Dom.empty(td);
						td.appendChild(objLabel);
						//Handsontable.renderers.TextRenderer.apply(this, arguments);
					}
					
					return td;
				}
				
				/* Función de callback después de haber hecho el upload de una imagen al server, indicando el nombre y ruta donde quedó el archivo (se usó por compatibilidad con
				componente uploadImg de otras ventanas
				En este caso tabName es el número de columna del atributo, strRowId es el número de row modificada, y strId es el key del valor modificado
				*/
				//Corrección Ticket #QND4OY: Sólo se aceptan imagenes JPG, GIF y PNG
				var errorMsg;
				errorMsg="";
				var blnError;
				blnError=false;
				
				function setUserImageForObject(strId, tabName, pathImg, strRowId, iObjectType, iObjectID) {
					//debugger;
					if (blnError) {
						if(errorMsg!="")
						{
							alert(errorMsg);
							errorMsg="";
						}
						else
						{
							alert("<?=translate("There was an error uploading the image file, please contact Technical Support")?>: \n" + pathImg);
						}
						
						//OMMC 2015-11-23: Limpiar el valor del input file ya que prevenía que se subiera la misma imagen 2 veces de manera sucesiva.
						//O que si se borraba la imagen y se deseaba subir una con el mismo nombre, no se podía.
						if($('#file'+strId).val() != ""){
							$('#file'+strId).val("");
						}
						
						blnError=false;
						return;
					}
					
					if (!objGridValues /*|| !$.isNumeric(strId)*/ || !$.isNumeric(tabName) || !$.isNumeric(strRowId)) {
						if (objGridValues && strId == "refresh") {
							//En este caso sólo se aplicará un refresh a la lista de valores ya que se acaba de subir un paquéte de imágenes (no hay nombre de imagen)
							applyFilter();
							$('#fileZIP').val(null);
							objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
						}
						return;
					}
					//strId = parseInt(strId);
					tabName = parseInt(tabName);
					strRowId = parseInt(strRowId);
					
					if (!pathImg) {
						pathImg = '';
					}
					
					objGridValues.setDataAtCell(strRowId, tabName, pathImg);
				}
				//JAPR
				function doOnLoad()
				{
					//JAPR 2016-07-19: Agregada la scrollbar horizontal a la tabla de valores (#CSOWSK)
					//Se agregó la celda con el nombre del DataSource para que no se calcule mal el espacio usado por la tabla de valores
					objFormsLayoutDataSource = new dhtmlXLayoutObject({
						parent: document.body,
						pattern: "2E"
					});
					objFormsLayoutDataSource.cells(dhxDataSourceName).hideHeader();
					objFormsLayoutDataSource.cells(dhxDataSourceName).attachHTMLString("<div id='div_catalogName' style='font-family: Helvetica; margin-left: 10px; width: 90%;'><b><span style='font-size:14px'>"+catalogName+"</span></b></div>");
					objFormsLayoutDataSource.cells(dhxDataSourceName).setMinHeight(20);
					objFormsLayoutDataSource.cells(dhxDataSourceName).setHeight(20);
					objFormsLayoutDataSource.cells(dhxDataSourceName).fixSize(true, true);
					<?	//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)?>
					<?	//Agregado un nuevo LayOut para incluir la barra de paginación	?>
					<?
						//Asigna una valor manual para la combo si el actual no corresponde a alguno de los default
						$intNumValuesPerPage = $this->NumValuesPerPage;
						$intManualFilterPos = 0;
						$strManualFilter = '';
						$strClosingSep = '';
						//@JAPR 2016-12-12: Agregado el esquema de paginación para obtener valores (#U7J0XU)
						$arrDefaultFilters = array("0" => 0, "20" => 20, "50" => 50, "75" => 75, "100" => 100);
						if (!isset($arrDefaultFilters[$intNumValuesPerPage])) {
							if ($intNumValuesPerPage < 20) {
								$intManualFilterPos = 1;
							}
							elseif ($intNumValuesPerPage < 50) {
								$intManualFilterPos = 2;
							}
							elseif ($intNumValuesPerPage < 75) {
								$intManualFilterPos = 3;
							}
							elseif ($intNumValuesPerPage < 100) {
								$intManualFilterPos = 4;
							}
							elseif ($intNumValuesPerPage > 100) {
								$intManualFilterPos = 5;
							}
							
							if ($intManualFilterPos) {
								$strManualFilter = "{text: ".$intNumValuesPerPage.", value: ".$intNumValuesPerPage.", selected:true}".(($intManualFilterPos != 5)?',':'');
								if ($intManualFilterPos == 5) {
									$strClosingSep = ',';
								}
							}
						}
						
						//Calcula la cantidad de páginas adicionales a mostrar si es que hay alguna cantidad configurada
						$arrPagesToShow = array();
						if (DEFAULT_ADDITIONAL_PAGESTOSHOW > 3) {
							//Considerando la página actual como el centro ($this->CurrentValuesPage), calcula que páginas deberían mostrarse hacia la izquierda y derecha hasta
							//llegar al inicio o final del total de páginas
							$intTotalPagesPerSide = DEFAULT_ADDITIONAL_PAGESTOSHOW / 2;
							//Agrega las páginas a la izquierda
							if ($this->CurrentValuesPage > 1) {
								for ($intPagesAdded = 0, $intPageNum = $this->CurrentValuesPage -1; $intPageNum > 0 && $intPagesAdded < $intTotalPagesPerSide; $intPageNum--, $intPagesAdded++) {
									array_unshift($arrPagesToShow, $intPageNum);
								}
							}
							
							//Agrega el selector de páginas, identificado por el valor de 0
							$arrPagesToShow[] = 0;
							
							//Agrega las páginas a la derecha
							if ($this->CurrentValuesPage < $this->TotalValuesPagesNum) {
								for ($intPagesAdded = 0, $intPageNum = $this->CurrentValuesPage +1; $intPageNum <= $this->TotalValuesPagesNum && $intPagesAdded < $intTotalPagesPerSide; $intPageNum++, $intPagesAdded++) {
									$arrPagesToShow[] = $intPageNum;
								}
							}
						}
					?>
					objFormsLayoutDataSourceTable = objFormsLayoutDataSource.cells(dhxDataSource).attachLayout({
						parent: document.body,
						pattern: "2E"
					});
					objFormsLayoutDataSourceTable.cells(dhxDataSourceNav).hideHeader();
					var objFormData = [
						{type:"block", blockOffset:10, offsetTop:0, list:[
							{type: "label", label: "<?=translate("Rows per page")?>", className: "dtSrcLbls"},
							{type:"newcolumn", offset:20},
							{type: "combo", label: "", name: "NumValuesPerPage", className:"nobrdrCbo dtSrcLbls", options:[
								{text: "<?=translate("All")?>", value: "0" <?=($intNumValuesPerPage <= 0)?', selected:true':''?>},
								<?=($intManualFilterPos == 1)?$strManualFilter:''?>
								{text: "20", value: "20" <?=($intNumValuesPerPage == 20)?', selected:true':''?>},
								<?=($intManualFilterPos == 2)?$strManualFilter:''?>
								{text: "50", value: "50" <?=($intNumValuesPerPage == 50)?', selected:true':''?>},
								<?=($intManualFilterPos == 3)?$strManualFilter:''?>
								{text: "75", value: "75" <?=($intNumValuesPerPage == 75)?', selected:true':''?>},
								<?=($intManualFilterPos == 4)?$strManualFilter:''?>
								{text: "100", value: "100" <?=($intNumValuesPerPage == 100)?', selected:true':''?>}<?=$strClosingSep?>
								<?=($intManualFilterPos == 5)?$strManualFilter:''?>
							]},	
							{type:"newcolumn", offset:20},
							{type: "label", label: "<?=str_ireplace("{var1}", $this->CurrentValuesPage, str_ireplace("{var2}", $this->TotalValuesPagesNum, translate("Page {var1} of {var2}")))?>", className:"pageCounterlbl dtSrcLbls"},
							{type:"newcolumn", offset:20},
							{type:"button", name: "btnPrevPage", offsetTop:10, value:"", className:"leftChgPagBtn"},
							<?
							//Muestra páginas adicionales con la posibilidad de configurar a un número específico de página con el input central que representa a la página actual,
							//cada página será una nueva columna y estará controlado por el total de páginas a mostrar configurado globalmente en config.php
							foreach ($arrPagesToShow as $intNumPage) {
								echo('{type:"newcolumn", offset:0},');
								if ($intNumPage == 0) {
									//Es el selector de páginas y contendrá la página actual
									echo('{type: "label", label: "'.translate("Page").'", className: "dtSrcLbls"},');
									echo('{type:"newcolumn", offset:0},');
									echo('{type:"input", name:"txtChangePage", value:"'.$this->CurrentValuesPage.'", inputWidth:50, className:"nobrdrCbo dtSrcLbls"},');
								}
								else {
									//Es el enlace a una página específica
									echo('{type:"button", name: "pglnk'.$intNumPage.'", offsetTop:10, value:"'.$intNumPage.'", className:"pglnkStyle dtSrcLbls"},');
								}
							}
							?>
							{type:"newcolumn", offset:0},
							{type:"button", name: "btnNextPage", offsetTop:10, value:"", className:"rightChgPagBtn"},
							{type:"newcolumn", offset:20},
							{type: "label", label: "<?=translate("Total rows").": ".(int) @self::$ArrDataSourceRowCount[$this->DataSourceID]?>", className: "dtSrcLbls"}
						]}
					];
					objValuesPageNavForm = objFormsLayoutDataSourceTable.cells(dhxDataSourceNav).attachForm(objFormData);
<?
					$strToolbarCell = "b";
					$intNavCellHeight = 0;
					if ($this->EnablePagination) {
						$strToolbarCell = "a";
						$intNavCellHeight = 81;
					}
?>
					objFormsLayoutDataSourceTable.cells(dhxDataSourceNav).setMinHeight(<?=$intNavCellHeight?>);
					objFormsLayoutDataSourceTable.cells(dhxDataSourceNav).setHeight(<?=$intNavCellHeight?>);
					objFormsLayoutDataSourceTable.cells(dhxDataSourceNav).fixSize(true, true);
					
					objValuesPageNavForm.attachEvent("onButtonClick", function(name) {
						console.log('objValuesPageNavForm.onButtonClick ' + name);
						switch (name) {
							case "btnPrevPage":	//Salta página previa en el orden
								if (intCurrentValuesPage <= 1) {
									break;
								}
								
								setTimeout(function () {
									intCurrentValuesPage--;
									doChangeValuesPage();
								}, 100);
								break;
							
							case "btnNextPage":	//Salta página siguiente en el orden
								if (intCurrentValuesPage >= intTotalValuesPagesNum) {
									break;
								}
								
								setTimeout(function () {
									intCurrentValuesPage++;
									doChangeValuesPage();
								}, 100);
								break;
							
							default:	//Salta a la página especificada por el nombre del botón
								if (name.substr(0, 5) == "pglnk") {
									var intNewValuesPage = parseInt(name.substr(5));
								}
								if (!$.isNumeric(intNewValuesPage) || intNewValuesPage <= 0) {
									break;
								}
								
								intCurrentValuesPage = intNewValuesPage;
								setTimeout(function () {
									doChangeValuesPage();
								}, 100);
								break;
						}
					});
					
					objValuesPageNavForm.attachEvent("onChange", function(name, value, state) {
						console.log("objCombo.onChange " + name + ' = ' + value);
						
						switch (name) {
							case "NumValuesPerPage":
								var intNewNumValuesPerPage = parseInt(this.getItemValue(name));
								if (!$.isNumeric(intNewNumValuesPerPage) || intNewNumValuesPerPage < 0) {
									break;
								}
								
								intNumValuesPerPage = intNewNumValuesPerPage;
								setTimeout(function () {
									doChangeValuesPage();
								}, 100);
								break;
						}
					});
					
					objValuesPageNavForm.attachEvent("onKeyup", function(inp, ev, name, value) {
						console.log("objInput.onKeyup " + name + ' = ' + value);
						
						if (ev.keyCode != 13) {
							return;
						}
						
						switch (name) {
							case "txtChangePage":	//Salta a la página configurada manualmente
								var intNewValuesPage = parseInt(this.getItemValue("txtChangePage"));
								if (!$.isNumeric(intNewValuesPage) || intNewValuesPage <= 0 || intNewValuesPage > intTotalValuesPagesNum) {
									break;
								}
								
								intCurrentValuesPage = intNewValuesPage;
								setTimeout(function () {
									doChangeValuesPage();
								}, 100);
								break;
						}
					});
					
					<?	//JAPR		?>
					objFormsLayoutDataSourceTable.cells(dhxDataSource).hideHeader();
					objFormsLayoutDataSourceTable.cells("<?=$strToolbarCell?>").attachToolbar({
						parent:"objectsCollection"
					});
					
					objToolbarAttr = objFormsLayoutDataSourceTable.cells("<?=$strToolbarCell?>").getAttachedToolbar();
					objToolbarAttr.setIconsPath("<?=$strScriptPath?>/images/admin/");
					var intPos = 0;
					var arrSelectOpts = [];
					
					//Agrega todos los botones personalizados, excepto si son considerados default, ya que en ese caso se personalizó sólo su diseño pero no debe
					//duplicar el botón (debe ser un array)
					/*@JRPP 2015-09-08:La opcion de agregar se puede utilizar en todos los typos de datasource*/
					if (dataSourceType == 0 || dataSourceType == <?=dstyManual?> || dataSourceType == <?=dstyeBavel?>) {
						var strId = "Add";
						var strLabel = "<?=translate("Add")?>";
						var strImage = "add.png";
						arrSelectOpts.push([strId, 'obj', strLabel, strImage]);
						//objToolbarAttr.addButton(strId, intPos++, strLabel, strImage);
					}
					/*@JRPP 2015-09-08: Todas excepto eBavel*/
					if (dataSourceType == 0 || dataSourceType == <?=dstyManual?> || dataSourceType == <?=dstyExcel?> ||dataSourceType == <?=dstyDropBox?> || dataSourceType == <?=dstyGoogleDrive?> || dataSourceType == <?=dstyFTP?>) {
						var strId = "Import";
						var strLabel = "<?=translate("Upload now")?>";
						var strImage = "catalogEntry.gif";
						arrSelectOpts.push([strId, 'obj', strLabel, strImage]);
						//objToolbarAttr.addButton(strId, intPos++, strLabel, strImage);
					}
					if (dataSourceType == 0 || dataSourceType == <?=dstyManual?> || dataSourceType == <?=dstyExcel?> || dataSourceType == <?=dstyDropBox?> || dataSourceType == <?=dstyGoogleDrive?> || dataSourceType == <?=dstyFTP?> || dataSourceType == <?=dstyHTTP?> || dataSourceType == <?=dstyTable?>) {
						//@JRPP 2014-05-13: Se comenta seccion de codigo hsata pulir detalles
						var strId = "Reload";
						var strLabel = "<?=translate("Schedule it")?>";
						var strImage = "add.png";
						arrSelectOpts.push([strId, 'obj', strLabel, strImage]);
						//objToolbarAttr.addButton(strId, intPos++, strLabel, strImage);
					}
					
					//OMMC 2019-03-22: Agregado el rediseño de la toolbar.
					var strId = "Actions";
					var strLabel = "<?=translate("Actions")?>";
					objToolbarAttr.addButtonSelect("strId", intPos++, strLabel, arrSelectOpts, "actions.png", "actions.png");
					
					//@JRPP 2016-01-27: Agregamos nueva opcion al detalle del datasource donde podremos visalizar una lista de todas las formas
					//					que utilizan el datasource seleccionado
					if (true) {
						var strId = "Forms";
						var strLabel = "<?=translate("Forms")?>";
						var strImage = "forms_18x18.png";
						objToolbarAttr.addButton(strId, intPos++, strLabel, strImage);
					}
					/*if (dataSourceType == 0 || dataSourceType == <?=dstyManual?> || dataSourceType == <?=dstyExcel?> || dataSourceType == <?=dstyDropBox?> || dataSourceType == <?=dstyGoogleDrive?> || dataSourceType == <?=dstyFTP?>) {
						//@JRPP 2014-05-13: Se comenta seccion de codigo hsata pulir detalles
						var strId = "Recurrence";
						var strLabel = "<?=translate("Recurrence")?>";
						var strImage = "edit.png";
						objToolbarAttr.addButtonTwoState(strId, intPos++, strLabel, strImage);
					}*/
					/*Agrego el input*/
					objToolbarAttr.addInput("Search1", intPos++, "", "200");
					objToolbarAttr.addButton("Search2", intPos++, "", "lupita.gif");
					
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					var strLabel = "<?=translate("Model")?>";
					objToolbarAttr.addButtonTwoState("IsModel", intPos++, strLabel, null, null);
					objToolbarAttr.setItemState("IsModel", isModel, null);
					if (isModel) {
						objToolbarAttr.setItemImage("IsModel", "AR.png");
					} else {
						objToolbarAttr.setItemImage("IsModel", "notAR.png");
					}
					
					objToolbarAttr.attachEvent("onClick", function(id) {
						if (id == "Home"){
							var url = 'main.php?BITAM_SECTION=DataSourceCollection'
							document.location.href = url;
						}else if(id  == "Add"){
							addObject();
						}else if(id  == "Import" && DatasourceFirst){
							$(parent.window.document.getElementById('fondo')).show();
							importObject();
						}else if(id  == "Search2"){
							applyFilter();
						}else if(id  == "Recurrence"){
							//buildRecurrenceWindow();
						}else if(id  == "Reload" && DatasourceFirst){
							$(parent.window.document.getElementById('fondo')).show();
							buildReloadWindow();
						}else if(id  == "Forms" && DatasourceFirst){
							buildFormsWindow(formsFromDatasource);
						}
					});
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					objToolbarAttr.attachEvent("onStateChange", function(id, state) {
						if (id == "IsModel") {
							doSendDataModifyDataSource({IsModel: state?1:0});
						}
					});
									
					//OMMC 2015-11-20: Agregado para el boton que muestra si el catálogo se cargo sin errores.
					if(errorDisplayMsg != ''){
						errorDisplay = "<div style='width: 10px; height: 10px; border-radius: 10px; background-color: #C60500; margin-top: 5px;'></div>";
						objToolbarAttr.addButton("errorDisplay", intPos++, errorDisplay);
							var errorPopUp = new dhtmlXPopup({ 
					        toolbar: objToolbarAttr,
					        id: "errorDisplay"
						});
						errorPopUp.attachHTML(errorDisplayMsg);
					}else{
						errorDisplay = "<div style='width: 10px; height: 10px; border-radius: 10px; background-color: #009C0A; margin-top: 5px;'></div>";
						objToolbarAttr.addButton("errorDisplay", intPos++, errorDisplay);
					}
					
					//OMMC 2015/09/25: Información adicional para el DataSource
					fileInfoFieldText = "<div style='font-family: Helvetica; margin-top: -7px; margin-left: 10px; float:left; width: 200px'><b style='font-size:14px'><?= translate('Data Source')?>:</b><br><span style='font-size:12px'>"+dataSourceSourceName+"</span></div>";
					typeInfoFieldText = "<div style='font-family: Helvetica; margin-top: -7px; float:left; width: 150px'><b style='font-size:14px'><?= translate('Data Source Type')?>:</b><br><span style='font-size:12px'>"+dataSourceTypeName+"</span></div>";
					dateInfoFieldText = "<div style='font-family: Helvetica; margin-top: -7px; float:left; width: 150px'><b style='font-size:14px'><?= translate('Last Synchronization')?>:</b><br><span style='font-size:12px'>"+dataSourceSyncDate+"</span></div>";
					objToolbarAttr.addText("fileInfoField", intPos++, fileInfoFieldText);
					//OMMC 2015-12-30: Tooltip agregado en caso de que el nombre sea muy largo.
					if(sourceTooltip != ''){
						objToolbarAttr.setItemToolTip("fileInfoField", sourceTooltip);
					}
					objToolbarAttr.addText("typeInfoField", intPos++, typeInfoFieldText);
					objToolbarAttr.addText("dateInfoField", intPos++, dateInfoFieldText);
					
					objToolbarAttr.attachEvent("onEnter", function(id, value) {
						applyFilter();
					});
					//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)
					objFormsLayoutDataSourceTable.cells(dhxDataSource).attachHTMLString("<div id='hotDataSource'></div>");
					//JAPR 2016-07-19: Agregada la scrollbar horizontal a la tabla de valores (#CSOWSK)
					objGridValuesContainer = document.getElementById('hotDataSource');
					//OMMC 2015-10-01: Agregado un campo para mostrar el nombre del catálogo (ISSUE: X76OY1)
					//$("<div id='div_catalogName' style='font-family: Helvetica; margin-left: 10px; width: 90%;'><b><span style='font-size:14px'>"+catalogName+"</span></b></div>").insertBefore('#hotDataSource');
					
					//JAPR 2016-07-19: Agregada la scrollbar horizontal a la tabla de valores (#CSOWSK)
					objFormsLayoutDataSourceTable.attachEvent("onResizeFinish", function() {
						doResizeValuesTable();
					});
					
					//JAPR 2016-06-21: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
					Handsontable.renderers.registerRenderer('imageRenderer', imageRenderer);
					attributeIsImage = [ <?= implode(',', $attributeIsImage) ?> ];
					colMemberIDs = [ <?= implode(',', $colMemberIDs) ?> ];
					//JAPR
					var colHeaders = [ <?= implode(',', $headers) ?> ];
					var colWidths = [ <?= implode(',', $widths) ?> ];
					var columns = <?= json_encode($columns) ?>;
					data = <?= json_encode($data) ?>;
					membersDatasource = <?= json_encode($membersDatasource) ?>;
					dataString =  JSON.stringify(data);
					eBavelfieldforms = {};
					<?if (!is_null($eBavelfieldforms)) {?>
					eBavelfieldforms = <?= json_encode($eBavelfieldforms) ?>;
					<?}?>
					objGridValues = new Handsontable(objGridValuesContainer, {
												data: data,
												minSpareRows: 1,
												colHeaders: colHeaders,
												colWidths: colWidths,
												manualColumnResize: true,
												columnSorting: true,
												rowHeaders: true,
												columns: columns,
												//contextMenu: [ 'remove_row' ],
												//JAPR 2016-01-07: Incrementado el límite de rows permitidas para copia al límite de MX Excel 2013 pues el default es de 1000
												copyRowsLimit: 1048576,
												contextMenu: {
													 callback: function (key, options) {
														switch(key) {
															case 'delete_col':
																var selectedArray = objGridValues.getSelected();
																// timeout is used to make sure the menu collapsed before alert is shown
																setTimeout(function () {
																	if (selectedArray && selectedArray[0] == 0 && selectedArray[2] == (objGridValues.countRows() - 1)) { 
																		if (confirm('<?= translate("The selected columns will be removed.\\nAre you sure?") ?>')) {
																			//var colMemberIDs = [ <?= implode(',', $colMemberIDs) ?> ];
																			var columnsToDelete = [];
																			for (var col = selectedArray[1]; col <= selectedArray[3]; col++) {
																				columnsToDelete.push({ col: col, memberID: colMemberIDs[col] });
																			}
																			doSendDataDeleteAttr(columnsToDelete);
																		}
																	}
																}, 1);
																break;
															//JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
															case 'set_as_attribute':
																var selectedArray = objGridValues.getSelected();
																if (!colMemberIDs || !attributeIsImage || !selectedArray || selectedArray[1] != selectedArray[3]) {
																	return;
																}
																
																attributeIsImage[selectedArray[1]] = 0;
																doSendDataEditAttr({MemberID:colMemberIDs[selectedArray[1]], IsImage:attributeIsImage[selectedArray[1]]});
																break;
															case 'set_as_image':
																var selectedArray = objGridValues.getSelected();
																if (!colMemberIDs || !attributeIsImage || !selectedArray || selectedArray[1] != selectedArray[3]) {
																	return;
																}
																
																attributeIsImage[selectedArray[1]] = 1;
																doSendDataEditAttr({MemberID:colMemberIDs[selectedArray[1]], IsImage:attributeIsImage[selectedArray[1]]});
																break;
															case 'uploadImageZip':
																var selectedArray = objGridValues.getSelected();
																//Sólo se puede cambiar un atributo a la vez
																if (!attributeIsImage || !selectedArray || selectedArray[1] != selectedArray[3]) {
																	return;
																}
																
																//objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOn();
																document.getElementById('fileZIP').click();
																break;
														}
													},
													items: {
														'delete_col': {
															name: '<?= translate("Remove Column") ?>',
															disabled: function () {
																var selectedArray = objGridValues.getSelected();
																return !(selectedArray && selectedArray[0] == 0 && selectedArray[2] == (objGridValues.countRows() - 1));
															}
														},
														'remove_row': {
															name: '<?= translate("Remove Row") ?>',
															disabled: function (index, algo) {
																<?
																//@JAPR 2016-07-21: Corregido un bug, no deben ser editables los catálogos de tipo eBavel ni ODBC (#AZFTW7)
																if ($this->Type == dstyeBavel || $this->Type == dstyTable) {
																?>
																	return true;
																<?
																}
																?>
																var selectedArray = objGridValues.getSelected();
																//JAPR 2016-06-22: Corregido un bug, estaba permitiendo eliminar la última row pero no la primera, la validación debía ser lo contrario (#ME58B5)
																return !(selectedArray && selectedArray[3] == (objGridValues.countCols() - 1) && selectedArray[1] == 0 && !(selectedArray[0] == objGridValues.countRows() - 1));
															}
														}
														<?
														//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
														if ($this->Type != dstyeBavel && $this->Type != dstyTable) {?>
														, 'set_as_attribute': {
															name: '<?= translate("Change to attribute") ?>',
															disabled: function (index, algo) {
																var selectedArray = objGridValues.getSelected();
																//Sólo se puede cambiar un atributo a la vez
																if (!attributeIsImage || !selectedArray || selectedArray[1] != selectedArray[3]) {
																	return true;
																}
																
																var intIsImage = attributeIsImage[selectedArray[1]];
																return !(selectedArray && selectedArray[0] == 0 && selectedArray[2] == (objGridValues.countRows() - 1) && intIsImage);
															}
														},
														'set_as_image': {
															name: '<?= translate("Change to attribute: image") ?>',
															disabled: function (index, algo) {
																var selectedArray = objGridValues.getSelected();
																//Sólo se puede cambiar un atributo a la vez
																if (!attributeIsImage || !selectedArray || selectedArray[1] != selectedArray[3]) {
																	return true;
																}
																
																var intIsImage = attributeIsImage[selectedArray[1]];
																return !(selectedArray && selectedArray[0] == 0 && selectedArray[2] == (objGridValues.countRows() - 1) && !intIsImage);
															}
														},
														'uploadImageZip': {
															name: '<?= translate("Upload images file ZIP") ?>',
															disabled: function (index, algo) {
																var selectedArray = objGridValues.getSelected();
																//Sólo se puede cambiar un atributo a la vez
																if (!attributeIsImage || !selectedArray || selectedArray[1] != selectedArray[3]) {
																	return true;
																}
																
																var intIsImage = attributeIsImage[selectedArray[1]];
																return !(selectedArray && selectedArray[0] == 0 && selectedArray[2] == (objGridValues.countRows() - 1) && intIsImage);
															}
														}
														<?}?>
													}
												},
												outsideClickDeselects: false,
												search: true,
												afterGetColHeader: function(col, thElem) {
													thElem.columnIndex = col;
													thElem.addEventListener("contextmenu", function(event) {
														objGridValues.contextMenu.closeAll();
														event.preventDefault();
														event.stopImmediatePropagation();
														var selectedArray = objGridValues.getSelected();
														if (!(selectedArray && 
																selectedArray[0] == 0 && 
																selectedArray[2] == (objGridValues.countRows() - 1) && 
																this.columnIndex >= selectedArray[1] && 
																this.columnIndex <= selectedArray[3])) {
															objGridValues.selectCell(0, this.columnIndex, objGridValues.countRows() - 1, this.columnIndex);
														}
														var menu = objGridValues.contextMenu.createMenu();
														var items = objGridValues.contextMenu.getItems(objGridValues.getSettings().contextMenu);
														objGridValues.contextMenu.show(menu, items);
														objGridValues.contextMenu.setMenuPosition(event, menu);
														objGridValues.contextMenu.eventManager.addEventListener(document.documentElement, 'mousedown', function() { objGridValues.contextMenu.closeAll() });
													});
												},
												beforeRemoveRow: function(index, ammount) {
													var allData = this.getData();
													var dataChanges = {};
													dataChanges.deletedData = [];
													var i = index;
													while (i < index + ammount && i < allData.length) {
														var id = allData[i]['id'];
														if (id !== null) {
															dataChanges.deletedData.push(id);
														}
														i++;
													}
													if (dataChanges.deletedData.length > 0) {
														if (confirm('<?= translate("The selected rows will be removed.\\nAre you sure?") ?>')) {
															objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOn();
															window.dhx4.ajax.post('main.php?<?= $this->get_QueryString() ?>', 'dataChanges=' + JSON.stringify(dataChanges), 
																function(loader) {
																	if (loader.xmlDoc.status == 200 && loader.xmlDoc.status < 300) {
																		var result = null;
																		try {
																			result = JSON.parse(loader.xmlDoc.responseText); // convert response to json object
																		} catch (e) {
																		}																		
																		if (result == null || (typeof(result.data) == 'undefined' && typeof(result.error) == 'undefined')) {
																			alert('<?= translate("The server returned an invalid or unrecognized response") ?>');
																			objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
																			data = JSON.parse(dataString);
																			applyFilter();
																		} else if (typeof(result.error) !== 'undefined') {
																			alert('<?= translate("Error:") ?>' + result.error);
																			objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
																			if (typeof(result.data) !== 'undefined') {
																				if (result.data !== null) {
																					data = result.data;
																					dataString = JSON.stringify(data);
																					applyFilter();
																				} else {
																					dataString = JSON.stringify(data);
																					applyFilter();
																				}
																			} else {
																				data = JSON.parse(dataString);
																				applyFilter();
																			}
																		} else {
																			objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
																			if (result.data !== null) {
																				data = result.data;
																				dataString = JSON.stringify(data);
																				applyFilter();
																			} else {
																				dataString = JSON.stringify(data);
																				applyFilter();
																			}
																		}
																	} else {
																		alert('<?= translate("Connection Error:") ?>' + loader.xmlDoc.statusText);
																		objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
																		data = JSON.parse(dataString);
																		applyFilter();
																	}
															});
														} else {
															return false;
														}
													}
													return true;
												},
												beforeChange: function(changes, source) {
													var emptyRows = [];
													var allData = this.getData();
													for (var i = 0; i < changes.length; i++) {
														if (!allData[changes[i][0]]['id']) {
															if (changes[i][3]) {
																var p = emptyRows.indexOf(changes[i][0]);
																if (p >= 0) {
																	emptyRows.splice(p, 1);
																}
															} else {
																emptyRows.push(changes[i][0]);
															}
														}
													}
													for (var i = changes.length - 1; i >= 0; i--) {
														if (emptyRows.indexOf(changes[i][0]) >= 0) {
															changes.splice(i, 1);
														} else {
															if (changes[i][2] == changes[i][3]) {
																changes.splice(i, 1);
															}
														}
													}
												},
												afterChange: function(changes, source) {
													if (source == 'loadData') {
														return;
													}
													var allData = this.getData();
													var dataChanges = {};
													dataChanges.insertedData = {};
													dataChanges.updatedData = {};
													for (var i = 0; i < changes.length; i++) {
														var id = allData[changes[i][0]]['id'];
														if (id == null) {
															if (!dataChanges.insertedData.hasOwnProperty(changes[i][0])) {
																dataChanges.insertedData[changes[i][0]]= {};
															}
															var row = dataChanges.insertedData[changes[i][0]];
															row[changes[i][1]] = changes[i][3]; 
														} else {
															if (!dataChanges.updatedData.hasOwnProperty(id)) {
																dataChanges.updatedData[id] = {};
															}
															row = dataChanges.updatedData[id];
															row[changes[i][1]] = changes[i][3];
														}
													}
													objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOn();
													//JAPR 2016-07-18: Corregido el paso de parámetros (#LSJYAB)
													window.dhx4.ajax.post('main.php?<?= $this->get_QueryString() ?>', 'dataChanges=' + encodeURIComponent(JSON.stringify(dataChanges)),	
														function(loader) {
															if (loader.xmlDoc.status == 200 && loader.xmlDoc.status < 300) {
																var result = null;
																try {
																	result = JSON.parse(loader.xmlDoc.responseText); // convert response to json object
																} catch (e) {
																}																		
																if (result == null || (typeof(result.data) == 'undefined' && typeof(result.error) == 'undefined')) {
																	alert('<?= translate("The server returned an invalid or unrecognized response") ?>');
																	objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
																	data = JSON.parse(dataString);
																	applyFilter();
																} else if (typeof(result.error) !== 'undefined') {
																	alert('<?= translate("Error:") ?>' + result.error);
																	objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
																	if (typeof(result.data) !== 'undefined') {
																		if (result.data !== null) {
																			data = result.data;
																			dataString =  JSON.stringify(data);
																			applyFilter();
																		} else {
																			dataString =  JSON.stringify(data);
																			applyFilter();
																		}
																	} else {
																		data = JSON.parse(dataString);
																		applyFilter();
																	}
																} else {
																	objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
																	if (result.data !== null) {
																		data = result.data;
																		dataString =  JSON.stringify(data);
																		applyFilter();
																	} else {
																		dataString = JSON.stringify(data);
																		applyFilter();
																	}
																}
															} else {
																alert('<?= translate("Connection Error:") ?>' + loader.xmlDoc.statusText);
																objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
																data = JSON.parse(dataString);
																applyFilter();
															}
													});
												}
					});
				}
				
				//JAPR 2016-07-19: Agregada la scrollbar horizontal a la tabla de valores (#CSOWSK)
				/* Invoca al método que reconstruye la tabla para que en base al nuevo tamaño coloque los scrollbars adecuados a su contenido */
				function doResizeValuesTable() {
					if (!objGridValues || !objGridValues.render) {
						return;
					}
					
					objGridValues.render();
				}
				//JAPR
				
				function importObject() {
					DatasourceFirst = false;
					var dataSourceId ='<?= $this->DataSourceID ?>';
					objGridValues.unlisten();
					require(['views/loadexcel/wizardLoadExcel'], function( Wizard ) {
						$(parent.window.document.getElementById('fondo')).hide();
						/** Funcion para traducciones */
						$.t = function( s ) { return s; };
						Wizard(dataSourceId, null, null).done(function(idCatalog,label) {
							objGridValues.listen();
							var dhxList = "b";
							strURL = "<?= 'main.php?'.$this->get_QueryString() ?>";
							parent.objFormsLayoutMenu.cells(dhxList).progressOn();
							parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
						});
					});
				}
				function buildFormsWindow(formsFromDatasource) {
					if (objGridValues.view.isCellEdited()) {
						var selectedArray = objGridValues.getSelected();
						if (selectedArray) {
							objGridValues.selectCell(selectedArray[0], selectedArray[1], selectedArray[2], selectedArray[3]);
						}
					}
					objGridValues.unlisten();
					if (!objWindows) {
						//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
						objWindows = new dhtmlXWindows({
							image_path:"images/"
						});
					}
					objDialog = objWindows.createWindow({
						id:"newObject",
						left:0,
						top:0,
						width:400,
						height:300,
						center:true,
						modal:true
					});
					
					objDialog.setText('<?=translate("Forms")?>');
					objDialog.denyPark();
					objDialog.denyResize();
					objDialog.setIconCss("without_icon");
					
					//Al cerrar realizará el cambio de sección de la pregunta
					objDialog.attachEvent("onClose", function() {
						objGridValues.listen();
						return true;
					});
					var objForm = objDialog.attachGrid();
					objForm.setHeader("<?=translate("Forms")?>", null, ["background-color:cccccc;cursor:pointer;"]);//La cabecera de las columnas
			        //objForm.setInitWidths("100%");          //el ancho de las columna s 
					objForm.setInitWidthsP("100");
			        objForm.setColAlign("left");       //la alineacion de las columnas  
			        objForm.setColTypes("ro");                //el tipo "ro"(solo lectura)
			        //objForm.setColSorting("str");          //el tipo  "str"(cadena)
			        objForm.init();
			        rows = [];
					data = {};
					for (var x in formsFromDatasource) {
						rows.push({id:formsFromDatasource[x].SurveyID, data: [formsFromDatasource[x].SurveyName]});
					}
					data.rows = rows;
			        objForm.parse(data,"json");
					//objForm.setStyle("font-size: 14px; font-weight: bold;", "cursor:pointer;");
				}
				function addObject(){
					if (objGridValues.view.isCellEdited()) {
						var selectedArray = objGridValues.getSelected();
						if (selectedArray) {
							objGridValues.selectCell(selectedArray[0], selectedArray[1], selectedArray[2], selectedArray[3]);
						}
					}
					objGridValues.unlisten();
					if (!objWindows) {
						//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
						objWindows = new dhtmlXWindows({
							image_path:"images/"
						});
					}
					objDialog = objWindows.createWindow({
						id:"newObject",
						left:0,
						top:0,
						width:400,
						height:500,
						center:true,
						modal:true
					});
					
					objDialog.setText('<?=translate("New member")?>');
					objDialog.denyPark();
					objDialog.denyResize();
					objDialog.setIconCss("without_icon");
					
					//Al cerrar realizará el cambio de sección de la pregunta
					objDialog.attachEvent("onClose", function() {
						objGridValues.listen();
						return true;
					});
					var objFormData = [];
					// El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma 
					switch (dataSourceType) {
						case "<?=dstyManual?>":
						case "<?=dstyExcel?>":
						case "<?=dstyDropBox?>":
						case "<?=dstyGoogleDrive?>":
						case "<?=dstyFTP?>":
							//@JRPP 2015-09-02: En caso de ser una captura normal muestro un input para que indicque el nombre del nuevo miembro
							objFormData = [
								{type:"settings"/*, offsetLeft:20*/},
								{type:"input", name:"MemberName", label:"<?=translate("Name")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
								//JAPR 2016-06-21: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
								{type:"checkbox", name:"IsImage", label:"<?=translate("Image attribute")?>", checked:false, position:"label-right", offsetLeft:100, labelLeft:50},
								//JAPR
								{type:"block", blockOffset:0, offsetLeft:100, list:[
									{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
									{type:"newcolumn"},
									{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
								]}
							];
							break;
						case "<?=dstyeBavel?>":
							var options = [];
							for (var id in eBavelfieldforms) {
								var objoption = {};
								objoption.value = eBavelfieldforms[id].id_fieldform;
								objoption.text = eBavelfieldforms[id].label;
								options.push(objoption);
							}
							//@JRPP 2015-09-02: Para el caso de importaciones desde eBavel si se permite agregar pero en vez de captura manual,
							//se debe de mostrar un listado con los fieldsForms de la forma seleccionada de eBavel.
							objFormData = [
								{type:"settings"/*, offsetLeft:20*/},
								{type: "combo", name: "myCombo", label: "<?=translate("Attribute")?>", options:options},
								{type:"block", blockOffset:0, offsetLeft:100, list:[
									{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
									{type:"newcolumn"},
									{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
								]}
							];
							break;
					}
					var objForm = objDialog.attachForm(objFormData);
					objForm.adjustParentSize();
					objForm.setItemFocus("MemberName");
					objForm.attachEvent("onBeforeValidate", function (id) {
						console.log('Before validating the form: id == ' + id);
					});
					
					objForm.attachEvent("onAfterValidate", function (status) {
						console.log('After validating the form: status == ' + status);
					});
					
					objForm.attachEvent("onValidateSuccess", function (name, value, result) {
						console.log('Validating rule ok: name == ' + name + ', value == ' + value + ', result == ' + result);
					});
					
					objForm.attachEvent("onValidateError", function (name, value, result) {
						console.log('Error validating the form: name == ' + name + ', value == ' + value + ', result == ' + result);
					});
					
					
					objForm.attachEvent("onButtonClick", function(name) {
						switch (name) {
							case "btnCancel":
								setTimeout(function () {
									doUnloadDialog();
								}, 100);
								break;
								
							case "btnOk":
								setTimeout(function() {
									if (objForm.validate()) {
										doSendData();
									}
								}, 100);
								break;
						}
					});
				}
				function doSendData() {
					var catalogId ='<?= $this->DataSourceID?>';
					var TableName ='<?= GetValidJScriptText($this->TableName,array("fullhtmlspecialchars" => 1))?>';
					if (!objDialog) {
						return;
					}
					objDialog.progressOn();
					var objForm = objDialog.getAttachedObject();
					var objData = objForm.getFormData();
					var urlPath = 'loadexcel/';
					var member = "";
					var isImage = 0;
					if (dataSourceType == "<?=dstyeBavel?>"){
						var objfield = eBavelfieldforms[objData.myCombo];
						var idFieldForm = objfield.id_fieldform;
						var tagname = objfield.tagname
						member = objfield.label;
						//JAPR 2016-07-18: Corregido el paso de parámetros (#LSJYAB)
						$.get(urlPath +'action.php?action=createMember&Members=' + encodeURIComponent(member) + '&catalogId=' + catalogId + '&TableName=' + TableName + '&type=' + dataSourceType + '&idFieldForm=' + idFieldForm + '&tagname=' + tagname).done(function( r ) {
							//termino de insertar por tanto cargo la pantalla del detalle
							var dhxList = "b";
							strURL = "<?= 'main.php?'.$this->get_QueryString() ?>";
							parent.objFormsLayoutMenu.cells(dhxList).progressOn();
							parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
						});
					}else {
						member = objData.MemberName;
						//JAPR 2016-06-21: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
						isImage = objData.IsImage;
						//JAPR 2016-07-18: Corregido el paso de parámetros (#LSJYAB)
						$.get(urlPath +'action.php?action=createMember&Members=' + encodeURIComponent(member) + '&catalogId=' + catalogId + '&TableName=' + TableName + '&type=' + dataSourceType + '&IsImage=' + isImage).done(function( r ) {
							//termino de insertar por tanto cargo la pantalla del detalle
							var dhxList = "b";
							strURL = "<?= 'main.php?'.$this->get_QueryString() ?>";
							parent.objFormsLayoutMenu.cells(dhxList).progressOn();
							parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
						});
					}

				}
				
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				//Permite editar propiedades del DS cargado en base a un objeto{property: value}
				function doSendDataModifyDataSource(oData) {
					if (!oData) {
						return;
					}
					
					console.log("doSendDataModifyDataSource");
					
					//Prepara los parámetros con los datos a enviar
					var objParams = {
						RequestType:1,
						Process:"Edit",
						DataSourceID:<?=$this->DataSourceID?>,
						ObjectType:<?=otyDataSource?>
					};
					
					$.extend(objParams, oData);
					
					var strParams = '';
					var strAnd = '';
					for (var strProp in objParams) {
						//JAPR 2018-11-01: Corregido un bug con el escape de caracteres en requests (#S0SRC1)
						strParams += strAnd + strProp+"="+encodeURIComponent(objParams[strProp]);
						//JAPR
						strAnd = '&';
					}
					var fnEditAttrCallback = function() {
						var dhxList = "b";
						strURL =  "<?='main.php?'.$this->get_QueryString()?>";
						parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
					}
					
					objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOn();
					window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
						doSaveConfirmation(loader, fnEditAttrCallback, false, true);
					});
				}

				//Elimina el atributo indicado
				function doSendDataDeleteAttr(columnsToDeleteArray) {
					console.log("doSendDataDeleteAttr");
					
					//Prepara los parámetros con los datos a enviar
					var objParams = {
						//SurveyID:intSurveyID,
						//Design:1,
						RequestType:1,
						Process:"Delete",
						DataSourceID:<?=$this->DataSourceID?>,
						ObjectType:<?=otyDataSourceMember?>
					};
					
					var strParams = '';
					var strAnd = '';
					for (var strProp in objParams) {
						//JAPR 2018-11-01: Corregido un bug con el escape de caracteres en requests (#S0SRC1)
						strParams += strAnd + strProp+"="+encodeURIComponent(objParams[strProp]);
						//JAPR
						strAnd = '&';
					}
					for (var i = 0; i < columnsToDeleteArray.length ; i++) {
						var iDataSourceMemberID = columnsToDeleteArray[i].memberID;
						strParams += strAnd + "ObjectID[]="+iDataSourceMemberID;
						strAnd = '&';
					}
					var fnDeleteAttrCallback = function() {
						var dhxList = "b";
						strURL =  "<?='main.php?'.$this->get_QueryString()?>";
						parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
					}
					
					objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOn();
					//JAPR 2015-10-25: Agregado el proceso de confirmación de borrado cuando existen referencias
					//Almacena los parámetro de la última invocación de borrado para volver a invocar al método si se desea continuar con el proceso después de una confirmación
					//Si el objeto ya existiera y tuviera el parámetro para evitar validaciones activo, lo tiene que enviar como parte del request
					if (goObjectToDelete && goObjectToDelete.SkipValidation) {
						strParams += strAnd + "SkipValidation=1";
						strAnd = '&';
					}
					
					goObjectToDelete = new Object();
					goObjectToDelete.columnsToDeleteArray = columnsToDeleteArray;
					//JAPR
					window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
						doSaveConfirmation(loader, fnDeleteAttrCallback, true);
					});
				}
				
				//JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
				//Realiza la actualización de alguna propiedad de un atributo recibida y además identificado en el objeto oParams
				function doSendDataEditAttr(oParams) {
					if (!oParams || !oParams.MemberID) {
						return;
					}
					
					console.log("doSendDataEditAttr");
					
					//Prepara los parámetros con los datos a enviar
					var objParams = {
						//SurveyID:intSurveyID,
						//Design:1,
						RequestType:1,
						Process:"Edit",
						DataSourceID:<?=$this->DataSourceID?>,
						ObjectType:<?=otyDataSourceMember?>
					};
					
					$.extend(objParams, oParams);
					
					var strParams = '';
					var strAnd = '';
					for (var strProp in objParams) {
						//JAPR 2018-11-01: Corregido un bug con el escape de caracteres en requests (#S0SRC1)
						strParams += strAnd + strProp+"="+encodeURIComponent(objParams[strProp]);
						//JAPR
						strAnd = '&';
					}
					var fnEditAttrCallback = function() {
						var dhxList = "b";
						strURL =  "<?='main.php?'.$this->get_QueryString()?>";
						parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
					}
					
					objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOn();
					window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
						doSaveConfirmation(loader, fnEditAttrCallback, false, true);
					});
				}
				
				/* Procesa la respuesta de un request de actualización de datos asíncrona y presenta el error en pantalla en caso de existir alguno
				Esta función está pensada como un callback de un request Ajax que regresará un contenido en JSon con infomación adicional de error, sin embargo se puede recibir
				un callback adicional en el parámetro oCallbackFn
				El parámetro bRemoveProgress se usa generalmente sólo en requests Ajax y permite restablecer la ventana de diseño en caso de error, removiendo la progres bar del TabBar
				*/
				function doSaveConfirmation(loader, oCallbackFn, bRemoveProgress, bForceCallback) {
					console.log('doSaveConfirmation');
					
					$('#divSaving').hide();
					
					if (!loader || !loader.xmlDoc) {
						if (bRemoveProgress) {
							objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
						}
						return;
					}
					
					if (loader.xmlDoc && loader.xmlDoc.status != 200) {
						alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
						if (bRemoveProgress) {
							objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
						}
						return;
					}
					
					//Si llega a este punto es que se recibió una respuesta correctamente
					var response = loader.xmlDoc.responseText;
					try {
						var objResponse = JSON.parse(response);
					} catch(e) {
						alert(e + "\r\n" + response.substr(0, 1000));
						if (bRemoveProgress) {
							objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
						}
						return;
					}
					
					if (objResponse.error) {
						alert(objResponse.error.desc);
						if (bRemoveProgress) {
							objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
						}
						return;
					}
					else {
						//JAPR 2015-10-25: Agregado el proceso de confirmación de borrado cuando existen referencias
						if (objResponse.confirm && objResponse.confirm.desc) {
							//Si se pidió confirmación, pide al usuario autorización y si se acepta, repite el proceso de borrado pero esta vez ya sin validaciones
							var answer = confirm(objResponse.confirm.desc);
							if (answer) {
								if (goObjectToDelete) {
									goObjectToDelete.SkipValidation = 1;
								}
								
								setTimeout(function() {
									doSendDataDeleteAttr(goObjectToDelete.columnsToDeleteArray);
								}, 100);
								return;
							}
							
							//En este caso simplemente quita la barra de progreso para que todo regrese a la normalidad y ya no continua con el proceso
							if (bRemoveProgress) {
								objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
							}
							return;
						}
						else {
							if (objResponse.warning) {
								console.log(objResponse.warning.desc);
							}
						}
						//JAPR
					}
					
					//JAPR 2015-07-17: Agregado el borrado de objetos directamente desde Ajax, los cuales no regresan código desde el server sino sólo una referencia para que se
					//pueda ejecutar un código de callback
					if ((objResponse.deleted && objResponse.objectType) || bForceCallback) {
						if (oCallbackFn && typeof oCallbackFn == "function") {
							oCallbackFn(objResponse);
						}
						
						if (bRemoveProgress) {
							objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
						}
						return;
					}
					//JAPR 2015-10-25: Agregado el proceso de confirmación de borrado cuando existen referencias
					//Dada la manera en que se procesa ahora, podría haber regresado algún status de error, pero mínimo restablecerá la ventana
					else {
						if (bRemoveProgress) {
							objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOff();
						}
					}
					//JAPR
				}
				
				<?	//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)?>
				/* Forza a un refresh de la ventana de valores apuntando al número de página indicado */
				function doChangeValuesPage() {
					intCurrentValuesPage = parseInt(intCurrentValuesPage);
					if (!$.isNumeric(intCurrentValuesPage)) {
						intCurrentValuesPage = 1;
					}
					
					var dhxList = "b";
					strURL =  "<?='main.php?'.$this->get_QueryString()?>" + doGeneratePagingParams();
					objFormsLayoutDataSourceTable.progressOn();
					parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
				}
				
				/* Basado en los valores actuales de los parámetros de paginación genera la porción de la URL con sus valores actuales */
				function doGeneratePagingParams() {
					return "&CurrentValuesPage=" + intCurrentValuesPage + "&NumValuesPerPage=" + intNumValuesPerPage + "&NumValuesPerPageOld=" + intNumValuesPerPageOld;
				}
			</script>
			
			<body onload="doOnLoad();" onunload="doOnUnload();" style="width: 100%; height: 100%;">
				<form method="POST" action="uploadUserImage.php?objectType=<?=otyImagePackage?>&strId=ZIP&tabName=ZIP&strRowId=ZIP" enctype="multipart/form-data" style="display:inline" target="frameUploadFile" onsubmit="objFormsLayoutDataSourceTable.cells(dhxDataSource).progressOn();return true;">
					<input type="file" id="fileZIP" name="fileZIP" value="" class="noVisFile" onchange="document.getElementById('btnsubmitZIP').click();">
					<!--<button type="button" class="noSpcBut" onclick="document.getElementById('fileZIP').click();"><?=translate("...")?></button>-->
					<input type="submit" id ="btnsubmitZIP" name="btnsubmitZIP" value="<?=translate("Upload file")?>" style="display: none; visibility: hidden;">
				</form>
				<iframe id="frameUploadFile" name="frameUploadFile" style="display:none; position:absolute; left:100, top:150">
				</iframe>
			</body>
		</html>
<?	
		die();
	}

	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstance($aRepository)
	{
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return eval('return new '.$strCalledClass.'($aRepository);');
		//return new BITAMDataSource($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aDataSourceID) {
		$anInstance = null;
		$strCalledClass = static::class;
		
		if (((int)  $aDataSourceID) < 0)
		{
			return $anInstance;
		}
		
		$anInstance =& BITAMGlobalFormsInstance::GetDataSourceInstanceWithID($aDataSourceID);
		if (!is_null($anInstance)) {
			return $anInstance;
		}
		
		//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
		$strAdditionalFields = "";
		if (getAppVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', VersionNum ';
		}
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', eBavelAppID, eBavelFormID';
		}
		//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
		if (getMDVersion() >= esvRedesign) {
			$strAdditionalFields .= ', eBavelFormType';
		}
		//@JRPP 2015-09-03: Nuevo campo para guardar el nombre del documento
		if (getMDVersion() >= esvDataSourceDocument) {
			$strAdditionalFields .= ', Document';
		}
		//@JAPR 2015-09-06: Agregados los parámetros de recurrencia al DataSource basado en el cambio implementado por JCEM para SurveyAgendaScheduler
		if (getMDVersion() >= esvDataSourceAgent) {
			$strAdditionalFields .= ', RecurPatternType, RecurPatternOpt, RecurPatternNum, RecurRangeOpt, id_ServiceConnection, LastSyncDate, Path, SheetName';
		}
		if (getMDVersion() >= esvDataSourceAgentRecurActive) {
			$strAdditionalFields .= ', RecurActive';
		}
		if (getMDVersion() >= esvDataSourceIdGdrive) {
			$strAdditionalFields .= ', IdGoogleDrive';
		}
		if (getMDVersion() >= esvDataSourceAgentHours) {
			$strAdditionalFields .= ', capturestarttime, `interval`';
		}		
		//@JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
		if (getMDVersion() >= esvDataSourceErrors) {
			$strAdditionalFields .= ', LastSyncErrorDate, LastSyncError';
		}
		//@JAPR
		if (getMDVersion() >= esvDataDestination) {
			//@JAPR 2015-12-22: Agregado el tipo de DataSource basado en Tabla
			$strAdditionalFields .= ', parameters, ServiceDBName, ServiceTableName';
		}
		//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
		if (getMDVersion() >= esvDataSourceDataVersion) {
			$strAdditionalFields .= ', DataVersionDate';
		}
		//@JAPR
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		if (getMDVersion() >= esvQuestionAR) {
			$strAdditionalFields .= ', IsModel';
		}
		
		$sql = "SELECT DataSourceID, DataSourceName, TableName, Type $strAdditionalFields 
			FROM SI_SV_DataSource 
			WHERE DataSourceID = ".((int) $aDataSourceID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstanceFromRS($aRepository, $aRS) {
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		$inteBavelAppID = 0;
		$inteBavelFormID = 0;
        $eBavelFormType = ebftForms;
		if (getMDVersion() >= esveBavelCatalogs) {
			$inteBavelAppID = (int) @$aRS->fields["ebavelappid"];
			$inteBavelFormID = (int) @$aRS->fields["ebavelformid"];
		}
		
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository);
		$anInstance->DataSourceID = (int) @$aRS->fields["datasourceid"];
		$anInstance->DataSourceName= GetValidJScriptText($aRS->fields["datasourcename"], array("fullhtmlspecialchars" => 1));
		$anInstance->TableName= GetValidJScriptText($aRS->fields["tablename"], array("fullhtmlspecialchars" => 1));
		$anInstance->Type= $aRS->fields["type"];
		//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
		$anInstance->VersionNum = (int) @$aRS->fields["versionnum"];
		//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
		$anInstance->DataVersionDate = (string) @$aRS->fields["dataversiondate"];
		if ( $anInstance->DataVersionDate ) {
			$anInstance->DataVersionNum = (int) implode(privateUnformatDateTime($anInstance->DataVersionDate, ""), "");
		}
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		$anInstance->eBavelAppID = (int) @$aRS->fields["ebavelappid"];
		$anInstance->eBavelFormID = (int) @$aRS->fields["ebavelformid"];
		//@JAPR
		//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
		if (getMDVersion() >= esvRedesign) {
			$anInstance->eBavelFormType = (int) @$aRS->fields["ebavelformtype"];
		}
		//@JRPP 2015-09-03: Nuevo campo para guardar el nombre del documento
		if (getMDVersion() >= esvDataSourceDocument) {
			$anInstance->Document = (string) @$aRS->fields["document"];
		}
		//@JAPR 2015-09-06: Agregados los parámetros de recurrencia al DataSource basado en el cambio implementado por JCEM para SurveyAgendaScheduler
		$intRecurPatternType = @$aRS->fields["recurpatterntype"];
		if (!is_null($intRecurPatternType)) {
			$anInstance->RecurPatternType = (int) @$aRS->fields["recurpatterntype"];
		}
        $anInstance->RecurPatternOpt = (string) @$aRS->fields["recurpatternopt"];
        $anInstance->RecurPatternNum = (int) @$aRS->fields["recurpatternnum"];
        $anInstance->RecurRangeOpt = (string) @$aRS->fields["recurrangeopt"];
		$anInstance->ServiceConnectionID = (int) @$aRS->fields["id_serviceconnection"];
		$anInstance->Path = (string) @$aRS->fields["path"];
		$anInstance->SheetName = (string) @$aRS->fields["sheetname"];
		$anInstance->LastSyncDate = (string) @$aRS->fields["lastsyncdate"];
		$anInstance->RecurActive = (int) @$aRS->fields["recuractive"];
		$anInstance->IdGoogleDrive = (string) @$aRS->fields["idgoogledrive"];
		//@JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
		$anInstance->LastSyncErrorDate = (string) @$aRS->fields["lastsyncerrordate"];
		$anInstance->LastSyncError = (string) @$aRS->fields["lastsyncerror"];
		//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
		$anInstance->LastModDateID = (string) @$aRS->fields["lastmoddateid"];
		$anInstance->LastModUserID = (int) @$aRS->fields["lastmoduserid"];
		//@JAPR

		$strCaptureStartTime = trim((string) @$aRS->fields["capturestarttime"]);
		$anInstance->parameters = (string) @$aRS->fields["parameters"];
		//@JAPR 2015-12-22: Agregado el tipo de DataSource basado en Tabla
		$anInstance->SourceDatabase = (string) @$aRS->fields["servicedbname"];
		$anInstance->SourceTable = (string) @$aRS->fields["servicetablename"];
		//@JAPR
		$anInstance->CaptureStartTime_Hours = 0;
		$anInstance->CaptureStartTime_Minutes = 0;
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		if (getMDVersion() >= esvQuestionAR) {
			$anInstance->IsModel = (int) @$aRS->fields["ismodel"];
		}
		if ($strCaptureStartTime != '')
		{
			$arrHour = explode(':', $strCaptureStartTime);
			if (count($arrHour) == 2)
			{
				if (is_numeric($arrHour[0]))
				{
					$anInstance->CaptureStartTime[0] = $arrHour[0];
					$anInstance->CaptureStartTime_Hours =(int) $arrHour[0];
				}
				if (is_numeric($arrHour[1]))
				{
					$anInstance->CaptureStartTime[1] = $arrHour[1];
					$anInstance->CaptureStartTime_Minutes =(int) $arrHour[1];
				}
			}
		}

		$anInstance->Interval = (int) @$aRS->fields["interval"];
		
		BITAMGlobalFormsInstance::AddDataSourceInstanceWithID($anInstance->DataSourceID, $anInstance);
		//OMMC 2015/09/28: Obtener el nombre de la sección (forma) si el catálogo es eBavel
       	if ($anInstance->eBavelFormID > 0) {
			$sql = "SELECT id, sectionName FROM si_forms_sections WHERE id_section = " . $anInstance->eBavelFormID;
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
			    $anInstance->eBavelSectionName = @$aRS->fields["sectionname"];
				//@JAPR 2016-10-10: Corregido un bug, se había usado el nombre lógico en lugar del nombre físico para obtener la ruta de imagenes de eBavel (#P04H52)
				$anInstance->eBavelFormName = @$aRS->fields["id"];
			}
			
			//@JAPR 2016-09-14: Corregido un bug, a partir de eBavel 6.0, las imágenes asociadas a los campos de sus formas ya no se almacenan físicamente en el mismo lugar que en
			//las versiones anteriores, por lo que se debe utilizar este proceso para obtener la ruta físcia real y así poder obtener los datos de fecha y hora (#P04H52)
			//Ahora obtiene el codeName de la Aplicación de eBavel para no tener que cargarlo en otros puntos
			$sql = 'SELECT CONCAT("SELECT MAX(modifiedDate) AS moddate FROM SI_FORMS_", T2.CODENAME,"_",T1.id) AS TABLA, T2.CODENAME AS CODENAME FROM SI_FORMS_SECTIONS T1 INNER JOIN SI_FORMS_APPS T2 ON T2.id_app = T1.appid WHERE T1.ID_SECTION = ' . $anInstance->eBavelFormID;
			//@JAPR
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				//@JAPR 2016-09-14: Corregido un bug, a partir de eBavel 6.0, las imágenes asociadas a los campos de sus formas ya no se almacenan físicamente en el mismo lugar que en
				//las versiones anteriores, por lo que se debe utilizar este proceso para obtener la ruta físcia real y así poder obtener los datos de fecha y hora (#P04H52)
				$anInstance->eBavelAppCodeName = (string) @$aRS->fields["codename"];
				//@JAPR
			    $sql = @$aRS->fields["TABLA"];
			    $aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
				    $anInstance->eBavelLastModDate = @$aRS->fields["moddate"];
				}
			}


		}
		//@JAPR
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		if (array_key_exists("DataSourceID", $aHTTPRequest->POST))
		{
			$aDataSourceID = $aHTTPRequest->POST["DataSourceID"];
			if (is_array($aDataSourceID))
			{
				
				$aCollection = BITAMDataSourceCollection::NewInstance($aRepository, $aDataSourceID);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
				$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, $aDataSourceID);
				if (is_null($anInstance))
				{
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}

				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						//@JRPP 2015-06-27: Rediseñado el Admin con DHTMLX
						$anInstance = $strCalledClass::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("DataSourceID", $aHTTPRequest->GET))
		{
			$aDataSourceID = $aHTTPRequest->GET["DataSourceID"];
			//@JRPP 2015-06-27: Rediseñado el Admin con DHTMLX
			if (array_key_exists("dataChanges", $aHTTPRequest->POST))
			{
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aDataSourceID);
				if (is_null($anInstance))
				{
					die(json_encode(array('error' => sprintf(translate("Unknown Data Source: %d"), (int) $aDataSourceID ))));
					return null;
				}
				$dataChangesArray = json_decode($aHTTPRequest->POST["dataChanges"], true);
				if (!isset($dataChangesArray['insertedData']) && !isset($dataChangesArray['updatedData']) && !isset($dataChangesArray['deletedData'])) {
					die(json_encode(array('error' => sprintf(translate("Invalid Request: %s"), $aHTTPRequest->POST["dataChanges"] ))));
					return null;
				}
				$resultData = null;
				if (isset($dataChangesArray['deletedData']) && count($dataChangesArray['deletedData']) > 0) {
					$anArrayOfDeletes = $dataChangesArray['deletedData'];
					foreach ($anArrayOfDeletes as $anID) {
						$sql = $anInstance->get_DeleteMembers($anID);
						if ($anInstance->Repository->DataADOConnection->Execute($sql) === false)
						{
							die(json_encode(array('error' => "(".__METHOD__.") ".translate("Error accessing")." ".$anInstance->TableName." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql, 'data' => $anInstance->get_AllData())));
							return null;
						}
					}
				}
				//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
				$blnHasImageAttrib = false;
				//@JAPR
				if (isset($dataChangesArray['updatedData']) && count($dataChangesArray['updatedData']) > 0) {
					$anArrayOfUpdates = $dataChangesArray['updatedData'];
					//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
					foreach ($anArrayOfUpdates as $anID => $anArrayOfValues) {
						$sql = $anInstance->get_UpdateMembers($anID, $anArrayOfValues, $blnHasImageAttrib);
						if (!is_null($sql)) { 
							if ($anInstance->Repository->DataADOConnection->Execute($sql) === false)
							{
								die(json_encode(array('error' => "(".__METHOD__.") ".translate("Error accessing")." ".$anInstance->TableName." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql, 'data' => $anInstance->get_AllData())));
								return null;
							}
						}
					}
					//@JAPR
				}
				if (isset($dataChangesArray['insertedData']) && count($dataChangesArray['insertedData']) > 0) {
					$anArrayOfInserts = $dataChangesArray['insertedData'];
					foreach ($anArrayOfInserts as $anArrayOfValues) {
						$sql = $anInstance->get_InsertMembers($anArrayOfValues);
						if (!is_null($sql)) { 
							if ($anInstance->Repository->DataADOConnection->Execute($sql) === false)
							{
								die(json_encode(array('error' => "(".__METHOD__.") ".translate("Error accessing")." ".$anInstance->TableName." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql, 'data' => $anInstance->get_AllData())));
								return null;
							}
						}
					}
					$resultData = $anInstance->get_AllData();
					//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
					$blnHasImageAttrib = false;
					//@JAPR
				}
				
				//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
				if ($blnHasImageAttrib) {
					//Si al llegar a este punto está asignada esta variable, quiere decir que se actualizó un atributo imagen y que no se insertaron nuevos valores, así que forzará
					//a regresar la colección completa de valores porque debido al atributo imagen, pudo existir una actualización del valor recibido para ajustarlo a la ruta donde
					//se hizo el upload de la imagen
					$resultData = $anInstance->get_AllData();
				}
				//@JAPR
				
				//@JAPR 2015-08-02: Agregada la actualización de versión de las definiciones
				BITAMDataSource::updateLastUserAndDateInDataSource($aRepository, (int)$aDataSourceID);
				//@JAPR
				die(json_encode(array('data' => $resultData)));
				return null;
			}
			else
			{
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aDataSourceID);
				//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)
				if (!is_null($anInstance)) {
					//En este caso hay que sobreescribir la instancia con las propiedades del esquema de paginación si es que se encuentra habilitado, heredando la configuración actual
					//de la pantalla que puede ser cambiada por el usuario
					if ($anInstance->EnablePagination) {
						$anInstance->NumValuesPerPageOld = getParamValue('NumValuesPerPageOld', 'both', '(int)', true);
						if (is_null($anInstance->NumValuesPerPageOld) || $anInstance->NumValuesPerPageOld < 0) {
							$anInstance->NumValuesPerPageOld = DEFAULT_DATASOURCE_VALUESPERPAGE;
						}
						$anInstance->NumValuesPerPage = getParamValue('NumValuesPerPage', 'both', '(int)', true);
						if (is_null($anInstance->NumValuesPerPage) || $anInstance->NumValuesPerPage < 0) {
							$anInstance->NumValuesPerPage = DEFAULT_DATASOURCE_VALUESPERPAGE;
						}
						$anInstance->CurrentValuesPage = getParamValue('CurrentValuesPage', 'both', '(int)');
						if ($anInstance->CurrentValuesPage <= 0) {
							$anInstance->CurrentValuesPage = 1;
						}
						
						//Obtiene el número total de páginas de valores entre las que se podrán mover dentro de la ventana. Este es el único punto que puede asignar este valor,
						//ya que es el único punto que generará la ventana de valores del DataSource
						$anInstance->TotalValuesPagesNum = $anInstance->getAttributeValuesPageCount($anInstance->NumValuesPerPage);
						
						//Ajusta el número de página actual basado en la nueva cantidad de rows por página y el nuevo número de paginas totales calculadas
						if ($anInstance->CurrentValuesPage > 1 && $anInstance->NumValuesPerPage != $anInstance->NumValuesPerPageOld) {
							//$intTotalValuesPagesNumOld = $anInstance->TotalValuesPagesNum * $anInstance->NumValuesPerPage / $anInstance->NumValuesPerPageOld;
							$intTotalValuesPagesNumOld = $anInstance->getAttributeValuesPageCount($anInstance->NumValuesPerPageOld);
							//Aplicando proporcionalidad, se obtiene el nuevo número actual de página respecto al nuevo total de páginas después de cambiar la cantidad de registros por cada página
							$intNewCurrentValuesPage = $anInstance->TotalValuesPagesNum * $anInstance->CurrentValuesPage / $intTotalValuesPagesNumOld;
							//die("<br>\r\n anInstance->TotalValuesPagesNum == {$anInstance->TotalValuesPagesNum}, anInstance->NumValuesPerPage == {$anInstance->NumValuesPerPage}, anInstance->NumValuesPerPageOld == {$anInstance->NumValuesPerPageOld}, anInstance->CurrentValuesPage == {$anInstance->CurrentValuesPage}, intTotalValuesPagesNumOld == {$intTotalValuesPagesNumOld}, intNewCurrentValuesPage == {$intNewCurrentValuesPage}");
							$anInstance->CurrentValuesPage = (int) ceil($intNewCurrentValuesPage);
							//La mínima página es 1
							if ($anInstance->CurrentValuesPage < 0) {
								$anInstance->CurrentValuesPage = 1;
							}
						}
					}
				}
				//@JAPR
				else {
					//@JRPP 2015-06-27: Rediseñado el Admin con DHTMLX
					$anInstance = $strCalledClass::NewInstance($aRepository);
					if (!is_null($anInstance)) {
						$anInstance->EnablePagination = false;
					}
				}
			}
		}
		else
		{
			//@JRPP 2015-06-27: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstance($aRepository);
		}
		return $anInstance;
	}
	
	//@JAPR 2015-08-02: Agregadas las variables de catálogo al editor de fórmulas
	/* Dado un DataSource específico, reemplaza todas las variables de usuario de sus DataSourceMembers por las variables internas con las que se debe almacenar en la metadata
	*/
	static function ReplaceMemberNamesByIDs($aRepository, $aDataSourceID, $sText) {
		if ($aDataSourceID <= 0) {
			return $sText;
		}
		
		$strFilterText = $sText;
		
		$attributes = BITAMDataSourceMemberCollection::NewInstance($aRepository, $aDataSourceID);
		if (is_null($attributes)) {
			return $sText;
		}
		
		$numFields = count($attributes->Collection);
		$arrUsedVarNames = array();
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		for($i=0; $i<$numFields; $i++) {
			$objCatMember = $attributes->Collection[$i];
			$strCatMemberVarName = $objCatMember->getVariableName();
			if (isset($arrUsedVarNames[$strCatMemberVarName])) {
				$strCatMemberVarName .= $objCatMember->MemberOrder;
			}
			$arrUsedVarNames[$strCatMemberVarName] = $strCatMemberVarName;
			$arrUsedVarNamesByPos[$i] = $strCatMemberVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$i] = $objCatMember->getAttribIDVariableName();
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		//En este caso el ordenamiento no es por el array de variables sino por el de nombres de atributos, ya que se ordena por quien se busca
		//para que el reemplazo no deje caracteres adicionales que alteren la expresión grabada
		natcasesort($arrUsedVarNamesByPos);
		$arrUsedVarNamesByPos = array_reverse($arrUsedVarNamesByPos, true);
		foreach ($arrUsedVarNamesByPos as $intMemberOrder => $strCatMemberVarName)	{
			$strMemberVarName = (string) @$arrVariableNames[$intMemberOrder];
			$strFilterText = str_ireplace($strCatMemberVarName, $strMemberVarName, $strFilterText);
		}
	    //@JAPR
		
		return $strFilterText;
	}
	
	/* Dado un DataSource específico, reemplaza todas las variables internas de sus DataSourceMembers por las variables de usuario con las que se debe mostrar en la configuración
	*/
	static function ReplaceMemberIDsByNames($aRepository, $aDataSourceID, $sText) {
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Validado para salir si no hay patrones a reemplazar
		if ($aDataSourceID <= 0 || stripos($sText, '@DMID') === false) {
			return $sText;
		}
		//@JAPR
		
		$strFilterText = $sText;
		
		$attributes = BITAMDataSourceMemberCollection::NewInstance($aRepository, $aDataSourceID);
		if (is_null($attributes)) {
			return $sText;
		}
		
		$numFields = count($attributes->Collection);
		$arrUsedVarNames = array();
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		for($i=0; $i<$numFields; $i++) {
			$objCatMember = $attributes->Collection[$i];
			$strCatMemberVarName = $objCatMember->getVariableName();
			if (isset($arrUsedVarNames[$strCatMemberVarName])) {
				$strCatMemberVarName .= $objCatMember->MemberOrder;
			}
			$arrUsedVarNames[$strCatMemberVarName] = $strCatMemberVarName;
			$arrUsedVarNamesByPos[$i] = $strCatMemberVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$i] = $objCatMember->getAttribIDVariableName();
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		//En este caso el ordenamiento no es por el array de variables sino por el de nombres de atributos, ya que se ordena por quien se busca
		//para que el reemplazo no deje caracteres adicionales que alteren la expresión grabada
		natcasesort($arrUsedVarNamesByPos);
		$arrUsedVarNamesByPos = array_reverse($arrUsedVarNamesByPos, true);
		foreach ($arrUsedVarNamesByPos as $intMemberOrder => $strCatMemberVarName)	{
			$strMemberVarName = (string) @$arrVariableNames[$intMemberOrder];
			//echo("<br>\r\n Testing replace: this '{$strMemberVarName}' for this '{$strCatMemberVarName}'");
			$strFilterText = str_ireplace($strMemberVarName, $strCatMemberVarName, $strFilterText);
		}
	    //@JAPR
		
		//die("<br>\r\n Reemplazando variable $aDataSourceID == {$strFilterText}");
		return $strFilterText;
	}
	//@JAPR

	/* Dado un DataSource específico, reemplaza todas las variables internas de sus DataSourceMembers por los nombres de campos con los que se debe ejecutar en un filtro Where para
	extraer valores
	//@JAPR 2015-09-30: Corregido un bug, esta función no consideraba DataSources de tipo eBavel, además su función está repetida en TranslateAttributesVarsToFieldNames
	//por lo que se optará por esta última y se descontinuará esta función
	//@JAPRDescontinuada: Se debe utilizar TranslateAttributesVarsToFieldNames en lugar de esta
	*/
	static function ReplaceMemberIDsByFields($aRepository, $aDataSourceID, $sText) {
		if ($aDataSourceID <= 0) {
			return $sText;
		}
		
		$strFilterText = $sText;
		
		$attributes = BITAMDataSourceMemberCollection::NewInstance($aRepository, $aDataSourceID);
		if (is_null($attributes)) {
			return $sText;
		}
		
		$numFields = count($attributes->Collection);
		$arrUsedVarNames = array();
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		for($i=0; $i<$numFields; $i++) {
			$objCatMember = $attributes->Collection[$i];
			$strCatMemberVarName = $objCatMember->FieldName;
			$arrUsedVarNames[$strCatMemberVarName] = $strCatMemberVarName;
			$arrUsedVarNamesByPos[$i] = $strCatMemberVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$i] = "{".$objCatMember->getAttribIDVariableName()."}";
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		//En este caso el ordenamiento no es por el array de variables sino por el de nombres de atributos, ya que se ordena por quien se busca
		//para que el reemplazo no deje caracteres adicionales que alteren la expresión grabada
		natcasesort($arrUsedVarNamesByPos);
		$arrUsedVarNamesByPos = array_reverse($arrUsedVarNamesByPos, true);
		foreach ($arrUsedVarNamesByPos as $intMemberOrder => $strCatMemberVarName)	{
			$strMemberVarName = (string) @$arrVariableNames[$intMemberOrder];
			$strFilterText = str_ireplace($strMemberVarName, $strCatMemberVarName, $strFilterText);
		}
	    //@JAPR
		
		return $strFilterText;
	}
	//@JAPR
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("DataSourceID", $anArray))
		{
			$this->DataSourceID = (int) $anArray["DataSourceID"];
		}
		
	 	if (array_key_exists("DataSourceName", $anArray))
		{
			$this->DataSourceName = rtrim($anArray["DataSourceName"]);
		}
		
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
	 	if (array_key_exists("eBavelAppID", $anArray))
		{
			$this->eBavelAppID = (int) rtrim($anArray["eBavelAppID"]);
		}
		
	 	if (array_key_exists("eBavelFormID", $anArray))
		{
			$this->eBavelFormID = (int) rtrim($anArray["eBavelFormID"]);
		}
		//@JAPR

		//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
		if (array_key_exists("eBavelFormType", $anArray))
		{
			$this->eBavelFormType = (int) rtrim($anArray["eBavelFormType"]);
		}

		/** Indica si la recurrencia esta activa o no */
		if (array_key_exists("RecurActive", $anArray))
		{
			$this->RecurActive = (int) rtrim($anArray["RecurActive"]);
		}

		if (array_key_exists("CaptureStartTime_Hours", $anArray))
		{
			$this->CaptureStartTime[0] = (int)$anArray["CaptureStartTime_Hours"];
			$this->CaptureStartTime_Hours = $this->CaptureStartTime[0];
		}
		
		if (array_key_exists("CaptureStartTime_Minutes", $anArray))
		{
			$this->CaptureStartTime[1] = (int)$anArray["CaptureStartTime_Minutes"];
			$this->CaptureStartTime_Minutes = $this->CaptureStartTime[1];
		}

		if (array_key_exists("Interval", $anArray))
		{
			$this->Interval = (int) $anArray["Interval"];
		}
		
		//@JAPR 2015-09-06: Agregados los parámetros de recurrencia al DataSource basado en el cambio implementado por JCEM para SurveyAgendaScheduler
		if (array_key_exists("RecurPatternType", $anArray))
		{
			$this->RecurPatternType = (int)$anArray["RecurPatternType"];
	        switch ($this->RecurPatternType) {
	            case frecDay:	//diario
	                $this->RecurPatternOpt = isset($anArray['DailyOpt'])?$anArray['DailyOpt']:"";
	                $this->RecurPatternNum = isset($anArray['DNumDays'])?$anArray['DNumDays']:0;
	                break;
	            case frecWeek:		//semanal
	                $tmp = (isset($anArray['WDaysSu'])?$anArray['WDaysSu']."|":"").(isset($anArray['WDaysM'])?$anArray['WDaysM']."|":"").(isset($anArray['WDaysTu'])?$anArray['WDaysTu']."|":"").(isset($anArray['WDaysW'])?$anArray['WDaysW']."|":"").(isset($anArray['WDaysTh'])?$anArray['WDaysTh']."|":"").(isset($anArray['WDaysF'])?$anArray['WDaysF']."|":"").(isset($anArray['WDaysSa'])?$anArray['WDaysSa']."|":"");
	                $this->RecurPatternNum = isset($anArray['WNumWeeks'])?$anArray['WNumWeeks']:0;
	                $this->RecurPatternOpt=substr($tmp, 0, strlen($tmp)-1);
	                break;
	            //@AAL 14/04/2015: Opción agregada para crear agendas por semanas del mes
	            case frecWeekOfMonth:		//Semanas del Mes
	                $this->RecurPatternNum = 1;
	                $The  = isset($anArray['The']) ? $anArray['The'] : "";
	                $FreqOrdinal = isset($anArray['The_FreqOrdinal']) ? $anArray['The_FreqOrdinal'] : "";
	                $DaysWeek = isset($anArray['The_DaysWeek']) ? $anArray['The_DaysWeek'] : "";
	                $this->RecurPatternOpt = $The . "/" . $FreqOrdinal . "|" .  $DaysWeek . "|1";
	                break;
	            //@AAL
	            case frecMonth:		//mensual
	                $this->RecurPatternNum = isset($anArray['M_Day_NumMonth'])?$anArray['M_Day_NumMonth']:( $anArray['M_The_DaysWeek'] ? $anArray['M_The_DaysWeek'] : 0 );
	                switch(isset($anArray['MonthlyOpt'])?$anArray['MonthlyOpt']:""){
	                    case 0://day
	                        $this->RecurPatternOpt = (isset($anArray['MonthlyOpt'])?$anArray['MonthlyOpt']:"")."/".(isset($anArray['tDaysOfMonthSel'])?$anArray['tDaysOfMonthSel']:"");
	                        break;
	                    case 1://the
	                        $this->RecurPatternOpt = (isset($anArray['MonthlyOpt'])?$anArray['MonthlyOpt']:"")."/".(isset($anArray['M_The_FreqOrdinal'])?$anArray['M_The_FreqOrdinal']:"")."|".(isset($anArray['M_The_DaysWeek'])?$anArray['M_The_DaysWeek']:"")."|".(isset($anArray['M_The_NumMonth'])?$anArray['M_The_NumMonth']:"");
	                        break;
	                    default:
	                        break;
	                }
	                break;
	            case frecYear:		//anual
	                $this->RecurPatternNum = isset($anArray['YNumYears'])?$anArray['YNumYears']:0;
	                switch (isset($anArray['YearlyOpt'])?$anArray['YearlyOpt']:"" ){
	                    case "0"://on
	                        $this->RecurPatternOpt = (isset($anArray['YearlyOpt'])?$anArray['YearlyOpt']:"")."/".(isset($anArray['Y_On_NumDay'])?$anArray['Y_On_NumDay']:"")."|".(isset($anArray['Y_On_Months'])?$anArray['Y_On_Months']:"");
	                        break;
	                    case "1"://onthe
	                        $this->RecurPatternOpt = (isset($anArray['YearlyOpt'])?$anArray['YearlyOpt']:"")."/".(isset($anArray['Y_OnThe_FreqOrdinal'])?$anArray['Y_OnThe_FreqOrdinal']:"")."|".(isset($anArray['Y_OnThe_Days'])?$anArray['Y_OnThe_Days']:"")."|".(isset($anArray['Y_OnThe_Months'])?$anArray['Y_OnThe_Months']:(isset($anArray['Y_On_Months'])?$anArray['Y_On_Months']:""));
	                        break;
	                }
	                break;
	            default:		//?
	                break;
	        }

	        $this->RecurRangeOpt = (isset($anArray['RangeRecurrenceStart'])?$anArray['RangeRecurrenceStart']:"")."|".(isset($anArray['RangeRecurOpt'])?$anArray['RangeRecurOpt']:"")."|";
	        
	        switch(isset($anArray['RangeRecurOpt'])?$anArray['RangeRecurOpt']:"")
	        {
	            //0: sin fin, no se utiliza opciones
	            case "1"://numero ocurrencias
	                $this->RecurRangeOpt .= (isset($anArray['R_NumOcurrences'])?$anArray['R_NumOcurrences']:"");
	                break;
	            case "2"://fecha fin
	                $this->RecurRangeOpt .= (isset($anArray['RangeRecurrenceEnd'])?$anArray['RangeRecurrenceEnd']:"");
	                break;
	        }
		}
		//@JAPR
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		if (getMDVersion() >= esvQuestionAR) {
			if (array_key_exists("IsModel", $anArray))
			{
				$this->IsModel = (int) $anArray["IsModel"];
			}
		}
		
		return $this;
	}
	
	//@JAPR 2018-05-07: Optimizada la carga de eForms (#G87C4W)
	/* Agregado el parámetro $bUpdateVersions cuyo default será true para compatibilidad hacia atrás, pero se forzará a false durante el proceso de actualización de la
	fecha de última modificación de los datos para no forzar a cambiar la versión del catálogo en si sino sólo la versión de los datos cada vez que el agente
	realiza dicho proceso */
	function save($bUpdateVersions = true) {
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
		$currentDate = date("Y-m-d H:i:s");
		$this->LastModDateID = $currentDate;
		$this->LastModUserID = $_SESSION["PABITAM_UserID"];
		//@JAPR
		
	 	if ($this->isNewObject()) {
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(DataSourceID)", "0")." + 1 AS DataSourceID".
						" FROM SI_SV_DataSource";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$this->DataSourceID = (int) $aRS->fields["datasourceid"];
			
			//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
			$strAdditionalFields = "";
			$strAdditionalValues = "";
			//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
			if (!is_null($this->LastModUserID) && (int) $this->LastModUserID > 0) {
				$strAdditionalFields .= ', LastModUserID';
				$strAdditionalValues .= ', '.(int) $this->LastModUserID;
			}
			if (!is_null($this->LastModDateID) && trim((string) $this->LastModDateID) != '') {
				$strAdditionalFields .= ', LastModDate';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->DBTimeStamp($this->LastModDateID);
			}
			//@JAPR
			if (getAppVersion() >= esvMultiDynamicAndStandarization) {
				$strAdditionalFields .= ', VersionNum';
				$strAdditionalValues .= ', 1';
			}
			//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
			if (getMDVersion() >= esveBavelCatalogs) {
				$strAdditionalFields .= ', eBavelAppID, eBavelFormID';
				$strAdditionalValues .= ", {$this->eBavelAppID}, {$this->eBavelFormID}";
			}
			//@JAPR
			//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
			if (getMDVersion() >= esvRedesign) {
				$strAdditionalFields .= ', eBavelFormType';
				$strAdditionalValues .= ", {$this->eBavelFormType}";
			}
			//@JRPP 2015-09-03: Nuevo campo para guardar el nombre del documento
			if (getMDVersion() >= esvDataSourceDocument) {
				$strAdditionalFields .= ', Document';
				$strAdditionalValues .= ", ". $this->Repository->DataADOConnection->Quote((string) $this->Document);
			}
			//@JAPR 2015-09-06: Agregados los parámetros de recurrencia al DataSource basado en el cambio implementado por JCEM para SurveyAgendaScheduler
			if (getMDVersion() >= esvDataSourceAgent) {
				$strAdditionalFields .= ', RecurPatternType, RecurPatternOpt, RecurPatternNum, RecurRangeOpt, id_ServiceConnection, Path, SheetName';
				$strAdditionalValues .= ", {$this->RecurPatternType}, ".
					$this->Repository->DataADOConnection->Quote((string) $this->RecurPatternOpt).", {$this->RecurPatternNum}, ".
					$this->Repository->DataADOConnection->Quote((string) $this->RecurRangeOpt).", {$this->ServiceConnectionID}, ".
					$this->Repository->DataADOConnection->Quote((string) $this->Path).", ".
					$this->Repository->DataADOConnection->Quote((string) $this->SheetName);
			}
			
			/** Indica si la recurrencia esta activa o no */
			if (getMDVersion() >= esvDataSourceAgentRecurActive) {
				$strAdditionalFields .= ', RecurActive';
				$strAdditionalValues .= ", {$this->RecurActive} ";
			}
			
			/** Hora de inicio de la captura */
			if (getMDVersion() >= esvDataSourceAgentHours) {
				$strAdditionalFields .= ', capturestarttime, `interval`';
				$strAdditionalValues .= ", ". $this->Repository->DataADOConnection->Quote( $this->getCaptureStartTime() ) .", " . $this->Interval . ' ';
			}
			
			if (getMDVersion() >= esvDataSourceIdGdrive) {
				$strAdditionalFields .= ', IdGoogleDrive';
				$strAdditionalValues .= ", {$this->IdGoogleDrive} ";
			}	
			//@JAPR 2015-12-23: Agregado el tipo de DataSource basado en Tabla
			if (getMDVersion() >= esvDataDestination) {
				$strAdditionalFields .= ', parameters, ServiceDBName, ServiceTableName';
				$strAdditionalValues .= ", ".$this->Repository->DataADOConnection->Quote($this->parameters).
					", ".$this->Repository->DataADOConnection->Quote($this->SourceDatabase).
					", ".$this->Repository->DataADOConnection->Quote($this->SourceTable);
			}
			//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
			if (getMDVersion() >= esvDataSourceDataVersion) {
				$strAdditionalFields .= ', DataVersionDate';
				$strAdditionalValues .= ", ".((!$this->DataVersionDate)?"NULL":$this->Repository->DataADOConnection->DBTimeStamp($this->DataVersionDate));
			}	
			//@JAPR
			//OMMC 2019-03-25: Pregunta AR #4HNJD9
			if (getMDVersion() >= esvQuestionAR) {
				$strAdditionalFields .= ', IsModel';
				$strAdditionalValues .= ", {$this->IsModel}";
			}
			
			$sql = "INSERT INTO SI_SV_DataSource (".
						" DataSourceID".
			            ", DataSourceName".
			            ", Type".
			            ", TableName".
			            $strAdditionalFields.
			            ") VALUES ("
			            .$this->DataSourceID.
						",".$this->Repository->DataADOConnection->Quote($this->DataSourceName).
						",".$this->Type.
						",".$this->Repository->DataADOConnection->Quote($this->TableName).
						$strAdditionalValues.
						")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		else {
			$strAdditionalValues = "";
			//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
			$strAdditionalValues .= ', LastModUserID = '.$_SESSION["PABITAM_UserID"].
				', LastModDate = '.$this->Repository->DataADOConnection->DBTimeStamp($currentDate);
			//@JAPR
			if (getAppVersion() >= esvMultiDynamicAndStandarization) {
				$strAdditionalValues .= ', VersionNum = '.$this->VersionNum;
			}
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
			if (getMDVersion() >= esveBavelCatalogs) {
				$strAdditionalValues .= ', eBavelAppID = '.$this->eBavelAppID.', eBavelFormID = '.$this->eBavelFormID;
			}
			//@JAPR
			//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
			if (getMDVersion() >= esvRedesign) {
				$strAdditionalValues .= ', eBavelFormType = ' . $this->eBavelFormType;
			}
			//@AAL
			//@JRPP 2015-09-03: Nuevo campo para guardar el nombre del documento
			if (getMDVersion() >= esvDataSourceDocument) {
				$strAdditionalValues .= ', Document = ' . $this->Repository->DataADOConnection->Quote((string) $this->Document);
			}
			//@JAPR 2015-09-06: Agregados los parámetros de recurrencia al DataSource basado en el cambio implementado por JCEM para SurveyAgendaScheduler
			if (getMDVersion() >= esvDataSourceAgent) {
				$strAdditionalValues .= ", RecurPatternType = " . $this->RecurPatternType.
					", RecurPatternOpt = ".$this->Repository->DataADOConnection->Quote($this->RecurPatternOpt).
                    ", RecurPatternNum = ".$this->RecurPatternNum.
                    ", RecurRangeOpt = ".$this->Repository->DataADOConnection->Quote($this->RecurRangeOpt).
                    ", id_ServiceConnection = ".$this->ServiceConnectionID.
                    ", Path = ".$this->Repository->DataADOConnection->Quote($this->Path).
					", SheetName = ".$this->Repository->DataADOConnection->Quote($this->SheetName);
			}

			/** Se agrego la propiedad para indicar si la recurrencia esta activa o no */
			if (getMDVersion() >= esvDataSourceAgentRecurActive) {
				$strAdditionalValues .= ", RecurActive = " . $this->RecurActive ;
			}

			/** Hora de inicio de la captura */
			if (getMDVersion() >= esvDataSourceAgentHours) {
				$strAdditionalValues .= ", capturestarttime = " . $this->Repository->DataADOConnection->Quote( $this->getCaptureStartTime() ) . ', `interval` = ' . $this->Interval ;
			}

			if (getMDVersion() >= esvDataSourceIdGdrive) {
				$strAdditionalValues .= ", IdGoogleDrive = ".$this->Repository->DataADOConnection->Quote($this->IdGoogleDrive);
			}
			
			//@JAPR 2015-12-23: Agregado el tipo de DataSource basado en Tabla
			if (getMDVersion() >= esvDataDestination) {
				$strAdditionalValues .= ", parameters = ".$this->Repository->DataADOConnection->Quote($this->parameters).
					", ServiceDBName = ".$this->Repository->DataADOConnection->Quote($this->SourceDatabase).
					", ServiceTableName = ".$this->Repository->DataADOConnection->Quote($this->SourceTable);
			}
			//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
			if (getMDVersion() >= esvDataSourceDataVersion) {
				$strAdditionalValues .= ", DataVersionDate = ".((!$this->DataVersionDate)?"NULL":$this->Repository->DataADOConnection->DBTimeStamp($this->DataVersionDate));
			}
			//@JAPR
			//OMMC 2019-03-25: Pregunta AR #4HNJD9
			if (getMDVersion() >= esvQuestionAR) {
				$strAdditionalValues .= ", IsModel = " . $this->IsModel;
			}
			
			$sql = "UPDATE SI_SV_DataSource SET ".
						" DataSourceName = ".$this->Repository->DataADOConnection->Quote($this->DataSourceName).
						$strAdditionalValues.
						" WHERE DataSourceID = ".$this->DataSourceID;

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//Realizamos actualizacion de LastDate en las encuestas relacionadas con este catalogo
			//@JAPR 2015-07-21: Se remueve de este punto porque al final se invocará a la nueva función para actualizar las dependencias
			//BITAMSurvey::updateAllSurveyLastUserAndDateFromDataSource($this->Repository, $this->DataSourceID);
		}
		
		//Invoca a la actualización de definiciones en cualquier punto donde se requiera, aunque sólo cambia los nombres y versiones
		//@JAPR 2018-05-07: Optimizada la carga de eForms (#G87C4W)
		$this->updateAllCatalogDefinitions(true, false, false, $bUpdateVersions);
		//@JAPR
		
		return $this;
	}

	//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
	/* Crea/actualiza el catálogo indicado utilizando la definición de este objeto ya sea por ID o como instancia, si no se especifica uno entonces crea uno nuevo
	Regresa la instancia del catálogo creado/actualizado
	*/
	function createUpdateCatalog($aCatalogID = 0, $oCatalog = null) {
		$objCatalog = null;
		if ($aCatalogID <= 0 && is_null($oCatalog)) {
			//En este caso crea un nuevo catálogo
			$objCatalog = BITAMCatalog::NewInstance($this->Repository);
			if (is_null($objCatalog)) {
				return $objCatalog;
			}
		}
		else {
			if (!is_null($oCatalog)) {
				$objCatalog = $oCatalog;
			}
			else {
				$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $aCatalogID);
				if (is_null($objCatalog)) {
					return $objCatalog;
				}
			}
		}
		
		//Actualiza la definición del catálogo
		$objCatalog->DataSourceID = $this->DataSourceID;
		$objCatalog->CatalogName = $this->DataSourceName;
		$objCatalog->TableName = $this->TableName;
		//@JAPR 2015-12-07: Corregido un bug, al crear el Catálogo a partir de un DataSource, no estaba asignando el mapeo hacia eBavel (#ASIPRG)
		$objCatalog->eBavelAppID = $this->eBavelAppID;
		$objCatalog->eBavelFormID = $this->eBavelFormID;
		$objCatalog->eBavelFormType = $this->eBavelFormType;
		//@JAPR
		if (!$objCatalog->save()) {
			return $null;
		}
		
		return $objCatalog;
	}
	
	/* Actualiza la instancia de catálogo por cada pregunta/sección donde se encuentre definido el DataSource, además de actualizar las tablas de datos que por alguna razón estuvieran 
	inconsistentes, en caso de que se detecte alguna combinación de secciones/preguntas que deban enlazarse, unifica los catálogos generados para ellas de tal manera que lo reutilicen
	tal como se hacía en las versiones anteriores
	Este método es como un failsafe en caso de que la metadata quede inconsistente, al grabar el data source nuevamente (o al crear algún DataSourceMember) se actualizarían todas sus 
	referencias en catálogos, preguntas y secciones así como las tablas correspondientes
	Los parámetros indican el nivel de actualización deseado:
	$bRename = Actualiza los nombres de los catálogos y atributos
	$bSyncCatalogs = Actualiza la definición de metadata de los catálogos mapeados
	$bAlterStruct = Agrega las columnas faltantes a las tablas donde se utiliza el catálogo
	//@JAPR 2018-05-07: Optimizada la carga de eForms (#G87C4W)
	Agregado el parámetro $bUpdateVersions cuyo default será true para compatibilidad hacia atrás, pero se forzará a false durante el proceso de actualización de la
	fecha de última modificación de los datos para no forzar a cambiar la versión del catálogo en si sino sólo la versión de los datos cada vez que el agente
	realiza dicho proceso
	*/
	function updateAllCatalogDefinitions($bRename = true, $bSyncCatalogs = true, $bAlterStruct = true, $bUpdateVersions = true) {
		//Obtiene la definición de DataSourceMembers a actualizar
		$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->DataSourceID);
		if (is_null($objDataSourceMembersColl)) {
			return;
		}
		$arrDataSourceMembersByIDs = array();
		foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
			$arrDataSourceMembersByIDs[$objDataSourceMember->MemberID] = $objDataSourceMember;
		}
		
		//Actualiza el nombre del catálogo y los atributos en todas las tablas donde se tenga referencia a ellos (el orden no se actualiza porque ese no depende del DataSource)
		if ($bRename) {
			//@JAPR 2015-12-07: Corregido un bug, al crear el Catálogo a partir de un DataSource, no estaba asignando el mapeo hacia eBavel (#ASIPRG)
			//@JAPR 2015-12-15: Corregido un bug
			$sql = "UPDATE SI_SV_Catalog SET 
					CatalogName = ".$this->Repository->DataADOConnection->Quote($this->DataSourceName). " 
					, TableName = ".$this->Repository->DataADOConnection->Quote($this->TableName)." 
					, eBavelAppID = {$this->eBavelAppID} 
					, eBavelFormID = {$this->eBavelFormID} 
					, eBavelFormType = {$this->eBavelFormType} 
				WHERE DataSourceID = {$this->DataSourceID}";
			$this->Repository->DataADOConnection->Execute($sql);
			
			foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
				//@JAPR 2015-11-05: Corregido un bug, al crear el Catalog a partir del DataSource, no estaba heredando la propiedad de si era o no atributo imagen (#SS2EKF)
				//@JAPR 2015-12-07: Corregido un bug, al crear el Catálogo a partir de un DataSource, no estaba asignando el mapeo hacia eBavel (#ASIPRG)
				$sql = "UPDATE SI_SV_CatalogMember SET 
						MemberName = ".$this->Repository->DataADOConnection->Quote($objDataSourceMember->MemberName). " 
						, KeyOfAgenda = {$objDataSourceMember->KeyOfAgenda} 
						, AgendaLatitude = {$objDataSourceMember->AgendaLatitude} 
						, AgendaLongitude = {$objDataSourceMember->AgendaLongitude} 
						, AgendaDisplay = {$objDataSourceMember->AgendaDisplay} 
						, IsImage = {$objDataSourceMember->IsImage} 
						, eBavelFieldID = {$objDataSourceMember->eBavelFieldID} 
						, eBavelFieldDataID = {$objDataSourceMember->eBavelFieldDataID} 
					WHERE DataSourceMemberID = {$objDataSourceMember->MemberID}";
				$this->Repository->DataADOConnection->Execute($sql);
			}
		}
		
		//Sincroniza los catálogos agregando atributos que pudieran no existir, para esto carga todos los catálogos que apuntan a este DataSource así como sus respectivos atributos,
		//para posteriormente agregar los que necesite si no estaban (siempre los agrega al final) y remover aquellos que ya no existan (al remover automáticamente se reordenan, por lo
		//tanto primero elimina y luego agrega)
		if ($bSyncCatalogs) {
			$arrCatalogIDs = array();
			$sql = "SELECT DISTINCT A.CatalogID 
				FROM SI_SV_Catalog A 
				WHERE A.DataSourceID = {$this->DataSourceID}";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS) {
				while(!$aRS->EOF) {
					$intCatalogID = (int) @$aRS->fields["catalogid"];
					$arrCatalogIDs[] = $intCatalogID;
					$aRS->MoveNext();
				}
				
				foreach ($arrCatalogIDs as $intCatalogID) {
					$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $intCatalogID);
					if (is_null($objCatalog) || $objCatalog->CreationAdminVersion < esveFormsv6) {
						continue;
					}
					
					$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $intCatalogID);
					if (is_null($objCatalogMembersColl)) {
						continue;
					}
					
					$arrMappedCatMembersIDs = array();
					//Primero elimina los atributos que ya no se encuentren mapeados a un DataSourceMember
					foreach ($objCatalogMembersColl as $objCatalogMember) {
						$intDataSourceMemberID = $objCatalogMember->DataSourceMemberID;
						if ($intDataSourceMemberID <= 0 || !isset($arrDataSourceMembersByIDs[$intDataSourceMemberID])) {
							//Este es un atributo inválido, así que lo elimina
							$objCatalogMember->remove();
						}
						else {
							//Este es un atributo que ya estaba mapeado y si existe en el DataSource, así que lo conservará
							$arrMappedCatMembersIDs[$intDataSourceMemberID] = $objCatalogMember->MemberID;
						}
					}
					
					//Ahora agrega aquellos atributos que no se encontraran en el catálogo, lo hace al final porque para este momento luego de haber eliminado algún atributo, todos
					//habrían sido reordenados automáticamente, así que se puede simplemente considerar el último orden según la cantidad de atributos mapeados válidos, y agregar el nuevo
					//en dicha posición. El orden empieza en 1, así que le incrementa una unidad al total de atributos
					$intOrder = count($arrMappedCatMembersIDs) +1;
					foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
						//Sólo agrega el atributo si no se encontraba mapeado previamente
						$intDataSourceMemberID = $objDataSourceMember->MemberID;
						if (isset($arrMappedCatMembersIDs[$intDataSourceMemberID])) {
							continue;
						}
						
						$objCatalogMember = $objDataSourceMember->createUpdateCatalogMember($intCatalogID, 0, null, $intOrder);
						if (is_null($objCatalogMember)) {
							continue;
						}
						
						//En este punto el atributo se creó correctamente
						$intOrder++;
					}
				}
			}
		}
		
		//Agrega las columnas de atributos faltantes a las tablas de preguntas/secciones existentes que utilicen este DataSource. Nunca elimina las columnas para permitir edición
		//de capturas previas o que los posibles cubos mapeados a ellas sigan funcionando
		if ($bAlterStruct) {
			//Crea una instancia vacía de sección y pregunta sólo para permitir obtener los nombres de tablas a modificará (realmente los IDs son irrelevantes en este punto)
			$objSection = BITAMSection::NewInstance($this->Repository, 0);
			$objQuestion = BITAMQuestion::NewInstance($this->Repository, 0);
			if (!is_null($objSection) && !is_null($objQuestion)) {
				$objSection->CreationAdminVersion = ESURVEY_SERVICE_VERSION;
				$objQuestion->CreationAdminVersion = ESURVEY_SERVICE_VERSION;
				$sql = "SELECT A.SurveyID, A.QuestionID AS ObjectID, ".otyQuestion." AS ObjectType, A.SectionID AS ParentID 
					FROM SI_SV_Question A 
					WHERE A.DataSourceID = {$this->DataSourceID} AND A.CreationVersion >= ".esveFormsv6." AND A.ValuesSourceType = ".tofCatalog." 
					UNION 
					SELECT A.SurveyID, A.SectionID AS ObjectID, ".otySection." AS ObjectType, A.SurveyID AS ParentID 
					FROM SI_SV_Section A 
					WHERE A.DataSourceID = {$this->DataSourceID} AND A.CreationVersion >= ".esveFormsv6." AND A.ValuesSourceType = ".tofCatalog." 
					ORDER BY 1, 3, 2";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS) {
					while (!$aRS->EOF) {
						$intSurveyID = (int) @$aRS->fields["surveyid"];
						$intParentID = (int) @$aRS->fields["parentid"];
						$intObjectType = (int) @$aRS->fields["objecttype"];
						$intObjectID = (int) @$aRS->fields["objectid"];
						$strTableName = '';
						if ($intSurveyID > 0 && $intParentID > 0 && $intObjectID > 0) {
							switch ($intObjectType) {
								case otySection:
									$objSection->SurveyID = $intSurveyID;
									$objSection->SectionID = $intObjectID;
									$objSection->setTableAndFieldsProperties();
									//@JAPR 2015-08-29: Corregido un bug, estaba modificando la tabla de sección equivocada, se usa SectionMultTable porque así no
									//requiere determinar si la sección es o no Inline, y si en algún momento lo fue y aún tiene la referencia al DataSource, al menos
									//mantendría actualizada la tabla con los últimos atributos
									$strTableName = $objSection->SectionMultTable;
									//@JAPR
									break;
								
								case otyQuestion:
									$objQuestion->SurveyID = $intSurveyID;
									$objQuestion->SectionID = $intParentID;
									$objQuestion->QuestionID = $intObjectID;
									$objQuestion->setTableAndFieldsProperties();
									$strTableName = $objQuestion->QuestionDetTable;
									break;
							}
						}
						
						if ($strTableName != ''); {
							//Agrega todos los atributos a esta tabla
							$this->regenerateMemberColumns($strTableName);
						}
						
						$aRS->MoveNext();
					}
				}
			}
		}
		
		//Al final del proceso, se invocara el nivel que se invocara, debe actualizar las versiones de formas que se requieran si utilizan a este DataSource
		if ( $bUpdateVersions && ( $bRename || $bSyncCatalogs || $bAlterStruct ) ) {
			BITAMDataSource::updateLastUserAndDateInDataSource($this->Repository, $this->DataSourceID);
		}
	}
	
	/* Dado un nombre de una tabla (originalmente utilizado para las tablas de preguntas/secciones ligadas a catálogos), agrega tantas columnas como DataSourceMembers se encuentren
	definidos en este DataSource
	*/
	function regenerateMemberColumns($sTableName) {
		$sTableName = trim($sTableName);
		if ($sTableName == '') {
			return;
		}
		
		//Agrega la columna de apoyo para almacenar la estructura de los atributos utilizados en la captura
		//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
		$sql = "ALTER TABLE {$sTableName} ADD AttributeDesc LONGTEXT NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->DataSourceID);
		foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
			$intDataSourceMemberID = $objDataSourceMember->MemberID;
			$strFieldName = BITAMQuestion::$DescFieldCatMember.$intDataSourceMemberID;
			$sql = "ALTER TABLE {$sTableName} ADD {$strFieldName} VARCHAR(255) NULL";
			$this->Repository->DataADOConnection->Execute($sql);
		}
	}
	
	/* Elimina el DataSource de la metadata, así como todas las instancias de catálogos generados a partir de él
	*/
	function remove() {
		$arrCatalogIDs = array();
		$sql = "SELECT CatalogID 
			FROM SI_SV_Catalog 
			WHERE DataSourceID = {$this->DataSourceID}";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while(!$aRS->EOF) {
				$intCatalogID = (int) @$aRS->fields["catalogid"];
				$arrCatalogIDs[] = $intCatalogID;
				$aRS->MoveNext();
			}
			
			foreach ($arrCatalogIDs as $intCatalogID) {
				$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $intCatalogID);
				if (is_null($objCatalog) || $objCatalog->CreationAdminVersion < esveFormsv6) {
					continue;
				}
				
				$objCatalog->remove();
			}
		}
		
		//@JAPRWarning: No implementado el borrado de tablas adicionales
		//@JAPR 2013-02-13: Corregido un bug, no estaba eliminando información relacionada al catálogo cuando este era eliminado
		$sql = "DELETE FROM SI_SV_DataSourceFilterUser WHERE FilterID IN (
			SELECT FilterID FROM SI_SV_DataSourceFilter WHERE DataSourceID = {$this->DataSourceID})";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_DataSourceFilterRol WHERE FilterID IN (
			SELECT FilterID FROM SI_SV_DataSourceFilter WHERE DataSourceID = {$this->DataSourceID})";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_DataSourceFilter WHERE DataSourceID = ".$this->DataSourceID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2015-07-26: Agregada la eliminación de los catálogos internos de las preguntas cuando se elimina la pregunta o el DataSource
		//Al eliminar un DataSource se deben actualizar todas las preguntas que lo utilizan, pueden existir múltiples preguntas y para este momento ya se
		//eliminaron los catálogos así que las preguntas ya no apuntan internamente a él, sin embargo aún se encuentran apuntando al DataSource
		$sql = "UPDATE SI_SV_Section SET 
				DataSourceID = 0 
				, DataSourceMemberID = 0 
			WHERE DataSourceID = {$this->DataSourceID}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql = "DELETE FROM SI_SV_QuestionMembers 
				WHERE MemberID IN (
					SELECT MemberID 
					FROM SI_SV_DataSourceMember 
					WHERE DataSourceID = {$this->DataSourceID})";
		$this->Repository->DataADOConnection->Execute($sql);
		
		//@JAPR 2016-04-29: Corregido un bug, no estaba eliminando el campo del atributo imagen
		$strAdditionalFields = '';
		if (getMDVersion() >= esvSimpleChoiceCatOpts) {
			//@JAPR 2016-12-20: Corregido un bug, estaba reemplazando en lugar de concatenar los campos adicionales (#DBNNGH)
			$strAdditionalFields .= ', DataMemberImageID = 0';
		}
		//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		if (getMDVersion() >= esvSChoiceMetro) {
			//@JAPR 2016-12-20: Corregido un bug, estaba reemplazando en lugar de concatenar los campos adicionales (#DBNNGH)
			$strAdditionalFields .= ', DataMemberOrderID = 0';
		}
		$sql = "UPDATE SI_SV_Question SET 
				DataSourceID = 0 
				, DataSourceMemberID = 0 
				, DataMemberLatitudeID = 0 
				, DataMemberLongitudeID = 0 
				{$strAdditionalFields} 
			WHERE DataSourceID = {$this->DataSourceID}";
		//@JAPR
		$this->Repository->DataADOConnection->Execute($sql);
		//@JAPR
		
		$sql = "DELETE FROM SI_SV_DataSourceMember WHERE DataSourceID = ".$this->DataSourceID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		/*2015-08-13@JRPP Cuando se elimina un datasource tambien elimianmos la tabla de valores correspondiente*/
		$sql = "DROP TABLE IF EXISTS `" . $this->TableName . "`;";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." " . $this->TableName . " ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$sql = "DELETE FROM SI_SV_DataSource WHERE DataSourceID = ".$this->DataSourceID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->DataSourceID < 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Catalog");
		}
		else
		{
			return $this->DataSourceName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=DataSource";
		}
		else
		{
			return "BITAM_PAGE=DataSource&DataSourceID=".$this->DataSourceID;
		}
	}

	function get_Parent()
	{
		return BITAMDataSourceCollection::NewInstance($this->Repository,null);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function generateBeforeFormCode($aUser)
 	{	
?>
 	<script language="JavaScript">
 		function verifyFieldsAndSave(target)
 		{
 			var strAlert = '';
 			
 			if(Trim(BITAMCatalog_SaveForm.DataSourceName.value)=='')
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Name"))?>';
 			}
 			var MDVersion = <?=getMDVersion()?>;
 			var esvRedesign = <?=esvRedesign?>;
 			/*@AAL 19/03/2015: Si esta dentro de la versión Correcta se debe validar */
 			if (MDVersion >= esvRedesign) {
 				/*@AAL 19/03/2015: Se Valida que si se ha seleccionado una Aplicación eBavel, 
	            también debe seleccionarse ya sea una Forma o Vista y algún elementeo de ésta.*/
	            if(BITAMCatalog_SaveForm.eBavelAppID.value != "0" &&  BITAMCatalog_SaveForm.eBavelFormID.value == "0")
	            {
	                alert("Must select a eBavel data type and some eBavel data element");
	                return false;
	            }
 			}
            //@AAL

 			//En esta parte se verifica si el AttributeName ha sido modificado por valores validos
			var attributeNameOld = '<?=$this->DataSourceName?>';
			var DataSourceID = <?=$this->DataSourceID?>;
			var attributeName = BITAMCatalog_SaveForm.DataSourceName.value;

			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('<?=translate('XML Object not supported')?>');
				return(new Array);
			}

			xmlObj.open('POST', 'verifyAttributeName.php', false);
			xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
	 		xmlObj.send('ObjectID='+DataSourceID+'&AttributeName='+encodeURIComponent(attributeName)+'&AttributeNameOld='+encodeURIComponent(attributeNameOld));

	 		if(xmlObj.status == 404)
			{
				alert('<?=translate('Unable to launch validation process')?>');
				return false;
			}

	 		strResponseData = Trim(unescape(xmlObj.responseText));

	 		arrResponseData = strResponseData.split('<SVSEP>');
	 		if(arrResponseData.length == 0)
	 		{
	 			alert('<?=translate('Error found during data validation')?>');
				return false;
	 		}

	 		if(arrResponseData[0]!="OK")
	 		{
				strErrMsg = arrResponseData[0];
				if(arrResponseData.length > 1)
				{
					strErrMsg = arrResponseData[1];
				}
				
  				strAlert += '\n- ' + '<?=translate('Error found during data validation')?>: ' + strErrMsg;
	 		}

 			if(Trim(strAlert)!='')
 			{
 				alert(strAlert);
 			}
 			else
 			{
 				BITAMCatalog_Ok(target);
 			}
 		} 

        /*@AAL 19/03/2015: Esta función actualiza la lista que mostrará ya sea los elementos de las Formas o de las vistas*/
        function UpdateCatalogeBavel()
        {
            var ObjComb = BITAMCatalog_SaveForm.eBavelFormID;
            //Si no se ha seleccionado ninguna aplicación entonces hacemos todo a NONE
            if(BITAMCatalog_SaveForm.eBavelAppID.value == "0" || ObjComb.options.length == 0){
                BITAMCatalog_SaveForm.eBavelFormType.selectedIndex = 0;
                BITAMCatalog_SaveForm.eBavelFormID.selectedIndex = 0;
                ObjComb.options.length = 0;
                ObjComb.options.add(new Option("(<?=translate('None')?>)", "0"));
            }
            else{
                //Si hemos selecionado una aplicación y la lista de las formas o vistas contienen elementos 
                if(BITAMCatalog_SaveForm.eBavelFormIDTemp.options.length > 0 || BITAMCatalog_SaveForm.eBavelViewIDTemp.options.length > 0){
                    var eBavelFormID = null;
                    //Estamos solicitando los elementos de las formas.
                    if(BITAMCatalog_SaveForm.eBavelFormType.value == "<?=ebftForms?>")
                    	eBavelFormID = BITAMCatalog_SaveForm.eBavelFormIDTemp.cloneNode(true);
                    else  //Si hemos selecconado la opción 1 estamos indicando que queremos los elementos de las vistas y clonamos el Cbbx oculto
                        eBavelFormID = BITAMCatalog_SaveForm.eBavelViewIDTemp.cloneNode(true);
                    //Como no se mostraban visibles entonces las mostramos.
                    ObjComb.parentNode.replaceChild(eBavelFormID, ObjComb);
                    eBavelFormID.style.display = "inline";
                    eBavelFormID.setAttribute("name",  "eBavelFormID");
                    ObjComb = null;
                }
                else{//Si se llega a este caso es por que no hay elementos ya sea en las vistas o formas y clonamos el Cbbx oculto
                    ObjComb.options.length = 0;
                    ObjComb.options.add(new Option("(<?=translate('None')?>)", "0"));
                }
            }
        }
        //@AAL

		function setParentValuesIDs()
		{
			frmSetCatalogValues.submit();
		}
 	</script>
<?
?>	 
  <iframe name="frameUploadCatalogsFile" style="display:none"></iframe>
  <iframe name="frameAppendCatalogsFile" style="display:none"></iframe>
<?

	}

	function generateAfterFormCode($aUser)
	{
		global $gbIsGeoControl;
?>
	<script language="JavaScript">
		
		objBody= document.getElementsByTagName("body");
		objBody[0].onkeypress= function onc(event)
		{
			var event = (window.event) ? window.event : event;
			//if (window.event.keyCode ==13)
			if (event.keyCode ==13)
			return false ;
		}
		objOkSelfButton = document.getElementById("BITAMCatalog_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMCatalog_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMCatalog_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>

	</script>

<?
	}
	
	function get_FormIDFieldName()
	{
		return 'DataSourceID';
	}
	
	function get_FormFieldName() {
		return 'DataSourceName';
	}

	function get_FormFields($aUser)
	{
		global $gbIsGeoControl;
		
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataSourceName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			//Obtiene la lista de aplicaciones de eBavel creadas en el repositorio
			$arreBavelApps = array(0 => '('.translate('None').')');
			require_once('eBavelIntegration.inc.php');
			$arreBavelApps = GetEBavelCollectionByField(@GetEBavelApplications($this->Repository), 'applicationName', 'key', true, '', 0);
			
			$aFieldApps = BITAMFormField::NewFormField();
			$aFieldApps->Name = "eBavelAppID";
			$aFieldApps->Title = translate("eBavel application");
			$aFieldApps->Type = "Object";
			$aFieldApps->VisualComponent = "Combobox";
			$aFieldApps->Options = $arreBavelApps;
			if (getMDVersion() >= esvRedesign) $aFieldApps->OnChange = "UpdateCatalogeBavel();";
			$myFields[$aFieldApps->Name] = $aFieldApps;
			
			//Obtiene la lista de formas de eBavel creadas en el repositorio
			$arreBavelForms = array('' => array('' => '('.translate('None').')'));
			$arreBavelForms = GetEBavelCollectionByField(@GetEBavelSections($this->Repository), 'sectionName', 'key', true, 'appId', 0);

			/*@AAL 19/03/2015: Modificado para soportar vistas de eBavel*/
			if (getMDVersion() >= esvRedesign) {
				//Obtiene la Vistas de eBavel asociadas a una aplicación creada en el repositorio
				$arreBavelViews = array('' => array('' => '('.translate('None').')'));
				$arreBavelViews = GetEBavelCollectionByField(@GetEBavelViews($this->Repository), 'label', 'key', true, 'appId', 0);
				
				/*@AAL 19/03/2015: Se agraga un componentes Cbbx para elegir entre una Forma o una vista*/
	            $aFieldeBavelFormType = BITAMFormField::NewFormField();
	            $aFieldeBavelFormType->Name = "eBavelFormType";
	            $aFieldeBavelFormType->Title = translate("eBavel Data Type");
	            $aFieldeBavelFormType->Type = "Object";
	            $aFieldeBavelFormType->VisualComponent = "Combobox";
	            $aFieldeBavelFormType->Options = array(ebftForms => "Forms", ebftViews => "Views");
	            $aFieldeBavelFormType->OnChange = "UpdateCatalogeBavel();";
	            $myFields[$aFieldeBavelFormType->Name] = $aFieldeBavelFormType;
	            //@AAL
	                                    
	            /*@AAL 19/03/2015: Modifiacado para mostrar la lista de elementos dependiendo si se selecciono una Forma o una Vista*/
				$aFieldForms = BITAMFormField::NewFormField();
				$aFieldForms->Name = "eBavelFormID";
				$aFieldForms->Title = translate("eBavel data");
				$aFieldForms->Type = "Object";
				$aFieldForms->VisualComponent = "Combobox";
	            if (!$this->isNewObject()) {
	            	//Si se trata de una Forma de eBavel
	                if ($this ->eBavelFormType == ebftForms) {
	                    $aFieldForms->Options = $arreBavelForms[$this->eBavelAppID];
	                    if (isset($arreBavelForms[$this->eBavelAppID][$this->eBavelFormID])) 
	                        $aFieldForms->Options[$this->eBavelFormID] = $arreBavelForms[$this->eBavelAppID][$this->eBavelFormID];
	                }
	                else { //Se trata de una Vista de eBavel
	                    $aFieldForms->Options = $arreBavelViews[$this->eBavelAppID];
	                    if (isset($arreBavelViews[$this->eBavelAppID][$this->eBavelFormID])) 
	                        $aFieldForms->Options[$this->eBavelFormID] = $arreBavelViews[$this->eBavelAppID][$this->eBavelFormID];
	                }
	            }
	            else
	                $aFieldForms->Options = array(0 => '(' . translate('None') . ')');
				$myFields[$aFieldForms->Name] = $aFieldForms;
	            
	            /*@AAL 019/03/2015: Se agraga un componentes Cbbx oculto para almacenar la lista de elementos de una Forma*/            
	            $aFieldeBavelFormIDTemp = BITAMFormField::NewFormField();
	            $aFieldeBavelFormIDTemp->Name = "eBavelFormIDTemp";
	            //$aFieldeBavelFormIDTemp->Title = "Hidden eBavel Form Id";
	            $aFieldeBavelFormIDTemp->Type = "Object";
	            $aFieldeBavelFormIDTemp->VisualComponent = "Combobox";
	            $aFieldeBavelFormIDTemp->Options = $arreBavelForms;
	            $aFieldeBavelFormIDTemp->IsVisible = false;
	            $myFields[$aFieldeBavelFormIDTemp->Name] = $aFieldeBavelFormIDTemp;
	            $aFieldeBavelFormIDTemp->Parent = $aFieldApps;
	            $aFieldApps->Children[] = $aFieldeBavelFormIDTemp;
	            
	            /*@AAL 19/03/2015: Se agraga un componentes Cbbx oculto para almacenar la lista de elementos de una Vista*/
	            $aFieldeBavelViewIDTemp = BITAMFormField::NewFormField();
	            $aFieldeBavelViewIDTemp->Name = "eBavelViewIDTemp";
	            //$aFieldeBavelViewIDTemp->Title = "Hidden eBavel View Id";
	            $aFieldeBavelViewIDTemp->Type = "Object";
	            $aFieldeBavelViewIDTemp->VisualComponent = "Combobox";
	            $aFieldeBavelViewIDTemp->Options = $arreBavelViews;
	            $aFieldeBavelViewIDTemp->IsVisible = false;
	            $myFields[$aFieldeBavelViewIDTemp->Name] = $aFieldeBavelViewIDTemp;
	            $aFieldeBavelViewIDTemp->Parent = $aFieldApps;
	            $aFieldApps->Children[] = $aFieldeBavelViewIDTemp;
	            //@AAL
				
			} else {
				$aFieldForms = BITAMFormField::NewFormField();
				$aFieldForms->Name = "eBavelFormID";
				$aFieldForms->Title = translate("eBavel data form");
				$aFieldForms->Type = "Object";
				$aFieldForms->VisualComponent = "Combobox";
				$aFieldForms->Options = $arreBavelForms;
				$myFields[$aFieldForms->Name] = $aFieldForms;
				$aFieldForms->Parent = $aFieldApps;
				$aFieldApps->Children[] = $aFieldForms;
				//@JAPR
			}
		}
				
		return $myFields;
	}
	
	function canEdit($aUser)
	{
		return true;
	}
	
	//@JAPR 2011-06-23: Integrada la captura de valores para el catálogo
 	function addButtons($aUser)
	{
		if (!$this->isNewObject())
		{
	?>
		<!--<a href="#" class="alinkescfav" onclick="setParentValuesIDs();"><img src="images/catalogEntry.gif" displayMe=1> <?=translate("Catalog values")?> </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
		<button id="btnCatalogValues" class="alinkescfav" onclick="setParentValuesIDs();"><img src="images/catalogEntry.gif" alt="<?=translate('Catalog values')?>" title="<?=translate('Catalog values')?>" displayMe="1" /> <?=translate("Catalog values")?></button>
	<?	}
	//@JAPR 2012-05-03: Corregido un bug, las opciones de importación y templates sólo deben estar visibles durante la edición
	if (!$this->isNewObject()) //($_SESSION["PA_ADMIN"] == 1)
		{
?>
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="downloadTemplate();"><img src="images/download_xls_templates.gif"> <?=translate("Download catalogs template")."   "?>  </span>-->
	<?
		}
		/*
		if (strlen($this->logfile) > 0)
		{
?>
		<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="showLogFile('<?=$this->logfile?>');"><img src="images/showMsg.gif"> <?=translate("View last upload log")."   "?>  </span>
<?		  
		}*/
	}
	//@JAPR
	
	function canRemove($aUser)
	{
		//@JAPR 2015-10-27: Removido todo el código que se había validado para que permita la validación detallada en el server en lugar de validar en la lista de DataSources
		return true;
	}

	function get_Children($aUser)
	{
		$myChildren = array();

		if (!$this->isNewObject())
		{
			$myChildren[] = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->DataSourceID);
			$myChildren[] = BITAMCatalogFilterCollection::NewInstance($this->Repository, $this->DataSourceID);
		}
		
		return $myChildren;
	}
	
	//Obtiene la definición en JSon de este objeto y sus dependencias (no hay referencias a catálogos u otros objetos globales, sólo son aquellos objetos que dependen
	//directamente de este
	//@JAPR 2014-06-12: Validado para que sólo regrese valores del catálogo si hay por lo menos una encuesta que no tenga filtro dinámico para él
	function getJSonDefinition($bGetValues = true) {
		$arrDef = array();
		
		$arrDef['id'] = $this->DataSourceID;
		$arrDef['name'] = $this->DataSourceName;
		//@JAPR 2014-08-26: Validado que por el momento los catálogos mapeados desde eBavel se descarguen siempre como modificados, ya que pueden
		//contener cambios que eForms no tiene forma de identificar
		$intVersionNum = $this->VersionNum;
		//@JAPR 2016-01-06: Corregido un bug, los DataSource tipo Tabla también deben regresar constantemente los datos sin mantener versión local (#012P4U)
		if (($this->eBavelAppID > 0 && $this->eBavelFormID > 0) || $this->Type == dstyTable) {
			$intVersionNum = -1;
		}
		$arrDef['versionNum'] = $intVersionNum;
		//@JAPR 2018-05-07: Optimizada la carga de eForms (#G87C4W)
		$arrDef['dataVersionNum'] = $this->DataVersionNum;
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if ($this->eBavelAppID && $this->eBavelFormID) {
			$arrDef['eBavelFormID'] = $this->eBavelFormID;
		}
		//@JAPR 2015-09-06: Agregados los parámetros de recurrencia al DataSource basado en el cambio implementado por JCEM para SurveyAgendaScheduler
		$arrDef['RecurPatternType'] = $this->RecurPatternType;
		$arrDef['RecurPatternOpt'] = $this->RecurPatternOpt;
		$arrDef['RecurPatternNum'] = $this->RecurPatternNum;
		$arrDef['RecurRangeOpt'] = $this->RecurRangeOpt;
		$arrDef['serviceConnectionID'] = $this->ServiceConnectionID;
		$arrDef['path'] = $this->Path;
		$arrDef['sheetName'] = $this->SheetName;
		$arrDef['lastSyncDate'] = $this->LastSyncDate;
		$arrDef['RecurActive'] = $this->RecurActive;
		$arrDef['IdGoogleDrive'] = $this->IdGoogleDrive;
		$arrDef['Interval'] = $this->Interval;
		$arrDef['CaptureStartTime_Hours'] = ( $this->CaptureStartTime_Hours < 10 ? '0' . $this->CaptureStartTime_Hours : $this->CaptureStartTime_Hours );
		$arrDef['CaptureStartTime_Minutes'] = ( $this->CaptureStartTime_Minutes < 10 ? '0' . $this->CaptureStartTime_Minutes : $this->CaptureStartTime_Minutes );
		//@JAPR
		
		$intMaxOrder = 0;
		//Obtiene la posición máxima del atributo usado ya sea en sección (mas uno) o como pregunta, ya que se asume que el catálogo será usado
		//sólo hasta esos niveles por lo que cualquier nivel posterior sería un desperdicio de memoria y tiempo de transferencia
		$sql = "SELECT A.CatalogID, MAX(B.MemberOrder) +1 AS MemberOrder
			FROM SI_SV_Section A, SI_SV_DataSourceMember B
			WHERE A.CatalogID = B.DataSourceID AND A.CatMemberID = B.MemberID AND A.CatalogID = ".$this->DataSourceID." AND A.CatMemberID > 0
			GROUP BY A.CatalogID
			
			UNION
			
			SELECT A.CatalogID, MAX(B.MemberOrder) AS MemberOrder
			FROM SI_SV_Question A, SI_SV_DataSourceMember B
			WHERE A.CatalogID = B.DataSourceID AND A.CatMemberID = B.MemberID AND A.CatalogID = ".$this->DataSourceID." AND A.CatMemberID > 0
			GROUP BY A.CatalogID";
	    $aRS = $this->Repository->DataADOConnection->Execute($sql);
	    if ($aRS && !$aRS->EOF) {
		    while (!$aRS->EOF) {
		        $intNewMaxOrder = (int) $aRS->fields["memberorder"];
		        if ($intNewMaxOrder > $intMaxOrder) {
		        	$intMaxOrder = $intNewMaxOrder;
		        }
		        $aRS->MoveNext();
		    }
	    }
		//@JRPP 2015-06-27: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
	    //@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
	    $intMaxOrder = 0;
		//@JAPR 2015-08-29: Rediseñadas las agendas en v6
		//Agregado el parámetro $iCheckPointID para especificar si se cargarán o no por un CheckPoint específico, se forza un -1 para que el join no encuentre
		//match con los checkpoint y así en caso de que existieran varios CheckPoint hacia este DataSource no duplicaría los atributos
		$arrDef['attributes'] = $strCalledClass::GetCatalogAttributes($this->Repository, $this->DataSourceID, $intMaxOrder, null, -1);
		//@JAPR
		$arrDef['attributesOrder'] = array();
		foreach ($arrDef['attributes'] as $aMemberID) {
			$arrDef['attributesOrder'][] = (int) $aMemberID['id'];
		}
		//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
		//$arrDef['attributeValues'] = BITAMDataSource::GetCatalogAttributeValues($this->Repository, $this, array_values($arrDef['attributes']));
		//@JAPR 2014-06-12: Validado para que sólo regrese valores del catálogo si hay por lo menos una encuesta que no tenga filtro dinámico para él
		if ($bGetValues) {
			//@JAPR 2015-12-22: Se removió el parámetro #5 $attribPosition, ya que no se usaba, fue un mal intento de simular BITAMCatalogFilter
			$arrDef['attributeValues'] = $this->getAttributeValues(array_values($arrDef['attributes']), -1, '', null, false, true);
		}
		else {
			$arrDef['attributeValues'] = array();
		}
		//@JAPR
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		if (getMDVersion() >= esvQuestionAR) {
			$arrDef['isModel'] = $this->IsModel;
		}
		
		return $arrDef;
	}
  	
	//@JAPR 2012-05-07: Agregada la colección de catalogos usados pero no asociados a ninguna encuesta
	//@JAPR 2012-08-17: Agregado para v4 el parámetro $iMaxOrder para indicar hasta que atributo se desea recuperar el catálogo, de forma que no
	//se incluyan atributos que no se piensan usar (si se omite, se usarán todos los atributos)
	//@JAPR 2014-11-25: Agregado el parámetro $anArrayAttribIDs para especificar directamente que atributos se desean cargar. Si no se especifica
	//entonces se usarán el resto de los parámetros o bien se obtendrán todos los atributos
	//@JAPR 2015-08-29: Rediseñadas las agendas en v6
	/* Agregado el parámetro $iCheckPointID para especificar si se cargarán o no por un CheckPoint específico, en caso contrario se cargarán independientemente
	del CheckPoint (a la fecha de implementación sólo podía haber un DataSource ligado a un único CheckPoint, así que esto es válido, pero en caso de poder crear
	múltiples instancias se deberá de empezar a enviar el parámetro del CheckPoint siempre)
	*/
	static function GetCatalogAttributes($aRepository, $aDataSourceID, $iMaxOrder = 0, $anArrayAttribIDs = null, $iCheckPointID = 0) {
		global $appVersion;
		
		//@JAPR 2014-11-25: Agregado el parámetro $anArrayAttribIDs para especificar directamente que atributos se desean cargar. Si no se especifica
		//entonces se usarán el resto de los parámetros o bien se obtendrán todos los atributos
		//Validación para asegurar que la función será compatible hacia atrás y que no genere error en caso de recibir un parámetro inválido
		if (!is_null($anArrayAttribIDs) && !is_array($anArrayAttribIDs)) {
			$anArrayAttribIDs = array();
		}
		if (count($anArrayAttribIDs) == 0) {
			$anArrayAttribIDs = null;
		}
		//@JAPR
		
		//Identifica los atributos que funcionan como llave y que se deben ocultar
        $arrKeyAttributes = array();
		//@JAPR 2015-08-29: Rediseñadas las agendas en v6
		//Esta funcionalidad no aplica para v6
		if (getMDVersion() < esveFormsv6 && $appVersion >= esvFormatMaskHideAttribs) {
	        $sql = "SELECT A.MemberID 
	        	FROM SI_SV_DataSourceMember A 
	        	WHERE A.DataSourceID = $aDataSourceID AND A.DescriptorID > 0";
	        $aRS = $aRepository->DataADOConnection->Execute($sql);
	        if ($aRS) {
		        while (!$aRS->EOF) {
		            $intMemberID = (int) $aRS->fields["memberid"];
			        $arrKeyAttributes[$intMemberID] = $intMemberID;
		            $aRS->MoveNext();
		        }
			}
		}
		
		//@JAPR 2015-08-29: Rediseñadas las agendas en v6
		//Si se especificó un CheckPoint (el valor de 0 significaría el primer checkpoint que haga match con el DataSource) entonces carga la definición de los
		//tipos de atributo basados en él, ya que en esta versión se desligó del DataSource
		$arrDataSouceMemberTypes = array();
		if (getMDVersion() >= esveAgendas && $iCheckPointID >= 0) {
			$sql = "SELECT attr_id, type 
				FROM si_sv_checkpointattr 
				WHERE ".(($iCheckPointID > 0)?"checkpoint_id = {$iCheckPointID} AND ":"")." attr_id IN ( 
					SELECT MemberID 
					FROM SI_SV_DataSourceMember 
					WHERE DataSourceID = {$aDataSourceID} 
				)";
	        $aRS = $aRepository->DataADOConnection->Execute($sql);
	        if ($aRS) {
		        while (!$aRS->EOF) {
		            $intMemberID = (int) @$aRS->fields["attr_id"];
					$intType = (int) @$aRS->fields["type"];
					if ($intMemberID) {
						if (!isset($arrDataSouceMemberTypes[$intMemberID])) {
							$arrDataSouceMemberTypes[$intMemberID] = array();
						}
						$arrDataSouceMemberTypes[$intMemberID][$intType] = 1;
					}
		            $aRS->MoveNext();
		        }
			}
		}
		
		//@JAPR 2013-02-11: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$strAdditionalFields = '';
		//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', eBavelFieldID';
		}
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', KeyOfAgenda, AgendaLatitude, AgendaLongitude, AgendaDisplay';
		}
		if (getMDVersion() >= esveBavelGeolocationAttribs) {
			$strAdditionalFields .= ', eBavelFieldDataID';
		}
		//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
		if (getMDVersion() >= esvCatalogAttributeImage) {
			$strAdditionalFields .= ', IsImage';
		}
	    $sql = "SELECT MemberID, MemberName, MemberOrder $strAdditionalFields 
	    	FROM SI_SV_DataSourceMember 
	    	WHERE DataSourceID = " . $aDataSourceID;
	    //@JAPR 2012-08-17: Agregado para v4 el parámetro $iMaxOrder para indicar hasta que atributo se desea recuperar el catálogo
		if ($iMaxOrder > 0 ) {
			$sql .= " AND MemberOrder <= ".$iMaxOrder;
		}
	    $sql .= " ORDER BY MemberOrder";
	    //@JAPR
		
	    $aRS = $aRepository->DataADOConnection->Execute($sql);
		//@JAPR 2015-10-30: Corregido un bug, no se debe validar contra EOF generando un die, así que se dejará pasar ahora regresando array vacio (#1UTZS0)
	    if (!$aRS) {
	        //conchita 1-nov-2011 ocultar mensajes sql
	        //die("(".__METHOD__.") ".translate("Error accessing"));
	        die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_DataSourceMember " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
	    }
	    $arrayAttribIDs = array();
	    $i = 0;
		
	    while (!$aRS->EOF) {
	        $intMemberID = (int) $aRS->fields["memberid"];
			//@JAPR 2014-11-25: Agregado el parámetro $anArrayAttribIDs para especificar directamente que atributos se desean cargar. Si no se
			//especifica entonces se usarán el resto de los parámetros o bien se obtendrán todos los atributos
	        if (is_null($anArrayAttribIDs) || in_array($intMemberID, $anArrayAttribIDs)) {
		        $arrayAttribIDs[$intMemberID] = array();
		        $arrayAttribIDs[$intMemberID]["id"] = $intMemberID;
				//@JAPR 2015-08-30: Corregido un bug, por compatibilidad con BITAMCatalog, se requiere la propiedad sourceMemberID
				$arrayAttribIDs[$intMemberID]["sourceMemberID"] = $intMemberID;
				//ears 2017-01-13 validación de caracteres especiales
		        $arrayAttribIDs[$intMemberID]["name"] = GetValidJScriptText((string) $aRS->fields["membername"],array("fullhtmlspecialchars" => 1));
		        $arrayAttribIDs[$intMemberID]["order"] = (int) $aRS->fields["memberorder"];
		        $arrayAttribIDs[$intMemberID]["isKey"] = (int) isset($arrKeyAttributes[$intMemberID]);
				//@JAPR 2013-02-11: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		        $arrayAttribIDs[$intMemberID]["indDimID"] = (int) @$aRS->fields["inddimid"];
				//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
				$inteBavelFieldID = (int) @$aRS->fields["ebavelfieldid"];
				$inteBavelFieldDataID = (int) @$aRS->fields["ebavelfielddataid"];
				if ($inteBavelFieldID > 0) {
		        	$arrayAttribIDs[$intMemberID]["eBavelFieldID"] = $inteBavelFieldID;
					$arrayAttribIDs[$intMemberID]["eBavelFieldDataID"] = $inteBavelFieldDataID;
				}
				if (getMDVersion() >= esvAgendaRedesign) {
					$arrayAttribIDs[$intMemberID]["KeyOfAgenda"] = (int) @$aRS->fields[strtolower("KeyOfAgenda")];
					$arrayAttribIDs[$intMemberID]["AgendaLatitude"] = (int) @$aRS->fields[strtolower("AgendaLatitude")];
					$arrayAttribIDs[$intMemberID]["AgendaLongitude"] = (int) @$aRS->fields[strtolower("AgendaLongitude")];
					$arrayAttribIDs[$intMemberID]["AgendaDisplay"] = (int) @$aRS->fields[strtolower("AgendaDisplay")];
				}
				
				//@JAPR 2015-08-29: Rediseñadas las agendas en v6
				//Si se especificó un CheckPoint (el valor de 0 significaría el primer checkpoint que haga match con el DataSource) entonces carga la definición de los
				//tipos de atributo basados en él, ya que en esta versión se desligó del DataSource
				$arrayAttribIDs[$intMemberID]["KeyOfAgenda"] = (int) @$arrDataSouceMemberTypes[$intMemberID][1];
				$arrayAttribIDs[$intMemberID]["AgendaLatitude"] = (int) @$arrDataSouceMemberTypes[$intMemberID][3];
				$arrayAttribIDs[$intMemberID]["AgendaLongitude"] = (int) @$arrDataSouceMemberTypes[$intMemberID][4];
				$arrayAttribIDs[$intMemberID]["AgendaDisplay"] = (int) @$arrDataSouceMemberTypes[$intMemberID][2];
				
				//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
				if (getMDVersion() >= esvCatalogAttributeImage) {
					$blnIsImage = (int) @$aRS->fields["isimage"];
					$arrayAttribIDs[$intMemberID]["isImage"] = ($blnIsImage)?1:0;
				}
		        //@JAPR
		        $i++;
	        }
	        //@JAPR
	        $aRS->MoveNext();
	    }
		
	    return $arrayAttribIDs;
	}
	//@JAPR
	
	//@JAPR 2012-05-07: Agregada la colección de catalogos usados pero no asociados a ninguna encuesta
	//@JAPRWarning: En realidad el parámetro $aSurvey no se requiere, así que en lugar de generar una nueva función que sólo reciba el catálogo, se 
	//reutilizará esta enviando $aSurvey como null puesto que el resultado en ambas funciones sería el mismo
	//@JAPR 2012-08-17: Agregados estos métodos en forma estática para ser usados desde cualquier punto. Modificado para sintaxis de v4
	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	//@JAPR 2013-02-11: Agregada la edición de datos históricos
	//Agregado el parámetro $bJustGetHistoricEntryData para cargar sólo los valores usados durante la captura especificada sin regresar también
	//los valores propios del catálogo en este momento. Esto es útil sólo para generar el array de valor de la captura ya que se usa una numeración
	//negativa y tiene que se consistente con el array usado durante la edición en el browser
	//@JAPR 2014-05-28: Agregado el parámetro $bGetDistinctValues para que en el caso de catálogos mapeados de eBavel se obtengan los valores únicos
	//@JAPRDescontinuada: Ya no se requiere en los procesos de eForms v6, hay que evitar usarla
	static function GetCatalogAttributeValues($aRepository, $aCatalog, $arrayAttribIDs = null, $aUserID = -1, $filter="", $entryID = null, $entrySurveyID = null, $bJustGetHistoricEntryData = false, $bGetDistinctValues = false) {
		die("Bad request in function parameters");
	}
	
	//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
	//Dado un catálogo, identifica si es de eForms o de eBavel y traduce las variables de cada atributo a los nombres de campo correspondientes
	//regresando el filtro listo para ejecutarse según el contexto
	static function TranslateAttributesVarsToFieldNames($aRepository, $aDataSourceID, $aFilterText) {
		if (trim($aFilterText) == '') {
			return '';
		}
		//@JRPP 2015-06-27: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$objCatalog = $strCalledClass::NewInstanceWithID($aRepository, $aDataSourceID);
		if (is_null($objCatalog)) {
			return $aFilterText;
		}
		
		$objCatalogMembersColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $aDataSourceID);
		if (is_null($objCatalogMembersColl) || count($objCatalogMembersColl->Collection) == 0) {
			return $aFilterText;
		}
		
		$strFilterText = $aFilterText;
		$blneBavelCatalog = false;
		$arreBavelFieldIDs = array();
		if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
			$blneBavelCatalog = true;
			$arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');
			foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
				foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
					$arreBavelFieldIDs[$intFieldID] = $arrFieldData;
				}
			}
		}
		
		//Independientemente del tipo de catálog, debe recorrer la definición de atributos ya que los nombres de variables finalmente se manejan
		//mediante el CatMemberID
	    //@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
		foreach ($objCatalogMembersColl->Collection as $intMemberOrder => $objCatalogMember) {
			if ($objCatalogMember->MemberID > 0) {
				$strMemberVarName = $objCatalogMember->getAttribIDVariableName();
				$arrVariableNames[$intMemberOrder] = "{".$strMemberVarName."}";
			}
		}
		
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		natcasesort($arrVariableNames);
		$arrVariableNames = array_reverse($arrVariableNames, true);
		foreach ($arrVariableNames as $intMemberOrder => $strMemberVarName)	{
			$objCatalogMember = @$objCatalogMembersColl->Collection[$intMemberOrder];
			if (is_null($objCatalogMember)) {
				continue;
			}
			
			if ($blneBavelCatalog && $objCatalogMember->eBavelFieldID > 0) {
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				//En este caso el atributo que mapea a un campo tipo Geolocalización utilizará el id del campo, sin hacer referencia a cual de
				//las partes (Latitude o Longitude) especificamente utilizará, ya que los filtros de eBavel lo resuelven unificado
				$strFieldName = (string) @$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID]['id'];
			}
			else {
				$strFieldName = $objCatalogMember->FieldName;
			}
			
			$strFilterText = str_ireplace($strMemberVarName, $strFieldName, $strFilterText);
		}
		//@JAPR
		
		return $strFilterText;
	}
	
	//Actualiza la versión del catálogo (invocada desde procesos que lo pudieran afectar sin necesidad de forzar al grabado de la instancia)
	static function updateLastUserAndDateInDataSource($aRepository, $aDataSourceID)
	{
		$currentDate = date("Y-m-d H:i:s");
		
		//@JAPR 2016-12-05: Agregados los datos de última edición (#OK0KN7)
		$sql = "UPDATE SI_SV_DataSource SET ".
				" LastModUserID = ".$_SESSION["PABITAM_UserID"].
				", LastModDate = ".$aRepository->DataADOConnection->DBTimeStamp($currentDate).
				", VersionNum = ".$aRepository->DataADOConnection->IfNull("VersionNum", "0")." +1".
			" WHERE DataSourceID = ".$aDataSourceID;
		if ($aRepository->DataADOConnection->Execute($sql)) {
			//Actualiza la última versión de los catálogos derivados del DataSource
			$sql = "UPDATE SI_SV_Catalog SET 
					VersionNum = ".$aRepository->DataADOConnection->IfNull("VersionNum", "0")." +1 
				WHERE DataSourceID = {$aDataSourceID}";
			$aRepository->DataADOConnection->Execute($sql);
			
			//Realizamos actualizacion de LastDate en las encuestas relacionadas con este DataSource
			BITAMSurvey::updateAllSurveyLastUserAndDateFromDataSource($aRepository, $aDataSourceID);
		}
	}
	
	//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
	/* Actualiza la versión de los datos del catálogo (invocada desde procesos que lo pudieran afectar sin necesidad de forzar al grabado de la instancia).
	Originalmente utilizada desde el Agente de eForms durante el procesamiento de DataSources para permitir cambiar exclusivamente la versión de los datos de los
	DataSources / catálogos sin afectar a la versión del DataSource en sí */
	static function UpdateLastDataDateInDataSource($aRepository, $aDataSourceID, $sNewLastDataDate = null)
	{
		if ( is_null($sNewLastDataDate) ) {
			$sNewLastDataDate = date("Y-m-d H:i:s");
		}
		
		$sql = "UPDATE SI_SV_DataSource SET 
				DataVersionDate = ".$aRepository->DataADOConnection->DBTimeStamp($sNewLastDataDate)." 
			WHERE DataSourceID = ".$aDataSourceID;
		if ($aRepository->DataADOConnection->Execute($sql)) {
			//Actualiza la última versión de los catálogos derivados del DataSource
			$sql = "UPDATE SI_SV_Catalog SET 
					DataVersionDate = ".$aRepository->DataADOConnection->DBTimeStamp($sNewLastDataDate)." 
				WHERE DataSourceID = {$aDataSourceID}";
			$aRepository->DataADOConnection->Execute($sql);
		}
	}
	
	//@JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
	/* Actualiza el último error de sincronización al especificado en la instancia, asignando la hora actual como la fecha de última notificación
	Esto es necesario durante el proceso de sincronización de datos de DataSources desde el Agente para evitar que se envíen las mismas notificaciones de error a lo largo de todo el
	día mientras el agente esté procesando una y otra vez el mismo DataSource
	*/
	function updateLastDataSyncError($sNewDataSyncError) {
		$currentDate = date("Y-m-d H:i:s");
		
		$sql = "UPDATE SI_SV_DataSource SET ".
				" LastSyncErrorDate = ".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
				", LastSyncError = ".$this->Repository->DataADOConnection->Quote((string) $sNewDataSyncError).
			" WHERE DataSourceID = ".$this->DataSourceID;
		$this->Repository->DataADOConnection->Execute($sql);
	}
	
	//@JAPR 2013-02-11: Agregada la edición de datos históricos
	//Regresa los datos del catálogo según como se grabaron en la captura de la encuesta especificada
	function getAttributeValuesForEntryID($arrayAttribIDs = null, $entrySurveyID = 0, $entryID = 0)
	{
		$i=0;
		$attribValues = array();
		
		if ($entrySurveyID <= 0 || $entryID <= 0) {
			return $attribValues;
		}
		
		//Si se está editando una captura, debe generar como primer elemento del array de datos la combinación exacta con la que se grabó la
		//captura indicada, asignandole a cada combinación un key negativo para hacer diferencia entre los datos actuales del catálogo
		//En dado caso que todas las combinaciones grabadas si fueran exactas a registros actual del catálogo, se podría omitar esta parte y
		//simplemente regresar el catálogo tal cual, ya que el App podría identificar la combinación usada en sus datos
		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $entrySurveyID);
		
		//Solo si la encuesta utiliza dimensiones independientes para su catálogos es necesario obtener las descripciones directo de dichas
		//dimensiones, puesto que en cualquier otro caso se usaba sólo el catálogo tal como existiera al momento de la edición
		if (is_null($surveyInstance)) {
			return $attribValues;
		}
		
		/*Se asumirán varias cosas llegado a este punto:
			- A la fecha de esta funcionalidad, no debía haber preguntas de catálogo del mismo catálogo de la dinámica posteriores a dicha
		sección, por tanto es seguro asumir que para ese catálogo bastaría con obtener la combinación de la dinámica para generar los datos
		usados en este catálogo
			- Si hubiera preguntas simple-choice de este catálogo y no hay secciones dinámicas, la última pregunta está obligada a filtrarse
		por las anteriores, por lo tanto basta con obtener el valor de la última pregunta en el orden para obtener la combinación completa,
		sin embargo algunas preguntas pueden ser omitidas y en esos casos hubiera grabado el valor de No Aplica, así que es necesario traer
		en cada registro todas las preguntas del catálogo y comparar si alguna de ellas es diferente al No Aplica (esto mismo sucede con
		las secciones dinámicas, pudiera no haberse contestado dicha sección por lo que la combinación realmente la traería la pregunta que
		la genera y no la propia sección)
		*/
		$intFactKeyDimID = $surveyInstance->FactKeyDimID;
		
		if (is_null($arrayAttribIDs)) {
			$arrayAttribIDs = array();
		}
		
		//@JAPRWarning: Validar que todas las dimensiones se encuentren en el cubo, si no es así, recordar el array de atributos hasta el máximo
		//usado para evitar que se genere un error en el query
		//Probablemente se deba cambiar a LEFT JOIN para evitar que en caso de venir como NULL el valor de una dimensión, se terminen omitiendo
		//combinaciones que si se grabaron
		//@JAPR 2013-07-19: Corregido un bug, ahora se validará correctamente las dimensiones independientes que se encuentran asociadas al modelo
		//para sólo intentar utilizar dichas dimensiones y que no genere errores el query, sin embargo se debe analizar si es o no necesario agregar
		//dummies de las dimensiones omitidas o no, ya que estas combinaciones al ser de la edición, exclusivamente tendrán los atributos que
		//finalmente se utilizaron para el grabado y que teoricamente son los únicos que usa la encuesta, por lo que en la primera fase no se
		//incluirán NAs para los atributos no usados/existentes en el cubo esperando que no afecte en nada a la edición
		$arrModelCatDimens = array();
		$arrAttribIDs = array();
		$lim = count($arrayAttribIDs);
		for($i=0; $i<$lim; $i++) {
			$attribID = $arrayAttribIDs[$i]["indDimID"];
			$arrAttribIDs[] = $attribID;
		}
		
		//@JAPR 2013-04-24: Corregido un bug durante la edición, la forma de corregir esto no será validando en busca de excluir registros cuando
		//venga el valor de *NA en alguno de ellos, porque para por lo menos 2 casos de clientes (La Redonda y Rich) se usaron dichos valores
		//en secciones dinámicas por lo que excluyendolos se removían algunos checkboxes y descuadraban la captura. Ahora lo que se hará será
		//filtrar el query por la sección dinámica si es que el catálogo se usó en ella para evitar traer los *NA que sean de la simple choice,
		//de lo contrario si solo hay estándaras o maestro-detalle se asume que son simple choices y no importa si se incluyen valores con *NA
		//por lo que se hará el query como originalmente se hacía
		$dynamicSectionID = (int) @BITAMSection::existDynamicSection($this->Repository, $entrySurveyID);
		$blnFilterByDynamicSection = false;
		$dynamicSectionCatID = 0;
		//Si hay sección dinámica, se obtiene el ID para buscarlo en la captura
		if ($dynamicSectionID > 0) {
			$dynamicSection = BITAMSection::NewInstanceWithID($this->Repository, $dynamicSectionID);
			if (!is_null($dynamicSection)) {
				$dynamicSectionCatID = $dynamicSection->CatalogID;
			}
			
			if ($dynamicSectionCatID == $this->DataSourceID) {
				$blnFilterByDynamicSection = true;
			}
		}
		
		//Si el catálogo corresponde al de la sección dinámica, verifica si hay o no algún registro de dicha sección, si lo hay entonces en cualquier
		//pregunta de otro tipo que usara este catálogo solo podría haber por fuerza valores que estaban como parte de esta dinámica (a la fecha
		//de este cambio no se soportaba que se mezclaran dinámicas y maestro-detalle que reutilizaran el mismo catálogo, ni simple choice del
		//mismo catálogo posteriores a la dinámica, así que en la dinámica terminaban los posibles valores usados del catálogo), si no hubiera
		//registros entonces se pueden tomar todos incluyendo donde las dimensiones tuvieran valores de *NA
		if ($blnFilterByDynamicSection) {
			$sql = "SELECT EntrySectionID 
				FROM {$surveyInstance->SurveyTable} 
				WHERE FactKeyDimVal = $entryID AND EntrySectionID = $dynamicSectionID";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nVerifying EntrySectionID: $sql<br>\r\n");
			}
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				return $attribValues;
			}
			if($aRS->EOF)
			{
				//Si no hubiera registros entonces significa que no capturaron la sección dinámica, por tanto no es necesario filtrar por ella
				$blnFilterByDynamicSection = false;
			}
		}
		
		$strFields = "";
		$strFieldsGroup = "";
		$arrIdxs = array();
		$arrLast = array();
		
		$lim = count($arrayAttribIDs);
		$fromCat = "";
		$joinCat = "";
		for($i=0; $i<$lim; $i++)
		{
			$attribID = $arrayAttribIDs[$i]["indDimID"];
			//@JAPR 2013-07-19: Corregido un bug, ahora se validará correctamente las dimensiones independientes que se encuentran asociadas al modelo
			if (!isset($arrModelCatDimens[$attribID])) {
				//No existe dimensión para este atributo en el modelo, así que lo omite (teóricamente, no podría faltar una dimensión intermedia
				//así que a partir de este punto ya no debería agregar ningún atributo mas, pero se dejará abierta la revisión, sólo se agregará
				//el array vacio de estos atributos para que el ciclo de llenado no genere errores)
				$arrIdxs[$i] = -1;
				$arrLast[$i] = null;
				eval('$attrib'.$i.'=array();');
				continue;
			}
			
			if($strFields!="")
			{
				$strFields.=", ";
				$strFieldsGroup.=", ";
			}
			//@JAPR
			
			$selectFieldKey = "RIDIM_".$attribID."KEY";
			$tableName = "RIDIM_".$attribID;
			//@JAPR 2013-04-23: Corregido un bug durante la edición, debe excluir a los valores de *NA grabados porque al considerarlos
			//los estaba interpretando como páginas dinámicas u opciones adicionales que no se deben capturar, por lo que ahora solo considerará
			//aquellos miembros de las dimensiones cuyo key > 1
			$strFields .= "MAX(".$tableName.".".$selectFieldKey.") AS ".$selectFieldKey.", ".$tableName.".DSC_".$attribID;
			$strFieldsGroup .= $tableName.".DSC_".$attribID;
			$fromCat .= ', '.$tableName;
			//@JAPR 2013-03-05: Corregido un bug, la condición de que el KEY tiene que ser > 1 no debe aplicar realmente, ya que aunque es buena
			//idea no presentar valores de NA, como se trata de un join compuesto si no capturan alguno de los últimos atributos entonces no
			//traería ningún registro y eso no es correcto
			$joinCat .= ' AND '.$surveyInstance->FactTable.'.'.$selectFieldKey.' = '.$tableName.'.'.$selectFieldKey;
				//' AND '.$tableName.'.'.$selectFieldKey.' > 1';
			//@JAPR
			$arrIdxs[$i] = -1;
			$arrLast[$i] = null;
			
			eval('$attrib'.$i.'=array();');
		}
		
		$selectFieldKey = "RIDIM_".$intFactKeyDimID."KEY";
		$where = " WHERE ".$selectFieldKey." = ".$entryID;
		//@JAPR 2013-04-24: Corregido un bug durante la edición, la forma de corregir esto no será validando en busca de excluir registros cuando
		//venga el valor de *NA en alguno de ellos, porque para por lo menos 2 casos de clientes (La Redonda y Rich) se usaron dichos valores
		//en secciones dinámicas por lo que excluyendolos se removían algunos checkboxes y descuadraban la captura. Ahora lo que se hará será
		//filtrar el query por la sección dinámica si es que el catálogo se usó en ella para evitar traer los *NA que sean de la simple choice,
		//de lo contrario si solo hay estándaras o maestro-detalle se asume que son simple choices y no importa si se incluyen valores con *NA
		//por lo que se hará el query como originalmente se hacía
		$strFromSVSurvey = '';
		$strJoinSVSurvey = '';
		if ($blnFilterByDynamicSection) {
			$strFromSVSurvey = ', '.$surveyInstance->SurveyTable;
			$strJoinSVSurvey = " AND ".$surveyInstance->FactTable.".FactKey = ".$surveyInstance->SurveyTable.".FactKey AND ".
				$surveyInstance->SurveyTable.".EntrySectionID = ".$dynamicSectionID;
		}
		//@JAPR
		
		//@JAPR 2012-08-20: Corregido un bug si el catálogo sólo tenía un atributo, entonces debe agrupar por dicho atributo
		//@JAPR 2013-12-10: Corregido un bug, si se ordena por FactKey, entonces los catálogos que no son de las secciones dinámicas y donde hubiera
		//múltiples atributos, hubieran generado un array donde se repitieran en muchas ocasiones los atributos padre pero intercalados entre
		//diferentes valores, así que al pintar en el App las combos siempre hubiera tomado la primera ruta, por tanto no se debe ordenar por FactKey
		//sino por los propios atributos, pero esto sólo es cierto si el catálogo en cuestión no es el mismo de la sección dinámica, ya que si lo
		//fuera en ese caso si debe respetar el orden en que se insertaron los valores
		$strOrderBy = '';
		if ($dynamicSectionCatID == $this->DataSourceID) {
			//En este caso se trata del catálogo dinámico, por tanto se ordena en el orden de inserción de los valores en el cubo
			$strOrderBy = "ORDER BY {$surveyInstance->FactTable}.FactKey";
		}
		else {
			//En este caso se trata de un catálogo diferente al dinámico, así que se ordena por los atributos en jerarquía
			if (trim($strFieldsGroup) != '') {
				$strOrderBy = "ORDER BY {$strFieldsGroup}";
			}
		}
		
		$sql = "SELECT DISTINCT ".$strFields." 
			FROM ".$surveyInstance->FactTable.$strFromSVSurvey.$fromCat.$where.$strJoinSVSurvey.$joinCat." 
			GROUP BY ".$strFieldsGroup." 
			{$strOrderBy}";	//.$strFields;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nCatalog combination query: $sql<br>\r\n");
		}
		//@JAPR
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nError reading catalog combination: ".@$this->Repository->DataADOConnection->ErrorMsg()."<br>\r\n");
			}
			return $attribValues;
		}
		if($aRS->EOF)
		{
			return $attribValues;
		}
		
		$valueFieldKey = -1;
		while(!$aRS->EOF)
		{
			for($i=0; $i<$lim; $i++)
			{
				//@JAPR 2013-07-19: Corregido un bug, ahora se validará correctamente las dimensiones independientes que se encuentran asociadas al modelo
				$attribID = $arrayAttribIDs[$i]["indDimID"];
				if (!isset($arrModelCatDimens[$attribID])) {
					//No existe dimensión para este atributo en el modelo, así que lo omite (teóricamente, no podría faltar una dimensión intermedia
					//así que a partir de este punto ya no debería agregar ningún atributo mas, pero se dejará abierta la revisión)
					continue;
				}
				//@JAPR
				
				$field = "DSC_".$arrayAttribIDs[$i]["indDimID"];
				$attribVal = $aRS->fields[strtolower($field)];
				//@JAPR 2013-04-23: Corregido un bug durante la edición, debe excluir a los valores de *NA grabados porque al considerarlos
				//los estaba interpretando como páginas dinámicas u opciones adicionales que no se deben capturar, por lo que ahora solo considerará
				//aquellos miembros de las dimensiones cuyo key > 1
				$selectFieldKey = "RIDIM_".$arrayAttribIDs[$i]["indDimID"]."KEY";
				$attribKey = (int) @$aRS->fields[strtolower($selectFieldKey)];
				//Si es el valor de *NA o no se pudo leer, no puede continuar
				//@JAPR 2013-04-24: Corregido un bug durante la edición, la forma de corregir esto no será validando en busca de excluir registros cuando
				//venga el valor de *NA en alguno de ellos, porque para por lo menos 2 casos de clientes (La Redonda y Rich) se usaron dichos valores
				//en secciones dinámicas por lo que excluyendolos se removían algunos checkboxes y descuadraban la captura. Ahora lo que se hará será
				//filtrar el query por la sección dinámica si es que el catálogo se usó en ella para evitar traer los *NA que sean de la simple choice,
				//de lo contrario si solo hay estándaras o maestro-detalle se asume que son simple choices y no importa si se incluyen valores con *NA
				//por lo que se hará el query como originalmente se hacía
				/*
				if ($attribKey <= 1) {
					continue;
				}
				*/
				
				//@JAPR
				if($i!=($lim-1))
				{
					if($attribVal!==$arrLast[$i])
					{
						if($arrLast[$i]!==null)
						{
							for($j=($lim-2); $j>=$i; $j--)
							{
								eval('$attrib'.$j.'[$arrIdxs[$j]]["children"]=$attrib'.($j+1).';');
							}
							
							for($j=$i+1;$j<$lim;$j++)
							{
								eval('$attrib'.$j.'=array();');
								$arrIdxs[$j] = -1;
								$arrLast[$j] = null;
							}
						}
						
						$arrIdxs[$i] = $arrIdxs[$i] + 1;
						eval('$attrib'.$i.'[$arrIdxs[$i]]["key"]=$valueFieldKey;');
						eval('$attrib'.$i.'[$arrIdxs[$i]]["value"]=$attribVal;');
					}
				}
				else 
				{
					$arrIdxs[$i]=$arrIdxs[$i]+1;
					eval('$attrib'.$i.'[$arrIdxs[$i]]["key"]=$valueFieldKey;');
					eval('$attrib'.$i.'[$arrIdxs[$i]]["value"]=$attribVal;');
					eval('$attrib'.$i.'[$arrIdxs[$i]]["children"]=array();');
				}
				
				$arrLast[$i] = $attribVal;
			}
			
			$aRS->MoveNext();
			$valueFieldKey--;
		}
		
		if($lim>1)
		{
			for($j=$lim-2;$j>=0;$j--)
			{
				eval('$attrib'.$j.'[$arrIdxs[$j]]["children"]=$attrib'.($j+1).';');
			}
		}
		
		$attribValues = $attrib0;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nCatalog values:<br>\r\n");
			PrintMultiArray($attribValues);
		}
		return $attribValues;
	}
	//@JAPR
	
	//@JAPR 2015-12-22: Agregado el tipo de DataSource basado en Tabla
	/* Esta función se encarga de obtener los datos de DataSource con conexión externa según los parámetros indicados (todos excepto el tipo eBavel, el cual ya
	tiene un método directo) funcionando de la misma manera en que lo hace la función getCatalogValues, a la cual de hecho se invoca cuando es un DataSource de
	tipo eBavel
	Este NO es el método que se usa para procesar el DataSource desde el agente, el cual primero descarga el archivo indicado y luego llena la tabla de datos, pero
	podría utilizarse precisamente para lo mismo sólo que regresando un array de datos tal como lo requiere la función getAttributeValues, que es quien invocaría
	a este método, realmente se implementó para los DataSource de tipo Tabla, aunque como se indica, se podría extender para el resto de los casos disponibles
	Como todos los DataSource externos utilizan el nombre del campo, columna o lo que corresponda (se basan en un XLS todos, excepto Tabla que es literalmente una
	tabla de mySQL) el nombre del atributo es lo que se intentará consultar en dicho origen externo, y que a la fecha de implementación no existía una etiqueta local
	para eForms que permitiera renombrarlo
	A la fecha de implementación, el parámetro $bGetRecordset era irrelevante, ya que se implementó esta función para usarse dentro de getAttributeValues de tal
	manera que se devuelva un recordset para que dicha función pueda terminar correctamente, así que se asumirá que siempre viene en true ese parámetro, sin embargo
	se puede extender la funcionalidad para regresar un array mas elaborado en lugar de un recordset
	//@JAPR 2016-06-13: Agregada la configuración para limitar los resultados de los valores de catálogo en modo de diseño exclusivamente (#14KN4H)
	Agregado el parámetro $iTopValues para controlar la cantidad de registros a recuperar en el catálogo
	*/
	function getExternalCatalogValues($arrayAttribIDs = null, $aUserID = -1, $filter="", $bGetRecordset = false, $inclueidValue = false, $withoutGroup = false, $iFetchMode = null, $withSecurity = true, $bIncludeSystemFields = false, &$bError = false, $iTopValues = 0) {
		$aRS = null;
		
		switch ($this->Type) {
			case dstyeBavel:
				//El DataSource tipo eBavel tiene su propia función, así que simplemente la invocará con los mismos parámetros
				$aRS = $this->getAttributeValues($arrayAttribIDs, $aUserID, $filter, $bGetRecordset, $inclueidValue, $withoutGroup, $iFetchMode, $withSecurity, $bIncludeSystemFields, $bError);
				break;
				
			case dstyTable:
				//El DataSource tipo Tabla realizará la conexión al servicio externo e intentará obtener los datos mediante un query usando los parámetros
				$objServiceConnection = BITAMServiceConnection::NewInstanceWithID($this->Repository, $this->ServiceConnectionID);
				if (is_null($objServiceConnection)) {
					break;
				}
				
				//Sin datos de conexión suficientes no puede obtener los valores del DataSource
				if (trim($objServiceConnection->ServiceName) == '' || trim($this->SourceDatabase) == '' || trim($this->SourceTable) == '') {
					break;
				}
				if (getMDVersion()>=esvTypeOfDatabase) {
					if ($objServiceConnection->Servicedbtype == ddrtSQLServer ){
						$type = 'mssql';
					}else {
						$type = 'mysql';
					}
				}else {
					$type = 'mysql';
				}				
				//Realiza la conexión a la Base de datos especificada
				ob_start();
				$objDBConnection = BITAMConnection::NewInstance($objServiceConnection->ServiceName, $objServiceConnection->User, $objServiceConnection->Password, $this->SourceDatabase, $type);
				if (is_null($objDBConnection) || is_null($objDBConnection->ADOConnection)) {
					//En caso de error, no se tiene un método para reportarlo así que simplemente se manda al Debugger, pero la función debe continuar como si
					//no hubiera logrado obtener sus valores, ya que es una función que se usa durante un servicio del App así que no puede generar un error
					return $aRS;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
						ob_end_clean();
						ECHOString("Unable to connect to '{$objServiceConnection->ServiceName}': {$strErrorMsg}");
						break;
					}
				}
				
				//En este caso se pudo establecer la conexión, así que continua con el Query de los datos solicitados
				/* Le indicamos a las conexiones que usaremos UTF8 (link http://stackoverflow.com/a/2159453) */
				$objDBConnection->ADOConnection->Execute("set names 'utf8'");
				
				$where = "";
				$arrfiels = array();
				$arrDataSourceMembersByIDs = array();
				$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->DataSourceID);
				if (is_null($objDataSourceMembersColl)) {
					break;
				}
				foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
					$arrDataSourceMembersByIDs[$objDataSourceMember->MemberID] = $objDataSourceMember;
				}
				
				//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				$strSpecialOrderFields = '';
				$arrfielsOrder = array();
				foreach ($arrayAttribIDs as $key => $value) {
					$intDataSourceMemberID = (int) @$arrayAttribIDs[$key]["sourceMemberID"];
					$objDataSourceMember = @$arrDataSourceMembersByIDs[$intDataSourceMemberID];
					if (is_null($objDataSourceMember)) {
						return null;
					}
					
					//Se utilizará el FieldName como el Alias del campo para que el resto del proceso sea natural, pero se consulta a partir del MemberName
					//ya que es ahí donde se almacena el nombre real de la columna en la tabla origen
					$arrayAttribIDs[$key]["fieldName"] = $objDataSourceMember->FieldName;
					//$arrayAttribIDs[$key]["fieldName"] = ($objDataSourceMember->FieldName)?$objDataSourceMember->FieldName:$objDataSourceMember->MemberName;
					$arrfiels[] = $objDataSourceMember->MemberName." AS ".$arrayAttribIDs[$key]["fieldName"];
					//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
					$arrfielsOrder[] = $objDataSourceMember->MemberName;
					if ((int) @$arrayAttribIDs[$key]["isOrder"]) {
						//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
						$strOrientation = '';
						if ((int) @$arrayAttribIDs[$key]["sortDir"] == ordbDsc) {
							$strOrientation = " DESC";
						}
						$strSpecialOrderFields .= $objDataSourceMember->MemberName.$strOrientation.', ';
						//@JAPR
					}
					//@JAPR
				}
				
				if(is_array($filter)){
					foreach ($filter as $key2 => $value2 ){
						$andArr = array();
						foreach ($filter[$key2] as $key3 => $value3) {
							$andArr[] =  "'{$value3}'";
						}
						
						$objDataSourceMember = @$arrDataSourceMembersByIDs[$key2];
						if ($objDataSourceMember) {
							$fieldName = $objDataSourceMember->FieldName;
							$whereArr[] = " {$fieldName} in (" . implode(', ', $andArr) . ")";
						}
					}
					$where = " WHERE " . implode(' AND ', $whereArr);
				}else if (trim($filter) != ''){
					$filter = trim($filter);
					$where = " WHERE " . $filter;
				}
				
				if ($withSecurity) {
					//@JAPR 2015-12-24: Corregido un bug, no estaba asignada la variable del usuario (#012P4U)
					$userID = (int) @$_SESSION["PABITAM_UserID"];
					//@JAPR
					$whereDatafilter =  BITAMDataSourceFilter::generateResultFilterByUser($this->Repository, $this->DataSourceID, $userID);
					if ($whereDatafilter!=""){
						$where = ($where==""? " WHERE " . $whereDatafilter : $where . " AND " . $whereDatafilter);
					}
				}
				$arrGroupby = array();
				
				if ($this->Type == dstyTable) {
					foreach ($arrfiels as $key => $value) {
						$pos = strpos($value, " AS ");
						if ($pos === false) {
							$arrGroupby[] = $value;
						}else{
							$arrGroupby[] = substr($value, 0,$pos);
						}
					}
					//@JRPP 2016-02-04: Actualizo el where cuando se trata de una fuente tipo tabla 
					if ($where != ""){
						foreach ($arrfiels as $key => $value) {
							$pos = strpos($value, " AS ");
							if (!$pos === false) {
								$oriRemplaza = substr($value, $pos + 4);
								$newRemplaza = substr($value, 0,$pos);
								$where=str_replace($oriRemplaza, $newRemplaza, $where);
							}
						}
					}
				}else {
					$arrGroupby = $arrfiels;
				}
				
				//@JAPR 2016-05-06: Agregado el ordenamiento de atributos, ya que se había dejado sin esa cláudula y se dependía del ORDER BY o bien el orden de creación de los campos/índice
				//según MySQL, y eventualmente se podrán reordenar atributos
				//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				$iTopValues = (int) $iTopValues;
				$strLimit = '';
				//@JAPR 2016-12-13: Agregado el esquema de paginación para obtener valores (#U7J0XU)
				//Validado el LIMIT para MySQL, para los otros tipos de BDs usará una instrucción nativa de ADODB
				$blnUseSelectLimit = false;
				$intStartingRow = null;
				if ($type == 'mysql') {
					if ($iTopValues > 0) {
						$strLimit = " LIMIT {$iTopValues}";
					}
				}
				
				//@JAPR 2016-12-12: Agregado el esquema de paginación para obtener valores (#U7J0XU)
				//Si se asignaron las propiedades de paginación, no podrían convivir con el límite como parámetro pues no son compatibles, así que tendrá precedencia la paginación, en cuyo
				//caso asigna el límite inicial y cantidad de rows a obtener (el limit inicia en 0)
				if ($this->EnablePagination && $this->TotalValuesPagesNum > 1) {
					//@JAPR 2016-12-13: Agregado el esquema de paginación para obtener valores (#U7J0XU)
					//Validado el LIMIT para MySQL, para los otros tipos de BDs usará una instrucción nativa de ADODB
					$intStartingRow = ($this->CurrentValuesPage - 1) * $this->NumValuesPerPage;
					//Se forzará a utilizar el SelectLimit para catálogos tipo ODBC exclusivamente, ya que se comprobó que funciona bien para MySQL y es la alternativa para SQL Server,
					//por tanto la condición del if no se va a cumplir jamás para forzar el caso
					if (false && $type == 'mysql') {
						$strLimit = " LIMIT {$intStartingRow}, {$this->NumValuesPerPage}";
					}
					else {
						$strLimit = '';
						$blnUseSelectLimit = true;
					}
					//@JAPR
				}
				
				$sql = "SELECT " . implode(', ', $arrfiels) . " FROM ". $this->SourceTable . $where .($withoutGroup?"":" GROUP BY " . implode(', ', $arrGroupby)).
					" ORDER BY " .$strSpecialOrderFields. implode(', ', $arrfielsOrder).$strLimit;
				//@JAPR
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\nVerifying tablename: $this->TableName<br>\r\n");
					echo("<br>\r\nVerifying arrfiels: " . implode(', ', $arrfiels) . "<br>\r\n");
					echo("<br>\r\nVerifying where: $where<br>\r\n");
					echo("<br>\r\nVerifying sql: $sql<br>\r\n");
				}
				
				//@JAPR 2015-08-02: Corregido un bug, la función debe regresar un array vacio incluso en caso de error para no detener la ejecución de la descarga de definiciones 
				//@JAPR 2015-08-23: Validado que el fetchMode sólo aplique si se recibe el parámetro $iFetchMode
				if (!is_null($iFetchMode)) {
					$intCurrentFecthMode = $objDBConnection->ADOConnection->SetFetchMode($iFetchMode);
				}
				
				//@JAPR 2016-12-13: Agregado el esquema de paginación para obtener valores (#U7J0XU)
				//Validado el LIMIT para MySQL, para los otros tipos de BDs usará una instrucción nativa de ADODB
				if ($blnUseSelectLimit) {
					if ($intStartingRow) {
						$aRS = $objDBConnection->ADOConnection->SelectLimit($sql, $this->NumValuesPerPage, $intStartingRow);
					}
					else {
						$aRS = $objDBConnection->ADOConnection->SelectLimit($sql, $this->NumValuesPerPage);
					}
				}
				else {
					$aRS = $objDBConnection->ADOConnection->Execute($sql);
				}
				//@JAPR
				if (!is_null($iFetchMode)) {
					$objDBConnection->ADOConnection->SetFetchMode($intCurrentFecthMode);
				}
				break;
				
			default:
				//Cualquier otro tipo de DataSource externo por ahora, se basa en el principio de un archivo XLS o un JSON, en ninguno de los dos casos se trata
				//de un objeto recordset ni que pueda simularlo, así que para implementar correctamente esta función se tendría que generar una clase que lo simule,
				//mientras tanto no estarán disponibles ya que no se cargan bajo demanda sus datos, sino con un proceso del Agente que si utiliza el XLS/JSON completo
				break;
		}
		
		return $aRS;
	}
	
	//@JAPR 2016-12-12: Agregado el esquema de paginación para obtener valores (#U7J0XU)
	/* Obtiene la cantidad de registros que existen en una tabla de servidor externo ODBC */
	function getExternalCatalogValuesCount($arrayAttribIDs = null, $aUserID = -1, $filter="", $bGetRecordset = false, $inclueidValue = false, $withoutGroup = false, $iFetchMode = null, $withSecurity = true, $bIncludeSystemFields = false, &$bError = false, $iTopValues = 0) {
		$intTotalRowsCount = 0;
		$aRS = null;
		
		//El DataSource tipo Tabla realizará la conexión al servicio externo e intentará obtener los datos mediante un query usando los parámetros
		$objServiceConnection = BITAMServiceConnection::NewInstanceWithID($this->Repository, $this->ServiceConnectionID);
		if (is_null($objServiceConnection)) {
			return 0;
		}
		
		//Sin datos de conexión suficientes no puede obtener los valores del DataSource
		if (trim($objServiceConnection->ServiceName) == '' || trim($this->SourceDatabase) == '' || trim($this->SourceTable) == '') {
			return 0;
		}
		if (getMDVersion()>=esvTypeOfDatabase) {
			if ($objServiceConnection->Servicedbtype == ddrtSQLServer ){
				$type = 'mssql';
			}else {
				$type = 'mysql';
			}
		}else {
			$type = 'mysql';
		}				
		//Realiza la conexión a la Base de datos especificada
		ob_start();
		$objDBConnection = BITAMConnection::NewInstance($objServiceConnection->ServiceName, $objServiceConnection->User, $objServiceConnection->Password, $this->SourceDatabase, $type);
		if (is_null($objDBConnection) || is_null($objDBConnection->ADOConnection)) {
			return 0;
		}
		
		//En este caso se pudo establecer la conexión, así que continua con el Query de los datos solicitados
		/* Le indicamos a las conexiones que usaremos UTF8 (link http://stackoverflow.com/a/2159453) */
		$objDBConnection->ADOConnection->Execute("set names 'utf8'");
		
		$where = "";
		$arrfiels = array();
		$arrDataSourceMembersByIDs = array();
		$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->DataSourceID);
		if (is_null($objDataSourceMembersColl)) {
			return 0;
		}
		foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
			$arrDataSourceMembersByIDs[$objDataSourceMember->MemberID] = $objDataSourceMember;
		}
		
		//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		$strSpecialOrderFields = '';
		$arrfielsOrder = array();
		foreach ($arrayAttribIDs as $key => $value) {
			$intDataSourceMemberID = (int) @$arrayAttribIDs[$key]["sourceMemberID"];
			$objDataSourceMember = @$arrDataSourceMembersByIDs[$intDataSourceMemberID];
			if (is_null($objDataSourceMember)) {
				return 0;
			}
			
			//Se utilizará el FieldName como el Alias del campo para que el resto del proceso sea natural, pero se consulta a partir del MemberName
			//ya que es ahí donde se almacena el nombre real de la columna en la tabla origen
			$arrayAttribIDs[$key]["fieldName"] = $objDataSourceMember->FieldName;
			//$arrayAttribIDs[$key]["fieldName"] = ($objDataSourceMember->FieldName)?$objDataSourceMember->FieldName:$objDataSourceMember->MemberName;
			$arrfiels[] = $objDataSourceMember->MemberName." AS ".$arrayAttribIDs[$key]["fieldName"];
			//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			$arrfielsOrder[] = $objDataSourceMember->MemberName;
			if ((int) @$arrayAttribIDs[$key]["isOrder"]) {
				//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
				$strOrientation = '';
				if ((int) @$arrayAttribIDs[$key]["sortDir"] == ordbDsc) {
					$strOrientation = " DESC";
				}
				$strSpecialOrderFields .= $objDataSourceMember->MemberName.$strOrientation.', ';
				//@JAPR
			}
			//@JAPR
		}
		
		if(is_array($filter)){
			foreach ($filter as $key2 => $value2 ){
				$andArr = array();
				foreach ($filter[$key2] as $key3 => $value3) {
					$andArr[] =  "'{$value3}'";
				}
				
				$objDataSourceMember = @$arrDataSourceMembersByIDs[$key2];
				if ($objDataSourceMember) {
					$fieldName = $objDataSourceMember->FieldName;
					$whereArr[] = " {$fieldName} in (" . implode(', ', $andArr) . ")";
				}
			}
			$where = " WHERE " . implode(' AND ', $whereArr);
		}else if (trim($filter) != ''){
			$filter = trim($filter);
			$where = " WHERE " . $filter;
		}
		
		if ($withSecurity) {
			//@JAPR 2015-12-24: Corregido un bug, no estaba asignada la variable del usuario (#012P4U)
			$userID = (int) @$_SESSION["PABITAM_UserID"];
			//@JAPR
			$whereDatafilter =  BITAMDataSourceFilter::generateResultFilterByUser($this->Repository, $this->DataSourceID, $userID);
			if ($whereDatafilter!=""){
				$where = ($where==""? " WHERE " . $whereDatafilter : $where . " AND " . $whereDatafilter);
			}
		}
		$arrGroupby = array();
		
		if ($this->Type == dstyTable) {
			foreach ($arrfiels as $key => $value) {
				$pos = strpos($value, " AS ");
				if ($pos === false) {
					$arrGroupby[] = $value;
				}else{
					$arrGroupby[] = substr($value, 0,$pos);
				}
			}
			//@JRPP 2016-02-04: Actualizo el where cuando se trata de una fuente tipo tabla 
			if ($where != ""){
				foreach ($arrfiels as $key => $value) {
					$pos = strpos($value, " AS ");
					if (!$pos === false) {
						$oriRemplaza = substr($value, $pos + 4);
						$newRemplaza = substr($value, 0,$pos);
						$where=str_replace($oriRemplaza, $newRemplaza, $where);
					}
				}
			}
		}else {
			$arrGroupby = $arrfiels;
		}
		
		//@JAPR 2016-05-06: Agregado el ordenamiento de atributos, ya que se había dejado sin esa cláudula y se dependía del ORDER BY o bien el orden de creación de los campos/índice
		//según MySQL, y eventualmente se podrán reordenar atributos
		//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		$iTopValues = (int) $iTopValues;
		$strLimit = '';
		if ($iTopValues > 0) {
			$strLimit = " LIMIT {$iTopValues}";
		}
		
		//@JAPR 2016-12-12: Agregado el esquema de paginación para obtener valores (#U7J0XU)
		//Si se asignaron las propiedades de paginación, no podrían convivir con el límite como parámetro pues no son compatibles, así que tendrá precedencia la paginación, en cuyo
		//caso asigna el límite inicial y cantidad de rows a obtener (el limit inicia en 0)
		if ($this->EnablePagination && $this->TotalValuesPagesNum > 1) {
			$intStartingRow = ($this->CurrentValuesPage - 1) * $this->NumValuesPerPage;
			$strLimit = " LIMIT {$intStartingRow}, {$this->NumValuesPerPage}";
		}
		
		//@JAPR 2016-12-13: Agregado el esquema de paginación para obtener valores (#U7J0XU)
		//Para SQL Server el ORDER BY no puede existir si no es parte del SELECT o GROUP BY, así que para este caso que se requiere el COUNT no aplica
		$sql = "SELECT COUNT(*) AS TotalRowCount 
			FROM ". $this->SourceTable . $where .($withoutGroup?"":" GROUP BY " . implode(', ', $arrGroupby)).
			" ".$strLimit;
		//@JAPR
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nVerifying tablename: $this->TableName<br>\r\n");
			echo("<br>\r\nVerifying arrfiels: " . implode(', ', $arrfiels) . "<br>\r\n");
			echo("<br>\r\nVerifying where: $where<br>\r\n");
			echo("<br>\r\nVerifying sql: $sql<br>\r\n");
		}
		
		//@JAPR 2015-08-02: Corregido un bug, la función debe regresar un array vacio incluso en caso de error para no detener la ejecución de la descarga de definiciones 
		//@JAPR 2015-08-23: Validado que el fetchMode sólo aplique si se recibe el parámetro $iFetchMode
		if (!is_null($iFetchMode)) {
			$intCurrentFecthMode = $objDBConnection->ADOConnection->SetFetchMode($iFetchMode);
		}
		$aRS = $objDBConnection->ADOConnection->Execute($sql);
		if (!is_null($iFetchMode)) {
			$objDBConnection->ADOConnection->SetFetchMode($intCurrentFecthMode);
		}
		
		if ($aRS && !$aRS->EOF) {
			$intTotalRowsCount = (int) @$aRS->fields["totalrowcount"];
		}
		
		return $intTotalRowsCount;
	}
	//@JAPR
	
	/*Carga la lista de valores y regresa instancias de BITAMeBavelCatalogValue de todos los valores existentes
    El parámetro $bGetRecordset permite que se regrese sólo el RecordSet en lugar de la colección de valores
    El parámetro $bIncludeSystemFields permite recuperar también los campos de sistema como la fecha o el key de cada valor (sólo aplica si $bGetRecordset == true)
    //@JAPR 2014-05-28: Agregado el parámetro $bGetDistinctValues para que en el caso de catálogos mapeados de eBavel se obtengan los valores únicos
    para la combinación de atributos, ya que al ser formas de captura de eBavel y no necesariamente catálogos reales, se pueden llegar a repetir
    (esto es especialmente útil al momento de descargar las definiciones a los móviles para reducir el tiempo)
    //@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
    Agregado el parámetro $filter para permitir especificar un Filtro SQL directo al momento de cargar los valores (no se debe combinar
	//@JAPRWarning 2015-12-22: Corregido un bug, esta función en DataSource NO estaba estandarizada, se creó completamente dispareja en cuanto a los
	parámetros que recibe de su contraparte en BITAMCatalog/BITAMCatalogFilter, pero se seguían enviando parámetros que nadie leía, así que se hará un primer
	intento por al menos reenviar correctamente los parámetros que si son usados, aunque eso dejará a BITAMDataSource sin la funcionalidad
	original de BITAMCatalog, la cual básicamente era para edición de capturas. El parámetro $bError será el único que se respete porque ese
	era independiente del proceso de edición de valores, mientras que se asumirá que $bGetDistinctValues vendría siempre en false ya que
	BITAMDataSource lo trata de esa manera (@JAPRWarning, eso es un error, no está optimizado eBavel, de tal manera que el proceso es innecesariamente
	lento al no agrupar los registros de la consulta con eBavel). Se agregó el parámetro $bError. 
	//@JAPR 2015-12-22: Se removieron los parámetros #4, #5 y #6 $entryID, $entrySurveyID y $bJustGetHistoricEntryData ya que no se usaban
	//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
	Agregado el parámetro $arrOrderedAttribIDs con los IDs de atributos que se deben ordenar previo al ordenamiento default (se agregaron como parámetro, dado a que el $arrayAttribIDs
	no contiene la definición de los CatMembers como lo hace en la función getAttributeValues, sino sólo el ID, así que para no forzar a otro Join con SI_SV_Question sólo para saber que
	DataMembers estarían asociados a preguntas como atributo orden - además que en ese caso si podría haber múltiples DataMembers que funcionan de esa manera - simplemente se recibirá
	ya la lista de por qué atributo se debe ordenar el query)
	//@JAPR 2018-07-27: Agregado el parámetro $inclueidValue que no se había integrado originalmente, aunque negado porque como se agregó a destiempo no se puede
	usar el mismo default que para las otras funciones de extracción de datos ya que el código funcionaba con este valor siempre en true (#2PB40B)
	*/
    function getCatalogValues($arrayAttribIDs = null, $aUserID = -1, $filter="", $bGetRecordset = false, $bGetDistinctValues = false, $bIncludeSystemFields = false, $iTopValues = -1, &$bError = false, $arrOrderedAttribIDs = null, $inclueidValue = true)
    {
		global $appSettings;
		//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		//@JAPR 2015-03-11: A la fecha de implementación, eBavel Si reportaba los errores y de hecho detenía la ejecución del producto,
		//lo cual no era deseable, mientras que los catálogos de eForms no reportaban los errores y dejaban continuar, esto tampoco es muy bueno pero
		//por lo menos no detenía al App. Se modificará para que eBavel utilice una variable global cuando se estén obteniendo los valores de los
		//catálogos para las agendas, de tal manera que no truene eBavel sino que regrese indicación de error, y con este nuevo parámetro se
		//decidirá si hay o no error para saber si se debe usar la colección de valores vacia o si simplemente no se debe incluir. Los errores que
		//reportaba eBavel no incluían errores por no tener las clases necesarias, así que se modificará también la respuesta en esos casos, bajo
		//el entendido que en caso de faltar algo importante de todas maneras fallará el código y detendrá la ejecución
		/* De momento lo relevante con las agendas se comenta pero posteriormente se adecuara */
		global $gbProcessingAgendaCatalogs;
		if (!isset($gbProcessingAgendaCatalogs)) {
			$gbProcessingAgendaCatalogs = false;
		}
		//@JAPR 2015-10-02: Validado que en caso de error no detenga la ejecución si se encuentra en modo diseño (#MBN30C)
		global $gblShowErrorSurvey;
		//@JAPR
		
		//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		if (is_null($arrOrderedAttribIDs) || !is_array($arrOrderedAttribIDs)) {
			$arrOrderedAttribIDs = array();
		}
		//@JAPR
		
		$objDataSource = $this;
		$aRepository = $objDataSource->Repository;
		$DataSourceID = $this->DataSourceID;
		$arrValues = array();
		require_once('settingsvariable.inc.php');
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'EBAVELURL');
		if (is_null($objSetting) || trim($objSetting->SettingValue == '')) {
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:$arrValues;//$anEmptyInstance;
			//@JAPR
		}
		//Verica si el código de eBavel está disponible
		$strPath = '';
		$streBavelPath = '';
		$arrURL = parse_url($objSetting->SettingValue);
		$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
		$strPath = str_replace('/', '\\', $strPath);
		$strPath = str_replace('\\\\', '\\', $strPath);
		if (file_exists($strPath))
		{
			@require_once($strPath);
			$streBavelPath = str_ireplace('service.php', '', $strPath);
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			if (!function_exists('NotifyError')) {
				echo("<br>\r\nNo NotifyError function");
			}
			else {
			    echo("<br>\r\nNotifyError function exists");
			}
		}
		
		if (!class_exists('Tables'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:$arrValues;//$anEmptyInstance;
			//@JAPR
		}
		if (!class_exists('DBForm'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:$arrValues;//$anEmptyInstance;
			//@JAPR
		}
		//Carga la definición de la forma de eBavel a utilizar
		$arreBavelFormColl = @GetEBavelSections($aRepository, false, $objDataSource->eBavelAppID, array($objDataSource->eBavelFormID));
		if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0) {
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:$arrValues;//$anEmptyInstance;
			//@JAPR
		}
		
		$objForm = $arreBavelFormColl[0];
		$strFormID = $objForm['id'];
		
		$arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objDataSource->eBavelAppID, array($objDataSource->eBavelFormID), null, false, '');
		$objCatalogMembersColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $DataSourceID);
		//@JAPR 2014-05-28: Modificado para no especificar explícitamente el campo id_FormID, porque mas abajo siempre se habilitan los campos de
		//sistema de eBavel y ahí está incluído, además que debido a la necesidad de agrupar por los campos, si se agrega a este array se hubiera
		//tenido que quitar antes de usarlo en el Group By para no agrupar por la clave única
		$arreBavelFieldIDsStr = array();               //array(0 => 'id_'.$strFormID);
		//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel
		//Este array contiene los campos en el orden del catálogo de eForms, ya que en ese se esperan
		$arreBavelOrderByFieldIDs = array();
		//Este array contiene los campos en el orden de eBavel, sólo se usa temporalmente para al final procesar los campos según el orden
		$arreBavelOrderByeBavelFieldIDs = array();
		//de eForms
		//@JAPR
		if (is_null($arrayAttribIDs)) {
			foreach($objCatalogMembersColl->Collection as $objCatalogMember){
				$arrayAttribIDs[] = $objCatalogMember->MemberID;
			}
		}
		$arreBavelFieldIDs = array();
		$arrAntes = array();
		foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
			if ($objCatalogMember->eBavelFieldID > 0 && in_array($objCatalogMember->MemberID, $arrayAttribIDs)) {
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				if ($objCatalogMember->eBavelFieldDataID > ebfdValue) {
					//En este caso el atributo que mapea a un campo tipo Geolocalización utilizará el id del campo, sin hacer referencia a cual de
					//las partes (Latitude o Longitude) especificamente utilizará, ya que los filtros de eBavel lo resuelven unificado
					//Se agregará un array para saber que valores se utilizaron de este campo
					if (!isset($arreBavelFieldIDs[$objCatalogMember->eBavelFieldID])) {
						$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID] = array();
					}
					$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID][] = $objCatalogMember->eBavelFieldDataID;
					$arrAntes[$objCatalogMember->MemberID] = $objCatalogMember->eBavelFieldID; 
				}
				else {
					$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID] = $objCatalogMember->eBavelFieldID;
					$arrAntes[$objCatalogMember->MemberID] = $objCatalogMember->eBavelFieldID; 
				}
				//@JAPR
			}
		}
		if (count($arreBavelFieldIDs) == 0) {
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:$arrValues;//$anEmptyInstance;
		}
		
		//Siempre el primer campo es el ID consecutivo único del valor
		$anBavelFieldIDsColl = array(array('id' => 'id_'.$strFormID, 'tagname' => 'widget_field_numeric', 'label' => 'id', 'section_id' => $objDataSource->eBavelFormID, 'appId' => $objDataSource->eBavelAppID, 'id_fieldform' => -1, 'key' => -1, 'formID' => $strFormID));
		/*@JRPP 2015-08-25: Agrego el ID a la lista de campos*/
		//@JAPR 2018-07-27: Agregado el parámetro $inclueidValue que no se había integrado originalmente, aunque negado porque como se agregó a destiempo no se puede
		//usar el mismo default que para las otras funciones de extracción de datos ya que el código funcionaba con este valor siempre en true (#2PB40B)
		if ( $inclueidValue ) {
			$arreBavelFieldIDsStr[] = 'id_'.$strFormID;
		}
		//@JAPR
		foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
			foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
				if (isset($arreBavelFieldIDs[$intFieldID])) {
					$arreBavelFieldIDsStr[$intFieldID] = $arrFieldData['id'];
					//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel
					$arreBavelOrderByeBavelFieldIDs[$intFieldID] = array('id' => $arrFieldData['id'], 'order' => 1);
					
					//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
					//Modificado para que no utilice el ID de campo sino que se agreguen tantos campos como atributos de eForms existan, sin
					//embargo para atributos que mapean campos Geolocation, hay que agregar al $arrFieldData la indicación del tipo de información
					//que se desea extraer (Latitude o Longitude)
					//$anBavelFieldIDsColl[$intFieldID] = $arrFieldData;
					if ($intQTypeID == qtpLocation) {
						//En este caso se trata de un campo tipo Geolocation, así que se tuvo que haber recibido un array si es que se mapeaaron
						//atributos divididos en sus componentes, de lo contrario era un valor directo de GPS
						$arrDataIDs = $arreBavelFieldIDs[$intFieldID];
						if (is_array($arrDataIDs)) {
							//Si eran atributos divididos, tiene que agregar ambos a la definición
							foreach ($arrDataIDs as $inteBavelFieldDataID) {
								$arrFieldData['dataID'] = $inteBavelFieldDataID;
								$anBavelFieldIDsColl[] = $arrFieldData;
							}
						}
					}
					else {
						$anBavelFieldIDsColl[] = $arrFieldData;
					}
					//@JAPR
				}
			}
		}
		
		//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel
		//El array de campos a ordenar se debe generar en el orden de eForms, no el de eBavel, así que se recorre el mismo array de eForms y se
		//actualiza la información de ordenamiento obtenida del array de campos usados en el query
		foreach ($arreBavelFieldIDs as $intFieldID => $arrDataIDs) {
			if (isset($arreBavelOrderByeBavelFieldIDs[$intFieldID])) {
				$arreBavelOrderByFieldIDs[$intFieldID] = $arreBavelOrderByeBavelFieldIDs[$intFieldID];
			}
		}
		
		//@JAPR 2014-05-20: Corregido un bug, para que eBavel use seguridad, necesita tener esta variable de sesión asignada
		//@JAPR 2016-07-06: Corregido un bug, para el acceso al API simplemente se sobrescriben las variables, condicionarlo de esta manera evitaba que se pudiera usar el
		//login de eForms independiente (sin pasar por KPIOnline) ya que ese no sobreescribe estas variables, así que quedaban mezcladas (#S36ANP)
		//if (is_null(@$_SESSION["BITAM_UserName"]) || trim($_SESSION["BITAM_UserName"]) == '') {
		//@session_register("BITAM_UserName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		//}
		//@JAPR
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n ANTES DEL ORDENAR arreBavelFieldIDsStr: ");
			PrintMultiArray($arreBavelFieldIDsStr);
		}
		//$aRS = DBForm::name($strFormID)->fields($arreBavelFieldIDsStr)->whereRaw("FFRMS_10026 = 'The Home Depot'")->get($aRepository->ADOConnection);
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
		$arreBavelFieldIDsStrordenado =  array();
		//@JAPR 2018-07-27: Agregado el parámetro $inclueidValue que no se había integrado originalmente, aunque negado porque como se agregó a destiempo no se puede
		//usar el mismo default que para las otras funciones de extracción de datos ya que el código funcionaba con este valor siempre en true (#2PB40B)
		if ( $inclueidValue ) {
			$arreBavelFieldIDsStrordenado[] = $arreBavelFieldIDsStr[0];
		}
		//@JAPR
		$arreBavelOrderByFieldIDs = array();
		//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		//Si se recibió el parámetro de campos con ordenamiento especial, los agrega antes de generar el orden propio de la consulta para que tengan preferencia
		if (count($arrOrderedAttribIDs) > 0) {
			foreach ($arrOrderedAttribIDs as $value) {
				//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
				//Si el valor del atributo es realmente un array, entonces se trata de una definición extendida que incluye una propiedad de ordenamiento específica
				$intDataSourceMemberID = $value;
				$intSortingDir = ordbAsc;
				if (is_array($intDataSourceMemberID)) {
					$intDataSourceMemberID = $value['id'];
					$intSortingDir = (int) @$value['sortDir'];
				}
				if (array_key_exists($intDataSourceMemberID, $arrAntes)){
					if (array_key_exists($arrAntes[$intDataSourceMemberID], $arreBavelFieldIDsStr)) {
						$arreBavelFieldIDsStrordenado[$arrAntes[$intDataSourceMemberID]]=$arreBavelFieldIDsStr[$arrAntes[$intDataSourceMemberID]];
						//Para el ordenamiento en eBavel, el campo order con valor 0|1 == ascendente, así que como $intSortingDir valdría 1 cuando es descendente, se hace 
						//invertida la asignación de la propiedad de ordenamiento de eBavel para mandar 2 == descendente
						$arreBavelOrderByFieldIDs[$arrAntes[$intDataSourceMemberID]] = array('id'=> $arreBavelFieldIDsStr[$arrAntes[$intDataSourceMemberID]], 'order'=> (($intSortingDir)?2:1));
					}
				}
				//@JAPR
			}
		}
		
		//@JRPP 2015-10-24: Ordenamos correctamente los atributos que pedimos en el query
		foreach ($arrayAttribIDs as $key => $value) {
			if (array_key_exists($value, $arrAntes)){
				//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
				//Si el elemento ya se había ordenado, no se debe volver a agregar para no cambiar la orientación del ordenamiento
				if (array_key_exists($arrAntes[$value], $arreBavelFieldIDsStr) && !isset($arreBavelFieldIDsStrordenado[$arrAntes[$value]])) {
					$arreBavelFieldIDsStrordenado[$arrAntes[$value]]=$arreBavelFieldIDsStr[$arrAntes[$value]];
					$arreBavelOrderByFieldIDs[$arrAntes[$value]] = array('id'=> $arreBavelFieldIDsStr[$arrAntes[$value]], 'order'=> 1);
				}
				//@JAPR
			}
		}
		$arreBavelFieldIDsStr = $arreBavelFieldIDsStrordenado;
		//2015-10-24@JRPP 
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n DESPUES DE ORDENAR arreBavelFieldIDsStr: ");
			PrintMultiArray($arreBavelFieldIDsStr);
		}
		$objQuery = DBForm::name($strFormID)->fields($arreBavelFieldIDsStr);
		
		//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
		//Agregado el parámetro $filter para permitir especificar un Filtro SQL directo al momento de cargar los valores (no se debe combinar
		if (trim($filter) != '') {
			$objQuery->whereRaw($filter);
		}
		
		//@JAPR 2014-05-28: Agregado el parámetro $bGetDistinctValues para que en el caso de catálogos mapeados de eBavel se obtengan los valores únicos
		if ($bGetDistinctValues) {
			@$objQuery->groupBy($arreBavelFieldIDsStr);
		}
		//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel cuando no estaban
		//grabados consecutivamente
		@$objQuery->orderBy($arreBavelOrderByFieldIDs);
		//@JAPR
		//$objQuery->onlyFields(false);
		//Si sólo se pedirá el RecordSet, hay que validar si se incluyen o no los campos de sistema
		//if ($bGetRecordset) {
		$objQuery->onlyFields(!$bIncludeSystemFields);
		//}
		if ($iTopValues > 0) {
			$objQuery->limit = $iTopValues;
		}
		
		//@JAPR 2016-12-12: Agregado el esquema de paginación para obtener valores (#U7J0XU)
		//Si se asignaron las propiedades de paginación, no podrían convivir con el límite como parámetro pues no son compatibles, así que tendrá precedencia la paginación, en cuyo
		//caso asigna el límite inicial y cantidad de rows a obtener (el limit inicia en 0)
		$blnExecuteDBFormQuery = true;
		if ($this->EnablePagination && $this->TotalValuesPagesNum > 1) {
			if (method_exists($objQuery, 'pageSize') && method_exists($objQuery, 'currentPage')) {
				//Usa el método directo para asegurar el resultado correcto
				$objQuery->pageSize($this->NumValuesPerPage);
				$objQuery->currentPage($this->CurrentValuesPage);
			}
			else {
				//Modifica el query para aplicar el LIMIT OFFSET, COUNT
				if (method_exists($objQuery, 'compileQuery')) {
					//@JAPR 2014-08-19: Agregada esta asignación por compatibilidad con eBavel a partir de cambios de esta fecha
					//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
					//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
					//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
					//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
					//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
					global $eBavelServiceVersion;
					global $theUser;
					if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
						$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
						Metadata::setRepository($aneBavelRepository);
					}
					else {
						if (class_exists('Metadata')) {
							@Metadata::setRepository($aRepository);
						}
					}
					//@JAPR
					
					$sql = (string) @$objQuery->compileQuery($aRepository->ADOConnection);
					global $queriesLogFile;
					$aLogString="\r\n--------------------------------------------------------------------------------------";
					$aLogString.="\r\nBITAMDataSource::getCatalogValues: \r\n";
					$aLogString.=$sql;
					@updateQueriesLogFile($queriesLogFile,$aLogString);
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						if ($filter) {
							echo("<br>\r\nBITAMDataSource::getCatalogValues ({$DataSourceID}) Filter:\r\n{$filter}<br>\r\n");
						}
						echo("<br>\r\nBITAMDataSource::getCatalogValues ({$DataSourceID}):\r\n{$sql}<br>\r\n");
					}
					
					$intStartingRow = ($this->CurrentValuesPage - 1) * $this->NumValuesPerPage;
					$strLimit = " LIMIT {$intStartingRow}, {$this->NumValuesPerPage}";
					
					$sql .= " ".$strLimit;
					//Exclusivamente la parte que resuelve el query debe ser asociativa, así que eBavel en el método get internamente lo cambia, pero como eForms lo puso numérico porque
					//todos los demás métodos de eBavel así lo requieren, y como en este punto no está pasando por el método get, hay que ponerlo asociativo temporalmente y luego
					//regresarlo a numérico de nuevo (aunque inmediatamente se volverá a poner asociativo porque así es como lo requiere eForms, pero se hace de esta manera por consistencia
					//con el método get de eBavel que es lo que habría hecho)
					$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
					$aRS = $aRepository->ADOConnection->Execute($sql);
					$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
					$objQuery->sLastErrorMsg = $aRepository->ADOConnection->ErrorMsg();
					$objQuery->intLastErrorNo = $aRepository->ADOConnection->ErrorNo();
					$blnExecuteDBFormQuery = false;
				}
			}
		}
		
		//@JAPR 2016-12-12: Agregado el esquema de paginación para obtener valores (#U7J0XU)
		//Cuando no hay modo de paginación (como por ejemplo cuando se obtienen los valores para las definiciones del App o filtros dinámicos) entonces se utilizan los métodos
		//directos de eBavel tal como siempre se había hecho, lo mismo si se encuentra en la versión de eBavel que soporta paginación (eBavel 6+), de lo contrario se utilizará
		//un query directo tal como el método get lo realizaba a la fecha de esta implementación porque había que forzar al LIMIT de manera manual
		if ($blnExecuteDBFormQuery) {
			$sql = '';
			if (method_exists($objQuery, 'compileQuery')) {
				//@JAPR 2014-08-19: Agregada esta asignación por compatibilidad con eBavel a partir de cambios de esta fecha
				//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
				//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
				//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
				//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
				//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
				global $eBavelServiceVersion;
				global $theUser;
				if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
					$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
					Metadata::setRepository($aneBavelRepository);
				}
				else {
					if (class_exists('Metadata')) {
						@Metadata::setRepository($aRepository);
					}
				}
				//@JAPR
				
				$sql = (string) @$objQuery->compileQuery($aRepository->ADOConnection);
				global $queriesLogFile;
				$aLogString="\r\n--------------------------------------------------------------------------------------";
				$aLogString.="\r\nBITAMDataSource::getCatalogValues: \r\n";
				$aLogString.=$sql;
				@updateQueriesLogFile($queriesLogFile,$aLogString);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					if ($filter) {
						echo("<br>\r\nBITAMDataSource::getCatalogValues ({$DataSourceID}) Filter:\r\n{$filter}<br>\r\n");
					}
					echo("<br>\r\nBITAMDataSource::getCatalogValues ({$DataSourceID}):\r\n{$sql}<br>\r\n");
				}
			}
			
			$aRS = $objQuery->get($aRepository->ADOConnection);
		}
		//@JAPR
		
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		if ($aRS === false)
		{
			//@JAPR 2014-09-12: Corregido un bug, se estaba reportando el error con la conexión equivocada
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			//@JAPR 2015-10-02: Validado que en caso de error no detenga la ejecución si se encuentra en modo diseño (#MBN30C)
			if ($gbProcessingAgendaCatalogs || !$gblShowErrorSurvey) {
				return false;
			}
			else {
				die("(".__METHOD__.") ".translate("Error accessing")." ".$strFormID." ".translate("form").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			//@JAPR
		}
		
		//Si solo se pidió el recordset, entonces se regresa directamente para que lo procese quien invoca al método, pero no se puede usar así
		//como parte del FrameWork
		
		if ($bGetRecordset) {
			return array('rs'=> $aRS, 'id'=> 'id_'.$strFormID);
		}
		
		$anInstance = new BITAMeBavelCatalogValueCollection($aRepository, $DataSourceID, $bApplyFilters, $filter, $iTopValues);
		while (!$aRS->EOF) {
			$anObject = BITAMeBavelCatalogValue::NewInstanceFromRS($anInstance->Repository, $aRS, $DataSourceID, $anBavelFieldIDsColl);
			$anInstance->Collection[] = $anObject;
			$aRS->MoveNext();
		}
		
		return $anInstance;
    }
	
	//@JAPR 2016-12-12: Agregado el esquema de paginación para obtener valores (#U7J0XU)
	/* Obtiene la cantidad de registros que existen en una forma de eBavel */
    function getCatalogValuesCount($arrayAttribIDs = null, $aUserID = -1, $filter="", $bGetRecordset = false, $bGetDistinctValues = false, $bIncludeSystemFields = false, $iTopValues = -1, &$bError = false, $arrOrderedAttribIDs = null)
    {
		global $appSettings;
		global $gbProcessingAgendaCatalogs;
		if (!isset($gbProcessingAgendaCatalogs)) {
			$gbProcessingAgendaCatalogs = false;
		}
		//@JAPR 2015-10-02: Validado que en caso de error no detenga la ejecución si se encuentra en modo diseño (#MBN30C)
		global $gblShowErrorSurvey;
		//@JAPR
		
		//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		if (is_null($arrOrderedAttribIDs) || !is_array($arrOrderedAttribIDs)) {
			$arrOrderedAttribIDs = array();
		}
		//@JAPR
		
		$intTotalRowsCount = 0;
		$objDataSource = $this;
		$aRepository = $objDataSource->Repository;
		$DataSourceID = $this->DataSourceID;
		$arrValues = array();
		require_once('settingsvariable.inc.php');
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'EBAVELURL');
		if (is_null($objSetting) || trim($objSetting->SettingValue == '')) {
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:0;
			//@JAPR
		}
		//Verica si el código de eBavel está disponible
		$strPath = '';
		$streBavelPath = '';
		$arrURL = parse_url($objSetting->SettingValue);
		$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
		$strPath = str_replace('/', '\\', $strPath);
		$strPath = str_replace('\\\\', '\\', $strPath);
		if (file_exists($strPath))
		{
			@require_once($strPath);
			$streBavelPath = str_ireplace('service.php', '', $strPath);
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			if (!function_exists('NotifyError')) {
				echo("<br>\r\nNo NotifyError function");
			}
			else {
			    echo("<br>\r\nNotifyError function exists");
			}
		}
		
		if (!class_exists('Tables'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:0;
			//@JAPR
		}
		if (!class_exists('DBForm'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:0;
			//@JAPR
		}
		//Carga la definición de la forma de eBavel a utilizar
		$arreBavelFormColl = @GetEBavelSections($aRepository, false, $objDataSource->eBavelAppID, array($objDataSource->eBavelFormID));
		if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0) {
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:0;
			//@JAPR
		}
		
		$objForm = $arreBavelFormColl[0];
		$strFormID = $objForm['id'];
		
		$arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objDataSource->eBavelAppID, array($objDataSource->eBavelFormID), null, false, '');
		$objCatalogMembersColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $DataSourceID);
		//@JAPR 2014-05-28: Modificado para no especificar explícitamente el campo id_FormID, porque mas abajo siempre se habilitan los campos de
		//sistema de eBavel y ahí está incluído, además que debido a la necesidad de agrupar por los campos, si se agrega a este array se hubiera
		//tenido que quitar antes de usarlo en el Group By para no agrupar por la clave única
		$arreBavelFieldIDsStr = array();               //array(0 => 'id_'.$strFormID);
		//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel
		//Este array contiene los campos en el orden del catálogo de eForms, ya que en ese se esperan
		$arreBavelOrderByFieldIDs = array();
		//Este array contiene los campos en el orden de eBavel, sólo se usa temporalmente para al final procesar los campos según el orden
		$arreBavelOrderByeBavelFieldIDs = array();
		//de eForms
		//@JAPR
		if (is_null($arrayAttribIDs)) {
			foreach($objCatalogMembersColl->Collection as $objCatalogMember){
				$arrayAttribIDs[] = $objCatalogMember->MemberID;
			}
		}
		$arreBavelFieldIDs = array();
		$arrAntes = array();
		foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
			if ($objCatalogMember->eBavelFieldID > 0 && in_array($objCatalogMember->MemberID, $arrayAttribIDs)) {
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				if ($objCatalogMember->eBavelFieldDataID > ebfdValue) {
					//En este caso el atributo que mapea a un campo tipo Geolocalización utilizará el id del campo, sin hacer referencia a cual de
					//las partes (Latitude o Longitude) especificamente utilizará, ya que los filtros de eBavel lo resuelven unificado
					//Se agregará un array para saber que valores se utilizaron de este campo
					if (!isset($arreBavelFieldIDs[$objCatalogMember->eBavelFieldID])) {
						$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID] = array();
					}
					$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID][] = $objCatalogMember->eBavelFieldDataID;
					$arrAntes[$objCatalogMember->MemberID] = $objCatalogMember->eBavelFieldID; 
				}
				else {
					$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID] = $objCatalogMember->eBavelFieldID;
					$arrAntes[$objCatalogMember->MemberID] = $objCatalogMember->eBavelFieldID; 
				}
				//@JAPR
			}
		}
		if (count($arreBavelFieldIDs) == 0) {
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:0;
		}
		
		//Siempre el primer campo es el ID consecutivo único del valor
		$anBavelFieldIDsColl = array(array('id' => 'id_'.$strFormID, 'tagname' => 'widget_field_numeric', 'label' => 'id', 'section_id' => $objDataSource->eBavelFormID, 'appId' => $objDataSource->eBavelAppID, 'id_fieldform' => -1, 'key' => -1, 'formID' => $strFormID));
		/*@JRPP 2015-08-25: Agrego el ID a la lista de campos*/
		$arreBavelFieldIDsStr[] = 'id_'.$strFormID;
		foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
			foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
				if (isset($arreBavelFieldIDs[$intFieldID])) {
					$arreBavelFieldIDsStr[$intFieldID] = $arrFieldData['id'];
					//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel
					$arreBavelOrderByeBavelFieldIDs[$intFieldID] = array('id' => $arrFieldData['id'], 'order' => 1);
					
					//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
					//Modificado para que no utilice el ID de campo sino que se agreguen tantos campos como atributos de eForms existan, sin
					//embargo para atributos que mapean campos Geolocation, hay que agregar al $arrFieldData la indicación del tipo de información
					//que se desea extraer (Latitude o Longitude)
					//$anBavelFieldIDsColl[$intFieldID] = $arrFieldData;
					if ($intQTypeID == qtpLocation) {
						//En este caso se trata de un campo tipo Geolocation, así que se tuvo que haber recibido un array si es que se mapeaaron
						//atributos divididos en sus componentes, de lo contrario era un valor directo de GPS
						$arrDataIDs = $arreBavelFieldIDs[$intFieldID];
						if (is_array($arrDataIDs)) {
							//Si eran atributos divididos, tiene que agregar ambos a la definición
							foreach ($arrDataIDs as $inteBavelFieldDataID) {
								$arrFieldData['dataID'] = $inteBavelFieldDataID;
								$anBavelFieldIDsColl[] = $arrFieldData;
							}
						}
					}
					else {
						$anBavelFieldIDsColl[] = $arrFieldData;
					}
					//@JAPR
				}
			}
		}
		
		//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel
		//El array de campos a ordenar se debe generar en el orden de eForms, no el de eBavel, así que se recorre el mismo array de eForms y se
		//actualiza la información de ordenamiento obtenida del array de campos usados en el query
		foreach ($arreBavelFieldIDs as $intFieldID => $arrDataIDs) {
			if (isset($arreBavelOrderByeBavelFieldIDs[$intFieldID])) {
				$arreBavelOrderByFieldIDs[$intFieldID] = $arreBavelOrderByeBavelFieldIDs[$intFieldID];
			}
		}
		
		//@JAPR 2014-05-20: Corregido un bug, para que eBavel use seguridad, necesita tener esta variable de sesión asignada
		//@JAPR 2016-07-06: Corregido un bug, para el acceso al API simplemente se sobrescriben las variables, condicionarlo de esta manera evitaba que se pudiera usar el
		//login de eForms independiente (sin pasar por KPIOnline) ya que ese no sobreescribe estas variables, así que quedaban mezcladas (#S36ANP)
		//if (is_null(@$_SESSION["BITAM_UserName"]) || trim($_SESSION["BITAM_UserName"]) == '') {
		//@session_register("BITAM_UserName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		//}
		//@JAPR
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
		$arreBavelFieldIDsStrordenado =  array();
		$arreBavelFieldIDsStrordenado[] = $arreBavelFieldIDsStr[0];
		$arreBavelOrderByFieldIDs = array();
		//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		//Si se recibió el parámetro de campos con ordenamiento especial, los agrega antes de generar el orden propio de la consulta para que tengan preferencia
		if (count($arrOrderedAttribIDs) > 0) {
			foreach ($arrOrderedAttribIDs as $value) {
				//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
				//Si el valor del atributo es realmente un array, entonces se trata de una definición extendida que incluye una propiedad de ordenamiento específica
				$intDataSourceMemberID = $value;
				$intSortingDir = ordbAsc;
				if (is_array($intDataSourceMemberID)) {
					$intDataSourceMemberID = $value['id'];
					$intSortingDir = (int) @$value['sortDir'];
				}
				if (array_key_exists($intDataSourceMemberID, $arrAntes)){
					if (array_key_exists($arrAntes[$intDataSourceMemberID], $arreBavelFieldIDsStr)) {
						$arreBavelFieldIDsStrordenado[$arrAntes[$intDataSourceMemberID]]=$arreBavelFieldIDsStr[$arrAntes[$intDataSourceMemberID]];
						//Para el ordenamiento en eBavel, el campo order con valor 0|1 == ascendente, así que como $intSortingDir valdría 1 cuando es descendente, se hace 
						//invertida la asignación de la propiedad de ordenamiento de eBavel para mandar 2 == descendente
						$arreBavelOrderByFieldIDs[$arrAntes[$intDataSourceMemberID]] = array('id'=> $arreBavelFieldIDsStr[$arrAntes[$intDataSourceMemberID]], 'order'=> (($intSortingDir)?2:1));
					}
				}
				//@JAPR
			}
		}
		
		//@JRPP 2015-10-24: Ordenamos correctamente los atributos que pedimos en el query
		foreach ($arrayAttribIDs as $key => $value) {
			if (array_key_exists($value, $arrAntes)){
				//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
				//Si el elemento ya se había ordenado, no se debe volver a agregar para no cambiar la orientación del ordenamiento
				if (array_key_exists($arrAntes[$value], $arreBavelFieldIDsStr) && !isset($arreBavelFieldIDsStrordenado[$arrAntes[$value]])) {
					$arreBavelFieldIDsStrordenado[$arrAntes[$value]]=$arreBavelFieldIDsStr[$arrAntes[$value]];
					$arreBavelOrderByFieldIDs[$arrAntes[$value]] = array('id'=> $arreBavelFieldIDsStr[$arrAntes[$value]], 'order'=> 1);
				}
				//@JAPR
			}
		}
		$arreBavelFieldIDsStr = $arreBavelFieldIDsStrordenado;
		//2015-10-24@JRPP 
		$objQuery = DBForm::name($strFormID)->fields($arreBavelFieldIDsStr);
		
		//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
		//Agregado el parámetro $filter para permitir especificar un Filtro SQL directo al momento de cargar los valores (no se debe combinar
		if (trim($filter) != '') {
			$objQuery->whereRaw($filter);
		}
		
		//@JAPR 2014-05-28: Agregado el parámetro $bGetDistinctValues para que en el caso de catálogos mapeados de eBavel se obtengan los valores únicos
		if ($bGetDistinctValues) {
			@$objQuery->groupBy($arreBavelFieldIDsStr);
		}
		//@JAPR 2014-11-10: Corregido un bug, no estaba agregando el ordenamiento así que repetía valores de la forma de eBavel cuando no estaban
		//grabados consecutivamente
		@$objQuery->orderBy($arreBavelOrderByFieldIDs);
		//@JAPR
		//$objQuery->onlyFields(false);
		//Si sólo se pedirá el RecordSet, hay que validar si se incluyen o no los campos de sistema
		//if ($bGetRecordset) {
		$objQuery->onlyFields(!$bIncludeSystemFields);
		//}
		if ($iTopValues > 0) {
			$objQuery->limit = $iTopValues;
		}
		
		$sql = '';
		//La función count del objeto de eBavel sólo está disponible a partir de eBavel 6.0, así que se optará por modificar el query regresado por compileQuery para reemplazar toda
		//la porción del SELECT por un SELECT COUNT(*) a partir de identificar el primer " FROM "
		if (method_exists($objQuery, 'count')) {
			//@JAPR 2018-06-22: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
			//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
			//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
			//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
			//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
			//@JAPR 2018-06-22: Corregido este bug que se presentó a partir del issue de eBavel #7O0OGI (#X39X6H)
			//La función "count" del objeto de eBavel internamente utiliza compileQuery, así que se debe hacer este proceso antes de invocarla
			global $eBavelServiceVersion;
			global $theUser;
			if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
				$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
				Metadata::setRepository($aneBavelRepository);
			}
			else {
				if (class_exists('Metadata')) {
					@Metadata::setRepository($aRepository);
				}
			}
			//@JAPR
			
			//Usa el método directo para asegurar el resultado correcto
			$intTotalRowsCount = (int) @$objQuery->count($aRepository->ADOConnection);
			$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		}
		else {
			//Modifica el query para obtener el SELECT COUNT(*)
			if (method_exists($objQuery, 'compileQuery')) {
				//@JAPR 2014-08-19: Agregada esta asignación por compatibilidad con eBavel a partir de cambios de esta fecha
				//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
				//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
				//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
				//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
				//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
				global $eBavelServiceVersion;
				global $theUser;
				if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
					$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
					Metadata::setRepository($aneBavelRepository);
				}
				else {
					if (class_exists('Metadata')) {
						@Metadata::setRepository($aRepository);
					}
				}
				//@JAPR
				
				$sql = (string) @$objQuery->compileQuery($aRepository->ADOConnection);
				global $queriesLogFile;
				$aLogString="\r\n--------------------------------------------------------------------------------------";
				$aLogString.="\r\nBITAMDataSource::getCatalogValuesCount: \r\n";
				$aLogString.=$sql;
				@updateQueriesLogFile($queriesLogFile,$aLogString);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					if ($filter) {
						echo("<br>\r\nBITAMDataSource::getCatalogValuesCount ({$DataSourceID}) Filter:\r\n{$filter}<br>\r\n");
					}
					echo("<br>\r\nBITAMDataSource::getCatalogValuesCount ({$DataSourceID}):\r\n{$sql}<br>\r\n");
				}
				
				$intPos = mb_stripos($sql, "\nFROM ");
				$sql = "SELECT COUNT(*) AS TotalRowCount ".mb_substr($sql, $intPos);
				$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					$intTotalRowsCount = (int) @$aRS->fields["totalrowcount"];
				}
			}
		}
		
		return $intTotalRowsCount;
    }
	
	//@JAPR 2018-05-03: Optimizada la carga de eForms (#G87C4W)
	/* Obtiene la fecha de última modificación de un catálogo de tipo eBavel Exclusivamente, buscando el campo modifiedDate de sistema que debería tener toda forma
	de eBavel. Originalmente utilizado para optimizar los accesos a los catálogos de eBavel manejando esta fecha como un número de versión equivalente al número
	de versión de los catálogos estáticos de eForms. DAbdo determinó el día de esta implementación que sólo se soportarían casos simples donde el catálogos de eBavel
	está formado exclusivamente por campos que pertenecen a dicha forma, no por campos que requieren joins con tablas adicionales (otros catálogos de eBavel o formas
	detalle) que pudieran cambiar su valor sin que la tabla maestra sea modificada, por tanto esta función determinará la última fecha de modificación aplicable para
	el usuario que consulta incluyendo los filtros tanto de eForms como de eBavel o cualquiera externo recibido.
	Se maneja similar a la función getCatalogValues, su resultado puede ser un objeto que contenga información adicional como los filtros exactos aplicados, o bien
	simplemente el recordset con el resultado del MAX a la fecha de modificación
	*/
	function getCatalogValuesLastModifiedDate($aUserID = -1, $filter="")
	{
		global $appSettings;
		//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		//@JAPR 2015-03-11: A la fecha de implementación, eBavel Si reportaba los errores y de hecho detenía la ejecución del producto,
		//lo cual no era deseable, mientras que los catálogos de eForms no reportaban los errores y dejaban continuar, esto tampoco es muy bueno pero
		//por lo menos no detenía al App. Se modificará para que eBavel utilice una variable global cuando se estén obteniendo los valores de los
		//catálogos para las agendas, de tal manera que no truene eBavel sino que regrese indicación de error, y con este nuevo parámetro se
		//decidirá si hay o no error para saber si se debe usar la colección de valores vacia o si simplemente no se debe incluir. Los errores que
		//reportaba eBavel no incluían errores por no tener las clases necesarias, así que se modificará también la respuesta en esos casos, bajo
		//el entendido que en caso de faltar algo importante de todas maneras fallará el código y detendrá la ejecución
		/* De momento lo relevante con las agendas se comenta pero posteriormente se adecuara */
		global $gbProcessingAgendaCatalogs;
		if (!isset($gbProcessingAgendaCatalogs)) {
			$gbProcessingAgendaCatalogs = false;
		}
		//@JAPR 2015-10-02: Validado que en caso de error no detenga la ejecución si se encuentra en modo diseño (#MBN30C)
		global $gblShowErrorSurvey;
		//@JAPR
		
		$objDataSource = $this;
		$aRepository = $objDataSource->Repository;
		$DataSourceID = $this->DataSourceID;
		require_once('settingsvariable.inc.php');
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'EBAVELURL');
		if (is_null($objSetting) || trim($objSetting->SettingValue == '')) {
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:null;
			//@JAPR
		}
		//Verica si el código de eBavel está disponible
		$strPath = '';
		$streBavelPath = '';
		$arrURL = parse_url($objSetting->SettingValue);
		$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
		$strPath = str_replace('/', '\\', $strPath);
		$strPath = str_replace('\\\\', '\\', $strPath);
		if (file_exists($strPath))
		{
			@require_once($strPath);
			$streBavelPath = str_ireplace('service.php', '', $strPath);
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			if (!function_exists('NotifyError')) {
				echo("<br>\r\nNo NotifyError function");
			}
			else {
			    echo("<br>\r\nNotifyError function exists");
			}
		}
		
		if (!class_exists('Tables'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:null;
			//@JAPR
		}
		if (!class_exists('DBForm'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:null;
			//@JAPR
		}
		//Carga la definición de la forma de eBavel a utilizar
		$arreBavelFormColl = @GetEBavelSections($aRepository, false, $objDataSource->eBavelAppID, array($objDataSource->eBavelFormID));
		if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0) {
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			return ($gbProcessingAgendaCatalogs)?false:null;
			//@JAPR
		}
		
		$objForm = $arreBavelFormColl[0];
		$strFormID = $objForm['id'];
		
		//Busca la referencia al campo de fecha de última modificación
		$arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objDataSource->eBavelAppID, array($objDataSource->eBavelFormID), null, false, '');
		$objModifiedDateFieldForm = null;
		foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
			foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
				if ( stripos($arrFieldData['id'], "modifiedDate") !== false ) {
					$objModifiedDateFieldForm = $arrFieldData;
					break;
				}
				if ( $objModifiedDateFieldForm ) {
					break;
				}
			}
		}
		
		//No tiene caso continuar si no se encontró el campo de fecha de última modificación
		if ( !$objModifiedDateFieldForm ) {
			return ($gbProcessingAgendaCatalogs)?false:null;
		}
		
		//@JAPR 2014-05-20: Corregido un bug, para que eBavel use seguridad, necesita tener esta variable de sesión asignada
		//@JAPR 2016-07-06: Corregido un bug, para el acceso al API simplemente se sobrescriben las variables, condicionarlo de esta manera evitaba que se pudiera usar el
		//login de eForms independiente (sin pasar por KPIOnline) ya que ese no sobreescribe estas variables, así que quedaban mezcladas (#S36ANP)
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		//@JAPR
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
		
		$arreBavelFieldIDsStr = array();
		$arreBavelFieldIDsStr[] = $objModifiedDateFieldForm['id'];
		$arreBavelFieldIDsStr[] = "MAX(".$objModifiedDateFieldForm['id'].")";
		$objQuery = DBForm::name($strFormID)->fields($arreBavelFieldIDsStr);
		
		//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
		//Agregado el parámetro $filter para permitir especificar un Filtro SQL directo al momento de cargar los valores (no se debe combinar
		if (trim($filter) != '') {
			$objQuery->whereRaw($filter);
		}
		
		//Los campos de sistema son requeridos porque precisamente ese es el que se está consultando
		$objQuery->onlyFields(!false);
		
		$sql = '';
		if (method_exists($objQuery, 'compileQuery')) {
			//@JAPR 2014-08-19: Agregada esta asignación por compatibilidad con eBavel a partir de cambios de esta fecha
			//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
			//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
			//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
			//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
			//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
			global $eBavelServiceVersion;
			global $theUser;
			if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
				$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
				Metadata::setRepository($aneBavelRepository);
			}
			else {
				if (class_exists('Metadata')) {
					@Metadata::setRepository($aRepository);
				}
			}
			//@JAPR
			
			$sql = (string) @$objQuery->compileQuery($aRepository->ADOConnection);
			global $queriesLogFile;
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMDataSource::getCatalogValuesLastModifiedDate: \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				if ($filter) {
					echo("<br>\r\nBITAMDataSource::getCatalogValuesLastModifiedDate ({$DataSourceID}) Filter:\r\n{$filter}<br>\r\n");
				}
				echo("<br>\r\nBITAMDataSource::getCatalogValuesLastModifiedDate ({$DataSourceID}):\r\n{$sql}<br>\r\n");
			}
		}
		
		//Se deben volver a configurar los campos a pedir con todo y agregados porque se detectó que el compileQuery está removiendo los agregados cuando se
		//ejecuta, así que se habría perdido el MAX configurado en este proceso
		$objQuery->fields($arreBavelFieldIDsStr);
		
		$aRS = $objQuery->get($aRepository->ADOConnection);
		
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		if ($aRS === false)
		{
			//@JAPR 2014-09-12: Corregido un bug, se estaba reportando el error con la conexión equivocada
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			//@JAPR 2015-10-02: Validado que en caso de error no detenga la ejecución si se encuentra en modo diseño (#MBN30C)
			if ($gbProcessingAgendaCatalogs || !$gblShowErrorSurvey) {
				return false;
			}
			else {
				//die("(".__METHOD__.") ".translate("Error accessing")." ".$strFormID." ".translate("form").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				return null;
			}
			//@JAPR
		}
		
		$strLastModifiedDate = null;
		if (!$aRS->EOF) {
			$strLastModifiedDate = (string) @$aRS->fields["MAX_".$objModifiedDateFieldForm['id']];
		}
		
		return $strLastModifiedDate;
	}
	
	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	//@JAPR 2013-02-11: Agregada la edición de datos históricos
	//Agregado el parámetro $bJustGetHistoricEntryData para cargar sólo los valores usados durante la captura especificada sin regresar también
	//los valores propios del catálogo en este momento. Esto es útil sólo para generar el array de valor de la captura ya que se usa una numeración
	//negativa y tiene que se consistente con el array usado durante la edición en el browser
	//@JAPR 2014-05-28: Agregado el parámetro $bGetDistinctValues para que en el caso de catálogos mapeados de eBavel se obtengan los valores únicos
	//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
	//@JAPR 2015-03-11: Agregado el parámetro de retorno $bError para indicar si existió o no algún error al cargar los valores, lo que básicamente
	//significa que falló el query. A la fecha de implementación, eBavel Si reportaba los errores y de hecho detenía la ejecución del producto,
	//lo cual no era deseable, mientras que los catálogos de eForms no reportaban los errores y dejaban continuar, esto tampoco es muy bueno pero
	//por lo menos no detenía al App. Se modificará para que eBavel utilice una variable global cuando se estén obteniendo los valores de los
	//catálogos para las agendas, de tal manera que no truene eBavel sino que regrese indicación de error, y con este nuevo parámetro se
	//decidirá si hay o no error para saber si se debe usar la colección de valores vacia o si simplemente no se debe incluir
	//Agregado el parámetro $iFetchMode para forzar a usar un FetchMode diferente, si no se especifica (null) no hará cambio alguno al actual
	//@JAPR 2015-08-23: Corregido un bug, la ventana de valores del catálogo no debe aplicar el filtro del usuario logeado
	/*El parámetro $exitType determinará el tipo de resultado y/o ventana que se espera. El default es 1 (esto es independiente de su se usa o no $bGetRecordset)
		$exitType == 1: Resultado en forma de árbol como se espera en las definiciones de catálogos del App
		$exitType == 2: Arreglo lineal de todos los valores de los atributos seleccionados
	*/
	/*El parámetro $withoutSecurity se usa cuando no deseas aplicar la seguridad definida en los catalagos tanto por usuarios como por roles
	//@JAPRWarning 2015-12-22: Corregido un bug, esta función en DataSource NO estaba estandarizada, se creó completamente dispareja en cuanto a los
	//parámetros que recibe de su contraparte en BITAMCatalog, pero se seguían enviando parámetros que nadie leía, así que se hará un primer
	//intento por al menos reenviar correctamente los parámetros que si son usados, aunque eso dejará a BITAMDataSource sin la funcionalidad
	//original de BITAMCatalog, la cual básicamente era para edición de capturas. El parámetro $bError será el único que se respete porque ese
	//era independiente del proceso de edición de valores, mientras que se asumirá que $bGetDistinctValues vendría siempre en false ya que
	//BITAMDataSource lo trata de esa manera (@JAPRWarning, eso es un error, no está optimizado eBavel, de tal manera que el proceso es innecesariamente
	//lento al no agrupar los registros de la consulta con eBavel). Se agregó el parámetro $bError. 
	//@JAPR 2015-12-22: Se removió el parámetro #5 $attribPosition, ya que no se usaba, fue un mal intento de simular BITAMCatalogFilter
	//@JAPR 2016-06-13: Agregada la configuración para limitar los resultados de los valores de catálogo en modo de diseño exclusivamente (#14KN4H)
	Agregado el parámetro $iTopValues para controlar la cantidad de registros a recuperar en el catálogo
	*/
	function getAttributeValues($arrayAttribIDs = null, $aUserID = -1, $filter="", $exitType = '', $bGetRecordset = false, $inclueidValue = false, $withoutGroup = false, $iFetchMode = null, $withSecurity = true, $bIncludeSystemFields = false, &$bError = false, $iTopValues = 0) {
		global $blnWebMode;
		if (!isset($blnWebMode)) {
			$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
		}
		//@JAPR 2016-06-13: Agregada la configuración para limitar los resultados de los valores de catálogo en modo de diseño exclusivamente (#14KN4H)
		global $gbDesignMode;
		//@JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
		//Verificará mediante una variable global si se encuentra o no asignando valores dinámicos, dado a que heredar los parámetros hasta este punto sería mas complicado. En caso de estar
		//resolviendo un filtro dinámico y si el DataSource es de tipo eBavel (sólo para esos casos aplicará) entonces no realizará el ajuste si la versión del App que realiza el request
		//es la que tiene que resolver las imágenes directo desde el servidor
		global $appVersion;
		global $gbGettingDynamicValues;
		
		if (!isset($gbGettingDynamicValues)) {
			$gbGettingDynamicValues = false;
		}
		elseif ($gbGettingDynamicValues) {
			if ($appVersion < esvDynAppImagesFromServer) {
				$gbGettingDynamicValues = false;
			}
		}
		
		$blnReturnFullServerImgPaths = $gbGettingDynamicValues;
		if ($this->Type != dstyeBavel) {
			$blnReturnFullServerImgPaths = false;
		}
		
		//@JAPR 2015-08-02: Corregido un bug, la función debe regresar un array vacio incluso en caso de error para no detener la ejecución de la descarga de definiciones 
		$attribValues = array();
		//@JAPR 2015-08-23: Asignado un default válido en caso de que no se reciba (Catalog->getAttributeValues)
		if ($exitType == '') {
			$exitType = '1';
		}
		
		$streBavelPath = '';
		$strFields = "";
		$arrIdxs = array();
		$arrLast = array();
		$arrfiels = array();
		$whereArr = array();
		$where = "";
		//GCRUZ 2016-04-13. Asignar $userID el id de usuario en la sesión sólo si $aUserID == -1
		if ($aUserID == -1)
			$userID = $_SESSION["PABITAM_UserID"];
		else
			$userID = $aUserID;
		//@JAPR 2015-07-18: Adaptada para funcionar tal como la recibe desde BITAMCatalog, esto NO debió ser así, se debe corregir
		$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->DataSourceID);
		/*@JRPP 2015-08-22 Si no obtenemos parametros de arrIDs se intuye que se quieren todos*/
		if (is_null($arrayAttribIDs)) {
			$arrayAttribIDs = array();
			foreach($objDataSourceMembersColl->Collection as $objDataSourceMember){
				$anObjectMember = array();
				$anObjectMember["DataSourceID"]= $objDataSourceMember->DataSourceID;
				$anObjectMember["sourceMemberID"]= $objDataSourceMember->MemberID;
				$anObjectMember["MemberName"]= $objDataSourceMember->MemberName;
				$anObjectMember["FieldName"]= $objDataSourceMember->FieldName;
				$anObjectMember["eBavelFieldID"]= $objDataSourceMember->eBavelFieldID;
				$anObjectMember["eBavelFieldDataID"]= $objDataSourceMember->eBavelFieldDataID;
				$arrayAttribIDs[] = $anObjectMember;
			}
		} else {
			$arrDataSourceMembersByIDs = array();
			foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
				$arrDataSourceMembersByIDs[$objDataSourceMember->MemberID] = $objDataSourceMember;
			}
			//@JAPR
			$blnAddHistoricData = false;
			foreach ($arrayAttribIDs as $key => $value) {
				$intDataSourceMemberID = (int) @$arrayAttribIDs[$key]["sourceMemberID"];
				$objDataSourceMember = @$arrDataSourceMembersByIDs[$intDataSourceMemberID];
				if (is_null($objDataSourceMember)) {
					return null;
				}
				
				$arrayAttribIDs[$key]["fieldName"] = $objDataSourceMember->FieldName;
				$arrayAttribIDs[$key]["eBavelFieldID"] = $objDataSourceMember->eBavelFieldID;
				$arrayAttribIDs[$key]["eBavelFieldDataID"] = $objDataSourceMember->eBavelFieldDataID;
				//@JAPR 2016-05-06: Corregido un bug, al agregar a este array en este punto los atributos, se estaban generando duplicados en el SELECT, ya que abajo se vuelven a agregar
				//para catálogos tipo eForms, que son los únicos que usan este array
				//$arrfiels[] = $arrayAttribIDs[$key]["fieldName"];
				//@JAPR
				$arrIdxs[$key] = -1;
				$arrLast[$key] = null;
			}
		}
		
		$lim = count($arrayAttribIDs);
		
		//@JAPR 2019-05-03: Modificado para mostrar valores dummy en los atributos en lugar de realizar la consulta real a los catálogos (#270TYE)
		if ( $gbDesignMode ) {
			$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'DISABLEPREVIEWDUMMIES');
			//Aplicará los dummies mientras no se deshabiliten mediante la configuración para forzar a usar datos de catálogo
			if ( is_null($objSetting) || !((int) trim($objSetting->SettingValue)) ) {
				//Se generarán 5 registros dummy, dado a que no existen tipos de dato en los atributos (a pesar que se podría intentar obtener el tipo de campo cuando son formas de eBavel)
				//simplemente se regresará la palabara dummy#n.m para cada registros en cada atributo, donde #n es el número de registro y .m es el número de atributo
				$attribValues[0] = BITAMDataSource::GetDummyFieldDataRecursive(1, 1, $lim);
				$attribValues[1] = BITAMDataSource::GetDummyFieldDataRecursive(2, 1, $lim);
				$attribValues[2] = BITAMDataSource::GetDummyFieldDataRecursive(3, 1, $lim);
				$attribValues[3] = BITAMDataSource::GetDummyFieldDataRecursive(4, 1, $lim);
				$attribValues[4] = BITAMDataSource::GetDummyFieldDataRecursive(5, 1, $lim);
				
				return $attribValues;
			}
		}
		//@JAPR
		
		//@JAPR 2016-06-13: Agregada la configuración para limitar los resultados de los valores de catálogo en modo de diseño exclusivamente (#14KN4H)
		require_once('settingsvariable.inc.php');
		//$iTopValues = 0; (esta variable es un parámetro, así que se recibe, sólo se inicializa si está en modo diseño)
		$strLimit = '';
		$iTopValues = (int) $iTopValues;
		if ($gbDesignMode && $iTopValues <= 0) {
			$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'DESIGNCATALOGROWSLIMIT');
			if (!is_null($objSetting) && trim((string) @$objSetting->SettingValue) != '') {
				$iTopValues = @$objSetting->SettingValue;
			}
			else {
				$iTopValues = DEFAULT_CATALOGVALUES_LIMIT;
			}
			$iTopValues = (int) $iTopValues;
		}
		if ($iTopValues > 0) {
			$strLimit = " LIMIT {$iTopValues}";
		}
		
		//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)
		//Si se asignaron las propiedades de paginación, no podrían convivir con el límite como parámetro pues no son compatibles, así que tendrá precedencia la paginación, en cuyo
		//caso asigna el límite inicial y cantidad de rows a obtener (el limit inicia en 0)
		if ($this->EnablePagination && $this->TotalValuesPagesNum > 1) {
			$intStartingRow = ($this->CurrentValuesPage - 1) * $this->NumValuesPerPage;
			$strLimit = " LIMIT {$intStartingRow}, {$this->NumValuesPerPage}";
		}
		
		//@JAPR 2015-12-22: Agregado el tipo de DataSource basado en Tabla
		if ($this->Type == dstyTable) {
			//Si se trata de un catálogo tipo Tabla, se utiliza un método diferente para obtener los valores ya que requiere una conexión externa
			//Se espera recibir el equivalente a un RecordSet con los datos del catálogo, si se recibe NULL se asume que ocurrió un error al obtenerlos
			//@JAPR 2016-06-13: Agregada la configuración para limitar los resultados de los valores de catálogo en modo de diseño exclusivamente (#14KN4H)
			$aRS = $this->getExternalCatalogValues($arrayAttribIDs, $aUserID, $filter, true, $inclueidValue, $withoutGroup, $iFetchMode, $withSecurity, $bIncludeSystemFields, $bError, $iTopValues);
		}
		elseif ($this->eBavelAppID > 0 && $this->eBavelFormID > 0) {
			//@JAPR 2014-12-18: Corregido un bug, para atributos de eBavel si hay que hacer un proceso extra ya que la ruta a regresar debe
			//ser absoluta, de lo contrario no encontrará la imagen relativa al servicio de eForms
			$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'EBAVELURL');
			if (!is_null($objSetting)) {
				$streBavelPath = trim((string) @$objSetting->SettingValue);
			}
			$arrFieldseBavelID = array();
			//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			$arrSpecialOrderFields = array();
			foreach ($arrayAttribIDs as $key => $value) {
				$arrFieldseBavelID [] = @$arrayAttribIDs[$key]["sourceMemberID"];
				//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				if ((int) @$arrayAttribIDs[$key]["isOrder"]) {
					//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
					if ((int) @$arrayAttribIDs[$key]["sortDir"] == ordbDsc) {
						$arrSpecialOrderFields[] = array('id' => (int) @$arrayAttribIDs[$key]["sourceMemberID"], 'sortDir' => ordbDsc);
					}
					else {
						$arrSpecialOrderFields[] = (int) @$arrayAttribIDs[$key]["sourceMemberID"];
					}
				}
				//@JAPR
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nVerifying lim: $lim<br>\r\n");
				echo("<br>\r\nVerifying arrFieldseBavelID: " . implode(', ', $arrFieldseBavelID) . "<br>\r\n");
			}
			if(is_array($filter)){
				//GCRUZ 2016-06-09. Filtros como arreglo. Bug en issue: MXVIZA
				$aSQL = "SELECT id_fieldform, id FROM si_forms_fieldsforms WHERE section_id = ".$this->eBavelFormID;
				$rsEbavelFields = $this->Repository->ADOConnection->Execute($aSQL);
				$arrEbavelFields = array();
				if ($rsEbavelFields && $rsEbavelFields->_numOfRows != 0)
				{
					while(!$rsEbavelFields->EOF)
					{
						$arrEbavelFields[$rsEbavelFields->fields['id_fieldform']] = $rsEbavelFields->fields['id'];
						$rsEbavelFields->MoveNext();
					}
				}
				foreach ($filter as $key2 => $value2 ){
					$andArr = array();
					foreach ($filter[$key2] as $key3 => $value3) {
						$andArr[] =  "'{$value3}'";
					}
					$thisMember = null;
					foreach ($objDataSourceMembersColl->Collection as $aObjDataSourceMember)
					{
						if ($aObjDataSourceMember->MemberID == $key2)
							$thisMember = $aObjDataSourceMember;
					}
					$fieldName = @$arrEbavelFields[$thisMember->eBavelFieldID];
					$whereArr[] = " {$fieldName} in (" . implode(', ', $andArr) . ")";
				}
				$where = implode(' AND ', $whereArr);
			}else if ($filter != ''){
				$filter = trim($filter);
				$where = $filter;
			}
			
			if ($withSecurity) {
				$whereDatafilter =  BITAMDataSourceFilter::generateResultFilterByUser($this->Repository, $this->DataSourceID, $userID);
				if ($whereDatafilter!=""){
					$where = ($where==""? $whereDatafilter : $where . " AND " . $whereDatafilter);
				}
			}
			
			//@JAPR 2015-12-22: Se removieron los parámetros #4, #5 y #6 $entryID, $entrySurveyID y $bJustGetHistoricEntryData ya que no se usaban
			//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			//Agregado el parámetro $arrOrderedAttribIDs con los IDs de atributos que se deben ordenar previo al ordenamiento default (se agregaron como parámetro, dado a que el $arrayAttribIDs
			//no contiene la definición de los CatMembers como lo hace en la función getAttributeValues, sino sólo el ID, así que para no forzar a otro Join con SI_SV_Question sólo para saber que
			//DataMembers estarían asociados a preguntas como atributo orden - además que en ese caso si podría haber múltiples DataMembers que funcionan de esa manera - simplemente se recibirá
			//ya la lista de por qué atributo se debe ordenar el query)
			//@JAPR 2016-06-13: Agregada la configuración para limitar los resultados de los valores de catálogo en modo de diseño exclusivamente (#14KN4H)
			//@JAPR 2019-04-30: Corregido un bug al utilizar variables de objetos del App en filtros dinámicos, el uso de comillas sencillas dentro de strings causaba errores de queries (#PDZMWE)
			$objeBavel = $this->getCatalogValues($arrFieldseBavelID, -1, ReplaceAppObjectVariablesForFilters($where), true, false, $bIncludeSystemFields, $iTopValues, $bError, $arrSpecialOrderFields);
			//@JRPP 2015-11-23 Validacion de objeto cuando por alguna razon no se obtienen datos de la consulta de ebavel
			if (!$objeBavel) {
				return null;
			}
			
			$aRS = $objeBavel['rs'];
			$fieldKey = $objeBavel['id'];
			
			$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $this->eBavelAppID, array($this->eBavelFormID), null, false, '');
			$arreBavelFieldIDs = array();
			foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
				foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
					$arreBavelFieldIDs[$intFieldID] = $arrFieldData;
				}
			}
			
			for($i=0; $i<$lim; $i++) {
		        //@JAPR 2015-06-17: Corregido un bug, estos valores sólo vienen si el catálogo es de eBavel, así que se validarán para no generar error
				$inteBavelFieldID = (int) @$arrayAttribIDs[$i]["eBavelFieldID"];
				$inteBavelFieldDataID = (int) @$arrayAttribIDs[$i]["eBavelFieldDataID"];
		        if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		            echo("<br>\r\n FieldID dimID: ".@$arrayAttribIDs[$i]["eBavelFieldID"]);
		            echo("<br>\r\n FieldID fieldName: ".@$arrayAttribIDs[$i]["fieldName"]);
		            echo("<br>\r\n inteBavelFieldID: {$inteBavelFieldID}");
		            echo("<br>\r\n inteBavelFieldDataID: {$inteBavelFieldDataID}");
		        }
		        //@JAPR
				$strSuffix = '';
				if ($inteBavelFieldDataID > ebfdValue) {
					switch ($inteBavelFieldDataID) {
						case ebfdLatitude:
							$strSuffix = '_L';
							break;
						case ebfdLongitude:
							$strSuffix = '_A';
							break;
					}
				}
				
				//@JAPR 2016-07-07: Corregido un bug, no se había estandarizado la lectura de campos eBavel tipo firma a como lo habían programado para la ventana de valores del catálogo,
				//así que aunque en la lista de atributos del catálogo si se veía como una ruta de imagen, al App (o cualquier proceso que usara esta función) le habría mandado el
				//valor del campo con las coordenadas en lugar de la ruta de la imagen (#PLAUHF)
				$objeBavelField = @$arrEBavelFormsFields[qtpSignature][$inteBavelFieldID];
				if (!is_null($objeBavelField)) {
					$strSuffix = '_U';
				}
				//@JAPR
				if ($inteBavelFieldID > 0 && isset($arreBavelFieldIDs[$inteBavelFieldID])) {
					$field = $arreBavelFieldIDs[$inteBavelFieldID]['id'];
					$arrayAttribIDs[$i]["fieldName"] = $field . $strSuffix;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n New FieldID fieldName: ".@$arrayAttribIDs[$i]["fieldName"]);
					}
				}
				else {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n inteBavelFieldID not found. eBavelFields list:");
						PrintMultiArray($arreBavelFieldIDs);
					}
				}
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n inteBavelFieldID: {$inteBavelFieldID}");
				echo("<br>\r\n inteBavelFieldDataID: {$inteBavelFieldDataID}");
			}
		} else {
			//Se trata de un catálogo de eForms
			$arrDataSourceMembersByIDs = array();
			foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
				$arrDataSourceMembersByIDs[$objDataSourceMember->MemberID] = $objDataSourceMember;
			}
			
			$blnAddHistoricData = false;
			//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			$strSpecialOrderFields = '';
			foreach ($arrayAttribIDs as $key => $value) {
				$intDataSourceMemberID = (int) @$arrayAttribIDs[$key]["sourceMemberID"];
				$objDataSourceMember = @$arrDataSourceMembersByIDs[$intDataSourceMemberID];
				if (is_null($objDataSourceMember)) {
					return null;
				}
				
				$arrayAttribIDs[$key]["fieldName"] = $objDataSourceMember->FieldName;
				$arrfiels[] = $arrayAttribIDs[$key]["fieldName"];
				$arrIdxs[$key] = -1;
				$arrLast[$key] = null;
				//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				if ((int) @$arrayAttribIDs[$key]["isOrder"]) {
					//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
					$strOrientation = '';
					if ((int) @$arrayAttribIDs[$key]["sortDir"] == ordbDsc) {
						$strOrientation = " DESC";
					}
					$strSpecialOrderFields .= $arrayAttribIDs[$key]["fieldName"].$strOrientation.', ';
					//@JAPR
				}
				//@JAPR
			}
			
			if(is_array($filter)){
				foreach ($filter as $key2 => $value2 ){
					$andArr = array();
					foreach ($filter[$key2] as $key3 => $value3) {
						$andArr[] =  "'{$value3}'";
					}
					$query = "SELECT fieldname FROM SI_SV_DATASOURCEmember WHERE memberID = ".$key2;
					$aRsFieldName = $this->Repository->DataADOConnection->Execute($query);
					if($aRsFieldName){
						$fieldName=$aRsFieldName->fields["fieldname"];
						$whereArr[] = " {$fieldName} in (" . implode(', ', $andArr) . ")";
					}
				}
				$where = " WHERE " . implode(' AND ', $whereArr);
			}else if ($filter != ''){
				$filter = trim($filter);
				$where = " WHERE " . $filter;
			}
			
			//@JAPR 2015-08-23: Corregido un bug, la ventana de valores del catálogo no debe aplicar el filtro del usuario logeado
			if ($withSecurity) {
				$whereDatafilter =  BITAMDataSourceFilter::generateResultFilterByUser($this->Repository, $this->DataSourceID, $userID);
				if ($whereDatafilter!=""){
					$where = ($where==""? " WHERE " . $whereDatafilter : $where . " AND " . $whereDatafilter);
				}
			}
			
			//@JAPR 2016-05-06: Agregado el ordenamiento de atributos, ya que se había dejado sin esa cláudula y se dependía del ORDER BY o bien el orden de creación de los campos/índice
			//según MySQL, y eventualmente se podrán reordenar atributos
			//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			//@JAPR 2016-06-13: Agregada la configuración para limitar los resultados de los valores de catálogo en modo de diseño exclusivamente (#14KN4H)
			$sql = "SELECT " . ($inclueidValue?"id, ":"") . implode(', ', $arrfiels) . " FROM ". $this->TableName . $where .($withoutGroup?"":" GROUP BY " . implode(', ', $arrfiels)).
				" ORDER BY " .$strSpecialOrderFields. implode(', ', $arrfiels).$strLimit;
			//@JAPR
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nVerifying tablename: $this->TableName<br>\r\n");
				echo("<br>\r\nVerifying arrfiels: " . implode(', ', $arrfiels) . "<br>\r\n");
				echo("<br>\r\nVerifying where: $where<br>\r\n");
				echo("<br>\r\nVerifying sql: $sql<br>\r\n");
			}
			//@JAPR 2015-08-02: Corregido un bug, la función debe regresar un array vacio incluso en caso de error para no detener la ejecución de la descarga de definiciones 
			//@JAPR 2015-08-23: Validado que el fetchMode sólo aplique si se recibe el parámetro $iFetchMode
			if (!is_null($iFetchMode)) {
				$intCurrentFecthMode = $this->Repository->DataADOConnection->SetFetchMode($iFetchMode);
			}
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!is_null($iFetchMode)) {
				$this->Repository->DataADOConnection->SetFetchMode($intCurrentFecthMode);
			}
			$fieldKey = 'id';
		}
		if (!$aRS || @$aRS->EOF) {
			return $attribValues;
		}
		if ($bGetRecordset) {
			return $aRS;
		}
		//@JAPR 2015-07-18: Adaptada para funcionar tal como la recibe desde BITAMCatalog, esto NO debió ser así, se debe corregir, debe ser compatible con BITAMCatalog
		if ($exitType == "1" || $exitType == "3" ) {
			while(!$aRS->EOF)
			{
				for($i=0; $i<$lim; $i++)
				{
					//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
					//Por compatibilidad con el soporte a eBavel, ahora el array guardará el nombre de campo a buscar en el recordset
					//@JAPR 2015-12-10: Validado que campos de tipo eBavel no existentes no generen Warnings por intentar ser usados, dado a que eBavel no generaría error de query (#FN0BJM)
					//si se le pide un campo no existente pues simplemente lo ignora, pero el recordset resultando no lo traería obviamente por ya no existir así que no se podría usar
					$field = $arrayAttribIDs[$i]["fieldName"];
					$attribVal = (string) @$aRS->fields[$field];
					//@JRPP 2015-11-27: Esto para los casos de los campos que vienen de eBavel. Se limitan todas las cadenas a 250 caracteres que es lo que soportan los campos de eForms
					$maxlongeForms = strlen($attribVal);
					//@JAPR 2015-12-10: Se reutilizó la variable del valor en lugar de estar pidiendola constantemente al fields (#FN0BJM)
					//$maxlongeForms = strlen($aRS->fields[$field]);
					if ($maxlongeForms>255) {
						$maxlongeForms = 255;
					}
					//@JAPR 2015-12-10: Se reutilizó la variable del valor en lugar de estar pidiendola constantemente al fields (#FN0BJM)
					$attribVal = substr($attribVal, 0, $maxlongeForms);
					//$attribVal = substr($aRS->fields[$field], 0, $maxlongeForms);
					
					/*@AAL 15/04/2015 Issue #QOI4L5 corregido: Cuando venia un campo como NULL desde eBavel $attribVal generaba
					 un array con key -1 y se ocasionaba un error al general el Array attributeValues*/
					if($attribVal == NULL) 
						$attribVal = "";
					//@AAL

					//$field = "DSC_".$arrayAttribIDs[$i]["dimID"];
					//$attribVal = $aRS->fields[strtolower($field)];
					//@JAPR 2016-09-06: Validado que en caso de tratarse de un campo tipo Hora de eBavel, se aplique una conversión a formato de 24hrs vía un objeto DateTime
					//@JAPR 2016-09-07: Finalmente el problema por el cual se grababan las fechas en campos de eBavel con formato AM/PM fue debido a cargas directas que hacía ACeballos, así
					//que LRoux decidió que eForms NO aplicara este parche, pero se guardara el código por si eventualmente se tenía que hacer algo similar por otra causa
					/*if ((int) @$arrayAttribIDs[$i]["isTime"]) {
						if (trim($attribVal) != '') {
							try {
								$dteTime = new DateTime($attribVal);
								$attribVal = $dteTime->format("H:i:s");
							} catch (Exception $e) {
							}
						}
					}*/
					
					//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
					//Si el atributo es imagen, entonces debe ajustar las rutas para que funcionen con dispositivos
					//@JAPR 2014-12-16: Corregido un bug, estaba mal la validación, se debe hacer contra el valor no la existencia de la propiedad
					//@JAPR 2014-12-18: Corregido un bug, para atributos de eBavel si hay que hacer un proceso extra ya que la ruta a regresar debe
					//ser absoluta, de lo contrario no encontrará la imagen relativa al servicio de eForms
					//@JAPR 2016-09-07: Finalmente el problema por el cual se grababan las fechas en campos de eBavel con formato AM/PM fue debido a cargas directas que hacía ACeballos, así
					//que LRoux decidió que eForms NO aplicara este parche, pero se guardara el código por si eventualmente se tenía que hacer algo similar por otra causa
					//elseif ((int) @$arrayAttribIDs[$i]["isImage"] && $attribVal && (!$blnWebMode || ($this->eBavelAppID > 0 && $this->eBavelFormID > 0))) {
					//@JAPR
					if ((int) @$arrayAttribIDs[$i]["isImage"] && $attribVal && (!$blnWebMode || ($this->eBavelAppID > 0 && $this->eBavelFormID > 0))) {
						if ($blnWebMode) {
							//En este caso es modo Web por lo que el catalogo tiene que ser de tipo eBavel, así que se convierte la URL a absoluta
							$attribVal = $streBavelPath.$attribVal;
						}
						else {
							//@JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
							if ($blnReturnFullServerImgPaths) {
								//En este caso es es un valor dinámico por lo que el catalogo tiene que ser de tipo eBavel, así que se convierte la URL a absoluta
								$attribVal = $streBavelPath.$attribVal;
							}
							else {
								$attribVal = "{@IMGPATH}".GetImageRelativeNameForApp($this->Repository, $attribVal, $this);
							}
							//@JAPR
						}
					}
					//@JAPR
					
					if($i!=($lim-1))
					{
						if($attribVal!==$arrLast[$i])
						{
							if($arrLast[$i]!==null)
							{
								for($j=($lim-2); $j>=$i; $j--)
								{
									eval('$attrib'.$j.'[$arrIdxs[$j]]["children"]=$attrib'.($j+1).';');
								}
								
								for($j=$i+1;$j<$lim;$j++)
								{
									eval('$attrib'.$j.'=array();');
									$arrIdxs[$j] = -1;
									$arrLast[$j] = null;
								}
							}
							
							$arrIdxs[$i] = $arrIdxs[$i] + 1;
							//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
							//Por compatibilidad con el soporte a eBavel, ahora el array guardará el nombre de campo a buscar en el recordset
							if($inclueidValue) {
								$valueFieldKey = (int) @$aRS->fields[$fieldKey];
								//@JAPR
								eval('$attrib'.$i.'[$arrIdxs[$i]]["key"]=$valueFieldKey;');
							}
							eval('$attrib'.$i.'[$arrIdxs[$i]]["value"]=$attribVal;');
						}
					}
					else 
					{
						//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
						//Por compatibilidad con el soporte a eBavel, ahora el array guardará el nombre de campo a buscar en el recordset
						$arrIdxs[$i]=$arrIdxs[$i]+1;
						if($inclueidValue) {
							$valueFieldKey = (int) @$aRS->fields[$fieldKey];
							//@JAPR
							eval('$attrib'.$i.'[$arrIdxs[$i]]["key"]=$valueFieldKey;');
						}
						eval('$attrib'.$i.'[$arrIdxs[$i]]["value"]=$attribVal;');
						eval('$attrib'.$i.'[$arrIdxs[$i]]["children"]=array();');
					}
					
					$arrLast[$i] = $attribVal;
				}
				
				$aRS->MoveNext();
			}
			if($lim>1)
			{
				for($j=$lim-2;$j>=0;$j--)
				{
					eval('$attrib'.$j.'[$arrIdxs[$j]]["children"]=$attrib'.($j+1).';');
				}
			}
			
			//@JAPR 2013-02-11: Agregada la edición de datos históricos
			if ($blnAddHistoricData) {
				//Si se regresarán los datos históricos, primero se deben anexar al array los datos históricos y luego los calculados, pero por la
				//manera en que se genera el array dinámicamente, se hace un recorrido al final para pasar de un array al otro
				foreach ($attrib0 as $aParentValueArr) {
					$attribValues[] = $aParentValueArr;
				}
			}
			else {
				$attribValues = $attrib0;
			}
		} else if ($exitType == "2") {
			//@JAPR 2015-12-21: Corregido un bug, esta función no estaba preparada para regresar el caso de eForms v5 de la clase CatalogFilter, en la que
			//sólo se requiere un único campo, así que $exitType == 2 será dicho caso, si se desea un array plano pero con múltiples campos, se tendrá que
			//programar otra función o bien regresar el árbol de valores de los $exitType == 1 y 3 (#SS2EKF)
			$fieldName = "";
			if (count($arrayAttribIDs) > 0) {
				$fieldName = (string) @$arrayAttribIDs[0]["fieldName"];
			}
			$field = strtolower($fieldName);
			while(!$aRS->EOF)
			{
				//@JAPR 2015-12-10: Validado que campos de tipo eBavel no existentes no generen Warnings por intentar ser usados, dado a que eBavel no generaría error de query (#FN0BJM)
				//si se le pide un campo no existente pues simplemente lo ignora, pero el recordset resultando no lo traería obviamente por ya no existir así que no se podría usar
				$value = (string) @$aRS->fields[$field];
				//@JAPR 2015-12-21: Corregido un bug, no estaba asignando a la variable de salida correcta (#SS2EKF)
				$attribValues[$value] = $value;
				//@JAPR
				$aRS->MoveNext();
			}
		//@JAPR 2018-05-17: Optimizada la carga de eForms (#G87C4W)
		//Modificado el proceso para que el query de catálogos de eBavel pueda regresar la fecha de última modificación del registro, originalmente utilizado
		//por el nuevo proceso de obtención de versiones de las imágenes para saber la última fecha en que pudo haber cambiado la imagen en eBavel
		} else if ($exitType == "4") {
			//@JAPR 2015-12-21: Corregido un bug, esta función no estaba preparada para regresar el caso de eForms v5 de la clase CatalogFilter, en la que
			//sólo se requiere un único campo, así que $exitType == 2 será dicho caso, si se desea un array plano pero con múltiples campos, se tendrá que
			//programar otra función o bien regresar el árbol de valores de los $exitType == 1 y 3 (#SS2EKF)
			$fieldName = "";
			if (count($arrayAttribIDs) > 0) {
				$fieldName = (string) @$arrayAttribIDs[0]["fieldName"];
			}
			$field = strtolower($fieldName);
			while(!$aRS->EOF)
			{
				//@JAPR 2015-12-10: Validado que campos de tipo eBavel no existentes no generen Warnings por intentar ser usados, dado a que eBavel no generaría error de query (#FN0BJM)
				//si se le pide un campo no existente pues simplemente lo ignora, pero el recordset resultando no lo traería obviamente por ya no existir así que no se podría usar
				$value = (string) @$aRS->fields[$field];
				$modifiedDate = (string) @$aRS->fields["modifiedDate"];
				//@JAPR 2015-12-21: Corregido un bug, no estaba asignando a la variable de salida correcta (#SS2EKF)
				$attribValues[$value] = array("value" => $value, "lastDate" => $modifiedDate);
				//@JAPR
				$aRS->MoveNext();
			}
		}
		//@JAPR
		return $attribValues;
	}
	
	//@JAPR 2016-12-09: Agregado el esquema de paginación para obtener valores (#U7J0XU)
	/* Obtiene el número de páginas que se podrán desplegar basado en el total de rows del DataSource (según el tipo de fuente correspondiente) dividido entre el total configurado
	de rows por página especificado en el parámetro */
	function getAttributeValuesPageCount($aNumValuesPerPage = null) {
		if (is_null($aNumValuesPerPage) || !is_numeric($aNumValuesPerPage) || ($aNumValuesPerPage = (int) $aNumValuesPerPage) < 0) {
			$aNumValuesPerPage = DEFAULT_DATASOURCE_VALUESPERPAGE;
		}
		
		$intTotalValuesPagesNum = @self::$ArrDataSourceRowCount[$this->DataSourceID];
		$where = "";
		
		if (is_null($intTotalValuesPagesNum)) {
			if ($this->Type == dstyTable) {
				//Se trata de un catálogo tipo Tabla (ODBC)
				$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->DataSourceID);
				$arrayAttribIDs = array();
				foreach($objDataSourceMembersColl->Collection as $objDataSourceMember){
					$anObjectMember = array();
					$anObjectMember["DataSourceID"]= $objDataSourceMember->DataSourceID;
					$anObjectMember["sourceMemberID"]= $objDataSourceMember->MemberID;
					$anObjectMember["MemberName"]= $objDataSourceMember->MemberName;
					$anObjectMember["FieldName"]= $objDataSourceMember->FieldName;
					$anObjectMember["eBavelFieldID"]= $objDataSourceMember->eBavelFieldID;
					$anObjectMember["eBavelFieldDataID"]= $objDataSourceMember->eBavelFieldDataID;
					$arrayAttribIDs[] = $anObjectMember;
				}
				
				//Es importante mandar el parámetro para no agrupar o el count saldrá mal ya que se mostrará un 1 por cada agrupación (parámetro #6 == true)
				$intTotalValuesPagesNum = $this->getExternalCatalogValuesCount($arrayAttribIDs, -1, "", false, false, true);
			}
			elseif ($this->eBavelAppID > 0 && $this->eBavelFormID > 0) {
				//Se trata de un catálogo de eBavel
				$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->DataSourceID);
				$arrayAttribIDs = array();
				foreach($objDataSourceMembersColl->Collection as $objDataSourceMember){
					$anObjectMember = array();
					$anObjectMember["DataSourceID"]= $objDataSourceMember->DataSourceID;
					$anObjectMember["sourceMemberID"]= $objDataSourceMember->MemberID;
					$anObjectMember["MemberName"]= $objDataSourceMember->MemberName;
					$anObjectMember["FieldName"]= $objDataSourceMember->FieldName;
					$anObjectMember["eBavelFieldID"]= $objDataSourceMember->eBavelFieldID;
					$anObjectMember["eBavelFieldDataID"]= $objDataSourceMember->eBavelFieldDataID;
					$arrayAttribIDs[] = $anObjectMember;
				}
				
				//@JAPR 2014-12-18: Corregido un bug, para atributos de eBavel si hay que hacer un proceso extra ya que la ruta a regresar debe
				//ser absoluta, de lo contrario no encontrará la imagen relativa al servicio de eForms
				$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'EBAVELURL');
				if (!is_null($objSetting)) {
					$streBavelPath = trim((string) @$objSetting->SettingValue);
				}
				$arrFieldseBavelID = array();
				//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				$arrSpecialOrderFields = array();
				foreach ($arrayAttribIDs as $key => $value) {
					$arrFieldseBavelID [] = @$arrayAttribIDs[$key]["sourceMemberID"];
					//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
					if ((int) @$arrayAttribIDs[$key]["isOrder"]) {
						//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
						if ((int) @$arrayAttribIDs[$key]["sortDir"] == ordbDsc) {
							$arrSpecialOrderFields[] = array('id' => (int) @$arrayAttribIDs[$key]["sourceMemberID"], 'sortDir' => ordbDsc);
						}
						else {
							$arrSpecialOrderFields[] = (int) @$arrayAttribIDs[$key]["sourceMemberID"];
						}
					}
					//@JAPR
				}
				
				$intTotalValuesPagesNum = $this->getCatalogValuesCount($arrFieldseBavelID);
			}
			else {
				//Se trata de un catálogo de eForms
				$sql = "SELECT COUNT(*) AS TotalRowCount 
					FROM {$this->TableName}";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					$intTotalValuesPagesNum = (int) @$aRS->fields["totalrowcount"];
				}
			}
			
			self::$ArrDataSourceRowCount[$this->DataSourceID] = $intTotalValuesPagesNum;
		}
		
		//Si se especificó un 0 como total de rows por página, quiere decir que se quiere ver el total de registros (sólo una página)
		if ($aNumValuesPerPage == 0) {
			return 1;
		}
		
		if ($intTotalValuesPagesNum > 0) {
			$intTotalValuesPagesNum = (int) ceil($intTotalValuesPagesNum / $aNumValuesPerPage);
			if ($intTotalValuesPagesNum < 1) {
				$intTotalValuesPagesNum = 1;
			}
		}
		else {
			$intTotalValuesPagesNum = 1;
		}
		
		return $intTotalValuesPagesNum;
	}
	//@JAPR
	
	function getCallerFunction($iCallingLevel = 4, $rArray = false) {
	//return @array_shift(array_intersect_key(array_shift(array_splice(debug_backtrace(),1)), array('function' => 0)));
	$strCaller = '';
	$arrCaller = array();
	$trace = @debug_backtrace();

	for($i=0; $i<$iCallingLevel; $i++)
	{
		$caller = @$trace[$i];
		if (!is_null($caller)) {
			$strClass = (string) @$caller['class'];
			if (isset($caller['function'])) {
				if($caller["function"] == "getCallerFunction") continue;
				$strCaller .= "------------
Function: ". (($strClass != '')?"$strClass".(string) @$caller['type']:'').$caller['function'] ."
File:". $caller['file'] ."
Line:". $caller['line'] ."
";
				$arrCaller[] = (($strClass != '')?"$strClass".(string) @$caller['type']:'') . $caller['function'] . ', File: ' . $caller['file'] . ', Line: ' . $caller['line'];
			}
		}
	}
	
	return ($rArray ? $arrCaller : $strCaller);
}
	
	/* Verifica si la fecha especificada es válida según la calendarización del Scheduler de este DataSource
	Internamente utilizará el mismo método que BITAMSurveyAgendaScheduler para centralizar los procesos
	*/
    function isValidDate($dFechaValidar = null){
		require_once('surveyAgendaScheduler.inc.php');
		$objAgendaScheduler = BITAMSurveyAgendaScheduler::NewInstance($this->Repository);
		if (is_null($objAgendaScheduler)) {
			return false;
		}
		
		$objAgendaScheduler->RecurPatternType = $this->RecurPatternType;
		$objAgendaScheduler->RecurPatternOpt = $this->RecurPatternOpt;
		$objAgendaScheduler->RecurPatternNum = $this->RecurPatternNum;
		$objAgendaScheduler->RecurRangeOpt = $this->RecurRangeOpt;
		$objAgendaScheduler->Ocurrences = $this->Ocurrences;
		$objAgendaScheduler->CreationDateID = $this->CreationDateID;
		return $objAgendaScheduler->isValidDate($dFechaValidar);
	}

	function getCaptureStartTime()
	{
		return (($this->CaptureStartTime_Hours < 10)?'0':'').$this->CaptureStartTime_Hours.':'.(($this->CaptureStartTime_Minutes < 10)?'0':'').$this->CaptureStartTime_Minutes;
	}
	
	//@JAPR 2019-05-03: Modificado para mostrar valores dummy en los atributos en lugar de realizar la consulta real a los catálogos (#270TYE)
	/* Esta función recibe un número de registro, número de campo y un máximo de campo, y genera un elemento de valor de campo Dummy para un catálogo simulado de este DataSource, invocandose
	a si misma para rellenar el siguiente nivel de valores de tal forma que el valor originalmente resultante es la raiz de un árbol de valores tal como se requieren en los catálogos
	del App de eForms a la fecha de implementación (versión 6.02031, inicios de la versión 6.03xxx). La recursividad terminará cuando se llegue al máximo número de campo solicitado */
	static function GetDummyFieldDataRecursive($aRowNumber, $aFieldNumber, $aMaxFieldNumber = 0) {
		$aRowNumber = (int) @$aRowNumber;
		$aFieldNumber = (int) @$aFieldNumber;
		$aMaxFieldNumber = (int) @$aMaxFieldNumber;
		
		//Valida que no se cree un ciclo infinito, regresando un NULL en caso de error
		if ( $aFieldNumber > $aMaxFieldNumber) {
			return null;
		}
		
		//Genera el siguiente valor a regresar
		$aFieldValue = array();
		$aFieldValue['key'] = $aRowNumber;
		$aFieldValue['value'] = "Dummy {$aRowNumber}.{$aFieldNumber}";
		$aFieldValue['children'] = array();
		
		//En caso de aún quedar mas campos que procesar, invoca recursivamente a la función para generar el siguiente hasta completar la rama
		if ( $aFieldNumber < $aMaxFieldNumber ) {
			$aFieldValue['children'][] = BITAMDataSource::GetDummyFieldDataRecursive($aRowNumber, ++$aFieldNumber, $aMaxFieldNumber);
		}
		
		return $aFieldValue;
	}
	//@JAPR
}

class BITAMDataSourceCollection extends BITAMCollection
{
	use BITAMCollectionExt;

	public $ObjectType = otyDataSource;

  	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}

	function get_CustomButtons() {
		$arrButtons = array();
		$arrButtons[] = array("id" => "add", "url" => "", "onclick" => "addNewObject();", "isDefault" => 1);	//*"label" => translate("Add"), "image" => "add.png", */
		//$arrButtons[] = array("id" => "import", "label" => translate("Import"), "image" => "catalogEntry.gif", "url" => "", "onclick" => "importObject();");
		return $arrButtons;
	}
	static function NewInstance($aRepository, $anArrayOfCatalogIDs = null)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;

		//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
		if(is_null($anArrayOfCatalogIDs) || count($anArrayOfCatalogIDs)<=0)
		{
			$anInstance =& BITAMGlobalFormsInstance::GetDataSourceCollectionAll();
			if(!is_null($anInstance))
			{
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("(DataSourceCollectionCls->NewInstance) ByReference");
				}
				return $anInstance;
			}
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("(DataSourceCollectionCls->NewInstance) ByLoadingFromDB");
		}
		
		//@JRPP 2015-06-27: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository);
		
		$where = "";
		if (!is_null($anArrayOfCatalogIDs))
		{
			switch (count($anArrayOfCatalogIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE DataSourceID = ".((int) $anArrayOfCatalogIDs[0]);
					break;
				default:
					foreach ($anArrayOfCatalogIDs as $aCatalogID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $aCatalogID; 
					}
					if ($where != "")
					{
						$where = "WHERE DataSourceID IN (".$where.")";
					}
					break;
			}
		}
		
		//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
		$strAdditionalFields = "";
		if (getAppVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', VersionNum ';
		}
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', eBavelAppID, eBavelFormID';
		}
		//@JAPR

		//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
		if (getMDVersion() >= esvRedesign) {
			$strAdditionalFields .= ', eBavelFormType';
		}
		//@JRPP 2015-09-03: Nuevo campo para guardar el nombre del documento
		if (getMDVersion() >= esvDataSourceDocument) {
			$strAdditionalFields .= ', Document';
		}
		//@JAPR 2015-09-06: Agregados los parámetros de recurrencia al DataSource basado en el cambio implementado por JCEM para SurveyAgendaScheduler
		if (getMDVersion() >= esvDataSourceAgent) {
			$strAdditionalFields .= ', RecurPatternType, RecurPatternOpt, RecurPatternNum, RecurRangeOpt, id_ServiceConnection, LastSyncDate, Path, SheetName';
		}
		//@JAPR 2015-09-14: Corregido un bug, había faltado agregar este campo así que el proceso del Agente no ejecutaba las tareas de carga de DataSources
		if (getMDVersion() >= esvDataSourceAgentRecurActive) {
			$strAdditionalFields .= ', RecurActive';
		}
		//@JAPR
		if (getMDVersion() >= esvDataSourceIdGdrive) {
			$strAdditionalFields .= ', IdGoogleDrive';
		}
		//@JAPR 2015-09-17: Corregido un bug, había faltado agregar este campo así que el proceso del Agente no ejecutaba las tareas de carga de DataSources
		if (getMDVersion() >= esvDataSourceAgentHours) {
			$strAdditionalFields .= ', capturestarttime, `interval`';
		}		
		//@JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
		if (getMDVersion() >= esvDataSourceErrors) {
			$strAdditionalFields .= ', LastSyncErrorDate, LastSyncError';
		}
		if (getMDVersion() >= esvDataDestination) {
			//@JAPR 2015-12-22: Agregado el tipo de DataSource basado en Tabla
			$strAdditionalFields .= ', parameters, ServiceDBName, ServiceTableName';
		}	
		//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
		if (getMDVersion() >= esvDataSourceDataVersion) {
			$strAdditionalFields .= ', DataVersionDate';
		}
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		if (getMDVersion() >= esvQuestionAR) {
			$strAdditionalFields .= ', IsModel';
		}
		//@JAPR 2016-11-11: Corregido un bug, se estaba permitiendo capturar nombres con demasiados espacios en blanco que la interfaz no presentaba, se elimiarán (#FJAJS8, referencia #070KU7)
		$sql = "SELECT DataSourceID, DataSourceName, TableName, Type $strAdditionalFields 
			FROM SI_SV_DataSource ".$where." 
			ORDER BY LTRIM(RTRIM(DataSourceName))";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSource ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
			$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		//@JAPR 2018-07-13 Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		if (is_null($anArrayOfCatalogIDs) || count($anArrayOfCatalogIDs)<=0)
		{
			BITAMGlobalFormsInstance::AddDataSourceCollectionAll($anInstance);
		}
		//@JAPR
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2015-04-25: Rediseñado el Admin con DHTMLX
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($aRepository, null);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Catalogs");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=DataSourceCollection";
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_SECTION=DataSource";
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=DataSource";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'DataSourceID';
	}

	function get_FormFieldName() {
		return 'DataSourceName';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataSourceName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}

	function canAdd($aUser)
	{
		return true;	
	}
	
	function addButtons($aUser)
	{
	}

	function generateBeforeFormCode($aUser)
 	{
	}

	function generateAfterFormCode($aUser)
	{

?>
	<script language="JavaScript">
		var objWindows;
		var objWindows1;
		var objDialog;
		var intDialogWidth = 380;
		var intDialogHeight = 500;
		var reqAjax = 1;
		var newTypeTable = false;
		DatasourceFirst = true;
<?
		if ( getMDVersion() >= esvDataDestination) {
?>
			var x_count = 4;
			var widthAdd = 485;
			var	heightAdd = 250;
			var newTypeHTTP = true;
			var newTypeTable = true;

<?
		} else {
?>
			var x_count = 3;
			var widthAdd = 400;
			var	heightAdd = 250;
			var newTypeHTTP = false;
<?
		}
?>
		//Descarga la instancia de la forma de la memoria
		function doUnloadDialog() {
			if (objDialog) {
				var objForm = objDialog.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
			}
			
			if (objWindows) {
				if (objWindows.unload) {
					objWindows.unload();
					objWindows = null;
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
			}
			
			objDialog = null;
		}
		
		//Manda los datos al server para ser procesados
		function doSendData() {
			if (!objDialog) {
				return;
			}
			
			objDialog.progressOn();
			var objForm = objDialog.getAttachedObject();
			var objData = objForm.getFormData();
			//JAPR 2016-07-29: Corregido un bug, no debe reemplazar el <enter> por otros caracteres imprimibles (#LSJYAB)
			//objData.Members = objData.Members.replace(/\n/g,",");
			//JAPR
			objData.SurveyName = objData.SurveyName;
			var data = JSON.stringify(objData);
			var urlPath = 'loadexcel/';
			$.post(urlPath +'action.php?action=createDatasource', {"data":data, "type": 1}).done(function( r ) {
				/*termino de insertar por tanto cargo la pantalla del detalle*/
				var dhxList = "b";
				strURL =  "<?='main.php?'.$this->get_AddRemoveQueryString().'&DataSourceID='?>"+ r.idNewCatalog;
				parent.objFormsLayoutMenu.cells(dhxList).progressOn();
				parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
			});
		}
		//Muestra el diálogo para crear una nueva forma
		function addNewObject(){
			if (!objWindows1) {
				//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
				objWindows1 = new dhtmlXWindows({
					image_path:"images/"
				});
			}
			
			objDialog1 = objWindows1.createWindow({
				id:"newObject",
				left:0,
				top:0,
				width:widthAdd,
				height:heightAdd,
				center:true,
				modal:true
			});
			
			objDialog1.setText("<?=translate("New catalog")?>");
			objDialog1.denyPark();
			objDialog1.denyResize();
			objDialog1.setIconCss("without_icon");
			
			//Al cerrar realizará el cambio de sección de la pregunta
			objDialog1.attachEvent("onClose", function() {
				return true;
			});
			var objDataViewNew = objDialog1.attachDataView({
				container:"",
				type:{
					template:"#picture#<div style='font-family:Roboto Regular;font-size:13px;font-weight:normal; text-align: center;' data-tooltip='#tooltip#'>#description#</div><div class='checkContainer'><div><i class='icon-check-sign'></i></div></div>",
					width:"100",
					border:0,
					height:58
				},
				x_count: x_count
			});
			objDataViewNew.add({id:"Manual",description:"<?=translate("Manual")?>", picture: '<img style="height:43px; width:43px;" src="images/admin/catalogs_64x64_azul.png" />'});
			objDataViewNew.add({id:"Excel",description:"<?=translate("Excel")?>", picture: '<img  src="images/admin/excel azul.png" />'});
			objDataViewNew.add({id:"eBavel",description:"<?=translate("eBavel")?>", picture: '<img  src="images/datadestinations/ebavel.png" />'});
			objDataViewNew.add({id:"FTP",description:"<?=translate("FTP")?>", picture: '<img style="height:43px; width:43px;" src="images/datadestinations/ftp.png" />'});
			objDataViewNew.add({id:"Dropbox",description:"<?=translate("Dropbox")?>", picture: '<img style="height:43px; width:43px;" src="images/datadestinations/dropbox.png" />'});
			objDataViewNew.add({id:"GoogleDrive",description:"<?=translate("Google Drive")?>", picture: '<img style="height:43px; width:43px;" src="images/datadestinations/drive.png" />'});
			if (newTypeHTTP){
				objDataViewNew.add({id:"HTTP",description:"<?=translate("HTTP")?>", picture: '<img style="height:43px; width:43px;" src="images/datadestinations/http.png" />'});	
				objDataViewNew.add({id:"table",description:"<?=translate("ODBC")?>", picture: '<img style="height:43px; width:43px;" src="images/datadestinations/data_connection.png" />'});		
			}

			objDataViewNew.attachEvent("onItemClick", function (id, ev, html) {//2015-06-15@JRPP cambio onItemDblClick por peticion de claudia
			console.log("objListDataView.onItemClick " + id);
				switch(id) {
					case "Manual":
							addNewObjectmanual();
						break;
					case "Excel" :
						if (DatasourceFirst){
							$(parent.window.document.getElementById('fondo')).show();
							DatasourceFirst = false;
							//@ears 2016-07-22 se aplica progressOn antes de invocar el dialog de captura de los datos en Excel (issue GNFIZE)
							objDialog1.progressOn();
							require(['views/loadexcel/wizardLoadExcel'], function( Wizard ) {
								objDialog1.progressOff();
								$(parent.window.document.getElementById('fondo')).hide();
								var idModel = null;
								$.t = function( s ) { return s; };
								Wizard(idModel, null, null).done(function(idCatalog,label) {
									var dhxList = "b";
									strURL =  "<?='main.php?'.$this->get_AddRemoveQueryString().'&DataSourceID='?>"+ idCatalog;
									parent.objFormsLayoutMenu.cells(dhxList).progressOn();
									parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
								});
							});
						} else {
							$(parent.window.document.getElementById('fondo')).hide();
							return;
						}
						break;
					case "eBavel" :
						if (DatasourceFirst){
							$(parent.window.document.getElementById('fondo')).show();
							DatasourceFirst = false;
							//@ears 2016-07-22 se aplica progressOn antes de invocar el dialog de captura de los datos en eBavel (issue GNFIZE)
							objDialog1.progressOn();							
							require(['views/loadeBavel/wizardLoadeBavel'], function( Wizard ) {
								objDialog1.progressOff();								
								$(parent.window.document.getElementById('fondo')).hide();
								var idModel = null;
								$.t = function( s ) { return s; };
								Wizard(idModel, null, null).done(function(idCatalog,label) {
									var dhxList = "b";
									strURL =  "<?='main.php?'.$this->get_AddRemoveQueryString().'&DataSourceID='?>"+ idCatalog;
									parent.objFormsLayoutMenu.cells(dhxList).progressOn();
									parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
								});
							});
						} else {
							$(parent.window.document.getElementById('fondo')).hide();
							return;
						}
						break;	
					case "FTP" :
						if (DatasourceFirst){
							$(parent.window.document.getElementById('fondo')).show();
							DatasourceFirst = false;
							//@ears 2016-07-22 se aplica progressOn antes de invocar el dialog de captura de los datos en FTP (issue GNFIZE)
							objDialog1.progressOn();														
							require(['loadexcel/js/views/loadDatasource/wizardloadDatasource.php?'], function( Wizard ) {
								objDialog1.progressOff();																
								$(parent.window.document.getElementById('fondo')).hide();
								var idModel = null;
								$.t = function( s ) { return s; };
								Wizard(idModel, "5").done(function(idCatalog, label) {
									var dhxList = "b";
									strURL =  "<?='main.php?'.$this->get_AddRemoveQueryString().'&DataSourceID='?>"+ idCatalog;
									parent.objFormsLayoutMenu.cells(dhxList).progressOn();
									parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
								});
							});
						} else {
							$(parent.window.document.getElementById('fondo')).hide();
							return;
						}
						break;
					case "Dropbox" :
						if (DatasourceFirst){
							$(parent.window.document.getElementById('fondo')).show();
							DatasourceFirst = false;
							//@ears 2016-07-22 se aplica progressOn antes de invocar el dialog de captura de los datos en Dropbox (issue GNFIZE)
							objDialog1.progressOn();																					
							require(['loadexcel/js/views/loadDatasource/wizardloadDatasource.php?'], function( Wizard ) {
								objDialog1.progressOff();																								
								$(parent.window.document.getElementById('fondo')).hide();
								var idModel = null;
								$.t = function( s ) { return s; };
								Wizard(idModel, "2").done(function(idCatalog,label) {
									var dhxList = "b";
									strURL =  "<?='main.php?'.$this->get_AddRemoveQueryString().'&DataSourceID='?>"+ idCatalog;
									parent.objFormsLayoutMenu.cells(dhxList).progressOn();
									parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
								});
							});
						} else {
							$(parent.window.document.getElementById('fondo')).hide();
							return;
						}
						break;	
					case "GoogleDrive" :
						if (DatasourceFirst){
							$(parent.window.document.getElementById('fondo')).show();
							DatasourceFirst = false;
							//@ears 2016-07-22 se aplica progressOn antes de invocar el dialog de captura de los datos en GoogleDrive (issue GNFIZE)
							objDialog1.progressOn();																												
							require(['loadexcel/js/views/loadDatasource/wizardloadDatasource.php?'], function( Wizard ) {
								objDialog1.progressOff();																																
								$(parent.window.document.getElementById('fondo')).hide();
								var idModel = null;
								$.t = function( s ) { return s; };
								Wizard(idModel, "3").done(function(idCatalog,label) {
									var dhxList = "b";
									strURL =  "<?='main.php?'.$this->get_AddRemoveQueryString().'&DataSourceID='?>"+ idCatalog;
									parent.objFormsLayoutMenu.cells(dhxList).progressOn();
									parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
								});
							});
						} else {
							$(parent.window.document.getElementById('fondo')).hide();
							return;
						}
						break;
					case "HTTP" :
						if (DatasourceFirst){
							$(parent.window.document.getElementById('fondo')).show();
							DatasourceFirst = false;
							//@ears 2016-07-22 se aplica progressOn antes de invocar el dialog de captura de los datos en HTTP (issue GNFIZE)
							objDialog1.progressOn();																																			
							require(['loadexcel/js/views/loadHTML/wizardLoadHTML.php?'], function( Wizard ) {
								objDialog1.progressOff();																																								
								$(parent.window.document.getElementById('fondo')).hide();
								var idModel = null;
								$.t = function( s ) { return s; };
								Wizard(idModel, null, null).done(function(idCatalog,label) {
									var dhxList = "b";
									strURL =  "<?='main.php?'.$this->get_AddRemoveQueryString().'&DataSourceID='?>"+ idCatalog;
									parent.objFormsLayoutMenu.cells(dhxList).progressOn();
									parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
								});
							});
						} else {
							$(parent.window.document.getElementById('fondo')).hide();
							return;
						}
						break;	
					case "table" :
						if (DatasourceFirst){
							$(parent.window.document.getElementById('fondo')).show();
							DatasourceFirst = false;
							//@ears 2016-07-22 se aplica progressOn antes de invocar el dialog de captura de los datos en table (issue GNFIZE)
							objDialog1.progressOn();																																										
							require(['loadexcel/js/views/loadTable/wizardLoadTable.php?'], function( Wizard ) {
								objDialog1.progressOff();																																																
								$(parent.window.document.getElementById('fondo')).hide();
								var idModel = null;
								$.t = function( s ) { return s; };
								Wizard(idModel, null, null).done(function(idCatalog,label) {
									var dhxList = "b";
									strURL =  "<?='main.php?'.$this->get_AddRemoveQueryString().'&DataSourceID='?>"+ idCatalog;
									parent.objFormsLayoutMenu.cells(dhxList).progressOn();
									parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
								});
							});
						} else {
							$(parent.window.document.getElementById('fondo')).hide();
							return;
						}
						break;	
				}
			});
		}
		function addNewObjectmanual() {
			if (!objWindows) {
				//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
			}
			
			objDialog = objWindows.createWindow({
				id:"newObject",
				left:0,
				top:0,
				width:intDialogWidth,
				height:intDialogHeight,
				center:true,
				modal:true
			});
			
			objDialog.setText("<?=translate("New catalog")?>");
			objDialog.denyPark();
			objDialog.denyResize();
			objDialog.setIconCss("without_icon");
			
			//Al cerrar realizará el cambio de sección de la pregunta
			objDialog.attachEvent("onClose", function() {
				return true;
			});
			
			/* El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma */
			var objFormData = [
				{type:"settings"/*, offsetLeft:20*/},
				{type:"input", name:"SurveyName", label:"<?=translate("Name")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
				{type:"input", name:"Members", label:"<?=translate("Members")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty", rows:3,note:{text:"<?=translate("Enter the members for single selection separated by <enter> character.")?>"}},
				{type:"block", blockOffset:0, offsetLeft:100, list:[
					{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
					{type:"newcolumn"},
					{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
				]},
				{type: "hidden", name:"type", value:"1"}
			];
			
			var objForm = objDialog.attachForm(objFormData);
			//objForm.hideItem("Catalog");
			objForm.adjustParentSize();
			objForm.setItemFocus("SurveyName");
			objForm.attachEvent("onBeforeValidate", function (id) {
				console.log('Before validating the form: id == ' + id);
			});
			
			objForm.attachEvent("onAfterValidate", function (status) {
				console.log('After validating the form: status == ' + status);
			});
			
			objForm.attachEvent("onValidateSuccess", function (name, value, result) {
				console.log('Validating rule ok: name == ' + name + ', value == ' + value + ', result == ' + result);
			});
			
			objForm.attachEvent("onValidateError", function (name, value, result) {
				console.log('Error validating the form: name == ' + name + ', value == ' + value + ', result == ' + result);
			});
			
			
			objForm.attachEvent("onButtonClick", function(name) {
				switch (name) {
					case "btnCancel":
						setTimeout(function () {
							doUnloadDialog();
						}, 100);
						break;
					case "btnOk":
						setTimeout(function() {
							if (objForm.validate()) {
								doSendData();
							}
						}, 100);
						break;
				}
			});			
		}
		function importObject() {
			require(['views/loadexcel/wizardLoadExcel'], function( Wizard ) {
				/** Funcion para traducciones */
				var idModel = null;
				$.t = function( s ) { return s; };
				Wizard(idModel, null, null).done(function(idCatalog,label) {
					$(parent.window.document.getElementById('fondo')).show();
					var dhxList = "b";
					strURL =  "<?='main.php?'.$this->get_AddRemoveQueryString().'&DataSourceID='?>"+ idCatalog;
					parent.objFormsLayoutMenu.cells(dhxList).progressOn();
					parent.objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
				});
			});
		}
	</script>
<?
	
	}
}
?>