<?php
/*Realiza la conversión de todos los catálogos y encuestas para utilizar Dimensiones independientes de los atributos de los catálogos
Este proceso se debe realizar mientras no existan sincronizaciones de capturas, ya que modificará la estructura de las tablas y podría
afectar el grabado de algunas capturas si estas se realizan cuando el proceso aun no esté completo
Al finalizar el proceso, se debería hacer una revisión de los reportes con la cuenta afectada para verificar que todo quedó correctamente,
además de realizar capturas de prueba de las encuestas que fueron alteradas
Dependiendo de los posibles errores de grabado durante la vida de las encuestas, pudiera ser necesario cierta cantidad de correcciones manuales
en los datos de las tablas al finalizar este proceso
*/
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$dteCurrDate = date("'Y-m-d H:i:s'");
//Si se cambia esta variable a false, se ejecutará todo el proceso de validaciones y creación de campos, pero no se ejecutarán los UPDATEs que 
//cambian datos, esto puede ser útil para actualizar sólo la estructura pero si no se quiere lanzar la actualización de datos para hacer validaciones
//de último momento, o por si sólo se quiere que la cuenta sea corregida en caso de haber algún problema con la estructura esperada
$blnExecuteProcess = true;
$blnSimulate = getParamValue('Simulate', 'both', "(boolean)");
if ($blnSimulate) {
	$blnExecuteProcess = false;
}
//$blnExecuteProcess = false;	//Descomentar sólo durante una corrida de depuración del proceso
/*
if ($blnExecuteProcess) {
	die('No funcionó el simulador');
}
*/
$blnDebugBreak = getParamValue('DebugBreak', 'both', "(boolean)");

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
/*Desactiva el reporte de errores que termina la ejecución del proceso, con esto se permitirá reportar como resultado los casos de error
y continuar con aquello que sea posible
Hasta antes de este cambio, sólo los siguientes archivos utilizaban esta variable:
	- eSurveyService.php
	- fillGlobalSurveyModel_singlerecord.php
	- processSurveyFunctions.php
Cualquier archivo diferente de estos que use esta variable, sería así por preparación para este proceso. Como el comportamiento default era que
se interrumpiera la ejecución en caso de cualquier error, se asignará en config.php esta variable con un valor default de true
*/
require_once("processsurveyfunctions.php");

global $gblShowErrorSurvey;
$gblShowErrorSurvey = false;

//Si se agregan IDs, se realizará el proceso exclusivamente a estos catálogos/encuestas, de lo contrario se hará al total de la BD
$arrFilteredCatalogs = array();
$arrFilteredSurveys = array();
$strFilteredCatalogs = getParamValue('CatalogFilter', 'both', "(string)");
$strFilteredCatalogs = trim(str_replace(' ', '', $strFilteredCatalogs));
if ($strFilteredCatalogs != '') {
	$arrFilteredCatalogs = explode(',', $strFilteredCatalogs);
}
$strFilteredSurveys = getParamValue('SurveyFilter', 'both', "(string)");
$strFilteredSurveys = trim(str_replace(' ', '', $strFilteredSurveys));
if ($strFilteredSurveys != '') {
	$arrFilteredSurveys = explode(',', $strFilteredSurveys);
}

$objSurveyColl = BITAMSurveyCollection::NewInstance($aRepository, $arrFilteredSurveys, false);
if (!is_object($objSurveyColl)) {
	echo(sendUpgradeMessage("***** Error loading survey collection: ".(string) $objSurveyColl));
	exit();
}

//Si se aplicó algún filtro de encuestas, entonces se obtendrán todos los catálogos que esta encuesta podría utilizar para forzar la migración
//de esos exclusivamente
if (count($arrFilteredSurveys) > 0) {
	$strSurveyIDs = implode(', ', $arrFilteredSurveys);
    $arrCatalogIDs = array();
    $arrFilteredCatalogsFlip = array_flip($arrFilteredCatalogs);
    if ($strSurveyIDs != '') {
	    $sql = "SELECT DISTINCT A.CatalogID 
	        	FROM SI_SV_Question A 
	        	WHERE A.CatalogID > 0 ";
	    if ($strSurveyIDs != '') {
	    	$sql .= "AND A.SurveyID IN (".$strSurveyIDs.") ";
	    }
	    $sql .= "UNION 
	    		SELECT DISTINCT A.CatalogID 
	    		FROM SI_SV_Section A 
	    		WHERE A.CatalogID > 0 ";
	    if ($strSurveyIDs != '') {
	    	$sql .= "AND A.SurveyID IN (".$strSurveyIDs.") ";
	    }
	    $sql .= "UNION 
	    		SELECT DISTINCT A.CatalogID 
	    		FROM SI_SV_Survey A 
	    		WHERE A.CatalogID > 0 ";
	    if ($strSurveyIDs != '') {
	    	$sql .= "AND A.SurveyID IN (".$strSurveyIDs.")";
		}
	    $aRS = $aRepository->DataADOConnection->Execute($sql);
	    if ($aRS) {
		    while (!$aRS->EOF) {
		        $intCatalogID = (int) $aRS->fields["catalogid"];
		        if (!isset($arrFilteredCatalogsFlip[$intCatalogID])) {
		        	$arrFilteredCatalogs[] = $intCatalogID;
		        }
		        $aRS->MoveNext();
		    }
		}
    }
}

//*************************************************************************************************************************
//Crear las dimensiones independientes de los catálogos
//*************************************************************************************************************************
echo("<BR><BR>***** Migrating catalogs");
$arrCatalogsByID = array();
$arrCatMembersByCatalog = array();
$objCatalogColl = BITAMCatalogCollection::NewInstance($aRepository, $arrFilteredCatalogs);
if (!is_object($objCatalogColl)) {
	echo(sendUpgradeMessage("***** Error loading catalog collection: ".(string) $objCatalogColl));
	exit();
}

//Array con los catálogos que si se pudieron convertir, mas adelante las encuestas no se procesarán si por lo menos uno de sus catálogos no se
//logró convertir
$arrValidCatalogs = array();
$arrOldFmtCatalogs = array();
foreach ($objCatalogColl->Collection as $objCatalog) {
	echo("<BR><BR>***** Catalog: {$objCatalog->CatalogName}");
	
	$intCatalogID = $objCatalog->CatalogID;
	if (!$objCatalog->DissociateCatDimens) {
		$arrOldFmtCatalogs[$intCatalogID] = $intCatalogID;
	}
	
	//Cambia el catálogo al tipo con dimensiones independientes, esto creará además la jerarquía para los atributos en el cubo del catálogo
	//Este proceso se realiza independientemente de si el catálogo estaba o no ya convertido al formato de dimensiones independientes, ya que en
	//añgunos casos se había realizado una conversión parcial de sus atributos por lo que es necesario generar todos bajo este esquema
	echo("<BR>***** Migrating definition");
	$objCatalog->DissociateCatDimens = 1;
	$objSts = $objCatalog->save();
	if (is_object($objSts)) {
		//Debe actualizar el TableName porque al invocar al Save, por alguna razón estaba reseteando el TableName incorrectamente como RIDIM_
		$objCatalog->TableName = "RIDIM_".$objCatalog->ParentID;
		
		//Crea las dimensiones independientes del catálogo
		echo("<BR>***** Creating dissociated dimensions");
		$blnSts = $objCatalog->generateDissociatedDimensions(true);
		if ($blnSts === true) {
			$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $intCatalogID);
			if (is_object($objCatalogMembersColl)) {
				$arrValidCatalogs[$intCatalogID] = $intCatalogID;
				$arrCatalogsByID[$intCatalogID] = $objCatalog;
				$arrCatMembersByCatalog[$intCatalogID] = $objCatalogMembersColl;
			}
			else {
				echo(sendUpgradeMessage("***** Error: ".(string) $objCatalogMembersColl));
			}
		}
		else {
			echo(sendUpgradeMessage("***** Error: ".(string) $blnSts));
		}
	}
	else {
		echo(sendUpgradeMessage("***** Error: ".(string) $objSts));
	}
}

echo("<BR><BR>***** Migrating surveys");

//*************************************************************************************************************************
/*Identificar las encuestas candidatas a migración, además de los catálogos que utiliza cada encuesta
Si la encuesta ya estaba migrada, no se requiere ejecutar el proceso o por lo menos no en todos los niveles. Se puede
dividir el proceso en las siguiente secciones igualmente importantes y cada una requiere a las anteriores para poder existir:
1- Verificar si contiene los campos de identificación de la sección que graba cada registro
2- Verificar si contiene un registro único para las secciones estándar
3- Verificar si sus dimensiones se encuentran desligadas de los catálogos
*/
//*************************************************************************************************************************
echo("<BR>***** Identifying missing upgrades");
$arrValidSurveys = array();
$arrCatalogsBySurvey = array();
$arrSectionsBySurvey = array();
$arrQuestionsBySurvey = array();
$arrSurveyStats = array();
foreach ($objSurveyColl->Collection as $objSurvey) {
	echo("<BR>***** Survey: {$objSurvey->SurveyName}");
	$intSurveyID = $objSurvey->SurveyID;
	$arrSurveyStats[$intSurveyID] = array('Name' => $objSurvey->SurveyName, 'RequiredMigration' => false);
	$strSurveyTable = $objSurvey->SurveyTable;
	//Agrega el campo con el historial de actualizaciones
	//$sql = "ALTER TABLE ".$strSurveyTable." ADD UpgradeDate VARCHAR(25) NULL";
	//$aRepository->DataADOConnection->Execute($sql);
	//$sql = "ALTER TABLE ".$strSurveyTable." ADD UpgradesApplied INT NULL";
	//$aRepository->DataADOConnection->Execute($sql);
	
	//Identifica los catálogos utilizados por la encuesta
	$arrCatalogIDs = $objSurvey->getUsedCatalogsIDs();
	if (!is_array($arrCatalogIDs)) {
		echo(sendUpgradeMessage("***** Error: ".(string) $arrCatalogIDs));
		continue;
	}
	
	$arrCatalogsBySurvey[$intSurveyID] = $arrCatalogIDs;
	
	//Verifica si aún contiene por lo menos un factor no migrado que requiera ser actualizado, si ya contiene todo entonces se marca como no
	//válida para que el proceso la ignore
	$blnValid = false;
	$intTotalEntries = 0;
	$arrStepsRequired = array();
	$objSurvey->getSpecialFieldsStatus();
	
	//Obtiene el total de capturas, ya que si no hay datos en la encuesta entonces la mayoría de los procesos se reducen a simplemente crear
	//las columnas pero no hay que actualizar datos
	$sql = "SELECT COUNT(DISTINCT A.FactKeyDimVal) AS 'TotalEntries' FROM ".$strSurveyTable." A";
	echo(sendUpgradeMessage($sql, "font-style: italic;"));
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS === false) {
		echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
		continue;
	}
	
	if (!$aRS->EOF) {
		$intTotalEntries = (int) @$aRS->fields["totalentries"];
	}
	$arrSurveyStats[$intSurveyID]['TotalEntries'] = $intTotalEntries;
	
	//1- Verificar si contiene los campos de identificación de la sección que graba cada registro
	echo("<BR>***** Checking for source fields");
	if (!$objSurvey->UseSourceFields) {
		$blnValid = true;
		$arrStepsRequired['SourceFields'] = 1;
		$arrStepsRequired['SourceFieldsData'] = 1;
		$arrStepsRequired['SingleRecordData'] = ($intTotalEntries > 0)?1:0;
	}
	else {
		//Verifica si aún existen registros que no están totalmente actualizados a pesar de tener las columnas correctas
		$intNumberOfCases = 0;
		$sql = "SELECT DISTINCT A.EntrySectionID, A.EntryCatDimID 
			FROM ".$strSurveyTable." A 
			WHERE A.EntrySectionID IS NULL OR A.EntrySectionID = -1 OR A.EntryCatDimID IS NULL OR A.EntryCatDimID = -1";
		echo(sendUpgradeMessage($sql, "font-style: italic;"));
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
			continue;
		}
		
		if (!$aRS->EOF) {
			$blnValid = true;
			$arrStepsRequired['SourceFieldsData'] = 1;
		}
	}
	
	//2- Verificar si contiene un registro único para las secciones estándar
	echo("<BR>***** Checking for single record data");
	$intSingleRecordEntries = -1;
	
	if (!$objSurvey->UseStdSectionSingleRec) {
		$blnValid = true;
		$arrStepsRequired['SingleRecordData'] = ($intTotalEntries > 0)?1:0;
		$arrSurveyStats[$intSurveyID]['SingleRecordEntries'] = 0;
	}
	elseif ($objSurvey->UseSourceFields) {
		$sql = "SELECT COUNT(DISTINCT A.FactKeyDimVal) AS 'TotalEntries' FROM ".$strSurveyTable." A WHERE A.EntrySectionID = 0";
		echo(sendUpgradeMessage($sql, "font-style: italic;"));
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
			continue;
		}
		
		if (!$aRS->EOF) {
			$intSingleRecordEntries = (int) @$aRS->fields["totalentries"];
			$arrSurveyStats[$intSurveyID]['SingleRecordEntries'] = $intSingleRecordEntries;
		}
		
		if ($intTotalEntries > 0 && $intTotalEntries > $intSingleRecordEntries) {
			//Aunque ya exista la configuración de registro único activada, aun hay capturas que no lo tienen identificado por lo que se necesitará
			//agregar dicho registro, para esto se tendrá que hacer una simulación de grabado de la captura generando primero un archivo de datos
			//como si acabara de subir de una edición web, además de invocar en secuencia a la URL de la edición de todos ellos
			$blnValid = true;
			$arrStepsRequired['SingleRecordData'] = 1;
		}
	}
	
	//Obtiene la información de la encuesta
	echo("<BR>***** Reading survey definition");
	$objSectionColl = BITAMSectionCollection::NewInstance($aRepository, $intSurveyID);
	if (!is_object($objSectionColl)) {
		echo(sendUpgradeMessage("***** Error: ".(string) $objSectionColl));
		continue;
	}
	$arrSectionsBySurvey[$intSurveyID] = $objSectionColl;
	
	$objQuestionColl = BITAMQuestionCollection::NewInstance($aRepository, $intSurveyID);
	if (!is_object($objQuestionColl)) {
		echo(sendUpgradeMessage("***** Error: ".(string) $objQuestionColl));
		continue;
	}
	$arrQuestionsBySurvey[$intSurveyID] = $objQuestionColl;
	
	//3- Verificar si sus dimensiones se encuentran desligadas de los catálogos
	echo("<BR>***** Checking for dissociated dimensions");
	$blnHasOldFmtCatalogs = false;
	foreach ($arrCatalogIDs as $intCatalogID) {
		//Se considerará también casos donde no se encuentre en la colección de catálogos válidos, ya que pudiera haber un filtro aplicado por lo
		//que el catálogo no hubiera sido procesado en esta corrida
		if (isset($arrOldFmtCatalogs[$intCatalogID]) || !isset($arrValidCatalogs[$intCatalogID])) {
			$blnHasOldFmtCatalogs = true;
			break;
		}
	}
	
	if (!$objSurvey->DissociateCatDimens) {
		$blnValid = true;
		$arrStepsRequired['DissociateDimensions'] = 1;
		$arrStepsRequired['DissociateDimensionsData'] = ($intTotalEntries > 0)?1:0;
	}
	elseif ($blnHasOldFmtCatalogs) {
		//Si tiene por lo menos un catálogo no migrado, entonces la encuesta debe ser migrada a catálogos independientes
		$blnValid = true;
		$arrStepsRequired['DissociateDimensions'] = 1;
		$arrStepsRequired['DissociateDimensionsData'] = ($intTotalEntries > 0)?1:0;
	}
	else {
		//Aparentemente todos los catálogos están migrados, pero se debe revisar la estructura y datos de la encuesta para confirmarlo
		if (!$objSurvey->UseDynamicPageField || !$objSurvey->UseDynamicOptionField) {
			$blnValid = true;
			$arrStepsRequired['DissociateDimensions'] = 1;
			$arrStepsRequired['DissociateDimensionsData'] = ($intTotalEntries > 0)?1:0;
		}
		else {
			//Se tienen los campos generales, pero se debe verificar si todas las preguntas de esta encuesta tienen sus campos correspondientes
			$arrCatalogFields = array();
			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID > 0) {
					$arrCatalogFields[] = $objQuestion->SurveyCatField;
				}
			}
			
			if (count($arrCatalogFields) > 0) {
				$sql = "SELECT ".implode(', ', $arrCatalogFields)." 
					FROM ".$strSurveyTable." WHERE 1 = 2";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS === false) {
					$blnValid = true;
					$arrStepsRequired['DissociateDimensions'] = 1;
					$arrStepsRequired['DissociateDimensionsData'] = ($intTotalEntries > 0)?1:0;
				}
			}
			
			//@JAPRWarning: Verificar si todas las columnas contienen datos sería tal vez innecesario, se puede simplemente dejar correr el proceso
			//el cual incluye UPDATES validados para no sobreescribir en caso de ya haber datos, así que si aun hubieran columnas que no trajeran
			//información, estás se actualizarían al terminar el proceso. Lo que si sería importante es verificar los casos donde la información
			//por alguna razón estuviera dañada, como es el caso del campo DynamicPageDSC que es el mas común que pudiera tener error ya que en
			//algunas circunstancias se grabó con SectionValue-#### por lo que de esa menera no serviría
			//Nunca se debe ejecutar un UPDATE sin validación para sobreescribir datos de los campos, ya que éstos se tendrían que basar en los
			//valores actuales del catálogo los cuales no necesariamente son los correctos
			if (!$blnValid) {
				
			}
		}
	}
	
	//Si por lo menos una de las condiciones de migración activó la bandera, entonces se debe procesar la encuesta para agregar/corregir lo que
	//faltara
	if ($blnValid) {
		$arrSteps = array_keys($arrStepsRequired);
		echo("<BR>***** Survey requires migration. Required steps: ".implode(', ', $arrSteps));
		
		$arrValidSurveys[$intSurveyID] = $intSurveyID;
		$arrSurveyStats[$intSurveyID]['RequiredMigration'] = true;
	}
}

echo("<BR><BR>\r\n\r\n");

$aGblModelInstance = null;
$aGblSurveyInstanceDim = null;
$aGblSurveyGlobalInstanceDim = null;
$aGblSurveySingleRecordInstanceDim = null;
//*************************************************************************************************************************
//Inicia el proceso de migración de las encuestas
//*************************************************************************************************************************
foreach ($objSurveyColl->Collection as $objSurvey) {
	$intSurveyID = $objSurvey->SurveyID;
	$blnValid = (bool) @$arrValidSurveys[$intSurveyID];
	echo("<BR><BR>");
	if ($blnValid) {
		echo("<BR>***** Survey: '{$objSurvey->SurveyName}'");
	}
	else {
		echo("<BR>***** Survey '{$objSurvey->SurveyName}' is already migrated, this process is not required");
		continue;
	}
	
	$objQuestionColl = @$arrQuestionsBySurvey[$intSurveyID];
	if (is_null($objQuestionColl)) {
		echo(sendUpgradeMessage("***** Error: No questions found for survey '($objSurvey->SurveyName)'"));
		continue;
	}
	$objSectionColl = $arrSectionsBySurvey[$intSurveyID];
	if (is_null($objSectionColl)) {
		echo(sendUpgradeMessage("***** Error: No sections found for survey '($objSurvey->SurveyName)'"));
		continue;
	}
	
	$strSurveyTable = $objSurvey->SurveyTable;
	$strFactTable = $objSurvey->FactTable;
	//Si esta variable no se cambia a true al final del proceso, entonces si se convertirá la encuesta en su totalidad al nuevo formato
	$blnError = false;
	
	//*************************************************************************************************************************
	//Estadísticas iniciales
	//*************************************************************************************************************************
	$intTotalRows = 0;
	$sql = "SELECT COUNT(*) AS 'TotalRows' FROM {$strSurveyTable}";
	echo(sendUpgradeMessage($sql, "font-style: italic;"));
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$intTotalRows = (int) @$aRS->fields["totalrows"];
	}
	$arrSurveyStats[$intSurveyID]['TotalRowsSurveyTable'] = $intTotalRows;

	$intTotalRows = 0;
	$sql = "SELECT COUNT(*) AS 'TotalRows' FROM {$strFactTable}";
	echo(sendUpgradeMessage($sql, "font-style: italic;"));
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$intTotalRows = (int) @$aRS->fields["totalrows"];
	}
	$arrSurveyStats[$intSurveyID]['TotalRowsFactTable'] = $intTotalRows;
	
	//*************************************************************************************************************************
	//Creación de los campos generales faltantes para soportar la nueva funcionalidad
	//*************************************************************************************************************************
	if (!$objSurvey->UseSourceFields) {
		//Crear campo EntrySectionID
		echo("<BR>***** Adding EntrySectionID field");
		$sql = "ALTER TABLE ".$strSurveyTable." ADD EntrySectionID INT NULL";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			echo("<BR>Field already exists");
		}
		else {
			echo("<BR>EntrySectionID field added");
		}
		
		$sql = "UPDATE ".$strSurveyTable." SET EntrySectionID = -1 WHERE EntrySectionID IS NULL";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			echo("<BR>Updating EntrySectionID for previos eForms versions");
		}
	
		//Crear campo EntryCatDimID
		echo("<BR>***** Adding EntryCatDimID field");
		$sql = "ALTER TABLE ".$strSurveyTable." ADD EntryCatDimID INT NULL";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			echo("<BR>Field already exists");
		}
		else {
			echo("<BR>EntryCatDimID field added");
		}
		
		$sql = "UPDATE ".$strSurveyTable." SET EntryCatDimID = -1 WHERE EntryCatDimID IS NULL";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			echo("<BR>Updating EntryCatDimID for previous eForms versions");
		}
	}
	else {
		echo("<BR>EntrySectionID and EntryCatDimID fields already exists");
	}
	
	//Crear campo eFormsVersionNum
	echo("<BR>***** Adding eFormsVersionNum field");
	$sql = "ALTER TABLE ".$strSurveyTable." ADD eFormsVersionNum VARCHAR(10) NULL";
	echo(sendUpgradeMessage($sql, "color: blue;"));
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>Field already exists");
	}
	else {
		echo("<BR>eFormsVersionNum field added");
	}
	
	$sql = "UPDATE ".$strSurveyTable." SET eFormsVersionNum = '' WHERE eFormsVersionNum IS NULL";
	echo(sendUpgradeMessage($sql, "color: blue;"));
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>Updating eFormsVersionNum for previous eForms versions");
	}
	
	if (!$objSurvey->UseDynamicPageField) {
		//Crear campo DynamicPageDSC
		echo("<BR>***** Adding DynamicPageDSC field");
		$sql = "ALTER TABLE ".$strSurveyTable." ADD DynamicPageDSC VARCHAR(255) NULL";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			echo("<BR>Field already exists");
		}
		else {
			echo("<BR>DynamicPageDSC field added");
		}
		
		$sql = "UPDATE ".$strSurveyTable." SET DynamicPageDSC = '' WHERE DynamicPageDSC IS NULL";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			echo("<BR>Updating DynamicPageDSC for previous eForms versions");
		}
	}
	else {
		echo("<BR>DynamicPageDSC field already exists");
	}
	
	if (!$objSurvey->UseDynamicOptionField) {
		//Crear campo DynamicOptionDSC
		echo("<BR>***** Adding DynamicOptionDSC field");
		$sql = "ALTER TABLE ".$strSurveyTable." ADD DynamicOptionDSC VARCHAR(255) NULL";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			echo("<BR>Field already exists");
		}
		else {
			echo("<BR>DynamicOptionDSC field added");
		}
		
		$sql = "UPDATE ".$strSurveyTable." SET DynamicOptionDSC = '' WHERE DynamicOptionDSC IS NULL";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			echo("<BR>Updating DynamicOptionDSC for previous eForms versions");
		}
		
		//Crear campo DynamicValue
		echo("<BR>***** Adding DynamicValue field");
		//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
		$sql = "ALTER TABLE ".$strSurveyTable." ADD DynamicValue LONGTEXT NULL";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			echo("<BR>Field already exists");
		}
		else {
			echo("<BR>DynamicValue field added");
		}
		
		$sql = "UPDATE ".$strSurveyTable." SET DynamicValue = '' WHERE DynamicValue IS NULL";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			echo("<BR>Updating DynamicValue for previous eForms versions");
		}
	}
	else {
		echo("<BR>DynamicOptionDSC and DynamicValue fields already exists");
	}
	
	//*************************************************************************************************************************
	//Asociar las dimensiones independientes de cada catálogo utilizado en la encuesta hasta el último atributo requerido
	//*************************************************************************************************************************
	echo("<BR><BR>***** Adding catalog dimensions to survey's model");
	//Si hay sección dinámica en la encuesta, entonces el último atributo usado dependería no de la última pregunta del mismo catálogo
	//sino de si tiene o no pregunta múltiple choice en la sección dinámica de dicho catálogo
	$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $intSurveyID);
	$dynamicSectionCatID = 0;
	$dynamicSectionCatMemberID = 0;
	$dynamicSectionChildCatMemberID = 0;
	$dynamicSection = null;
	$aSCDynQuestionInstance = null;
	if ($dynamicSectionID > 0) {
		$dynamicSection = BITAMSection::NewInstanceWithID($aRepository, $dynamicSectionID);
		if (!is_null($dynamicSection)) {
			$dynamicSectionCatID = $dynamicSection->CatalogID;
			$dynamicSectionCatMemberID = $dynamicSection->CatMemberID;
			$dynamicSectionChildCatMemberID = $dynamicSection->ChildCatMemberID;
			
			//Obtiene la última pregunta del mismo catálogo de la dinámica para saber exactamente hasta que atributo se usa antes de la dinámica
			//En la práctica durante el proceso de migración, esto solo es necesario para saber si la pregunta ya fué previamente migrada a
			//catálogos desasociados, porque si ya lo hizo entonces ya no es posible aplicar el mismo join con la tabla del catálogo pues los
			//valores no necesariamente coincidirían
			$blnDynamicSectionFound = false;
			foreach ($objQuestionColl->Collection as $objQuestion) {
				//@JAPR 2012-06-08: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
				if (!$blnDynamicSectionFound)
				{
					if ($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID == $dynamicSectionCatID)
					{
						//Como pudiera haber varias preguntas de catálogo sincronizadas, debemos tomar la última para saber exactamente por qué atributo 
						//se debe filtrar
						$aSCDynQuestionInstance = $objQuestion;
					}
				}
				
				//si las preguntas pertenecen a la seccion dinamica y que estan dentro de la seccion
				//entonces si son repetibles y se ignoran dichas preguntas
				if($objQuestion->SectionID == $dynamicSectionID)
				{
					$blnDynamicSectionFound = true;
					break;
				}
			}
		}
	}
	
	$arrCatalogIDs = @$arrCatalogsBySurvey[$intSurveyID];
	if (is_null($arrCatalogIDs) || !is_array($arrCatalogIDs)) {
		$arrCatalogIDs = array();
	}
	
	foreach ($arrCatalogIDs as $intCatalogID) {
		$objCatalog = @$arrCatalogsByID[$intCatalogID];
		if (is_null($objCatalog)) {
			echo(sendUpgradeMessage("***** Error: Catalog not found ($intCatalogID)"));
			$blnError = true;
			continue;
		}
		echo("<BR>***** Catalog: ".$objCatalog->CatalogName);
		
		$intCatMemberID = 0;
		if ($intCatalogID == $dynamicSectionCatID) {
			//Si es el catálogo de la sección dinámica, teoricamente no debería existir ninguna pregunta de catálogo posterior a la sección, así
			//que se reutilizan directamente hasta los atributos de la sección nada mas
			if ($dynamicSectionChildCatMemberID > 0) {
				$intCatMemberID = $dynamicSectionChildCatMemberID;
			}
			else {
				$intCatMemberID = $dynamicSectionCatMemberID;
			}
		}
		else {
			//En este caso se identifica si hay alguna pregunta simple choice que utilice este catálogo y se obtiene el último atributo usado,
			//como las preguntas se cargan en orden basta con obtener la última ocurrencia del mismo catálogo
			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID == $intCatalogID) {
					if ($objQuestion->CatMemberID > 0) {
						$intCatMemberID = $objQuestion->CatMemberID;
					}
				}
			}
		}
		
		if ($intCatMemberID > 0) {
			echo("<BR>***** Adding dimensions for '{$objSurvey->SurveyName}' up to attribute ID {$intCatMemberID}");
			//@JAPR 2013-06-17: Agregado el proceso de migración a catálogos independientes
			//Agregado el parámetro $bForceAssoc para que aunque no se consideren convertidas las encuestas, de todas formas asocie sus atributos
			$intResult = $objCatalog->addDissociatedCatalogDimensions($intSurveyID, $intCatMemberID, true);
			if ($intResult <= 0) {
				$blnError = true;
			}
			echo("<BR>***** Answers: {$intResult}");
			$arrSurveyStats[$intSurveyID]['LastMemberAddedForCatalog'.$intCatalogID] = $intResult;
		}
	}
	
	//*************************************************************************************************************************
	//Creación de los campos cat_ para las preguntas que son de tipo catálogo
	//*************************************************************************************************************************
	echo("<BR><BR>***** Adding Cat_ fields");
	foreach ($objQuestionColl->Collection as $objQuestion) {
		if ($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID > 0) {
			echo("<BR>***** Adding {$objQuestion->SurveyCatField} field");
			//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
			$sql = "ALTER TABLE ".$strSurveyTable." ADD ".$objQuestion->SurveyCatField." LONGTEXT NULL";
			echo(sendUpgradeMessage($sql, "color: blue;"));
			if ($aRepository->DataADOConnection->Execute($sql) === false) {
				echo("<BR>Field already exists");
			}
			else {
				echo("<BR>{$objQuestion->SurveyCatField} field added");
			}
		}
	}
	
	//Validaciones previas
    $arrMasterDetSections = BITAMSection::existMasterDetSections($aRepository, $intSurveyID, true);
    if (!is_array($arrMasterDetSections) || count($arrMasterDetSections) == 0) {
    	$arrMasterDetSections = array();
    }
	$blnHastMasterDetSections = count($arrMasterDetSections) > 0;
	$arrMasterDetSectionsIDs = array_flip($arrMasterDetSections);
	
	//*************************************************************************************************************************
	//Llenado del campo EntryCatDimID
	//*************************************************************************************************************************
	echo("<BR><BR>***** Updating EntryCatDimID field");
	$blnContinue = false;
	
	//Validaciones previas
	//Verifica cuantas preguntas hay de tipo categoría de dimensión
	$intNumCategoryDimQuestions = 0;
	$intFirstCategoryDimQuestionID = 0;
	foreach ($objQuestionColl->Collection as $objQuestion) {
		if ($objQuestion->QTypeID == qtpMulti && $objQuestion->QDisplayMode == dspVertical && $objQuestion->MCInputType == mpcNumeric && $objQuestion->IsMultiDimension == 0 && $objQuestion->UseCategoryDimChoice != 0) {
			$blnContinue = true;
			$intNumCategoryDimQuestions++;
			if ($intFirstCategoryDimQuestionID == 0) {
				$intFirstCategoryDimQuestionID = $objQuestion->QuestionID;
			}
		}
	}
	
	$intTotalRows = 0;
	$sql = "SELECT COUNT(*) AS 'TotalRows' 
		FROM {$strSurveyTable} 
		WHERE EntryCatDimID = -1 OR EntryCatDimID IS NULL";
	echo(sendUpgradeMessage($sql, "font-style: italic;"));
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$intTotalRows = (int) @$aRS->fields["totalrows"];
	}
	$arrSurveyStats[$intSurveyID]['EntryCatDimIDEmptyRows'] = $intTotalRows;
	
	//Si hay preguntas categoría de dimensión y hay por lo menos un registro con este campo no asignado, así que tiene que actualizarlo
	if ($blnContinue && $intTotalRows > 0) {
		//@JAPRWarning: Por ahora no se soportará actualizar encuestas donde no esté totalmente asignado el campo EntryCatDimID, ya que para
		//v4 no se grabaron correctamente los registros en la tabla de categorías de dimensión, por lo que es complicado determinarlo, se
		//tendrá que correr un proceso previo a este el cual deberá basarse en los casos donde si se hubiera grabado dicha tabla para hacer
		//join con la paralela y actualizar los valores, pero en los casos donde no se hubiera grabado se deberá de obtener el total de opciones
		//de respuesta de cada una de estas preguntas e ir cargando captura por captura en el orden de los FactKeys para asignar cada registro
		//a las preguntas de categoría de dimensión por número de pregunta hasta que se agoten la cantidad de respuestas, ya que así es como
		//se grababan
		
		//Por ahora sólo es posible continuar con la actualización si además hubiera secciones dinámicas y/o maestro-detalle, ya que en esos
		//casos las preguntas tipo categoría de dimensión de todas formas se hubieran grabado mal porque no se soportaban juntas
		if (!$blnHastMasterDetSections && $dynamicSectionID <= 0) {
			//No hay secciones múltiples, así que por lo pronto en caso de haber un EntrySectionID no asignado, no se puede continuar, a menos
			//que sólo exista una pregunta de tipo categoría de dimensión, ya que en ese caso todos los registros hubieran pertenecido a ella
			if ($intNumCategoryDimQuestions == 1 && $intFirstCategoryDimQuestionID > 0) {
				//Todos los registros faltantes deben ser de la única pregunta categoría de dimensión
				$sql = "UPDATE {$strSurveyTable} SET 
						EntryCatDimID = {$intFirstCategoryDimQuestionID} 
					WHERE EntryCatDimID = -1 OR EntryCatDimID IS NULL";
				echo(sendUpgradeMessage($sql, "color: blue;"));
				if ($blnExecuteProcess) {
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						$blnError = true;
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						continue;	//Este es un error crítico, así que no puede continuar
					}
					else {
						$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
						$arrSurveyStats[$intSurveyID]['EntryCatDimIDAffectedRows'] = $intAffectedRows;
					}
				}
			}
			else {
				echo(sendUpgradeMessage("***** Error: Migration of surveys with multiple category dimension questions is not supported at this time"));
				$blnError = true;
				continue;
			}
		}
		else {
			echo("<BR>***** The survey has sections with multiple rows, category dimension questions are not supported in this context, this field will be reset to 0");
			//En este caso, como hay secciones Dinámicas y/o Maestro-detalle y eso no se soporta hasta la fecha de implementación de este
			//proceso, simplemente se actualizará el campo para asignar un valor de 0 porque ninguno de los registros teóricamente pudo haber
			//sido realmente un Categoría de Dimensión
			$sql = "UPDATE {$strSurveyTable} SET 
					EntryCatDimID = 0 
				WHERE EntryCatDimID = -1 OR EntryCatDimID IS NULL";
			echo(sendUpgradeMessage($sql, "color: blue;"));
			if ($blnExecuteProcess) {
				if ($aRepository->DataADOConnection->Execute($sql) === false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
					continue;	//Este es un error crítico, así que no puede continuar
				}
				else {
					$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
					$arrSurveyStats[$intSurveyID]['EntryCatDimIDAffectedRows'] = $intAffectedRows;
				}
			}
		}
	}
	else {
		if ($intTotalRows > 0) {
			//Simplemente se limpia el contenido del campo ya que no existen categorías de dimensión
			$sql = "UPDATE {$strSurveyTable} SET 
					EntryCatDimID = 0 
				WHERE EntryCatDimID = -1 OR EntryCatDimID IS NULL";
			echo(sendUpgradeMessage($sql, "color: blue;"));
			if ($blnExecuteProcess) {
				if ($aRepository->DataADOConnection->Execute($sql) === false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
					continue;	//Este es un error crítico, así que no puede continuar
				}
				else {
					$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
					$arrSurveyStats[$intSurveyID]['EntryCatDimIDAffectedRows'] = $intAffectedRows;
				}
			}
		}
		
		if ($blnContinue) {
			echo("<BR>***** This field is already updated");
		}
		else {
			echo("<BR>***** This survey do not has a category dimension questions, the process is not required");
		}
	}
	
	//*************************************************************************************************************************
	//Llenado del campo EntrySectionID
	//*************************************************************************************************************************
	/* Este proceso es tal vez el mas importante y riesgozo, si se determina mal la sección a la que pertenece un registro, el resto del proceso
	no actualizará correctamente los campos clave que requerirá para editar dicha captura. Hay condiciones simples que se pueden convertir
	directamente, como por ejemplo cuando la encuesta no tiene ninguna sección múltiple o si sólo hay una sección maestro-detalle, ya que en esos
	casos sólo hay un registro que sería el estándar, o bien se puede asumir que cualquier registro pertenecería a la sección maestro-detalle, y 
	en caso de haber omitido dicha sección lo peor que pasaría sería duplicar el registro único y se agregaría un registro en blanco para la 
	maestro-detalle, pero si se combinan múltiples secciones de estos tipos entonces se complica determinar a quien corresponde cada registro
	*/
	echo("<BR><BR>***** Updating EntrySectionID field");
	$blnContinue = ($blnHastMasterDetSections || $dynamicSectionID > 0);
	
	$intTotalRows = 0;
	$sql = "SELECT COUNT(*) AS 'TotalRows' 
		FROM {$strSurveyTable} 
		WHERE EntrySectionID = -1 OR EntrySectionID IS NULL";
	echo(sendUpgradeMessage($sql, "font-style: italic;"));
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$intTotalRows = (int) @$aRS->fields["totalrows"];
	}
	$arrSurveyStats[$intSurveyID]['EntrySectionIDEmptyRows'] = $intTotalRows;
	
	//Si hay secciones múltiples y hay por lo menos un registro con este campo no asignado, así que tiene que actualizarlo
	if ($blnContinue && $intTotalRows > 0) {
		/* Hay por lo menos una sección maestro-detalle o dinámica, así que se recorrerán una a una para verificar si tienen alguna respuesta
		contestada de sus preguntas, si estas secciones tienen alguna pregunta de catálogo (en el caso de las maestro-detalle) o simple choice,
		se les dará prioridad a ellas ya que si la respuesta es <> de 1 para catálogos (obviamente los no migrados) o diferente de '' para las
		simple choice, entonces es seguro que hay una respuesta puesto que dichas preguntas no soportaban grabar vacios. De no contener estos
		tipos de preguntas entonces el proceso se hará mas complicado y tendrá que revisar el resto de los tipos de preguntas sin importar
		el tipo, ya que todas tienen las mismas probabilidades de estar o no contestadas y grabadas de acuerdo a esto. No se considerarán las
		fotos ni los comentarios en primera instancia, porque en secciones con múltiples registros no estaban correctamente implementados así
		que no hay confiabilidad en ese dato
		*/
		
		$arrEntryReportIDs = array();
		$sql = "SELECT DISTINCT a.FactKeyDimVal 
			FROM {$strSurveyTable} a 
			WHERE a.EntrySectionID = -1 OR a.EntrySectionID IS NULL 
			ORDER BY a.FactKeyDimVal";
		echo(sendUpgradeMessage($sql, "font-style: italic;"));
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS == false) {
			$blnError = true;
			echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
			continue;	//Este es un error crítico, así que no puede continuar
		}
		
		while(!$aRS->EOF) {
			$intFactKeyDimVal = (int) @$aRS->fields["factkeydimval"];
			if ($intFactKeyDimVal > 0) {
				$arrEntryReportIDs[] = $intFactKeyDimVal;
			}
			$aRS->MoveNext();
		}
		
		if (count($arrEntryReportIDs) > 0) {
			//Prepara el SQL para cargar los datos de las preguntas que ayudará en la toma de decisión, pero sólo de aquellas secciones que
			//se consideran múltiples
			$strSqlSELECT = "FactKey, EntrySectionID, EntryCatDimID";
			$arrQuestionsBySectionID = array();
			$arrSCHQuestionsBySectionID = array();
			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->SectionType == sectDynamic || $objQuestion->SectionType == sectMasterDet) {
					$blnValidQuestion = true;
					switch ($objQuestion->QTypeID) {
						case qtpShowValue:
						case qtpPhoto:
						case qtpDocument:
						case qtpSketch:
						case qtpAudio:
						case qtpVideo:
						case qtpSignature:
						case qtpMessage:
						case qtpSkipSection:
						//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
						case qtpSync:
						//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
						case qtpPassword:
						//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
						case qtpSection:
						//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
						case qtpExit:
							$blnValidQuestion = false;
							break;
						
						default:
							break;
					}
					
					if (!$blnValidQuestion) {
						continue;
					}
					
					$intSectionID = $objQuestion->SectionID;
					if ($objQuestion->QTypeID == qtpSingle || $objQuestion->QTypeID == qtpCallList) {
						if (!isset($arrSCHQuestionsBySectionID[$intSectionID])) {
							$arrSCHQuestionsBySectionID[$intSectionID] = array();
						}
						$arrSCHQuestionsBySectionID[$intSectionID][] = $objQuestion;
					}
					else {
						if (!isset($arrQuestionsBySectionID[$intSectionID])) {
							$arrQuestionsBySectionID[$intSectionID] = array();
						}
						$arrQuestionsBySectionID[$intSectionID][] = $objQuestion;
					}
					
					$strSqlSELECT .= ', '.$objQuestion->SurveyField;
				}
			}
			
			//Empieza a actualizar una a una las capturas que aun tienen registros no identificados
			foreach ($arrEntryReportIDs as $intFactKeyDimVal) {
				echo("<BR>***** Updating EntrySectionID field for Entry # {$intFactKeyDimVal}");
				
				//Identifica si hay registro único
				$blnHasSingleStandarRecord = false;
				$sql = "SELECT COUNT(*) AS 'TotalRows' 
					FROM {$strSurveyTable} 
					WHERE FactKeyDimVal = {$intFactKeyDimVal} AND EntrySectionID = 0";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS == false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				
				if (!$aRS->EOF) {
					$blnHasSingleStandarRecord = ((int) @$aRS->fields["totalrows"]) > 0;
				}
				
				//Obtiene los FactKeys de esta encuesta que tienen el problema
				/*
				$sql = "SELECT FactKey 
					FROM {$strSurveyTable} 
					WHERE FactKeyDimVal = {$intFactKeyDimVal} AND (EntrySectionID = -1 OR EntrySectionID IS NULL)";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS == false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				*/
				
				/*
				$arrEntryFactKeys = array();
				while(!$aRS->EOF) {
					$intFactKey = (int) @$aRS->fields["factkey"];
					if ($intFactKey > 0) {
						$arrEntryFactKeys[] = $intFactKey;
					}
					$aRS->MoveNext();
				}
				*/
				
				//Recorre todos los FactKeys encontrados y carga los datos para identificar el tipo de registro del que se trata
				$sql = "SELECT {$strSqlSELECT} 
					FROM {$strSurveyTable} 
					WHERE FactKeyDimVal = {$intFactKeyDimVal} AND (EntrySectionID = -1 OR EntrySectionID IS NULL) 
					ORDER BY FactKey";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS == false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				
				$blnCanCheckDynSection = ($dynamicSectionID > 0 && (isset($arrQuestionsBySectionID[$dynamicSectionID]) || isset($arrSCHQuestionsBySectionID[$dynamicSectionID])));
				while(!$aRS->EOF) {
					$intSectionIDForRow = 0;
					$intFactKey = (int) @$aRS->fields["factkey"];
					echo("<BR>***** Updating EntrySectionID field for FactKey # {$intFactKey}");
					
					$arrayData = array();
					foreach ($aRS->fields as $strFieldName => $strFieldValue) {
						$arrayData[strtolower($strFieldName)] = $strFieldValue;
					}
					
					//Primero intenta identificar el registro como de la sección dinámica si es que hay una
					if ($blnCanCheckDynSection) {
						$intSectionID = $dynamicSectionID;
						//Si hay preguntas simple choice, se les da prioridad a esas
						foreach ($arrSCHQuestionsBySectionID[$intSectionID] as $objQuestion) {
							$strFieldName = strtolower($objQuestion->SurveyField);
							$strFieldValue = @$arrayData[$strFieldName];
							
							//Con una sola pregunta simple choice que no tenga el valor de NA o vacio/null, se asumirá que esta es su sección
							if (!is_null($strFieldValue) && trim((string) $strFieldValue) != '') {
								if ($objQuestion->CatalogID > 0) {
									//Si es una pregunta de catálogo, estaría grabando los keys subrrogados así que se busca el del NA
									if (trim($strFieldValue) != '1') {
										$intSectionIDForRow = $intSectionID;
									}
								}
								else {
									//En preguntas simple choice normales se graba directamente la descripción
									$intSectionIDForRow = $intSectionID;
								}
							}
						}
						
						if (!$intSectionIDForRow) {
							//Intenta con el resto de las preguntas
							foreach ($arrQuestionsBySectionID[$intSectionID] as $objQuestion) {
								$strFieldName = strtolower($objQuestion->SurveyField);
								$strFieldValue = @$arrayData[$strFieldName];
								//Si el valor es null, no tiene caso continuar
								if (is_null($strFieldValue)) {
									continue;
								}
								
								//Verifica que no se trate del default valido según el tipo de pregunta, si lo es entonces no se considera contestada
								switch ($objQuestion->QTypeID) {
									case qtpOpenNumeric:
									case qtpCalc:
										//Las preguntas numéricas y calculadas guardaban por default 0
										if ((int) $strFieldValue != 0) {
											$intSectionIDForRow = $intSectionID;
										}
										break;
										
									case qtpOpenDate:
									case qtpOpenString:
									case qtpOpenAlpha:
									case qtpOpenTime:
									//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
									case qtpGPS:
									//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
									case qtpBarCode:
										//Las preguntas que responden texto graban vacio por default
										if (trim($strFieldValue) != '') {
											$intSectionIDForRow = $intSectionID;
										}
										break;
										
									case qtpAction:
										//Las preguntas tipo acción se responden con un texto con formato pero con separadores, si el 4to elemento
										//está asignado entonces si está respondida ya que eso representa la acción y no se valen acciones vacias
										//3_SVSep_2012-09-25_SVSep_4_SVSep_Jugar
										if (trim($strFieldValue) != '') {
											$arrActionPieces = explode('_SVSep_', $strFieldValue);
											if (count($arrActionPieces) >= 4) {
												if (trim($arrActionPieces[3]) != '') {
													$intSectionIDForRow = $intSectionID;
												}
											}
										}
										break;
										
									case qtpMulti:
										//Las preguntas múltiple choice en dinámicas se graban dependiendo del tipo de captura
										switch ($objQuestion->MCInputType) {
											case mpcCheckBox:
												//CheckBox == 1 | 0 (marcada | desmarcada). Sólo se puede saber si se contestó si está marcada, 
												//no cuando está desmarcada porque pudiera ser el default
											case mpcNumeric:
												//Numérica == Número capturado. Tal como las numéricas/calculadas, graban un 0 por default
												if ((int) $strFieldValue != 0) {
													$intSectionIDForRow = $intSectionID;
												}
												break;
											
											default:
												//Texto == Texto capturado. Tal como las alfanuméricas graban '' por default
												if (trim($strFieldValue) != '') {
													$intSectionIDForRow = $intSectionID;
												}
												break;
										}
										break;
								}
								
								//Si ya se identificó el registro, no tiene caso continuar con el resto de las preguntas
								if ($intSectionIDForRow) {
									break;
								}
							}
						}
					}
					
					//Si después de la sección dinámica no se logró identificar que el registro pertenece a ella, intenta con las secciones maestro-detalle
					if (!$intSectionIDForRow) {
						foreach ($arrMasterDetSections as $intSectionID) {
							//Si hay preguntas simple choice, se les da prioridad a esas
							foreach ($arrSCHQuestionsBySectionID[$intSectionID] as $objQuestion) {
								$strFieldName = strtolower($objQuestion->SurveyField);
								$strFieldValue = @$arrayData[$strFieldName];
								
								//Con una sola pregunta simple choice que no tenga el valor de NA o vacio/null, se asumirá que esta es su sección
								if (!is_null($strFieldValue) && trim($strFieldValue) != '') {
									if ($objQuestion->CatalogID > 0) {
										//Si es una pregunta de catálogo, estaría grabando los keys subrrogados así que se busca el del NA
										if (trim($strFieldValue) != '1') {
											$intSectionIDForRow = $intSectionID;
										}
									}
									else {
										//En preguntas simple choice normales se graba directamente la descripción
										$intSectionIDForRow = $intSectionID;
									}
								}
							}
							
							if (!$intSectionIDForRow) {
								//Intenta con el resto de las preguntas
								foreach ($arrQuestionsBySectionID[$intSectionID] as $objQuestion) {
									$strFieldName = strtolower($objQuestion->SurveyField);
									$strFieldValue = @$arrayData[$strFieldName];
									//Si el valor es null, no tiene caso continuar
									if (is_null($strFieldValue)) {
										continue;
									}
									
									//Verifica que no se trate del default valido según el tipo de pregunta, si lo es entonces no se considera contestada
									switch ($objQuestion->QTypeID) {
										case qtpOpenNumeric:
										case qtpCalc:
											//Las preguntas numéricas y calculadas guardaban por default 0
											if ((int) $strFieldValue != 0) {
												$intSectionIDForRow = $intSectionID;
											}
											break;
											
										case qtpOpenDate:
										case qtpOpenString:
										case qtpOpenAlpha:
										case qtpOpenTime:
										//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
										case qtpGPS:
										//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
										case qtpBarCode:
											//Las preguntas que responden texto graban vacio por default
											if (trim($strFieldValue) != '') {
												$intSectionIDForRow = $intSectionID;
											}
											break;
											
										case qtpAction:
											//Las preguntas tipo acción se responden con un texto con formato pero con separadores, si el 4to elemento
											//está asignado entonces si está respondida ya que eso representa la acción y no se valen acciones vacias
											//3_SVSep_2012-09-25_SVSep_4_SVSep_Jugar
											if (trim($strFieldValue) != '') {
												$arrActionPieces = explode('_SVSep_', $strFieldValue);
												if (count($arrActionPieces) >= 4) {
													if (trim($arrActionPieces[3]) != '') {
														$intSectionIDForRow = $intSectionID;
													}
												}
											}
											break;
											
										case qtpMulti:
											//Las preguntas múltiple choice en maestro-detalle se graban dependiendo del tipo de captura
											switch ($objQuestion->MCInputType) {
												case mpcCheckBox:
													//CheckBox == Lista separada por ; de las opciones seleccionadas solamente
													if (trim($strFieldValue) != '') {
														$intSectionIDForRow = $intSectionID;
													}
													break;
													
												case mpcNumeric:
													//Numérica == Lista separada por ; de los números capturados pero incluyendo el total de
													//opciones existentes en la captura, llenando sus espacios con vacio
												default:
													//Texto == Lista separada por ; de los textos capturados  pero incluyendo el total de
													//opciones existentes en la captura, llenando sus espacios con vacio
													
													//En estos casos primero se convierten todas las ";" en vacio y luego hace un trim, si quedó
													//algún texto entonces si está contestada ya que no se permiten opciones vacias
													$strFieldValue = str_replace(';', '', $strFieldValue);
													if (trim($strFieldValue) != '') {
														$intSectionIDForRow = $intSectionID;
													}
													break;
											}
											break;
									}
									
									//Si ya se identificó el registro, no tiene caso continuar con el resto de las preguntas
									if ($intSectionIDForRow) {
										break;
									}
								}
							}
						}
					}
					
					//Actualiza el EntrySectionID basado en el que encontró, si no hubiera identificado el registro, lo actualizará al registro
					//único pues significaría que la pregunta no pertenecía a ninguna sección, sin embargo si ya había un registro único entonces
					//generará un error porque no podrían existir mas de uno
					if ($intSectionIDForRow > 0) {
						$sql = "UPDATE {$strSurveyTable} 
								SET EntrySectionID = {$intSectionIDForRow} 
							WHERE FactKey = {$intFactKey}";
						echo(sendUpgradeMessage($sql, "color: blue;"));
						if ($blnExecuteProcess) {
							if ($aRepository->DataADOConnection->Execute($sql) === false) {
								$blnError = true;
								echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
								continue 3;	//Este es un error crítico, así que no puede continuar
							}
						}
					}
					else {
						if (!$blnHasSingleStandarRecord) {
							$sql = "UPDATE {$strSurveyTable} 
									SET EntrySectionID = 0 
								WHERE FactKey = {$intFactKey}";
							echo(sendUpgradeMessage($sql, "color: blue;"));
							if ($blnExecuteProcess) {
								if ($aRepository->DataADOConnection->Execute($sql) === false) {
									$blnError = true;
									echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
									continue 3;	//Este es un error crítico, así que no puede continuar
								}
							}
							
							//Se marca el registro como actualizado al único por si en el mismo reporte se intentara actualizar un 2do registro,
							//esto generaria un error antes de dañar aun mas la captura. Esta situación es posible cuando la encuesta está
							//diseñada de manera que no es fácil identificar si el registro pertenece o no a la sección múltiple que se está 
							//procesando, lo cual llevó a asignarlo incorrectamente como el de la sección estándar
							$blnHasSingleStandarRecord = true;
						}
						else {
							$blnError = true;
							echo(sendUpgradeMessage("***** This entry row failed to identify the section which stored it's data, unable to continue with this process"));
							continue 3;	//Este es un error crítico, así que no puede continuar
							//Este tipo de error debe validarse manualmente para ajustar el proceso de identificación, ya que no se puede continuar si
							//este campo está mal asignado, por tanto se interrumpirá el proceso completo para esta encuesta. El continue 3 asegura
							//que continuará en la siguiente encuesta brincando lo que faltara por migrar de esta
						}
					}
					$aRS->MoveNext();
				}
			}
		}
		else {
			echo("<BR>***** This survey has a every entry associated with it's respective section, the process is not required");
		}
	}
	else {
		if ($intTotalRows > 0) {
			//Simplemente se limpia el contenido del campo ya que no existen secciones múltiples, por lo que todos los registros serían el registro
			//único de secciones estándar
			$sql = "UPDATE {$strSurveyTable} SET 
					EntrySectionID = 0 
				WHERE EntrySectionID = -1 OR EntrySectionID IS NULL";
			echo(sendUpgradeMessage($sql, "color: blue;"));
			if ($blnExecuteProcess) {
				if ($aRepository->DataADOConnection->Execute($sql) === false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
					continue;	//Este es un error crítico, así que no puede continuar
				}
				else {
					$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
					$arrSurveyStats[$intSurveyID]['EntrySectionIDAffectedRows'] = $intAffectedRows;
				}
			}
		}
		
		if ($blnContinue) {
			echo("<BR>***** This field is already updated");
		}
		else {
			echo("<BR>***** This survey do not has sections with multiple rows, the process is not required");
		}
	}
	
	//*************************************************************************************************************************
	//Agregar el registro único para las secciones estándar
	//*************************************************************************************************************************
	/* A partir de este punto, el resto de los procesos generalmente se basarán en el campo EntrySectionID y EntryCatDimID para decidir lo que
	tienen que hacer, en este caso el registro único, si es necesario agregarlo, se hará tomando los valores de las preguntas de secciones estándar
	del último registro agregado para cada captura, pero no se actualizarán los valores de preguntas numéricas a NULL en cualquier otro registro
	para evitar perder cualquier tipo de información en caso de que algo no salga correctamente (esto sólo afectaría si existieran secciones
	maestro-detalle y/o dinámicas)
	*/
	echo("<BR><BR>***** Adding single standard record");
	
	//Validaciones previas
	$blnContinue = ($blnHastMasterDetSections || $dynamicSectionID > 0);
	/*Primero hay que obtener los IDs de las capturas que tienen este problema, ya que se debe actualizar registro a registro para cada una,
	para esto la forma mas óptima consiste en crear una tabla temporal a la cual se le agregarán índices, para posteriormente hacer join
	con la tabla paralela a la cual también se le crearán índices (ya que no los tenía a la fecha de esta implementación), así se podrá
	identificar que registros de la paralela no se encuentran entre aquellos de la temporal que son los que si tienen registro único grabado
	*/
	//Primero crea el Primary Key y los índices a la tabla Paralela
	$sql = "ALTER TABLE {$strSurveyTable} ADD PRIMARY KEY (FactKey)";
	echo(sendUpgradeMessage($sql, "color: blue;"));
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
		//continue;	//Si no se puede crear, es porque posiblemente ya existía así que se ignora
	}
	
	$sql = "ALTER TABLE {$strSurveyTable} ADD INDEX IX{$strSurveyTable}DateID (DateID)";
	echo(sendUpgradeMessage($sql, "color: blue;"));
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
		//continue;	//Si no se puede crear, es porque posiblemente ya existía así que se ignora
	}
	
	$sql = "ALTER TABLE {$strSurveyTable} ADD INDEX IX{$strSurveyTable}FactKeyDimVal (FactKeyDimVal)";
	echo(sendUpgradeMessage($sql, "color: blue;"));
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
		//continue;	//Si no se puede crear, es porque posiblemente ya existía así que se ignora
	}
	
	//Crea la tabla temporal que servirá de Join en el query principal
	$sql = "CREATE TEMPORARY TABLE {$strSurveyTable}T 
		SELECT DISTINCT FactKeyDimVal 
  		FROM {$strSurveyTable} 
  		WHERE EntrySectionID = 0";
	echo(sendUpgradeMessage($sql, "color: blue;"));
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		$blnError = true;
		echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
		continue;	//Este es un error crítico, así que no puede continuar
	}
	
	$sql = "ALTER TABLE {$strSurveyTable}T ADD PRIMARY KEY (FactKeyDimVal)";
	echo(sendUpgradeMessage($sql, "color: blue;"));
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		$blnError = true;
		echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
		continue;	//Este es un error crítico, así que no puede continuar
	}
	
	$sql = "ALTER TABLE {$strSurveyTable}T ADD INDEX IX{$strSurveyTable}TFactKeyDimVal (FactKeyDimVal)";
	echo(sendUpgradeMessage($sql, "color: blue;"));
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		$blnError = true;
		echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
		continue;	//Este es un error crítico, así que no puede continuar
	}

	$intTotalRows = 0;
	$sql = "SELECT COUNT(*) AS 'TotalRows' 
		FROM {$strSurveyTable} a 
				LEFT JOIN {$strSurveyTable}T b ON a.FactKeyDimVal = b.FactKeyDimVal 
		WHERE COALESCE(b.FactKeyDimVal, 0) = 0";
	echo(sendUpgradeMessage($sql, "font-style: italic;"));
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$intTotalRows = (int) @$aRS->fields["totalrows"];
	}
	$arrSurveyStats[$intSurveyID]['EntrySectionIDWithNoSingleStdRecord'] = $intTotalRows;
	
	//Si hay secciones múltiples y hay por lo menos una captura sin registro único, así que tiene que agregarlo donde falte
	if ($blnContinue && $intTotalRows > 0) {
		$arrEntryReportIDs = array();
		$sql = "SELECT DISTINCT a.FactKeyDimVal 
			FROM {$strSurveyTable} a 
  				LEFT JOIN {$strSurveyTable}T b ON a.FactKeyDimVal = b.FactKeyDimVal 
			WHERE COALESCE(b.FactKeyDimVal, 0) = 0";
		echo(sendUpgradeMessage($sql, "font-style: italic;"));
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS == false) {
			$blnError = true;
			echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
			continue;	//Este es un error crítico, así que no puede continuar
		}
		
		while(!$aRS->EOF) {
			$intFactKeyDimVal = (int) @$aRS->fields["factkeydimval"];
			if ($intFactKeyDimVal > 0) {
				$arrEntryReportIDs[] = $intFactKeyDimVal;
			}
			$aRS->MoveNext();
		}
		
		if (count($arrEntryReportIDs) > 0) {
			$arrQuestionsBySectionID = array();
			
			/*Ya que se tienen identificadas las capturas donde no existe el registro único, el proceso cargará una a una los datos de todas las
			preguntas simple choice que tuviera para insertarlos nuevamente con un FactKey distinto, sin embargo sólo cargará los datos del
			FactKey mas grande actualmente grabado para cada captura (esto realmente no importa, es sólo por seguir un patrón). Es correcto esto
			porque en el formato original cada registro hubiera contenido el mismo valor para este tipo de preguntas, así que da lo mismo cual
			se utilice para replicar. Se empezará por la tabla de hechos ya que se necesita obtener el siguiente FactKey disponible, continuando
			con las tablas paralelas y demás en el siguiente orden de importancia:
			- RIFACT_####
			- SVSurvey_#### (FactKey y FactKeyDimVal tienen correspondencia con saveData)
				Por pregunta:
					- SVSurveyActions. DESCONTINUADA (Requiere un SurveyID, FactKey tiene correspondencia con saveData pero FactKeyDimVal se graba en SurveyDimVal)
					- SI_SV_SurveyAnswerImage (FactKey se graba en MainFactKey y FactKeyDimVal se graba en FactKey)
					- SI_SV_SurveyAnswerDocument (FactKey se graba en MainFactKey y FactKeyDimVal se graba en FactKey)
					- SI_SV_SurveyAnswerComment (FactKey se graba en MainFactKey y FactKeyDimVal se graba en FactKey)
					- SVSurveyMatrixData_#### (FactKey y FactKeyDimVal tienen correspondencia con saveData)
					- SVSurveyCatDimVal_#### (FactKey y FactKeyDimVal tienen correspondencia con saveData)
				Al finalizar:
				- SI_SV_SurveySignatureImg (sólo graba el campo FactKey el cual tien correspondencia)
			- SurveyGlobalModel (RIFACT_xxxx) (FactKey y FactKeyDimVal se tienen que convertar a subrrogadas de dimensiones independientes con cierto formato)
			*/
			
			$strOriginalWD = getcwd();
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
			
			require_once("../model_manager/model.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			require_once("../model_manager/indicatorkpi.inc.php");
			$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $objSurvey->ModelID);
			
			$arrayInstancesIndicator = array();
			$arrayInstancesIndDim = array();
			$arrayInstancesImgDim = array();
			$arrayInstancesImgDimFields = array();
			$arrayInstancesDocDim = array();
			$arrayInstancesDocDimFields = array();
			$arrayInstanceMultiDim = array();
			$arrayInstanceMultiInd = array();
			$arrayInstancesCatDim = array();
			$arrayProcessedCatalogs = array();
			$descriptKeysCollection = BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository, $anInstanceModel->ModelID);
			$dimPeriodo = $anInstanceModel->dim_periodo;
			$dateKeyField = getJoinField($dimPeriodo, "", $descriptKeysCollection, true);
			$syncDateDimField = '';
			$syncDateField = '';
			if ($objSurvey->SyncDateDimID > 0) {
				$aSyncDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->SyncDateDimID);
				$syncDateDimField = $aSyncDateInstanceDim->Dimension->TableName."KEY";
				$syncDateField = 'SyncDate';
			}
			$syncTimeDimField = '';
			$syncTimeField = '';
			if ($objSurvey->SyncTimeDimID > 0) {
				$aSyncTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->SyncTimeDimID);
				$syncTimeDimField = $aSyncTimeInstanceDim->Dimension->TableName."KEY";
				$syncTimeField = 'SyncTime';
			}
			$tableDimFactKey = "RIDIM_".$objSurvey->FactKeyDimID;
			$fieldDimFactKeyKey = $tableDimFactKey."KEY";
			$userDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->UserDimID);
			$fieldUserDim = $userDimension->Dimension->TableName."KEY";
			$tableDimEmail = "RIDIM_".$objSurvey->EmailDimID;
			$fieldDimEMailKey = $tableDimEmail."KEY";
			$tableDimStatus = "RIDIM_".$objSurvey->StatusDimID;
			$fieldDimStatusKey = $tableDimStatus."KEY";
			$tableDimScheduler= "RIDIM_".$objSurvey->SchedulerDimID;
			$fieldDimSchedulerKey = $tableDimScheduler."KEY";
			$aStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->StartDateDimID);
			$anEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->EndDateDimID);
			$aStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->StartTimeDimID);
			$anEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->EndTimeDimID);
			$startDateDimField = $aStartDateInstanceDim->Dimension->TableName."KEY";
			$endDateDimField = $anEndDateInstanceDim->Dimension->TableName."KEY";
			$startTimeDimField = $aStartTimeInstanceDim->Dimension->TableName."KEY";
			$endTimeDimField = $anEndTimeInstanceDim->Dimension->TableName."KEY";
			$answeredQuestionsIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $objSurvey->AnsweredQuestionsIndID);
			$fieldAnsweredQuestionsInd = $answeredQuestionsIndicator->field_name.$answeredQuestionsIndicator->IndicatorID;
			$durationIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $objSurvey->DurationIndID);
			$fieldDurationInd = $durationIndicator->field_name.$durationIndicator->IndicatorID;
			$latitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $objSurvey->LatitudeIndID);
			$longitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $objSurvey->LongitudeIndID);
			$fieldLatitudeInd = $latitudeIndicator->field_name.$latitudeIndicator->IndicatorID;
			$fieldLongitudeInd = $longitudeIndicator->field_name.$longitudeIndicator->IndicatorID;
			if ($objSurvey->ServerStartDateDimID > 0) {
				$aServerStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->ServerStartDateDimID);
				$serverStartDateDimField = $aServerStartDateInstanceDim->Dimension->TableName."KEY";
				$serverStartDateField = 'ServerStartDate';
			}
			if ($objSurvey->ServerEndDateDimID > 0) {
				$aServerEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->ServerEndDateDimID);
				$serverEndDateDimField = $aServerEndDateInstanceDim->Dimension->TableName."KEY";
				$serverEndDateField = 'ServerEndDate';
			}
			if ($objSurvey->ServerStartTimeDimID > 0) {
				$aServerStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->ServerStartTimeDimID);
				$serverStartTimeDimField = $aServerStartTimeInstanceDim->Dimension->TableName."KEY";
				$serverStartTimeField = 'ServerStartTime';
			}
			if ($objSurvey->ServerEndTimeDimID > 0) {
				$aServerEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->ModelID, -1, $objSurvey->ServerEndTimeDimID);
				$serverEndTimeDimField = $aServerEndTimeInstanceDim->Dimension->TableName."KEY";
				$serverEndTimeField = 'ServerEndTime';
			}
			
			//Obtiene algunos datos del modelo global de encuestas
			if (is_null($aGblModelInstance)) {
				$aGblModelInstance = @BITAMModel::NewModelWithModelID($aRepository, $objSurvey->GblSurveyModelID);
				if (is_null($aGblModelInstance)) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error loading the global Surveys model"));
					continue;	//Este es un error crítico, así que no puede continuar
				}
			}
			$strGblFactTable = $aGblModelInstance->nom_tabla;
			$arrGblFactTableFields = array();
			$arrGblFactTableFields[] = "DateKey";
			$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblSurveyDimID}KEY";
			$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblSurveyGlobalDimID}KEY";
			$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblUserDimID}KEY";
			$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblStartDateDimID}KEY";
			$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblEndDateDimID}KEY";
			$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblStartTimeDimID}KEY";
			$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblEndTimeDimID}KEY";
			$arrGblFactTableFields[] = "IND_{$objSurvey->GblAnsweredQuestionsIndID}";
			$arrGblFactTableFields[] = "IND_{$objSurvey->GblDurationIndID}";
			$arrGblFactTableFields[] = "IND_{$objSurvey->GblLatitudeIndID}";
			$arrGblFactTableFields[] = "IND_{$objSurvey->GblLongitudeIndID}";
			$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblSyncDateDimID}KEY";
			$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblSyncTimeDimID}KEY";
			if ($objSurvey->GblServerStartDateDimID > 0) {
				$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblServerStartDateDimID}KEY";
			}
			if ($objSurvey->GblServerEndDateDimID > 0) {
				$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblServerEndDateDimID}KEY";
			}
			if ($objSurvey->GblServerStartTimeDimID > 0) {
				$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblServerStartTimeDimID}KEY";
			}
			if ($objSurvey->GblServerEndTimeDimID > 0) {
				$arrGblFactTableFields[] = "RIDIM_{$objSurvey->GblServerEndTimeDimID}KEY";
			}
			$strGblFactTableFields = implode(', ', $arrGblFactTableFields);
			
			if (is_null($aGblSurveyInstanceDim)) {
				$aGblSurveyInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->GblSurveyModelID, -1, $objSurvey->GblSurveyDimID);
				if (is_null($aGblSurveyInstanceDim)) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error loading the global Survey ID dimension"));
					continue;	//Este es un error crítico, así que no puede continuar
				}
			}
			
			if (is_null($aGblSurveyGlobalInstanceDim)) {
    			$aGblSurveyGlobalInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->GblSurveyModelID, -1, $objSurvey->GblSurveyGlobalDimID);
    			if (is_null($aGblSurveyGlobalInstanceDim)) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error loading the global Survey report dimension"));
					continue;	//Este es un error crítico, así que no puede continuar
    			}
			}
			
			if (is_null($aGblSurveySingleRecordInstanceDim)) {
    			$aGblSurveySingleRecordInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objSurvey->GblSurveyModelID, -1, $objSurvey->GblSurveySingleRecordDimID);
    			if (is_null($aGblSurveySingleRecordInstanceDim)) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error loading the global Survey single record dimension"));
					continue;	//Este es un error crítico, así que no puede continuar
    			}
			}
			chdir($strOriginalWD);
			
			//Prepara el SQL para cargar los datos de las preguntas que ayudará en la toma de decisión, pero sólo de las secciones estándar
			//Carga sin embargo un array con las preguntas de secciones múltiples para grabarlas con el valor de NA
			$arrFactTableFields = array();
			$arrFactTableFields[] = $dateKeyField;
			$arrFactTableFields[] = $fieldDimFactKeyKey;
			if ($objSurvey->SyncDateDimID > 0) {
				$arrFactTableFields[] = $syncDateDimField;
			}
			if ($objSurvey->SyncTimeDimID > 0) {
				$arrFactTableFields[] = $syncTimeDimField;
			}
			$arrFactTableFields[] = $fieldUserDim;
			$arrFactTableFields[] = $fieldDimEMailKey;
			$arrFactTableFields[] = $fieldDimStatusKey;
			$arrFactTableFields[] = $fieldDimSchedulerKey;
			$arrFactTableFields[] = $startDateDimField;
			$arrFactTableFields[] = $endDateDimField;
			$arrFactTableFields[] = $startTimeDimField;
			$arrFactTableFields[] = $endTimeDimField;
			$arrFactTableFields[] = $fieldAnsweredQuestionsInd;
			$arrFactTableFields[] = $fieldDurationInd;
			$arrFactTableFields[] = $fieldLatitudeInd;
			$arrFactTableFields[] = $fieldLongitudeInd;
			if ($objSurvey->ServerStartDateDimID > 0) {
				$arrFactTableFields[] = $serverStartDateDimField;
			}
			if ($objSurvey->ServerEndDateDimID > 0) {
				$arrFactTableFields[] = $serverEndDateDimField;
			}
			if ($objSurvey->ServerStartTimeDimID > 0) {
				$arrFactTableFields[] = $serverStartTimeDimField;
			}
			if ($objSurvey->ServerEndTimeDimID > 0) {
				$arrFactTableFields[] = $serverEndTimeDimField;
			}
			$strFactTableFields = implode(', ', $arrFactTableFields);
			if ($blnDebugBreak) {
				echo("<br>\r\nDefault Fact Table fields: $strFactTableFields");
			}
			$arrFactTableFieldsRSNames = explode(', ', strtolower($strFactTableFields));
			$strSurveyTableStrFields = strtolower(",DateID,HourID,UserID,StartTime,EndTime,LastDateID,LastHourID,LastStartTime,LastEndTime,SyncDate,SyncTime,ServerStartDate,ServerEndDate,ServerStartTime,ServerEndTime,");
			//$arrFactTableFieldsRSNames = array_flip(explode(', ', strtolower($strFactTableFields)));
			/*
			$strFactTableFields = $dateKeyField.", ".$fieldDimFactKeyKey.
				(($objSurvey->SyncDateDimID > 0)?", $syncDateDimField":'').
				(($objSurvey->SyncTimeDimID > 0)?", $syncTimeDimField":'').
				$fieldUserDim.", ".$fieldDimEMailKey.", ".$fieldDimStatusKey.", ".$fieldDimSchedulerKey.", ".
				$startDateDimField.", ".$endDateDimField.", ".$startTimeDimField.", ".$endTimeDimField.", ".
				$fieldAnsweredQuestionsInd.", ".$fieldDurationInd.", ".$fieldLatitudeInd.", ".$fieldLongitudeInd.", ".
				(($objSurvey->ServerStartDateDimID > 0)?", $serverStartDateDimField":'').
				(($objSurvey->ServerEndDateDimID > 0)?", $serverEndDateDimField":'').
				(($objSurvey->ServerStartTimeDimID > 0)?", $serverStartTimeDimField":'').
				(($objSurvey->ServerEndTimeDimID > 0)?", $serverEndTimeDimField":'');
			*/
			
			$arrSurveyTableFields = array();
			$arrSurveyTableFields[] = "DateID";
			$arrSurveyTableFields[] = "HourID";
			$arrSurveyTableFields[] = "UserID";
			$arrSurveyTableFields[] = "StartTime";
			$arrSurveyTableFields[] = "EndTime";
			$arrSurveyTableFields[] = "LastDateID";
			$arrSurveyTableFields[] = "LastHourID";
			$arrSurveyTableFields[] = "LastStartTime";
			$arrSurveyTableFields[] = "LastEndTime";
			if ($objSurvey->SyncDateDimID > 0) {
				$arrSurveyTableFields[] = $syncDateField;
			}
			if ($objSurvey->SyncTimeDimID > 0) {
				$arrSurveyTableFields[] = $syncTimeField;
			}
			$arrSurveyTableFields[] = "eFormsVersionNum";
			if ($objSurvey->ServerStartDateDimID > 0) {
				$arrSurveyTableFields[] = $serverStartDateField;
			}
			if ($objSurvey->ServerEndDateDimID > 0) {
				$arrSurveyTableFields[] = $serverEndDateField;
			}
			if ($objSurvey->ServerStartTimeDimID > 0) {
				$arrSurveyTableFields[] = $serverStartTimeField;
			}
			if ($objSurvey->ServerEndTimeDimID > 0) {
				$arrSurveyTableFields[] = $serverEndTimeField;
			}
			$strSurveyTableFields = implode(', ', $arrSurveyTableFields);
			$arrSurveyTableFieldsRSNames = explode(', ', strtolower($strSurveyTableFields));
			//$arrSurveyTableFieldsRSNames = array_flip(explode(', ', strtolower($strSurveyTableFields)));
			/*
			$strSurveyTableFields = "DateID, HourID, UserID, StartTime, EndTime, LastDateID, LastHourID, LastStartTime, LastEndTime".
				(($objSurvey->SyncDateDimID > 0)?", $syncDateField":'').
				(($objSurvey->SyncTimeDimID > 0)?", $syncTimeField":'').
				", eFormsVersionNum".
				(($objSurvey->ServerStartDateDimID > 0)?", $serverStartDateField":'').
				(($objSurvey->ServerEndDateDimID > 0)?", $serverEndDateField":'').
				(($objSurvey->ServerStartTimeDimID > 0)?", $serverStartTimeField":'').
				(($objSurvey->ServerEndTimeDimID > 0)?", $serverEndTimeField":'');
			*/
			//$strFactTableFields = '';
			//$strSurveyTableFields = '';
			$strAnd = ', ';
			$arrQuestionsBySectionID = array();
			$arrInvalidMultiRowQuestions = array();
			$arrStandardQuestionsColl = array();
			$arrStandardFactQuestionsColl = array();
			$arrStandardPhotoQuestionsColl = array();
			$arrStandardDocQuestionsColl = array();
			foreach ($objQuestionColl->Collection as $objQuestion) {
				$intSectionID = $objQuestion->SectionID;
				$blnIsMultipleSection = ($objQuestion->SectionType == sectDynamic || $objQuestion->SectionType == sectMasterDet);
				$blnValidQuestion = true;
				$intImgDimID = 0;
				$intDocDimID = 0;
				switch ($objQuestion->QTypeID) {
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
						//Este tipo de preguntas tiene una dimensión adicional, así que la asigna
						$intDocDimID = $objQuestion->DocDimID;
						
					case qtpPhoto:
					case qtpSketch:
					case qtpSignature:
						//Este tipo de preguntas pudiera soportar una foto, así que e identifica si la tiene
						$intImgDimID = $objQuestion->ImgDimID;
						
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
						$blnValidQuestion = false;
						break;
					
					default:
						break;
				}
				
				$blnAddQuestion = true;
				if ($blnIsMultipleSection || 
						$objQuestion->QTypeID == qtpMulti && $objQuestion->QDisplayMode == dspVertical && 
						$objQuestion->MCInputType == mpcNumeric && $objQuestion->IsMultiDimension == 0 && 
						$objQuestion->UseCategoryDimChoice != 0) {
					$blnAddQuestion = false;
				}
				
				//Sólo graba las dimensiones de respuesta si son preguntas que pudieran contener alguna
				if ($blnValidQuestion) {
					if ($blnAddQuestion) {
						//Es una pregunta de sección estándar, se debe incluir en el query para respetar su valor
						$arrStandardQuestionsColl[] = $objQuestion;
						$arrStandardFactQuestionsColl[] = $objQuestion;
						$strSurveyTableFields .= ', '.$objQuestion->SurveyField;
					}
					else {
						//Es una sección múltiple, así que esta no se incluye en el query pues siempre grabará NA
						//También las preguntas tipo categoría de dimensión entran en este caso, ya que esas se grababan como sección múltiple
						if (!isset($arrQuestionsBySectionID[$intSectionID])) {
							$arrQuestionsBySectionID[$intSectionID] = array();
						}
						$arrQuestionsBySectionID[$intSectionID][] = $objQuestion;
					}
					
					switch ($objQuestion->QTypeID) {
						case qtpMulti:
							//Las preguntas múltiples dependen de si están o no en una sección dinámica, si lo están entonces contienen sólo
							//una dimensión (y en el caso de las numéricas pudieran tener un indicador) pero si no lo están entonces depende de
							//si fueron o no configuradas como MultiDimension, en cuyo caso habrá una dimensión por cada opción de respuesta
							if ($objQuestion->SectionType == sectDynamic) {
								$aQuestionDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objQuestion->ModelID, -1, $objQuestion->IndDimID);
								chdir($strOriginalWD);
								//$arrayInstancesIndDim[$objQuestion->QuestionID] = $aQuestionDimension;
								$fieldName = $aQuestionDimension->Dimension->TableName."KEY";
								$arrayInstancesIndDim[$objQuestion->QuestionID] = $fieldName;
								if ($blnAddQuestion) {
									if ($blnDebugBreak) {
										echo("<br>\r\nAdding Multi question: {$objQuestion->QuestionID} -> $fieldName");
									}
									$strFactTableFields .= $strAnd.$fieldName;
									$strAnd = ', ';
								}
								
								if ($objQuestion->MCInputType == mpcNumeric) {
									if ($objQuestion->IsIndicatorMC == 1) {
										$aQuestionIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $objQuestion->IndicatorID);
										chdir($strOriginalWD);
										//$arrayInstancesIndicator[$objQuestion->QuestionID] = $aQuestionIndicator;
										$fieldName = $aQuestionIndicator->field_name.$aQuestionIndicator->IndicatorID;
										$arrayInstancesIndicator[$objQuestion->QuestionID] = $fieldName;
										if ($blnAddQuestion) {
											if ($blnDebugBreak) {
												echo("<br>\r\nAdding Multi question indicator: {$objQuestion->QuestionID} -> $fieldName");
											}
											$strFactTableFields .= $strAnd.$fieldName;
											$strAnd = ', ';
										}
									}
								}
							}
							else {
								if ($objQuestion->IsMultiDimension) {
									$QIndDimIDs = $objQuestion->QIndDimIds;
									
									//Obtenemos las instancias de las dimensiones de las opciones
									$arrayInstanceMultiDim[$objQuestion->QuestionID] = array();
									$arrayInstanceMultiInd[$objQuestion->QuestionID] = array();
									foreach ($QIndDimIDs as $qKey => $qDimID) {
										$existMultiOptions = true;
										$aQuestionDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objQuestion->ModelID, -1, $qDimID);
										chdir($strOriginalWD);
										//$arrayInstanceMultiDim[$objQuestion->QuestionID][$qKey] = $aQuestionDimension;
										$fieldName = $aQuestionDimension->Dimension->TableName."KEY";
										$arrayInstanceMultiDim[$objQuestion->QuestionID][$qKey] = $fieldName;
										if ($blnAddQuestion) {
											if ($blnDebugBreak) {
												echo("<br>\r\nAdding Multi question multi-dimension: {$objQuestion->QuestionID} ($qKey) -> $fieldName");
											}
											$strFactTableFields .= $strAnd.$fieldName;
											$strAnd = ', ';
										}
									}
								
									foreach ($QIndDimIDs as $qKey => $qDimID) {
										if($objQuestion->MCInputType == mpcCheckBox) 
										{
											$tmpInd = (int) @$objQuestion->QIndicatorIds[$qKey];
											if ($tmpInd != 0) {
												$aQuestionIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $tmpInd);
												chdir($strOriginalWD);
												//$arrayInstanceMultiInd[$objQuestion->QuestionID][$qKey] = $aQuestionIndicator;
												$fieldName = $aQuestionIndicator->field_name.$aQuestionIndicator->IndicatorID;
												$arrayInstanceMultiInd[$objQuestion->QuestionID][$qKey] = $fieldName;
												if ($blnAddQuestion) {
													if ($blnDebugBreak) {
														echo("<br>\r\nAdding Multi question multi-dimension indicator: {$objQuestion->QuestionID} ($qKey) -> $fieldName");
													}
													$strFactTableFields .= $strAnd.$fieldName;
													$strAnd = ', ';
												}
											}
										}
									}
								}
							}
							break;
						
						case qtpSingle:
							//Estas preguntas, dependiendo de si usan o no un catálogo, tendrían una dimensión independiente o bien compartirían
							//la misma dimensión si son del mismo catálogo, así que en esos casos se asegura de sólo incluirla una vez entre los
							//campos del query a la FactTable
							if ($objQuestion->CatalogID > 0) {
								if (!isset($arrayProcessedCatalogs[$objQuestion->CatalogID])) {
									$arrayProcessedCatalogs[$objQuestion->CatalogID] = $objQuestion->CatalogID;
									$aQuestionDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objQuestion->ModelID, -1, $objQuestion->IndDimID);
									chdir($strOriginalWD);
									//$arrayInstancesCatDim[$objQuestion->QuestionID] = $aQuestionDimension;
									$fieldName = $aQuestionDimension->Dimension->TableName."KEY";
									$arrayInstancesCatDim[$objQuestion->QuestionID] = $fieldName;
									if ($blnAddQuestion) {
										if ($blnDebugBreak) {
											echo("<br>\r\nAdding Single catalog question: {$objQuestion->QuestionID} -> $fieldName");
										}
										$strFactTableFields .= $strAnd.$fieldName;
										$strAnd = ', ';
									}
								}
							}
							else {
								$aQuestionDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objQuestion->ModelID, -1, $objQuestion->IndDimID);
								chdir($strOriginalWD);
								//$arrayInstancesIndDim[$objQuestion->QuestionID] = $aQuestionDimension;
								$fieldName = $aQuestionDimension->Dimension->TableName."KEY";
								$arrayInstancesIndDim[$objQuestion->QuestionID] = $fieldName;
								if ($blnAddQuestion) {
									if ($blnDebugBreak) {
										echo("<br>\r\nAdding Single question: {$objQuestion->QuestionID} -> $fieldName");
									}
									$strFactTableFields .= $strAnd.$fieldName;
									$strAnd = ', ';
								}
								
								//Verifica si tiene además asociado un indicador (preguntas numéricas, calculadas o simples con score)
								if ($objQuestion->IndicatorID > 0) {
									$aQuestionIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $objQuestion->IndicatorID);
									chdir($strOriginalWD);
									//$arrayInstancesIndicator[$objQuestion->QuestionID] = $aQuestionIndicator;
									$fieldName = $aQuestionIndicator->field_name.$aQuestionIndicator->IndicatorID;
									$arrayInstancesIndicator[$objQuestion->QuestionID] = $fieldName;
									if ($blnAddQuestion) {
										if ($blnDebugBreak) {
											echo("<br>\r\nAdding Single question indicator: {$objQuestion->QuestionID} -> $fieldName");
										}
										$strFactTableFields .= $strAnd.$fieldName;
										$strAnd = ', ';
									}
								}
							}
							break;
							
						case qtpOpenNumeric:
						case qtpCalc:
							//Verifica si tiene además asociado un indicador (preguntas numéricas, calculadas o simples con score)
							if ($objQuestion->IsIndicator) {
								$aQuestionIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $objQuestion->IndicatorID);
								chdir($strOriginalWD);
								//$arrayInstancesIndicator[$objQuestion->QuestionID] = $aQuestionIndicator;
								$fieldName = $aQuestionIndicator->field_name.$aQuestionIndicator->IndicatorID;
								$arrayInstancesIndicator[$objQuestion->QuestionID] = $fieldName;
								if ($blnAddQuestion) {
									if ($blnDebugBreak) {
										echo("<br>\r\nAdding question indicator: {$objQuestion->QuestionID} -> $fieldName");
									}
									$strFactTableFields .= $strAnd.$fieldName;
									$strAnd = ', ';
								}
							}
							
						case qtpAction:
						default:
							//Este tipo de preguntas simplemente tienen una dimensión para su respuesta
							$aQuestionDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objQuestion->ModelID, -1, $objQuestion->IndDimID);
							chdir($strOriginalWD);
							//$arrayInstancesIndDim[$objQuestion->QuestionID] = $aQuestionDimension;
							$fieldName = $aQuestionDimension->Dimension->TableName."KEY";
							$arrayInstancesIndDim[$objQuestion->QuestionID] = $fieldName;
							if ($blnAddQuestion) {
								if ($blnDebugBreak) {
									echo("<br>\r\nAdding question: {$objQuestion->QuestionID} -> $fieldName");
								}
								$strFactTableFields .= $strAnd.$fieldName;
								$strAnd = ', ';
							}
							break;
					}
					
					if ($objQuestion->HasReqPhoto > 0 && $objQuestion->ImgDimID > 0) {
						$intImgDimID = $objQuestion->ImgDimID;
					}
				}
				else {
					//Si no se considera válida pero tiene una dimensión especial, se tiene que agregar también a la colección para respetar este
					//orden cuando se graba en la tabla de hechos (estas no importan para la tabla paralela, porque se procesan directamente en
					//sus propias tablas de eForms)
					if ($intImgDimID > 0 || $intDocDimID > 0) {
						if ($blnAddQuestion) {
							$arrStandardFactQuestionsColl[] = $objQuestion;
						}
						else {
							$arrInvalidMultiRowQuestions[] = $objQuestion;
						}
					}
				}
				
				//Agrega las dimensiones "especiales" por si no tiene dimensión específica para otro tipo de respuesta, pero esto sólo
				//si se tratan de preguntas de secciones estándar
				if ($intImgDimID > 0) {
					$aQuestionImgDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objQuestion->ModelID, -1, $intImgDimID);
					chdir($strOriginalWD);
					$arrayInstancesImgDim[$objQuestion->QuestionID] = $aQuestionImgDimension;
					$fieldName = $aQuestionImgDimension->Dimension->TableName."KEY";
					$arrayInstancesImgDimFields[$objQuestion->QuestionID] = $fieldName;
					if ($blnAddQuestion) {
						if ($blnDebugBreak) {
							echo("<br>\r\nAdding image: {$objQuestion->QuestionID} -> $fieldName");
						}
						$strFactTableFields .= $strAnd.$fieldName;
						$strAnd = ', ';
						$arrStandardPhotoQuestionsColl[] = $objQuestion;
					}
				}
				
				if ($intDocDimID > 0) {
					$aQuestionDocDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objQuestion->ModelID, -1, $intDocDimID);
					chdir($strOriginalWD);
					$arrayInstancesDocDim[$objQuestion->QuestionID] = $aQuestionDocDimension;
					$fieldName = $aQuestionDocDimension->Dimension->TableName."KEY";
					$arrayInstancesDocDimFields[$objQuestion->QuestionID] = $fieldName;
					if ($blnAddQuestion) {
						if ($blnDebugBreak) {
							echo("<br>\r\nAdding document question: {$objQuestion->QuestionID} -> $fieldName");
						}
						$strFactTableFields .= $strAnd.$fieldName;
						$strAnd = ', ';
						$arrStandardDocQuestionsColl[] = $objQuestion;
					}
				}
			}
			
			$arrayInstancesImgDimRSNames = array();
			if (count($arrayInstancesImgDimFields) > 0) {
				$arrayInstancesImgDimRSNames = array_flip(explode(',', strtolower(implode(',', $arrayInstancesImgDimFields))));
			}
			
			$arrayInstancesDocDimRSNames = array();
			if (count($arrayInstancesDocDimFields) > 0) {
				$arrayInstancesDocDimRSNames = array_flip(explode(',', strtolower(implode(',', $arrayInstancesDocDimFields))));
			}
			
			//Genera la lista de columnas y valores default de aquellas que son de secciones múltiples, es decir, de las que no interesa grabar
			//como parte del registro único
			$strEmptyFactTableFields = '';
			$strEmptyFactTableValues = '';
			$strEmptySurveyTableFields = '';
			$strEmptySurveyTableValues = '';
			foreach ($arrQuestionsBySectionID as $intSectionID => $arrQuestions) {
				foreach ($arrQuestions as $objQuestion) {
					$strEmptySurveyTableFields .= $strAnd.$objQuestion->SurveyField;
					$strEmptySurveyTableValues .= $strAnd.getEmptyValueForQuestion($objQuestion);
					switch ($objQuestion->QTypeID) {
						case qtpMulti:
							if ($objQuestion->SectionType == sectDynamic) {
								$fieldName = $arrayInstancesIndDim[$objQuestion->QuestionID];
								$strValue = 1;
								$strEmptyFactTableFields .= $strAnd.$fieldName;
								$strEmptyFactTableValues .= $strAnd.$strValue;
								$strAnd = ', ';
								
								if ($objQuestion->MCInputType == mpcNumeric) {
									if ($objQuestion->IsIndicatorMC == 1) {
										$fieldName = $arrayInstancesIndicator[$objQuestion->QuestionID];
										$strValue = 0;
										$strEmptyFactTableFields .= $strAnd.$fieldName;
										$strEmptyFactTableValues .= $strAnd.$strValue;
										$strAnd = ', ';
									}
								}
							}
							else {
								if ($objQuestion->IsMultiDimension) {
									$arrDims = @$arrayInstanceMultiDim[$objQuestion->QuestionID];
									if (is_null($arrDims)) {
										$arrDims = array();
									}
									foreach ($arrDims as $fieldName) {
										$strValue = 1;
										$strEmptyFactTableFields .= $strAnd.$fieldName;
										$strEmptyFactTableValues .= $strAnd.$strValue;
										$strAnd = ', ';
									}
									
									$arrInds = @$arrayInstanceMultiInd[$objQuestion->QuestionID];
									if (is_null($arrInds)) {
										$arrInds = array();
									}
									foreach ($arrInds as $fieldName) {
										$strValue = 0;
										$strEmptyFactTableFields .= $strAnd.$fieldName;
										$strEmptyFactTableValues .= $strAnd.$strValue;
										$strAnd = ', ';
									}
								}
							}
							break;
							
						case qtpSingle:
							if ($objQuestion->CatalogID > 0) {
								if (!isset($arrayProcessedCatalogs[$objQuestion->CatalogID])) {
									$arrayProcessedCatalogs[$objQuestion->CatalogID] = $objQuestion->CatalogID;
									$fieldName = $arrayInstancesCatDim[$objQuestion->QuestionID];
									$strValue = 1;
									$strEmptyFactTableFields .= $strAnd.$fieldName;
									$strEmptyFactTableValues .= $strAnd.$strValue;
									$strAnd = ', ';
								}
							}
							else {
								$fieldName = $arrayInstancesIndDim[$objQuestion->QuestionID];
								$strValue = 1;
								$strEmptyFactTableFields .= $strAnd.$fieldName;
								$strEmptyFactTableValues .= $strAnd.$strValue;
								$strAnd = ', ';
								
								if ($objQuestion->IndicatorID > 0) {
									$fieldName = $arrayInstancesIndicator[$objQuestion->QuestionID];
									$strValue = 0;
									$strEmptyFactTableFields .= $strAnd.$fieldName;
									$strEmptyFactTableValues .= $strAnd.$strValue;
									$strAnd = ', ';
								}
							}
							break;
							
						case qtpOpenNumeric:
						case qtpCalc:
							if ($objQuestion->IsIndicator) {
								$fieldName = $arrayInstancesIndicator[$objQuestion->QuestionID];
								$strValue = 1;
								$strEmptyFactTableFields .= $strAnd.$fieldName;
								$strEmptyFactTableValues .= $strAnd.$strValue;
								$strAnd = ', ';
							}
						
						case qtpAction:
						default:
							$fieldName = $arrayInstancesIndDim[$objQuestion->QuestionID];
							$strValue = 1;
							$strEmptyFactTableFields .= $strAnd.$fieldName;
							$strEmptyFactTableValues .= $strAnd.$strValue;
							$strAnd = ', ';
							break;
					}
					
					//Si tiene foto se agrega también
					if ($objQuestion->HasReqPhoto > 0 && $objQuestion->ImgDimID > 0) {
						$fieldName = (string) @$arrayInstancesImgDimFields[$objQuestion->QuestionID];
						if (trim($fieldName) != '') {
							$strValue = 1;
							$strEmptyFactTableFields .= $strAnd.$fieldName;
							$strEmptyFactTableValues .= $strAnd.$strValue;
							$strAnd = ', ';
						}
					}
				}
			}
			
			//Finalmente agrega las fotos y documentos de aquellas que son secciones múltiples pero en preguntas especializadas de estos tipos (es
			//decir, no son fotos opcionales de otros tipos de preguntas como simple choice o numéricas)
			foreach ($arrInvalidMultiRowQuestions as $objQuestion) {
				if ($objQuestion->HasReqPhoto > 0 && $objQuestion->ImgDimID > 0) {
					$fieldName = (string) @$arrayInstancesImgDimFields[$objQuestion->QuestionID];
					if (trim($fieldName) != '') {
						$strValue = 1;
						$strEmptyFactTableFields .= $strAnd.$fieldName;
						$strEmptyFactTableValues .= $strAnd.$strValue;
						$strAnd = ', ';
					}
				}
				
				if ($objQuestion->DocDimID > 0) {
					$fieldName = (string) @$arrayInstancesDocDimFields[$objQuestion->QuestionID];
					if (trim($fieldName) != '') {
						$strValue = 1;
						$strEmptyFactTableFields .= $strAnd.$fieldName;
						$strEmptyFactTableValues .= $strAnd.$strValue;
						$strAnd = ', ';
					}
				}
			}
			
			//Inicia el proceso de grabado del registro único captura por captura
			foreach ($arrEntryReportIDs as $intFactKeyDimVal) {
				echo("<BR><BR>***** Inserting single standard record for entry # {$intFactKeyDimVal}");
				
				//Obtiene el máximo FactKey para basarse en él para insertar el registro único
				$sql = "SELECT FactKeyDimVal, MAX(FactKey) AS FactKey 
					FROM {$strSurveyTable} 
					WHERE FactKeyDimVal = {$intFactKeyDimVal}";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS == false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				
				$intFactKey = (int) @$aRS->fields["factkey"];
				if ($intFactKey <= 0) {
					//No sería posible esta situación en condiciones normales, así que simplemente se ignorará si se llega a presentar
					continue;
				}
				
				//*************************************************************************************************************************
				//Agrega el registro a la tabla de hechos
				$sql = "SELECT {$strFactTableFields} 
					FROM {$strFactTable} 
					WHERE FactKey = {$intFactKey}";
					echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS == false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				elseif ($aRS->EOF) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Unable to find the FactTable record #$intFactKey"));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				
				$arrayData = array();
				$strInsertValues = '';
				$strAnd = '';
				$arrStandardPhotoQuestionsValues = array();
				$arrStandardDoctoQuestionsValues = array();
				foreach ($aRS->fields as $strFieldName => $strFieldValue) {
					$arrayData[strtolower($strFieldName)] = $strFieldValue;
					
					if (isset($arrayInstancesImgDimRSNames[strtolower($strFieldName)])) {
						//Sólo agrega el key de la imagen si no es el valor de NA
						if ((int) $strFieldValue > 1) {
							$arrStandardPhotoQuestionsValues[strtolower($strFieldName)] = $strFieldValue;
						}
					}
					if (isset($arrayInstancesDocDimRSNames[strtolower($strFieldName)])) {
							$arrStandardDoctoQuestionsValues[strtolower($strFieldName)] = $strFieldValue;
					}
				}
				
				//Primero agrega los valores de los campos que no pertenecen a preguntas
				foreach ($arrFactTableFieldsRSNames as $fieldName) {
					$strValue = getFieldValueFromArr($arrayData, $fieldName);
					if ($blnDebugBreak) {
						echo("<br>\r\nAdding Default field answer: $fieldName == $strValue");
					}
					$strInsertValues .= $strAnd.$strValue;
					$strAnd = ', ';
				}
				
				//Agrega las preguntas de secciones estándar
				$arrayProcessedCatalogs = array();
				//foreach ($arrStandardQuestionsColl as $objQuestion) {
				foreach ($arrStandardFactQuestionsColl as $objQuestion) {
					$blnValidQuestion = true;
					$intImgDimID = 0;
					$intDocDimID = 0;
					switch ($objQuestion->QTypeID) {
						case qtpDocument:
						case qtpAudio:
						case qtpVideo:
							//Este tipo de preguntas tiene una dimensión adicional, así que la asigna
							$intDocDimID = $objQuestion->DocDimID;
							
						case qtpPhoto:
						case qtpSketch:
						case qtpSignature:
							//Este tipo de preguntas pudiera soportar una foto, así que e identifica si la tiene
							$intImgDimID = $objQuestion->ImgDimID;
							
						case qtpShowValue:
						case qtpMessage:
						case qtpSkipSection:
						//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
						case qtpSync:
						//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
						case qtpPassword:
						//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
						case qtpSection:
						//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
						case qtpExit:
							$blnValidQuestion = false;
							break;
						
						default:
							if ($objQuestion->HasReqPhoto > 0 && $objQuestion->ImgDimID > 0) {
								$intImgDimID = $objQuestion->ImgDimID;
							}
							break;
					}
					
					if ($blnValidQuestion) {
						switch ($objQuestion->QTypeID) {
							case qtpMulti:
								if ($objQuestion->SectionType == sectDynamic) {
									$fieldName = $arrayInstancesIndDim[$objQuestion->QuestionID];
									$strValue = getFieldValueFromArr($arrayData, $fieldName);
									if ($blnDebugBreak) {
										echo("<br>\r\nAdding Multi answer: {$objQuestion->QuestionID} -> $fieldName == $strValue");
									}
									$strInsertValues .= $strAnd.$strValue;
									$strAnd = ', ';
									
									if ($objQuestion->MCInputType == mpcNumeric) {
										if ($objQuestion->IsIndicatorMC == 1) {
											$fieldName = $arrayInstancesIndicator[$objQuestion->QuestionID];
											$strValue = getFieldValueFromArr($arrayData, $fieldName);
											if ($blnDebugBreak) {
												echo("<br>\r\nAdding Multi indicator answer: {$objQuestion->QuestionID} -> $fieldName == $strValue");
											}
											$strInsertValues .= $strAnd.$strValue;
											$strAnd = ', ';
										}
									}
								}
								else {
									if ($objQuestion->IsMultiDimension) {
										$arrDims = @$arrayInstanceMultiDim[$objQuestion->QuestionID];
										if (is_null($arrDims)) {
											$arrDims = array();
										}
										foreach ($arrDims as $fieldName) {
											$strValue = getFieldValueFromArr($arrayData, $fieldName);
											if ($blnDebugBreak) {
												echo("<br>\r\nAdding Multi multi-dimension answer: {$objQuestion->QuestionID} -> $fieldName == $strValue");
											}
											$strInsertValues .= $strAnd.$strValue;
											$strAnd = ', ';
										}
										
										$arrInds = @$arrayInstanceMultiInd[$objQuestion->QuestionID];
										if (is_null($arrInds)) {
											$arrInds = array();
										}
										foreach ($arrInds as $fieldName) {
											$strValue = getFieldValueFromArr($arrayData, $fieldName);
											if ($blnDebugBreak) {
												echo("<br>\r\nAdding Multi multi-dimension indicator answer: {$objQuestion->QuestionID} -> $fieldName == $strValue");
											}
											$strInsertValues .= $strAnd.$strValue;
											$strAnd = ', ';
										}
									}
								}
								break;
								
							case qtpSingle:
								if ($objQuestion->CatalogID > 0) {
									if (!isset($arrayProcessedCatalogs[$objQuestion->CatalogID])) {
										$arrayProcessedCatalogs[$objQuestion->CatalogID] = $objQuestion->CatalogID;
										$fieldName = $arrayInstancesCatDim[$objQuestion->QuestionID];
										$strValue = getFieldValueFromArr($arrayData, $fieldName);
										if ($blnDebugBreak) {
											echo("<br>\r\nAdding Single catalog answer: {$objQuestion->QuestionID} -> $fieldName == $strValue");
										}
										$strInsertValues .= $strAnd.$strValue;
										$strAnd = ', ';
									}
								}
								else {
									$fieldName = $arrayInstancesIndDim[$objQuestion->QuestionID];
									$strValue = getFieldValueFromArr($arrayData, $fieldName);
									if ($blnDebugBreak) {
										echo("<br>\r\nAdding Single answer: {$objQuestion->QuestionID} -> $fieldName == $strValue");
									}
									$strInsertValues .= $strAnd.$strValue;
									$strAnd = ', ';
									
									if ($objQuestion->IndicatorID > 0) {
										$fieldName = $arrayInstancesIndicator[$objQuestion->QuestionID];
										$strValue = getFieldValueFromArr($arrayData, $fieldName);
										if ($blnDebugBreak) {
											echo("<br>\r\nAdding Single indicator answer: {$objQuestion->QuestionID} -> $fieldName == $strValue");
										}
										$strInsertValues .= $strAnd.$strValue;
										$strAnd = ', ';
									}
								}
								break;
								
							case qtpOpenNumeric:
							case qtpCalc:
								if ($objQuestion->IsIndicator) {
									$fieldName = $arrayInstancesIndicator[$objQuestion->QuestionID];
									$strValue = getFieldValueFromArr($arrayData, $fieldName);
									if ($blnDebugBreak) {
										echo("<br>\r\nAdding indicator: {$objQuestion->QuestionID} -> $fieldName == $strValue");
									}
									$strInsertValues .= $strAnd.$strValue;
									$strAnd = ', ';
								}
							
							case qtpAction:
							default:
								$fieldName = $arrayInstancesIndDim[$objQuestion->QuestionID];
								$strValue = getFieldValueFromArr($arrayData, $fieldName);
								if ($blnDebugBreak) {
									echo("<br>\r\nAdding answer: {$objQuestion->QuestionID} -> $fieldName == $strValue");
								}
								$strInsertValues .= $strAnd.$strValue;
								$strAnd = ', ';
								break;
						}
					}
					
					//Agrega el valor de las dimensiones "especiales" en el mismo orden que se hizo en la lista de campos
					if ($intImgDimID > 0) {
						$fieldName = $arrayInstancesImgDimFields[$objQuestion->QuestionID];
						$strValue = getFieldValueFromArr($arrayData, $fieldName);
						if ($blnDebugBreak) {
							echo("<br>\r\nAdding image answer: {$objQuestion->QuestionID} -> $fieldName == $strValue");
						}
						$strInsertValues .= $strAnd.$strValue;
						$strAnd = ', ';
					}
					
					if ($intDocDimID > 0) {
						$fieldName = $arrayInstancesDocDimFields[$objQuestion->QuestionID];
						$strValue = getFieldValueFromArr($arrayData, $fieldName);
						if ($blnDebugBreak) {
							echo("<br>\r\nAdding document answer: {$objQuestion->QuestionID} -> $fieldName == $strValue");
						}
						$strInsertValues .= $strAnd.$strValue;
						$strAnd = ', ';
					}
				}
				
				//Genera el Insert utilizando el valor leído de las preguntas de secciones estándar, pero un default válido en las preguntas que
				//son de otras secciones
				if ($blnDebugBreak) {
					echo("<br>\r\nstrFactTableFields: $strFactTableFields");
					echo("<br>\r\nstrEmptyFactTableFields: $strEmptyFactTableFields");
					echo("<br>\r\nstrInsertValues: $strInsertValues");
					echo("<br>\r\nstrEmptyFactTableValues: $strEmptyFactTableValues");
				}
				$sql = "INSERT INTO {$strFactTable} ($strFactTableFields $strEmptyFactTableFields) 
					VALUES ($strInsertValues $strEmptyFactTableValues)";
				echo(sendUpgradeMessage($sql, "color: blue;"));
				if ($blnExecuteProcess) {
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						$blnError = true;
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						continue 2;	//Este es un error crítico, así que no puede continuar
					}
				}
				
				$intNewFactKey = (int) @$aRepository->DataADOConnection->_insertid();
				
				//*************************************************************************************************************************
				//Agrega el registro a la tabla paralela
				$sql = "SELECT {$strSurveyTableFields} 
					FROM {$strSurveyTable} 
					WHERE FactKey = {$intFactKey}";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS == false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				elseif ($aRS->EOF) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Unable to find the FactTable record #$intFactKey"));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				
				$arrayData = array();
				$strInsertValues = '';
				$strAnd = '';
				foreach ($aRS->fields as $strFieldName => $strFieldValue) {
					$arrayData[strtolower($strFieldName)] = $strFieldValue;
				}
				
				//Primero agrega los valores de los campos que no pertenecen a preguntas
				foreach ($arrSurveyTableFieldsRSNames as $fieldName) {
					$strValue = getFieldValueFromArr($arrayData, $fieldName);
					if (strpos($strSurveyTableStrFields, ','.strtolower($fieldName).',') !== false) {
						$blnNullValue = is_null(getFieldValueFromArr($arrayData, $fieldName, false));
						if (!$blnNullValue) {
							$strValue = $aRepository->DataADOConnection->Quote($strValue);
						}
					}
					
					if ($blnDebugBreak) {
						echo("<br>\r\nAdding Default field answer: $fieldName == $strValue");
					}
					$strInsertValues .= $strAnd.$strValue;
					$strAnd = ', ';
				}
				
				//Agrega las preguntas de secciones estándar
				$arrayProcessedCatalogs = array();
				foreach ($arrStandardQuestionsColl as $objQuestion) {
					switch ($objQuestion->QTypeID) {
						case qtpMulti:
							$fieldName = $objQuestion->SurveyField;
							$strValue = getFieldValueFromArr($arrayData, $fieldName);
							if ($objQuestion->SectionType == sectDynamic && $objQuestion->MCInputType == mpcNumeric && 
									$objQuestion->IsIndicatorMCNew == 1) {
								$strInsertValues .= $strAnd.$strValue;
							}
							else {
								$blnNullValue = is_null(getFieldValueFromArr($arrayData, $fieldName, false));
								if ($blnNullValue) {
									$strInsertValues .= $strAnd.$strValue;
								}
								else {
									$strInsertValues .= $strAnd.$aRepository->DataADOConnection->Quote($strValue);
								}
							}
							$strAnd = ', ';
							break;
							
						case qtpOpenNumeric:
						case qtpCalc:
							if ($objQuestion->IsIndicator) {
								$fieldName = $objQuestion->SurveyField;
								$strValue = getFieldValueFromArr($arrayData, $fieldName);
								$strInsertValues .= $strAnd.$strValue;
								$strAnd = ', ';
							}
							break;
						
						case qtpSingle:
						case qtpAction:
						default:
							$fieldName = $objQuestion->SurveyField;
							$strValue = getFieldValueFromArr($arrayData, $fieldName);
							$blnNullValue = is_null(getFieldValueFromArr($arrayData, $fieldName, false));
							if ($blnNullValue) {
								$strInsertValues .= $strAnd.$strValue;
							}
							else {
								$strInsertValues .= $strAnd.$aRepository->DataADOConnection->Quote($strValue);
							}
							$strAnd = ', ';
							break;
					}
				}
				
				//Genera el Insert utilizando el valor leído de las preguntas de secciones estándar, pero un default válido en las preguntas que
				//son de otras secciones
				$sql = "INSERT INTO {$strSurveyTable} ($strSurveyTableFields, FactKeyDimVal, FactKey, EntrySectionID, EntryCatDimID $strEmptySurveyTableFields) 
					VALUES ($strInsertValues, $intFactKeyDimVal, $intNewFactKey, 0, 0 $strEmptySurveyTableValues)";
				echo(sendUpgradeMessage($sql, "color: blue;"));
				if ($blnExecuteProcess) {
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						$blnError = true;
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						continue 2;	//Este es un error crítico, así que no puede continuar
					}
				}
				
				//*************************************************************************************************************************
				//Agrega el registro a la tabla de fotos/imagenes
				//En este caso, se tomará la imagen que corresponda al registro con el Key mayor y simplemente se renombrará, ya que el FactKey
				//cambió para representar al registro de No Aplica por lo que a partir de ahora la imagen será buscada con un nombre distinto, 
				//así mismo habría que insertar el nuevo valor en todas las dimensiones Imagen de dichas preguntas y actualizar en la tabla
				//de hechos el nuevo FactKey. El renombrado de la imagen se hará al final para que por lo menos las preguntas sigan apuntando
				//a la imagen original
				//ESurveyV4_test/surveyimages/fbm_bmd_0081/survey_368/photo_368_4826_9.jpg
				//eFormsPath/surveyimages/Repository/survey_SurveyID/photo_SurveyID_QuestionID_FactKeyDimVal.jpg
				$strAlterFields = '';
				$strAnd = '';
				$arrInserts = array();
				$arrFileNamesToRename = array();
				foreach ($arrStandardPhotoQuestionsColl as $objQuestion) {
					$intImgDimID = $objQuestion->ImgDimID;
					
					//Verifica su hay o no una imagen grabada en la dimensión
					$fieldName = strtolower($arrayInstancesImgDimFields[$objQuestion->QuestionID]);
					$intImageKey = 1;
					if (isset($arrStandardPhotoQuestionsValues[$fieldName])) {
						$intImageKey = (int) @$arrStandardPhotoQuestionsValues[$fieldName];
						if ($intImageKey <= 1) {
							$intImageKey = 1;
						}
					}
					
					//Si hubiera una imagen grabada, se debe extraer el nombre de la misma para convertirlo, de lo contrario no es necesario
					//ningún proceso adicional
					if ($intImageKey > 1) {
						$strFullImagePath = '';
						$tableDimImage = "RIDIM_".$objQuestion->ImgDimID;
						$fieldDimImageKey = $tableDimImage."KEY";
						$fieldDimImageDsc = "DSC_".$objQuestion->ImgDimID;
						$sql = "SELECT {$fieldDimImageDsc} AS 'ImagePath' 
							FROM {$tableDimImage} 
							WHERE {$fieldDimImageKey} = {$intImageKey}";
						echo(sendUpgradeMessage($sql, "font-style: italic;"));
						$aRS = $aRepository->DataADOConnection->Execute($sql);
						if ($aRS && !$aRS->EOF) {
							$strFullImagePath = (string) @$aRS->fields["imagepath"];
						}
						$intImageKey = 1;
						
						if (trim($strFullImagePath) != '') {
							//En este caso si hay una imagen grabada, así que se tiene que hacer la inserción del valor nuevo (si no estaba)
							//y hacer el UPDATE de la tabla de hechos, pero antes se convierte el nombre usando el nuevo FactKey
							$arrPathPieces = explode('/', $strFullImagePath);
							if (is_array($arrPathPieces) && count($arrPathPieces) == 5) {
								$strImageName = trim((string) @$arrPathPieces[4]);
								$strOldFileName = $strImageName;
								if ($strImageName != '') {
									$arrPhotoPieces = explode('_', $strImageName);
									if (is_array($arrPhotoPieces) && count($arrPhotoPieces) == 4 && stripos($arrPhotoPieces[3], '.jpg') !== false) {
										$arrPhotoPieces[3] = $intNewFactKey.'.jpg';
										$strImageName = implode('_', $arrPhotoPieces);
										$arrPathPieces[4] = $strImageName;
										$strFullImagePath = implode('/', $arrPathPieces);
										$aQuestionImgDimension = @$arrayInstancesImgDim[$objQuestion->QuestionID];
										if (!is_null($aQuestionImgDimension)) {
											$intImageKey = getInsertDimValueKey($aRepository, $aQuestionImgDimension, array($strFullImagePath));
											if ($intImageKey !== false && is_array($intImageKey) && count($intImageKey) > 0) {
												$intImageKey = (int) @$intImageKey[0];
												$strSurveyAnswerPath = $arrPathPieces[3].'/'.$arrPathPieces[4];
												$sql = "INSERT INTO SI_SV_SurveyAnswerImage (SurveyID, QuestionID, FactKey, MainFactKey, PathImage) 
													VALUES ({$objQuestion->SurveyID}, {$objQuestion->QuestionID}, {$intFactKeyDimVal}, {$intNewFactKey}, ".
													$aRepository->DataADOConnection->Quote($strSurveyAnswerPath).")";
												$arrInserts[] = $sql;
												$arrFileNamesToRename[] = array($strOldFileName, $strImageName);
											}
											else {
												$intImageKey = false;
											}
										}
									}
								}
							}
						}
						
						//Si al llegar a este punto si se logró obtener un Key, entonces si ejecuta el UPDATE de esta dimensión
						if ($intImageKey !== false && $intImageKey > 1) {
							$strAlterFields .= $strAnd."{$fieldDimImageKey} = $intImageKey ";
							$strAnd = ', ';
						}
					}
				}
				
				//Actualiza la tabla de hechos
				if (trim($strAlterFields) != '') {
					$sql = "UPDATE {$strFactTable} 
							SET {$strAlterFields} 
						WHERE FactKey = {$intNewFactKey}";
					echo(sendUpgradeMessage($sql, "color: blue;"));
					if ($blnExecuteProcess) {
						if ($aRepository->DataADOConnection->Execute($sql) === false) {
							$blnError = true;
							echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
							continue 2;	//Este es un error crítico, así que no puede continuar
						}
					}
					
					$blnPathExists = false;
					$strCompleteSourcePath = getcwd()."\\surveyimages\\".trim($_SESSION["PABITAM_RepositoryName"]).
						"\\"."survey_".$objSurvey->SurveyID;
					if (file_exists($strCompleteSourcePath)) {
						$blnPathExists = true;
					}
					
					$intCont = 0;
					foreach ($arrInserts as $sql) {
						echo(sendUpgradeMessage($sql, "color: blue;"));
						if ($blnExecuteProcess) {
							if ($aRepository->DataADOConnection->Execute($sql) === false) {
								$blnError = true;
								echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
								continue 2;	//Este es un error crítico, así que no puede continuar
							}
							
							//Renombra el archivo especificado
							if ($blnPathExists && isset($arrFileNamesToRename[$intCont])) {
								if (is_array($arrFileNamesToRename[$intCont])) {
									$strSourceName = (string) @$arrFileNamesToRename[$intCont][0];
									$strTargetName = (string) @$arrFileNamesToRename[$intCont][0];
									if ($strSourceName != '' && $strTargetName != '') {
										@rename($strCompleteSourcePath."\\".$strSourceName, $strCompleteSourcePath."\\".$strTargetName);
									}
								}
							}
						}
						$intCont++;
					}
				}
				
				//*************************************************************************************************************************
				//Agrega el registro a la tabla de documentos
				//El proceso es prácticamente idéntico al de fotos, con la diferencia en el dato grabado sólo en el último de sus elementos,
				//el cual agrega el FileName por lo que el FactKey queda independiente
				//ESurveyV4_test/surveyimages/fbm_bmd_0081/survey_212/document_212_2384_9_memes.rar
				//eFormsPath/surveyimages/Repository/survey_SurveyID/document_SurveyID_QuestionID_FactKeyDimVal_FileName.ext
				$strAlterFields = '';
				$strAnd = '';
				$arrInserts = array();
				$arrFileNamesToRename = array();
				foreach ($arrStandardDocQuestionsColl as $objQuestion) {
					$intDocDimID = $objQuestion->DocDimID;
					
					//Verifica su hay o no un documento grabado en la dimensión
					$fieldName = strtolower($arrayInstancesDocDimFields[$objQuestion->QuestionID]);
					$intDocKey = 1;
					if (isset($arrStandardDoctoQuestionsValues[$fieldName])) {
						$intDocKey = (int) @$arrStandardDoctoQuestionsValues[$fieldName];
						if ($intDocKey <= 1) {
							$intDocKey = 1;
						}
					}
					
					//Si hubiera un documento grabado, se debe extraer el nombre del mismo para convertirlo, de lo contrario no es necesario
					//ningún proceso adicional
					if ($intDocKey > 1) {
						$strFullDocPath = '';
						$tableDimDoc = "RIDIM_".$objQuestion->DocDimID;
						$fieldDimDocKey = $tableDimDoc."KEY";
						$fieldDimDocDsc = "DSC_".$objQuestion->DocDimID;
						$sql = "SELECT {$fieldDimDocDsc} AS 'DocPath' 
							FROM {$tableDimDoc} 
							WHERE {$fieldDimDocKey} = {$intDocKey}";
						$aRS = $aRepository->DataADOConnection->Execute($sql);
						if ($aRS && !$aRS->EOF) {
							$strFullDocPath = (string) @$aRS->fields["docpath"];
						}
						$intDocKey = 1;
						
						if (trim($strFullDocPath) != '') {
							//En este caso si hay un documento grabado, así que se tiene que hacer la inserción del valor nuevo (si no estaba)
							//y hacer el UPDATE de la tabla de hechos, pero antes se convierte el nombre usando el nuevo FactKey
							$arrPathPieces = explode('/', $strFullDocPath);
							if (is_array($arrPathPieces) && $arrPathPieces == 5) {
								$strDocName = trim((string) @$arrPathPieces[4]);
								$strOldFileName = $strDocName;
								if ($strDocName != '') {
									$arrDoctoPieces = explode('_', $strDocName);
									if (is_array($arrDoctoPieces) && count($arrDoctoPieces) >= 4) {
										$arrDoctoPieces[3] = $intNewFactKey;
										$strDocName = implode('_', $arrDoctoPieces);
										$arrPathPieces[4] = $strDocName;
										$strFullDocPath = implode('/', $arrPathPieces);
										$aQuestionDocDimension = @$arrayInstancesDocDim[$objQuestion->QuestionID];
										if (!is_null($aQuestionDocDimension)) {
											$intDocKey = getInsertDimValueKey($aRepository, $aQuestionDocDimension, array($strFullDocPath));
											if ($intDocKey !== false && is_array($intDocKey) && count($intDocKey) > 0) {
												$intDocKey = $intDocKey[0];
												$strSurveyAnswerPath = $arrPathPieces[3].'/'.$arrPathPieces[4];
												$sql = "INSERT INTO SI_SV_SurveyAnswerDocument (SurveyID, QuestionID, FactKey, MainFactKey, PathDocument) 
													VALUES ({$objQuestion->SurveyID}, {$objQuestion->QuestionID}, {$intFactKeyDimVal}, {$intNewFactKey}, ".
													$aRepository->DataADOConnection->Quote($strSurveyAnswerPath).")";
												$arrInserts[] = $sql;
												$arrFileNamesToRename[] = array($strOldFileName, $strDocName);
											}
											else {
												$intDocKey = false;
											}
										}
									}
								}
							}
						}
						
						//Si al llegar a este punto si se logró obtener un Key, entonces si ejecuta el UPDATE de esta dimensión
						if ($intDocKey !== false && $intDocKey > 1) {
							$strAlterFields .= $strAnd."{$fieldDimDocKey} = $intDocKey ";
							$strAnd = ', ';
						}
					}
				}
				
				//Actualiza la tabla de hechos
				if (trim($strAlterFields) != '') {
					$sql = "UPDATE {$strFactTable} 
							SET {$strAlterFields} 
						WHERE FactKey = {$intNewFactKey}";
					echo(sendUpgradeMessage($sql, "color: blue;"));
					if ($blnExecuteProcess) {
						if ($aRepository->DataADOConnection->Execute($sql) === false) {
							$blnError = true;
							echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
							continue 2;	//Este es un error crítico, así que no puede continuar
						}
					}
					
					$blnPathExists = false;
					$strCompleteSourcePath = getcwd()."\\surveydocuments\\".trim($_SESSION["PABITAM_RepositoryName"]).
						"\\"."survey_".$objSurvey->SurveyID;
					if (file_exists($strCompleteSourcePath)) {
						$blnPathExists = true;
					}
					
					$intCont = 0;
					foreach ($arrInserts as $sql) {
						echo(sendUpgradeMessage($sql, "color: blue;"));
						if ($blnExecuteProcess) {
							if ($aRepository->DataADOConnection->Execute($sql) === false) {
								$blnError = true;
								echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
								continue 2;	//Este es un error crítico, así que no puede continuar
							}
							
							//Renombra el archivo especificado
							if ($blnPathExists && isset($arrFileNamesToRename[$intCont])) {
								if (is_array($arrFileNamesToRename[$intCont])) {
									$strSourceName = (string) @$arrFileNamesToRename[$intCont][0];
									$strTargetName = (string) @$arrFileNamesToRename[$intCont][0];
									if ($strSourceName != '' && $strTargetName != '') {
										@rename($strCompleteSourcePath."\\".$strSourceName, $strCompleteSourcePath."\\".$strTargetName);
									}
								}
							}
						}
						$intCont++;
					}
				}
				
				//*************************************************************************************************************************
				//Agrega el registro a la tabla de comentarios
				//Para este proceso, simplemente obtiene todos los comentarios del FactKey seleccionado para las preguntas estándar que los
				//tienen activados y los graba con el nuevo FactKey
				$sql = "INSERT INTO SI_SV_SurveyAnswerComment (SurveyID, QuestionID, FactKey, MainFactKey, StrComment) 
					SELECT A.SurveyID, A.QuestionID, A.FactKey, {$intNewFactKey}, A.StrComment 
					FROM SI_SV_SurveyAnswerComment A, SI_SV_Question B, SI_SV_Section C 
					WHERE A.QuestionID = B.QuestionID AND B.SectionID = C.SectionID AND (C.SectionType IS NULL OR C.SectionType = ".sectNormal.") 
  					AND A.SurveyID = {$objSurvey->SurveyID} AND A.FactKey = {$intFactKeyDimVal} AND A.MainFactKey = {$intFactKey}";
				echo(sendUpgradeMessage($sql, "color: blue;"));
				if ($blnExecuteProcess) {
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						$blnError = true;
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						continue 2;	//Este es un error crítico, así que no puede continuar
					}
				}
				
				//*************************************************************************************************************************
				//Actualiza el registro de la tabla de firmas
				//En este caso, por error pudo haber agregado una firma de encuesta en cada uno de los registros de las secciones dinámicas
				//o maestro-detalle, siendo que realmente se necesita sólo un archivo por encuesta, pero el problema es que debido a un mal
				//diseño, se asoció como ID de la firma el FactKey en lugar del FactKeyDimVal, por lo tanto se tienen que renombrar estos
				//archivos y actualizar los registros para reflejar cual es ahora el registro único estándar de las encuestas
				//Para esto se tomará el primer archivo existente de firma, y se renombrará usando el nuevo FactKey, agregando el nombre
				//final como un nuevo registro
				//survey_22/signature_22_56.jpg
				$blnPathExists = false;
				$strCompleteSourcePath = getcwd()."\\surveysignature\\".trim($_SESSION["PABITAM_RepositoryName"]).
					"\\"."survey_".$objSurvey->SurveyID;
				if (file_exists($strCompleteSourcePath)) {
					$blnPathExists = true;
				}
				
				if ($blnPathExists) {
					$sql = "SELECT A.SurveyID, A.FactKey, A.PathImage 
						FROM SI_SV_SurveySignatureImg A 
						WHERE A.SurveyID = {$objSurvey->SurveyID} 
						ORDER BY A.SurveyID, A.FactKey";
					echo(sendUpgradeMessage($sql, "font-style: italic;"));
					$aRS = $aRepository->DataADOConnection->Execute($sql);
					if ($aRS == false) {
						$blnError = true;
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						continue 2;	//Este es un error crítico, así que no puede continuar
					}
					
					while(!$aRS->EOF) {
						$intTmpFactKey = (int) @$aRS->fields["factkey"];
						$strSignaturePath = (string) @$aRS->fields["pathimage"];
						if (trim($strSignaturePath) != '') {
							//En este caso si hay una firma grabada, así que se tiene que renombrar la imagen y agregar el registro apuntando al
							//nuevo FactKey
							$arrPathPieces = explode('/', $strSignaturePath);
							if ($arrPathPieces == 2) {
								$strSignatureName = trim((string) @$arrPathPieces[1]);
								$strOldFileName = $strSignatureName;
								if ($strSignatureName != '') {
									$arrSignaturePieces = explode('_', $strSignatureName);
									if (count($arrSignaturePieces) == 3) {
										$arrSignaturePieces[3] = $intNewFactKey.'.jpg';
										$strSignatureName = implode('_', $arrSignaturePieces);
										$arrPathPieces[1] = $strSignatureName;
										$strSignaturePath = implode('/', $arrPathPieces);
										if (file_exists($strCompleteSourcePath."\\".$strSignaturePath)) {
											//Este archivo de firma si existe, así que se utilizará para renombrar
											$sql = "INSERT INTO SI_SV_SurveySignatureImg (SurveyID, FactKey, PathImage) 
												VALUES ({$objSurvey->SurveyID}, {$intNewFactKey}, ".
												$aRepository->DataADOConnection->Quote($strSignaturePath).")";
											echo(sendUpgradeMessage($sql, "color: blue;"));
											if ($blnExecuteProcess) {
												if ($aRepository->DataADOConnection->Execute($sql) === false) {
													$blnError = true;
													echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
													continue 3;	//Este es un error crítico, así que no puede continuar
												}
											
												@rename($strCompleteSourcePath."\\".$strOldFileName, $strCompleteSourcePath."\\".$strSignatureName);
											}
										}
									}
								}
							}
							
							//Sólo tiene que actualizar la primer imagen de firma encontrada, las demás se pueden quedar
							break;
						}
						$aRS->MoveNext();
					}
				}
				
				//*************************************************************************************************************************
				//Agrega el registro al cubo global de encuestas
				//En este caso simplemente se extraerá la información del mismo cubo pero para el FactKey utilizado en el proceso, y nada mas
				//generará los keys de las dimensiones que deben cambiar para ajustar con el nuevo FactKey generado
				// ->GblSurveyDimID	->GblSurveyGlobalDimID	->GblSurveySingleRecordDimID
				global $gblEFormsErrorMessage;
				$intOldSurveyKey = getSurveyDimKey($aRepository, $aGblSurveyInstanceDim, $objSurvey);
				if ($intOldSurveyKey == false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Unable to get the global survey key. Error: ".((string) @$gblEFormsErrorMessage)));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				
				$intOldSurveyGlobalKey = getSurveyGlobalDimKey($aRepository, $aGblSurveyGlobalInstanceDim, $objSurvey, $intOldSurveyKey, $intFactKeyDimVal);
				if ($intOldSurveyGlobalKey == false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Unable to get the global survey report key. Error: ".((string) @$gblEFormsErrorMessage)));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				
				$intOldSurveySingleRecordKey = getSurveySingleRecordDimKey($aRepository, $aGblSurveySingleRecordInstanceDim, $objSurvey, $intOldSurveyKey, $intFactKeyDimVal, $intFactKey);
				if ($intOldSurveySingleRecordKey == false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Unable to get the global Survey single record key. Error: ".((string) @$gblEFormsErrorMessage)));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				
				//Obtiene la nueva llave de la dimensión de la FactKey agregada
				$intSurveySingleRecordKey = getSurveySingleRecordDimKey($aRepository, $aGblSurveySingleRecordInstanceDim, $objSurvey, $intOldSurveyKey, $intFactKeyDimVal, $intNewFactKey);
				if ($intSurveySingleRecordKey == false) {
					$blnError = true;
					echo(sendUpgradeMessage("***** Unable to get the new global Survey single record key. Error: ".((string) @$gblEFormsErrorMessage)));
					continue 2;	//Este es un error crítico, así que no puede continuar
				}
				
				//Inserta los datos del FactKey generado utilizado los del FactKey anterior
				$strGblSurveyDimID = "RIDIM_{$objSurvey->GblSurveyDimID}KEY";
				$strGblSurveyGlobalDimID = "RIDIM_{$objSurvey->GblSurveyGlobalDimID}KEY";
				$strGblSurveySingleRecordDimID = "RIDIM_{$objSurvey->GblSurveySingleRecordDimID}KEY";
				$sql = "INSERT INTO {$strGblFactTable} ($strGblSurveySingleRecordDimID, {$strGblFactTableFields}) 
					SELECT {$intSurveySingleRecordKey}, $strGblFactTableFields 
					FROM {$strGblFactTable} 
					WHERE $strGblSurveyDimID = {$intOldSurveyKey} AND 
						{$strGblSurveyGlobalDimID} = {$intOldSurveyGlobalKey} AND 
						{$strGblSurveySingleRecordDimID} = {$intOldSurveySingleRecordKey}";
				echo(sendUpgradeMessage($sql, "color: blue;"));
				if ($blnExecuteProcess) {
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						$blnError = true;
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						continue 2;	//Este es un error crítico, así que no puede continuar
					}
				}
			}
		}
		else {
			echo("<BR>***** This survey has a single standard record for every entry, the process is not required");
		}
	}
	else {
		echo("<BR>***** This survey has a single standard record for every entry, the process is not required");
	}
	
	//*************************************************************************************************************************
	//Llenado de los campos de sección dinámica
	//*************************************************************************************************************************
	//Este campo contiene un valor sólo en el primer FactKey de cada agrupación de página de la sección dinámica
	if (!is_null($dynamicSection)) {
		echo("<BR><BR>***** Updating Dynamic section fields");
		/* Sí hay una sección dinámica y se asume que el campo ya tiene valores vacios o en el peor de los casos NULL, así que se
		hace un join con la tabla original del catálogo de la dinámica (si es que aún no está desasociada) para obtener las
		descripciones y basado en la del atributo usado en la dinámica actualizará el campo (es independiente de si hay o no alguna
		pregunta múltiple choice ya que las páginas son las mismas para todos los registros específicos si hubiera multiple-choice)
		*/
		$blnContinue = true;
		//Validaciones previas
		$objCatalog = @$arrCatalogsByID[$dynamicSectionCatID];
		if (is_null($objCatalog)) {
			echo(sendUpgradeMessage("***** Error: Catalog not found ($dynamicSectionCatID)"));
			$blnError = true;
			$blnContinue = false;
		}
		else {
			if (is_null($aSCDynQuestionInstance)) {
				echo(sendUpgradeMessage("***** Error: Unable to find a catalog question inside a standard section to generate this dynamic section"));
				$blnError = true;
				$blnContinue = false;
			}
			else {
				if ($aSCDynQuestionInstance->IndDimID != $objCatalog->ParentID) {
					echo("<BR>***** Question '{$aSCDynQuestionInstance->QuestionText}' is already migrated, this process is not required");
					$blnContinue = false;
				}
			}
		}
		
		if ($blnContinue) {
			//Sí es necesario realizar el proceso
			//*************************************************************************************************************************
			//Llenado del campo DynamicPageDSC
			//*************************************************************************************************************************
			/* Este campo se llena con el valor exacto del atributo asignado a la sección dinámica, pero sólo para el primer FactKey que
			utilice dicho valor, debido a esto primero se tiene que hacer una agrupación de los registros que tienen este valor en cada captura
			y sólo actualizar el Min(FactKey) de cada una ya que siempre se graban en orden, por lo que ese sería el primero literalmente
			*/
			echo("<BR><BR>***** Updating DynamicPageDSC field");
			$intCatMemberID = $dynamicSectionCatMemberID;
			$objCatalogMembersColl = $arrCatMembersByCatalog[$dynamicSectionCatID];
			foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
				if ($objCatalogMember->MemberID == $intCatMemberID) {
					break;
				}
			}
			
			if (!is_null($objCatalogMember)) {
				$intTotalRows = 0;
				$sql = "SELECT COUNT(*) AS 'TotalRows' 
					FROM {$strSurveyTable} 
					WHERE DynamicPageDSC = '' OR DynamicPageDSC IS NULL";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					$intTotalRows = (int) @$aRS->fields["totalrows"];
				}
				$arrSurveyStats[$intSurveyID]['DynamicPageDSCEmptyRows'] = $intTotalRows;
				
				$strCatalogTable = $objCatalog->TableName;
				$tmpFieldKey = $strCatalogTable."KEY";
				$fieldName = "DSC_".$objCatalogMember->ClaDescrip;
				$sql = "UPDATE {$strSurveyTable} t1 
  						INNER JOIN ( 
    						SELECT MIN(A.FactKey) AS 'MinGrpdFactKey', MAX(B.{$fieldName}) AS 'AttributeValue' 
    						FROM {$strSurveyTable} A 
	      						INNER JOIN {$strCatalogTable} B ON A.{$aSCDynQuestionInstance->SurveyField} = B.{$tmpFieldKey} 
    						WHERE A.EntrySectionID = {$dynamicSectionID} 
    						GROUP BY A.FactKeyDimVal, A.EntrySectionID, B.{$fieldName} 
  						) t2 ON t1.FactKey = t2.MinGrpdFactKey 
							SET t1.DynamicPageDSC = t2.AttributeValue 
					WHERE t1.DynamicPageDSC = '' OR t1.DynamicPageDSC IS NULL";
				echo(sendUpgradeMessage($sql, "color: blue;"));
				if ($blnExecuteProcess) {
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						$blnError = true;
					}
					else {
						$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
						$arrSurveyStats[$intSurveyID]['DynamicPageDSCAffectedRows'] = $intAffectedRows;
					}
				}
			}
			else {
				echo(sendUpgradeMessage("***** Error: Catalog member not found ($dynamicSectionCatID - $intCatMemberID)"));
				$blnError = true;
			}
			
			//*************************************************************************************************************************
			//Llenado del campo DynamicOptionDSC
			//*************************************************************************************************************************
			/* Este campo se llena con el valor exacto del atributo siguiente al asignado a la sección dinámica si es que existen preguntas
			multiple-choice, de lo contrario se graba directamente el mismo valor que en el campo DynamicPageDSC ya que significaría que cada
			p+agina dinámica es el único registro que se graba por ella, en lugar de grabar múltiples registros por página
			*/
			echo("<BR><BR>***** Updating DynamicOptionDSC field");
			if ($dynamicSectionChildCatMemberID > 0) {
				$intCatMemberID = $dynamicSectionChildCatMemberID;
			}
			else {
				$intCatMemberID = $dynamicSectionCatMemberID;
			}
			$objCatalogMembersColl = $arrCatMembersByCatalog[$dynamicSectionCatID];
			foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
				if ($objCatalogMember->MemberID == $intCatMemberID) {
					break;
				}
			}
			
			if (!is_null($objCatalogMember)) {
				$intTotalRows = 0;
				$sql = "SELECT COUNT(*) AS 'TotalRows' 
					FROM {$strSurveyTable} 
					WHERE DynamicOptionDSC = '' OR DynamicOptionDSC IS NULL";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					$intTotalRows = (int) @$aRS->fields["totalrows"];
				}
				$arrSurveyStats[$intSurveyID]['DynamicOptionDSCEmptyRows'] = $intTotalRows;
				
				$strCatalogTable = $objCatalog->TableName;
				$tmpFieldKey = $strCatalogTable."KEY";
				$fieldName = "DSC_".$objCatalogMember->ClaDescrip;
				$sql = "UPDATE {$strSurveyTable} A 
  						INNER JOIN {$strCatalogTable} B ON A.{$aSCDynQuestionInstance->SurveyField} = B.{$tmpFieldKey} 
						SET A.DynamicOptionDSC = B.{$fieldName} 
					WHERE A.entrysectionID = {$dynamicSectionID} AND (A.DynamicOptionDSC IS NULL OR A.DynamicOptionDSC = '')";
				echo(sendUpgradeMessage($sql, "color: blue;"));
				if ($blnExecuteProcess) {
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						$blnError = true;
					}
					else {
						$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
						$arrSurveyStats[$intSurveyID]['DynamicOptionDSCAffectedRows'] = $intAffectedRows;
					}
				}
			}
			else {
				echo(sendUpgradeMessage("***** Error: Catalog member not found ($dynamicSectionCatID - $intCatMemberID)"));
				$blnError = true;
			}
			
			//*************************************************************************************************************************
			//Llenado del campo DynamicValue
			//*************************************************************************************************************************
			/* Este campo se llena de forma similar al DynamicOptionDSC, sólo que en lugar de traer directamente la descripción del atributo
			en cuestión, trae la concatenación de todos los atributos previos incluyendo al finalmente usado en DynamicOptionDSC, en un formato 
			que entiende el proceso de grabado el cual incluye separadores y los CLA_DESCRIP de las dimensiones independientes correspondientes
			a cada atributo
			*/
			echo("<BR><BR>***** Updating DynamicValue field");
			if ($dynamicSectionChildCatMemberID > 0) {
				$intCatMemberID = $dynamicSectionChildCatMemberID;
			}
			else {
				$intCatMemberID = $dynamicSectionCatMemberID;
			}
			$objCatalogMembersColl = $arrCatMembersByCatalog[$dynamicSectionCatID];
			$arrConcatElements = array();
			$strNewAttribSep = '';
			$strNA = $aRepository->DataADOConnection->Quote('*NA');
			foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
				$fieldName = "DSC_".$objCatalogMember->ClaDescrip;
				$arrConcatElements[] = $aRepository->DataADOConnection->Quote($strNewAttribSep."key_{$objCatalogMember->IndDimID}_SVSep_");
				$arrConcatElements[] = "COALESCE(b.{$fieldName}, {$strNA})";
				
				if ($objCatalogMember->MemberID == $intCatMemberID) {
					break;
				}
				$strNewAttribSep = '_SVElem_';
			}
			
			if (!is_null($objCatalogMember) && count($arrConcatElements) > 0) {
				$intTotalRows = 0;
				$sql = "SELECT COUNT(*) AS 'TotalRows' 
					FROM {$strSurveyTable} 
					WHERE DynamicValue = '' OR DynamicValue IS NULL";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					$intTotalRows = (int) @$aRS->fields["totalrows"];
				}
				$arrSurveyStats[$intSurveyID]['DynamicValueEmptyRows'] = $intTotalRows;
				
				$strCatalogTable = $objCatalog->TableName;
				$tmpFieldKey = $strCatalogTable."KEY";
				$fieldName = "DSC_".$objCatalogMember->ClaDescrip;
				$sql = "UPDATE {$strSurveyTable} A 
  						INNER JOIN {$strCatalogTable} B ON A.{$aSCDynQuestionInstance->SurveyField} = B.{$tmpFieldKey} 
						SET A.DynamicValue = concat(".implode(',', $arrConcatElements).") 
					WHERE A.entrysectionID = {$dynamicSectionID} AND (A.DynamicValue IS NULL OR A.DynamicValue = '')";
				echo(sendUpgradeMessage($sql, "color: blue;"));
				if ($blnExecuteProcess) {
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						$blnError = true;
					}
					else {
						$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
						$arrSurveyStats[$intSurveyID]['DynamicValueAffectedRows'] = $intAffectedRows;
					}
				}
			}
			else {
				echo(sendUpgradeMessage("***** Error: Catalog member not found ($dynamicSectionCatID - $intCatMemberID)"));
				$blnError = true;
			}
		}
	}
	else {
		echo("<BR>***** This survey do not has a dynamic section");
	}
	
	//*************************************************************************************************************************
	//Llenado de los campos Cat_
	//*************************************************************************************************************************
	/* Estos campos se llenan idéntico a DynamicValue, sólo que no son exclusivos de secciones dinámicas y el string concatenado en
	ese mismo formato se genera hasta el atributo específico seleccionado para la pregunta, por lo que dependiendo de esta es la cantidad
	de atributos a concatenar
	*/
	echo("<BR><BR>***** Updating cat_ fields");
	foreach ($objQuestionColl->Collection as $objQuestion) {
		if ($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID > 0) {
			echo("<BR>***** Updating {$objQuestion->SurveyCatField} field");
			
			$blnContinue = true;
			//Validaciones previas
			$intCatalogID = $objQuestion->CatalogID;
			$objCatalog = @$arrCatalogsByID[$intCatalogID];
			if (is_null($objCatalog)) {
				echo(sendUpgradeMessage("***** Error: Catalog not found ($intCatalogID)"));
				$blnError = true;
				$blnContinue = false;
			}
			else {
				if ($objQuestion->IndDimID != $objCatalog->ParentID) {
					echo("<BR>***** Question '{$objQuestion->QuestionText}' is already migrated, this process is not required");
					$blnContinue = false;
				}
			}
			
			if ($blnContinue) {
				$intCatMemberID = $objQuestion->CatMemberID;
				$objCatalogMembersColl = $arrCatMembersByCatalog[$intCatalogID];
				$arrConcatElements = array();
				$strNewAttribSep = '';
				$strNA = $aRepository->DataADOConnection->Quote('*NA');
				foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
					$fieldName = "DSC_".$objCatalogMember->ClaDescrip;
					$arrConcatElements[] = $aRepository->DataADOConnection->Quote($strNewAttribSep."key_{$objCatalogMember->IndDimID}_SVSep_");
					$arrConcatElements[] = "COALESCE(b.{$fieldName}, {$strNA})";
					
					if ($objCatalogMember->MemberID == $intCatMemberID) {
						break;
					}
					$strNewAttribSep = '_SVElem_';
				}
				
				if (!is_null($objCatalogMember) && count($arrConcatElements) > 0) {
					$intTotalRows = 0;
					$sql = "SELECT COUNT(*) AS 'TotalRows' 
						FROM {$strSurveyTable} 
						WHERE {$objQuestion->SurveyCatField} = '' OR {$objQuestion->SurveyCatField} IS NULL";
					echo(sendUpgradeMessage($sql, "font-style: italic;"));
					$aRS = $aRepository->DataADOConnection->Execute($sql);
					if ($aRS && !$aRS->EOF) {
						$intTotalRows = (int) @$aRS->fields["totalrows"];
					}
					$arrSurveyStats[$intSurveyID][$objQuestion->SurveyCatField.'EmptyRows'] = $intTotalRows;
					
					$strCatalogTable = $objCatalog->TableName;
					$tmpFieldKey = $strCatalogTable."KEY";
					$fieldName = "DSC_".$objCatalogMember->ClaDescrip;
					$sql = "UPDATE {$strSurveyTable} A 
  							INNER JOIN {$strCatalogTable} B ON A.{$objQuestion->SurveyField} = B.{$tmpFieldKey} 
							SET A.{$objQuestion->SurveyCatField} = concat(".implode(',', $arrConcatElements).") 
						WHERE (A.{$objQuestion->SurveyCatField} IS NULL OR A.{$objQuestion->SurveyCatField} = '')";
					echo(sendUpgradeMessage($sql, "color: blue;"));
					if ($blnExecuteProcess) {
						if ($aRepository->DataADOConnection->Execute($sql) === false) {
							echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
							$blnError = true;
						}
						else {
							$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
							$arrSurveyStats[$intSurveyID][$objQuestion->SurveyCatField.'AffectedRows'] = $intAffectedRows;
						}
					}
				}
				else {
					echo(sendUpgradeMessage("***** Error: Catalog member not found ($intCatalogID - $intCatMemberID)"));
					$blnError = true;
				}
			}
		}
	}
	
	$strOriginalWD = getcwd();
	//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
	//para asignarlos a las variables de session que se ocupa en el model manager
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
	require_once("../model_manager/filescreator.inc.php");
	require_once("../model_manager/attribdimension.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	chdir($strOriginalWD);
	
	//*************************************************************************************************************************
	//Llenado de las dimensiones independientes
	//*************************************************************************************************************************
	/* En este caso, se deben recorrer todos los catálogos de la encuesta y obtener el último atributo usado del mismo (y en el caso
	del catálogo de la sección Maestro-detalle, el último utilizado por ella) para posteriormente llenar las tablas de todas las dimensiones
	de atributos desde el primero hasta el correspondiente con este catálogo basado en los keys subrrogados, esto sin embargo sólo en los
	casos donde las preguntas no estuvieran ya convertidas al nuevo formato, porque de lo contrario no hay seguridad que los keys grabados
	serían los correctos de la captura que originalmente se llevó a cabo
	*/
	echo("<BR><BR>***** Updating dimension's data");
	foreach ($arrCatalogIDs as $intCatalogID) {
		$objCatalog = @$arrCatalogsByID[$intCatalogID];
		if (is_null($objCatalog)) {
			echo(sendUpgradeMessage("***** Error: Catalog not found ($intCatalogID)"));
			$blnError = true;
			continue;
		}
		echo("<BR><BR>***** Catalog: ".$objCatalog->CatalogName);
		
		$intCatalogID = $objCatalog->CatalogID;
		$intCatMemberID = 0;
		$blnContinue = false;
		$objFirstCatalogQuestion = null;
		if ($intCatalogID == $dynamicSectionCatID) {
			//Si es el catálogo de la sección dinámica, teoricamente no debería existir ninguna pregunta de catálogo posterior a la sección, así
			//que se reutilizan directamente hasta los atributos de la sección nada mas
			if ($dynamicSectionChildCatMemberID > 0) {
				$intCatMemberID = $dynamicSectionChildCatMemberID;
			}
			else {
				$intCatMemberID = $dynamicSectionCatMemberID;
			}
			
			//Sin embargo, el proceso sólo será válido si por lo menos alguna pregunta simple choice que use este catálogo aun no ha sido
			//migrada, ya que de lo contrario la sección ya debería estar convertida y no se debe sobreescribir la información
			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID == $intCatalogID) {
					if ($objQuestion->CatMemberID > 0) {
						if (is_null($objFirstCatalogQuestion)) {
							$objFirstCatalogQuestion = $objQuestion;
						}
						
						if ($objQuestion->IndDimID == $objCatalog->ParentID) {
							//Esta pregunta no ha sido migrada, así que es válida para migrar
							$blnContinue = true;
						}
					}
				}
			}
		}
		else {
			//En este caso se identifica si hay alguna pregunta simple choice que utilice este catálogo y se obtiene el último atributo usado,
			//como las preguntas se cargan en orden basta con obtener la última ocurrencia del mismo catálogo
			//Sin embargo, el proceso sólo será válido si por lo menos alguna pregunta simple choice que use este catálogo aun no ha sido
			//migrada, ya que de lo contrario la sección ya debería estar convertida y no se debe sobreescribir la información
			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID == $intCatalogID) {
					if ($objQuestion->CatMemberID > 0) {
						if (is_null($objFirstCatalogQuestion)) {
							$objFirstCatalogQuestion = $objQuestion;
						}
						
						if ($objQuestion->IndDimID == $objCatalog->ParentID) {
							//Esta pregunta no ha sido migrada, así que es válida para migrar
							$blnContinue = true;
							$intCatMemberID = $objQuestion->CatMemberID;
						}
					}
				}
			}
		}
		
		//Teóricamente, si hay alguna pregunta no migrada y a la vez existen otras que si lo están, sólo podría ser porque el proceso de migración
		//fué interrumpido antes de terminar y/o se hizo una edición con versiones v3 o anteriores a la encuesta, por lo tanto se asumirá que
		//en cualquier caso donde exista al menos una pregunta no migrada, el proceso no está completo y se volverá a ejecutar en todas las 
		//dimensiones desde la primera hasta la última usada por el catálogo, asumiendo que el catálogo original y los keys subrrogados de las
		//preguntas que apuntan a él aun son un match perfecto
		if ($intCatMemberID > 0 && $blnContinue) {
			//echo("<BR>***** Updating {$objQuestion->SurveyCatField} field");
			$objCatalogMembersColl = $arrCatMembersByCatalog[$intCatalogID];
			foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
				echo("<BR>***** Inserting attribute '{$objCatalogMember->MemberName}' data in the dimension table");
				if ($objCatalogMember->IndDimID <= 0) {
					echo(sendUpgradeMessage("***** Error: Dissociated dimension not found for attribue '{$objCatalogMember->MemberName}'"));
					$blnError = true;
					continue;
				}
				
				$strOriginalWD = getcwd();
				$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, -1, -1, $objCatalogMember->IndDimID);
				chdir($strOriginalWD);
				if (is_null($anInstanceModelDim)) {
					echo(sendUpgradeMessage("***** Error: Unable to load dissociated dimension {$objCatalogMember->IndDimID} for attribue '{$objCatalogMember->MemberName}'"));
					$blnError = true;
					continue;
				}
				
				$strDimensionTable = "RIDIM_".$objCatalogMember->IndDimID;
				$strDimensionFieldKey = $strDimensionTable."KEY";
				$strDimensionFieldName = "DSC_".$objCatalogMember->IndDimID;
				$intTotalRows = 0;
				$sql = "SELECT COUNT(*) AS 'TotalRows' 
					FROM {$strDimensionTable} 
					WHERE $strDimensionFieldKey > 0";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					$intTotalRows = (int) @$aRS->fields["totalrows"];
				}
				$arrSurveyStats[$intSurveyID][$objCatalogMember->MemberName.'InitialRows'] = $intTotalRows;
				
				$strCatalogTable = $objCatalog->TableName;
				$tmpFieldKey = $strCatalogTable."KEY";
				$fieldName = "DSC_".$objCatalogMember->ClaDescrip;
				$sql = "INSERT INTO {$strDimensionTable} ({$strDimensionFieldName}) 
					SELECT DISTINCT b.{$fieldName} as 'DimDesc' 
					FROM {$strSurveyTable} a 
  						INNER JOIN {$strCatalogTable} b ON a.{$objFirstCatalogQuestion->SurveyField} = b.{$tmpFieldKey} 
  						LEFT JOIN {$strDimensionTable} c ON b.{$fieldName} = c.{$strDimensionFieldName} 
					WHERE b.{$tmpFieldKey} > 1 AND COALESCE(c.{$strDimensionFieldKey}, 0) = 0";
				echo(sendUpgradeMessage($sql, "color: blue;"));
				if ($blnExecuteProcess) {
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						$blnError = true;
					}
					else {
						$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
						$arrSurveyStats[$intSurveyID][$objCatalogMember->MemberName.'NewRows'] = $intAffectedRows;
					}
				}
				
				echo("<BR>***** Updating attribute '{$objCatalogMember->MemberName}' data in the fact table");

				$intTotalRows = 0;
				$sql = "SELECT COUNT(*) AS 'TotalRows' 
					FROM {$strFactTable} 
					WHERE {$strDimensionFieldKey} IS NULL";
				echo(sendUpgradeMessage($sql, "font-style: italic;"));
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					$intTotalRows = (int) @$aRS->fields["totalrows"];
				}
				$arrSurveyStats[$intSurveyID][$objCatalogMember->MemberName.'EmptyRows'] = $intTotalRows;
				
				//Se agregó el OR porque por alguna razón cuando se probó el proceso, el campo join en la tabla de hechos ya tenía el valor de 1
				//asignado a todos los registros, así que no actualizaba nada buscando sólamente NULL
				$sql = "UPDATE $strFactTable c 
  						INNER JOIN {$strSurveyTable} a ON a.FactKey = c.FactKey 
  						INNER JOIN {$strCatalogTable} b ON a.{$objFirstCatalogQuestion->SurveyField} = b.{$tmpFieldKey} 
  						INNER JOIN {$strDimensionTable} d ON b.{$fieldName} = d.{$strDimensionFieldName} 
						SET c.{$strDimensionFieldKey} = d.{$strDimensionFieldKey} 
					WHERE c.{$strDimensionFieldKey} IS NULL OR c.{$strDimensionFieldKey} = 1";
				echo(sendUpgradeMessage($sql, "color: blue;"));
				if ($blnExecuteProcess) {
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
						$blnError = true;
					}
					else {
						$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
						$arrSurveyStats[$intSurveyID][$objCatalogMember->MemberName.'AffectedRows'] = $intAffectedRows;
					}
				}
				
				if ($objCatalogMember->MemberID == $intCatMemberID) {
					break;
				}
			}
		}
		else {
			echo("<BR>***** This catalog is already updated");
		}
	}
	
	//*************************************************************************************************************************
	//Cambiar el switch para usar la nueva estructura
	//*************************************************************************************************************************
	/* Sólo se puede cambiar el switch si no se considera que hubo un error, de lo contrario la encuesta al intentar grabarse
	con el nuevo formato podría causar mas problemas. Se requerirá solucionar el problema que hubiera ocurrido y lanzar de
	nuevo el proceso de migración
	*/
	echo("<BR><BR>***** Switching to dissociated dimensions");
	if (!$blnError) {
		$arrCatalogIDs = @$arrCatalogsBySurvey[$intSurveyID];
		if (is_null($arrCatalogIDs) || !is_array($arrCatalogIDs)) {
			$arrCatalogIDs = array();
		}
		
		foreach ($arrCatalogIDs as $intCatalogID) {
			$objCatalog = @$arrCatalogsByID[$intCatalogID];
			if (is_null($objCatalog)) {
				echo(sendUpgradeMessage("***** Error: Catalog not found ($intCatalogID)"));
				$blnError = true;
				continue;
			}
			echo("<BR>***** Catalog: ".$objCatalog->CatalogName);
			
			$objCatalogMembersColl = @$arrCatMembersByCatalog[$intCatalogID];
			if (is_null($objCatalogMembersColl)) {
				echo(sendUpgradeMessage("***** Error: Catalog members not found ($intCatalogID)"));
				$blnError = true;
				continue;
			}
			
			//En este caso se identifica si hay alguna pregunta simple choice que utilice este catálogo y se cambia el CLA_DESCRIP del atributo
			//que utiliza, el cual originalmente apuntaba a la dimensión catálogo pero ahora lo tiene que hacer hacia la dimensión independiente
			//del atributo correspondiente
			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID == $intCatalogID) {
					if ($objQuestion->CatMemberID > 0) {
						$intCatMemberID = $objQuestion->CatMemberID;
						
						foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
							if ($objCatalogMember->MemberID == $intCatMemberID) {
								break;
							}
						}
						
						if (!is_null($objCatalogMember)) {
							//Actualiza el ID de dimensión a utilizar, sólo si la pregunta aun apuntaba al ID del catálogo
							if ($objQuestion->IndDimID == $objCatalog->ParentID) {
								$sql = "UPDATE SI_SV_Question SET 
										IndDimID = {$objCatalogMember->IndDimID} 
									WHERE QuestionID = {$objQuestion->QuestionID}";
								echo(sendUpgradeMessage($sql, "color: blue;"));
								if ($blnExecuteProcess) {
									if ($aRepository->DataADOConnection->Execute($sql) === false) {
										echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
										$blnError = true;
										continue 2;
									}
									$objQuestion->IndDimID = $objCatalogMember->IndDimID;
								}
							}
						}
					}
				}
			}
		}
		
		//Finalmente actualiza la definición de la encuesta para permitir grabado con dimensiones independientes y que se refresquen las
		//definiciones
		$sql = "UPDATE SI_SV_Survey SET 
				UseStdSectionSingleRec = 1, 
				DissociateCatDimens = 1 
			WHERE SurveyID = {$intSurveyID}";
		echo(sendUpgradeMessage($sql, "color: blue;"));
		if ($blnExecuteProcess) {
			if ($aRepository->DataADOConnection->Execute($sql) === false) {
				echo(sendUpgradeMessage("***** Error: ".(string) @$aRepository->DataADOConnection->ErrorMsg()));
				$blnError = true;
			}
			else {
				$objSurvey->UseStdSectionSingleRec = 1;
				$objSurvey->DissociateCatDimens = 1;
				$objSts = $objSurvey->save();
				if (!is_object($objSts)) {
					echo(sendUpgradeMessage("***** Error: ".(string) $objSts));
					$blnError = true;
				}
			}
		}
	}
	
	//*************************************************************************************************************************
	//Reporte de inconsistencias
	//*************************************************************************************************************************
	/* A partir de este punto, se mostrarán todos los Keys que contienen algún tipo de error así como las estadísticas de lo que se
	actualizó con el proceso de migración
	*/
	echo(sendUpgradeMessage("<BR>Statistics for '{$objSurvey->SurveyName}'", "color: green; font-weight: bold; font-size: 20px;"));
	PrintMultiArray($arrSurveyStats[$intSurveyID]);
}

echo("<BR><BR><BR>***** Process finished *****");

//Regresa el valor del campo especificado del array o NULL si no se encuentra. Si se activa el parámetro $bNullString, en lugar de un NULL se
//regresará un string con la palabra NULL listo para usarse en una sentencia UPDATE|INSERT
function getFieldValueFromArr($arrayData, $sFieldName, $bNullString = true) {
	$fieldName = strtolower($sFieldName);
	$strValue = @$arrayData[$fieldName]; 
	
	if (is_null($strValue) && $bNullString) {
		$strValue = "NULL";
	}
	
	return $strValue;
}

//Regresa un valor default válido para una pregunta cuando esta se considera no contestada, basandose en el tipo de sección y pregunta
//Esta función es sólo para los valores de la tabla paralela, ya que en la FactTable todos son dimensiones así que siempre el default del valor
//no seleccionado es el Key 1
//El parámetro $bQuoteStrings provocará que en el caso de regresar Strings se aplique un Quote para prepararlo para un estatuto SQL
//Si se activa el parámetro $bNullString, en lugar de un NULL se regresará un string con la palabra NULL listo para usarse en una sentencia UPDATE|INSERT
function getEmptyValueForQuestion($aQuestionInstance, $bQuoteStrings = true, $bNullString = true) {
	$objQuestion = $aQuestionInstance;
	if (is_null($objQuestion) || !is_object($objQuestion)) {
		if ($bNullString) {
			return 'NULL';
		}
		else {
			return NULL;
		}
	}
	
	$strDefaultValue = @$aQuestionInstance->Repository->DataADOConnection->Quote('');
	switch ($objQuestion->QTypeID) {
		case qtpOpenNumeric:
		case qtpCalc:
			//Las preguntas numéricas y calculadas guardaban por default 0
			$strDefaultValue = 0;
			break;
			
		case qtpOpenDate:
		case qtpOpenString:
		case qtpOpenAlpha:
		case qtpOpenTime:
		//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		case qtpGPS:
		//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
		case qtpBarCode:
			//Las preguntas que responden texto graban vacio por default
			$strDefaultValue = @$aQuestionInstance->Repository->DataADOConnection->Quote('');
			break;
			
		case qtpSingle:
			if ($objQuestion->CatalogID > 0) {
				$strDefaultValue = @$aQuestionInstance->Repository->DataADOConnection->Quote('1');
			}
			else {
				$strDefaultValue = @$aQuestionInstance->Repository->DataADOConnection->Quote('');
			}
			break;
			
		case qtpMulti:
			//Las preguntas múltiple choice en dinámicas se graban dependiendo del tipo de captura
			switch ($objQuestion->MCInputType) {
				case mpcCheckBox:
					if ($objQuestion->SectionType == sectDynamic) {
						//CheckBox == 1 | 0 (marcada | desmarcada). Sólo se puede saber si se contestó si está marcada, 
						//no cuando está desmarcada porque pudiera ser el default
						$strDefaultValue = 0;
					}
					else {
						$strDefaultValue = @$aQuestionInstance->Repository->DataADOConnection->Quote('');
					}
					
				case mpcNumeric:
					if ($objQuestion->SectionType == sectDynamic) {
						//Numérica == Número capturado. Tal como las numéricas/calculadas, graban un 0 por default
						$strDefaultValue = 0;
					}
					else {
						//Numérica == Lista separada por ; de los números capturados pero incluyendo el total de
						//opciones existentes en la captura, llenando sus espacios con vacio
						//Se grabará NULL para no complicar las cosas identificando cuantas opciones de respuesta tiene
						$strDefaultValue = NULL;
					}
					break;
				
				default:
					if ($objQuestion->SectionType == sectDynamic) {
						//Texto == Texto capturado. Tal como las alfanuméricas graban '' por default
						$strDefaultValue = @$aQuestionInstance->Repository->DataADOConnection->Quote('');
					}
					else {
						//Texto == Lista separada por ; de los textos capturados  pero incluyendo el total de
						//opciones existentes en la captura, llenando sus espacios con vacio
						//Se grabará NULL para no complicar las cosas identificando cuantas opciones de respuesta tiene
						$strDefaultValue = NULL;
					}
					break;
			}
			break;
	}
	
	if (is_null($strDefaultValue) && $bNullString) {
		$strDefaultValue = "NULL";
	}
	
	return $strDefaultValue;
}
?>