<?php
/*
2013-11-11@ARMZ:(@userErrorHandler)  Clase que al instanciarla en tables.class.php recibira los errores que el usuario tenga en la aplicacion
*/

class BITAMAppError
{
	
	//
	public $errors = array();
	
	static function NewInstance()
	{
		$aBitamAppError = new BITAMAppError();
		return $aBitamAppError;
	}
	
	public function setError($errorDescription, $errorMsg = "", $errorNo = 0, $query = "")
	{
		$this->setEntry($errorDescription, $errorMsg, $errorNo, $query, 1);
	}

	public function setLog($errorDescription, $errorMsg = "", $errorNo = 0, $query = "")
	{
		$this->setEntry($errorDescription, $errorMsg, $errorNo, $query, 2);
	}

	/**
	 * Agrega una entrada a la bitacora
	 * @param [type]  $errorDescription [description]
	 * @param string  $errorMsg         [description]
	 * @param integer $errorNo          [description]
	 * @param string  $query            [description]
	 * @param integer $type             Tipo de entrada, 1, error, 2 log
	 */
	public function setEntry($errorDescription, $errorMsg = "", $errorNo = 0, $query = "", $type = 1)
	{
		$res = $errorDescription;
		if(!is_array($res))
		{
			$res = array("ErrorNo"=>$errorNo, "ErrorMsg"=>$errorMsg, "Query" => $query, "ErrorDescription"=>$errorDescription, "type" => $type);
		} else 
		{
			$res["type"] = $type;
		}
		$this->errors[] = $res;	
	}
	
	public function getArrayErrorDescription()
	{
		$res = array();
		foreach($this->errors as $eachError)
		{
			$res[] = $eachError["ErrorDescription"];
		}
		
		return $res;
	}

	/**
	 * Regresa un array de string de cada error almacenado
	 * @return array Array de strings
	 */
	public function toString()
	{
		$res = array();

		foreach($this->errors as $eachError)
		{
			$res[] = "ErrorNo: {$eachError["ErrorNo"]},\n Description: {$eachError["ErrorDescription"]},\n Message: {$eachError["ErrorMsg"]},\n Query: {$eachError["Query"]}";
		}

		return $res;
	}
	
	/**
	 * Cuenta los numeros de entrada que sean errores
	 * @return [type] [description]
	 */
	public function count()
	{
		$nCountRes = 0;
		$nCount = count($this->errors);
		for($i = 0; $i < $nCount; $i++)
		{
			if( $this->errors[$i]["type"] == 1 ) $nCountRes = $nCountRes + 1;
		}
		
		return $nCountRes;
	}

/*
	public function sendReportToEmail($subject, $body, $emails)
	{
		global $aConfiguration;
		$mail = $aConfiguration->getEmailObject();

		global $path_eBavel;
		$template = file_get_contents($path_eBavel . '/template/misc/errorreportsync.html');

		if(!is_array($emails))
		{
			$arrEmails[] = $emails;
		} else {
			$arrEmails = $emails;
		}

		foreach ($arrEmails as $eachEmail) {
			$mail->AddAddress($eachEmail);
		}
			
		$mail->Subject = $subject;
		$mail->CharSet = 'UTF-8';

		$body .= "\n\n--------------------------------------------\n\n";
		$body .= implode("\n\n--------------------------------------------\n\n", $this->toString());

		$body = nl2br($body);

		$template = str_replace("{title}", $subject, $template);
		$template = str_replace("{body}", $body, $template);

		$mail->Body = $template;
		$mail->FromName = "Data Sync Error!";
		try
		{
			$res = $mail->Send();
		}
		catch (Exception $e)
		{
			$res = false;
		}

		return $res;
	}
*/
	
}

?>