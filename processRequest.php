<?php
/* Este archivo recibirá solicitudes de grabado y borrado de diferentes elementos desde el árbol de la ventana de rediseño, así que ejecutará la acción
correspondiente utilizando las clases normales del Framework, pero al terminar ejecutará código específico de la ventana del Administrador DHTMLX para actualizar
el árbol, o cambiar los componentes visibles según el tipo de proceso que se hubiera solicitado
	Por facilidad NO se permitirá eliminar la Forma ni copiarla, pero los demás eventos de Borrado o Grabado (sólo nuevos elementos) deben actualizar componentes
	
	Eventos soportados y probados:
	********************************************************************************************************************************************************
	**** Utilizando Ajax y JSON **** reqAjax
		- Se recibirán parámetros del objeto a crear en formato JSON
		- Se asume que se invoca a este archivo desde Ajax, así que regresará un JSON con código de error o bien la URL que habría que cargar, de esta manera
			se puede permitir quedarse en la ventana de creación/edición sin cambiar el foco
		- Quien invoca es responsable de hacer las redirecciones
	**** Eventos soportados:
		- Grabado de una nueva forma (desde la vista tipo Metro)
			- Parámetros: RequestType=reqAjax & ObjectType=# & Process=Add & responseformat=json
	********************************************************************************************************************************************************
	
	********************************************************************************************************************************************************
	**** Utilizando IFrame y ejecución de código **** reqIFrame
		- Se recibirán parámetros del evento a realizar con algunos datos adicionales en GET/POST directo (No en JSON, ya que vienen de una forma generalmente
			o bien la simulan)
		- Se asume que se invoca este archivo desde un attachURL que espera cargar contenido en el frame de una celda del LayOut de la ventana de diseño
		- El propio código generado es responsable de las redirecciones o ejecución de funcionalidad
	**** Eventos soportados:
	
	X- Crear una nueva sección
*/
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("eSurveyServiceMod.inc.php");

global $gbDesignMode;
$gbDesignMode = true;
//@JAPR 2015-07-31: Corregido un bug, si no se activa el appVersion en este punto, las definiciones de imagenes no se descargan como el preview las necesita, así
//que en este punto lo debe activar pero NO en la carga de definiciones de arriba. Lo mismo sucede con $blnWebMode ya que debe estar activado (es lo mismo que
//se hace en indexDesign para que funcione el preview)
$blnWebMode = true;
$appVersion = ESURVEY_SERVICE_VERSION;
//@JAPR

//Tipos de proceso
define('evntNone', 0);
define('evntAdd', 1);		//Crear un nuevo objeto del tipo especificado
define('evntDelete', 2);	//Eliminar el objeto indicado
define('evntRefresh', 3);	//Refresh de la ventana de Preview (sólo en vista de diseño de forma)
//@JAPR 2015-06-18: Agregada la edición de datos desde la ventana de rediseño
define('evntEdit', 4);		//Editar las propiedades del objeto del tipo especificado
//@JAPR 2015-06-22: Agregado el reordenamiento de objetos
define('evntReorder', 5);	//Cambia la posición del objeto del tipo especificado
//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
define('evntInvite', 6);	//Cambia la posición del objeto del tipo especificado
//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
define('evntIntegrity', 7);		//Realiza un proceso de revisión de integridad de la forma, incluyendo la revisión de la integridad del cubo global de surveys (este último una sóla vez por día)
//GCRUZ 2015-08-19. Permisos de Formas
define('evntFormPermission', 8);
//@JAPR 2017-02-09: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
define('evntBuildForm', 9);		//Realiza el proceso de actualización de la forma de producción a partir de la definición actual de la forma de desarrollo
//@JAPR 2019-08-08: Agregada la posibilidad de almacenar colores personalizados en algunos componentes de diseño del usuario (#BT0NMN)
define('evntSaveCustomColors', 12);		//Realiza el grabado de los colores personalizados del usuario para el Editor de texto enriquecido del diseño (eventualmente se podrían compartir con los del redactor)

//Tipos de request del attachURL o invocación al save de una forma DHTMLX (usado en ProcessRequest.php)
//define('reqIFrame', 0);		//Se hizo una petición vía attachURL(url, false/null, data) o bien mediante un POST/URL normal de una forma HTML, se espera una 
							//respuesta que ejecute código o bien que devuelva en formato texto el resultado del proceso
//define('reqAjax', 1);		//Se hizo una petición vía attachURL(url, true, data) o bien mediante un objForm.send(url, verb, callback) de DHTMLX, se espera una
							//respuesta en formato JSON sobre el resultado del proceso

$strResponseFormat = strtolower(getParamValue('responseformat', 'both', '(string)'));
$intRequestType = getParamValue('RequestType', 'both', '(int)');
if ($intRequestType < 0 || $intRequestType > reqAjax) {
	$intRequestType = reqAjax;
}

$intSelSurveyID = getParamValue('SelSurveyID', 'both', '(int)');
//@JAPR 2015-06-12: Agregados los parámetros iSelType e iSelId para especificar que objeto deberá quedar dibujado al terminar de hacer el refresh
//@JAPR 2015-07-30: Validado que al actualizar la vista se regrese a donde se había quedado, ya sea una pregunta o una sección (#9AUDGB)
$intSelectionType = getParamValue('selItemType', 'both', '(int)');
$intSelectionID = getParamValue('selItemID', 'both', '(int)');
$intSelSectionID = getParamValue('selSectionID', 'both', '(int)');
//@JAPR 2015-10-25: Agregado el proceso de confirmación de borrado cuando existen referencias
$intSkipValidations = getParamValue('SkipValidation', 'both', '(int)');
//@JAPR
$intObjectType = getParamValue('ObjectType', 'both', '(int)');
//$intObjectID = getParamValue('ObjectID', 'both', '(int)');
$intObjectID = @$_REQUEST['ObjectID'];
if (is_null($intObjectID) || !is_array($intObjectID)) {
	$intObjectID = (int) $intObjectID;
}
$intNewObjectType = 0;
$intNewObjectID = 0;
$intEventType = evntNone;
$theHTTPRequest = BITAMHTTPRequest::NewHTTPRequest();
$aHTTPRequest = $theHTTPRequest;
$strProcess = getParamValue('Process', 'both', '(string)', true);
$strObjectType = "";
$strRequestError = "";
//@JAPR 2015-10-22: Agregada la validación de uso de objetos en propiedades que se pueden limpiar o que cuya referencia puede quedarse sin afectar al producto
$strRequestWarning = "";
$blnRefreshDefinitions = false;
if ($strProcess) {
	switch ($strProcess) {
		case 'Add':
			$intEventType = evntAdd;
			break;
		case 'Refresh':
			$intEventType = evntRefresh;
			break;
		case 'Delete':
			$intEventType = evntDelete;
			break;
		case 'Edit':
			$intEventType = evntEdit;
			break;
		case 'Reorder':
			$intEventType = evntReorder;
			break;
		case 'Invite':
			$intEventType = evntInvite;
			break;
		case 'FormPermission':
			$intEventType = evntFormPermission;
			break;
		//@JAPR 2017-02-09: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		case 'Build':
			$intEventType = evntBuildForm;
			break;
		//@JAPR 2019-08-08: Agregada la posibilidad de almacenar colores personalizados en algunos componentes de diseño del usuario (#BT0NMN)
		case 'SaveCustomColors':
			$intEventType = evntSaveCustomColors;
			break;
		//@JAPR
	}
}
/*
echo(nl2br("\r\n intObjectType: $intObjectType, intSelSurveyID == $intSelSurveyID"));
echo(nl2br("\r\n POST:"));
PrintMultiArray($_POST);
echo(nl2br("\r\n GET:"));
PrintMultiArray($_GET);
die();
*/
/*
header("Content-Type: text/javascript; charset=UTF-8");
echo(json_encode($_POST));
die();
*/

//$theUser;
//$theRepository;
$aRepository = $theRepository;
$aUser  = $theUser;

$objNewObject = null;
//********************************************************************************************************************************************************
//Procesos vía Ajax
//********************************************************************************************************************************************************
//@JAPR 2015-07-03: Modificado para procesar ambos tipos de request igual, pero regresar una salida diferente según el tipo de request
//if ($intRequestType == reqAjax) {
	//Cuando el request se hace con Ajax, se debe bloquear toda salida de error para permitir regresar JSON bien estructurado
	if (!$strResponseFormat) {
		$strResponseFormat = 'javascript';
	}
	ob_start();
	switch ($intEventType) {
		/* Proceso de creación de un nuevo objeto */
		case evntAdd:
		case evntEdit:
			switch ($intObjectType) {
				case otySurvey:
					require_once('surveyExt.inc.php');
					$intNewObjectType = otySurvey;
					$strObjectType = "Survey";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMSurveyExt::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = $theHTTPRequest->RedirectTo;
					
					//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
					//Cuando se graba la forma se realizará la revisión de integridad. Sólo la ventana de diseño pasa por este punto, así que los grabados de forma debido a cambios de 
					//versión u otros procesos no realizarán esta revisión. Adicionalmente, la función está condicionada a realizar el proceso sólo una vez por día
					if (!is_null($objNewObject)) {
						$objNewObject->checkIntegrity();
					}
					//@JAPR
					break;
					
				case otySection:
					require_once('sectionExt.inc.php');
					$intNewObjectType = otySection;
					$strObjectType = "Section";
					//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
					//En modo diseño el separador no es \r\n sino sólo \n
					if (isset($theHTTPRequest->POST["StrSelectOptions"])) {
						//OMMC 2015-11-09: Se eliminan los registros vacíos.
						$theHTTPRequest->POST["StrSelectOptions"] = rtrim($theHTTPRequest->POST["StrSelectOptions"]);
						$strExplode = explode("\n", $theHTTPRequest->POST["StrSelectOptions"]);
						if(count($strExplode)>1){
							$c = count($strExplode);
							for($i=0; $i<$c; $i++){
								if($strExplode[$i] == ""){
									unset($strExplode[$i]);
								}
							}
							$strExplode = implode("\r\n", $strExplode);
							$theHTTPRequest->POST["StrSelectOptions"] = $strExplode;
						}else{
							$theHTTPRequest->POST["StrSelectOptions"] = str_ireplace("\n", "\r\n", $theHTTPRequest->POST["StrSelectOptions"]);
						}
						//OMMC
					}
					//@JAPR
					
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMSectionExt::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = $theHTTPRequest->RedirectTo;
					break;
				
				case otyQuestion:
					require_once('questionExt.inc.php');
					$intNewObjectType = otyQuestion;
					$strObjectType = "Question";
					$intSectionID = getParamValue('SectionID', 'both', '(int)');
					$_GET["SectionID"] = $intSectionID;
					$theHTTPRequest->GET["SectionID"] = $intSectionID;
					//En modo diseño el separador no es \r\n sino sólo \n
					if (isset($theHTTPRequest->POST["StrSelectOptions"])) {
						$theHTTPRequest->POST["StrSelectOptions"] = str_ireplace("\n", "\r\n", $theHTTPRequest->POST["StrSelectOptions"]);
					}

					//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
					//Si la pregunta se está creando como tipo Metro, se debe agregar el valor de DataSourceMemberID a CatMemberList para que se grabe como debe ser en la lista de atributos
					if (isset($theHTTPRequest->POST["QTypeID"]) && $theHTTPRequest->POST["QTypeID"] == qtpSingle && 
							isset($theHTTPRequest->POST["QDisplayMode"]) && $theHTTPRequest->POST["QDisplayMode"] == dspMetro &&
							isset($theHTTPRequest->POST["DataSourceMemberID"])) {
						$theHTTPRequest->POST["CatMembersList"] = array($theHTTPRequest->POST["DataSourceMemberID"]);
					}
					//@JAPR
					
					//Si la pregunta es de catálogo e incluía el valor de los atributos usados, debe sobreescribir el ID de atributo con el último utilizado en la lista recibida
					if (isset($theHTTPRequest->POST["CatMembersList"])) {
						$arrCatMembersList = $theHTTPRequest->POST["CatMembersList"];
						if (!is_null($arrCatMembersList) && is_array($arrCatMembersList)) {
							$intCatMemberID = 0;
							if (count($arrCatMembersList) > 0) {
								$intCatMemberID = (int) @$arrCatMembersList[count($arrCatMembersList) -1];
								//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
								$theHTTPRequest->POST["DataSourceMemberID"] = $intCatMemberID;
								//$theHTTPRequest->POST["CatMemberID"] = $intCatMemberID;
								//@JAPR
							}
						}
					}
					
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMQuestionExt::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = $theHTTPRequest->RedirectTo;
					
					//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
					//Si el objeto contiene una propiedad de error de Tablas, continuará con el proceso pero reportará el error en pantalla
					if (!is_null($objNewObject)) {
						if (property_exists($objNewObject, 'TableErrors')) {
							$strErrorMsg = $objNewObject->TableErrors;
							if ($strErrorMsg) {
								$strRequestError = str_ireplace("{var1}", translate($strObjectType), translate("There was an error creating the {var1}"))." ('{$objNewObject->QuestionText}'). ".translate("Please contact your System Administrator");
								//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
								$strLogPath = GeteFormsLogPath();
								$strLogFilename = $strLogPath."questionCreationErrors.log";
								@error_log(date('Y-m-d H:i:s')."- ".((string) @$_SESSION["PABITAM_RepositoryName"])."\r\n", 3, $strLogFilename);
								@error_log($strRequestError.": {$strErrorMsg}\r\n", 3, $strLogFilename);
								//@JAPR
								
								//Si está creando la pregunta, debe eliminarla para que no quede como basura
								if ($intEventType == evntAdd) {
									$objNewObject->remove();
								}
								$objNewObject = null;
							}
						}
					}
					//@JAPR
					
					//Si se logró crear la pregunta, debe cargar la instancia ya que al devolver la definición se requieren toda la información completa que sólo dicho
					//proceso carga
					if (!is_null($objNewObject)) {
						//Si la pregunta es tipo llamada, al recibir el Número de teléfono, debe sobreescribir la única opción de respuesta que tiene, o bien grabar una en este momento
						if (isset($theHTTPRequest->POST["TelephoneNum"]) && $objNewObject->QTypeID == qtpCallList) {
							$strTelephoneNum = (string) @$theHTTPRequest->POST["TelephoneNum"];
							require_once('questionOptionExt.inc.php');
							$objQuestionOptionColl = BITAMQuestionOptionCollection::NewInstance($aRepository, $objNewObject->QuestionID);
							if (!is_null($objQuestionOptionColl)) {
								$objNewQuestionOption = null;
								foreach ($objQuestionOptionColl->Collection as $objQuestionOption) {
									//Sólo sobreescribe la primer opción de respuesta, se asume que de todas maneras sólo existía esta, si no hubiera entonces tiene que crear una
									$objNewQuestionOption = $objQuestionOption;
									break;
								}
								
								//@JAPR 2015-09-28: Corregido un bug, validado que no se pueda grabar una opción vacía (#FZ43UJ)
								if (is_null($objNewQuestionOption) && trim($strTelephoneNum) != '') {
									//@JAPR 2015-09-28: Corregido un bug, había faltado la referencia al repositorio (#FZ43UJ)
									$objNewQuestionOption = BITAMQuestionOption::NewInstance($aRepository, $objNewObject->QuestionID);
									//@JAPR
								}
								if (!is_null($objNewQuestionOption)) {
									$objNewQuestionOption->QuestionOptionName = $strTelephoneNum;
									$objNewQuestionOption->phoneNum = $strTelephoneNum;
									$objNewQuestionOption->SortOrder = 0;
									$objNewQuestionOption->save();
								}
							}
						}
					
						$objQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $objNewObject->QuestionID);
						$objNewObject = $objQuestion;
						
						//Si la pregunta es tipo llamada y se está creando, la única opción de respuesta que debe tener ya debe asociar automáticamente el número telefónico
						if (!is_null($objQuestion) && $objQuestion->QTypeID == qtpCallList) {
							$objQuestionOptionsColl = BITAMQuestionOptionCollection::NewInstance($aRepository, $objQuestion->QuestionID);
							if (!is_null($objQuestionOptionsColl)) {
								foreach ($objQuestionOptionsColl as $objQuestionOption) {
									//@JAPR 2015-09-28: Corregido un bug, validado que no se pueda grabar una opción vacía (#FZ43UJ)
									//Se removió la validación para teléfonos totalmente numéricos, ahora será durante la configuración donde se valide
									if ($objQuestionOption->QuestionOptionName && (int) $objQuestionOption->QuestionOptionName != (int) $objQuestionOption->phoneNum) {
										$objQuestionOption->phoneNum = $objQuestionOption->QuestionOptionName;
										$objQuestionOption->save();
									}
								}
							}
						}
						
						//Si la pregunta es de catálogo e incluía el valor de los atributos usados, hace el grabado en forma independiente, ya que la clase BITAMQuestion no lo realiza
						if (isset($theHTTPRequest->POST["CatMembersList"])) {
							$arrCatMembersList = $theHTTPRequest->POST["CatMembersList"];
							if (!is_null($arrCatMembersList) && is_array($arrCatMembersList)) {
								$objNewObject->CatMembersList = $arrCatMembersList;
								$objNewObject->saveCatMembersIDs();
							}
						}

						//OMMC 2015-11-30: Agregado para las preguntas OCR
						//Si la pregunta es OCR e incluye el valor de el arreglo de preguntas, se graban de manera independiente en un método llamado saveOCRQuestionIDs()
						if(isset($theHTTPRequest->POST["QuestionList"])){
							$arrQuestionList = $theHTTPRequest->POST["QuestionList"];
							if(!is_null($arrQuestionList) && is_array($arrQuestionList)){
								$objNewObject->QuestionList = $arrQuestionList;
								$objNewObject->saveOCRQuestionIDs();
							}
						}
					}
					break;
					
				case otyOption:
					require_once('questionOptionExt.inc.php');
					$intNewObjectType = otyOption;
					$strObjectType = "Option";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMQuestionOptionExt::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					
					//Si la opción de respuesta incluía el valor de los ShowQuestions, hace el grabado en forma independiente, ya que la clase BITAMQuestionOption no lo realiza
					if (isset($theHTTPRequest->POST["ShowQuestionList"])) {
						$arrShowQuestions = $theHTTPRequest->POST["ShowQuestionList"];
						$intQuestionID = getParamValue('QuestionID', 'both', '(int)');
						$intQuestionOptionID = getParamValue('QuestionOptionID', 'both', '(int)');
						if (!is_null($arrShowQuestions) && is_array($arrShowQuestions) && $intQuestionID && $intQuestionOptionID) {
							$sql = "DELETE FROM SI_SV_ShowQuestions WHERE QuestionID = ".$intQuestionID." AND ConsecutiveID = ".$intQuestionOptionID;
							$aRepository->DataADOConnection->Execute($sql);
							
							foreach ($arrShowQuestions as $intShowQuestionID) {
								if ($intShowQuestionID > 0) {
									$objShowQuestion = BITAMShowQuestion::NewInstance($aRepository, $intQuestionOptionID);
									if (!is_null($objShowQuestion)) {
										$objShowQuestion->ShowQuestionID = $intShowQuestionID;
										$objShowQuestion->save();
									}
								}
							}
						}
					}
					
					$objNewObject = $theHTTPRequest->RedirectTo;
					if (!is_null($objNewObject)) {
						$objQuestionOption = BITAMQuestionOption::NewInstanceWithID($aRepository, $objNewObject->QuestionOptionID);
						$objNewObject = $objQuestionOption;
					}
					break;
					
				//@JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
				case otySkipRule:
					require_once('skipRule.inc.php');
					$intNewObjectType = otySkipRule;
					$strObjectType = "SkipRule";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMSkipRule::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					
					$objNewObject = $theHTTPRequest->RedirectTo;
					if (!is_null($objNewObject)) {
						$objSkipRule = BITAMSkipRule::NewInstanceWithID($aRepository, $objNewObject->SkipRuleID);
						$objNewObject = $objSkipRule;
					}
					break;
				//@JAPR
					
				case otySetting:
					require_once('settingsvariableext.inc.php');
					$intNewObjectType = otySetting;
					$strObjectType = "Settings";
					$theHTTPRequest->POST["SettingID"] = -1;
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMSettingsVariableExt::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = $theHTTPRequest->RedirectTo;
					break;
				//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
				/*case otyAppCustomization:
					require_once('eformsappcustomizationext.inc.php');
					require_once('eformsappcustomizationtplext.inc.php');
					$intNewObjectType = otyAppCustomization;
					$strObjectType = "Template";
					$theHTTPRequest->POST["SettingID"] = -1;
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					if($intEventType==evntAdd)
					{
						BITAMEFormsAppCustomizationTplExt::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					}
					else
					{
						BITAMEFormsAppCustomizationExt::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					}
					$objNewObject = $theHTTPRequest->RedirectTo;
					break;
				*/
				//@JAPR 2016-02-23: Agregados los templates de estilos personalizados de v6
				case otyAppCustStyles:
					$_GET['DebugBreak'] = 1;
					require_once("eformsAppCustomStyles.inc.php");
					require_once("eformsAppCustomStylesTplExt.inc.php");
					$intNewObjectType = otyAppCustStyles;
					$strObjectType = "Template";
					$theHTTPRequest->POST["SettingID"] = -1;
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					if ($intEventType == evntAdd) {
						BITAMEFormsAppCustomStylesTplExt::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					}
					else {
						BITAMEFormsAppCustomStyles::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					}
					$objNewObject = $theHTTPRequest->RedirectTo;
					break;
				//@JAPR
				case otyLink:
					require_once('link.inc.php');
					$intNewObjectType = otyLink;
					$strObjectType = "Link";
					$theHTTPRequest->POST["SettingID"] = -1;
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMLink::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = $theHTTPRequest->RedirectTo;
					break;
				case otyUser: //@AAL Agregado para crear o editar un usuario
					require_once('appUserExt.inc.php');
					$intNewObjectType = otyUser;
					$strObjectType = "User";
					//AAL 21/07/2015 Si llego el Email, quiere decir que se actualizara ya sea el FirstName, LastName o Password
					$Email = getParamValue('Email', 'both', '(string)', true);
					if($intEventType == evntEdit && $Email){
						//Si llegó el FirstName/LastName/Password este se actualizará
						$Field = ""; $ValueField = "";
						$FirstName = getParamValue('FirstName', 'both', '(string)', true);
						$LastName = getParamValue('LastName', 'both', '(string)', true);
						$Password = getParamValue('Password', 'both', '(string)', true);
						if($FirstName){
							$Field = "first_name";
							$ValueField = $FirstName;
						}
						if($LastName){
							$Field = "last_name";
							$ValueField = $LastName;
						}
						if($Password){
							$Field = "pwd";
							$ValueField = $Password;
						}

						//sbrande_aalvarez_16jul2015: obtener FirstName y LastName de la saas_users
					    include("../../../fbm/conns.inc.php");
					    $mysql_Connection = mysql_connect($server, $server_user, $server_pwd);
					    if ($mysql_Connection) 
					    {
					    	$db_selected = mysql_select_db($masterdbname, $mysql_Connection);
					      	if ($db_selected) 
					      	{
					        	require_once("../../../fbm/ESurvey.class.php");
					        	// instantiate the host user
					        	$aUser = SAASUser::getSAASUserWithEmail($Email, $mysql_Connection);
					        	if (!is_null($aUser))
					        	{
					        		// save the new data
					         		$anArray[$Field] = $ValueField;
					         		$aUser->updateFromArray($anArray);
					         		$aUser->saveToDB($mysql_Connection);
					         		if($Field == "pwd"){
					         			$sql = "UPDATE SI_USUARIO SET PASSWORD = " . $aRepository->DataADOConnection->Quote(BITAMEncryptPassword($ValueField)) . " WHERE CUENTA_CORREO = " . $aRepository->DataADOConnection->Quote($Email);
										$aRepository->DataADOConnection->Execute($sql);	
					         		}
					        	}
					      	}
					    }
					}
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMAppUserExt::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = $theHTTPRequest->RedirectTo;
					break;
				case otyUserGroup: //@AAL 17/07/2015 Agregado para crear o editar Grupos de usuario
					require_once('usergroupExt.inc.php');
					$intNewObjectType = otyUserGroup;
					$strObjectType = "Usergroup";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMUserGroupExt::PerformHTTPRequest($aRepository, $theHTTPRequest);
					$objNewObject = $theHTTPRequest->RedirectTo;
					break;
				case otyAttribute:
					require_once('dataSourceFilter.inc.php');
					$intNewObjectType = otyAttribute;
					$strObjectType = "Attribute";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMDataSourceFilter::PerformHTTPRequest($aRepository, $theHTTPRequest);
					//GCRUZ 2016-07-19. Filtro por fórmula. Después de guardar, retornar el filtro por IDs a Nombres, para mostrar...
					if (getMDVersion() >= esvDataSourceFilterFormula) {
						if ($theHTTPRequest->RedirectTo->FilterFormula != '')
							$theHTTPRequest->RedirectTo->FilterFormula = BITAMDataSource::ReplaceMemberIDsByNames($theHTTPRequest->RedirectTo->Repository, $theHTTPRequest->RedirectTo->CatalogID, $theHTTPRequest->RedirectTo->FilterFormula);
					}
					$objNewObject = $theHTTPRequest->RedirectTo;
					//@JAPR 2015-09-03: Corregido un bug, no estaba actualizando la versión de los catálogos que dependen de este DataSource modificado (#K8ZJZJ)
					$intDataSourceID = getParamValue('CatalogID', 'both', '(int)');
					if ($intDataSourceID > 0) {
						BITAMDataSource::updateLastUserAndDateInDataSource($aRepository, $intDataSourceID);
					}
					//@JAPR
					break;
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				case otyDataSource:
					require_once('dataSource.inc.php');
					$intNewObjectType = otyDataSource;
					$strObjectType = "DataSource";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMDataSource::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = $theHTTPRequest->RedirectTo;
					break; 
				//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
				case otyDataSourceMember:
					require_once('dataSource.inc.php');
					require_once('dataSourceMember.inc.php');
					$intNewObjectType = otyDataSourceMember;
					$strObjectType = "DataSourceMember";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMDataSourceMember::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = $theHTTPRequest->RedirectTo;
					break;
				//GCRUZ 2015-08-25. Agregado para crear menus
				case otyMenu:
					require_once('surveyMenuExt.inc.php');
					$intNewObjectType = otyMenu;
					$strObjectType = "Menu";
					//Obtener las surveyIDs y meterlas al HTTPRequest
					if (isset($theHTTPRequest->POST['FormsAssct']) && $theHTTPRequest->POST['FormsAssct'] != '')
					{
						$theHTTPRequest->POST['SurveyIDs'] = array();
						if ($theHTTPRequest->POST['MenuID'] == -1)
						{
							$theHTTPRequest->POST['FormsAssct'] = explode(',', $theHTTPRequest->POST['FormsAssct']);
							foreach ($theHTTPRequest->POST['FormsAssct'] as $aSurveyIDPOST)
								//@JAPR 2016-07-27: Corregido un bug, estaba grabando un elemento -1 cuando desde una clist no se seleccionaba valor
								if ((int) $aSurveyIDPOST > 0) {
									$theHTTPRequest->POST['SurveyIDs'][] = $aSurveyIDPOST;
								}
								//@JAPR
						}
						else
						{
							foreach ($theHTTPRequest->POST['FormsAssct'] as $aSurveyIDPOST => $aSurveyPOST) {
								//@JAPR 2016-07-27: Corregido un bug, estaba grabando un elemento -1 cuando desde una clist no se seleccionaba valor
								if ((int) $aSurveyIDPOST > 0) {
									$theHTTPRequest->POST['SurveyIDs'][] = $aSurveyIDPOST;
								}
								//@JAPR
							}
						}
					}
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMSurveyMenuExt::PerformHTTPRequest($aRepository, $theHTTPRequest, null);
					$objNewObject = $theHTTPRequest->RedirectTo;
					break;
				//GCRUZ 2016-05-10. Agregado grabado de mensajes push
				case otyPushMsg:
					require_once('pushMsg.inc.php');
					$strObjectType = "Message";
					$intDestinationType = intval($theHTTPRequest->POST['DestinationType']);
					
					if (getMDVersion() >= esvRealPushMsg) {					
						//@ears 2016-11-09 nueva funcionalidad en donde para 1 o mas usuarios o grupos de usuarios
						//solo se genera un registro de "encabezado de notificación", el detallado se vacía en otras tablas
											
						$aUserCollection = BITAMAppUserCollection::NewInstance($aRepository);
						$aGroupCollection = new BITAMUserGroupCollection($aRepository, null);
						
						//foreach ($arrDestinations as $aDestination)
						//{
						$newPushMsg = BITAMPushMsg::NewInstance($aRepository);
						$newPushMsg->DestinationType = $intDestinationType;
						//$newPushMsg->DestinationID = intval($aDestination);
						$newPushMsg->DestinationID = 0;
						$newPushMsg->UserCollection = $aUserCollection->Collection;
						$newPushMsg->GroupCollection = $aGroupCollection->Collection;
						$newPushMsg->getDestinationName($aRepository);
						$appUser = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $aUser->UserID);
						$newPushMsg->CreationUserID = $appUser->UserID;
						$newPushMsg->CreationDateID = date('Y-m-d H:i:s');
						$newPushMsg->Title = $theHTTPRequest->POST['Title'];
						$newPushMsg->PushMessage = $theHTTPRequest->POST['Message'];
						$newPushMsg->State = 0;
						$newPushMsg->arrDestinationsD = array();
						
						switch($intDestinationType)
						{
							case pushMsgUser:
								$newPushMsg->arrDestinationsD =  explode(',', $theHTTPRequest->POST['DestinationUser']);
								break;
							case pushMsgGroup:
								$newPushMsg->arrDestinationsD =  explode(',', $theHTTPRequest->POST['DestinationGroup']);
								break;
						}															

						$newPushMsg->save();
					}
					else {
						$arrDestinations = array(); 
						switch($intDestinationType)
						{
							case pushMsgUser:
								$arrDestinations = explode(',', $theHTTPRequest->POST['DestinationUser']);
								break;
							case pushMsgGroup:
								$arrDestinations = explode(',', $theHTTPRequest->POST['DestinationGroup']);
								break;
						}
						$aUserCollection = BITAMAppUserCollection::NewInstance($aRepository);
						$aGroupCollection = new BITAMUserGroupCollection($aRepository, null);
						foreach ($arrDestinations as $aDestination)
						{
							$newPushMsg = BITAMPushMsg::NewInstance($aRepository);
							$newPushMsg->DestinationType = $intDestinationType;
							$newPushMsg->DestinationID = intval($aDestination);
							$newPushMsg->UserCollection = $aUserCollection->Collection;
							$newPushMsg->GroupCollection = $aGroupCollection->Collection;
							$newPushMsg->getDestinationName($aRepository);
							$appUser = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $aUser->UserID);
							$newPushMsg->CreationUserID = $appUser->UserID;
							$newPushMsg->CreationDateID = date('Y-m-d H:i:s');
							$newPushMsg->Title = $theHTTPRequest->POST['Title'];
							$newPushMsg->PushMessage = $theHTTPRequest->POST['Message'];
							$newPushMsg->State = 0;

							$newPushMsg->save();
						}						
					}
					
					//}
					//GCRUZ 2016-05-13. No hay necesidad de redireccionar en push.
					die('OK');
					break;
					$objNewObject = $theHTTPRequest->RedirectTo;
			}
			break;
		
		//@JAPR 2015-07-03: Agregada la eliminación de objetos
		case evntDelete:
			switch ($intObjectType) {
				case otySection:
					//La sección se elimina desde la ventana de diseño, así que carga sólo la instancia indicada, no hay manera de eliminar mas de una a la vez
					require_once("section.inc.php");
					$strObjectType = "Section";
					$objNewObject = BITAMSection::NewInstanceWithID($aRepository, $intObjectID);
					//@JAPR 2015-07-28: Agregada la validación de borrado (#DVGQ8G)
					if (!is_null($objNewObject)) {
						//En este caso se verifica si la sección es utilizada en alguna otra configuración (regresa el detalle completo)
						//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
						$strVarsUsed = '';
						if (!$intSkipValidations) {
							$strVarsUsed = SearchVariablePatternInMetadata($aRepository, $objNewObject->SurveyID, $intObjectID, $intObjectType);
						}
						if (trim($strVarsUsed) != '') {
							$strRequestError = translate("This section cannot be removed because the following configurations contain references to it").": \n".str_replace("\r", "", $strVarsUsed);
							$objNewObject = null;
						}
						else {
							//En este caso se verifica si alguna de las preguntas de esta sección es utilizada en alguna otra configuración (sólo regresa los
							//nombres de las preguntas que se utilizan en otras configuraciones)
							//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
							if (!$intSkipValidations) {
								$strVarsUsed = $objNewObject->getQuestionsWithVarReferences();
							}
							//@JAPR 2015-10-25: Agregada la validación de uso de objetos en propiedades que se pueden limpiar o que cuya referencia puede quedarse sin afectar al producto
							//Ahora el array se regresará como un texto directamente con los casos encontrados
							//if (!is_null($arrUsedQuestions) && is_array($arrUsedQuestions)) {
							if (trim($strVarsUsed) != '') {
								//if (count($arrUsedQuestions) > 0) {
								$strRequestError = translate("This section cannot be removed because the following questions contains references in other configurations").": \n".str_replace("\r", "", $strVarsUsed);
								$objNewObject = null;
								//}
							}
							else {
								//@JAPR 2015-10-22: Agregada la validación de uso de objetos en propiedades que se pueden limpiar o que cuya referencia puede quedarse sin afectar al producto
								//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
								if (!$intSkipValidations) {
									$strVarsUsed = SearchObjectReferenceInMetadata($aRepository, $objNewObject->SurveyID, $intObjectID, $intObjectType);
								}
								if (trim($strVarsUsed) != '') {
									$strRequestWarning = translate("This section cannot be removed because the following configurations contain references to it").". ".translate("Do you wish to delete the items anyway?").": \n".str_replace("\r", "", $strVarsUsed);
									$objNewObject = null;
								}
							}
						}
					}
					//@JAPR
					
					if (!is_null($objNewObject)) {
						if (!$intSelSurveyID) {
							$intSelSurveyID = $objNewObject->SurveyID;
						}
						$blnRefreshDefinitions = true;
						
						$objNewObject->remove();
					}
					break;
					
				case otyQuestion:
					//La pregunta se elimina desde la ventana de diseño, así que carga sólo la instancia indicada, no hay manera de eliminar mas de una a la vez
					require_once("question.inc.php");
					$strObjectType = "Question";
					$objNewObject = BITAMQuestion::NewInstanceWithID($aRepository, $intObjectID);
					//@JAPR 2015-07-28: Agregada la validación de borrado (#DVGQ8G)
					if (!is_null($objNewObject)) {
						//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
						$strVarsUsed = '';
						if (!$intSkipValidations) {
							$strVarsUsed = SearchVariablePatternInMetadata($aRepository, $objNewObject->SurveyID, $intObjectID, $intObjectType);
						}
						if (trim($strVarsUsed) != '') {
							$strRequestError = translate("This question cannot be removed because the following configurations contain references to it").": \n".str_replace("\r", "", $strVarsUsed);
							$objNewObject = null;
						}
						else {
							//@JAPR 2015-10-22: Agregada la validación de uso de objetos en propiedades que se pueden limpiar o que cuya referencia puede quedarse sin afectar al producto
							//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
							if (!$intSkipValidations) {
								$strVarsUsed = SearchObjectReferenceInMetadata($aRepository, $objNewObject->SurveyID, $intObjectID, $intObjectType);
							}
							if (trim($strVarsUsed) != '') {
								$strRequestWarning = translate("This question cannot be removed because the following configurations contain references to it").". ".translate("Do you wish to delete the items anyway?").": \n".str_replace("\r", "", $strVarsUsed);
								$objNewObject = null;
							}
						}
					}
					//@JAPR
					
					if (!is_null($objNewObject)) {
						if (!$intSelSurveyID) {
							$intSelSurveyID = $objNewObject->SurveyID;
						}
						$blnRefreshDefinitions = true;
						$objNewObject->remove();
						
						//Sobreescribe el parámetro para redireccionar a la sección de esta pregunta
						$_GET["selItemType"] = otySection;
						$_GET["selItemID"] = $objNewObject->SectionID;
					}
					break;
					
				case otySurvey:
					//La forma se elimina desde la lista global, así que se pueden eliminar varias a la vez, por lo tanto deja que la procese el request de object.inc
					require_once('surveyExt.inc.php');
					$intNewObjectType = otySurvey;
					$strObjectType = "Survey";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					//@JAPR 2016-01-08: Validado que antes de eliminar una forma se verifique si está o no siendo usada por una Agenda/Scheduler de Agenda, para que
					//en ese caso no se permita eliminarla (#AU6V5R)
					$blnValidDelete = true;
					$arrSurveyIDs = @getParamValue('SurveyID', 'both', '(array)', true);
					$strAndError = '';
					$strAndWarning = '';
					if (!is_null($arrSurveyIDs) && is_array($arrSurveyIDs) && count($arrSurveyIDs) > 0) {
						foreach ($arrSurveyIDs as $intSurveyID) {
							if ($intSurveyID > 0) {
								$objSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $intSurveyID);
								if (!is_null($objSurvey)) {
									$strSurveyName = translate("Survey").": ".$objSurvey->SurveyName;
									//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
									$strVarsUsed = '';
									if (!$intSkipValidations) {
										$strVarsUsed = SearchFormReferenceInMetadata($aRepository, $intSurveyID);
									}
									if (trim($strVarsUsed) != '') {
										$strRequestError .= $strAndError."\n".$strSurveyName."\n".str_replace("\r", "", $strVarsUsed);
										$strAndError = "\n";
										$objSurvey = null;
										$blnValidDelete = false;
									}
								}
							}
						}						
					}
					
					if ($blnValidDelete) {
						BITAMSurveyExt::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
						$objNewObject = BITAMSurveyExt::NewInstance($aRepository);
					}
					else {
						if ($strRequestError != '') {
							$strRequestError = translate("This form cannot be removed because the following configurations contain references to it").": \n".$strRequestError;
						}
						if ($strRequestWarning != '') {
							$strRequestWarning = translate("This form cannot be removed because the following configurations contain references to it").". ".translate("Do you wish to delete the items anyway?").": \n".$strRequestWarning;
						}
					}
					
					//@JAPR
					break;
				
				case otyOption:
					//La opción de respuesta se elimina desde la ventana de diseño, así que carga sólo la instancia indicada, no hay manera de eliminar mas de una a la vez, adicionalmente
					//en este caso no se necesita que force a un refresh de la ventana, así que debería haberse recibido el parámetro de request como Ajax para indicar que no se desea
					//regresar código javascript/html que ejecute algún proceso en el cliente
					require_once("questionoption.inc.php");
					$strObjectType = "Value";
					$objNewObject = BITAMQuestionOption::NewInstanceWithID($aRepository, $intObjectID);
					if (!is_null($objNewObject)) {
						$objNewObject->remove();
					}
					break;
					
				//@JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
				case otySkipRule:
					//La configuración de saltos se elimina desde la ventana de diseño, así que carga sólo la instancia indicada, no hay manera de eliminar mas de una a la vez, adicionalmente
					//en este caso no se necesita que force a un refresh de la ventana, así que debería haberse recibido el parámetro de request como Ajax para indicar que no se desea
					//regresar código javascript/html que ejecute algún proceso en el cliente
					require_once("skipRule.inc.php");
					$strObjectType = "Skip rule";
					$objNewObject = BITAMSkipRule::NewInstanceWithID($aRepository, $intObjectID);
					if (!is_null($objNewObject)) {
						$objNewObject->remove();
					}
					break;
				//@JAPR
					
				case otyDataSource:
					/*2015-07-19@JRPP Se agrega para poder eliminar un objeto de tipo Datasource*/
					require_once('dataSource.inc.php');
					$intNewObjectType = otyDataSource;
					$strObjectType = "DataSource";
					
					//@JAPR 2015-10-21: Agregada la validación de eliminación de mas tipos de objetos
					$blnValidDelete = true;
					$arrDataSourceIDs = getParamValue('DataSourceID', 'both', '(array)');
					$strAndError = '';
					$strAndWarning = '';
					foreach ($arrDataSourceIDs as $intDataSourceID) {
						if ($intDataSourceID > 0) {
							$objDataSource = BITAMDataSource::NewInstanceWithID($aRepository, $intDataSourceID);
							if (!is_null($objDataSource)) {
								$strCatalogName = translate("Catalog").": ".$objDataSource->DataSourceName;
								//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
								$strVarsUsed = '';
								if (!$intSkipValidations) {
									$strVarsUsed = SearchObjectReferenceInMetadata($aRepository, -1, $intDataSourceID, otyDataSource);
								}
								if (trim($strVarsUsed) != '') {
									$strRequestWarning .= $strAndWarning."\n".$strCatalogName."\n".str_replace("\r", "", $strVarsUsed);
									$strAndWarning = "\n";
									$objDataSource = null;
									$blnValidDelete = false;
								}
								
								//Verifica si alguno de los atributos es usado directamente por preguntas, ya que esos casos no deben permitir eliminar tampoco al DataSource
								//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
								if (!$intSkipValidations) {
									$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $intDataSourceID);
									if (!is_null($objDataSourceMembersColl)) {
										$strAnd = '';
										foreach ($objDataSourceMembersColl as $objDataSourceMember) {
											$intDataSourceMemberID = $objDataSourceMember->MemberID;
											$strVarsUsed = SearchVariablePatternInMetadata($aRepository, -1, $intDataSourceMemberID, otyDataSourceMember);
											if (trim($strVarsUsed) != '') {
												$strRequestError .= $strAndError."\n".$strCatalogName." - ".translate("Attribute").": ".$objDataSourceMember->MemberName."\n".str_replace("\r", "", $strVarsUsed);
												$strAndError = "\n";
												$blnValidDelete = false;
											}
											
											//Verifica si existen configuraciones directas que apunten a este atributo como un ID
											$strVarsUsed = SearchObjectReferenceInMetadata($aRepository, -1, $intDataSourceMemberID, otyDataSourceMember);
											if (trim($strVarsUsed) != '') {
												$strRequestWarning .= $strAndWarning."\n".$strCatalogName." - ".translate("Attribute").": ".$objDataSourceMember->MemberName."\n".str_replace("\r", "", $strVarsUsed);
												$strAndWarning = "\n";
												$blnValidDelete = false;
											}
										}
									}
								}
								//@JAPR
							}
						}
					}
					
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					if ($blnValidDelete) {
						BITAMDataSource::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
						$objNewObject = BITAMDataSource::NewInstance($aRepository);
					}
					else {
						if ($strRequestError != '') {
							$strRequestError = translate("This catalog cannot be removed because the following configurations contain references to it").": \n".$strRequestError;
						}
						if ($strRequestWarning != '') {
							$strRequestWarning = translate("This catalog cannot be removed because the following configurations contain references to it").". ".translate("Do you wish to delete the items anyway?").": \n".$strRequestWarning;
						}
					}
					//@JAPR
					break;
				case otyDataSourceMember:
					//Un atributo se elimina desde la lista de valores del DataSource, desde donde puede venir uno sólo o una colección
					require_once('dataSource.inc.php');
					require_once('dataSourceMember.inc.php');
					$intNewObjectType = otyDataSourceMember;
					$strObjectType = "DataSourceMember";
					if (is_array($intObjectID)) {
						$theHTTPRequest->POST['MemberID'] = $intObjectID;
						
						//@JAPR 2015-10-21: Agregada la validación de eliminación de mas tipos de objetos
						$blnValidDelete = true;
						$strAndError = '';
						$strAndWarning = '';
						$intDataSourceID = getParamValue('DataSourceID', 'both', '(int)');
						$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $intDataSourceID);
						if (!is_null($objDataSourceMembersColl)) {
							$strAnd = '';
							foreach ($objDataSourceMembersColl as $objDataSourceMember) {
								$intDataSourceMemberID = $objDataSourceMember->MemberID;
								if (!in_array($intDataSourceMemberID, $intObjectID)) {
									continue;
								}
								
								//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
								$strVarsUsed = '';
								if (!$intSkipValidations) {
									$strVarsUsed = SearchVariablePatternInMetadata($aRepository, -1, $intDataSourceMemberID, otyDataSourceMember);
								}
								if (trim($strVarsUsed) != '') {
									$strRequestError .= $strAndError."\n".translate("Attribute").": ".$objDataSourceMember->MemberName."\n".str_replace("\r", "", $strVarsUsed);
									$strAndError = "\n";
									$blnValidDelete = false;
								}
								else {
									//Verifica si existen configuraciones directas que apunten a este atributo como un ID
									//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
									if (!$intSkipValidations) {
										$strVarsUsed = SearchObjectReferenceInMetadata($aRepository, -1, $intDataSourceMemberID, otyDataSourceMember);
									}
									if (trim($strVarsUsed) != '') {
										$strRequestWarning .= $strAndWarning."\n".translate("Attribute").": ".$objDataSourceMember->MemberName."\n".str_replace("\r", "", $strVarsUsed);
										$strAndWarning = "\n";
										$blnValidDelete = false;
									}
								}
							}
						}
						//@JAPR
						
						if ($blnValidDelete) {
							BITAMDataSourceMember::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
							//@JAPR 2015-08-29: Corregido un bug, faltaba el segundo parámetro
							$objNewObject = BITAMDataSource::NewInstanceWithID($aRepository, $intDataSourceID);
						}
						else {
							if ($strRequestError != '') {
								$strRequestError = translate("This attribute cannot be removed because the following configurations contain references to it").": \n".$strRequestError;
							}
							if ($strRequestWarning != '') {
								$strRequestWarning = translate("This attribute cannot be removed because the following configurations contain references to it").". ".translate("Do you wish to delete the items anyway?").": \n".$strRequestWarning;
							}
						}
						//@JAPR
					}
					else {
						$objNewObject = BITAMDataSourceMember::NewInstanceWithID($aRepository, $intObjectID);
						//@JAPR 2015-10-21: Agregada la validación de eliminación de mas tipos de objetos
						if (!is_null($objNewObject)) {
							//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
							$strVarsUsed = '';
							if (!$intSkipValidations) {
								$strVarsUsed = SearchVariablePatternInMetadata($aRepository, -1, $intObjectID, otyDataSourceMember);
							}
							if (trim($strVarsUsed) != '') {
								$strRequestError = translate("This attribute cannot be removed because the following configurations contain references to it").": \n".str_replace("\r", "", $strVarsUsed);
								$objNewObject = null;
							}
							else {
								//@JAPR 2015-10-22: Agregada la validación de uso de objetos en propiedades que se pueden limpiar o que cuya referencia puede quedarse sin afectar al producto
								//@JAPR 2015-10-25: Agregado el parámetro para evitar validar si el objeto es o no utilizado antes de prevenir su borrado
								if (!$intSkipValidations) {
									$strVarsUsed = SearchObjectReferenceInMetadata($aRepository, -1, $intObjectID, otyDataSourceMember);
								}
								if (trim($strVarsUsed) != '') {
									$strRequestWarning = translate("This attribute cannot be removed because the following configurations contain references to it").": \n".str_replace("\r", "", $strVarsUsed);
									$objNewObject = null;
								}
							}
						}
						//@JAPR
						
						if (!is_null($objNewObject)) {
							$objNewObject->remove();
						}
					}
					
					//@JAPR 2015-09-03: Corregido un bug, no estaba actualizando la versión de los catálogos que dependen de este DataSource modificado (#K8ZJZJ)
					if ($intDataSourceID > 0 && ($blnValidDelete || !is_null($objNewObject))) {
						BITAMDataSource::updateLastUserAndDateInDataSource($aRepository, $intDataSourceID);
					}
					//@JAPR
					break;
				case otyUserGroup: //@AAL 17/07/2015 Agregado para crear o editar Grupos de usuario
					require_once('usergroupExt.inc.php');
					$intNewObjectType = otyUserGroup;
					$strObjectType = "Usergroup";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMUserGroupExt::PerformHTTPRequest($aRepository, $theHTTPRequest);
					$objNewObject = BITAMUserGroupExt::NewUserGroup($aRepository);
				break;
				//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
				/*case otyAppCustomization:
					require_once('eformsappcustomizationext.inc.php');
					require_once('eformsappcustomizationtplext.inc.php');
					$intNewObjectType = otyAppCustomization;
					$strObjectType = "Template";
					$theHTTPRequest->POST["SettingID"] = -1;					
					BITAMEFormsAppCustomizationTplExt::PerformHTTPRequest($aRepository, $theHTTPRequest);
					$objNewObject = BITAMEFormsAppCustomizationTplExt::NewInstance($aRepository);
				break;
				*/
				//@JAPR 2016-02-23: Agregados los templates de estilos personalizados de v6
				case otyAppCustStyles:
					require_once("eformsAppCustomStyles.inc.php");
					require_once("eformsAppCustomStylesTplExt.inc.php");
					$intNewObjectType = otyAppCustStyles;
					$strObjectType = "Template";
					$theHTTPRequest->POST["SettingID"] = -1;					
					BITAMEFormsAppCustomStylesTplExt::PerformHTTPRequest($aRepository, $theHTTPRequest);
					$objNewObject = BITAMEFormsAppCustomStylesTplExt::NewInstance($aRepository);
				break;
				//@JAPR
				case otyLink:
					require_once('link.inc.php');
					$intNewObjectType = otyLink;
					$strObjectType = "Link";
					$theHTTPRequest->POST["SettingID"] = -1;
					BITAMLink::PerformHTTPRequest($aRepository, $theHTTPRequest);
					$objNewObject = BITAMLink::NewInstance($aRepository);
				break;
				case otyDataDestinations:
					require_once('eFormsDataDestinations.inc.php');
					$intNewObjectType = otyDataDestinations;
					$strObjectType = "DataDestinations";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMeFormsDataDestinations::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = BITAMeFormsDataDestinations::NewInstance($aRepository);
				break;
				case otyArtusModels:
					require_once('eFormsModels.inc.php');
					$intNewObjectType = otyArtusModels;
					$strObjectType = "eFormsModels";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMeFormsModels::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = BITAMeFormsModels::NewInstance($aRepository);
					break;
				case otyAgendasCheckpoint:
					require_once('agendas.inc.php');
					$intNewObjectType = otyAgendasCheckpoint;
					$strObjectType = "Checkpoint";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMCheckpoint::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = BITAMCheckpoint::NewInstance($aRepository);
				break;
				case otyAgendasScheduler:
					require_once('surveyAgendaScheduler.inc.php');
					$intNewObjectType = otyAgendasScheduler;
					$strObjectType = "AgendaScheduler";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMSurveyAgendaScheduler::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = BITAMSurveyAgendaScheduler::NewInstance($aRepository);
				break;
				case otyAgendasMonitor:
					require_once('surveyAgenda.inc.php');
					$intNewObjectType = otyAgendasMonitor;
					$strObjectType = "SurveyAgenda";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMSurveyAgenda::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = BITAMSurveyAgenda::NewInstance($aRepository);
				break;
				
				case otyReport:
					//El reporte se puede eliminar desde uno directamente cargado o bien desde el Log de capturas, en cualquier caso la forma de grabar datos es muy diferente
					//y ahora se puede solicitar eliminar reportes desde diferentes formas simultáneamente, así que no se puede delegar al método de borrado original por lo que se
					//realizará un ciclo en este punto dependiendo de que parámetros llegaron
					require_once("report.inc.php");
					//@JAPR 2016-08-24: Agregado el borrado de datos de los modelos de Artus generados para formas por parte del usuario
					require_once("artusmodelsurvey.class.php");
					//@JAPR
					
					$intNewObjectType = otyReport;
					$strObjectType = "Report";
					
					//@JAPR 2016-08-24: Agregado el borrado de datos de los modelos de Artus generados para formas por parte del usuario
					//@JAPR 2016-08-24: Implementada la eliminación múltiple de reportes
					//Cuando se manda a eliminar reportes desde la lista, no se tiene el ID de reporte numérico en ese punto debido a que se genera la lista desde la tabla de Tareas/Log,
					//por lo tanto se recibirá un array con los RowID del grid de dicha lista, que son una combinación de Forma + Usuario + Fecha-Hora, con lo cual se obtendrá la referencia
					//del reporte y se llenarán los arrays que estaba esperando en los parámetros SurveyID y ObjectID
					//El formato recibido es: "1_680_2016-08-24 10:18:34_8"
					//Respectivamente: "ConsecutivoDelGrid_SurveyID_Fecha Hora_UserID"
					$arrReportRowID = @$_REQUEST['ReportRowID'];
					if (!is_null($arrReportRowID)) {
						$intObjectID = array();
						
						//Si se recibe el parámetro ReportRowID, se asume que esta lista de llaves contiene las referencias necesarias para calcular el ReportID y ya incluye SurveyID,
						//así que sobreescribirá los parámetros ObjectID y SurveyID (este caso se utiliza desde la lista de capturas generada a partir de SI_SV_SurveyLog, dode no se tiene
						//el ID de la captura)
						if (!is_array($arrReportRowID)) {
							$arrReportRowID = array($arrReportRowID);
						}
						
						$arrSurveyIDs = array();
						foreach ($arrReportRowID as $strReportRowID) {
							$arrReportKeys = explode("_", $strReportRowID);
							$intSurveyID = (int) @$arrReportKeys[1];
							$strDateID = (string) @$arrReportKeys[2];
							$intUserID = (int) @$arrReportKeys[3];
							if ($intSurveyID > 0) {
								$intReportID = BITAMSurvey::GetEntryIDFromDateAndUser($aRepository, $intSurveyID, $intUserID, $strDateID);
								if ($intReportID > 0) {
									$arrSurveyIDs[] = $intSurveyID;
									$intObjectID[] = $intReportID;
								}
							}
						}
						
						//Sobreescribe el parámetro SurveyID con el array de IDs de formas identificado correspondiente a los IDs de capturas de $intObjectID
						$intSurveyID = $arrSurveyIDs;
					}
					else {
						//En este caso continua procesando el request con los parámetros SurveyID y ObjectID
						$intSurveyID = @$_REQUEST['SurveyID'];
					}
					//@JAPR
					
					if (is_null($intSurveyID) || !is_array($intSurveyID)) {
						$intSurveyID = (int) $intSurveyID;
					}
					
					if (is_array($intObjectID) && is_array($intSurveyID)) {
						//Se eliminarán una serie de reportes cuyo índice entre ambos arrays debe coincidir en forma numérica ascendente
						$arrSurveyIDs = $intSurveyID;
						$arrObjectIDs = $intObjectID;
					}
					else {
						//Se eliminará un único reporte
						$arrSurveyIDs = array($intSurveyID);
						$arrObjectIDs = array($intObjectID);
					}
					
					//@JAPR 2016-08-24: Agregado el borrado de datos de los modelos de Artus generados para formas por parte del usuario
					//Adicionalmente borra el modelo de Agendas, pero no del modelo global Surveys, ya que ese modelo se elimina con el proceso mas abajo captura por captura)
					//El proceso de borrado de datos de modelos debe mandar todos los IDs de las capturas por cada una de las formas, así que primero se agrupan de esa manera
					$arrReportsBySurvey = array();
					foreach ($arrObjectIDs as $intIndex => $intReportID) {
						$intSurveyID = (int) @$arrSurveyIDs[$intIndex];
						if ($intSurveyID <= 0) {
							continue;
						}
						
						if (!isset($arrReportsBySurvey[$intSurveyID])) {
							$arrReportsBySurvey[$intSurveyID] = array();
						}
						
						$arrReportsBySurvey[$intSurveyID][] = $intReportID;
					}
					
					//Se invocará primero el borrado de los datos de los modelos, asumiendo que el proceso no causará ningún tipo de error que deje inconsistente la información (aunque
					//si se diera el caso, debería bastar con invocar de nuevo al proceso de llenado de los modelos, ya que la información de eForms no se ha eliminado en este punto)
					$arrErrDesc = array();
					$arrSurveysWithErrors = array();
					foreach ($arrReportsBySurvey as $intSurveyID => $arrReportIDs) {
						$strErrDesc = '';
						if (!ARTUSModelFromSurvey::deleteDataModelFromSurvey($aRepository, $intSurveyID, $arrReportIDs, $strErrDesc)) {
							$arrSurveysWithErrors[$intSurveyID] = $intSurveyID;
							$arrErrDesc[$intSurveyID] = $strErrDesc;
						}
					}
					
					//Si ocurriera algún error durante el borrado de los datos de los modelos de cierta forma, no se procesará el borrado de los datos de ella y se reportará en pantalla
					if (count($arrErrDesc) > 0) {
						$arrSurveyNames = BITAMSurvey::getSurveys($aRepository);
						$strAndError = "";
						foreach ($arrErrDesc as $intSurveyID => $strErrDesc) {
							$strRequestError .= $strAndError.translate("Survey").": ".((string) @$arrSurveyNames[$intSurveyID]).". ".str_replace("\r", "", $strErrDesc);
							$strAndError = "\n";
						}
					}
					//@JAPR
					
					//Recorre todos los reportes especificados para eliminar cada uno
					foreach ($arrObjectIDs as $intIndex => $intReportID) {
						$intSurveyID = (int) @$arrSurveyIDs[$intIndex];
						//@JAPR 2016-08-24: Agregado el borrado de datos de los modelos de Artus generados para formas por parte del usuario
						//En caso de que esta forma esté marcada como con error durante el borrado de los datos del modelo, no se ejecutará el borrado de los datos de la captura para
						//permitir reprocesar el modelo si fuera necesario, sólo se borrarán datos de formas que no tuvieran problemas al borrar en los modelos
						if ($intReportID <= 0 || $intSurveyID <= 0 || isset($arrSurveysWithErrors[$intSurveyID])) {
							continue;
						}
						//@JAPR
						
						BITAMReport::RemoveV6($aRepository, $intSurveyID, $intReportID);
					}
					
					//Por ahora no hay reporte de error al eliminar capturas, así que se genera una instancia vacia de Report para que el proceso se considere terminado correctamente
					//@JAPR 2016-08-24: Agregado el borrado de datos de los modelos de Artus generados para formas por parte del usuario
					//En caso que ocurriera algún error en el borrado de los datos del modelo, se mostrará el error en pantalla así que no se asigna el objeto $objNewObject
					if (trim($strRequestError) == '') {
						$objNewObject = BITAMReport::NewInstanceEmpty($aRepository);
					}
					//@JAPR
					break;
				//GCRUZ 2015-08-25. Agregado para eliminar menus
				case otyMenu:
					require_once('surveyMenuExt.inc.php');
					$intNewObjectType = otyMenu;
					$strObjectType = "otyMenu";
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMSurveyMenuExt::PerformHTTPRequest($aRepository, $theHTTPRequest, null);
					$objNewObject = BITAMSurveyMenuExt::NewInstance($aRepository);
					break;
			}
			break;
		
		case evntReorder:
			switch ($intObjectType) {
				case otySection:
					require_once('sectionExt.inc.php');
					$intNewObjectType = otySection;
					$strObjectType = "Section";
					$intSectionID = getParamValue('SectionID', 'both', '(int)');
					
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMSectionExtCollection::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = BITAMSection::NewInstanceWithID($aRepository, $intSectionID);
					break;
				
				case otyQuestion:
					require_once('questionExt.inc.php');
					$intNewObjectType = otyQuestion;
					$strObjectType = "Question";
					$intSectionID = getParamValue('SectionID', 'both', '(int)');
					$intQuestionID = getParamValue('QuestionID', 'both', '(int)');
					$_GET["SectionID"] = $intSectionID;
					$theHTTPRequest->GET["SectionID"] = $intSectionID;
					
					//Se procesará el request con la clase correspondiente, pero no habrá redirección
					BITAMQuestionExtCollection::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
					$objNewObject = BITAMQuestion::NewInstanceWithID($aRepository, $intQuestionID);
					break;
			}
			break;
			
		//@JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
		case evntInvite:
			//Agrega/elimina usuarios/grupos del Scheduler asociado a la forma indicada, si no había uno entonces lo genera en este momento
			//Según el tipo de objeto y ya teniendo la instancia del Scheduler generada, limpia el array correspondiente y rellena la propiedad con los IDs
			//de los elementos que aún continuan seleccionados
			
			require_once("surveyScheduler.inc.php");
			//Primero verifica que la forma tenga un Scheduler asociado, si no es así se lo creación
			$intSurveyID = getParamValue('SurveyID', 'both', '(int)');
			$objSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $intSurveyID);
			if (!is_null($objSurvey)) {
				//Comprueba si hay o no un Scheduler generado, si no lo hay lo crea en este punto
				$objSurveyScheduler = null;
				$intSurveySchedulerID = $objSurvey->SchedulerID;
				if ($intSurveySchedulerID <= 0) {
					$strObjectType = "Scheduler";
					$objSurvey->save();
					$intSurveySchedulerID = $objSurvey->SchedulerID;
				}
				
				if ($intSurveySchedulerID) {
					$objSurveyScheduler = BITAMSurveyScheduler::NewInstanceWithID($aRepository, $intSurveySchedulerID);
				}
				if (!is_null($objSurveyScheduler)) {
					//Inicia la asociación de los usuarios y grupos según el dato recibido
					switch ($intObjectType) {
						case otyUser:
							$strObjectType = "Scheduler user";
							break;
						case otyUserGroup:
							$strObjectType = "Scheduler user group";
							break;
					}
					
					$strItems = getParamValue('StrUserRol', 'both', '(string)');
					$objSurveyScheduler->StrUserRol = $strItems;
					
					//Realiza el cambio en la configuración afectando sólo el tipo de objeto especificado
					$objSurveyScheduler->UpdateUserIDsFromScheduler(array($intObjectType => $intObjectType));
					$objNewObject = $objSurvey;
				}
			}
			break;
		//GCRUZ 2015-08-19. Permisos de Formas
		case evntFormPermission:
			require_once("surveyScheduler.inc.php");
			//Obtener las formas
			$strForms = getParamValue('StrForms', 'both', '(string)');
			$strOperation = getParamValue('Operation', 'both', '(string)');
			$arrForms = json_decode($strForms, true);
			foreach ($arrForms as $aForm)
			{
				//Primero verifica que la forma tenga un Scheduler asociado, si no es así se lo creación
				$intSurveyID = intval(str_replace('form', '', $aForm['id']));
				$objSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $intSurveyID);
				if (!is_null($objSurvey)) {
					//Comprueba si hay o no un Scheduler generado, si no lo hay lo crea en este punto
					$objSurveyScheduler = null;
					$intSurveySchedulerID = $objSurvey->SchedulerID;
					if ($intSurveySchedulerID <= 0) {
						$strObjectType = "Scheduler";
						$objSurvey->save();
						$intSurveySchedulerID = $objSurvey->SchedulerID;
					}
					
					if ($intSurveySchedulerID) {
						$objSurveyScheduler = BITAMSurveyScheduler::NewInstanceWithID($aRepository, $intSurveySchedulerID);
					}

					if (!is_null($objSurveyScheduler)) {
						//Inicia la asociación de los usuarios y grupos según el dato recibido
						switch ($intObjectType) {
							case otyUser:
								require_once('appUser.inc.php');
								$intObjectID = getParamValue('ClaUsuario', 'both', '(int)');
								$intUserID = getParamValue('UserID', 'both', '(int)');
								$objNewObject = BITAMAppUser::NewInstanceWithID($aRepository, $intUserID);
								break;
							case otyUserGroup:
								require_once('usergroup.inc.php');
								$intObjectID = getParamValue('UserGroupID', 'both', '(int)');
								$objNewObject = BITAMUserGroup::NewUserGroupWithID($aRepository, $intObjectID);
								break;
						}

						$res = $objSurveyScheduler->UpdateSchedulerUserRol($intObjectType, $intObjectID, $strOperation);
						if ($res[0] === false)
							break;
						
					}
				}
			}
			break;
		
		//@JAPR 2017-02-09: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		case evntBuildForm:
			// Carga la forma especificada que corresponde con una forma de desarrollo para actualizar la forma de producción correspondiente con la misma definición
			$strRequestError = BITAMSurvey::BuildSurvey($aRepository, $intSelSurveyID);
			if ($strRequestError == '') {
				$objNewObject = BITAMSurvey::NewInstance($aRepository);
			}
			break;
		
		//@JAPR 2019-08-08: Agregada la posibilidad de almacenar colores personalizados en algunos componentes de diseño del usuario (#BT0NMN)
		case evntSaveCustomColors:
			$arrCustomizedColor = @$_REQUEST['CustomizedColor'];
			if ( !is_null($arrCustomizedColor) ) {
				if ( is_array($arrCustomizedColor) ) {
					$appUser = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $aUser->UserID);
					if ( !is_null($appUser) ) {
						$appUser->saveCustomColorsArray($arrCustomizedColor);
					}
				}
			}
			
			//No hay necesidad de redireccionar
			die('OK');
			break;
		//@JAPR
	}
	
	$strBuffer = trim(ob_get_contents());
	ob_end_clean();
	$arrData = array();
	if ($strBuffer) {
		//@JAPR 2017-02-09: Agregado el parámetro para mostrar los Warnings en pantalla en lugar de eliminarlos (utilizado en conjunto con DebugBreak, ya que al entrar por processRequest.php
		//automáticamente toda la salida a pantalla de DebugBreak se pierde, pero con esta opción se podrá mostrar aunque evidentemente fallará la respuesta JSON)
		if (getParamValue('ShowWarnings', 'both', '(int)')) {
			echo($strBuffer);
		}
		else {
			$arrData['warning'] = array('desc' => $strBuffer);
		}
		//@JAPR
	}
	
	if (is_null($objNewObject)) {
		//@JAPR 2015-07-28: Agregada la validación de borrado (#DVGQ8G)
		//@JAPR 2015-07-28: Agregados los errores personalizados durante el proceso
		if ($strRequestError != '') {
			$arrData['error'] = array('desc' => $strRequestError);
		}
		else {
			//@JAPR 2015-10-22: Agregada la validación de uso de objetos en propiedades que se pueden limpiar o que cuya referencia puede quedarse sin afectar al producto
			//Cuando el código que invoque reciba un confirm, se debe reenviar al mismo request con el parámetro "IgnoreVal=1" para permitir que esta ocasión termine el proceso
			//que previamente generó la advertencia, eso en caso de que se confirme que si se quiere continuar, de lo contrario ya no se recibirá un nuevo request
			if ($strRequestWarning != '') {
				$arrData['confirm'] = array('desc' => $strRequestWarning);
			}
			else {
				$arrData['error'] = array('desc' => str_ireplace("{var1}", translate($strObjectType), translate("There was an error creating the {var1}")));
			}
		}
	}
	else {
		$arrData['url'] = "main.php?".$objNewObject->get_QueryString();
		//GCRUZ 2015-08-25. Agregado para crear menus. getJSonDefinition ocupa $theRepository como parámetro
		if ($intObjectType == otyMenu)
			$arrData['newObject'] = $objNewObject->getJSonDefinition($aRepository);
		else
		$arrData['newObject'] = $objNewObject->getJSonDefinition();
	}
	//GCRUZ 2015-08-20. Errores al actualizar scheduler
	if (isset($res[0]) && $res[0] === false)
		$arrData['error'] = array('desc' => $res[1]);
	
	//@JAPR 2015-07-03: Agregada la eliminación de objetos
	//Si se está eliminando un objeto, deja continuar el proceso para que termine de cargar la definición que finalmente quedó y actualizarla en la página de diseño, al mismo tiempo
	//que forza un refresh
	if ($intRequestType == reqAjax) {
		//@JAPR 2017-02-09: Agregado el parámetro para mostrar los Warnings en pantalla en lugar de eliminarlos (utilizado en conjunto con DebugBreak, ya que al entrar por processRequest.php
		//automáticamente toda la salida a pantalla de DebugBreak se pierde, pero con esta opción se podrá mostrar aunque evidentemente fallará la respuesta JSON)
		if (!getParamValue('ShowWarnings', 'both', '(int)')) {
			//Regresa la salida en formato JSON
			if( $strResponseFormat == 'json' )
			{
				header("Content-Type:application/json; charset=UTF-8");
			}
			else
			{
				header("Content-Type: text/javascript; charset=UTF-8");	
			}
		}
		
		//@JAPR 2015-07-17: Agregado el borrado de objetos directamente desde Ajax, los cuales no regresan código desde el server sino sólo una referencia
		//Si el request es tipo Ajax y se pidió eliminar un objeto, al objeto regresado se le anexa el parámetro que permitirá indicar que ejecute el callback de borrado en la ventana
		//de diseño
		if ($intEventType == evntDelete && !is_null($objNewObject)) {
			if ($blnRefreshDefinitions) {
				$strJSONDefinitions = '';
				$strJSONDefinitionsVersions = '';
				//@JAPR 2016-07-06: Rediseñado el Admin con DXHTML
				//Agregado el parámetro $aParamsColl para indicar cuales de los valores que se hubieran tomado del request serán tomados de manera directa de dicho array (#XWOMIF)
				$arrJSON = @getEFormsDefinitionsVersions($aRepository, array('Survey' => $intSelSurveyID, 'Agenda' => -1));
				//@JAPR
				if (is_null($arrJSON) || !is_array($arrJSON)) {
					$arrJSON = array('data' => array());
				}
				else {
					$arrJSON = array('data' => $arrJSON);
				}
				$strJSONDefinitionsVersions = $arrJSON;
				$arrData['definitionVersions'] = $strJSONDefinitionsVersions;
				
				//@JAPR 2019-05-06: Modificado para mostrar valores dummy en los atributos en lugar de realizar la consulta real a los catálogos (#270TYE)
				//Esta invocación no se podría optimizar si se quieren ver los valores en el preview, ya que los objetos generados con ella se usarán para actualizar tanto el preview donde si se
				//requieren los valores, como el diseño donde no se requieren, así que no se puede remover la carga de valores como se hizo en el request similar dentro del diseño de la forma
				$arrJSON = @getEFormsDefinitions($aRepository, array('Survey' => $intSelSurveyID, 'Agenda' => -1));
				//@JAPR
				if (is_null($arrJSON) || !is_array($arrJSON)) {
					$arrJSON = array('data' => array());
				}
				else {
					$arrJSON = array('data' => $arrJSON);
				}
				//@JAPR 2018-10-29: Corregido un bug, algunos caracteres fuera del rango válido de utf8 provocaban que el response fallara, por lo tanto se removerán cambiándolos por "?" (#KGW0CU)
				$arrJSON = FixNonUTF8CharsInArray($arrJSON);
				//@JAPR
				$strJSONDefinitions = $arrJSON;
				$arrData['definitions'] = $strJSONDefinitions;
			}
			
			$arrData['objectType'] = $intObjectType;
			$arrData['deleted'] = 1;
		}
		//@JAPR
		
		echo(json_encode($arrData));
		die();
	}
	
	//@JAPR
//}

//El request es tipo reqIFrame, así que espera regresar HTML con un código a ejecutar según lo que se pidió que se hiciera
header("Content-Type:text/html; charset=UTF-8");

/* Proceso de edición de un objeto (sólo detalles) o borrado de uno o mas objetos del mismo tipo
*/
if ($intEventType == evntAdd * -1) {
	switch ($intObjectType) {
		case otySurvey:
			break;
	
		case otySection:
			//Creación o borrado de una sección
			if (array_key_exists("SectionID", $aHTTPRequest->POST)) {
				require_once('section.inc.php');
				$aSectionID = $aHTTPRequest->POST["SectionID"];
				$intNewObjectType = otySection;
				$intNewObjectID = $aSectionID;
				$intEventType = evntAdd;
				if (is_array($aSectionID))
				{
					$intEventType = evntDelete;
				}
				
				//Se procesará el request con la clase correspondiente, pero no habrá redirección
				BITAMSection::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
			}
			break;
	
		case otyQuestion:
			//Creación o borrado de una pregunta
			if (array_key_exists("QuestionID", $aHTTPRequest->POST)) {
				require_once("question.inc.php");
				$aQuestionID = $aHTTPRequest->POST["QuestionID"];
				$intNewObjectType = otyQuestion;
				$intNewObjectID = $aQuestionID;
				$intEventType = evntAdd;
				if (is_array($aQuestionID))
				{
					$intEventType = evntDelete;
				}
				
				//Se procesará el request con la clase correspondiente, pero no habrá redirección
				BITAMQuestion::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
			}
			break;
		
		case otyOption:
			//Creación o borrado de una opción de respuesta
			if (array_key_exists("QuestionOptionID", $aHTTPRequest->POST)) {
				require_once("questionoption.inc.php");
				$aQuestionOptionID = $aHTTPRequest->POST["QuestionOptionID"];
				$intNewObjectType = otyOption;
				$intNewObjectID = $aQuestionOptionID;
				$intEventType = evntAdd;
				if (is_array($aQuestionOptionID))
				{
					$intEventType = evntDelete;
				}
				
				//Se procesará el request con la clase correspondiente, pero no habrá redirección
				BITAMQuestionOption::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
			}
			break;
			
		case otySurveyFilter:
			//Creación o borrado de un filtro de forma
			if (array_key_exists("FilterID", $aHTTPRequest->POST)) {
				require_once("surveyfilter.inc.php");
				$aFilterID = $aHTTPRequest->POST["FilterID"];
				$intNewObjectType = otySurveyFilter;
				$intNewObjectID = $aFilterID;
				$intEventType = evntAdd;
				if (is_array($aFilterID))
				{
					$intEventType = evntDelete;
				}
				
				//Se procesará el request con la clase correspondiente, pero no habrá redirección
				BITAMSurveyFilter::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
			}
			break;
			
		case otyQuestionFilter:
			//Creación o borrado de un filtro de pregunta
			if (array_key_exists("FilterID", $aHTTPRequest->POST)) {
				require_once("questionFilter.inc.php");
				$aFilterID = $aHTTPRequest->POST["FilterID"];
				$intNewObjectType = otyQuestionFilter;
				$intNewObjectID = $aFilterID;
				$intEventType = evntAdd;
				if (is_array($aFilterID))
				{
					$intEventType = evntDelete;
				}
				
				//Se procesará el request con la clase correspondiente, pero no habrá redirección
				BITAMQuestionFilter::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
			}
			break;
			
		case otyShowQuestion:
			//Creación o borrado de un Show question
			if (array_key_exists("ShowID", $aHTTPRequest->POST)) {
				require_once("showquestion.inc.php");
				$aShowID = $aHTTPRequest->POST["ShowID"];
				$intNewObjectType = otyShowQuestion;
				$intNewObjectID = $aShowID;
				$intEventType = evntAdd;
				if (is_array($aShowID))
				{
					$intEventType = evntDelete;
				}
				
				//Se procesará el request con la clase correspondiente, pero no habrá redirección
				BITAMShowQuestion::PerformHTTPRequest($aRepository, $theHTTPRequest, $aUser);
			}
			break;
	}
}

switch ($intEventType * -1) {
	case evntDelete:
		$arrObjsToRemove = @$_POST['objsToDelete'];
		if (is_string($arrObjsToRemove)) {
			$arrObjsToRemove = @json_decode($arrObjsToRemove);
			if (is_null($arrObjsToRemove)) {
				$arrObjsToRemove = array();
			}
		}
		
		//El proceso de Borrado recibe un parámetro con la colección de todos los elementos a eliminar, así que debe clasificarlos por tipo e invocar al proceso
		//que elimina cada uno a partir del Framework original, que es donde está programado todo lo relacionado con el borrado
		//Al finalizar este proceso, hará un refresh tanto del árbol como del previo
		if (!$arrObjsToRemove || (!is_array($arrObjsToRemove) && !is_object($arrObjsToRemove))) {
			$arrObjsToRemove = array();
		}
		
		//Prepara los Arrays indexados por objetos con todos los hijos del mismo nivel que deben ser eliminados, basado en el funcionamiento de PerformHTTPRequest
		$arrSections = array();
		$arrQuestions = array();
		$arrQuestionOptions = array();
		$arrSurveyFilters = array();
		$arrQuestionFilters = array();
		$arrShowQuestions = array();
		foreach ($arrObjsToRemove as $strObjToDelete) {
			if (is_object($strObjToDelete)) {
				$intObjectType = (int) @$strObjToDelete->type;
				$intObjectID = @$strObjToDelete->id;
				$intParentType = (int) @$strObjToDelete->parentType;
				$intParentID = @$strObjToDelete->parentID;
				$intQuestionID = (int) @$strObjToDelete->questionID;
			}
			else {
				$intObjectType = (int) @$strObjToDelete['type'];
				$intObjectID = @$strObjToDelete['id'];
				$intParentType = (int) @$strObjToDelete['parentType'];
				$intParentID = @$strObjToDelete['parentID'];
				$intQuestionID = (int) @$strObjToDelete['questionID'];
			}
			if ($intObjectType != otyShowQuestion) {
				$intParentID = (int) $intParentID;
			}
			if ($intObjectType != otyOption) {
				$intObjectID = (int) $intObjectID;
			}
			
			if (!$intObjectType || !$intObjectID) {
				continue;
			}
			
			switch ($intObjectType) {
				case otySection:
					//Las secciones se eliminan directamente desde la Forma
					//$aCollection = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID, $aSectionID);
					$arrSections[$intObjectID] = $intObjectID;
					break;
				
				case otyQuestion:
					//Las preguntas se eliminan desde las secciones a las que pertenecen, pero se pueden eliminar directas
					//$aCollection = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID, $aQuestionID);
					$arrQuestions[$intObjectID] = $intObjectID;
					break;
				
				case otyOption:
					//Las opciones de respuesta se eliminan desde la pregunta/sección a la que pertenecen por ID numérico, pero se cambiará a usar el OptionName
					//$aCollection = BITAMQuestionOptionCollection::NewInstance($aRepository, $aQuestionID, $aQuestionOptionID, false);
					switch ($intParentType) {
						case otyQuestion:
							if (!isset($arrQuestionOptions[$intParentID])) {
								$arrQuestionOptions[$intParentID] = array();
							}
							$arrQuestionOptions[$intParentID][$intObjectID] = $intObjectID;
							break;
					}
					break;
					
				case otySurveyFilter:
					//Los filtros de formas se eliminan directamente desde la Forma
					//$aCollection = BITAMSurveyFilterCollection::NewInstance($aRepository, $aSurveyID, $aFilterID);
					$arrSurveyFilters[$intObjectID] = $intObjectID;
					break;
					
				case otyQuestionFilter:
					//Los filtros de pregunta se eliminan desde las preguntas a las que pertenecen
					//$aCollection = BITAMQuestionFilterCollection::NewInstance($aRepository, $aQuestionID, $aFilterID);
					if (!isset($arrQuestionFilters[$intParentID])) {
						$arrQuestionFilters[$intParentID] = array();
					}
					$arrQuestionFilters[$intParentID][$intObjectID] = $intObjectID;
					break;
				
				case otyShowQuestion:
					//Los show questions se eliminan desde las preguntas a las que pertenecen
					//$aCollection = BITAMShowQuestionCollection::NewInstance($aRepository, $aConsecutiveID, $aShowID);
					if (!isset($arrShowQuestions[$intQuestionID])) {
						$arrShowQuestions[$intQuestionID] = array();
					}
					if (!isset($arrShowQuestions[$intQuestionID][$intParentID])) {
						$arrShowQuestions[$intQuestionID][$intParentID] = array();
					}
					$arrShowQuestions[$intQuestionID][$intParentID][$intObjectID] = $intObjectID;
					break;
					
				default:
					//Tipo de objeto no soportado. La forma no se puede eliminar desde este proceso
					break;
			}
		}
		
		//Elimina los objetos configurados según lo que se hubiera leído del parámetro, por la manera en que se asignaron los objetos, no existiría posibilidad
		//de que un objeto que es padre de uno de nivel inferior que fue marcado para eliminación, se hubiera eliminado previo a borrar a su descendiente, ya que
		//en ese caso simplemente el descendiente nunca se debió haber recibido, por tanto procesará por jerarquía desde la mayor hacia abajo
		//Secciones
		if (count($arrSections)) {
		}
		
		//Preguntas
		if (count($arrQuestions)) {
			require_once("question.inc.php");
			$arrObjects = array_keys($arrQuestions);
			$aCollection = BITAMQuestionCollection::NewInstance($aRepository, $intSelSurveyID, $arrObjects);
			if ($aCollection) {
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
			}
		}
		
		//Opciones de respuesta de las preguntas
		if (count($arrQuestionOptions)) {
			require_once("question.inc.php");
			require_once("questionoption.inc.php");
			require_once("questionoptioncol.inc.php");
			foreach ($arrQuestionOptions as $intParentID => $arrChildren) {
				$aCollection = BITAMQuestionOptionCollection::NewInstance($aRepository, $intParentID, null, false);
				if ($aCollection) {
					foreach ($aCollection as $anInstanceToRemove)
					{
						//Las opciones de respuesta se deben buscar por el nombre de la opción
						if (isset($arrChildren[$anInstanceToRemove->QuestionOptionName])) {
							$anInstanceToRemove->remove();
						}
					}
				}
			}
		}
		
		//Filtros de formas
		if (count($arrSurveyFilters)) {
			require_once("surveyfilter.inc.php");
			$arrObjects = array_keys($arrSurveyFilters);
			$aCollection = BITAMSurveyFilterCollection::NewInstance($aRepository, $intSelSurveyID, $arrObjects);
			if ($aCollection) {
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
			}
		}
		
		//Filtros de preguntas
		if (count($arrQuestionFilters)) {
			require_once("question.inc.php");
			require_once("questionFilter.inc.php");
			foreach ($arrQuestionFilters as $intParentID => $arrChildren) {
				$arrObjects = array_keys($arrChildren);
				$aCollection = BITAMQuestionFilterCollection::NewInstance($aRepository, $intParentID, $arrObjects);
				if ($aCollection) {
					foreach ($aCollection as $anInstanceToRemove)
					{
						$anInstanceToRemove->remove();
					}
				}
			}
		}
		
		//Show questions
		if (count($arrShowQuestions)) {
			require_once("question.inc.php");
			require_once("questionoption.inc.php");
			require_once("questionoptioncol.inc.php");
			require_once("showquestion.inc.php");
			foreach ($arrShowQuestions as $intParentID => $arrOptions) {
				$aCollection = BITAMQuestionOptionCollection::NewInstance($aRepository, $intParentID, null, false);
				if ($aCollection) {
					foreach ($aCollection as $anOption)
					{
						//Las opciones de respuesta se deben buscar por el nombre de la opción
						if (isset($arrOptions[$anOption->QuestionOptionName])) {
							$arrObjects = array_keys($arrOptions[$anOption->QuestionOptionName]);
							$arrChildren = BITAMShowQuestionCollection::NewInstance($aRepository, $anOption->QuestionOptionID, $arrObjects);
							foreach ($arrChildren as $anInstanceToRemove)
							{
								$anInstanceToRemove->remove();
							}
						}
					}
				}
			}
		}
		break;
}

//@JAPR 2019-05-06: Modificado para mostrar valores dummy en los atributos en lugar de realizar la consulta real a los catálogos (#270TYE)
//Movido este código que originalmente estaba debajo de la carga de definiciones y versiones de definición, ya que si pretende matar el request porque no se necesita regresar nada mas, 
//no era necesario realizar la carga de tales objetos lo cual consumirá mas tiempo
//Si no se reconoce el proceso entonces termina sin regresar script
if (!$intEventType) {
	die("");
}
//@JAPR

/*	Al terminar de procesar el request, se verifica el proceso que tendrá que ejecutarse
	En los casos de creación de nuevos objetos, el evento sucede en el LayOut de propiedades de la ventana de Admin DHTMLX, así que la redirección deberá dejar
las propiedades en dicho objeto además de agregarlo al árbol en el nivel correspondiente (se forzará a pintar de nuevo el árbol)
	En los casos de borrado de elementos, se puede borrar cualquier cantidad de elementos seleccionados, así que la ventana regresará a la forma que es el único
elemento que no es posible eliminar. En este caso con mayor razón debe reconstruir el árbol
*/
$strJSONDefinitions = '';
$strJSONDefinitionsVersions = '';
if ($intSelSurveyID) {
	//Al final debe cargar las definiciones y versiones de definiciones. Cargará sólo la definición de la Forma que se está editando incluyendo todos los catálogos
	//que estén relacionados con ella
	//@JAPR 2016-07-06: Rediseñado el Admin con DXHTML
	//Agregado el parámetro $aParamsColl para indicar cuales de los valores que se hubieran tomado del request serán tomados de manera directa de dicho array (#XWOMIF)
	$arrJSON = @getEFormsDefinitionsVersions($theRepository, array('Survey' => $intSelSurveyID, 'Agenda' => -1));
	//@JAPR
	if (is_null($arrJSON) || !is_array($arrJSON)) {
		$arrJSON = array('data' => array());
	}
	else {
		$arrJSON = array('data' => $arrJSON);
	}
	$strJSONDefinitionsVersions = json_encode($arrJSON);
	
	//@JAPR 2019-05-06: Modificado para mostrar valores dummy en los atributos en lugar de realizar la consulta real a los catálogos (#270TYE)
	//Esta invocación no se podría optimizar si se quieren ver los valores en el preview, ya que los objetos generados con ella se usarán para actualizar tanto el preview donde si se
	//requieren los valores, como el diseño donde no se requieren, así que no se puede remover la carga de valores como se hizo en el request similar dentro del diseño de la forma
	$arrJSON = @getEFormsDefinitions($theRepository, array('Survey' => $intSelSurveyID, 'Agenda' => -1));
	//@JAPR
	if (is_null($arrJSON) || !is_array($arrJSON)) {
		$arrJSON = array('data' => array());
	}
	else {
		$arrJSON = array('data' => $arrJSON);
	}
	//@JAPR 2018-10-29: Corregido un bug, algunos caracteres fuera del rango válido de utf8 provocaban que el response fallara, por lo tanto se removerán cambiándolos por "?" (#KGW0CU)
	$arrJSON = FixNonUTF8CharsInArray($arrJSON);
	//@JAPR
	$strJSONDefinitions = json_encode($arrJSON);
	
	//@JAPR 2015-07-30: Validado que al actualizar la vista se regrese a donde se había quedado, ya sea una pregunta o una sección (#9AUDGB)
	//Si se pidió un refresh y se especificó que el tipo de objeto a seleccionar es pregunta, debe cargar la referencia de la pregunta porque hay que determinar la sección a la que
	//pertenece si es que no se especificó, esto es indispensable en el cambio de sección de una pregunta, de lo contrario no se va a poder hacer la selección automática correctamente
	if ($intSelectionType == otyQuestion && $intSelectionID > 0) {
		if ($intSelSectionID <= 0) {
			$arrDefObject = @$arrJSON['data']['surveyList'][$intSelSurveyID]['questions'][$intSelectionID];
			if (!is_null($arrDefObject)) {
				$intSelSectionID = $arrDefObject['sectionID'];
			}
		}
	}
}

//@JAPR 2019-05-06: Modificado para mostrar valores dummy en los atributos en lugar de realizar la consulta real a los catálogos (#270TYE)
//Movido este código arriba de la carga de definiciones y versiones de definición, ya que si pretende matar el request porque no se necesita regresar nada mas, tampoco era necesario
//haber realizado la carga de tales objetos lo cual consumirá mas tiempo
//Si no se reconoce el proceso entonces termina sin regresar script
/*if (!$intEventType) {
	die();
}*/
//@JAPR
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
		<script>
		var dhxPreviewCell = "b";
		var dhxTreeCell = "a";
		var intEventType = <?=$intEventType?>;
		EventIDs = {
			evntNone: 0,
			evntAdd: 1,
			evntDelete: 2,
			evntRefresh: 3
		}
		
		function updateAppData() {
			console.log('updateAppData eventType == <?=$intEventType?>');
			
			try {
				if (parent && parent.objFormsLayout && parent.objFormsLayout.cells(dhxTreeCell)) {
					if (parent.objFormsDefs) {
<?
						if (trim($strJSONDefinitions) == '') {
?>
						parent.objFormsDefs = new Object();
<?
						}
						else {
?>
						parent.objFormsDefs = JSON.parse("<?=addslashes($strJSONDefinitions)?>");
						parent.selSurvey = parent.objFormsDefs.data.surveyList[<?=$intSelSurveyID?>];
						//Después de actualizar la definición de la forma, es necesario volver a extender los objetos actualizados
						if (parent.addClassPrototype) {
							parent.addClassPrototype();
						}
<?
						}
?>
					}
					
					/*
					if (parent.doDrawTree) {
						parent.doDrawTree();
					}
					*/
					if (parent.doDrawSections) {
						parent.doDrawSections();
					}

<?
	switch ($intEventType) {
		case evntDelete:
		case evntRefresh:
?>
					if (parent && parent.objFormsLayout) {
						var objFormsLayout = parent.objFormsLayout;
						
						var objiFrame = objFormsLayout.cells(parent.dhxPreviewCell).getFrame();
						if (!objiFrame.contentWindow.frmDevice || !(objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp)) {
							return;
						}
						
						var objApp = objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp;
						objApp.getNewDefinitionsVersion(JSON.parse("<?=addslashes($strJSONDefinitionsVersions)?>"));
						objApp.getNewDefinitions(JSON.parse("<?=addslashes($strJSONDefinitions)?>"));
						
						//Genera nuevamente la forma indicada en el previo
						//Sobreescribe el evento al terminar de cargar el App ya que no es necesario cambiar la ventana de propiedades por cambio de dispositivo
						parent.giSectionID = 0;
<?	
	//Esta función no se puede ejecutar en eventos del Delete porque por alguna razón que no logré identificar, cuando se sobreescribe el contenido de la función
	//desde un script cargado en un iFrame diferente accesando a este código que reside en otro iFrame, no está logrando ejecutar el código pero marca errores por 
	//objetos no encontrados, probablemente se utiliza algún worker interno o algo similar que impide acceso a dichos objetos, así que sólo se utilizará si el 
	//contexto del script que realiza la llamada se ejecuta en el mismo iFrame (el caso del evento de Refresh)
	//if ($intEventType == evntRefresh) {
		if ($intSelectionType && $intSelectionID) {
			switch ($intSelectionType) {
				case otySection:
					//Sólo selecciona la sección y despliega sus propiedades
?>
						parent.giSectionID = <?=$intSelectionID?>;
						objApp.notifySurveyShown = function() {
							console.log('notifySurveyShown');
							
							if (parent.doSelectSection) {
								parent.doSelectSection(parent.giSectionID, true);
							}
							if (parent.doSectionAction) {
								parent.doSectionAction(parent.giSectionID);
							}
							if (parent.giSectionID && parent.objSectionsDataView) {
								parent.objSectionsDataView.select(parent.giSectionID);
							}
						}
<?
					break;
					
				case otyQuestion:
					//Selecciona la sección, pero además selecciona la pregunta indicada y despliega sus propiedades, para esto realiza el mismo proceso de selección de la sección
					//pero adicionalmente marca la pregunta considerada seleccionada, así la función colocará el foco en dicha pregunta
?>
						parent.giSectionID = <?=$intSelSectionID?>;
						parent.giQuestionID = <?=$intSelectionID?>;
						objApp.notifySurveyShown = function() {
							console.log('notifySurveyShown');
							
							if (parent.doSelectSection) {
								//JAPR 2015-07-31: Agregado el parámetro bChangeQuestionFocus para indicar cuando si se quiere modificar el foco de la pregunta, ya que en los clicks a la propia pregunta
								//desde el preview no es necesario hacer esto pues provocaría un efecto extraño al mover ligeramente el top de la página justo al inicio de la pregunta incluso si según el
								//scroll actual se veía abajo, así que eso sólo se requiere cuando se está haciendo un refresh
								parent.doSelectSection(parent.giSectionID, true, true);
							}
							if (parent.doQuestionAction) {
								parent.doQuestionAction(parent.giQuestionID);
							}
							if (parent.giSectionID && parent.objSectionsDataView) {
								parent.objSectionsDataView.select(parent.giSectionID);
							}
						}
<?
					break;
			}
		}
?>
<?	//}
?>
						
						objApp.backToFormsList(true);
						if (objiFrame.contentWindow.frmDevice.hasOwnProperty('gblBuildingPage')) {
							objiFrame.contentWindow.frmDevice.gblBuildingPage = false;
						} else {
							objiFrame.contentWindow.frmDevice.contentWindow.gblBuildingPage = false;
						}
						objApp.showSurvey(parent.intSurveyID);
						
						objFormsLayout.cells(parent.dhxPreviewCell).progressOff();
						if (parent.objFormTabs) {
							parent.objFormTabs.tabs(parent.tabDesign).progressOff();
						}
					}
<?
			break;
	}
	
	switch ($intEventType) {
		case evntDelete:
?>
					//if (parent.doShowSurveyProps) {
					//	parent.doShowSurveyProps('sv<?=$intSelSurveyID?>');
					//};
<?
		break;
	}
?>					
				}
			} catch(e) {
				alert('updateAppData error: ' + e);
			}
		}
		</script>
	</head>
	<body onload="updateAppData();">
		<div data-role="page" data-url="emptyPage" id="emptyPage">
		</div>
	</body>
</html>
