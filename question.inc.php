<?php 

require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("catalog.inc.php");
require_once("dataSource.inc.php");
require_once("filtersv.inc.php");
require_once("surveyscheduler.inc.php");
require_once("questionoption.inc.php");
require_once("questionoptioncol.inc.php");
require_once('settingsvariable.inc.php');
require_once('questionFilter.inc.php');
//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
//Agregada la carga de este archivo ya que es dependiente de question, no tiene caso cargarlo manualmente en cada proceso que lo podría utilizar
require_once("skipRule.inc.php");
//@JAPR
global $generatePDFXLS;

class BITAMQuestion extends BITAMObject
{
	//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas (campo FormatMask)
	static $arrNumericFormats = array(
		0 => '',
		1 => '',
		2 => '0',
		3 => '#',
		4 => '#,##0',
		5 => '$#,##0',
		6 => '#,##0.00',
		7 => '$#,##0.00',
		8 => '# %',
		9 => '0 %',
		10 => '0.0 %'
	);
	//@JAPR 2015-07-21: Convertidas a variables estáticas para no depender de una instancia
	static $KeyField = "QuestionKey";					//SVeForms_####_QuestionDet_xxxx.QuestionKey: Key auto generado que representa una de las posibles n respuestas seleccionadas de una pregunta Múltiple choice
	static $DescFieldCatMember = "Attribute_";			//SVeForms_####_QuestionDet_xxxx.Attribute_####: Descripción del atributo de catálogo (valor extraído de un DataSource) correspondiente al ID ####, existe uno por atributo tanto usado como no usado en la pregunta (cuando no es usado, guardará el primer valor asociado según la combinación de los usados)
	static $CatStructField = "AttributeDesc";			//SVeForms_####_QuestionDet_xxxx.AttributeDesc: Un string conteniendo los pares key_####_NombreDelAtributo, separados por "_SVElem_", de todos los atributos utilizados en la pregunta al momento de la captura de una pregunta de catálogo, con el cual se podrá regenerar el componente si se decide editar eventualmente vía Web, incluso si la estructura del catálogo hubiera cambiado en la definición de la metadata
	static $CatStructKeyField = "AttributeDesc";		//SVeForms_####_QuestionDet_xxxx.AttributeDesc: Un string conteniendo los pares key_####_NombreDelAtributo, separados por "_SVElem_", de todos los atributos utilizados en la pregunta al momento de la captura de una pregunta de catálogo, con el cual se podrá regenerar el componente si se decide editar eventualmente vía Web, incluso si la estructura del catálogo hubiera cambiado en la definición de la metadata
	//@JAPR
	
	static $arrDateFormats = array(
		
		0 => 'dd/mmm/yyyy',
		1 => 'dd/yyyy/mmm',
		2 => 'mmm/dd/yyyy',
		3 => 'mmm/yyyy/dd',
		4 => 'yyyy/mmm/dd',
		5 => 'yyyy/dd/mmm',
		 
		6 => 'dd/yyyy/mm',
		7 => 'dd/mm/yyyy',
		8 => 'mm/dd/yyyy',
		9 => 'mm/yyyy/dd',
		10 => 'yyyy/mm/dd',
		11 => 'yyyy/dd/mm',
		 
		12 => 'dd/yyyy/mmmm',
		13 => 'dd/mmmm/yyyy',
		14 => 'mmmm/dd/yyyy',
		15 => 'mmmm/yyyy/dd',
		16 => 'yyyy/mmmm/dd',
		17 => 'yyyy/dd/mmmm',
		 
		18 => 'dd/yy/mmm',
		19 => 'dd/mmm/yy',
		20 => 'mmm/dd/yy',
		21 => 'mmm/yy/dd',
		22 => 'yy/mmm/dd',
		23 => 'yy/dd/mmm',
		 
		24 => 'dd/yy/mm',
		25 => 'dd/mm/yy',
		26 => 'mm/dd/yy',
		27 => 'mm/yy/dd',
		28 => 'yy/mm/dd',
		29 => 'yy/dd/mm',
		 
		30 => 'dd/yy/mmmm',
		31 => 'dd/mmmm/yy',
		32 => 'mmmm/dd/yy',
		33 => 'mmmm/yy/dd',
		34 => 'yy/mmmm/dd',
		35 => 'yy/dd/mmmm'
		
	);
	
	public $ForceNew;
	public $SurveyID;
	public $SurveyName;
	public $SectionID;
	public $OldSectionID;
	public $SectionName;
	public $QuestionID;
	public $QuestionNumber;
	public $QuestionText;
	//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
	public $QuestionTextHTML;		//Equivalente al QuestionText pero contiene tags HTML para personalizar la etiqueta
	//@JAPR
	public $DisplayQuestion;
	//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
	public $QTypeID;				//Tipo de pregunta 
		//qtpSingle=2=Selección secilla,	qtpMulti=3=Selección múltiple
		//Cualquier otra opción es pregunta abierta (qtpOpenNumeric=1=Numérica, qtpOpenDate=4=Fecha, qtpOpenString=5=Texto, qtpOpenAlpha=6=Alfanumerico)
		//Se dejó de esta forma en lugar de agrupar los 3 tipos de dato en un tipo Abierta por compatibilidad con el código, ya que para el día
		//que se pidió agregar la clasificación de tipo separada de la del tipo de dato, ya había demasiado código involucrado con menos de 1
		//día para las presentaciones del producto, así que se optó por no modificar lo que ya estaba probado
	//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
	//Ahora el tipo de llenado del componente será un campo mas en la metadata
	public $ValuesSourceType;		//Indica el tipo de fuente para llenar los valores de esta pregunta Simple o Múltiple choice
	//JAPR 2016-12-21: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
	public $ValuesSourceTypeOld;	//Valor de ValuesSourceType antes de ser modificado dinámicamente o mediante la función UpdateFromArray
	//@JAPR 2012-12-07: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
	public $EBQTypeID;				//Es un equivalente a QTypeID usado sólo en get_FormFields porque QTypeID no se puede usar directo o sobreescribiría a dicho campo si no se seleccionan opciones de eBavel
	public $GQTypeID;				//Tipo de pregunta agrupado. La única diferencia vs QTypeID es que cualquier tipo abierto se clasifica como qtpOpen=0 (NO se debe usar para grabar, es sólo para despliegue, pero debe ser consistente con QTypeID)
	public $QDataType;				//Tipo de datos de la pregunta (sólo aplica si GQTypeID == qtpOpen). Equivale al QTypeID cuando éste es abierto, dtpString cuando es de selección sencilla o múltiple (NO se debe usar para grabar, es sólo para despliegue, pero debe ser consistente con QTypeID)
		//dtpNumeric=1
		//dtpDate=4
		//dtpString=5
		//dtpAlpha=6
	public $QDisplayMode;			//Modo de despliegue de la pregunta
		//dspVertical=0 	(RadioButtons/CheckBoxes según el tipo, colocados de arriba hacia abajo - No aplica para preguntas Abiertas). Default para Sencillas y Múltiples
		//dspHorizontal=1 	(RadioButtons/CheckBoxes según el tipo, colocados de izquierda a derecha - No aplica para preguntas Abiertas)
		//dspMenu=2 		(Selector de opciones tipo "Botón" o con Spin según el tipo - No aplica para preguntas Abiertas)
		//dspMatrix=3 		(RadioButtons/CheckBoxes/TextBoxes según el tipo, colocados como cuadrícula con Renglones y Columnas)
		//dspEntry=4 		(Textbox para captura de la respuesta - Sólo aplica para preguntas Abiertas). Default para Abiertas
	//@JAPR 2012-04-30: Agregada la opción para configurar el estilo visual del componente (depende del QTypeID directamente, originalmente usado
	//sólo para las preguntas de selección sencilla)
	public $QComponentStyleID;
		//QTypeID == qtpSingle
		//	0 = Combo diseño nativo
		//	1 = Combo diseño JQuery Mobile
	//@JAPR 2011-06-29: Agregada la longitud de los campos Alfanumericos
	public $QLength;
	//@JAPR 2015-05-19: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
	//estaban limitadas a 255 caracteres (#BA7866)
	public $OldQLength;
	//@JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
	public $MaxQLength;
	//@JAPR
	//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
	public $NoFlow;
	public $OldQTypeID;
	public $QComment;
	public $QActions;
	public $MinValue;
	public $MaxValue;
	public $MinDate;
	public $MaxDate;
	public $HasValidation;
	public $HasValidationNew;

	public $SelectOptions;
    public $QConsecutiveIDs;
    public $Scores;
	public $StrSelectOptions;
	public $QIndDimIds;
	public $QIndicatorIds;

	//@JAPR 2011-07-15: Agregada la lectura de la siguiente pregunta para las respuestas
	public $NextQuestion;
	//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
	public $HTMLText;
	//@JAPR 2012-10-11: Agregada la lectura de la imagen asociada para regresar en el servicio
	public $DisplayImage;
	//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
	public $Visible;
	//@JAPR
	public $ModelID;
	public $IndDimID;
	public $ImgDimID;
	public $DocDimID;
	public $SurveyTable;
	public $SurveyMatrixTable;
	public $SurveyField;
	//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	public $SurveyCatField;			//Campo con sintaxis "cat_" + QuestionID que contiene el valor del catálogo tal como lo manda el App (tabla paralela solamente)
	//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	public $CatIndDimID;			//Cla_descrip de la dimensión independiente del atributo de catálogo al que hace referencia esta pregunta (sólo aplica a simple choice de catálogo)
	//@JAPR
	public $AttributeName;
	public $AttributeNameOld;
	public $IsFieldSurveyID;
	public $IsIndicator;
	public $IsIndicatorBack;
	public $IsIndicatorNew;
	public $IndicatorID;
	public $OldIndicatorID;
	public $IsReqQuestion;
	public $HasReqComment;
	public $HasReqCommentOld;
	public $HasReqPhoto;
	//@JAPR 2012-06-08: Ahora se permitirá cambiar la opción de usar Foto durante la edición de preguntas
	public $HasReqPhotoOld;
	//@JAPR
	public $HasReqDocument;
	public $HasReqDocumentOld;
	public $HasActions;
	public $UseCatalog;
	public $UseCatalogOld;
	public $CatalogID;
	public $CatalogIDOld;
	public $CatMemberID;
	public $CatMemberIDOld;
	//@JAPR 2015-07-16: Agregada la integración con DataSources
	public $DataSourceID;					//ID del DataSource (catálogo de v6) al que se asocia esta pregunta
	public $DataSourceIDOld;				//ID del DataSource (catálogo de v6) al que se asocia esta pregunta
	public $DataSourceMemberID;				//ID del DataSourceMember (atributo de un catálogo de v6) al que se asocia esta pregunta
	public $DataMemberLatitudeID;			//ID del DataSourceMember para Latitude
	public $DataMemberLongitudeID;			//ID del DataSourceMember para Longitude
	//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
	public $DataMemberImageID;				//ID del DataSourceMember para imagen
	//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
	public $DataMemberOrderID;				//ID del DataSourceMember para ordenar los elementos extraídos desde el catálogo
	//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
	public $DataMemberOrderDir;				//La dirección del ordenamiento del atributo indicado en DataMemberOrderID (0=Ascendente, 1=Descendente)
	public $CustomLayout;					//El template HTML a utilizar como contenido de cada item en la lista de valores de la pregunta. Si está vacío entonces se generará la vista default
	//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
	public $DataSourceFilter;				//Filtro de catálogo personalizable a nivel de pregunta
	//@JAPR 2015-07-15: Rediseñadas las preguntas y secciones de catálogo para permitir seleccionar los atributos a usar y el orden deseado (uso de DataSources)
	public $CatMembersList;					//Array con los IDs de SI_SV_CatalogMember asociados a la pregunta
	//@JAPR
	
	//OMMC 2015-11-27: Agregadas las propiedades de las preguntas OCR
	public $SavePhoto;
	public $QuestionList;
	//OMMC

	public $CatMemberLatitudeID;
	public $CatMemberLatitudeIDOld;
	public $CatMemberLongitudeID;
	public $CatMemberLongitudeIDOld;
	//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
	public $CatMemberImageID;
	//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
	public $CatMemberOrderID;
	//@JAPR
	public $UseMap;
	public $MapRadio;
	
	public $SectionNumber;
	public $IsMultiDimension;
	public $IsMultiDimensionOld;
	public $Styled;
	public $UseCatToFillMC;
	public $UseCatToFillMCOld;

	public $MCInputType;
	public $MCInputTypeOld;

	public $FatherQuestionID;
	public $ChildQuestionID;

	public $OutsideOfSection;
	public $ImportCatOptions; //conchita 10-oct-2011
	public $GoToQuestion; //conchita 18-oct-2011
	public $IsIndicatorMC;
	public $IsIndicatorMCNew;
	public $IsIndicatorMCBack;
	
	//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
	public $SelectOptionsCol;
    public $QConsecutiveIDsCol;
    public $ScoresCol;
	public $StrSelectOptionsCol;
	public $ScoreOperatorsCol;
	//@JAPR
	public $QIndDimIdsCol;
	public $QIndicatorIdsCol;
	
	public $OneChoicePer;
	//@JAPR 2013-04-15: Agregada la opción de despliegue de texto para algunas preguntas (originalmente usado para las skip to section, foto personalizada y sincronización)
	//Este campo aparentemente se planeaba usar en conjunto con la pregunta ShowValue/ShowQuestion, pero dicha pregunta nunca se implementó y por tanto ninguna pregunta tendría
	//un valor diferente de 0. Originalmente contendría valores de Single line y multi line así que si se retoma la idea se dejarán combos correspondientes con dichos valores, pero 
	//por ahora se aplicará para la posición del texto en las preguntas que se basan en generar un botón con imagen
	public $TextDisplayStyle;			//Determina la posición del texto (o si se oculta) dentro del botón en preguntas que generan un botón como componente principal (Skip to section, Foto personalizada o Sincronización). Sin imagen es irrelevante y se muestra el texto centrado
	
	//Agregar propiedades que haran referencia a 
	//CategoryDimension, ElementDimension, ElementIndicator
	public $UseCategoryDim;
	public $CategoryDimName;
	public $ElementDimName;
	public $ElementIndName;
	public $CategoryDimID;
	public $ElementDimID;
	public $ElementIndID;
	
	public $CatDimValSelectOptions;
	public $ElemDimValSelectOptions;
	
	public $UseCategoryDimChoice;
	//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales
	public $AllowAdditionalAnswers;
	//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
	public $OtherLabel;					//Etiqueta que se usará en lugar de la palabra "Other" (opción de AllowAdditionalAnswers == true)
	public $CommentLabel;				//Etiqueta que se usará en lugar de la palabra "Comment"
	//@JAPR 2012-05-15: Agregadas las preguntas tipo Action
	public $ResponsibleID;				//Responsable de la acción a crear con esta pregunta
	public $DaysDueDate;				//Días de deadline a partir de la fecha de fin de captura de la Encuesta/Acción
	public $CategoryID;					//Id del elemento seleccionado del catálogo de categorías (si se modifica el catálogo, los elementos seleccionados deberían resetearse)
	//@JAPR 2012-05-29: Agregada la opción para indicar el número de decimales de las preguntas numéricas
	public $DecimalPlaces;				//Número de decimales esperados en la respuesta a una pregunta numérica (forzará a capturar este tipo de preguntas con este número de digitos extras a la derecha, los cuales a falta de punto decimal en el teclado de las Apps serán considerados como decimales)
	//@JAPR 2012-05-29: Agregado el soporte para creación de preguntas (single y multi) cuyas respuestas dependan de las seleccionadas de una pregunta multi previa
	public $SourceQuestionID;			//ID de la pregunta Múltiple de la que se obtendrán las respuestas a usar en la encuesta
	public $SourceQuestionIDOld;
	//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
	public $SharedQuestionID;			//ID de la pregunta Simple o Multiple choice con la cual esta pregunta comparte las opciones de respuesta (las opciones se encuentran grabadas sólo en la pregunta que indica este ID)
	public $SharedQuestionIDOld;
	//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas
	public $FormatMaskID;				//ID default de formato (usado sólo en la captura, no se graba). Cuando se elige un formato personalizado este valor == 1, sin formato == 0, cualquier otro default es >= 1
	public $FormatMask;					//Formato (de una lista default o personalizado) aplicado en la captura de las preguntas numéricas (cualquier tipo, sean abiertas o múltiples)
	//@JAPR 2012-07-28: Agregado el valor de fault para las preguntas (por ahora sólo aplica para preguntas abiertas)
	public $DefaultValue;				//Es una cadena con el valor default a desplegar para la pregunta, puede contener variables, si las contiene se debe llenar la tabla de dependencias
	public $QuestionsToEval;			//Array de los IDs de las preguntas que dependen de la respuesta de esta para permitir una evaluación automática al cambiar su valor
	public $HasQuestionDep;				//Indica si hay preguntas que dependan de esta. Es un campo en la Metadata para saber al cargar la instancia si se debe o no llenar el array QuestionsToEval y no estar ejecutando un SELECT adicional si no existen dependencias (se asigna sólo al grabar en la pregunta que modifica)
	//@JAPR 2012-10-19: Agregadas las preguntas marcadas como Read-Only (el requisito es que tengan un valor default)
	public $ReadOnly;					//Indica si la pregunta es o no Read-Only
	//@JAPR
    
    public $MaxChecked;
    public $QuestionImageText;
    public $QuestionMessage;
    public $DateFormatMaskID;				//ID default de formato (usado sólo en la captura, no se graba). Cuando se elige un formato personalizado este valor == 1, sin formato == 0, cualquier otro default es >= 1
	public $DateFormatMask;
	//@JAPR 2014-07-31: Agregado el Responsive Design (Variables para configurar propiedades por dispositivo usando el Framework)
	public $QuestionMessageiPod;
	public $QuestionImageTextiPod;
	public $QuestionMessageiPad;
	public $QuestionImageTextiPad;
	public $QuestionMessageiPadMini;
	public $QuestionImageTextiPadMini;
	public $QuestionMessageiPhone;
	public $QuestionImageTextiPhone;
	public $QuestionMessageCel;
	public $QuestionImageTextCel;
	public $QuestionMessageTablet;
	public $QuestionImageTextTablet;
	//conchita 2014-10-07 agregadas para imagen pregunta tipo sketch
	public $QuestionImageSketch;
	public $QuestionImageSketchiPod;
	public $QuestionImageSketchiPad;
	public $QuestionImageSketchiPadMini;
	public $QuestionImageSketchiPhone;
	public $QuestionImageSketchCel;
	public $QuestionImageSketchTablet;
	
	public $ResponsiveDesignProps;	//Array indexado por el nombre de la propiedad de Responsive Design (formato NombreDeviceID) cuando dicha propiedad ya existía en la tabla, si no está asignado significa que es nueva (esto es útil durante el grabado)
	//@JAPR
	public $ExcludeSelected;
	public $SourceSimpleQuestionID;
	public $SourceSimpleQuestionIDOld;
	public $SummaryTotal;
	//@JAPR 2012-10-29: Agregada la información extra para las acciones
	public $ActionTitle;				//Contiene el texto que se usará como título de la acción (puede contener variables en formato {@Q(#)}
	public $ActionText;					//Contiene la descripción extendida que se usará para la acción (puede contener variables en formato {@Q(#)}. El texto ya anteriormente capturado se concatena a este, a menos que se encuentre la variable {Mobile_Action_Desc} pues entonces se reemplaza en dicha posición
	//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
	public $eBavelActionFieldsDataRead;	//Indica si ya se leyeron los datos de mapeo para no repetir la carga de esta información, ya que la instancia se usa en diferentes lugares y es probable que se intente cargar varias veces
	public $eBavelActionFormID;			//Id de la forma de eBavel sobre la que se crearán las Acciones
	public $eBavelActionFieldsData;		//Variable temporal (no se graba en la metadata) con los pares de datos "SurveyFieldMapName=eBavelFormFieldID" separados por "|" utilizados durante el grabado para mapear los campos de eBavel
	public $eBavelActionFieldsDataOld;	//Igual que la variable de arriba pero para detectar si hubo o no cambios en el mapeo
	public $QuestionFieldMap;			//Objeto que contiene el mapeo de campos de eBavel a preguntas/datos de eForms (se asigna sólo al entrar a la función readeBavelActionFieldsMapping)
	//@JAPR
	public $FormulaChildren;
	public $Formula;
	public $ShowConditionChildren;
	public $ShowCondition;
	//@JAPR 2012-12-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
	public $eBavelFieldID;
	public $SectionType;
	//@JAPR 2015-09-09: Agregada esta propiedad para apoyar a validar el issue #KVBUK4
	public $SectionDisplayMode;			//Modo de despliegue de la sección a la que pertenece esta pregunta
	//@JAPR 2013-02-21: Agregada la opción para ocultar los textos de opciones de respuesta si tienen imagen asociada
	public $ShowImageAndText;			//Indica si las opciones de respuesta de las simple y multiple choice deberán o no mostrar el texto de la opción además de la imagen (si es que tienen imagen, si no entonces siempre muestran el texto). Depende del tipo de despliegue de la pregunta
	//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
	public $HasScores;					//Indica si la pregunta tiene medios para calcular su Score (solo aplica para Simple choice sin catálogo o multiple-choice multi-dimensión tipo checkbox no en secciones dinámicas)
	public $HasScoresOld;
	//@JAPR 2015-06-14: Corregido un bug, estaba mal el case
	public $PhoneNum;					//Conchita 2013-04-29 Agregado el campo para num de telefono
	
	public $OptionDefaultValue;
	
	//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
	public $RegisterGPSAt;				//Indica si se debe registrar la posición GPS al inicio de la captura de la sección (rgpsAtStart) o al momento de cambiar de sección (rgpsAtEnd)
	public $LatitudeAttribID;			//Indica el ID de la dimensión atributo para la Longitude
	public $LongitudeAttribID;			//Indica el ID de la dimensión atributo para la Latitude
	public $AccuracyAttribID;			//Indica el ID de la dimensión atributo para el Accuracy del GPS
	//@JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
	public $InputWidth;					//Tamaño del input en preguntas múltiple choice con despliegue horizontal si se usa al mismo tiempo una imagen por opción
	public $PictLayOut;					//Posición relativa de la imagen de una opción de múltiple choice respecto al texto, cuando se usa despliegue horizonta y además se pide ver el texto simultáneamente
	//@JAPR 2014-05-19: Agregado el tipo de sección Inline
	public $IsSelector;					//Indica si esta pregunta se trata de la pregunta Simple Choice que sirve como switch de las secciones Inline para determinar si un valor se captura o no, si lo es, no debe permitirse eliminar ni cambiar de tipo entre otras cosas
	//@JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice
	public $EnableCheckBox;				//Indica si a las preguntas múltiple choice con captura tipo numérica o texto, se les habilitará un CheckBox para indicar si se capturará o no, si no se va a capturar entonces se oculta el input correspondiente
	public $SwitchBehaviour;			//Indica si al contestar la pregunta múltiple choice (aplica para las tipo CheckBox o bien las que tienen la opción de mostrar un Checkbox al inicio) marcado significa realmente marcado (0 - default) o desmarcado (1) y por tanto ocultaría su input si es captura numérica/texto
	public $ShowInSummary;
	public $canvasWidth;
	public $canvasHeight;
	//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
	public $imageWidth;
	public $imageHeight;
	//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
	public $LastModAdminVersion;		//Versión del último código del Administrador de eForms que realizó el grabado de este objeto
	//@JAPR 2014-09-26: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
	public $LastModDateID;				//Fecha de la última modificación del objeto
	public $LastModUserID;				//ID del último usuario que modificó el objeto
	public $CreationAdminVersion;		//Versión del código del Administrador de eForms que creó este objeto
	public $CreationDateID;				//Fecha de creación del objeto
	public $CreationUserID;				//ID del usuario que creó el objeto
	//@JAPR 2014-09-29: Agregado el ancho de columnas en secciones Inline o el Summary de maestro-detalle
	//JAPR 2016-06-08: Modificada la propiedad para que ahora pueda ser un String (#WFLVTR)
	public $ColumnWidth;				//Ancho de la columna en sección Inline con vista Inline, o en el Summary de maestro-detalle con vista Tabla (0 == Default == ancho automático determinado por la tabla)
	//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
	public $SourceSectionID;			//ID de la sección a la que mapea una pregunta tipo Sección para mostrar su contenido como si fuera una pregunta (originalmente sólo aplicaba para secciones Inline)
	//@JAPR 2014-10-22: Agregada la opción para redibujar las preguntas tipo mensaje automáticamente
	public $AutoRedraw;
	
	public $EditorType;
	public $HTMLEditorState;

	public $QuestionMessageDes;
	public $QuestionMessageiPodDes;
	public $QuestionMessageiPadDes;
	public $QuestionMessageiPadMiniDes;
	public $QuestionMessageiPhoneDes;
	public $QuestionMessageCelDes;
	public $QuestionMessageTabletDes;

	public $QuestionMessageDesHTML;
	public $QuestionMessageiPodDesHTML;
	public $QuestionMessageiPadDesHTML;
	public $QuestionMessageiPadMiniDesHTML;
	public $QuestionMessageiPhoneDesHTML;
	public $QuestionMessageCelDesHTML;
	public $QuestionMessageTabletDesHTML;
	
	public $IsInvisible; //propiedad agregada para mantener oculta una pregunta al desplegar en la encuesta
	//@JAPR 2016-01-21: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
	public $ShowTotals;				//Indica si la esta pregutna que debe ser de sección Inline Tabla, deberá o no mostrar su acumulado en la Row de totales (sólo aplica en preguntas numéricas y calculadas)
	public $TotalsFunction;			//Indica el tipo de función de agregación a aplicar en el cálculo de la row de totales para las secciones Inline Tabla (por default SUM)
	//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
	//Propiedades con nombres de campos y/o tablas que apuntan a la nueva estructura de grabado de datos de las formas
	//public $KeyField;				//SVeForms_####_QuestionDet_xxxx.QuestionKey: Key auto generado que representa una de las posibles n respuestas seleccionadas de una pregunta Múltiple choice
	public $DescKeyFieldFK;			//SVeForms_####_SectionStd.QuestionKey_#### o SVeForms_####_Section_xxxx.QuestionKey_####: Key de la respuesta correspondiente en la tabla de la sección
	public $ValueField;				//SVeForms_####_SectionStd.QuestionVal_#### o SVeForms_####_Section_xxxx.QuestionVal_####: Valor numérico de la respuesta correspondiente en la tabla de la sección
	public $DescKeyField;			//SVeForms_####_Question_xxxx.QuestionDescKey (equivale a SVeForms_####_SectionStd.QuestionKey_#### o SVeForms_####_Section_xxxx.QuestionKey_####) Key de la respuesta correspondiente al registro de la sección donde pertenece esta pregunta (se genera mientras se va grabando la captura con un MAX +1)
	public $DescField;				//SVeForms_####_Question_xxxx.QuestionDesc: Respuesta capturada para esta pregunta, según el tipo, puede ser una ruta, un número, un string, etc. JP- 2015-07-10 Dada la limitante de tablas en un join en MySQL, sólo las preguntas Simple y Multiple Choice normalizarán sus respuestas, así que sólo en ellas es necesario este campo
	public $DescFieldAttr;			//SVeForms_####_Question_xxxx.QuestionDesc_####: Respuesta capturada para esta pregunta, según el tipo, puede ser una ruta, un número, un string, etc. pero esta se encuentra directamente en la tabla de registros, NO en una tabla normalizada de catálogo de respuestas
	public $KeyFieldCommFK;			//SVeForms_####_SectionStd.QuestionCommKey_#### o SVeForms_####_Section_xxxx.QuestionCommKey_####: Key del comentario adicional capturado a la respuesta de la pregunta en la tabla de la sección
	public $KeyFieldComm;			//SVeForms_####_QuestionComm_xxxx.QuestionCommKey: Key del comentario adicional capturado a la respuesta de la pregunta en la tabla de catálogo de comentarios
	public $DescFieldComm;			//SVeForms_####_QuestionComm_xxxx.QuestionComm: Texto del comentario adicional capturado a la respuesta de la pregunta en la tabla de catálogo de comentarios
	public $KeyFieldDocFK;			//SVeForms_####_SectionStd.QuestionDocKey_#### o SVeForms_####_Section_xxxx.QuestionDocKey_####: Key del documento adicional capturado a la respuesta de la pregunta en la tabla de la sección
	public $KeyFieldDoc;			//SVeForms_####_QuestionDoc_xxxx.QuestionDocKey: Key del documento adicional capturado a la respuesta de la pregunta en la tabla de catálogo de comentarios
	public $DescFieldDoc;			//SVeForms_####_QuestionDoc_xxxx.QuestionDoc: Texto del documento adicional capturado a la respuesta de la pregunta en la tabla de catálogo de comentarios
	public $KeyFieldImgFK;			//SVeForms_####_SectionStd.QuestionImgKey_#### o SVeForms_####_Section_xxxx.QuestionImgKey_####: Key de la imagen adicional capturada a la respuesta de la pregunta en la tabla de la sección
	public $KeyFieldImg;			//SVeForms_####_QuestionImg_xxxx.QuestionImgKey: Key de la imagen adicional capturada a la respuesta de la pregunta en la tabla de catálogo de imagenes
	public $DescFieldImg;			//SVeForms_####_QuestionImg_xxxx.QuestionImg: Ruta de la imagen adicional capturada a la respuesta de la pregunta en la tabla de catálogo de imagenes
	public $KeyFieldPtsFK;			//SVeForms_####_SectionStd.QuestionPtsKey_#### o SVeForms_####_Section_xxxx.QuestionPtsKey_####: Key del score capturado con la respuesta de la pregunta en la tabla de la sección
	public $KeyFieldPts;			//SVeForms_####_QuestionPts_xxxx.QuestionPtsKey: Key del score capturado con la respuesta de la pregunta en la tabla de catálogo de imagenes
	public $DescFieldPts;			//SVeForms_####_QuestionPts_xxxx.QuestionPts: Valor del score capturado con la respuesta de la pregunta en la tabla de catálogo de imagenes
	public $KeyFieldValFK;			//SVeForms_####_QuestionDet_xxxx.QuestionValKey o SVeForms_####_QuestionVal_xxxx.QuestionValKey: Key de la respuesta adicional correspondiente al registro de la sección donde se seleccionó una opción de respuesta de esta pregunta Múltiple Choice (se genera mientras se va grabando la captura con un MAX +1). Es el mismo en la tabla del catálogo de respuestas adicionales
	public $KeyFieldVal;			//Igual que $KeyFieldValFK
	public $DescFieldVal;			//SVeForms_####_QuestionVal_xxxx.QuestionVal: Descripción de la respuesta adicional correspondiente al registro de la sección donde se seleccionó una opción de respuesta de esta pregunta Múltiple Choice en la tabla de catálogo de respuestas adicionales
	public $QuestionTable;			//SVeForms_####_Question_xxxx: Tabla que contiene las descripciones capturadas como respuesta de la pregunta, o bien los valores de los atributos si es de catálogo
	public $QuestionImgTable;		//SVeForms_####_QuestionImg_xxxx: Tabla que contiene las rutas de las imagenes capturadas como respuesta de la pregunta
	public $QuestionCommTable;		//SVeForms_####_QuestionComm_xxxx: Tabla que contiene los comentarios capturados adicionales a la respuesta de la pregunta
	public $QuestionDocTable;		//SVeForms_####_QuestionDoc_xxxx: Tabla que contiene los documentos capturados adicionales a la respuesta de la pregunta
	public $QuestionPtsTable;		//SVeForms_####_QuestionPts_xxxx: Tabla que contiene los scores asignados a las opciones de respuesta respondidas de la pregunta
	public $QuestionDetTable;		//SVeForms_####_QuestionDet_xxxx: Tabla que contiene los keys de las opciones de respuesta seleccionadas de una múltiple choice como respuesta, así como la referencia al valor extra capturado para ella en cada opción
	public $QuestionValTable;		//SVeForms_####_QuestionVal_xxxx: Tabla que contiene las descripciones (valores) adicionales a las opciones de respuesta, según el tipo de captura de la pregunta múltiple choice
	public $SectionTable;			//SVeForms_####_SectionStd o SVeForms_####_Section_xxxx: Nombre de la tabla que contiene los registros de la sección
	public $AllowGallery; 			//Permite el uso de la galería por pregunta esto aplica a partir de la 6.00029
	//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
	public $DataDestinationID;		//ID del destino de datos a invocar para grabar la información de la forma
	public $NextSectionIDIfTrue;	//Variable temporal (no se graba en la metadata). Indica la sección a saltar cuando la pregunta Update Data responde al grabado del destino con True
	public $NextSectionIDIfFalse;	//Variable temporal (no se graba en la metadata). Indica la sección a saltar cuando la pregunta Update Data responde al grabado del destino con False
	public $DisablePhoneNum; 		//Variable para el comportamiento default de la pregunta llamada, oculta el número telefónico.
	public $DisableSync;			//Variable para el comportamiento default de la pregunta llamada, desabilita la sincronización.
	public $CopiedQuestionID;		//Si la pregunta es generada mediante el proceso de copiado de forma entonces esta variable guarda el id de la pregunta que se copio
	public $CreateCat;				//Es utilizada para condicionar el código de creación de catálogo, en la copia de formas se hace un segundo save para ajustar IDs de la
									//pregunta en este caso no es necesario que se ejecute todo el código de creación de catálogos por que este se hizo en el primer save al copiar
									//la pregunta
	//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
	public $SourceID;				//ID del objeto original al que apunta esta instancia para mantener la sincronización entre formas de desarrollo/producción
	//@JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
	public $UploadMediaFiles;		//Indica si la pregunta updateData deberá o no sincronizar también los archivos de media de la captura (true) o simplemente las respuestas del .dat (false - default)
	//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
	public $IsCoordX;				//Indica si esta pregunta se trata de la pregunta Numérica que sirve para almacenar la posición X por registro de la sección Maestro-detalle que representa a un elemento colocado en una pregunta Sketch+
	public $IsCoordY;				//Indica si esta pregunta se trata de la pregunta Numérica que sirve para almacenar la posición Y por registro de la sección Maestro-detalle que representa a un elemento colocado en una pregunta Sketch+
	public $IsItemName;				//Indica si esta pregunta se trata de la pregunta Alfanumérica que sirve para almacenar el nombre de la opción de respuesta seleccionada por registro de la sección Maestro-detalle que representa a un elemento colocado en una pregunta Sketch+
	//OMMC 2019-03-25: Pregunta AR #4HNJD9
	public $DataMemberZipFileID;
	public $CatMemberZipFileID;
	public $DataMemberVersionID;
	public $CatMemberVersionID;
	public $DataMemberPlatformID;
	public $CatMemberPlatformID;
	public $DataMemberTitleID;
	public $CatMemberTitleID;
	//@JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
	public $RecalculateDependencies;	//Indica si cuando esta pregunta cambie de valor, deberá forzar a recalcular nuevamente el defaultValue de las preguntas que dependen de ellas
	//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
	public $AutoSelectEmptyOpt;		//Indica si esta pregunta (que solo podría ser selección sencilla tipo menú) seleccionará automáticamente la opción vacía (1) o si seleccionará la primer opción de respuesta que contenga (default - 1)
	
	function __construct($aRepository, $aSectionID)
	{
		//@JAPR 2018-10-30: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		global $gbDesignMode;
		//@JAPR
		
		BITAMObject::__construct($aRepository);
		//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas (campo FormatMask)
		self::$arrNumericFormats[1] = "(".translate("Customized").")";
		//@JAPR
		
		$this->ForceNew = false;
		$this->SurveyID = -1;
		$this->SurveyName = "";
		$this->SectionID = $aSectionID;
		$this->OldSectionID = $aSectionID;
		$this->SectionName = "";
		$this->QuestionID = -1;
		$this->QuestionNumber = 0;
		
		$this->QuestionText = "";
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		$this->QuestionTextHTML = "";
		//@JAPR
		$this->DisplayQuestion = "";
		$this->QTypeID = qtpOpenNumeric;
		//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
		//Ahora el tipo de llenado del componente será un campo mas en la metadata
		$this->ValuesSourceType = tofFixedAnswers;
		//JAPR 2016-12-21: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
		$this->ValuesSourceTypeOld = $this->ValuesSourceType;
		//@JAPR 2012-12-07: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		$this->EBQTypeID = $this->QTypeID;
		//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
		$this->GQTypeID = qtpOpen;
		$this->QDataType = dtpString;
		//@JAPR 2015-11-04: Corregido un bug, al no inicializar estos valores, cuando se eliminaba una pregunta generana un UPDATE mal armado que provocaba un erro (#7164EG)
		$this->QDisplayMode = dspVertical;
		$this->QComponentStyleID = kybNumeric;
		//@JAPR 2011-06-29: Agregada la longitud de los campos Alfanumericos
		$this->QLength = 0;
		//@JAPR 2015-05-19: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
		//estaban limitadas a 255 caracteres (#BA7866)
		$this->OldQLength = 0;
		//@JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
		$this->MaxQLength = 0;
		//@JAPR
		//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
		$this->NoFlow = 0;

		$this->SelectOptions = array();
        $this->QConsecutiveIDs = array();
        $this->Scores = array();
		$this->StrSelectOptions = "";
		$this->QIndDimIds = array();
		$this->QIndicatorIds = array();

		//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
		$this->SelectOptionsCol = array();
        $this->QConsecutiveIDsCol = array();
        $this->ScoresCol = array();
		$this->StrSelectOptionsCol = "";
		$this->ScoreOperatorsCol = array();
		$this->QIndDimIdsCol = array();
		$this->QIndicatorIdsCol = array();

		//@JAPR 2011-07-15: Agregada la lectura de la siguiente pregunta para las respuestas
		$this->NextQuestion = array();
		//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
		$this->HTMLText = array();
		//@JAPR 2012-10-11: Agregada la lectura de la imagen asociada para regresar en el servicio
		$this->DisplayImage = array();
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		$this->Visible = array();
		//@JAPR
		$this->CancelAgenda = array();
		
		//@JAPR 2015-06-14: Corregido un bug, estaba mal el case
		$this->PhoneNum = array();
		//@JAPR 2015-02-10: Corregido un bug, estaba mal escrita la referencia de this
		$this->OptionDefaultValue = array();
		
		$this->OldQTypeID = qtpOpenNumeric;
		$this->QComment = "";
		$this->QActions = "";
		$this->MinValue = "";
		$this->MaxValue = "";
		$this->MinDate = "";
		$this->MaxDate = "";
		$this->HasValidation = 0;
		$this->HasValidationNew = 0;
		$this->ModelID = -1;
		$this->IndDimID = 0;
		$this->ImgDimID = 0;
		$this->DocDimID = 0;
		$this->SurveyTable = "";
		$this->SurveyMatrixTable = "";
		$this->SurveyField = "";
		//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$this->SurveyCatField = "";
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$this->CatIndDimID = 0;
		//@JAPR
		$this->AttributeName = "";
		$this->AttributeNameOld = "";
		$this->IsFieldSurveyID = 0;
		$this->IsIndicator = 0;
		$this->IsIndicatorBack = 0;
		$this->IsIndicatorNew = 0;
		$this->IndicatorID = 0;
		$this->OldIndicatorID = 0;

		$this->IsReqQuestion = 0;
		$this->HasReqComment = 0;
		$this->HasReqCommentOld = 0;
		$this->HasReqPhoto = 0;
		$this->HasReqDocument = 0;
		//@JAPR 2012-06-08: Ahora se permitirá cambiar la opción de usar Foto durante la edición de preguntas
		$this->HasReqPhotoOld = 0;
		//@JAPR
		$this->HasReqDocumentOld = 0;
		$this->HasActions = 0;

		$this->UseCatalog = 0;
		$this->UseCatalogOld = 0;
		$this->CatalogID = 0;
		$this->CatalogIDOld = 0;
		$this->CatMemberID = 0;
		$this->CatMemberIDOld = 0;
		//@JAPR 2015-07-16: Agregada la integración con DataSources
		$this->DataSourceID = 0;
		$this->DataSourceIDOld = 0;
		$this->DataSourceMemberID = 0;
		$this->DataMemberLatitudeID = 0;
		$this->DataMemberLongitudeID = 0;
		//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
		$this->DataMemberImageID = 0;
		//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		$this->DataMemberOrderID = 0;
		//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
		$this->DataMemberOrderDir = ordbAsc;
		$this->CustomLayout = '';
		//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
		$this->DataSourceFilter = '';
		//@JAPR 2015-07-15: Rediseñadas las preguntas y secciones de catálogo para permitir seleccionar los atributos a usar y el orden deseado (uso de DataSources)
		$this->CatMembersList = array();
		//@JAPR
		//OMMC 2015-1-27: Agregadas para las preguntas tipo OCR
		$this->SavePhoto = 0;
		$this->QuestionList = array();
		//OMMC

		$this->CatMemberLatitudeID = 0;
		$this->CatMemberLatitudeIDOld = 0;
		$this->CatMemberLongitudeID = 0;
		$this->CatMemberLongitudeIDOld = 0;
		//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
		$this->CatMemberImageID = 0;
		//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		$this->CatMemberOrderID = 0;
		//@JAPR
		$this->UseMap = 0;
		$this->MapRadio = 0;
		
		$this->SectionNumber = 0;
		$this->IsMultiDimension = 0;
		$this->IsMultiDimensionOld = 0;
		$this->UseCatToFillMC = 0;
		$this->UseCatToFillMCOld = 0;

		$this->MCInputType = mpcCheckBox;
		$this->MCInputTypeOld = mpcCheckBox;

		$this->FatherQuestionID = 0;
		$this->ChildQuestionID = 0;
		$this->ImportCatOptions = 0; //Conchita 10-oct-2011
		$this->OutsideOfSection = 0;
		$this->GoToQuestion = 0; //Conchita 18-oct-2011
		$this->IsIndicatorMC = 0;
		$this->IsIndicatorMCNew = 0;
		$this->IsIndicatorMCBack = 0;
		
		$this->OneChoicePer = 0;
		$this->TextDisplayStyle = butTxtNone;
		
		$this->UseCategoryDim = 0;
		$this->CategoryDimName = "";
		$this->ElementDimName = "";
		$this->ElementIndName = "";
		$this->CategoryDimID = 0;
		$this->ElementDimID = 0;
		$this->ElementIndID = 0;
		
		$this->UseCategoryDimChoice = 0;
		
		$this->CatDimValSelectOptions = array();
		$this->ElemDimValSelectOptions = array();

		//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales
		$this->AllowAdditionalAnswers = 0;
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		$this->OtherLabel = '';
		$this->CommentLabel = '';
		//@JAPR 2012-05-15: Agregadas las preguntas tipo Action
		$this->ResponsibleID = -1;
		$this->CategoryID = 0;
		$this->DaysDueDate = 0;
		//@JAPR 2012-05-29: Agregada la opción para indicar el número de decimales de las preguntas numéricas
		//@JAPR 2012-05-29: Agregado el soporte para creación de preguntas (single y multi) cuyas respuestas dependan de las seleccionadas de una pregunta multi previa
		$this->DecimalPlaces = 0;
		$this->SourceQuestionID = 0;
		$this->SourceQuestionIDOld = 0;
		//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		$this->SharedQuestionID = 0;
		$this->SharedQuestionIDOld = 0;
		//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas
		$this->FormatMaskID = 0;
		$this->FormatMask = '';
		//@JAPR
        
        $this->MaxChecked = 0;
        $this->QuestionImageText = '';
        $this->QuestionMessage = '';
		//@JAPR 2014-07-31: Agregado el Responsive Design (Variables para configurar propiedades por dispositivo usando el Framework)
		$this->QuestionMessageiPod = '';
		$this->QuestionImageTextiPod = '';
		$this->QuestionMessageiPad = '';
		$this->QuestionImageTextiPad = '';
		$this->QuestionMessageiPadMini = '';
		$this->QuestionImageTextiPadMini = '';
		$this->QuestionMessageiPhone = '';
		$this->QuestionImageTextiPhone = '';
		$this->QuestionMessageCel = '';
		$this->QuestionImageTextCel = '';
		$this->QuestionMessageTablet = '';
		$this->QuestionImageTextTablet = '';
		//Conchita agregadas las imagenes para sketch
		$this->QuestionImageSketch = '';
		$this->QuestionImageSketchiPod = '';
		$this->QuestionImageSketchiPad = '';
		$this->QuestionImageSketchiPadMini = '';
		$this->QuestionImageSketchiPhone = '';
		$this->QuestionImageSketchCel = '';
		$this->QuestionImageSketchTablet = '';
		$this->ResponsiveDesignProps = array();
        //@JAPR
        $this->DateFormatMaskID = 0;
		$this->DateFormatMask = '';
		
		$this->DefaultValue = '';
		$this->HasQuestionDep = 0;
		
		$this->ExcludeSelected = 0;
		$this->SourceSimpleQuestionID = 0;
		$this->SourceSimpleQuestionIDOld = 0;
		
		$this->SummaryTotal = 0;
		$this->Formula = '';
		$this->ShowCondition = '';
		//@JAPR 2012-10-19: Agregadas las preguntas marcadas como Read-Only
		$this->ReadOnly = 0;
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		$this->ActionText = '';
		$this->ActionTitle = '';
		//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
		$this->eBavelActionFieldsDataRead = false;
		$this->eBavelActionFormID = 0;
		$this->eBavelActionFieldsData = '';
		$this->eBavelActionFieldsDataOld = '';
		$this->QuestionFieldMap = null;
		//@JAPR 2012-12-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		$this->eBavelFieldID = 0;
		//@JAPR 2013-02-21: Agregada la opción para ocultar los textos de opciones de respuesta si tienen imagen asociada
		$this->ShowImageAndText = 0;
		//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
		$this->HasScores = false;
		$this->HasScoresOld = false;
		//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		$this->RegisterGPSAt = rgpsAtEnd;
		$this->LatitudeAttribID = -1;
		$this->LongitudeAttribID = -1;
		$this->AccuracyAttribID = -1;
		//@JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
		$this->InputWidth = 0;
		$this->PictLayOut = plyoLeft;
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		$this->IsSelector = false;
		//@JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice
		$this->EnableCheckBox = 0;
		$this->SwitchBehaviour = 0;
		
		$this->ShowInSummary = 0;
		//OMMC 2016-07-04: Cambiada la inicialización de el width y height de 0 a '' ya que al crear la pregunta y definir una foto sin editar sus 
		//dimensiones se estaba creando con alto y ancho de 0 por lo que no aparecía en el App.
		$this->canvasWidth = '';
		$this->canvasHeight = '';
		//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
		$this->imageWidth = '';
		$this->imageHeight = '';
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		$this->LastModAdminVersion = 0;
		//@JAPR 2014-09-26: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
		$this->LastModDateID = '';
		$this->LastModUserID = 0;
		$this->CreationAdminVersion = 0;
		$this->CreationDateID = '';
		$this->CreationUserID = 0;
		//@JAPR 2014-09-29: Agregado el ancho de columnas en secciones Inline o el Summary de maestro-detalle
		//JAPR 2016-06-08: Modificada la propiedad para que ahora pueda ser un String (#WFLVTR)
		if (getMDVersion() >= esvColumnWidthString) {
			$this->ColumnWidth = '';
		}
		else {
			$this->ColumnWidth = 0;
		}
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		$this->SourceSectionID = 0;
		//@JAPR 2014-10-22: Agregada la opción para redibujar las preguntas tipo mensaje automáticamente
		$this->AutoRedraw = 0;
		//@JAPR 2013-02-25: Modificada la forma de validar para usar una función genérica que ya se tenía (caso de curl donde HTTP_USER_AGENT no existe)
		//$u_agent = $_SERVER['HTTP_USER_AGENT'];
		//if(preg_match('/MSIE/i',$u_agent)) 
		if (is_IE())
    	{
	    	$this->Styled = 62; 
    	} else {
    		$this->Styled = 100;
    	}
		
		//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		//@JAPR 2018-10-30: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//No se puede remover por completo la carga de esta información, ya que para preguntas nuevas era indispensable para poder ordernarlas correctamente, así que se validará
		//únicamente para entrar si está en modo de diseño
		if (getMDVersion() < esveFormsv6 || $gbDesignMode) {
			//Actualizar el ModelID, pero tambien actualiza 
			//el SectionName, SurveyTable y SectionNumber
			$this->getModel();
		}
		//@JAPR
		if ($this->SectionID > 0) {
			$curSectionInstance = BITAMSection::NewInstanceWithID($this->Repository, $this->SectionID);
			if (!is_null($curSectionInstance)) {
				$this->SectionType = $curSectionInstance->SectionType;
				//@JAPR 2015-09-09: Agregada esta propiedad para apoyar a validar el issue #KVBUK4
				$this->SectionDisplayMode = $curSectionInstance->DisplayMode;
				//@JAPR 2014-05-20: Asignado automáticamente el SurveyID ya que de todas formas se tiene asignado el SectionID y se carga su instancia
				$this->SurveyID = $curSectionInstance->SurveyID;
			}
		}
		//@JAPR
		
		//Opciones - [0] = "Tinymce", 1 = "Designer";
		//@JAPR 2015-08-15: Agregado el ipo de pregunta HTML
		$this->EditorType = edtpMessage;
		//@JAPR

		//Opciones  - 0 = "Graphic", 1 = "HTML";
		//OMMC 2016-01-18: Agregado para la pregunta HTML (redactor)
		$this->HTMLEditorState = edstGraphic;

		$this->IsInvisible = 0; //Agregada propiedad para marcar si es invisible una question no la muestra en la encuesta
		//@JAPR 2016-01-21: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
		$this->ShowTotals = 0;
		$this->TotalsFunction = 'SUM';
		//@JAPR
		$this->QuestionMessageDes = '';
		$this->QuestionMessageiPodDes = '';
		$this->QuestionMessageiPadDes = '';
		$this->QuestionMessageiPadMiniDes = '';
		$this->QuestionMessageiPhoneDes = '';
		$this->QuestionMessageCelDes = '';
		$this->QuestionMessageTabletDes = '';
		$this->QuestionMessageDesHTML = '';
		$this->QuestionMessageiPodDesHTML = '';
		$this->QuestionMessageiPadDesHTML = '';
		$this->QuestionMessageiPadMiniDesHTML = '';
		$this->QuestionMessageiPhoneDesHTML = '';
		$this->QuestionMessageCelDesHTML = '';
		$this->QuestionMessageTabletDesHTML = '';
		//OMMC 2016-02-24: Agregado el uso de la galería de fotos
		$this->AllowGallery = 0;
		//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
		$this->DataDestinationID = 0;
		$this->NextSectionIDIfTrue = 0;
		$this->NextSectionIDIfFalse = 0;
		//OMMC 2016-09-01: Modificado el comportamiento default de la pregunta llamada
		$this->DisablePhoneNum = 0;
		$this->DisableSync = 0;
		$this->CopiedQuestionID = 0; 
		$this->CreateCat = true;
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		$this->SourceID = 0;
		//@JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)		
		$this->UploadMediaFiles = 0;
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$this->IsCoordX = 0;
		$this->IsCoordY = 0;
		$this->IsItemName = 0;
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		$this->DataMemberZipFileID = 0;
		$this->CatMemberZipFileID = 0;
		$this->DataMemberVersionID = 0;
		$this->CatMemberVersionID = 0;
		$this->DataMemberPlatformID = 0;
		$this->CatMemberPlatformID = 0;
		$this->DataMemberTitleID = 0;
		$this->CatMemberTitleID = 0;
		//@JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
		$this->RecalculateDependencies = 0;
		//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
		$this->AutoSelectEmptyOpt = 0;
	}
	
	public static function getQuery ($opts) {
		$sQuery = BITAMQuestion::getMainQuery();
		//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//El join se debe agregar independientemente de si se reciben o no filtros en el array de parámetros
		$sQuery .= " WHERE t1.SurveyID = t2.SurveyID AND t1.SectionID = t3.SectionID ";
		//@JAPR
		if(is_array($opts)) {
			if(isset($opts["filter"])) {
				$filter = $opts["filter"];
				if(is_string($filter) && $filter != '') {
					$sQuery .= $filter;
				}
			}
			if(isset($opts["orderBy"])) {
				$orderBy = $opts["orderBy"];
				if(is_string($orderBy) && $orderBy != '') {
					$sQuery .= (" ORDER BY " .$orderBy);
				}
			}
		}
		return $sQuery;
	}
	
	public static function deployableFields () {
		$strAdditionalFields = "";
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		BITAMQuestion::applyUptodate(esvExtendedActionsData, $strAdditionalFields, ', t1.ActionText, t1.ActionTitle ');
		//@JAPR 2012-12-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		BITAMQuestion::applyUptodate(esveFormsV41Func, $strAdditionalFields, ', t1.ReadOnly, t1.Formula, t1.HasReqDocument, t1.DocDimID, t1.ShowCondition ');
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//@JAPR 2013-02-21: Agregada la opción para ocultar los textos de opciones de respuesta si tienen imagen asociada
		BITAMQuestion::applyUptodate(esvCatalogDissociation, $strAdditionalFields, ', t2.DissociateCatDimens, t1.ShowImageAndText');
		//@JAPR 2013-04-19: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		BITAMQuestion::applyUptodate(esveBavelMapping, $strAdditionalFields, ', t1.eBavelFieldID');
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
			$strAdditionalFields .= ', t1.OtherLabel, t1.CommentLabel ';
		}
		//@JAPR 2013-07-15: Agregado el mapeo de una forma de eBavel para acciones
		BITAMQuestion::applyUptodate(esveBavelSuppActions123, $strAdditionalFields, ', t1.eBavelActionFormID');
		//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		BITAMQuestion::applyUptodate(esvSimpleChoiceSharing, $strAdditionalFields, ', t1.SharedQuestionID');
		//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		BITAMQuestion::applyUptodate(esvGPSQuestion, $strAdditionalFields, ', t1.RegisterGPSAt, t1.LatitudeAttribID, t1.LongitudeAttribID, t1.AccuracyAttribID');
		//@JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
		BITAMQuestion::applyUptodate(esvPasswordLangAndRedesign, $strAdditionalFields, ', t1.InputWidth, t1.PictLayOut');
		BITAMQuestion::applyUptodate(esvNearBy, $strAdditionalFields, ', t1.CatMemberLatitudeID, t1.CatMemberLongitudeID, t1.UseMap, t1.MapRadio');
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		BITAMQuestion::applyUptodate(esvInlineSection, $strAdditionalFields, ', t3.SelectorQuestionID');
		//@JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice
		BITAMQuestion::applyUptodate(esvMCHSwitchBehav, $strAdditionalFields, ', t1.EnableCheckBox, t1.SwitchBehaviour');
		//@JAPR 2014-08-18: Corregido un bug, se había agregado la constante de versión como String por lo que fallaba la validación
		BITAMQuestion::applyUptodate(esvShowInSummary, $strAdditionalFields, ', t1.ShowInSummary');
		BITAMQuestion::applyUptodate(esvSketch, $strAdditionalFields, ', t1.CanvasWidth');
		BITAMQuestion::applyUptodate(esvSketch, $strAdditionalFields, ', t1.CanvasHeight');
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		BITAMQuestion::applyUptodate(esvExtendedModifInfo, $strAdditionalFields, ', t1.CreationUserID, t1.LastUserID, t1.CreationDateID, t1.LastDateID, t1.CreationVersion, t1.LastVersion');
		//@JAPR 2014-09-29: Agregado el ancho de columnas en secciones Inline o el Summary de maestro-detalle
		BITAMQuestion::applyUptodate(esvInlineMerge, $strAdditionalFields, ', t1.ColumnWidth');
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		BITAMQuestion::applyUptodate(esvSectionQuestions, $strAdditionalFields, ', t1.SourceSectionID');
		//Conchita agregado el SketchImage
		BITAMQuestion::applyUptodate(esvSketchImage, $strAdditionalFields, ', t1.SketchImage');
		//Conchita agregada la propiedad isinvisible
		BITAMQuestion::applyUptodate(esvIsInvisible, $strAdditionalFields, ', t1.IsInvisible');
		//@JAPR 2014-10-22: Agregada la opción para redibujar las preguntas tipo mensaje automáticamente
		BITAMQuestion::applyUptodate(esvInlineTypeOfFilling, $strAdditionalFields, ', t1.AutoRedraw');
		BITAMQuestion::applyUptodate(esvEditorHTML, $strAdditionalFields, ', t1.EditorType, t1.QuestionMessageDes');
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		BITAMQuestion::applyUptodate(esvAdminWYSIWYG, $strAdditionalFields, ', t1.QuestionTextHTML');
		//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
		//Ahora el tipo de llenado del componente será un campo mas en la metadata
		BITAMQuestion::applyUptodate(esvAdminWYSIWYG, $strAdditionalFields, ', t1.ValuesSourceType');
		//@JAPR 2015-07-16: Agregada la integración con DataSources
		BITAMQuestion::applyUptodate(esvAdminWYSIWYG, $strAdditionalFields, ', t1.DataSourceID');
		BITAMQuestion::applyUptodate(esvAdminWYSIWYG, $strAdditionalFields, ', t1.DataSourceMemberID');
		BITAMQuestion::applyUptodate(esvAdminWYSIWYG, $strAdditionalFields, ', t1.DataMemberLatitudeID');
		BITAMQuestion::applyUptodate(esvAdminWYSIWYG, $strAdditionalFields, ', t1.DataMemberLongitudeID');
		//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
		BITAMQuestion::applyUptodate(esvAdminWYSIWYG, $strAdditionalFields, ', t1.DataSourceFilter');
		//@JAPR
		//OMMC 2015-11-30: Agregado el tipo de pregunta OCR
		BITAMQuestion::applyUptodate(esvOCRQuestionFields, $strAdditionalFields, ', t1.SavePhoto');
		//@JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
		BITAMQuestion::applyUptodate(esvMaxQLength, $strAdditionalFields, ', t1.MaxQLength');
		//@JAPR
		//OMMC 2016-01-18: Agregado para las preguntas HTML
		BITAMQuestion::applyUptodate(esvHTMLEditorState, $strAdditionalFields, ', t1.HTMLEditorState');
		//@JAPR 2016-01-21: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
		BITAMQuestion::applyUptodate(esvShowTotalsInline, $strAdditionalFields, ', t1.ShowTotals, t1.TotalsFunction');
		//OMMC 2016-02-24: Agregada la configuración para permitir la galería de fotos
		BITAMQuestion::applyUptodate(esvAllowGallery, $strAdditionalFields, ', t1.AllowGallery');
		//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
		BITAMQuestion::applyUptodate(esvSimpleChoiceCatOpts, $strAdditionalFields, ', t1.CatMemberImageID, t1.DataMemberImageID');
		//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
		BITAMQuestion::applyUptodate(esvUpdateData, $strAdditionalFields, ', t1.DataDestinationID');
		//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
		BITAMQuestion::applyUptodate(esvNoFlow, $strAdditionalFields, ', t1.NoFlow');
		//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		BITAMQuestion::applyUptodate(esvSChoiceMetro, $strAdditionalFields, ', t1.CatMemberOrderID, t1.DataMemberOrderID');
		//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
		BITAMQuestion::applyUptodate(esvSChoiceMetro, $strAdditionalFields, ', t1.DataMemberOrderDir, t1.CustomLayout');
		//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
		BITAMQuestion::applyUptodate(esvImageDisplay, $strAdditionalFields, ', t1.ImageWidth');
		BITAMQuestion::applyUptodate(esvImageDisplay, $strAdditionalFields, ', t1.ImageHeight');
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		BITAMQuestion::applyUptodate(esvCopyForms, $strAdditionalFields, ', t1.SourceID');
		//OMMC 2016-09-01: Modificado el comportamiento default de la pregunta llamada
		BITAMQuestion::applyUptodate(esvPhoneCall, $strAdditionalFields, ', t1.DisablePhoneNum, t1.DisableSync');
		//@JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
		BITAMQuestion::applyUptodate(esvUpdateDataWithFiles, $strAdditionalFields, ', t1.UploadMediaFiles');
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		BITAMQuestion::applyUptodate(esvSketchPlus, $strAdditionalFields, ', t3.XPosQuestionID, t3.YPosQuestionID, t3.ItemNameQuestionID');
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		BITAMQuestion::applyUptodate(esvUpdateDataWithFiles, $strAdditionalFields, ', t1.DataMemberZipFileID, t1.CatMemberZipFileID, t1.DataMemberVersionID, t1.CatMemberVersionID, t1.DataMemberPlatformID, t1.CatMemberPlatformID, t1.DataMemberTitleID, t1.CatMemberTitleID');
		//@JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
		BITAMQuestion::applyUptodate(esvRecalcDep, $strAdditionalFields, ', t1.RecalculateDependencies');
		//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
		BITAMQuestion::applyUptodate(esvAutoSelectSChOpt, $strAdditionalFields, ', t1.AutoSelectEmptyOpt');
		
		return $strAdditionalFields;
	}
	
	public static function applyUptodate($version, &$target, $subject) {
		if (getMDVersion() >= $version) {
			$target .= $subject;
		}
	}
	
	public static function getMainQuery () {
		$strAdditionalFields = BITAMQuestion::deployableFields();
		$sQuery = "SELECT t1.SurveyID, t2.SurveyName, t1.SectionID, t1.QuestionID, t1.QuestionNumber, t1.QuestionText, t1.QTypeID, 
					t1.QComment, t1.QActions, t1.HasValidation, t1.MinValue, t1.MaxValue, t1.MinDate, t1.MaxDate, t1.OptionsText, t1.IndDimID, 
					t1.ImgDimID, t1.AttributeName, t1.IsFieldSurveyID, t1.IsIndicator, t1.IndicatorID, t1.IsReqQuestion, t1.HasReqComment, 
					t1.HasReqPhoto, t1.HasActions, t1.MCInputType, t1.UseCatalog, t1.CatalogID, t1.CatMemberID, t1.QDisplayMode, t1.OptionsTextCol, t1.QLength,
					t1.IsMultiDimension, t1.UseCatToFillMC, t1.FatherQuestionID, t1.ChildQuestionID, t1.OutsideOfSection, t3.SectionNumber, t3.SectionName, t1.GoToSection, 
					t1.IsIndicatorMC, t1.OneChoicePer, t1.TextDisplayStyle, t1.UseCategoryDim, t1.CategoryDimName, t1.ElementDimName, t1.ElementIndName, t1.CategoryDimID, 
					t1.ElementDimID, t1.ElementIndID, t1.AllowAdditionalAnswers, t1.ResponsibleID, t1.CategoryID, t1.DaysDueDate, t1.QComponentStyleID, 
					t1.DecimalPlaces, t1.SourceQuestionID, t1.FormatMask, t1.MaxChecked, t1.QuestionImageText, t1.QuestionMessage, t1.DateFormatMask, t1.DefaultValue, t1.HasQuestionDep,
					t1.ExcludeSelected, t1.SourceSimpleQuestionID, t1.SummaryTotal $strAdditionalFields
					FROM SI_SV_Question t1, SI_SV_Survey t2, SI_SV_Section t3";
		return $sQuery;
	}

	static function NewInstanceEmpty($aRepository, $aSectionID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $aSectionID);
	}
	
	static function NewInstance($aRepository, $aSectionID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $aSectionID);
	}

	static function NewInstanceWithID($aRepository, $aQuestionID, $aGetValues = false)
	{
		$anInstance = null;
		
		if (((int) $aQuestionID) <= 0)
		{
			return $anInstance;
		}
		
		$sFilter = "AND t1.QuestionID = ".$aQuestionID;
		$qOpts = array("filter" => $sFilter);
		$sql = BITAMQuestion::getQuery($qOpts);
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS, $aGetValues);
		}
		
		return $anInstance;
	}

	//@JAPR 2014-07-31: Agregado el Responsive Design
	/* Carga las propiedades específicas del Responsive Design basado en los parámetros, agregándolas a las instancias especificadas en la
	colección o array proporcionado, de tal forma que este método se puede invocar bajo demanda sólo en los casos donde realmente se necesiten
	estas propiedades sin tener que forzar a que el NewInstanceFromRS se sature con queries y asignaciones que no van a ser utilizadas
	El parámetro $anArrayOfObjects se asumirá que es un array ya indexado por el QuestionID, sin embargo si se trata de una colección
	de preguntas, entonces primero se procede a crear el array interno para facilitar el acceso a las instancias
	//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//@JAPRDescontinuada en V6
	*/
	static function GetResponsiveDesignProps($aRepository, $anArrayOfObjects, $aSurveyID = null, $anArrayOfObjectIDs = null, $aSectionID = null, $anObjectType = null) {
		return true;
		global $gblShowErrorSurvey;
		
		$aSurveyID = (int) $aSurveyID;
		$aSectionID = (int) $aSectionID;
		if (is_null($anArrayOfObjectIDs) || !is_array($anArrayOfObjectIDs)) {
			$anArrayOfObjectIDs = array();
		}
		if (is_null($anObjectType)) {
			$anObjectType = -1;
		}
		
		//Asigna el array de preguntas indexado por el ID
		$arrObjects = $anArrayOfObjects;
		if (!is_array($anArrayOfObjects)) {
			$arrObjects = array();
			foreach ($anArrayOfObjects->Collection as $objObject) {
				$arrObjects[$objObject->QuestionID] = $objObject;
			}
		}
		
		//Si no se hubiera especificado ningún filtro, utiliza el array de instancias para obtener las llaves y cargar sólo los objetos que
		//fueron recibidos, de esta forma se puede sólo recibir el array de instancias sin especificar mas parámetros y funcionará bien
		if ($aSurveyID <= 0 && $aSectionID <= 0 && count($anArrayOfObjectIDs) == 0 && $anObjectType <= -1) {
			$anArrayOfObjectIDs = array_keys($arrObjects);
		}
		
		//Genera el filtro basado en los parámetros especificados
		$where = "";
		if ($aSurveyID > 0) {
			$where .= " AND A.SurveyID = {$aSurveyID}";
		}
		if ($aSectionID > 0) {
			$where .= " AND B.SectionID = {$aSectionID}";
		}
		
		if ($anObjectType >= qtpOpen) {
			$where .= " AND B.QTypeID = {$anObjectType}";
		}
		
		$filter = '';
		if (!is_null($anArrayOfObjectIDs))
		{
			switch (count($anArrayOfObjectIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND A.ObjectID = ".((int)$anArrayOfObjectIDs[0]);
					break;
				default:
					foreach ($anArrayOfObjectIDs as $anObjectID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $anObjectID; 
					}
					if ($filter != "")
					{
						$filter = " AND A.ObjectID IN (".$filter.")";
					}
					break;
			}
		}
		$where .= $filter;
		//Conchita agregado el SketchImage
		$strAdditionalFields = '';
		if (getMDVersion() >= esvSketchImage) {
			$strAdditionalFields .= ', A.SketchImage AS QuestionImageSketch';
		}
		
		if (getMDVersion() >= esvEditorHTML)
		{
			$strAdditionalFields .= ", A.QuestionMessageDes";
		}
		
		$sql = "SELECT B.SurveyID, B.QuestionID, A.DeviceID, A.FormattedText AS QuestionMessage, A.ImageText AS QuestionImageText $strAdditionalFields FROM SI_SV_SurveyHTML A 
				INNER JOIN SI_SV_Question B ON A.ObjectID = B.QuestionID 
			WHERE A.ObjectType = ".otyQuestion.$where;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyHTML, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyHTML, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			$intObjectID = (int) @$aRS->fields["questionid"];
			$objObject = @$arrObjects[$intObjectID];
			if (!is_null($objObject)) {
				$intDeviceID = (int) @$aRS->fields["deviceid"];
				$objObject->ResponsiveDesignProps[$intDeviceID] = 1;
				
				$strProperty = "QuestionMessage";
				$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
				$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue);
				$strProperty = "QuestionImageText";
				$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
				$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue);
				
				//Conchita agregado QuestionImageSketch
				$strProperty = "QuestionImageSketch";
				$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
				$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue);
				
				if (getMDVersion() >= esvEditorHTML)
				{
					$strProperty = "QuestionMessage";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)."des"];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue, 1);

					$strProperty = "QuestionMessage";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue, 1, 1);
				}
				
			}
			
			$aRS->MoveNext();
		}
	}
	
	/* Asigna dinámicamente el valor de la propiedad especificada ajustada al tipo de dispositivo indicado en el parámetro $aDeviceName, 
	si este es numérico entonces se convierte a su equivalente en String para completar el nombre de propiedad
	*/
	public function setResponsiveDesignProperty($aPropertyName, $aDeviceName, $aValue, $editor=0, $html=0) {
		if (is_numeric($aDeviceName)) {
			$aDeviceName = getDeviceNameFromID($aDeviceName);
		}
		
		$strPropName = trim($aPropertyName.$aDeviceName);
		
		if ($editor==1)
		{
			$strPropName = $strPropName."Des";
			
			if($html==1)
			{
				$strPropName = $strPropName."HTML";
			}
		}
				
		if ($strPropName != '') {
			@$this->$strPropName = $aValue;
		}
	}
	
	/* Obtiene dinámicamente el valor de la propiedad especificada ajustada al tipo de dispositivo indicado en el parámetro $aDeviceName, 
	si este es numérico entonces se convierte a su equivalente en String para completar el nombre de propiedad
	*/
	public function getResponsiveDesignProperty($aPropertyName, $aDeviceName, $bDefaultIfEmpty = false, $editor=0) {
		if (is_numeric($aDeviceName)) {
			$aDeviceName = getDeviceNameFromID($aDeviceName);
		}
		
		$aValue = null;
		$strPropName = trim($aPropertyName.$aDeviceName);
		if ($strPropName != '') {
			
			if ($editor==1)
			{
				$strPropName = $strPropName."Des";		
			}			
			
			$aValue = @$this->$strPropName;
			
			//Si no se encuentra configurado nada específico para el tipo de dispositivo y no se trata del dispositivo default, entonces
			//extrae el valor default directo 
			if (trim($aValue) == '' && $aDeviceName != '' && $bDefaultIfEmpty) {
				$aPropertyName = trim($aPropertyName);
				if ($editor==1)
				{
					$aPropertyName = $aPropertyName."Des";
				}
				$aValue = @$this->$aPropertyName;
			}
		}
		
		return $aValue;
	}
	//@JAPR
	
	static function NewInstanceFromRS($aRepository, $aRS, $aGetValues = false)
	{
		$aSurveyID = (int)$aRS->fields["surveyid"];
		$aSectionID = (int)$aRS->fields["sectionid"];
		
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository, $aSectionID);
		//@JAPR
		$anInstance->SurveyName = $aRS->fields["surveyname"];
		
		$anInstance->QuestionID = (int)$aRS->fields["questionid"];
		$anInstance->QuestionNumber = (int)$aRS->fields["questionnumber"];
		$anInstance->QuestionText = $aRS->fields["questiontext"];
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$anInstance->QuestionTextHTML = $aRS->fields["questiontexthtml"];
			//Si el texto formateado no viene asignado, se usará por default el texto normal (compatibilidad hacia atrás)
			if (trim($anInstance->QuestionTextHTML) == '') {
				$anInstance->QuestionTextHTML = $anInstance->QuestionText;
			}
		}
		//@JAPR
		$anInstance->DisplayQuestion = $anInstance->QuestionNumber.". ".$anInstance->QuestionText;
		
		$anInstance->QTypeID = (int)$aRS->fields["qtypeid"];
		//@JAPR 2012-12-07: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		$anInstance->EBQTypeID = $anInstance->QTypeID;
		//@JAPR
		$anInstance->OldQTypeID = (int)$aRS->fields["qtypeid"];
		$anInstance->QComment = $aRS->fields["qcomment"];
		$anInstance->QActions = $aRS->fields["qactions"];
		//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
		switch ($anInstance->QTypeID) {
			case qtpSingle:
			case qtpMulti:
			case qtpPhoto:
			case qtpDocument:
			case qtpAudio:
			case qtpVideo:
			case qtpAction:
			case qtpSignature:
			case qtpMessage:
			case qtpMapped:
			case qtpSkipSection:
			case qtpCallList:	
			case qtpCalc:
			//@JAPR 2013-04-12: Agregados nuevos tipos de pregunta Sincronización
			case qtpSync:
			//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
			case qtpGPS:
			//2014-09-05 Conchita Agregado el nuevo tipo sketch (firma con imagen de fondo)
			case qtpSketch:
			//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			case qtpSketchPlus:
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			case qtpPassword:
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			case qtpSection:
			//@AAL 06/05/2015: Agregado el tipo de pregunta Codigo de barra.
			case qtpBarCode:
			//OMMC 2015-11-30: Agregado el tipo de pregunta OCR
			case qtpOCR:
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			case qtpExit:
			//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
			case qtpUpdateDest:
			//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
			case qtpMyLocation:
				$anInstance->GQTypeID = $anInstance->QTypeID;
				break;
				
			default:		//El resto de los tipos de preguntas que no son específicos, tienen que ser variantes de Open
				$anInstance->GQTypeID = qtpOpen;
				break;
		}
		
		//@JAPR 2016-06-14: Modificado el comportamiento del tamaño del texto de preguntas BarCode para que sea como las alfanuméricas (#TEI1OO)
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		$anInstance->LastModAdminVersion = (float) @$aRS->fields["lastversion"];
		//@JAPR 2014-09-26: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
		$anInstance->LastModDateID = (string) @$aRS->fields["lastdateid"];
		$anInstance->LastModUserID = (int) @$aRS->fields["lastuserid"];
		$anInstance->CreationAdminVersion = (float) @$aRS->fields["creationversion"];
		$anInstance->CreationDateID = (string) @$aRS->fields["creationdateid"];
		$anInstance->CreationUserID = (int) @$aRS->fields["creationuserid"];
		//Conchita 2013-04-29 agregado calllist type
		//$anInstance->GQTypeID = ($anInstance->QTypeID == qtpSingle || $anInstance->QTypeID == qtpMulti)?$anInstance->QTypeID:qtpOpen;
		$anInstance->QDataType = ($anInstance->QTypeID == qtpSingle || $anInstance->QTypeID == qtpCallList || $anInstance->QTypeID == qtpMulti)?dtpString:$anInstance->QTypeID;
		$anInstance->QDisplayMode = (int)$aRS->fields["qdisplaymode"];
		//@JAPR 2012-04-30: Agregada la opción para configurar el estilo visual del componente (Campo QComponentStyleID)
		$anInstance->QComponentStyleID = (int) $aRS->fields["qcomponentstyleid"];
		//@JAPR 2011-06-29: Agregada la longitud de los campos Alfanumericos
		$anInstance->QLength = (int)$aRS->fields["qlength"];
		//@JAPR 2016-06-14: Modificado el comportamiento del tamaño del texto de preguntas BarCode para que sea como las alfanuméricas (#TEI1OO)
		if ($anInstance->QLength <= 0) {
			switch ($anInstance->QTypeID) {
				case qtpOpenAlpha:
					$anInstance->QLength = DEFAULT_ALPHA_LENGTH;
					break;
				case qtpBarCode:
					//Las preguntas código de barra, si bien se comportarán como alfanuméricas, hasta antes de esta actualización tenían un default diferente y de hecho no tenían
					//asignada una longitud, así que no se pondrá un default para entender que en ese caso deberán usar el default original. Lo mismo aplica para todas las preguntas
					//que eventualmente cambien el tamaño default de su campo, sólo a partir de la versión esvNewQuestionLengths se asignará esta propiedad
					if ($anInstance->CreationAdminVersion >= esvNewQuestionLengths) {
						$anInstance->QLength = DEFAULT_ALPHA_LENGTH;
					}
					break;
				case qtpOpenDate:
				case qtpOpenTime:
					if ($anInstance->CreationAdminVersion >= esvNewQuestionLengths) {
						$anInstance->QLength = DEFAULT_DATETIME_LENGTH;
					}
					break;
				case qtpOpenNumeric:
				case qtpCalc:
				case qtpCallList:
				case qtpGPS:
				case qtpMyLocation:
					if ($anInstance->CreationAdminVersion >= esvNewQuestionLengths) {
						$anInstance->QLength = DEFAULT_GENERICQ_LENGTH;
					}
					break;
				case qtpSingle:
				case qtpMulti:
				case qtpAction:
					if ($anInstance->CreationAdminVersion >= esvNewQuestionLengths) {
						$anInstance->QLength = DEFAULT_SCHMCH_LENGTH;
					}
					break;
			}
		}
		//@JAPR 2015-05-19: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
		//estaban limitadas a 255 caracteres (#BA7866)
		$anInstance->OldQLength = $anInstance->QLength;
		//@JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
		$anInstance->MaxQLength = (int) @$aRS->fields["maxqlength"];
		if ($anInstance->MaxQLength <= DEFAULT_ALPHA_LENGTH) {
			//El mínimo con el que se creaban estas preguntas en v6 era este, así que MaxQLength nunca podría ser menor que tal valor
			$anInstance->MaxQLength = DEFAULT_ALPHA_LENGTH;
		}
		//@JAPR
		
		$anInstance->TextDisplayStyle = (int)$aRS->fields["textdisplaystyle"];
		
		$anInstance->MinValue = $aRS->fields["minvalue"];
		$anInstance->MaxValue = $aRS->fields["maxvalue"];
		$anInstance->MinDate = $aRS->fields["mindate"];
		$anInstance->MaxDate = $aRS->fields["maxdate"];
		$anInstance->StrSelectOptions = $aRS->fields["optionstext"];
		$anInstance->IndDimID = (int)$aRS->fields["inddimid"];
		$anInstance->ImgDimID = (int)$aRS->fields["imgdimid"];
		$anInstance->DocDimID = (int)$aRS->fields["docdimid"];
		//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
		$anInstance->StrSelectOptionsCol = $aRS->fields["optionstextcol"];
		//@JAPR
		
		$anInstance->OneChoicePer = (int)$aRS->fields["onechoiceper"];
		
		$anInstance->HasValidation = (int)$aRS->fields["hasvalidation"];
		//@JAPR 2012-03-28: Movidos porque se necesitaban para los valores máximo y mínimo
		$anInstance->MCInputType = (int)$aRS->fields["mcinputtype"];
		$anInstance->MCInputTypeOld = (int)$aRS->fields["mcinputtype"];
		//@JAPR
		
		//if($anInstance->HasValidation==1)
		//{		gQTypeID == <qtpMulti && qDisplayMode == <dspVertical && mcinputtype == <mpcNumeric
			if($anInstance->QTypeID==qtpOpenNumeric || ($anInstance->QTypeID == qtpMulti && $anInstance->QDisplayMode == dspVertical && $anInstance->MCInputType == mpcNumeric))
			{
				$anInstance->MinValue = $aRS->fields["minvalue"];
				$anInstance->MaxValue = $aRS->fields["maxvalue"];
			}
			else if($anInstance->QTypeID==qtpOpenDate)
			{
				$anInstance->MinDate = $aRS->fields["mindate"];
				$anInstance->MaxDate = $aRS->fields["maxdate"];
			}
		//}
		
		//Establecer el nombre del campo de la pregunta dentro de la tabla de la encuesta
		if($anInstance->QTypeID==qtpOpenNumeric)
		{
			$anInstance->SurveyField = "dim_".$anInstance->QuestionID;

		}
		else 
		{
			$anInstance->SurveyField = "dim_".$anInstance->QuestionID;
		}
		
		$anInstance->AttributeName = $aRS->fields["attributename"];
		$anInstance->AttributeNameOld = $aRS->fields["attributename"];
		$anInstance->IsFieldSurveyID = (int)$aRS->fields["isfieldsurveyid"];
		$anInstance->IsIndicator = (int)$aRS->fields["isindicator"];
		$anInstance->IsIndicatorBack = (int)$aRS->fields["isindicator"];
		$anInstance->IndicatorID = (int)$aRS->fields["indicatorid"];
		$anInstance->OldIndicatorID = (int)$aRS->fields["indicatorid"];
		
		$anInstance->IsReqQuestion = (int)$aRS->fields["isreqquestion"];
		$anInstance->HasReqComment = (int)$aRS->fields["hasreqcomment"];
		$anInstance->HasReqCommentOld = $anInstance->HasReqComment;
		$anInstance->HasReqPhoto = (int)$aRS->fields["hasreqphoto"];
		$anInstance->HasReqDocument = (int)$aRS->fields["hasreqdocument"];
		//@JAPR 2012-05-14: Agregados nuevos tipos de pregunta Foto y Action
		//@JAPR 2012-05-22: Agregadas las preguntas tipo Firma Conchita 2014-09-05 agregadas las preguntas tipo sketch
		//@JAPR 2015-09-09: Corregido un bug, las preguntas tipo BarCode también tienen foto opcional
		//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if ($anInstance->QTypeID == qtpPhoto || $anInstance->QTypeID == qtpSignature || $anInstance->QTypeID == qtpSketch || $anInstance->QTypeID == qtpSketchPlus || 
				$anInstance->QTypeID == qtpBarCode || $anInstance->QTypeID == qtpOCR) {
			$anInstance->HasReqPhoto = 2;
		}
		//Conchita agregado el isinvisible 2014-12-01
		if (getMDVersion() >= esvIsInvisible) {
			$anInstance->IsInvisible = (int)$aRS->fields["isinvisible"];
		}
		//@JAPR 2012-06-08: Ahora se permitirá cambiar la opción de usar Foto durante la edición de preguntas
		$anInstance->HasReqPhotoOld = $anInstance->HasReqPhoto;
		//@JAPR
		$anInstance->HasReqDocumentOld = $anInstance->HasReqDocument;		
		$anInstance->HasActions = (int)$aRS->fields["hasactions"];
		//@JAPR 2012-03-28: Movidos mas arriba porque se necesitaban para los valores máximo y mínimo
		//$anInstance->MCInputType = (int)$aRS->fields["mcinputtype"];
		//$anInstance->MCInputTypeOld = (int)$aRS->fields["mcinputtype"];
		//@JAPR
		$anInstance->UseCatalog = (int) @$aRS->fields["usecatalog"];
		$anInstance->UseCatalogOld = (int) @$aRS->fields["usecatalog"];
		$anInstance->CatalogID = (int) @$aRS->fields["catalogid"];
		$anInstance->CatalogIDOld = (int) @$aRS->fields["catalogid"];
		$anInstance->CatMemberID = (int) @$aRS->fields["catmemberid"];
		$anInstance->CatMemberIDOld = (int) @$aRS->fields["catmemberid"];
		//@JAPR 2015-07-16: Agregada la integración con DataSources
		$anInstance->DataSourceID = (int) @$aRS->fields["datasourceid"];
		$anInstance->DataSourceIDOld = $anInstance->DataSourceID;
		$anInstance->DataSourceMemberID = (int) @$aRS->fields["datasourcememberid"];
		$anInstance->DataMemberLatitudeID = (int) @$aRS->fields["datamemberlatitudeid"];
		$anInstance->DataMemberLongitudeID = (int) @$aRS->fields["datamemberlongitudeid"];
		//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
		$anInstance->DataSourceFilter = (string) @$aRS->fields["datasourcefilter"];
		//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
		$anInstance->CatMemberImageID = (int)@$aRS->fields["catmemberimageid"];
		$anInstance->DataMemberImageID = (int) @$aRS->fields["datamemberimageid"];
		//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		$anInstance->CatMemberOrderID = (int)@$aRS->fields["catmemberorderid"];
		$anInstance->DataMemberOrderID = (int) @$aRS->fields["datamemberorderid"];
		//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
		$anInstance->DataDestinationID = (int) @$aRS->fields["datadestinationid"];
		//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
		$anInstance->DataMemberOrderDir = (int) @$aRS->fields["datamemberorderdir"];
		$anInstance->CustomLayout = (string) @$aRS->fields["customlayout"];
		//@JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
		$anInstance->UploadMediaFiles = (int) @$aRS->fields["uploadmediafiles"];
		//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
		$anInstance->AutoSelectEmptyOpt = (int) @$aRS->fields["autoselectemptyopt"];
		//@JAPR
		
		$anInstance->CatMemberLatitudeID = (int)@$aRS->fields["catmemberlatitudeid"];
		$anInstance->CatMemberLatitudeIDOld = (int)@$aRS->fields["catmemberlatitudeid"];
		$anInstance->CatMemberLongitudeID = (int)@$aRS->fields["catmemberlongitudeid"];
		$anInstance->CatMemberLongitudeIDOld = (int)@$aRS->fields["catmemberlongitudeid"];
		$anInstance->UseMap = (int)@$aRS->fields["usemap"];
		$anInstance->MapRadio = (int)@$aRS->fields["mapradio"];
		
		$anInstance->SectionNumber = (int)$aRS->fields["sectionnumber"];
		$anInstance->IsMultiDimension = (int)$aRS->fields["ismultidimension"];
		$anInstance->IsMultiDimensionOld = (int)$aRS->fields["ismultidimension"];
		$anInstance->UseCatToFillMC = (int)$aRS->fields["usecattofillmc"];
		$anInstance->UseCatToFillMCOld = (int)$aRS->fields["usecattofillmc"];
		
		$anInstance->FatherQuestionID = (int)$aRS->fields["fatherquestionid"];
		$anInstance->ChildQuestionID = (int)$aRS->fields["childquestionid"];
		$anInstance->OutsideOfSection = (int)$aRS->fields["outsideofsection"];
		$anInstance->GoToQuestion = (int)$aRS->fields["gotosection"]; //Conchita 18-oct-2011
		$anInstance->IsIndicatorMC = (int)$aRS->fields["isindicatormc"];
		$anInstance->IsIndicatorMCBack = (int)$aRS->fields["isindicatormc"];
		
		$anInstance->UseCategoryDim = (int)$aRS->fields["usecategorydim"];
		$anInstance->CategoryDimName = $aRS->fields["categorydimname"];
		$anInstance->ElementDimName = $aRS->fields["elementdimname"];
		$anInstance->ElementIndName = $aRS->fields["elementindname"];
		$anInstance->CategoryDimID = (int)$aRS->fields["categorydimid"];
		$anInstance->ElementDimID = (int)$aRS->fields["elementdimid"];
		$anInstance->ElementIndID = (int)$aRS->fields["elementindid"];
		
		$anInstance->UseCategoryDimChoice = (int)$aRS->fields["categorydimid"];

		//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales
		$anInstance->AllowAdditionalAnswers = (int) $aRS->fields["allowadditionalanswers"];
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		$anInstance->OtherLabel = (string) @$aRS->fields["otherlabel"];
		$anInstance->CommentLabel = (string) @$aRS->fields["commentlabel"];
		//@JAPR 2012-05-15: Agregadas las preguntas tipo Action (Campos ResponsibleID, CategoryID, DaysDueDate)
		$anInstance->ResponsibleID = (int) $aRS->fields["responsibleid"];
		$anInstance->CategoryID = (int) $aRS->fields["categoryid"];
		$anInstance->DaysDueDate = abs((int) $aRS->fields["daysduedate"]);
		//@JAPR 2012-05-29: Agregada la opción para indicar el número de decimales de las preguntas numéricas (campo DecimalPlaces)
		//@JAPR 2012-05-29: Agregado el soporte para creación de preguntas (single y multi) cuyas respuestas dependan de las seleccionadas de una pregunta multi previa (campo SourceQuestionID)
		$anInstance->DecimalPlaces = (int) $aRS->fields["decimalplaces"];
		$anInstance->SourceQuestionID = (int) $aRS->fields["sourcequestionid"];
		$anInstance->SourceQuestionIDOld = $anInstance->SourceQuestionID;
		//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		$anInstance->SharedQuestionID = (int) @$aRS->fields["sharedquestionid"];
		$anInstance->SharedQuestionIDOld = $anInstance->SharedQuestionID;
		//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		$anInstance->RegisterGPSAt = (int) @$aRS->fields["registergpsat"];
		$anInstance->LatitudeAttribID = (int) @$aRS->fields["latitudeattribid"];
		$anInstance->LongitudeAttribID = (int) @$aRS->fields["longitudeattribid"];
		$anInstance->AccuracyAttribID = (int) @$aRS->fields["accuracyattribid"];
		//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas (campo FormatMask)
		$anInstance->FormatMask = (string) @$aRS->fields["formatmask"];
		$anInstance->getFormatID();
		//@JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
		$anInstance->InputWidth = (int) @$aRS->fields["inputwidth"];
		$anInstance->PictLayOut = (int) @$aRS->fields["pictlayout"];
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		$anInstance->IsSelector = ($anInstance->SectionType == sectInline && ((int) @$aRS->fields["selectorquestionid"]) == $anInstance->QuestionID);
		//@JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice
		$anInstance->EnableCheckBox = (int) @$aRS->fields["enablecheckbox"];
		$anInstance->SwitchBehaviour = (int) @$aRS->fields["switchbehaviour"];
		//@JAPR 2014-09-29: Agregado el ancho de columnas en secciones Inline o el Summary de maestro-detalle
		//@JAPR 2016-06-08: Modificada la propiedad para que ahora pueda ser un String (#WFLVTR)
		if (getMDVersion() >= esvColumnWidthString) {
			$anInstance->ColumnWidth = (string) @$aRS->fields["columnwidth"];
		}
		else {
			$anInstance->ColumnWidth = (int) @$aRS->fields["columnwidth"];
		}
		//@JAPR
		
		$anInstance->MaxChecked = (int) @$aRS->fields["maxchecked"];
		$anInstance->QuestionImageText = (string) @$aRS->fields["questionimagetext"];
		$anInstance->QuestionMessage = (string) @$aRS->fields["questionmessage"];
		$anInstance->QuestionImageSketch = (string) @$aRS->fields["sketchimage"]; //Conchita agregado el questionimagesketch
		$anInstance->DateFormatMask = (string) @$aRS->fields["dateformatmask"];
		$anInstance->getDateFormatID();
		
		$anInstance->DefaultValue = (string) @$aRS->fields["defaultvalue"];
		$anInstance->HasQuestionDep = (int) @$aRS->fields["hasquestiondep"];
		
		//if($anInstance->HasQuestionDep) {
		$anInstance->getQuestionsToEval();
		$anInstance->getFormulaChildren();
		$anInstance->getShowConditionChildren();
		//}
		
		$anInstance->ExcludeSelected = (int) @$aRS->fields["excludeselected"];
		$anInstance->SourceSimpleQuestionID = (int) $aRS->fields["sourcesimplequestionid"];
		$anInstance->SourceSimpleQuestionIDOld = $anInstance->SourceSimpleQuestionID;
		
		$anInstance->SummaryTotal = (int) @$aRS->fields["summarytotal"];
		//@JAPR 2012-10-19: Agregadas las preguntas marcadas como Read-Only
		//@JAPR 2012-12-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		if (getMDVersion() >= esveFormsV41Func) {
			$anInstance->ReadOnly = (int) @$aRS->fields["readonly"];
			$anInstance->Formula = (string) @$aRS->fields["formula"];
			$anInstance->HasReqDocument = (int) @$aRS->fields["hasreqdocument"];
			$anInstance->DocDimID = (int) @$aRS->fields["docdimid"];
			$anInstance->ShowCondition = (string) @$aRS->fields["showcondition"];
		}
		//Conchita agregados el width y height para las tipo sketch checar con juan
		if (getMDVersion() >= esvSketch) {
			$anInstance->canvasWidth = (string) @$aRS->fields["canvaswidth"];
			$anInstance->canvasHeight = (string) @$aRS->fields["canvasheight"];
		}

		$anInstance->eBavelFieldID = (int) @$aRS->fields["ebavelfieldid"];
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		if (getMDVersion() >= esvExtendedActionsData) {
			$anInstance->ActionText = (string) @$aRS->fields["actiontext"];
			$anInstance->ActionTitle = (string) @$aRS->fields["actiontitle"];
		}
		//@JAPR 2013-02-21: Agregada la opción para ocultar los textos de opciones de respuesta si tienen imagen asociada
		if (getMDVersion() >= esvCatalogDissociation) {
			$anInstance->ShowImageAndText = (int) @$aRS->fields["showimageandtext"];
		}
		if (getMDVersion() >= esvShowInSummary) {
			$anInstance->ShowInSummary = (int) @$aRS->fields["showinsummary"];
		}
		//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
		$anInstance->eBavelActionFormID = (int) @$aRS->fields["ebavelactionformid"];
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		$anInstance->SourceSectionID = (int) @$aRS->fields["sourcesectionid"];
		//@JAPR 2014-10-22: Agregada la opción para redibujar las preguntas tipo mensaje automáticamente
		$anInstance->AutoRedraw = (int) @$aRS->fields["autoredraw"];

		//Sólo leerá los posibles valores de mapeo si se está solicitando, lo cual aplica básicamente para la edición, el resto de las veces
		//se debería de pedir explícitamente a la instancia que lea esta información exclusivamente cuando se va a utilizar
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			if (getMDVersion() >= esveBavelSuppActions123) {
				if ($anInstance->eBavelActionFormID > 0 && $aGetValues) {
					$anInstance->readeBavelActionFieldsMapping();
				}
			}
		}
		
		//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
		//Ahora el tipo de llenado del componente será un campo mas en la metadata
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$anInstance->ValuesSourceType = @$aRS->fields["ValuesSourceType"];
			//Si no está asignada la propiedad, se asume que es una versión vieja de la definición así que se ajusta dependiendo de otras configuraciones
			if (is_null($anInstance->ValuesSourceType)) {
				switch ($anInstance->QTypeID) {
					case qtpSingle:
						$anInstance->ValuesSourceType = $anInstance->typeOfFillingSC();
						break;
					case qtpMulti:
						$anInstance->ValuesSourceType = $anInstance->typeOfFillingMC();
						break;
					default:
						$anInstance->ValuesSourceType = tofFixedAnswers;
						break;
				}
			}
			
			//Dependiendo del tipo de llenado, se desactivan algunas otras configuraciones
			switch ($anInstance->ValuesSourceType) {
				case tofFixedAnswers:
					break;
				case tofMCHQuestion:
					break;
				case tofSharedAnswers:
					break;
				default:
				//case tofFixedAnswers:
					break;
			}
			
			//JAPR 2016-12-21: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
			$anInstance->ValuesSourceTypeOld = $anInstance->ValuesSourceType;
			//JAPR
		}
		
		//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//@AAL 27/05/2015 Agregado para que solo se ejecute la acción en EformsV6 y siguientes (se quitan las dimensiones)
		if ($anInstance->QTypeID == qtpSingle && $anInstance->UseCatalog && $anInstance->CreationAdminVersion < esveFormsv6) {
			$anInstance->SurveyCatField = "cat_".$anInstance->QuestionID;
			//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ((int) @$aRS->fields["dissociatecatdimens"] && $anInstance->CatMemberID > 0) {
				$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $anInstance->CatalogID);
				if (!is_null($objCatalogMembersColl)) {
					foreach ($objCatalogMembersColl as $objCatalogMember) {
						if ($objCatalogMember->MemberID == $anInstance->CatMemberID) {
							$anInstance->CatIndDimID = $objCatalogMember->IndDimID;
							break;
						}
					}
				}
			}
		}
		//@JAPR
		
		//Si el tipo de pregunta es de seleccion simple o multiple se
		//tienen q obtener las opciones almacenadas
		//Agregado el tipo calllist conchita 2013-04-29
		//se agrego la tipo callList
		//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
		//@JAPR 2019-02-06: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if ($anInstance->QTypeID == qtpSingle || $anInstance->QTypeID == qtpMulti || $anInstance->QTypeID == qtpCallList || $anInstance->QTypeID == qtpUpdateDest || $anInstance->QTypeID == qtpSketchPlus) {
			$anInstance->getSelectOptions();
		}
		//@JAPR 2015-07-15: Rediseñadas las preguntas y secciones de catálogo para permitir seleccionar los atributos a usar y el orden deseado (uso de DataSources)
		if (($anInstance->QTypeID == qtpSingle || $anInstance->QTypeID == qtpMulti) && $anInstance->ValuesSourceType == tofCatalog && $anInstance->CatalogID > 0) {
			//Si es una pregunta que utiliza un catálogo, debe cargar los atributos utilizados por ella en el orden que están definidos
			$anInstance->getCatMembersIDs();
		}
		//@JAPR
		//OMMC 2015-11-30: Si la pregunta es de tipo OCR, debe cargar la relación de preguntas abiertas.
		if($anInstance->QTypeID == qtpOCR && getMDVersion() >= esvOCRQuestionFields){
			$anInstance->getOCRQuestionIDs();
			$anInstance->SavePhoto = (int) @$aRS->fields["savephoto"];
			$anInstance->canvasHeight = (string) @$aRS->fields["canvasheight"];
		}
		//OMMC

		//OMMC 2016-01-18: Si la pregunta es de tipo mensaje debe cargar este valor.
		if($anInstance->QTypeID == qtpMessage && getMDVersion() >= esvHTMLEditorState){
			$anInstance->HTMLEditorState = (int) @$aRS->fields["HTMLEditorState"];
		}

		//Sep-2014		
		$anInstance->EditorType = (int) @$aRS->fields["editortype"];
		$anInstance->QuestionMessageDes = trim(@$aRS->fields["questionmessagedes"]);
		$anInstance->QuestionMessageDesHTML = trim(@$aRS->fields["questionmessage"]);
		//@JAPR 2016-01-21: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
		$anInstance->ShowTotals = (int) @$aRS->fields["showtotals"];
		$anInstance->TotalsFunction = strtoupper(trim((string) @$aRS->fields["totalsfunction"]));
		if (stripos("|SUM|AVG|MAX|MIN|COUNT|", "|".$anInstance->TotalsFunction."|") === false) {
			$anInstance->TotalsFunction = "SUM";
		}
		
		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
		$anInstance->setTableAndFieldsProperties();
		//@JAPR

		//OMMC 2016-02-24: Agregado para permitir acceso a la galería
		if(getMDVersion() >= esvAllowGallery) {
			$anInstance->AllowGallery = (int) @$aRS->fields["allowgallery"];
		}
		
		//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
		if ($anInstance->QTypeID == qtpUpdateDest) {
			//Sólo puede haber dos opciones de respuesta para este tipo de pregunta, por el momento sólo se requieren estos valores, así que se actualizan como propiedad de la pregunta
			//directamente, aunque están disponibles en la colección de opciones de respuesta
			foreach ($anInstance->SelectOptions as $optionKey => $optionValue) {
				$intNextSectionID = (int) @$anInstance->NextQuestion[$optionKey];
				switch (trim(strtolower($optionValue))) {
					case "true":
						$anInstance->NextSectionIDIfTrue = $intNextSectionID;
						break;
					default:
						$anInstance->NextSectionIDIfFalse = $intNextSectionID;
						break;
				}
			}
		}
		//@JAPR

		//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
		if (getMDVersion() >= esvNoFlow) {
			$anInstance->NoFlow = (int) @$aRS->fields["noflow"];
		}

		//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
		if (getMDVersion() >= esvImageDisplay) {
			$anInstance->imageWidth = (string) @$aRS->fields["imagewidth"];
			$anInstance->imageHeight = (string) @$aRS->fields["imageheight"];
		}
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		$anInstance->SourceID = (int) @$aRS->fields["sourceid"];
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$anInstance->IsCoordX = ($anInstance->SectionType == sectMasterDet && ((int) @$aRS->fields["xposquestionid"]) == $anInstance->QuestionID);
		$anInstance->IsCoordY = ($anInstance->SectionType == sectMasterDet && ((int) @$aRS->fields["yposquestionid"]) == $anInstance->QuestionID);
		$anInstance->IsItemName = ($anInstance->SectionType == sectMasterDet && ((int) @$aRS->fields["itemnamequestionid"]) == $anInstance->QuestionID);
		//@JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
		$anInstance->RecalculateDependencies = (int) @$aRS->fields["recalculatedependencies"];
		//@JAPR
		
		//OMMC 2016-09-01: Modificado el comportamiento default de la pregunta llamada
		if (getMDVersion() >= esvPhoneCall) {
			$anInstance->DisablePhoneNum = (int) @$aRS->fields["disablephonenum"];
			$anInstance->DisableSync = (int) @$aRS->fields["disablesync"];
		}

		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		if (getMDVersion() >= esvQuestionAR) {
			$anInstance->DataMemberZipFileID = (int) @$aRS->fields["datamemberzipfileid"];
			$anInstance->CatMemberZipFileID = (int) @$aRS->fields["catmemberzipfileid"];
			$anInstance->DataMemberVersionID = (int) @$aRS->fields["datamemberversionid"];
			$anInstance->CatMemberVersionID = (int) @$aRS->fields["catmemberversionid"];
			$anInstance->DataMemberPlatformID = (int) @$aRS->fields["datamemberplatformid"];
			$anInstance->CatMemberPlatformID = (int) @$aRS->fields["catmemberplatformid"];
			$anInstance->DataMemberTitleID = (int) @$aRS->fields["datamembertitleid"];
			$anInstance->CatMemberTitleID = (int) @$aRS->fields["datamembertitleid"];
		}
		return $anInstance;
	}

	//OMMC 2015-11-30: 
	/* Obtiene la colección ordenada de IDs de preguntas abiertas seleccionadas que serán usadas por la pregunta OCR, la relación es almacenada en una tabla aparte.
	*/
	function getOCRQuestionIDs() {
		$this->QuestionList = array();

		$sql = "SELECT A.OpenQuestionID 
			FROM SI_SV_OCRQuestion A 
			WHERE A.OCRQuestionID = {$this->QuestionID} 
			ORDER BY A.QuestionOrder";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while (!$aRS->EOF) {
				$intOpenQuestionID = (int) @$aRS->fields["openquestionid"];
				if ($intOpenQuestionID > 0) {
					$this->QuestionList[] = $intOpenQuestionID;
				}
				
				$aRS->MoveNext();
			}
		}
	}

	//@JAPR 2015-07-15: Rediseñadas las preguntas y secciones de catálogo para permitir seleccionar los atributos a usar y el orden deseado (uso de DataSources)
	/* Obtiene la colección ordenada de IDs de atributos de catálogo y simultáneamente la de los datasources correspondientes que este objeto tiene asociados
	*/
	function getCatMembersIDs() {
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		if ( BITAMGlobalFormsInstance::IsSurveyFullyLoadedFromDB($this->SurveyID) ) {
			$this->CatMembersList = BITAMGlobalFormsInstance::GetCatMembersListByQuestionWithID($this->QuestionID);
			return;
		}
		//@JAPR
		
		$this->CatMembersList = array();
		
		//@JAPR 2015-07-21: Se removió el join porque regresaba múltiples instancias de CatMember, realmente no se necesitan esos IDs, así que se elimina ese array
		/*
		$sql = "SELECT A.MemberID, B.DataSourceMemberID 
			FROM SI_SV_QuestionMembers A 
				LEFT OUTER JOIN SI_SV_CatalogMember B ON A.MemberID = B.MemberID 
			WHERE A.QuestionID = {$this->QuestionID} 
			ORDER BY A.MemberID";
		*/
		$sql = "SELECT A.MemberID AS DataSourceMemberID 
			FROM SI_SV_QuestionMembers A 
			WHERE A.QuestionID = {$this->QuestionID} 
			ORDER BY A.MemberOrder";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while (!$aRS->EOF) {
				$intDataSourceMemberID = (int) @$aRS->fields["datasourcememberid"];
				if ($intDataSourceMemberID > 0) {
					$this->CatMembersList[] = $intDataSourceMemberID;
				}
				
				$aRS->MoveNext();
			}
		}
	}
	
	/* Graba la colección de atributos asignados al objeto en el mismo orden que se definieron */
	function saveOCRQuestionIDs() {
		$sql = "DELETE FROM SI_SV_OCRQuestion WHERE OCRQuestionID = {$this->QuestionID}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$intOrder = 1;
		foreach ($this->QuestionList as $intOpenQuestionID) {
			if ($intOpenQuestionID > 0) {
				$sql = "INSERT INTO SI_SV_OCRQuestion (OCRQuestionID, OpenQuestionID, QuestionOrder) 
					VALUES ({$this->QuestionID}, {$intOpenQuestionID}, {$intOrder})";
				$this->Repository->DataADOConnection->Execute($sql);
				$intOrder++;
			}
		}
	}
	
	/* Graba la colección de atributos asignados al objeto en el mismo orden que se definieron */
	function saveCatMembersIDs() {
		$sql = "DELETE FROM SI_SV_QuestionMembers WHERE QuestionID = {$this->QuestionID}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$intOrder = 1;
		foreach ($this->CatMembersList as $intDataSourceMemberID) {
			if ($intDataSourceMemberID > 0) {
				$sql = "INSERT INTO SI_SV_QuestionMembers (QuestionID, MemberID, MemberOrder) 
					VALUES ({$this->QuestionID}, {$intDataSourceMemberID}, {$intOrder})";
				$this->Repository->DataADOConnection->Execute($sql);
				$intOrder++;
			}
		}
	}
	
	//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData, se requiere un Join con el DataSource
	/* Graba el filtro dinámico de la forma para el catálogo de esta pregunta
	*/
	function saveDynamicFilter() {
		//@JAPR 2015-09-30: Habilitado el filtro dinámico de preguntas de catálogo para todos los tipos
		//Se removió la condición que permitía sólo a las preguntas tipo GetData aplicar esta funcionalidad
		if ($this->ValuesSourceType != tofCatalog || $this->CatalogID <= 0 /* || $this->QDisplayMode != dspGetData*/) {
			return;
		}
		
		if (trim($this->DataSourceFilter) == '') {
			$sql = "DELETE FROM SI_SV_SurveyFilter WHERE CatalogID = ".$this->CatalogID;
			$this->Repository->DataADOConnection->Execute($sql);
			return;
		}
		
		//Obtiene la referencia al filtro dinámico actual para actualizar si es que hay uno
		$intFilterID = 0;
		$sql = "SELECT FilterID 
			FROM SI_SV_SurveyFilter 
			WHERE SurveyID = {$this->SurveyID} AND CatalogID = {$this->CatalogID}";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$intFilterID = (int) @$aRS->fields["filterid"];
		}
		
		$objSurveyFilter = null;
		if ($intFilterID > 0) {
			$objSurveyFilter = BITAMSurveyFilter::NewInstanceWithID($this->Repository, $intFilterID);
		}
		if (is_null($objSurveyFilter)) {
			$objSurveyFilter = BITAMSurveyFilter::NewInstance($this->Repository, $this->SurveyID);
		}
		
		if (is_null($objSurveyFilter)) {
			return;
		}
		
		$objSurveyFilter->FilterName = $this->QuestionText;
		$objSurveyFilter->CatalogID = $this->CatalogID;
		$objSurveyFilter->FilterText = $this->DataSourceFilter;
		$objSurveyFilter->save();
	}
	
	//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
	/* Asigna las propiedades que contienen los nombres de tablas y campos a utilizar durante el proceso de generación de las tablas de datos o de grabado
	de las capturas realizadas para las formas de v6. Si la forma fuera previa a v4 y la propiedad hubiera existido previamente, entonces le deja el mismo
	nombre que originalmente se utilizaba */
	function setTableAndFieldsProperties() {
		if ($this->CreationAdminVersion >= esveFormsv6) {
			$this->DescKeyFieldFK = "QuestionKey_{$this->QuestionID}";
			$this->ValueField = "QuestionVal_{$this->QuestionID}";
			//$this->KeyField = "QuestionKey";
			$this->DescKeyField = "QuestionDescKey";
			$this->DescField = "QuestionDesc";
			$this->DescFieldAttr = "QuestionDesc_{$this->QuestionID}";
			$this->KeyFieldCommFK = "QuestionCommKey_{$this->QuestionID}";
			$this->KeyFieldComm = "QuestionCommKey";
			$this->DescFieldComm = "QuestionComm";
			$this->KeyFieldDocFK = "QuestionDocKey_{$this->QuestionID}";
			$this->KeyFieldDoc = "QuestionDocKey";
			$this->DescFieldDoc = "QuestionDoc";
			$this->KeyFieldImgFK = "QuestionImgKey_{$this->QuestionID}";
			$this->KeyFieldImg = "QuestionImgKey";
			$this->DescFieldImg = "QuestionImg";
			//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			//Para la pregunta Sketch+ se agregó una columna adicional que contiene la imagen original usada para los trazos, pero no se agregó como propiedad de la instancia sino que sobre
			//la misma propiedad del campo imagen se le concatena el sufijo "Src"
			//$this->DescFieldImg = "QuestionImg"."Src";
			//@JAPR
			$this->KeyFieldPtsFK = "QuestionPtsKey_{$this->QuestionID}";
			$this->KeyFieldPts = "QuestionPtsKey";
			$this->DescFieldPts = "QuestionPts";
			$this->KeyFieldValFK = "QuestionValKey";
			$this->KeyFieldVal = $this->KeyFieldValFK;
			$this->DescFieldVal = "QuestionVal";
			$this->QuestionTable = "SVeForms_{$this->SurveyID}_Question_{$this->QuestionID}";
			$this->QuestionImgTable = "SVeForms_{$this->SurveyID}_QuestionImg_{$this->QuestionID}";
			$this->QuestionCommTable = "SVeForms_{$this->SurveyID}_QuestionComm_{$this->QuestionID}";
			$this->QuestionDocTable = "SVeForms_{$this->SurveyID}_QuestionDoc_{$this->QuestionID}";
			$this->QuestionPtsTable = "SVeForms_{$this->SurveyID}_QuestionPts_{$this->QuestionID}";
			$this->QuestionDetTable = "SVeForms_{$this->SurveyID}_QuestionDet_{$this->QuestionID}";
			$this->QuestionValTable = "SVeForms_{$this->SurveyID}_QuestionVal_{$this->QuestionID}";
			if ($this->SectionType == sectNormal) {
				$this->SectionTable = "SVeForms_{$this->SurveyID}_SectionStd";
			}
			else {
				$this->SectionTable = "SVeForms_{$this->SurveyID}_Section_{$this->SectionID}";
			}
		}
		else {
		}
	}
	
	//@JAPR 2014-02-07: Agrupadas las opciones de llenado de preguntas simple y multiple choice
	//Regresa el tipo de llenado de la pregunta cuando es simple choice (tofFixedAnswers, tofCatalog, tofMCHQuestion, tofSharedAnswers)
	//La validación da prioridad a las preguntas de catálogo, luego a las opciones de Shared o Source Question, y si no hay nada entonces asume que
	//es de tipo fija
	function typeOfFillingSC() {
		$intTypeOfFilling = tofFixedAnswers;
		
		if ($this->UseCatalog && $this->CatalogID > 0) {
			$intTypeOfFilling = tofCatalog;
		}
		elseif ($this->SourceQuestionID > 0) {
			$intTypeOfFilling = tofMCHQuestion;
		}
		elseif ($this->SharedQuestionID > 0) {
			$intTypeOfFilling = tofSharedAnswers;
		}
		
		return $intTypeOfFilling;
	}

	//Regresa el tipo de llenado de la pregunta cuando es multiple choice (tofFixedAnswers, tofMCHQuestion, tofSharedAnswers)
	//La validación da prioridad a las opciones de Shared o Source Question, y si no hay nada entonces asume que es de tipo fija (este tipo de
	//preguntas a la fecha de implementación, explícitamente no podían ser de tipo catálogo)
	function typeOfFillingMC() {
		$intTypeOfFilling = tofFixedAnswers;
		
		if ($this->SourceQuestionID > 0) {
			$intTypeOfFilling = tofMCHQuestion;
		}
		elseif ($this->SharedQuestionID > 0) {
			$intTypeOfFilling = tofSharedAnswers;
		}
		
		return $intTypeOfFilling;
	}
	
	//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
	//Lee el mapeo de campos de eForms hacia eBavel y los almacena como un string de pares NombreCampo=Id que se puede usar en la ventana para su configuración
	function readeBavelActionFieldsMapping() {
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//@JAPRDescontinuada en v6
		return;
		//@JAPR
		if ($this->eBavelActionFormID <= 0 || $this->eBavelActionFieldsDataRead) {
			return;
		}
		
		require_once("questionActionMap.inc.php");
		
		$this->eBavelActionFieldsData = '';
		$this->eBavelActionFieldsDataOld = '';
		$this->QuestionFieldMap = null;
		$objQuestionFieldMap = BITAMQuestionActionMap::NewInstanceWithAnswerID($this->Repository, $this->SurveyID, $this->QuestionID);
		if (is_null($objQuestionFieldMap) || !isset($objQuestionFieldMap->ArrayVariables) || count($objQuestionFieldMap->ArrayVariables) == 0) {
			return;
		}
		$this->QuestionFieldMap = $objQuestionFieldMap;
		
		$strAnd = '';
		$streBavelFieldsData = '';
		foreach($objQuestionFieldMap->ArrayVariables as $inteBavelFieldID => $strVariableName) {
			$streBavelFieldsData .= $strAnd.$strVariableName.'='.((int) @$objQuestionFieldMap->ArrayVariableValues[$inteBavelFieldID]);
			$strAnd = '|';
		}
		
		$this->eBavelActionFieldsData = $streBavelFieldsData;
		$this->eBavelActionFieldsDataOld = $this->eBavelActionFieldsData;
		$this->eBavelActionFieldsDataRead = true;
	}
	
	//Graba el string de pares NombreCampo=Id en la tabla de mapeos de campos de eForms hacia eBavel sólo si hubo cambios
	function saveeBavelActionFieldsMapping() {
		if (trim((string) $this->eBavelActionFieldsData) == trim((string) $this->eBavelActionFieldsDataOld)) {
			return;
		}
		
		require_once("questionActionMap.inc.php");
		
		//El siguiente objeto, esté o no algo grabado en la Metadata, contendrá la lista de los campos configurables por encuesta indexados por
		//el ID, con dicha lista se podrán actualizar los campos prviamente grabados o se agregarán las nuevas configuraciones
		$objQuestionFieldMap = BITAMQuestionActionMap::NewInstanceWithAnswerID($this->Repository, $this->SurveyID, $this->QuestionID);
		if (is_null($objQuestionFieldMap) || !isset($objQuestionFieldMap->ArrayVariables) || count($objQuestionFieldMap->ArrayVariables) == 0) {
			return;
		}
		
		$arrFieldsByName = array_flip($objQuestionFieldMap->ArrayVariables);
		
		//Recorre el String recibido actualizando, insertando o eliminando según el caso los campos recibidos
		$arrFieldsData = explode('|', $this->eBavelActionFieldsData);
		$arrProcessedFields = array();
		
		foreach ($arrFieldsData as $aFieldData) {
			if (trim($aFieldData) == '') {
				continue;
			}
			
			$arrFieldInfo = explode('=', $aFieldData);
			if (count($arrFieldInfo) < 2) {
				continue;
			}
			
			$streBavelFieldName = $arrFieldInfo[0];
			$inteBavelFieldID = (int) @$arrFieldsByName[$streBavelFieldName];
			if ($inteBavelFieldID == 0) {
				continue;
			}
			
			//Es un mapeo de campo válido, identifica si debe agregar o actualizar la información
			if (!isset($objQuestionFieldMap->ArrayVariableValues[$inteBavelFieldID])) {
				//Es un elemento nuevo, pero como el array ya debe venir con los defaults conocidos, se ignorará pues debe ser un error
				//No se intentará eliminar, simplemente al cargar la instancia nunca será reconocído como un tipo de mapeo válido en esta versión
			}
			else {
				//Es un elemento ya existente, se actualizará/insertará dependiendo de si ya estaba o no en la Metadata, pero se actualiza su
				//mapeo. El grabado se hace en base a la propiedad y no al array de valores, por lo tanto se agrega/modifica dicha propiedad
				$objQuestionFieldMap->ArrayVariableValues[$inteBavelFieldID] = $arrFieldInfo[1];
				$objQuestionFieldMap->$streBavelFieldName = $arrFieldInfo[1];
				$arrProcessedFields[$streBavelFieldName] = $streBavelFieldName;
			}
		}
		
		//Invoca al método que graba los mapeos
		@$objQuestionFieldMap->save();
	}
	//@JAPR
	
	//@JAPR 2012-11-24: Agregada la lectura en cadena de opciones de respuesta a partir de preguntas que llenan a otras
	//Obtiene el ID de la pregunta ancestro que llena a todas las demás, incluyendo a los padres de la pregunta indicada (sólo existe un origen
	//cuando se define este tipo de dependencias). Se regresa a si misma si no tiene un ancestro
	static function GetRootSourceQuestionID($aRepository, $intQuestionID) {
		$intSourceQuestionID = 0;
		
		if ($intQuestionID <= 0) {
			return 0;
		}
		
		$sql = "SELECT A.SourceQuestionID 
			FROM SI_SV_Question A 
			WHERE A.QuestionID = ".$intQuestionID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$intSourceQuestionID = (int) @$aRS->fields["sourcequestionid"];
		}
		if ($intSourceQuestionID > 0) {
			$intSourceQuestionID = BITAMQuestion::GetRootSourceQuestionID($aRepository, $intSourceQuestionID);
		}
		else {
			$intSourceQuestionID = $intQuestionID;
		}
		
		return $intSourceQuestionID;
	}
	
	//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
	/* Regresa el AttributeName configurado para la pregunta, si se especifica el parámetro $bRaw = true significa que únicamente se debe preguntar por el valor
	de la propiedad AttributeName tal como está grabado, de lo contrario quiere decir que en caso de no estar configurado, deberá regresar el QuestionText que
	es la etiqueta configurada pero ya sin el HTML
	*/
	function getAttributeName($bRaw = false) {
		$strAttributeName = trim($this->AttributeName);
		
		if (!$bRaw && ($strAttributeName == '' || $strAttributeName == "dim_".$this->QuestionID)) {
			$strAttributeName = $this->QuestionText;
		}
		
		return $strAttributeName;
	}
	
	//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas (campo FormatMask)
	//Obtiene el FormatMaskID que deberá ser usado para seleccionar el valor correcto de la lista durante la captura, todo formato no registrado
	//se asume como personalizado y habilitará el Textbox correspondiente
	//Si se envía el parámetro $sFormatMask, se sobreescribirá dicha propiedad
	function getFormatID($sFormatMask = null)
	{
		if (!is_null($sFormatMask))
		{
			$this->FormatMask = $sFormatMask;
		}
		
		$intFormatID = array_search($this->FormatMask, self::$arrNumericFormats);
		if ($intFormatID !== false)
		{
			$this->FormatMaskID = $intFormatID;
		}
		else 
		{
			$this->FormatMaskID = 1;
		}
		
		return $this->FormatMaskID;
	}
	
	function getDateFormatID($sFormatMask = null)
	{
		if (!is_null($sFormatMask))
		{
			$this->DateFormatMask = $sFormatMask;
		}
		
		$intDateFormatID = array_search($this->DateFormatMask, self::$arrDateFormats);
		if ($intDateFormatID !== false)
		{
			$this->DateFormatMaskID = $intDateFormatID;
		}
		else 
		{
			$this->DateFormatMaskID = '';
		}
		
		return $this->DateFormatMaskID;
	}
	
	//Basado en el FormatMaskID asigna el valor de la máscara (si la máscara es Pesonalizada, no hace nada pues ya debe venir asignada)
	//Si se envía el parámetro $iFormatID, se sobreescribirá dicha propiedad
	function setFormatMask($iFormatID = null)
	{
		if (!is_null($iFormatID))
		{
			$this->FormatMaskID = $iFormatID;
		}
		
		if ($this->FormatMaskID != 1 && isset(self::$arrNumericFormats[$this->FormatMaskID]))
		{
			$this->FormatMask = self::$arrNumericFormats[$this->FormatMaskID];
		}
		
		return $this->FormatMask;
	}
	//@JAPR
	
	function setDateFormatMask($iFormatID = null)
	{
		if (!is_null($iFormatID))
		{
			$this->DateFormatMaskID = $iFormatID;
		}
		
		if (isset(self::$arrDateFormats[$this->DateFormatMaskID]))
		{
			$this->DateFormatMask = self::$arrDateFormats[$this->DateFormatMaskID];
		}
	    //@JAPR 2012-10-12: Corregido un bug, se supone que al mandar un formato inválido (o bien intencionalmente vacio) se quiere limpiar, pero por como habían genedado el array
	    //de formatos de fecha no había índice para cuando era un formato vacio así que nunca limpiaba y dejaba un default no deseado en preguntas que no son fecha
	    else {
	      $this->DateFormatMask = '';
	    }
	    //@JAPR
		
		return $this->DateFormatMask;
	}
	
	function getModel()
	{
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//@JAPRDescontinuada en v6
		//@JAPR 2018-10-30: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//No se puede remover por completo la carga de esta información, ya que para preguntas nuevas era indispensable para poder ordernarlas correctamente, así que se validará
		//únicamente para entrar si está en modo de diseño
		global $gbDesignMode;
		
		if (!$gbDesignMode) {
			return;
		}
		//@JAPR
		if ($this->SectionID <= 0) {
			return;
		}
		
		$sql = "SELECT t1.SurveyID, t2.ModelID, t1.SectionName, t1.SectionNumber FROM SI_SV_Section t1, SI_SV_Survey t2 WHERE t1.SectionID = ".$this->SectionID." AND t1.SurveyID = t2.SurveyID";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$this->SurveyID = (int)$aRS->fields["surveyid"];
			$this->ModelID = (int)$aRS->fields["modelid"];
			$this->SurveyTable = "SVSurvey_".$this->ModelID;
			$this->SurveyMatrixTable = "SVSurveyMatrixData_".$this->ModelID;
			$this->SectionName = $aRS->fields["sectionname"];
			$this->SectionNumber = (int)$aRS->fields["sectionnumber"];
		}
	}
	
	function updateQuestionsToEval() {
		$this->removeQuestionsToEval();
		$this->setQuestionsToEval();
	}
	
	function removeQuestionsToEval() {
		$sql = "DELETE FROM SI_SV_QuestionDep WHERE QuestionID = ".$this->QuestionID;		
		if ($this->Repository->DataADOConnection->Execute($sql) === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	function setQuestionsToEval() {
		$subject = $this->DefaultValue;
		//@JAPR 2014-09-19: Corregido un bug, al usar ahora IDs en lugar de números, este proceso ya no funciona, así que hay que convertir a números
		//antes de continuar
		$subject = $this->TranslateVariableQuestionIDsByNumbers($subject);
		//@JAPR
		
		$pattern = '/[\{]{1}[@]{1}[Q|q]{1}[0-9]+(\([0-9|a-z|A-Z]*\))?[}]{1}/';
		//Con este patrón obtenemos todos los {@Q<qnumbers>(attribNum)} que esten en la formula
		preg_match_all($pattern, $subject, $arrayEvalQuestions);
		//@JAPR 2014-10-23: Corregido un bug, faltaba considerar al nuevo formato de preguntas
		$arrayQuestions = array();
		if (count($arrayEvalQuestions[0]) > 0) {
			$arrayQuestions = array_unique($arrayEvalQuestions[0]);
		}
		
		//Agregará al mismo array de preguntas las referencias de las que se encuentran en el nuevo formato, ya eliminando datos extras
		//Con este patron obtnememos todos los {$Q<qnumbers>.prop} o {$Q<qnumbers>.func(param)} que esten en la formula
		//@JAPR 2014-10-28: Corregido un bug, se cambió la expresión para que el parámetro de la función sea cualquier caracter
		//$pattern = '/[\{]{1}[$]{1}[Q|q]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\(([0-9|a-z|A-Z\'\"]*)\)))|){1}[}]{1}/';
		$pattern = '/[\{]{1}[$]{1}[Q|q]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/';
		preg_match_all($pattern, $subject, $arrayEvalQuestionsAdv);
		if (count($arrayEvalQuestionsAdv[0]) > 0) {
			$arrayQuestionsAdv = array_unique($arrayEvalQuestionsAdv[0]);
			array_walk($arrayQuestionsAdv, 'replaceNewVariableParams');
			$arrayQuestions = array_merge($arrayQuestions, $arrayQuestionsAdv);
		}
		
		//@JAPR 2016-01-12: Agregado el encadenamiento de refresh a las preguntas tipo Mensaje (siempre) / HTML (solo si tiene Auto-Redraw)
		if ($this->QTypeID == qtpMessage) {
			//Si es tipo Mensaje siempre hará el refresh, si es tipo HTML dependerá del Auto-redraw exclusivamente
			if ($this->EditorType == edtpMessage || ($this->EditorType == edtpHTML && $this->AutoRedraw)) {
				$subject = $this->QuestionMessage;
				$subject = $this->TranslateVariableQuestionIDsByNumbers($subject);
				$pattern = '/[\{]{1}[@]{1}[Q|q]{1}[0-9]+(\([0-9|a-z|A-Z]*\))?[}]{1}/';
				preg_match_all($pattern, $subject, $arrayEvalQuestions);
				if (count($arrayEvalQuestions[0]) > 0) {
					$arrayQuestionsAdd = array_unique($arrayEvalQuestions[0]);
					$arrayQuestions = array_merge($arrayQuestions, $arrayQuestionsAdd);
				}
				
				$pattern = '/[\{]{1}[$]{1}[Q|q]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/';
				preg_match_all($pattern, $subject, $arrayEvalQuestionsAdv);
				if (count($arrayEvalQuestionsAdv[0]) > 0) {
					$arrayQuestionsAdv = array_unique($arrayEvalQuestionsAdv[0]);
					array_walk($arrayQuestionsAdv, 'replaceNewVariableParams');
					$arrayQuestions = array_merge($arrayQuestions, $arrayQuestionsAdv);
				}
			}
		}
		//@JAPR
		
		if(count($arrayQuestions) == 0) {
			return;
		}
		
		//Debemos obtener el numero de pregunta solamente asi que eliminamos los demas
		$charToErase = array("{","}","@","Q","$");
		$replaceWith = array("","","","","");
		//@JAPR 2012-10-19: Faltaba reemplazar la parte de los atributos, la cual no es necesaria para este proceso
		$intMax = count($arrayQuestions);
		for ($intIndex = 0; $intIndex < $intMax; $intIndex++) {
			$strQuestionID = $arrayQuestions[$intIndex];
			$intPos = strpos($strQuestionID, '(');
			if ($intPos !== false) {
				$strQuestionID = substr($strQuestionID, 0, $intPos);
				$arrayQuestions[$intIndex] = $strQuestionID;
			}
		}
		//@JAPR
		$arrayQuestionNumbers = str_ireplace($charToErase,$replaceWith,$arrayQuestions);
		//@JAPR 2012-11-22: Agregada una validación para que en caso de no haber identificado un patrón válido, simplemente salga de la función
		if (count($arrayQuestionNumbers) == 0) {
			return;
		}
		//@JAPR
		
		//ahora vamos a buscar a que id pertenece ese numero de pregunta
		//@JAPR 2017-02-13: Corregido un bug, desde que inició la versión 6, las fórmulas y demás expresiones ya no se graban con número de pregunta, así que esta condición sólo podría
		//haber funcionado para las primeras formas pero no para las últimas donde los IDs de preguntas superan por mucho a la cantidad de preguntas de la forma
		$sql1 = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID.
				" AND QuestionNumber IN (".implode(', ',$arrayQuestionNumbers).")";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Checking question dependencies default ({$this->QuestionID}): {$sql1}", 2, 0, "color:blue;");
		}
		$aRS1 = $this->Repository->DataADOConnection->Execute($sql1);
		if ($aRS1 === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql1);
		}
		while(!$aRS1->EOF) {
			
			//Obtenemos el AutoEvalID
	    	$sql2 =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(AutoEvalID)", "0")." + 1 AS AutoEvalID".
						" FROM SI_SV_QuestionDep";
			$aRS2 = $this->Repository->DataADOConnection->Execute($sql2);
			if (!$aRS2 || $aRS2->EOF) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql2);
			}
			$autoevalid = (int)$aRS2->fields["autoevalid"];
			
			//este es el id de la pregunta
			$anEvalQuestionID = (int)$aRS1->fields["questionid"];
			
			//insertamos en SI_SV_QuestionDep las relaciones padre-hija
			$sql3 = "INSERT INTO SI_SV_QuestionDep (AutoEvalID, QuestionID, EvalQuestionID) VALUES (".
					$autoevalid.
					", ".$this->QuestionID.
					", ".$anEvalQuestionID.
					")";	
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Inserting question dependency default ({$this->QuestionID}): {$sql3}", 2, 0, "color:blue;");
			}
			if ($this->Repository->DataADOConnection->Execute($sql3) === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql3);
			}
			
			//seteamos la variable de la instancia
			$this->QuestionsToEval[] = $anEvalQuestionID;
			
			$aRS1->MoveNext();
		}
	}
	
	function getQuestionsToEval() {
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		if ( BITAMGlobalFormsInstance::IsSurveyFullyLoadedFromDB($this->SurveyID) ) {
			$this->QuestionsToEval = BITAMGlobalFormsInstance::GetQuestionToEvalQuestionWithID($this->QuestionID);
			return;
		}
		//@JAPR
		
		$sql = "SELECT QuestionID FROM SI_SV_QuestionDep WHERE EvalQuestionID = ".$this->QuestionID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		$this->QuestionsToEval = array();
		while(!$aRS->EOF) {
			$this->QuestionsToEval[] = (int)$aRS->fields["questionid"];
			$aRS->MoveNext();
		}
	}
	
	function updateFormulaChildren() {
		$this->removeFormulaChildren();
		$this->setFormulaChildren();
	}
	
	function removeFormulaChildren() {
		$sql = "DELETE FROM SI_SV_QuestionFormulaDep WHERE QuestionID = ".$this->QuestionID;		
		if ($this->Repository->DataADOConnection->Execute($sql) === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFormulaDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	function setFormulaChildren() {
		//@JAPR 2013-11-12: Corregido un bug, si no se elimina este array, al volver a invocar al método después de un reposicionamiento
		//se estarían conservando los antiguos valores de la instancia
		$this->FormulaChildren = array();
		//@JAPR
		$subject = $this->Formula;
		//@JAPR 2014-09-19: Corregido un bug, al usar ahora IDs en lugar de números, este proceso ya no funciona, así que hay que convertir a números
		//antes de continuar
		$subject = $this->TranslateVariableQuestionIDsByNumbers($subject);
		//@JAPR
		$pattern = '/[\{]{1}[@]{1}[Q|q]{1}[0-9]+(\([0-9|a-z|A-Z]*\))?[}]{1}/';
		
		//Con este patron obtnememos todos los {@Q<qnumbers>(attribNum)} que esten en la formula
		preg_match_all($pattern, $subject, $arrayEvalQuestions);
		//@JAPR 2014-10-23: Corregido un bug, faltaba considerar al nuevo formato de preguntas
		$arrayQuestions = array();
		if (count($arrayEvalQuestions[0]) > 0) {
			$arrayQuestions = array_unique($arrayEvalQuestions[0]);
		}
		
		//Agregará al mismo array de preguntas las referencias de las que se encuentran en el nuevo formato, ya eliminando datos extras
		//Con este patron obtnememos todos los {$Q<qnumbers>.prop} o {$Q<qnumbers>.func(param)} que esten en la formula
		//@JAPR 2014-10-28: Corregido un bug, se cambió la expresión para que el parámetro de la función sea cualquier caracter
		//$pattern = '/[\{]{1}[$]{1}[Q|q]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\(([0-9|a-z|A-Z\'\"]*)\)))|){1}[}]{1}/';
		$pattern = '/[\{]{1}[$]{1}[Q|q]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/';
		preg_match_all($pattern, $subject, $arrayEvalQuestionsAdv);
		if (count($arrayEvalQuestionsAdv[0]) > 0) {
			$arrayQuestionsAdv = array_unique($arrayEvalQuestionsAdv[0]);
			array_walk($arrayQuestionsAdv, 'replaceNewVariableParams');
			$arrayQuestions = array_merge($arrayQuestions, $arrayQuestionsAdv);
		}
		
		if(count($arrayQuestions) == 0) {
			return;
		}
		
		//Debemos obtener el numero de pregunta solamente asi que eliminamos los demas
		$charToErase = array("{","}","@","Q","$");
		$replaceWith = array("","","","","");
		//@JAPR 2012-10-19: Faltaba reemplazar la parte de los atributos, la cual no es necesaria para este proceso
		$intMax = count($arrayQuestions);
		for ($intIndex = 0; $intIndex < $intMax; $intIndex++) {
			$strQuestionID = $arrayQuestions[$intIndex];
			$intPos = strpos($strQuestionID, '(');
			if ($intPos !== false) {
				$strQuestionID = substr($strQuestionID, 0, $intPos);
				$arrayQuestions[$intIndex] = $strQuestionID;
			}
		}
		//@JAPR
		$arrayQuestionNumbers = str_ireplace($charToErase,$replaceWith,$arrayQuestions);
		//@JAPR 2012-11-22: Agregada una validación para que en caso de no haber identificado un patrón válido, simplemente salga de la función
		if (count($arrayQuestionNumbers) == 0) {
			return;
		}
		//@JAPR
		
		//ahora vamos a buscar a que id pertenece ese numero de pregunta
		$sql1 = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID.
				" AND QuestionNumber IN (".implode(', ',$arrayQuestionNumbers).")";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Checking question dependencies formula ({$this->QuestionID}): {$sql1}", 2, 0, "color:blue;");
		}
		$aRS1 = $this->Repository->DataADOConnection->Execute($sql1);
		if ($aRS1 === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql1);
		}
		while(!$aRS1->EOF) {
			
			//Obtenemos el AutoEvalID
	    	$sql2 =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(AutoEvalID)", "0")." + 1 AS AutoEvalID".
						" FROM SI_SV_QuestionFormulaDep";
			$aRS2 = $this->Repository->DataADOConnection->Execute($sql2);
			if (!$aRS2 || $aRS2->EOF) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFormulaDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql2);
			}
			$autoevalid = (int)$aRS2->fields["autoevalid"];
			
			//este es el id de la pregunta
			$anEvalQuestionID = (int)$aRS1->fields["questionid"];
			
			//insertamos en SI_SV_QuestionFormulaDep las relaciones padre-hija
			$sql3 = "INSERT INTO SI_SV_QuestionFormulaDep (AutoEvalID, QuestionID, EvalQuestionID) VALUES (".
					$autoevalid.
					", ".$this->QuestionID.
					", ".$anEvalQuestionID.
					")";	
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Inserting question dependency formula ({$this->QuestionID}): {$sql3}", 2, 0, "color:blue;");
			}
			if ($this->Repository->DataADOConnection->Execute($sql3) === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFormulaDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql3);
			}
			
			//seteamos la variable de la instancia
			$this->FormulaChildren[] = $anEvalQuestionID;
			
			$aRS1->MoveNext();
		}
	}
	
	function getFormulaChildren() {
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		if ( BITAMGlobalFormsInstance::IsSurveyFullyLoadedFromDB($this->SurveyID) ) {
			$this->FormulaChildren = BITAMGlobalFormsInstance::GetFormulaChildDepByQuestionWithID($this->QuestionID);
			return;
		}
		//@JAPR
		
		$sql = "SELECT QuestionID FROM SI_SV_QuestionFormulaDep WHERE EvalQuestionID = ".$this->QuestionID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFormulaDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFormulaDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		//@JAPR 2012-11-24: Modificado para permitir que las preguntas numéricas de catálogo sean evaluadas al cambiar a la pregunta padre
		//en cualquier tipo de sección
		$arrFormulaChildren = array();
		$this->FormulaChildren = array();
		while(!$aRS->EOF) {
			$intQuestionID = (int)$aRS->fields["questionid"];
			$this->FormulaChildren[] = $intQuestionID;
			$arrFormulaChildren[$intQuestionID] = $intQuestionID;
			$aRS->MoveNext();
		}
		
		//@JAPRWarning: Para v6 NO existe un solo caso donde el código de abajo aplique, ya que se dejaron de soportar las secciones dinámicas como tal, y por ende las preguntas numéricas
		//asociadas automáticamente a un catálogo, así que ya no existe dependencia de fórmula de dichas preguntas
		//Agrega a la lista todas aquellas preguntas abiertas numéricas que provengan del mismo catálogo que esta (si es que esta es una simple
		//choice de catálogo) ya que al cambiarlo deberán cambiar a dichas preguntas, pero se deben encontrar después de esta pregunta ya que
		//si estuvieran antes entonces no pudo haber cambiado su valor porque no habría selección aún de la pregunta padre, además no deben estar
		//dentro de secciones dinámicas del mismo catálogo porque esas automáticamente ya se hubieran regenerado
		if ($this->QTypeID == qtpSingle && $this->UseCatalog && $this->CatalogID > 0) {
			$sql = "SELECT A.QuestionID 
				FROM SI_SV_Question A, SI_SV_Section B 
				WHERE A.SectionID = B.SectionID AND A.SurveyID = ".$this->SurveyID." AND 
					A.QTypeID = ".qtpOpenNumeric." AND A.CatalogID = ".$this->CatalogID." AND 
					(B.SectionType <> ".sectDynamic." OR B.CatalogID <> A.CatalogID) AND 
					A.QuestionNumber > ".$this->QuestionNumber;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false) {
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question A, SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question A, SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			while(!$aRS->EOF) {
				$intQuestionID = (int)$aRS->fields["questionid"];
				if (!isset($arrFormulaChildren[$intQuestionID])) {
					$this->FormulaChildren[] = $intQuestionID;
					$arrFormulaChildren[$intQuestionID] = $intQuestionID;
				}
				$aRS->MoveNext();
			}
		}
		//@JAPR
	}
	
	function updateShowConditionChildren() {
		$this->removeShowConditionChildren();
		$this->setShowConditionChildren();
	}
	
	function removeShowConditionChildren() {
		$sql = "DELETE FROM SI_SV_ShowConditionDep WHERE ElementID = ".$this->QuestionID;		
		if ($this->Repository->DataADOConnection->Execute($sql) === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowConditionDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	function setShowConditionChildren() {
		$subject = $this->ShowCondition;
		//@JAPR 2014-09-19: Corregido un bug, al usar ahora IDs en lugar de números, este proceso ya no funciona, así que hay que convertir a números
		//antes de continuar
		$subject = $this->TranslateVariableQuestionIDsByNumbers($subject);
		//@JAPR
		$pattern = '/[\{]{1}[@]{1}[Q|q]{1}[0-9]+(\([0-9|a-z|A-Z]*\))?[}]{1}/';
		
		//Con este patron obtnememos todos los {@Q<qnumbers>(attribNum)} que esten en la formula
		preg_match_all($pattern, $subject, $arrayEvalQuestions);
		//@JAPR 2014-10-23: Corregido un bug, faltaba considerar al nuevo formato de preguntas
		$arrayQuestions = array();
		if (count($arrayEvalQuestions[0]) > 0) {
			$arrayQuestions = array_unique($arrayEvalQuestions[0]);
		}
		
		//Agregará al mismo array de preguntas las referencias de las que se encuentran en el nuevo formato, ya eliminando datos extras
		//Con este patron obtnememos todos los {$Q<qnumbers>.prop} o {$Q<qnumbers>.func(param)} que esten en la formula
		//@JAPR 2014-10-28: Corregido un bug, se cambió la expresión para que el parámetro de la función sea cualquier caracter
		//$pattern = '/[\{]{1}[$]{1}[Q|q]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\(([0-9|a-z|A-Z\'\"]*)\)))|){1}[}]{1}/';
		$pattern = '/[\{]{1}[$]{1}[Q|q]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/';
		preg_match_all($pattern, $subject, $arrayEvalQuestionsAdv);
		if (count($arrayEvalQuestionsAdv[0]) > 0) {
			$arrayQuestionsAdv = array_unique($arrayEvalQuestionsAdv[0]);
			array_walk($arrayQuestionsAdv, 'replaceNewVariableParams');
			$arrayQuestions = array_merge($arrayQuestions, $arrayQuestionsAdv);
		}
		
		if(count($arrayQuestions) == 0) {
			return;
		}
		
		//Debemos obtener el numero de pregunta solamente asi que eliminamos los demas
		$charToErase = array("{","}","@","Q","$");
		$replaceWith = array("","","","","");
		//@JAPR 2012-10-19: Faltaba reemplazar la parte de los atributos, la cual no es necesaria para este proceso
		$intMax = count($arrayQuestions);
		for ($intIndex = 0; $intIndex < $intMax; $intIndex++) {
			$strQuestionID = $arrayQuestions[$intIndex];
			$intPos = strpos($strQuestionID, '(');
			if ($intPos !== false) {
				$strQuestionID = substr($strQuestionID, 0, $intPos);
				$arrayQuestions[$intIndex] = $strQuestionID;
			}
		}
		//@JAPR
		$arrayQuestionNumbers = str_ireplace($charToErase,$replaceWith,$arrayQuestions);
		//@JAPR 2012-11-22: Agregada una validación para que en caso de no haber identificado un patrón válido, simplemente salga de la función
		if (count($arrayQuestionNumbers) == 0) {
			return;
		}
		//@JAPR
		
		//ahora vamos a buscar a que id pertenece ese numero de pregunta
		$sql1 = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID.
				" AND QuestionNumber IN (".implode(', ',$arrayQuestionNumbers).")";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Checking question dependencies show condition ({$this->QuestionID}): {$sql1}", 2, 0, "color:blue;");
		}
		$aRS1 = $this->Repository->DataADOConnection->Execute($sql1);
		if ($aRS1 === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql1);
		}
		while(!$aRS1->EOF) {
			
			//Obtenemos el AutoEvalID
	    	$sql2 =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(AutoEvalID)", "0")." + 1 AS AutoEvalID".
						" FROM SI_SV_ShowConditionDep";
			$aRS2 = $this->Repository->DataADOConnection->Execute($sql2);
			if (!$aRS2 || $aRS2->EOF) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowConditionDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql2);
			}
			$autoevalid = (int)$aRS2->fields["autoevalid"];
			
			//este es el id de la pregunta
			$anEvalQuestionID = (int)$aRS1->fields["questionid"];
			
			//insertamos en SI_SV_ShowConditionDep las relaciones padre-hija
			$sql3 = "INSERT INTO SI_SV_ShowConditionDep (AutoEvalID, ElementID, ElementType, EvalElementID) VALUES (".
					$autoevalid.
					", ".$this->QuestionID.
					", 0".
					", ".$anEvalQuestionID.
					")";	
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Inserting question dependency show condition ({$this->QuestionID}): {$sql3}", 2, 0, "color:blue;");
			}
			if ($this->Repository->DataADOConnection->Execute($sql3) === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowConditionDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql3);
			}
			
			//seteamos la variable de la instancia
			$this->ShowConditionChildren[] = $anEvalQuestionID;
			
			$aRS1->MoveNext();
		}
	}
	
	function getShowConditionChildren() {
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		if ( BITAMGlobalFormsInstance::IsSurveyFullyLoadedFromDB($this->SurveyID) ) {
			$this->ShowConditionChildren = BITAMGlobalFormsInstance::GetShowCondDepByQuestionWithID($this->QuestionID);
			return;
		}
		//@JAPR
		
		$sql = "SELECT ElementID FROM SI_SV_ShowConditionDep WHERE EvalElementID = ".$this->QuestionID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowConditionDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowConditionDep ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		//@JAPR 2012-11-24: Modificado para permitir que las preguntas numéricas de catálogo sean evaluadas al cambiar a la pregunta padre
		//en cualquier tipo de sección
		$arrShowConditionChildren = array();
		$this->ShowConditionChildren = array();
		while(!$aRS->EOF) {
			$intQuestionID = (int)$aRS->fields["elementid"];
			$this->ShowConditionChildren[] = $intQuestionID;
			$arrShowConditionChildren[$intQuestionID] = $intQuestionID;
			$aRS->MoveNext();
		}
		
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Corregido un bug, todo este query era desperdiciado porque su ciclo estaba comentado, así que se removerá
		//Agrega a la lista todas aquellas preguntas abiertas numéricas que provengan del mismo catálogo que esta (si es que esta es una simple
		//choice de catálogo) ya que al cambiarlo deberán cambiar a dichas preguntas, pero se deben encontrar después de esta pregunta ya que
		//si estuvieran antes entonces no pudo haber cambiado su valor porque no habría selección aún de la pregunta padre, además no deben estar
		//dentro de secciones dinámicas del mismo catálogo porque esas automáticamente ya se hubieran regenerado
		//@JAPR
	}
	
	//@JAPR 2012-10-19: Agregada la colección de preguntas Simple y multiple Choice que serán llenadas a partir de esta y para excluir
	function getChildrenToFill() {
		$arrQuestionsToFill = array();
		
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SourceQuestionID = ".$this->QuestionID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF) {
			$arrQuestionsToFill[] = (int)$aRS->fields["questionid"];
			$aRS->MoveNext();
		}
		
		return $arrQuestionsToFill;
	}
	
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	/* Función equivalente a getChildrenToFill pero que reutiliza el caché previamente cargado de forma masiva */
	function getChildrenToFillRef() {
		$arrQuestionsToFill = BITAMGlobalFormsInstance::GetChildrenToFillByQuestionWithID($this->QuestionID);
		if (is_null($arrQuestionsToFill) || !is_array($arrQuestionsToFill)) {
			$arrQuestionsToFill = array();
		}
		
		return $arrQuestionsToFill;
	}
	
	//@JAPR 2014-10-21: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
	//Obtiene la lista de secciones Inline cuyas opciones de registros deberán llenarse a partir de esta pregunta
	function getSectionsToFill() {
		$arrSectionsToFill = array();
		
		//Si la pregunta es múltiple choice o tipo sección - inline, entonces se verifica si como pregunta hay alguna sección Inline que sea llenada
		//por ella, en ese caso se asignan dichas secciones como las que deben ser regeneradas al cambiar esta pregunta
		if ($this->QTypeID == qtpMulti || $this->QTypeID == qtpSection) {
			$sql = "SELECT SectionID FROM SI_SV_Section WHERE SourceQuestionID = ".$this->QuestionID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while(!$aRS->EOF) {
				$arrSectionsToFill[] = (int)$aRS->fields["sectionid"];
				$aRS->MoveNext();
			}
		}
		elseif ($this->QTypeID == qtpSingle && $this->IsSelector) {
			//Si la pregunta es simple choice y es la selectora de una sección Inline, entonces hay que verificar si como sección hay alguna otra
			//sección Inline que sea llenada por ella, en ese caso se asignan dichas secciones como las que deben ser regeneradas al cambiar esta pregunta
			$sql = "SELECT SectionID 
				FROM SI_SV_Section 
				WHERE SurveyID = {$this->SurveyID} AND SourceSectionID = {$this->SectionID} 
					UNION 
				SELECT SectionID 
				FROM SI_SV_Section 
				WHERE SurveyID = {$this->SurveyID} AND SourceQuestionID IN (
						SELECT QuestionID 
						FROM SI_SV_Question 
						WHERE SurveyID = {$this->SurveyID} AND QTypeID = ".qtpSection." AND SourceSectionID = {$this->SectionID} 
					)";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while(!$aRS->EOF) {
				$arrSectionsToFill[] = (int)$aRS->fields["sectionid"];
				$aRS->MoveNext();
			}
		}
		
		return $arrSectionsToFill;
	}
	
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	/* Función equivalente a getSectionsToFill pero que reutiliza el caché previamente cargado de forma masiva */
	function getSectionsToFillRef() {
		$arrSectionsToFill = array();
		
		//Si la pregunta es múltiple choice o tipo sección - inline, entonces se verifica si como pregunta hay alguna sección Inline que sea llenada
		//por ella, en ese caso se asignan dichas secciones como las que deben ser regeneradas al cambiar esta pregunta
		if ($this->QTypeID == qtpMulti || $this->QTypeID == qtpSection) {
			$arrSectionsToFill = BITAMGlobalFormsInstance::GetSectionsToFillByQuestionWithID($this->QuestionID);
			if (is_null($arrSectionsToFill) || !is_array($arrSectionsToFill)) {
				$arrSectionsToFill = array();
			}
		}
		elseif ($this->QTypeID == qtpSingle && $this->IsSelector) {
			//Si la pregunta es simple choice y es la selectora de una sección Inline, entonces hay que verificar si como sección hay alguna otra
			//sección Inline que sea llenada por ella, en ese caso se asignan dichas secciones como las que deben ser regeneradas al cambiar esta pregunta
			//Primero obtiene la colección de secciones que son llenadas por esta sección
			$arrSectionsToFillBySection = BITAMGlobalFormsInstance::GetSectionsToFillBySectionWithID($this->SectionID);
			if (is_null($arrSectionsToFillBySection) || !is_array($arrSectionsToFillBySection)) {
				$arrSectionsToFillBySection = array();
			}
			$arrSectionsToFill = array_merge($arrSectionsToFillBySection, array());
			
			//Posteriormente obtiene todas las secciones que son llenadas por preguntas que a su vez son llenadas por esta sección, por tanto obtiene las preguntas de selección
			//llenadas por esta sección y realiza un ciclo para obtener las secciones que cada una de ellas llena
			$arrSectionQuestions = BITAMGlobalFormsInstance::GetSectionQuestionsBySourceSectionWithID($this->SectionID);
			if (is_null($arrSectionQuestions) || !is_array($arrSectionQuestions)) {
				$arrSectionQuestions = array();
			}
			foreach ( $arrSectionQuestions as $intSectionQuestionID ) {
				if ( $intSectionQuestionID > 0 ) {
					$arrSectionsToFillByQuestion = BITAMGlobalFormsInstance::GetSectionsToFillByQuestionWithID($intSectionQuestionID);
					if (is_null($arrSectionsToFillByQuestion) || !is_array($arrSectionsToFillByQuestion)) {
						continue;
					}
					
					//Realiza el merge de los IDs de secciones con los acumulados
					$arrSectionsToFill = array_merge($arrSectionsToFill, $arrSectionsToFillByQuestion);
				}
			}
			
			//Finalmente verifica que no existan duplicados (aunque no debería poder ser posible, ya que significaría que dos o mas fuentes llenan la misma sección)
			$arrSectionsToFill = array_unique($arrSectionsToFill);
		}
		
		return $arrSectionsToFill;
	}
	//@JAPR
	
	function getChildrenToExclude() {
		$arrQuestionsToExclude = array();
		
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SourceSimpleQuestionID = ".$this->QuestionID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF) {
			$arrQuestionsToExclude[] = (int)$aRS->fields["questionid"];
			$aRS->MoveNext();
		}
		
		return $arrQuestionsToExclude;
	}
	
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	/* Función equivalente a getChildrenToExclude pero que reutiliza el caché previamente cargado de forma masiva */
	function getChildrenToExcludeRef() {
		$arrQuestionsToExclude = BITAMGlobalFormsInstance::GetChildrenToExcludeByQuestionWithID($this->QuestionID);
		if (is_null($arrQuestionsToExclude) || !is_array($arrQuestionsToExclude)) {
			$arrQuestionsToExclude = array();
		}
		
		return $arrQuestionsToExclude;
	}
	//@JAPR
	
	function getSelectOptions()
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$catDimVal = 0;
		
		$blnExcludeNonHeritableSettings = false;
		$intSourceQuestionID = $this->QuestionID;

		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Movida la inicialización delas opciones antes del query para permitir reutilizar la colección de caché en lugar de crearse siempre desde un recordset
		$this->SelectOptions = array();
		$this->QConsecutiveIDs = array();
		$this->Scores = array();
		$this->NextQuestion = array();
		$this->QIndDimIds = array();
		$this->QIndicatorIds = array();
		$this->CatDimValSelectOptions = array();
		$this->ElemDimValSelectOptions = array();
		//@JAPR 2012-10-11: Agregada la lectura de la imagen asociada para regresar en el servicio
		$this->DisplayImage = array();
		//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
		$this->HTMLText = array();
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		$this->Visible = array();
		//@JAPR
		$this->CancelAgenda = array();
		//Agregado el num de tel
		$this->PhoneNum = array();
		
		$this->OptionDefaultValue = array();
		//@JAPR
		
		if($this->QTypeID==qtpMulti && $this->QDisplayMode==dspVertical && $this->MCInputType==mpcNumeric && $this->IsMultiDimension==0 && $this->UseCategoryDimChoice!=0)
		{
			//Obtenemos el valor de la llave subrogada de CategoryDimensionName
			$tableCatDim = "RIDIM_".$this->CategoryDimID;
			
			$sql = "SELECT ".$tableCatDim."KEY AS CatDimVal FROM ".$tableCatDim." 
				WHERE DSC_".$this->CategoryDimID." = ".$this->Repository->DataADOConnection->Quote($this->AttributeName);
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableCatDim." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." ".$tableCatDim." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			if(!$aRS->EOF)
			{
				$catDimVal = (int)$aRS->fields["catdimval"];
			}
			
			$tableElemDim = "RIDIM_".$this->ElementDimID;
			
			//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
			$strAdditionalFields = '';
			if (getMDVersion() >= esvHideAnswers) {
				$strAdditionalFields .= ', t1.Hidden';
			}
			//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				$strAdditionalFields .= ', t1.HTMLText';
			}
			
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalFields .= ', t1.CancelAgenda';
			}
			//@JAPR
			if($catDimVal!=0)
			{
				//@JAPR 2012-10-11: Agregada la lectura de la imagen asociada para regresar en el servicio
				$sql = "SELECT t1.ConsecutiveID, t1.SortOrder, t1.DisplayText, t1.NextSection, t1.IndDimID, t1.IndicatorID, t1.Score,
						t2.".$tableElemDim."KEY AS ElemDimVal, t1.DisplayImage $strAdditionalFields 
					FROM SI_SV_QAnswers t1, ".$tableElemDim." t2 WHERE t1.QuestionID = ".$this->QuestionID." 
						AND t1.DisplayText = t2.DSC_".$this->ElementDimID." 
					ORDER BY t1.SortOrder";
			}
			else 
			{
				//@JAPR 2012-10-11: Agregada la lectura de la imagen asociada para regresar en el servicio
				$sql = "SELECT t1.ConsecutiveID, t1.SortOrder, t1.DisplayText, t1.NextSection, t1.IndDimID, t1.IndicatorID, t1.Score, 
						0 AS ElemDimVal, t1.DisplayImage $strAdditionalFields 
					FROM SI_SV_QAnswers t1 
					WHERE t1.QuestionID = ".$this->QuestionID." 
					ORDER BY t1.SortOrder";
			}
			
			//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
			//Movida la asignación como recorset para el único caso (descontinuado en v6) que aún cargaba la colección como recordset
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			while(!$aRS->EOF)
			{
				//@JAPR 2013-03-11: Corregido un bug, el lugar donde graba el indicador depende del tipo de pregunta
				if (($this->QTypeID == qtpSingle && $this->IndicatorID > 0)) {
					$this->HasScores = true;
				}
				//@JAPR
				
				$sortOrder = (int)$aRS->fields["sortorder"];
				$displayText = $aRS->fields["displaytext"];
				$this->SelectOptions[] = $displayText;
				$this->QConsecutiveIDs[] = (int)$aRS->fields["consecutiveid"];
				$this->Scores[] = (float) $aRS->fields["score"];
				$nextQuestionID = (int) $aRS->fields["nextsection"];
				$this->NextQuestion[] = $nextQuestionID;
				//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
				//@JAPR 2013-08-16: Corregido un bug, no se estaba inicializando esta variable
				$intVisible = true;
				if (getMDVersion() >= esvHideAnswers) {
					$intVisible = (int) !((int) @$aRS->fields["hidden"]);
				}
				$this->Visible[] = $intVisible;
				//@JAPR
				$intCancelAgenda = false;
				if (getMDVersion() >= esvAgendaRedesign) {
					$intCancelAgenda = (int) @$aRS->fields["cancelagenda"];
				}
				$this->CancelAgenda[] = $intCancelAgenda;
				if (!$blnExcludeNonHeritableSettings) {
					$indDimID = (int) $aRS->fields["inddimid"];
					$this->QIndDimIds[] = $indDimID;
					$indicatorID = (int)$aRS->fields["indicatorid"];
					$this->QIndicatorIds[] = $indicatorID;
					//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
					//@JAPR 2013-03-11: Corregido un bug, el lugar donde graba el indicador depende del tipo de pregunta
					if (($this->QTypeID == qtpMulti && $indicatorID > 0)) {
						$this->HasScores = true;
					}
					//@JAPR
				}
				$this->CatDimValSelectOptions[] = $catDimVal;
				//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
				$this->ElemDimValSelectOptions[] = (int)@$aRS->fields["elemdimval"];
				//@JAPR 2012-10-11: Agregada la lectura de la imagen asociada para regresar en el servicio
				$this->DisplayImage[] = (string) $aRS->fields["displayimage"];
				//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
				$this->HTMLText[] = (string) @$aRS->fields["htmltext"];
				//@JAPR
				//conchita 2013-04-30
				if (getMDVersion() >= esvCallList) {
					$this->PhoneNum[] = (string) @$aRS->fields["phonenumber"];
				}
				
				if (getMDVersion() >= esvDefaultOption) {
					$this->OptionDefaultValue[] = (string) @$aRS->fields["defaultvalue"];
				}
				//Conchita
				$aRS->MoveNext();
			}
			//@JAPR
		}
		else 
		{
			//@JAPR 2012-11-24: Agregada la lectura en cadena de opciones de respuesta a partir de la pregunta que llena a esta, ya que en esos
			//casos la propia pregunta no tiene el listado de opciones. Adicionalmente se deshabilita la lectura de dimensiones porque como no
			//tiene sus propias opciones, no puede sobreescribir en las dimensiones de la pregunta original, pero el resto de las características
			//si son heredables
			if ($this->SourceQuestionID > 0) {
				$intSourceQuestionID = BITAMQuestion::GetRootSourceQuestionID($this->Repository, $intSourceQuestionID);
				$blnExcludeNonHeritableSettings = true;
			}
			elseif($this->SharedQuestionID > 0) {
				//En este caso, la pregunta si tendrá sus propias opciones de respuesta ya que sería demasiado complicado hacer apuntadores con
				//tantas cosas que se deberían poder personalizar en cada pregunta compartida, simplemente se va a bloquear el poder editar los
				//textos, agregar o borrar mas opciones de respuesta, y se sincronizarán esos procesos a cuando la pregunta origen sea manipulada
			}
			
			//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
			//Cambiada la asignación para utilizar la colección de opciones de respuesta en lugar de un recordset
			$objQuestionOptionsCollection = BITAMQuestionOptionCollection::NewInstanceByQuestion($this->Repository, $intSourceQuestionID);
			foreach ($objQuestionOptionsCollection->Collection as $objQuestionOption) {
				//@JAPR 2013-03-11: Corregido un bug, el lugar donde graba el indicador depende del tipo de pregunta
				if (($this->QTypeID == qtpSingle && $this->IndicatorID > 0)) {
					$this->HasScores = true;
				}
				//@JAPR
				
				$sortOrder = $objQuestionOption->SortOrder;
				$displayText = $objQuestionOption->QuestionOptionName;
				$this->SelectOptions[] = $displayText;
				$this->QConsecutiveIDs[] = $objQuestionOption->QuestionOptionID;
				$this->Scores[] = $objQuestionOption->Score;
				$nextQuestionID = $objQuestionOption->NextQuestion;
				$this->NextQuestion[] = $nextQuestionID;
				//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
				//@JAPR 2013-08-16: Corregido un bug, no se estaba inicializando esta variable
				$intVisible = true;
				if (getMDVersion() >= esvHideAnswers) {
					$intVisible = $objQuestionOption->Visible;
				}
				$this->Visible[] = $intVisible;
				//@JAPR
				$intCancelAgenda = false;
				if (getMDVersion() >= esvAgendaRedesign) {
					$intCancelAgenda = $objQuestionOption->CancelAgenda;
				}
				$this->CancelAgenda[] = $intCancelAgenda;
				if (!$blnExcludeNonHeritableSettings) {
					$indDimID = $objQuestionOption->IndDimID;
					$this->QIndDimIds[] = $indDimID;
					$indicatorID = $objQuestionOption->IndicatorID;
					$this->QIndicatorIds[] = $indicatorID;
					//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
					//@JAPR 2013-03-11: Corregido un bug, el lugar donde graba el indicador depende del tipo de pregunta
					if (($this->QTypeID == qtpMulti && $indicatorID > 0)) {
						$this->HasScores = true;
					}
					//@JAPR
				}
				$this->CatDimValSelectOptions[] = $catDimVal;
				//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
				$this->ElemDimValSelectOptions[] = 0;
				//@JAPR 2012-10-11: Agregada la lectura de la imagen asociada para regresar en el servicio
				$this->DisplayImage[] = $objQuestionOption->DisplayImage;
				//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
				$this->HTMLText[] = $objQuestionOption->HTMLText;
				//@JAPR
				//conchita 2013-04-30
				if (getMDVersion() >= esvCallList) {
					$this->PhoneNum[] = $objQuestionOption->phoneNum;
				}
				
				if (getMDVersion() >= esvDefaultOption) {
					$this->OptionDefaultValue[] = $objQuestionOption->DefaultValue;
				}
			}
			//@JAPR
		}
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Movida la asignación como recorset para el único caso (descontinuado en v6) que aún cargaba la colección como recordset
		//@JAPR
		
		if (getMDVersion() >= esvDefaultOption) {
			//@JAPR 2014-05-27: Corregido un bug, sólo se debe forzar un defaultValue si es múltiple choice (porque al día de implementación
			//a nivel de pregunta no aplicaba para ellas) y si realmente hay por lo menos una opción de respuesta con un valor default
			if($this->QTypeID == qtpMulti && count($this->OptionDefaultValue) > 0) {
				if (trim(implode('', $this->OptionDefaultValue)) != '') {
					$this->DefaultValue = 1;
				}
			}
			//@JAPR
		}
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		$this->SelectOptionsCol = array();
        $this->QConsecutiveIDsCol = array();
        $this->ScoresCol = array();
        $this->ScoreOperatorsCol = array();
		$this->QIndDimIdsCol = array();
		$this->QIndicatorIdsCol = array();
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
			//Para las respuestas cuando se trata de un campo con captura como Matriz se requirió una tabla adicional
			$sql = "SELECT t1.ConsecutiveID, t1.SortOrder, t1.DisplayText, t1.IndDimID, t1.IndicatorID, t1.Score, t1.ScoreOperation 
				FROM SI_SV_QAnswersCol t1 
				WHERE t1.QuestionID = ".$intSourceQuestionID." ORDER BY t1.SortOrder";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			while(!$aRS->EOF)
			{
				$sortOrder = (int)$aRS->fields["sortorder"];
				$displayText = $aRS->fields["displaytext"];
				$this->SelectOptionsCol[] = $displayText;
				$this->QConsecutiveIDsCol[] = (int)$aRS->fields["consecutiveid"];
				$this->ScoresCol[] = (float)$aRS->fields["score"];
				$this->ScoreOperatorsCol[] = (int)$aRS->fields["scoreoperation"];
				if (!$blnExcludeNonHeritableSettings) {
					$indDimID = (int) $aRS->fields["inddimid"];
					$this->QIndDimIdsCol[] = $indDimID;
					$indicatorID = (int)$aRS->fields["indicatorid"];
					$this->QIndicatorIdsCol[] = $indicatorID;
				}
				$aRS->MoveNext();
			}
		}
		//@JAPR
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		//@JAPR 2015-06-18: Agregada la edición de datos desde la ventana de rediseño
		//Se leerá directo del request, en caso de no venir se leerá del objeto enviado como parámetro para procesar el request
		$aSectionID = getParamValue('SectionID', 'both', '(int)', true);
		if (is_null($aSectionID)) {
			if (array_key_exists("SectionID", $aHTTPRequest->GET))
			{
				$aSectionID = (int) $aHTTPRequest->GET["SectionID"];
			}
			else
			{
				$aSectionID = 0;
			}
		}
		//@JAPR
		
		if (array_key_exists("QuestionID", $aHTTPRequest->POST))
		{
			$aQuestionID = $aHTTPRequest->POST["QuestionID"];
			
			if (is_array($aQuestionID))
			{
				$aCollection = BITAMQuestionCollection::NewInstanceBySection($aRepository, $aSectionID, $aQuestionID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				
				$anInstance = BITAMQuestion::NewInstance($aRepository, $aSectionID);
				
				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aQuestionID, true);
				if (is_null($anInstance))
				{
					//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
					$anInstance = $strCalledClass::NewInstance($aRepository, $aSectionID);
				}
				//@JAPR 2014-07-31: Agregado el Responsive Design
				else {
					//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
					//Estas propiedades están descontinuadas a partir de eForms v6
					if (getMDVersion() < esveFormsv6 ) {
						if (getMDVersion() >= esvResponsiveDesign) {
							//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
							$strCalledClass::GetResponsiveDesignProps($aRepository, array($anInstance->QuestionID => $anInstance));
						}
					}
					//@JAPR
				}
				//@JAPR
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
						$anInstance = $strCalledClass::NewInstance($aRepository, $aSectionID);
					}
				}
				
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("QuestionID", $aHTTPRequest->GET))
		{
			$aQuestionID = $aHTTPRequest->GET["QuestionID"];
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aQuestionID, true);
			
			if (is_null($anInstance))
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstance($aRepository, $aSectionID);
			}
			//@JAPR 2014-07-31: Agregado el Responsive Design
			else {
				//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
				//Estas propiedades están descontinuadas a partir de eForms v6
				if (getMDVersion() < esveFormsv6 ) {
					if (getMDVersion() >= esvResponsiveDesign) {
						//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
						$strCalledClass::GetResponsiveDesignProps($aRepository, array($anInstance->QuestionID => $anInstance));
					}
				}
				//@JAPR
			}
			//@JAPR
		}
		else
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstance($aRepository, $aSectionID);
		}
		
		return $anInstance;
	}

	//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
	public function isOpenType()
	{
		switch ($this->QTypeID)
		{
			case qtpSingle:
			case qtpMulti:
			case qtpCallList:
				//conchita 2013-04-29 Agregado tipo call list
			//@JAPR 2013-04-12: Agregada la opción de visualización de preguntas tipo Foto
			case qtpPhoto:
				$blnIsOpenType = false;
				break;
			default:
				$blnIsOpenType = true;
				break;
		}
		
		return $blnIsOpenType;
	}
	//@JAPR
	
	function updateFromArray($anArray) {
		//@JAPR 2015-06-13: Rediseñado el Admin con DHTMLX
		global $gbDesignMode;
		//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		global $garrUpdatedPropsWithVars;
		if (!isset($garrUpdatedPropsWithVars)) {
			$garrUpdatedPropsWithVars = array();
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"])) {
			$garrUpdatedPropsWithVars["Questions"] = array();
		}
		//@JAPR
		
		if (array_key_exists("QuestionID", $anArray))
		{
			$this->QuestionID = (int)$anArray["QuestionID"];
		}
		//Conchita 2014-09-09 agregados width y height para las tipo sketch
		//OMMC 2017-01-19: Agregado el trim para los valores string.
		if (array_key_exists("canvasWidth", $anArray))
		{
			$this->canvasWidth = trim((string)$anArray["canvasWidth"]);
		}
		if (array_key_exists("canvasHeight", $anArray))
		{
			$this->canvasHeight = trim((string)$anArray["canvasHeight"]);
		}

		if (array_key_exists("SectionID", $anArray))
		{
			$this->SectionID = (int)$anArray["SectionID"];
		}
		
 		if (array_key_exists("QuestionText", $anArray))
		{
			$this->QuestionText = rtrim($anArray["QuestionText"]);
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->QuestionText = $this->TranslateVariableQuestionNumbersByIDs($this->QuestionText, optyLabel);
			//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$garrUpdatedPropsWithVars["Questions"]["QuestionText"] = 1;
			//@JAPR
		}
		
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			if (array_key_exists("QuestionTextHTML", $anArray))
			{
				//Si existe la propiedad con la etiqueta tipo HTML, quiere decir que se entró por modo de diseño, así que hay que asignar la etiqueta que no es HTML a partir de este valor
				$this->QuestionTextHTML = rtrim($anArray["QuestionTextHTML"]);
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->QuestionTextHTML = $this->TranslateVariableQuestionNumbersByIDs($this->QuestionTextHTML, optyLabel);
				//strip_tags(html_entity_decode($text))	<< Utilizar html_entity_decode si hay problema con escapes de caracteres HTML
				//@JAPR 2015-10-08: Corregido un bug, por alguna razón cuando se copia un texto de una celda de MS Excel y se pega como nombre de una pregunta en el DHTMLX Editor,
				//lo está pegando como una table de HTML con cierto formato y eso provocaba <enters> que dañaban el despliegue de los reportes, así que se removerán en este punto
				//que es el único lugar que asignaría esta propiedad (como nota, no se puede pegar otro tipo de HTML directo, ni el DHTMLX Editor genera nunca este caso, es solo con
				//la situación descrita que sucede)
				//@JAPR 2015-10-14: Corregido un bug, al remover el HTML podrían haber quedado espacios en dicho formato como &nbsp; que se desplegaban literal en ciertos procesos (#WN65X7)
				//@JAPR 2015-11-12: Corregido un bug, el resto de las entidades HTML estaban dejandose codificadas, así que mejor se reemplazarán todas (#WN65X7)
				$this->QuestionText = trim(str_replace("\n", "", str_replace("\r", "", html_entity_decode(strip_tags($this->QuestionTextHTML)))));
				//OMMC 2016-05-02: Agregada la corrección de nombres de pregunta con múltiples espacios
				$this->QuestionText = mb_ereg_replace("\\".chr(194).chr(160), ' ', $this->QuestionText);
				$blnDoNotEnter = true;
				//$this->QuestionText = trim(str_replace("\n", "", str_replace("\r", "", str_ireplace("&nbsp;", " ", strip_tags($this->QuestionTextHTML)))));
				//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
				$garrUpdatedPropsWithVars["Questions"]["QuestionTextHTML"] = 1;
				$garrUpdatedPropsWithVars["Questions"]["QuestionText"] = 1;
				//@JAPR
			}
		}
		//@JAPR
		
		if (array_key_exists("QComment", $anArray))
		{
			$this->QComment = rtrim($anArray["QComment"]);
		}
		
		if (array_key_exists("QActions", $anArray))
		{
			$this->QAction = rtrim($anArray["QActions"]);
		}
		
		if(array_key_exists("addvalidation", $anArray))
		{
			$this->HasValidationNew = 1;
		}

		if (array_key_exists("MinValue", $anArray))
		{
			$this->MinValue = trim($anArray["MinValue"]);
		}
		
 		if (array_key_exists("MaxValue", $anArray))
		{
			$this->MaxValue = trim($anArray["MaxValue"]);
		}
		
 		if (array_key_exists("MinDate", $anArray))
		{
			$this->MinDate = substr(trim($anArray["MinDate"]), 0, 10);
		}
		
 		if (array_key_exists("MaxDate", $anArray))
		{
			$this->MaxDate = substr(trim($anArray["MaxDate"]), 0, 10);
		}
		
		if (array_key_exists("AttributeName", $anArray))
		{
			$this->AttributeName = rtrim($anArray["AttributeName"]);
		}
		
		if (array_key_exists("IsFieldSurveyID", $anArray))
		{
			$this->IsFieldSurveyID = (int)$anArray["IsFieldSurveyID"];
		}
		
		if(array_key_exists("IsIndicator", $anArray))
		{
			$this->IsIndicatorNew = 1;
		}
		
		if (array_key_exists("IsReqQuestion", $anArray))
		{
			$this->IsReqQuestion = (int)$anArray["IsReqQuestion"];
		}
		//Conchita agregado isinvisible para preguntas que no se mostraran en la app
		//2014-12-01
		if (array_key_exists("IsInvisible", $anArray))
		{
			$this->IsInvisible = (int)$anArray["IsInvisible"];
		}
		
		if (array_key_exists("HasReqComment", $anArray))
		{
			//OMMC 2016-02-22: Validado para ver si es una nueva pregunta, si es así y el check está activo, se crea como opcional: 2, anteriormente se creaba como Sí: 1, haciéndola siempre requerida.
			//$this->HasReqComment = (int)$anArray["HasReqComment"];
			//@JAPR 2015-06-13: Rediseñado el Admin con DHTMLX
			//En modo diseño se captura mediante checkbox, así que no hay forma de variar este dato
			/*if ($gbDesignMode && $this->HasReqComment) {
				$this->HasReqComment = 2;
			}*/
			if ($this->isNewObject() && ((int)$anArray["HasReqComment"] == 1)) {
				$this->HasReqComment = 2;
			}else{
				$this->HasReqComment = (int)$anArray["HasReqComment"];	
			}
			//@JAPR
		}
		//conchita agregados width y height para las sketch
		//@JAPR 2016-04-14: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
		//Removida la asignación duplicada de este valor, ya que estaba además haciendo una conversión a entero
		/*if (array_key_exists("canvasWidth", $anArray))
		{
			$this->canvasWidth = (int)$anArray["canvasWidth"];
		}

		if (array_key_exists("canvasHeight", $anArray))
		{
			$this->canvasHeight = (int)$anArray["canvasHeight"];
		}
		*/
		//@JAPR

		if (array_key_exists("HasReqPhoto", $anArray))
		{
			//@JAPR 2015-06-13: Rediseñado el Admin con DHTMLX
			//En modo diseño se captura mediante checkbox, así que no hay forma de variar este dato
			//@JAPR 2015-08-15: Corregido un bug, ya no es un checkbox sino una lista, debe guardar el valor tal como llega
			//if ($gbDesignMode && $this->HasReqPhoto) {
			//	$this->HasReqPhoto = 2;
			//}
			//@JAPR
			//OMMC 2015-11-13: Validado para ver si es una nueva pregunta, si es así y el check está activo, se crea como opcional: 2, anteriormente se creaba como Sí: 1, haciéndola siempre requerida.
			if ($this->isNewObject() && ((int)$anArray["HasReqPhoto"] == 1)) {
				$this->HasReqPhoto = 2;
			}else{
				$this->HasReqPhoto = (int)$anArray["HasReqPhoto"];	
			}
		}
		
		if (array_key_exists("HasReqDocument", $anArray))
		{
			$this->HasReqDocument = (int)$anArray["HasReqDocument"];
		}
		
		if (array_key_exists("HasActions", $anArray))
		{
			$this->HasActions = (int)$anArray["HasActions"];
		}
		
		if (array_key_exists("QDisplayMode", $anArray))
		{
			$this->QDisplayMode = (int)$anArray["QDisplayMode"];
		}
		
		//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
		$blnAssignTypeRelatedProp = false;
		if (array_key_exists("QTypeID", $anArray))
		{
			$this->QTypeID = (int)$anArray["QTypeID"];
			//@JAPR 2015-07-18: Validado para que no asigne esta bandera ya que esa controlaba validaciones aplicables para la V5 o anteriores que en v6 no tienen mucho sentido
			//dado los nuevos campos de DataSources por ejemplo
			if (!$gbDesignMode) {
				$blnAssignTypeRelatedProp = true;
			}
			//@JAPR
		}

		//@JAPR 2012-04-30: Agregada la opción para configurar el estilo visual del componente (Campo QComponentStyleID)
		if (array_key_exists("QComponentStyleID", $anArray))
		{
			$this->QComponentStyleID = (int)$anArray["QComponentStyleID"];
		}
		//@JAPR
		
		if (array_key_exists("GQTypeID", $anArray))
		{
			//Se asume que si se recibió el GQTypeID entonces por fuerza existe el QDataType, ya que sin él no sería posible asignar el QTypeID
			if (array_key_exists("QDataType", $anArray))
			{
				$this->QDataType = (int)$anArray["QDataType"];
			}
			$this->GQTypeID = (int)$anArray["GQTypeID"];
			switch ($this->GQTypeID)
			{
				case qtpOpen:
					$this->QTypeID = $this->QDataType;
					break;
				default:
					$this->QTypeID = $this->GQTypeID;
					break;
			}
			
			if($this->GQTypeID==qtpMulti)
			{
				$this->IsMultiDimension = (int) @$anArray["IsMultiDimension"];
			}
			else 
			{
				$this->IsMultiDimension = 0;
			}
			
			//@JAPR 2015-07-18: Validado para que no asigne esta bandera ya que esa controlaba validaciones aplicables para la V5 o anteriores que en v6 no tienen mucho sentido
			//dado los nuevos campos de DataSources por ejemplo
			if (!$gbDesignMode) {
				$blnAssignTypeRelatedProp = true;
			}
			//@JAPR
		}
		
		//@JAPR 2011-06-29: Agregada la longitud de los campos Alfanumericos
		if (array_key_exists("QLength", $anArray))
		{
			$this->QLength = (int)$anArray["QLength"];
			
			//Se asume que si se recibe el QLength por fuerza existe el QDataType ya que no tiene sentido de otra forma
			if ($this->QLength <= 0)
			{
				if (array_key_exists("QDataType", $anArray))
				{
					$this->QDataType = (int)$anArray["QDataType"];
					if ($this->QDataType == dtpAlpha)
					{
						$this->QLength = DEFAULT_ALPHA_LENGTH;
					}
				}
				else {
					if (array_key_exists("QTypeID", $anArray))
					{
						$intQTypeID = (int)$anArray["QTypeID"];
						//@JAPR 2016-06-15: Modificado el comportamiento del tamaño del texto de preguntas BarCode para que sea como las alfanuméricas (#TEI1OO)
						if ($intQTypeID == qtpOpenAlpha || $intQTypeID == qtpBarCode)
						{
							$this->QLength = DEFAULT_ALPHA_LENGTH;
						}
					}
				}
			}
		}
		//@JAPR
		
		//@JAPR 2015-07-18: Validado para que no asigne esta bandera ya que esa controlaba validaciones aplicables para la V5 o anteriores que en v6 no tienen mucho sentido
		//dado los nuevos campos de DataSources por ejemplo
		if ($blnAssignTypeRelatedProp) {
			if($this->QTypeID==qtpMulti) {
				//Si la pregunta pertenece a una seccion dinamica debemos preguntar 
				$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
				
				//Verificamos antes si la pregunta multiple choice es de una
				//seccion dinamica y esta dentro de la seccion para limpiar valores
				//que puedan venir como basura y se requiere limpiar para grabar datos correctos
				//if($this->SectionID==$dynamicSectionID && $this->OutsideOfSection==0)
				if($this->SectionID==$dynamicSectionID) {
					$this->UseCatalog = 0;
					$this->UseCatToFillMC = 0;
					$this->IsMultiDimension = 0;
					$this->StrSelectOptions = "";
					$this->StrSelectOptionsCol = "";
					$this->CatalogID = 0;
					$this->CatMemberID = 0;
					$this->MCInputType = (int) @$anArray["MCInputType"];
					$this->OneChoicePer = 0;
					
					$this->UseCategoryDimChoice = 0;
					$this->UseCategoryDim = 0;
					$this->CategoryDimName = "";
					$this->ElementDimName = "";
					$this->ElementIndName = "";
				}
				else {
					$this->UseCatToFillMC = (int) @$anArray["UseCatToFillMC"];
	
					if(array_key_exists("MCInputType", $anArray) && $this->QDisplayMode == dspVertical) {
						$this->MCInputType = (int)$anArray["MCInputType"];
					}
					
					if($this->UseCatToFillMC==0) {
						$this->StrSelectOptions = trim((string) @$anArray["StrSelectOptions"]);
						//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
						$this->StrSelectOptionsCol = trim((string) @$anArray["StrSelectOptionsCol"]);
						//@JAPR
						$this->CatalogID = 0;
						$this->CatMemberID = 0;
						$this->UseCatalog = 0;
						$this->OneChoicePer = 0;
						
						$this->UseCategoryDimChoice = (int) @$anArray["UseCategoryDimChoice"];

						//Revisaremos el valor de QDisplayMode, MCInputType, IsMultiDimension y UseCategoryDimChoice para ver si es necesario
						//aplicar los valores a las propiedades de Category Dimension
						if($this->QDisplayMode==dspVertical && $this->MCInputType==mpcNumeric && $this->IsMultiDimension==0 && $this->UseCategoryDimChoice!=0) {
							$this->UseCategoryDim = 1;
							$this->CategoryDimName = trim((string) @$anArray["CategoryDimName"]);
							$this->ElementDimName = trim((string) @$anArray["ElementDimName"]);
							$this->ElementIndName = trim((string) @$anArray["ElementIndName"]);
							
							$this->HasReqComment = 0;
							$this->HasReqPhoto = 0;
							$this->HasReqDocument = 0;
						}
						else {
							$this->UseCategoryDimChoice = 0;
							$this->UseCategoryDim = 0;
							$this->CategoryDimName = "";
							$this->ElementDimName = "";
							$this->ElementIndName = "";
						}
					}
					else {
						if(array_key_exists("CatalogID", $anArray) && ((int)$anArray["CatalogID"])>0) {
							//Obtenemos todos los valores de dicho catalogo de 1 nivel para
							//llenar el campo StrSelectOptions
							$this->CatalogID = (int)$anArray["CatalogID"];
							$arrayCatMembers = $this->getArrayCatalogMembers($this->CatalogID);
							
							if(count($arrayCatMembers)>0) {
								//Salto de linea
								$LN = chr(13).chr(10);
								$this->StrSelectOptions = implode($LN, $arrayCatMembers);
								$this->StrSelectOptionsCol = "";
								$this->UseCatalog = 0;
								$this->CatMemberID = 0;
								$this->OneChoicePer = 0;
							}
							else {
								$this->QTypeID=qtpOpenString;
								$this->UseCatalog = 0;
								$this->StrSelectOptions = "";
								//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
								$this->StrSelectOptionsCol = "";
								//@JAPR
								$this->CatalogID = 0;
								$this->CatMemberID = 0;
								$this->IsMultiDimension = 0;
								$this->UseCatToFillMC = 0;
								$this->OneChoicePer = 0;
							}
						}
						else {
							//Si de plano llegan valores invalidos de CatalogID entonces hacer que la pregunta sea de tipo TextBox
							$this->QTypeID=qtpOpenString;
							$this->UseCatalog = 0;
							$this->StrSelectOptions = "";
							//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
							$this->StrSelectOptionsCol = "";
							//@JAPR
							$this->CatalogID = 0;
							$this->CatMemberID = 0;
							$this->IsMultiDimension = 0;
							$this->UseCatToFillMC = 0;
							$this->OneChoicePer = 0;
						}
						
						$this->UseCategoryDimChoice = 0;
						$this->UseCategoryDim = 0;
						$this->CategoryDimName = "";
						$this->ElementDimName = "";
						$this->ElementIndName = "";
					}
				}
			}
			else if($this->QTypeID==qtpSingle || $this->QTypeID==qtpCallList) {
				$this->UseCatalog = (int) @$anArray["UseCatalog"];

				if($this->UseCatalog==1) {
					if(array_key_exists("CatalogID", $anArray) && array_key_exists("CatMemberID", $anArray) && ((int)$anArray["CatalogID"])>0 && ((int)$anArray["CatMemberID"])>0) {
						$this->StrSelectOptions = "";
						//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
						$this->StrSelectOptionsCol = "";
						//@JAPR
						$this->CatalogID = (int)$anArray["CatalogID"];
						$this->CatMemberID = (int)$anArray["CatMemberID"];
						
						$this->CatMemberLatitudeID = (int)@$anArray["CatMemberLatitudeID"];
						$this->CatMemberLongitudeID = (int)@$anArray["CatMemberLongitudeID"];
						$this->UseMap = (int)@$anArray["UseMap"];
						$this->MapRadio = (int)@$anArray["MapRadio"];
						
						$this->IsMultiDimension = 0;
						$this->UseCatToFillMC = 0;
						$this->MCInputType = mpcCheckBox;
						$this->OneChoicePer = 0;
					}
					else {
						//Si de plano llegan valores invalidos de CatalogID y CatMemberID entonces hacer que la pregunta sea de tipo TextBox
						$this->QTypeID=qtpOpenString;
						$this->UseCatalog = 0;
						$this->StrSelectOptions = "";
						//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
						$this->StrSelectOptionsCol = "";
						//@JAPR
						$this->CatalogID = 0;
						$this->CatMemberID = 0;
						
						$this->CatMemberLatitudeID = 0;
						$this->CatMemberLongitudeID = 0;
						$this->UseMap = 0;
						$this->MapRadio = 0;
						
						$this->IsMultiDimension = 0;
						$this->UseCatToFillMC = 0;
						$this->MCInputType = mpcCheckBox;
						$this->OneChoicePer = 0;
					}
				}
				else {
					$this->StrSelectOptions = trim((string) @$anArray["StrSelectOptions"]);
					
					if($this->QDisplayMode==dspMatrix) {
						//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
						$this->StrSelectOptionsCol = trim((string) @$anArray["StrSelectOptionsCol"]);
						$this->OneChoicePer = (int) @$anArray["OneChoicePer"];
					}
					else {
						$this->StrSelectOptionsCol = "";
						$this->OneChoicePer = 0;
					}

					$this->CatalogID = 0;
					$this->CatMemberID = 0;
					$this->IsMultiDimension = 0;
					$this->UseCatToFillMC = 0;
					$this->MCInputType = mpcCheckBox;
				}
				
				$this->UseCategoryDimChoice = 0;
				$this->UseCategoryDim = 0;
				$this->CategoryDimName = "";
				$this->ElementDimName = "";
				$this->ElementIndName = "";
			}
			else if($this->QTypeID==qtpShowValue) {
				if(array_key_exists("CatalogID", $anArray) && array_key_exists("CatMemberID", $anArray) && array_key_exists("TextDisplayStyle", $anArray) && ((int)$anArray["CatalogID"])>0 && ((int)$anArray["CatMemberID"])>0) {
					$this->CatalogID = (int)$anArray["CatalogID"];
					$this->CatMemberID = (int)$anArray["CatMemberID"];
					$this->TextDisplayStyle = (int)$anArray["TextDisplayStyle"];

					$this->StrSelectOptions = "";
					$this->StrSelectOptionsCol = "";
					$this->UseCatalog = 0;
					$this->IsMultiDimension = 0;
					$this->UseCatToFillMC = 0;
					$this->MCInputType = mpcCheckBox;
					$this->OneChoicePer = 0;
				}
				
				$this->UseCategoryDimChoice = 0;
				$this->UseCategoryDim = 0;
				$this->CategoryDimName = "";
				$this->ElementDimName = "";
				$this->ElementIndName = "";
			}
			else {
				$this->StrSelectOptions = "";
				//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
				$this->StrSelectOptionsCol = "";
				//@JAPR
				$this->CatalogID = 0;
				$this->CatMemberID = 0;
				$this->UseCatalog = 0;
				$this->IsMultiDimension = 0;
				$this->UseCatToFillMC = 0;
				$this->MCInputType = mpcCheckBox;
				$this->OneChoicePer = 0;
				
				$this->UseCategoryDimChoice = 0;
				$this->UseCategoryDim = 0;
				$this->CategoryDimName = "";
				$this->ElementDimName = "";
				$this->ElementIndName = "";
				
				//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
				//Sólo aplica si la pregunta está dentro de una sección dinámica, pero eso se valida en el Servicio
				if ($this->QTypeID == qtpOpenNumeric)
				{
					$this->UseCatalog = (int) @$anArray["UseCatalog"];
					
					if($this->UseCatalog==1)
					{
						if(array_key_exists("CatalogID", $anArray) && array_key_exists("CatMemberID", $anArray) && ((int)$anArray["CatalogID"])>0 && ((int)$anArray["CatMemberID"])>0)
						{
							$this->CatalogID = (int)$anArray["CatalogID"];
							$this->CatMemberID = (int)$anArray["CatMemberID"];
						}
					}
				}
				//@JAPR
			}
		}
		else {
			//En este caso asigna las propiedades tal como lleguen, ya que cada una se graba de manera independiente por lo general, así que todas aplican incluso si causan inconsistencias
			//pues el esquema de validaciones cambió completamente en esta versión
			if (array_key_exists("MCInputType", $anArray)) {
				$this->MCInputType = (int)$anArray["MCInputType"];
			}
			
			if (array_key_exists("StrSelectOptions", $anArray)) {
				$this->StrSelectOptions = (string)$anArray["StrSelectOptions"];
			}
			//CatalogID	Se validará mas abajo
			//CatMemberID Se validará mas abajo
			//UseCatalog Se asignará automáticamente
			if (array_key_exists("CatMemberLatitudeID", $anArray)) {
				$this->CatMemberLatitudeID = (int)$anArray["CatMemberLatitudeID"];
			}
			
			if (array_key_exists("CatMemberLongitudeID", $anArray)) {
				$this->CatMemberLongitudeID = (int)$anArray["CatMemberLongitudeID"];
			}
			
			if (array_key_exists("UseMap", $anArray)) {
				$this->UseMap = (int)$anArray["UseMap"];
			}
			
			if (array_key_exists("MapRadio", $anArray)) {
				$this->MapRadio = (int)$anArray["MapRadio"];
			}
		}
		
		if (array_key_exists("QDataType", $anArray))
		{
			$this->QDataType = (int)$anArray["QDataType"];
		}
		
		//@JAPR
		
		if (array_key_exists("SectionID", $anArray))
		{
			$this->SectionID = (int)$anArray["SectionID"];
		}

		if (array_key_exists("OutsideOfSection", $anArray))
		{
			$this->OutsideOfSection = (int)$anArray["OutsideOfSection"];
		}

		//Conchita 18-oct-2011
		if (array_key_exists("GoToQuestion", $anArray))
		{
			$this->GoToQuestion = (int)$anArray["GoToQuestion"];
		}
		
		if(array_key_exists("IsIndicatorMC", $anArray))
		{
			$this->IsIndicatorMCNew = 1;
		}

		//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales
		if(array_key_exists("AllowAdditionalAnswers", $anArray))
		{
			//@JAPR 2012-04-25: Corregido un bug, siempre se estaba asignando como verdadero esta opción
			$this->AllowAdditionalAnswers = (int)$anArray["AllowAdditionalAnswers"];
		}
		
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		if(array_key_exists("OtherLabel", $anArray))
		{
			//@JAPR 2012-04-25: Corregido un bug, siempre se estaba asignando como verdadero esta opción
			$this->OtherLabel = (string)$anArray["OtherLabel"];
		}
		
		if(array_key_exists("CommentLabel", $anArray))
		{
			//@JAPR 2012-04-25: Corregido un bug, siempre se estaba asignando como verdadero esta opción
			$this->CommentLabel = (string)$anArray["CommentLabel"];
		}
		//@JAPR
		
		//@JAPR 2012-05-15: Agregadas las preguntas tipo Action (Campos ResponsibleID, CategoryID, DaysDueDate)
		if(array_key_exists("ResponsibleID", $anArray))
		{
			$this->ResponsibleID = (int)$anArray["ResponsibleID"];
		}
		
		if(array_key_exists("CategoryID", $anArray))
		{
			$this->CategoryID = (int)$anArray["CategoryID"];
		}
		
		if(array_key_exists("DaysDueDate", $anArray))
		{
			$this->DaysDueDate = (int)$anArray["DaysDueDate"];
		}
		
		//@JAPR 2012-05-29: Agregada la opción para indicar el número de decimales de las preguntas numéricas
		//@JAPR 2012-05-29: Agregado el soporte para creación de preguntas (single y multi) cuyas respuestas dependan de las seleccionadas de una pregunta multi previa
		if (array_key_exists("DecimalPlaces", $anArray))
		{
			$this->DecimalPlaces = (int) $anArray["DecimalPlaces"];
		}
		
		if (array_key_exists("SourceQuestionID", $anArray))
		{
			$this->SourceQuestionID = (int) $anArray["SourceQuestionID"];
		}

		//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		if (array_key_exists("SharedQuestionID", $anArray)) {
			$this->SharedQuestionID = (int) $anArray["SharedQuestionID"];
		}
		
		//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas (campo FormatMask)
		if (array_key_exists("FormatMaskID", $anArray))
		{
			$intFormatID = (int)$anArray["FormatMaskID"];
			if ($intFormatID == 1)
			{
				if (array_key_exists("FormatMask", $anArray))
				{
					$this->getFormatID($anArray["FormatMask"]);
				}
				else 
				{
					$this->getFormatID('');
				}
			}
			else 
			{
				$this->setFormatMask($intFormatID);
			}
			
			if (trim($this->FormatMask) == '')
			{
				$this->getFormatID('');
			}
		}
		
		//@JAPR 2015-07-03: Rediseñado el Admin con DHTMLX
		//En modo diseño el formato se puede especificar directo, no necesariamente de una lista
		if ($gbDesignMode) {
			if (array_key_exists("FormatMask", $anArray))
			{
				if ($this->QTypeID != qtpOpenDate) {
					$this->FormatMask = (string) $anArray["FormatMask"];
				}
			}
			
			if (array_key_exists("DateFormatMask", $anArray))
			{
				if ($this->QTypeID == qtpOpenDate) {
					$this->DateFormatMask = (string) $anArray["DateFormatMask"];
				}
			}
		}
		//@JAPR
        
        if (array_key_exists("MaxChecked", $anArray))
		{
			$this->MaxChecked = (int) $anArray["MaxChecked"];
		}
		
		if (array_key_exists("QuestionImageText", $anArray))
		{
			$this->QuestionImageText = $anArray["QuestionImageText"];
		}
		
		if (array_key_exists("QuestionMessage", $anArray))
		{
			$this->QuestionMessage = $anArray["QuestionMessage"];
			//@JAPR 2016-01-12: Agregado el encadenamiento de refresh a las preguntas tipo Mensaje (siempre) / HTML (solo si tiene Auto-Redraw)
			if(stripos($this->QuestionMessage, '@Q') || stripos($this->QuestionMessage, '$Q')) {
				$this->HasQuestionDep = 1;
			}
			
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->QuestionMessage = $this->TranslateVariableQuestionNumbersByIDs($this->QuestionMessage, optyFormattedText);
			//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$garrUpdatedPropsWithVars["Questions"]["QuestionMessage"] = 1;
			//@JAPR
		}
		
		//@JAPR 2015-08-15: Agregado el ipo de pregunta HTML
		if (array_key_exists("HTMLCode", $anArray))
		{
			$this->QuestionMessage = $anArray["HTMLCode"];
			//@JAPR 2016-01-12: Agregado el encadenamiento de refresh a las preguntas tipo Mensaje (siempre) / HTML (solo si tiene Auto-Redraw)
			if(stripos($this->QuestionMessage, '@Q') || stripos($this->QuestionMessage, '$Q')) {
				$this->HasQuestionDep = 1;
			}
			
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->QuestionMessage = $this->TranslateVariableQuestionNumbersByIDs($this->QuestionMessage, optyFormattedText);
			//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$garrUpdatedPropsWithVars["Questions"]["QuestionMessage"] = 1;
			//@JAPR
		}
		//@JAPR
		
		//conchita agregado el questionimagesketch 2014-10-11
		if (array_key_exists("QuestionImageSketch", $anArray))
		{
			$this->QuestionImageSketch = $anArray["QuestionImageSketch"];
		}
		//conchita
		
		//OMMCWarning 2015-12-10: Estos no se deben de castear a (int) ya que las dimensiones de los canvas pueden ser por porcentaje o pixeles, por lo que se almacenan en la base de datos como varchars
		//Esto es diferente a los anchos de las columnas en secciones tabla cuyo valor es (int). 
		//@JAPR 2016-04-14: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
		//Removida la asignación duplicada de este valor, ya que estaba además equivocado el Case de la propiedad
		/*if (array_key_exists("CanvasWidth", $anArray))
		{
			$this->CanvasWidth = $anArray["CanvasWidth"];
		}
		if (array_key_exists("CanvasHeight", $anArray))
		{
			$this->CanvasHeight = $anArray["CanvasHeight"];
		}
		*/
		//@JAPR
    	if (array_key_exists("DateFormatMaskID", $anArray))
		{
			$intDateFormatID = (int)$anArray["DateFormatMaskID"];
			$this->setDateFormatMask($intDateFormatID);
			
			if (trim($this->DateFormatMask) == '')
			{
				$this->getDateFormatID('');
			}
		}
		
		if (array_key_exists("DefaultValue", $anArray))
		{
			$this->DefaultValue = $anArray["DefaultValue"];
			//@JAPR 2014-10-23: Corregido un bug, faltaba considerar al nuevo formato de preguntas
			if(stripos($this->DefaultValue, '@Q') || stripos($this->DefaultValue, '$Q')) {
				$this->HasQuestionDep = 1;
			}
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->DefaultValue = $this->TranslateVariableQuestionNumbersByIDs($this->DefaultValue, optyDefaultValue);
			//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$garrUpdatedPropsWithVars["Questions"]["DefaultValue"] = 1;
			//@JAPR
		}
		
		if (array_key_exists("ExcludeSelected", $anArray))
		{
			$this->ExcludeSelected = (int) $anArray["ExcludeSelected"];
		}
		
		if (array_key_exists("SourceSimpleQuestionID", $anArray))
		{
			$this->SourceSimpleQuestionID = (int) $anArray["SourceSimpleQuestionID"];
		}
		
		if (array_key_exists("SummaryTotal", $anArray))
		{
			$this->SummaryTotal = (int) $anArray["SummaryTotal"];
		}
		
		//@JAPR 2012-10-19: Agregadas las preguntas marcadas como Read-Only
		if(array_key_exists("ReadOnly", $anArray))
		{
			//@JAPR 2012-04-25: Corregido un bug, siempre se estaba asignando como verdadero esta opción
			$this->ReadOnly = (int)$anArray["ReadOnly"];
		}
		
		if (array_key_exists("Formula", $anArray))
		{
			$this->Formula = (string) $anArray["Formula"];
			if (getMDVersion() >= esveFormsV41Func) {
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->Formula = $this->TranslateVariableQuestionNumbersByIDs($this->Formula, optyFormula);
				//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
				$garrUpdatedPropsWithVars["Questions"]["Formula"] = 1;
				//@JAPR
			}
		}
		
		if (array_key_exists("ShowCondition", $anArray))
		{
			$this->ShowCondition = (string) $anArray["ShowCondition"];
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esveFormsV41Func) {
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->ShowCondition = $this->TranslateVariableQuestionNumbersByIDs($this->ShowCondition, optyShowCondition);
				//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
				$garrUpdatedPropsWithVars["Questions"]["ShowCondition"] = 1;
				//@JAPR
			}
		}
		
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		if (array_key_exists("ActionText", $anArray))
		{
			$this->ActionText = $anArray["ActionText"];
			if (getMDVersion() >= esvExtendedActionsData) {
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->ActionText = $this->TranslateVariableQuestionNumbersByIDs($this->ActionText, optyActionText);
			}
		}
		
		if (array_key_exists("ActionTitle", $anArray))
		{
			$this->ActionTitle = $anArray["ActionTitle"];
			if (getMDVersion() >= esvExtendedActionsData) {
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->ActionTitle = $this->TranslateVariableQuestionNumbersByIDs($this->ActionTitle, optyActionTitle);
			}
		}
		
		//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
		if (array_key_exists("eBavelActionFormID", $anArray))
		{
			$this->eBavelActionFormID = (int)$anArray["eBavelActionFormID"];
		}
		
		if (array_key_exists("eBavelActionFieldsData", $anArray))
		{
			$this->eBavelActionFieldsData = $anArray["eBavelActionFieldsData"];
		}
		
		//@JAPR 2012-12-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		if (array_key_exists("eBavelFieldID", $anArray)) {
			$this->eBavelFieldID = $anArray["eBavelFieldID"];
		}
		
		//@JAPR 2013-02-21: Agregada la opción para ocultar los textos de opciones de respuesta si tienen imagen asociada
		if (array_key_exists("ShowImageAndText", $anArray)) {
			$this->ShowImageAndText = $anArray["ShowImageAndText"];
		}
		
		//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		if (array_key_exists("RegisterGPSAt", $anArray)) {
			$this->RegisterGPSAt = $anArray["RegisterGPSAt"];
		}
		
		if (array_key_exists("LatitudeAttribID", $anArray)) {
			$this->LatitudeAttribID = $anArray["LatitudeAttribID"];
		}

		if (array_key_exists("LongitudeAttribID", $anArray)) {
			$this->LongitudeAttribID = $anArray["LongitudeAttribID"];
		}

		if (array_key_exists("AccuracyAttribID", $anArray)) {
			$this->AccuracyAttribID = $anArray["AccuracyAttribID"];
		}
		
		//@JAPR 2014-02-07: Agrupadas las opciones de llenado de preguntas simple y multiple choice
		if (array_key_exists("typeOfFillingSC", $anArray) && $this->QTypeID == qtpSingle) {
			//Se agregará una propiedad dinámica para saber que se está grabando mediante el FrameWork, así en el método Save se pueden aplicar
			//validaciones para limpiar valores que no deberían estar asignados
			$this->newTypeOfFilling = (int) @$anArray["typeOfFillingSC"];
		}
		if (array_key_exists("typeOfFillingMC", $anArray) && $this->QTypeID == qtpMulti) {
			$this->newTypeOfFilling = (int) @$anArray["typeOfFillingMC"];
		}

		//@JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
		if (array_key_exists("InputWidth", $anArray)) {
			$this->InputWidth = $anArray["InputWidth"];
		}
		
		if (array_key_exists("PictLayOut", $anArray)) {
			$this->PictLayOut = $anArray["PictLayOut"];
		}
		
		//@JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice
		if (array_key_exists("EnableCheckBox", $anArray)) {
			$this->EnableCheckBox = $anArray["EnableCheckBox"];
		}
		
		if (array_key_exists("SwitchBehaviour", $anArray)) {
			$this->SwitchBehaviour = $anArray["SwitchBehaviour"];
		}
		
		if (array_key_exists("ShowInSummary", $anArray)) {
			$this->ShowInSummary = $anArray["ShowInSummary"];
		}
		
		//@JAPR 2014-07-31: Agregado el Responsive Design
		if (getMDVersion() >= esvResponsiveDesign) {
			for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
				$strDeviceName = getDeviceNameFromID($intDeviceID);
				
				$strProperty = "QuestionMessage";
				$strFieldName = $strProperty.$strDeviceName;
				$strValue = '';
		 		if (array_key_exists($strFieldName, $anArray))
				{
					$strValue = $anArray[$strFieldName];
					//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
					//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
					$strValue = $this->TranslateVariableQuestionNumbersByIDs($strValue, optyFormattedText);
					//@JAPR
					$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue);
				}
				
				$strProperty = "QuestionImageText";
				$strFieldName = $strProperty.$strDeviceName;
				$strValue = '';
		 		if (array_key_exists($strFieldName, $anArray))
				{
					$strValue = $anArray[$strFieldName];
					$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue);
				}
				//Conchita agregado el questionimagesketch
				if (getMDVersion() >= esvSketchImage) {
					$strProperty = "QuestionImageSketch";
					$strFieldName = $strProperty.$strDeviceName;
					$strValue = '';
					if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue);
					}
					//conchita
				}
			}
		}
		
		//@JAPR 2014-09-29: Agregado el ancho de columnas en secciones Inline o el Summary de maestro-detalle
		//OMMC 2015-12-10: Faltaba castearlo a (int), si no se mandaba nada en el grid, arrojaba error en el query.
		//JAPR 2016-06-08: Modificada la propiedad para que ahora pueda ser un String (#WFLVTR)
		if (array_key_exists("ColumnWidth", $anArray)) {
			if (getMDVersion() >= esvColumnWidthString) {
				$this->ColumnWidth = substr((string) @$anArray["ColumnWidth"], 0, 20);
			}
			else {
				$this->ColumnWidth = (int) @$anArray["ColumnWidth"];
			}
		}
		
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		if (array_key_exists("SourceSectionID", $anArray)) {
			$this->SourceSectionID = $anArray["SourceSectionID"];
		}
		
		//@JAPR 2014-10-22: Agregada la opción para redibujar las preguntas tipo mensaje automáticamente
		if(array_key_exists("AutoRedraw",$anArray)) {
			$this->AutoRedraw = (int) $anArray["AutoRedraw"];
		}
		//@JAPR
		
		if (getMDVersion() >= esvEditorHTML)
		{
	 		$editor = 1;
	 		
			if (array_key_exists("EditorType", $anArray))
			{
				$this->EditorType = (int) $anArray["EditorType"];
			}
			
	 		if (array_key_exists("QuestionMessageDes", $anArray))
			{
				$this->QuestionMessageDes = $anArray["QuestionMessageDes"];
			}
			
	 		if (array_key_exists("QuestionMessageDesHTML", $anArray))
			{
				$this->QuestionMessageDesHTML = $anArray["QuestionMessageDesHTML"];
			}
			
			if (getMDVersion() >= esvResponsiveDesign) {
				for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
					$strDeviceName = getDeviceNameFromID($intDeviceID);
					
					$strProperty = "QuestionMessage";
					//Campo correspondiente al nuevo editor por eso termina en "Des"
					$strFieldName = $strProperty.$strDeviceName."Des";
					$strValue = '';
					
			 		if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue, $editor);
					}
					
					$strFieldName = $strProperty.$strDeviceName."Des"."HTML";
					$strValue = '';
					
			 		if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue, $editor, 1);
					}
				}
			}
		
		}
		
		//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
		//Ahora el tipo de llenado del componente será un campo mas en la metadata
		if (getMDVersion() >= esvAdminWYSIWYG) {
			if (array_key_exists("ValuesSourceType",$anArray)) {
				$this->ValuesSourceType = (int) $anArray["ValuesSourceType"];
				//Dependiendo del tipo de llenado, se desactivan algunas otras configuraciones
				//En este caso el proceso de grabado ya valida algunas cosas según el valor de la propiedad newTypeOfFilling, así que se asigna en este punto
				$this->newTypeOfFilling = $this->ValuesSourceType;
				if ($this->newTypeOfFilling == tofCatalog) {
					$this->UseCatalog = 1;
				}
				else {
					$this->UseCatalog = 0;
				}
				
				//@JAPR 2015-11-05: Corregido un bug, al cambiar el ValuesSourceType, se debe verificar si el QDisplayMode sigue siendo válido y en caso de no serlo resetearlo
				//a dspMenu, que a la fecha de implementación era el común denominador (esto para Simple Choice) (#8CJIDE)
				if ($this->QTypeID == qtpSingle) {
					$arrAvailableDisplayModes = array();
					if ($this->newTypeOfFilling == tofCatalog) {
						$arrAvailableDisplayModes[dspMenu] = dspMenu;
						$arrAvailableDisplayModes[dspAutocomplete] = dspAutocomplete;
						//@JAPR 2015-11-11: Corregido un bug, con la validación descrita arriba al cambiar el tipo de llenado, se le pegó a la creación de preguntas GetData ya que no
						//estaban incluídas en el array de tipos válidos (#8CJIDE)
						$arrAvailableDisplayModes[dspGetData] = dspGetData;
						//@JAPR 2015-11-13: Corregido un bug, con la validación descrita arriba, al crear una pregunta tipo mapa se estaba creando como Menú, siendo que ese único
						//caso excepción debería crearse como tipo Vertical (#8CJIDE)
						if ($this->UseMap) {
							$arrAvailableDisplayModes[dspVertical] = dspVertical;
						}
						//@JAPR 2016-04-22: Corregido un bug, al agregar el soporte de preguntas simple choice verticales/horizontales de catálogo, este ajuste a despliegue menú ya no
						//es necesario en ese caso (#2YCZE0)
						$arrAvailableDisplayModes[dspVertical] = dspVertical;
						$arrAvailableDisplayModes[dspHorizontal] = dspHorizontal;
						//@JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
						$arrAvailableDisplayModes[dspMetro] = dspMetro;
						//@JAPR
						//OMMC 2019-03-25: Pregunta AR #4HNJD9
						$arrAvailableDisplayModes[dspAR] = dspAR;
					}
					else {
						$arrAvailableDisplayModes[dspVertical] = dspVertical;
						$arrAvailableDisplayModes[dspHorizontal] = dspHorizontal;
						$arrAvailableDisplayModes[dspMenu] = dspMenu;
						$arrAvailableDisplayModes[dspAutocomplete] = dspAutocomplete;
					}
					
					if (!isset($arrAvailableDisplayModes[$this->QDisplayMode])) {
						$this->QDisplayMode = dspMenu;
					}
				}
				//@JAPR
			}
			
			//@JAPR 2015-07-15: Modificado para que pueda recibir un valor de catálogo siempre que el tipo de captura ya esté configurado de tal manera
			if (array_key_exists("CatalogID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->CatalogID = (int) $anArray["CatalogID"];
				if ($this->CatalogID > 0) {
					$this->UseCatalog = 1;
				}
			}
			if (array_key_exists("CatMemberID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->CatMemberID = (int) $anArray["CatMemberID"];
				if ($this->CatMemberID > 0) {
					$this->UseCatalog = 1;
				}
			}
			
			//@JAPR 2015-07-15: Rediseñadas las preguntas y secciones de catálogo para permitir seleccionar los atributos a usar y el orden deseado (uso de DataSources)
			if (array_key_exists("DataSourceID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataSourceID = (int) $anArray["DataSourceID"];
				if ($this->DataSourceID > 0) {
					$this->UseCatalog = 1;
				}
			}
			
			if (array_key_exists("DataSourceMemberID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataSourceMemberID = (int) $anArray["DataSourceMemberID"];
				if ($this->DataSourceMemberID > 0) {
					$this->UseCatalog = 1;
				}
				else {
					//@JAPR 2015-08-21: Validado que si se limpia el DataSourceMemberID, se debe limpiar el CatMemberID también para mantener consistencia
					$this->DataSourceMemberID = 0;
					$this->CatMemberID = 0;
				}
				
				//Cuando viene asignado DataSourceMemberID, generalmente viene asignado también CatMembersList, así que lo verifica
				//Si no vieniera esta lista, quiere decir que es una pregunta que usará todos los atributos, así que no hay un orden definido sino que se usará el único atributo
				//recibido como el único de dicha lista
				if (array_key_exists("CatMembersList",$anArray) && $this->ValuesSourceType == tofCatalog) {
					//Es un array con los IDs a utilizar en el orden deseado, así que se sobreescribe el array de la pregunta
					$this->CatMembersList = @$anArray["CatMembersList"];
					if (is_null($this->CatMembersList) || !is_array($this->CatMembersList)) {
						$this->CatMembersList = array();
					}
					
					//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
					//Para este momento la instancia ya se llenó con las propiedades necesarias para identificarla si fuera nueva pregunta, o bien es una instancia leída de metadata
					//si se trata de edición, sea cual sea el caso, se puede usar ya el tipo de despliegue para validar que para las que son tipo Metro, el atributo asignado a la pregunta
					//NO es el último de la lista como en las tipo Menú sino el primero, ya que ProcessRequest no podría hacer esa validación sin cargar la instancia así que en ese
					//archivos siempre se reasigna el primer atributo como el asociado a la pregunta
					if ($this->QTypeID == qtpSingle && $this->QDisplayMode == dspMetro && count($this->CatMembersList) > 0 && (int) @$this->CatMembersList[0] > 0) {
						$this->DataSourceMemberID = (int) @$this->CatMembersList[0];
					}
					//@JAPR
				}
				else {
					//Es un único atributo el que se seleccionará como el "usado" para la pregunta
					$this->CatMembersList = array();
					$this->CatMembersList[] = $this->DataSourceMemberID;
				}
			}
			
			if (array_key_exists("DataMemberLatitudeID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataMemberLatitudeID = (int) $anArray["DataMemberLatitudeID"];
			}
			
			if (array_key_exists("DataMemberLongitudeID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataMemberLongitudeID = (int) $anArray["DataMemberLongitudeID"];
			}
			
			//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
			if (array_key_exists("DataSourceFilter",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataSourceFilter = (string) $anArray["DataSourceFilter"];
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->DataSourceFilter = $this->TranslateVariableQuestionNumbersByIDs($this->DataSourceFilter, optyFormula);
				//@JAPR 2015-09-30: Habilitado el filtro dinámico de preguntas de catálogo para todos los tipos
				//Se removió la condición que permitía sólo a las preguntas tipo GetData aplicar esta funcionalidad
				//@JAPR 2016-05-30: Corregido un bug, faltaba este reemplazo para las preguntas múltiple choice, simplemente se removerá la condición por tipo de pregunta
				if (/*$this->QTypeID == qtpSingle && */ /*&& $this->QDisplayMode == dspGetData*/ $this->DataSourceID > 0) {
					$this->DataSourceFilter = BITAMDataSource::ReplaceMemberNamesByIDs($this->Repository, $this->DataSourceID, $this->DataSourceFilter);
				}
				//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
				$garrUpdatedPropsWithVars["Questions"]["DataSourceFilter"] = 1;
				//@JAPR
			}
			
			//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			if (array_key_exists("DataMemberImageID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataMemberImageID = (int) $anArray["DataMemberImageID"];
			}
			
			if (array_key_exists("CatMemberImageID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->CatMemberImageID = (int) $anArray["CatMemberImageID"];
			}
			
			//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			if (array_key_exists("DataMemberOrderID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataMemberOrderID = (int) $anArray["DataMemberOrderID"];
			}
			
			if (array_key_exists("CatMemberOrderID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->CatMemberOrderID = (int) $anArray["CatMemberOrderID"];
			}
			
			//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
			if (array_key_exists("DataMemberOrderDir",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataMemberOrderDir = (int) $anArray["DataMemberOrderDir"];
			}
			
			if (array_key_exists("CustomLayout",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->CustomLayout = (string) $anArray["CustomLayout"];
				$this->CustomLayout = $this->TranslateVariableQuestionNumbersByIDs($this->CustomLayout, optyFormattedText);
				//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
				$garrUpdatedPropsWithVars["Questions"]["CustomLayout"] = 1;
				//@JAPR
			}
			//@JAPR
		}
		
		if(getMDVersion() >= esvOCRQuestionFields){
			if (array_key_exists("SavePhoto", $anArray))
			{
				$this->SavePhoto = (int)$anArray["SavePhoto"];
			}
		}
		
		if(getMDVersion() >= esvHTMLEditorState){
			if (array_key_exists("HTMLEditorState", $anArray)){
				$this->HTMLEditorState = (int)$anArray["HTMLEditorState"];
			}
		}
		
		//@JAPR 2016-01-21: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
		if (array_key_exists("ShowTotals", $anArray)){
			$this->ShowTotals = (int) @$anArray["ShowTotals"];
		}
		
		if (array_key_exists("TotalsFunction", $anArray)){
			$this->TotalsFunction = strtoupper(trim((string) @$anArray["TotalsFunction"]));
		}
		//@JAPR

		if(getMDVersion() >= esvAllowGallery){
			if (array_key_exists("AllowGallery", $anArray)){
				$this->AllowGallery = (int)$anArray["AllowGallery"];
			}
		}
		
		//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
		if (array_key_exists("NextSectionIDIfTrue", $anArray)){
			$this->NextSectionIDIfTrue = (int) @$anArray["NextSectionIDIfTrue"];
		}
		
		if (array_key_exists("NextSectionIDIfFalse", $anArray)){
			$this->NextSectionIDIfFalse = (int) @$anArray["NextSectionIDIfFalse"];
		}
		
		if (array_key_exists("DataDestinationID", $anArray)){
			$this->DataDestinationID = (int) @$anArray["DataDestinationID"];
		}
		
		//@JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
		if (array_key_exists("UploadMediaFiles", $anArray)){
			$this->UploadMediaFiles = (int) @$anArray["UploadMediaFiles"];
		}
		
		//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
		if (array_key_exists("AutoSelectEmptyOpt", $anArray)){
			$this->AutoSelectEmptyOpt = (int) @$anArray["AutoSelectEmptyOpt"];
		}
		//@JAPR
		
		//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
		if (getMDVersion() >= esvNoFlow) {
			if(array_key_exists("NoFlow", $anArray)) {
				$this->NoFlow = (int) @$anArray["NoFlow"];
			}
		}

		//OMMC 2016-05-02: Agregada la corrección de nombres de pregunta con múltiples espacios
		if ($this->LastModAdminVersion < esvQNameCorrection && !isset($blnDoNotEnter)) {
			$this->QuestionText = mb_ereg_replace("\\".chr(194).chr(160), ' ', $this->QuestionText);
		}

		//OMMC 2016-09-01: Modificado el comportamiento default de la pregunta llamada
		if (array_key_exists("DisablePhoneNum", $anArray)) {
			$this->DisablePhoneNum = (int) @$anArray["DisablePhoneNum"];
		}

		if (array_key_exists("DisableSync", $anArray)) {
			$this->DisableSync = (int) @$anArray["DisableSync"];
		}
		
		//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
		//OMMC 2017-01-19: Agregado el trim para los valores string.
		if (array_key_exists("imageWidth", $anArray))
		{
			$this->imageWidth = trim((string)$anArray["imageWidth"]);
		}
		if (array_key_exists("imageHeight", $anArray))
		{
			$this->imageHeight = trim((string)$anArray["imageHeight"]);
		}
		
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		if (getMDVersion() >= esvQuestionAR) {
			if (array_key_exists("DataMemberZipFileID", $anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataMemberZipFileID = (int) $anArray["DataMemberZipFileID"];
			}
			if (array_key_exists("CatMemberZipFileID", $anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->CatMemberZipFileID = (int) $anArray["CatMemberZipFileID"];
			}
			if (array_key_exists("DataMemberVersionID", $anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataMemberVersionID = (int) $anArray["DataMemberVersionID"];
			}
			if (array_key_exists("CatMemberVersionID", $anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->CatMemberVersionID = (int) $anArray["CatMemberVersionID"];
			}
			if (array_key_exists("DataMemberPlatformID", $anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataMemberPlatformID = (int) $anArray["DataMemberPlatformID"];
			}
			if (array_key_exists("CatMemberPlatformID", $anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->CatMemberPlatformID = (int) $anArray["CatMemberPlatformID"];
			}
			if (array_key_exists("DataMemberTitleID", $anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataMemberTitleID = (int) $anArray["DataMemberTitleID"];
			}
			if (array_key_exists("CatMemberTitleID", $anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->CatMemberTitleID = (int) $anArray["CatMemberTitleID"];
			}
		}

		//@JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
		if (array_key_exists("RecalculateDependencies", $anArray)) {
			$this->RecalculateDependencies = $anArray["RecalculateDependencies"];
		}
		//@JAPR
		
		return $this;
	}

	function getArrayCatalogMembers($catalogID)
	{
		$arrayCatMembers = array();
		
		$catalogInstance = BITAMCatalog::NewInstanceWithID($this->Repository, $catalogID);
		$tableName = $catalogInstance->TableName;
		$dimID = $catalogInstance->ParentID;
		$fieldKey = "RIDIM_".$dimID."KEY";
		
		$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$catalogID." ORDER BY MemberOrder DESC";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$parentID = (int)$aRS->fields["parentid"];
			
			$fieldName = "DSC_".$parentID;
			
			$sql = "SELECT ".$fieldName." FROM ".$tableName." WHERE ".$fieldKey." <> 1 ORDER BY ".$fieldKey;
			
			$aRSCat = $this->Repository->DataADOConnection->Execute($sql);
			
			if (!$aRSCat)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while(!$aRSCat->EOF)
			{
				$arrayCatMembers[] = $aRSCat->fields[strtolower($fieldName)];

				$aRSCat->MoveNext();
			}
		}

		return $arrayCatMembers;
	}
	
	//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
	/* Función equivalente a ReplaceVariableQuestionNumbersByIDs, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionNumbersByIDs($sText, $iPropertyID = 0) {
		$blnServerVariables = false;
		return (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($this->Repository, $this->SurveyID, $sText, $this->LastModAdminVersion, otyQuestion, $this->QuestionID, $iPropertyID, $blnServerVariables);
	}
	
	/* Función equivalente a ReplaceVariableQuestionIDsByNumbers, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionIDsByNumbers($sText, $bServerVariables = false) {
		return (string) @BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($this->Repository, $this->SurveyID, $sText, $this->LastModAdminVersion, $bServerVariables);
	}
	
	//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
	/* Esta función actualizará las propiedades que pudieran tener variables en su interior y que no fueron asignadas por la función UpdateFromArray, forzando a traducirlas primero 
	a números de pregunta para que en caso de que el objeto no hubiera sido grabado aún con la versión que ya corregía los IDs se puedan recuperar correctamente, posteriormente las
	volverá a traducir a IDs pero ahora aplicando ya la corrección para no realizar reemplazos parciales. Esta función debe utilizarse siempre antesde un Save del objeto para que 
	durante el primer grabado sea cual fuera la razón para hacerlo, se puedan traducir correctamente las propiedades al momento de asignar el numero de versión de este servicio que ya
	asumirá que graba correctamente este tipo de propiededes, por tanto su uso es:
	1- Esperar a un UpdateFromArray (opcional, si no se invocó, simplemente sobreescribirá todas las propiedades, como por ejemplo en el proceso de copiado)
	2- Ejecutar esta función (si hubo un UpdateFromArray, algunas propiedades podrían ya estar asignadas en el array de reemplazos, así que esas se ignoran)
	3- Ejecutar al Save (esto sobreescribirá al objeto y sus propiedades que utilizan variables
	*/
	function fixAllPropertiesWithVarsNotSet() {
		global $garrUpdatedPropsWithVars;
		
		if (!isset($garrUpdatedPropsWithVars) || !is_array($garrUpdatedPropsWithVars) || !isset($garrUpdatedPropsWithVars["Questions"])) {
			$garrUpdatedPropsWithVars = array();
			$garrUpdatedPropsWithVars["Questions"] = array();
		}
		
		//Si ya se había aplicado un grabado a este objeto por lo menos con la versión de este fix, entonces ya no tiene sentido continuar con el proceso pues no habra diferencia
		if ($this->LastModAdminVersion >= esvPartialIDsReplaceFix) {
			return;
		}
		
		//Actualiza las propiedades no asignadas en el array de propiedades modificadas
		//Para que el proceso funcione correctamente, es indispensable que se engañe a la instancia sobreescribiendo temporalmente LastModAdminVersion a la versión actual pero sólo
		//cuando ya se va a reemplazar por IDs, cuando se va a reemplazar por números si puede dejar la versión actual para que auto-corrija el error con el que había grabado usando IDs
		//previamente
		$dblLastModAdminVersion = $this->LastModAdminVersion;
		$blnUpdatedProperties = false;
		//Primero traducirá de IDs a números todas las propiedades no grabadas manualmente para corregir los IDs mal grabados
		if (!isset($garrUpdatedPropsWithVars["Questions"]["QuestionText"])) {
			$this->QuestionText = $this->TranslateVariableQuestionIDsByNumbers($this->QuestionText);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["QuestionTextHTML"])) {
			$this->QuestionTextHTML = $this->TranslateVariableQuestionIDsByNumbers($this->QuestionTextHTML);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["QuestionMessage"])) {
			$this->QuestionMessage = $this->TranslateVariableQuestionIDsByNumbers($this->QuestionMessage);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["DefaultValue"])) {
			$this->DefaultValue = $this->TranslateVariableQuestionIDsByNumbers($this->DefaultValue);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["Formula"])) {
			$this->Formula = $this->TranslateVariableQuestionIDsByNumbers($this->Formula);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["ShowCondition"])) {
			$this->ShowCondition = $this->TranslateVariableQuestionIDsByNumbers($this->ShowCondition);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["DataSourceFilter"])) {
			$this->DataSourceFilter = $this->TranslateVariableQuestionIDsByNumbers($this->DataSourceFilter);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["CustomLayout"])) {
			$this->CustomLayout = $this->TranslateVariableQuestionIDsByNumbers($this->CustomLayout);
			$blnUpdatedProperties = true;
		}
		
		if (!$blnUpdatedProperties) {
			return;
		}
		
		//Finalmente actualizará las propiedades no grabadas manualmente a IDs ya aplicando la corrección sobreescribiendo la versión del objeto
		//Como esta función se supone que se usará justo antes de un save, es correcto dejar la propiedad LastModAdminVersion actualizada, ya que de todas formas se modificaría en el save
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		if (!isset($garrUpdatedPropsWithVars["Questions"]["QuestionText"])) {
			$this->QuestionText = $this->TranslateVariableQuestionNumbersByIDs($this->QuestionText, optyLabel);
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["QuestionTextHTML"])) {
			$this->QuestionTextHTML = $this->TranslateVariableQuestionNumbersByIDs($this->QuestionTextHTML, optyLabel);
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["QuestionMessage"])) {
			$this->QuestionMessage = $this->TranslateVariableQuestionNumbersByIDs($this->QuestionMessage, optyFormattedText);
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["DefaultValue"])) {
			$this->DefaultValue = $this->TranslateVariableQuestionNumbersByIDs($this->DefaultValue, optyDefaultValue);
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["Formula"])) {
			$this->Formula = $this->TranslateVariableQuestionNumbersByIDs($this->Formula, optyFormula);
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["ShowCondition"])) {
			$this->ShowCondition = $this->TranslateVariableQuestionNumbersByIDs($this->ShowCondition, optyShowCondition);
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["DataSourceFilter"])) {
			$this->DataSourceFilter = $this->TranslateVariableQuestionNumbersByIDs($this->DataSourceFilter, optyFormula);
		}
		if (!isset($garrUpdatedPropsWithVars["Questions"]["CustomLayout"])) {
			$this->CustomLayout = $this->TranslateVariableQuestionNumbersByIDs($this->CustomLayout, optyFormattedText);
		}
	}
	//@JAPR
	
	//@JAPR 2015-01-28: Modificado para permitir que durante una copia de forma las configuraciones que pudieran contener variables
	//se modifiquen para utilizar IDs en lugar de números (#Z9SE00)
	/* Para este objeto convierte todas las variables que están en el viejo formato con uso de números, por el equivalente pero ahora utilizando
	los IDs, esto sólo si la última versión con la que se grabó la instancia era previa a la fecha en que ya se soportaba este proceso de manera
	automática durante la captura, adicionalmente se actualizará dicha fecha en la instancia, ya que el método está pensado para ser utilizado
	previo a la copia de una Forma, o bien durante un proceso de migración, así que se requiere volver a grabar la pregunta de una forma o de
	otra
	Regresa un valor boolean indicando si se llevó a cabo el proceso de conversión, lo cual implica que el texto convertido si es realmente
	diferente al original, ya que de lo contrario no tendría caso volver a grabar la pregunta si no hay cambios, incluso si se considera como
	última versión de grabado una que es anterior a la actual
	*/
	function translateObjectNumbersByIDsInCopy() {
		//Si no es necesario el proceso simplemente regresa false
		if ($this->LastModAdminVersion >= esvExtendedModifInfo) {
			return false;
		}
		
		//Inicia la conversión de cada propiedad de este objeto que pudiera utilizar variables
		$blnUpdateVersion = false;
		$strPrevData = $this->QuestionText;
		if (trim($strPrevData) != '') {
			$this->QuestionText = $this->TranslateVariableQuestionNumbersByIDs($this->QuestionText, optyLabel);
			if ($strPrevData != $this->QuestionText) {
				$blnUpdateVersion = true;
			}
		}
		
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$strPrevData = $this->QuestionTextHTML;
			if (trim($strPrevData) != '') {
				$this->QuestionTextHTML = $this->TranslateVariableQuestionNumbersByIDs($this->QuestionTextHTML, optyLabel);
				if ($strPrevData != $this->QuestionTextHTML) {
					$blnUpdateVersion = true;
				}
			}
		}
		//@JAPR
		
		$strPrevData = $this->QuestionMessage;
		if (trim($strPrevData) != '') {
			$this->QuestionMessage = $this->TranslateVariableQuestionNumbersByIDs($this->QuestionMessage, optyFormattedText);
			if ($strPrevData != $this->QuestionMessage) {
				$blnUpdateVersion = true;
			}
		}
		
		$strPrevData = $this->DefaultValue;
		if (trim($strPrevData) != '') {
			$this->DefaultValue = $this->TranslateVariableQuestionNumbersByIDs($this->DefaultValue, optyDefaultValue);
			if ($strPrevData != $this->DefaultValue) {
				$blnUpdateVersion = true;
			}
		}
		
		$strPrevData = $this->Formula;
		if (trim($strPrevData) != '') {
			$this->Formula = $this->TranslateVariableQuestionNumbersByIDs($this->Formula, optyFormula);
			if ($strPrevData != $this->Formula) {
				$blnUpdateVersion = true;
			}
		}
		
		$strPrevData = $this->ShowCondition;
		if (trim($strPrevData) != '') {
			$this->ShowCondition = $this->TranslateVariableQuestionNumbersByIDs($this->ShowCondition, optyShowCondition);
			if ($strPrevData != $this->ShowCondition) {
				$blnUpdateVersion = true;
			}
		}
		
		$strPrevData = $this->ActionText;
		if (trim($strPrevData) != '') {
			$this->ActionText = $this->TranslateVariableQuestionNumbersByIDs($this->ActionText, optyActionText);
			if ($strPrevData != $this->ActionText) {
				$blnUpdateVersion = true;
			}
		}

		$strPrevData = $this->ActionTitle;
		if (trim($strPrevData) != '') {
			$this->ActionTitle = $this->TranslateVariableQuestionNumbersByIDs($this->ActionTitle, optyActionTitle);
			if ($strPrevData != $this->ActionTitle) {
				$blnUpdateVersion = true;
			}
		}
		
		//Procesa todas las propiedades relacionadas con el Responsive Design
		if (getMDVersion() >= esvResponsiveDesign) {
			for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
				$strDeviceName = getDeviceNameFromID($intDeviceID);
				
				$strProperty = "QuestionMessage";
				$strPrevData = $this->getResponsiveDesignProperty($strProperty, $intDeviceID);
				if (trim($strPrevData) != '') {
					$strValue = $this->TranslateVariableQuestionNumbersByIDs($strPrevData, optyFormattedText);
					if ($strPrevData != $strValue) {
						$blnUpdateVersion = true;
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue);
					}
				}
			}
		}
		
		//Si se necesita actualizar la versión, lo hace para que el objeto se considere grabado a partir de esta versión
		if ($blnUpdateVersion) {
			$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		}
		
		return $blnUpdateVersion;
	}
	//@JAPR
	
	//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
	//Agregado el parámetro $iFromQuestionID para indicar si las opciones de respuesta se deben obtener a partir de esta pregunta indicada en el
	//parámetro o bien si se deben obtener del QuestionID de la instancia. Usada sólo durante el clonado de preguntas para permitir que la 
	//pregunta copia herede todas las opciones de respuesta de la pregunta original en el método Save, ya que él invoca a saveSelectOptions
	//@JAPR 2015-07-26: Descontinuado el parámetro $bJustMobileTables
	//@JAPR 2017-02-13: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
	//Agregado el parámetro $bUpdateDep para indicar cuando se desea actualizar las dependencias. Originalmente usado sólo para el proceso de Generación de formas de producción, ya que
	//durante el grabado normal es evidente que se deben ejecutar, durante el copiado de formas NO se usa para que tome el default y si se copien puesto que para el momento en que se
	//invoca a este grabado ya se actualizaron todas las propiedades con los IDs de la copia, pero en el proceso de generación, el cual invoca a un grabado usando las instancias de
	//desarrollo pues aún no se tienen necesariamente todos los IDs de producción generados (los nuevos), así que en ese grabado original que NO es copia sino mas como un grabado normal,
	//no se debe ejecutar este proceso y por tanto se usa este nuevo parámetro para bloquearlo
	function save($bJustMobileTables = false, $iMobileSurveyID = -1, $iMobileSectionID = -1, $iFromQuestionID = 0, $bCopy=false, $bUpdateDep = true) {
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		global $form;
		
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		//@JAPR 2015-06-14: Rediseñado el Admin con DHTMLX
		global $gbDesignMode;
		
		$gblModMngrErrorMessage = '';
		
		$isNewObj = $this->isNewObject();
		if (getMDVersion() >= esvEditorHTML && $this->EditorType == 1)
		{
			//Los campos HTML del editor anterior serán sustituidos con el código HTML generado con el nuevo editor
			if($this->QuestionMessageDes!="" || $this->QuestionMessageiPodDes !="" || $this->QuestionMessageiPadDes !=""
			|| $this->QuestionMessageiPadMiniDes!="" || $this->QuestionMessageiPhoneDes!="" || $this->QuestionMessageCelDes!=""
			|| $this->QuestionMessageTabletDes!="")
			{
				$this->QuestionMessage = $this->QuestionMessageDesHTML;
				$this->QuestionMessageiPod = $this->QuestionMessageiPodDesHTML;
				$this->QuestionMessageiPad = $this->QuestionMessageiPadDesHTML;
				$this->QuestionMessageiPadMini = $this->QuestionMessageiPadMiniDesHTML;
				$this->QuestionMessageiPhone = $this->QuestionMessageiPhoneDesHTML;
				$this->QuestionMessageCel = $this->QuestionMessageCelDesHTML;
				$this->QuestionMessageTablet = $this->QuestionMessageTabletDesHTML;
			}
		}		
		
		//@JAPR 2015-01-13: Corregido un bug, las preguntas tipo sección no se deben poder mover dentro de otra sección múltiple (#XWKDHA)
		if (!$this->isNewObject() && $this->SectionID != $this->OldSectionID) {
			//Si se detecta un cambio de sección (lo cual sólo puede ser en una pregunta editada) entonces debe verificar si el tipo
			//de pregunta que se dejará (ya que se puede estar cambiando) es inválido en la sección a la que se cambiará, de ser así se
			//debe impedir el cambio de sección y regresar a la sección original
			if (!$this->isValidSectionChange()) {
				$this->SectionID = $this->OldSectionID;
			}
		}
		//@JAPR
		
		if (isset($this->newTypeOfFilling)) {
			//Si viene asignado el tipo de llenado y se trata de una pregunta tipo Simple Choice o Multiple Choice, se tiene que evaluar el tipo
			//para reemplazar otras configuraciones que ya no aplicarían
			$inttypeOfFillingSC = (int) @$this->newTypeOfFilling;
			//@JAPR 2016-04-25: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
			//Esta validación ya no aplica, ahora las preguntas múltiple choice si pueden ser de catálogo
			/*if ($inttypeOfFillingSC == tofCatalog && $this->QTypeID == qtpMulti) {
				$inttypeOfFillingSC = tofFixedAnswers;
			}
			*/
			
			switch ($inttypeOfFillingSC) {
				case tofCatalog:
					$this->SourceQuestionID = 0;
					$this->SourceSimpleQuestionID = 0;
					$this->SharedQuestionID = 0;
					$this->SelectOptions = array();
					$this->StrSelectOptions = '';
					$this->SelectOptionsCol = array();
					$this->StrSelectOptionsCol = '';
					break;
				case tofMCHQuestion:
					$this->SharedQuestionID = 0;
					$this->SelectOptions = array();
					$this->StrSelectOptions = '';
					$this->SelectOptionsCol = array();
					$this->StrSelectOptionsCol = '';
					$this->UseCatalog = 0;
					$this->CatalogID = 0;
					$this->CatMemberID = 0;
					break;
				case tofSharedAnswers:
					$this->SourceQuestionID = 0;
					if ($this->isNewObject()) {
						//En caso de haber alcanzado a agregar opciones antes de especificar que será compartida, se tienen que limpiar dichas
						//opciones, durante la edición no se hace para que no se borren accidentalmente
						$this->SelectOptions = array();
						$this->StrSelectOptions = '';
						$this->SelectOptionsCol = array();
						$this->StrSelectOptionsCol = '';
					}
					$this->UseCatalog = 0;
					$this->CatalogID = 0;
					$this->CatMemberID = 0;
					break;
				case tofFixedAnswers:
				default:
					$this->SourceQuestionID = 0;
					$this->SharedQuestionID = 0;
					$this->UseCatalog = 0;
					$this->CatalogID = 0;
					$this->CatMemberID = 0;
					break;
			}
			
			//@JAPR 2016-12-20: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
			///Si se cambió el tipo de llenado a algo que no es un catálogo, a partir de ahora se limpiará el filtro dinámico de la pregunta y la lista de atributos que hubiera tenido
			//asociada así como el DataSource (requiere una contraparte en surveyExt.inc.php para limpiar localmente en memoria dichas referencias)
			if ($inttypeOfFillingSC != tofCatalog) {
				$this->DataSourceFilter = '';
				$this->DataSourceID = 0;
				$this->DataSourceMemberID = 0;
				$this->DataMemberLatitudeID = 0;
				$this->DataMemberLongitudeID = 0;
				$this->DataMemberImageID = 0;
				$this->DataMemberOrderID = 0;
				//Faltaba eliminar el resto de las referencias de atributos del catálogo, ya que al cambiar CatalogID = 0, se eliminaba el catálogo pero estos atributos seguían siendo
				//utilizados
				$this->CatMemberImageID = 0;
				$this->CatMemberLatitudeID = 0;
				$this->CatMemberLongitudeID = 0;
				$this->CatMemberOrderID = 0;
				//Limpia el array de atributos asociados (aunque el tipo de pregunta no los hubiera requerido) para mas abajo eliminarlos invocando a la función que hace el grabado
				$this->CatMembersList = array();
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				$this->DataMemberZipFileID = 0;
				$this->CatMemberZipFileID = 0;
				$this->DataMemberVersionID = 0;
				$this->CatMemberVersionID = 0;
				$this->DataMemberPlatformID = 0;
				$this->CatMemberPlatformID = 0;
				$this->DataMemberTitleID = 0;
				$this->CatMemberTitleID = 0;
			}
			//@JAPR
		}
		
		//@JAPR 2014-09-29: Agregado el ancho de columnas en secciones Inline o el Summary de maestro-detalle
		if ($this->SectionType != sectInline && $this->SectionType != sectMasterDet) {
			//JAPR 2016-06-08: Modificada la propiedad para que ahora pueda ser un String (#WFLVTR)
			if (getMDVersion() >= esvColumnWidthString) {
				$this->ColumnWidth = '';
			}
			else {
				$this->ColumnWidth = 0;
			}
		}
		
		//@JAPR 2012-03-28: Habilitados los valores máximo y mínimo para preguntas de selección múltiple
		if($this->QTypeID!=qtpOpenNumeric && ($this->QTypeID != qtpMulti || $this->QDisplayMode != dspVertical || $this->MCInputType != mpcNumeric))
		//if($this->QTypeID!=1)
		{
			$this->MinValue = "";
			$this->MaxValue = "";
		}

		//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
		if($this->QTypeID!=qtpOpenNumeric && $this->QTypeID!=qtpCalc)
		{
			$this->IsIndicatorNew = 0;
		}
		
		//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales
		//@JAPR 2012-05-10: Validado que sólo se pueda usar la opción de Otro si la pregunta no es de catálogo
		//@JAPR 2013-12-03: Corregido un bug, faltaba incluir a las preguntas con despliegue horizontal en la validación
		if ($this->QTypeID != qtpSingle || ($this->QDisplayMode != dspVertical && $this->QDisplayMode != dspMenu && $this->QDisplayMode != dspHorizontal) || $this->UseCatalog == 1)
		{
			$this->AllowAdditionalAnswers = 0;
		}
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		if ($this->QTypeID != qtpSingle && $this->QTypeID != qtpMulti) {
			$this->OtherLabel = '';
			$this->CommentLabel = '';
		}
		//Conchita agregada la tipo llamada
		//@JAPR 2013-02-21: Agregada la opción para ocultar los textos de opciones de respuesta si tienen imagen asociada
		//@JAPR 2014-03-07: Corregido un bug, se había agregado mal la condició de las CallList con un || en lugar de &&
		if (($this->QTypeID != qtpCallList && $this->QTypeID != qtpSingle && $this->QTypeID != qtpMulti) || $this->CatalogID > 0) {
			$this->ShowImageAndText = 0;
		}

		//@JAPR 2012-05-29: Agregada la opción para indicar el número de decimales de las preguntas numéricas
		//@JAPR 2012-05-29: Agregado el soporte para creación de preguntas (single y multi) cuyas respuestas dependan de las seleccionadas de una pregunta multi previa
		if ($this->DecimalPlaces < 0) $this->DecimalPlaces = 0;
		if ($this->SourceQuestionID < 0) $this->SourceQuestionID = 0;
		//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		if ($this->SharedQuestionID < 0) $this->SharedQuestionID = 0;
		//@JAPR 2013-12-05: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		//Si es una pregunta con opciones compartidas, no se puede llenar a partir de las respuestas de otra pregunta, aunque si pudiera excluir
		//de las respuestas de otra
		if ($this->SharedQuestionID > 0) {
			$this->SourceQuestionID = 0;
		}
		//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas (campo FormatMask)
		//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
		if ($this->QTypeID != qtpOpenNumeric && ($this->QTypeID != qtpMulti || $this->MCInputType != mpcNumeric) && $this->QTypeID != qtpCalc)
		{
			$this->setFormatMask(0);
		}
    
    	if ($this->QTypeID != qtpOpenDate)
		{
			$this->setDateFormatMask('');
		}
		
		//@JAPR 2012-05-15: Agregadas las preguntas tipo Action (Campos ResponsibleID, CategoryID, DaysDueDate)
		if ($this->QTypeID != qtpAction)
		{
			$this->ResponsibleID = -1;
			$this->CategoryID = 0;
			$this->DaysDueDate = 0;
			//@JAPR 2012-10-29: Agregada la información extra para las acciones
			$this->ActionText = '';
			$this->ActionTitle = '';
		}
		$this->DaysDueDate = abs($this->DaysDueDate);
		
		//@JAPR 2012-05-10: Validado que la opción de ComponentStyle sólo aplique para preguntas sencillas tipo Menu
		//@JAPR 2012-06-15: Agregada la opción para configurar el tipo de teclado en las preguntas numéricas
		//@JAPR 2015-08-15: Agregada la configuración para el tipo de teclado en preguntas múltiple choice con captura numérica
		if (($this->QTypeID != qtpSingle || ($this->QDisplayMode != dspMenu)) && ($this->QTypeID != qtpOpenNumeric) && ($this->QTypeID != qtpMulti || $this->MCInputType != mpcNumeric)) {
			$this->QComponentStyleID = 0;
		}
		
		//Unificado un solo punto para validaciones
		//@JAPR 2014-03-12: Agregados los tipos de posicionamiento del texto en MCH horizontal
		if ($this->QTypeID != qtpMulti) {
			$this->PictLayOut = plyoLeft;
			$this->InputWidth = 0;
			//@JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice
			if (($this->QDisplayMode != dspVertical && $this->QDisplayMode != dspHorizontal) || $this->MCInputType == mpcCheckBox) {
				//Si no es una captura de múltiple choice que permita Inputs entonces no aplica esta funcionalidad
				$this->EnableCheckBox = 0;
				$this->SwitchBehaviour = 0;
			}
		} else {
			//OMMC 2016-07-07: Agregados los widths default de los inputs a petición de CGil
			if ($this->QDisplayMode == dspHorizontal) {
				if ($this->MCInputType == mpcNumeric) {
					$this->InputWidth = 80;
				} else if ($this->MCInputType == mpcText) {
					$this->InputWidth = 160;
				}
			}
		}
		
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		if ($this->QTypeID != qtpSection || $this->SectionType != sectNormal) {
			$this->SourceSectionID = 0;
		}
		//@JAPR
		
		switch ($this->QTypeID) {
			case qtpMulti:
				//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
				//Si la pregunta obtendrá sus respuestas de otra pregunta dinámicamente, se remueven sus respuestas y se desmarcan opciones innecesarias
				if ($this->SharedQuestionID > 0)
				{
					$this->UseCatalog = 0;
					$this->CatalogID = 0;
					$this->CatMemberID = 0;
					$this->AllowAdditionalAnswers = 0;
					//En este caso las opciones de respuesta si existirán localmente en esta pregunta, aunque su manipulación en cuanto al texto,
					//agregar mas o eliminarlas, dependerá por completo de la pregunta origen, sólo existen para manipular sus configuraciones
					//específicas para esta pregunta compartida
					if ($this->isNewObject()) {
						//En caso de haber alcanzado a agregar opciones antes de especificar que será compartida, se tienen que limpiar dichas
						//opciones, durante la edición no se hace para que no se borren accidentalmente
						$this->SelectOptions = array();
						$this->StrSelectOptions = '';
						$this->SelectOptionsCol = array();
						$this->StrSelectOptionsCol = '';
					}
				}
				break;
				
			case qtpSingle:
				//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
				//Si la pregunta obtendrá sus respuestas de otra pregunta dinámicamente, se remueven sus respuestas y se desmarcan opciones innecesarias
				if ($this->SourceQuestionID > 0 || $this->SharedQuestionID > 0)
				{
					$this->UseCatalog = 0;
					$this->CatalogID = 0;
					$this->CatMemberID = 0;
					
					$this->CatMemberLatitudeID = 0;
					$this->CatMemberLongitudeID = 0;
					$this->UseMap = 0;
					$this->MapRadio = 0;
					
					if ($this->SourceQuestionID > 0) {
						$this->AllowAdditionalAnswers = 0;
						$this->SelectOptions = array();
						$this->StrSelectOptions = '';
						$this->SelectOptionsCol = array();
						$this->StrSelectOptionsCol = '';
					}
					else {
						//En este caso las opciones de respuesta si existirán localmente en esta pregunta, aunque su manipulación en cuanto al texto,
						//agregar mas o eliminarlas, dependerá por completo de la pregunta origen, sólo existen para manipular sus configuraciones
						//específicas para esta pregunta compartida
						if ($this->isNewObject()) {
							//En caso de haber alcanzado a agregar opciones antes de especificar que será compartida, se tienen que limpiar dichas
							//opciones, durante la edición no se hace para que no se borren accidentalmente
							$this->SelectOptions = array();
							$this->StrSelectOptions = '';
							$this->SelectOptionsCol = array();
							$this->StrSelectOptionsCol = '';
						}
					}
				}
				break;
				
			//@JAPR 2014-04-02: Corregido un bug, las preguntas tipo password siempre son requeridas
			case qtpPassword:
				$this->IsReqQuestion = 1;
				break;
				
			//@JAPR 2015-08-01: Corregidas las preguntas tipo GPS en v6 para que tomen como QuestionText el AttributeName
			case qtpGPS:
				if ($this->isNewObject() || $iFromQuestionID > 0 || $this->CreationAdminVersion >= esveFormsv6) {
					$this->QuestionText = $this->AttributeName;
					$this->QuestionTextHTML = $this->AttributeName;
				}
				break;
			
			//JAPR 2015-09-01: Por instrucción de DAbdo, se oculta el nombre de pregunta para este tipo, sólo se configurará el HTML/Texto con Formato, y shortname
			case qtpMessage:
				//Se generará un nombre compuesto por el ID de la pregunta y la palabra "Message"
				$this->QuestionText = translate("Message")." #".$this->QuestionID;
				$this->QuestionTextHTML = $this->QuestionText;
				break;
			
			//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
			case qtpUpdateDest:
				//El tipo de pregunta Update Data no tiene opciones de respuestas configurables, pero se usará la misma estructura que una Simple choice para las respuestas fijas por si es
				//necesario extenderlo mas adelante, tanto para mas posibles respuestas como para mas opciones en cada respuesta (como visibilidad de preguntas, no sólo brinco, o un score)
				//Las únicas opciones disponibles a la fecha de implementación son: True y False
				$this->StrSelectOptions = "True"."\r\n"."False";
				break;
			//@JAPR
		}
		
		//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
		if ($this->isOpenType())
		{
			$this->QDisplayMode = dspEntry;
			//@JAPR 2011-06-29: Agregada la longitud de los campos Alfanumericos
			//@JAPR 2015-05-13: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
			//estaban limitadas a 255 caracteres (#BA7866)
			//@JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
			//@JAPR 2016-06-15: Modificado el comportamiento del tamaño del texto de preguntas BarCode para que sea como las alfanuméricas (#TEI1OO)
			if ($this->QTypeID == qtpOpenAlpha || $this->QTypeID == qtpBarCode) {
				if ($this->QLength <= 0 || $this->QLength > MAX_ALPHA_LENGTH) {
					if ($this->QLength > MAX_ALPHA_LENGTH) {
						$this->QLength = MAX_ALPHA_LENGTH;
					}
					else {
						$this->QLength = DEFAULT_ALPHA_LENGTH;
					}
				}
				
				//@JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
				//Al modificar le QLength, MaxQLength sólo debe ser actualizado si el nuevo QLength > MaxQLength, ya que el máximo histórico nunca puede disminuir
				if ($this->QLength > $this->MaxQLength || $this->MaxQLength <= 0) {
					$this->MaxQLength = $this->QLength;
				}
				//@JAPR
			}
			//@JAPR
		}
		else 
		{
			//@JAPR 2013-05-07: Agregado el tipo de pregunta CallList
			//Las preguntas tipo CallList solo pueden ser verticales por ahora
			if ($this->QTypeID == qtpCallList) {
				$this->QDisplayMode = dspVertical;
			}
			//@JAPR
			
			//Una pregunta que NO sea Numérica no puede ser indicador, y como en este caso no es Abierta pues no puede ser numérica así que
			//limpia las propiedades de indicador y asigna un tipo de dato String
			$this->QDataType = dtpString;
			$this->IsIndicator = 0;
			$this->IsIndicatorNew = 0;
			//@JAPR 2016-06-15: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
			//Ahora mas tipos de preguntas registrarán el QLength automáticamente al ser creadas (a menos que sea configurable por el usuario, en ese caso se registrará en esta función
			//save para permitir ajustarlo, tal como sucede con las alfanuméricas o barcode) así que no se va a resetear a 0 para esos tipos de preguntas, ya que este valor ayudará a
			//controlar los tamaños de dimensiones durante la generación de modelos
			switch ($this->QTypeID) {
				case qtpOpenAlpha:
				case qtpBarCode:
				case qtpOpenDate:
				case qtpOpenTime:
				case qtpOpenNumeric:
				case qtpCalc:
				case qtpCallList:
				case qtpGPS:
				case qtpMyLocation:
				case qtpSingle:
				case qtpMulti:
				case qtpAction:
					//Todos estos tipos tienen tamaños automáticos o controlados por el usuario, aunque muchos realmente no entran a este else debido a isOpenType, pero se dejarán
					//aquí por claridad de intención en la omisión de este ajuste en la longitud
					break;
				default:
					$this->QLength = 0;
					break;
			}
			//Una pregunta cuyo despliegue no sea tipo matriz no debe grabar las respuestas por columna
			if ($this->QDisplayMode != dspMatrix)
			{
				$this->SelectOptionsCol = array();
				$this->StrSelectOptionsCol = '';
			}
		}

		//@JAPR 2012-05-14: Agregados nuevos tipos de pregunta Foto y Action
		//@JAPR 2012-05-22: Agregadas las preguntas tipo Firma
		//conchita 2014-09-05: Agregadas las preguntas tipo sketch
		//@AAL 06/05/2015: agregado para que las preguntas tipo código de Barras requieran de Foto
		//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		if ($this->QTypeID == qtpPhoto || $this->QTypeID == qtpSignature || $this->QTypeID == qtpSketch || $this->QTypeID == qtpSketchPlus || $this->QTypeID == qtpBarCode || 
				$this->QTypeID == qtpOCR || ($this->QTypeID == qtpSingle && $this->QDisplayMode == dspAR))
		{
			$this->HasReqPhoto = 2;
			//@JAPR 2014-12-15: Agregado el valor default para preguntas tipo Foto y Sketch
			$this->ReadOnly = 0;

			//OMMC 2017-01-12: Validado al nuevo tipo de despliegue a que siempre sea readonly
			if (getMDVersion() >= esvImageDisplay) {
				if ($this->QTypeID == qtpPhoto && $this->QDisplayMode == dspImage) {
					$this->ReadOnly = 1;
				}
			}

			if ($this->DefaultValue && $this->QTypeID == qtpPhoto) {
				//@JAPR 2014-12-15: Agregado el valor default para preguntas tipo Foto y Sketch
				$this->ReadOnly = 1;
			}
			//@JAPR
		}
		//@JAPR
		
		if ($this->QTypeID == qtpDocument)
		{
			$this->HasReqDocument = 2;
		}
		if (getMDVersion() >= esvQuestionAudioVideo) {
			if($this->QTypeID == qtpAudio || $this->QTypeID == qtpVideo) {
				$this->HasReqDocument = 2;
			}
		}
		//Conchita 2015-01-13
		//Se valida si el usuario selecciono que la pregunta fuera invisible
		//no puede ser requerida 
		if (getMDVersion() >= esvIsInvisible){
			if($this->IsReqQuestion == 1 && $this->IsInvisible == 1){
				$this->IsReqQuestion = 0;
			}
			if($this->IsInvisible == 1 && $this->HasReqComment == 1 ){
				$this->HasReqComment = 2;
			}
			if($this->IsInvisible == 1 && $this->HasReqPhoto == 1 ){
				$this->HasReqPhoto = 2;
			}
            //2015.02.04 JCEM #NAEUJY si la pregunta pertenece a una 
            //seccion inline estos campos deben de ir en 0
            if ($this->SectionType == sectInline){
				//@JAPR 2015-09-09: Corregido un bug, esta asignación para deshabilitar la toma de fotos no debe ser permanente en la metadata, sino que se debe
				//hacer para secciones Inline y siempre y cuando estén en modo tabla y en forma local al App, no tiene caso que se sobreescriba en Metadata (#KVBUK4)
                //$this->HasReqComment = 0;
                //@AAL 06/05/2015: Las secciones inLine con preguntas tipo BarCode deben de mostrar el Bóton Para abrir la camara.
                //$this->HasReqPhoto = ($this->QTypeID == qtpBarCode) ? 2 : 0;
				//@JAPR
                $this->AllowAdditionalAnswers = 0;
            }
		}
		//Conchita
		
		//Validacion de IsIndicatorMCNew para q venga con valor valido,
		//en caso contrario se asigna a cero
		if($this->QTypeID!=qtpMulti)
		{
			$this->IsIndicatorMCNew = 0;
		}
		else 
		{
			if($this->MCInputType!=mpcNumeric)
			{
				$this->IsIndicatorMCNew = 0;
			}
		}
		
		//@JAPR 2012-12-21: Corregido un bug, en varios puntos se usaba esta variable pero no se estaba asignando en todos
		//Si la pregunta pertenece a una seccion dinamica debemos preguntar 
		$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
		//@JAPR
		
		$currentDate = date("Y-m-d H:i:s");
		
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//Al realizar el grabado del objeto (pero no copiado) se deben actualizar a la par todas las configuraciones que pudieran tener variables para arreglar los IDs mal grabados
		//Se debe realizar antes de la asignación de LastModAdminVersion o de lo contrario ya no tendría efecto pues ya consideraría que está grabado con la versión corregida
		if (!$bCopy) {
			$this->fixAllPropertiesWithVarsNotSet();
		}
		
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		//Si se tratara de un clonado, no se debe actualizar el número de última versión que hizo el grabado del objeto, ya que para eso deberían
		//haberse actualizado las propiedades que utilizan variables tal como se hace en el método UpdateFromArray, de lo contrario el método se
		//estarían guardando tal como se leyeron al cargar la instancia y eso podría ser una versión que aún las guardaba como número, por lo que
		//actualizar la versión del servicio al último dejaría los datos dañados pues la siguiente ocasión los leería como si fueran IDs de preguntas
		if ($iFromQuestionID <= 0) {
			//@JAPR 2014-09-26: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
			//Si no se está haciendo un clonado, entonces si puede actualizar esta información según si se trata de un objeto nuevo o no
			$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
			$this->LastModDateID = $currentDate;
			$this->LastModUserID = $_SESSION["PABITAM_UserID"];
			if ($this->isNewObject()) {
				$this->CreationAdminVersion = ESURVEY_SERVICE_VERSION;
				$this->CreationDateID = $currentDate;
				$this->CreationUserID = $_SESSION["PABITAM_UserID"];
			}
		}
		//@JAPR 2014-09-29: Se determinó que los datos de creación deben actualizarse al copiar, mas no así los datos de última modificación
		else {
			//En este caso, se trata de un clonado de pregunta, por lo que si se está creando una nueva instancia así que hay que actualizar sólo
			//los datos de creación, pero no los datos de modificación puesto que la información de la pregunta no está cambiando
			$this->CreationAdminVersion = ESURVEY_SERVICE_VERSION;
			$this->CreationDateID = $currentDate;
			$this->CreationUserID = $_SESSION["PABITAM_UserID"];
		}
		//@JAPR 2016-04-14: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
		//Se corta el texto a 10 caracteres máximo que es el nuevo límite dependiendo de la metadata
		if (getMDVersion() >= esvSimpleChoiceCatOpts) {
			$this->canvasWidth = substr($this->canvasWidth, 0, 10);
			$this->canvasHeight = substr($this->canvasHeight, 0, 10);
		}
		else {
			$this->canvasWidth = substr($this->canvasWidth, 0, 5);
			$this->canvasHeight = substr($this->canvasHeight, 0, 10);
		}
		//@JAPR

		//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
		if (getMDVersion() >= esvImageDisplay) {
			$this->imageWidth = substr($this->imageWidth, 0, 10);
			$this->imageHeight = substr($this->imageHeight, 0, 10);
		}
		
		if ($this->isNewObject()) {
			$arraySelectOptions = array();
			
			//@JAPR 2013-12-05: Agregada la opción para compartir las opciones de respuesta con otras preguntas
			if (getMDVersion() >= esvSimpleChoiceSharing) {
				//Si la pregunta es compartida desde otra, como esta creandose apenas, le asigna las mismas opciones de respuesta que tenga la pregunta
				//origen para que ya se creen sus diferentes respuestas pero considerando la configuración de esta pregunta compartida
				if ($this->SharedQuestionID > 0 && !$bCopy) {
					
					$objSharedQuestion = @BITAMQuestion::NewInstanceWithID($this->Repository, $this->SharedQuestionID);
					if (!is_null($objSharedQuestion)) {
						$this->StrSelectOptions = $objSharedQuestion->StrSelectOptions;
						if ($this->QDisplayMode == dspMatrix) {
							$this->StrSelectOptionsCol = $objSharedQuestion->StrSelectOptionsCol;
						}
					}
				}
			}
			//@JAPR
			
			//Validamos que las opciones sean unicas
			if(trim($this->StrSelectOptions)!="")
			{
				$arraySelectOptions = explode("\r\n", $this->StrSelectOptions);
			}
			
			$arraySelectOptions = array_values(array_unique($arraySelectOptions));
			
			//20Marzo2013: Que no se graben espacios en blanco al inicio y al final de cada opción
			foreach ($arraySelectOptions as $idx=>$value)
			{
				$arraySelectOptions[$idx] = trim($value);
			}
			
			$LN = chr(13).chr(10);
			$this->StrSelectOptions = implode($LN, $arraySelectOptions);
			
			$arraySelectOptionsCol = array();
			
			//Validamos que las opciones de columnas sean unicas
			if(trim($this->StrSelectOptionsCol)!="")
			{
				$arraySelectOptionsCol = explode("\r\n", $this->StrSelectOptionsCol);	
			}
			
			$arraySelectOptionsCol = array_values(array_unique($arraySelectOptionsCol));
			
			//20Marzo2013: Que no se graben espacios en blanco al inicio y al final de cada opción
			foreach ($arraySelectOptionsCol as $idx=>$value)
			{
				$arraySelectOptionsCol[$idx] = trim($value);
			}
			
			$LN = chr(13).chr(10);
			$this->StrSelectOptionsCol = implode($LN, $arraySelectOptionsCol);
			
			//Obtener el QuestionID
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(QuestionID)", "0")." + 1 AS QuestionID".
						" FROM SI_SV_Question";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->QuestionID = (int)$aRS->fields["questionid"];
			
			//OMMC 2015-10-27: Se refresca el ID de las preguntas mensaje y HTML porque al crearse se iniciaban con ID de -1. 
			//ISSUE #F90NCY
			if($this->QTypeID == qtpMessage){
				$this->QuestionText = translate("Message")." #".$this->QuestionID;
				$this->QuestionTextHTML = $this->QuestionText;
			}

			//Obtener el QuestionNumber
			$sqlNumber =  "SELECT ".
							$this->Repository->DataADOConnection->IfNull("MAX(A.QuestionNumber)", "0")." + 1 AS QuestionNumber 
							FROM SI_SV_Question A, SI_SV_Section B WHERE A.SurveyID = ".$this->SurveyID." 
							AND A.SectionID = B.SectionID AND B.SectionNumber <= ".$this->SectionNumber;
			$aRS = $this->Repository->DataADOConnection->Execute($sqlNumber);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlNumber);
			}
			
			$this->QuestionNumber = (int)$aRS->fields["questionnumber"];
			
			//Actualizar QuestionNumber posteriores
			$sqlUpd = "UPDATE SI_SV_Question SET QuestionNumber = QuestionNumber + 1 WHERE SurveyID = ".$this->SurveyID." AND QuestionNumber >= ".$this->QuestionNumber;
			if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
			}
			
			//@JAPR 2015-06-13: Si no se especifica un nombre al crear, se asignará uno basado en el ID asignado a la pregunta
			//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
			//if (trim($this->AttributeName) == "") {
			//	$this->AttributeName = "dim_".$this->QuestionID;
			//}
			
			//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas (campo QDisplayMode)
			//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz (campo OptionsTextCol)
			//@JAPR 2011-06-29: Agregada la longitud de los campos Alfanumericos
			//Conchita agregado GoToSection 18-oct-2011
			//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales (Campo AllowAdditionalAnswers)
			//@JAPR 2012-04-30: Agregada la opción para configurar el estilo visual del componente (Campo QComponentStyleID)
			//@JAPR 2012-05-15: Agregadas las preguntas tipo Action (Campos ResponsibleID, CategoryID, DaysDueDate)
			//@JAPR 2012-05-29: Agregada la opción para indicar el número de decimales de las preguntas numéricas (campo DecimalPlaces)
			//@JAPR 2012-05-29: Agregado el soporte para creación de preguntas (single y multi) cuyas respuestas dependan de las seleccionadas de una pregunta multi previa (campo SourceQuestionID)
			//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas (campo FormatMask)
			//@JAPR 2012-10-19: Agregadas las preguntas marcadas como Read-Only
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			//@JAPR 2012-10-29: Agregada la información extra para las acciones
			if (getMDVersion() >= esvExtendedActionsData) {
				$strAdditionalFields .= ',ActionTitle,ActionText';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->ActionTitle).','.
					$this->Repository->DataADOConnection->Quote($this->ActionText);
			}
			//@JAPR 2012-12-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
			if (getMDVersion() >= esveFormsV41Func) {
				$strAdditionalFields .= ',ReadOnly,Formula,HasReqDocument,ShowCondition ';
				$strAdditionalValues .= ','.$this->ReadOnly.','.$this->Repository->DataADOConnection->Quote($this->Formula);
				$strAdditionalValues .= ','.$this->HasReqDocument.','.$this->Repository->DataADOConnection->Quote($this->ShowCondition);
			}
			//@JAPR 2013-02-21: Agregada la opción para ocultar los textos de opciones de respuesta si tienen imagen asociada
			if (getMDVersion() >= esvCatalogDissociation) {
				$strAdditionalFields .= ',ShowImageAndText ';
				$strAdditionalValues .= ','.$this->ShowImageAndText;
			}
			//@JAPR 2013-04-19: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
			if (getMDVersion() >= esveBavelMapping) {
				$strAdditionalFields .= ',eBavelFieldID';
				$strAdditionalValues .= ','.$this->eBavelFieldID;
			}
			//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
			if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
				$strAdditionalFields .= ',OtherLabel,CommentLabel';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->OtherLabel).','.
					$this->Repository->DataADOConnection->Quote($this->CommentLabel);
			}
			//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions123) {
				$strAdditionalFields .= ',eBavelActionFormID';
				$strAdditionalValues .= ','.$this->eBavelActionFormID;
			}
			//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
			if (getMDVersion() >= esvSimpleChoiceSharing) {
				$strAdditionalFields .= ',SharedQuestionID';
				$strAdditionalValues .= ','.$this->SharedQuestionID;
			}
			//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
			if (getMDVersion() >= esvGPSQuestion) {
				$strAdditionalFields .= ',RegisterGPSAt';
				$strAdditionalValues .= ','.$this->RegisterGPSAt;
			}
			//@JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				$strAdditionalFields .= ',InputWidth,PictLayOut';
				$strAdditionalValues .= ','.$this->InputWidth.','.$this->PictLayOut;
			}
			if (getMDVersion() >= esvNearBy) {
				$strAdditionalFields .= ' ,CatMemberLatitudeID, CatMemberLongitudeID, UseMap, MapRadio';
				$strAdditionalValues .= ','.$this->CatMemberLatitudeID.','.$this->CatMemberLongitudeID.','.$this->UseMap.','.$this->MapRadio;
			}
			//@JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice
			if (getMDVersion() >= esvMCHSwitchBehav) {
				$strAdditionalFields .= ',EnableCheckBox,SwitchBehaviour';
				$strAdditionalValues .= ','.$this->EnableCheckBox.','.$this->SwitchBehaviour;
			}
			//@JAPR
			if (getMDVersion() >= esvShowInSummary) {
				$strAdditionalFields .= ',ShowInSummary';
				$strAdditionalValues .= ','.$this->ShowInSummary;
			}
			//agregado el width y height
			if (getMDVersion() >= esvSketch) {
				$strAdditionalFields .= ',CanvasWidth';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->canvasWidth);
				$strAdditionalFields .= ',CanvasHeight';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->canvasHeight);
			}
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				//@JAPR 2014-09-26: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
				//Las fechas y usuarios de creación y modificación se deben validar con cuiado, ya que no todos los objetos las tienen definidas
				if (!is_null($this->CreationUserID) && (int) $this->CreationUserID > 0) {
					$strAdditionalFields .= ', CreationUserID';
					$strAdditionalValues .= ', '.(int) $this->CreationUserID;
				}
				if (!is_null($this->LastModUserID) && (int) $this->LastModUserID > 0) {
					$strAdditionalFields .= ', LastUserID';
					$strAdditionalValues .= ', '.(int) $this->LastModUserID;
				}
				if (!is_null($this->CreationDateID) && trim((string) $this->CreationDateID) != '') {
					$strAdditionalFields .= ', CreationDateID';
					$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->DBTimeStamp($this->CreationDateID);
				}
				if (!is_null($this->LastModDateID) && trim((string) $this->LastModDateID) != '') {
					$strAdditionalFields .= ', LastDateID';
					$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->DBTimeStamp($this->LastModDateID);
				}
				$strAdditionalFields .= ', CreationVersion, LastVersion';
				$strAdditionalValues .= ', '.$this->CreationAdminVersion.', '.$this->LastModAdminVersion;
			}
			//@JAPR 2014-09-29: Agregado el ancho de columnas en secciones Inline o el Summary de maestro-detalle
			if (getMDVersion() >= esvInlineMerge) {
				$strAdditionalFields .= ',ColumnWidth';
				//JAPR 2016-06-08: Modificada la propiedad para que ahora pueda ser un String (#WFLVTR)
				if (getMDVersion() >= esvColumnWidthString) {
					$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->ColumnWidth);
				}
				else {
					$strAdditionalValues .= ','.$this->ColumnWidth;
				}
			}
			//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
			if (getMDVersion() >= esvSectionQuestions) {
				$strAdditionalFields .= ',SourceSectionID';
				$strAdditionalValues .= ','.$this->SourceSectionID;
			}
			//@JAPR
			//Conchita 2014-10-11 agregado el sketchimage
			if (getMDVersion() >= esvSketchImage) {
				$strAdditionalFields .= ',SketchImage';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->QuestionImageSketch);
			}
			//@JAPR 2014-10-22: Agregada la opción para redibujar las preguntas tipo mensaje automáticamente
			if (getMDVersion() >= esvInlineTypeOfFilling) {
				$strAdditionalFields .= ',AutoRedraw';
				$strAdditionalValues .= ','.$this->AutoRedraw;
			}
			//Sep-2014: Agregado el nuevo campo para definir que editor se utiliza
			if (getMDVersion() >= esvEditorHTML)
			{
				$strAdditionalFields .= ', EditorType, QuestionMessageDes';
				$strAdditionalValues .= ', '.$this->EditorType.", ".$this->Repository->DataADOConnection->Quote($this->QuestionMessageDes);
			}
			
				//Conchita agregado isinvisible 2014-12-01
			if (getMDVersion() >= esvIsInvisible)
			{
				$strAdditionalFields .= ',IsInvisible';
				$strAdditionalValues .= ','.$this->IsInvisible;
			}
			//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
			if (getMDVersion() >= esvAdminWYSIWYG) {
				$strAdditionalFields .= ', QuestionTextHTML, ValuesSourceType';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->QuestionTextHTML).', '.
					$this->ValuesSourceType;
				//@JAPR 2015-07-16: Agregada la integración con DataSources
				//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
				$strAdditionalFields .= ', DataSourceID, DataSourceMemberID, DataMemberLatitudeID, DataMemberLongitudeID, DataSourceFilter';
				$strAdditionalValues .= ', '.$this->DataSourceID.', '.$this->DataSourceMemberID.
					', '.$this->DataMemberLatitudeID.', '.$this->DataMemberLongitudeID.', '.
					$this->Repository->DataADOConnection->Quote($this->DataSourceFilter);
			}
			//@JAPR
			//OMMC 2015-11-30: Agregado para preguntas OCR
			if (getMDVersion() >= esvOCRQuestionFields){
				$strAdditionalFields .= ', SavePhoto';
				$strAdditionalValues .= ', '.$this->SavePhoto;
			}
			//@JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
			if (getMDVersion() >= esvMaxQLength) {
				$strAdditionalFields .= ',MaxQLength';
				$strAdditionalValues .= ','.$this->MaxQLength;
			}
			//@JAPR
			//OMMC 2016-01-18: Agregado para las preguntas HTML, guarda el estado en el que se dejó el editor HTML, inicia como 0.
			if(getMDVersion() >= esvHTMLEditorState){
				$strAdditionalFields .= ', HTMLEditorState';
				$strAdditionalValues .= ', '.$this->HTMLEditorState;
			}
			//@JAPR 2016-01-21: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
			if (getMDVersion() >= esvShowTotalsInline) {
				$strAdditionalFields .= ', ShowTotals, TotalsFunction';
				$strAdditionalValues .= ', '.$this->ShowTotals.', '.$this->Repository->DataADOConnection->Quote($this->TotalsFunction);
			}
			//@JAPR
			//OMMC 2015-11-30: Agregado para permitir acceso a la galería
			if (getMDVersion() >= esvAllowGallery) {
				$strAdditionalFields .= ', AllowGallery';
				$strAdditionalValues .= ', '.$this->AllowGallery;
			}
			//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			if (getMDVersion() >= esvSimpleChoiceCatOpts) {
				$strAdditionalFields .= ', CatMemberImageID, DataMemberImageID';
				$strAdditionalValues .= ', '.$this->CatMemberImageID.', '.$this->DataMemberImageID;
			}
			//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
			if (getMDVersion() >= esvUpdateData) {
				$strAdditionalFields .= ', DataDestinationID';
				$strAdditionalValues .= ', '.$this->DataDestinationID;
			}
			//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
			if (getMDVersion() >= esvNoFlow) {
				$strAdditionalFields .= ', NoFlow';
				$strAdditionalValues .= ', '.$this->NoFlow;
			}
			//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
			if (getMDVersion() >= esvSChoiceMetro) {
				$strAdditionalFields .= ', CatMemberOrderID, DataMemberOrderID, DataMemberOrderDir, CustomLayout';
				$strAdditionalValues .= ', '.$this->CatMemberOrderID.', '.$this->DataMemberOrderID.', '.$this->DataMemberOrderDir.', '.
					$this->Repository->DataADOConnection->Quote($this->CustomLayout);
			}

			//OMMC 2016-09-01: Modificado el comportamiento default de la pregunta llamada
			if (getMDVersion() >= esvPhoneCall) {
				$strAdditionalFields .= ', DisablePhoneNum, DisableSync';
				$strAdditionalValues .= ', '.$this->DisablePhoneNum.', '.$this->DisableSync;
			}
			//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
			if (getMDVersion() >= esvImageDisplay) {
				$strAdditionalFields .= ',ImageWidth';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->imageWidth);
				$strAdditionalFields .= ',ImageHeight';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->imageHeight);
			}
			
			//RV Feb2016: Agregar el campo que indica cual es el ID de la pregunta a partir de la cual se copio esta pregunta (cuando sea el caso)
			//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
			//$form sólo estará asignada si se trata de un proceso de generación de forma de desarrollo iniciado por processCopyForm.php
			if (getMDVersion() >= esvCopyForms) {
				$strAdditionalFields .= ', SourceID';
				if (isset($form) && $form->ActionType == devAction) {
					$strAdditionalValues .= ', '.$this->CopiedQuestionID;
				}
				else {
					$strAdditionalValues .= ', 0';
				}
			}
			//@JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
			if (getMDVersion() >= esvUpdateDataWithFiles) {
				$strAdditionalFields .= ', UploadMediaFiles';
				$strAdditionalValues .= ', '.$this->UploadMediaFiles;
			}
			//@JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
			if (getMDVersion() >= esvRecalcDep) {
				$strAdditionalFields .= ', RecalculateDependencies';
				$strAdditionalValues .= ', '.$this->RecalculateDependencies;
			}
			//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
			if (getMDVersion() >= esvAutoSelectSChOpt) {
				$strAdditionalFields .= ', AutoSelectEmptyOpt';
				$strAdditionalValues .= ', '.$this->AutoSelectEmptyOpt;
			}
			//@JAPR
			//OMMC 2019-03-25: Pregunta AR #4HNJD9
			if (getMDVersion() >= esvQuestionAR) {
				$strAdditionalFields .= ', DataMemberZipFileID, CatMemberZipFileID, DataMemberVersionID, CatMemberVersionID, DataMemberPlatformID, CatMemberPlatformID, DataMemberTitleID, CatMemberTitleID';
				$strAdditionalValues .= ', '.$this->DataMemberZipFileID.', '.$this->CatMemberZipFileID.', '.$this->DataMemberVersionID.', '.$this->CatMemberVersionID.', '.$this->DataMemberPlatformID.', '.$this->CatMemberPlatformID.', '.$this->DataMemberTitleID.', '.$this->CatMemberTitleID;
			}
			$sql = "INSERT INTO SI_SV_Question (".
					"SurveyID".
					",SectionID".
					",QuestionID".
					",QuestionNumber".
					",QuestionText".
					",QTypeID".
					",QComment".
					",QActions".
					",HasValidation".
					",MinValue".
					",`MaxValue`".
					",MinDate".
					",MaxDate".
					",OptionsText".
					",AttributeName".
					",IsFieldSurveyID".
					",IsIndicator".
					",IsReqQuestion".
					",HasReqComment".
					",HasReqPhoto".
					",HasActions".
					",MCInputType".
					",UseCatalog".
					",CatalogID".
					",CatMemberID".
					",QDisplayMode".
					",OptionsTextCol".
					",QLength".
					",IsMultiDimension".
					",UseCatToFillMC".
					",FatherQuestionID".
					",ChildQuestionID".
					",OutsideOfSection".
					",GoToSection".
					",IsIndicatorMC".
					",OneChoicePer".
					",TextDisplayStyle".
					",UseCategoryDim".
					",CategoryDimName".
					",ElementDimName".
					",ElementIndName".
					",AllowAdditionalAnswers".
					",ResponsibleID".
					",CategoryID".
					",DaysDueDate".
					",QComponentStyleID".
					",DecimalPlaces".
					",SourceQuestionID".
					",FormatMask".
					",MaxChecked".
					",QuestionImageText".
					",QuestionMessage".
					",DateFormatMask".
					",DefaultValue".
					",HasQuestionDep".
					//",ExcludeSelected".
					",SourceSimpleQuestionID".
					",SummaryTotal".
					$strAdditionalFields.
					") VALUES (".
					$this->SurveyID.
					",".$this->SectionID.
					",".$this->QuestionID.
					",".$this->QuestionNumber.
					",".$this->Repository->DataADOConnection->Quote($this->QuestionText).
					",".$this->QTypeID.
					",".$this->Repository->DataADOConnection->Quote($this->QComment).
					",".$this->Repository->DataADOConnection->Quote($this->QActions).
					",".$this->HasValidationNew.
					",".$this->Repository->DataADOConnection->Quote($this->MinValue).
					",".$this->Repository->DataADOConnection->Quote($this->MaxValue).
					",".$this->Repository->DataADOConnection->Quote($this->MinDate).
					",".$this->Repository->DataADOConnection->Quote($this->MaxDate).
					",".$this->Repository->DataADOConnection->Quote($this->StrSelectOptions).
					",".$this->Repository->DataADOConnection->Quote($this->AttributeName).
					",".$this->IsFieldSurveyID.
					",".$this->IsIndicatorNew.
					",".$this->IsReqQuestion.
					",".$this->HasReqComment.
					",".$this->HasReqPhoto.
					",".$this->HasActions.
					",".$this->MCInputType.
					",".$this->UseCatalog.
					",".$this->CatalogID.
					",".$this->CatMemberID.
					",".$this->QDisplayMode.
					",".$this->Repository->DataADOConnection->Quote($this->StrSelectOptionsCol).
					",".$this->QLength.
					",".$this->IsMultiDimension.
					",".$this->UseCatToFillMC.
					",".$this->FatherQuestionID.
					",".$this->ChildQuestionID.
					",".$this->OutsideOfSection.
					",".$this->GoToQuestion.
					",".$this->IsIndicatorMCNew.
					",".$this->OneChoicePer.
					",".$this->TextDisplayStyle.
					",".$this->UseCategoryDim.
					",".$this->Repository->DataADOConnection->Quote($this->CategoryDimName).
					",".$this->Repository->DataADOConnection->Quote($this->ElementDimName).
					",".$this->Repository->DataADOConnection->Quote($this->ElementIndName).
					",".$this->AllowAdditionalAnswers.
					",".$this->ResponsibleID.
					",".$this->CategoryID.
					",".$this->DaysDueDate.
					",".$this->QComponentStyleID.
					",".$this->DecimalPlaces.
					",".$this->SourceQuestionID.
					",".$this->Repository->DataADOConnection->Quote($this->FormatMask).
					",".$this->MaxChecked.
					",".$this->Repository->DataADOConnection->Quote($this->QuestionImageText).
					",".$this->Repository->DataADOConnection->Quote($this->QuestionMessage).
					",".$this->Repository->DataADOConnection->Quote($this->DateFormatMask).
					",".$this->Repository->DataADOConnection->Quote($this->DefaultValue).
					",".$this->HasQuestionDep.
					//",".$this->ExcludeSelected.
					",".$this->SourceSimpleQuestionID.
					",".$this->SummaryTotal.
					$strAdditionalValues.
					")";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Inserting question: {$sql}", 2, 0, "color:blue;");
			}
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
			if(!$bCopy)
			{
				//@JAPR 2017-02-13: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				//Agregado el parámetro $bUpdateDep para indicar cuando se desea actualizar las dependencias. Originalmente usado sólo para el proceso de Generación de formas de producción
				if ($bUpdateDep) {
					if($this->HasQuestionDep) {
						$this->setQuestionsToEval();
					}
					$this->setFormulaChildren();
					$this->setShowConditionChildren();
				}
				//@JAPR
				
				//Se deberan almacenar las opciones de los combos de seleccion simple y multiple
				//Conchita 2013-04-29 agregado el tipo calllist
				//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
				if (($this->QTypeID == qtpSingle && $this->UseCatalog == 0) || $this->QTypeID == qtpMulti || $this->QTypeID == qtpCallList || $this->QTypeID == qtpUpdateDest) {
					//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
					//Agregado el parámetro $iFromQuestionID para indicar si las opciones de respuesta se deben obtener a partir de esta pregunta indicada en el
					//parámetro o bien si se deben obtener del QuestionID de la instancia. Usada sólo durante el clonado de preguntas para permitir que la 
					//pregunta copia herede todas las opciones de respuesta de la pregunta original en el método Save, ya que él invoca a saveSelectOptions
					$this->saveSelectOptions($iFromQuestionID);
					//@JAPR
				}
				
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				//Cambiado a un switch para mayor facilidad
				$blnValidQuestion = true;
				switch ($this->QTypeID) {
					case qtpSync:
					case qtpPassword:
					case qtpMessage:
					case qtpMapped:
					case qtpSkipSection:
					case qtpSection:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValidQuestion = false;
						break;
				}
				
				//@AAL 27/05/2015: En EformsV6 y siguientes las Dimensiones no aplican
				if ($this->CreationAdminVersion < esveFormsv6)
				{
					//Siempre y cuando el tipo de dato sea diferente a Show Value se crean 
					//las dimensiones e indicadores q se requiera
					//@JAPR 2012-05-14: Agregados nuevos tipos de pregunta Foto y Action
					//@JAPR 2012-11-20: Corregido un bug, había un AND aquí así que en realidad en todo v3 si estuvo creando campo en la paralela
					//@JAPR 2012-11-21: Validado que las preguntas tipo mensaje y salto no generen campo en la tabla paralela ni dimensión
					//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
					//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
					if ($this->QTypeID == qtpGPS || $this->QTypeID == qtpMyLocation) {
						//Para este tipo de preguntas se generan 3 dimensiones, una de ella es la dimensión base donde se graba la posición como un par
						//"Latitude,Longitude", mientras que las otras dos son dimensiones-atributo precisamente para grabar por separado (pero ligadas
						//a la base) la Latitude y Longitude. No requiere imagen, comentario ni captura en general de nada extra
						$this->CreateGPSDimension();
						
						//Crear campo dimension en tabla paralela
						$this->SurveyField = "dim_".$this->QuestionID;
						$this->createSurveyDimField();
					}
					//@JAPR 2014-10-02: Corregido un bug, algunos tipos de pregunta estaban preparados para generar una dimensión imagen aunque no aplique en ellos
					elseif ($this->QTypeID == qtpPhoto || $this->QTypeID == qtpSignature || $this->QTypeID == qtpSketch)
					{
						//Para este tipo de preguntas sólo se genera la dimensión imagen en la tabla del cubo, pero no habrá campo dimensión en tabla
						//paralela ya que las imagenes se obtendrán por referencia de su nombre
						$this->createModelDimensionIMG();
					}
					//@JAPR 2013-04-12: Agregados nuevos tipos de pregunta Sincronización
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					//@JAPR 2014-10-02: Corregido un bug, algunos tipos de pregunta estaban preparados para generar una dimensión imagen aunque no aplique en ellos
					elseif (!$blnValidQuestion) {
						//Este tipo de preguntas no crean absolutamente nada en las tablas
					}
					else if($this->QTypeID!=qtpShowValue)
					{
						//Procedemos a crear el indicador o dimension dependiendo del tipo de dato de la respuesta
						//Si se trata de un dato numerico lo damos de alta como dimension y revisamos si tambien
						//lo requieren dar de alta como indicador 
						//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
						if($this->QTypeID==qtpOpenNumeric || $this->QTypeID==qtpCalc)
						{
							//Primero se crea la dimension
							$this->createModelDimension();
							
							//Ahora se revisa si tambien se requiere crearse como indicador
							if($this->IsIndicatorNew==1)
							{
								$this->createIndicatorKPI();
							}
							
							//Crear campo dimension en tabla paralela con tipo de dato double/float
							$this->SurveyField = "dim_".$this->QuestionID;
							$this->createSurveyIndField();
							
							$this->createModelDimensionIMG();
						}
						else 
						{
							//Mientras sea diferente de seleccion multiple
							//si se insertara la dimension
							if($this->QTypeID!=qtpMulti)
							{
								if($this->QTypeID!=qtpSingle && $this->QTypeID!=qtpCallList) //Conchita 2013-04-29
								{
									//@JAPR 2014-10-02: Corregido un bug, los casos de preguntas que no caen en alguna otra condición llegan aquí, pero
									//no todos necesitan una dimensión Valor ni campo en la tabla paralela, los únicos casos que si deberían crear estos
									//elementos son preguntas Abiertas (todos los tipos) o Acción
									//Estos tipos no requieren ni campo en la tabla paralela ni dimensión valor, sus dimensiones Documento o Imagen se
									//crearán (en caso de ser necesario) al salir de estos Ifs
									$blnValidQuestion = true;
									switch ($this->QTypeID) {
										case qtpDocument:
										case qtpAudio:
										case qtpVideo:
											$blnValidQuestion = false;
											break;
											
										//case qtpOpenDate:
										//case qtpOpenString:
										//case qtpOpenAlpha:
										//case qtpOpenTime:
										//case qtpAction:
										//@AAL 06/05/2015: Agregado el tipo de pregunta Codigo de barra.
										//case qtpBarCode:
										default:
											//Básicamente las preguntas que entran aquí deberían ser las Abiertas (no numérica) o la Acción
											break;
									}
									
									//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
									if ($blnValidQuestion) {
										$this->createModelDimension();
										
										//Crear campo dimension en tabla paralela
										$this->SurveyField = "dim_".$this->QuestionID;
										$this->createSurveyDimField();
									}
									//@JAPR
								}
								else if($this->QTypeID==qtpCallList || ($this->QTypeID==qtpSingle && $this->UseCatalog==0))
								{
									//Conchita 2013-04-26 agregado el type calllist
									//Si es una pregunta de seleccion simple que no es Matriz,
									//entonces si se crea una sola dimension como siempre
									if($this->QDisplayMode <> dspMatrix)
									{
										$this->createModelDimension();
										
										//Crear campo dimension en tabla paralela
										$this->SurveyField = "dim_".$this->QuestionID;
										$this->createSurveyDimField();
									}
									else 
									{
										//Para este tipo de dato los datos se graban en la tabla SVSURVEYMatrixData_<ModelID>
										//No sabe a ciencia cierta si crear el campo o no crearlo
										//por el momento se creara pero no se dara seguimiento
										//solo se realizara esta creacion para no generar problemas
										//en las clases de report y exportsurvey
										
										//Crear campo dimension en tabla paralela
										$this->SurveyField = "dim_".$this->QuestionID;
										$this->createSurveyDimField();
										
										//Si se trata de una matriz entonces tenemos que crear
										//las dimensiones correspondientes ya sea para las filas
										//o las columnas
										$this->createModelDimMatrixData();
									}
								}
								else 
								{
									//Este caso es exclusivamente para preguntas Simple choice con catálogo
									$this->createCatalogDimension();
									
									//Crear campo dimension en tabla paralela
									//Pero esta dimension grabara la clave subrogada
									//y en este caso ahorita el campo se crea varchar
									//quizas mas adelante cambie esto
									$this->SurveyField = "dim_".$this->QuestionID;
									//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
									$this->SurveyCatField = "cat_".$this->QuestionID;
									//@JAPR
									$this->createSurveyDimField();
								}
								
								//Crea las dimensiones Imagen y Documento para los tipos de pregunta que no requieren trato especial
								$this->createModelDimensionIMG();
								if ($this->QTypeID==qtpDocument) {
									$this->createModelDimensionDOC();
								}
								if (getMDVersion() >= esvQuestionAudioVideo) {
									if($this->QTypeID == qtpAudio || $this->QTypeID == qtpVideo) {
										$this->createModelDimensionDOC();
									}
								}
							}
							else 
							{
								//Este caso es exclusivamente para preguntas múltiple choice
								//Si la pregunta pertenece a una seccion dinamica debemos preguntar 
								$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
								
								//Verificamos antes si la pregunta multiple choice no sea de una
								//seccion dinamica dentro de la seccion
								//if($this->SectionID==$dynamicSectionID && $this->OutsideOfSection==0)
								if($this->SectionID==$dynamicSectionID)
								{
									//Primero se crea la dimension y unica no habra multidimensiones
									$this->createModelDimension();
									
									//Si es MCInputType = mpcNumeric quiere decir q es number y ademas 
									//requiere q se cree el indicador
									if($this->MCInputType==mpcNumeric && $this->IsIndicatorMCNew==1)
									{
										$this->createIndicatorKPI();
										
										//Crear campo dimension en tabla paralela con tipo de dato double/float
										$this->SurveyField = "dim_".$this->QuestionID;
										$this->createSurveyIndField();
									}
									else 
									{
										//Crear campo dimension en tabla paralela
										$this->SurveyField = "dim_".$this->QuestionID;
										$this->createSurveyDimField();
									}
									//ahora se permite activar la opcion de photos en secciones dinamicas
									$this->createModelDimensionIMG();
								}
								else 
								{
									//En esta parte creamos la dimension por cada una de las respuestas almacenadas en la tabla SI_SV_QAnswers
									//Se debe verificar si dicha pregunta de seleccion multiple se comporta como
									//una pregunta de multiples dimensiones
									//Este comportamiento solo aplica para las preguntas que no estan en secciones dinamicas 
									if($this->IsMultiDimension==1)
									{
										$this->createSelectOptionDims();
										
										//Si es MCInputType = mpcNumeric quiere decir q es number y ademas 
										//requiere q se cree el indicador por el valor de la propiedad IsIndicatorMCNew
										//@JAPR 2014-09-04: Corregido un bug, al grabar una pregunta nueva, sólo podría traer el valor de HasScores si se
										//estuviera basando de otra pregunta, por lo cual o se trata de un proceso de copiado, o bien se está basando
										//literalmente durante el proceso de creación de alguna pregunta, pero eso no se tiene por ahora contemplado ya
										//que el caso de herencia (SharedQuestionID) no reutiliza la instancia original para crear la nueva,
										//así que sólo durante una copia se puede invocar a la creación de indicadores ya que se habrían reseteado los 
										//mismos desde el método de BITAMSurvey::copyQuestions
										if(($this->MCInputType==mpcNumeric && $this->IsIndicatorMCNew==1) || ($this->HasScores && $iFromQuestionID > 0))
										{
											$this->createSelectOptionInds();
										}
									}
									else 
									{
										//En esta parte se puede dar el caso de una pregunta Category Dimension
										//Por lo que verificamos antes los valores siguientes
										if($this->QDisplayMode==dspVertical && $this->MCInputType==mpcNumeric && $this->IsMultiDimension==0 && $this->UseCategoryDimChoice!=0)
										{
											$this->createCategoryDimension();
										}
									}
									
									//Crear campo dimension en tabla paralela
									$this->SurveyField = "dim_".$this->QuestionID;
									$this->createSurveyDimField();
									
									$this->createModelDimensionIMG();
								}
							}
						}
					}
					else 
					{
						//Crear campo dimension en tabla paralela
						$this->SurveyField = "dim_".$this->QuestionID;
						$this->createSurveyDimField();
					}
				}
				//@JAPR
				
				//Actualizamos la jerarquia de preguntas de seleccion simple que utilizan el mismo catalogo
				if($this->QTypeID==qtpSingle && $this->UseCatalog==1)
				{
					$this->updateQuestionHierarchy();
				}
				
				//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, los Indicadores ya no aplican
				if ($this->CreationAdminVersion < esveFormsv6){
					$this->IsIndicator = $this->IsIndicatorNew;
					$this->IsIndicatorMC = $this->IsIndicatorMCNew;
				}
			}
		}
		else {
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			
			//@JAPR 2015-07-15: Modificado para ya no eliminar la pregunta al cambiar alguna propiedad que en las versiones anteriores si lo hacía, aunque se queden configuraciones
			//inconsistentes, se validará al momento de descargar definiciones para sólo enviar lo que sea aplicable, lo mismo en el resto de los procesos, pero ya no se forzará a un
			//borrado innecesario, así que se habilitará el grabado de todos los campos en este punto
				//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas (campo QDisplayMode)
				//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz (campo OptionsTextCol)
				//@JAPR 2011-06-29: Agregada la longitud de los campos Alfanumericos
				//Conchita 18-oct-2011
				//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales (Campo AllowAdditionalAnswers)
				//@JAPR 2012-04-30: Agregada la opción para configurar el estilo visual del componente (Campo QComponentStyleID)
				//@JAPR 2012-05-15: Agregadas las preguntas tipo Action (Campos ResponsibleID, CategoryID, DaysDueDate)
				//@JAPR 2012-05-29: Agregada la opción para indicar el número de decimales de las preguntas numéricas (campo DecimalPlaces)
				//@JAPR 2012-05-29: Agregado el soporte para creación de preguntas (single y multi) cuyas respuestas dependan de las seleccionadas de una pregunta multi previa (campo SourceQuestionID)
				//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas (campo FormatMask)
				//@JAPR 2012-10-19: Agregadas las preguntas marcadas como Read-Only
				$strAdditionalValues = '';
				//@JAPR 2012-10-29: Agregada la información extra para las acciones
				if (getMDVersion() >= esvExtendedActionsData) {
					$strAdditionalValues .= ', ActionTitle = '.$this->Repository->DataADOConnection->Quote($this->ActionTitle).
						', ActionText = '.$this->Repository->DataADOConnection->Quote($this->ActionText);
				}
				//@JAPR 2012-12-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
				if (getMDVersion() >= esveFormsV41Func) {
					$strAdditionalValues .= ', ReadOnly = '.$this->ReadOnly.
						', Formula = '.$this->Repository->DataADOConnection->Quote($this->Formula).
						', ShowCondition = '.$this->Repository->DataADOConnection->Quote($this->ShowCondition).
						', HasReqDocument = '.$this->HasReqDocument;
				}
				//@JAPR 2013-02-21: Agregada la opción para ocultar los textos de opciones de respuesta si tienen imagen asociada
				if (getMDVersion() >= esvCatalogDissociation) {
					$strAdditionalValues .= ', ShowImageAndText = '.$this->ShowImageAndText;
				}
				//@JAPR 2013-04-19: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
				if (getMDVersion() >= esveBavelMapping) {
					$strAdditionalValues .= ', eBavelFieldID = '.$this->eBavelFieldID;
				}
				//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
				if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
					$strAdditionalValues .= ', OtherLabel = '.$this->Repository->DataADOConnection->Quote($this->OtherLabel).
						', CommentLabel = '.$this->Repository->DataADOConnection->Quote($this->CommentLabel);
				}
				//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
				if (getMDVersion() >= esveBavelSuppActions123) {
					$strAdditionalValues .= ', eBavelActionFormID = '.$this->eBavelActionFormID;
				}
				//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
				if (getMDVersion() >= esvSimpleChoiceSharing) {
					$strAdditionalValues .= ', SharedQuestionID = '.$this->SharedQuestionID;
				}
				//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				if (getMDVersion() >= esvGPSQuestion) {
					$strAdditionalValues .= ', RegisterGPSAt = '.$this->RegisterGPSAt;
				}
				//@JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
				if (getMDVersion() >= esvPasswordLangAndRedesign) {
					$strAdditionalValues .= ', InputWidth = '.$this->InputWidth.
						', PictLayOut = '.$this->PictLayOut;
				}
				if (getMDVersion() >= esvNearBy) {
					$strAdditionalValues .= ', CatMemberLatitudeID = '.$this->CatMemberLatitudeID.
						', CatMemberLongitudeID = '.$this->CatMemberLongitudeID.
						', UseMap = '.$this->UseMap.
						', MapRadio = '.$this->MapRadio;
				}
				//@JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice
				if (getMDVersion() >= esvMCHSwitchBehav) {
					$strAdditionalValues .= ', EnableCheckBox = '.$this->EnableCheckBox.
						', SwitchBehaviour = '.$this->SwitchBehaviour;
				}
				if (getMDVersion() >= esvShowInSummary) {
					$strAdditionalValues .= ', ShowInSummary = '.$this->ShowInSummary;
				}
				if (getMDVersion() >= esvSketch) {
					$strAdditionalValues .= ', CanvasWidth = '.$this->Repository->DataADOConnection->Quote($this->canvasWidth);
					$strAdditionalValues .= ', CanvasHeight = '.$this->Repository->DataADOConnection->Quote($this->canvasHeight);
				}
				//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
				if (getMDVersion() >= esvExtendedModifInfo) {
					$strAdditionalValues .= ', LastUserID = '.$_SESSION["PABITAM_UserID"].
						', LastDateID = '.$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
						', LastVersion = '.$this->LastModAdminVersion;
				}
				//@JAPR 2014-09-29: Agregado el ancho de columnas en secciones Inline o el Summary de maestro-detalle
				if (getMDVersion() >= esvInlineMerge) {
					if (getMDVersion() >= esvColumnWidthString) {
						$strAdditionalValues .= ', ColumnWidth = '.$this->Repository->DataADOConnection->Quote($this->ColumnWidth);
					}
					else {
						$strAdditionalValues .= ', ColumnWidth = '.$this->ColumnWidth;
					}
				}
				//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
				if (getMDVersion() >= esvSectionQuestions) {
					$strAdditionalValues .= ', SourceSectionID = '.$this->SourceSectionID;
				}
				//@JAPR
				//Conchita 2014-10-11 agregado el sketchimage
				if (getMDVersion() >= esvSketchImage) {
					$strAdditionalValues .= ', SketchImage = '.$this->Repository->DataADOConnection->Quote($this->QuestionImageSketch);
		
				}
				//@JAPR 2014-10-22: Agregada la opción para redibujar las preguntas tipo mensaje automáticamente
				if (getMDVersion() >= esvInlineTypeOfFilling) {
					$strAdditionalValues .= ', AutoRedraw = '.$this->AutoRedraw;
				}
				//Sep-2014: Agregado el nuevo campo para definir que editor se utiliza
				if (getMDVersion() >= esvEditorHTML)
				{
					$strAdditionalValues .= ', EditorType = '.$this->EditorType
					.', QuestionMessageDes = '.$this->Repository->DataADOConnection->Quote($this->QuestionMessageDes);
				}
				//conchita 2014-12-01 agregado isinvisible
				if (getMDVersion() >= esvIsInvisible) {
					$strAdditionalValues .= ', IsInvisible = '.$this->IsInvisible;
				}					
				//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
				if (getMDVersion() >= esvAdminWYSIWYG) {
					$strAdditionalValues .= ', QuestionTextHTML = '.$this->Repository->DataADOConnection->Quote($this->QuestionTextHTML).
						', ValuesSourceType = '.$this->ValuesSourceType;
					//@JAPR 2015-07-16: Agregada la integración con DataSources
					//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
					$strAdditionalValues .= ', DataSourceID = '.$this->DataSourceID.', DataSourceMemberID = '.$this->DataSourceMemberID.
						', DataMemberLatitudeID = '.$this->DataMemberLatitudeID.', DataMemberLongitudeID = '.$this->DataMemberLongitudeID.
						', DataSourceFilter = '.$this->Repository->DataADOConnection->Quote($this->DataSourceFilter);
				}
				//OMMC 2015-11-30: Agregado para las preguntas OCR
				if(getMDVersion() >= esvOCRQuestionFields){
					$strAdditionalValues .= ', SavePhoto = '.$this->SavePhoto;
				}
				//@JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
				if (getMDVersion() >= esvMaxQLength) {
					$strAdditionalValues .= ', MaxQLength = '.$this->MaxQLength;
				}
				//@JAPR
				//OMMC 2016-01-18: Agregado para guardar el estado del editor de preguntas HTML
				if(getMDVersion() >= esvHTMLEditorState){
					$strAdditionalValues .= ', HTMLEditorState = '.$this->HTMLEditorState;
				}
				//@JAPR 2016-01-21: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
				if (getMDVersion() >= esvShowTotalsInline) {
					$strAdditionalValues .= ', ShowTotals = '.$this->ShowTotals.
						', TotalsFunction = '.$this->Repository->DataADOConnection->Quote($this->TotalsFunction);
				}
				//@JAPR
				//OMMC 2016-02-24: Agregado para permitir acceso a la galería
				if (getMDVersion() >= esvAllowGallery){
					$strAdditionalValues .= ', AllowGallery = '.$this->AllowGallery;
				}
				//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
				if (getMDVersion() >= esvSimpleChoiceCatOpts){
					$strAdditionalValues .= ', CatMemberImageID = '.$this->CatMemberImageID.', DataMemberImageID = '.$this->DataMemberImageID;
				}
				//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
				if (getMDVersion() >= esvUpdateData){
					$strAdditionalValues .= ', DataDestinationID = '.$this->DataDestinationID;
				}
				//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
				if (getMDVersion() >= esvNoFlow) {
					$strAdditionalValues .= ', NoFlow = '.$this->NoFlow;
				}
				//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
				if (getMDVersion() >= esvSChoiceMetro) {
					$strAdditionalValues .= ', CatMemberOrderID = '.$this->CatMemberOrderID.', DataMemberOrderID = '.$this->DataMemberOrderID.
						', DataMemberOrderDir = '.$this->DataMemberOrderDir.
						', CustomLayout = '.$this->Repository->DataADOConnection->Quote($this->CustomLayout);
				}

				//OMMC 2016-09-01: Modificado el comportamiento default de la pregunta llamada
				if (getMDVersion() >= esvPhoneCall) {
					$strAdditionalValues .= ', DisablePhoneNum = '.$this->DisablePhoneNum.', DisableSync = '.$this->DisableSync;
				}
				//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
				if (getMDVersion() >= esvImageDisplay) {
					$strAdditionalValues .= ', ImageWidth = '.$this->Repository->DataADOConnection->Quote($this->imageWidth);
					$strAdditionalValues .= ', ImageHeight = '.$this->Repository->DataADOConnection->Quote($this->imageHeight);
				}
				//@JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
				if (getMDVersion() >= esvUpdateDataWithFiles){
					$strAdditionalValues .= ', UploadMediaFiles = '.$this->UploadMediaFiles;
				}
				//@JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
				if (getMDVersion() >= esvRecalcDep) {
					$strAdditionalValues .= ', RecalculateDependencies = '.$this->RecalculateDependencies;
				}					
				//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
				if (getMDVersion() >= esvAutoSelectSChOpt){
					$strAdditionalValues .= ', AutoSelectEmptyOpt = '.$this->AutoSelectEmptyOpt;
				}
				//@JAPR
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				if (getMDVersion() >= esvQuestionAR) {
					$strAdditionalValues .= ', DataMemberZipFileID = '.$this->DataMemberZipFileID;
					$strAdditionalValues .= ', CatMemberZipFileID = '.$this->CatMemberZipFileID;
					$strAdditionalValues .= ', DataMemberVersionID = '.$this->DataMemberVersionID;
					$strAdditionalValues .= ', CatMemberVersionID = '.$this->CatMemberVersionID;
					$strAdditionalValues .= ', DataMemberPlatformID = '.$this->DataMemberPlatformID;
					$strAdditionalValues .= ', CatMemberPlatformID = '.$this->CatMemberPlatformID;
					$strAdditionalValues .= ', DataMemberTitleID = '.$this->DataMemberTitleID;
					$strAdditionalValues .= ', CatMemberTitleID = '.$this->CatMemberTitleID;
				}
				
				$sql = "UPDATE SI_SV_Question SET 
						QuestionText = ".$this->Repository->DataADOConnection->Quote($this->QuestionText)." 
						, AttributeName = ".$this->Repository->DataADOConnection->Quote($this->AttributeName)." 
						, IsFieldSurveyID = ".$this->IsFieldSurveyID." 
						, IsReqQuestion = ".$this->IsReqQuestion." 
						, HasReqComment = ".$this->HasReqComment." 
						, HasReqPhoto = ".$this->HasReqPhoto."
						, HasActions = ".$this->HasActions." 
						, QDisplayMode = ".$this->QDisplayMode." 
						, QLength = ".$this->QLength." 
						, IsIndicator = ".$this->IsIndicatorNew." 
						, OutsideOfSection = ".$this->OutsideOfSection." 
						, IsIndicatorMC = ".$this->IsIndicatorMCNew." 
						, GoToSection = ".$this->GoToQuestion." 
						, MinValue = ".$this->Repository->DataADOConnection->Quote($this->MinValue)." 
						, `MaxValue` = ".$this->Repository->DataADOConnection->Quote($this->MaxValue)." 
						, TextDisplayStyle = ".$this->Repository->DataADOConnection->Quote($this->TextDisplayStyle)."
						, AllowAdditionalAnswers = ".$this->AllowAdditionalAnswers."
						, ResponsibleID = ".$this->ResponsibleID."
						, CategoryID = ".$this->CategoryID."
						, DaysDueDate = ".$this->DaysDueDate."
						, QComponentStyleID = ".$this->QComponentStyleID."
						, DecimalPlaces = ".$this->DecimalPlaces."
						, SourceQuestionID = ".$this->SourceQuestionID."
						, FormatMask = ".$this->Repository->DataADOConnection->Quote($this->FormatMask)."
						, MaxChecked = ".$this->MaxChecked."
						, QuestionImageText = ".$this->Repository->DataADOConnection->Quote($this->QuestionImageText)."
						, QuestionMessage = ".$this->Repository->DataADOConnection->Quote($this->QuestionMessage)."
						, DateFormatMask = ".$this->Repository->DataADOConnection->Quote($this->DateFormatMask)."
						, DefaultValue = ".$this->Repository->DataADOConnection->Quote($this->DefaultValue)."
						, HasQuestionDep = ".$this->HasQuestionDep."
						, SourceSimpleQuestionID = ".$this->SourceSimpleQuestionID."
						, SummaryTotal = ".$this->SummaryTotal."
						, MCInputType = ".$this->MCInputType."
						, UseCatalog = ".$this->UseCatalog."
						, CatalogID = ".$this->CatalogID."
						, CatMemberID = ".$this->CatMemberID;
				$sql .= $strAdditionalValues." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Updating question: {$sql}", 2, 0, "color:blue;");
				}
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//@JAPR 2017-02-13: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				//Agregado el parámetro $bUpdateDep para indicar cuando se desea actualizar las dependencias. Originalmente usado sólo para el proceso de Generación de formas de producción
				if ($bUpdateDep) {
					//Actualizamos las dependencias padre-hija de campos calculados que existan
					if($this->HasQuestionDep) {
						$this->updateQuestionsToEval();
					}
					$this->updateFormulaChildren();
					$this->updateShowConditionChildren();
				}
				
				//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, los Indicadores o Dimensiones ya no aplican
				if ($this->CreationAdminVersion < esveFormsv6) {
					//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
					if(($this->QTypeID==qtpOpenNumeric || $this->QTypeID==qtpCalc) && $this->IsIndicatorNew!=$this->IsIndicatorBack)
					{
						//Si es Numerico el campo y se le indica que va a ser indicador tambien
						//entonces se crea el indicador
						if($this->IsIndicatorNew==1)
						{
							$this->createIndicatorKPI();
						}
						else 
						{
							//Por el contrario entonces se elimina dicho indicador
							$this->removeIndicatorKPI();
						}
					}
					else if($this->QTypeID==qtpMulti && $this->IsMultiDimension==1 && $this->MCInputType==mpcNumeric)
					{
						//Si la pregunta pertenece a una seccion dinamica debemos preguntar 
						$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
						
						if($this->SectionID!=$dynamicSectionID)
						{
							if($this->IsIndicatorMCNew!=$this->IsIndicatorMCBack)
							{
								//Si es Numerico el campo y se le indica que va a ser indicador tambien
								//entonces se crean los indicadores
								if($this->IsIndicatorMCNew==1)
								{
									$this->createSelectOptionInds();
								}
								else 
								{
									//Por el contrario entonces se eliminan dichos indicadores
									$this->removeSelectOptionInds();
								}
							}
						}
					}
				}
				
				//@JAPR 2012-06-14: Corregido un bug, esta validación había sido corregida para ya no eliminar la dimensión de la pregunta, pero
				//como aún estaba en este IF, no entraba a la parte que actualizaba otras propiedades de la pregunta así que al cambiar el nombre
				//corto de una siempre se forzaba a exclusivamente cambiar eso, no se podía combinar con otros ajustes
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				//@JAPRDescontinuada en v6, ya no se debe actualizar el AttributeName de forma independiente, ya es parte del UPDATE previo
				/*
				if($this->AttributeName!=$this->AttributeNameOld)
				{
					//@JAPR 2012-05-11: Corregido un bug, NO es necesario eliminar la dimensión (y por tanto las respuestas) para renombrar la
					//pregunta, ya que el nombre no se ocupa para generar ningún campo, en este caso símplemente renombramos tanto la pregunta
					//como la dimensión correspondiente
					//$this->saveNewAfterRemove($bJustMobileTables, $iMobileSurveyID, $iMobileSectionID);
					
					//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas (campo QDisplayMode)
					//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz (campo OptionsTextCol)
					//@JAPR 2011-06-29: Agregada la longitud de los campos Alfanumericos
					$sql = "UPDATE SI_SV_Question SET 
							AttributeName = ".$this->Repository->DataADOConnection->Quote($this->AttributeName)." 
						WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, los Indicadores o Dimensiones ya no aplican
					if ($this->CreationAdminVersion < esveFormsv6){
						//Agregado el renombrado de la dimensión correspondiente a la pregunta
						$this->updateQuestionNomLogico();
						//@JAPR
					}
				}
				*/
				
				//@JAPR 2015-05-19: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
				//estaban limitadas a 255 caracteres (#BA7866)
				//@JAPR 2016-06-15: Modificado el comportamiento del tamaño del texto de preguntas BarCode para que sea como las alfanuméricas (#TEI1OO)
				if (($this->QTypeID == qtpOpenAlpha || $this->QTypeID == qtpBarCode) && $this->QLength <> $this->OldQLength) {
					//En este caso la pregunta cambiará de tamaño para aumentar solamente, así que ejecuta el proceso que cambia su
					//tamaño tanto en la dimensión como en el tabla de la forma
					$this->updateQuestionLength();
				}

				//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, los Indicadores o Dimensiones ya no aplican
				if ($this->CreationAdminVersion < esveFormsv6) {
					//@JAPR 2012-06-14: Ahora se permitirá cambiar la opción de usar Foto durante la edición de preguntas
					//De acuerdo al código cuando se inserta la pregunta, prácticamente cualquier tipo de pregunta excepto ShowValue pudiera tener
					//una foto, así que se agrega la dimensión para todas (se valida si debe o no agregarla dentro de la función)
					//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
					//La pregunta tipo GPS no es visible, así que no hay posibilidad de que se pueda tomar una foto para ella
					//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
					if ($this->QTypeID != qtpShowValue && ($this->QTypeID != qtpGPS || $this->QTypeID != qtpMyLocation))
					{
						$this->updateModelDimensionIMG();
						//@JAPR 2014-09-25: Corregido un bug, no estaba volviendo a construir la dimensión documento en caso de no existir cuando se editaba
						if ($this->QTypeID==qtpDocument) {
							$this->updateModelDimensionDOC();
						}
						if (getMDVersion() >= esvQuestionAudioVideo) {
							if($this->QTypeID == qtpAudio || $this->QTypeID == qtpVideo) {
								$this->updateModelDimensionDOC();
							}
						}
						//@JAPR
					}
					
					//@JAPR 2012-11-20: Corregido un bug, ahora cuando se edite después de que por alguna razón no se pudo crear la dimensión de 
					//la pregunta, se va a intentar crear en este punto siguiendo las mismas reglas como si fuera la primera vez
					if ($this->IndDimID <= 0) {
						$blnCreateIndDimID = false;
						switch ($this->QTypeID) {
							case qtpPhoto:
							case qtpSignature:
							case qtpSketch:
							case qtpShowValue:
							case qtpMessage:
							case qtpMapped:
							case qtpSkipSection:
							//@JAPR 2013-04-12: Agregados nuevos tipos de pregunta Sincronización
							case qtpSync:
							//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
							//La pregunta GPS crea su dimensión dentro de la función especializada, así que no es necesario hacerlo en este punto
							case qtpGPS:
								//Estas preguntas no crean dimensión en el cubo
							//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
							case qtpPassword:
							//@JAPR 2014-10-02: Corregido un bug, estos tipos de pregunta estaban creando la dimensión valor siendo que no lo requieren
							case qtpDocument:
							case qtpAudio:
							case qtpVideo:
							//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
							case qtpSection:
							//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
							case qtpExit:
							//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
							case qtpUpdateDest:
							//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
							case qtpMyLocation:
								break;
								
							case qtpCallList:
							case qtpSingle:
								//Conchita 2013-04-29 agregado el tipo calllist
								//En el caso de simple choice, si utiliza un catálogo entonces se debe crear la dimensión pero de tipo catálogo,
								//si no lo usa entonces si crea la dimensión normal
								if($this->UseCatalog==0) {
									if($this->QDisplayMode <> dspMatrix) {
										$blnCreateIndDimID = true;
									}
								}
								else {
									$this->createCatalogDimension();
								}
								break;
							
							case qtpMulti:
								//En el caso de múltiple choice, sólo se crean si pertenecen a la sección dinámica
								if($this->SectionID==$dynamicSectionID) {
									$blnCreateIndDimID = true;
								}
								break;
							
							case qtpOpenNumeric:
							case qtpCalc:
							default:
								$blnCreateIndDimID = true;
								break;
						}
						
						//Crea la dimensión
						if ($blnCreateIndDimID) {
							$this->createModelDimension();
						}
					}
					
					//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
					//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
					if ($this->QTypeID == qtpGPS || $this->QTypeID == qtpMyLocation) {
						//Para este tipo de preguntas se generan 3 dimensiones, una de ella es la dimensión base donde se graba la posición como un par
						//"Latitude,Longitude", mientras que las otras dos son dimensiones-atributo precisamente para grabar por separado (pero ligadas
						//a la base) la Latitude y Longitude. No requiere imagen, comentario ni captura en general de nada extra
						$this->CreateGPSDimension();
					}
					//@JAPR 2012-12-21: Corregido un bug, si las dimensiones de las opciones de respuesta no se habían generado, este proceso las volverá a crear
					elseif ($this->QTypeID == qtpMulti && $this->IsMultiDimension == 1 && $this->SectionID != $dynamicSectionID) {
						$this->createSelectOptionDims();
					}
					//@JAPR
				}
				
				//Se va actualizar el SectionID en el dado caso que este haya sido modificado
				if ($this->SectionID != $this->OldSectionID) {
					$currentSectionNumber = $this->SectionNumber;
					//Obtenemos el SectionNumber del nuevo sectionID a donde se va a trasladar el question
					//@JAPR 2015-07-29: Corregido un bug, al cambiar la sección de la pregunta, no estaba reconstruyendo la columna correctamente porque faltaba actualizar información
					//de la sección que se quedaba apuntando a la sección anterior. Se modificó para mejor cargar la instancia de Sección en este punto (#TJFQKT)
					$objSection = BITAMSection::NewInstanceWithID($this->Repository, $this->SectionID);
					if (!is_null($objSection)) {
						$newSectionNumber = $objSection->SectionNumber;
						$this->SectionType = $objSection->SectionType;
					}
					else {
						$newSectionNumber = BITAMSection::getSectionNumber($this->Repository, $this->SectionID);
					}
					
					$prevOrderPos = $this->QuestionNumber;
					if ($newSectionNumber != 0) {
						if($newSectionNumber>$currentSectionNumber)
						{
							$selectField = $this->Repository->DataADOConnection->IfNull("MAX(A.QuestionNumber)", "0")." AS QuestionNumber";
						}
						else 
						{
							$selectField = $this->Repository->DataADOConnection->IfNull("MAX(A.QuestionNumber)", "0")." + 1 AS QuestionNumber";
						}
						
						//Obtener el OrderPos
						$sqlOrder =  "SELECT ".$selectField." FROM SI_SV_Question A, SI_SV_Section B 
									WHERE A.SurveyID = ".$this->SurveyID." AND A.SectionID = B.SectionID 
									AND B.SectionNumber <= ".$newSectionNumber;
						$aRS = $this->Repository->DataADOConnection->Execute($sqlOrder);
						if (!$aRS || $aRS->EOF)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question, SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlOrder);
						}
						
						$orderPos = (int)$aRS->fields["questionnumber"];
						
						//En el dado caso que si sean diferentes la posicion que heredara el question entonces si se realiza
						//reordamiento en las tablas de las preguntas, en caso contrario, no se realiza y solo se efectua el
						//cambio de seccion
						if($orderPos!=$prevOrderPos)
						{
							BITAMQuestionCollection::Reorder($this->Repository, $this->OldSectionID, $this->QuestionID, $orderPos);
						}
						
						$sql = "UPDATE SI_SV_Question SET SectionID = ".$this->SectionID." 
								WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
					else 
					{
						$this->SectionID = $this->OldSectionID;
					}
				}
				
				//Actualizar datos de modificacion de la encuesta
				BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
				
				//@JAPR 2016-04-25: Validado que si se limpia el catálogo se debe remover la instancia, ya que no habrá mas elementos que lo utilicen en v6 (#6AEB6O)
				if ($this->CreationAdminVersion >= esveFormsv6) {
					//Al editar si se eliminó la referencia al catálogo, debe eliminar la instancia en la metadata
					if ($this->CatalogID <= 0 && $this->CatalogIDOld > 0) {
						$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogIDOld);
						if (!is_null($objCatalog)) {
							$objCatalog->remove();
						}
					}
				}
				
			//@JAPR 2015-07-15: Modificado para ya no eliminar la pregunta al cambiar alguna propiedad que en las versiones anteriores si lo hacía, aunque se queden configuraciones
			//inconsistentes, se validará al momento de descargar definiciones para sólo enviar lo que sea aplicable, lo mismo en el resto de los procesos, pero ya no se forzará a un
			//borrado innecesario, así que se habilitará el grabado de todos los campos en este punto
			
			//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, los Indicadores o Dimensiones ya no aplican
			if ($this->CreationAdminVersion < esveFormsv6){
				//@JAPR 2014-12-17: Corregido un bug, no estaba heredando el formato de la pregunta numérica
				//Si la pregunta es aplicable para contener indicadores con formato, actualiza el formato en Artus de todos los que se necesiten
				$blnUpdateOwnIndicator = false;
				$blnUpdateOptionsIndicators = false;
				switch ($this->QTypeID) {
					case qtpOpenNumeric:
					case qtpCalc:
						//Este tipo de preguntas sólo contiene su propio indicador, siempre tiene formato
						if ($this->IsIndicatorNew == 1) {
							$blnUpdateOwnIndicator = true;
						}
						break;
						
					case qtpMulti:
						//Este tipo de preguntas depende de la sección donde se utiliza, si es dentro de una dinámica entonces sólo puede contener
						//un indicador para ella misma, de lo contrario podría contener indicadores para sus opciones de respuesta, en ambos casos
						//sólo si es captura tipo numérica, ya que en otras situaciones puede contener indicadores pero de Score y esos no requieren
						//formato personalizado
						if ($this->IsIndicatorMCNew == 1) {
							if ($this->SectionID == $dynamicSectionID) {
								$blnUpdateOwnIndicator = true;
							}
							else {
								$blnUpdateOptionsIndicators = true;
							}
						}
						break;
				}
				
				if ($blnUpdateOwnIndicator) {
					//En este caso sólo hay un indicador y está asociado directamente a la pregunta
					if ($this->IndicatorID > 0) {
						$sql = "UPDATE SI_INDICADOR SET FORMATO = ".$this->Repository->ADOConnection->Quote($this->FormatMask)." 
							WHERE CLA_INDICADOR = {$this->IndicatorID}";
						$this->Repository->ADOConnection->Execute($sql);
					}
				}
				
				if ($blnUpdateOptionsIndicators) {
					//En este caso puede haber muchos indicadores, cada uno asociado a las opciones de respuesta de esta pregunta, así que primero
					//se identifica cada uno de ellos
					$sql = "SELECT QuestionID, ConsecutiveID, IndicatorID 
						FROM SI_SV_QAnswers 
						WHERE QuestionID = {$this->QuestionID}";
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS) {
						$arrIndicatorIDs = array();
						while (!$aRS->EOF) {
							$intIndicatorID = (int) @$aRS->fields["indicatorid"];
							if ($intIndicatorID > 0) {
								$arrIndicatorIDs[] = $intIndicatorID;
							}
							$aRS->MoveNext();
						}
						
						if (count($arrIndicatorIDs) > 0) {
							$sql = "UPDATE SI_INDICADOR SET FORMATO = ".$this->Repository->ADOConnection->Quote($this->FormatMask)." 
								WHERE CLA_INDICADOR IN (".implode(',', $arrIndicatorIDs).")";
							$this->Repository->ADOConnection->Execute($sql);
						}
					}
				}
				//@JAPR
			}
		}
		
		if(!$bCopy)
        {
			//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, los Indicadores o Dimensiones ya no aplican
			if ($this->CreationAdminVersion < esveFormsv6){
				//@JAPR 2013-02-01: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($this->QTypeID == qtpMulti && $this->SectionID == $dynamicSectionID) {
					//Si la pregunta es multiple choice dentro de una sección dinámica, se tiene que agregar al modelo el atributo que usará esta pregunta
					//por lo que invoca al método de la sección que ya se encarga de eso
					$objSection = BITAMSection::NewInstanceWithID($this->Repository, $this->SectionID);
					if (!is_null($objSection)) {
						//@JAPR 2013-02-19: Corregido un bug, durante la creación de una pregunta multiple choice en sección dinámica, se cargaba la instancia
						//de section antes de crear la pregunta, así que cuando la cargaba de nuevo para validar si había o no multiple choice la instancia no se
						//leía de la metadata y quedaba con la propiedad sin asignar, provocando que no se agregara la dimensión correspondiente, ahora mejor se
						//separará el método para identificar las multiple choice dentro de la sección
						$objSection->getChildCatMemberID();
						//@JAPR
						
						$objSection->createDissociatedCatalogDimension();
					}
				}
			}
			
			//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
			if (trim($gblModMngrErrorMessage) != '') {
				die("(".__METHOD__.") ".translate("There were some errors while saving the question").":\r\n".$gblModMngrErrorMessage.". ");
			}
			
			//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
			//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
			//Estas propiedades están descontinuadas a partir de eForms v6
			if (getMDVersion() < esveFormsv6 && getMDVersion() >= esveBavelSuppActions123) {
				if ($this->eBavelActionFormID > 0) {
					if ($this->eBavelActionFieldsData != $this->eBavelActionFieldsDataOld) {
						$this->saveeBavelActionFieldsMapping();
					}
				}
			}
			
			//@JAPR 2013-12-05: Agregada la opción para compartir las opciones de respuesta con otras preguntas
			//Si no se forzó a reconstruir la pregunta y es una edición, entonces se lanza una proceso que verifica simplemente si todas las opciones
			//de respuesta de la pregunta origen se encuentran grabadas como opciones de respuesta de esta pregunta y agrega las faltantes. Si se
			//hubiera creado recien la pregunta, el método Save habría asignado el string de opciones de respuesta desde la pregunta origen para que
			//se crearan localmente todos los indicadores y dimensiones según el caso, pero se tiene que entrar a este proceso para generar el mapeo de
			//las opciones de respuesta, si se hubiera cambiado la pregunta fuente, habría entrado al método saveNewAfterRemove el cual ya habría generado
			//todo lo necesario tal como si fuera una creación de pregunta, pero aún así se debe entrar a este método para generar el mapeo
			if (getMDVersion() >= esvSimpleChoiceSharing) {
				$this->checkSharedQuestionOptionMap();
			}
			
			//@JAPR 2014-07-31: Agregado el Responsive Design
			//@JAPR 2018-07-09: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
			if (getMDVersion() < esveFormsv6 ) {
				if (getMDVersion() >= esvResponsiveDesign) {
					$this->saveResponsiveDesignProps();
				}
			}
			//@JAPR
		}
		//@JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
		//Si está configurada como pregunta de catálogo y hay un DataSource asignado, debe crear el catálogo correspondiente al mismo
        //$this->ValuesSourceType == tofCatalog && $this->DataSourceID > 0 && $this->DataSourceMemberID > 0
        if ($this->ValuesSourceType == tofCatalog && $this->DataSourceID > 0 && $this->DataSourceMemberID > 0 && $this->CreateCat) {
			//Si el DataSource utilizado ha cambiado, se deben resetear todas las configuraciones que utilizaban el DataSource en esta pregunta porque quedarían inconsistentes (quien
			//ha invocado al grabado debe hacer también una actualización de la definición)
			$blnChangeDataSource = false;
            if($bCopy)
            {    //RV Sep2016: Cuando se está copiando una forma se inicializan estos valores para crear un catálogo nuevo para la sección
                $this->CatalogID = 0;
                $this->CatMemberID = 0;
                $this->CatalogIDOld = 0;
            }
			//@JAPR 2015-07-31: Corregido un bug, toda esta validación es correcta excepto si apenas se está definiendo el catálogo, porque en ese caso
			//removía las configuraciones de definidas durante la creación
			if ($this->DataSourceID != $this->DataSourceIDOld && (!$isNewObj)) {
				$blnChangeDataSource = true;
				$this->DataSourceMemberID = 0;
				$this->DataMemberLatitudeID = 0;
				$this->DataMemberLongitudeID = 0;
				$this->CatMemberID = 0;
				$this->CatMemberLatitudeID = 0;
				$this->CatMemberLongitudeID = 0;
				//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
				$this->DataMemberImageID = 0;
				$this->CatMemberImageID = 0;
				//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				$this->DataMemberOrderID = 0;
				$this->CatMemberOrderID = 0;
				//@JAPR
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				$this->DataMemberZipFileID = 0;
				$this->CatMemberZipFileID = 0;
				$this->DataMemberVersionID = 0;
				$this->CatMemberVersionID = 0;
				$this->DataMemberPlatformID = 0;
				$this->CatMemberPlatformID = 0;
				$this->DataMemberTitleID = 0;
				$this->CatMemberTitleID = 0;
			}
			
			$intCatalogID = $this->CatalogID;
			$intCatMemberID = $this->DataSourceMemberID;
			$intCatMemberLatitudeID = $this->DataMemberLatitudeID;
			$intCatMemberLongitudeID = $this->DataMemberLongitudeID;
			//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			$intCatMemberImageID = $this->DataMemberImageID;
			//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			$intCatMemberOrderID = $this->DataMemberOrderID;
			//@JAPR
			//OMMC 2019-03-25: Pregunta AR #4HNJD9
			$intCatMemberZipFileID = $this->DataMemberZipFileID;
			$intCatMemberVersionID = $this->DataMemberVersionID;
			$intCatMemberPlatformID = $this->DataMemberPlatformID;
			$intCatMemberTitleID = $this->DataMemberTitleID;
			$arrMappedAttribs = array();
			//@JAPR 2015-07-29: Corregido un bug, al corregir el problema de remapeo de atributos, había faltado entrar a esta condición si hay un atributo seleccionado
			//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			if ($intCatMemberID > 0 || $intCatMemberLatitudeID > 0 || $intCatMemberLongitudeID > 0 || $intCatMemberImageID > 0 || $intCatMemberOrderID > 0 || $intCatMemberZipFileID > 0 || $intCatMemberVersionID > 0 || $intCatMemberPlatformID > 0) {
				$arrMappedAttribs['DataMemberID'] = array('DataSourceMemberID' => $intCatMemberID);
				$arrMappedAttribs['DataMemberLatitudeID'] = array('DataSourceMemberID' => $intCatMemberLatitudeID);
				$arrMappedAttribs['DataMemberLongitudeID'] = array('DataSourceMemberID' => $intCatMemberLongitudeID);
				//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
				$arrMappedAttribs['DataMemberImageID'] = array('DataSourceMemberID' => $intCatMemberImageID);
				//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				$arrMappedAttribs['DataMemberOrderID'] = array('DataSourceMemberID' => $intCatMemberOrderID);
				//@JAPR
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
				//Corregido un bug, se habían agregado los nuevos campos al proceso pero no con el índice DataSourceMemberID que este proceso requiere, por eso no los mapeaba en los campos
				//CatMember correspondientes
				$arrMappedAttribs['DataMemberZipFileID'] = array('DataSourceMemberID' => $intCatMemberZipFileID);
				$arrMappedAttribs['DataMemberVersionID'] = array('DataSourceMemberID' => $intCatMemberVersionID);
				$arrMappedAttribs['DataMemberPlatformID'] = array('DataSourceMemberID' => $intCatMemberPlatformID);
				$arrMappedAttribs['DataMemberTitleID'] = array('DataSourceMemberID' => $intCatMemberTitleID);
			}
			
			//@JAPR 2015-10-09: Corregido un bug, si la pregunta NO era de tipo Simple choice Menú, el Array de CatMembersList no venía asignado así que al no enviarse a este
			//proceso terminaba reordenando los atributos del catálogo de la pregunta en el orden del DataSource, eso afectaba a la pregunta tipo Auto-complete (esto sucedía al
			//guardar cualquier otra propiedad excepto el Atributo), así que ahora se enviará un array local con el Atributo seleccionado si es que hay alguno (#1L4L7Z)
			//@JAPR 2016-04-22: Corregido un bug similar al de arriba, si la pregunta actualmente NO es tipo Menú, este array de CatMembersList se estaba quedando asignado con la última
			//configuración de atributos que hubieran usado cuando era menú, eso provocaba un reordenamiento inusual al cambiar cualquier atributo adicional (como Imagen, Latitud, Longitud, 
			//etc.) así que se forzará a limpiar el array cuando la pregunta no sea tipo menú (#VZ88H6)
			//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
			if ($this->QDisplayMode == dspMenu || $this->QDisplayMode == dspMetro) {
				$arrDataSourceMembersIDs = $this->CatMembersList;
			}
			else {
				$arrDataSourceMembersIDs = null;
			}
			
			if (is_null($arrDataSourceMembersIDs) || !is_array($arrDataSourceMembersIDs) || count($arrDataSourceMembersIDs) == 0) {
				if ($this->DataSourceMemberID > 0) {
					$arrDataSourceMembersIDs[] = $this->DataSourceMemberID;
				}
				else {
					$arrDataSourceMembersIDs = array();
				}
			}
			
			$arrReturn = BITAMCatalog::CreateUpdateCatalogFromDataSource($this->Repository, $this->DataSourceID, $arrDataSourceMembersIDs, $intCatalogID, $arrMappedAttribs);
			//@JAPR
			$intCatMemberID = 0;
			$intCatMemberLatitudeID = 0;
			$intCatMemberLongitudeID = 0;
			//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			$intCatMemberImageID = 0;
			//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			$intCatMemberOrderID = 0;
			//@JAPR
			//OMMC 2019-03-25: Pregunta AR #4HNJD9
			$intCatMemberZipFileID = 0;
			$intCatMemberVersionID = 0;
			$intCatMemberPlatformID = 0;
			$intCatMemberTitleID = 0;
			if ($arrReturn !== false && is_array($arrReturn)) {
				$intCatalogID = (int) @$arrReturn["CatalogID"];
				$intCatMemberID = (int) @$arrReturn["DataMemberID"];
				$intCatMemberLatitudeID = (int) @$arrReturn["DataMemberLatitudeID"];
				$intCatMemberLongitudeID = (int) @$arrReturn["DataMemberLongitudeID"];
				//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
				$intCatMemberImageID = (int) @$arrReturn["DataMemberImageID"];
				//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				$intCatMemberOrderID = (int) @$arrReturn["DataMemberOrderID"];
				//@JAPR
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				$intCatMemberZipFileID = (int) @$arrReturn["DataMemberZipFileID"];;
				$intCatMemberVersionID = (int) @$arrReturn["DataMemberVersionID"];;
				$intCatMemberPlatformID = (int) @$arrReturn["DataMemberPlatformID"];;
				$intCatMemberTitleID = (int) @$arrReturn["DataMemberTitleID"];;
			}
			//@JAPR 2015-07-29: Removida la validación de IDs positivos, ya que se puede limpiar la configuración y debe hacer el Update (en caso donde cambie el DataSource por ejemplo)
			/*
			if (($intCatalogID != $this->CatalogID && $intCatalogID > 0) || ($intCatMemberID != $this->CatMemberID && $intCatMemberID > 0) ||
					($intCatMemberLatitudeID != $this->DataMemberLatitudeID && $intCatMemberLatitudeID > 0) || 
					($intCatMemberLongitudeID != $this->DataMemberLongitudeID && $intCatMemberLongitudeID > 0)) {
			*/
			//@JAPR 2015-10-12: Corregido un bug, la comparación de CatMemberLatitudeID y CatMemberLongitudeID las estaba haciendo contra DataMemberLatitudeID y DataMemberLongitudeID,
			//lo cual por definición es incorrecto porque queremos saber si han cambiado dichos valores, así que compararlos contra un dato que no tiene relación alguna con ellos
			//es casi un hecho que si serían diferentes prácticamente siempre, sin embargo en el caso del primer catálogo para la primer pregunta tipo mapa, el DataSource y el Catálogo
			//correspondiente a dicha pregunta tenían exactamente la misma estructura y tomaron los mismos IDs, así que esta condición por el contrario nunca se cumplía, provocando que
			//dicho mapa sólo mostrara un punto cercano a Africa aunque todo estuviera correctamente definido
			//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			//OMMC 2019-03-25: Pregunta AR #4HNJD9
			if ($blnChangeDataSource || $intCatalogID != $this->CatalogID || $intCatMemberID != $this->CatMemberID ||
					$intCatMemberLatitudeID != $this->CatMemberLatitudeID || $intCatMemberLongitudeID != $this->CatMemberLongitudeID ||
					$intCatMemberImageID != $this->CatMemberImageID || $intCatMemberOrderID != $this->CatMemberOrderID ||
					$intCatMemberZipFileID != $this->CatMemberZipFileID || $intCatMemberVersionID != $this->CatMemberVersionID ||
					$intCatMemberPlatformID != $this->CatMemberPlatformID || $intCatMemberTitleID != $this->CatMemberTitleID) {
				$strAnd = "";
				$strAdditionalValues = "";
				if ($intCatalogID != $this->CatalogID) {
					$strAdditionalValues .= $strAnd."CatalogID = {$intCatalogID}";
					$strAnd = ", ";
				}
				if ($blnChangeDataSource) {
					//Si el DataSource fue modificado, entonces es irrelevante lo que se había preparado para actualizar, ya que todos los IDs de configuraciones de atributos se resetean
					//a 0 exceptuando el catálogo, el cual se debe ajustar al nuevo DataSource
					$strAdditionalValues .= $strAnd."CatMemberID = 0, CatMemberLatitudeID = 0, CatMemberLongitudeID = 0, 
						DataSourceMemberID = 0, DataMemberLatitudeID = 0, DataMemberLongitudeID = 0";
					//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
					if (getMDVersion() >= esvSimpleChoiceCatOpts){
						$strAdditionalValues .= ", CatMemberImageID = 0, DataMemberImageID = 0";
					}
					//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
					if (getMDVersion() >= esvSChoiceMetro){
						$strAdditionalValues .= ", CatMemberOrderID = 0, DataMemberOrderID = 0";
					}
					//@JAPR
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					if (getMDVersion() >= esvQuestionAR) {
						$strAdditionalValues .= ", CatMemberZipFileID = 0, CatMemberVersionID = 0, CatMemberPlatformID = 0, CatMemberTitleID = 0";
					}
				}
				else {
					if ($intCatMemberID != $this->CatMemberID) {
						$strAdditionalValues .= $strAnd."CatMemberID = {$intCatMemberID}";
						$strAnd = ", ";
					}
					//@JAPR 2015-10-12: Corregido un bug, la comparación de CatMemberLatitudeID y CatMemberLongitudeID las estaba haciendo contra DataMemberLatitudeID y DataMemberLongitudeID,
					//lo cual por definición es incorrecto porque queremos saber si han cambiado dichos valores, así que compararlos contra un dato que no tiene relación alguna con ellos
					//es casi un hecho que si serían diferentes prácticamente siempre, sin embargo en el caso del primer catálogo para la primer pregunta tipo mapa, el DataSource y el Catálogo
					//correspondiente a dicha pregunta tenían exactamente la misma estructura y tomaron los mismos IDs, así que esta condición por el contrario nunca se cumplía, provocando que
					//dicho mapa sólo mostrara un punto cercano a Africa aunque todo estuviera correctamente definido
					if ($intCatMemberLatitudeID != $this->CatMemberLatitudeID) {
						$strAdditionalValues .= $strAnd."CatMemberLatitudeID = {$intCatMemberLatitudeID}";
						$strAnd = ", ";
					}
					if ($intCatMemberLongitudeID != $this->CatMemberLongitudeID) {
						$strAdditionalValues .= $strAnd."CatMemberLongitudeID = {$intCatMemberLongitudeID}";
						$strAnd = ", ";
					}
					//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
					if (getMDVersion() >= esvSimpleChoiceCatOpts){
						if ($intCatMemberImageID != $this->CatMemberImageID) {
							$strAdditionalValues .= $strAnd."CatMemberImageID = {$intCatMemberImageID}";
							$strAnd = ", ";
						}
					}
					//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
					if (getMDVersion() >= esvSChoiceMetro){
						if ($intCatMemberOrderID != $this->CatMemberOrderID) {
							$strAdditionalValues .= $strAnd."CatMemberOrderID = {$intCatMemberOrderID}";
							$strAnd = ", ";
						}
					}
					//@JAPR
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					if (getMDVersion() >= esvQuestionAR) {
						if ($intCatMemberZipFileID != $this->CatMemberZipFileID) {
							$strAdditionalValues .= $strAnd."CatMemberZipFileID = {$intCatMemberZipFileID}";
							$strAnd = ", ";
						}
						if ($intCatMemberVersionID != $this->CatMemberVersionID) {
							$strAdditionalValues .= $strAnd."CatMemberVersionID = {$intCatMemberVersionID}";
							$strAnd = ", ";
						}
						if ($intCatMemberPlatformID != $this->CatMemberPlatformID) {
							$strAdditionalValues .= $strAnd."CatMemberPlatformID = {$intCatMemberPlatformID}";
							$strAnd = ", ";
						}
						if ($intCatMemberTitleID != $this->CatMemberTitleID) {
							$strAdditionalValues .= $strAnd."CatMemberTitleID = {$intCatMemberTitleID}";
							$strAnd = ", ";
						}
					}
				}
				
				if ($strAdditionalValues) {
					//En este caso si se logró crear un catálogo a partir del DataSource, así que actualiza la información
					$sql = "UPDATE SI_SV_Question SET {$strAdditionalValues} 
						WHERE QuestionID = {$this->QuestionID}";
					$this->Repository->DataADOConnection->Execute($sql);
				}
			}
		}
		
		//@JAPR 2015-11-17: Corregido un bug, si el DataSource fue limpiado, se debe remover la configuración de Atributos corresponidiente, pero al no haber entrado al If previo
		//por no traer DataSource, no hizo la limpieza durante dicho proceso (#VQ92PE)
		$blnRemoveCatMembersList = false;
		if ($this->ValuesSourceType == tofCatalog && $this->DataSourceID <= 0) {
			//En este caso al no haber un DataSource asignado, simplemente limpiará todas las configuraciones que se relacionen con atributos tanto de DataSource como de Catalog
			//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			$strAdditionalValues = "";
			if (getMDVersion() >= esvSimpleChoiceCatOpts){
				$strAdditionalValues .= ", CatMemberImageID = 0, DataMemberImageID = 0";
			}
			//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			if (getMDVersion() >= esvSChoiceMetro) {
				$strAdditionalValues .= ", CatMemberOrderID = 0, DataMemberOrderID = 0";
			}
			//@JAPR
			//OMMC 2019-03-25: Pregunta AR #4HNJD9
			if (getMDVersion() >= esvQuestionAR) {
				$strAdditionalValues .= ", CatMemberZipFileID = 0, CatMemberVersionID = 0, CatMemberPlatformID = 0, CatMemberTitleID = 0";
			}
			
			$sql = "UPDATE SI_SV_Question SET CatMemberID = 0, 
					CatMemberLatitudeID = 0, 
					CatMemberLongitudeID = 0, 
					DataSourceMemberID = 0, 
					DataMemberLatitudeID = 0, 
					DataMemberLongitudeID = 0 
					{$strAdditionalValues} 
				WHERE QuestionID = {$this->QuestionID}";
			$this->Repository->DataADOConnection->Execute($sql);
			
			//@JAPR 2016-12-23: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
			//Agregadas validaciones adicionales para que limipie los atributos si ha cambiado el tipo de llenado pero el nuevo NO es de catálogo, o bien si se habilitó la bandera adicional
			//la cual a la fecha de esta implementación se hacía cuando se limpiaba el catálogo a utilizar, ya que se estaba quedando la referencia de catalogID y por ende la instancia de
			//Catálogo apartado por si se elegía otro, pero no tenía sentido pues eso impedía eliminarlo así que se optará por removerlo
			$blnRemoveCatMembersList = true;
			$this->CatMembersList = array();
			
			//En este punto se removerá además la instancia del catálogo asociado a esta pregunta
			if (!$isNewObj && $this->CreationAdminVersion >= esveFormsv6) {
				//Al editar si se indicó que ya no se usará un DataSource pero había evidentemente un Catálogo asociado, ahora se eliminará la instancia del catálogo, siendo que arriba
				//ya se removieron las referencias de atributos, algo que realmente sería innecesario dado a que al eliminar el catálogo automáticamente las removería
				if ($this->CatalogID > 0) {
					$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogIDOld);
					if (!is_null($objCatalog)) {
						$objCatalog->remove();
					}
				}
			}
			//@JAPR
		}
		
		//@JAPR 2016-12-20: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
		//@JAPR 2016-12-23: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
		//Agregadas validaciones adicionales para que limipie los atributos si ha cambiado el tipo de llenado pero el nuevo NO es de catálogo, o bien si se habilitó la bandera adicional
		//la cual a la fecha de esta implementación se hacía cuando se limpiaba el catálogo a utilizar, ya que se estaba quedando la referencia de catalogID y por ende la instancia de
		//Catálogo apartado por si se elegía otro, pero no tenía sentido pues eso impedía eliminarlo así que se optará por removerlo
		if (!$isNewObj && (($this->ValuesSourceType != $this->ValuesSourceTypeOld && $this->ValuesSourceType != tofCatalog) || $blnRemoveCatMembersList)) {
			$this->saveCatMembersIDs();
		}
		//@JAPR
		
		/*@AAL 23/06/2015 Agregado para EformsV6 y siguientes, Se crea la Tabla paralela para el caso de 
		las preguntas SimpChoice y MultChoice*/
		$this->createDataTablesAndFields($isNewObj);
        if(!$bCopy)
        {
			//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData, se requiere un Join con el DataSource
			$this->saveDynamicFilter();
			
			//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
			//Si el tipo de pregunta que se está grabando es Update Data, para este momento ya debe tener su colección de opciones de respuesta grabadas (vacías si es una pregunta nueva), así 
			//que carga la instancia y actualiza el valor del salto de cada una de ellas según se recibió
			if ($this->QTypeID == qtpUpdateDest) {
				$objQuestionOptionColl = BITAMQuestionOptionCollection::NewInstance($this->Repository, $this->QuestionID);
				if (!is_null($objQuestionOptionColl)) {
					foreach ($objQuestionOptionColl->Collection as $objQuestionOption) {
						switch (trim(strtolower($objQuestionOption->QuestionOptionName))) {
							case 'true':
								$objQuestionOption->NextQuestion = $this->NextSectionIDIfTrue;
								break;
							default:
								$objQuestionOption->NextQuestion = $this->NextSectionIDIfFalse;
								break;
						}
						$objQuestionOption->save();
					}
				}
			}
			//@JAPR
		}
		return $this;
	}

	//@AAL 23/06/2015 Agregado para EformsV6 y siguientes, Se crea la Tabla para Preguntas tipo Simple Choice o Multiple Choice
	//El parámetro $bIsNew nos indicará si se estaba creando o no el objeto, ya que siempre que se invoca ya tiene activado un QuestionID, así que no hay forma de identificar si se estaba
	//creando o no realmente con la propiedad isNewObject en este punto
	function createDataTablesAndFields($bIsNew = false) {
		global $gblEFormsNA;
		global $gblEFormsNAKey;
		
		if ($this->CreationAdminVersion < esveFormsv6 || $this->SectionType == sectFormatted) {
			return;
		}
		
		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
		$this->setTableAndFieldsProperties();
		//@JAPR
		
		//@AAL Primero se Define el tipo de columna (QuestionDesc) correspondiente a la descripción de la pregunta.
		$blnValid = true;
		$blnNumeric = false;
		$dataType = "VARCHAR(".DEFAULT_ALPHA_LENGTH.")";
		//@JAPR 2016-06-14: Modificado el valor default de otros tipos de preguntas (#Q0GXKE)
		$defaultDataType = "VARCHAR(255)";
		//$defaultDataType = "VARCHAR(100)";	//255
		//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
		$strFieldError = '';
		$arrTableErrors = array("ErrorDesc" => '');
		$arrQuestionTables = array();
		
		switch ($this->QTypeID) {
			case qtpOpenNumeric:
			case qtpCalc:
				//Estas preguntas sólo pueden guardar un valor numérico, sin embargo a manera de dimensión guardan un segundo campo con un numérico real, este es para agrupar
				//como si se tratara de una dimensión donde un valor no capturado se graba como NA
				$blnNumeric = true;
				//@JAPR 2016-06-14: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
				$dataType = "VARCHAR(".DEFAULT_GENERICQ_LENGTH.")"; //$defaultDataType;
				break;
			case qtpSingle:
			case qtpMulti:
			case qtpAction:
				//@JAPR 2016-06-14: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
				//Todos estos tipos de preguntas generan un campo con el tamaño default
				$dataType = "VARCHAR(".DEFAULT_SCHMCH_LENGTH.")";	//$defaultDataType;
				break;
			case qtpOpenDate:
			case qtpOpenTime:
				//@JAPR 2016-06-14: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
				$dataType = "VARCHAR(".DEFAULT_DATETIME_LENGTH.")";
				break;
			case qtpCallList:
			case qtpGPS:
			//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
			case qtpMyLocation:
				//@JAPR 2015-11-26: Modificado para incrementar el tamaño del campo, ya que a un cliente le sucedió que los 3 valores (Lat, Long y Acc) los devolvió como un Float con
				//11+ decimales (incluso el Accuracy), así que sobrepasó el valor de 50 caracteres original
				//@JAPR 2016-06-14: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
				$dataType = "VARCHAR(".DEFAULT_GENERICQ_LENGTH.")";
				break;
			case qtpOpenString: 
			//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			case qtpSketchPlus:
				//Las preguntas tipo Sketch+ grabarán el conjunto de vectores para poder regenerarlas
				//Las preguntas tipo Text no tienen límite de caracteres
				//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
				$dataType = "LONGTEXT";
				break;
			//@JAPR 2015-05-13: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
			//estaban limitadas a 255 caracteres (#BA7866)
			//@JAPR 2016-06-14: Modificado el comportamiento del tamaño del texto de preguntas BarCode para que sea como las alfanuméricas (#TEI1OO)
			case qtpBarCode:
				//La pregunta BarCode funcionará como una alfanumérica en cuanto al tamaño variable del texto
				//$dataType = $defaultDataType;
				//break;
			case qtpOpenAlpha:
				//Las preguntas alfanuméricas dependen de la longitud configurada, pueden crecer pero no reducir su tamaño
				$intLength = $this->QLength;
				if (is_null($intLength) || !is_numeric($intLength) || (int) $intLength <= 0 || (int) $intLength > MAX_ALPHA_LENGTH) {
					if ((int) $intLength > MAX_ALPHA_LENGTH) {
						$intLength = MAX_ALPHA_LENGTH;
					}
					else {
						$intLength = DEFAULT_ALPHA_LENGTH;
					}
				}
				
				if ((int) $intLength <= (int) $this->OldQLength) {
					$intLength = $this->OldQLength;
				}
				//@JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
				//Al modificar le QLength, MaxQLength sólo debe ser actualizado si el nuevo QLength > MaxQLength, ya que el máximo histórico nunca puede disminuir
				if ((int) $intLength <= (int) $this->MaxQLength) {
					$intLength = $this->MaxQLength;
				}
				//@JAPR
				
				$dataType = "VARCHAR({$intLength})";
				break;
				
			case qtpShowValue:
			case qtpPhoto:
			case qtpSignature:
			case qtpMessage:
			case qtpSkipSection:
			case qtpDocument:
			case qtpSync:
			case qtpPassword:
			case qtpMapped:
			case qtpAudio:
			case qtpVideo:
			case qtpSketch:
			case qtpSection:
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			case qtpExit:
			//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
			case qtpUpdateDest:
			default:
				//Estas preguntas no deben generar un campo para su respuesta
				$blnValid = false;
				break;
		}
		
		if ($blnValid) {
			//@JAPR 2015-07-10: Dada la limitante de tablas en un join en MySQL, sólo las preguntas Simple y Multiple Choice normalizarán sus respuestas, así que sólo en ellas es necesario 
			//crear la tabla con su catálogo de respuestas capturadas
			//Crear la tabla SVeForms_####_Question_XXXX correspondiente a las repuestas de las preguntas
			switch ($this->QTypeID) {
				case qtpSingle:
				case qtpMulti:
					$strTableName = $this->QuestionDetTable;
					//@JAPR 2015-08-12: Agregado el campo BITAMSurvey::$KeyField para grabar el Key de la captura en la tabla de las preguntas
					//@JAPR 2015-08-12: Convertidas a variables estáticas para no depender de una instancia
					$sql = "CREATE TABLE {$strTableName} (
						SectionKey INTEGER NOT NULL,
						".BITAMSurvey::$KeyField." INTEGER NOT NULL, 
						".BITAMQuestion::$KeyField." INTEGER NOT NULL AUTO_INCREMENT, 
						PRIMARY KEY (`".BITAMQuestion::$KeyField."`)
					) AUTO_INCREMENT = 1";
					$this->Repository->DataADOConnection->Execute($sql);
					
					//El campo con la descripción de cada opción de respuesta
					$sql = "ALTER TABLE {$strTableName} ADD {$this->DescFieldAttr} {$dataType} NULL";
					$this->Repository->DataADOConnection->Execute($sql);
					
					if ($this->ValuesSourceType == tofCatalog) {
						if ($this->DataSourceID > 0) {
							$objDataSource = BITAMDataSource::NewInstanceWithID($this->Repository, $this->DataSourceID);
							if (!is_null($objDataSource)) {
								$objDataSource->regenerateMemberColumns($strTableName);
							}
						}
					}
					
					//@JAPR 2015-08-12: Agregado el campo BITAMSurvey::$KeyField para grabar el Key de la captura en la tabla de las preguntas
					$sql = "ALTER TABLE {$strTableName} ADD ".BITAMSurvey::$KeyField." INTEGER NULL";
					$this->Repository->DataADOConnection->Execute($sql);
					//@JAPR
					break;
					
				default:
					//El resto de las preguntas con respuesta válida deben crear el campo directamente en la tabla donde estarán sus registros por sección
					$strTableName = $this->SectionTable;
					$sql = "ALTER TABLE {$strTableName} ADD {$this->DescFieldAttr} {$dataType} NULL";
					//@JAPR 2015-12-10: Corregido un bug, si la pregunta es alfanumérica, debe modificar el tamaño del campo durante el grabado, ya que en la creación no se asigna (#8PT8T5)
					$strFieldError = '';
					if (!$this->Repository->DataADOConnection->Execute($sql)) {
						$strFieldError .= (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
						if ($this->QTypeID == qtpOpenAlpha) {
							//Si no se pudo agregar, quiere decir que ya existían, así que hace un ALTER al campo para cambiar el tamaño
							$sql = "ALTER TABLE {$strTableName} MODIFY {$this->DescFieldAttr} {$dataType} NULL";
							if (!$this->Repository->DataADOConnection->Execute($sql)) {
								$strFieldError .= ". ".(string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
							}
						}
					}
					//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
					if (!isset($arrQuestionTables[$strTableName])) {
						$arrQuestionTables[$strTableName] = $arrTableErrors;
					}
					$arrQuestionTables[$strTableName][$this->DescFieldAttr] = $this->DescFieldAttr;
					if ($strFieldError) {
						$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
					}
					//@JAPR
					
					//Si el tipo de pregunta se resuelve a numérico, crea un segundo campo donde el tipo si es DOUBLE y permitirá NULLs
					if ($blnNumeric) {
						$strFieldError = '';
						$sql = "ALTER TABLE {$strTableName} ADD {$this->ValueField} DOUBLE NULL";
						if (!$this->Repository->DataADOConnection->Execute($sql)) {
							$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
							$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
						}
						//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
						$arrQuestionTables[$strTableName][$this->DescFieldAttr] = $this->ValueField;
					}
					break;
			}
		}
		
		//Si la pregunta creada usa catalogo hacemos un alter a la tabla SVeForms_####_Question_XXXX y agregamos la columna Atributte_Member
		//@JAPRWarning: Faltan crear los atributos del catálogo de la sección
		/*
		if ($this->ValuesSourceType == 1) {
			$sql = "ALTER TABLE " . $TableName . " ADD Attribute_" . $this->CatMemberID . " VARCHAR(255)";
			$this->Repository->DataADOConnection->Execute($sql);
		}
		*/
		
		//Crea los campos adicionales de la pregunta según su configuración
		$strTableName = $this->SectionTable;
		//@JAPR 2015-07-11: Dada la limitante de tablas en un join en MySQL, sólo las preguntas Simple y Multiple Choice normalizarán sus respuestas, así que sólo en ellas es necesario 
		/*
		//@AAL Agregar la columna QuestionKey_#### ya sea a la tabla SVeFormsQuestionStd o SVeFormsSection
		$sql = "ALTER TABLE {$strTableName} ADD {$this->DescKeyFieldFK} INTEGER NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		*/
		
		$blnGPSQuestion = false;
		//Esta condición debe actualizar la tabla cuando cambia el tipo de captura o cuando es una pregunta nueva
		//@JAPR 2015-09-23: Validado que al cambiar de sección también se repita este proceso, ya que pudo cambiar entre secciones con tablas diferentes (#UOU7JV)
		//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
		if (($this->QTypeID == qtpGPS || $this->QTypeID == qtpMyLocation) && ($this->OldQTypeID != $this->QTypeID || $bIsNew || $this->SectionID != $this->OldSectionID)) {
			//Si se está definiendo como pregunta tipo GPS, agrega el campo con el Accuracy, el cual es un valor independiente de la posición
			$blnGPSQuestion = true;
			//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
			if (!isset($arrQuestionTables[$strTableName])) {
				$arrQuestionTables[$strTableName] = $arrTableErrors;
			}
			$arrQuestionTables[$strTableName]["QuestionLat_{$this->QuestionID}"] = "QuestionLat_{$this->QuestionID}";
			$arrQuestionTables[$strTableName]["QuestionLong_{$this->QuestionID}"] = "QuestionLong_{$this->QuestionID}";
			$arrQuestionTables[$strTableName]["QuestionAcc_{$this->QuestionID}"] = "QuestionAcc_{$this->QuestionID}";
			//@JAPR
			
			$sql = "ALTER TABLE {$strTableName} ADD QuestionLat_{$this->QuestionID} DOUBLE NULL";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
				$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
			}
			
			$sql = "ALTER TABLE {$strTableName} ADD QuestionLong_{$this->QuestionID} DOUBLE NULL";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
				$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
			}
			
			$sql = "ALTER TABLE {$strTableName} ADD QuestionAcc_{$this->QuestionID} DOUBLE NULL";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
				$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
			}
		}
		
		//Crear la tabla SVeFormsQuestionImg para almacenar las imagenes de las preguntas que lo requieran
		//Esta condición debe actualizar la tabla cuando cambia el tipo de captura o cuando es una pregunta nueva
		//@JAPR 2015-09-23: Validado que al cambiar de sección también se repita este proceso, ya que pudo cambiar entre secciones con tablas diferentes (#UOU7JV)
		if ($this->HasReqPhoto > 0 && ($this->HasReqPhotoOld <= 0 || $bIsNew || $this->SectionID != $this->OldSectionID)) {
			//@JAPR 2015-07-11: Dada la limitante de tablas en un join en MySQL, sólo las preguntas Simple y Multiple Choice normalizarán sus respuestas, así que sólo en ellas es necesario 
			/*
			$strTableName = $this->QuestionImgTable;
			$sql = "CREATE TABLE {$strTableName} (
				{$this->KeyFieldImg} INTEGER NOT NULL AUTO_INCREMENT, 
				{$this->DescFieldImg} VARCHAR(255), 
				PRIMARY KEY (`{$this->KeyFieldImg}`)
			) AUTO_INCREMENT = 2";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$sql = "INSERT INTO {$strTableName} ({$this->KeyFieldImg}, {$this->DescFieldImg}) VALUES ({$gblEFormsNAKey}, ".$this->Repository->DataADOConnection->Quote($gblEFormsNA).")";
			$this->Repository->DataADOConnection->Execute($sql);
			
			//@AAL Agregar ya sea a la tabla SVeFormsSectionStd o SVeFormsSection la columna de QuestionPhotoKey_####
			$strTableName = $this->SectionTable;
			$sql = "ALTER TABLE {$strTableName} ADD {$this->KeyFieldImgFK} INTEGER NULL";
			$this->Repository->DataADOConnection->Execute($sql);
			*/
			//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
			if (!isset($arrQuestionTables[$strTableName])) {
				$arrQuestionTables[$strTableName] = $arrTableErrors;
			}
			$arrQuestionTables[$strTableName]["{$this->DescFieldImg}_{$this->QuestionID}"] = "{$this->DescFieldImg}_{$this->QuestionID}";
			//@JAPR 2016-06-15: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
			$sql = "ALTER TABLE {$strTableName} ADD {$this->DescFieldImg}_{$this->QuestionID} VARCHAR(".DEFAULT_IMAGE_LENGTH.") NULL";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
				$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
			}
			
			//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			//En el caso de las preguntas Sketch+ se necesita un campo adicional para almacenar la imagen original sin los trazos
			if ( $this->QTypeID == qtpSketchPlus ) {
				$arrQuestionTables[$strTableName]["{$this->DescFieldImg}Src_{$this->QuestionID}"] = "{$this->DescFieldImg}Src_{$this->QuestionID}";
				//@JAPR 2016-06-15: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
				$sql = "ALTER TABLE {$strTableName} ADD {$this->DescFieldImg}Src_{$this->QuestionID} VARCHAR(".DEFAULT_IMAGE_LENGTH.") NULL";
				if (!$this->Repository->DataADOConnection->Execute($sql)) {
					$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
					$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
				}
			}
			//@JAPR
		}
	
		//Crear la tabla SVeFormsQuestioComm para almacenar los comentarios de las preguntas que lo requieran
		//Esta condición debe actualizar la tabla cuando cambia el tipo de captura o cuando es una pregunta nueva
		//@JAPR 2015-09-23: Validado que al cambiar de sección también se repita este proceso, ya que pudo cambiar entre secciones con tablas diferentes (#UOU7JV)
		if ($this->HasReqComment > 0 && ($this->HasReqCommentOld <= 0 || $bIsNew || $this->SectionID != $this->OldSectionID)) {
			//@JAPR 2015-07-11: Dada la limitante de tablas en un join en MySQL, sólo las preguntas Simple y Multiple Choice normalizarán sus respuestas, así que sólo en ellas es necesario 
			/*
			$strTableName = $this->QuestionCommTable;
			$sql = "CREATE TABLE {$strTableName} (
				{$this->KeyFieldComm} INTEGER NOT NULL AUTO_INCREMENT, 
				{$this->DescFieldComm} VARCHAR(255), 
				PRIMARY KEY (`{$this->KeyFieldComm}`)
			) AUTO_INCREMENT = 2";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$sql = "INSERT INTO {$strTableName} ({$this->KeyFieldComm}, {$this->DescFieldComm}) VALUES ({$gblEFormsNAKey}, ".$this->Repository->DataADOConnection->Quote($gblEFormsNA).")";
			$this->Repository->DataADOConnection->Execute($sql);
			
			//@AAL Agregar ya sea a la tabla SVeFormsSectionStd o SVeFormsSection la columna de QuestionCommKey_####
			$strTableName = $this->SectionTable;
			$sql = "ALTER TABLE {$strTableName} ADD {$this->KeyFieldCommFK} INTEGER NULL";
			$this->Repository->DataADOConnection->Execute($sql);
			*/
			//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
			if (!isset($arrQuestionTables[$strTableName])) {
				$arrQuestionTables[$strTableName] = $arrTableErrors;
			}
			$arrQuestionTables[$strTableName]["{$this->DescFieldComm}_{$this->QuestionID}"] = "{$this->DescFieldComm}_{$this->QuestionID}";
			//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
			$sql = "ALTER TABLE {$strTableName} ADD {$this->DescFieldComm}_{$this->QuestionID} LONGTEXT NULL";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
				$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
			}
		}
	
		//Crear la tabla SVeFormsQuestioDoc para almacenar los documentos de las preguntas que lo requieran
		//Esta condición debe actualizar la tabla cuando cambia el tipo de captura o cuando es una pregunta nueva
		//@JAPR 2015-09-23: Validado que al cambiar de sección también se repita este proceso, ya que pudo cambiar entre secciones con tablas diferentes (#UOU7JV)
		if ($this->HasReqDocument > 0 && ($this->HasReqDocumentOld <= 0 || $bIsNew || $this->SectionID != $this->OldSectionID)) {
			//@JAPR 2015-07-11: Dada la limitante de tablas en un join en MySQL, sólo las preguntas Simple y Multiple Choice normalizarán sus respuestas, así que sólo en ellas es necesario 
			/*
			$strTableName = $this->QuestionDocTable;
			$sql = "CREATE TABLE {$strTableName} (
				{$this->KeyFieldDoc} INTEGER NOT NULL AUTO_INCREMENT, 
				{$this->DescFieldDoc} VARCHAR(255), 
				PRIMARY KEY (`{$this->KeyFieldDoc}`)
			) AUTO_INCREMENT = 2";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$sql = "INSERT INTO {$strTableName} ({$this->KeyFieldDoc}, {$this->DescFieldDoc}) VALUES ({$gblEFormsNAKey}, ".$this->Repository->DataADOConnection->Quote($gblEFormsNA).")";
			$this->Repository->DataADOConnection->Execute($sql);
			
			//@AAL Agregar ya sea a la tabla SVeFormsSectionStd o SVeFormsSection la columna de QuestionDocKey_####
			$strTableName = $this->SectionTable;
			$sql = "ALTER TABLE {$strTableName} ADD {$this->KeyFieldDocFK} INTEGER NULL";
			$this->Repository->DataADOConnection->Execute($sql);
			*/
			//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
			if (!isset($arrQuestionTables[$strTableName])) {
				$arrQuestionTables[$strTableName] = $arrTableErrors;
			}
			$arrQuestionTables[$strTableName]["{$this->DescFieldDoc}_{$this->QuestionID}"] = "{$this->DescFieldDoc}_{$this->QuestionID}";
			//@JAPR 2016-06-15: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
			//Modificado el tipo de dato de los campos de documentos a TEXT para permitir cualquier nombre por largo que sea
			//$sql = "ALTER TABLE {$strTableName} ADD {$this->DescFieldDoc}_{$this->QuestionID} VARCHAR(255) NULL";
			//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
			$sql = "ALTER TABLE {$strTableName} ADD {$this->DescFieldDoc}_{$this->QuestionID} LONGTEXT NULL";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
				$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
			}
		}
	
		//Crear tabla SVeFormsQuestionPts para las preguntas que utilizen Score
		//Esta condición debe actualizar la tabla cuando cambia el tipo de captura o cuando es una pregunta nueva
		//@JAPR 2015-09-23: Validado que al cambiar de sección también se repita este proceso, ya que pudo cambiar entre secciones con tablas diferentes (#UOU7JV)
		if ($this->HasScores && (!$this->HasScoresOld || $bIsNew || $this->SectionID != $this->OldSectionID)) {
			//@JAPR 2015-07-11: Dada la limitante de tablas en un join en MySQL, sólo las preguntas Simple y Multiple Choice normalizarán sus respuestas, así que sólo en ellas es necesario 
			/*
			$strTableName = $this->QuestionPtsTable;
			$sql = "CREATE TABLE {$strTableName} (
				{$this->KeyFieldPts} INTEGER NOT NULL AUTO_INCREMENT, 
				{$this->DescFieldPts} VARCHAR(255), 
				PRIMARY KEY (`{$this->KeyFieldPts}`)
			) AUTO_INCREMENT = 2";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$sql = "INSERT INTO {$strTableName} ({$this->KeyFieldPts}, {$this->DescFieldPts}) VALUES ({$gblEFormsNAKey}, ".$this->Repository->DataADOConnection->Quote($gblEFormsNA).")";
			$this->Repository->DataADOConnection->Execute($sql);
			
			//@AAL Agregar ya sea a la tabla SVeFormsSectionStd o SVeFormsSection la columna de QuestionScoreKey_####
			$strTableName = $this->SectionTable;
			$sql = "ALTER TABLE {$strTableName} ADD {$this->KeyFieldPtsFK} INTEGER NULL";
			$this->Repository->DataADOConnection->Execute($sql);
			*/
			//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
			if (!isset($arrQuestionTables[$strTableName])) {
				$arrQuestionTables[$strTableName] = $arrTableErrors;
			}
			$arrQuestionTables[$strTableName]["{$this->DescFieldPts}_{$this->QuestionID}"] = "{$this->DescFieldPts}_{$this->QuestionID}";
			$sql = "ALTER TABLE {$strTableName} ADD {$this->DescFieldPts}_{$this->QuestionID} DOUBLE NULL";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
				$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
			}
		}
		
		//Crear tabla SVeFormsQuestionDet si se trata de una pregunta Multiple Choice
		if ($this->QTypeID == qtpSingle || $this->QTypeID == qtpMulti) {
			//@JAPR 2015-07-11: Dada la limitante de tablas en un join en MySQL, sólo las preguntas Simple y Multiple Choice normalizarán sus respuestas, así que sólo en ellas es necesario 
			//@AAL Agregar la columna QuestionKey_#### ya sea a la tabla SVeFormsQuestionStd o SVeFormsSection
			/*
			$sql = "ALTER TABLE {$strTableName} ADD {$this->DescKeyFieldFK} INTEGER NULL";
			$this->Repository->DataADOConnection->Execute($sql);
			*/
			
			//El resto de los campos se grabarán en la tabla normalizada de las respuestas de la pregunta
			$strTableName = $this->QuestionDetTable;
			
			//El campo del Score de cada opción de respuesta
			//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
			if (!isset($arrQuestionTables[$strTableName])) {
				$arrQuestionTables[$strTableName] = $arrTableErrors;
			}
			$arrQuestionTables[$strTableName]["{$this->DescFieldPts}_{$this->QuestionID}"] = "{$this->DescFieldPts}_{$this->QuestionID}";
			$sql = "ALTER TABLE {$strTableName} ADD {$this->DescFieldPts}_{$this->QuestionID} DOUBLE NULL";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
				$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
			}
			
			if ($this->QTypeID == qtpMulti) {
				//Esta condición debe actualizar la tabla cuando cambia el tipo de captura o cuando es una pregunta nueva
				if ($this->MCInputType != $this->MCInputTypeOld || $bIsNew) {
					switch ($this->MCInputType) {
						case mpcCheckBox:
							$dataType = "INTEGER";
							break;
						case mpcNumeric:
							$dataType = "DOUBLE";
							break;
						case mpcText:
						default:
							//@JAPR 2016-06-15: Modificado el tamaño default de las columnas VarChar de diferentes tipos de preguntas (#TEI1OO)
							$dataType = "VARCHAR(".DEFAULT_SCHMCH_LENGTH.")";
					}
					
					//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
					$strFieldError = '';
					if (!isset($arrQuestionTables[$strTableName])) {
						$arrQuestionTables[$strTableName] = $arrTableErrors;
					}
					$arrQuestionTables[$strTableName]["{$this->ValueField}"] = "{$this->ValueField}";
					//Si es múltiple choice, requiere un campo adicional con la captura extra de cada opción de respuesta según el tipo de catptura configurado
					$sql = "ALTER TABLE {$strTableName} ADD {$this->ValueField} {$dataType} NULL";
					if (!$this->Repository->DataADOConnection->Execute($sql)) {
						$strFieldError .= (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
						//Si no se pudo agregar, quiere decir que ya existían, así que hace un ALTER al campo
						$sql = "ALTER TABLE {$strTableName} MODIFY {$this->ValueField} {$dataType} NULL";
						if (!$this->Repository->DataADOConnection->Execute($sql)) {
							$strFieldError .= ". ".(string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
							//Si no pudo hacer la conversión, entonces tendrá que eliminar el campo y agregarlo con el nuevo tipo, pero el histórico se va a perder
							$sql = "ALTER TABLE {$strTableName} DROP COLUMN {$this->ValueField}";
							$this->Repository->DataADOConnection->Execute($sql);
							$sql = "ALTER TABLE {$strTableName} ADD {$this->ValueField} {$dataType} NULL";
							if (!$this->Repository->DataADOConnection->Execute($sql)) {
								$strFieldError .= ". ".(string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
							}
						}
					}
					if ($strFieldError) {
						$strFieldError = (string) @$this->Repository->DataADOConnection->ErrorMsg()." - ".$sql;
						$arrQuestionTables[$strTableName]["ErrorDesc"] .= "\r\n".$strFieldError;
					}
				}
			}
			
			//@JAPR 2015-07-11: Dada la limitante de tablas en un join en MySQL, sólo las preguntas Simple y Multiple Choice normalizarán sus respuestas, así que sólo en ellas es necesario 
			/*
			$strTableName = $this->QuestionValTable;
			$sql = "CREATE TABLE {$strTableName} (
				{$this->KeyFieldVal} INTEGER, 
				{$this->DescFieldVal} VARCHAR(255),
				PRIMARY KEY (`{$this->KeyFieldVal}`)
			)";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$sql = "INSERT INTO {$strTableName} ({$this->KeyFieldVal}, {$this->DescFieldVal}) VALUES ({$gblEFormsNAKey}, ".$this->Repository->DataADOConnection->Quote($gblEFormsNA).")";
			$this->Repository->DataADOConnection->Execute($sql);
			*/
		}
		
		//@JAPR 2016-06-14: Agregada la validación para verificar que se lograran crear todas las columnas necesarias en las tablas de esta pregunta (#Q0GXKE)
		//Verifica si ocurrió algún error durante la creación de las columnas de alguna de las tablas
		foreach ($arrQuestionTables as $strTableName => $arrTableFields) {
			if ($strTableName && is_array($arrTableFields)) {
				$strTableError = (string) @$arrTableFields["ErrorDesc"];
				unset($arrTableFields["ErrorDesc"]);
				if (count($arrTableFields) > 0) {
					$sql = "SELECT ".implode(', ', $arrTableFields)." 
						FROM {$strTableName} 
						WHERE 1 = 2";
					$aRS = $this->Repository->DataADOConnection->Execute($sql);
					$blnError = false;
					if (!$aRS) {
						$this->TableErrors = $strTableError;
						$blnError = true;
					}
					break;
				}
			}
		}
		//@JAPR
	}
	
	//@JAPR 2015-01-13: Corregido un bug, las preguntas tipo sección no se deben poder mover dentro de otra sección múltiple (#XWKDHA)
	/* Verifica si de acuerdo a la definición de esta instancia asumiendo que la propiedad SectionID ya contiene la sección a la cual se
	desea hacer el cambio, si el cambio especificado es válido o no. Originalmente usado sólo para validar que las preguntas tipo sección no
	se pudieran mover a secciones multi registro
	El parámetro $aNewSectionID permite validar si el cambio es o no factible sin tener que haber modificado la instancia
	*/
	function isValidSectionChange($aNewSectionID = -1) {
		$blnValid = true;
		
		$intNewSectionID = $aNewSectionID;
		if ($intNewSectionID < 0) {
			$intNewSectionID = $this->SectionID;
		}
		
		if ($this->OldSectionID != $intNewSectionID && $intNewSectionID > 0) {
			$objSection = BITAMSection::NewInstanceWithID($this->Repository, $intNewSectionID);
			if (!is_null($objSection)) {
				switch ($this->QTypeID) {
					case qtpSection:
						//Las preguntas tipo Sección-Inline no son válidas para ninguna sección multi registro
						if ($objSection->SectionType != sectNormal) {
							$blnValid = false;
						}
						break;
				}
			}
		}
		
		return $blnValid;
	}
	//@JAPR
	
	//@JAPR 2014-07-31: Agregado el Responsive Design
	/* Graba las propiedades que corresponden al Responsive Design de forma especial, ya que se graban en la misma tabla pero variando el ID
	en base al tipo de dispositivo del que se trate
	//@JAPR 2018-07-09: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//@JAPRDescontinuada en V6
	*/
	function saveResponsiveDesignProps() {
		return true;
		//Se debe verificar si este objeto ya tenía grabada su propiedad de Responsive Design para cierto tipo de dispositivo, de tal forma
		//que se pueda armar un INSERT o un UPDATE según el caso (No es necesario grabar la propiedad Default pues esa está directamente
		//en la tabla del objeto)
		//Conchita 11-10-2014 agregado el SketchImage

		for ($intDeviceID = dvciPod; $intDeviceID <= dvcTablet; $intDeviceID++) {
			$strQuestionMessage = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("QuestionMessage", $intDeviceID));
			$strQuestionImageText = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("QuestionImageText", $intDeviceID));
			if (getMDVersion() >= esvSketchImage) {
					$strSketchImage = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("QuestionImageSketch", $intDeviceID));
			}
			$strEmpty = $this->Repository->DataADOConnection->Quote("");
			
			$strUpdate = "";
			$strInsertFields = "";
			$strInsertValues = "";
			if (getMDVersion() >= esvEditorHTML)
			{
				$strQuestionMessageDes = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("QuestionMessage", $intDeviceID, false, 1));
				$strUpdate = ", QuestionMessageDes = ". $strQuestionMessageDes;
				$strInsertFields = ", QuestionMessageDes";
				$strInsertValues = " ,".$strQuestionMessageDes;
			}
						
			if (isset($this->ResponsiveDesignProps[$intDeviceID])) {
					$strAdditionalFields = '';
				if (getMDVersion() >= esvSketchImage) {
					$strAdditionalFields .= ", SketchImage = {$strSketchImage} ";
				}
				$sql = "UPDATE SI_SV_SurveyHTML SET 
						FormattedText = {$strQuestionMessage} 
						, HTMLHeader = {$strEmpty} 
						, HTMLFooter = {$strEmpty} 
						, ImageText = {$strQuestionImageText} 
						{$strUpdate}
					{$strAdditionalFields} WHERE ObjectType = ".otyQuestion." AND DeviceID = {$intDeviceID} AND ObjectID = {$this->QuestionID}";

			}
			else {
				$strAdditionalFields = '';
				$strAdditionalValues = '';
				
				if (getMDVersion() >= esvSketchImage) {
					//@JAPR 2016-12-20: Corregido un bug, estaba reemplazando en lugar de concatenar los campos adicionales (#DBNNGH)
					$strAdditionalFields .= ", SketchImage ";
					$strAdditionalValues .= ",{$strSketchImage} ";
					
				}
				$sql = "INSERT INTO SI_SV_SurveyHTML (SurveyID, ObjectType, ObjectID, DeviceID, FormattedText, HTMLHeader, HTMLFooter, ImageText {$strAdditionalFields} {$strInsertFields}) 
					VALUES ({$this->SurveyID}, ".otyQuestion.", {$this->QuestionID}, {$intDeviceID}, 
						{$strQuestionMessage}, {$strEmpty}, {$strEmpty}, {$strQuestionImageText} $strAdditionalValues {$strInsertValues})";
			}
			@$this->Repository->DataADOConnection->Execute($sql);
		}
	}
	
	//@JAPR 2013-12-05: Agregada la opción para compartir las opciones de respuesta con otras preguntas
	/* Verifica dependiendo de si la pregunta se trata de una pregunta origen o una compartida, si todas las preguntas compartidas correspondientes
	contienen la misma cantidad de opciones de respuesta, o si esta pregunta compartida tiene la misma cantidad de opciones de respuesta de la 
	pregunta origen respectivamente, creando las opciones de respuesta faltantes, eliminando las excedentes y definiendo el mapeo entre las mismas
	a partir del texto de la opción de respuesta
	*/
	function checkSharedQuestionOptionMap() {
		//Primero se verificará si existe el mapeo entre las opciones, ya que de no existirlo, eso es lo primero que se tiene que agregar para
		//determinar que opciones agregar o remover
		if ($this->SharedQuestionID > 0) {
			$sql = "DELETE FROM SI_SV_QAnswersMatch WHERE SharedQuestionID = ".$this->QuestionID;
			$this->Repository->DataADOConnection->Execute($sql);
			
			//Es una pregunta compartida
			//Verifica que opciones si se encuentran compartidas para agregar el Match de sus IDs
			$sql = "SELECT A.DisplayText AS 'SourceDisplayText', A.ConsecutiveID AS 'SourceConsecutiveID', B.DisplayText AS 'SharedDisplayText', B.ConsecutiveID AS 'SharedConsecutiveID' 
				FROM SI_SV_QAnswers A 
						LEFT OUTER JOIN SI_SV_QAnswers B ON A.DisplayText = B.DisplayText AND B.QuestionID = {$this->QuestionID} 
				WHERE A.QuestionID = {$this->SharedQuestionID}";
			//@JAPR 2015-08-08: Corregido un bug, a partir de V6 al grabar una pregunta no se eliminan automáticamente sus opciones de respuesta, así que hay que removerlas en este
			//punto pues de lo contrario se duplicarían o agregarían a las previamente registradas (por alguna razón el RIGHT JOIN no funcionó en MySQL, y dada la falta de FULL JOINs,
			//la opción que queda es invertir por completo el query con un LEFT join)
			if ($this->CreationAdminVersion >= esveFormsv6) {
				$sql .= " UNION 
					SELECT B.DisplayText AS 'SourceDisplayText', B.ConsecutiveID AS 'SourceConsecutiveID', A.DisplayText AS 'SharedDisplayText', A.ConsecutiveID AS 'SharedConsecutiveID' 
					FROM SI_SV_QAnswers A 
							LEFT OUTER JOIN SI_SV_QAnswers B ON A.DisplayText = B.DisplayText AND B.QuestionID = {$this->SharedQuestionID} 
					WHERE A.QuestionID = {$this->QuestionID}";
			}
			//@JAPR
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF) {
				//@JAPR 2014-12-08: Corregido un bug, tenía un Continue en lugar de return
				return;
			}
			
			//Agrega el mapeo de opciones en las preguntas compartidas entre ambos que si existan, agrega a la pregunta compartida las que le
			//falten y elimina las que le sobren
			while (!$aRS->EOF) {
				$intSourceConsecutiveID = (int) @$aRS->fields["sourceconsecutiveid"];
				$intSharedConsecutiveID = (int) @$aRS->fields["sharedconsecutiveid"];
				if ($intSourceConsecutiveID > 0 && $intSharedConsecutiveID > 0) {
					//Si ambos IDs existen, se agrega el Mapeo (tiene primary key así que no lo podría duplicar)
					$sql = "INSERT INTO SI_SV_QAnswersMatch (SourceQuestionID, SourceConsecutiveID, SharedQuestionID, SharedConsecutiveID) 
						VALUES ({$this->SharedQuestionID}, {$intSourceConsecutiveID}, {$this->QuestionID}, {$intSharedConsecutiveID})";
					$this->Repository->DataADOConnection->Execute($sql);
				}
				elseif ($intSourceConsecutiveID > 0) {
					//Si sólo existe el ID en la pregunta origen, entonces se trata de una opción que se debe agregar en la pregunta compartida
					//y por tanto se crea en este momento
					$objQuestionOption = @BITAMQuestionOption::NewInstanceWithID($this->Repository, $intSourceConsecutiveID);
					if (!is_null($objQuestionOption)) {
						$aQuestionOption = clone($objQuestionOption);
						//Modifica la instancia clonada para que pertenezca a la pregunta compartida
						$aQuestionOption->QuestionOptionID = -1;
						$aQuestionOption->QuestionID = $this->QuestionID;
						$aQuestionOption->QTypeID = $this->QTypeID;
						
						//Limpia las configuraciones que no se desean heredar en las preguntas compartidas, ya que ahí pudiera tener diferente
						//características la pregunta y no necesariamente aplicar, creando basura innecesaria, especialmente dimensiones o indicadores
						$aQuestionOption->Score = "";
						$aQuestionOption->ActionText = '';
						$aQuestionOption->ActionTitle = '';
						$aQuestionOption->eBavelActionFormID = 0;
						$aQuestionOption->Visible = 1;
						$aQuestionOption->CancelAgenda = 0;
						$aQuestionOption->ResponsibleID = -1;
						$aQuestionOption->CategoryID = 0;
						$aQuestionOption->DaysDueDate = 0;
						$aQuestionOption->InsertWhen = ioaChecked;
						
						//Invoca al grabado ya como una opción de respuesta de otra pregunta
						$aQuestionOption->save();
						
						//Inserta el mapeo entre la nueva opción y la original
						$sql = "INSERT INTO SI_SV_QAnswersMatch (SourceQuestionID, SourceConsecutiveID, SharedQuestionID, SharedConsecutiveID) 
							VALUES ({$this->SharedQuestionID}, {$intSourceConsecutiveID}, {$this->QuestionID}, {$aQuestionOption->QuestionOptionID})";
						$this->Repository->DataADOConnection->Execute($sql);
					}
				}
				else {
					//Si sólo existe el ID en la pregunta compartida, entonces se trata de una opción que debe ser removida de esa pregunta,
					//y por tanto la elimina en este momento
					//@JAPR 2015-08-08: Corregido un bug, estaba usando la variable equivocada
					$aQuestionOption = @BITAMQuestionOption::NewInstanceWithID($this->Repository, $intSharedConsecutiveID);
					if (!is_null($aQuestionOption)) {
						//Invoca al borrado de respuesta de la pregunta compartida
						@$aQuestionOption->remove();
					}
				}
				
				$aRS->MoveNext();
			}
		}
		else {
			//Es la pregunta origen, lo que haga se tiene que replicar entre todas las preguntas compartidas así que se hace dentro de un ciclo
			//Lo primero es determinar a que otras pregunta se comparten las opciones de respuesta
			//@JAPR 2014-05-20: Corregido un bug, estaba usando $mainQuestion en lugar de $this
			$sql = "SELECT QuestionID 
				FROM SI_SV_Question 
				WHERE SurveyID = {$this->SurveyID} AND SharedQuestionID = {$this->QuestionID}";
			$aRSSharedQuestions = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRSSharedQuestions || $aRSSharedQuestions->EOF) {
				return ;
			}
			
			while (!$aRSSharedQuestions->EOF) {
				$intSharedQuestionID = (int) @$aRSSharedQuestions->fields["questionid"];
				$objSharedQuestion = @BITAMQuestion::NewInstanceWithID($this->Repository, $intSharedQuestionID);
				if (is_null($objSharedQuestion)) {
					continue;
				}
				
				$sql = "DELETE FROM SI_SV_QAnswersMatch WHERE SharedQuestionID = ".$intSharedQuestionID;
				$this->Repository->DataADOConnection->Execute($sql);
				
				//Verifica que opciones si se encuentran compartidas para agregar el Match de sus IDs
				$sql = "SELECT A.DisplayText AS 'SourceDisplayText', A.ConsecutiveID AS 'SourceConsecutiveID', B.DisplayText AS 'SharedDisplayText', B.ConsecutiveID AS 'SharedConsecutiveID' 
					FROM SI_SV_QAnswers A 
  						LEFT OUTER JOIN SI_SV_QAnswers B ON A.DisplayText = B.DisplayText AND B.QuestionID = {$intSharedQuestionID} 
					WHERE A.QuestionID = {$this->QuestionID}";
				//@JAPR 2015-08-08: Corregido un bug, a partir de V6 al grabar una pregunta no se eliminan automáticamente sus opciones de respuesta, así que hay que removerlas en este
				//punto pues de lo contrario se duplicarían o agregarían a las previamente registradas (por alguna razón el RIGHT JOIN no funcionó en MySQL, y dada la falta de FULL JOINs,
				//la opción que queda es invertir por completo el query con un LEFT join)
				if ($this->CreationAdminVersion >= esveFormsv6) {
					$sql .= " UNION 
						SELECT B.DisplayText AS 'SourceDisplayText', B.ConsecutiveID AS 'SourceConsecutiveID', A.DisplayText AS 'SharedDisplayText', A.ConsecutiveID AS 'SharedConsecutiveID' 
						FROM SI_SV_QAnswers A 
								LEFT OUTER JOIN SI_SV_QAnswers B ON A.DisplayText = B.DisplayText AND B.QuestionID = {$this->QuestionID} 
						WHERE A.QuestionID = {$intSharedQuestionID}";
				}
				//@JAPR
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF) {
					continue;
				}
				
				//Agrega el mapeo de opciones en las preguntas compartidas entre ambos que si existan, agrega a la pregunta compartida las que le
				//falten y elimina las que le sobren
				while (!$aRS->EOF) {
					$intSourceConsecutiveID = (int) @$aRS->fields["sourceconsecutiveid"];
					$intSharedConsecutiveID = (int) @$aRS->fields["sharedconsecutiveid"];
					
					if ($intSourceConsecutiveID > 0 && $intSharedConsecutiveID > 0) {
						//Si ambos IDs existen, se agrega el Mapeo (tiene primary key así que no lo podría duplicar)
						$sql = "INSERT INTO SI_SV_QAnswersMatch (SourceQuestionID, SourceConsecutiveID, SharedQuestionID, SharedConsecutiveID) 
							VALUES ({$this->QuestionID}, {$intSourceConsecutiveID}, {$intSharedQuestionID}, {$intSharedConsecutiveID})";
						$this->Repository->DataADOConnection->Execute($sql);
					}
					elseif ($intSourceConsecutiveID > 0) {
						//Si sólo existe el ID en la pregunta origen, entonces se trata de una opción que se debe agregar en la pregunta compartida
						//y por tanto se crea en este momento
						$objQuestionOption = @BITAMQuestionOption::NewInstanceWithID($this->Repository, $intSourceConsecutiveID);
						if (!is_null($objQuestionOption)) {
							$aQuestionOption = clone($objQuestionOption);
							//Modifica la instancia clonada para que pertenezca a la pregunta compartida
							$aQuestionOption->QuestionOptionID = -1;
							$aQuestionOption->QuestionID = $intSharedQuestionID;
							$aQuestionOption->QTypeID = $objSharedQuestion->QTypeID;
							
							//Limpia las configuraciones que no se desean heredar en las preguntas compartidas, ya que ahí pudiera tener diferente
							//características la pregunta y no necesariamente aplicar, creando basura innecesaria, especialmente dimensiones o indicadores
							$aQuestionOption->Score = "";
							$aQuestionOption->ActionText = '';
							$aQuestionOption->ActionTitle = '';
							$aQuestionOption->eBavelActionFormID = 0;
							$aQuestionOption->Visible = 1;
							$aQuestionOption->CancelAgenda = 0;
							$aQuestionOption->ResponsibleID = -1;
							$aQuestionOption->CategoryID = 0;
							$aQuestionOption->DaysDueDate = 0;
							$aQuestionOption->InsertWhen = ioaChecked;
							
							//Invoca al grabado ya como una opción de respuesta de otra pregunta
							$aQuestionOption->save();
							
							//Inserta el mapeo entre la nueva opción y la original
							$sql = "INSERT INTO SI_SV_QAnswersMatch (SourceQuestionID, SourceConsecutiveID, SharedQuestionID, SharedConsecutiveID) 
								VALUES ({$this->QuestionID}, {$intSourceConsecutiveID}, {$intSharedQuestionID}, {$aQuestionOption->QuestionOptionID})";
							$this->Repository->DataADOConnection->Execute($sql);
						}
					}
					else {
						//Si sólo existe el ID en la pregunta compartida, entonces se trata de una opción que debe ser removida de esa pregunta,
						//y por tanto la elimina en este momento
						//@JAPR 2015-08-08: Corregido un bug, estaba usando la variable equivocada
						$aQuestionOption = @BITAMQuestionOption::NewInstanceWithID($this->Repository, $intSharedConsecutiveID);
						if (!is_null($aQuestionOption)) {
							//Invoca al borrado de respuesta de la pregunta compartida
							@$aQuestionOption->remove();
						}
					}
					
					$aRS->MoveNext();
				}
				
				//Continua con la siguiente pregunta
				$aRSSharedQuestions->MoveNext();
			}
		}
	}
	//@JAPR
	
	//@JAPRDescontinuada en v6
	function updateQuestionHierarchy()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Primero checamos si hay mas preguntas con el mismo catalogo si es asi
		//procedemos a actualizar jerarquias, en caso contrario no se realiza nada
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND CatalogID = ".$this->CatalogID." AND UseCatalog = 1 AND QuestionID <> ".$this->QuestionID." AND QTypeID <> ".qtpOpenNumeric;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS) {
			die("(".__METHOD__.") ".translate("Error accessing")." "."SI_SV_Question"." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Si ya existe otras preguntas que utilizan el mismo catalogo entonces procedemos a actualizar los campos
		//FatherQuestionID, ChildQuestionID
		if(!$aRS->EOF) {
			$fatherQuestionID = 0;
			$childQuestionID = 0;
			
			//Obtenemos la ultima pregunta antes de la actual que tenga el mismo catalogo
			$sql = "SELECT A.QuestionID FROM SI_SV_Question A 
				WHERE A.SurveyID = ".$this->SurveyID." AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1 
					AND A.CatalogID = ".$this->CatalogID." AND A.QuestionNumber < ".$this->QuestionNumber." 
				ORDER BY A.QuestionNumber DESC";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if (!$aRS->EOF) {
				//Obtenemos el QuestionID
				$fatherQuestionID = (int)$aRS->fields["questionid"];
				
				//Actualizamos en dicha pregunta el ChildQuestionID
				$sql = "UPDATE SI_SV_Question SET ChildQuestionID = ".$this->QuestionID." WHERE QuestionID = ".$fatherQuestionID." AND SurveyID = ".$this->SurveyID;
				if ($this->Repository->DataADOConnection->Execute($sql) === false) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//Obtenemos la primera pregunta despues de la actual que tenga el mismo catalogo
			$sql = "SELECT A.QuestionID FROM SI_SV_Question A 
				WHERE A.SurveyID = ".$this->SurveyID." AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1 
					AND A.CatalogID = ".$this->CatalogID." AND A.QuestionNumber > ".$this->QuestionNumber." 
				ORDER BY A.QuestionNumber ASC";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if (!$aRS->EOF) {
				//Obtenemos el QuestionID
				$childQuestionID = (int)$aRS->fields["questionid"];
				
				//Actualizamos en dicha pregunta el FatherQuestionID
				$sql = "UPDATE SI_SV_Question SET FatherQuestionID = ".$this->QuestionID." WHERE QuestionID = ".$childQuestionID." AND SurveyID = ".$this->SurveyID;

				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//Por ultimo actualizamos en nuestra pregunta actual tanto el FatherQuestionID como el ChildQuestionID
			$sql = "UPDATE SI_SV_Question SET FatherQuestionID = ".$fatherQuestionID.", ChildQuestionID = ".$childQuestionID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	
	//@JAPRDescontinuada en v6
	function updateQuestionNomLogico() {
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		$strOriginalWD = getcwd();
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");

		//@JAPR 2012-07-11: Corregido un bug, no se estaba validando realmente que existiera una dimensión así que en casos donde no hay una
		//asociada estaba generando un error de PhP pues no se cargó ninguna instancia
		if (!is_null($this->IndDimID) && $this->IndDimID > 0)
		{
			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->IndDimID);
			$dimensionName = $this->AttributeName;
			global $generateXLS;
			$generateXLS = false;
			
			$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
			$anInstanceModelDim->save();
			chdir($strOriginalWD);
		}
		//@JAPR
		
		//Modificamos el nombre logico de la dimension de la imagen si es que hay imagen asociada a dicha dimension
		if(!is_null($this->ImgDimID) && $this->ImgDimID>0)
		{
			$anInstanceModelDimImg = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->ImgDimID);
			$dimensionName = "IMG ".$this->AttributeName;
			global $generateXLS;
			$generateXLS = false;
					
			$anInstanceModelDimImg->Dimension->DimensionName = $dimensionName;
			$anInstanceModelDimImg->save();
			chdir($strOriginalWD);
		}
		
		//Modificamos el nombre logico de la dimension del documento si es que hay imagen asociada a dicha dimension
		if(!is_null($this->DocDimID) && $this->DocDimID>0)
		{
			$anInstanceModelDimImg = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->DocDimID);
			$dimensionName = "DOC ".$this->AttributeName;
			global $generateXLS;
			$generateXLS = false;
					
			$anInstanceModelDimImg->Dimension->DimensionName = $dimensionName;
			$anInstanceModelDimImg->save();
			chdir($strOriginalWD);
		}
	}
	
	//@JAPR 2015-05-19: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
	//estaban limitadas a 255 caracteres (#BA7866)
	/* Cambia la longitud del campo en la tabla de la forma y de la dimensión correspondiente para la pregunta tipo Alfanumérica sobre la
	que se invoca el método, pero sólo lo hace si la longitud se incrementó, para evitar perder datos en caso de que el usuario decida
	reducir el tamaño (seguirá variando el tamaño del texto durante la captura de la forma, pero internamente conservará el tamaño anterior)
	//@JAPRDescontinuada en v6, la actualización de la longitud del campo sucede en la función createDataTablesAndFields
	*/
	function updateQuestionLength()
	{
		//Si no es del tipo correcto o la longitud no ha crecido, entonces no hay necesidad de ejecutar el ALTER de los campos
		$intLength = $this->QLength;
		if (is_null($intLength) || !is_numeric($intLength) || (int) $intLength <= 0 || (int) $intLength > MAX_ALPHA_LENGTH) {
			if ((int) $intLength > MAX_ALPHA_LENGTH) {
				$intLength = MAX_ALPHA_LENGTH;
			}
			else {
				$intLength = DEFAULT_ALPHA_LENGTH;
			}
		}
		if ($this->QTypeID != qtpOpenAlpha || (int) $intLength <= (int) $this->OldQLength) {
			return;
		}

		//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, los Indicadores o Dimensiones ya no aplican
		if ($this->CreationAdminVersion < esveFormsv6) {
			$strOriginalWD = getcwd();
			session_register("BITAM_UserID");
			session_register("BITAM_UserName");
			session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/model.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			require_once("../model_manager/modeldimension.inc.php");

			if (!is_null($this->IndDimID) && $this->IndDimID > 0)
			{
				try {
					$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->IndDimID);
					if (!is_null($anInstanceModelDim)) {
						if (method_exists($anInstanceModelDim, "AlterLongDimension")) {
							@$anInstanceModelDim->AlterLongDimension();
						}
					}
				} catch (Exception $e) {
					//No reporta este error, simplemente no realizará el ALTER del campo
					return;
				}
				chdir($strOriginalWD);
			}
		}		
		else {
			//En v6 ya no tiene caso continuar con la función, porque la tabla ya no contiene una columna con el nombre $this->SurveyField
			return;
		}
		
		//Cambia el tamaño del campo en la tabla de la forma
		$dataType = "VARCHAR({$intLength})";
		$sql = "ALTER TABLE ".$this->SurveyTable." MODIFY COLUMN ".$this->SurveyField." ".$dataType;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	//@JAPR
	
	//@JAPR 2015-07-26: Descontinuado el parámetro $bJustMobileTables
	//@JAPRDescontinuada en v6
	function saveNewAfterRemove($bJustMobileTables = false, $iMobileSurveyID = -1, $iMobileSectionID = -1) {
		return;
	}
	
	function validateSelectOptions() {
		//Modificamos esta funcion para que si se puedan eliminar elementos de las opciones
		//aunque esten presentes en la tabla de hechos
		$arraySelectOptions = array();
		
		//Validamos que las opciones sean unicas
		if(trim($this->StrSelectOptions)!="")
		{
			$arraySelectOptions = explode("\r\n", $this->StrSelectOptions);
		}
		
		$arraySelectOptions = array_values(array_unique($arraySelectOptions));

		$LN = chr(13).chr(10);
		$this->StrSelectOptions = implode($LN, $arraySelectOptions);

		return;
	}
	
	//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
	//Agregado el parámetro $iFromQuestionID para indicar si las opciones de respuesta se deben obtener a partir de esta pregunta indicada en el
	//parámetro o bien si se deben obtener del QuestionID de la instancia. Usada sólo durante el clonado de preguntas para permitir que la 
	//pregunta copia herede todas las opciones de respuesta de la pregunta original en el método Save, ya que él invoca a saveSelectOptions
	function saveSelectOptions($iFromQuestionID = 0)
    {
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		global $form;
		
		if (is_null($iFromQuestionID))
		{
			$iFromQuestionID = 0;
		}
		$iFromQuestionID = (int) $iFromQuestionID;
		
		$currentDate = date("Y-m-d H:i:s");
		$intNumAnswers = null;

		//@JAPR 2011-07-17: Corregido un bug, NO se estaban respaldando estas encuestas si se estaban generando multi-encuestas
		//Antes de eliminar todas las opciones anteriores debemos respaldar el NextQuestion
		$arrayNextQuestion = array();
		$arrayIndDimIDs = array();
		$arrayIndicatorIDs = array();
		$arrayScores = array();
		$arrayConsecutiveIDs = array();
		//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
		$arrayActionTexts = array();
		$arrayResponsibleIDs = array();
		$arrayCategoryIDs = array();
		$arrayDaysDueDate = array();
		$arrayInsertWhen = array();
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		$arrayActionTitles = array();
		
		$arrayDisplayImage = array();
		//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
		$arrayHTMLText = array();
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		$arrayVisible = array();
		$arrayCancelAgenda = array();
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		$arrayCreationUser = array();
		$arrayLastUser = array();
		$arrayCreationDate = array();
		$arrayLastDate = array();
		$arrayCreationVersion = array();
		$arrayLastVersion = array();
		
		//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
		$intSrcQuestionID = $iFromQuestionID;
		if ($intSrcQuestionID <= 0)
		{
			$intSrcQuestionID = $this->QuestionID;
		}
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		$strAdditionalFields = '';
		if (getMDVersion() >= esvExtendedActionsData) {
			$strAdditionalFields .= ', ActionTitle';
		}
		//@JAPR
		//Agregado el num de tel Conchita 2013-04-30
		if (getMDVersion() >= esvCallList) {
			$strAdditionalFields .= ', phoneNumber';
		}
		//conchita
		//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
		if (getMDVersion() >= esvHideAnswers) {
			$strAdditionalFields .= ', Hidden';
		}
		//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$strAdditionalFields .= ', HTMLText';
		}
		
		if (getMDVersion() >= esvDefaultOption) {
			$strAdditionalFields .= ', DefaultValue';
		}
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		if (getMDVersion() >= esvExtendedModifInfo) {
			$strAdditionalFields .= ', CreationUserID, LastUserID, CreationDateID, LastDateID, CreationVersion, LastVersion';
		}
		//@JAPR
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', CancelAgenda';
		}
		
		$sql = "SELECT DisplayText, NextSection, IndDimID, IndicatorID, Score, ConsecutiveID, ActionText, ResponsibleID, CategoryID, DaysDueDate, InsertWhen, DisplayImage $strAdditionalFields 
			FROM SI_SV_QAnswers 
			WHERE QuestionID = ".$intSrcQuestionID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		while(!$aRS->EOF)
		{
			$displaytext = $aRS->fields["displaytext"];
			$nextquestion = (int)$aRS->fields["nextsection"];
			//@JAPR 2013-04-22: Corregido un bug, al copiar preguntas no se deben heredar las dimensiones e indicadores de las respuestas ya que
			//esas se generan por cubo y en el de esta nueva encuesta no existirían, con esto se forzará a crearlas nuevamente
			//@JAPR 2014-09-04: Corregido un bug, la corrección previamente documentada se implementó mal, estaba basandose en la variable local en
			//lugar del parámetro, por lo que en la edición de opciones hubiera limpiado la referencia a indicadores y dimensiones, pero además
			//la condición estaba invertida, se estaban limpiando estos datos sólo no había una pregunta fuente, si ese fuera el caso de todas
			//maneras no hubiera traído valores en estas propiedades
			if ($iFromQuestionID > 0) {
				$inddimid = 0;
				$indicatorid = 0;
			}
			else {
				$inddimid = (int)$aRS->fields["inddimid"];
				$indicatorid = (int)$aRS->fields["indicatorid"];
			}
			//@JAPR
			$consecutiveid = (int)$aRS->fields["consecutiveid"];
			$score = (is_null($aRS->fields["score"])?"":$aRS->fields["score"]);
			
			$arrayNextQuestion[$displaytext] = $nextquestion;
			$arrayIndDimIDs[$displaytext] = $inddimid;
			$arrayIndicatorIDs[$displaytext] = $indicatorid;
			$arrayScores[$displaytext] = $score;
			$arrayConsecutiveIDs[$displaytext] = $consecutiveid;
			//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
			$strActionText = (string) $aRS->fields["actiontext"];
			$intResponsibleID = (int) $aRS->fields["responsibleid"];
			$intCategoryID = (int) $aRS->fields["categoryid"];
			$intDaysDueDate = (int) $aRS->fields["daysduedate"];
			$intInsertWhen = (int) $aRS->fields["insertwhen"];
			$arrayActionTexts[$displaytext] = $strActionText;
			$arrayResponsibleIDs[$displaytext] = $intResponsibleID;
			$arrayCategoryIDs[$displaytext] = $intCategoryID;
			$arrayDaysDueDate[$displaytext] = $intDaysDueDate;
			$arrayInsertWhen[$displaytext] = $intInsertWhen;
			//@JAPR 2012-10-29: Agregada la información extra para las acciones
			$strActionTitle = (string) @$aRS->fields["actiontitle"];
			$arrayActionTitles[$displaytext] = $strActionTitle;
			//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
			$arrayHTMLText[$displaytext] = (string) $aRS->fields["htmltext"];
			//@JAPR
			
			$strDisplayImage = (string) $aRS->fields["displayimage"];
			$arrayDisplayImage[$displaytext] = $strDisplayImage;
			//Conchita 2013-04-30 agregado el phonenum
			if (getMDVersion() >= esvCallList) {
				$strPhoneNumber = (string) @$aRS->fields["phonenumber"];
				$arrayPhoneNumber[$displaytext] = $strPhoneNumber;
			}
			//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
			$intVisible = (int) !((int) @$aRS->fields["hidden"]);
			$arrayVisible[$displaytext] = $intVisible;
			//@JAPR
			
			if (getMDVersion() >= esvAgendaRedesign) {
				$intCancelAgenda = (int) @$aRS->fields["cancelagenda"];
				$arrayCancelAgenda[$displaytext] = $intCancelAgenda;
			}
			
			if (getMDVersion() >= esvDefaultOption) {
				$strOptionDefaultValue = (string) @$aRS->fields["defaultvalue"];
				$arrayOptionDefaultValue[$displaytext] = $strOptionDefaultValue;
			}			
			
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strData = (int) @$aRS->fields["creationuserid"];
				$arrayCreationUser[$displaytext] = $strData;
				$strData = (int) @$aRS->fields["lastuserid"];
				$arrayLastUser[$displaytext] = $strData;
				$strData = (string) @$aRS->fields["creationdateid"];
				$arrayCreationDate[$displaytext] = $strData;
				$strData = (string) @$aRS->fields["lastdateid"];
				$arrayLastDate[$displaytext] = $strData;
				$strData = (float) @$aRS->fields["creationversion"];
				$arrayCreationVersion[$displaytext] = $strData;
				$strData = (float) @$aRS->fields["lastversion"];
				$arrayLastVersion[$displaytext] = $strData;
				//@JAPR 2014-09-29: Se determinó que los datos de creación deben actualizarse al copiar, mas no así los datos de última modificación
				//Si se trata de un clonado, debe actualizar los datos de creación
				if ($iFromQuestionID > 0) {
					$arrayCreationUser[$displaytext] = $_SESSION["PABITAM_UserID"];
					$arrayCreationDate[$displaytext] = $currentDate;
					$arrayCreationVersion[$displaytext] = ESURVEY_SERVICE_VERSION;
				}
			}
			//@JAPR
			
			$aRS->MoveNext();
		}
		//@JAPR
		
		//Se eliminan las opciones anteriores y se almacenan las actuales
		$sql = "DELETE FROM SI_SV_QAnswers WHERE QuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$arraySelectOptions = array();
		
		//Validamos que las opciones sean unicas
		if(trim($this->StrSelectOptions)!="")
		{
			$arraySelectOptions = explode("\r\n", $this->StrSelectOptions);
		}
		
		$arraySelectOptionsCol = array();
		
		//Validamos que las opciones sean unicas
		if(trim($this->StrSelectOptionsCol)!="")
		{		
			$arraySelectOptionsCol = explode("\r\n", $this->StrSelectOptionsCol);
		}

		$this->SelectOptions = array();
	
		$count = 0;
		$intNumAnswers = 0;
		foreach ($arraySelectOptions as $optionKey => $optionValue)
		{
			if(trim($optionValue)!="")
			{
				$displaytext = $optionValue;
				$nextquestion = ((isset($arrayNextQuestion[$displaytext]))?$arrayNextQuestion[$displaytext]:0);
				$inddimid = ((isset($arrayIndDimIDs[$displaytext]))?$arrayIndDimIDs[$displaytext]:0);
				$indicatorid = ((isset($arrayIndicatorIDs[$displaytext]))?$arrayIndicatorIDs[$displaytext]:0);
				$score = ((isset($arrayScores[$displaytext]))?$arrayScores[$displaytext]:"");
				//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
				$strActionText = ((isset($arrayActionTexts[$displaytext]))?$arrayActionTexts[$displaytext]:"");
				$intResponsibleID = ((isset($arrayResponsibleIDs[$displaytext]))?$arrayResponsibleIDs[$displaytext]:-1);
				$intCategoryID = ((isset($arrayCategoryIDs[$displaytext]))?$arrayCategoryIDs[$displaytext]:0);
				$intDaysDueDate = abs((isset($arrayDaysDueDate[$displaytext]))?$arrayDaysDueDate[$displaytext]:0);
				$intInsertWhen = ((isset($arrayInsertWhen[$displaytext]))?$arrayInsertWhen[$displaytext]:0);
				//@JAPR 2012-10-29: Agregada la información extra para las acciones
				$strActionTitle = ((isset($arrayActionTitles[$displaytext]))?$arrayActionTitles[$displaytext]:"");
				//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
				$strHTMLText = ((isset($arrayHTMLText[$displaytext]))?$arrayHTMLText[$displaytext]:'');
				//@JAPR
				
				$strDisplayImage = ((isset($arrayDisplayImage[$displaytext]))?$arrayDisplayImage[$displaytext]:'');

				//conchita 2013-04-30
				$strPhoneNumber = ((isset($arrayPhoneNumber[$displaytext]))?$arrayPhoneNumber[$displaytext]:'');
				//conchita
				//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
				$intVisible = ((isset($arrayVisible[$displaytext]))?$arrayVisible[$displaytext]:1);
				
				if (getMDVersion() >= esvAgendaRedesign) {
					$intCancelAgenda = ((isset($arrayCancelAgenda[$displaytext]))?$arrayCancelAgenda[$displaytext]:0);
				}
				
				$strOptionDefaultValue = ((isset($arrayOptionDefaultValue[$displaytext]))?$arrayOptionDefaultValue[$displaytext]:'');
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				$intCreationUserID = $_SESSION["PABITAM_UserID"];
				$intLastUserID = $_SESSION["PABITAM_UserID"];
				$strCreationDate = $currentDate;
				$strLastDate = $currentDate;
				$dblCreationVersion = ESURVEY_SERVICE_VERSION;
				$dblLastVersion = ESURVEY_SERVICE_VERSION;
				if (getMDVersion() >= esvExtendedModifInfo) {
					$intCreationUserID = ((isset($arrayCreationUser[$displaytext]))?$arrayCreationUser[$displaytext]:$_SESSION["PABITAM_UserID"]);
					$intLastUserID = ((isset($arrayLastUser[$displaytext]))?$arrayLastUser[$displaytext]:$_SESSION["PABITAM_UserID"]);
					$strCreationDate = ((isset($arrayCreationDate[$displaytext]))?$arrayCreationDate[$displaytext]:$currentDate);
					$strLastDate = ((isset($arrayLastDate[$displaytext]))?$arrayLastDate[$displaytext]:$currentDate);
					$dblCreationVersion = ((isset($arrayCreationVersion[$displaytext]))?$arrayCreationVersion[$displaytext]:ESURVEY_SERVICE_VERSION);
					$dblLastVersion = ((isset($arrayLastVersion[$displaytext]))?$arrayLastVersion[$displaytext]:ESURVEY_SERVICE_VERSION);
				}
				//@JAPR
				
				//@JAPR 2012-05-18: Agregadas las acciones en preguntas de selección sencilla/múltiple (Campos ActionText, ResponsibleID, CategoryID, DaysDueDate y InsertWhen)
				//@JAPR 2012-10-29: Agregada la información extra para las acciones
				$strAdditionalFields = '';
				$strAdditionalValues = '';
				if (getMDVersion() >= esvExtendedActionsData) {
					$strAdditionalFields .= ', ActionTitle';
					$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($strActionTitle);
				}
				//@JAPR
				//Conchita 2013-04-30 agregado el num de telefono
				if (getMDVersion() >= esvCallList) {
					$strAdditionalFields .= ', phoneNumber';
					$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($strPhoneNumber);
				}
				//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
				if (getMDVersion() >= esvHideAnswers) {
					$strAdditionalFields .= ', Hidden';
					$strAdditionalValues .= ', '.(int) !$intVisible;
				}
				//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
				if (getMDVersion() >= esvPasswordLangAndRedesign) {
					$strAdditionalFields .= ', HTMLText';
					$strAdditionalValues .= ', '.(string) $this->Repository->DataADOConnection->Quote($strHTMLText);
				}
				
				if (getMDVersion() >= esvDefaultOption) {
					$strAdditionalFields .= ', DefaultValue';
					$strAdditionalValues .= ', '.(string) $this->Repository->DataADOConnection->Quote($strOptionDefaultValue);
				}
				//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
				if (getMDVersion() >= esvExtendedModifInfo) {
					$strAdditionalFields .= ',CreationUserID,LastUserID,CreationDateID,LastDateID,CreationVersion,LastVersion';
					$strAdditionalValues .= ','.$intCreationUserID.','.$intLastUserID.
						','.$this->Repository->DataADOConnection->DBTimeStamp($strCreationDate).
						','.$this->Repository->DataADOConnection->DBTimeStamp($strLastDate).
						','.$dblCreationVersion.','.$dblLastVersion;
				}
				//@JAPR
				if (getMDVersion() >= esvAgendaRedesign) {
					$strAdditionalFields .= ', CancelAgenda';
					$strAdditionalValues .= ', '.(int) $intCancelAgenda;
				}
				
				if (getMDVersion() >= esvEditorHTML) {
					//@JAPR 2015-08-15: Agregado el ipo de pregunta HTML
					$strAdditionalFields .= ', EditorType';
					$strAdditionalValues .= ', '.edtpMessage;
				}
				//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				//$form sólo estará asignada si se trata de un proceso de generación de forma de desarrollo iniciado por processCopyForm.php
				if (getMDVersion() >= esvCopyForms) {
					$strAdditionalFields .= ', SourceID';
					if (isset($form) && $form->ActionType == devAction) {
						$strAdditionalValues .= ', '.$this->CopiedOptionID;
					}
					else {
						$strAdditionalValues .= ', 0';
					}
				}
				//@JAPR
				
				$sql = "INSERT INTO SI_SV_QAnswers (".
						"QuestionID".
						",SortOrder".
						",DisplayText".
						",NextSection".
						",IndDimID".
						",IndicatorID".
						",Score".
						",ActionText".
						",ResponsibleID".
						",CategoryID".
						",DaysDueDate".
						",InsertWhen".
						",DisplayImage".
						$strAdditionalFields.
						") VALUES (".
						$this->QuestionID.
						",".$optionKey.
						",".$this->Repository->DataADOConnection->Quote($optionValue).
						",".$nextquestion.
						",".$inddimid.
						",".$indicatorid.
						",".$this->Repository->DataADOConnection->Quote($score).
						",".$this->Repository->DataADOConnection->Quote($strActionText).
						",".$intResponsibleID.
						",".$intCategoryID.
						",".$intDaysDueDate.
						",".$intInsertWhen.
						",".$this->Repository->DataADOConnection->Quote($strDisplayImage).
						$strAdditionalValues.
						")";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				$this->SelectOptions[$count] = $optionValue;
				//@JAPR 2011-06-27: Agregado el grabado automático de la Metadata para disp. móviles
				$intNumAnswers++;
				//@JAPR
				$count++;
			}
		}
		
		//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
		//Si se especificó una pregunta diferente como el origen, quiere decir que esta instancia debe ser una nueva pregunta, por lo tanto
		//no hay necesidad de ejecutar el siguiente código pues no hay nada que actualizar sino que apenas se va a insertar
		if ($iFromQuestionID <= 0)
		{
			//Realizamos un select sobre todas las opciones insertadas para ver los nuevos ConsecutiveIDs
			$sql = "SELECT ConsecutiveID, DisplayText FROM SI_SV_QAnswers WHERE QuestionID = ".$this->QuestionID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if(!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Actualizamos los ConsecutiveIDs de la tabla SI_SV_ShowQuestions
			while(!$aRS->EOF)
			{
				$newConsecutive = (int)$aRS->fields["consecutiveid"];
				$displaytext = $aRS->fields["displaytext"];
				
				if(isset($arrayConsecutiveIDs[$displaytext]))
				{
					$oldConsecutive = $arrayConsecutiveIDs[$displaytext];
					
					$sqlUpd = "UPDATE SI_SV_ShowQuestions SET ConsecutiveID = ".$newConsecutive." 
							WHERE QuestionID = ".$this->QuestionID." AND ConsecutiveID = ".$oldConsecutive;
					if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
					}
					
					//Si la pregunta es de tipo despliegue matriz, entonces actualizamos los consecutivos en la tabla matriz
					//siempre que las dimensiones se encuentren en el renglon
					if($this->QTypeID==qtpSingle && $this->UseCatalog==0 && $this->QDisplayMode==dspMatrix && $this->OneChoicePer==0)
					{
						$sqlUpd = "UPDATE ".$this->SurveyMatrixTable." SET ConsecutiveID = ".$newConsecutive." 
									WHERE QuestionID = ".$this->QuestionID." AND ConsecutiveID = ".$oldConsecutive;
						if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false)
						{
							//die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyMatrixTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
						}
					}
					
					//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
					$sqlUpd = "UPDATE SI_SV_SurveyAnswerActionFieldsMap SET ConsecutiveID = ".$newConsecutive." 
							WHERE QuestionID = ".$this->QuestionID." AND ConsecutiveID = ".$oldConsecutive;
					if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false)
					{
						//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
					}
					
					//@JAPR 2013-12-09: Agregada la opción para compartir las opciones de respuesta con otras preguntas
					if (getMDVersion() >= esvSimpleChoiceSharing) {
						//Actualiza la tabla de mapeos de opciones de respuesta entre preguntas compartidas
						$sqlUpd = "UPDATE SI_SV_QAnswersMatch SET SourceConsecutiveID = ".$newConsecutive." 
								WHERE SourceQuestionID = ".$this->QuestionID." AND SourceConsecutiveID = ".$oldConsecutive;
						if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false) {
						}
						
						$sqlUpd = "UPDATE SI_SV_QAnswersMatch SET SharedConsecutiveID = ".$newConsecutive." 
								WHERE SharedQuestionID = ".$this->QuestionID." AND SharedConsecutiveID = ".$oldConsecutive;
						if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false) {
						}
					}
					//@JAPR
				}
	
				$aRS->MoveNext();
			}
		}
		
		//Antes de eliminar todas las opciones anteriores debemos respaldar las dimensiones, indicadores y scores
		$arrayIndDimIDsCol = array();
		$arrayIndicatorIDsCol = array();
		$arrayScoresCol = array();
		$arrayConsecutiveIDsCol = array();

		//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
		$intSrcQuestionID = $iFromQuestionID;
		if ($intSrcQuestionID <= 0)
		{
			$intSrcQuestionID = $this->QuestionID;
		}
		$sql = "SELECT DisplayText, IndDimID, IndicatorID, Score, ConsecutiveID FROM SI_SV_QAnswersCol WHERE QuestionID = ".$intSrcQuestionID;
		//@JAPR
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		while(!$aRS->EOF)
		{
			$displaytext = $aRS->fields["displaytext"];
			//@JAPR 2013-04-22: Corregido un bug, al copiar preguntas no se deben heredar las dimensiones e indicadores de las respuestas ya que
			//esas se generan por cubo y en el de esta nueva encuesta no existirían, con esto se forzará a crearlas nuevamente
			//@JAPR 2014-09-04: Corregido un bug, la corrección previamente documentada se implementó mal, estaba basandose en la variable local en
			//lugar del parámetro, por lo que en la edición de opciones hubiera limpiado la referencia a indicadores y dimensiones, pero además
			//la condición estaba invertida, se estaban limpiando estos datos sólo no había una pregunta fuente, si ese fuera el caso de todas
			//maneras no hubiera traído valores en estas propiedades
			if ($iFromQuestionID > 0) {
				$inddimid = 0;
				$indicatorid = 0;
			}
			else {
				$inddimid = (int)$aRS->fields["inddimid"];
				$indicatorid = (int)$aRS->fields["indicatorid"];
			}
			//@JAPR
			$score = (is_null($aRS->fields["score"])?"":$aRS->fields["score"]);
			$consecutiveid = (int)$aRS->fields["consecutiveid"];
			
			$arrayIndDimIDsCol[$displaytext] = $inddimid;
			$arrayIndicatorIDsCol[$displaytext] = $indicatorid;
			$arrayScoresCol[$displaytext] = $score;
			$arrayConsecutiveIDsCol[$displaytext] = $consecutiveid;

			$aRS->MoveNext();
		}

		//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
		//Para las respuestas cuando se trata de un campo con captura como Matriz se requirió una tabla adicional
		//Se eliminan las opciones anteriores y se almacenan las actuales
		$sql = "DELETE FROM SI_SV_QAnswersCol WHERE QuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$this->SelectOptionsCol = array();

		$count = 0;
		
		foreach ($arraySelectOptionsCol as $optionKey => $optionValue)
		{
			if(trim($optionValue)!="")
			{
				$displaytext = $optionValue;
				$inddimid = ((isset($arrayIndDimIDsCol[$displaytext]))?$arrayIndDimIDsCol[$displaytext]:0);
				$indicatorid = ((isset($arrayIndicatorIDsCol[$displaytext]))?$arrayIndicatorIDsCol[$displaytext]:0);
				$score = ((isset($arrayScoresCol[$displaytext]))?$arrayScoresCol[$displaytext]:"");

				$sql = "INSERT INTO SI_SV_QAnswersCol (".
						"QuestionID".
						",SortOrder".
						",DisplayText".
						",IndDimID".
						",IndicatorID".
						",Score".
						") VALUES (".
						$this->QuestionID.
						",".$optionKey.
						",".$this->Repository->DataADOConnection->Quote($optionValue).
						",".$inddimid.
						",".$indicatorid.
						",".$this->Repository->DataADOConnection->Quote($score).
						")";
	
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$this->SelectOptionsCol[$count] = $optionValue;
				if ($this->QDisplayMode == dspMatrix)
				{
					$intNumAnswers++;
				}
				$count++;
			}
		}
		
		//Realizamos un select sobre todas las opciones insertadas para ver los nuevos ConsecutiveIDs
		//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
		//Si se especificó una pregunta diferente como el origen, quiere decir que esta instancia debe ser una nueva pregunta, por lo tanto
		//no hay necesidad de ejecutar el siguiente código pues no hay nada que actualizar sino que apenas se va a insertar
		if ($iFromQuestionID <= 0)
		{
			$sql = "SELECT ConsecutiveID, DisplayText FROM SI_SV_QAnswersCol WHERE QuestionID = ".$this->QuestionID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if(!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Actualizamos los ConsecutiveIDs de la tabla SVSurveyMatrixData_<ModelID>
			while(!$aRS->EOF)
			{
				$newConsecutive = (int)$aRS->fields["consecutiveid"];
				$displaytext = $aRS->fields["displaytext"];
				
				if(isset($arrayConsecutiveIDsCol[$displaytext]))
				{
					$oldConsecutive = $arrayConsecutiveIDsCol[$displaytext];
	
					//Si la pregunta es de tipo despliegue matriz, entonces actualizamos los consecutivos en la tabla matriz
					//siempre que las dimensiones se encuentren en la columna
					if($this->QTypeID==qtpSingle && $this->UseCatalog==0 && $this->QDisplayMode==dspMatrix && $this->OneChoicePer==1)
					{
						$sqlUpd = "UPDATE ".$this->SurveyMatrixTable." SET ConsecutiveID = ".$newConsecutive." 
									WHERE QuestionID = ".$this->QuestionID." AND ConsecutiveID = ".$oldConsecutive;
						if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyMatrixTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
						}
					}
				}
	
				$aRS->MoveNext();
			}
		}
		//@JAPR
	}
	
	/* Obtiene el ID de dimensión global que tiene el mismo nombre que el especificado. Se modificó esta función para sólo considerar aquellas dimensiones
	que ya son de eForms, ya que son las únicas que con seguridad se sabe que serán compatibles con el producto y que podríamos modificar como sea necesario,
	adicionalmente la dimensión tiene que ser independiente y no una dimensión que sea un atributo de un catálogo de eForms, ya que esos no tienen una tabla
	única y sus datos se graban de manera diferente a lo esperado en una dimensión normal
	*/
	//@JAPRDescontinuada en v6
	function getDimClaDescripByDimName($dimensionName)
	{
		$cla_descrip = -1;
		//@JAPR 2015-05-05: Corregida la reutilización de dimensiones para que no provoque errores
		//Se agregó el subquery para descartar a todas las dimensiones que están configuradas como Snow Flake o bien como dimensiones atributo, incluyendo
		//obviamente a sus padres, esto debido a que tales configuraciones no podrían ser capturadas adecuadamente por eForms dado a que no conocerá ni sabría
		//como llenar los campos adicionales que puede contener, o bien porque al no ser un catálogo único estaría corrompiendo tablas preparadas para usos muy
		//diferentes. Adicionalmente sólo se consideran a las dimensiones creadas por el propio eForms
		//No se considerarán como válidas las dimensiones FCD que son las desasociadas en los catálogos a partir de v4, ya que se mantendrá la consistencia
		//en comportamiento simulando que son dimensiones atributos (aunque técnicamente se podrían reutilizar sin problema)
		//@JAPRWarning: Se tiene que hacer referencia a tablas de bases de datos diferentes, así que en caso de ya no poder tener acceso directo, se
		//deberá modificar este query
		$strDataWarehouseName = $this->Repository->DataADODBDatabase;
		$sql = "SELECT A.CLA_DESCRIP 
			FROM SI_DESCRIP_ENC A 
			WHERE A.CREATED_BY = ".CREATED_BY_EFORMS." AND 
				A.NOM_LOGICO = ".$this->Repository->ADOConnection->Quote($dimensionName)." AND 
				A.CLA_DESCRIP NOT IN ( 
					SELECT DISTINCT CLA_DESCRIP 
					FROM SI_DESCRIP_DET 
					UNION 
					SELECT DISTINCT CLA_PADRE 
					FROM SI_DESCRIP_DET 
					UNION 
					SELECT DISTINCT IndDimID 
					FROM {$strDataWarehouseName}.SI_SV_CatalogMember 
					WHERE IndDimID > 0 
				)";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$cla_descrip = (int)$aRS->fields["cla_descrip"];
		}
		
		return $cla_descrip;
	}
	
	//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
	//Para este tipo de preguntas se generan 3 dimensiones, una de ella es la dimensión base donde se graba la posición como un par
	//"Latitude,Longitude", mientras que las otras dos son dimensiones-atributo precisamente para grabar por separado (pero ligadas
	//a la base) la Latitude y Longitude. No requiere imagen, comentario ni captura en general de nada extra
	//@JAPRDescontinuada en v6
	function CreateGPSDimension() {
		global $gblModMngrErrorMessage;
		global $gblEFormsNA;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		$strOriginalWD = getcwd();
		
		//Primero crea la dimensión normal de la pregunta, esta contendrá la posición del GPS en par (es sólo una dimensión de relleno para tener
		//una dimensión base donde agregar los atributos que realmente pueden explotarse en Artus con la Latitude y Longitude)
		if ($this->IndDimID <= 0) {
			$this->createModelDimension();
		}
		
		//Si no se pudo crear la dimensión con el proceso de arriba, entonces no debe continuar
		if ($this->IndDimID <= 0) {
			return;
		}
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/attribdimension.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		global $generateXLS;
		$generateXLS = false;
		
		//Despues de crear la dimension de la pregunta se debe crear el atributo Latitud
		if ($this->LatitudeAttribID <= 0) {
			$AttribName = $this->AttributeName." Latitude";
			$cla_descrip = -1;
			$cla_descrip_parent = $this->IndDimID;
			$cla_concepto = $this->ModelID;
			
			//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
			//Dimensión atributo, no debe ser reutilizada de otro cubo
			$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
			$anInstanceAttrib->Dimension->DimensionName = $AttribName;
			$anInstanceAttrib->Dimension->ParentDimensionID = $cla_descrip_parent;
			$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
			$anInstanceAttrib->Dimension->DimensionID = $cla_descrip;
			$objMMAnswer = @$anInstanceAttrib->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error creating Latitude dimension").": ".$strMMError.". ";
				return;
			}
			
			$this->LatitudeAttribID = $anInstanceAttrib->Dimension->DimensionClaDescrip;
			
			$sql = "UPDATE SI_SV_Question SET LatitudeAttribID = ".$this->LatitudeAttribID." WHERE QuestionID = ".$this->QuestionID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Despues de crear el atributo Latitud se debe crear el atributo Longitud
		if ($this->LongitudeAttribID <= 0) {
			$AttribName = $this->AttributeName." Longitude";
			$cla_descrip = -1;
			$cla_descrip_parent = $this->IndDimID;
			$cla_concepto = $this->ModelID;
			
			//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
			//Dimensión atributo, no debe ser reutilizada de otro cubo
			$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
			$anInstanceAttrib->Dimension->DimensionName = $AttribName;
			$anInstanceAttrib->Dimension->ParentDimensionID = $cla_descrip_parent;
			$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
			$anInstanceAttrib->Dimension->DimensionID = $cla_descrip;
			//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
			$objMMAnswer = @$anInstanceAttrib->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error creating Longitude dimension").": ".$strMMError.". ";
				return;
			}
			
			$this->LongitudeAttribID = $anInstanceAttrib->Dimension->DimensionClaDescrip;
			
			$sql = "UPDATE SI_SV_Question SET LongitudeAttribID = ".$this->LongitudeAttribID." WHERE QuestionID = ".$this->QuestionID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Despues de crear el atributo Longitud se debe crear el atributo Accuracy
		if ($this->AccuracyAttribID <= 0) {
			$AttribName = $this->AttributeName." Accuracy";
			$cla_descrip = -1;
			$cla_descrip_parent = $this->IndDimID;
			$cla_concepto = $this->ModelID;
			
			//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
			//Dimensión atributo, no debe ser reutilizada de otro cubo
			$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
			$anInstanceAttrib->Dimension->DimensionName = $AttribName;
			$anInstanceAttrib->Dimension->ParentDimensionID = $cla_descrip_parent;
			$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
			$anInstanceAttrib->Dimension->DimensionID = $cla_descrip;
			$objMMAnswer = @$anInstanceAttrib->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error creating Accuracy dimension").": ".$strMMError.". ";
				return;
			}
			
			$this->AccuracyAttribID = $anInstanceAttrib->Dimension->DimensionClaDescrip;
			
			$sql = "UPDATE SI_SV_Question SET AccuracyAttribID = ".$this->AccuracyAttribID." WHERE QuestionID = ".$this->QuestionID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Actualiza el valor de No Aplica
		$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->IndDimID);
		if (!is_null($anInstanceModelDim)) {
			$tableName = $anInstanceModelDim->Dimension->TableName;
			$fieldSubrogateKey = $tableName."KEY";
			$fieldLatDescription = "DSC_".$this->LatitudeAttribID;
			$fieldLongDescription = "DSC_".$this->LongitudeAttribID;
			$fieldAccDescription = "DSC_".$this->AccuracyAttribID;
			
			$sql = "UPDATE ".$tableName." SET ".
					$fieldLatDescription." = ".$this->Repository->DataADOConnection->Quote($gblEFormsNA).
					", ".$fieldLongDescription." = ".$this->Repository->DataADOConnection->Quote($gblEFormsNA).
					", ".$fieldAccDescription." = ".$this->Repository->DataADOConnection->Quote($gblEFormsNA).
				" WHERE ".$fieldSubrogateKey." = 1";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	
	//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
	//Elimina tanto la dimensión de la pregunta como los atributos asociados a la misma
	//@JAPRDescontinuada en v6
	function removeGPSDimension() {
		//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		global $gblEFormsNA;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/attribdimension.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		global $generateXLS;
		$generateXLS = false;
		
		//Elimina el atributo Latitude
		if ($this->LatitudeAttribID > 0) {
			$cla_descrip_parent = $this->IndDimID;
			$cla_concepto = $this->ModelID;
			
			$anInstanceAttrib = BITAMAttribDimension::NewAttribDimensionWithDimensionID($this->Repository, $cla_concepto, $cla_descrip_parent, -1, $this->LatitudeAttribID);
			if (!is_null($anInstanceAttrib)) {
				$anInstanceAttrib->Dimension->CheckStages = false;
				@$anInstanceAttrib->Dimension->remove();
			}
			chdir($strOriginalWD);
			
			$sql = "UPDATE SI_SV_Question SET LatitudeAttribID = null WHERE QuestionID = ".$this->QuestionID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Elimina el atributo Longitude
		if ($this->LongitudeAttribID > 0) {
			$cla_descrip_parent = $this->IndDimID;
			$cla_concepto = $this->ModelID;
			
			$anInstanceAttrib = BITAMAttribDimension::NewAttribDimensionWithDimensionID($this->Repository, $cla_concepto, $cla_descrip_parent, -1, $this->LongitudeAttribID);
			if (!is_null($anInstanceAttrib)) {
				$anInstanceAttrib->Dimension->CheckStages = false;
				@$anInstanceAttrib->Dimension->remove();
			}
			chdir($strOriginalWD);
			
			$sql = "UPDATE SI_SV_Question SET LongitudeAttribID = null WHERE QuestionID = ".$this->QuestionID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Elimina el atributo Accuracy
		if ($this->AccuracyAttribID > 0) {
			$cla_descrip_parent = $this->IndDimID;
			$cla_concepto = $this->ModelID;
			
			$anInstanceAttrib = BITAMAttribDimension::NewAttribDimensionWithDimensionID($this->Repository, $cla_concepto, $cla_descrip_parent, -1, $this->AccuracyAttribID);
			if (!is_null($anInstanceAttrib)) {
				$anInstanceAttrib->Dimension->CheckStages = false;
				@$anInstanceAttrib->Dimension->remove();
			}
			chdir($strOriginalWD);
			
			$sql = "UPDATE SI_SV_Question SET AccuracyAttribID = null WHERE QuestionID = ".$this->QuestionID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Elimina la dimensión de la pregunta
		if ($this->IndDimID > 0) {
			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->IndDimID);
			if (!is_null($anInstanceModelDim)) {
				$anInstanceModelDim->Dimension->CheckStages = false;
				@$anInstanceModelDim->Dimension->remove();
			}
		}
	}
	//@JAPR
	
	//@JAPR 2015-05-13: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
	//estaban limitadas a 255 caracteres (#BA7866)
	/* Crea la dimensión base de esta pregunta, la cual en general es un VARCHAR(255) y aplica para la mayoría de los casos de preguntas,
	excepto contadas situaciones como las alfanuméricas que permitirán especificar la longitud, o las tipo TEXT que se crearán con la máxima
	longitud posible
	//@JAPRDescontinuada en v6
	*/
	function createModelDimension()
	{
		global $gblEFormsNA;
		//@JAPR 2013-03-20: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//@JAPR 2015-05-13: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
		//estaban limitadas a 255 caracteres (#BA7866)
		$intLength = null;
		if (!is_null($this->QTypeID)) {
			switch ($this->QTypeID) {
				case qtpOpenString:
					$intLength = DEFAULT_TEXT_LENGTH;
					break;
				case qtpOpenAlpha:
					$intLength = $this->QLength;
					if (is_null($intLength) || !is_numeric($intLength) || (int) $intLength <= 0 || (int) $intLength > MAX_ALPHA_LENGTH) {
						if ((int) $intLength > MAX_ALPHA_LENGTH) {
							$intLength = MAX_ALPHA_LENGTH;
						}
						else {
							$intLength = DEFAULT_ALPHA_LENGTH;
						}
					}
					break;
			}
		}
		//@JAPR
		
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		//@JAPR 2015-05-05: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión se comparte siempre y cuando se reutilice una de eForms que no caiga entre las que causan excepción
		$dimensionName = $this->AttributeName;
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//@JAPR 2015-05-13: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
		//estaban limitadas a 255 caracteres (#BA7866)
		try {
			switch ($this->QTypeID) {
				case qtpOpenAlpha:
				case qtpOpenString:
					if (property_exists($anInstanceModelDim->Dimension, "LongKey")) {
						$anInstanceModelDim->Dimension->LongKey = $intLength;
					}
					if (property_exists($anInstanceModelDim->Dimension, "LongDescription")) {
						$anInstanceModelDim->Dimension->LongDescription = $intLength;
					}
					break;
			}
		} catch (Exception $e) {
			//No se reporta error, pero generará la dimensión con el default del Model Manager
		}
		//@JAPR
		
		//Verificamos si hay dimension existente con el nombre de AttributeName en caso contario
		//entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = $this->getDimClaDescripByDimName($dimensionName);
		
		//@JAPR 2015-05-05: Corregida la reutilización de dimensiones para que no provoque errores
		//@JAPR 2015-05-05: Modificado para reutilizar código común independientemente de si se reutilizó o no una dimensión
		global $generateXLS;
		$generateXLS = false;
		
		if ($cla_descrip > 0) {
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
		}
		//@JAPR 2013-03-20: Agregado el control de errores del Model Manager
		$objMMAnswer = @$anInstanceModelDim->save();
		chdir($strOriginalWD);
		$strMMError = identifyModelManagerError($objMMAnswer);
		if ($strMMError == '') {
			//Si el tipo de pregunta es de seleccion simple se debera realizar la insercion de los
			//valores/opciones a la tabla del catalogo de dimension
			//Conchita 2013-04-29 agregado callList
			$blnInsertNoApplyValue = true;
			if ($this->QTypeID == qtpSingle || $this->QTypeID == qtpCallList) {
				if ($cla_descrip > 0) {
					$this->updateDimensionValues($anInstanceModelDim);
				}
				else {
					$this->insertDimensionValues($anInstanceModelDim);
				}
			}
			//Si se trata de una pregunta multiple choice insertamos los valores como si 
			//fuera una opcion de dimension incluyendo valor de No Aplica
			//que solo sucede en preguntas de dimension dinamica dentro de dicha seccion
			elseif ($this->QTypeID == qtpMulti) {
				$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
				if ($this->SectionID == $dynamicSectionID) {
					$blnInsertNoApplyValue = false;
					$this->insertValuesInDimOption($anInstanceModelDim);
				}
			}
			
			if ($blnInsertNoApplyValue) {
				//Insertamos el valor de No Aplica en el catalogo
				$this->insertNoApplyValue($anInstanceModelDim);
			}
			
			//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
			$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
			$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
			
			$dimTableName = $anInstanceModelDim->Dimension->TableName;
			$dimFieldDesc = $anInstanceModelDim->Dimension->FieldDescription;
			
			//Obtenemos el ID con el que se inserto el valor de No Aplica para una dimension determinada
			$sql = "SELECT ".$dimFieldKey." FROM ".$dimTableName." WHERE ".$dimFieldDesc." = ".$this->Repository->DataADOConnection->Quote($gblEFormsNA);
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				//@JAPR 2013-03-20: Ya no interrumpirá el proceso por este error porque de hacerlo no se podría eliminar la dimensión
				//die("(".__METHOD__.") ".translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			}
			
			$noApplyKey = @$aRS->fields[strtolower($dimFieldKey)];
			if (!is_null($noApplyKey)) {
				$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = ".$noApplyKey;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					//@JAPR 2013-03-20: Ya no interrumpirá el proceso por este error porque de hacerlo no se podría eliminar la dimensión
					//die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				}
			}
			
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
			if ($cla_descrip > 0) {
				$this->IndDimID = $cla_descrip;
			}
			else {
				$this->IndDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			
			$sql = "UPDATE SI_SV_Question SET IndDimID = ".$this->IndDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else {
			if ($cla_descrip > 0) {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding question's dimension").": ".$strMMError.". ";
			}
			else {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error creating question's dimension").": ".$strMMError.". ";
			}
		}
	}
	
	//@JAPRDescontinuada en v6
	function createModelDimensionIMG()
	{
		global $gblEFormsNA;
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		if($this->HasReqPhoto>0)
		{
			$strOriginalWD = getcwd();
	
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			
			//@JAPR 2015-05-05: Corregida la reutilización de dimensiones para que no provoque errores
			//Esta dimensión se comparte siempre y cuando se reutilice una de eForms que no caiga entre las que causan excepción
			$dimensionName = "IMG ".$this->AttributeName;
			$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->ModelID);
			$anInstanceModelDim->Dimension->Estatus = 1;
			$anInstanceModelDim->Dimension->ParentID = -1;
			$anInstanceModelDim->Dimension->IsIncremental = 0;
			$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
			$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
			$anInstanceModelDim->Dimension->UseKey = 0;
			//@JAPR 2014-10-30: Agregada la propiedad para indicar a Artus Web que esta dimensión contiene imagenes
			@$anInstanceModelDim->Dimension->showImage = 1;
			//@JAPR
			
			//Verificamos si hay dimension existente con el nombre de AttributeName en caso contario
			//entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
			$cla_descrip = $this->getDimClaDescripByDimName($dimensionName);
			
			//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
			//@JAPR 2015-05-06: Modificado para reutilizar código común independientemente de si se reutilizó o no una dimensión
			global $generateXLS;
			$generateXLS = false;
			
			if ($cla_descrip > 0) {
				$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			}
			//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica en el catalogo
				$this->insertNoApplyValue($anInstanceModelDim);
				
				//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
				$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
				$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
				
				$dimTableName = $anInstanceModelDim->Dimension->TableName;
				$dimFieldDesc = $anInstanceModelDim->Dimension->FieldDescription;
				
				//Obtenemos el ID con el que se inserto el valor de No Aplica para una dimension determinada
				$sql = "SELECT ".$dimFieldKey." FROM ".$dimTableName." WHERE ".$dimFieldDesc." = ".$this->Repository->DataADOConnection->Quote($gblEFormsNA);
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$noApplyKey = $aRS->fields[strtolower($dimFieldKey)];
				
				$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = ".$noApplyKey;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
				if ($cla_descrip > 0) {
					$this->ImgDimID = $cla_descrip;
				}
				else {
					$this->ImgDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
				}
				
				$sql = "UPDATE SI_SV_Question SET ImgDimID = ".$this->ImgDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			else {
				if ($cla_descrip > 0) {
					$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding question's photo dimension").": ".$strMMError.". ";
				}
				else {
					$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error creating question's photo dimension").": ".$strMMError.". ";
				}
			}
		}
	}
	
	//JAPRDescontinuada en v6
	function createModelDimensionDOC()
	{
		global $gblEFormsNA;
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//@JAPR 2014-09-25: Corregido un bug, no estaba volviendo a construir la dimensión documento en caso de no existir cuando se editaba
		if($this->HasReqDocument>0 && $this->DocDimID <= 0)
		{
			$strOriginalWD = getcwd();
			
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			
			//@JAPR 2015-05-05: Corregida la reutilización de dimensiones para que no provoque errores
			//Esta dimensión se comparte siempre y cuando se reutilice una de eForms que no caiga entre las que causan excepción
			$dimensionName = "DOC ".$this->AttributeName;
			$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->ModelID);
			$anInstanceModelDim->Dimension->Estatus = 1;
			$anInstanceModelDim->Dimension->ParentID = -1;
			$anInstanceModelDim->Dimension->IsIncremental = 0;
			$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
			$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
			$anInstanceModelDim->Dimension->UseKey = 0;
		
			//Verificamos si hay dimension existente con el nombre de AttributeName en caso contario
			//entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
			$cla_descrip = $this->getDimClaDescripByDimName($dimensionName);
			
			//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
			//@JAPR 2015-05-06: Modificado para reutilizar código común independientemente de si se reutilizó o no una dimensión
			global $generateXLS;
			$generateXLS = false;
			
			if ($cla_descrip > 0) {
				$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			}
			//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica en el catalogo
				$this->insertNoApplyValue($anInstanceModelDim);
				
				//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
				$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
				$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
				
				$dimTableName = $anInstanceModelDim->Dimension->TableName;
				$dimFieldDesc = $anInstanceModelDim->Dimension->FieldDescription;
				
				//Obtenemos el ID con el que se inserto el valor de No Aplica para una dimension determinada
				$sql = "SELECT ".$dimFieldKey." FROM ".$dimTableName." WHERE ".$dimFieldDesc." = ".$this->Repository->DataADOConnection->Quote($gblEFormsNA);
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$noApplyKey = $aRS->fields[strtolower($dimFieldKey)];
				
				$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = ".$noApplyKey;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
				if ($cla_descrip > 0) {
					$this->DocDimID = $cla_descrip;
				}
				else {
					$this->DocDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
				}
			}
			else {
				if ($cla_descrip > 0) {
					$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding question's document dimension").": ".$strMMError.". ";
				}
				else {
					$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error creating question's document dimension").": ".$strMMError.". ";
				}
			}
			
			$sql = "UPDATE SI_SV_Question SET DocDimID = ".$this->DocDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	
	//Esta funcion solamente se manda a llamar cuando la pregunta es de seleccion multiple
	//@JAPRDescontinuada en v6
	function createSelectOptionDims()
	{
		global $generatePHP;
		global $generateXLS;
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		$generatePHP = false; //Conchita 2013-01-03 Se inicializa en false y se pondra en true para que si se genere desde model manager
		$generateXLS = false; //siempre en false
		//Por si las dudas verificamos si la pregunta es realmente una
		//pregunta de con comportamiento de multiples dimensiones
		
		if($this->IsMultiDimension==0)
		{
			return;
		}
		
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		$selectLenght = count($this->SelectOptions);
		$i = 0;
		foreach ($this->SelectOptions as $optionKey=>$optionValue)
		{
			$dimid = 0;
			if ($i == $selectLenght - 1){
			//Es el ultimo, por lo tanto hay que poner en true la bandera para que modelmanager genere el pdf o xls
				$generatePHP = true;
			
			}
			if(isset($this->QIndDimIds[$optionKey]))
			{
				$dimid = (int)$this->QIndDimIds[$optionKey];
			}
			
			if(is_null($dimid)||$dimid==0)
			{
				//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
				//Dimensión de opción de respuesta, estas dimensiones no se habían programado para reutilizarse, sino que en caso de identificarse otra
				//con el mismo nombre (lo cual sólo podría haber sido bajo mucha planeación o bien pura suerte para crear formas diferentes donde algunas
				//preguntas en la misma posición tuvieran el mismo nombre, o bien, que fuera una copia de una forma) simplemente se le agregaba la referencia
				//a la forma que pertenecían para forzar que se consideraran diferentes, así que se dejará programado de esa manera
				$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->ModelID);
				$anInstanceModelDim->Dimension->Estatus = 1;
				$anInstanceModelDim->Dimension->ParentID = -1;
				$anInstanceModelDim->Dimension->IsIncremental = 0;
				$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
				$anInstanceModelDim->Dimension->UseKey = 0;
				$dimensionName = "Q".$this->QuestionNumber."-".$this->AttributeName."-".$optionValue;
				$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
				$cla_descrip = $this->getDimClaDescripByDimName($dimensionName);
				if($cla_descrip!=-1)
				{
					$dimensionName = "SV".$this->SurveyID."Q".$this->QuestionNumber."-".$this->AttributeName."-".$optionValue;
				}
				
				$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
				//@JAPR 2013-02-14: Agregado el control de errores del Model Manager
				$objMMAnswer = @$anInstanceModelDim->save();
				chdir($strOriginalWD);
				$strMMError = identifyModelManagerError($objMMAnswer);
				//@JAPR 2013-03-26: Corregido un bug, estaba invertida la condición y reportaba cuando no había error
				if ($strMMError != '') {
					die("(".__METHOD__.") ".translate("Error adding multiple choice option's dimension ($dimensionName)").": ".$strMMError.". ");
				}
				
				//Insertamos los valores a esa opcion - dimension
				$this->insertValuesInDimOption($anInstanceModelDim);
				
				//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
				$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
				$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
				
				//Obtenemos el ID con el que se inserto el valor de No Aplica para la opcion - dimension
				$noApplyKey = 1;
				
				//Actualizamos la tabla de hechos para los registros existentes a los cuales se les agrego un campo dimension nuevo
				$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = ".$noApplyKey;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_QAnswers
				$this->QIndDimIds[$optionKey] = $anInstanceModelDim->Dimension->DimensionClaDescrip;
				
				$sql = "UPDATE SI_SV_QAnswers SET IndDimID = ".$this->QIndDimIds[$optionKey]." 
					WHERE QuestionID = ".$this->QuestionID." AND SortOrder = ".$optionKey;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			$i++;
		}
	}
	
	//Esta funcion solamente se manda a llamar cuando la pregunta es de seleccion simple con matriz
	//JAPRDescontinuada en v6
	function createModelDimMatrixData()
	{
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		$strOriginalWD = getcwd();

		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		$arrayDimensionNames = array();
		$arrayAnswerOptions = array();
		$strQIndDimIds = "";

		if($this->OneChoicePer==0)
		{
			$arrayDimensionNames = $this->SelectOptions;
			$arrayAnswerOptions = $this->SelectOptionsCol;
			$strQIndDimIds = "QIndDimIds";
			$theDimensions = $this->QIndDimIds;
			$strEFormsTable = "SI_SV_QAnswers";
		}
		else 
		{
			$arrayDimensionNames = $this->SelectOptionsCol;
			$arrayAnswerOptions = $this->SelectOptions;
			$strQIndDimIds = "QIndDimIdsCol";
			$theDimensions = $this->QIndDimIdsCol;
			$strEFormsTable = "SI_SV_QAnswersCol";
		}

		foreach ($arrayDimensionNames as $optionKey=>$optionValue)
		{
			$dimid = 0;
			
			if(isset($theDimensions[$optionKey]))
			{
				$dimid = (int)$theDimensions[$optionKey];
			}

			if(is_null($dimid)||$dimid==0)
			{
				$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->ModelID);
				$anInstanceModelDim->Dimension->Estatus = 1;
				$anInstanceModelDim->Dimension->ParentID = -1;
				$anInstanceModelDim->Dimension->IsIncremental = 0;
				$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
				$anInstanceModelDim->Dimension->UseKey = 0;
				$dimensionName = $this->AttributeName."-".$optionValue;
				$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
				
				$cla_descrip = $this->getDimClaDescripByDimName($dimensionName);
				if($cla_descrip!=-1)
				{
					$dimensionName = "Q".$this->QuestionNumber."-".$this->AttributeName."-".$optionValue;
				}
				$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
				global $generateXLS;
				$generateXLS = false;
				
				//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
				$objMMAnswer = @$anInstanceModelDim->save();
				chdir($strOriginalWD);
				
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError == '') {
					//Insertamos los valores a esa opcion de matriz - dimension
					$this->insertValuesInDimMatrixOption($anInstanceModelDim, $arrayAnswerOptions);
					
					//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
					$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
					$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
					
					//Obtenemos el ID con el que se inserto el valor de No Aplica para la opcion - dimension
					$noApplyKey = 1;
					
					//Actualizamos la tabla de hechos para los registros existentes a los cuales se les agrego un campo dimension nuevo
					$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = ".$noApplyKey;
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_QAnswers
					$this->$strQIndDimIds[$optionKey] = $anInstanceModelDim->Dimension->DimensionClaDescrip;
					$sql = "UPDATE ".$strEFormsTable." SET IndDimID = ".$this->$strQIndDimIds[$optionKey]." 
						WHERE QuestionID = ".$this->QuestionID." AND SortOrder = ".$optionKey;
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$strEFormsTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
				else {
					$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding questions's matrix dimension ($dimensionName)").": ".$strMMError.". ";
				}
			}
		}
	}
	
	//@JAPRDescontinuada en v6
	//Esta funcion solamente se manda a llamar cuando la pregunta es de seleccion multiple
	function createMatrixDataIndicators()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
	
		$strOriginalWD = getcwd();

		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		
		$arrayDimensionNames = array();
		$arrayAnswerOptions = array();
		$strQIndicatorIds = "";

		if($this->OneChoicePer==0)
		{
			$arrayOptions = $this->SelectOptions;
			$strQIndicatorIds = "QIndicatorIds";
			$strEFormsTable = "SI_SV_QAnswers";
			$theQIndicatorIds = $this->QIndicatorIds;
		}
		else 
		{
			$arrayOptions = $this->SelectOptionsCol;
			$strQIndicatorIds = "QIndicatorIdsCol";
			$strEFormsTable = "SI_SV_QAnswersCol";
			$theQIndicatorIds = $this->QIndicatorIdsCol;
		}

		foreach ($arrayOptions as $optionKey=>$optionValue)
		{
			$indicatorid = 0;
			
			if(isset($theQIndicatorIds[$optionKey]))
			{
				$indicatorid = (int)$theQIndicatorIds[$optionKey];
			}

			if(is_null($indicatorid)||$indicatorid==0)
			{
				$indicatorName = $this->AttributeName."-".$optionValue;

				$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
				$anInstanceIndicator->ModelID = $this->ModelID;
				$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->ModelID);
				$anInstanceIndicator->IndicatorName = $indicatorName;
				$anInstanceIndicator->formato = "#,##0";
				$anInstanceIndicator->agrupador = "SUM";
				$anInstanceIndicator->sql_source = "SUM";
				global $generateXLS;
				$generateXLS = false;

				$anInstanceIndicator->save();

				chdir($strOriginalWD);
				
				//Procedemos a actualizar el indicador en la tabla de hechos
				$indicatorField = $anInstanceIndicator->field_name;
				$factTableName = $anInstanceIndicator->oModel->nom_tabla;
				
				$sql = "UPDATE ".$factTableName." SET ".$indicatorField." = 0 WHERE ".$indicatorField." IS NULL";
				
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				//Obtenemos el Id del indicador creado y lo almacenamos en la tabla SI_SV_QAnswers
				$this->$strQIndicatorIds[$optionKey] = $anInstanceIndicator->IndicatorID;
				
				$sql = "UPDATE ".$strEFormsTable." SET IndicatorID = ".$this->$strQIndicatorIds[$optionKey]." 
						WHERE QuestionID = ".$this->QuestionID." AND SortOrder = ".$optionKey;
				
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$strEFormsTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}

	//Esta funcion solamente se manda a llamar cuando la pregunta es de seleccion multiple
	//@JAPRDescontinuada en v6
	function createSelectOptionInds()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
	
		//Por si las dudas verificamos si la pregunta es realmente una
		//pregunta con comportamiento de multiples dimensiones y que 
		//no provenga de una matriz
		if($this->IsMultiDimension==0 || $this->QDisplayMode == dspMatrix)
		{
			return;
		}
		
		$strOriginalWD = getcwd();

		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");

		foreach ($this->SelectOptions as $optionKey=>$optionValue)
		{
			$indicatorid = 0;
			
			if(isset($this->QIndicatorIds[$optionKey]))
			{
				$indicatorid = (int)$this->QIndicatorIds[$optionKey];
			}

			if(is_null($indicatorid)||$indicatorid==0)
			{
				$indicatorName = "Q".$this->QuestionNumber."-".$this->AttributeName."-".$optionValue;

				$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
				$anInstanceIndicator->ModelID = $this->ModelID;
				$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->ModelID);
				$anInstanceIndicator->IndicatorName = $indicatorName;
				//@JAPR 2014-12-17: Corregido un bug, no estaba heredando el formato de la pregunta numérica
				if ($this->MCInputType == mpcNumeric && $this->IsIndicatorMCNew == 1) {
					//Se trata de un indicador basado en una pregunta numérica, por tanto hereda el formato personalizado
					$anInstanceIndicator->formato = $this->FormatMask;
				}
				else {
					//En este caso debe ser un indicador de Score, así que no hay formato personalizado
					$anInstanceIndicator->formato = "#,##0";
				}
				//@JAPR
				$anInstanceIndicator->agrupador = "SUM";
				$anInstanceIndicator->sql_source = "SUM";
				global $generateXLS;
				$generateXLS = false;

				$anInstanceIndicator->save();

				chdir($strOriginalWD);
				
				//Procedemos a actualizar el indicador en la tabla de hechos
				$indicatorField = $anInstanceIndicator->field_name;
				$factTableName = $anInstanceIndicator->oModel->nom_tabla;
				
				$sql = "UPDATE ".$factTableName." SET ".$indicatorField." = 0 WHERE ".$indicatorField." IS NULL";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				//Obtenemos el Id del indicador creado y lo almacenamos en la tabla SI_SV_QAnswers
				$this->QIndicatorIds[$optionKey] = $anInstanceIndicator->IndicatorID;
				
				$sql = "UPDATE SI_SV_QAnswers SET IndicatorID = ".$this->QIndicatorIds[$optionKey]." 
						WHERE QuestionID = ".$this->QuestionID." AND SortOrder = ".$optionKey;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}

	//@JAPR 2015-05-13: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
	//estaban limitadas a 255 caracteres (#BA7866)
	/* Crea el campo dim_#### dentro de la tabla paralela según el tipo de pregunta de la que se trate */
	//@JAPRDescontinuada en v6
	function createSurveyDimField()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		$dataType = "VARCHAR(255)";
		
		switch ($this->QTypeID)
		{
			case qtpSingle:
			case qtpCallList:
				//Conchita 2013-04-29 agregado callList
				if($this->QDisplayMode <> dspMatrix)
				{
					$dataType = "VARCHAR(255)";
				}
				else 
				{
					//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
					$dataType = "LONGTEXT";
				}
				break;
			case qtpMulti;
				//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
				$dataType = "LONGTEXT";
				break;
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Hora
			case qtpOpenTime:
			case qtpOpenDate:
				$dataType = "VARCHAR(64)";
				break;
			case qtpOpenString:
				//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
				$dataType = "LONGTEXT";
				break;
			//@JAPR 2015-05-13: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
			//estaban limitadas a 255 caracteres (#BA7866)
			case qtpOpenAlpha:
				//@JAPR 2015-05-13: Corregido un bug, las preguntas abiertas alfanuméricas no creaban el campo del tamaño indicado, además
				//estaban limitadas a 255 caracteres (#BA7866)
				$intLength = $this->QLength;
				if (is_null($intLength) || !is_numeric($intLength) || (int) $intLength <= 0 || (int) $intLength > MAX_ALPHA_LENGTH) {
					if ((int) $intLength > MAX_ALPHA_LENGTH) {
						$intLength = MAX_ALPHA_LENGTH;
					}
					else {
						$intLength = DEFAULT_ALPHA_LENGTH;
					}
				}
				
				$dataType = "VARCHAR({$intLength})";
				//@JAPR
				break;
			//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
			case qtpGPS:
			//@AAL 06/05/2015: Agregado el tipo de pregunta BarCode
			case qtpBarCode:   
				$dataType = "VARCHAR(255)";
				break;
			case qtpShowValue:
			//@JAPR 2012-05-14: Agregados nuevos tipos de pregunta Foto y Action
			case qtpPhoto:
			case qtpDocument:
			case qtpSketch:
			case qtpAudio:
			case qtpVideo:
			case qtpSignature:
			//@JAPR 2012-11-21: Validado que las preguntas tipo mensaje y salto no generen campo en la tabla paralela ni dimensión
			case qtpMessage:
			case qtpMapped:
			case qtpSkipSection:
			//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
			case qtpMyLocation:
				$dataType = "VARCHAR(255)";
				break;
		}
		
		$sql = "ALTER TABLE ".$this->SurveyTable." ADD ".$this->SurveyField." ".$dataType;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Verificamos si la pregunta es de seleccion simple y con catalogo para poder establecer el valor de 1 (*NA)
		//porque en los joins del reporte este fallando dicho join porque no se le estaba aplicando un valor a dicho campo
		if($this->QTypeID==qtpSingle && $this->UseCatalog==1)
		{
			//Antes de establecer valor de no aplica al campo nuevo creado, debemos verificar si no hay otro campo
			//que haga referencia al mismo catalogo, si es asi se deberá igualar de ese campo al otro campo que hace 
			//referencia al mismo catalogo
			//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
			//encadenan como Padres/Hijas de otras preguntas Single Choice
			$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND CatalogID = ".$this->CatalogID." AND UseCatalog = 1 AND QuestionID <> ".$this->QuestionID." AND QTypeID <> ".qtpOpenNumeric;
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
	
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." "."SI_SV_Question"." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if(!$aRS->EOF)
			{
				$tmpQuestionID = (int)$aRS->fields["questionid"];
				$otherField = "dim_".$tmpQuestionID;
				
				$sql = "UPDATE ".$this->SurveyTable." SET ".$this->SurveyField." = ".$otherField." WHERE ".$this->SurveyField." IS NULL";
				
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			else 
			{
				$sql = "UPDATE ".$this->SurveyTable." SET ".$this->SurveyField." = 1 WHERE ".$this->SurveyField." IS NULL";
				
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			//Si se usarán catálogos desasociados de sus dimensiones, se tiene que crear el campo adicional para preguntas de catálogo
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			if (!is_null($aSurveyInstance)) {
				if ($aSurveyInstance->DissociateCatDimens) {
					//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
					$sql = "ALTER TABLE ".$this->SurveyTable." ADD ".$this->SurveyCatField." LONGTEXT";
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					$sql = "UPDATE ".$this->SurveyTable." SET ".$this->SurveyCatField." = ".$this->Repository->DataADOConnection->Quote("")." WHERE ".$this->SurveyCatField." IS NULL";
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
			//@JAPR
		}
		else if($this->QTypeID==qtpShowValue)
		{
			//Antes de establecer valor de no aplica al campo nuevo creado, debemos verificar si no hay otro campo
			//que haga referencia al mismo catalogo, si es asi se deberá igualar de ese campo al otro campo que hace 
			//referencia al mismo catalogo
			//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
			//encadenan como Padres/Hijas de otras preguntas Single Choice
			$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND CatalogID = ".$this->CatalogID." AND UseCatalog = 1 AND QuestionID <> ".$this->QuestionID." AND QTypeID <> ".qtpOpenNumeric;
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
	
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." "."SI_SV_Question"." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if(!$aRS->EOF)
			{
				$tmpQuestionID = (int)$aRS->fields["questionid"];
				$otherField = "dim_".$tmpQuestionID;
				
				$sql = "UPDATE ".$this->SurveyTable." SET ".$this->SurveyField." = ".$otherField." WHERE ".$this->SurveyField." IS NULL";
				
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			else 
			{
				$sql = "UPDATE ".$this->SurveyTable." SET ".$this->SurveyField." = 1 WHERE ".$this->SurveyField." IS NULL";
				
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}
	
	//@JAPRDescontinuada en v6
	function createCategoryDimension()
	{
		global $gblEFormsNA;
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		$strOriginalWD = getcwd();

		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");

		//Verificamos si quieren crear la dimension categoria
		if($this->UseCategoryDimChoice==-1)
		{
			global $generateXLS;
			$generateXLS = false;

			//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
			//Estas dimensiones nunca se programaron para ser reutilizadas, así que se dejará de esa manera, aunque por el tipo de pregunta, estas si
			//hubiera sido deseable compartir ya que son categorías diferentes (una por pregunta) agrupadas en un mismo lugar (dimensiones)
			//Insertamos la dimension Category Dimension
			$dimensionName = $this->CategoryDimName;
			$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->ModelID);
			$anInstanceModelDim->Dimension->Estatus = 1;
			$anInstanceModelDim->Dimension->ParentID = -1;
			$anInstanceModelDim->Dimension->IsIncremental = 0;
			$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
			$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
			$anInstanceModelDim->Dimension->UseKey = 0;
			//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica en el catalogo
				$this->insertNoApplyValue($anInstanceModelDim);
				
				//Insertamos el valor del Attribute Name como miembro de dimension
				$this->updateCategoryDimensionValues($anInstanceModelDim);
	
				//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
				$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
				$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
				
				$dimTableName = $anInstanceModelDim->Dimension->TableName;
				$dimFieldDesc = $anInstanceModelDim->Dimension->FieldDescription;
				
				//Obtenemos el ID con el que se inserto el valor de No Aplica para una dimension determinada
				$sql = "SELECT ".$dimFieldKey." FROM ".$dimTableName." WHERE ".$dimFieldDesc." = ".$this->Repository->DataADOConnection->Quote($gblEFormsNA);
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$noApplyKey = $aRS->fields[strtolower($dimFieldKey)];
				
				$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = ".$noApplyKey;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question.CategoryDimID
				$this->CategoryDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
				
				$sql = "UPDATE SI_SV_Question SET CategoryDimID = ".$this->CategoryDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			else {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding questions's category dimension").": ".$strMMError.". ";
			}
			
			global $generateXLS;
			$generateXLS = false;
			
			//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
			//Estas dimensiones nunca se programaron para ser reutilizadas, así que se dejará de esa manera, aunque por el tipo de pregunta, estas si
			//hubiera sido deseable compartir ya que son categorías diferentes (una por pregunta) agrupadas en un mismo lugar (dimensiones)
			//Insertamos la dimension Element Dimension
			$dimensionName = $this->ElementDimName;
			$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->ModelID);
			$anInstanceModelDim->Dimension->Estatus = 1;
			$anInstanceModelDim->Dimension->ParentID = -1;
			$anInstanceModelDim->Dimension->IsIncremental = 0;
			$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
			$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
			$anInstanceModelDim->Dimension->UseKey = 0;
			//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);

			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica en el catalogo
				$this->insertNoApplyValue($anInstanceModelDim);
				
				//Insertamos el valor del Attribute Name como miembro de dimension
				$this->updateElementDimensionValues($anInstanceModelDim);
	
				//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
				$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
				$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
				
				$dimTableName = $anInstanceModelDim->Dimension->TableName;
				$dimFieldDesc = $anInstanceModelDim->Dimension->FieldDescription;
				
				//Obtenemos el ID con el que se inserto el valor de No Aplica para una dimension determinada
				$sql = "SELECT ".$dimFieldKey." FROM ".$dimTableName." WHERE ".$dimFieldDesc." = ".$this->Repository->DataADOConnection->Quote($gblEFormsNA);
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$noApplyKey = $aRS->fields[strtolower($dimFieldKey)];
				
				$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = ".$noApplyKey;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question.CategoryDimID
				$this->ElementDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
				
				$sql = "UPDATE SI_SV_Question SET ElementDimID = ".$this->ElementDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			else {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding questions's element dimension").": ".$strMMError.". ";
			}
			
			$indicatorName = $this->ElementIndName;
			$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
			$anInstanceIndicator->ModelID = $this->ModelID;
			$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->ModelID);
			$anInstanceIndicator->IndicatorName = $indicatorName;
			$anInstanceIndicator->formato = "#,##0";
			$anInstanceIndicator->agrupador = "SUM";
			$anInstanceIndicator->sql_source = "SUM";
			
			global $generateXLS;
			$generateXLS = false;
			
			$anInstanceIndicator->save();
			chdir($strOriginalWD);
			
			//Procedemos a actualizar el indicador en la tabla de hechos
			$indicatorField = $anInstanceIndicator->field_name;
			$factTableName = $anInstanceIndicator->oModel->nom_tabla;
			
			$sql = "UPDATE ".$factTableName." SET ".$indicatorField." = 0 WHERE ".$indicatorField." IS NULL";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Obtenemos el Id del indicador creado y lo almacenamos en SI_SV_Question.ElementIndID
			$this->ElementIndID = $anInstanceIndicator->IndicatorID;
			
			$sql = "UPDATE SI_SV_Question SET ElementIndID = ".$this->ElementIndID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else 
		{
			$sql = "SELECT CategoryDimID, ElementDimID, ElementIndID 
				FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." 
					AND QTypeID = ".qtpMulti." AND QDisplayMode = ".dspVertical." AND MCInputType = ".mpcNumeric." AND IsMultiDimension <> 1 
					AND UseCategoryDim = 1 AND CategoryDimID = ".$this->UseCategoryDimChoice;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." "."SI_SV_Question"." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Se esperaria que existiera la dimension y por lo tanto no deberia regresar vacio el recordset en caso contrario entonces 
			//se procede a crear la dimension Category, Element y el indicador Element
			if(!$aRS->EOF)
			{
				$this->CategoryDimID = (int)$aRS->fields["categorydimid"];
				$this->ElementDimID = (int)$aRS->fields["elementdimid"];
				$this->ElementIndID = (int)$aRS->fields["elementindid"];
				
				//Generamos la instancia de la dimension Category para insertar el valor
				//de AttributeName como miembro de dimension
				//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
				$anInstanceModelDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->CategoryDimID);
				chdir($strOriginalWD);
				
				if (!is_null($anInstanceModelDim)) {
					//Insertamos el valor del Attribute Name como miembro de dimension
					$this->updateCategoryDimensionValues($anInstanceModelDim);
				
					$sql = "UPDATE SI_SV_Question SET CategoryDimID = ".$this->CategoryDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
				
				//Generamos la instancia de la dimension Element para insertar los valores de las opciones
				//como miembro de dimension
				//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
				$anInstanceModelDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->ElementDimID);
				chdir($strOriginalWD);
				
				if (!is_null($anInstanceModelDim)) {
					//Insertamos los valores de las opciones como miembro de dimension
					$this->updateElementDimensionValues($anInstanceModelDim);
					
					$sql = "UPDATE SI_SV_Question SET ElementDimID = ".$this->ElementDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
				
				//Actualizamos la propiedad ElementIndID con el valor que se recogio del recordset
				$sql = "UPDATE SI_SV_Question SET ElementIndID = ".$this->ElementIndID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			else 
			{
				$this->UseCategoryDimChoice = -1;
				$this->createCategoryDimension();
			}
		}
	}
	
	//@JAPRDescontinuada en v6
	function createCatalogDimension()
	{
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
		if (!is_null($aSurveyInstance)) {
			if ($aSurveyInstance->DissociateCatDimens) {
				//Si la encuesta utilizará catálogos con dimensiones independientes, invoca al nuevo método que asocia a dichas dimensiones a la
				//pregunta y al resto de los elementos
				$this->createDissociatedCatalogDimension();
				return;
			}
		}
		
		//Antes de crear la dimension catalogo verificamos si dicha dimension ya se encuentra presente
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
		$sql = "SELECT IndDimID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND CatalogID = ".$this->CatalogID." AND UseCatalog = 1 AND QuestionID <> ".$this->QuestionID." AND QTypeID <> ".qtpOpenNumeric;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." "."SI_SV_Question"." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Si ya existe dicho catalogo entonces no creamos la dimension solamente se crea el campo en la tabla paralela
		if(!$aRS->EOF)
		{
			$indDimID = (int)$aRS->fields["inddimid"];
			$this->IndDimID = $indDimID;
			
			$sql = "UPDATE SI_SV_Question SET IndDimID = ".$this->IndDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else 
		{
			$strOriginalWD = getcwd();
	
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			require_once("../model_manager/attribdimension.inc.php");
			
			//Obtener cla_descrip del catalogo
			$sql = "SELECT ParentID FROM SI_SV_Catalog WHERE CatalogID = ".$this->CatalogID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			$parentCladescrip = null;
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." "."SI_SV_Catalog"." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$parentCladescrip = (int)$aRS->fields["parentid"];
			}
			
			if(!is_null($parentCladescrip))
			{
				global $generateXLS;
				$generateXLS = false;
				
				//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
				//Esta dimensión siempre se reutiliza, ya que todas las preguntas del mismo catalogo apuntan a la misma dimensión catalogo (NO es la
				//dimensión desasociada de los catálogos, en este caso es el esquema original en que era una única jerarquía con dimensiones-atributo)
				
				//Insertamos la dimension dummy
				$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->ModelID);
				$anInstanceModelDim->Dimension->Estatus = 1;
				$anInstanceModelDim->Dimension->ParentID = -1;
				$anInstanceModelDim->Dimension->IsIncremental = 0;
				$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
				$anInstanceModelDim->Dimension->UseKey = 0;
				$anInstanceModelDim->Dimension->DimensionClaDescrip = $parentCladescrip;
				//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
				$objMMAnswer = @$anInstanceModelDim->save();
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError == '') {
					$this->IndDimID = $parentCladescrip;
					
					$sql = "UPDATE SI_SV_Question SET IndDimID = ".$this->IndDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
				else {
					$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding question's catalog dimension").": ".$strMMError.". ";
					//Si no pudo crear la dimensión, no tiene caso continuar con los atributos
					return;
				}
				
				//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
				$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
				$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
				
				$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = 1";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Insertamos los atributos
				$sql = "SELECT MemberOrder FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->CatalogID." AND MemberID = ".$this->CatMemberID;
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				$memberOrder = null;
				if (!$aRS)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else
				{
					if ($aRS->EOF) {
						die("(".__METHOD__.") ".translate("The specified attribute no longer exists in this catalog"));
					}
					$memberOrder = (int)$aRS->fields["memberorder"];
				}
				
				//$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->CatalogID." AND MemberOrder <= ".$memberOrder." ORDER BY MemberOrder";
				$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->CatalogID." ORDER BY MemberOrder";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					if ($aRS->EOF) {
						die("(".__METHOD__.") ".translate("The specified catalog is empty, the question must have an attribute assigned"));
					}
				}
				
				while(!$aRS->EOF)
				{
					$cla_descrip = (int)$aRS->fields["parentid"];
					global $generateXLS;
					$generateXLS = false;
					
					$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($this->Repository, $this->ModelID, $parentCladescrip);
					$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
					//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
					$objMMAnswer = @$anInstanceAttrib->Dimension->save();
					$strMMError = identifyModelManagerError($objMMAnswer);
					if ($strMMError != '') {
						$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding question's attribute dimension").": ".$strMMError.". ";
					}
					
					$aRS->MoveNext();
				}
			}
			
			chdir($strOriginalWD);
		}
	}

	//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//@JAPRDescontinuada en v6
	function createDissociatedCatalogDimension()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Primero obtiene la instancia del catálogo junto a sus atributos para asegurarse que está ya migrado al formato de dimensiones
		//independientes, puesto que sin eso no se puede continuar con el proceso
		$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		if (is_null($objCatalog)) {
			return;
		}
		
		$objCatalog->generateDissociatedDimensions();
		if (!$objCatalog->DissociateCatDimens) {
			die("(".__METHOD__.") ".translate("The selected catalog is not properly configured to support dissociated dimensions, please select another catalog"));
		}
		
		//Asocia al cubo todas las dimensiones de los atributos hasta encontrar al atributo de esta pregunta
		$indDimID = $objCatalog->addDissociatedCatalogDimensions($this->SurveyID, $this->CatMemberID);
		if ($indDimID > 0) {
			$this->IndDimID = $indDimID;
			$sql = "UPDATE SI_SV_Question SET IndDimID = ".$this->IndDimID." 
				WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	//@JAPR
	
	//@JAPRDescontinuada en v6
	function removeIndicatorKPI()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		$strOriginalWD = getcwd();
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		require_once("../model_manager/modeldimension.inc.php");

		$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($this->Repository, $this->IndicatorID);
	    if (!is_null($anInstanceIndicator)) {
			$anInstanceIndicator->remove();
	    }
		chdir($strOriginalWD);
		
		$sql = "UPDATE SI_SV_Question SET IndicatorID = NULL WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	//@JAPRDescontinuada en v6
	function createIndicatorKPI()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		$strOriginalWD = getcwd();

		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		
		$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
		$anInstanceIndicator->ModelID = $this->ModelID;
		$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->ModelID);
		$anInstanceIndicator->IndicatorName = $this->AttributeName;
		//$anInstanceIndicator->descripcion = $this->QuestionText;
		//@JAPR 2014-12-17: Corregido un bug, no estaba heredando el formato de la pregunta numérica
		if ((($this->QTypeID == qtpOpenNumeric || $this->QTypeID == qtpCalc) && $this->IsIndicatorNew == 1) ||
				($this->MCInputType == mpcNumeric && $this->IsIndicatorMCNew == 1)) {
			$anInstanceIndicator->formato = $this->FormatMask;
		}
		else {
			$anInstanceIndicator->formato = "#,##0";
		}
		//@JAPR
		$anInstanceIndicator->agrupador = "SUM";
		$anInstanceIndicator->sql_source = "SUM";
		global $generateXLS;
		$generateXLS = false;

		$anInstanceIndicator->save();
		
		//Procedemos a actualizar el indicador en la tabla de hechos
		$indicatorField = $anInstanceIndicator->field_name;
		$factTableName = $anInstanceIndicator->oModel->nom_tabla;
		
		$sql = "UPDATE ".$factTableName." SET ".$indicatorField." = 0 WHERE ".$indicatorField." IS NULL";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
		$this->IndicatorID = $anInstanceIndicator->IndicatorID;
		
		$sql = "UPDATE SI_SV_Question SET IndicatorID = ".$this->IndicatorID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		chdir($strOriginalWD);
	}
	
	//@JAPRDescontinuada en v6
	function createSurveyIndField()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		$dataType = "FLOAT";
		if($this->Repository->DataADOConnection->dataProvider=="mysql")
		{
			$dataType = "DOUBLE";
		}
		
		$sql = "ALTER TABLE ".$this->SurveyTable." ADD ".$this->SurveyField." ".$dataType." NULL";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	//@JAPRDescontinuada en v6
	function insertNoApplyValue($anInstanceModelDim)
	{
		global $gblEFormsNA;
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Procedemos a almacenar el valor de No Aplica en el Catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		$sql = "INSERT INTO ".$tableName." (".$fieldDescription.") VALUES (".$this->Repository->DataADOConnection->Quote($gblEFormsNA).")";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//@JAPR 2015-05-05: Corregida la reutilización de dimensiones para que no provoque errores
			//@JAPR 2015-05-05: Validado que no interrumpa la generación de los objetos en caso de error
			//die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	//@JAPRDescontinuada en v6
	function insertDimensionValues($anInstanceModelDim)
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Procedemos a almacenar los elementos en la tabla del catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		foreach ($this->SelectOptions as $dimensionValue)
		{
			$sql = "INSERT INTO ".$tableName." (".$fieldDescription.") VALUES (".$this->Repository->DataADOConnection->Quote($dimensionValue).")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//@JAPR 2014-11-04: Validado que no marque error al insertar valores en la dimensión, ya que en la práctica el único que podría
				//haber estado duplicado era el valor de NA que se agrega al momento de crear la dimensión, dificilmente podría haber un error por
				//permisos de escritura en la tabla para este producto, y aunque así fuera, eso no debería impedir asociar la dimensión a la pregunta
				//die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	
	//@JAPRDescontinuada en v6
	function insertValuesInDimOption($anInstanceModelDim)
	{
		global $gblEFormsNA;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Procedemos a almacenar los elementos en la tabla del catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldKey = $tableName."KEY";
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		//Se verifica que la tabla no tenga valores
		$sql = "SELECT ".$fieldDescription." FROM ".$tableName;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$arrayValues = array();
		$arrayValues[1] = $gblEFormsNA;
		
		//Si la pregunta es multidimension y ademas no sea campo de captura de numericos o string
		//entonces si se agregan los demas valores de 1 y 0
		if($this->MCInputType==mpcCheckBox)
		{
			$arrayValues[2] = "1";
			$arrayValues[3] = "0";
		}
		
		//Si no existen valores entonces se procede a insertarlos
		if($aRS->EOF)
		{
			foreach ($arrayValues as $dimensionKey=>$dimensionValue)
			{
				$sql = "INSERT INTO ".$tableName." (".$fieldKey.", ".$fieldDescription.") VALUES ( ".$dimensionKey.", ".$this->Repository->DataADOConnection->Quote($dimensionValue).")";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}
	
	//@JAPRDescontinuada en v6
	function insertValuesInDimMatrixOption($anInstanceModelDim, $arrayAnswerOptions)
	{
		global $gblEFormsNA;
		
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Procedemos a almacenar los elementos en la tabla del catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldKey = $tableName."KEY";
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		
		//Insertamos primero el  valor de no Aplica para dimension de opcion de matriz
		$dimensionKey = 1;
		$dimensionValue = $gblEFormsNA;
		$sql = "INSERT INTO ".$tableName." (".$fieldKey.", ".$fieldDescription.") VALUES ( ".$dimensionKey.", ".$this->Repository->DataADOConnection->Quote($dimensionValue).")";
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Insertamos las demas opciones
		$dimensionKey = 2;
		foreach ($arrayAnswerOptions as $dimensionValue)
		{
			$sql = "INSERT INTO ".$tableName." (".$fieldKey.", ".$fieldDescription.") VALUES ( ".$dimensionKey.", ".$this->Repository->DataADOConnection->Quote($dimensionValue).")";
			
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$dimensionKey++;
		}
	}
	
	//@JAPRDescontinuada en v6
	function updateDimensionValues($anInstanceModelDim)
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Procedemos a almacenar los elementos en la tabla del catalogo siempre y cuando no esten en dicho catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		foreach ($this->SelectOptions as $dimensionValue)
		{
			//Se verifica si el valor realmente existe
			$sql = "SELECT ".$fieldDescription." FROM ".$tableName." WHERE ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($dimensionValue);
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Si el valor no existe entonces se procede a insertarlo
			if($aRS->EOF)
			{
				$sql = "INSERT INTO ".$tableName." (".$fieldDescription.") VALUES (".$this->Repository->DataADOConnection->Quote($dimensionValue).")";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}
	
	//@JAPRDescontinuada en v6
	function updateCategoryDimensionValues($anInstanceModelDim)
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Procedemos a almacenar los elementos en la tabla del catalogo siempre y cuando no esten en dicho catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;

		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		//Se verifica si el valor realmente existe
		$sql = "SELECT ".$fieldDescription." FROM ".$tableName." WHERE ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($this->AttributeName);
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Si el valor no existe entonces se procede a insertarlo
		if($aRS->EOF)
		{
			$sql = "INSERT INTO ".$tableName." (".$fieldDescription.") VALUES (".$this->Repository->DataADOConnection->Quote($this->AttributeName).")";
			
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	
	//@JAPRDescontinuada en v6
	function updateElementDimensionValues($anInstanceModelDim)
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Procedemos a almacenar los elementos en la tabla del catalogo siempre y cuando no esten en dicho catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;

		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		foreach ($this->SelectOptions as $dimensionValue)
		{
			//Se verifica si el valor realmente existe
			$sql = "SELECT ".$fieldDescription." FROM ".$tableName." WHERE ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($dimensionValue);
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			//Si el valor no existe entonces se procede a insertarlo
			if($aRS->EOF)
			{
				$sql = "INSERT INTO ".$tableName." (".$fieldDescription.") VALUES (".$this->Repository->DataADOConnection->Quote($dimensionValue).")";
				
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}
	
	//@JAPRDescontinuada en v6
	function updateMassiveDimensionValues($anInstanceModelDim)
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Procedemos a almacenar las opciones en la tabla del catalogo siempre y cuando no esten en dicho catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldKey = $tableName."KEY";
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		$sql = "SELECT A.DisplayText, B.".$fieldKey." FROM SI_SV_QAnswers A LEFT JOIN ".$tableName." B 
				ON A.DisplayText = B.".$fieldDescription." WHERE A.QuestionID = ".$this->QuestionID." 
				GROUP BY A.DisplayText, B.".$fieldKey;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers,".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while(!$aRS->EOF)
		{
			$valueKey = $aRS->fields[strtolower($fieldKey)];
			$strOption = $aRS->fields["displaytext"];
			
			//Si el valor regresado es nulo o cero eso quiere decir que el valor no existe en la dimension
			if(is_null($valueKey) || $valueKey == 0)
			{
				$sql = "INSERT INTO ".$tableName." (".$fieldDescription.") VALUES (".$this->Repository->DataADOConnection->Quote($strOption).")";
				
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}

			$aRS->MoveNext();
		}
	}
	
	//@JAPR 2012-06-08: Ahora se permitirá cambiar la opción de usar Foto durante la edición de preguntas
	//Dependiendo de si se cambió o no la opción para usar foto, se agrega o remueve la dimensión correspondiente
	//@JAPRDescontinuada en v6
	function updateModelDimensionIMG()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		if ($this->HasReqPhoto > 0 && $this->HasReqPhotoOld <= 0)
		{
			//Se está configurando para que si tenga foto, así que crea la dimensión imagen, pero hay que validar que sea un tipo de pregunta
			//que si lo deba soportar
			$this->createModelDimensionIMG();
		}
		elseif ($this->HasReqPhoto <=0 && $this->HasReqPhotoOld > 0)
		{
			//Se está configurando para que no tenga foto, simplemente valida que si había una dimensión imagen entonces esta sea removida (se
			//valida dentro de la función)
			$this->removeModelDimensionIMG();
		}
		else 
		{
			//Cualquier otro caso o no cambió la configuración, o bien sólo cambió de ser foto opcional a requerida o visceversa, así que no
			//hay necesidad de agregar ni remover la dimensión de imagen
		}
	}
	//@JAPR
	
	//@JAPR 2014-09-25: Corregido un bug, no estaba volviendo a construir la dimensión documento en caso de no existir cuando se editaba
	//@JAPRDescontinuada en v6
	function updateModelDimensionDOC()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//@JAPR 2014-09-25: Corregido un bug, no estaba volviendo a construir la dimensión documento en caso de no existir cuando se editaba
		//Modificada la validación para incluir casos en los que no se había podido crear la dimensión, ya que esta propiedad no es configurable por
		//el usuario sino automática según el tipo de pregunta, así que una vez creada la pregunta nunca mas entraría por aquí
		if (($this->HasReqDocument > 0 && $this->HasReqDocumentOld <= 0) || ($this->HasReqDocument > 0 && $this->DocDimID <= 0))
		{
			//Se está configurando para que si tenga foto, así que crea la dimensión imagen, pero hay que validar que sea un tipo de pregunta
			//que si lo deba soportar
			$this->createModelDimensionDOC();
		}
		elseif ($this->HasReqDocument <=0 && $this->HasReqDocumentOld > 0)
		{
			//Se está configurando para que no tenga foto, simplemente valida que si había una dimensión imagen entonces esta sea removida (se
			//valida dentro de la función)
			$this->removeModelDimensionDOC();
		}
		else 
		{
			//Cualquier otro caso o no cambió la configuración, o bien sólo cambió de ser foto opcional a requerida o visceversa, así que no
			//hay necesidad de agregar ni remover la dimensión de imagen
		}
	}
	
	//@JAPRDescontinuada en v6
	function removeSelectOptionDims()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Antes de eliminar las opciones de la tabla SI_SV_QAnswers se procede a eliminar las dimensiones
		//para aquellas preguntas de seleccion multiple y que tenga activado el comportamiento MultiDimension
		//En este caso no entran las preguntas de tipo Category Dimension ya que no son Multidimension
		if($this->QTypeID==qtpMulti && $this->IsMultiDimension==1)
		{
			$strOriginalWD = getcwd();
			//Eliminar una dimension
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			require_once("../model_manager/modeldata.inc.php");

			foreach ($this->QIndDimIds as $indDimID)
			{
				$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $indDimID);
				//Vamos a eliminar la dimension aun cuando este siendo usada en un escenario por ello deshabilitamos la validacion
		        if (!is_null($anInstanceModelDim)) {
					$anInstanceModelDim->Dimension->CheckStages = false;
					$anInstanceModelDim->Dimension->remove();
		        }
			}

			chdir($strOriginalWD);
		}
	}
	
	//@JAPRDescontinuada en v6
	function removeMatrixSelectOptionDims()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Antes de eliminar las opciones de la tabla SI_SV_QAnswers se procede a eliminar las dimensiones
		//para aquellas preguntas de seleccion multiple y que tenga activido el comportamiento MultiDimension
		if($this->QTypeID==qtpSingle && $this->UseCatalog==0 && $this->QDisplayMode == dspMatrix)
		{
			$strOriginalWD = getcwd();
			//Eliminar una dimension
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			
			if($this->OneChoicePer==0)
			{
				$strQIndDimIds = "QIndDimIds";
			}
			else 
			{
				$strQIndDimIds = "QIndDimIdsCol";
			}

			foreach ($this->$strQIndDimIds as $indDimID)
			{
				$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $indDimID);
				//Vamos a eliminar la dimension aun cuando este siendo usada en un escenario por ello deshabilitamos la validacion
		        if (!is_null($anInstanceModelDim)) {
					$anInstanceModelDim->Dimension->CheckStages = false;
					$anInstanceModelDim->Dimension->remove();
		        }
			}

			chdir($strOriginalWD);
		}
	}
	
	//@JAPRDescontinuada en v6
	function removeSelectOptionInds()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Antes de eliminar las opciones de la tabla SI_SV_QAnswers se procede a eliminar los indicadores
		//para aquellas preguntas de seleccion multiple y que tenga activado el comportamiento MultiDimension-Number y que es Indicador
		//Pareciera que no hay para las preguntas tipo Category Dimension ya que no tienen valor los elementos del arreglo ->QIndicatorIds
		if(($this->QTypeID==qtpMulti && $this->IsMultiDimension==1 && $this->MCInputType==mpcNumeric && $this->IsIndicatorMC==1)
			|| $this->QTypeID==qtpMulti)
		{
			$strOriginalWD = getcwd();
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager

			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/model.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			require_once("../model_manager/indicatorkpi.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			
			foreach ($this->QIndicatorIds as $indicatorID)
			{
				if(!is_null($indicatorID) && $indicatorID>0)
				{
					$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($this->Repository, $indicatorID);
			        if (!is_null($anInstanceIndicator)) {
						$anInstanceIndicator->remove();
			        }
					
					//Eliminar la referencia del indicador
					$sql = "UPDATE SI_SV_QAnswers SET IndicatorID = NULL WHERE QuestionID = ".$this->QuestionID." AND IndicatorID = ".$indicatorID;
			
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}

			chdir($strOriginalWD);
		}
	}
	
	//@JAPR 2013-12-04: Agregada la opción para compartir las opciones de respuesta con otras preguntas
	//Agregado el parámetro $bKeepSharedQuestionData para indicar que no se elimine la información de preguntas compartidas, de tal forma que
	//en el método que invoca a este remove se pueda reconstruir una vez que se termine de grabar nuevamente la pregunta (utilizado exclusivamente
	//dentro del método saveNewAfterRemove)
	function remove($bKeepSharedQuestionData = false)
	{
		//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, las dimensiones ya no aplican
		if ($this->CreationAdminVersion < esveFormsv6) {
			//Antes de eliminar las opciones se deben eliminar las dimensiones asociadas a las opciones
			//cuando la pregunta sea de tipo seleccion multiple
			$this->removeSelectOptionDims();
			
			//Antes de eliminar las opciones se deben eliminar los indicadores asociados a las opciones
			//cuando la pregunta sea de tipo seleccion multiple numerica con indicador creado
			$this->removeSelectOptionInds();
			
			//Antes de eliminar las opciones de una matriz se deben eliminar las dimensiones asociadas a las opciones
			//cuando la pregunta sea de tipo seleccion simple sin catalogo despliegue tipo matriz
			$this->removeMatrixSelectOptionDims();
		}
		
		//Eliminamos relaciones padre-hija de campos calculados
		$this->removeQuestionsToEval();
		$this->removeFormulaChildren();
		$this->removeShowConditionChildren();

		//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, la estructura de la tabla cambia
		if($this->CreationAdminVersion < esveFormsv6){
			//Se eliminan las referencias de imagenes por pregunta
			$sql = "DELETE FROM SI_SV_SurveyAnswerImage WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			//Se eliminan los comentarios por pregunta
			$sql = "DELETE FROM SI_SV_SurveyAnswerComment WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Se eliminan las acciones por pregunta
		$sql = "DELETE FROM SI_SV_SurveyQuestionActions WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Se eliminan las acciones por opcion de pregunta
		$sql = "DELETE FROM SVSurveyActions WHERE SurveyID = ".$this->SurveyID." AND QuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Se eliminan las acciones de las opciones de la pregunta si es que aplica este caso
		$sql = "DELETE FROM SI_SV_QOptionActions WHERE QuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QOptionActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Eliminamos los ShowQuestions de la pregunta que se esta eliminando
		$sql = "DELETE FROM SI_SV_ShowQuestions WHERE QuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2013-02-14: Corregido un bug, no estaba eliminando la referencia del ShowQuestions cuando esta es la pregunta a mostrar (#27519)
		$sql = "DELETE FROM SI_SV_ShowQuestions WHERE ShowQuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Se eliminan los mapeos a eBavel de las opciones de las pregunta de la encuesta si es q tienen
		$sql = "DELETE FROM SI_SV_SurveyAnswerActionFieldsMap WHERE QuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2014-12-02: Corregido un bug, no se estaba eliminando el mapeo cuando la pregunta es el dato mapeado
		$sql = "DELETE FROM SI_SV_SurveyAnswerActionFieldsMap WHERE SurveyFieldID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2015-12-08: Agregado el borrado de la tabla de Destinos de datos
		$sql = "DELETE FROM SI_SV_DataDestinationsMapping WHERE id_question = ".$this->QuestionID;
		$this->Repository->DataADOConnection->Execute($sql);
		
		//@JAPR 2015-12-08: Agregado el borrado de la tabla de Mapeos de modelos
		$sql = "DELETE FROM SI_SV_ModelArtus_Mapping WHERE id_question = ".$this->QuestionID;
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql = "DELETE FROM SI_SV_ModelArtus_Questions WHERE id_question = ".$this->QuestionID;
		$this->Repository->DataADOConnection->Execute($sql);
		//@JAPR
		
		//@JAPR 2013-12-04: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		if (getMDVersion() >= esvSimpleChoiceSharing) {
			if (!$bKeepSharedQuestionData) {
				//Se eliminan los mapeos de opciones con preguntas compartidas
				if ($this->SharedQuestionID > 0) {
					$sql = "DELETE FROM SI_SV_QAnswersMatch WHERE SharedQuestionID = ".$this->QuestionID;
				}
				else {
					$sql = "DELETE FROM SI_SV_QAnswersMatch WHERE SourceQuestionID = ".$this->QuestionID;
				}
				$this->Repository->DataADOConnection->Execute($sql);
				
				if ($this->SharedQuestionID <= 0) {
					//Elimina además el mapeo de pregunta compartida si se está eliminando la pregunta origen
					$sql = "UPDATE SI_SV_Question SET SharedQuestionID = 0 WHERE SharedQuestionID = ".$this->QuestionID;
					$this->Repository->DataADOConnection->Execute($sql);
				}
			}
		}
		//@JAPR
		
		//Se eliminan las opciones de la pregunta si es q tiene
		$sql = "DELETE FROM SI_SV_QAnswers WHERE QuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2011-06-29: Había faltado eliminar las opciones de columnas
		$sql = "DELETE FROM SI_SV_QAnswersCol WHERE QuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		//Antes de realizar la eliminacion de la pregunta y la modificacion de los QuestionNumber se debe considerar la jerarquia 
		//si es que va a ser afectada para las preguntas de seleccion simple que utilicen catalogo
		if($this->QTypeID==qtpSingle && $this->UseCatalog==1)
		{
			$this->updateQuestionHierarchyDueToRemove();
		}

		//Antes de eliminar la pregunta actualizamos los QuestionNumbers
		//Ya no obtenemos el QuestionNumber desde la instancia ya que puede venir desde una colección y si hay
		//un recorrido de la coleccion para eliminar cada uno de los elementos dichos elementos no estaran actualizados
		//con su QuestionNumber actual ya que en esta funcion remove se hace la actualización de este campo y mas sin embargo
		//los elementos de la colección se quedaron desactualizados
		
	    $sqlNumber =  "SELECT QuestionNumber FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND QuestionID = ".$this->QuestionID;
		$aRS = $this->Repository->DataADOConnection->Execute($sqlNumber);
		if (!$aRS || $aRS->EOF)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlNumber);
		}

		$questionNumber = (int)$aRS->fields["questionnumber"] + 1;

		$sqlUpd = "UPDATE SI_SV_Question SET QuestionNumber = QuestionNumber - 1 WHERE SurveyID = ".$this->SurveyID." AND QuestionNumber >= ".$questionNumber;
		if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
		}
		
		//@JAPR 2013-02-19: Movido justo antes del borrado de la pregunta para que no se quede basura si habilitaran el die, aunque tentativamente
		//por ahora deshabilitará el die para no interrumpir el proceso pues solo debería haber errores por falta de tablas, no por armado de sentencias
		//13Feb2013: Si es pregunta simple choice que usa catálogo, se verifica si la encuensta está en alguna agenda que utilice el catálogo y atributo de la pregunta
		//y si es así quita la encuesta de esas agendas
		if($this->QTypeID==qtpSingle && $this->UseCatalog)
		{
			
			$sql= "SELECT t1.AgendaID 
				FROM SI_SV_SurveyAgenda t1, SI_SV_SurveyAgendaDet t2
				WHERE t1.AgendaID = t2.AgendaID
					AND t1.CatalogID=".$this->CatalogID." AND t1.AttributeID=".$this->CatMemberID." AND SurveyID=".$this->SurveyID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS !== false)
			{
				//IDs de agendas que utilizan el catálogo y atributo de la pregunta Simple Choice y tienen asociadas la encuesta
				$arrayAgendasIDs=array();
				
				while (!$aRS->EOF)
				{
					$arrayAgendasIDs[] = (int) $aRS->fields["agendaid"];
				
					$aRS->MoveNext();
				}
				
				//@JAPR 2013-02-19: Corregido un bug, si no hay agendas no se debe realizar el DELETE
				if (count($arrayAgendasIDs) > 0) {
					$sql = "DELETE FROM SI_SV_SurveyAgendaDet WHERE SurveyID=".$this->SurveyID." AND AgendaID IN (".implode(",", $arrayAgendasIDs).")";
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						//die( translate("Error accessing")." SI_SV_SurveyAgendaDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
			//@JAPR
		}
		
		//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
		$sql = "DELETE FROM SI_SV_SurveyQuestionActionFieldsMap WHERE QuestionID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2014-12-02: Corregido un bug, no se estaba eliminando el mapeo cuando la pregunta es el dato mapeado
		$sql = "DELETE FROM SI_SV_SurveyQuestionActionFieldsMap WHERE SurveyFieldID = ".$this->QuestionID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		//@JAPR 2015-07-15: Rediseñadas las preguntas y secciones de catálogo para permitir seleccionar los atributos a usar y el orden deseado (uso de DataSources)
		$sql = "DELETE FROM SI_SV_QuestionMembers WHERE QuestionID = ".$this->QuestionID;
		$this->Repository->DataADOConnection->Execute($sql);
		
		//JAPR 2016-04-15: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
		if (getMDVersion() >= esvSimpleChoiceCatOpts) {
			$sql = "DELETE FROM SI_SV_SkipRules WHERE QuestionID = ".$this->QuestionID;
			$this->Repository->DataADOConnection->Execute($sql);
		}
		
		//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, el borrado de las tablas paralelas cambia, puesto que las tablas son diferentes.
		if ($this->CreationAdminVersion >= esveFormsv6) {
			//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData, se requiere un Join con el DataSource
			//Elimina los filtros dinámicos creados a partir de las preguntas que son tipo GetData, como ahora los catálogos son únicos por pregunta, es seguro hacer este proceso
			$sql = "DELETE FROM SI_SV_SurveyFilter WHERE CatalogID = ".$this->CatalogID;
			$this->Repository->DataADOConnection->Execute($sql);
			
			//@JAPR 2015-07-26: Agregada la eliminación de los catálogos internos de las preguntas cuando se elimina la pregunta o el DataSource
			//@JAPRWarning: Mejorar este proceso, borrar si hay CatalogID > 0 y no se usa en otra pregunta/sección/Agenda/Scheduler de Agenda
			if ($this->ValuesSourceType == tofCatalog && $this->CatalogID > 0) {
				$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
				if (!is_null($objCatalog)) {
					$objCatalog->remove();
				}
				
				//En caso de que el proceso no terminara completo, se actualiza la definición de la pregunta para que no quede inconsistente
				$sql = "UPDATE SI_SV_Question SET 
						CatalogID = 0 
						, CatMemberID = 0 
						, DataSourceID = 0 
						, DataSourceMemberID = 0 
					WHERE QuestionID = {$this->QuestionID}";
				$this->Repository->DataADOConnection->Execute($sql);
			}
			//@JAPR
		
			//@AAL Se eliminan las tablas correspondientes a las Preguntas, Imagenes, Comentarios, Puntos(Score), Detalles.
			$sql = "DROP TABLE {$this->QuestionImgTable}";
			$this->Repository->DataADOConnection->Execute($sql);
			$sql = "DROP TABLE {$this->QuestionCommTable}";
			$this->Repository->DataADOConnection->Execute($sql);
			$sql = "DROP TABLE {$this->QuestionDocTable}";
			$this->Repository->DataADOConnection->Execute($sql);
			$sql = "DROP TABLE {$this->QuestionPtsTable}";
			$this->Repository->DataADOConnection->Execute($sql);
			$sql = "DROP TABLE {$this->QuestionDetTable}";
			$this->Repository->DataADOConnection->Execute($sql);
			$sql = "DROP TABLE {$this->QuestionValTable}";
			$this->Repository->DataADOConnection->Execute($sql);
			$sql = "DROP TABLE {$this->QuestionTable}";
			$this->Repository->DataADOConnection->Execute($sql);
			
			//Elimina los campos agregados en la tabla de la sección
			if ($this->SectionType != sectFormatted) {
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->DescKeyFieldFK}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->KeyFieldCommFK}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->KeyFieldDocFK}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->KeyFieldImgFK}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->KeyFieldPtsFK}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN QuestionLat_{$this->QuestionID}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN QuestionLong_{$this->QuestionID}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN QuestionAcc_{$this->QuestionID}";
				$this->Repository->DataADOConnection->Execute($sql);
				
				//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->DescFieldImg}Src_{$this->QuestionID}";
				$this->Repository->DataADOConnection->Execute($sql);
				//@JAPR
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->DescFieldImg}_{$this->QuestionID}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->DescFieldComm}_{$this->QuestionID}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->DescFieldDoc}_{$this->QuestionID}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->DescFieldPts}_{$this->QuestionID}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->ValueField}";
				$this->Repository->DataADOConnection->Execute($sql);
				$sql = "ALTER TABLE {$this->SectionTable} DROP COLUMN {$this->DescFieldAttr}";
				$this->Repository->DataADOConnection->Execute($sql);
			}
		}
		else {
			//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($this->QTypeID==qtpSingle && $this->UseCatalog==1) {
				$sql = "ALTER TABLE ".$this->SurveyTable." DROP COLUMN ".$this->SurveyCatField;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					//die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			//@JAPR
			
			//Por el momento si se mantiene el campo de la tabla paralela para las preguntas tipo matriz para que no falle la clase Report
			//ya que accede al campo aunq no realiza nada con el ya que se trae los datos de la tabla svsurveymatrixdata_<modelid>
			//Tambien se crea este campo para las categorydimension, pero en la exportacion a excel de la clase Report hace
			//referencia a dicho campo, solo hay q modificarlo para que se comporte semejante a como se hace en la matrix
			$sql = "ALTER TABLE ".$this->SurveyTable." DROP COLUMN ".$this->SurveyField;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//@AAL 27/05/2015 Agregado condición para EformsV6 y siguientes, el borrado de dimensiones e indicadores ya no aplican
			if($this->QTypeID==qtpSingle && $this->UseCatalog==0 && $this->QDisplayMode == dspMatrix)
			{
				$sql = "DELETE FROM ".$this->SurveyMatrixTable." WHERE QuestionID = ".$this->QuestionID;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					//die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
			//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
			//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
			if ($this->QTypeID == qtpGPS || $this->QTypeID == qtpMyLocation) {
				//Debe remover no sólo la dimensión de la pregunta sino sus atributos asociados
				$this->removeGPSDimension();
			}
			elseif($this->QTypeID==qtpOpenNumeric || $this->QTypeID==qtpCalc) {
				$strOriginalWD = getcwd();
				
				//Eliminar una dimension
				//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
				//para asignarlos a las variables de session que se ocupa en el model manager
				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				//session_register("BITAM_UserID");
				//session_register("BITAM_UserName");
				//session_register("BITAM_RepositoryName");
				$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
				$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
				$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
				
				require_once("../model_manager/filescreator.inc.php");
				require_once("../model_manager/modeldimension.inc.php");
				require_once("../model_manager/modeldata.inc.php");
				
				//@JAPR 2012-10-19: Validado en caso de no existir mas la dimensión especificada
				$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->IndDimID);
				if (!is_null($anInstanceModelDim)) {
					//Vamos a eliminar la dimension aun cuando este siendo usada en un escenario por ello deshabilitamos la validacion
					$anInstanceModelDim->Dimension->CheckStages = false;
					$anInstanceModelDim->Dimension->remove();
				}
				//@JAPR
				
				chdir($strOriginalWD);
				
				if($this->IsIndicator==1)
				{
					//Eliminar un indicador
					//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
					//para asignarlos a las variables de session que se ocupa en el model manager
					//@JAPR 2015-02-09: Agregado soporte para php 5.6
					//session_register("BITAM_UserID");
					//session_register("BITAM_UserName");
					//session_register("BITAM_RepositoryName");
					$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
					$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
					$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
					
					require_once("../model_manager/filescreator.inc.php");
					require_once("../model_manager/model.inc.php");
					require_once("../model_manager/modeldata.inc.php");
					require_once("../model_manager/indicatorkpi.inc.php");
					require_once("../model_manager/modeldimension.inc.php");
					
					$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($this->Repository, $this->IndicatorID);
			        if (!is_null($anInstanceIndicator)) {
						$anInstanceIndicator->remove();
			        }
				}
			}
			else {
				//@JAPR 2012-05-15: Agregados nuevos tipos de pregunta Foto y Action
				//@JAPR 2013-04-12: Agregados nuevos tipos de pregunta Sincronización
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				//Cambiado a un switch para mayor facilidad
				$blnValidQuestion = true;
				switch ($this->QTypeID) {
					case qtpShowValue:
					case qtpPhoto:
					//@JAPR 2014-10-02: Corregido un bug, las preguntas audio, video y documento (y probablemente algunas mas) estaban creando la dimensión
					//respuesta, así que ahora al eliminar las preguntas se validará si realmente viene o no asignada dicha dimensión antes de hacerlo, pero
					//por compatibilidad hacia atrás se intentará eliminar dicha dimensión para estas preguntas (ya en esta versión no la va a crear)
					//case qtpDocument:
					case qtpSignature:
					case qtpSketch:
					case qtpSkipSection:
					case qtpMessage:
					case qtpMapped:
					case qtpSync:
					case qtpPassword:
					//@JAPR 2014-10-02: Corregido un bug, las preguntas audio, video y documento (y probablemente algunas mas) estaban creando la dimensión
					//respuesta, así que ahora al eliminar las preguntas se validará si realmente viene o no asignada dicha dimensión antes de hacerlo, pero
					//por compatibilidad hacia atrás se intentará eliminar dicha dimensión para estas preguntas (ya en esta versión no la va a crear)
					//case qtpAudio:
					//case qtpVideo:
					case qtpSection:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValidQuestion = false;
						break;
				}
				
				//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
				if ($blnValidQuestion)
				{
					if($this->QTypeID!=qtpMulti)
					{
						$blnRemove = true;
						
						//Antes de eliminar la dimension vamos a verificar si es de seleccion simple y que utilice catalogo
						//y que dicho catalogo se utilice en más de una pregunta de la encuesta, si es asi, no se puede
						//eliminar el catalogo
						if($this->QTypeID==qtpSingle && $this->UseCatalog==1)
						{
							//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
							//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
							//encadenan como Padres/Hijas de otras preguntas Single Choice
							$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND CatalogID = ".$this->CatalogID." AND UseCatalog = 1 AND QuestionID <> ".$this->QuestionID." AND QTypeID <> ".qtpOpenNumeric;
							$aRS = $this->Repository->DataADOConnection->Execute($sql);
							if (!$aRS)
							{
								die("(".__METHOD__.") ".translate("Error accessing")." "."SI_SV_Question"." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							
							//Si no es fin del recordset entonces no se debe remover la dimension porque existe para otras preguntas
							if(!$aRS->EOF)
							{
								$blnRemove = false;
							}
						}
						
						//Verificamos si la pregunta es de seleccion simple simple sin catalogo y de despliegue matriz para no realizar eliminacion
						//de dimension ya que se realizo anteriormente para todas las opciones de dicha matriz
						if($this->QTypeID==qtpSingle && $this->UseCatalog==0 && $this->QDisplayMode == dspMatrix)
						{
							$blnRemove = false;
						}
						
						//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
						//para asignarlos a las variables de session que se ocupa en el model manager
						//@JAPR 2015-02-09: Agregado soporte para php 5.6
						//session_register("BITAM_UserID");
						//session_register("BITAM_UserName");
						//session_register("BITAM_RepositoryName");
						$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
						$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
						$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
						
						if($blnRemove)
						{
							//Si la dimension es de seleccion simple y catalogo
							//se deben eliminar primero los atributos
							//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
							$blnUseDissociatedCatDimens = false;
							if($this->QTypeID==qtpSingle && $this->UseCatalog==1)
							{
								//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
								$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
								if (!is_null($aSurveyInstance)) {
									$blnUseDissociatedCatDimens = ($aSurveyInstance->DissociateCatDimens == 1);
								}
								
								//Este método se encargará de remover a las dimensiones desasociadas, como para ejecutarlo significaría que esta era
								//la última pregunta de este catálogo que usa la encuesta, es seguro en ese punto remover las dimensiones del modelo
								//pero se debe validar mas abajo que ya no remueva la dimensión de esta pregunta directamente, pues ya no existíría
								//(si la encuesta no usa catálogo desasociados, entonces el código anterior funciona de la misma manera)
								$this->removeAllAttribDimension($blnUseDissociatedCatDimens);
								//@JAPR
							}
							
							//Eliminar una dimension
							require_once("../model_manager/filescreator.inc.php");
							require_once("../model_manager/modeldimension.inc.php");
							require_once("../model_manager/modeldata.inc.php");
							
							//@JAPR 2012-10-19: Validado en caso de no existir mas la dimensión especificada
							//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
							//Si se manejan catálogo desasociados, ya no se necesita remover la dimensión de la pregunta porque el método que remueve
							//los atributos ya se encargó de hacerlo
							//@JAPR 2014-10-02: Corregido un bug, las preguntas audio, video y documento (y probablemente algunas mas) estaban creando la dimensión
							//respuesta, así que ahora al eliminar las preguntas se validará si realmente viene o no asignada dicha dimensión antes de hacerlo
							if (!$blnUseDissociatedCatDimens && $this->IndDimID > 0) {
								$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->IndDimID);
								if (!is_null($anInstanceModelDim)) {
									//Vamos a eliminar la dimension aun cuando este siendo usada en un escenario por ello deshabilitamos la validacion
									$anInstanceModelDim->Dimension->CheckStages = false;
									
									if($this->QTypeID==qtpSingle && $this->UseCatalog==1)
									{
										//Establecemos por default que se trata de un catalogo para q no elimine los atributos y dimension principal
										//de la tabla si_descrip_enc ya que si_sv_catalog y si_sv_catalogmember hace referencia a ellos
										$anInstanceModelDim->Dimension->IsCatalog = true;
									}
									$anInstanceModelDim->Dimension->remove();
								}
							}
							
							//@JAPR 2012-05-15: Agregadas las preguntas tipo Action
							//Las preguntas tipo Action entran a esta parte ya que sólo contienen la dimensión para el valor del Action capturado
							//aunque faltaría validar si se debe o no eliminar la acción correspondiente
							if ($this->QTypeID == qtpAction)
							{
								//@JAPRWarning: Eliminar la acción correspondiente con las acciones generadas por esta pregunta ?
							}
						}
						
						//Verificamos si la pregunta es de tipo Simple Choice y si tiene indicador definido
						//por algun Score que se haya dado de alta en las opciones
						//Conchita 2013-04-29 Agregado el tipo callList (revisar con Juan si aqui es correcto**)
						if(($this->QTypeID==qtpCallList || $this->QTypeID==qtpSingle) && $this->UseCatalog==0 && $this->IndicatorID>0 && $this->QDisplayMode!=dspMatrix)
						{
							//Eliminar un indicador
							require_once("../model_manager/filescreator.inc.php");
							require_once("../model_manager/model.inc.php");
							require_once("../model_manager/modeldata.inc.php");
							require_once("../model_manager/indicatorkpi.inc.php");
							require_once("../model_manager/modeldimension.inc.php");
							
							$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($this->Repository, $this->IndicatorID);
				            if (!is_null($anInstanceIndicator)) {
								$anInstanceIndicator->remove();
				            }
						}
					}
					else 
					{
						//Si la pregunta Multiple Choice pertenece a una seccion dinamica debemos preguntar 
						$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
						
						//Verificamos antes si la pregunta multiple choice no sea de una
						//seccion dinamica dentro de la seccion
						//if($this->SectionID==$dynamicSectionID && $this->OutsideOfSection==0)
						if($this->SectionID==$dynamicSectionID)
						{
							//Eliminar una dimension
							//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
							//para asignarlos a las variables de session que se ocupa en el model manager
							//@JAPR 2015-02-09: Agregado soporte para php 5.6
							//session_register("BITAM_UserID");
							//session_register("BITAM_UserName");
							//session_register("BITAM_RepositoryName");
							$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
							$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
							$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
							
							require_once("../model_manager/filescreator.inc.php");
							require_once("../model_manager/modeldimension.inc.php");
							require_once("../model_manager/modeldata.inc.php");
							
							//@JAPR 2012-10-19: Validado en caso de no existir mas la dimensión especificada
							$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->IndDimID);
							if (!is_null($anInstanceModelDim)) {
								//Vamos a eliminar la dimension aun cuando este siendo usada en un escenario por ello deshabilitamos la validacion
								$anInstanceModelDim->Dimension->CheckStages = false;
								$anInstanceModelDim->Dimension->remove();
							}
							//@JAPR
							
							//Si es MCInputType = mpcNumeric quiere decir q es number y ademas 
							//requiere q se elimine el indicador
							if($this->MCInputType==mpcNumeric && $this->IsIndicatorMC==1)
							{
								//Eliminar un indicador
								//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
								//para asignarlos a las variables de session que se ocupa en el model manager
								//@JAPR 2015-02-09: Agregado soporte para php 5.6
								//session_register("BITAM_UserID");
								//session_register("BITAM_UserName");
								//session_register("BITAM_RepositoryName");
								$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
								$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
								$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
								
								require_once("../model_manager/filescreator.inc.php");
								require_once("../model_manager/model.inc.php");
								require_once("../model_manager/modeldata.inc.php");
								require_once("../model_manager/indicatorkpi.inc.php");
								require_once("../model_manager/modeldimension.inc.php");
								
								$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($this->Repository, $this->IndicatorID);
					            if (!is_null($anInstanceIndicator)) {
									$anInstanceIndicator->remove();
					            }
							}
						}
						
						//Para las preguntas category dimension no se van a eliminar sus dimensiones e indicador asociados
					}
				}
			}
			
			//Borramos la dimension de la imagen si es que existe
			if(!is_null($this->ImgDimID) && $this->ImgDimID>0)
			{
				$strOriginalWD = getcwd();
				
				//Eliminar una dimension
				//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
				//para asignarlos a las variables de session que se ocupa en el model manager
				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				//session_register("BITAM_UserID");
				//session_register("BITAM_UserName");
				//session_register("BITAM_RepositoryName");
				$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
				$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
				$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
				require_once("../model_manager/filescreator.inc.php");
				require_once("../model_manager/modeldimension.inc.php");
				require_once("../model_manager/modeldata.inc.php");
				
				//@JAPR 2012-10-19: Validado en caso de no existir mas la dimensión especificada
				$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->ImgDimID);
				if (!is_null($anInstanceModelDim)) {
					//Vamos a eliminar la dimension aun cuando este siendo usada en un escenario por ello deshabilitamos la validacion
					$anInstanceModelDim->Dimension->CheckStages = false;
					$anInstanceModelDim->Dimension->remove();
				}
				//@JAPR
				
				chdir($strOriginalWD);
			}

			//Borramos la dimension del documento si es que existe
			if(!is_null($this->DocDimID) && $this->DocDimID>0)
			{
				$strOriginalWD = getcwd();
				
				//Eliminar una dimension
				//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
				//para asignarlos a las variables de session que se ocupa en el model manager
				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				//session_register("BITAM_UserID");
				//session_register("BITAM_UserName");
				//session_register("BITAM_RepositoryName");
				$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
				$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
				$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
				require_once("../model_manager/filescreator.inc.php");
				require_once("../model_manager/modeldimension.inc.php");
				require_once("../model_manager/modeldata.inc.php");
				
				//@JAPR 2012-10-19: Validado en caso de no existir mas la dimensión especificada
				$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->DocDimID);
				if (!is_null($anInstanceModelDim)) {
					//Vamos a eliminar la dimension aun cuando este siendo usada en un escenario por ello deshabilitamos la validacion
					$anInstanceModelDim->Dimension->CheckStages = false;
					$anInstanceModelDim->Dimension->remove();
				}
				//@JAPR
				
				chdir($strOriginalWD);
			}
		}
		
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$sql = "UPDATE SI_SV_Section SET XPosQuestionID = 0 WHERE XPosQuestionID = ".$this->QuestionID;	
		if($this->Repository->DataADOConnection->Execute($sql) == false){
			//translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		}
		$sql = "UPDATE SI_SV_Section SET YPosQuestionID = 0 WHERE YPosQuestionID = ".$this->QuestionID;	
		if($this->Repository->DataADOConnection->Execute($sql) == false){
			//translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		}
		$sql = "UPDATE SI_SV_Section SET ItemNameQuestionID = 0 WHERE ItemNameQuestionID = ".$this->QuestionID;	
		if($this->Repository->DataADOConnection->Execute($sql) == false){
			//translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		}
		
		//OMMC 2015-11-26: Se elimina la referencia de la pregunta selector hacia la sección, si la pregunta selector se eliminaba el ID de la pregunta se conservaba.
		$sql = "UPDATE SI_SV_Section SET SelectorQuestionID = 0 WHERE SelectorQuestionID = ".$this->QuestionID;	
		if($this->Repository->DataADOConnection->Execute($sql) == false){
			//translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		}
		
		//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		$sql = "UPDATE SI_SV_Survey SET QuestionIDForEntryDesc = 0 WHERE QuestionIDForEntryDesc = ".$this->QuestionID;	
		$this->Repository->DataADOConnection->Execute($sql);
		//@JAPR
		
		$sql = "DELETE FROM SI_SV_Question WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Deleting question: {$sql}", 2, 0, "color:blue;");
		}
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Actualizar datos de modificacion de la encuesta
		BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
		
		return $this;
	}
	
	//@JAPRDescontinuada en v6
	function removeAllAttribDimension($bUseDissociatedCatDimens)
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$strAdditionalFields = '';
		if (getMDVersion() >= esvCatalogDissociation && $bUseDissociatedCatDimens) {
			$strAdditionalFields .= ', A.IndDimID ';
		}
		$sql = "SELECT A.ParentID AS ClaDescrip, A.MemberName AS MemberName, B.ParentID AS ClaDescripParent $strAdditionalFields 
			FROM SI_SV_CatalogMember A, SI_SV_Catalog B 
			WHERE A.CatalogID = B.CatalogID AND A.CatalogID = ".$this->CatalogID." 
			ORDER BY A.MemberOrder DESC";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$strOriginalWD = getcwd();
		
		//Eliminar una dimension atributo
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/attribdimension.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");

		while(!$aRS->EOF)
		{
			$cla_descrip = (int)$aRS->fields["cladescrip"];
			$AttribName = $aRS->fields["membername"];
			$cla_descrip_parent = (int)$aRS->fields["cladescripparent"];
			$cla_concepto = $this->ModelID;
			$anInstanceModelDim = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
			if(!is_null($anInstanceModelDim))
			{
				$anInstanceModelDim->Dimension->isCATALOG = true;
				$anInstanceModelDim->Dimension->DimensionName = $AttribName;
				$anInstanceModelDim->Dimension->ParentDimensionID = $cla_descrip_parent;
				$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
				$anInstanceModelDim->Dimension->FieldDescription = 'DSC_'.$cla_descrip;
				$anInstanceModelDim->Dimension->DimensionID = $cla_descrip;
				//Vamos a forzar que se elimine el atributo aun cuando la dimension esta presente en algun escenario
			    if (!is_null($anInstanceModelDim)) {
					$anInstanceModelDim->Dimension->CheckStages = false;
					$anInstanceModelDim->remove();
			    }
			}
			
			//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if (getMDVersion() >= esvCatalogDissociation && $bUseDissociatedCatDimens) {
				$intIndDimID = (int) @$aRS->fields["inddimid"];
				if ($intIndDimID > 0) {
					//En este caso hay una dimensión alternativa configurada, si el modelo la contiene se debe eliminar pero asegurandose que
					//no se remueva en forma global, para eso se utiliza un nuevo parámetro agregado en el Model Manager con este fin
					$anInstanceModelDim = null;
					$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $intIndDimID);
					if (!is_null($anInstanceModelDim)) {
						//Vamos a eliminar la dimension aun cuando este siendo usada en un escenario por ello deshabilitamos la validacion
						$anInstanceModelDim->Dimension->CheckStages = false;
						
						//El parámetro es importante para que no se remueve la dimensión global
						$anInstanceModelDim->Dimension->remove(false);
					}
				}
			}
			//@JAPR
			
			$aRS->MoveNext();
		}
		
		chdir($strOriginalWD);
	}
	
	//@JAPRDescontinuada en v6
	function updateQuestionHierarchyDueToRemove()
	{
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		//Primero checamos si hay mas preguntas con el mismo catalogo si es asi
		//procedemos a actualizar jerarquias, en caso contrario no se realiza nada
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND CatalogID = ".$this->CatalogID." AND UseCatalog = 1 AND QuestionID <> ".$this->QuestionID." AND QTypeID <> ".qtpOpenNumeric;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." "."SI_SV_Question"." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Si ya existe otras preguntas que utilizan el mismo catalogo entonces procedemos a actualizar los campos
		//FatherQuestionID, ChildQuestionID de la pregunta anterior y de la pregunta posterior
		if(!$aRS->EOF)
		{
			$fatherQuestionID = 0;
			$childQuestionID = 0;
			
			//Obtenemos la ultima pregunta antes de la actual que tenga el mismo catalogo
			$sql = "SELECT A.QuestionID FROM SI_SV_Question A 
				WHERE A.SurveyID = ".$this->SurveyID." AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1 
					AND A.CatalogID = ".$this->CatalogID." AND A.QuestionNumber < ".$this->QuestionNumber." 
				ORDER BY A.QuestionNumber DESC";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if (!$aRS->EOF)
			{
				//Obtenemos el QuestionID
				$fatherQuestionID = (int)$aRS->fields["questionid"];
			}
				
			//Obtenemos la primera pregunta despues de la actual que tenga el mismo catalogo
			$sql = "SELECT A.QuestionID FROM SI_SV_Question A 
				WHERE A.SurveyID = ".$this->SurveyID." AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1 
					AND A.CatalogID = ".$this->CatalogID." AND A.QuestionNumber > ".$this->QuestionNumber." 
				ORDER BY A.QuestionNumber ASC";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if (!$aRS->EOF)
			{
				//Obtenemos el QuestionID
				$childQuestionID = (int)$aRS->fields["questionid"];
			}
			
			//Actualizamos la pregunta padre si es que existe pregunta padre para que el ChildQuestionID sea igual al obtenido
			if($fatherQuestionID!=0)
			{
				//Actualizamos en dicha pregunta el ChildQuestionID
				$sql = "UPDATE SI_SV_Question SET ChildQuestionID = ".$childQuestionID." WHERE QuestionID = ".$fatherQuestionID." AND SurveyID = ".$this->SurveyID;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//Actualizamos la pregunta hija si es que existe pregunta hija para que el FatherQuestionID sea igual al obtenido
			if($childQuestionID!=0)
			{
				//Actualizamos en dicha pregunta el FatherQuestionID
				$sql = "UPDATE SI_SV_Question SET FatherQuestionID = ".$fatherQuestionID." WHERE QuestionID = ".$childQuestionID." AND SurveyID = ".$this->SurveyID;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}
	
	function isNewObject()
	{
		return ($this->QuestionID <= 0 || $this->ForceNew);
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Question");
		}
		else
		{
			return translate("Question")." ".$this->QuestionNumber;
		}
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Question&SectionID=".$this->SectionID;
		}
		else
		{
			return "BITAM_PAGE=Question&SectionID=".$this->SectionID."&QuestionID=".$this->QuestionID;
		}
	}
	
	function get_Parent()
	{
		return BITAMSection::NewInstanceWithID($this->Repository, $this->SectionID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'QuestionID';
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			
			//Si está en desarrollo la encuesta si se puede editar la pregunta
			if($aSurveyInstance->Status==0)
			{

				//Por cuestiones de validaciones vamos a impedir que se editen las preguntas
				//que estan dentro de una seccion dinamica porque no se han validado todos los casos
				$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
				
				if($this->SectionID==$dynamicSectionID) //Conchita 2013-01-14 Se permitira la edicion en preguntas de secciones dinamicas
				{
					return true;
				}
				else
				{
					return true;
				}
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		//@JAPR 2015-01-21: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
		//si es utilizado y advertir al usuario (#75TPJE)
		//Con esta propiedad se forzará dinámicamente a bloquear la posibilidad de remover el objeto bajo alguna condición asignada en forma
		//externa a la propia instancia (útil cuando se tiene una colección que debe bloquear ciertas preguntas a partir de consultas y no
		//se quieren lanzar dichas consultas por cada instancia de los objetos de la colección, o bien si no se quiere ocultar el botón para
		//eliminar de la propia instancia de objeto sino controlar que sólo no se pueda eliminar desde la colección)
		if (!$this->enableRemove) {
			return false;
		}
		
		//Si es pregunta tipo simple choice que utiliza catálogo y hay en la encuesta una sección dinámica que utilice el mismo
		//catálogo entonces este catálogo no podrá modificarse, por lo tanto se pone como sólo lectura
		$canRemove=true;
		
		if($this->QTypeID == qtpSingle && $this->UseCatalog && $this->SectionType == sectNormal)
		{
			//Verificar si en la encuesta hay una sección dinámica que utilice el mismo catálogo de la pregunta simple choice
			$sql = "SELECT SectionID FROM SI_SV_Section WHERE SurveyID=".$this->SurveyID." AND SectionType = ".sectDynamic." AND CatalogID=".$this->CatalogID;
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
	
			if(!$aRS->EOF)
			{
				//Sí existe una sección dinámica con el mismo catálogo de la sch, ahora se valida que si es la unica pregunta sch con ese catálogo
				//entonces no pueda cambiarse pero si hay otras con esa característica entonces esta pregunta sí puede cambiarse
				$sql = "SELECT Count(*) AS CountQuestion 
				FROM SI_SV_Question A, SI_SV_Section B 
				WHERE A.SectionID=B.SectionID
				AND A.SurveyID=".$this->SurveyID.
				" AND B.SectionType = ".sectNormal." AND A.QTypeID=".qtpSingle." AND A.UseCatalog=1 AND A.CatalogID=".$this->CatalogID;
				
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
		
				if(!$aRS->EOF)
				{
					$countQuestion = (int)$aRS->fields["countquestion"];
					
					if($countQuestion<=1)
					{
						$canRemove = false;
					}
				}
			}
		}
		
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		//Las preguntas que son selectores de las secciones Inline no se pueden remover, ya que son preguntas generadas automáticamente e indispensables
		if($_SESSION["PABITAM_UserRole"]==1 && !$this->IsSelector && $canRemove)
		{
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			
			if($aSurveyInstance->Status==0)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
	}
	
	function get_Children($aUser)
	{
		$myChildren = array();
		
		if (!$this->isNewObject())
		{
			//@JAPR 2012-06-01: Agregado el soporte para creación de preguntas (single y multi) cuyas respuestas dependan de las seleccionadas de una pregunta multi previa
			//@JAPR 2014-10-01: Agregados los filtros de preguntas para definir colores de los marcadores en simple choice con mapa
			if ($this->QTypeID==qtpSingle && $this->UseCatalog==1 && $this->CatalogID > 0 && $this->UseMap) {
				if (getMDVersion() >= esvQuestionFilters) {
					$myChildren[] = BITAMQuestionFilterCollection::NewInstance($this->Repository, $this->QuestionID);
				}
			}
			elseif(($this->QTypeID==qtpSingle && $this->UseCatalog==0 && $this->QDisplayMode!=dspMatrix && $this->SourceQuestionID<=0) || ($this->QTypeID==qtpMulti && $this->UseCatToFillMC==0 && $this->QDisplayMode!=dspMatrix) || ($this->QTypeID == qtpCallList))
			{
				//@JAPR 2013-05-06: Agregado el tipo de pregunta CallList
				if($this->QTypeID==qtpSingle || $this->QTypeID == qtpCallList)
				{
					$myChildren[] = BITAMQuestionOptionCollection::NewInstance($this->Repository, $this->QuestionID, null, false);
				}
				else 
				{
					//Si la pregunta pertenece a una seccion dinamica debemos preguntar 
					$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
					
					//Si no cumple la condicion de que sea una pregunta de la seccion dinamica y q este dentro de la seccion
					//entonces si desplegamos sus opciones
					if(!($this->SectionID==$dynamicSectionID))
					{
						$myChildren[] = BITAMQuestionOptionCollection::NewInstance($this->Repository, $this->QuestionID, null, false);
					}
				}
			}
			else if(($this->QTypeID==qtpSingle && $this->UseCatalog==0 && $this->QDisplayMode==dspMatrix))
			{
				$myChildren[] = BITAMQuestionOptionCollection::NewInstance($this->Repository, $this->QuestionID, null, false);
				$myChildren[] = BITAMQuestionOptionColCollection::NewInstance($this->Repository, $this->QuestionID, null, false);
			}
		}
		
		return $myChildren;
	}
	
	//@JAPR 2012-12-07: Agregados los parámetros para permitir filtros
	static function getQuestionTypes($aRepository, $anArrayOfIDs = null, $anExcludeArrayOfIDs = null)
	{
		$arrayQTypes = array();
		//@JAPR 2014-08-20: Agregads los datos dinámicos de secciones tipo Recap
		//Ahora este array se generará en forma fija, ya que la tabla que se consultaba antes no se ha actualizado en años
		$arrayQTypes[qtpOpen] = translate("None");
		$arrayQTypes[dtpNumeric] = translate("Number");
		$arrayQTypes[dtpDate] = translate("Date");
		$arrayQTypes[dtpString] = translate("Text");
		$arrayQTypes[dtpAlpha] = translate("Alphanumeric");
		$arrayQTypes[dtpTime] = translate("Time");
		$arrayQTypes[qtpSingle] = translate("Simple choice");
		$arrayQTypes[qtpMulti] = translate("Multiple choice");
		$arrayQTypes[qtpPhoto] = translate("Photo");
		$arrayQTypes[qtpAction] = translate("Action");
		$arrayQTypes[qtpSignature] = translate("Signature");
		$arrayQTypes[qtpMessage] = translate("Message");
		$arrayQTypes[qtpSkipSection] = translate("Skip to section");
		$arrayQTypes[qtpCalc] = translate("Calculated");
		$arrayQTypes[qtpDocument] = translate("Document");
		//Conchita agregados audio y video
		$arrayQTypes[qtpAudio] = translate("Audio");
		$arrayQTypes[qtpVideo] = translate("Video");
		$arrayQTypes[qtpSync] = translate("Synchronize");
		$arrayQTypes[qtpPassword] = translate("Password");
		$arrayQTypes[qtpCallList] = translate("Call List");
		//El tipo de campo qtpLocation se usó originalmente para identificar a campos tipo Geolocalización antes de que existieran las preguntas tipo
		//GPS en eForms, así que por compatibilidad hacia atrás se respetará dicho valor, sin embargo el tipo de campo qtpGPS creado precisamente
		//para las preguntas tipo GPS de eForms, se debe tratar igual que qtpLocation cuando es regresado por esta y otras funciones
		$arrayQTypes[qtpLocation] = translate("GPS");
		$arrayQTypes[qtpMapped] = translate("Mapped");
		$arrayQTypes[qtpSketch] = translate("Sketch");
		//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$arrayQTypes[qtpSketchPlus] = translate("Sketch");
		//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
		$arrayQTypes[qtpSection] = translate("inline");
		//@AAL 06/05/2015: Agregadas las preguntas para código de Barras
		$arrayQTypes[qtpBarCode] = translate("Barcode");
		//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
		$arrayQTypes[qtpExit] = translate("Exit");
		//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
		$arrayQTypes[qtpUpdateDest] = translate("Update data");
		//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
		$arrayQTypes[qtpMyLocation] = translate("Visual geolocation");
		//@JAPR
		return $arrayQTypes;
		
		//@JAPR 2012-12-07: Agregados los parámetros para permitir filtros
		$filter = "";
		$strAnd = "WHERE ";
		if (!is_null($anArrayOfIDs))
		{
			switch (count($anArrayOfIDs))
			{
				case 0:
					break;
				case 1:
					$filter = $strAnd." t1.QTypeID = ".((int)$anArrayOfIDs[0]);
					$strAnd = "AND ";
					break;
				default:
					foreach ($anArrayOfIDs as $anID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $anID; 
					}
					if ($filter != "")
					{
						$filter = $strAnd." t1.QTypeID IN (".$filter.")";
						$strAnd = "AND ";
					}
					break;
			}
		}
		
		if (!is_null($anExcludeArrayOfIDs))
		{
			switch (count($anExcludeArrayOfIDs))
			{
				case 0:
					break;
				case 1:
					$filter = $strAnd." t1.QTypeID <> ".((int)$anExcludeArrayOfIDs[0]);
					$strAnd = "AND ";
					break;
				default:
					foreach ($anExcludeArrayOfIDs as $anID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $anID; 
					}
					if ($filter != "")
					{
						$filter = $strAnd." t1.QTypeID NOT IN (".$filter.")";
						$strAnd = "AND ";
					}
					break;
			}
		}
		//@JAPR
		
		$sql = "SELECT t1.QTypeID, t1.QTypeName FROM SI_SV_QType t1 $filter ORDER BY t1.QTypeID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QType ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$qtypeid = $aRS->fields["qtypeid"];
			$qtypename = $aRS->fields["qtypename"];
			$arrayQTypes[$qtypeid] = $qtypename;
			$aRS->MoveNext();
		}

		return $arrayQTypes;
	}
	
	static function getRecapGroupedQuestionTypes () {
		$arrayQTypes = array();
		$arrayQTypes[qtpOpen] = translate("Open");
		$arrayQTypes[qtpSingle] = translate("Simple choice");
		$arrayQTypes[qtpMulti] = translate("Multiple choice");
		$arrayQTypes[qtpPhoto] = translate("Photo");
		$arrayQTypes[qtpSignature] = translate("Signature");
		$arrayQTypes[qtpSketch] = translate("Sketch");
		//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$arrayQTypes[qtpSketchPlus] = translate("Sketch");
		//@JAPR
		$arrayQTypes[qtpCalc] = translate("Calculated");
		$arrayQTypes[qtpDocument] = translate("Document");
		if (getMDVersion() >= esvGPSQuestion) {
			$arrayQTypes[qtpGPS] = translate("GPS");
		}
		if(getMDVersion() >= esvQuestionAudioVideo) {
			$arrayQTypes[qtpAudio] = translate("Audio");
			$arrayQTypes[qtpVideo] = translate("Video");
		}
		//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
		if(getMDVersion() >= esvMyLocation) {
			$arrayQTypes[qtpMyLocation] = translate("Visual geolocation");
		} 
		return $arrayQTypes;
	}

	//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
	//Regresa los tipos de pergunta agrupando aquellas que son Abiertas en lugar de incluir todas las subcategorías de estas
	static function getGroupedQuestionTypes($aRepository, $opts)
	{
		$arrayQTypes = array();
		//@JP_ML
		$arrayQTypes[qtpOpen] = translate("Open");
		$arrayQTypes[qtpSingle] = translate("Simple choice");
		$arrayQTypes[qtpMulti] = translate("Multiple choice");
		//@JAPR 2012-05-14: Agregados nuevos tipos de pregunta Foto y Action
		//@JAPR 2013-03-12: Ocultado este tipo de pregunta porque no se terminó de implementar (#28813)
		//$arrayQTypes[qtpShowValue] = "Show value";
		$arrayQTypes[qtpPhoto] = translate("Photo");
		$arrayQTypes[qtpAction] = translate("Action");
		$arrayQTypes[qtpSignature] = translate("Signature");
		//@JAPR
		$arrayQTypes[qtpMessage] = translate("Message");
		$arrayQTypes[qtpSkipSection] = translate("Skip to section");
		$arrayQTypes[qtpCalc] = translate("Calculated");
		$arrayQTypes[qtpDocument] = translate("Document");
		//@JAPR 2013-04-12: Agregados nuevos tipos de pregunta Sincronización
		//@JAPR 2013-04-18: Validado por versión de la Metadata, si usan una versión previa se desconfigurará la pregunta
		if (getMDVersion() >= esvSyncQuestionAndSkipFoto) {
			$arrayQTypes[qtpSync] = translate("Synchronize");
		}
		//Conchita 2013-04-29 agregado call list
		if (getMDVersion() >= esvCallList) {
			$arrayQTypes[qtpCallList] = translate("Call List");
		}
		//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		if (getMDVersion() >= esvGPSQuestion) {
			$arrayQTypes[qtpGPS] = translate("GPS");
		}
		//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$arrayQTypes[qtpPassword] = translate("Password");
		}
		if(getMDVersion() >= esvQuestionAudioVideo) {
			$arrayQTypes[qtpAudio] = translate("Audio");
			$arrayQTypes[qtpVideo] = translate("Video");
		}
		if(getMDVersion() >= esvSketch) {
			$arrayQTypes[qtpSketch] = translate("Sketch");
		}
		//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if(getMDVersion() >= esvSketchPlus) {
			$arrayQTypes[qtpSketchPlus] = translate("Sketch");
		}
		//@JAPR
		if(getMDVersion() >= esvSectionQuestions) {
			$arrayQTypes[qtpSection] = translate("Inline");
		}
		
		if (getMDVersion() >= esvSectionRecap) {
			$aSectionID = (int) @$opts['SectionID'];
			//@JAPR 2014-07-28: Corregido un bug, se validó que las secciones que no son tipo Recap no se queden
			//ligadas a una SectionFormID, lo cual en el código las convertiría automáticamente en secciones tipo Recap
			if ($aSectionID > 0) {
				$aSection = BITAMSection::NewInstanceWithID($aRepository, $aSectionID);
				if($aSection && $aSection->SectionType == sectRecap && $aSection->SectionFormID) {
					$arrayQTypes = BITAMQuestion::getRecapGroupedQuestionTypes();
				}
			}
			//@JAPR
		}
		//@AAL 06/05/2015: Agregado las preguntas tipo Código de Barras
		if(getMDVersion() >= esvRedesign) {
			$arrayQTypes[qtpBarCode] = translate("Barcode");
		}
		//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
		if(getMDVersion() >= esvExitQuestion) {
			$arrayQTypes[qtpExit] = translate("Exit");
		}
		//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
		if (getMDVersion() >= esvUpdateData) {
			$arrayQTypes[qtpUpdateDest] = translate("Update data");
		}
		//@JAPR
		//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
		if (getMDVersion() >= esvMyLocation) {
			$arrayQTypes[qtpMyLocation] = translate("Visual geolocation");
		}
		return $arrayQTypes;
	}
	
	//@JAPR 2015-01-13: Agregada información extra para las preguntas
	static function GetAllQuestionTypes($aRepository) {
		$arrayQTypes = array();
		$arrayQTypes[qtpOpenNumeric] = translate("Open").'-'.translate("Number");
		$arrayQTypes[qtpOpenDate] = translate("Open").'-'.translate("Date");
		$arrayQTypes[qtpOpenString] = translate("Open").'-'.translate("Text");
		$arrayQTypes[qtpOpenAlpha] = translate("Open").'-'.translate("Alphanumeric");
		$arrayQTypes[qtpOpenTime] = translate("Open").'-'.translate("Time");
		$arrayQTypes[qtpSingle] = translate("Simple choice");
		$arrayQTypes[qtpMulti] = translate("Multiple choice");
		$arrayQTypes[qtpPhoto] = translate("Photo");
		$arrayQTypes[qtpAction] = translate("Action");
		$arrayQTypes[qtpSignature] = translate("Signature");
		$arrayQTypes[qtpMessage] = translate("Message");
		$arrayQTypes[qtpSkipSection] = translate("Skip to section");
		$arrayQTypes[qtpCalc] = translate("Calculated");
		$arrayQTypes[qtpDocument] = translate("Document");
		$arrayQTypes[qtpSync] = translate("Synchronize");
		$arrayQTypes[qtpCallList] = translate("Call List");
		$arrayQTypes[qtpGPS] = translate("GPS");
		$arrayQTypes[qtpPassword] = translate("Password");
		$arrayQTypes[qtpAudio] = translate("Audio");
		$arrayQTypes[qtpVideo] = translate("Video");
		$arrayQTypes[qtpSketch] = translate("Sketch");
		//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$arrayQTypes[qtpSketchPlus] = translate("Sketch");
		//@JAPR
		$arrayQTypes[qtpSection] = translate("Inline");
		//@AAL 06/05/2015: Agregado las preguntas tipo Código de Barras
		if(getMDVersion() >= esvRedesign) {
			$arrayQTypes[qtpBarCode] = translate("Barcode");
		}
		//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
		if(getMDVersion() >= esvExitQuestion) {
			$arrayQTypes[qtpExit] = translate("Exit");
		}
		//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
		if (getMDVersion() >= esvUpdateData) {
			$arrayQTypes[qtpUpdateDest] = translate("Update data");
		}
		//@JAPR
		//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
		if (getMDVersion() >= esvMyLocation) {
			$arrayQTypes[qtpMyLocation] = translate("Visual geolocation");
		}
		
		return $arrayQTypes;
	}
	//@JAPR
	
	//Regresa los tipos de dato soportados para las preguntas
	static function getQuestionDataTypes($aRepository, $aQuestion)
	{
		$arrDataTypes = array();
		//@JP_ML
		$arrDataTypes[dtpNumeric] = translate("Number");
		$arrDataTypes[dtpDate] = translate("Date");
		$arrDataTypes[dtpString] = translate("Text");
		$arrDataTypes[dtpAlpha] = translate("Alphanumeric");
		$arrDataTypes[dtpTime] = translate("Time");
		
		return $arrDataTypes;
	}
	
	//Regresa los tipos de despliegue válidos según el tipo de pregunta
	static function getQuestionDisplayModes($iQTyeID, $isNewObj=true, $iQDisplayMode=null)
	{
		$arrQDisplayMode = array();
		//Conchita 2013-05-02 agregado call list, solo es despliegue vertical nada mas
		switch ($iQTyeID)
		{
			case qtpCallList:
			case qtpSingle:
				//@JP_ML
				if($isNewObj==true || ($isNewObj==false && $iQDisplayMode!=dspMatrix))
				{
					$arrQDisplayMode[dspVertical] = translate("Vertical");
					$arrQDisplayMode[dspHorizontal] = translate("Horizontal");
					$arrQDisplayMode[dspMenu] = translate("Menu");
					$arrQDisplayMode[dspAutocomplete] = translate("Autocomplete");
					$arrQDisplayMode[dspLabelnum] = translate("Ranking");
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					$arrQDisplayMode[dspAR] = translate("AR");
				}
				
				if(/*$isNewObj==true || */($isNewObj==false && $iQDisplayMode==dspMatrix))
				{
					$arrQDisplayMode[dspMatrix] = "Matrix";
				}
				break;
			case qtpMulti:
				//@JP_ML
				if($isNewObj==true || ($isNewObj==false && $iQDisplayMode!=dspMatrix))
				{
					$arrQDisplayMode[dspVertical] = translate("Vertical");
					$arrQDisplayMode[dspHorizontal] = translate("Horizontal");
					$arrQDisplayMode[dspMenu] = translate("Menu");
					$arrQDisplayMode[dspAutocomplete] = translate("Autocomplete");
					$arrQDisplayMode[dspLabelnum] = translate("Ranking");
				}
				
				if($isNewObj==true || ($isNewObj==false && $iQDisplayMode==dspMatrix))
				{
					//$arrQDisplayMode[dspMatrix] = "Matrix";
				}
				break;
			//@JAPR 2013-04-12: Agregada la opción de visualización de preguntas tipo Foto
			case qtpPhoto:
				$arrQDisplayMode[dspDefault] = translate("Default");
				$arrQDisplayMode[dspCustomized] = translate("Customized");
				break;
			//@JAPR
			default:
				//@JP_ML
				if($isNewObj==true || ($isNewObj==false && $iQDisplayMode!=dspMatrix))
				{
					$arrQDisplayMode[dspEntry] = "Entry";
				}
				
				if($isNewObj==true || ($isNewObj==false && $iQDisplayMode==dspMatrix))
				{
					//$arrQDisplayMode[dspMatrix] = "Matrix";
				}
				break;
		}
		
		return $arrQDisplayMode;
	}
	//@JAPR
	
	//@JAPR 2011-06-27: Agregado el grabado automático de la Metadata para disp. móviles
	function getMobileControlId()
	{
		$intControlId = 9;		// Open Una Sencillo
		//conchita agregado el tipo call list
		switch ($this->GQTypeID)
		{
			case qtpCallList:
				switch ($this->QDisplayMode)
				{
					case dspVertical:
						$intControlId = 1;		// Seleccion Simple Vertical
						break;
					default:
						$intControlId = 1;	
						break;
				}
				break;
			case qtpSingle:
				switch ($this->QDisplayMode)
				{
					case dspVertical:
						$intControlId = 1;		// Seleccion Simple Vertical
						break;
					case dspHorizontal:
						$intControlId = 2;		// Seleccion Simple Horizontal
						break;
					case dspMatrix:
						$intControlId = 4;		// Seleccion Simple Matriz
						break;
					default:	// dspMenu
						$intControlId = 3;		// Seleccion Simple Menu
						break;
				}
				break;
			case qtpMulti:
				switch ($this->QDisplayMode)
				{
					case dspVertical:
						$intControlId = 5;		// Seleccion Múltiple Vertical
						break;
					case dspHorizontal:
						$intControlId = 6;		// Seleccion Múltiple Horizontal
						break;
					case dspMatrix:
						$intControlId = 8;		// Seleccion Múltiple Matriz
						break;
					default:	// dspMenu
						$intControlId = 7;		// Seleccion Múltiple Menu
						break;
				}
				break;
			default:		//qtpOpen
				switch ($this->QDisplayMode)
				{
					case dspMatrix:
						$intControlId = 10;		// Open Una Matrix
						break;
					default:	// dspEntry
						$intControlId = 9;		// Open Una Sencillo
				}
				break;
		}
		
		return $intControlId;
	}
	//@JAPR
	
	//@JAPRDescontinuada: Esta función ya no se utilizaba al día 2014-11-10, sin embargo si se quisiera reutilizar, sería necesario que
	//se desligada o mínimo se validara que no intente acceder a la tabla de eBavel SI_FORMS_FIELDSFORMS si no existe en ese repositorio
	function GetEbavelFieldsToRecap ($aRepository, $opts) {
		$arreBavelForms = array();
		$surveyID = @$opts['SurveyID'];
		$ebavelSectionID = @$opts['eBavelSectionID'];
		//@JAPR 2014-08-14: Corregido un bug, había una referencia a eBavel que se estaba pidiendo al DWH, funcionaba sólo si tenían repositorio de desarrollo pero eso es incorrecto
		//incluso en esos casos pues pudiera haber diferencias
		$sql = "SELECT t2.id_fieldform, t2.label FROM SI_SV_SurveyAnswerActionFieldsMap t1 JOIN {$this->Repository->ADOConnection->databaseName}.SI_FORMS_FIELDSFORMS t2 ON t1.EBavelFieldID = t2.id_fieldform JOIN {$this->Repository->ADOConnection->databaseName}.SI_FORMS_SECTIONS t3 ON t2.section_id = t3.id_section WHERE t1.SurveyID = $surveyID AND t3.id_section = $ebavelSectionID";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap, SI_FORMS_FIELDSFORMS ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap, SI_FORMS_FIELDSFORMS ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		while(!$aRS->EOF) {
			$fieldformID = (int) @$aRS->fields["id_fieldform"];
			$fieldformLabel = (string) @$aRS->fields["label"];
			$arreBavelForms[$fieldformID] = $fieldformLabel;
			$aRS->MoveNext();
		}
		return $arreBavelForms;
	}
	
	function get_FormField_ShowInSummary (&$myFields = null) {
		$aField = null;
		if(deployable("esvShowInSummary")) {
			$aField = $this->get_FormField("ShowInSummary", "Show in sumary?", "Checkbox");
			if(!$this->isNewObject()) {
				if ($this->IsSelector) {
					$aField->IsDisplayOnly = true;
				}
			}
			if(!is_null($myFields)) {
				$myFields[$aField->Name] = $aField;
			}
		}
		return $aField;
	}
	
	function get_FormField ($name, $title, $type, $options = null) {
		$aField = BITAMFormField::NewFormField();
		$aField->Name = $name;
		$aField->Title = translate($title);
		switch($type) {
			case 'Checkbox':
				$aField->Type = "Boolean";
				$aField->VisualComponent = $type;
				break;
			case 'Combobox':
				$aField->Type = "Object";
				$aField->VisualComponent = $type;
				$aField->Options = ((isset($options['options']))? $options['options'] : array());
				break;
			case 'Integer':
				$aField->Type = $type;
				$aField->FormatMask = ((isset($options['formatMask']))? $options['formatMask'] : '');
				break;
			case 'String':
				$aField->Type = $type;
				$aField->Size = ((isset($options['size']))? $options['size'] : 255);
				break;
			default:
				$aField->Type = $type;
				$aField->Size = 255;
				break;
		}
		return $aField;
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$isNewObj = $this->isNewObject();
		$myFields = array();
		
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
		$this->QuestionText = $this->TranslateVariableQuestionIDsByNumbers($this->QuestionText);
		//@JAPR
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QuestionText";
		$aField->Title = translate("Question");
		$aField->Type = "String";
		$aField->Size = 255;
		/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
		$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(images/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'QuestionText\');">';
		$myFields[$aField->Name] = $aField;
		//Conchita 2013-01-14 Movido este fragmento de codigo ya que se utiliza para validar si se muestra o no el short name
		//Si la pregunta pertenece a una seccion dinamica debemos preguntar 
		$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
		
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
		$this->QuestionMessage = $this->TranslateVariableQuestionIDsByNumbers($this->QuestionMessage);
		if (getMDVersion() >= esvResponsiveDesign) {
			for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
				$strDeviceName = getDeviceNameFromID($intDeviceID);
				$strProperty = "QuestionMessage";
				$strValue = $this->getResponsiveDesignProperty($strProperty, $strDeviceName);
				$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $this->TranslateVariableQuestionIDsByNumbers($strValue));
			}
		}
		//@JAPR
		//Conchita agregado el campo de questionimagesketch
		$this->QuestionImageSketch = $this->TranslateVariableQuestionIDsByNumbers($this->QuestionImageSketch);
		if (getMDVersion() >= esvResponsiveDesign) {
			for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
				$strDeviceName = getDeviceNameFromID($intDeviceID);
				$strProperty = "QuestionImageSketch";
				$strValue = $this->getResponsiveDesignProperty($strProperty, $strDeviceName);
				$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $this->TranslateVariableQuestionIDsByNumbers($strValue));
			}
		}
		
		//Conchita
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AttributeName";
		$aField->Title = translate("Short Name");
		$aField->Type = "String";
		$aField->Size = 255;
		if(!$isNewObj)
		{
			if($this->GQTypeID==qtpMulti && $this->QDisplayMode==dspVertical && $this->MCInputType==mpcNumeric && $this->IsMultiDimension==0 && $this->UseCategoryDimChoice!=0)
			{
				$aField->IsDisplayOnly = true;
			}
			if($this->SectionID==$dynamicSectionID){ //Conchita 2013-01-14 si es edicion no habilitar el cambiar question type si es dinamica
				$aField->IsDisplayOnly = true;
			}
			
		}
		$myFields[$aField->Name] = $aField;
		
		
		
		$strChecked = "";

		if($this->HasValidation == 1)
		{
			$strChecked = "checked";
		}
		
		$strCheckedIsInd = "";

		if($this->IsIndicator == 1)
		{
			$strCheckedIsInd = "checked";
		}
		
		//$strCodeValidation = '<span id="lblvalidation" name="lblvalidation" style="display:none">&nbsp;&nbsp;<input type="checkbox" id="addvalidation" name="addvalidation" '.$strChecked.' onclick="javascript:showHideValidFields();">&nbsp;&nbsp;'.translate("Add Validation")."</span>";
		$strCodeValidation = '<span id="lblIsIndicator" name="lblIsIndicator">&nbsp;&nbsp;<input type="checkbox" id="IsIndicator" name="IsIndicator" '.$strCheckedIsInd.'>&nbsp;&nbsp;'.translate("Is Field Indicator?")."</span>";
		
		//Si es pregunta tipo simple choice que utiliza catálogo y hay en la encuesta una sección dinámica que utilice el mismo
		//catálogo entonces este catálogo no podrá modificarse, por lo tanto se pone como sólo lectura
		$canEditFields=true;
		
		if($this->QTypeID == qtpSingle && $this->UseCatalog && $this->SectionType == sectNormal)
		{
			//Verificar si en la encuesta hay una sección dinámica que utilice el mismo catálogo de la pregunta simple choice
			$sql = "SELECT SectionID FROM SI_SV_Section WHERE SurveyID=".$this->SurveyID." AND SectionType = ".sectDynamic." AND CatalogID=".$this->CatalogID;
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
	
			if(!$aRS->EOF)
			{
				//Sí existe una sección dinámica con el mismo catálogo de la sch, ahora se valida que si es la unica pregunta sch con ese catálogo
				//entonces no pueda cambiarse pero si hay otras con esa característica entonces esta pregunta sí puede cambiarse
				$sql = "SELECT Count(*) AS CountQuestion 
				FROM SI_SV_Question A, SI_SV_Section B 
				WHERE A.SectionID=B.SectionID
				AND A.SurveyID=".$this->SurveyID.
				" AND B.SectionType = ".sectNormal." AND A.QTypeID=".qtpSingle." AND A.UseCatalog=1 AND A.CatalogID=".$this->CatalogID;
				
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
		
				if(!$aRS->EOF)
				{
					$countQuestion = (int)$aRS->fields["countquestion"];
					
					if($countQuestion<=1)
					{
						$canEditFields = false;
					}
				}
			}
		}
		
		$aField = BITAMFormField::NewFormField();
		//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
		//Modificado para agrupar los tipos de preguntas abiertas en uno sólo
		$aField->Name = "GQTypeID";
		$aField->Title = translate("Question Type");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		//Modificado para agrupar los tipos de preguntas abiertas en uno sólo
		$aField->Options = BITAMQuestion::getGroupedQuestionTypes($this->Repository, array('SectionID' => $this->SectionID));
		//@JAPR 2015-01-13: Corregido un bug, las preguntas tipo sección no se deben poder mover dentro de otra sección múltiple (#XWKDHA)
		if ($this->SectionType != sectNormal) {
			unset($aField->Options[qtpSection]);
		}
		//@JAPR
		$aField->OnChange = "showHideChkValidation();";
		
		//@JAPR 2014-10-03: Corregido un bug, las rows ocultas en el diseño se ven como un espacio vacio
		//Sólo los campos definidos arriba se mantienen sin el atributo Style debido a que no están condicionados, todos los demás si tienen
		//atributo Style pues dependen del tipo. Algunos que se ocultan directamente en la definición, se muestran como una row muy pequeña en
		//blanco lo cual se hace mas evidente si se acumulan varios. Se van a ocultar directamente al cambiar el tipo basado en que no
		//contienen una propiedad Style
		
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		//Esta lista contiene las secciones que no deben ser utilizadas para saltos ni acciones similares por ser secciones mapeadas a preguntas de
		//tipo qtpSection, por lo que no se comportarán como una sección realmente (no tienen una página generada para ellas)
		$arrInvalidSections = array();
		if (getMDVersion() >= esvSectionQuestions) {
			$arrInvalidSections = @BITAMSection::GetMappedSections($this->Repository, $this->SurveyID, false);
			$arrInvalidSections = array_keys($arrInvalidSections);
		}
		
		if(!$isNewObj)
		{
			if($this->QTypeID==qtpSingle && $this->UseCatalog==0 && $this->QDisplayMode == dspMatrix)
			{
				$aField->IsDisplayOnly = true;
			}
			else if($this->QTypeID==qtpMulti && $this->QDisplayMode==dspVertical && $this->MCInputType==mpcNumeric && $this->IsMultiDimension==0 && $this->UseCategoryDimChoice!=0)
			{
				$aField->IsDisplayOnly = true;
			}
			//@JAPR 2014-05-20: Agregado el tipo de sección Inline
			elseif ($this->IsSelector) {
				$aField->IsDisplayOnly = true;
			}
			//@JAPR 2012-05-14: Agregados nuevos tipos de pregunta Foto y Action
			else if ($this->QTypeID == qtpPhoto || $this->QTypeID == qtpAction || $this->QTypeID == qtpSignature || $this->QTypeID == qtpSketch)
			{
				$aField->IsDisplayOnly = false;
			}
			//@JAPR
			else 
			{
				$aField->IsDisplayOnly = false;
			}
			if($this->SectionID==$dynamicSectionID){ //Conchita 2013-01-14 si es edicion no habilitar el cambiar question type si es dinamica
				$aField->IsDisplayOnly = true;
			}
		}

		if(!$canEditFields)
		{
			$aField->IsDisplayOnly=true;
		}

		$myFields[$aField->Name] = $aField;

		$this->AvailSelDisplayModes = dspVertical;
		$this->AvailEntryDisplayModes = dspEntry;
		$this->AvailPhotoDisplayModes = dspDefault;
		$this->AvailNumericVisualStyles = 0;
		$this->AvailMenuVisualStyles = 0;

		/**********************************************/
		
		$this->CatalogSCID = 0;
		
		$this->CatalogMCID = 0;

		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
		$catalogID = $surveyInstance->CatalogID;
		$arrayUniqueCatalog = array();
		
		if(!is_null($catalogID) && $catalogID!=0)
		{
			$catalogInstance = BITAMCatalog::NewInstanceWithID($this->Repository, $catalogID);
			
			if(!is_null($catalogInstance))
			{
				$arrayUniqueCatalog[$catalogID] = $catalogInstance->CatalogName;
			}
		}

		$this->CatalogSVID = 0;

		/**********************************************/
		
		$optionsGral = array();
		$optionsGral[0] = translate("No");
		$optionsGral[1] = translate("Yes");
		
		if (getMDVersion() >= esvNearBy) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "UseMap";
			$aField->Title = translate("Use map?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->OnChange = "showHideMapAtributes();";
			$aField->Options = $optionsGral;
			//@JAPR 2014-05-20: Agregado el tipo de sección Inline
			if(!$this->isNewObject()) {
				if ($this->IsSelector) {
					$aField->IsDisplayOnly = true;
				}
			}
			//@JAPR
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "MapRadio";
			$aField->Title = translate("Map radio");
			$aField->Type = "Integer";
			$myFields[$aField->Name] = $aField;
		}
				//Conchita 2014-09-08 agregar el ancho y alto de las tipo sketch
		if (getMDVersion() >= esvSketch) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "canvasWidth";
			$aField->Title = translate("Canvas Width");
			$aField->Type = "String";
			$aField->Size = 4;
			//$aField->InTitle = true;
			
			$myFields[$aField->Name] = $aField;
			
			if(!$isNewObj && $this->QTypeID!=qtpSketch){
				
				$aField->IsVisible = false;
			}
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "canvasHeight";
			$aField->Title = translate("Canvas Height");
			$aField->Type = "String";
			$aField->Size = 4;
			//$aField->InTitle = true;
		
			$myFields[$aField->Name] = $aField;
			if(!$isNewObj && $this->QTypeID!=qtpSketch){
				
				$aField->IsVisible = false;
			}
		}
		//Si se va a editar la pregunta que se oculte la opcion Matrix si es que no ha sido seleccionada
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QDisplayMode";
		$aField->Title = translate("Entry display mode");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMQuestion::getQuestionDisplayModes($this->QTypeID, $isNewObj, $this->QDisplayMode);
		$aField->OnChange = "showHideChkValidation();";
		if(!$isNewObj)
		{
			if($this->QTypeID==qtpSingle && $this->UseCatalog==0 && $this->QDisplayMode == dspMatrix)
			{
				$aField->IsDisplayOnly = true;
			}
			else if($this->QTypeID==qtpMulti && $this->QDisplayMode==dspVertical && $this->MCInputType==mpcNumeric && $this->IsMultiDimension==0 && $this->UseCategoryDimChoice!=0)
			{
				$aField->IsDisplayOnly = true;
			}
			//@JAPR 2014-05-20: Agregado el tipo de sección Inline
			elseif ($this->IsSelector) {
				$aField->IsDisplayOnly = true;
			}
			//@JAPR
			else 
			{
				$aField->IsDisplayOnly = false;
			}
		}
		$myFields[$aField->Name] = $aField;
		
		$strCheckedIsIndMC = "";
		if($this->IsIndicatorMC == 1)
		{
			$strCheckedIsIndMC = "checked";
		}

		$strDisplay = "";
		if($this->SectionID!=$dynamicSectionID)
		{
			$strDisplay = ' style="visibility:hidden"';
		}

		$strCodeValidationMC = '<span id="lblIsIndicatorMC" name="lblIsIndicatorMC"'.$strDisplay.'>&nbsp;&nbsp;<input type="checkbox" id="IsIndicatorMC" name="IsIndicatorMC" '.$strCheckedIsIndMC.'>&nbsp;&nbsp;'.translate("Is Field Indicator?")."</span>";

		$optInputType = array();
		$optInputType[0] = translate("None");
		$optInputType[1] = translate("Numeric");
		$optInputType[2] = translate("Text");

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MCInputType";
		$aField->Title = translate("Add input to options");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optInputType;
		$aField->AfterMessage = $strCodeValidationMC;
		//$aField->OnChange = "showIsIndicatorMC();";
		$aField->OnChange = "showHideChkValidation();";
		if(!$isNewObj)
		{
			if($this->QTypeID==qtpMulti && $this->QDisplayMode==dspVertical && $this->MCInputType==mpcNumeric && $this->IsMultiDimension==0 && $this->UseCategoryDimChoice!=0)
			{
				$aField->IsDisplayOnly = true;
			}
			else 
			{
				$aField->IsDisplayOnly = false;
			}
		}

		$myFields[$aField->Name] = $aField;
        
	    $aField = BITAMFormField::NewFormField();
		$aField->Name = "MaxChecked";
		$aField->Title = translate("Max number of checked");
		$aField->Type = "Integer";
		$aField->FormatMask = "##0";
		$myFields[$aField->Name] = $aField;
        
		$optionsIsMulti = array();
		$optionsIsMulti[1] = translate("Yes");
		$optionsIsMulti[0] = translate("No");
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IsMultiDimension";
		$aField->Title = translate("Is Multiple Dimension?");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsIsMulti;
		$aField->OnChange = "showHideChkValidation();";
		if(!$isNewObj)
		{
			if($this->QTypeID==qtpMulti && $this->QDisplayMode==dspVertical && $this->MCInputType==mpcNumeric && $this->IsMultiDimension==0 && $this->UseCategoryDimChoice!=0)
			{
				$aField->IsDisplayOnly = true;
			}
			else 
			{
				$aField->IsDisplayOnly = false;
			}
		}
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QDataType";
		$aField->Title = translate("Data Type");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMQuestion::getQuestionDataTypes($this->Repository, $this);
		$aField->OnChange = "showHideChkValidation();";
		//@JAPR 2012-02-22: Agregada la configuración de indicador para preguntas calculadas (#28647)
		//$aField->AfterMessage = $strCodeValidation;
		if(!$isNewObj)
		{
			//$aField->IsDisplayOnly = true;
			if($this->SectionID==$dynamicSectionID){ //Conchita 2013-01-14 si es edicion no habilitar el cambiar question type si es dinamica
				$aField->IsDisplayOnly = true;
			}
		}
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2012-04-30: Agregada la opción para configurar el estilo visual del componente (Campo QComponentStyleID)
		//@JAPR 2014-02-07: Movido debajo del QDataType porque si es open numérica entonces se habilita este campo
		$optVisualStyle = array();
		$optVisualStyle[0] = translate("Native");
		$optVisualStyle[1] = translate("Mobile enhanced");
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QComponentStyleID";
		$aField->Title = translate("Visual Style");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optVisualStyle;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2012-02-22: Agregada la configuración de indicador para preguntas calculadas (#28647)
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IsIndicator";
		$aField->Title = translate("Is Field Indicator?");
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		$myFields[$aField->Name] = $aField;
		//@JAPR

		//@AAL 12/01/2015 Se saca de la condición para usar más adelante el tributo "CatalogID"
		$sectionInstance = BITAMSection::NewInstanceWithID($this->Repository, $this->SectionID);
		if (getMDVersion() >= esveBavelMapping) {
			//@JAPR 2012-12-07: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
			//Se agregó un clon del QTypeID dado a que se reutilizará el framework para que automáticamente seleccione los campos según el tipo
			//de pregunta, pero el tipo de pregunta que se permite seleccionar (GQTypeID) no contiene los mismos valores que QTypeID
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "EBQTypeID";
			$aField->Title = "";	//translate("eForms Question Type");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			$aField->Options = BITAMQuestion::getQuestionTypes($this->Repository, null, array(qtpMessage, qtpSkipSection, qtpShowValue, qtpSync, qtpExit));
			//@JAPR 2015-01-13: Corregido un bug, las preguntas tipo sección no se deben poder mover dentro de otra sección múltiple (#XWKDHA)
			if ($this->SectionType != sectNormal) {
				unset($aField->Options[qtpSection]);
			}
			//@JAPR
			//$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
			$QTypeIDField = $aField;
			require_once('eBavelIntegration.inc.php');
			//@JAPR 2014-08-21: Modificado el comportamiento de los mapeos en las secciones Recap, ahora no sólo se podrán mapear los campos
			//de eBavel que tengan alguna acción asociada, sino cualquier campo de la forma asignada en esta sección, pero se debe basar en
			//el tipo de pregunta del que se trate, ya que ahora no nada mas se pueden agregar preguntas tipo Mapeo, estas últimas sin embargo
			//podrán mapearse a cualquier tipo de campo por igual
			if ($sectionInstance->SectionType == sectRecap) {
				$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, null, array($sectionInstance->SectionFormID), null, true, '');
				/*
				$surveyID = $sectionInstance->SurveyID;
				$eBavelSectionID = $sectionInstance->SectionFormID;
				$arrMyeBavelFormsFields = array();
				$ebavelFieldsToRecap = $this->GetEbavelFieldsToRecap($this->Repository, array('SurveyID' => $surveyID, 'eBavelSectionID' => $eBavelSectionID));			
				if(!$isNewObj && !in_array($this->eBavelFieldID, $ebavelFieldsToRecap)) {
					//$fieldFormObj = @GetEBavelFieldForm($this->Repository, array('fieldformID' => $this->eBavelFieldID));
					$arrMyeBavelFormsFields[$this->eBavelFieldID] = 'Default';
				}
				
				foreach($ebavelFieldsToRecap as $akey => $aField) {
					$arrMyeBavelFormsFields[$akey] = $aField;
				}
				*/
			} else {
				//@JAPR 2014-08-21: Corregido un bug, al agregar las secciones tipo Recap se cambió la forma de eBavel de la que se extraen
				//los campos por aquella de la sección, siendo que en este Else entra cuando no es una sección Recap y por tanto el mapeo
				//debe corresponder con la forma de la encuesta
				$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $surveyInstance->eBavelAppID, array($surveyInstance->eBavelFormID), null, true, '');
			}
			
			//Array indexado por tipo de dato (de eForms) + EBavelSectionId + EBavelFieldID para obtener los arrays que se usarán al sincronizar
			//las combos de cada campo de mapeo de eForms
			$arrAllEBavelFormsFields = array('0' => array(0 => translate('None')));
			
			//Primero tiene que generar una serie de Arrays basados en los tipos de campos pero agrupados por FormID
			//ya que finalmente los campos se deben entrelazar conforme se van seleccionando las combos padre. La agrupación final será por
			//el tipo de campo para poder aplicar en cada combo la colección correcta
			//@JAPR 2014-08-21: Modificado para que no dependa del tipo de la pregunta, ya que eso se determina en el server pero se
			//puede cambiar en el cliente durante la definición, así que ahora se refrescará según lo que seleccione el usuario
			foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
				//@JAPR 2014-02-19: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//El tipo de campo qtpLocation se usó originalmente para identificar a campos tipo Geolocalización antes de que existieran las preguntas tipo
				//GPS en eForms, así que por compatibilidad hacia atrás se respetará dicho valor, sin embargo el tipo de campo qtpGPS creado precisamente
				//para las preguntas tipo GPS de eForms, se debe tratar igual que qtpLocation cuando es regresado por esta y otras funciones
				//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
				$intQTypeIDTemp = $intQTypeID;
				if ($intQTypeIDTemp == qtpGPS || $intQTypeIDTemp == qtpMyLocation) {
					$intQTypeIDTemp = qtpLocation;
				}
				//@JAPR
				
				if (!isset($arrAllEBavelFormsFields[$intQTypeIDTemp])) {
					$arrAllEBavelFormsFields[$intQTypeIDTemp] = array();
					$arrAllEBavelFormsFields[$intQTypeIDTemp][0] = translate('None');
				}
				
				foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
					if ($intFieldID > 0) {
						$arrAllEBavelFormsFields[$intQTypeIDTemp][$intFieldID] = $arrFieldData['label'];
					}
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "eBavelFieldID";
			$aField->Title = translate("eBavel form field");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrAllEBavelFormsFields;
			$myFields[$aField->Name] = $aField;
			$eBavelFieldsField = $aField;
			
			$eBavelFieldsField->Parent = $QTypeIDField;
			$QTypeIDField->Children[] = $eBavelFieldsField;
		}
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SummaryTotal";
		$aField->Title = translate("Show total in summary");
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		$myFields[$aField->Name] = $aField;
		
		$strDateFormatMask = '<span id="lblDateFormatMask" name="lblDateFormatMask" style="display:none">&nbsp;&nbsp;<input type="text" id="DateFormatMask" name="DateFormatMask" size=32 maxlength=30 value="'.htmlentities($this->DateFormatMask).'"></span>';
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DateFormatMaskID";
		$aField->Title = translate("Format");
		$aField->Type = "Object";
		$aField->VisualComponent = "ComboBox";
		$aField->Options = self::$arrDateFormats;
		$aField->Size = 30;
		$aField->OnChange = "showHideChkValidation();";
		$aField->AfterMessage = $strDateFormatMask;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2011-06-29: Agregada la longitud de los campos Alfanumericos
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QLength";
		$aField->Title = translate("Length");
		$aField->Type = "Integer";
		$myFields[$aField->Name] = $aField;
		//@JAPR
		
		//@JAPR 2013-04-15: Agregada la opción de despliegue de texto para algunas preguntas
		$optionsTextStyle = array();
		//$optionsTextStyle[0] = "Single line";
		//$optionsTextStyle[1] = "Multi line";
		$optionsTextStyle[butTxtNone] = translate("Hide");
		$optionsTextStyle[butTxtTop] = translate("Top");
		$optionsTextStyle[butTxtLeft] = translate("Left");
		$optionsTextStyle[butTxtBottom] = translate("Bottom");
		$optionsTextStyle[butTxtRight] = translate("Right");
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TextDisplayStyle";
		$aField->Title = translate("Display type");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsTextStyle;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-02-07: Agrupadas las opciones de llenado de preguntas simple y multiple choice
		$arrSCHTypeOfFilling = array();
		$arrSCHTypeOfFilling[tofFixedAnswers] = translate("Fixed answers");
		$arrSCHTypeOfFilling[tofCatalog] = translate("Catalog");
		$arrSCHTypeOfFilling[tofMCHQuestion] = translate("From another multiple choice's answers");
		if (getMDVersion() >= esvSimpleChoiceSharing) {
			$arrSCHTypeOfFilling[tofSharedAnswers] = translate("Share with another question");
		}
		
		//@JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$arrPictLayOut = array(plyoLeft => translate("Left"), plyoTop => translate("Top"), plyoBehind => translate("Behind"));
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "PictLayOut";
			$aField->Title = translate("Response images layout");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrPictLayOut;
			$myFields[$aField->Name] = $aField;
			
		    $aField = BITAMFormField::NewFormField();
			$aField->Name = "InputWidth";
			$aField->Title = translate("Input width");
			$aField->Type = "Integer";
			$aField->FormatMask = "##0";
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "typeOfFillingSC";
		//@JAPR 2013-02-22: Modificado porque hay un caso con las abiertas donde si se deben permitir de catálogo
		$aField->Title = translate("Type of filling");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrSCHTypeOfFilling;
		$aField->OnChange = "showFillingQuestionsSCH();";
		//@JAPR 2014-05-20: Agregado el tipo de sección Inline
		if(!$this->isNewObject()) {
			if ($this->IsSelector) {
				$aField->IsDisplayOnly = true;
			}
		}
		//@JAPR
		
		if(!$canEditFields)
		{
			$aField->IsDisplayOnly=true;
		}

		$myFields[$aField->Name] = $aField;
		
		$arrMCHTypeOfFilling = array();
		$arrMCHTypeOfFilling[0] = translate("Fixed answers");
		$arrMCHTypeOfFilling[2] = translate("From another multiple choice's answers");
		if (getMDVersion() >= esvSimpleChoiceSharing) {
			$arrMCHTypeOfFilling[3] = translate("Share with another question");
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "typeOfFillingMC";
		//@JAPR 2013-02-22: Modificado porque hay un caso con las abiertas donde si se deben permitir de catálogo
		$aField->Title = translate("Type of filling");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrMCHTypeOfFilling;
		$aField->OnChange = "showFillingQuestionsMCH();";
		$myFields[$aField->Name] = $aField;
		//@JAPR

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UseCatalog";
		//@JAPR 2013-02-22: Modificado porque hay un caso con las abiertas donde si se deben permitir de catálogo
		$aField->Title = translate("Use Catalog?");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsGral;
		$aField->OnChange = "showHideCatalogs();";
		//29Ene2013: En secciones dinámicas no se permite preguntas que utilicen catálogo
		//@JAPR 2013-02-22: Modificado porque hay un caso con las abiertas donde si se deben permitir de catálogo
		/*
		if($this->SectionType==sectDynamic)
		{
			$aField->IsVisible=false;
		}
		*/
		$myFields[$aField->Name] = $aField;
		
		
		
		if (getMDVersion() >= esveFormsV41Func) {
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
			$this->Formula = $this->TranslateVariableQuestionIDsByNumbers($this->Formula);
			//@JAPR
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Formula";
			$aField->Title = translate("Formula");
			//@JAPR 2013-11-12: Corregido un bug, las fórmulas deben ser un TEXT en lugar de un String
			$aField->Type = "LargeString";
			$aField->Size = 5000;
			//$aField->Type = "String";
			//$aField->Size = 255;
			//@JAPR
			/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
			$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(images/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin: 5px 535px" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'Formula\');">';
			$myFields[$aField->Name] = $aField;
		}
		
		$strFormatMask = '<span id="lblFormatMask" name="lblFormatMask" style="display:none">&nbsp;&nbsp;<input type="text" id="FormatMask" name="FormatMask" size=32 maxlength=30 value="'.htmlentities($this->FormatMask).'"></span>';
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FormatMaskID";
		$aField->Title = translate("Format");
		$aField->Type = "Object";
		$aField->VisualComponent = "ComboBox";
		$aField->Options = self::$arrNumericFormats;
		$aField->Size = 30;
		$aField->OnChange = "showHideChkValidation();";
		$aField->AfterMessage = $strFormatMask;
		$myFields[$aField->Name] = $aField;
		//@JAPR 2013-02-22: Corregido un bug, este campo no afecta al grabado así que puede estar habilitado en dinámicas (#28647)
		//Conchita 2013-01-14 si es edicion no habilitar el cambiar question type si es dinamica
		//$aField->IsDisplayOnly = true;
		//@JAPR

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MinValue";
		$aField->Title = translate("Min");
		$aField->Type = "Float";
		$aField->FormatMask = "##0.00";
		$aField->TextAlign = "right";
		//@JAPR 2012-10-12: Corregido un bug, no debe forzarse a un máximo y mínimo de 0
		$aField->DefaultNumberZero = false;
		//@JAPR
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MaxValue";
		$aField->Title = translate("Max");
		$aField->Type = "Float";
		$aField->FormatMask = "##0.00";
		$aField->TextAlign = "right";
		//@JAPR 2012-10-12: Corregido un bug, no debe forzarse a un máximo y mínimo de 0
		$aField->DefaultNumberZero = false;
		//@JAPR
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2012-05-29: Agregada la opción para indicar el número de decimales de las preguntas numéricas
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DecimalPlaces";
		$aField->Title = translate("Decimal places");
		$aField->Type = "Integer";
		$myFields[$aField->Name] = $aField;
		//@JAPR 2013-02-22: Corregido un bug, este campo no afecta al grabado así que puede estar habilitado en dinámicas (#28647)
		//if(!$isNewObj && $this->SectionID==$dynamicSectionID){ //Conchita 2013-01-14 si es edicion no habilitar el cambiar question type si es dinamica
		//	$aField->IsDisplayOnly = true;
		//}
		//@JAPR
		
		if (getMDVersion() >= esveFormsV41Func) {
			//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ReadOnly";
			$aField->Title = translate("Read only?");
			$aField->Type = "Boolean";
			$aField->VisualComponent = "Checkbox";
			$myFields[$aField->Name] = $aField;
			
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
			$this->ShowCondition = $this->TranslateVariableQuestionIDsByNumbers($this->ShowCondition);
			//@JAPR
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ShowCondition";
			$aField->Title = translate("Show condition");
			//@JAPR 2013-11-12: Corregido un bug, las fórmulas deben ser un TEXT en lugar de un String
			$aField->Type = "LargeString";
			$aField->Size = 5000;
			/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
			$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(images/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin: 5px 535px" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'ShowCondition\');">';
			//$aField->Type = "String";
			//$aField->Size = 255;
			//@JAPR
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-02-06: Removidas algunas configuraciones por solicitud de DAbdo
		//Se reacomodaron estas configuraciones porque algunas depende de que no se utilice catálogo, así que es correcto que se capturen después
		//de dicha configuración
		$intLastQuestionNumber = $this->getLastQuestionNumberOfSection();
		//@JAPR 2013-06-26: Corregido un bug, como no se permite utilizar a la misma pregunta como el origen/exclusión salvo en secciones 
		//maestro-detalle, se estaba obteniendo el ID de la última pregunta de la sección actual para obtener sólo aquellas antes de esa, así que
		//en realidad si esta fuera una pregunta nueva, se hubiera excluído a la inmediata anterior. No sólo eso, si la pregunta no fuera nueva, 
		//entonces por el contrario el error consistiría en que se hubieran mostrado todas las preguntas incluso hasta la última de la sección, lo que
		//llevaría a poder depender de preguntas que aun no habían sido contestadas
		if ($this->isNewObject()) {
			//Si es una pregunta nueva, no tiene por qué excluir a la última pregunta de la sección, así que se agregará uno al orden
			$intLastQuestionNumber++;
		}
		else {
			//Si no es una pregunta nueva, entonces se usa el número de esta pregunta
			$intLastQuestionNumber = $this->QuestionNumber;
		}
		//@JAPR
		
		//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		if (getMDVersion() >= esvSimpleChoiceSharing) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "SharedQuestionID";
			$aField->Title = translate("Share response items with the following question");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = BITAMQuestion::GetSurveySimpleAndMultiQuestionsWithAnswersBeforePosition($this->Repository, $this->SurveyID, $intLastQuestionNumber, true);
			$aField->OnChange = "showHideChkValidation();";
			//@JAPR 2014-05-20: Agregado el tipo de sección Inline
			if(!$this->isNewObject()) {
				if ($this->IsSelector) {
					$aField->IsDisplayOnly = true;
				}
			}
			//@JAPR
			$myFields[$aField->Name] = $aField;
		}
		
		$intLastQuestionNumber = $this->getLastQuestionNumberOfSection();
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SourceQuestionID";
		$aField->Title = translate("Obtain response items from the following multiple choice question");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMQuestion::GetSurveyMultiQuestionsBeforePosition($this->Repository, $this->SurveyID, $intLastQuestionNumber, true, $this->isMasterDet());
		$aField->OnChange = "showHideChkValidation();";
		//@JAPR 2014-05-20: Agregado el tipo de sección Inline
		if(!$this->isNewObject()) {
			if ($this->IsSelector) {
				$aField->IsDisplayOnly = true;
			}
		}
		//@JAPR
		$myFields[$aField->Name] = $aField;
		
		$arrMultiQuestionsColl = BITAMQuestion::GetSurveySimpleQuestionsBeforePosition($this->Repository, $this->SurveyID, $intLastQuestionNumber, false, $this->isMasterDet());
		unset($arrMultiQuestionsColl[0]);
		$arrtmpsckeys = array_keys($arrMultiQuestionsColl);
		$arrMCOptions = $aField->Options;
		unset($arrMCOptions[0]);
		$arrtmpmckeys = array_keys($arrMCOptions);
		$arrtmpall = array_merge($arrtmpsckeys, $arrtmpmckeys);
		$arrSimplMultiQuestionsColl = array_merge($arrMultiQuestionsColl, $arrMCOptions);
	    $arrcombined = array();
	    if (count($arrtmpall) != 0 && count($arrSimplMultiQuestionsColl) != 0) {
	  		//$arrcombined = array_merge($arrcombined, array_combine($arrtmpall, $arrSimplMultiQuestionsColl));
			$arrcombined = array_combine($arrtmpall, $arrSimplMultiQuestionsColl);
	    }
	    
		$elementscombined = array();
		$elementscombined[0] = translate("None");
		foreach($arrcombined as $key=>$val) {
			$elementscombined[$key] = $val;
	    }
		$arrSCAndMCQuestions = array();
		$arrSCAndMCQuestions = $elementscombined;
		
		if(!$isNewObj && $this->SectionID==$dynamicSectionID){ //Conchita 2013-01-14 si es edicion no habilitar el cambiar question type si es dinamica
			$aField->IsDisplayOnly = true;
		}
		
		$intLastQuestionNumber = $this->getLastQuestionNumberOfSection();
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SourceSimpleQuestionID";
		$aField->Title = translate("Exclude answer from the following simple or multiple choice question");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrSCAndMCQuestions;
		$aField->OnChange = "showHideChkValidation();";
		//@JAPR 2014-05-20: Agregado el tipo de sección Inline
		if(!$this->isNewObject()) {
			if ($this->IsSelector) {
				$aField->IsDisplayOnly = true;
			}
		}
		//@JAPR
		$myFields[$aField->Name] = $aField;
		if(!$isNewObj && $this->SectionID==$dynamicSectionID){ //Conchita 2013-01-14 si es edicion no habilitar el cambiar question type si es dinamica
			$aField->IsDisplayOnly = true;
		}
        
		//2014.12.24 JCEM se mueve este codigo para resolver el ticket #PR1KVT 
        //@JAPR 2014-02-13: Movido debajo del CatalogID porque si es open numérica de catálogo entonces no se habilita este campo
		//Campo Default value puede contener constantes o formulas para contener un valor calculado
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
		$this->DefaultValue = $this->TranslateVariableQuestionIDsByNumbers($this->DefaultValue);
		//@JAPR
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DefaultValue";
		$aField->Title = translate("Default value");
		$aField->Type = "String";
		$aField->Size = 255;
		/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
		$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(images/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'DefaultValue\');">';
		$myFields[$aField->Name] = $aField;        
        
		//@JAPR 2012-10-29: Agregada la información extra para las acciones
		if (getMDVersion() >= esvExtendedActionsData) {
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
			$this->ActionText = $this->TranslateVariableQuestionIDsByNumbers($this->ActionText);
			//@JAPR
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ActionText";
			$aField->Title = translate("Action description");
			$aField->Type = "LargeString";
			$aField->Size = 5000;
			/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
			$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); margin: 5px 535px; background-image: url(images/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'ActionText\');">';
			$myFields[$aField->Name] = $aField;
			
			//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
			$this->ActionTitle = $this->TranslateVariableQuestionIDsByNumbers($this->ActionTitle);
			//@JAPR
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ActionTitle";
			$aField->Title = translate("Action title");
			$aField->Type = "LargeString";
			$aField->Size = 5000;
			/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
			$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); margin: 5px 535px; background-image: url(images/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'ActionTitle\');">';
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CategoryID";
		$aField->Title = translate("Category");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMCatalog::GetCategories($this->Repository, true);
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ResponsibleID";
		$aField->Title = translate("Responsible");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		//@JAPR 2013-07-15: Agregado el mapeo de Supervisores y MySelf como responsables de acciones desde el servicio
		$userList = BITAMCatalog::GetUsers($this->Repository, true);
		if (getMDVersion() >= esveBavelSuppActions123) {
			//En este caso como es la primera versión que va a incluir esta configuración desde el servidor, se pueden utilizar los valores que
			//sean convenientes, por lo que los valores negativos serán utilizados para representar a los Supervisores, mientras el valor 0
			//representará a MySql (que así es como ya funcionaba de todas formas). Sin embargo debido a que en v4 el valor default para indicador
			//que la pregunta NO tenía un responsable especificado al grabar las definiciones de los móviles era el -1, se tendrá que empezar con
			//el valor -2 para los Supervisores
			$userList[(string) 0] = '('.translate("Myself").')';								//En version >= esveBavelSuppActions123 Se manda 0
			$userList[(string) -2] = '('.translate("Supervisor").' 1)';	//En version >= esveBavelSuppActions123 Realmente se manda -2
			$userList[(string) -3] = '('.translate("Supervisor").' 2)';	//En version >= esveBavelSuppActions123 Realmente se manda -3
			$userList[(string) -4] = '('.translate("Supervisor").' 3)';	//En version >= esveBavelSuppActions123 Realmente se manda -4
		}
		$aField->Options = $userList;
		//@JAPR
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DaysDueDate";
		$aField->Title = translate("Days due date");
		$aField->Type = "Integer";
		//$aField->FormatMask = "##0.00";
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
		$blnAddeBavelDummyFields = true;
		if (getMDVersion() >= esveBavelSuppActions123) {
			//Obtiene la lista de formas de eBavel creadas en el repositorio a partir de la aplicación que se definió en la encuesta
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			$inteBavelAppID = $aSurveyInstance->eBavelAppID;
			if ($inteBavelAppID > 0) {
				$blnAddeBavelDummyFields = false;
				$this->eBavelAppID = $inteBavelAppID;
				$aFieldApps = BITAMFormField::NewFormField();
				$aFieldApps->Name = "eBavelAppID";
				$aFieldApps->Title = translate("eBavel application");
				$aFieldApps->Type = "Object";
				$aFieldApps->VisualComponent = "Combobox";
				$aFieldApps->Options = array($inteBavelAppID => 'eBavel App');
				//$aFieldApps->OnChange = "cleareBavelFields();cleareBavelActionFields();";
				//$aFieldApps->InTitle = true;
				//$aFieldApps->IsVisible = false;
				$myFields[$aFieldApps->Name] = $aFieldApps;
				
				$arreBavelForms = array('' => '('.translate('None').')');
				//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
				require_once('eBavelIntegration.inc.php');
				$arreBavelForms = GetEBavelCollectionByField(@GetEBavelSections($this->Repository, true, $inteBavelAppID), 'sectionName', 'key', true);
				
				$aFieldActionForms = BITAMFormField::NewFormField();
				$aFieldActionForms->Name = "eBavelActionFormID";
				$aFieldActionForms->Title = translate("eBavel action data form");
				$aFieldActionForms->Type = "Object";
				$aFieldActionForms->VisualComponent = "Combobox";
				$aFieldActionForms->Options = $arreBavelForms;
				$aFieldActionForms->OnChange = "cleareBavelActionFields();";
				if (getMDVersion() >= esveBavelSuppActions123) {
					//$aFieldActionForms->AfterMessage = "<span id=\"mapeBavelActionFields\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);background-repeat:no-repeat;background-position:100% 100%;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssigneBavelActionFields();\">".translate("Map action fields")."</span>";
					$aFieldActionForms->AfterMessage = "<span id=\"mapeBavelActionFields\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;
						font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" 
						onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssigneBavelActionFields();\">".
						"<img src=\"btngenerar.gif\" style=\"position:relative; left:0px; top:2px; width:150px; height:16px;\"/ displayMe=1>".
						"<span style=\"position:relative; left:-140px; top:-3px\">".
						translate("Map action fields")."</span></span>";
				}
				else {
					//$aFieldActionForms->InTitle = true;
					//$aFieldActionForms->IsVisible = false;
				}
				//@JAPR
				$myFields[$aFieldActionForms->Name] = $aFieldActionForms;
				//$aFieldActionForms->Parent = $aFieldApps;
				//$aFieldApps->Children[] = $aFieldActionForms;
			}
		}
		//@JAPR
		
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		$strOtherLabelSpan = '';
		if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
			$strDisplay = "";
			if ($this->QTypeID == qtpSingle || $this->QTypeID == qtpMulti) {
				$strDisplay = ' style="display:none"';
			}

			//03-Ene-2014: Validación para que el campo OtherLabel esté activo sólo cuando AllowAdditionalAnswers está seleccionado
			if($this->AllowAdditionalAnswers==0)
			{
				$otherLabelReadOnly = "readonly";
			}
			else
			{
				$otherLabelReadOnly = "";
			}
			
			$strOtherLabelSpan = '<span id="spnOtherLabel" name="spnOtherLabel"'.$strDisplay.'>&nbsp;&nbsp;'.translate("Other label").': <input '.$otherLabelReadOnly.' type="text" id="OtherLabel" name="OtherLabel" value="'.$this->OtherLabel.'" ></span>';
			
		}
		
		//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AllowAdditionalAnswers";
		$aField->Title = translate("Allow entering new answers?");
		$aField->Type = "Boolean";
		$aField->AfterMessage = $strOtherLabelSpan;
		$aField->VisualComponent = "Checkbox";
		$aField->OnChange = "onChangeAllowAdditionalAnswers();";
		//@JAPR 2014-05-20: Agregado el tipo de sección Inline
		if(!$this->isNewObject()) {
			if ($this->IsSelector) {
				$aField->IsDisplayOnly = true;
			}
		}
		$myFields[$aField->Name] = $aField;
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UseCatToFillMC";
		$aField->Title = translate("Use Catalog?");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsGral;
		$aField->OnChange = "showHideCatalogs();";
		if(!$isNewObj)
		{
			if($this->QTypeID==qtpMulti && $this->QDisplayMode==dspVertical && $this->MCInputType==mpcNumeric && $this->IsMultiDimension==0 && $this->UseCategoryDimChoice!=0)
			{
				$aField->IsDisplayOnly = true;
			}
		}
		$myFields[$aField->Name] = $aField;

		//Conchita agregado 10-oct-2011
		//@JAPR 2014-02-07: Removidas algunas configuraciones por solicitud de DAbdo
		/*
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ImportCatOptions";
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		if(!$isNewObj)
		{
			$aField->IsDisplayOnly = true;
			$aField->IsVisible = false;
			$aField->Title = "";
		}
		else 
		{
			$aField->Title = translate("Import Catalog Options");
			$aField->OnChange="filloptionstext()";
		}
        $aField->Options = BITAMCatalog::getCatalogs($this->Repository, true, 1);
        */
        //@JAPR
		$myFields[$aField->Name] = $aField;
		//fin agregado 10-oct

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "StrSelectOptions";
		$aField->Type = "LargeString";
		$aField->Size = 5000;
		if(!$isNewObj)
		{
			$aField->IsDisplayOnly = true;
			$aField->IsVisible = false;
			$aField->Title = "";
		}
		else 
		{
			$aField->Title = translate("Response items")." - ".translate("One per row");
		}
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2011-06-24: Agregado el tipo de despliegue como Matriz
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "StrSelectOptionsCol";
		$aField->Type = "LargeString";
		$aField->Size = 5000;
		if(!$isNewObj)
		{
			$aField->IsDisplayOnly = true;
			$aField->IsVisible = false;
			$aField->Title = "";
		}
		else 
		{
			$aField->Title = translate("Response options")." - ".translate("one per column");
		}
		$myFields[$aField->Name] = $aField;
		//@JAPR
		
		$optionsOneChoicePer = array();
		$optionsOneChoicePer[0] = "Row";
		$optionsOneChoicePer[1] = "Column";

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "OneChoicePer";
		$aField->Title = translate("One choice per");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsOneChoicePer;
		if(!$isNewObj)
		{
			$aField->IsDisplayOnly = true;
		}
		$myFields[$aField->Name] = $aField;
		
		if($this->SectionID!=$dynamicSectionID)
		{
			$optionsUseCategory = $this->getUseCategoryDimensions();
	
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "UseCategoryDimChoice";
			$aField->Title = translate("Use Category Dimension");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $optionsUseCategory;
			$aField->OnChange = "showHideCategoryFields();";
			if(!$isNewObj)
			{
				$aField->IsDisplayOnly = true;
			}
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "CategoryDimName";
			$aField->Title = translate("Category Dimension Name");
			$aField->Type = "String";
			$aField->Size = 255;
			if(!$isNewObj)
			{
				$aField->IsDisplayOnly = true;
			}
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ElementDimName";
			$aField->Title = translate("Element Dimension Name");
			$aField->Type = "String";
			$aField->Size = 255;
			if(!$isNewObj)
			{
				$aField->IsDisplayOnly = true;
			}
			$myFields[$aField->Name] = $aField;
	
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ElementIndName";
			$aField->Title = translate("Element Indicator Name");
			$aField->Type = "String";
			$aField->Size = 255;
			if(!$isNewObj)
			{
				$aField->IsDisplayOnly = true;
			}
			$myFields[$aField->Name] = $aField;
		}

		$catalogField = BITAMFormField::NewFormField();
		$catalogField->Name = "CatalogID";
		$catalogField->Title = translate("Select Catalog");
		$catalogField->Type = "Object";
		$catalogField->VisualComponent = "Combobox";
		$catalogField->OnChange="updateFields()";
		
		$valSectionID=null;
		$valSectionNumber=null;
		if($this->SectionType==sectDynamic || $this->SectionType==sectMasterDet || $this->SectionType==sectInline)
		{
			$valSectionID=$this->SectionID;
		}
		else
		{
			$valSectionID=$this->SectionID;
			$valSectionNumber=$this->SectionNumber;
		}
		
		if(!$isNewObj)
		{
			//$catalogField->IsDisplayOnly = true;
			if($this->QTypeID==qtpMulti)
			{
				
				$catalogField->Options = BITAMCatalog::getCatalogs($this->Repository, false, 1);
			}
			else 
			{	
				$catalogField->Options = BITAMCatalog::getCatalogs($this->Repository, false, null, null, $valSectionID,$valSectionNumber);
			}
		}
		else 
		{
			$catalogField->Options = BITAMCatalog::getCatalogs($this->Repository, false, null, null, $valSectionID,$valSectionNumber);
			
			//@JAPR 2013-06-26: Validado que si es un objeto nuevo, se seleccione por default el primer Atributo disponible
			foreach ($catalogField->Options as $this->CatalogID => $strCatalogName)
			{
				break;
			}
		}
		/*@AAL 12/01/2015 Se valida que en una sección dinámica una pregunta MultipleChoice no muestre
		  el mismo catalogo */
		if ($this->SectionType == sectDynamic) 
			unset($catalogField->Options[$sectionInstance->CatalogID]);
		//@AAL
		
		if(!$canEditFields)
		{
			$catalogField->IsDisplayOnly=true;
		}
		
		$myFields[$catalogField->Name] = $catalogField;
		
		$catmemberField = BITAMFormField::NewFormField();
		$catmemberField->Name = "CatMemberID";
		$catmemberField->Title = translate("Select Attribute");
		$catmemberField->Type = "Object";
		$catmemberField->VisualComponent = "Combobox";
		$catmemberField->Options = $this->getUnusedCatMembersByCatalogID();
		if(!$isNewObj)
		{
			//$catmemberField->IsDisplayOnly = true;
		}
		else {
			//@JAPR 2013-06-26: Validado que si es un objeto nuevo, se seleccione por default el primer Atributo disponible
			if ($this->CatalogID > 0 && isset($catmemberField->Options[$this->CatalogID]) && count($catmemberField->Options[$this->CatalogID]) > 0)
			{
				foreach ($catmemberField->Options[$this->CatalogID] as $this->CatMemberID => $strAttributeName)
				{
					break;
				}
			}
		}
		
		if(!$canEditFields)
		{
			$catmemberField->IsDisplayOnly=true;
		}
		
		$myFields[$catmemberField->Name] = $catmemberField;
		
		$catmemberField->Parent = $catalogField;
		$catalogField->Children[] = $catmemberField;
		
		if (getMDVersion() >= esvNearBy) {
		
			$catmemberLatitudeField = BITAMFormField::NewFormField();
			$catmemberLatitudeField->Name = "CatMemberLatitudeID";
			$catmemberLatitudeField->Title = translate("Select Attribute Latitude");
			$catmemberLatitudeField->Type = "Object";
			$catmemberLatitudeField->VisualComponent = "Combobox";
			$catmemberLatitudeField->Options = $this->getUnusedCatMembersByCatalogID();
			if(!$isNewObj)
			{
				//$catmemberField->IsDisplayOnly = true;
			}
			else {
				//@JAPR 2013-06-26: Validado que si es un objeto nuevo, se seleccione por default el primer Atributo disponible
				if ($this->CatalogID > 0 && isset($catmemberLatitudeField->Options[$this->CatalogID]) && count($catmemberLatitudeField->Options[$this->CatalogID]) > 0)
				{
					foreach ($catmemberLatitudeField->Options[$this->CatalogID] as $this->CatMemberLatitudeID => $strAttributeName)
					{
						break;
					}
				}
			}
			$myFields[$catmemberLatitudeField->Name] = $catmemberLatitudeField;
			$catmemberLatitudeField->Parent = $catalogField;
			$catalogField->Children[] = $catmemberLatitudeField;
			
			$catmemberLongitudeField = BITAMFormField::NewFormField();
			$catmemberLongitudeField->Name = "CatMemberLongitudeID";
			$catmemberLongitudeField->Title = translate("Select Attribute Longitude");
			$catmemberLongitudeField->Type = "Object";
			$catmemberLongitudeField->VisualComponent = "Combobox";
			$catmemberLongitudeField->Options = $this->getUnusedCatMembersByCatalogID();
			if(!$isNewObj)
			{
				//$catmemberField->IsDisplayOnly = true;
			}
			else {
				//@JAPR 2013-06-26: Validado que si es un objeto nuevo, se seleccione por default el primer Atributo disponible
				if ($this->CatalogID > 0 && isset($catmemberLongitudeField->Options[$this->CatalogID]) && count($catmemberLongitudeField->Options[$this->CatalogID]) > 0)
				{
					foreach ($catmemberLongitudeField->Options[$this->CatalogID] as $this->CatMemberLongitudeID => $strAttributeName)
					{
						break;
					}
				}
			}
			$myFields[$catmemberLongitudeField->Name] = $catmemberLongitudeField;
			$catmemberLongitudeField->Parent = $catalogField;
			$catalogField->Children[] = $catmemberLongitudeField;
		}
		
		//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		if (getMDVersion() >= esvGPSQuestion) {
			$arrWhenToSetGPS = array(rgpsAtEnd => translate("When changing to another section"), 1 => translate("When starting data entry in the section"));
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "RegisterGPSAt";
			$aField->Title = translate("REGISTERGPSDATAAT");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrWhenToSetGPS;
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IsFieldSurveyID";
		$aField->Title = translate("Show in report list?");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsGral;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-09-29: Agregado el ancho de columnas en secciones Inline o el Summary de maestro-detalle
		if (getMDVersion() >= esvInlineMerge && ($this->SectionType == sectInline || $this->SectionType == sectMasterDet)) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ColumnWidth";
			$aField->Title = translate("Column width");
			$aField->Type = "Integer";
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IsReqQuestion";
		$aField->Title = translate("Is a required question?");
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		//@JAPR 2014-05-20: Agregado el tipo de sección Inline
		if(!$this->isNewObject()) {
			if ($this->IsSelector) {
				$aField->IsDisplayOnly = true;
			}
		}
		//@JAPR
		$myFields[$aField->Name] = $aField;
		
		//Conchita 2014-12-01 agregada la propiedad isinvisible
		if (getMDVersion() >= esvIsInvisible){
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "IsInvisible";
			$aField->Title = translate("Is an invisible question?");
			$aField->Type = "Boolean";
			$aField->VisualComponent = "Checkbox";
			$myFields[$aField->Name] = $aField;
			
		}
		
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		$strCommentLabelSpan = '';
		if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
			$strDisplay = "";
			if ($this->QTypeID == qtpSingle || $this->QTypeID == qtpMulti) {
				$strDisplay = ' style="display:none"';
			}
			//03-Ene-2014: Validación para que el campo CommentLabel esté activo sólo cuando HasReqComment tenga seleccionada
			//cualquiera de las opciones "Required" o "Optional"
			if($this->HasReqComment==0)
			{
				$commentLabelReadOnly = "readonly";
			}
			else
			{
				$commentLabelReadOnly = "";
			}
			
			$strCommentLabelSpan = '<span id="spnCommentLabel" name="spnCommentLabel"'.$strDisplay.'>&nbsp;&nbsp;'.translate("Comment label").': <input '.$commentLabelReadOnly.' type="text" id="CommentLabel" name="CommentLabel" value="'.$this->CommentLabel.'"></span>';
		}
		//@JAPR
		
		$optionsReq = array();
		$optionsReq[0] = translate("No");
		$optionsReq[1] = translate("Required");
		$optionsReq[2] = translate("Optional");

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "HasReqComment";
		$aField->Title = translate("Requires a comment");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->AfterMessage = $strCommentLabelSpan;
		$aField->Options = $optionsReq;
		$aField->OnChange = "onChangeHasReqComment();";
		//@JAPR 2014-05-20: Agregado el tipo de sección Inline
		if(!$this->isNewObject()) {
			if ($this->IsSelector) {
				$aField->IsDisplayOnly = true;
			}
		}
		//@JAPR
		$myFields[$aField->Name] = $aField;

		$optionsPhoto = array();
		$optionsPhoto[0] = translate("No");
		$optionsPhoto[1] = translate("Required");
		$optionsPhoto[2] = translate("Optional");

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "HasReqPhoto";
		$aField->Title = translate("Requires a photo");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsPhoto;
		//@JAPR 2012-06-14: Ahora se permitirá cambiar la opción de usar Foto durante la edición de preguntas
		/*
		if(!$isNewObj)
		{
			$aField->IsDisplayOnly = true;
		}
		*/
		//@JAPR
		//@JAPR 2014-05-20: Agregado el tipo de sección Inline
		if(!$this->isNewObject()) {
			if ($this->IsSelector) {
				$aField->IsDisplayOnly = true;
			}
		}
		//@JAPR
		$myFields[$aField->Name] = $aField;

		$optionsDocument = array();
		$optionsDocument[0] = "No Document";
		$optionsDocument[1] = "Required";
		$optionsDocument[2] = "Optional";
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "HasReqDocument";
		$aField->Title = translate("Requires a document");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsDocument;
		//@JAPR 2012-06-14: Ahora se permitirá cambiar la opción de usar Foto durante la edición de preguntas
		/*
		if(!$isNewObj)
		{
			$aField->IsDisplayOnly = true;
		}
		*/
		//@JAPR
		$myFields[$aField->Name] = $aField;
		
		//Conchita Agregar GoToQuestion 19-oct-2011
		//@JAPR 2013-04-19: Modificado para que permita saltar a cualquier sección
		//$arrayNextSections = $this->getNextSections();
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		$arrExcludeSectionIDs = array($this->SectionID);
		if (getMDVersion() >= esvSectionQuestions) {
			$arrExcludeSectionIDs = array_merge($arrExcludeSectionIDs, $arrInvalidSections);
		}
		$arrayNextSections = $this->getSections(null, $arrExcludeSectionIDs);
		//@JAPR
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "GoToQuestion";
		$aField->Title = translate("Skip to section");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrayNextSections;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2013-02-21: Agregada la opción para ocultar los textos de opciones de respuesta si tienen imagen asociada
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ShowImageAndText";
		$aField->Title = translate("Show image and text for every answer?");
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		$aField->IsVisible = false;
		if (getMDVersion() >= esvCatalogDissociation) {
			$aField->IsVisible = true;
		}
		//@JAPR 2014-05-20: Agregado el tipo de sección Inline
		if(!$this->isNewObject()) {
			if ($this->IsSelector) {
				$aField->IsDisplayOnly = true;
			}
		}
		//@JAPR
		$myFields[$aField->Name] = $aField;
		
		$this->get_FormField_ShowInSummary($myFields);
		
		//@JAPR 2014-06-03: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice
		if (getMDVersion() >= esvMCHSwitchBehav) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "EnableCheckBox";
			$aField->Title = translate("Enable checkbox for entry");
			$aField->Type = "Boolean";
			$aField->VisualComponent = "Checkbox";
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "SwitchBehaviour";
			$aField->Title = translate("Switch default behaviour when selecting");
			$aField->Type = "Boolean";
			$aField->VisualComponent = "Checkbox";
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-10-22: Agregada la opción para redibujar las preguntas tipo mensaje automáticamente
		if (getMDVersion() >= esvInlineTypeOfFilling) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "AutoRedraw";
			$aField->Title = translate("Auto redraw");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = array(0 => translate('No'), 1 => translate('Yes'));
			if (!$isNewObj && $this->QTypeID != qtpMessage) {
				$aField->IsVisible = false;
			}
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		$objSectionColl = BITAMSectionCollection::NewInstanceEmpty($this->Repository, $this->SurveyID);
		if (getMDVersion() >= esvSectionQuestions) {
			$objSectionColl = BITAMSectionCollection::NewInstance($this->Repository, $this->SurveyID);
			
			//La implementación inicial de esta funcionalidad sólo consideraba secciones Inline
			$arrInlineSections = array(0 => '('.translate("None").')');
			foreach ($objSectionColl->Collection as $objSection) {
				//Se debe validar que sólo se pueda mapear una vez cada sección como pregunta tipo qtpSection, adicionalmente si se está
				//editando esta pregunta, se puede agregar la misma sección que ya tenía mapeada
				$blnValidSection = true;
				if (in_array($objSection->SectionID, $arrInvalidSections) && ($this->QTypeID != qtpSection || $this->SourceSectionID != $objSection->SectionID)) {
					$blnValidSection = false;
				}
				
				if ($objSection->SectionType == sectInline && $blnValidSection) {
					$arrInlineSections[$objSection->SectionID] = $objSection->SectionName;
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "SourceSectionID";
			$aField->Title = translate("Section");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrInlineSections;
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		if(!$isNewObj)
		{
			//14Marzo2013: Que no puedas cambiar una pregunta simple choice de catálogo 
			//a una sección dinámica existente en la encuesta
			
			$arrayExcept = array();
			//@JAPR 2013-12-09: Validado que algunos repositorios si permitan esta funcionalidad
			if (trim($_SESSION["PABITAM_RepositoryName"])=="fbm_bmd_") {
				//En este caso estos repositorios si podrán hacer uso de esta funcionalidad
			}
			else {
				if($this->QTypeID==qtpSingle && $this->UseCatalog==1)
				{
					//Obtener la sección dinámica si existe, si no existe retornará cero
					$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
					
					if($dynamicSectionID>0)
					{
						//Sí existe sección dinámica
						$arrayExcept[0] = $dynamicSectionID;
					}
				}
			}
			
			//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
			//Si la pregunta es tipo qtpSection, no se puede cambiar a otra sección que tenga múltiples registros
			if (getMDVersion() >= esvSectionQuestions && $this->QTypeID == qtpSection) {
				if (!is_null($objSectionColl)) {
					foreach ($objSectionColl->Collection as $objSection) {
						if ($objSection->SectionType != sectNormal) {
							$arrayExcept[] = $objSection->SectionID;
						}
					}
				}
			}
			//@JAPR
			
			//14Feb2013: Se agregró el cuarto parámetro para que no incluya las secciones formateadas
			//@JAPR 2015-01-13: Corregido un bug, las preguntas tipo sección no se deben poder mover dentro de otra sección múltiple (#XWKDHA)
			//Modificado el parámetro $includeSecFormated por $arrExcepSectionTypes para permitir especificar que tipos de sección no se deben regresar
			$arrSectionTypeExcluded = array(sectFormatted);
			$sectionOptions = BITAMSection::getSectionsFromSurvey($this->Repository, $this->SurveyID, $arrayExcept, $arrSectionTypeExcluded);
			//@JAPR
			if(count($sectionOptions)>0)
			{
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "SectionID";
				$aField->Title = translate("Change Section");
				$aField->Type = "Object";
				$aField->VisualComponent = "Combobox";
				$aField->Options = $sectionOptions;
				//@JAPR 2014-05-20: Agregado el tipo de sección Inline
				if(!$this->isNewObject()) {
					if ($this->IsSelector) {
						$aField->IsDisplayOnly = true;
					}
				}
				//@JAPR
				
				if(!$canEditFields)
				{
					$aField->IsDisplayOnly=true;
				}
				
				$myFields[$aField->Name] = $aField;
			}
			if($this->SectionID==$dynamicSectionID){ //Conchita 2013-01-14 si es edicion no habilitar el cambiar question type si es dinamica
				$aField->IsDisplayOnly = true;
			}
		}

		//@JAPR 2012-06-15: Agregada la opción para configurar el tipo de teclado en las preguntas numéricas
		$optVisualStyle = array();
		$optVisualStyle[0] = "Numeric keypad";
		$optVisualStyle[1] = "Telephone keypad";
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AvailNumericVisualStyles";
		//$aField->Title = translate("Available selection display modes");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optVisualStyle;
		//$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		$optVisualStyle = array();
		$optVisualStyle[0] = "Native";
		$optVisualStyle[1] = "Mobile enhanced";
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AvailMenuVisualStyles";
		//$aField->Title = translate("Available selection display modes");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optVisualStyle;
		//$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2012-05-14: Movidos todos los campos ocultos al final para que no deje el espacio en blanco que se acumula cuando hay muchos juntos
		//Agrega formfields dummy con los tipos de captura según el tipo de pregunta para hacer el llenado de la combo de configuración
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AvailSelDisplayModes";
		//$aField->Title = translate("Available selection display modes");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMQuestion::getQuestionDisplayModes(qtpSingle, $isNewObj, $this->QDisplayMode);
		//$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AvailEntryDisplayModes";
		//$aField->Title = translate("Available entry display modes");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMQuestion::getQuestionDisplayModes(qtpOpenString, $isNewObj, $this->QDisplayMode);
		//$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AvailPhotoDisplayModes";
		//$aField->Title = translate("Available entry display modes");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMQuestion::getQuestionDisplayModes(qtpPhoto, $isNewObj, $this->QDisplayMode);
		//$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogSCID";
		//$aField->Title = translate("Select Catalog");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		//@JAPR 2015-04-01: Corregido un bug, no se había validado a la par esta combo que se usa para modificar el contenido de los catálogos
		//dentro del javascript, faltaba especificar el parámetro $valSectionNumber
		$aField->Options = BITAMCatalog::getCatalogs($this->Repository, false, null, null, $valSectionID,$valSectionNumber);
		//$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;

		/*@AAL 12/01/2015 Se valida que en una sección dinámica una pregunta MultipleChoice no muestre
		  el mismo catalogo */
		if ($this->SectionType == sectDynamic) 
			unset($aField->Options[$sectionInstance->CatalogID]);
		//@AAL

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogMCID";
		//$aField->Title = translate("Select Catalog");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMCatalog::getCatalogs($this->Repository, false, 1);
		//$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogSVID";
		//$aField->Title = translate("Select Catalog");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrayUniqueCatalog;
		//$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2013-07-12: Agregado el mapeo de una forma de eBavel para acciones
		if ($blnAddeBavelDummyFields) {
			//Por facilidad al mostrar/ocultar componentes, se agregará este campo aunque su valor realmente no aplicará y estará invisible
			$this->eBavelAppID = 0;
			$aFieldApps = BITAMFormField::NewFormField();
			$aFieldApps->Name = "eBavelAppID";
			//$aFieldApps->Title = translate("eBavel application");
			$aFieldApps->Type = "Object";
			$aFieldApps->VisualComponent = "Combobox";
			$aFieldApps->Options = array(0 => 'eBavel App');
			//$aFieldApps->InTitle = true;
			$aFieldApps->IsVisible = false;
			$myFields[$aFieldApps->Name] = $aFieldApps;
			
			$aFieldActionForms = BITAMFormField::NewFormField();
			$aFieldActionForms->Name = "eBavelActionFormID";
			//$aFieldActionForms->Title = translate("eBavel action data form");
			$aFieldActionForms->Type = "Object";
			$aFieldActionForms->VisualComponent = "Combobox";
			$aFieldActionForms->Options = array(0 => 'eBavel Form');
			//$aFieldActionForms->InTitle = true;
			$aFieldActionForms->IsVisible = false;
			$myFields[$aFieldActionForms->Name] = $aFieldActionForms;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "eBavelActionFieldsData";
			//$aField->Title = "eBavel action fields mapping";
			$aField->Type = "LargeString";
			$aField->Size = 5000;
			//$aField->InTitle = true;
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "eBavelActionFieldsData";
		//$aField->Title = "eBavel action fields mapping";
		$aField->Type = "LargeString";
		$aField->Size = 5000;
		//$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-08-18: Corregido un bug, esta validación aplica sólo si es una sección tipo Recap, ya que de lo contrario deshabilita
		//la funcionalidad de edición de preguntas para todas las demás, además aunque se valide en este punto, es necesario crear funciones
		//adicionales para cuando es Recap en una sección nueva, ya que esta validación no podrá controlar dicho caso en el servicio
		if (getMDVersion() >= esvSectionRecap) {
			if (!$isNewObj && $this->SectionType == sectRecap) {
				foreach($myFields as $aField) {
					if($aField->Name != "GQTypeID" && $aField->Name != "QDataType" && $aField->OnChange != "") {
						$aField->OnChange = "";
					}
				}
			}
		}
		//@JAPR
			//Conchita agregado el sketch image con responsive design
		if (getMDVersion() >= esvSketchImage) {
			//@JAPR 2014-08-04: Agregado el Responsive Design
			$arrDevices = array();
		
			$arrDevices[] = getDeviceNameFromID(dvciPod);
			$arrDevices[] = getDeviceNameFromID(dvciPad);
			$arrDevices[] = getDeviceNameFromID(dvciPadMini);
			$arrDevices[] = getDeviceNameFromID(dvciPhone);
			$arrDevices[] = getDeviceNameFromID(dvcCel);
			$arrDevices[] = getDeviceNameFromID(dvcTablet);
			
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "QuestionImageSketch";
			$aField->Title = translate("Image for Sketch");
			$aField->Type = "LargeStringHTML";
			$aField->HTMLFormatButtons = "image";
			//@JAPR 2014-08-04: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign) {
				$aField->MultiDeviceNames = $arrDevices;
			}
			//@JAPR
			$aField->Size = 5000;
			$myFields[$aField->Name] = $aField;
			
		
		}
		//Conchita	
		
		//Sep-2014
		if (getMDVersion() >= esvEditorHTML)
		{
			if($isNewObj)
			{
				$arrEditorType[0] = "Tinymce";
				$arrEditorType[1] = translate("Image Editor");
			}
			else
			{
				if($this->EditorType==0)
				{
					$arrEditorType[0] = "Tinymce";
				}
				else
				{
					$arrEditorType[1] = translate("Image Editor");
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "EditorType";
			$aField->Title = translate("Editor");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrEditorType;
			if($isNewObj) $aField->OnChange = "onChangeEditor(true)";
			else $aField->OnChange = "onChangeEditor()";
			$myFields[$aField->Name] = $aField;
		}
			
		return $myFields;
	}
	
	function getUseCategoryDimensions()
	{
		$optionsUseCategory = array();
		$optionsUseCategory[0] = "No";
		$optionsUseCategory[-1] = "[New]";

		$sql = "SELECT CategoryDimID, CategoryDimName FROM SI_SV_Question 
				WHERE QTypeID = ".qtpMulti." AND MCInputType = ".mpcNumeric." AND IsMultiDimension <> 1 
				AND UseCategoryDim = 1 AND SurveyID = ".$this->SurveyID;

		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while(!$aRS->EOF)
		{
			$intCatDimID = (int)$aRS->fields["categorydimid"];
			$catDimName = $aRS->fields["categorydimname"];
			
			$optionsUseCategory[$intCatDimID] = $catDimName;
			
			$aRS->MoveNext();
		}
		
		return $optionsUseCategory;
	}
	
	static function getAllCategoryDimQuestionsInfo($aRepository, $surveyID)
	{
		global $gblEFormsNA;

		$catDimQuestionsInfo = array();
		$noApplyValuesCatDim = array();
		$noApplyValuesElemDim = array();
		
		$sql = "SELECT QuestionID, CategoryDimID, ElementDimID, ElementIndID FROM SI_SV_Question 
				WHERE QTypeID = ".qtpMulti." AND MCInputType = ".mpcNumeric." AND IsMultiDimension <> 1 AND UseCategoryDim = 1 
				AND SurveyID = ".$surveyID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while(!$aRS->EOF)
		{
			$questionID = (int)$aRS->fields["questionid"];
			$categoryDimID = (int)$aRS->fields["categorydimid"];
			$elementDimID = (int)$aRS->fields["elementdimid"];
			$elementIndID = (int)$aRS->fields["elementindid"];
			
			if(!isset($noApplyValuesCatDim[$categoryDimID]))
			{
				$dimFieldKey = "RIDIM_".$categoryDimID."KEY";
				$dimTableName = "RIDIM_".$categoryDimID;
				$dimFieldDesc = "DSC_".$categoryDimID;
				
				//Obtenemos el ID con el que se inserto el valor de No Aplica para una dimension determinada
				$sql = "SELECT ".$dimFieldKey." FROM ".$dimTableName." WHERE ".$dimFieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA);
				
				$aRSExist = $aRepository->DataADOConnection->Execute($sql);
				
				if(!$aRSExist)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
		
				if(!$aRSExist->EOF)
				{
					$noApplyKey = $aRSExist->fields[strtolower($dimFieldKey)];
				}
				else 
				{
					$noApplyKey = 1;
				}
				
				$noApplyValuesCatDim[$categoryDimID] = $noApplyKey;
			}

			if(!isset($noApplyValuesElemDim[$elementDimID]))
			{
				$dimFieldKey = "RIDIM_".$elementDimID."KEY";
				$dimTableName = "RIDIM_".$elementDimID;
				$dimFieldDesc = "DSC_".$elementDimID;
				
				//Obtenemos el ID con el que se inserto el valor de No Aplica para una dimension determinada
				$sql = "SELECT ".$dimFieldKey." FROM ".$dimTableName." WHERE ".$dimFieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA);
				
				$aRSExist = $aRepository->DataADOConnection->Execute($sql);
				
				if(!$aRSExist)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
		
				if(!$aRSExist->EOF)
				{
					$noApplyKey = $aRSExist->fields[strtolower($dimFieldKey)];
				}
				else 
				{
					$noApplyKey = 1;
				}
				
				$noApplyValuesElemDim[$elementDimID] = $noApplyKey;
			}
			
			$catDimQuestionsInfo[$questionID] = array();
			$catDimQuestionsInfo[$questionID]["CategoryDimID"] = $categoryDimID;
			$catDimQuestionsInfo[$questionID]["ElementDimID"] = $elementDimID;
			$catDimQuestionsInfo[$questionID]["ElementIndID"] = $elementIndID;

			$catDimQuestionsInfo[$questionID]["CategoryDimIDNA"] = $noApplyValuesCatDim[$categoryDimID];
			$catDimQuestionsInfo[$questionID]["ElementDimIDNA"] = $noApplyValuesElemDim[$elementDimID];
			
			$aRS->MoveNext();
		}
		
		return $catDimQuestionsInfo;
	}
	
	//Obtiene la lista de preguntas que se pueden usar como el campo Element de las preguntas tipo Acción. Se excluirán las que son:
	//Preguntas de secciones dinámicas, formateadas o Maestro - Detalle (ya que puede haber múltiples capturas para estas)
	//Preguntas que son de tipo Múltiple, Foto, Acción o Firma (ya que hay o multiples respuestas, o ninguna respuesta, y en el caso de las Action,
	//se pudiera terminar agregando como Element a la misma acción que se le asocia por lo que tampoco se considerarían)
	//Preguntas de categoría de dimensión porque también incluyen varios registros
	static function GetQuestionsForActionElement($aRepository, $surveyID, $includeNone)
	{
		$arrQuestions = array();
		if($includeNone==true)
		{
			$arrQuestions[0] = translate("None");
		}

		if ($surveyID <= 0)
		{
			return $arrQuestions;
		}
		//Conchita 2014-09-05 agregado el tipo sketch que es similar a la firma
		$sql = "SELECT A.QuestionID, A.QuestionText FROM SI_SV_Question A, SI_SV_Section B 
			WHERE A.SectionID = B.SectionID AND A.SurveyID = ".$surveyID." AND B.SectionType = 0 AND 
				A.QTypeID NOT IN (".qtpMulti.",".qtpPhoto.",".qtpAction.",".qtpSketch.",".qtpSignature.") AND (A.UseCategoryDim <= 0 OR A.UseCategoryDim IS NULL)
			ORDER BY QuestionNumber";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question, SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$questionID = (int) $aRS->fields["questionid"];
			$questionText = (string) $aRS->fields["questiontext"];
			$arrQuestions[$questionID] = $questionText;
			$aRS->MoveNext();
		}
		
		return $arrQuestions;
	}

	//@JAPR 2012-06-01: Agregado el soporte para creación de preguntas (single y multi) cuyas respuestas dependan de las seleccionadas de una pregunta multi previa
	static function GetSurveyMultiQuestionsBeforePosition($aRepository, $surveyID, $QuestionNumber, $includeNone, $isMasterDet)
	{
		$arrQuestions = array();
		if($includeNone==true)
		{
			$arrQuestions[0] = translate("None");
		}
		if (!isset($QuestionNumber) || is_null($QuestionNumber))
		{
			$QuestionNumber = 0;
		}
		$QuestionNumber = abs($QuestionNumber);
		
		if ($surveyID <= 0 || $QuestionNumber <= 0)
		{
			return $arrQuestions;
		}
		
		if($isMasterDet) {
			$sql = "SELECT A.QuestionID, A.QuestionText 
				FROM SI_SV_Question A 
				WHERE A.SurveyID = ".$surveyID." AND 
					A.QTypeID = ".qtpMulti." AND A.QuestionNumber <= ".$QuestionNumber." 
				ORDER BY A.QuestionNumber";
		} else {
			$sql = "SELECT A.QuestionID, A.QuestionText 
				FROM SI_SV_Question A 
				WHERE A.SurveyID = ".$surveyID." AND 
					A.QTypeID = ".qtpMulti." AND A.QuestionNumber < ".$QuestionNumber." 
				ORDER BY A.QuestionNumber";
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$questionID = (int) $aRS->fields["questionid"];
			$questionText = (string) $aRS->fields["questiontext"];
			$arrQuestions[$questionID] = $questionText;
			$aRS->MoveNext();
		}
		
		return $arrQuestions;
	}
	//@JAPR
	
	static function GetSurveySimpleQuestionsBeforePosition($aRepository, $surveyID, $QuestionNumber, $includeNone, $isMasterDet)
	{
		$arrQuestions = array();
		if($includeNone==true)
		{
			$arrQuestions[0] = translate("None");
		}
		if (!isset($QuestionNumber) || is_null($QuestionNumber))
		{
			$QuestionNumber = 0;
		}
		$QuestionNumber = abs($QuestionNumber);
		
		if ($surveyID <= 0 || $QuestionNumber <= 0)
		{
			return $arrQuestions;
		}
		
		if ($isMasterDet) {
			$sql = "SELECT A.QuestionID, A.QuestionText 
				FROM SI_SV_Question A 
				WHERE A.SurveyID = ".$surveyID." AND 
					A.QTypeID = ".qtpSingle." AND A.QuestionNumber <= ".$QuestionNumber." 
				ORDER BY A.QuestionNumber";
		} else {
			$sql = "SELECT A.QuestionID, A.QuestionText 
				FROM SI_SV_Question A 
				WHERE A.SurveyID = ".$surveyID." AND 
					A.QTypeID = ".qtpSingle." AND A.QuestionNumber < ".$QuestionNumber." 
				ORDER BY A.QuestionNumber";
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$questionID = (int) $aRS->fields["questionid"];
			$questionText = (string) $aRS->fields["questiontext"];
			$arrQuestions[$questionID] = $questionText;
			$aRS->MoveNext();
		}
		
		return $arrQuestions;
	}
	
	//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
	/* Regresa la colección combinada de preguntas de tipo simple y múltiple choice que pueden tener opciones de respuesta (sin considerar a las
	de catálogo) para permitir configurar con cual de ellas se compartirán las opciones.
	*/
	static function GetSurveySimpleAndMultiQuestionsWithAnswersBeforePosition($aRepository, $surveyID, $QuestionNumber, $includeNone, $bSourceQuestionsOnly = true)
	{
		$arrQuestions = array();
		if($includeNone==true)
		{
			$arrQuestions[0] = translate("None");
		}
		if (!isset($QuestionNumber) || is_null($QuestionNumber))
		{
			$QuestionNumber = 0;
		}
		$QuestionNumber = abs($QuestionNumber);
		
		if ($surveyID <= 0 || $QuestionNumber <= 0)
		{
			return $arrQuestions;
		}
		
		$strAdditionalFilters = '';
		if ($bSourceQuestionsOnly) {
			$strAdditionalFilters .= " AND (A.SharedQuestionID <= 0 OR A.SharedQuestionID IS NULL)";
		}
		$sql = "SELECT A.QuestionID, A.QuestionText, A.QuestionNumber 
			FROM SI_SV_Question A 
			WHERE A.SurveyID = ".$surveyID." AND 
				A.QTypeID IN (".qtpSingle.", ".qtpMulti.") AND A.QuestionNumber < ".$QuestionNumber." {$strAdditionalFilters} 
			ORDER BY A.QuestionNumber";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$questionID = (int) $aRS->fields["questionid"];
			$questionText = (string) $aRS->fields["questionnumber"].'. '.(string) $aRS->fields["questiontext"];
			$arrQuestions[$questionID] = $questionText;
			$aRS->MoveNext();
		}
		
		return $arrQuestions;
	}
	
	//@JAPR 2013-04-15: Corregido un bug, esta funcionalidad ya no era válida en v4, donde de hecho el mismo campo se usó para
	//las preguntas skip to section
	//@JAPREliminar: Descontinuada
	function isLastQuestionInThisSection()
	{
		$isLast = true;
		
		$sql = "SELECT QuestionID FROM SI_SV_Question 
				WHERE SurveyID = ".$this->SurveyID." AND SectionID = ".$this->SectionID." 
				ORDER BY QuestionNumber DESC";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$questionid = (int)$aRS->fields["questionid"];
			
			if($this->QuestionID==$questionid)
			{
				$isLast = true;
			}
			else 
			{
				$isLast = false;
			}
		}
		
		return $isLast;
	}
	
	function getNextSections()
	{
		$arrayNextSections = array();
		$arrayNextSections[0] = "None";
		
		$sql = "SELECT SectionID, SectionNumber, SectionName FROM SI_SV_Section 
				WHERE SurveyID = ".$this->SurveyID." AND SectionNumber > ".$this->SectionNumber." ORDER BY SectionNumber";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$sectionid = (int)$aRS->fields["sectionid"];
			$sectionname = $aRS->fields["sectionname"];
			$arrayNextSections[$sectionid] = $sectionname;

			$aRS->MoveNext();
		}
		
		return $arrayNextSections;
	}
	
	//@JAPR 2013-04-19: Modificado para que permita saltar a cualquier sección
	//Regresa una lista de todas las secciones incluyendo/excluyendo las especificadas
	function getSections($arrIncludeIDs = null, $arrExcludeIDs = null)
	{
		$arrayNextSections = array();
		$arrayNextSections[0] = translate("None");
		
		$strWhere = '';
		$strAnd = 'AND ';
		if (!is_null($arrIncludeIDs)) {
			if (is_array($arrIncludeIDs)) {
				if (count($arrIncludeIDs) > 0) {
					$strWhere .= $strAnd.'SectionID IN ('.implode(',', $arrIncludeIDs).')';
				}
			}
			else {
				$strWhere .= $strAnd.'SectionID = '.(int) $arrIncludeIDs;
			}
		}
		
		if (!is_null($arrExcludeIDs)) {
			if (is_array($arrExcludeIDs)) {
				if (count($arrExcludeIDs) > 0) {
					$strWhere .= $strAnd.'SectionID NOT IN ('.implode(',', $arrExcludeIDs).')';
				}
			}
			else {
				$strWhere .= $strAnd.'SectionID <> '.(int) $arrExcludeIDs;
			}
		}
		
		$sql = "SELECT SectionID, SectionNumber, SectionName FROM SI_SV_Section 
				WHERE SurveyID = ".$this->SurveyID." $strWhere ORDER BY SectionNumber";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$sectionid = (int)$aRS->fields["sectionid"];
			$sectionname = $aRS->fields["sectionname"];
			$arrayNextSections[$sectionid] = $sectionname;

			$aRS->MoveNext();
		}
		
		return $arrayNextSections;
	}
	//@JAPR
	
	//funcion para obtener las siguientes preguntas Agregada por Conchita 18-oct-2011
	function getNextQuestions()
	{
		$arrayNextQuestions = array();
		$arrayNextQuestions[0] = "None";
		$arrayNextQuestions[-1] = "End Survey";
		
		$aQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$questionNumber = $aQuestion->QuestionNumber;
		$surveyID = $aQuestion->SurveyID;
		
		$sql = "SELECT QuestionID, QuestionNumber, QuestionText FROM SI_SV_Question 
				WHERE SurveyID = ".$surveyID." AND QuestionNumber > ".$questionNumber." ORDER BY QuestionNumber";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$theQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $theQuestion->SurveyID);

		while(!$aRS->EOF)
		{
			$questionid = (int)$aRS->fields["questionid"];
			$questiontext = $aRS->fields["questionnumber"].". ".$aRS->fields["questiontext"];
			$aQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $questionid);
			//si la pregunta esta en la seccion dinamica, solo podra saltar 
			//en preguntas que se encuentrren dentro de la misma seccion dinamica
			if($theQuestion->SectionID == $dynamicSectionID && $aQuestion->SectionID == $dynamicSectionID) {
					$arrayNextQuestions[$questionid] = $questiontext;
			} else {
				// si la pregunta no esta en la seccion dinamica
				//entonces no debe poder hacer un salto a preguntas dentro de la seccion dinamica
				if($aQuestion->SectionID != $dynamicSectionID) {
					$arrayNextQuestions[$questionid] = $questiontext;
				}
			}
			$aRS->MoveNext();
		}
		
		return $arrayNextQuestions;
	}
	//fin funcion
	
	//Por el momento esta funcion se llama para obtener los atributos de una pregunta de seleccion simple
	//siempre y cuando esta pregunta sea nueva, en caso contrario, no vamos a llamar a esta funcion
	//se llamaria a la anterior
	//Realizamos instancia de survey para obtener el catalogo el cual tenemos que revisar si hay q filtrar
	//los atributos del catalogo si se utilizaron en otra ocasion
	function getUnusedCatMembersByCatalogID()
	{
		//@JAPR 2014-02-14: Removida la configuración del catálogo de la encuesta
		//Ahora generará un array indexado por catálogo, con la información del atributo anterior y posterior usado. Si no hay alguna pregunta
		//de dicho catálogo entonces se asume que se pueden usar todos los atributos, de lo contrario ver el comentario previo al código final
		//para entender como funciona la validación
		//$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
		//$catalogID = $surveyInstance->CatalogID;
		$arrCatalogIDs = array();
		//Este array tiene dos elementos:
		//	'prev' contiene el último atributo usado antes de la posición de esta pregunta
		//	'post' contiene el siguiente atributo usado después de la posición de esta pregunta
		//si falta alguno de los dos, se asumirá que no hay restricción hacia ese lado
		$arrCatMembersByCatalog = array();
		$countQuestion = 0;
		$memberOrder = 0;
		$memberOrderLast = 999;
		
		//Primero obtiene todos los catálogos utilizados en la encuesta
		$sql = "SELECT DISTINCT CatalogID 
			FROM SI_SV_Question 
			WHERE QTypeID = ".qtpSingle." AND UseCatalog = 1 AND CatalogID > 0";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF) {
			$catalogID = (int) @$aRS->fields["catalogid"];
			if ($catalogID > 0) {
				$arrCatalogIDs[] = $catalogID;
				$arrCatMembersByCatalog[$catalogID] = array();
			}
			$aRS->MoveNext();
		}
		
		//if($catalogID!=0)
		//Recorre todos los catálogos involucrados en la encuesta para obtener los atributos disponibles
		foreach ($arrCatalogIDs as $catalogID)
		{
			//Ya no hay necesidad de este query porque todos los catálogos que entren aquí obviamente tienen al menos una pregunta
			$countQuestion = 1;
			/*
			//Verificamos si hay más preguntas de catalogo en la encuesta
			$sql = "SELECT COUNT(QuestionID) AS CountQuestion FROM SI_SV_Question 
					WHERE SurveyID = ".$this->SurveyID." AND QTypeID = ".qtpSingle." AND UseCatalog = 1 AND CatalogID = ".$catalogID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if (!$aRS->EOF)
			{
				$countQuestion = (int)$aRS->fields["countquestion"];
			}
			*/
			
			if ($countQuestion>0)
			{
				//Obtenemos el MemberOrder
				if($this->isNewObject())
				{
					//Obtenemos la ultima pregunta antes de esta nueva en la cual se tenga una pregunta de seleccion simple
					//con el mismo catalogid definido en la encuesta
					$sql = "SELECT A.CatMemberID, C.MemberOrder FROM SI_SV_Question A, SI_SV_Section B, SI_SV_CatalogMember C 
							WHERE A.SurveyID = ".$this->SurveyID." AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1 
							AND A.CatalogID = ".$catalogID." AND A.SectionID = B.SectionID 
							AND B.SectionNumber <= ".$this->SectionNumber." AND A.CatMemberID = C.MemberID 
							ORDER BY A.QuestionNumber DESC";
				}
				else 
				{
					//Obtenemos la ultima pregunta antes de esta nueva en la cual se tenga una pregunta de seleccion simple
					//con el mismo catalogid definido en la encuesta
					$sql = "SELECT A.CatMemberID, B.MemberOrder FROM SI_SV_Question A, SI_SV_CatalogMember B 
							WHERE A.SurveyID = ".$this->SurveyID." AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1 
							AND A.CatalogID = ".$catalogID." AND A.QuestionNumber < ".$this->QuestionNumber." 
							AND A.CatMemberID = B.MemberID 
							ORDER BY A.QuestionNumber DESC";
				}
				
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question, SI_SV_Section, SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$memberOrder = 0;
				if (!$aRS->EOF)
				{
					//Obtenemos el MemberOrder
					$memberOrder = (int) @$aRS->fields["memberorder"];
				}
				$arrCatMembersByCatalog[$catalogID]['prev'] = $memberOrder;
				
				//Obtenemos el MemberOrderLast
				if($this->isNewObject())
				{
					//Obtenemos la primera pregunta después de esta nueva en la cual se tenga una pregunta de seleccion simple
					//con el mismo catalogid definido en la encuesta
					$sql = "SELECT A.CatMemberID, C.MemberOrder FROM SI_SV_Question A, SI_SV_Section B, SI_SV_CatalogMember C 
							WHERE A.SurveyID = ".$this->SurveyID." AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1 
							AND A.CatalogID = ".$catalogID." AND A.SectionID = B.SectionID 
							AND B.SectionNumber > ".$this->SectionNumber." AND A.CatMemberID = C.MemberID 
							ORDER BY A.QuestionNumber ASC";
				}
				else 
				{
					//Obtenemos la primera pregunta después de esta nueva en la cual se tenga una pregunta de seleccion simple
					//con el mismo catalogid definido en la encuesta
					$sql = "SELECT A.CatMemberID, B.MemberOrder FROM SI_SV_Question A, SI_SV_CatalogMember B 
							WHERE A.SurveyID = ".$this->SurveyID." AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1 
							AND A.CatalogID = ".$catalogID." AND A.QuestionNumber > ".$this->QuestionNumber." 
							AND A.CatMemberID = B.MemberID 
							ORDER BY A.QuestionNumber ASC";
				}
				
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question, SI_SV_Section, SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$memberOrderLast = 999;
				if (!$aRS->EOF)
				{
					//Obtenemos el MemberOrderLast
					$memberOrderLast = (int) @$aRS->fields["memberorder"];
				}
				$arrCatMembersByCatalog[$catalogID]['post'] = $memberOrderLast;
			}
		}
		
		//@JAPR 2014-02-14: Removida la configuración del catálogo de la encuesta
		//Si no hay preguntas de catálogo ya creadas en la encuesta, entonces el array de validaciones viene vacio, por tanto todos los atributos
		//son válidos para usar
		
		//Obtiene todos los atributos que están disponibles entre el anterior y el siguiente usado a la posición donde se está creando/editando
		//la pregunta, si no hay uno después entonces se obtienen hasta el último disponible, si no hay uno antes entonces se obtiene desde el
		//primer atributo existente (en el ciclo previo se generó un Array indexado por catálogo con dicha información)
		$sql = "SELECT CatalogID, MemberID, MemberName, MemberOrder FROM SI_SV_CatalogMember ORDER BY CatalogID, MemberOrder";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();
		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["catalogid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["catalogid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["catalogid"];
					$aGroup = array();
				}
				
				$memberID = (int)$aRS->fields["memberid"];
				$memberName = $aRS->fields["membername"];
				$tmpOrder = (int)$aRS->fields["memberorder"];
				
				//Si se esta procesando el catalogo de la encuesta, entonces si revisamos 
				//que atributos se deben de ignorar por si hubo preguntas anteriores 
				//que se crearon con dicho catalogo
				//@JAPR 2014-02-14: Removida la configuración del catálogo de la encuesta
				//Ahora se preguntará por cualquier catálogo usado en la encuesta, ya que no existe mas el catálogo de la encuesta
				//if($lastID==$catalogID)
				if (isset($arrCatMembersByCatalog[$lastID]))
				{
					$memberOrder = (int) @$arrCatMembersByCatalog[$lastID]['prev'];
					$memberOrderLast = @$arrCatMembersByCatalog[$lastID]['post'];
					if (is_null($memberOrderLast)) {
						$memberOrderLast = 999;
					}
					
					//Si el orden es mayor al obtenido de las preguntas de catalogo
					//y tambien que el orden sea menor a sus preguntas posteriores
					//entonces si se agrega el atributo en caso contrario lo ignora
					if($tmpOrder>$memberOrder && $tmpOrder<$memberOrderLast)
					{
						$aGroup[$memberID] = $memberName;
					}
				}
				else 
				{
					$aGroup[$memberID] = $memberName;
				}

  				$aRS->MoveNext();

			}while (!$aRS->EOF);
			
			$allValues[$lastID] = $aGroup;
		}

		return $allValues;
	}
	
	static function getCatMembersByCatalogID($aRepository)
	{		
		$sql = "SELECT CatalogID, MemberID, MemberName, MemberOrder FROM SI_SV_CatalogMember ORDER BY CatalogID, MemberOrder";

		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();

		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["catalogid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["catalogid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["catalogid"];
					$aGroup = array();
				}
				
				$aGroup[$aRS->fields["memberid"]] = $aRS->fields["membername"];
  				$aRS->MoveNext();

			}while (!$aRS->EOF);
			
			$allValues[$lastID] = $aGroup;
		}

		return $allValues;
	}
	
	function generateInsideFormCode($aUser)
	{
		//Obtener secciones de la encuesta
		$sql = "SELECT t1.SectionID, t1.SectionName
				FROM SI_SV_Section t1
				WHERE t1.SurveyID = ".$this->SurveyID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$aSections = array();
		
?>
		<script language="JavaScript">
		var htmlOptionsBtn = '';
		aButtonElements[0]= "Exit";
<?			
		
		while (!$aRS->EOF)
		{
			$aSections[(int)$aRS->fields["sectionid"]]=$aRS->fields["sectionname"];
?>
			aButtonElements[<?=(int)$aRS->fields["sectionid"]?>] = '<?=str_replace("'", "\'", $aRS->fields["sectionname"])?>';
			htmlOptionsBtn += '<option value="<?=(int)$aRS->fields["sectionid"]?>"><?=str_replace("'", "\'", $aRS->fields["sectionname"])?></option>';
<?			
			$aRS->MoveNext();
		}
?>
		</script>
<?		
		include("formattedTextQuestion.php");
	}
	
	function insideEdit() {
		?>
		var qimage = document.getElementById("questionImageTextArea");
		var qimagedisable = document.getElementById("disableQuestionImage");
		qimagedisable.style.zIndex = 0;
		qimagedisable.style.display = "none";
		<?
		
		//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
		if (getMDVersion() >= esveBavelSuppActions123 && ((int) @$this->eBavelAppID) > 0) {
?>
		objSpanChange = document.getElementById("mapeBavelActionFields");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground.gif)';
		objSpanChange.style.cursor = 'pointer';
		objSpanChange.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChange.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChange.onclick = new Function("openAssigneBavelActionFields();");
<?
		}
	}
	
	function insideCancel() {
		?>
		var qimage = document.getElementById("questionImageTextArea");
		var qimagedisable = document.getElementById("disableQuestionImage");
		qimagedisable.style.zIndex = 100;
		qimagedisable.style.display = "block";
		<?
		
		//@JAPR 2013-07-04: Agregado el mapeo de una forma de eBavel para acciones
		if (getMDVersion() >= esveBavelSuppActions123 && ((int) @$this->eBavelAppID) > 0) {
?>
		objSpanChange = document.getElementById("mapeBavelActionFields");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground_off.gif)';
		objSpanChange.style.cursor = 'default';
		objSpanChange.onmouseover = "";
		objSpanChange.onmouseout = "";
		objSpanChange.onclick = "";
<?
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
		$myFormName = get_class($this);
		$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
		if($this->SectionID==$dynamicSectionID)
		{
			$insideDynamicSection = 1;
		}
		else 
		{
			$insideDynamicSection = 0;
		}
		
		$curSectionInstance = BITAMSection::NewInstanceWithID($this->Repository, $this->SectionID);
		$curSectionType = $curSectionInstance->SectionType;
		
?>
		<!--<script language="javascript" src="js/dialogs.js"></script>-->
		<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
 		<script language="JavaScript">
		var myServerPath='<?=(((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";?>'; 		
 		var sectionType = <?=$curSectionType?>;
 		var insideDynamicSection = <?=$insideDynamicSection?>;
		var mbHideMinMaxForNumericCatalog = false;
		
		function refreshCatalogs(iQTypeID, iCatalogID)
 		{
 			var objSourceCombo, objTargetCombo, objOption, objSourceOption, objDefOption;
 			var intIndex, intMaxElements, intDimID, intDefValue;
 			
 			objTargetCombo = <?=$myFormName?>_SaveForm.CatalogID;
 			if (objTargetCombo == null)
 			{
 				return;
 			}
 			
 			switch(iQTypeID)
 			{
  				case 2:
		 			objSourceCombo = <?=$myFormName?>_SaveForm.CatalogSCID;
 					break;
 				case 3:
		 			objSourceCombo = <?=$myFormName?>_SaveForm.CatalogMCID;
 					break;
 				default:
		 			objSourceCombo = <?=$myFormName?>_SaveForm.CatalogSVID;
 					break;
 			}
 		
			if (objSourceCombo == null)
			{
				return;
			}
			
			objDefOption = null;
			intMaxElements = objSourceCombo.options.length;
			if (intMaxElements >= 0)
			{
				objTargetCombo.options.length = 0;
				objDefOption = objSourceCombo.options.item(0);
			}
			for(intIndex=0; intIndex < intMaxElements; intIndex++)
			{
				objSourceOption = objSourceCombo.options.item(intIndex);
				objOption = document.createElement("OPTION");
				objTargetCombo.options.add(objOption);
				objOption.value = objSourceOption.value;
				objOption.text = objSourceOption.text;

				if (objOption.value == iCatalogID)
				{
					objTargetCombo.selectedIndex = intIndex;
					objDefOption = null;
				}
			}
			
			if (objDefOption != null)
			{
				if (intMaxElements > 0)
				{
					objTargetCombo.selectedIndex = 0;
				}
			}
			<?=$myFormName?>_CatMemberID_Update();
 		}

 		function refreshDisplayMode(iQTypeID, iDisplayMode)
 		{
 			var objSourceCombo, objTargetCombo, objOption, objSourceOption, objDefOption;
 			var intIndex, intMaxElements, intDimID, intDefValue;
 			
 			objTargetCombo = <?=$myFormName?>_SaveForm.QDisplayMode;
 			if (objTargetCombo == null)
 			{
 				return;
 			}

 			switch(iQTypeID)
 			{
 				case 2:
 				case 3:
 				case 17:
 					intDefValue = 2;
		 			objSourceCombo = <?=$myFormName?>_SaveForm.AvailSelDisplayModes;
 					break;
 				case 8:
 					intDefValue = 4;
 					objSourceCombo = <?=$myFormName?>_SaveForm.AvailPhotoDisplayModes;
 					break;
 				default:
 					intDefValue = 4;
		 			objSourceCombo = <?=$myFormName?>_SaveForm.AvailEntryDisplayModes;
 					break;
 			}
			
			if (objSourceCombo == null)
			{
				return;
			}
			
			objDefOption = null;
			intMaxElements = objSourceCombo.options.length;
			if (intMaxElements > 0)
			{
				objTargetCombo.options.length = 0;
				objDefOption = objSourceCombo.options.item(0);
			}
			for(intIndex=0; intIndex < intMaxElements; intIndex++)
			{
				objSourceOption = objSourceCombo.options.item(intIndex);
				
				//Validacion temporal para no mostrar la matriz en multiple choice
				if(!(iQTypeID==3 && objSourceOption.value==3) && !(iQTypeID==2 && objSourceOption.value==6))
				{
					objOption = document.createElement("OPTION");
					objTargetCombo.options.add(objOption);
					objOption.value = objSourceOption.value;
					objOption.text = objSourceOption.text;
					if (objOption.value == iDisplayMode)
					{
						objTargetCombo.selectedIndex = intIndex;
						objDefOption = null;
					}
					else
					{
						if (objOption.value == intDefValue && objTargetCombo.selectedIndex == -1)
						{
							objDefOption = objOption;
						}
					}
				}
			}
			
			if (objDefOption != null)
			{
				objDefOption.selected = true;
			}
			
			/*@AAL 03/02/2015 Se valida que al editar una pregunta Simple Choice que use mapa 
			  no se permita o muestre el Entry display mode. Issue: #BSM9I6 */
			var useMapselect = document.getElementsByName("UseMap")[0];
			var useMapValue = 0;
			if (useMapselect) {
				var useMapValue = useMapselect.selectedIndex;
			}
	 		var rowQDisplayMode = document.getElementById("Row_QDisplayMode");
	 		rowQDisplayMode.style.display = ((iQTypeID == 2 || iQTypeID == 3) && !useMapValue)?((bIsIE)?"inline":"table-row"):"none";
	 		//@AAL
	 		//rowQDisplayMode.style.display = (iQTypeID == 8 || iQTypeID == 9 || iQTypeID == 10 || iQTypeID == 11 || iQTypeID == 13 || iQTypeID == 14 || iQTypeID == 15 || iQTypeID == 16 || iQTypeID == 17 || iQTypeID == 18)?"none":((bIsIE)?"inline":"table-row");

<?
		if (getMDVersion() >= esvSyncQuestionAndSkipFoto) {
?>
			if (iQTypeID == 8) {
				rowQDisplayMode.style.display = ((bIsIE)?"inline":"table-row");
			}
<?
		}
?>
 		}
 		
 		function refreshVisualStyle(iQTypeID, iQDataType, iQDisplayMode, iVisualStyle)
 		{
 			var objSourceCombo, objTargetCombo, objOption, objSourceOption, objDefOption;
 			var intIndex, intMaxElements, intDimID, intDefValue;
 			
 			objTargetCombo = <?=$myFormName?>_SaveForm.QComponentStyleID;
 			if (objTargetCombo == null)
 			{
 				return;
 			}
			
			intDefValue = 0;
 			if (iQTypeID == 0 && iQDataType == 1)
 			{
	 			objSourceCombo = <?=$myFormName?>_SaveForm.AvailNumericVisualStyles;
 			}
 			else
 			{
	 			objSourceCombo = <?=$myFormName?>_SaveForm.AvailMenuVisualStyles;
 			}
			
			if (objSourceCombo == null)
			{
				return;
			}
			
			objDefOption = null;
			intMaxElements = objSourceCombo.options.length;
			if (intMaxElements > 0)
			{
				objTargetCombo.options.length = 0;
				objDefOption = objSourceCombo.options.item(0);
			}
			for(intIndex=0; intIndex < intMaxElements; intIndex++)
			{
				objSourceOption = objSourceCombo.options.item(intIndex);
				
				objOption = document.createElement("OPTION");
				objTargetCombo.options.add(objOption);
				objOption.value = objSourceOption.value;
				objOption.text = objSourceOption.text;
				if (objOption.value == iVisualStyle)
				{
					objTargetCombo.selectedIndex = intIndex;
					objDefOption = null;
				}
				else
				{
					if (objOption.value == intDefValue && objTargetCombo.selectedIndex == -1)
					{
						objDefOption = objOption;
					}
				}
			}
			
			if (objDefOption != null)
			{
				objDefOption.selected = true;
			}
			
	 		var rowQComponentStyleID = document.getElementById("Row_QComponentStyleID");
	 		rowQComponentStyleID.style.display = ((iQTypeID == 2 && iQDisplayMode == 2) || (iQTypeID == 0 && iQDataType == 1))?((bIsIE)?"inline":"table-row"):"none";
 		}
		
		function showHideMapAtributes() {
			var objCmb = <?=$myFormName?>_SaveForm.GQTypeID;
	 		var qTypeID = parseInt(objCmb.options[objCmb.selectedIndex].value);
			var rowUseMap = document.getElementById("Row_UseMap");
			rowUseMap.style.display = (qTypeID != 2)?"none":((bIsIE)?"inline":"table-row");
			var useMapselect = document.getElementsByName("UseMap")[0];
			//var useMapValue = useMapselect.options[useMapselect.selectedIndex].label;
			var useMapValue = 0;
			if (useMapselect) {
				var useMapValue = useMapselect.selectedIndex;
			}
			//var showMap = (useMapValue == 'Yes' && rowUseMap.style.display != "none") ? true : false;
			var showMap = (useMapValue && rowUseMap.style.display != "none") ? true : false;
			if(showMap) {
				document.getElementsByName("typeOfFillingSC")[0].selectedIndex = 1;
				<?=$myFormName?>_SaveForm.UseCatalog.selectedIndex = 1;
				showHideCatalogs();
			} else {
			
			}
			var rowQDisplayMode = document.getElementById("Row_QDisplayMode");
			rowQDisplayMode.style.display = (showMap)?"none":((bIsIE)?"inline":"table-row");
			var rowTypeOfFillingSC = document.getElementById("Row_typeOfFillingSC");
			rowTypeOfFillingSC.style.display = (showMap)?"none":((bIsIE)?"inline":"table-row");
			var rowCatMemberLatitudeID = document.getElementById("Row_CatMemberLatitudeID");
			rowCatMemberLatitudeID.style.display = (showMap)?((bIsIE)?"inline":"table-row"):"none";
			var rowCatMemberLongitudeID = document.getElementById("Row_CatMemberLongitudeID");
			rowCatMemberLongitudeID.style.display = (showMap)?((bIsIE)?"inline":"table-row"):"none";
			var rowMapRadio = document.getElementById("Row_MapRadio");
			rowMapRadio.style.display = (showMap)?((bIsIE)?"inline":"table-row"):"none";
		}
		
		var types = {
			open: 0,
			simplechoice: 2,
			multiplechoice: 3,
			photo: 8,
			signature: 10,
			calculated: 14,
			geolocation: 18
		};
		
		var opentype = {
			number: 1,
			date: 4,
			text: 5,
			alphanum: 6,
			hour: 12
		};
				
		var rows = {
			/*UseMap: "UseMap",
			MapRadio: "MapRadio",
			QDisplayMode: "QDisplayMode",
			MCInputType: "MCInputType",
			MaxChecked: "MaxChecked",
			IsMultiDimension: "IsMultiDimension",
			QDataType: "QDataType",
			QComponentStyleID: "QComponentStyleID",
			IsIndicator: "IsIndicator",
			eBavelFieldID: "eBavelFieldID",
			SummaryTotal: "SummaryTotal",
			DateFormatMaskID: "DateFormatMaskID",
			QLength: "QLength",
			TextDisplayStyle: "TextDisplayStyle",
			PictLayOut: "PictLayOut",
			InputWidth: "InputWidth",
			typeOfFillingSC: "typeOfFillingSC",
			typeOfFillingMC: "typeOfFillingMC",
			UseCatalog: "UseCatalog",
			DefaultValue: "DefaultValue",
			Formula: "Formula",
			FormatMaskID: "FormatMaskID",
			MinValue: "MinValue",
			MaxValue: "MaxValue",
			DecimalPlaces: "DecimalPlaces",
			ReadOnly: "ReadOnly",
			ShowCondition: "ShowCondition",
			SharedQuestionID: "SharedQuestionID",
			SourceQuestionID: "SourceQuestionID",
			SourceSimpleQuestionID: "SourceSimpleQuestionID",
			ActionText: "ActionText",
			ActionTitle: "ActionTitle",
			CategoryID: "CategoryID",
			ResponsibleID: "ResponsibleID",
			DaysDueDate: "DaysDueDate",
			eBavelAppID: "eBavelAppID",
			eBavelActionFormID: "eBavelActionFormID",
			AllowAdditionalAnswers: "AllowAdditionalAnswers",
			UseCatToFillMC: "UseCatToFillMC",
			StrSelectOptions: "StrSelectOptions",
			StrSelectOptionsCol: "StrSelectOptionsCol",
			OneChoicePer: "OneChoicePer",
			UseCategoryDimChoice: "UseCategoryDimChoice",
			CategoryDimName: "CategoryDimName",
			ElementDimName: "ElementDimName",
			ElementIndName: "ElementIndName",
			CatalogID: "CatalogID",
			CatMemberID: "CatMemberID",
			CatMemberLatitudeID: "CatMemberLatitudeID",
			CatMemberLongitudeID: "CatMemberLongitudeID",
			RegisterGPSAt: "RegisterGPSAt",
			IsFieldSurveyID: "IsFieldSurveyID",
			IsReqQuestion: "IsReqQuestion",
			HasReqComment: "HasReqComment",
			HasReqPhoto: "HasReqPhoto",
			HasReqDocument: "HasReqDocument",
			GoToQuestion: "GoToQuestion",
			ShowImageAndText: "ShowImageAndText",
			ShowInSummary: "ShowInSummary",
			EnableCheckBox: "EnableCheckBox",
			SwitchBehaviour: "SwitchBehaviour",
			AvailNumericVisualStyles: "AvailNumericVisualStyles",
			AvailMenuVisualStyles: "AvailMenuVisualStyles",
			AvailSelDisplayModes: "AvailSelDisplayModes",
			AvailEntryDisplayModes: "AvailEntryDisplayModes",
			AvailPhotoDisplayModes: "AvailPhotoDisplayModes",
			CatalogSCID: "CatalogSCID",
			CatalogMCID: "CatalogMCID",
			CatalogSVID: "CatalogSVID",
			eBavelActionFieldsData: "eBavelActionFieldsData",
			SectionID: "SectionID"*/
		};
		
		var rowsWithEvents = [
			//"QDataType"
		];
		
		function hideRowAll () {
			hideRows(rows);
		}
		
		function hideRows (arrayRows, removeEvents) {
			showHideRows(arrayRows, true, removeEvents);
		}
		
		function hideRow (strRow, removeEvents) {
			showHideRow(strRow, true, removeEvents);
		}
		
		function showRows (arrayRows) {
			showHideRows(arrayRows);
		}
		
		function showRow (strRow) {
			showHideRow(strRow);
		}
		
		function removeElementEvents (strRow) {
			if (rowsWithEvents.indexOf(strRow) >= 0) {
				return;
			}
			
			var rowInput = getRowInput(strRow);
			rowInput.setAttribute('onchange', '');
		}
		
		function showHideRow(strRow, hide, removeEvents) {
			var rowDOM = getRow(strRow);
			if(rowDOM) {
				if(hide) {
					rowDOM.style.display = "none";
				} else {
					rowDOM.style.display = ((bIsIE)?"inline":"table-row");
				}
				if(removeEvents) {
					removeElementEvents(strRow);
				}
			}
		}
		
		function showHideRows (arrayRows, hide, removeEvents) {
			for(var krow in arrayRows) {
				if(typeof arrayRows[krow] == 'string') {
					var strRow = arrayRows[krow];
					if(rows[strRow]) {
						if(hide) {
							hideRow(rows[strRow], removeEvents);
						} else {
							showRow(rows[strRow]);
						}
					}
				}
			}
		}
		
		function getRow (strRow) {
			return document.getElementById("Row_"+strRow);
		}
		
		function getRowInput (strRow) {
			return document.getElementsByName(strRow)[0];
		}
		
		function showHideRecap_Open_OnChange () {
			hideRowAll();
			showRows([
				rows.QDataType
			]);
			var objCmb = <?=$myFormName?>_SaveForm.QDataType;
			var valueDataType = parseInt(objCmb.options[objCmb.selectedIndex].value);
			switch(valueDataType) {
				case opentype.number:
					showRows([
						rows.QComponentStyleID,
						rows.FormatMaskID,
						rows.MinValue,
						rows.MaxValue,
						rows.DecimalPlaces,
					]);
					break;
				case opentype.date:
					showRows([
						rows.DateFormatMaskID
					]);
					break;
				case opentype.text:
					showRows([
						rows.QLength
					]);
					break;
				default:
					break;
			}
			showRows([
				rows.DefaultValue,
				rows.eBavelFieldID
			]);
			
			setEBQTypeID(0, valueDataType);
		}
		
		function showHideRecapValidation_Open () {
			var qDataType = getRowInput(rows.QDataType);
			showHideRecap_Open_OnChange();
			qDataType.setAttribute('onchange', 'showHideRecap_Open_OnChange()');
		}
		
		function getDOMRows () {
			var tmpRows = [];
			var trRows = $("tr[id^='Row']");
			var trRowsLength = trRows.length;
			for(var krow = 0; krow < trRowsLength; krow++) {
				var aRow = trRows[krow];
				var rowID = $(aRow).attr('id').replace('Row_', '');
				if(rowID != 'GQTypeID' && rowID != 'QuestionText' && rowID != 'AttributeName') {
					tmpRows[rowID] = rowID;
				}
			}
			return tmpRows;
		}
		
		function showHideRecapValidation () {
			if($.isEmptyObject(rows)) {
				rows = getDOMRows();
			}
			hideRowAll();
			var objCmb = <?=$myFormName?>_SaveForm.GQTypeID;
			var qTypeID = parseInt(objCmb.options[objCmb.selectedIndex].value);
 			qDataType = 0;
			switch(qTypeID) {
				case types.open:
		 			var objCmb = <?=$myFormName?>_SaveForm.QDataType;
		 			qDataType = parseInt(objCmb.options[objCmb.selectedIndex].value);
					showHideRecapValidation_Open();
					break;
				default:
					break;
			}
			showRows([
				rows.eBavelFieldID
			]);
			
			setEBQTypeID(qTypeID, qDataType);
		}
 		
 		function showHideChkValidation()
 		{
 			var intMDVersion = parseFloat('<?=getMDVersion()?>');
 			var intEForms401v = parseFloat('<?=esveFormsV41Func?>');
			var esvCatalogAttributeImage = parseFloat('<?=esvCatalogAttributeImage?>');
<?
			//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
			//Modificado para agrupar los tipos de preguntas abiertas en uno sólo
	 		//var objCmb = $myFormName_SaveForm.QTypeID;
			//@JAPR 2012-05-10: Validado que la opción de ComponentStyle sólo aplique para preguntas sencillas tipo Menu
			//@JAPR 2014-02-18: Corregido un bug con el Cancel de la forma, estaba reseteando el catálogo, así que ahora en esta función lo que
			//hará es que mientras aún esté activada la bandera de cancel luego de un reset, reemplazará el ID del catálogo en el array la primera
			//vez, ya que al hacer eso automáticamente limpia la bandera cancel pero como ya estaría seleccionado el valor correcto, ya no importa
			//las ejecuciones subsecuentes
            //2015.02.04 JCEM #NAEUJY Si la section es inline(4) no se permite hasreqphoto, hasreqcomment ni add answr
?>
			if (cancel==true) {
				var objCatalogID = <?=$myFormName?>_SaveForm.CatalogID;
				if (objCatalogID && initialSelectionCatalogID != null) {
					for(i=0;i<objCatalogID.options.length;i++) {
						if (objCatalogID.options[i].value==initialSelectionCatalogID) {
							objCatalogID.selectedIndex=i;
							break;
						}
					}
				}
				
				var objQDisplayMode = <?=$myFormName?>_SaveForm.QDisplayMode;
				if (objQDisplayMode && initialSelectionQDisplayMode != null) {
					for(i=0;i<objQDisplayMode.options.length;i++) {
						if (objQDisplayMode.options[i].value==initialSelectionQDisplayMode) {
							objQDisplayMode.selectedIndex=i;
							break;
						}
					}
				}
				
				var objQComponentStyleID = <?=$myFormName?>_SaveForm.QComponentStyleID;
				if (objQComponentStyleID && initialSelectionQComponentStyleID != null) {
					for(i=0;i<objQComponentStyleID.options.length;i++) {
						if (objQComponentStyleID.options[i].value==initialSelectionQComponentStyleID) {
							objQComponentStyleID.selectedIndex=i;
							break;
						}
					}
				}
			}
			
			if(sectionType == 5) {
				showHideRecapValidation();
				return;
			}

			var objCmb = <?=$myFormName?>_SaveForm.SourceQuestionID;
			var iSourceQuestionID = parseInt(objCmb.options[objCmb.selectedIndex].value);

			var objCmb = <?=$myFormName?>_SaveForm.FormatMaskID;
			var iFormatMaskID = parseInt(objCmb.options[objCmb.selectedIndex].value);
      
      		var objCmb = <?=$myFormName?>_SaveForm.DateFormatMaskID;
      		var iDateFormatMaskID = parseInt(objCmb.options[objCmb.selectedIndex].value);

	 		var objCmb = <?=$myFormName?>_SaveForm.QDisplayMode;
	 		var qDisplayMode = parseInt(objCmb.options[objCmb.selectedIndex].value);
			
	 		var objCmb = <?=$myFormName?>_SaveForm.QComponentStyleID;
	 		var qComponentStyleID = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		
	 		var objCmb = <?=$myFormName?>_SaveForm.GQTypeID;
	 		var qTypeID = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		var qDataType = 1;
	 		//Conchita 2013-04-29 agregado tipo 17 callList
	 		if(qTypeID == 17 || qTypeID == 2 || qTypeID == 3 || qTypeID == 8 || qTypeID == 9 || qTypeID == 10 || qTypeID == 11 || qTypeID == 13 || qTypeID == 14 || qTypeID == 15 || qTypeID == 16 || qTypeID == 18 || qTypeID == 19 || qTypeID == 20 || qTypeID == 21 || qTypeID == 22 || qTypeID == 23)
	 		{
	 			qDataType = 0;
	 			if (qTypeID != 2 || qDisplayMode == 3)
	 			{
	 				iSourceQuestionID = 0;
	 			}
	 		}
	 		else
	 		{
	 			var objCmb = <?=$myFormName?>_SaveForm.QDataType;
	 			qDataType = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		}
	 		
	 		//Obtengo valor de mcinputtype
	 		var objCmb = <?=$myFormName?>_SaveForm.MCInputType;
	 		var mcinputtype = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		
	 		//Obtengo valor de ismultidimension
	 		var objCmb = <?=$myFormName?>_SaveForm.IsMultiDimension;
	 		var ismultidimension = parseInt(objCmb.options[objCmb.selectedIndex].value);

	 		refreshDisplayMode(qTypeID, qDisplayMode);
	 		refreshVisualStyle(qTypeID, qDataType, qDisplayMode, qComponentStyleID)
	 		//Actualiza el qDisplayMode ya que al entrar al refresh previo, si se cambiaba el qTypeID quedaba un qDisplayMode incompatible
	 		//en ocasiones durante el resto de la función actual
	 		var objCmb = <?=$myFormName?>_SaveForm.QDisplayMode;
	 		var qDisplayMode = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		
	 		var rowQDataType = document.getElementById("Row_QDataType");
	 		//@AAL Si el qTypeID también es 25 no se debe mostrar el Row_QDataType
	 		rowQDataType.style.display = (qDataType == 0 || qTypeID == 25)?"none":((bIsIE)?"inline":"table-row");
	 		var rowQLength = document.getElementById("Row_QLength");
	 		//@AAL Si el qTypeID es 25 también se debe mostrar el Row_QLength
	 		rowQLength.style.display = ((qTypeID == 0 && qDataType == 6) || qTypeID == 25)?((bIsIE)?"inline":"table-row"):"none";
	 		var rowTextDisplayStyle = document.getElementById("Row_TextDisplayStyle");
	 		rowTextDisplayStyle.style.display = (qDataType != 7)?"none":((bIsIE)?"inline":"table-row");
			
			<?
			if (getMDVersion() >= esvNearBy) {
			?>
				var rowUseMap = document.getElementById("Row_UseMap");
				rowUseMap.style.display = (qTypeID != 2)?"none":((bIsIE)?"inline":"table-row");
                
                var useMapselect = document.getElementsByName("UseMap")[0];
                var useMapValue = 0;
                if (useMapselect) {
                	var useMapValue = useMapselect.selectedIndex;
                }
    			var showMap = (useMapValue && rowUseMap.style.display != "none") ? true : false;
               
	
				var rowCatMemberLatitudeID = document.getElementById("Row_CatMemberLatitudeID");
				rowCatMemberLatitudeID.style.display = (showMap)?((bIsIE)?"inline":"table-row"):"none";
				var rowCatMemberLongitudeID = document.getElementById("Row_CatMemberLongitudeID");
				rowCatMemberLongitudeID.style.display = (showMap)?((bIsIE)?"inline":"table-row"):"none";
				var rowMapRadio = document.getElementById("Row_MapRadio");
				rowMapRadio.style.display = (showMap)?((bIsIE)?"inline":"table-row"):"none";
			<?
			}
			?>
			
	 		//var rowQComponentStyleID = document.getElementById("Row_QComponentStyleID");
	 		//rowQComponentStyleID.style.display = (qTypeID != 2 || qDisplayMode != 2)?"none":((bIsIE)?"inline":"table-row");
	 		//Conchita agregados el width y height de las tipo sketch
			<?
			if (getMDVersion() >= esvSketch) {
			?>
				var rowWidth = document.getElementById("Row_canvasWidth");
				rowWidth.style.display = (qTypeID != 23)?"none":((bIsIE)?"inline":"table-row"); 
				var rowHeight = document.getElementById("Row_canvasHeight");
				rowHeight.style.display = (qTypeID != 23)?"none":((bIsIE)?"inline":"table-row"); 
			<?
			}
			?>
			//Conchita agregado el questionimagesketch
			<?
			if (getMDVersion() >= esvSketchImage) {
			?>
				var rowWidth = document.getElementById("Row_QuestionImageSketch");
				rowWidth.style.display = (qTypeID != 23)?"none":((bIsIE)?"inline":"table-row"); 
			<?
			}
			?>
			
			
	 		var rowSectionID = document.getElementById("Row_SectionID");
	 		if (rowSectionID != undefined)
	 		{ //Conchita 2014-09-05 agregado el 23 tipo sketch
	 			rowSectionID.style.display = (qTypeID == 8 || qTypeID == 9 || qTypeID == 10 || qTypeID == 23 || qTypeID == 15 || qTypeID == 20 || qTypeID == 21 || qTypeID == 22)?"none":((bIsIE)?"inline":"table-row");
	 		}
	 		var rowCategoryID = document.getElementById("Row_CategoryID");
	 		rowCategoryID.style.display = (qTypeID != 9)?"none":((bIsIE)?"inline":"table-row");
	 		var rowResponsibleID = document.getElementById("Row_ResponsibleID");
	 		rowResponsibleID.style.display = (qTypeID != 9)?"none":((bIsIE)?"inline":"table-row");
	 		var rowDaysDueDate = document.getElementById("Row_DaysDueDate");
	 		rowDaysDueDate.style.display = (qTypeID != 9)?"none":((bIsIE)?"inline":"table-row");
	 		var roweBavelActionFieldsData = document.getElementById("Row_eBavelActionFieldsData");
	 		roweBavelActionFieldsData.style.display = "none";
<?
		if (getMDVersion() >= esvExtendedActionsData) {
?>
	 		var rowActionText = document.getElementById("Row_ActionText");
	 		if (rowActionText != null) {
		 		rowActionText.style.display = (qTypeID != 9)?"none":((bIsIE)?"inline":"table-row");
	 		}
	 		var rowActionTitle = document.getElementById("Row_ActionTitle");
	 		if (rowActionTitle != null) {
		 		rowActionTitle.style.display = (qTypeID != 9)?"none":((bIsIE)?"inline":"table-row");
	 		}
<?
		}
		
		if (getMDVersion() >= esveBavelSuppActions123 && ((int) @$this->eBavelAppID) > 0) {
?>
	 		var roweBavelAppID = document.getElementById("Row_eBavelAppID");
	 		roweBavelAppID.style.display = "none";
	 		//roweBavelAppID.style.display = (qTypeID != 9)?"none":((bIsIE)?"inline":"table-row");
	 		var roweBavelActionFormID = document.getElementById("Row_eBavelActionFormID");
	 		roweBavelActionFormID.style.display = (qTypeID != 9)?"none":((bIsIE)?"inline":"table-row");
	 		var spanmapeBavelActionFields = document.getElementById("mapeBavelActionFields");
			if(qTypeID == 9) {
				spanmapeBavelActionFields.style.display = 'inline';
			}
			else {
				spanmapeBavelActionFields.style.display = 'none';
			}
<?
		}
			//@JAPR
?>
	 		//var spanIsIndicator = document.getElementById("lblIsIndicator");
			var spanIsIndicator = document.getElementById("Row_IsIndicator");
			var rowMCInputType = document.getElementById("Row_MCInputType");
	 		var rowUseCatalog = document.getElementById("Row_UseCatalog");
	 		var rowUseCatToFillMC = document.getElementById("Row_UseCatToFillMC");
			//var rowImportCatOptions = document.getElementById("Row_ImportCatOptions");
	 		var rowIsMultiDimension = document.getElementById("Row_IsMultiDimension");
	 		var rowStrSelectOptions = document.getElementById("Row_StrSelectOptions");
	 		var rowStrSelectOptionsCol = document.getElementById("Row_StrSelectOptionsCol");
	 		var rowOneChoicePer = document.getElementById("Row_OneChoicePer");
	 		var rowCatalogID = document.getElementById("Row_CatalogID");
	 		var rowCatMemberID = document.getElementById("Row_CatMemberID");
	 		var rowMinValue = document.getElementById("Row_MinValue");
	 		var rowMaxValue = document.getElementById("Row_MaxValue");
	 		var rowPictLayOut = document.getElementById("Row_PictLayOut");
	 		var rowInputWidth = document.getElementById("Row_InputWidth")
	 		var rowEnableCheckBox = document.getElementById("Row_EnableCheckBox")
	 		var rowSwitchBehaviour = document.getElementById("Row_SwitchBehaviour")
	 		
	 		var rowMaxChecked = document.getElementById("Row_MaxChecked");
	 		
			var rowFormatMaskID = document.getElementById("Row_FormatMaskID");
			var rowFormatMask = document.getElementById("lblFormatMask");
      
      		var rowDateFormatMaskID = document.getElementById("Row_DateFormatMaskID");
			var rowDateFormatMask = document.getElementById("lblDateFormatMask");
			
			var rowDefaultValue = document.getElementById("Row_DefaultValue");
      		var rowReadOnly = document.getElementById("Row_ReadOnly");
	 		var rowAllowAdditionalAnswers = document.getElementById("Row_AllowAdditionalAnswers");
	 		var rowShowImageAndText = document.getElementById("Row_ShowImageAndText");
	 		var rowSourceQuestionID = document.getElementById("Row_SourceQuestionID");
	 		var rowtypeOfFillingSC = document.getElementById("Row_typeOfFillingSC");
	 		var rowtypeOfFillingMC = document.getElementById("Row_typeOfFillingMC");

			var rowHasReqPhoto = document.getElementById("Row_HasReqPhoto");
			var rowHasReqDocument = document.getElementById("Row_HasReqDocument");
			var rowHasReqComment = document.getElementById("Row_HasReqComment");

			if(sectionType == 4 || qTypeID == 11 || qTypeID == 13 || qTypeID == 16 || qTypeID == 18 || qTypeID == 19 || qTypeID == 20) { //#NAEUJY  
				rowHasReqPhoto.style.display = "none";
			} else {
				rowHasReqPhoto.style.display = ((bIsIE)?"inline":"table-row");
			}
 			if (intMDVersion >= intEForms401v) {
				rowHasReqDocument.style.display = "none";
 			}
			if(sectionType == 4 || qTypeID == 11 || qTypeID == 13 || qTypeID == 16 || qTypeID == 18 || qTypeID == 19 || qTypeID == 20) { //#NAEUJY
				rowHasReqComment.style.display = "none";
			} else {
				rowHasReqComment.style.display = ((bIsIE)?"inline":"table-row");
			}

	 		var rowDecimalPlaces = document.getElementById("Row_DecimalPlaces");
	 		rowDecimalPlaces.style.display = (qTypeID == 0 && qDataType == 1)?((bIsIE)?"inline":"table-row"):"none";
			
	 		//var rowExcludeSelected = document.getElementById("Row_ExcludeSelected");
	 		var rowSourceSimpleQuestionID = document.getElementById("Row_SourceSimpleQuestionID");
	 		
	 		var rowSummaryTotal = document.getElementById("Row_SummaryTotal");
			var rowFormula = document.getElementById("Row_Formula");
			var rowShowCondition = document.getElementById("Row_ShowCondition");
			
			var rowShowInSummary = document.getElementById("Row_ShowInSummary");
			
 			if(insideDynamicSection==0)
			{
				var rowUseCategoryDimChoice = document.getElementById("Row_UseCategoryDimChoice");
				var rowCategoryDimName = document.getElementById("Row_CategoryDimName");
				var rowElementDimName = document.getElementById("Row_ElementDimName");
				var rowElementIndName = document.getElementById("Row_ElementIndName");
			}
			
			var rowIsIdentifier = document.getElementById("Row_IsFieldSurveyID");
			var rowIsRequiredQuestion = document.getElementById("Row_IsReqQuestion");
			var rowGoToSection = document.getElementById("Row_GoToQuestion");
			var rowRegisterGPSAt = document.getElementById("Row_RegisterGPSAt");
			var rowSharedQuestionID = document.getElementById("Row_SharedQuestionID");
			var roweBavelFieldID = document.getElementById("Row_eBavelFieldID");
			var rowIsInvisible = document.getElementById("Row_IsInvisible");
			if(((qTypeID == 0 && qDataType == 1) || qTypeID == 14)  && sectionType == 3) {
				rowSummaryTotal.style.display = ((bIsIE)?"inline":"table-row");
			} else {
				rowSummaryTotal.style.display = "none";
			}
			
			rowShowInSummary.style.display = 'none';
			<? if(getMDVersion() >= esvShowInSummary) { ?>
				if(sectionType == 3) {
					rowShowInSummary.style.display = ((bIsIE)?"inline":"table-row");
				}			
			<? } ?>
			
			if (rowRegisterGPSAt != undefined) {
				if (qTypeID == 18) {
					rowRegisterGPSAt.style.display = ((bIsIE)?"inline":"table-row");
				}
				else {
					rowRegisterGPSAt.style.display = "none";
				}
			}
			
			if(qTypeID==13 || qTypeID==11 || qTypeID==16 || qTypeID==18 || qTypeID==19 || qTypeID==20) {
				if (roweBavelFieldID != undefined) {
					if (qTypeID==18 || qTypeID==20) {
						roweBavelFieldID.style.display = ((bIsIE)?"inline":"table-row");
					}
					else {
						roweBavelFieldID.style.display = "none";
					}
				}
				rowHasReqPhoto.style.display = "none";
				if (intMDVersion >= intEForms401v) {
					rowHasReqDocument.style.display = "none";
				}
				rowHasReqComment.style.display = "none";
				rowIsIdentifier.style.display = "none";
				rowIsRequiredQuestion.style.display = "none";
								
				if(qTypeID==11) {
					rowGoToSection.style.display = "none";
					var qmessage = document.getElementById("questionMessageTextArea");
					qmessage.style.display = "block";
					var qimage = document.getElementById("questionImageTextArea");
					qimage.style.display = "none";
					var rowAutoRedraw = document.getElementById("Row_AutoRedraw");
		 			if (rowAutoRedraw) {
		 				rowAutoRedraw.style.display = ((bIsIE)?"inline":"table-row");
		 			}
				} else {//if (qTypeID==13 || qTypeID==16) {
					if (qTypeID==13) {
						rowGoToSection.style.display = ((bIsIE)?"inline":"table-row");
					}
					else {
						rowGoToSection.style.display = "none";
					}
					var qmessage = document.getElementById("questionMessageTextArea");
					qmessage.style.display = "none";
					var qimage = document.getElementById("questionImageTextArea");
					qimage.style.display = "block";
					var rowAutoRedraw = document.getElementById("Row_AutoRedraw");
		 			if (rowAutoRedraw) {
		 				rowAutoRedraw.style.display = 'none';
		 			}
				}
				if (intMDVersion >= intEForms401v) {
					rowFormula.style.display = "none";
					rowShowCondition.style.display = "none";
				}
			} else {
				if (roweBavelFieldID != undefined) {
					roweBavelFieldID.style.display = ((bIsIE)?"inline":"table-row");
				}
				var qmessage = document.getElementById("questionMessageTextArea");
				qmessage.style.display = "none";
				var qimage = document.getElementById("questionImageTextArea");
				qimage.style.display = "block";
				var rowAutoRedraw = document.getElementById("Row_AutoRedraw");
	 			if (rowAutoRedraw) {
	 				rowAutoRedraw.style.display = 'none';
	 			}
				//#NAEUJY
				rowHasReqPhoto.style.display = ((sectionType == 4)?"none":((bIsIE)?"inline":"table-row"));
				if (intMDVersion >= intEForms401v) {
					rowHasReqDocument.style.display = "none";
				}
				rowHasReqComment.style.display = ((sectionType == 4)?"none":((bIsIE)?"inline":"table-row")); //#NAEUJY
				rowIsIdentifier.style.display = ((bIsIE)?"inline":"table-row");
				rowIsRequiredQuestion.style.display = ((bIsIE)?"inline":"table-row");
				rowGoToSection.style.display = "none";
				
<?
		if (getMDVersion() >= esvSyncQuestionAndSkipFoto) {
?>
			if (qTypeID == 8) {
				rowGoToSection.style.display = ((bIsIE)?"inline":"table-row");
			}
<?
		}
?>
				
				var qmessage = document.getElementById("questionMessageTextArea");
				qmessage.style.display = "none";
				var qimage = document.getElementById("questionImageTextArea");
				qimage.style.display = "block";
				
				if (qTypeID == 14) {
					rowIsRequiredQuestion.style.display = "none";
					if (intMDVersion >= intEForms401v) {
						rowFormula.style.display = ((bIsIE)?"inline":"table-row");
						rowShowCondition.style.display = ((bIsIE)?"inline":"table-row");
					}
					rowDefaultValue.style.display = "none";
				} else {
					rowIsRequiredQuestion.style.display = ((bIsIE)?"inline":"table-row");
					if (intMDVersion >= intEForms401v) {
						rowFormula.style.display = "none";
						rowShowCondition.style.display = "none";
					}
				}
			}
	 		
	 		var objCmb = <?=$myFormName?>_SaveForm.CatalogID;
	 		if(objCmb.options.length>0)
	 		{
	 			var iCatalogID = parseInt(objCmb.options[objCmb.selectedIndex].value);	
	 		}
	 		else
	 		{
	 			var iCatalogID = 0;
	 		}
	 		
	 		refreshCatalogs(qTypeID, iCatalogID)
	 		mbHideMinMaxForNumericCatalog = false;
			if(qTypeID==2 || qTypeID==17)
			{
				if (rowSharedQuestionID != undefined) {
					rowSharedQuestionID.style.display = (qTypeID != 17)?((bIsIE)?"inline":"table-row"):'none';
				}
				rowAllowAdditionalAnswers.style.display = (sectionType != 4 && qTypeID != 17 && qDisplayMode != 3 && iSourceQuestionID <= 0)?((bIsIE)?"inline":"table-row"):'none'; //#NAEUJY
		 		var spanOtherLabel = document.getElementById("spnOtherLabel");
		 		if (spanOtherLabel != null) {
			 		var spanCommentLabel = document.getElementById("spnCommentLabel");
					if (qTypeID != 17 && qDisplayMode != 3 && iSourceQuestionID <= 0)
					{
						spanOtherLabel.style.display = 'inline';
						spanCommentLabel.style.display = 'inline';
					}
					else
					{
						spanOtherLabel.style.display = 'none';
						spanCommentLabel.style.display = 'none';
					}
		 		}
				
				rowSourceQuestionID.style.display = (qTypeID != 17)?((bIsIE)?"inline":"table-row"):'none';
				rowSourceSimpleQuestionID.style.display = (qTypeID != 17)?((bIsIE)?"inline":"table-row"):'none';
				//rowExcludeSelected.style.display = (bIsIE)?"inline":"table-row";
				rowMCInputType.style.display = 'none';
				if (rowPictLayOut) {
					rowPictLayOut.style.display = 'none';
				}
				if (rowInputWidth) {
					rowInputWidth.style.display = 'none';
				}
				if (rowEnableCheckBox) {
					rowEnableCheckBox.style.display = 'none';
				}
				if (rowSwitchBehaviour) {
					rowSwitchBehaviour.style.display = 'none';
				}
	 			
				//Si se selecciono despliegue de matriz entonces se  tienen q ocultar este campo
				var blnAllowSimpleChoiceInDynamics = 0;
<?
				if (trim($_SESSION["PABITAM_RepositoryName"])=="fbm_bmd_") {
?>
				var blnAllowSimpleChoiceInDynamics = 1;
<?					
		 			//rowUseCatalog.style.display = (qTypeID != 17 && qDisplayMode != 3 && iSourceQuestionID <= 0 && sectionType != 1)?((bIsIE)?"inline":"table-row"):'none';
				}
?>
	 			rowUseCatalog.style.display = (qTypeID != 17 && qDisplayMode != 3 && iSourceQuestionID <= 0 && (sectionType != 1 || blnAllowSimpleChoiceInDynamics))?((bIsIE)?"inline":"table-row"):'none';
	 			
	 			if (qTypeID==17) {
	 				showHideCatalogs();
	 			}
	 			else {
	 				showFillingQuestionsSCH();
	 			}
	 			
	 			rowIsMultiDimension.style.display = 'none';
	 			rowUseCatToFillMC.style.display = 'none';
				rowMinValue.style.display = 'none';
				rowMaxValue.style.display = 'none';
				if (qTypeID==2) {
			 		var objCmb = <?=$myFormName?>_SaveForm.typeOfFillingSC;
			 		if(objCmb && objCmb.options.length > 0) {
			 			var itypeOfFillingSC = parseInt(objCmb.options[objCmb.selectedIndex].value);	
			 		}
			 		else
			 		{
			 			var itypeOfFillingSC = -1;
			 		}
			 		
			 		if (itypeOfFillingSC == 1) {
			 			rowDefaultValue.style.display = 'none';
			 		}
			 		else {
						rowDefaultValue.style.display = (bIsIE)?"inline":"table-row";
			 		}
				}
				else {
					rowDefaultValue.style.display = 'none';
				}
				rowMaxChecked.style.display = 'none';
				rowFormatMaskID.style.display = 'none';
				rowDateFormatMask.style.display = 'none';
				if (intMDVersion >= intEForms401v) {
					rowReadOnly.style.display = 'none';
					rowFormula.style.display = 'none';
					rowShowCondition.style.display = 'none';
				}
	 			
				if(insideDynamicSection==0)
 				{
					rowUseCategoryDimChoice.style.display = 'none';
					rowCategoryDimName.style.display = 'none';
					rowElementDimName.style.display = 'none';
					rowElementIndName.style.display = 'none';
 				}
 				if (rowtypeOfFillingSC != undefined) {
 					rowtypeOfFillingSC.style.display = (qTypeID != 17)?((bIsIE)?"inline":"table-row"):'none';
 				}
 				if (rowtypeOfFillingMC != undefined) {
 					rowtypeOfFillingMC.style.display = 'none';
 				}
			}
	 		else if(qTypeID==3)
			{
				rowAllowAdditionalAnswers.style.display = 'none';
		 		var spanOtherLabel = document.getElementById("spnOtherLabel");
		 		if (spanOtherLabel != null) {
			 		var spanCommentLabel = document.getElementById("spnCommentLabel");
					spanOtherLabel.style.display = 'none';
					spanCommentLabel.style.display = 'inline';
		 		}
				
				if (insideDynamicSection) {
					//rowSourceQuestionID.style.display = 'none';
					rowSourceSimpleQuestionID.style.display = 'none';
				}
				else {
					//rowSourceQuestionID.style.display = (bIsIE)?"inline":"table-row";
					//rowExcludeSelected.style.display = 'none';
					rowSourceSimpleQuestionID.style.display = (bIsIE)?"inline":"table-row";
				}
				//Para las preguntas de multiple choice dentro de secciones dinamicas
				//sus opciones seran siempre los valores del atributo hijo del atributo 
				//definido en la seccion dinamica
				var strMCInputTypeDisplay = 'none';
				if ((qDisplayMode == 0 || qDisplayMode == 1) && (mcinputtype == 1 || mcinputtype == 2)) {
					strMCInputTypeDisplay = ((bIsIE)?"inline":"table-row");
				}
				if (rowEnableCheckBox) {
					rowEnableCheckBox.style.display = strMCInputTypeDisplay;
				}
				if (rowSwitchBehaviour) {
					rowSwitchBehaviour.style.display = strMCInputTypeDisplay;
				}
				
	 			if(insideDynamicSection==1)
 				{
		 			rowUseCatalog.style.display = 'none';
		 			showHideCatalogs();
	
					rowUseCatToFillMC.style.display = 'none';
					//rowImportCatOptions.style.display = 'none';
					rowIsMultiDimension.style.display = 'none';
					rowStrSelectOptions.style.display = 'none';
					rowStrSelectOptionsCol.style.display = 'none';
					rowCatalogID.style.display = 'none';
					rowMCInputType.style.display = (qDisplayMode == 0 || qDisplayMode == 1)?((bIsIE)?"inline":"table-row"):'none';
					rowOneChoicePer.style.display = 'none';
					rowShowImageAndText.style.display = 'none';
					if (rowPictLayOut) {
						rowPictLayOut.style.display = 'none';
					}
					if (rowInputWidth) {
						rowInputWidth.style.display = 'none';
					}
 				}
 				else {
	 				rowIsMultiDimension.style.display = (qDisplayMode != 3)?((bIsIE)?"inline":"table-row"):'none';
	 				rowStrSelectOptions.style.display = ((bIsIE)?"inline":"table-row");
	 				rowStrSelectOptionsCol.style.display = (qDisplayMode == 3)?((bIsIE)?"inline":"table-row"):'none';
	 				rowUseCatToFillMC.style.display = 'none';
					//rowUseCatToFillMC.style.display = (qDisplayMode != 3)?((bIsIE)?"inline":"table-row"):'none';
					rowMCInputType.style.display = (qDisplayMode == 0 || qDisplayMode == 1)?((bIsIE)?"inline":"table-row"):'none';
	 				rowOneChoicePer.style.display = 'none';
					rowShowImageAndText.style.display = ((bIsIE)?"inline":"table-row");
	 				
	 				if(qDisplayMode==0 && mcinputtype==1 && ismultidimension==0)
	 				{
	 					rowUseCategoryDimChoice.style.display = ((bIsIE)?"inline":"table-row");
	 					showHideCategoryFields();
	 					
	 					if(<?=$myFormName?>_SaveForm.UseCategoryDimChoice.value!=0)
	 					{
			 				rowHasReqPhoto.style.display = 'none';
			 				if (intMDVersion >= intEForms401v) {
								rowHasReqDocument.style.display = 'none';
			 				}
							rowHasReqComment.style.display = 'none';
	 					}
	 				}
	 				else
	 				{
	 					rowUseCategoryDimChoice.style.display = "none";
	 					rowCategoryDimName.style.display = 'none';
						rowElementDimName.style.display = 'none';
						rowElementIndName.style.display = 'none';
	 				}
					
	 				if (qDisplayMode==1) {
	 					if (rowPictLayOut) {
							rowPictLayOut.style.display = ((bIsIE)?"inline":"table-row");
	 					}
	 					if (rowInputWidth) {
							rowInputWidth.style.display = ((bIsIE)?"inline":"table-row");
	 					}
	 				}
	 				else {
						if (rowPictLayOut) {
							rowPictLayOut.style.display = 'none';
						}
						if (rowInputWidth) {
							rowInputWidth.style.display = 'none';
						}
	 				}
	 				
		 			rowUseCatalog.style.display = 'none';
		 			showHideCatalogs();
 				}
 				
	 			showFillingQuestionsMCH();
 				
				rowMinValue.style.display = ((qDisplayMode == 0 || qDisplayMode == 1) && mcinputtype==1)?((bIsIE)?"inline":"table-row"):'none';
				rowMaxValue.style.display = ((qDisplayMode == 0 || qDisplayMode == 1) && mcinputtype==1)?((bIsIE)?"inline":"table-row"):'none';
				rowDefaultValue.style.display = 'none';
				if (intMDVersion >= intEForms401v) {
					rowReadOnly.style.display = 'none';
				}
				rowMaxChecked.style.display = (qDisplayMode == 0)?((bIsIE)?"inline":"table-row"):'none';
				rowFormatMaskID.style.display = ((qDisplayMode == 0 || qDisplayMode == 1) && mcinputtype==1)?((bIsIE)?"inline":"table-row"):'none';
				rowFormatMask.style.display = ((qDisplayMode == 0 || qDisplayMode == 1) && mcinputtype==1 && iFormatMaskID==1)?((bIsIE)?"inline":"table-row"):'none';
        
        		rowDateFormatMaskID.style.display = 'none';
				rowDateFormatMask.style.display = 'none';
				
				//rowMinValue.style.display = 'none';
				//rowMaxValue.style.display = 'none';
 				if (rowtypeOfFillingSC != undefined) {
 					rowtypeOfFillingSC.style.display = 'none';
 				}
 				if (rowtypeOfFillingMC != undefined) {
					if (insideDynamicSection) {
 						rowtypeOfFillingMC.style.display = 'none';
					}
					else {
						rowtypeOfFillingMC.style.display = ((bIsIE)?"inline":"table-row");
					}
 				}
			}
			else
			{
				if (rowSharedQuestionID != undefined) {
					rowSharedQuestionID.style.display = 'none';
				}
				rowAllowAdditionalAnswers.style.display = 'none';
		 		var spanOtherLabel = document.getElementById("spnOtherLabel");
		 		if (spanOtherLabel != null) {
			 		var spanCommentLabel = document.getElementById("spnCommentLabel");
					spanOtherLabel.style.display = 'none';
					spanCommentLabel.style.display = 'none';
		 		}
				
				rowSourceQuestionID.style.display = 'none';
				//rowExcludeSelected.style.display = "none";
				rowSourceSimpleQuestionID.style.display = "none";
				rowUseCatalog.style.display = 'none';
				rowUseCatToFillMC.style.display = 'none';
				rowShowImageAndText.style.display = 'none';
				
				if(qDataType==7)
				{
					showHideCatalogs();
				}/*@AAL 22/01/2015 Solo en Secciones Dinámicas las preguntas abiertas de Tipo Numeric pueden usar catalogos. Issue: #VZ8LWK */
				else if (qTypeID == 0 && qDataType == 1 && insideDynamicSection == 1)
				{
					rowUseCatalog.style.display = (bIsIE)?"inline":"table-row";
					showHideCatalogs();
				}
				else
				{
					rowCatalogID.style.display = 'none';
					rowCatMemberID.style.display = 'none';
				}
				//@AAL 06/05/2015: Agregado para las preguntas tipo Barcode
				if (qTypeID == 0 || qTypeID == 25) {
					if (intMDVersion >= intEForms401v) {
						rowReadOnly.style.display = (bIsIE)?"inline":"table-row";
					}
					rowDefaultValue.style.display = (bIsIE)?"inline":"table-row";
				}
				else {
					if (intMDVersion >= intEForms401v) {
						if (qTypeID == 9) {
							rowReadOnly.style.display = (bIsIE)?"inline":"table-row";
						}
						else {
							rowReadOnly.style.display = 'none';
						}
					}
					
					if (qTypeID == 9) {
						rowDefaultValue.style.display = (bIsIE)?"inline":"table-row";
					} else {
						rowDefaultValue.style.display = 'none';
					}
					if (intMDVersion >= esvCatalogAttributeImage) {
						if (qTypeID == 8 || qTypeID == 23) {
							rowDefaultValue.style.display = (bIsIE)?"inline":"table-row";
						}
					}
				}
				
				//rowImportCatOptions.style.display = 'none';
				rowIsMultiDimension.style.display = 'none';
				rowStrSelectOptions.style.display = 'none';
				rowStrSelectOptionsCol.style.display = 'none';
				rowMCInputType.style.display = 'none';
				rowOneChoicePer.style.display = 'none';
				if (rowPictLayOut) {
					rowPictLayOut.style.display = 'none';
				}
				if (rowInputWidth) {
					rowInputWidth.style.display = 'none';
				}
				if (rowEnableCheckBox) {
					rowEnableCheckBox.style.display = 'none';
				}
				if (rowSwitchBehaviour) {
					rowSwitchBehaviour.style.display = 'none';
				}
				
				rowMinValue.style.display = 'none';
				rowMaxValue.style.display = 'none';
				rowMaxChecked.style.display = 'none';
				rowFormatMaskID.style.display = 'none';
				rowFormatMask.style.display = 'none';
				
				rowDateFormatMaskID.style.display = 'none';
				rowDateFormatMask.style.display = 'none';
				
				if (qDataType == 4)
 				{
        			rowDateFormatMaskID.style.display = (bIsIE)?"inline":"table-row";
					rowDateFormatMask.style.display = 'none';
 				}
 				
				if(insideDynamicSection==0)
 				{
					rowUseCategoryDimChoice.style.display = 'none';
					rowCategoryDimName.style.display = 'none';
					rowElementDimName.style.display = 'none';
					rowElementIndName.style.display = 'none';
 				}
 				//conchita 2014-09-05 agregado el tipo 23 (sketch)
 				if (qTypeID == 8 || qTypeID == 10 || qTypeID == 23)
 				{
 					rowHasReqPhoto.style.display = 'none';
 					if (intMDVersion >= intEForms401v) {
						rowHasReqDocument.style.display = 'none';
 					}
 				}
 				if (rowtypeOfFillingSC != undefined) {
 					rowtypeOfFillingSC.style.display = 'none';
 				}
 				if (rowtypeOfFillingMC != undefined) {
 					rowtypeOfFillingMC.style.display = 'none';
 				}
			}

<?
			//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
			//Modificado para agrupar los tipos de preguntas abiertas en uno sólo
			//if(qTypeID==1)
?>
			if((qDataType == 1 && qTypeID != 25) || (qTypeID==3 && (qDisplayMode == 0 || qDisplayMode == 1) && mcinputtype==1))
			{
				if (qDataType==1)
				{
					spanIsIndicator.style.display = (bIsIE)?"inline":"table-row";
					rowDefaultValue.style.display = (bIsIE)?"inline":"table-row";
					if (intMDVersion >= intEForms401v) {
						rowReadOnly.style.display = (bIsIE)?"inline":"table-row";
					}
				}
				else {
					spanIsIndicator.style.display = 'none';
				}
				rowMinValue.style.display = (bIsIE)?"inline":"table-row";
				rowMaxValue.style.display = (bIsIE)?"inline":"table-row";
				
				rowFormatMaskID.style.display = (bIsIE)?"inline":"table-row";
				rowFormatMask.style.display = (iFormatMaskID == 1)?((bIsIE)?"inline":"table-row"):'none';
        
        		rowDateFormatMaskID.style.display = 'none';
				rowDateFormatMask.style.display = 'none';
        
				if (mbHideMinMaxForNumericCatalog)
				{
					rowDecimalPlaces.style.display = 'none';
					rowMinValue.style.display = 'none';
					rowMaxValue.style.display = 'none';
					rowFormatMaskID.style.display = 'none';
					rowFormatMask.style.display = 'none';
				}
				/*
	 			if(bIsIE)
	 			{
					rowMinValue.style.display = 'inline';
					rowMaxValue.style.display = 'inline';
	 			}
	 			else
	 			{
					rowMinValue.style.display = 'table-row';
					rowMaxValue.style.display = 'table-row';
	 			}
	 			*/
			}
			else
			{
				spanIsIndicator.style.display = 'none';
				rowMinValue.style.display = 'none';
				rowMaxValue.style.display = 'none';
				
				if(qTypeID==3 && qDisplayMode == 0) {
					rowMaxChecked.style.display = (bIsIE)?"inline":"table-row";
				}
				
				rowFormatMaskID.style.display = 'none';
				rowFormatMask.style.display = 'none';
        
				rowDateFormatMaskID.style.display = 'none';
				rowDateFormatMask.style.display = 'none';
				
		        if(qDataType == 4) {
		           	rowDateFormatMaskID.style.display = (bIsIE)?"inline":"table-row";
					rowDateFormatMask.style.display = 'none';
		        }
		        
			}
			
			if(qTypeID == 14) {
				rowFormatMaskID.style.display = ((bIsIE)?"inline":"table-row");
				rowFormatMask.style.display = (iFormatMaskID == 1)?((bIsIE)?"inline":"table-row"):'none';
			}
			
			if(qTypeID!=14)
			{
				//29Ene2013: Si la pregunta no es de tipo "Calculated" el campo "Formula" no debe visualizarse
				rowFormula.style.display = 'none';
				<?=$myFormName?>_SaveForm.Formula.value = '';
			}
			
			//Aqui se muestra u oculta el span de IsIndicatorMC
			//que aplica para los multiple choice questions
			showIsIndicatorMC();
			setEBQTypeID(qTypeID, qDataType);
			
	 		var rowSourceSectionID = document.getElementById("Row_SourceSectionID");
	 		if (rowSourceSectionID) {
	 			rowSourceSectionID.style.display = (qTypeID == 24)?((bIsIE)?"inline":"table-row"):"none";
	 		}
	 		
	 		if (qTypeID == 24) {
	 			$('tr[id^="Row_"]').hide();
		 		var rowQuestionText = document.getElementById("Row_QuestionText");
		 		if (rowQuestionText) {
		 			rowQuestionText.style.display = (bIsIE)?"inline":"table-row";
		 		}
		 		var rowAttributeName = document.getElementById("Row_AttributeName");
		 		if (rowAttributeName) {
		 			rowAttributeName.style.display = (bIsIE)?"inline":"table-row";
		 		}
		 		var rowGQTypeID = document.getElementById("Row_GQTypeID");
		 		if (rowGQTypeID) {
		 			rowGQTypeID.style.display = (bIsIE)?"inline":"table-row";
		 		}
		 		var rowSourceSectionID = document.getElementById("Row_SourceSectionID");
		 		if (rowSourceSectionID) {
		 			rowSourceSectionID.style.display = (bIsIE)?"inline":"table-row";
		 		}
		 		var rowSectionID = document.getElementById("Row_SectionID");
		 		if (rowSectionID) {
		 			rowSectionID.style.display = (bIsIE)?"inline":"table-row";
		 		}
				var qimage = document.getElementById("questionImageTextArea");
				qimage.style.display = "none";
	 		}
	 		//Validación para que el combo del editor HTML sólo se muestre en las preguntas tipo mensaje
	 		
 			var objEditorCmb = <?=$myFormName?>_SaveForm.EditorType;
 			if(objEditorCmb)
 			{
		 		if(qTypeID == 11)
		 		{
		 			Row_EditorType.style.display = (bIsIE)?"inline":"table-row";
		 		}
		 		else
		 		{
		 			Row_EditorType.style.display='none';
		 		}
 			}
 		}
 		
 		function setEBQTypeID(iQTypeID, iDataType) {
      		var objCmb = <?=$myFormName?>_SaveForm.EBQTypeID;
      		if (!objCmb || !objCmb.options || !objCmb.options.length) {
      			return;
      		}
      		if (iDataType > 0) {
	      		var intQTypeID = iDataType;
      		}
      		else {
	      		var intQTypeID = iQTypeID;
      		}
      		
      		var intMaxOpts = objCmb.options.length;
      		for (var intIdx = 0; intIdx < intMaxOpts; intIdx++) {
      			if (objCmb.options[intIdx].value == intQTypeID) {
      				objCmb.selectedIndex = intIdx;
      				break;
      			}
      		}
      		
      		if (objCmb.onchange) {
      			objCmb.onchange();
      		}
 		}
 		
 		function showHideCategoryFields()
 		{
 			var intMDVersion = parseFloat('<?=getMDVersion()?>');
 			var intEForms401v = parseFloat('<?=esveFormsV41Func?>');
 			
 			//Siempre y cuando estemos en una seccion estatica
 			if(insideDynamicSection==0)
 			{
				var rowCategoryDimName = document.getElementById("Row_CategoryDimName");
				var rowElementDimName = document.getElementById("Row_ElementDimName");
				var rowElementIndName = document.getElementById("Row_ElementIndName");
				
				var rowHasReqPhoto = document.getElementById("Row_HasReqPhoto");
				if (intMDVersion >= intEForms401v) {
					var rowHasReqDocument = document.getElementById("Row_HasReqDocument");
				}
				var rowHasReqComment = document.getElementById("Row_HasReqComment");
				
 				//Dependiendo del valor mostraremos ciertos campos 
 				//o bien se mantendra algunos campos de solo lectura
				var objCmb = <?=$myFormName?>_SaveForm.UseCategoryDimChoice;
 				var useCatDimVal = parseInt(objCmb.options[objCmb.selectedIndex].value);
		 		var objCmb = <?=$myFormName?>_SaveForm.GQTypeID;
		 		var qTypeID = parseInt(objCmb.options[objCmb.selectedIndex].value);

 				switch (useCatDimVal)
 				{
					case 0:
	 					rowCategoryDimName.style.display = 'none';
						rowElementDimName.style.display = 'none';
						rowElementIndName.style.display = 'none'; //conchita 2014-09-05 agregado el tipo 23 sketch
						rowHasReqPhoto.style.display = (sectionType != 4 && qTypeID != 8 && qTypeID != 10 && qTypeID != 23 && qTypeID != 11 && qTypeID != 13 && qTypeID != 16 && qTypeID != 18 && qTypeID != 19 && qTypeID != 20 && qTypeID != 24)?((bIsIE)?"inline":"table-row"):"none"; //#NAEUJY
						if (intMDVersion >= intEForms401v) {
							rowHasReqDocument.style.display = "none";
						}
						rowHasReqComment.style.display = (sectionType != 4 && qTypeID != 11 && qTypeID != 13 && qTypeID != 16 && qTypeID != 18 && qTypeID != 19 && qTypeID != 20 && qTypeID != 24)?((bIsIE)?"inline":"table-row"):"none"; //#NAEUJY
 						break;
 					case -1:
	 					rowCategoryDimName.style.display = ((bIsIE)?"inline":"table-row");
						rowElementDimName.style.display = ((bIsIE)?"inline":"table-row");
						rowElementIndName.style.display = ((bIsIE)?"inline":"table-row");
						
		 				<?=$myFormName?>_SaveForm.CategoryDimName.disabled = false;
		 				<?=$myFormName?>_SaveForm.ElementDimName.disabled = false;
		 				<?=$myFormName?>_SaveForm.ElementIndName.disabled = false;

		 				<?=$myFormName?>_SaveForm.CategoryDimName.value = '';
		 				<?=$myFormName?>_SaveForm.ElementDimName.value = '';
		 				<?=$myFormName?>_SaveForm.ElementIndName.value = '';

						rowHasReqPhoto.style.display = 'none';
						if (intMDVersion >= intEForms401v) {
							rowHasReqDocument.style.display = 'none';
						}
						rowHasReqComment.style.display = 'none';
 						break;
 					default:
	 					rowCategoryDimName.style.display = 'none';
						rowElementDimName.style.display = ((bIsIE)?"inline":"table-row");
						rowElementIndName.style.display = ((bIsIE)?"inline":"table-row");

						rowHasReqPhoto.style.display = 'none';
						if (intMDVersion >= intEForms401v) {
							rowHasReqDocument.style.display = 'none';
						}
						rowHasReqComment.style.display = 'none';
						//Realizamos llamada con XMLObject para obtener los nombres de los elementos dimension e indicador
						var xmlObj = getXMLObject();
						if (xmlObj == null)
						{
							alert('XML Object not supported');
							return(new Array);
						}
			
						xmlObj.open('POST', 'getCategoryDimElements.php?CategoryDim=' + useCatDimVal + '&SurveyID=<?=$this->SurveyID?>', false);
				 		xmlObj.send(null);
				
				 		var strMembers = Trim(unescape(xmlObj.responseText));
				 		var arrayMembers = new Array();
				 		var i, count;
				 		if(strMembers!="")
				 		{
				 			arrayMembers = strMembers.split('_SVSep_');
				 			count = arrayMembers.length;
				 			
				 			if(count==3)
				 			{
				 				<?=$myFormName?>_SaveForm.CategoryDimName.value = arrayMembers[0];
				 				<?=$myFormName?>_SaveForm.ElementDimName.value = arrayMembers[1];
				 				<?=$myFormName?>_SaveForm.ElementIndName.value = arrayMembers[2];
				 				
				 				<?=$myFormName?>_SaveForm.CategoryDimName.disabled = true;
				 				<?=$myFormName?>_SaveForm.ElementDimName.disabled = true;
				 				<?=$myFormName?>_SaveForm.ElementIndName.disabled = true;
				 			}
				 			else
				 			{
				 				alert('There no elements for this category dimension');
				 				
				 				//Volvemos a poner el Combo de CategoryDimension con valor de [New]
				 				<?=$myFormName?>_SaveForm.UseCategoryDimChoice.value = -1;
				 				
								rowCategoryDimName.style.display = ((bIsIE)?"inline":"table-row");
								rowElementDimName.style.display = ((bIsIE)?"inline":"table-row");
								rowElementIndName.style.display = ((bIsIE)?"inline":"table-row");
				 			}
				 		}

 						break;
 				}
 			}
 		}

 		function showIsIndicatorMC()
 		{
	 		var objCmb = <?=$myFormName?>_SaveForm.MCInputType;
	 		var MCInputTypeID = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		var spanIsIndicatorMC = document.getElementById("lblIsIndicatorMC");
	 		
			if(MCInputTypeID==1)
			{
				spanIsIndicatorMC.style.display = 'inline';
			}
			else
			{
				spanIsIndicatorMC.style.display = 'none';
			}
 		}
		
 		function showFillingQuestionsSCH() {
 			var objCmb = <?=$myFormName?>_SaveForm.typeOfFillingSC;
 			if (objCmb == undefined || objCmb.selectedIndex == -1) {
 				return;
 			}
 			var iTypeOfFilling = parseInt(objCmb.options[objCmb.selectedIndex].value)
 			var objUseCatalog = <?=$myFormName?>_SaveForm.UseCatalog;
 			if (objUseCatalog == undefined) {
 				return;
 			}
 			var rowUseCatalog = document.getElementById("Row_UseCatalog");
 			if (rowUseCatalog != undefined) {
 				rowUseCatalog.style.display = 'none';
 			}
 			
 			if (iTypeOfFilling == <?=tofCatalog?>) {
 				if (objUseCatalog.selectedIndex <= 0) {
 					objUseCatalog.selectedIndex = 1;
 				}
 			}
 			else {
				objUseCatalog.selectedIndex = 0;
 			}
 			
 			showHideCatalogs();
			var rowSourceQuestionID = document.getElementById("Row_SourceQuestionID");
			var rowSharedQuestionID = document.getElementById("Row_SharedQuestionID");
			var rowStrSelectOptions = document.getElementById("Row_StrSelectOptions");
			var objCmbType = <?=$myFormName?>_SaveForm.GQTypeID;
			if (objCmbType == undefined || objCmbType.selectedIndex == -1) {
				return;
			}
 			var rowDefaultValue = document.getElementById("Row_DefaultValue");
			
 			switch (iTypeOfFilling) {
 				case <?=tofMCHQuestion?>:
 					if (rowSourceQuestionID != undefined) {
 						rowSourceQuestionID.style.display = ((bIsIE)?"inline":"table-row");
 					}
 					if (rowSharedQuestionID != undefined) {
 						rowSharedQuestionID.style.display = 'none';
 					}
 					if (rowStrSelectOptions != undefined) {
 						rowStrSelectOptions.style.display = 'none';
 					}
 					if (rowDefaultValue) {
 						rowDefaultValue.style.display = ((bIsIE)?"inline":"table-row");
 					}
 					break;
 				case <?=tofSharedAnswers?>:
 					if (rowSourceQuestionID != undefined) {
	 					rowSourceQuestionID.style.display = 'none';
 					}
 					if (rowSharedQuestionID != undefined) {
 						rowSharedQuestionID.style.display = ((bIsIE)?"inline":"table-row");
 					}
 					if (rowStrSelectOptions != undefined) {
 						rowStrSelectOptions.style.display = 'none';
 					}
 					if (rowDefaultValue) {
 						rowDefaultValue.style.display = ((bIsIE)?"inline":"table-row");
 					}
 					break;
 				default:
 					if (rowSourceQuestionID != undefined) {
	 					rowSourceQuestionID.style.display = 'none';
 					}
 					if (rowSharedQuestionID != undefined) {
 						rowSharedQuestionID.style.display = 'none';
 					}
 					
 					if (iTypeOfFilling == <?=tofCatalog?>) {
						if (rowDefaultValue) {
							rowDefaultValue.style.display = 'none';
						}
 					}
 					else {
	 					if (rowDefaultValue) {
	 						rowDefaultValue.style.display = ((bIsIE)?"inline":"table-row");
	 					}
 					}
 					
 					if (iTypeOfFilling != <?=tofFixedAnswers?>) {
	 					if (rowStrSelectOptions != undefined) {
	 						rowStrSelectOptions.style.display = 'none';
	 					}
 					}
 					break;
 			}
 		}
 		
 		function showFillingQuestionsMCH() {
 			var objCmb = <?=$myFormName?>_SaveForm.typeOfFillingMC;
 			if (objCmb == undefined || objCmb.selectedIndex == -1) {
 				return;
 			}
 			var iTypeOfFilling = parseInt(objCmb.options[objCmb.selectedIndex].value)
 			var rowUseCatalog = document.getElementById("Row_UseCatalog");
 			if (rowUseCatalog != undefined) {
 				rowUseCatalog.style.display = 'none';
 			}
 			
			var rowSourceQuestionID = document.getElementById("Row_SourceQuestionID");
			var rowSharedQuestionID = document.getElementById("Row_SharedQuestionID");
			var rowStrSelectOptions = document.getElementById("Row_StrSelectOptions");
			var objCmbType = <?=$myFormName?>_SaveForm.GQTypeID;
			if (objCmbType == undefined || objCmbType.selectedIndex == -1) {
				return;
			}
 			switch (iTypeOfFilling) {
 				case <?=tofMCHQuestion?>:
 					if (rowSourceQuestionID != undefined) {
 						rowSourceQuestionID.style.display = ((bIsIE)?"inline":"table-row");
 					}
 					if (rowSharedQuestionID != undefined) {
 						rowSharedQuestionID.style.display = 'none';
 					}
 					if (rowStrSelectOptions != undefined) {
 						rowStrSelectOptions.style.display = 'none';
 					}
 					break;
 				case <?=tofSharedAnswers?>:
 					if (rowSourceQuestionID != undefined) {
	 					rowSourceQuestionID.style.display = 'none';
 					}
 					if (rowSharedQuestionID != undefined) {
 						rowSharedQuestionID.style.display = ((bIsIE)?"inline":"table-row");
 					}
 					if (rowStrSelectOptions != undefined) {
 						rowStrSelectOptions.style.display = 'none';
 					}
 					break;
 				default:
 					if (rowSourceQuestionID != undefined) {
	 					rowSourceQuestionID.style.display = 'none';
 					}
 					if (rowSharedQuestionID != undefined) {
 						rowSharedQuestionID.style.display = 'none';
 					}
 					if (rowStrSelectOptions != undefined) {
 						if (insideDynamicSection) {
 							rowStrSelectOptions.style.display = 'none';
 						}
 						else {
 							rowStrSelectOptions.style.display = ((bIsIE)?"inline":"table-row");
 						}
 					}
 					break;
 			}
 		}
 		
 		function showHideCatalogs()
 		{
 			var intMDVersion = parseFloat('<?=getMDVersion()?>');
 			var intEForms401v = parseFloat('<?=esveFormsV41Func?>');
<?
			//@JAPR 2011-06-24: Agregado el tipo de dato y tipo de despliegue de las preguntas
			//Modificado para agrupar los tipos de preguntas abiertas en uno sólo
 			//var objCmbType = $myFormName_SaveForm.QTypeID;
?>
			var objCmb = <?=$myFormName?>_SaveForm.SourceQuestionID;
			var iSourceQuestionID = 0;
			if (objCmb.selectedIndex != -1) {
	  			var iSourceQuestionID = parseInt(objCmb.options[objCmb.selectedIndex].value);
			}
			
			var objCmb = <?=$myFormName?>_SaveForm.SourceSimpleQuestionID;
			var iSourceSimpleQuestionID = 0;
			if (objCmb.selectedIndex != -1) {
	  			var iSourceSimpleQuestionID = parseInt(objCmb.options[objCmb.selectedIndex].value);
			}
			
			var objCmb = <?=$myFormName?>_SaveForm.FormatMaskID;
			var iFormatMaskID = parseInt(objCmb.options[objCmb.selectedIndex].value);
      
      		var objCmb = <?=$myFormName?>_SaveForm.DateFormatMaskID;
			var iDateFormatMaskID = parseInt(objCmb.options[objCmb.selectedIndex].value);
      
	 		var objCmb = <?=$myFormName?>_SaveForm.QDisplayMode;
	 		var qDisplayMode = parseInt(objCmb.options[objCmb.selectedIndex].value);
 			var objCmbType = <?=$myFormName?>_SaveForm.GQTypeID;
 			var objCmbQDataType = <?=$myFormName?>_SaveForm.QDataType;
 			var QDataTypeVal = parseInt(objCmbQDataType.options[objCmbQDataType.selectedIndex].value);
 			/*@AAL 22/01/2015 Agregado valueTypeOfFill para validar que al entrar a una pregunta (creada previamente con el 
 			  Type of filling = Fixed answer) no se muestren las opciones Share response y Obtain response. Issue: #JA3TXZ */ 
			var objCmb_ = <?=$myFormName?>_SaveForm.typeOfFillingSC;
			var valueTypeOfFill = parseInt(objCmb_.options[objCmb_.selectedIndex].value);
			//@AAL
<?
			//@JAPR 2012-05-10: Validado que sólo se pueda usar la opción de Otro si la pregunta no es de catálogo
?>
 			var valueTypeID = parseInt(objCmbType.options[objCmbType.selectedIndex].value);
			
 			if(valueTypeID==2 || (QDataTypeVal==1))
 			{
 				var objCmb = <?=$myFormName?>_SaveForm.UseCatalog;
	 			if (valueTypeID != 2 || qDisplayMode == 3)
	 			{
	 				iSourceQuestionID = 0;
	 			}
 			}
 			else if(valueTypeID==3)
 			{
 				var objCmb = <?=$myFormName?>_SaveForm.UseCatToFillMC;
 			}
 			else if(valueTypeID==0)
 			{
 				var objCmb = null;
 			}

 			if(objCmb!=null)
 			{
	 			var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);
 			}
 			else
 			{
 				var valueID = 0;
 			}

			//var rowImportCatOptions = document.getElementById("Row_ImportCatOptions");
	 		var rowStrSelectOptions = document.getElementById("Row_StrSelectOptions");
	 		var rowStrSelectOptionsCol = document.getElementById("Row_StrSelectOptionsCol");
	 		var rowOneChoicePer = document.getElementById("Row_OneChoicePer");
	 		var rowCatalogID = document.getElementById("Row_CatalogID");
	 		var rowCatMemberID = document.getElementById("Row_CatMemberID");
			var rowAllowAdditionalAnswers = document.getElementById("Row_AllowAdditionalAnswers");
	 		var rowShowImageAndText = document.getElementById("Row_ShowImageAndText");
	 		var rowReadOnly = document.getElementById("Row_ReadOnly");
			var rowFormula = document.getElementById("Row_Formula");
			var rowShowCondition = document.getElementById("Row_ShowCondition");
			var rowSharedQuestionID = document.getElementById("Row_SharedQuestionID");
			if (rowSharedQuestionID != undefined) {
				rowSharedQuestionID.style.display = 'none';
			}
	 		var rowSourceQuestionID = document.getElementById("Row_SourceQuestionID");
	 		if (rowSourceQuestionID != undefined) {
				rowSourceQuestionID.style.display = 'none';
			}
	 		var rowSourceSimpleQuestionID = document.getElementById("Row_SourceSimpleQuestionID");
	 		if (rowSourceSimpleQuestionID != undefined) {
	 			rowSourceSimpleQuestionID.style.display = 'none';
	 		}
	 		
	 		// If SimpleChoice 
			var blnHideAllowAdditionalAnswers = true;
	 		if(valueTypeID==2)
	 		{
	 			//Si es diferente de matriz //#NAEUJY
	 			rowAllowAdditionalAnswers.style.display = (sectionType != 4 && qDisplayMode != 3 && valueID == 0 && iSourceQuestionID <= 0)?((bIsIE)?"inline":"table-row"):'none';
	 			rowShowImageAndText.style.display = rowAllowAdditionalAnswers.style.display;
	 			blnHideAllowAdditionalAnswers = false;
	 			if(qDisplayMode!=3)
	 			{
		 			//UseCatalog = Yes
			 		if(valueID==1)
			 		{
						rowCatalogID.style.display = ((bIsIE)?"inline":"table-row");
						rowCatMemberID.style.display = ((bIsIE)?"inline":"table-row");

			 			//rowImportCatOptions.style.display = 'none';
						rowStrSelectOptions.style.display = 'none';
						rowStrSelectOptionsCol.style.display = 'none';
						rowOneChoicePer.style.display = 'none';
			 		}
			 		else
			 		{
			 			//UseCatalog = No
			 			//rowImportCatOptions.style.display = (iSourceQuestionID <= 0)?((bIsIE)?"inline":"table-row"):'none';
						rowStrSelectOptions.style.display = (iSourceQuestionID <= 0)?((bIsIE)?"inline":"table-row"):'none';
						rowStrSelectOptionsCol.style.display = (qDisplayMode == 3 && iSourceQuestionID <= 0)?((bIsIE)?"inline":"table-row"):'none';
						
						rowOneChoicePer.style.display = 'none';
						rowCatalogID.style.display = 'none';
						rowCatMemberID.style.display = 'none';
						if (rowSharedQuestionID != undefined) {
							/*@AAL 22/01/2015 Se valida que no se muestren las opciones Share response y Obtain response. Issue: #JA3TXZ */
							if(valueTypeOfFill == 0 || valueTypeOfFill == 2)
								rowSharedQuestionID.style.display = "none";
							else
								rowSharedQuestionID.style.display = ((bIsIE)?"inline":"table-row");
						}
				 		if (rowSourceQuestionID != undefined) {
				 			/*@AAL 22/01/2015 Se valida que no se muestren las opciones Share response y Obtain response. Issue: #JA3TXZ */
							if(valueTypeOfFill == 0 || valueTypeOfFill == 3)
								rowSourceQuestionID.style.display = "none";
							else
								rowSourceQuestionID.style.display = ((bIsIE)?"inline":"table-row");
						}
				 		if (rowSourceSimpleQuestionID != undefined) {
				 			rowSourceSimpleQuestionID.style.display = ((bIsIE)?"inline":"table-row");
				 		}
			 		}
	 			}
	 			else
	 			{
						rowStrSelectOptions.style.display = ((bIsIE)?"inline":"table-row");
						rowStrSelectOptionsCol.style.display = ((bIsIE)?"inline":"table-row");
						rowOneChoicePer.style.display = ((bIsIE)?"inline":"table-row");

			 			//rowImportCatOptions.style.display = 'none';
						rowCatalogID.style.display = 'none';
						rowCatMemberID.style.display = 'none';
	 			}
	 		}
	 		else if(valueTypeID==17) {
				rowStrSelectOptions.style.display = ((bIsIE)?"inline":"table-row");
	 			//rowImportCatOptions.style.display = 'none';
				rowStrSelectOptionsCol.style.display = 'none';
				rowOneChoicePer.style.display = 'none';
				if (intMDVersion >= intEForms401v) {
					rowReadOnly.style.display = 'none';
				}
				rowCatalogID.style.display = 'none';
				rowCatMemberID.style.display = 'none';
				//if (rowSharedQuestionID != undefined) {
				//	rowSharedQuestionID.style.display = ((bIsIE)?"inline":"table-row");
				//}
	 		}
	 		else if(valueTypeID==3)
	 		{// If MultipleChoice

	 			if(qDisplayMode!=3)
	 			{
		 			//UseCatalog = Yes
			 		if(valueID==1)
			 		{
						rowCatalogID.style.display = ((bIsIE)?"inline":"table-row");
						rowCatMemberID.style.display = (valueTypeID == 2)?((bIsIE)?"inline":"table-row"):'none';
			 			
			 			//rowImportCatOptions.style.display = 'none';
						rowStrSelectOptions.style.display = 'none';
						rowStrSelectOptionsCol.style.display = 'none';
						rowOneChoicePer.style.display = 'none';
			 		}
			 		else
			 		{
			 			//UseCatalog = No
			 			//rowImportCatOptions.style.display = ((bIsIE)?"inline":"table-row");
						rowStrSelectOptions.style.display = ((bIsIE)?"inline":"table-row");
						rowStrSelectOptionsCol.style.display = (qDisplayMode == 3)?((bIsIE)?"inline":"table-row"):'none';
						rowOneChoicePer.style.display = 'none';
		 			
						rowCatalogID.style.display = 'none';
						rowCatMemberID.style.display = 'none';
			 		}
	 			}
	 			else
	 			{
					rowStrSelectOptions.style.display = ((bIsIE)?"inline":"table-row");
					rowStrSelectOptionsCol.style.display = ((bIsIE)?"inline":"table-row");
					rowOneChoicePer.style.display = ((bIsIE)?"inline":"table-row");

		 			//rowImportCatOptions.style.display = 'none';
					rowCatalogID.style.display = 'none';
					rowCatMemberID.style.display = 'none';
	 			}
				if (rowSharedQuestionID != undefined) {
					/*@AAL 22/01/2015 Se valida que no se muestren las opciones Share response y Obtain response. Issue: #JA3TXZ */
					if (insideDynamicSection || valueTypeOfFill == 0 || valueTypeOfFill == 2) {
						rowSharedQuestionID.style.display = 'none';
					}
					else {
						rowSharedQuestionID.style.display = ((bIsIE)?"inline":"table-row");
					}
				}
		 		if (rowSourceQuestionID != undefined) {
		 			/*@AAL 22/01/2015 Se valida que no se muestren las opciones Share response y Obtain response. Issue: #JA3TXZ */
					if (insideDynamicSection || valueTypeOfFill == 0 || valueTypeOfFill == 3) {
						rowSourceQuestionID.style.display = 'none';
					}
					else {
						rowSourceQuestionID.style.display = ((bIsIE)?"inline":"table-row");
					}
				}
		 		if (rowSourceSimpleQuestionID != undefined) {
					if (insideDynamicSection) {
		 				rowSourceSimpleQuestionID.style.display = 'none';
					}
					else {
		 				rowSourceSimpleQuestionID.style.display = ((bIsIE)?"inline":"table-row");
					}
		 		}
	 		}
	 		else if (valueTypeID==0 && QDataTypeVal==7)
	 		{// If Show Value
				rowCatalogID.style.display = ((bIsIE)?"inline":"table-row");
				rowCatMemberID.style.display = ((bIsIE)?"inline":"table-row");

	 			//rowImportCatOptions.style.display = 'none';
				rowStrSelectOptions.style.display = 'none';
				rowStrSelectOptionsCol.style.display = 'none';
				rowOneChoicePer.style.display = 'none';
				if (intMDVersion >= intEForms401v) {
					rowReadOnly.style.display = 'none';
				}
	 		}
	 		else if (valueTypeID == 0 && QDataTypeVal==1)
	 		{
		 		var rowMaxValue = document.getElementById("Row_MaxValue");
		 		var rowMinValue = document.getElementById("Row_MinValue");
		 		var rowDefaultValue = document.getElementById("Row_DefaultValue");
		 		var rowMaxChecked = document.getElementById("Row_MaxChecked");
		 		
				var rowFormatMaskID = document.getElementById("Row_FormatMaskID");
				var rowFormatMask = document.getElementById("lblFormatMask");
        
        		var rowDateFormatMaskID = document.getElementById("Row_DateFormatMaskID");
				var rowDateFormatMask = document.getElementById("lblDateFormatMask");
        
		 		var rowDecimalPlaces = document.getElementById("Row_DecimalPlaces");
		 		
	 			//UseCatalog = Yes
		 		if(valueID==1)
		 		{
					rowCatalogID.style.display = ((bIsIE)?"inline":"table-row");
					rowCatMemberID.style.display = ((bIsIE)?"inline":"table-row");
					mbHideMinMaxForNumericCatalog = true;
					rowMaxValue.style.display = 'none';
					rowMinValue.style.display = 'none';
					rowDefaultValue.style.display = 'none';
					rowMaxChecked.style.display = 'none';
					rowFormatMaskID.style.display = 'none';
					rowFormatMask.style.display = 'none';
          
          			rowDateFormatMaskID.style.display = 'none';
					rowDateFormatMask.style.display = 'none';
          
					rowDecimalPlaces.style.display = 'none';
					if (intMDVersion >= intEForms401v) {
						rowReadOnly.style.display = 'none';
						rowFormula.style.display = 'none';
						rowShowCondition.style.display = 'none';
					}
		 		}
		 		else
		 		{
					rowCatalogID.style.display = 'none';
					rowCatMemberID.style.display = 'none';
					rowMaxValue.style.display = ((bIsIE)?"inline":"table-row");
					rowMinValue.style.display = ((bIsIE)?"inline":"table-row");
					rowDefaultValue.style.display = ((bIsIE)?"inline":"table-row");
					rowFormatMaskID.style.display = ((bIsIE)?"inline":"table-row");
					rowFormatMask.style.display = (iFormatMaskID == 1)?((bIsIE)?"inline":"table-row"):'none';
        
					rowDateFormatMaskID.style.display = 'none';
					rowDateFormatMask.style.display = 'none';
					rowMaxChecked.style.display = "none";
					
					rowDecimalPlaces.style.display = ((bIsIE)?"inline":"table-row");
					if (intMDVersion >= intEForms401v) {
						rowReadOnly.style.display = ((bIsIE)?"inline":"table-row");
						if(valueTypeID==14)
						{
							rowFormula.style.display = ((bIsIE)?"inline":"table-row");
						}
						rowShowCondition.style.display = ((bIsIE)?"inline":"table-row");
					}
		 		}
	 		}
	 		else if (valueTypeID == 0) {
	 			//UseCatalog = Yes
	 			if (intMDVersion >= intEForms401v) {
			 		if(valueID==1) {
						rowReadOnly.style.display = 'none';
			 		}
			 		else {
						rowReadOnly.style.display = ((bIsIE)?"inline":"table-row");
			 		}
	 			}
	 		}
	 		
	 		if (blnHideAllowAdditionalAnswers)
	 		{
	 			rowAllowAdditionalAnswers.style.display = 'none';
	 			if (valueTypeID==2) {
	 				rowShowImageAndText.style.display = rowAllowAdditionalAnswers.style.display;
	 			}
	 		}
 		}
 		
		//funcion para llenar las opciones si no usa catalogos, seleccionando una opcion del combo de importcatalogoptions
		//conchita 11-oct-2011
		function filloptionstext()
		{
			var objCmb = <?=$myFormName?>_SaveForm.ImportCatOptions;
			if (objCmb == undefined || objCmb.selectedIndex == -1) {
				return;
			}
			var catsel = parseInt(objCmb.options[objCmb.selectedIndex].value); //obtener valor del catalogo seleccionado

		   	if(catsel==0)
			{
				<?=$myFormName?>_SaveForm.StrSelectOptions.value = '';
				<?=$myFormName?>_SaveForm.StrSelectOptions.disabled = false;
				return;
			}

			//Realizamos llamada con XMLObject para obtener los elementos del catalogo seleccionado
			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('XML Object not supported');
				return(new Array);
			}

			xmlObj.open('POST', 'getCatalogSelected.php?CatSel=' + catsel, false);
	 		xmlObj.send(null);
	
	 		var strMembers = Trim(unescape(xmlObj.responseText));
	 		var arrayMembers = new Array();
	 		var i, count;
	 		if(strMembers!="")
	 		{
	 			arrayMembers = strMembers.split('_SVSep_');
	 			count = arrayMembers.length;
	 			
	 			<?=$myFormName?>_SaveForm.StrSelectOptions.value = '';
	 			
	 			for(i=0; i<count; i++)
	 			{
	 				if(i!=0)
	 				{
	 					<?=$myFormName?>_SaveForm.StrSelectOptions.value = <?=$myFormName?>_SaveForm.StrSelectOptions.value + '\r';
	 				}
	 				
	 				<?=$myFormName?>_SaveForm.StrSelectOptions.value = <?=$myFormName?>_SaveForm.StrSelectOptions.value + arrayMembers[i];
	 			}
	 			
	 			<?=$myFormName?>_SaveForm.StrSelectOptions.disabled = true;
	 		}
		}
		//fin funcion	

		/*@AAL 25/02/2015: Abre la ventana de dialogo donde se mostrará el editor de fórmulas, recibe como 
		parámetro el nombre del elemento HTML (NameElement) de donde se tomará y escribirá la fórmula.*/
		function ShowWindowsDialogEditor(NameElement)
		{
			var SurveyID = '<?= $this->SurveyID ?>';
			var QuestionID = '<?= $this->QuestionID ?>';
			var aText = document.getElementsByName(NameElement);
			var text = "";
			if (NameElement.indexOf('QuestionMessage') != -1 && aText[0].style.display == "none")
				text = tinyMCE.activeEditor.selection.getContent({format: 'text'});
			else
				text = aText[0].value;

			var Parameters = 'SurveyID=' + SurveyID + '&QuestionID=' + QuestionID + '&NameElement=' + NameElement + '&Type=Q&aTextEditor=' + encodeURIComponent(text);
			openWindowDialog('formulaEditor.php?' + Parameters, [window], [NameElement], 620, 460, ResultEditFormulas, 'ResultEditFormulas');
		}

		/*@AAL 25/02/2015: Esta función recibe como parámetro sValue que corresponde al texto devuelto del editor de fórmulas 
		y sParams corresponde al nombre del elemento HTML donde se asignará la nueva fórmula o texto*/
		function ResultEditFormulas(sValue, sParams)
		{
			var aText = document.getElementsByName(sParams[0]);
			if (sParams[0].indexOf('QuestionMessage') != -1 && aText[0].style.display == "none")
				tinyMCE.activeEditor.selection.setContent(sValue + " ");
			else
				aText[0].value = sValue;
		}
		//@AAL
		
 		function updateFields()
		{
			if (cancel==true)
			{
				<?=$myFormName?>_CatMemberID_Update();
				
				var catMember = <?=$myFormName?>_SaveForm.CatMemberID;
				
				if (initialSelectionCatMember!=null)
				{
					for(i=0;i<catMember.options.length;i++)
					{
						if (catMember.options[i].value==initialSelectionCatMember)
						{	
							catMember.selectedIndex=i;
							break;
						}
					}
				}
			}

			cancel=false;
		}
		
		initialSelectionCatalogID = null;
		initialSelectionCatMember = null;
		initialSelectionCatMemberLatitude = null;
		initialSelectionCatMemberLongitude = null;
		initialSelectionQDisplayMode = null;
		initialSelectionQComponentStyleID = null;
		
		function setInitialValues()
		{
			var objCatalogID = <?=$myFormName?>_SaveForm.CatalogID;
			initialSelectionCatalogID = null;
			if (objCatalogID && objCatalogID.selectedIndex >= 0) {
				initialSelectionCatalogID = objCatalogID.options[objCatalogID.selectedIndex].value;
			}
			
	       	var catMember = <?=$myFormName?>_SaveForm.CatMemberID;
	        initialSelectionCatMember = null;
	        if (catMember && catMember.selectedIndex >= 0)
	        {
	        	initialSelectionCatMember = catMember.options[catMember.selectedIndex].value;
	        }
			
			var catMemberLatitude = <?=$myFormName?>_SaveForm.CatMemberLatitudeID;
	        initialSelectionCatMemberLatitude = null;
	        if (catMemberLatitude && catMemberLatitude.selectedIndex >= 0)
	        {
	        	initialSelectionCatMemberLatitude = catMemberLatitude.options[catMemberLatitude.selectedIndex].value;
	        }
			var catMemberLongitude = <?=$myFormName?>_SaveForm.CatMemberLongitudeID;
	        initialSelectionCatMemberLongitude = null;
	        if (catMemberLongitude && catMemberLongitude.selectedIndex >= 0)
	        {
	        	initialSelectionCatMemberLongitude = catMemberLongitude.options[catMemberLongitude.selectedIndex].value;
	        }
	        
	        var objQDisplayMode = <?=$myFormName?>_SaveForm.QDisplayMode;
	        initialSelectionQDisplayMode = null;
	        if (objQDisplayMode && objQDisplayMode.selectedIndex >= 0)
	        {
	        	initialSelectionQDisplayMode = objQDisplayMode.options[objQDisplayMode.selectedIndex].value;
	        }
	        
	        var objQComponentStyleID = <?=$myFormName?>_SaveForm.QComponentStyleID;
	        initialSelectionQComponentStyleID = null;
	        if (objQComponentStyleID && objQComponentStyleID.selectedIndex >= 0)
	        {
	        	initialSelectionQComponentStyleID = objQComponentStyleID.options[objQComponentStyleID.selectedIndex].value;
	        }
		}

		function verifyFieldsAndSave(target)
 		{
 			strBlanksField = "";

			if(Trim(<?=$myFormName?>_SaveForm.QuestionText.value)=="")
			{
				strBlanksField += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Question"))?>';
			}
			
			if(Trim(<?=$myFormName?>_SaveForm.AttributeName.value)=="")
			{
				strBlanksField += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Attribute Name"))?>';
			}
			
			if(Trim(<?=$myFormName?>_SaveForm.AttributeName.value)=="Surveys")
 			{	
 				strBlanksField+= '\n'+'<?=translate("\"Surveys\" is a reserved word. Please use another attribute name.")?>';
 			}
 			
			if(Trim(<?=$myFormName?>_SaveForm.AttributeName.value)=="User")
 			{	
 				strBlanksField+= '\n'+'<?=translate("\"User\" is a reserved word. Please use another attribute name.")?>';
 			}
			
			if(Trim(<?=$myFormName?>_SaveForm.AttributeName.value)=="Start Date")
 			{	
 				strBlanksField+= '\n'+'<?=translate("\"Start Date\" is a reserved word. Please use another attribute name.")?>';
 			}

			if(Trim(<?=$myFormName?>_SaveForm.AttributeName.value)=="End Date")
 			{	
 				strBlanksField+= '\n'+'<?=translate("\"End Date\" is a reserved word. Please use another attribute name.")?>';
 			}

			if(Trim(<?=$myFormName?>_SaveForm.AttributeName.value)=="Start Time")
 			{	
 				strBlanksField+= '\n'+'<?=translate("\"Start Time\" is a reserved word. Please use another attribute name.")?>';
 			}

			if(Trim(<?=$myFormName?>_SaveForm.AttributeName.value)=="End Time")
 			{	
 				strBlanksField+= '\n'+'<?=translate("\"End Time\" is a reserved word. Please use another attribute name.")?>';
 			}
 			/*@AAL 22/01/2015 Se valida que el número máximo de caracteres por respuesta sea menor o igual a 255. Issue:#6SFS1H*/
 			var Answers = Trim(<?=$myFormName?>_SaveForm.StrSelectOptions.value);
 			var Len = Answers.length;
 			if(Len > 0)
 			{
 				Answers = <?=$myFormName?>_SaveForm.StrSelectOptions.value.split('\n');
 				Len = Answers.length;  //Tamaño del nuevo array
 				var j = 0;
 				for(var i = 0; i < Len; i++)
 				{
 					if(Answers[i].length > 0)
 					{
 						j = j + 1;
 						if(Answers[i].length > 255)
 							strBlanksField += '\n- ' + '<?=sprintf(translate("Response items"))?>' + '(' + j + '): ' + '<?=sprintf(translate("Must have maximum 255 characters"))?>';
 					}
 				}
 			}
 			//@AAL
 			
 			//En esta parte se verifica si el AttributeName y el CatalogID ha sido modificado por valores validos
			var attributeNameOld = '<?=$this->AttributeName?>';
			var catalogIDOld = <?=$this->CatalogID?>;
			var questionID = <?=$this->QuestionID?>;

			var surveyID = <?=$this->SurveyID?>;
			var attributeName = <?=$myFormName?>_SaveForm.AttributeName.value;
			var gQTypeID = parseInt(<?=$myFormName?>_SaveForm.GQTypeID.value);
			var useCatalog = 0;
			var catalogID = 0;
			var selectedIdx;
			
	 		var qDataType = 1;
	 		if(gQTypeID == 2 || gQTypeID == 3)
	 		{
	 			qDataType = 0;
	 		}
	 		else
	 		{
	 			var objCmb = <?=$myFormName?>_SaveForm.QDataType;
	 			qDataType = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		}

			var catMemberID = 0;
			var selectedIdxCatMember;

			//Si la pregunta es de seleccion simple entonces se tiene q verificar
			//si hace referencia a un catalogo
			if(gQTypeID==<?=qtpSingle?>)
			{
				useCatalog = parseInt(<?=$myFormName?>_SaveForm.UseCatalog.value);

				if(useCatalog==1)
				{
					selectedIdx = parseInt(<?=$myFormName?>_SaveForm.CatalogID.selectedIndex);
					selectedIdxCatMember = parseInt(<?=$myFormName?>_SaveForm.CatMemberID.selectedIndex);
					
					if(selectedIdx<0)
					{
						useCatalog = 0;
						catalogID = 0;
						
						strBlanksField+= '\n'+'<?=translate("You must have selected a valid catalog.")?>';
					}
					else if(selectedIdxCatMember<0)
					{
						useCatalog = 0;
						catalogID = 0;
						
						strBlanksField+= '\n'+'<?=translate("You must have selected a valid attribute.")?>';
					}
					else
					{
						catalogID = parseInt(<?=$myFormName?>_SaveForm.CatalogID.value);
					}
				}
			}
			
			//Obtenemos los valores de QDisplayMode, MCInputType, IsMultiDimension para verificar que los valores de Category Dimension
			//sean correcto siempre y cuanto estemos dentro de una seccion estatica y para no verificar el AttributeName porque en este caso 
			//no aplica ya que al AttributeName terminaria siendo un elemento de Category Dimension
			var isnewobject = <?=(($this->isNewObject()==true)?1:0)?>;
			
			//Obtego valor de QDisplayMode
	 		var objCmb = <?=$myFormName?>_SaveForm.QDisplayMode;
	 		var qDisplayMode = parseInt(objCmb.options[objCmb.selectedIndex].value);

	 		//Obtengo valor de mcinputtype
	 		var objCmb = <?=$myFormName?>_SaveForm.MCInputType;
	 		var mcinputtype = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		
	 		//Obtengo valor de ismultidimension
	 		var objCmb = <?=$myFormName?>_SaveForm.IsMultiDimension;
	 		var ismultidimension = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		
	 		//Obtengo el valor de UseCategoryDimChoice
	 		var usecatdimchoice = 0;
	 		if (insideDynamicSection==0)
	 		{
	 			var objCmb = <?=$myFormName?>_SaveForm.UseCategoryDimChoice;
	 			usecatdimchoice = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		}
	 		
	 		if(isnewobject==1 && insideDynamicSection==0 && gQTypeID==3 && qDisplayMode==0 && mcinputtype==1 && ismultidimension==0 && usecatdimchoice==-1)
	 		{
	 			strBlanksFieldCatDim = '';
	 			
				if(Trim(<?=$myFormName?>_SaveForm.CategoryDimName.value)=="")
				{
					strBlanksFieldCatDim += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Category Dimension Name"))?>';
				}
				
				if(Trim(<?=$myFormName?>_SaveForm.ElementDimName.value)=="")
				{
					strBlanksFieldCatDim += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Element Dimension Name"))?>';
				}
				
				if(Trim(<?=$myFormName?>_SaveForm.CategoryDimName.value)==Trim(<?=$myFormName?>_SaveForm.ElementDimName.value))
				{
					strBlanksFieldCatDim+= '\n'+'<?=translate("Category Dimension Name must be different to Element Dimension Name. Please use different names.")?>';
				}
				
				if(Trim(<?=$myFormName?>_SaveForm.ElementIndName.value)=="Surveys")
	 			{	
	 				strBlanksFieldCatDim += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Element Indicator Name"))?>';
	 			}
	 			
	 			strBlanksField+=strBlanksFieldCatDim;
	 			
	 			//Si esta vacio es que no hubo errores anteriormente
	 			if(strBlanksFieldCatDim=='')
	 			{
					var xmlObj = getXMLObject();
					if (xmlObj == null)
					{
						alert('<?=translate('XML Object not supported')?>');
						return(new Array);
					}
		
					xmlObj.open('POST', 'verifyCategoryDimensionName.php', false);
					xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
			 		xmlObj.send('SurveyID='+surveyID+'&CategoryDimName='+encodeURIComponent(<?=$myFormName?>_SaveForm.CategoryDimName.value)+'&ElementDimName='+encodeURIComponent(<?=$myFormName?>_SaveForm.ElementDimName.value)+'&ElementIndName='+encodeURIComponent(<?=$myFormName?>_SaveForm.ElementIndName.value));
		
			 		if(xmlObj.status == 404)
					{
						alert('<?=translate('Unable to launch validation process')?>');
						return false;
					}
					
			 		strResponseData = Trim(unescape(xmlObj.responseText));
			 		
			 		arrResponseData = strResponseData.split('<SVSEP>');
			 		if(arrResponseData.length == 0)
			 		{
			 			alert('<?=translate('Error found during data validation')?>');
						return false;
			 		}
			 		
			 		if(arrResponseData[0]!="OK")
			 		{
						strErrMsg = arrResponseData[0];
						if(arrResponseData.length > 1)
						{
							strErrMsg = arrResponseData[1];
						}
						
		  				strBlanksField += '\n- ' + '<?=translate('Error found during data validation')?>: ' + strErrMsg;
			 		}
	 			}
	 		}
	 		else if(isnewobject==1 && insideDynamicSection==0 && gQTypeID==3 && qDisplayMode==0 && mcinputtype==1 && ismultidimension==0 && usecatdimchoice!=-1 && usecatdimchoice!=0)
	 		{
	 			//En este caso es cuando se esta usando una Category Dimension q ya fue creada anteriormente
	 			//Por lo tanto no se va a realizar accion al respecto porque en teoria se valido desde el momento 
	 			//de elegir la dimension se reviso que esta fuera valida
	 		}
			else
			{
				var xmlObj = getXMLObject();
				if (xmlObj == null)
				{
					alert('<?=translate('XML Object not supported')?>');
					return(new Array);
				}
				
				xmlObj.open('POST', 'verifyQuestionAttributeName.php', false);
				xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
		 		xmlObj.send('SurveyID='+surveyID+'&QuestionID='+questionID+'&AttributeName='+encodeURIComponent(attributeName)+'&CatalogID='+catalogID+'&AttributeNameOld='+encodeURIComponent(attributeNameOld)+'&CatalogIDOld='+catalogIDOld+'&GQTypeID='+gQTypeID);
				
		 		if(xmlObj.status == 404)
				{
					alert('<?=translate('Unable to launch validation process')?>');
					return false;
				}
				
		 		strResponseData = Trim(unescape(xmlObj.responseText));
		 		
		 		arrResponseData = strResponseData.split('<SVSEP>');
		 		if(arrResponseData.length == 0)
		 		{
		 			alert('<?=translate('Error found during data validation')?>');
					return false;
		 		}
		 		
		 		if(arrResponseData[0]!="OK")
		 		{
					strErrMsg = arrResponseData[0];
					if(arrResponseData.length > 1)
					{
						strErrMsg = arrResponseData[1];
					}
					
	  				strBlanksField += '\n- ' + '<?=translate('Error found during data validation')?>: ' + strErrMsg;
		 		}
			}
	 		
			//Si la pregunta es numerica entonces se tiene q verificar
			//si la validacion establecida es correcta
			var minValue, intMinValue, maxValue, intMaxValue;

			if(qDataType==<?=qtpOpenNumeric?> || (gQTypeID == <?=qtpMulti?> && (qDisplayMode == <?=dspVertical?> || qDisplayMode == <?=dspHorizontal?>) && mcinputtype == <?=mpcNumeric?>))
			{
				minValue = <?=$myFormName?>_SaveForm.MinValue.value;
				maxValue = <?=$myFormName?>_SaveForm.MaxValue.value;
				
				if(minValue!='' && maxValue!='')
				{
					intMinValue = parseInt(minValue);
					intMaxValue = parseInt(maxValue);
					
					if(intMinValue > intMaxValue)
					{
						strBlanksField += '\n- ' + '<?=translate('MAX value must be higher than MIN value')?>';
					}
				}
			}
			else
			{
				<?=$myFormName?>_SaveForm.MinValue.value = "";
				<?=$myFormName?>_SaveForm.MinValue._value = "";
				<?=$myFormName?>_SaveForm.MaxValue.value = "";
				<?=$myFormName?>_SaveForm.MaxValue._value = "";
			}

			var msgDeleteAnswers = false;
<?			
		//31Ene2013: Cuando se está editando una pregunta se tiene que mandar mensaje de advertencia al usuario
		//en los casos en los que la pregunta se vuelve a crear como dimensión por que las respuestas que se tengan
		//se perderán.				
		if(!$this->isNewObject())
		{
?>
			var oldQTypeID = <?=$this->QTypeID?>;
			useCatalog = parseInt(<?=$myFormName?>_SaveForm.UseCatalog.value);
			var useCatalogOld = <?=$this->UseCatalog?>;
			catalogID = parseInt(<?=$myFormName?>_SaveForm.CatalogID.value);
			catMemberID = parseInt(<?=$myFormName?>_SaveForm.CatMemberID.value);
			var catMemberIDOld = <?=$this->CatMemberID?>;
			
			if (<?=$myFormName?>_SaveForm.CatMemberLatitudeID) {
				catMemberLatitudeID = parseInt(<?=$myFormName?>_SaveForm.CatMemberLatitudeID.value);
			}
			var catMemberLatitudeIDOld = <?=$this->CatMemberLatitudeID?>;
			if (<?=$myFormName?>_SaveForm.CatMemberLongitudeID) {
				catMemberLongitudeID = parseInt(<?=$myFormName?>_SaveForm.CatMemberLongitudeID.value);
			}
			var catMemberLongitudeIDOld = <?=$this->CatMemberLongitudeID?>;
			
			var isMultiDimensionOld = <?=$this->IsMultiDimension?>;
			var mCInputTypeOld = <?=$this->MCInputType?>;
			var useCatToFillMC = <?=$myFormName?>_SaveForm.UseCatToFillMC.value;
			var useCatToFillMCOld = <?=$this->UseCatToFillMC?>;
			
			//Cambio de tipo de dato
			if(gQTypeID==<?=qtpOpen?>)
			{	//Si es pregunta tipo OPEN
				if(qDataType!=oldQTypeID)
				{
					msgDeleteAnswers = true;
				}
			}
			else
			{
				if(gQTypeID!= oldQTypeID)
				{
					msgDeleteAnswers = true;
				}
			}
			
			if(gQTypeID==<?=qtpSingle?> && useCatalog!=useCatalogOld)
			{
			//Si es pregunta simple choice y hubo cambio en el campo UseCatalog					
				msgDeleteAnswers = true;
			}
			else if(gQTypeID==<?=qtpSingle?> && useCatalog==1 && catalogID!=catalogIDOld)
			{
			//Si es pregunta simple choice que utiliza catálogo y se cambia el catálogo seleccionado
				msgDeleteAnswers = true;
			}
			else if(gQTypeID==<?=qtpSingle?> && useCatalog==1 && catalogID==catalogIDOld && catMemberID!=catMemberIDOld)
			{
			//Si es pregunta simple choice que utiliza catálogo y se selecciona un miembro diferente del catalogo aunque se utilice el mismo catálogo
				msgDeleteAnswers = true;
			}
			else if(gQTypeID==<?=qtpMulti?> && ismultidimension!=isMultiDimensionOld)
			{
			//Si es pregunta de seleccion multiple y cambia en su comportamiento multidimension
				msgDeleteAnswers = true;
			}
			else if(gQTypeID==<?=qtpMulti?> && mcinputtype!=mCInputTypeOld)
			{
			//Si es pregunta de seleccion multiple y cambia en su campo MCInputType
				msgDeleteAnswers = true;
			}
			else if(gQTypeID==<?=qtpMulti?> && useCatToFillMC!=useCatToFillMCOld)
			{
			//Si es pregunta de seleccion multiple y se cambia para usar/no usar un catalogo
				msgDeleteAnswers = true;
			}
			else if(gQTypeID==<?=qtpMulti?> && useCatToFillMC==1 && catalogID!=catalogIDOld)
			{
			//Si es pregunta de seleccion multiple que utiliza catálogo pero se cambia el catálogo
				msgDeleteAnswers = true;
			}
<?		
		}
		
		//Al editar una simple choice que no puedas elegir un atributo del catálogo de la dinámica 
		//que esté "abajo" del atributo de la dinámica.
		$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
		if($dynamicSectionID>0 && $dynamicSectionID!=$this->SectionID)
		{
			$dynamicSection = BITAMSection::NewInstanceWithID($this->Repository, $dynamicSectionID);
			require_once("catalogmember.inc.php");
			//@JAPR 2014-05-20: Corregido un bug, la función se llama a la clase del objeto, no a la de la colección
			$dynAtOrdePos = BITAMCatalogMember::getCurrentOrderPos($this->Repository, $dynamicSection->CatalogID, $dynamicSection->CatMemberID);
			//@JAPR
			
			$sql = "SELECT MemberID FROM SI_SV_CatalogMember WHERE CatalogID = ".$dynamicSection->CatalogID." AND MemberOrder >= ".$dynAtOrdePos." ORDER BY MemberID";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$arrayMemberIDs = array();
			
			while (!$aRS->EOF)
			{
				$arrayMemberIDs[] = (int) $aRS->fields["memberid"];
				$aRS->MoveNext();
			}			
?>
			var arrayMemberIDs = new Array();
			
<?	
			foreach ($arrayMemberIDs as $i => $memberID)
			{
?>			
				arrayMemberIDs[<?=$i?>] = <?=$memberID?>;
<?
			}
?>
			//Catálogo que utiliza la sección dinámica
			var dymSecCatalogID = <?=$dynamicSection->CatalogID?>;
			
			if(gQTypeID==<?=qtpSingle?> && useCatalog==1 && catalogID==dymSecCatalogID)
			{
				//Si es pregunta simple choice que utiliza catálogo y utiliza el catálogo de la sección dinámica
				//no puede seleccionar un atributo que tiene mayor nivel de detalle que el de la dinámica
				if(inArray(arrayMemberIDs, catMemberID))
				{
					strBlanksField += '\n- ' + '<?=translate("You cannot select an attribute with a higher detail level than the dynamic section\'s attribute")?>';
				}
				
			}			
<?
		}
?>
			if(strBlanksField!="")
  			{
				alert('<?=translate("Please verify")?>:' + strBlanksField );
  			}
  			else
  			{
  				var answerConfirm = true;
  				if(msgDeleteAnswers)
				{
					 answerConfirm = confirm('<?=translate("All saved entries will be lost if you change the question definition")?>');
				}

  				if(answerConfirm)
  				{
	  				//Antes de enviar a grabar verificar StrSelectOptions se encuentre habilitado para que lleguen los valores al POST
	  				<?=$myFormName?>_SaveForm.StrSelectOptions.disabled = false;
	  				
	  				//Y los campos de Catgory Dimension tambien se requieren deshabiitarse para q se vayan en el POST
		 			if(insideDynamicSection==0)
	 				{
	 					<?=$myFormName?>_SaveForm.CategoryDimName.disabled = false;
	 					<?=$myFormName?>_SaveForm.ElementDimName.disabled = false;
	 					<?=$myFormName?>_SaveForm.ElementIndName.disabled = false;
	 				}
	 				
	 				/************/
  					//Llenar los campos en los que se guardarán los componentes del diseñador
	  				var newEditor = false;
	  				for(var divName in aHTMLDivObjects)
	  				{
	  					var textAreaName = divName.replace("HDiv", "");
	  					textAreaName = textAreaName.replace("Default", "");
	  					textAreaNameObj = document.getElementById(textAreaName);
	  					textAreaNameObj.value=aHTMLDivObjects[divName].oPart.save(true,',');
	  					if(textAreaNameObj.value!='')
	  					{
	  						newEditor = true;
	  					}
	  				}
	  				//Si ya se utilizó por lo menos una vez el nuevo editor en la pantalla de sección
	  				//entonces los campos HTML que se manejaban anteriormente serán reemplazados con el HTML
	  				//generado a partir del nuevo editor, esto se hará para todos los dispositivos
	  				if(newEditor==true)
	  				{
		  				for(var divName in aHTMLDivObjects)
		  				{
		  					var textAreaName = divName.replace("HDiv", "");
		  					textAreaName = textAreaName.replace("Default", "");
							textAreaName = textAreaName+'HTML';
		  					textAreaNameObj = document.getElementById(textAreaName);
		  					textAreaNameObj.value=aHTMLDivObjects[divName].oPart.saveHTML();
	  						//alert(textAreaName+' - saveHTML(): '+aHTMLDivObjects[divName].oPart.saveHTML());
	  						//alert(textAreaName+' - textAreaNameObj.value: '+textAreaNameObj.value);
	  						//alert(textAreaName+' - textAreaNameObj.innerHTML: '+textAreaNameObj.innerHTML);
	
		  				}
	  				}
	
	  				<?=$myFormName?>_Ok(target);
  				}
  			}
 		}
<? 		
		//@JAPR 2015-01-20: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
		//si es utilizado y advertir al usuario (#75TPJE)
?>		
 		function verifyObjectAndRemove() {
			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('<?=translate('XML Object not supported')?>');
				return(new Array);
			}
			
			xmlObj.open('POST', 'verifyUsedObjectToRemove.php', false);
			xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
	 		xmlObj.send('ObjectID=<?=$this->QuestionID?>&ObjectType=<?=otyQuestion?>');
	 		if(xmlObj.status == 404)
			{
				alert('<?=translate('Unable to launch validation process')?>');
				return false;
			}
			
	 		strResponseData = Trim(unescape(xmlObj.responseText));
	 		arrResponseData = strResponseData.split('<SVSEP>');
	 		if(arrResponseData.length == 0)
	 		{
	 			alert('<?=translate('Error found during data validation')?>');
				return false;
	 		}
	 		
	 		var strErrMsg = '';
	 		if(arrResponseData[0]!="OK")
	 		{
				strErrMsg = arrResponseData[0];
				if(arrResponseData.length > 1)
				{
					strErrMsg = arrResponseData[1];
				}
				
  				strErrMsg = '<?=translate("This question cannot be removed because the following configurations contain references to it")?>: ' + "\r\n" + strErrMsg.replace(/\r/gi, "");
  				alert(strErrMsg);
  				return;
	 		}
	 		
	 		<?=$myFormName?>_Remove();
 		}
<?
		//@JAPR
?> 		
		function cleareBavelActionFields() {
			var objField = <?=$myFormName?>_SaveForm.eBavelActionFieldsData;
			if (objField != null) {
				objField.value = '';
			}
		}
		
		function openAssigneBavelActionFields()
		{
			var inteBavelAppID = 0;
			var inteBavelFormID = 0;
			var objField = <?=$myFormName?>_SaveForm.eBavelAppID;
			if (objField != null) {
				inteBavelAppID = objField.value;
			}
			var objField = <?=$myFormName?>_SaveForm.eBavelActionFormID;
			if (objField != null) {
				inteBavelFormID = objField.value;
			}
			
			openWindowDialog('main.php?BITAM_PAGE=QuestionActionMap&SurveyID=<?=$this->SurveyID?>&QuestionID=<?=$this->QuestionID?>&eBavelAppID='+inteBavelAppID+'&eBavelFormID='+inteBavelFormID, [window], [], 600, 600, openAssigneBavelActionFieldsDone, 'openAssigneBavelActionFieldsDone');
		}
		
		function openAssigneBavelActionFieldsDone(sValue, sParams)
		{
			var strInfo = sValue;
			//Actualiza la cadena de los campos mapeados de eBavel
			if(strInfo!=null && strInfo!=undefined)
			{
				try {
					<?=$myFormName?>_SaveForm.eBavelActionFieldsData.value = strInfo;
				} catch (e) {
				}
			}
		}
		
		//03-Ene-2014: Validación para que el campo OtherLabel esté activo sólo cuando AllowAdditionalAnswers está seleccionado
		function onChangeAllowAdditionalAnswers()
		{
			var objAllowAdditionalAnswers = <?=$myFormName?>_SaveForm.AllowAdditionalAnswers;
			var objOtherLabel = <?=$myFormName?>_SaveForm.OtherLabel;
			
			if(objOtherLabel!=undefined)
			{
				if(!objAllowAdditionalAnswers[1].checked)
				{
					objOtherLabel.readOnly = true;
					objOtherLabel.value = '';
				}
				else
				{
					objOtherLabel.readOnly = false;
				}
			}
		}
		
		//03-Ene-2014: Validación para que el campo CommentLabel esté activo sólo cuando HasReqComment tenga seleccionada
		//cualquiera de las opciones "Required" o "Optional"
		function onChangeHasReqComment()
		{
			var objHasReqComment = <?=$myFormName?>_SaveForm.HasReqComment;
			var objCommentLabel = <?=$myFormName?>_SaveForm.CommentLabel;
			
			if(objCommentLabel!=undefined)
			{
				if(objHasReqComment.options[objHasReqComment.selectedIndex].value==0)
				{
					objCommentLabel.readOnly = true;
					objCommentLabel.value = '';
				}
				else
				{
					objCommentLabel.readOnly = false;
				}
			}
		}
		
		/*@AAL 01/04/2015: Validación modificada para mostrar u ocultar ya sea Image Editor o tinyMCE cuado se quiera editar */
 		var IsEdit = false;
 		function onChangeEditor(IsNew)
 		{
 			var objCmbEditor = <?=$myFormName?>_SaveForm.EditorType;
 			var editorTypeValue = parseInt(objCmbEditor.options[objCmbEditor.selectedIndex].value);
 			if(IsNew)
 			{
 				if(editorTypeValue == 1){
	 				document.getElementById("QuestionMessageSpanDesigner").style.display="inline";
	 				document.getElementById("QuestionImageTextDefaultDiv").style.display="inline";
	 				
 				}
	 			else{
	 				document.getElementById("QuestionMessageSpanDesigner").style.display="none";
	 				document.getElementById("QuestionImageTextDefaultDiv").style.display="none";
	 			}
 			}
 			else{//Si se va a editar entonces se verifica el tipo de editor para mostrarlo al usuario
 				if (IsEdit) {
 					if(editorTypeValue == 1){
 						document.getElementById("QuestionMessageSpanDesigner").style.display="inline";
 						document.getElementById("QuestionImageTextDefaultDiv").style.display="inline";
 					}
		 			else{
						document.getElementById("QuestionMessageTabs").style.display="inline";
						document.getElementById("QuestionImageTextDefaultDiv").style.display="inline";
		 			}
					IsEdit = false;
				}
				else{
					document.getElementById("QuestionMessageSpanDesigner").style.display="none";
					document.getElementById("QuestionMessageTabs").style.display="none";
					document.getElementById("QuestionImageTextDefaultDiv").style.display="none";
					IsEdit = true;
				}
 			}
 			
			//Cargar el div de la pestaña default
 			var elemDefault = document.getElementById("defaultQuestionMessage");
			changeTab(elemDefault,'QuestionMessage');
 		}
		
 		</script>
<?
		//13Feb2013: Si es pregunta simple choice que usa catálogo, se verifica si la encuensta está en alguna agenda que utilice el catálogo y atributo de la pregunta
		//y si es así se manda el mensaje "Si elimina la pregunta entonces la encuesta ya no estará incluida en alguna agenda. ¿Desea eliminar la pregunta?"
		if($this->QTypeID==qtpSingle && $this->UseCatalog)
		{
		
			$sql= "SELECT t1.AgendaID 
					FROM SI_SV_SurveyAgenda t1, SI_SV_SurveyAgendaDet t2
					WHERE t1.AgendaID = t2.AgendaID
					AND t1.CatalogID=".$this->CatalogID." AND t1.AttributeID=".$this->CatMemberID." AND SurveyID=".$this->SurveyID;
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda t1, SI_SV_SurveyAgendaDet t2 ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//IDs de agendas que utilizan el catálogo y atributo de la pregunta Simple Choice y tienen asociadas la encuesta
			$arrayAgendasIDs=array();
			
			while (!$aRS->EOF)
			{
				$arrayAgendasIDs[] = (int) $aRS->fields["agendaid"];
			
				$aRS->MoveNext();
			}

			//Obtener nombre de las agendas			
			$arrayAgendasNames=array();
			$strAgendas = "";
			if(count($arrayAgendasIDs)>0)
			{

				$sql = "SELECT t1.AgendaID, t1.AgendaName, t1.CatalogID, t2.ParentID, t2.CatalogName, t2.TableName, t1.AttributeID, t3.MemberName, t1.FilterText, 
						t1.CaptureStartDate, t1.CaptureStartTime, t1.UserID, t1.LastDateID 
						FROM SI_SV_SurveyAgenda t1 
						LEFT OUTER JOIN SI_SV_Catalog t2 ON t1.CatalogID = t2.CatalogID 
						LEFT OUTER JOIN SI_SV_CatalogMember t3 ON t1.CatalogID = t3.CatalogID AND t1.AttributeID = t3.MemberID 
						WHERE t1.AgendaID IN (".implode(",", $arrayAgendasIDs).")";
				
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			
				while (!$aRS->EOF)
				{
					$agenName = $aRS->fields["catalogname"].' - '.$aRS->fields["membername"].' - '.$aRS->fields["filtertext"];
					$arrayAgendasNames[] = $agenName;
					$strAgendas.="\\r\\n"." - ".$agenName;
					$aRS->MoveNext();
				}
			}		
			
			if(trim($strAgendas)!="")
			{
 				//Si es pregunta simple choice que usa catálogo, se verifica si la encuensta está en alguna agenda que utilice el catálogo y atributo de la pregunta
 				//y si es así se manda el mensaje de advertencia cuando se elimine la pregunta
				$this->MsgConfirmRemove = translate("The form will not be included on any agenda if you delete the question").".".translate("Do you wish to delete the question?")."\\r\\n".translate("The Survey will be eliminated from the agendas").": ".$strAgendas;
			}
		}
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
 		$myFormName = get_class($this);
?>
	 	<script language="JavaScript">
	 	setInitialValues();

		objOkSelfButton = document.getElementById("<?=$myFormName?>_OkSelfButton");
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		
		objOkParentButton = document.getElementById("<?=$myFormName?>_OkParentButton");
	
		if(objOkParentButton)
		{
			objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		}
<?
		if($this->isNewObject())
 		{
?>
			var objCmb = <?=$myFormName?>_SaveForm.QDisplayMode;
			objCmb.options[objCmb.selectedIndex].value = 0; 			

			objOkNewButton = document.getElementById("<?=$myFormName?>_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
		//@JAPR 2015-01-20: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
		//si es utilizado y advertir al usuario (#75TPJE)
		else {
			//Se lanza a la función que validará si puede o no remover el objeto, para lo cual va al server a realizar una consulta personalizada
			//según el tipo de objeto
?>
			objRemoveButton = document.getElementById("<?=$myFormName?>_RemoveButton");
			if (objRemoveButton) {
				objRemoveButton.onclick=new Function("verifyObjectAndRemove();");
			}
<?
		}
		//@JAPR
?>
		try {
			$('tr:not([style])[id^="Row_"]:has(td[class=""])').hide();
		} catch (e) {}
		
		</script>
<?
	}
	
	function isMasterDet() {
		$isMasterDet = false;
		$theSection = BITAMSection::NewInstanceWithID($this->Repository, $this->SectionID);
		if($theSection->SectionType == sectMasterDet) {
			$isMasterDet = true;
		}
		return $isMasterDet;
	}
	
	function getLastQuestionNumberOfSection()
	{
		$lastQuestionNumber = 0;
		
		$sql = "SELECT QuestionNumber FROM SI_SV_Question WHERE SectionID = ".$this->SectionID." ORDER BY QuestionNumber DESC";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if($aRS===false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$lastQuestionNumber = (int)$aRS->fields["questionnumber"];
		}
		
		return $lastQuestionNumber;
	}
	
	//@JAPR 2012-06-08: Ahora se permitirá cambiar la opción de usar Foto durante la edición de preguntas
	//Elimina la dimensión imagen de las preguntas que tienen una foto opcional/requerida
	//@JAPRDescontinuada en v6
    function removeModelDimensionIMG()
    {
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		if(!is_null($this->ImgDimID) && $this->ImgDimID>0)
		{
			$strOriginalWD = getcwd();
			
			//Eliminar una dimension
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			
			//@JAPR 2012-10-19: Validado en caso de no existir mas la dimensión especificada
			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->ImgDimID);
			if (!is_null($anInstanceModelDim)) {
				//Vamos a eliminar la dimension aun cuando este siendo usada en un escenario por ello deshabilitamos la validacion
				$anInstanceModelDim->Dimension->CheckStages = false;
				$anInstanceModelDim->Dimension->remove();
			}
			//@JAPR
			
			chdir($strOriginalWD);
			
			//@JAPR 2012-06-14: Ahora se permitirá cambiar la opción de usar Foto durante la edición de preguntas
			$this->ImgDimID = 0;
			$sql = "UPDATE SI_SV_Question SET ImgDimID = ".$this->ImgDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//@JAPRWarning: ¿ Se deberían eliminar las fotos tomadas ? si se decide hacer, para facilidad se puede recorrer el contenido de la
			//tabla de dimensión buscando los nombres de imágenes que coincidan con el ID de encuesta y pregunta que está eliminando su dimensión Image
			//y sólo remover esas fotos, ya que de lo contrario se debería recorrer la carpeta de imagenes y buscar entre todos los archivos que
			//corresponderían a todas las preguntas de la encuesta, así que serían mas elementos a recorrer
		}
    }
    //@JAPR
    
	//@JAPRDescontinuada en v6
	function removeModelDimensionDOC()
    {
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		
		if(!is_null($this->DocDimID) && $this->DocDimID>0)
		{
			$strOriginalWD = getcwd();
			
			//Eliminar una dimension
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			
			//@JAPR 2012-10-19: Validado en caso de no existir mas la dimensión especificada
			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->ModelID, -1, $this->DocDimID);
			if (!is_null($anInstanceModelDim)) {
				//Vamos a eliminar la dimension aun cuando este siendo usada en un escenario por ello deshabilitamos la validacion
				$anInstanceModelDim->Dimension->CheckStages = false;
				$anInstanceModelDim->Dimension->remove();
			}
			//@JAPR
			
			chdir($strOriginalWD);
			
			//@JAPR 2012-06-14: Ahora se permitirá cambiar la opción de usar Foto durante la edición de preguntas
			$this->DocDimID = 0;
			$sql = "UPDATE SI_SV_Question SET DocDimID = ".$this->DocDimID." WHERE QuestionID = ".$this->QuestionID." AND SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//@JAPRWarning: ¿ Se deberían eliminar las fotos tomadas ? si se decide hacer, para facilidad se puede recorrer el contenido de la
			//tabla de dimensión buscando los nombres de imágenes que coincidan con el ID de encuesta y pregunta que está eliminando su dimensión Image
			//y sólo remover esas fotos, ya que de lo contrario se debería recorrer la carpeta de imagenes y buscar entre todos los archivos que
			//corresponderían a todas las preguntas de la encuesta, así que serían mas elementos a recorrer
		}
    }
    
	function getAnswerComment($entryID)
	{
		$strComment = "";
		$sql = "SELECT StrComment FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$this->SurveyID." 
				AND QuestionID = ".$this->QuestionID." AND FactKey = ".$entryID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if(!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$strComment = $aRS->fields["strcomment"];
		}
		
		return $strComment;
	}

	function getAnswerPhoto($entryID)
	{
		$strPhoto = "";
		
		$sql = "SELECT MainFactKey, PathImage FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$this->SurveyID." 
				AND QuestionID = ".$this->QuestionID." AND FactKey = ".$entryID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if(!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$strPhoto = $aRS->fields[strtolower("PathImage")];
                        $mainfactkey = $aRS->fields[strtolower("MainFactKey")];
		}
                
        $DocsDir = "surveyimages";

        if (!file_exists($DocsDir)) {
            return; //dir not found
        }
        
        $RepositoryDir = $DocsDir . "\\" . trim($_SESSION["PABITAM_RepositoryName"]);
        
        if (!file_exists($RepositoryDir)) {
            if (!mkdir($RepositoryDir)) {
                return; //the repository dir could not be created
            }
        }
        
        $ProjectDir = $RepositoryDir . "\\";
        
        if (!file_exists($ProjectDir)) {
            if (!mkdir($ProjectDir)) {
                return; //the project dir could not be created
            }
        }
        
        $ProjectDir = str_replace('\\','/',$ProjectDir);
        $strCompletePath = $ProjectDir . $strPhoto;
        
        if($strPhoto == '') {
           $strCompletePath = '';
           $mainfactkey = '';
        }

        $photoInfo = array();
        $photoInfo[0] = $strCompletePath;
        $photoInfo[1] = $mainfactkey;
		return $photoInfo;
	}
	
	static function getIdentifierQuestion($aRepository, $surveyID)
	{
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$surveyID." AND IsFieldSurveyID = 1 ORDER BY QuestionID";
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if($aRS->EOF)
		{
			return null;
		}
		
		$questionArray = array();
		while(!$aRS->EOF)
		{
			$questionID = (int)$aRS->fields["questionid"];
			$questionArray[] = $questionID;
			
			$aRS->MoveNext();
		}
		
		$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID, $questionArray);

		return $questionCollection;
	}
	
	static function getFirstCatalogQuestion($aRepository, $aSurveyID)
	{
		$aQuestion = null;
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE CatalogID >0 AND UseCatalog=1 AND SurveyID = ". $aSurveyID. " AND QTypeID <> ".qtpOpenNumeric." ORDER BY QuestionNumber";
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{	//Se considera sólo la primera pregunta encontrada que usa catálogo
			$questionID = (int) $aRS->fields["questionid"];
			$aQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $questionID);
		}
		
		return $aQuestion;
	}
	
	static function getCurrentOrderPos($aRepository, $aSurveyID, $aQuestionID)
	{
		$sql = "SELECT QuestionNumber FROM SI_SV_Question WHERE SurveyID = ".$aSurveyID." AND QuestionID = ".$aQuestionID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$orderPos = -1;

		if(!$aRS->EOF)
		{
			$orderPos = (int)$aRS->fields["questionnumber"];
		}
		
		return $orderPos;
	}
    
	function GetActionTitle() {
		return $this->ActionTitle;
	}
	
	function GetActionText() {
		return $this->ActionText;
	}
	
	function GetActionResponsible() {
		$responsible = "";
		$sql = "SELECT NOM_LARGO FROM SI_USUARIO WHERE CLA_USUARIO = ".$this->ResponsibleID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if(!$aRS->EOF) {
			$responsible = (string) @$aRS->fields["nom_largo"];
		}
		return $responsible;
	}
	
	function GetActionCategory() {
		$category = "";
		$arrayCategory = BITAMCatalog::GetCategories($this->Repository, true);
		foreach($arrayCategory as $catID => $category) {
			if($catID == $this->CategoryID) {
				$category = $category;
				break;
			}
		}
		return $category;
	}
	
	function GetActionDaysDueDate() {
		return $this->DaysDueDate;
	}
	
	function GetActionStatus() {
		$statusSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'EBAVELDEFAULTACTIONSTATUS');
		$strActionStatus = "";
		if (!is_null($statusSetting)) {
			$strActionStatus = $statusSetting->SettingValue;
		}
		return $strActionStatus;
	}
	
	function GetDefaultValueForMapped ($aSurveyFieldID) {
		$strDefaultValue = '';
		if ($aSurveyFieldID < 0) {
			$aSurveyFieldID *= -1;
			switch ($aSurveyFieldID) {
				//Actions
				case safidDescription:
					$strDefaultValue = $this->GetActionText();
					break;
				case safidResponsible:
					$strDefaultValue = $this->GetActionResponsible();
					break;
				case safidCategory:
					$strDefaultValue = $this->GetActionCategory();
					break;
				case safidDueDate:
					$strDefaultValue = $this->GetActionDaysDueDate();
					break;
				case safidStatus:
					$strDefaultValue = $this->GetActionStatus();
					break;
				case safidTitle:
					$strDefaultValue = $this->GetActionTitle();
					break;
				//Forms
				case sfidStartDate:
					$strDefaultValue = "{selSurvey.['surveyDate']}";
					break;
				case sfidStartHour:
					$strDefaultValue = "{selSurvey.['surveyHour']}";
					break;
				
				/*case sfidEndDate:
				
					break;
				case sfidEndHour:
				
					break;
				*/
				case sfidLocation:
					$strDefaultValue = "{selSurvey.['getSurveyCoords']}";
					break;
				/*case sfidSyncDate:
				
					break;
				case sfidSyncHour:
				
					break;
				case sfidServerStartDate:
				
					break;
				case sfidServerStartHour:
				
					break;
				case sfidServerEndDate:
				
					break;
				case sfidServerEndHour:
				
					break;
				case sfidScheduler:
				
					break;
				*/
				case sfidEMail:
					$strDefaultValue = "{objSettings.['user']}";
					break;
				/*
				case sfidDuration:
				
					break;
				case sfidAnsweredQuestions:
				
					break;
				case sfidEntrySection:
				
					break;
				case sfidDynamicPage:
				
					break;
				case sfidDynamicOption:
				
					break;
				case sfidFactKey:
				
					break;
				case sfidEntryID:
				
					break;
				case sfidUserID:
				
					break;
				*/
				case sfidUserName:
					$strDefaultValue = "{objSettings.['user']}";
					break;
				case sfidGPSCountry:
					$strDefaultValue = "{objSettings.['getGeolocationCountry']}";
					break;
				case sfidGPSState:
					$strDefaultValue = "{objSettings.['getGeolocationState']}";
					break;
				case sfidGPSCity:
					$strDefaultValue = "{objSettings.['getGeolocationCity']}";
					break;
				case sfidGPSAddress:
					$strDefaultValue = "{objSettings.['getGeolocationAddress']}";
					break;
				case sfidGPSZipCode:
					$strDefaultValue = "{objSettings.['getGeolocationZipCode']}";
					break;
				case sfidGPSFullAddress:
					$strDefaultValue = "{objSettings.['getGeolocationFullAddress']}";
					break;
				case sfidLocationAcc:
					$strDefaultValue = "{objSettings.['getGeolocationAccuracy']}";
					break;
				default:
					$strDefaultValue = "";
					break;
			}
		} else {
			$aQuestion = @BITAMQuestion::NewInstanceWithID($this->Repository, $aSurveyFieldID);
			//$aSection = BITAMSection::NewInstanceWithID($this->Repository, $aQuestion->SectionID);
			//$strDefaultValue = "{@Q".($aQuestion->QuestionNumber)."}";
			//@JAPR 2014-12-02: Validado que no genere error en caso de no existir ya la pregunta mapeada
			if (!is_null($aQuestion)) {
				$strDefaultValue = $aQuestion->QuestionID;
			}
		}
		return $strDefaultValue;
	}
	
	//Obtiene la definición en JSon de este objeto y sus dependencias (no hay referencias a catálogos u otros objetos globales, sólo son aquellos objetos que dependen
	//directamente de este
	function getJSonDefinition() {
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		global $appVersion;
		//@JAPR 2013-03-11: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
		global $blnWebMode;
		if (!isset($blnWebMode)) {
			$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
		}
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		global $gbDesignMode;
		
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		$arrInvalidSections = array();
		if (getMDVersion() >= esvSectionQuestions) {
			$arrInvalidSections = @BITAMSection::GetMappedSections($this->Repository, $this->SurveyID);
		}
		//@JAPR
		
		$arrDef = array();
		
		$arrDef['id'] = $this->QuestionID;
		$arrDef['sectionID'] = $this->SectionID;
		$arrDef['number'] = $this->QuestionNumber;
		//OMMC 2016-04-12: Agregado el attributeName a la definición para ser usado para las cabeceras de la pregunta mensaje
		$arrDef['attributeName'] = $this->AttributeName;
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
		$strData = $this->TranslateVariableQuestionIDsByNumbers($this->QuestionText);
		//OMMC 2016-05-02: Agregada la corrección de nombres de pregunta con múltiples espacios
		if ($this->LastModAdminVersion < esvQNameCorrection) {
			$strData = mb_ereg_replace("\\".chr(194).chr(160), ' ', $this->QuestionText);
		}
		$arrDef['name'] = $strData;
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
			//@JAPR 2013-03-11: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
			//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
			$strData = $this->TranslateVariableQuestionIDsByNumbers($this->QuestionTextHTML);
			$formattedText = $strData;
			//@JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
			//Se usarán propiedades alternativas tal como están grabadas en la metadata
			if ($gbDesignMode) {
				$arrDef['nameHTMLDes'] = $formattedText;
			}
			//@JAPR
			
			if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
				//Si esta habilitado el envio de imagenes entonces vamos a pasarlas a codigo base64
				//en caso contrario se convierten en etiquetas con la descripcion definida en Image Description
				$formattedText = BITAMSection::convertImagesToBase64($formattedText);
			}
			elseif (!$blnWebMode) {
				$formattedText = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $formattedText);
			}
			
			$arrDef['nameHTML'] = $formattedText;
			//JAPR 2015-06-26: Agregados los campos de nombre en formato HTML para separar la etiqueta formateada de la original con texto plano
			//Temporalmente se sobreescribirá el nombre del objeto con la versión formateada, pero el App debería decidir utilizar cada uno según el contexto
			
			if (trim($strData) != '' && $appVersion < esveFormsDHTMLX) {
				//Para el modo de diseño, se mandará la propiedad especial nameRaw para poder presentar el nombre sin formato alguno y sin afectar al App
				$arrDef['nameRaw'] = $arrDef['name'];
				$arrDef['name'] = $strData;
			}
			
			//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
			//Ahora el tipo de llenado del componente será un campo mas en la metadata
			$arrDef['valuesSourceType'] = $this->ValuesSourceType;
		}
		//@JAPR
		$arrDef['type'] = $this->QTypeID;
		$arrDef['useCatalog'] = $this->UseCatalog;
		$arrDef['catalogID'] = $this->CatalogID;
		$arrDef['catMemberID'] = $this->CatMemberID;
		//@JAPR 2015-08-21: Validado para que en caso de que no se tenga un dataSource configurado o no sea de dicho tipo, no se mande la info del catálogo
		if (getMDVersion() >= esveFormsv6) {
			if ($this->DataSourceID <= 0 || $this->ValuesSourceType != tofCatalog) {
				$arrDef['catalogID'] = 0;
				$arrDef['catMemberID'] = 0;
			}
		}
		//@JAPR
		
		if (getMDVersion() >= esvHideAnswers) {
			$arrDef['catMemberLatitudeID'] = $this->CatMemberLatitudeID;
			$arrDef['catMemberLongitudeID'] = $this->CatMemberLongitudeID;
			$arrDef['useMap'] = $this->UseMap;
			$arrDef['mapRadio'] = $this->MapRadio;
		}
		if (getMDVersion() >= esvIsInvisible) {
			$arrDef['isInvisible'] = $this->IsInvisible;
		}
		$arrDef['minValue'] = $this->MinValue;
		$arrDef['maxValue'] = $this->MaxValue;
		$arrDef['displayMode'] = $this->QDisplayMode;
		//@JAPR 2015-08-02: Validación para tipos de preguntas que internamente se graban con cierto tipo de visibilidad, pero que en el App deben tener una adecuada o no se verán
		if (getMDVersion() >= esveFormsv6) {
			switch ($arrDef['displayMode']) {
				case dspMap:
					//Las preguntas tipo mapa se deben ver como vertical o no se presentarán correctamente
					$arrDef['displayMode'] = dspVertical;
					$arrDef['useMap'] = 1;
					break;
				
				case dspGetData:
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				case dspAR:
					//Las preguntas tipo GetData se deben ver como menú o de lo contrario no auto-seleccionarán el primer valor, además generan un filtro dinámico automático y siempre usan
					//todos los atributos del catálogo
					if ($this->ValuesSourceType == tofCatalog && $this->CatalogID > 0) {
						$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
						if (!is_null($objCatalogMembersColl)) {
							$intCatMemberID = 0;
							foreach ($objCatalogMembersColl as $objCatalogMember) {
								$intCatMemberID = $objCatalogMember->MemberID;
							}
							
							if ($intCatMemberID > 0) {
								$arrDef['catMemberID'] = $intCatMemberID;
							}
						}
						
						//Las preguntas AR no deben ser marcadas como invisibles, simplemente el App deberá construir el HTML de manera diferente
						if ( $arrDef['displayMode'] != dspAR ) {
							$arrDef['isInvisible'] = 1;
						}
					}
					break;
			}
		}
		//@JAPR
		$arrDef['mcInputType'] = $this->MCInputType;
		$arrDef['length'] = $this->QLength;
		//@JAPR 2015-09-09: Corregido un bug, esta asignación para deshabilitar la toma de fotos no debe ser permanente en la metadata, sino que se debe
		//hacer para secciones Inline y siempre y cuando estén en modo tabla y en forma local al App, no tiene caso que se sobreescriba en Metadata (#KVBUK4)
		//OMMC 2016-01-06: Cambiado para que la OCR se dibuje en secciones tabla.
		$arrDef['hasReqPhoto'] = ($this->SectionType == sectInline && $this->SectionDisplayMode == sdspInline && $this->QTypeID != qtpOCR && $arrDef['displayMode'] != dspAR)?0:$this->HasReqPhoto; //#NAEUJY
		$arrDef['hasReqDocument'] = $this->HasReqDocument; 
		$arrDef['hasReqComment'] = ($this->SectionType == sectInline && $this->SectionDisplayMode == sdspInline)?0:$this->HasReqComment; //#NAEUJY
		//@JAPR
		$arrDef['isReqQuestion'] = $this->IsReqQuestion;
		$arrDef['allowAdditionalAnswers'] = ($this->SectionType == sectInline)?0:$this->AllowAdditionalAnswers; //#NAEUJY 
		$arrDef['componentStyle'] = $this->QComponentStyleID;
		$arrDef['categoryID'] = $this->CategoryID;
		$arrDef['responsibleID'] = $this->ResponsibleID;
		$arrDef['daysDueDate'] = $this->DaysDueDate;
		//MCBH 2012-11-12 agregado si es tipo fecha lo graba en format la mascara de formato
        if($this->QTypeID == qtpOpenDate){
			//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
			$arrDef['format'] = str_replace("m","M",$this->DateFormatMask);
        }
        else {
			//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
			$arrDef['format'] = $this->FormatMask;
        }
        //@OMMC 2015/09/02: Agregado para evitar que se agregue &nbsp en campos numéricos al seleccionar formato vacío
        if($arrDef['format'] == "&nbsp;"){
			$arrDef['format'] = "";
		}
        $arrDef['maxChecked'] = $this->MaxChecked;
        //@JAPR 2014-07-31: Corregido un bug, QuestionMessage no tiene por que enviarse con este nombre, ya que se maneja con formattedText
        //y requiere un procesamiento especial, así que simplemente estaba duplicando el HTML de esta pregunta al bajar las definiciones,
        //lo mismo con questionImageText
        //$arrDef['questionImageText'] = $this->QuestionImageText;
        //$arrDef['questionMessage'] = $this->QuestionMessage;
		$arrDef['decimalPlaces'] = $this->DecimalPlaces;
        $arrDef['fatherQuestionID'] = $this->FatherQuestionID;
		$arrDef['childQuestionID'] = $this->ChildQuestionID;
		//@JAPR 2012-10-19: Se modificó el comportamiento de estas propiedades para que ya no se use un CheckBox para excluir respuestas
		//sino que simplemente se seleccione la pregunta de la cual las respuestas seleccionadas serán excluidas
		$arrDef['excludeFromQuestionID'] = $this->SourceSimpleQuestionID;
		//$arrDef['excludeSelected'] = $this->ExcludeSelected;
		//@JAPR
		$arrDef['sourceQuestionID'] = $this->SourceQuestionID;
		$arrDef['summaryTotal'] = $this->SummaryTotal;
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		//No debe considerar a las secciones que ya se han mapeado pues ya no funcionarán como secciones en páginas independientes
		if (!in_array($this->GoToQuestion, $arrInvalidSections)) {
			$arrDef['nextSectionID'] = $this->GoToQuestion;
		}
		//@JAPR 2013-05-15: Corregido un bug, este array no tenía por qué enviarse a nivel de pregunta
		//$arrDef['phoneNum'] = $this->phoneNum; //Conchita 2013-04-30 agregado phonenum
		//Conchita 2014-09-09 se agregaron el canvaswidth y el canvasheight para las preguntas tipo sketch
		if (getMDVersion() >= esvSketch){
			$arrDef['canvasWidth'] = $this->canvasWidth;
			$arrDef['canvasHeight'] = $this->canvasHeight;
		}
		//@JAPR 2013-03-12: Corregido un bug, no estaba incrustando las imagenes de preguntas tipo mensaje (#28818)
		$formattedText = $this->QuestionMessage;
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			//@JAPR 2014-07-31: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign && $appVersion >= esvDeviceDataLog) {
				$intDeviceID = identifyDeviceType();
				$formattedText = $this->getResponsiveDesignProperty("QuestionMessage", $intDeviceID, true);
			}
		}
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
		$formattedText = $this->TranslateVariableQuestionIDsByNumbers($formattedText);
		//@JAPR 2015-09-05: Corregido un bug, en modo diseño debe reemplazar la variable de rutas relativas
		$formattedTextDes = $formattedText;
		
		//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
		if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
	        //Si esta habilitado el envio de imagenes entonces vamos a pasarlas a codigo base64
	        //en caso contrario se convierten en etiquetas con la descripcion definida en Image Description
	        $formattedText = BITAMSection::convertImagesToBase64($formattedText);
		}
		elseif (!$blnWebMode) {
			$formattedText = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $formattedText);
		}
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['formattedText'] = $formattedText;
		//@JAPR 2015-08-15: Agregado el ipo de pregunta HTML
		if ($gbDesignMode) {
			//@JAPR 2015-09-05: Corregido un bug, en modo diseño debe reemplazar la variable de rutas relativas
			$arrDef['formattedTextDes'] = $formattedTextDes;
			$arrDef['htmlCode'] = $formattedTextDes;
		}
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
		$strData = $this->TranslateVariableQuestionIDsByNumbers($this->DefaultValue);
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['defaultValue'] = $strData;
		//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales
		$arrDef['readOnly'] = $this->ReadOnly;
		//@JAPR
		if ($this->QTypeID == qtpCalc) {
			$arrDef['readOnly'] = 1;
		}
		
		//Conchita 2014-10-13 agregado el questionImageSketch
		if (getMDVersion() >= esvSketchImage) {
			$intDeviceID = identifyDeviceType();
			$strDisplayImage = $this->getResponsiveDesignProperty("QuestionImageSketch", $intDeviceID, true);
			if ($strDisplayImage != '') {
				//@JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
				//Se usarán propiedades alternativas tal como están grabadas en la metadata
				if ($gbDesignMode) {
					$arrDef['imageSketchDes'] = $strDisplayImage;
				}
				//@JAPR
				
				$strDisplayImage = trim(str_replace(array('<p>', '</p>'), '', $strDisplayImage));
				//@JAPR 2013-03-11: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
				//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
				if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
					$strDisplayImage = (string) @BITAMSection::convertImagesToBase64($strDisplayImage);	
				}
				elseif (!$blnWebMode) {
					//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
					$strDisplayImage = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $strDisplayImage, true);
				}
				else {
					//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
					if ($this->CreationAdminVersion >= esveFormsv6) {
						$strDisplayImage = '<img src="'.$strDisplayImage.'" />';
					}
					//@JAPR
				}
				//@JAPR
			}
			//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
			$arrDef['imageSketch'] = $strDisplayImage;
		}
		//Conchita 
		$strDisplayImage = trim($this->QuestionImageText);
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			//@JAPR 2014-08-01: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign && $appVersion >= esvDeviceDataLog) {
				$intDeviceID = identifyDeviceType();
				$strDisplayImage = $this->getResponsiveDesignProperty("QuestionImageText", $intDeviceID, true);
			}
		}
		//@JAPR
		
		if ($strDisplayImage != '') {
			//@JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
			//Se usarán propiedades alternativas tal como están grabadas en la metadata
			if ($gbDesignMode) {
				$arrDef['displayImageDes'] = $strDisplayImage;
			}
			//@JAPR
			
			$strDisplayImage = trim(str_replace(array('<p>', '</p>'), '', $strDisplayImage));
			//@JAPR 2013-03-11: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
			//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
			if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
				$strDisplayImage = (string) @BITAMSection::convertImagesToBase64($strDisplayImage);	
			}
			elseif (!$blnWebMode) {
				//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente

				$strDisplayImage = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $strDisplayImage, true);
				//@EVEG Para aplicar el resize de las imagenes asociadas a una pregunta
				//@JAPR 2015-09-05: Corregido un bug, la condición debe ser con And
				if((isset($this->canvasWidth) && isset($this->canvasHeight)) && ($this->canvasWidth!=0 && $this->canvasHeight!=0)){
					if(!strpos($this->canvasWidth,"%") && !strpos($this->canvasWidth,"px")){
						$this->canvasWidth=$this->canvasWidth."px";
					}
					if(!strpos($this->canvasHeight,"%") && !strpos($this->canvasHeight,"px")){
						$this->canvasHeight=$this->canvasHeight."px";
					}
					$strDisplayImage=str_replace('<img','<img style="height:'.$this->canvasHeight.'; width:'.$this->canvasWidth.';"',$strDisplayImage);
				}
			}
			else {
				//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
				if ($this->CreationAdminVersion >= esveFormsv6) {
					if((!isset($this->canvasWidth) && !isset($this->canvasHeight)) || ($this->canvasWidth==0 && $this->canvasHeight==0)){
						$strDisplayImage = '<img class="imgstyl" src="'.$strDisplayImage.'" />';
					}else{
						if(!strpos($this->canvasWidth,"%") && !strpos($this->canvasWidth,"px")){
							$this->canvasWidth=$this->canvasWidth."px";
						}
						if(!strpos($this->canvasHeight,"%") && !strpos($this->canvasHeight,"px")){
							$this->canvasHeight=$this->canvasHeight."px";
						}
						$strDisplayImage = '<img style="height:'.$this->canvasHeight.'; width:'.$this->canvasWidth.';" src="'.$strDisplayImage.'" />';
					}
				}
				//@JAPR
			}
			//@JAPR
		}
		//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
		$arrDef['displayImage'] = $strDisplayImage;
		//@JAPR
		$arrDef['options'] = array();
		$arrDef['optionsOrder'] = array();
		
		$arrDef['skipSection'] = $this->GoToQuestion;
		$arrDef['childrenEvaluate'] = $this->QuestionsToEval;
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		$arrDef['fillTargetChildren'] = $this->getChildrenToFillRef();
		//@JAPR 2014-10-21: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		if (getMDVersion() >= esvInlineTypeOfFilling && $appVersion >= esvInlineTypeOfFilling) {
			//Sólo se asigna la colección de secciones a llenar, si esta pregunta es tipo múltiple choice y hay alguna sección configurada para llenarse
			//a partir de ella, o bien si esta pregunta es simple choice selector de otra sección Inline y hay alguna sección configurada para llenarse
			//a partir de su sección Inline. Respecto al segundo caso, también aplica si esta pregunta es tipo Sección - Inline y la sección a la que
			//mapea se configuró para ser quien llena a otras secciones Inline
			if (($this->QTypeID == qtpMulti && $this->SectionType == sectNormal) || 
					($this->QTypeID == qtpSingle && $this->IsSelector) || 
					($this->QTypeID == qtpSection && $this->SectionType == sectNormal)) {
				//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
				$arrDef['fillTargetSectionChildren'] = $this->getSectionsToFillRef();
				//@JAPR
			}
		}
		//@JAPR
		$arrDef['sourceQuestionID'] = $this->SourceQuestionID;
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		$arrDef['excludeTargetChildren'] = $this->getChildrenToExcludeRef();
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
		$strData = $this->TranslateVariableQuestionIDsByNumbers($this->Formula);
		$arrDef['formula'] = $strData;
		//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
		$strData = $this->TranslateVariableQuestionIDsByNumbers($this->ShowCondition);
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['showCondition'] = $strData;
		//@JAPR
		$arrDef['formulaChildren'] = $this->FormulaChildren;
		$arrDef['showConditionChildren'] = $this->ShowConditionChildren;
		//@JAPR 2013-02-21: Agregada la opción para ocultar los textos de opciones de respuesta si tienen imagen asociada
		$arrDef['showImageAndText'] = $this->ShowImageAndText;
		//@JAPR 2013-12-03: Agregada la opción para compartir las opciones de respuesta con otras preguntas
		if (getMDVersion() >= esvSimpleChoiceSharing) {
			$arrDef['sharedQuestionID'] = $this->SharedQuestionID;
		}
		//@JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		if (getMDVersion() >= esvGPSQuestion) {
			$arrDef['registerGPSAt'] = $this->RegisterGPSAt;
		}
		//@JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$arrDef['inputSize'] = $this->InputWidth;
			$arrDef['pictureLayOut'] = $this->PictLayOut;
		}
		//@JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice
		if (getMDVersion() >= esvMCHSwitchBehav) {
			$arrDef['enableCheckBox'] = $this->EnableCheckBox;
			$arrDef['switchBehaviour'] = $this->SwitchBehaviour;
		}
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		if (getMDVersion() >= esvSectionQuestions) {
			$arrDef['sourceSectionID'] = $this->SourceSectionID;
		}
		//@JAPR
		if ($appVersion == esvCustomOtherComentLabelv403 || $appVersion >= esvCustomOtherComentLabelv404) {
			$strOtherLabel = '';
			$strCommentLabel = '';
			
			$aSurveyInstance = null;
			if (trim($this->OtherLabel) == '' || trim($strOtherLabel) == '') {
				$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			}
			
			if (trim($this->OtherLabel) != '') {
				$strOtherLabel = $this->OtherLabel;
			}
			else {
				if (!is_null($aSurveyInstance)) {
					$strOtherLabel = $aSurveyInstance->OtherLabel;
				}
			}
			if (trim($strOtherLabel) != '') {
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
				$arrDef['otherLabel'] = $strOtherLabel;
			}
			
			if (trim($this->CommentLabel) != '') {
				$strCommentLabel = $this->CommentLabel;
			}
			else {
				if (!is_null($aSurveyInstance)) {
					$strCommentLabel = $aSurveyInstance->CommentLabel;
				}
			}
			if (trim($strCommentLabel) != '') {
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
				$arrDef['commentLabel'] = $strCommentLabel;
			}
		}
		//@JAPR
		if (getMDVersion() >= esvShowInSummary) {
			$arrDef['showInSummary'] = $this->ShowInSummary;
		}
		//@JAPR 2014-09-29: Agregado el ancho de columnas en secciones Inline o el Summary de maestro-detalle
		if (getMDVersion() >= esvInlineMerge) {
			if ($this->SectionType == sectInline || $this->SectionType == sectMasterDet) {
				$arrDef['columnWidth'] = $this->ColumnWidth;
				//OMMC 2017-01-13: Modificado el columnwidth para las preguntas tipo imagen
				if (getMDVersion() >= esvImageDisplay) {
					if ($this->QTypeID == qtpPhoto && $this->imageWidth) {
						$arrDef['columnWidth'] = $this->imageWidth;		
					}
				}
			}
		}
		//@JAPR 2014-10-22: Agregada la opción para redibujar las preguntas tipo mensaje automáticamente
		if (getMDVersion() >= esvAutoRedrawFmtd && $this->QTypeID == qtpMessage) {
			$arrDef['autoRedraw'] = $this->AutoRedraw;
		}
		//@JAPR 2015-07-15: Rediseñadas las preguntas y secciones de catálogo para permitir seleccionar los atributos a usar y el orden deseado (uso de DataSources)
		if (getMDVersion() >= esveFormsv6) {
			if (($this->QTypeID == qtpSingle || $this->QTypeID == qtpMulti) && $this->ValuesSourceType == tofCatalog) {
				$arrDef['catMembersList'] = $this->CatMembersList;
				//@JAPR 2015-07-16: Agregada la integración con DataSources
				$arrDef['dataSourceID'] = $this->DataSourceID;
				$arrDef['dataSourceMemberID'] = $this->DataSourceMemberID;
				$arrDef['dataMemberLatitudeID'] = $this->DataMemberLatitudeID;
				$arrDef['dataMemberLongitudeID'] = $this->DataMemberLongitudeID;
				//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
				$strData = $this->TranslateVariableQuestionIDsByNumbers($this->DataSourceFilter);
				//@JAPR 2015-09-30: Habilitado el filtro dinámico de preguntas de catálogo para todos los tipos
				//Se removió la condición que permitía sólo a las preguntas tipo GetData aplicar esta funcionalidad
				//@JAPR 2016-05-30: Corregido un bug, faltaba este reemplazo para las preguntas múltiple choice, simplemente se removerá la condición por tipo de pregunta
				if (/*$this->QTypeID == qtpSingle && *//*&& $this->QDisplayMode == dspGetData*/ $this->DataSourceID > 0) {
					$strData = BITAMDataSource::ReplaceMemberIDsByNames($this->Repository, $this->DataSourceID, $strData);
				}
				$arrDef['dataSourceFilter'] = $strData;
				//@JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
				if (getMDVersion() >= esvSimpleChoiceCatOpts){
					$arrDef['catMemberImageID'] = $this->CatMemberImageID;
					$arrDef['dataMemberImageID'] = $this->DataMemberImageID;
				}
				//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				if (getMDVersion() >= esvSChoiceMetro) {
					$arrDef['catMemberOrderID'] = $this->CatMemberOrderID;
					$arrDef['dataMemberOrderID'] = $this->DataMemberOrderID;
					//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
					$arrDef['dataMemberOrderDir'] = $this->DataMemberOrderDir;
					if ($gbDesignMode) {
						$arrDef['customLayout'] = $this->TranslateVariableQuestionIDsByNumbers($this->CustomLayout);
					}
					//Esta configuración requiere un reemplazo especial de variables, ya que en el editor de fórmulas se grabarán referencias como si se trataran de variables para un
					//defaultValue (tal como el App las espera), pero el componente donde se utilizarán es un DHTMLXList, así que el template requiere las referencias como #propiedad#,
					//por lo que se invocará a un proceso que reemplace las referencias de variables por los parámetros del template de DHTMLX
					$strCustomLayout = $this->CustomLayout;
					//@JAPR 2016-08-11: Corregido un bug, el componente donde se usará esta configuración es un DHTMLXList, dentro de él este template se usará para generar código
					//dinámicamente de esta forma: a = 'return "' + a + '";';
					//Eso significa que si a (que es esta configuración) trae comillas dobles, romperá el string de código armado provocando un error que impedirá que se pinte la forma,
					//así que se reemplazarán las comillas dobles por su versión escapada (\") para permitir que se genere correctamente el componente (#XRKG3R)
					$strCustomLayout = $this->TranslateVariableQuestionIDsByNumbers(BITAMCatalog::ReplaceCatalogMemberVarsByTemplateProp($this->Repository, $this->CatalogID, $this->QuestionID, $strCustomLayout));
					$arrDef['optionsTemplate'] = str_ireplace('"', '\"', $strCustomLayout);
					//@JAPR
				}
				//@JAPR
			}
			//@JAPR 2016-04-21: Corregido un bug, al no regresar algunas propiedades, el configurador las mostraba como undefined, así que se regresarán si es modo de diseño (#WC6IA7)
			else {
				//Estas propiedades no aplican para ningún otro caso por default, pero al cambiar alguna otra configuración durante el diseño, se puede pedir su valor así que se
				//asigna un default apropiado para que no muestre undefined o haga cosas raras el Administrador (NO se mandan a la definición del App para no mandar basura)
				if ($gbDesignMode) {
					$arrDef['dataSourceFilter'] = '';
				}
			}
		}
		//@JAPR 2015-06-11: Rediseñado el Admin con DHTMLX
		$strAllOptionsImage = '';
		//@JAPR 2015-07-19: Integradas las nuevas preguntas tipo llamada
		if ($this->QTypeID == qtpCallList && $this->CreationAdminVersion >= esveFormsv6) {
			//Para las preguntas tipo llamada, a partir de la V6 la única imagen asociada será la de la pregunta, ya que sólo tienen un número telefónico
			$strAllOptionsImage = (string) @$arrDef['displayImage'];
			//OMMC 2016-05-10: Comentada la limpieza de las imagenes personalizadas en modo de diseño. 
			/*if (!$gbDesignMode) {
				//En modo diseño no se debe limpiar, porque en base a ella se configura, sólo se limpia para el App
				$arrDef['displayImage'] = '';
			}*/
		}
		//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
		if (getMDVersion() >= esvUpdateData ) {
			if ( $this->QTypeID == qtpUpdateDest) {
				$arrDef['dataDestinationID'] = $this->DataDestinationID;
				//@JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
				if (getMDVersion() >= esvUpdateData ) {
					$arrDef['uploadMediaFiles'] = $this->UploadMediaFiles;
				}
				//@JAPR
			}
		}
		//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
		if (getMDVersion() >= esvAutoSelectSChOpt ) {
			$arrDef['autoSelectEmptyOpt'] = $this->AutoSelectEmptyOpt;
		}
		//@JAPR
		
		if ($gbDesignMode) {
			$arrDef['objectType'] = otyQuestion;
			//En este caso el shortName se deja literal como fue definido por el usuario, ya que se mandará a la ventana de diseño
			$arrDef['shortName'] = $this->AttributeName;
			$arrDef['qLength'] = $this->QLength;
			$arrDef['defaultType'] = 0;
			$arrDef['telephoneNum'] = $this->StrSelectOptions;
			$arrDef['dateFormat'] = str_replace("m","M",$this->DateFormatMask);
			$arrDef['strSelectOptions'] = $this->StrSelectOptions;
			//@JAPR 2015-08-15: Agregado el ipo de pregunta HTML
			$arrDef['editorType'] = $this->EditorType;
			switch ($this->QTypeID) {
				case qtpSingle:
					$arrDef['typeOfFilling'] = $this->typeOfFillingSC();
					break;
				case qtpMulti:
					$arrDef['typeOfFilling'] = $this->typeOfFillingMC();
					break;
				default:
					$arrDef['typeOfFilling'] = 0;
					break;
			}
			
			//@JAPR 2015-06-11: Agregados los tipos de pregunta extendidos como clasificaciones de la ventana de diseño
			$intQTypeExt = $this->QTypeID;
			switch ($this->QTypeID) {
				case qtpSingle:
					if ($this->UseMap) {
						$intQTypeExt = qtpSingleMap;
					}
					else {
						switch ($this->QDisplayMode) {
							case dspVertical:
								$intQTypeExt = qtpSingleVert;
								break;
							case dspHorizontal:
								$intQTypeExt = qtpSingleHoriz;
								break;
							case dspAutocomplete:
								$intQTypeExt = qtpSingleAuto;
								break;
							//@JAPR 2016-05-10: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
							case dspMetro:
								$intQTypeExt = qtpSimpleMetro;
								break;
							//OMMC 2019-03-25: Pregunta AR #4HNJD9	
							case dspAR:
								$intQTypeExt = qtpSimpleAR;
								break;
							default:
								//El tipo por default es Menu, que es igual que el QTypeID
								break;
						}
					}
					break;
				case qtpMulti:
					switch ($this->QDisplayMode) {
						case dspVertical:
							$intQTypeExt = qtpMultiVert;
							break;
						case dspHorizontal:
							$intQTypeExt = qtpMultiHoriz;
							break;
						case dspAutocomplete:
							$intQTypeExt = qtpMultiAuto;
							break;
						case dspLabelnum:
							$intQTypeExt = qtpMultiRank;
							break;
						default:
							//El tipo por default es Menu, que es igual que el QTypeID
							break;
					}
					break;
				//@JAPR 2015-08-15: Agregado el ipo de pregunta HTML
				case qtpMessage:
					if ($this->EditorType == edtpHTML) {
						$intQTypeExt = qtpHTML;
					}
					break;
				//@JAPR
				default:
					//Todos los tipos no subclasificados (excepto Open, que desde su creación se separó) comparten el mismo ID que QTypeID por consistencia
					break;
			}
			$arrDef['typeExt'] = $intQTypeExt;
			
			//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
			if (getMDVersion() >= esvUpdateData && $this->QTypeID == qtpUpdateDest) {
				$arrDef['nextSectionIfTrue'] = $this->NextSectionIDIfTrue;
				$arrDef['nextSectionIfFalse'] = $this->NextSectionIDIfFalse;
			}
			//@JAPR
		}
		
		//JAPR 2014-10-01: Agregados los filtros de preguntas para definir colores de los marcadores en simple choice con mapa
		if (getMDVersion() >= esvQuestionFilters) {
			if ($this->QTypeID == qtpSingle && $this->CatalogID > 0 && $this->UseMap) {
		        $objQuestionFilterCollection = BITAMQuestionFilterCollection::NewInstance($this->Repository, $this->QuestionID);
		        if (!is_null($objQuestionFilterCollection) && count($objQuestionFilterCollection->Collection) > 0) {
			        $arrDef['filters'] = array();
			        foreach ($objQuestionFilterCollection->Collection as $objQuestionFilter) {
			        	$arrDef['filters'][] = $objQuestionFilter->getJSonDefinition();
			        }
		        }
			}
		}
        //@JAPR
		
		$arrOptionsPos = array();	//Array interno para agregar ShowQuestions
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			//Agrega todas las opciones de respuesta indexando por la propia opción de respuesta
			//@JAPR 2014-08-01: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign && count($this->SelectOptions)) {
				//Carga las propiedades dinámicas equivalentes al responsive Design pero sólo si se recibió el tipo de dispositivo que está
				//haciendo la petición
				$intDeviceID = identifyDeviceType();
				if ($intDeviceID > dvcWeb) {
					BITAMQuestionOption::GetResponsiveDesignProps($this->Repository, $this, null, null, $this->QuestionID);
				}
			}
		}
		//@JAPR
		
		//@JAPR 2014-10-17: Agregado el método para obtener la definición de este objeto
		//@JAPRWarning: Se implementó este método pero falta optimizar el caché de preguntas, así que no se utilizará hasta que en globalFormsInstance
		//se programe el caché, sin embargo se debería dar mantenimiento a esta función tal como se hace en el llenado de los options de Question
		/*
		$objOptionsColl = BITAMQuestionOptionCollection::NewInstance($this->Repository, $this->QuestionID);
		if (!is_null($objOptionsColl)) {
			foreach ($objOptionsColl->Collection as $objQuestionOption) {
				$optionValue = $objQuestionOption->QuestionOptionName;
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		    	$strOptionValueUTF = $optionValue;
		    	$arrDef['options'][$strOptionValueUTF] = $objQuestionOption->getJSonDefinition();
		    	$arrDef['optionsOrder'][] = $strOptionValueUTF;
			}
		}
		*/
		
		//@JAPR 2015-09-22: Corregido un bug, si la pregunta es de catálogo, no debe regresar la lista de opciones porque se generarán con el catálogo y porque si se hace entonces
		//se procesarán los saltos y demás que estas opciones tengan configurados (esta sería una manera de permitir dichas configuraciones para preguntas de catálogo, pero por
		//ahora es un bug y no un feature) (#B84LXO)
		if ($this->ValuesSourceType != tofCatalog) {
			foreach ($this->SelectOptions as $optionKey => $optionValue) {
				//@JAPR 2013-03-04: Corregido un bug, como este array se regresará al App, se debe codificar incluso en los índices
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
				$strOptionValueUTF = $optionValue;
				//@JAPR 2015-06-28: Rediseñado el Admin con DHTMLX
				/*
				if ($gbDesignMode) {
					$questionOptionObj = BITAMQuestionOption::NewInstanceWithID($this->Repository, $this->QConsecutiveIDs[$optionKey], true);
					//En modo diseño el id si es el consecutivo interno para permitir la edición de la etiqueta
					$strOptionValueUTF = $questionOptionObj->QuestionOptionID;
				}
				*/
				$arrDef['options'][$strOptionValueUTF] = array();
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
				$arrDef['options'][$strOptionValueUTF]['name'] = $optionValue;
				$arrDef['options'][$strOptionValueUTF]['nextSection'] = $this->NextQuestion[$optionKey];
				//@JAPR 2012-10-11: Agregada la lectura de la imagen asociada para regresar en el servicio
				$strDisplayImage = trim($this->DisplayImage[$optionKey]);
				if ($strDisplayImage != '') {
					//@JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
					//Se usarán propiedades alternativas tal como están grabadas en la metadata
					if ($gbDesignMode) {
						$arrDef['options'][$strOptionValueUTF]['displayImageDes'] = $strDisplayImage;
					}
					//@JAPR
					
					$strDisplayImage = trim(str_replace(array('<p>', '</p>'), '', $strDisplayImage));
					//@JAPR 2013-03-11: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
					//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
					//@JAPR 2019-02-14: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					//Con la pregunta Sketch+, las propiedades canvasWidth y canvasHeight literal representan el tamaño del canvas, así que ya no se pueden seguir usando para definir
					//el tamaño de las imágenes de los elementos a seleccionar, así que se usarán las propiedades imageWidth e imageHeight que originalmente eran para los tamaños de
					//las fotos en preguntas foto e imagen exclusivamente
					$strWidth = '';
					$strHeight = '';
					if ( $this->QTypeID == qtpSketchPlus ) {
						$strWidth = (string) @$this->imageWidth;
						$strHeight = (string) @$this->imageHeight;
					}
					else {
						$strWidth = (string) @$this->canvasWidth;
						$strHeight = (string) @$this->canvasHeight;
					}
					if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
						$strDisplayImage = (string) @BITAMSection::convertImagesToBase64($strDisplayImage);
					}
					elseif (!$blnWebMode) {
						//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
						//@JAPR 2015-08-06: Agregado el parámetro $bUseImgTag para indicar si se desea o no como tag <Img>
						$strDisplayImage = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $strDisplayImage, true);
						//@EVEG Resize de imagenes de las choice
						//@JAPR 2015-09-05: Corregido un bug, la condición debe ser con And
						if((isset($strWidth) && isset($strHeight)) && ($strWidth!=0 && $strHeight!=0)){
							if(!strpos($strWidth,"%") && !strpos($strWidth,"px")){
								$strWidth=$strWidth."px";
							}
							if(!strpos($strHeight,"%") && !strpos($strHeight,"px")){
								$strHeight=$strHeight."px";
							}
							$strDisplayImage=str_replace('<img','<img style="height:'.$strHeight.'; width:'.$strWidth.';"',$strDisplayImage);
						}
					}
					else {
						//@JAPR 2015-07-19: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
						if ($this->CreationAdminVersion >= esveFormsv6) {
							if((!isset($strWidth) && !isset($strHeight)) || ($strWidth==0 && $strHeight==0)){
								$strDisplayImage = '<img src="'.$strDisplayImage.'" />';
							}else{
								if(!strpos($strWidth,"%") && !strpos($strWidth,"px")){
									$strWidth=$strWidth."px";
								}
								if(!strpos($strHeight,"%") && !strpos($strHeight,"px")){
									$strHeight=$strHeight."px";
								}
								$strDisplayImage = '<img style="height:'.$strHeight.'; width:'.$strWidth.';" src="'.$strDisplayImage.'" />';
							}
						}
						//@JAPR
					}
					//@JAPR
				}
				//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
				$arrDef['options'][$strOptionValueUTF]['displayImage'] = $strDisplayImage;
				
				//@JAPR 2015-07-19: Integradas las nuevas preguntas tipo llamada
				if ($this->QTypeID == qtpCallList && $this->CreationAdminVersion >= esveFormsv6) {
					//Para las preguntas tipo llamada, a partir de la V6 la única imagen asociada será la de la pregunta, ya que sólo tienen un número telefónico
					$arrDef['options'][$strOptionValueUTF]['displayImage'] = $strAllOptionsImage;
				}
				
				$strHTMLText = trim($this->HTMLText[$optionKey]);
				if ($strHTMLText != '') {
					$strHTMLText = trim(str_replace(array('<p>', '</p>'), '', $strHTMLText));
					//@JAPR 2013-03-11: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
					//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
					if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
						$strHTMLText = (string) @BITAMSection::convertImagesToBase64($strHTMLText);
					}
					//@JAPR 2013-08-06: Corregido un bug, las rutas de imagenes deben convertirse a utf-8
					elseif (!$blnWebMode) {
						$strHTMLText = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $strHTMLText);
					}
					//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
					//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
					$strHTMLText = $this->TranslateVariableQuestionIDsByNumbers($strHTMLText);
				}
				//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
				$arrDef['options'][$strOptionValueUTF]['displayText'] = $strHTMLText;
				
				//conchita 2013-04-30 agregado el num de tel
				//@JAPR 2013-05-15: Corregido un bug, esta propiedad no tiene por qué enviarse en cuentas que no tenían dicha funcionalidad habilitada
				if (getMDVersion() >= esvCallList) {
					$strPhoneNum = trim($this->PhoneNum[$optionKey]); //Conchita 2013-04-30 num de tel
					$arrDef['options'][$strOptionValueUTF]['phoneNum'] = $strPhoneNum;
				}
				
				//conchita
				//@JAPR 2013-07-23: Agregada la opción para ocultar las respuestas
				if (getMDVersion() >= esvHideAnswers) {
					$intVisible = (int) @$this->Visible[$optionKey];
					//Sólo se agregará la propiedad para ocultar, porque por default se asume que todas las respuestas son visibles
					if (!$intVisible) {
						$arrDef['options'][$strOptionValueUTF]['hidden'] = 1;
					}
				}
				
				if (getMDVersion() >= esvAgendaRedesign) {
					$intCancelAgenda = (int) @$this->CancelAgenda[$optionKey];
					//Sólo se agregará la propiedad para ocultar, porque por default se asume que todas las respuestas son visibles
					if ($intCancelAgenda) {
						$arrDef['options'][$strOptionValueUTF]['cancelAgenda'] = 1;
					}
				}
				
				if (getMDVersion() >= esvDefaultOption) {
					$optiondefaultValue = (string) @$this->OptionDefaultValue[$optionKey];
					//@JAPR 2014-09-17: Agregados los datos extendidos de modificación
					//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
					$optiondefaultValue = $this->TranslateVariableQuestionIDsByNumbers($optiondefaultValue);
					//@JAPR
					$arrDef['options'][$strOptionValueUTF]['defaultValue'] = $optiondefaultValue;
				}
				
				if (getMDVersion() >= esvSectionRecap) {
					$questionOptionObj = BITAMQuestionOption::NewInstanceWithID($this->Repository, $this->QConsecutiveIDs[$optionKey], true);
					//@JAPR 2015-07-30: Validado que no se intente usar una instancia null, esto sucedía durante el borrado de una pregunta simple o múltiple choice dado a que se pide
					//cargar las opciones que ya no existen, sin embargo estuvo muy mal implementado pues no debió haberse cargado de esa manera sino utilizar arrays como en el resto
					//de las configuraciones de opciones de respuesta, además esta funcionalidad probablemente ya no aplica (#YWZRYY)
					///@JAPRWarning: Analizar si aún se utiliza esta funcionalidad/propiedades
					if (!is_null($questionOptionObj)) {
						//@JAPR 2015-04-15: Rediseñado el Admin con DHTMLX
						if ($gbDesignMode) {
							$arrDef['options'][$strOptionValueUTF]['consecutiveID'] = $questionOptionObj->QuestionOptionID;
							$arrDef['options'][$strOptionValueUTF]['id'] = $questionOptionObj->QuestionOptionID;
							$arrDef['options'][$strOptionValueUTF]['questionID'] = $questionOptionObj->QuestionID;
						}
						//@JAPR
						$optionActionFormID = $questionOptionObj->eBavelActionFormID;
						//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
						//Estas propiedades están descontinuadas a partir de eForms v6
						if (getMDVersion() < esveFormsv6 ) {
							if($optionActionFormID > 0) {
								if($questionOptionObj->QuestionOptionID > 0) {
									$arrDef['options'][$strOptionValueUTF]['optionActionFormID'] = $optionActionFormID;
									$mapFields = BITAMQuestionOption::GetActionMapFieldsIDs($this->Repository, $this->QuestionID, $questionOptionObj->QuestionOptionID);
									foreach($mapFields as $aMapFieldID) { 
										include_once("questionOptionActionMap.inc.php");
										$mapOptionObj = BITAMQuestionOptionActionMap::NewInstanceWithID($this->Repository, $aMapFieldID);
										if (!isset($arrDef['options'][$strOptionValueUTF]['optionFieldMap'])) {
											$arrDef['options'][$strOptionValueUTF]['optionFieldMap'] = array();
										}
										$arrDef['options'][$strOptionValueUTF]['optionFieldMap'][$mapOptionObj->eBavelFieldID] = $this->GetDefaultValueForMapped($mapOptionObj->SurveyFieldID);
									}
								}
							}
						}
						//@JAPR
					}
				}
				
				//@JAPR 2014-10-17: Agregado el score a la definición
				$arrDef['options'][$strOptionValueUTF]['score'] = $this->Scores[$optionKey];
				
				//@JAPR
				$arrDef['options'][$strOptionValueUTF]['showQuestionList'] = array();
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				if ($gbDesignMode) {
					$arrDef['options'][$strOptionValueUTF]['showQuestionCons'] = array();
					$arrDef['options'][$strOptionValueUTF]['showQuestionShowIDs'] = array();
					$arrDef['options'][$strOptionValueUTF]['showQuestionText'] = array();
				}
				$arrDef['optionsOrder'][] = $strOptionValueUTF;
				//@JAPR
				$arrOptionsPos[$this->QConsecutiveIDs[$optionKey]] = $strOptionValueUTF;
			}
		}
		//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
		else {
			//Si la pregunta es de catálogo, entonces puede tener reglas asignadas siempre y cuando se trate de una Simple choice
			if ($this->QTypeID == qtpSingle && $this->ValuesSourceType == tofCatalog && $this->CatalogID > 0) {
				$arrDef['skipRules'] = array();
				$arrDef['skipRulesOrder'] = array();
				$objSkipRulesColl = BITAMSkipRuleCollection::NewInstance($this->Repository, $this->QuestionID);
				if (!is_null($objSkipRulesColl)) {
					foreach ($objSkipRulesColl->Collection as $objSkipRule) {
						$arrDef['skipRules'][$objSkipRule->SkipRuleID] = $objSkipRule->getJSonDefinition();
						$arrDef['skipRulesOrder'][] = $objSkipRule->SkipRuleID;
					}
				}
			}
		}
		//@JAPR
		
		if (getMDVersion() >= esvSectionRecap) {
			if($this->eBavelFieldID > 0) {
				$arrDef['ebavelFieldID'] = $this->eBavelFieldID;
			}
		}

		//OMMC 2015-11-30: Agregado para preguntas OCR
		if (getMDVersion() >= esvOCRQuestionFields && $this->QTypeID == qtpOCR){
			$arrDef['savePhoto'] = $this->SavePhoto;
			$arrDef['questionList'] = $this->QuestionList;
		}

		//OMMC 2016-01-18: Agregado para guardar el estado del editor HTML
		if (getMDVersion() >= esvHTMLEditorState){
			$arrDef['HTMLEditorState'] = $this->HTMLEditorState;
		}
		//@JAPR 2016-01-21: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
		if (getMDVersion() >= esvShowTotalsInline && ($this->QTypeID == qtpOpenNumeric || $this->QTypeID == qtpCalc)) {
			$arrDef['showTotals'] = $this->ShowTotals;
			$arrDef['totalsFunction'] = strtolower($this->TotalsFunction);
			if ($gbDesignMode) {
				$arrDef['totalsFunctionDes'] = strtoupper($this->TotalsFunction);
			}
		}

		//OMMC 2015-11-30: Agregado para permitir el acceso a la galería
		if (getMDVersion() >= esvAllowGallery){
			$arrDef['allowGallery'] = $this->AllowGallery;
		}

		//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
		if (getMDVersion() >= esvNoFlow) {
			$arrDef['noFlow'] = $this->NoFlow;
		}

		//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
		if (getMDVersion() >= esvImageDisplay){
			$arrDef['imageWidth'] = $this->imageWidth;
			$arrDef['imageHeight'] = $this->imageHeight;
		}
		//@JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
		if (getMDVersion() >= esvRecalcDep) {
			$arrDef['recalculateDependencies'] = $this->RecalculateDependencies;
		}
		
		//OMMC 2016-09-01: Modificado el comportamiento default de la pregunta llamada
		if (getMDVersion() >= esvPhoneCall) {
			$arrDef['disablePhoneNum'] = $this->DisablePhoneNum;
			$arrDef['disableSync'] = $this->DisableSync;
		}
		
	    //@JAPR 2013-12-16: Corregido un bug, al no utilizar un ORDER BY, era posible que MySQL regresara las ShowQuestions en un orden tal que
	    //si hubieran existido dependencias entre ellas, probablemente se habrían intentado calcular y mostrar primero las preguntas dependientes
	    //por lo que no se hubieran podido resolver correctamente al estar ocultas aún las preguntas de las que dependía
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		$objShowQuestionsColl = BITAMGlobalFormsInstance::GetShowQuestionCollectionByQuestionWithID($this->QuestionID);
		if (is_null($objShowQuestionsColl) || !is_array($objShowQuestionsColl)) {
			$objShowQuestionsColl = array();
		}
		
		foreach ($objShowQuestionsColl as $objShowQuestion) {
			//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
			$showQuestionSectionID = $objShowQuestion->ShowQuestionSectionID;
			//En este caso se va a deshabilitar que se puedan ocultar preguntas de secciones que han sido mapeadas, ya que no hay un proceso
			//de refresh programado y se pudiera dar el caso de que tanto quien oculta como quien es ocultada (mediante una pregunta tipo qtpSection)
			//se encuentren en la misma página. Se validará si esto es o no deseable para en el futuro implementarlo
			if (!in_array($showQuestionSectionID, $arrInvalidSections)) {
				$showQuestionID = $objShowQuestion->ShowQuestionID;
				$consecutiveID = $objShowQuestion->ConsecutiveID;
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				if ($gbDesignMode) {
					$showID = $objShowQuestion->ShowID;
					$questionNumer = $objShowQuestion->QuestionNumber;
					$questionText = $objShowQuestion->QuestionText;
				}
				if ($showQuestionID > 0) {
					if (isset($arrOptionsPos[$consecutiveID])) {
						$optionValue = $arrOptionsPos[$consecutiveID];
						if (isset($arrDef['options'][$optionValue])) {
							$arrDef['options'][$optionValue]['showQuestionList'][] = $showQuestionID;
							//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
							if ($gbDesignMode) {
								$arrDef['options'][$optionValue]['showQuestionCons'][] = $consecutiveID;
								$arrDef['options'][$optionValue]['showQuestionShowIDs'][] = $showID;
								$arrDef['options'][$optionValue]['showQuestionText'][] = $questionNumer.". ".$questionText;
							}
						}
					}
				}
			}
			//@JAPR
		}
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		if (getMDVersion() >= esvQuestionAR) {
			//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
			//Agregada la validación extra para no regresar los atributos a menos que sea un tipo de despliegue de pregunta AR, aunque técnicamente a nivel de la pregunta no se utilizan
			//los atributos a la fecha de esta implementación
			if ($this->QTypeID == qtpSingle && $this->QDisplayMode == dspAR && $this->ValuesSourceType == tofCatalog && $this->CatalogID > 0) {
				$arrDef['dataMemberZipFileID'] = $this->DataMemberZipFileID;
				$arrDef['catMemberZipFileID'] = $this->CatMemberZipFileID;
				$arrDef['dataMemberVersionID'] = $this->DataMemberVersionID;
				$arrDef['catMemberVersionID'] = $this->CatMemberVersionID;
				$arrDef['dataMemberPlatformID'] = $this->DataMemberPlatformID;
				$arrDef['catMemberPlatformID'] = $this->CatMemberPlatformID;
				$arrDef['dataMemberTitleID'] = $this->DataMemberTitleID;
				$arrDef['catMemberTitleID'] = $this->CatMemberTitleID;
			}
		}

		//OMMC 2019-04-29: Agregada configuración del radio para cálculo de coordenadas #QEGRGA 
		if (!$gbDesignMode && getMDVersion() >= esvResetMapRadio) {
			$arrDef['mapRadio'] = 0;
		}
		
		return $arrDef;
	}
}

class BITAMQuestionCollection extends BITAMCollection
{
	public $SurveyID;
	public $SectionID;

	function __construct($aRepository, $aSurveyID)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->SurveyID = $aSurveyID;
		$this->SectionID = -1;
		$this->OrderPosField = "QuestionNumber";
	}

	static function NewInstanceEmpty($aRepository, $aSurveyID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $aSurveyID);
	}
	
	static function NewInstance($aRepository, $aSurveyID, $anArrayOfQuestionIDs = null, $isFieldSurveyID = null)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		if((is_null($anArrayOfQuestionIDs) || count($anArrayOfQuestionIDs)<=0)&&(is_null($isFieldSurveyID)))
		{
			$anInstance =& BITAMGlobalFormsInstance::GetQuestionCollectionBySurveyWithID($aSurveyID);
			if(!is_null($anInstance))
			{
				return $anInstance;
			}
		}
		
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);

		$filter = "";

		if (!is_null($anArrayOfQuestionIDs))
		{
			switch (count($anArrayOfQuestionIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND t1.QuestionID = ".((int)$anArrayOfQuestionIDs[0]);
					break;
				default:
					foreach ($anArrayOfQuestionIDs as $aQuestionID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aQuestionID; 
					}
					if ($filter != "")
					{
						$filter = " AND t1.QuestionID IN (".$filter.")";
					}
					break;
			}
		}
		$sFilter = "AND t1.SurveyID = ".$aSurveyID.$filter;
		$qOpts = array("filter" => $sFilter);
		$sql = BITAMQuestion::getQuery($qOpts);
		if($isFieldSurveyID!==null)
		{
			$sql .= " AND t1.IsFieldSurveyID = ".$isFieldSurveyID;
		}
		$sql .= " ORDER BY t3.SectionNumber, t1.QuestionNumber";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}
		
		if((is_null($anArrayOfQuestionIDs) || count($anArrayOfQuestionIDs)<=0)&&(is_null($isFieldSurveyID)))
		{
			BITAMGlobalFormsInstance::AddQuestionCollectionBySurveyWithID($aSurveyID, $anInstance);
		}

		return $anInstance;
	}
	
	static function NewInstanceWithExceptType($aRepository, $aSurveyID, $anArrayTypes)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$filter = "";

		switch (count($anArrayTypes))
		{
			case 0:
				break;
			case 1:
				$filter = " AND t1.QTypeID <> ".((int)$anArrayTypes[0]);
				break;
			default:
				foreach ($anArrayTypes as $aQTypeID)
				{
					if ($filter != "")
					{
						$filter .= ", ";
					}
					$filter .= (int) $aQTypeID; 
				}
				
				if ($filter != "")
				{
					$filter = " AND t1.QTypeID NOT IN (".$filter.")";
				}
				break;
		}
		$sFilter = "AND t1.SurveyID = ".$aSurveyID.$filter;
		$sOrderBy = "t3.SectionNumber, t1.QuestionNumber";
		$qOpts = array("filter" => $sFilter, "orderBy" => $sOrderBy);
		$sql = BITAMQuestion::getQuery($qOpts);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		return $anInstance;
	}
        
	static function NewInstanceWithExceptTypeQDisplayMode($aRepository, $aSurveyID, $anArrayTypes, $aQDisplayMode)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);

		$filter = "";

		switch (count($anArrayTypes))
		{
			case 0:
				break;
			case 1:
				$filter = " AND t1.QTypeID <> ".((int)$anArrayTypes[0]);
				break;
			default:
				foreach ($anArrayTypes as $aQTypeID)
				{
					if ($filter != "")
					{
						$filter .= ", ";
					}
					$filter .= (int) $aQTypeID; 
				}
				
				if ($filter != "")
				{
					$filter = " AND t1.QTypeID NOT IN (".$filter.")";
				}
                                
				break;
		}
                
                if($filter != "") {
                    $filter .= " AND t1.QDisplayMode NOT IN (".$aQDisplayMode.")";
                }
		$sFilter = "AND t1.SurveyID = ".$aSurveyID.$filter;
		$sOrderBy = "t3.SectionNumber, t1.QuestionNumber";
		$qOpts = array("filter" => $sFilter, "orderBy" => $sOrderBy);
		$sql = BITAMQuestion::getQuery($qOpts);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		return $anInstance;
	}
        
	static function NewInstanceWithSimpleChoiceMatrix($aRepository, $aSurveyID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$filter = " AND t1.QTypeID IN (".qtpSingle.") AND t1.QDisplayMode IN (".dspMatrix.")";
		$sFilter = "AND t1.SurveyID = ".$aSurveyID.$filter;
		$sOrderBy = "t3.SectionNumber, t1.QuestionNumber";
		$qOpts = array("filter" => $sFilter, "orderBy" => $sOrderBy);
		$sql = BITAMQuestion::getQuery($qOpts);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		return $anInstance;
	}
	
	static function NewInstanceIncludeTheseTypes($aRepository, $aSurveyID, $anArrayTypes)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);

		$filter = "";

		switch (count($anArrayTypes))
		{
			case 0:
				break;
			case 1:
				$filter = " AND t1.QTypeID = ".((int)$anArrayTypes[0]);
				break;
			default:
				foreach ($anArrayTypes as $aQTypeID)
				{
					if ($filter != "")
					{
						$filter .= ", ";
					}
					$filter .= (int) $aQTypeID; 
				}
				
				if ($filter != "")
				{
					$filter = " AND t1.QTypeID IN (".$filter.")";
				}
				break;
		}
		$sFilter = "AND t1.SurveyID = ".$aSurveyID.$filter;
		$sOrderBy = "t3.SectionNumber, t1.QuestionNumber";
		$qOpts = array("filter" => $sFilter, "orderBy" => $sOrderBy);
		$sql = BITAMQuestion::getQuery($qOpts);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		return $anInstance;
	}
	
	//@JAPR 2012-10-29: Agregada la posibilidad e consultar preguntas multiple choice sin multi-dimensión
	//con el parámetro iMultiDim, si no se envía entonces funciona como antes cargando sólo las que son multi-dimensión
	static function NewInstanceMultiChoiceDims($aRepository, $aSurveyID, $iMultiDim = 1)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		if (is_null($iMultiDim) || !is_numeric($iMultiDim)) {
			$iMultiDim = 1;
		}
		else {
			$iMultiDim = (int) $iMultiDim;
		}
		if ($iMultiDim < 0 || $iMultiDim > 1) {
			$iMultiDim = 1;
		}
		$sFilter = "AND t1.QTypeID = ".qtpMulti." AND t1.IsMultiDimension = $iMultiDim AND t1.SurveyID = ".$aSurveyID." AND ( t3.SectionType IN (".sectNormal.", ".sectMasterDet.") OR t3.SectionType IS NULL )";
		$sOrderBy = "t3.SectionNumber, t1.QuestionNumber";
		$qOpts = array("filter" => $sFilter, "orderBy" => $sOrderBy);
		$sql = BITAMQuestion::getQuery($qOpts);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		return $anInstance;
	}
	
	static function NewInstanceMultiChoiceUniqueDim($aRepository, $aSurveyID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$sFilter = "AND t1.QTypeID = ".qtpMulti." AND t1.SurveyID = ".$aSurveyID." AND t3.SectionType = ".sectDynamic." AND t3.CatalogID > 0 AND t3.CatMemberID > 0";
		$sOrderBy = "t3.SectionNumber, t1.QuestionNumber";
		$qOpts = array("filter" => $sFilter, "orderBy" => $sOrderBy);
		$sql = BITAMQuestion::getQuery($qOpts);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		return $anInstance;
	}

	static function NewInstanceBySection($aRepository, $aSectionID, $anArrayOfQuestionIDs = null)
	{
		if(is_null($anArrayOfQuestionIDs) || count($anArrayOfQuestionIDs)<=0)
		{
			$anInstance =& BITAMGlobalFormsInstance::GetQuestionCollectionBySectionWithID($aSectionID);
			if(!is_null($anInstance))
			{
				return $anInstance;
			}
		}
		
		$aSurveyID = BITAMSection::getSurveyIDBySectionID($aRepository, $aSectionID);

		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		$anInstance->SectionID = $aSectionID;

		$filter = "";

		if (!is_null($anArrayOfQuestionIDs))
		{
			switch (count($anArrayOfQuestionIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND t1.QuestionID = ".((int)$anArrayOfQuestionIDs[0]);
					break;
				default:
					foreach ($anArrayOfQuestionIDs as $aQuestionID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aQuestionID; 
					}
					if ($filter != "")
					{
						$filter = " AND t1.QuestionID IN (".$filter.")";
					}
					break;
			}
		}
		$sFilter = "AND t1.SurveyID = ".$aSurveyID." AND t1.SectionID = ".$aSectionID.$filter;
		$sOrderBy = "t3.SectionNumber, t1.QuestionNumber";
		$qOpts = array("filter" => $sFilter, "orderBy" => $sOrderBy);
		$sql = BITAMQuestion::getQuery($qOpts);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		if(is_null($anArrayOfQuestionIDs) || count($anArrayOfQuestionIDs)<=0)
		{
			BITAMGlobalFormsInstance::AddQuestionCollectionBySectionWithID($aSectionID, $anInstance);
		}

		return $anInstance;
	}
	
	static function NewInstanceWithExceptMatrix($aRepository, $aSurveyID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$filter = " AND NOT (t1.QTypeID = ".qtpSingle." AND t1.QDisplayMode = ".dspMatrix.")";
		$sFilter = "AND t1.SurveyID = ".$aSurveyID.$filter;
		$sOrderBy = "t3.SectionNumber, t1.QuestionNumber";
		$qOpts = array("filter" => $sFilter, "orderBy" => $sOrderBy);
		$sql = BITAMQuestion::getQuery($qOpts);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		return $anInstance;
	}

	//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	/* Obtiene la colección de preguntas pertenecientes a las formas indicadas en el parámetro, grabando en el caché de objetos indexado como colección para 
	cada forma y sección además de directamente cada objeto para optimizar su carga posterior (esta función se asume que es equivalente a invocar a NewInstance pero 
	con todos los parámetros en null excepto el SurveyID correspondiente) */
	static function NewInstanceFromDBFull($aRepository, $anArrayOfSurveyIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrQuestionsBySurveyID = array();
		$arrQuestionsBySectionID = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "AND t1.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = "AND t1.SurveyID IN (".$filter.")";
					}
					break;
			}
		}
		
		$qOpts = array("filter" => $filter);
		$sql = BITAMQuestion::getQuery($qOpts);
		$sql .= " ORDER BY t1.SurveyID, t3.SectionNumber, t1.QuestionNumber";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$objQuestion = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
			$anInstance->Collection[] = $objQuestion;
			
			//Agrega el objeto a la colección indexada por los padres
			$intSurveyID = $objQuestion->SurveyID;
			$intSectionID = $objQuestion->SectionID;
			if ( !isset($arrQuestionsBySurveyID[$intSurveyID]) ) {
				$arrQuestionsBySurveyID[$intSurveyID] = array();
			}
			$arrQuestionsBySurveyID[$intSurveyID][] = $objQuestion;
			if ( !isset($arrQuestionsBySectionID[$intSectionID]) ) {
				$arrQuestionsBySectionID[$intSectionID] = array();
			}
			$arrQuestionsBySectionID[$intSectionID][] = $objQuestion;
			
			$aRS->MoveNext();
		}
		
		//Finalmente procesa el array de formas y secciones para indexar por todos los padres incluyendo aquellos que no tenían elementos y agregarlos al caché 
		//(en este punto se asume que este método se ejecutó posterior a la carga masiva de todos los padres, así que se usarán las colecciones que ya deberían 
		//estar cachadas)
		if (isset(BITAMGlobalFormsInstance::$arrAllSurveyInstances)) {
			$arrAllQuestionCollection = array();
			foreach(BITAMGlobalFormsInstance::$arrAllSurveyInstances as $objSurvey) {
				$intSurveyID = $objSurvey->SurveyID;
				//Se debe usar directamente el elemento desde el array en lugar de una variable local, porque BITAMGlobalFormsInstance lo recibe por referencia
				$arrAllQuestionCollection[$intSurveyID] = new $strCalledClassColl($aRepository, $intSurveyID);
				//$objQuestionCollection = new $strCalledClassColl($aRepository, $intSurveyID);
				if ( isset($arrQuestionsBySurveyID[$intSurveyID]) ) {
					$arrAllQuestionCollection[$intSurveyID]->Collection = $arrQuestionsBySurveyID[$intSurveyID];
				}
				BITAMGlobalFormsInstance::AddQuestionCollectionBySurveyWithID($intSurveyID, $arrAllQuestionCollection[$intSurveyID]);
			}
		}

		if (isset(BITAMGlobalFormsInstance::$arrAllSectionInstances)) {
			$arrAllQuestionCollection = array();
			foreach(BITAMGlobalFormsInstance::$arrAllSectionInstances as $objSection) {
				$intSurveyID = $objSection->SurveyID;
				$intSectionID = $objSection->SectionID;
				//Se debe usar directamente el elemento desde el array en lugar de una variable local, porque BITAMGlobalFormsInstance lo recibe por referencia
				$arrAllQuestionCollection[$intSectionID] = new $strCalledClassColl($aRepository, $intSurveyID);
				//$objQuestionCollection = new $strCalledClassColl($aRepository, $intSurveyID);
				if ( isset($arrQuestionsBySectionID[$intSectionID]) ) {
					$arrAllQuestionCollection[$intSectionID]->Collection = $arrQuestionsBySectionID[$intSectionID];
				}
				BITAMGlobalFormsInstance::AddQuestionCollectionBySectionWithID($intSectionID, $arrAllQuestionCollection[$intSectionID]);
			}
		}
		
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Carga información adicional que de otra manera se tendría que cargar pregunta a pregunta
		//Carga las preguntas dependientes
		$sql = "SELECT t1.SourceQuestionID, t1.QuestionID 
			FROM SI_SV_Question t1 
			WHERE t1.SourceQuestionID > 0 ".$filter." 
			ORDER BY t1.SurveyID, t1.SourceQuestionID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$arrQuestionsToFill = array();
			while(!$aRS->EOF) {
				$intSourceQuestionID = (int)$aRS->fields["sourcequestionid"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				if ( $intSourceQuestionID && $intQuestionID ) {
					if ( !isset($arrQuestionsToFill[$intSourceQuestionID]) ) {
						$arrQuestionsToFill[$intSourceQuestionID] = array();
					}
					$arrQuestionsToFill[$intSourceQuestionID][] = $intQuestionID;
				}
				$aRS->MoveNext();
			}
			
			foreach ( $arrQuestionsToFill as $intSourceQuestionID => $arrQuestionsToFillColl ) {
				BITAMGlobalFormsInstance::AddChildrenToFillByQuestionWithID($intSourceQuestionID, $arrQuestionsToFill[$intSourceQuestionID]);
			}
		}
		
		//Carga las preguntas a excluir
		$sql = "SELECT t1.SourceSimpleQuestionID, t1.QuestionID 
			FROM SI_SV_Question t1 
			WHERE t1.SourceSimpleQuestionID > 0 ".$filter." 
			ORDER BY t1.SurveyID, t1.SourceSimpleQuestionID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$arrQuestionsToExclude = array();
			while(!$aRS->EOF) {
				$intSourceQuestionID = (int)$aRS->fields["sourcesimplequestionid"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				if ( $intSourceQuestionID && $intQuestionID ) {
					if ( !isset($arrQuestionsToExclude[$intSourceQuestionID]) ) {
						$arrQuestionsToExclude[$intSourceQuestionID] = array();
					}
					$arrQuestionsToExclude[$intSourceQuestionID][] = $intQuestionID;
				}
				$aRS->MoveNext();
			}
			
			foreach ( $arrQuestionsToExclude as $intSourceQuestionID => $arrQuestionsToExcludeColl ) {
				BITAMGlobalFormsInstance::AddChildrenToExcludeByQuestionWithID($intSourceQuestionID, $arrQuestionsToExclude[$intSourceQuestionID]);
			}
		}
		
		//Carga las secciones llenadas por otras preguntas
		$sql = "SELECT t1.SourceQuestionID, t1.SectionID 
			FROM SI_SV_Section t1 
			WHERE t1.SourceQuestionID > 0 ".$filter." 
			ORDER BY t1.SurveyID, t1.SourceQuestionID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$arrSectionsToFill = array();
			while(!$aRS->EOF) {
				$intSourceQuestionID = (int)$aRS->fields["sourcequestionid"];
				$intSectionID = (int)$aRS->fields["sectionid"];
				if ( $intSourceQuestionID && $intSectionID ) {
					if ( !isset($arrSectionsToFill[$intSourceQuestionID]) ) {
						$arrSectionsToFill[$intSourceQuestionID] = array();
					}
					$arrSectionsToFill[$intSourceQuestionID][] = $intSectionID;
				}
				$aRS->MoveNext();
			}
			
			foreach ( $arrSectionsToFill as $intSourceQuestionID => $arrSectionsToFillColl ) {
				BITAMGlobalFormsInstance::AddSectionsToFillByQuestionWithID($intSourceQuestionID, $arrSectionsToFill[$intSourceQuestionID]);
			}
		}
		
		//Carga las secciones llenadas por otras secciones
		$sql = "SELECT t1.SourceSectionID, t1.SectionID 
			FROM SI_SV_Section t1 
			WHERE t1.SourceSectionID > 0 ".$filter." 
			ORDER BY t1.SurveyID, t1.SourceSectionID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$arrSectionsToFill = array();
			while(!$aRS->EOF) {
				$intSourceSectionID = (int)$aRS->fields["sourcesectionid"];
				$intSectionID = (int)$aRS->fields["sectionid"];
				if ( $intSourceSectionID && $intSectionID ) {
					if ( !isset($arrSectionsToFill[$intSourceSectionID]) ) {
						$arrSectionsToFill[$intSourceSectionID] = array();
					}
					$arrSectionsToFill[$intSourceSectionID][] = $intSectionID;
				}
				$aRS->MoveNext();
			}
			
			foreach ( $arrSectionsToFill as $intSourceSectionID => $arrSectionsToFillColl ) {
				BITAMGlobalFormsInstance::AddSectionsToFillBySectionWithID($intSourceSectionID, $arrSectionsToFill[$intSourceSectionID]);
			}
		}
		
		//Carga las preguntas tipo sección llenadas por otras secciones
		$sql = "SELECT t1.SourceSectionID, t1.QuestionID 
			FROM SI_SV_Question t1 
			WHERE t1.QTypeID = ".qtpSection." AND t1.SourceSectionID > 0 ".$filter." 
			ORDER BY t1.SurveyID, t1.SourceSectionID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$arrSectionQuestions = array();
			while(!$aRS->EOF) {
				$intSourceSectionID = (int)$aRS->fields["sourcesectionid"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				if ( $intSourceSectionID && $intQuestionID ) {
					if ( !isset($arrSectionQuestions[$intSourceSectionID]) ) {
						$arrSectionQuestions[$intSourceSectionID] = array();
					}
					$arrSectionQuestions[$intSourceSectionID][] = $intQuestionID;
				}
				$aRS->MoveNext();
			}
			
			foreach ( $arrSectionQuestions as $intSourceSectionID => $arrSectionQuestionsColl ) {
				BITAMGlobalFormsInstance::AddSectionQuestionsBySourceSectionWithID($intSourceSectionID, $arrSectionQuestions[$intSourceSectionID]);
			}
		}
		//@JAPR
		
		return $anInstance;
	}
	
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	/* Obtiene la colección ordenada de IDs de atributos de catálogo y simultáneamente la de los datasources correspondientes */
	static function LoadCatMembersListFromDBFull($aRepository, $anArrayOfSurveyIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrCatMemberList = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "WHERE B.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = "WHERE B.SurveyID IN (".$filter.")";
					}
					break;
			}
		}
		
		$sql = "SELECT A.QuestionID, A.MemberID AS DataSourceMemberID 
			FROM SI_SV_QuestionMembers A 
				INNER JOIN SI_SV_Question B ON A.QuestionID = B.QuestionID ".(($filter)?$filter:"")." 
			ORDER BY A.QuestionID, A.MemberOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF) {
			$intQuestionID = (int) @$aRS->fields["questionid"];
			$intDataSourceMemberID = (int) @$aRS->fields["datasourcememberid"];
			if ($intQuestionID > 0 && $intDataSourceMemberID > 0) {
				if ( !isset($arrCatMemberList[$intQuestionID] ) ) {
					$arrCatMemberList[$intQuestionID] = array();
				}
				$arrCatMemberList[$intQuestionID][] = $intDataSourceMemberID;
			}
			
			$aRS->MoveNext();
		}
		
		foreach ($arrCatMemberList as $intQuestionID => $arrMemberIDs) {
			BITAMGlobalFormsInstance::AddCatMembersListByQuestionWithID($intQuestionID, $arrCatMemberList[$intQuestionID]);
		}
	}
	
	/* Obtiene la colección ordenada de IDs de preguntas dependientes de condición de visibilidad */
	static function LoadShowConditionChildrenFromDBFull($aRepository, $anArrayOfSurveyIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrShowConditionChildren = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "WHERE B.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = "WHERE B.SurveyID IN (".$filter.")";
					}
					break;
			}
		}
		
		$sql = "SELECT A.EvalElementID, A.ElementID 
			FROM SI_SV_ShowConditionDep A 
				INNER JOIN SI_SV_Question B ON A.EvalElementID = B.QuestionID ".(($filter)?$filter:"")." 
			ORDER BY A.EvalElementID, A.ElementID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF) {
			$intQuestionID = (int)$aRS->fields["elementid"];
			$intEvalQuestionID = (int)$aRS->fields["evalelementid"];
			if ($intQuestionID > 0 && $intEvalQuestionID > 0) {
				if ( !isset($arrShowConditionChildren[$intEvalQuestionID] ) ) {
					$arrShowConditionChildren[$intEvalQuestionID] = array();
				}
				$arrShowConditionChildren[$intEvalQuestionID][] = $intQuestionID;
			}
			
			$aRS->MoveNext();
		}
		
		foreach ($arrShowConditionChildren as $intEvalQuestionID => $arrMemberIDs) {
			BITAMGlobalFormsInstance::AddShowCondDepByQuestionWithID($intEvalQuestionID, $arrShowConditionChildren[$intEvalQuestionID]);
		}
	}
	
	/* Obtiene la colección ordenada de IDs de preguntas para dependencias de default values */
	static function LoadQuestionToEvalFromDBFull($aRepository, $anArrayOfSurveyIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrQuestionsToEval = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "WHERE B.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = "WHERE B.SurveyID IN (".$filter.")";
					}
					break;
			}
		}
		
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Corregido un bug, el query que se había dejado era el copiado de showQuestions así que no aplicaba los defaultValues
		$sql = "SELECT A.EvalQuestionID, A.QuestionID 
			FROM SI_SV_QuestionDep A 
				INNER JOIN SI_SV_Question B ON A.EvalQuestionID = B.QuestionID ".(($filter)?$filter:"")." 
			ORDER BY A.EvalQuestionID, A.QuestionID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF) {
			$intQuestionID = (int)$aRS->fields["questionid"];
			$intEvalQuestionID = (int)$aRS->fields["evalquestionid"];
			if ($intQuestionID > 0 && $intEvalQuestionID > 0) {
				if ( !isset($arrQuestionsToEval[$intEvalQuestionID] ) ) {
					$arrQuestionsToEval[$intEvalQuestionID] = array();
				}
				$arrQuestionsToEval[$intEvalQuestionID][] = $intQuestionID;
			}
			
			$aRS->MoveNext();
		}
		
		foreach ($arrQuestionsToEval as $intEvalQuestionID => $arrMemberIDs) {
			BITAMGlobalFormsInstance::AddQuestionToEvalQuestionWithID($intEvalQuestionID, $arrQuestionsToEval[$intEvalQuestionID]);
		}
	}
	
	/* Obtiene la colección ordenada de IDs de preguntas dependientes de preguntas fórmula */
	static function LoadFormulaChildrenFromDBFull($aRepository, $anArrayOfSurveyIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrFormulaChildren = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "WHERE B.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = "WHERE B.SurveyID IN (".$filter.")";
					}
					break;
			}
		}
		
		$sql = "SELECT A.EvalQuestionID, A.QuestionID 
			FROM SI_SV_QuestionFormulaDep A 
				INNER JOIN SI_SV_Question B ON A.EvalQuestionID = B.QuestionID ".(($filter)?$filter:"")." 
			ORDER BY A.EvalQuestionID, A.QuestionID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFormulaDep, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFormulaDep, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF) {
			$intQuestionID = (int)$aRS->fields["questionid"];
			$intEvalQuestionID = (int)$aRS->fields["evalquestionid"];
			if ($intQuestionID > 0 && $intEvalQuestionID > 0) {
				if ( !isset($arrFormulaChildren[$intEvalQuestionID] ) ) {
					$arrFormulaChildren[$intEvalQuestionID] = array();
				}
				$arrFormulaChildren[$intEvalQuestionID][] = $intQuestionID;
			}
			
			$aRS->MoveNext();
		}
		
		foreach ($arrFormulaChildren as $intEvalQuestionID => $arrMemberIDs) {
			BITAMGlobalFormsInstance::AddFormulaChildDepByQuestionWithID($intEvalQuestionID, $arrFormulaChildren[$intEvalQuestionID]);
		}
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("SectionID", $aHTTPRequest->GET))
		{
			$aSectionID = (int) $aHTTPRequest->GET["SectionID"];
		}
		else
		{
			$aSectionID = 0;
		}
		
		if(array_key_exists("QuestionNumber", $aHTTPRequest->POST))
		{
			$orderPos = (int)$aHTTPRequest->POST["QuestionNumber"];
			$aQuestionID = (int)$aHTTPRequest->POST["QuestionID"];
			BITAMQuestionCollection::Reorder($aRepository, $aSectionID, $aQuestionID, $orderPos);
		}
		
		if(array_key_exists("QuestionNumber", $aHTTPRequest->POST))
		{
			$anInstance = BITAMQuestion::NewInstance($aRepository, $aSectionID);
			$anInstance = $anInstance->get_Parent();
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		else 
		{
			return BITAMQuestionCollection::NewInstanceBySection($aRepository, $aSectionID);
		}
	}
	
	static function Reorder($aRepository, $aSectionID, $aQuestionID, $orderPos)
	{
		$aSurveyID = BITAMSection::getSurveyIDBySectionID($aRepository, $aSectionID);

		$prevOrderPos = BITAMQuestion::getCurrentOrderPos($aRepository, $aSurveyID, $aQuestionID);
		
		if($prevOrderPos>0) {
			if($orderPos > $prevOrderPos)
			{
				$sql = "UPDATE SI_SV_Question SET QuestionNumber = QuestionNumber - 1 
						WHERE ( QuestionNumber BETWEEN ".$prevOrderPos." AND ".$orderPos." ) AND SurveyID = ".$aSurveyID;
				if($aRepository->DataADOConnection->Execute($sql)===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			if ($prevOrderPos > $orderPos)
			{
				$sql = "UPDATE SI_SV_Question SET QuestionNumber = QuestionNumber + 1 
						WHERE ( QuestionNumber BETWEEN ".$orderPos." AND ".$prevOrderPos." ) AND SurveyID = ".$aSurveyID;
				if($aRepository->DataADOConnection->Execute($sql)===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$sql = "UPDATE SI_SV_Question SET QuestionNumber = ".$orderPos." 
					WHERE SurveyID = ".$aSurveyID." AND QuestionID = ".$aQuestionID;
			if($aRepository->DataADOConnection->Execute($sql)===false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($aRepository, $aSurveyID);
		}
	}
	
	function get_Parent()
	{
		return BITAMSection::NewInstanceWithID($this->Repository, $this->SectionID);
	}

	function get_Title()
	{
		return translate("Questions");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=QuestionCollection&SectionID=".$this->SectionID;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=Question&SectionID=".$this->SectionID;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'QuestionID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();
		
		//@JAPR 2014-09-18: Agregados los datos extendidos de modificación
		//Esta propiedad es sólo de despliegue, así que se puede traducir directamente sin problemas
		//En este punto se tiene que hacer un recorrido a la colección, ya que no se puede hacer el Replace de esta propiedad mientras se construye
		//pues el método de reemplazo carga otra colección y generaría un ciclo infinito, tampoco se puede condicionar mediante un parámetro porque
		//si ya se había cargado previamente, se queda en la colección global con o sin el reemplazo y la siguiente petición no necesariamente
		//tendría el reemplazo si no se usa como parte de la llave en la colección global
		foreach ($this->Collection as $aQuestion) {
			$aQuestion->DisplayQuestion = $aQuestion->TranslateVariableQuestionIDsByNumbers($aQuestion->DisplayQuestion);
		}
		//@JAPR
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DisplayQuestion";
		$aField->Title = translate("Question");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QTypeID";
		$aField->Title = translate("Question Type");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMQuestion::GetAllQuestionTypes($this->Repository);
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			
			if($aSurveyInstance->Status!=2)
			{
				//15Feb2013: Puede haber casos en los que la sección de preguntas esté habilitada para secciones formateadas
				//en ese caso no se podrán agregar preguntas
				$sectionType = BITAMSection::getSectionType($this->Repository, $this->SectionID);
				
				if($sectionType==sectFormatted)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else 
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			
			if($aSurveyInstance->Status==0)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
		//13Feb2013: Si es pregunta simple choice que usa catálogo, se verifica si la encuensta está en alguna agenda que utilice el catálogo y atributo de la pregunta
		//y si es así se manda el mensaje de advertencia
		
		//IDs de agendas que utilizan el catálogo y atributo de la pregunta Simple Choice y tienen asociadas la encuesta
		
		$msgByQuestion=array();
		$msgAgendaAlert = false;
		
		foreach ($this->Collection as $aQuestion)
		{
			$arrayAgendasIDs=array();
			
			if($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog)
			{
				$sql= "SELECT t1.AgendaID 
						FROM SI_SV_SurveyAgenda t1, SI_SV_SurveyAgendaDet t2
						WHERE t1.AgendaID = t2.AgendaID
						AND t1.CatalogID=".$aQuestion->CatalogID." AND t1.AttributeID=".$aQuestion->CatMemberID." AND SurveyID=".$this->SurveyID;
				
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda t1, SI_SV_SurveyAgendaDet t2 ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while (!$aRS->EOF)
				{
					$arrayAgendasIDs[] = (int) $aRS->fields["agendaid"];
				
					$aRS->MoveNext();
				}
				
				//Obtener nombre de las agendas			
				$arrayAgendasNames=array();
				$strAgendas = "";
				if(count($arrayAgendasIDs)>0)
				{
		
					$sql = "SELECT t1.AgendaID, t1.AgendaName, t1.CatalogID, t2.ParentID, t2.CatalogName, t2.TableName, t1.AttributeID, t3.MemberName, t1.FilterText, 
							t1.CaptureStartDate, t1.CaptureStartTime, t1.UserID, t1.LastDateID 
							FROM SI_SV_SurveyAgenda t1 
							LEFT OUTER JOIN SI_SV_Catalog t2 ON t1.CatalogID = t2.CatalogID 
							LEFT OUTER JOIN SI_SV_CatalogMember t3 ON t1.CatalogID = t3.CatalogID AND t1.AttributeID = t3.MemberID 
							WHERE t1.AgendaID IN (".implode(",", $arrayAgendasIDs).")";
					
					$aRS = $this->Repository->DataADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				
					while (!$aRS->EOF)
					{
						$agenName = $aRS->fields["catalogname"].' - '.$aRS->fields["membername"].' - '.$aRS->fields["filtertext"];
						$arrayAgendasNames[] = $agenName;
						$strAgendas.="\\r\\n"." - ".$agenName;
						$aRS->MoveNext();
					}
				}		
				
				if(trim($strAgendas)!="")
				{
					$this->MsgByElement[$aQuestion->QuestionID] = $strAgendas;
					$msgAgendaAlert = true;
				}
			}
			
			//@JAPR 2015-01-21: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
			//si es utilizado y advertir al usuario (#75TPJE)
			//Verifica si esta pregunta tiene o no dependencias, si es así entonces la marca como no eliminable para que el canRemove falle
			$blnValid = true;
			switch ($aQuestion->QTypeID) {
				case qtpShowValue:
				case qtpMessage:
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSketch:
				case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValid = false;
					break;
			}
			
			//Con esta propiedad se forzará dinámicamente a bloquear la posibilidad de remover el objeto bajo alguna condición asignada en forma
			//externa a la propia instancia (útil cuando se tiene una colección que debe bloquear ciertas preguntas a partir de consultas y no
			//se quieren lanzar dichas consultas por cada instancia de los objetos de la colección, o bien si no se quiere ocultar el botón para
			//eliminar de la propia instancia de objeto sino controlar que sólo no se pueda eliminar desde la colección)
			if ($blnValid) {
				$strVarsUsed = SearchVariablePatternInMetadata($this->Repository, $aQuestion->SurveyID, $aQuestion->QuestionID, otyQuestion);
				if ($strVarsUsed != '') {
					$aQuestion->enableRemove = false;
				}
			}
			//@JAPR
		}
		
		if($msgAgendaAlert)
		{
			$this->MsgConfirmRemove = translate("The form will not be included on any agenda if you delete the question").".".translate("Do you wish to delete the question?")."\\r\\n".translate("The Survey will be eliminated from the agendas").":";
		}
	}
}
?>