<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("cubeClasses.php");

class BITAMFilteredSV extends BITAMObject
{
	public $consecutivo;
	public $nom_tabla;
	public $nom_fisicok;
	public $values;
	public $except;
	//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
	public $formFieldID;
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		
		$this->consecutivo=-1;
		$this->nom_tabla="";
		$this->nom_fisicok="";
		$this->values=array();
		$this->except = false;
		//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
		$this->formFieldID = '';
	}
	
	static function NewInstance($aRepository)
	{
		return new BITAMFilteredSV($aRepository);
	}
	
	//@JAPRDescontinuada: Al día 2014-06-12 no se encontró ningún proceso que la involucrara
	function mappingFilterBeetweenCubes($aRepository,$dimensionIDs, $selectedValues, $sourceCube, $newCube, $noIncludedDims)
	{
		$filterInfo=array();
		//Dimensiones q no se consideran para el filtro (se elimina su id del arreglo $dimensionIDs)
		$numOfNoIncludedDims=count($noIncludedDims);
		for($i=0;$i<$numOfNoIncludedDims;$i++)
		{
			$noIncludedDimIndex = array_search($noIncludedDims[$i], $dimensionIDs);
			array_splice ($dimensionIDs, $noIncludedDimIndex, 1);
			array_splice ($selectedValues, $noIncludedDimIndex, 1);
		}
		//$arrayClaDescrip está ordenado por el consecutivo del $sourceCube
		$arrayClaDescrip = BITAMDimension::getClaDescripFromDimensionIDsAndCubeID($aRepository, $dimensionIDs, $sourceCube);
		$numClaDescrip=count($arrayClaDescrip);
		//consecutivos en el cubo $newCube
		$newDimIDs=array();
		//Contendrá sólo los valores de las dimensiones compartidas
		$newSelectedValues=array();
		
		for($i=0;$i<$numClaDescrip;$i++)
		{
			$dimID  = BITAMDimension::getDimIDByClaDescrip($aRepository,$arrayClaDescrip[$i],$newCube);
			//si sí existe la dimensión en el cubo $newCube
			if($dimID!==null)
			{
				$newDimIDs[]=$dimID;
				$newSelectedValues[]=$selectedValues[$i];
			}
		}
		
		array_multisort($newDimIDs,$newSelectedValues);
		
		if(count($newDimIDs)>0)
		{
			$dimensions = BITAMDimensionCollection::NewInstance($aRepository,$newDimIDs,$newCube,true,true,false);
			//$aSecurityFilterAndLevels=$this->createStringSecurityFilterAndLevels($comboDimensions,$comboDimensionValues);
			$filerCollection=BITAMFilteredSVCollection::NewInstanceByCollections($aRepository,$dimensions,$newSelectedValues);
			$filerCollection->GenerateStrFilterByDimensionCollection();
			$filterInfo["filter"]=$filerCollection->Filter;
			$filterInfo["levels"]=$filerCollection->Levels;
		}
		
		return $filterInfo;
	}
}

class BITAMFilteredSVCollection extends BITAMCollection
{
	public $Filter;
	public $Levels;
	public $NumFilteredDimensions;
	public $CubeID;
	//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
	public $CatalogID;					//Si se recibe el CatalogID, es muy probable que se trate de un filtro para un catálogo de eBavel, así que lo verificará para cambiar los nombres de los campos a utilizar
	
	//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
	//Si se recibe el parámetro $aCatalogID, es muy probable que se trate de un filtro para un catálogo de eBavel, así que lo verificará para cambiar
	//los nombres de los campos a utilizar
	function __construct($aRepository,$aFilter,$levels,$aCubeID,$aCatalogID=-1)
	{
		BITAMCollection::__construct($aRepository);
		$this->Filter = $aFilter;
		$this->Levels = $levels;
		$this->NumFilteredDimensions = 0;
		$this->CubeID = $aCubeID;
		//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
		$this->CatalogID = $aCatalogID;
	}
	
	//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
	//Si se recibe el parámetro $aCatalogID, es muy probable que se trate de un filtro para un catálogo de eBavel, así que lo verificará para cambiar
	//los nombres de los campos a utilizar
	static function NewInstance($aRepository,$aFilter="",$levels="",$aCubeID=-1, $aCatalogID=-1)
	{
		$anInstance = new BITAMFilteredSVCollection($aRepository, $aFilter, $levels, $aCubeID, $aCatalogID);
		
		return $anInstance;
	}
	
	//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
	//Si se recibe el parámetro $aCatalogID, es muy probable que se trate de un filtro para un catálogo de eBavel, así que lo verificará para cambiar
	//los nombres de los campos a utilizar
	static function NewInstanceByStrFilter($aRepository, $aFilter, $levels, $aCubeID, $aCatalogID=-1)
	{
		$anInstance = new BITAMFilteredSVCollection($aRepository,$aFilter,$levels,$aCubeID,$aCatalogID);
		
		$anInstance->GenerateFilteredDimensionCollection();
		//$anInstance->GenerateStrFilterByDimensionCollection();
		
		return $anInstance;
	}

	//@JAPRDescontinuada: Al día 2014-06-12 no se encontró ningún proceso que la involucrara (excepto otro descontinuado por lo mismo)
	static function NewInstanceByCollections($aRepository, $aDimensions, $aValues, $aDimensionsToSkip=array())
	{
		$anInstance = new BITAMFilteredSVCollection($aRepository,'','',-1,-1);
		$intNumDimensions=count($aDimensions->Collection);
		$intIndex=0;
		for($i=0;$i<$intNumDimensions;$i++)
		{
			$intConsecutive=$aDimensions->Collection[$i]->consecutivo;
			if(!array_key_exists($intConsecutive,$aDimensionsToSkip))
			{
				$arrValues=$aValues[$i];
				if(!is_array($arrValues))
				{
					$aValue=$arrValues;
					$arrValues=array();
					$arrValues[]=$aValue;
				}
				$intNumValues=count($arrValues);
				$aDimensionInstance=$aDimensions->Collection[$i];
				if($intNumValues>0)
				{
					$anInstance->Collection[$intIndex]=BITAMFilteredSV::NewInstance($anInstance->Repository);
					$anInstance->Collection[$intIndex]->consecutivo=$aDimensionInstance->consecutivo;
					$anInstance->Collection[$intIndex]->nom_tabla=$aDimensionInstance->nom_tabla;
					$anInstance->Collection[$intIndex]->nom_fisicok=$aDimensionInstance->nom_fisicok_bd;
					$anInstance->Collection[$intIndex]->values=$arrValues;
					$intIndex++;
				}
			}
		}
		
		return $anInstance;
	}

	//ISUAREZ 21-OCTUBRE-2009 
	//Elaboración del filtro de No Aplica personalizado para los presupuestos del modelo financiero
	//Caso Soportado => Dimensiones relacionadas con el catalogo maestro(niveles y cuenta afectable)
	//@JAPRDescontinuada: Al día 2014-06-12 no se encontró ningún proceso que la involucrara
	static function NewInstanceWithNoApplyValues($aRepository, $aDimensions, $aDimensionsToSkip, $except=false)
	{
		$anInstance = new BITAMFilteredSVCollection($aRepository,'','',-1,-1);
		$intNumDimensions=count($aDimensions->Collection);
		$intIndex=0;
		$arrValues=array();
		
		$existDimsFromFinancialModel = BITAMFilteredSVCollection::existDimsFromFinancialModel($aDimensions);
		$masterAccounstTable = "SI_EKT_MASTERACCOUNTSLEVELS";
		
		for($i=0;$i<$intNumDimensions;$i++)
		{
			if($existDimsFromFinancialModel["masteraccount"]==true)
			{
				if(strtoupper($aDimensions->Collection[$i]->nom_tabla)==$masterAccounstTable)
				{
					continue;
				}
			}
			
			$intConsecutive=$aDimensions->Collection[$i]->consecutivo;
			if(!array_key_exists($intConsecutive,$aDimensionsToSkip))
			{
				$aDimensionInstance=$aDimensions->Collection[$i];

				//Valor de 'No Aplica'
				$arrValues[0]=$aDimensionInstance->NoApplyValue;

				$anInstance->Collection[$intIndex]=BITAMFilteredSV::NewInstance($anInstance->Repository);
				$anInstance->Collection[$intIndex]->consecutivo=$aDimensionInstance->consecutivo;
				$anInstance->Collection[$intIndex]->nom_tabla=$aDimensionInstance->nom_tabla;
				$anInstance->Collection[$intIndex]->nom_fisicok=$aDimensionInstance->nom_fisicok_bd;
				$anInstance->Collection[$intIndex]->values=$arrValues;
				$anInstance->Collection[$intIndex]->except=$except;
				$intIndex++;
			}
		}
		
		return $anInstance;
	}
	
	//ISUAREZ 21-OCTUBRE-2009 
	//Elaboración del filtro de No Aplica personalizado para los presupuestos del modelo financiero
	//Caso Soportado => Dimensiones relacionadas con el catalogo maestro(niveles y cuenta afectable)
	static function existDimsFromFinancialModel($aDimensions)
	{
		//Verificaremos si hay dimensiones del modelo financiero que se esten procesando, ya que si estan involucrada para este
		//caso los niveles del catalogo maestro, entonces si se esta presupuestando por niveles excepto por la dimension
		//affectableaccount del catalogo maestro, entonces en el query del registro unico va a contener un filtro de
		//No Aplica -1 en el campo AffectableAccount lo cual será incorrecto ya que no hay un registro en la tabla
		//SI_EKT_MasterAccountsLevels que tenga valores diferentes de -1 en alguno de sus niveles y que en el campo 
		//AffectableAccount esté presente con el valor -1, esa combinación nunca va a existir y por lo tanto el query del registro 
		//unico no traerá datos y siempre indicará que EktosBS tiene q hacer una operación de INSERT en vez de UPDATE
		//Tambien pasa un caso similar cuando se esta presupuestando solamente con la dimensión AffectableAccount, entonces 
		//se coloca todo el filtro de los niveles de dimensión con valor de No Aplica, y sucede que en la tabla de niveles
		//no van a existir registros con con valor de No Aplica en todos los niveles y que en el campo AffectableAccount
		//si exista el valor de la cuenta, también se tiene q eliminar este filtro, sino indicara que esa combinación en 
		//la FactAccounts no existe y por lo tanto siempre hara INSERT
		//Otro caso adicional es cuando se tiene un presupuesto con algunos niveles de dimensión y se incluye la dimensión
		//AffectableAccount, por ejemplo Nivel1, Nivel2 y AffectableAccount, quiere decir q en la pantalla de captura
		//te mostrara todas las cuentas hijas, nietas..., de Nivel2 ya sea afectables o no afectables y la misma cuenta del Nivel2,
		//porque en la tabla de niveles para un NivelX en el campo AffectableAccount aparecera la cuenta del NivelX filtrada
		//así como todas las cuentas hijas, nietas..., afectables y no afectables, pero en el query del registro unico lo que va a pasar 
		//es q le pondra un filtro de No Aplica a partir del Nivel 3 en adelante y por lo tanto no traerá registros de combinanciones
		//unicas de cuentas de Nivel3 en adelante, más que el registro de la cuenta de Nivel2 filtrada en el combo, 
		//y por lo tanto los datos grabados en las demás cuentas de nivel 3 en adelante aparentaran que no estan grabados en registros
		//unicos y por lo tanto siempre hara operación de INSERT en vez de UPDATE
				
		//Queda pendiente el analisis de las dimensiones de los niveles de las cuentas de compañias y el affectableaccount
		// y las dimensiones de niveles de dimensión y la dimension AffectableDivision

		$masterAccounstTable = "SI_EKT_MASTERACCOUNTSLEVELS";
		$masterAccountsLevelsFields = array();
		$masterAccountsLevelsFields["LEVEL_1"] = 1;
		$masterAccountsLevelsFields["LEVEL_2"] = 1;
		$masterAccountsLevelsFields["LEVEL_3"] = 1;
		$masterAccountsLevelsFields["LEVEL_4"] = 1;
		$masterAccountsLevelsFields["LEVEL_5"] = 1;
		$masterAccountsLevelsFields["LEVEL_6"] = 1;
		$masterAccountsLevelsFields["LEVEL_7"] = 1;
		$masterAccountsLevelsFields["LEVEL_8"] = 1;
		$masterAccountsLevelsFields["LEVEL_9"] = 1;
		$masterAccountsLevelsFields["LEVEL_10"] = 1;
		$masterAccountsLevelsFields["LEVEL_11"] = 1;
		$masterAccountsLevelsFields["LEVEL_12"] = 1;
		$masterAccountsLevelsFields["LEVEL_13"] = 1;
		$masterAccountsLevelsFields["LEVEL_14"] = 1;
		$masterAccountsLevelsFields["LEVEL_15"] = 1;
		$masterAccountsLevelsFields["LEVEL_16"] = 1;
		$masterAccountsLevelsFields["AFFECTABLEACCOUNT"] = 1;

		$existDimsFromFinancialModel["masteraccount"] = false;
		$useDimsFromFinancialModel["masteraccount"] = false;
		
		$intNumDimensions=count($aDimensions->Collection);

		//Se verifica si se estan utilizando dimensiones de 
		//catalogo maestro o de division
		for($i=0; $i<$intNumDimensions; $i++)
		{
			$aDimensionInstance=$aDimensions->Collection[$i];
			$temporalTable = strtoupper($aDimensionInstance->nom_tabla);
			$temporalJoin = strtoupper($aDimensionInstance->nom_fisicok_bd);
			
			//Se checa si es dimension de catalogo maestro
			if($temporalTable==$masterAccounstTable)
			{
				//Se establece como Verdadero
				//ya que aqui se detectó que si se está usando el modelo financiero
				//pero posteriormente se verificará si se está usando 
				//las dimensiones del catalogo maestro para presupuestar
				$useDimsFromFinancialModel["masteraccount"] = true;
				
				if(isset($masterAccountsLevelsFields[$temporalJoin]))
				{
					$masterAccountsLevelsFields[$temporalJoin]=0;
				}
			}
		}
		
		if($useDimsFromFinancialModel["masteraccount"]==true)
		{
			foreach($masterAccountsLevelsFields as $value)
			{
				if($value==1)
				{
					$existDimsFromFinancialModel["masteraccount"] = true;
					break;
				}
			}
		}
		
		return $existDimsFromFinancialModel;
	}
	
	function GenerateFilteredDimensionCollection()
	{
		$aFilter = $this->Filter;
		
		$levels = $this->Levels;
		//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
		//Verifica si el fitlro se solicitó como parte de un catálogo de eBavel, si es así entonces agregará en la propiedad formFieldID el ID de los
		//campos de eBavel que corresponden con cada atributo involucrado en el filtro (atributo que no se encuentre actualmente definido, es un error
		//pero simplemente se ignorará y no llenará el formFieldID para que se excluya al generar la expresión del filtro)
		$blneBavelCatalog = false;
		$arrCatalogMembersByClaDescrip = array();
		$arrAllEBavelFormsFields = array();
		if ($this->CatalogID > 0) {
			require_once('catalog.inc.php');
			require_once('catalogmember.inc.php');
			$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
			if (!is_null($objCatalog)) {
				if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
					$blneBavelCatalog = true;
					$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
					if (!is_null($objCatalogMembersColl)) {
						foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
							$arrCatalogMembersByClaDescrip[$objCatalogMember->ClaDescrip] = $objCatalogMember;
						}
					}
				}
			}
			
			if ($blneBavelCatalog) {
				require_once('eBavelIntegration.inc.php');
				$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');
				foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
					foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
						if ($intFieldID > 0) {
							$arrAllEBavelFormsFields[$intFieldID] = $arrFieldData['id'];
						}
					}
				}
			}
		}
		//@JAPR
		
		if($aFilter!=null && trim($aFilter)!="" && $levels!=null && trim($levels)!="")
		{
			$auxFilteredDimensionIDs = explode("|", $levels); 
			$filteredDimensionIDs=array();
			$count=0;
			for ($i=0;$i<count($auxFilteredDimensionIDs);$i++)
			{
				if ($auxFilteredDimensionIDs[$i]>0 && $auxFilteredDimensionIDs[$i]!="")
				{
					$filteredDimensionIDs[$count]=(int)$auxFilteredDimensionIDs[$i];
					$count++;
				}
			}
			
			//Número de Dimensiones Filtradas
			$this->NumFilteredDimensions = $count;	
			
			for($i=0; $i<$this->NumFilteredDimensions; $i++)
			{
				//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
				$intClaDescript = (int) @$filteredDimensionIDs[$i];
				$streBavelFieldID = '';
				if ($blneBavelCatalog && $intClaDescript > 0 && isset($arrCatalogMembersByClaDescrip[$intClaDescript])) {
					$objCatalogMember = $arrCatalogMembersByClaDescrip[$intClaDescript];
					$inteBabelFieldID = $objCatalogMember->eBavelFieldID;
					$streBavelFieldID = (string) @$arrAllEBavelFormsFields[$inteBabelFieldID];
				}
				//@JAPR
				
				$this->Collection[$i]=BITAMFilteredSV::NewInstance($this->Repository);
				$this->Collection[$i]->consecutivo=$filteredDimensionIDs[$i];
				//$aDimensionInstance=BITAMDimension::NewInstanceWithID($this->Repository, $this->Collection[$i]->consecutivo, $this->CubeID,false);
				$aDimensionInstance=BITAMDimension::NewInstanceWithDescripID($this->Repository, $this->Collection[$i]->consecutivo, $this->CubeID, false);
				$this->Collection[$i]->nom_tabla=$aDimensionInstance->nom_tabla;
				$this->Collection[$i]->nom_fisicok=$aDimensionInstance->nom_fisicok_bd;
				//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
				$this->Collection[$i]->formFieldID = $streBavelFieldID;
				//@JAPR
			}
			
			$filteredDimensionsInf=explode("] AND [",$aFilter);
			$tempCount = count($filteredDimensionsInf);
			
			//A los elementos del arreglo $filteredDimensionsInf le vamos a regresar los corchetes
			//de apertura y cerradura para no afectar el codigo posterior
			for($idx=0; $idx<$tempCount; $idx++)
			{
				if ($filteredDimensionsInf[$idx] != "") 
				{
				    if ($idx==0 && $tempCount>1) 
				  	{
						$filteredDimensionsInf[$idx] = $filteredDimensionsInf[$idx]."]";
				    }
				    else if ($idx>0 && $idx<$tempCount-1) 
				    {
						$filteredDimensionsInf[$idx] = "[".$filteredDimensionsInf[$idx]."]";
				    }
				    else if ($idx>0 && $idx==$tempCount-1) 
				    {
						$filteredDimensionsInf[$idx] = "[".$filteredDimensionsInf[$idx];
				    }
				}
			}
			
			$count=0;
			$dimensionIndex=0;
			
			for ($i=0; $i<count($filteredDimensionsInf);$i++)
			{
				if (trim($filteredDimensionsInf[$i])!="")
				{
					$dimFilteredValue=explode("] OR [",$filteredDimensionsInf[$i]);
					$tempCount = count($dimFilteredValue);

					//A los elementos del arreglo $dimFilteredValue le vamos a regresar los corchetes
					//de apertura y cerradura para no afectar el codigo posterior
					for($idx=0; $idx<$tempCount; $idx++)
					{
						if ($dimFilteredValue[$idx] != "") 
						{
						    if ($idx==0 && $tempCount>1) 
						  	{
								$dimFilteredValue[$idx] = $dimFilteredValue[$idx]."]";
						    }
						    else if ($idx>0 && $idx<$tempCount-1) 
						    {
								$dimFilteredValue[$idx] = "[".$dimFilteredValue[$idx]."]";
						    }
						    else if ($idx>0 && $idx==$tempCount-1) 
						    {
								$dimFilteredValue[$idx] = "[".$dimFilteredValue[$idx];
						    }
						}
					}
					
					for ($j=0;$j<count($dimFilteredValue);$j++)
					{
						$aDimFilteredValue=$dimFilteredValue[$j];
						
						$aTempDimFilteredValue=$dimFilteredValue[$j];
						
						$posicionToken = strpos($aTempDimFilteredValue, "[EXCEPT] ");
						
						if(!($posicionToken===false))
						{
							$aDimFilteredValue = str_replace("[EXCEPT] ", "", $aTempDimFilteredValue);
						}
						
						$aDimFilteredValueInfo=explode("].[",$aDimFilteredValue);
						$nom_fisicok_consecutivo_aux=$aDimFilteredValueInfo[1];
						$nom_fisicok_consecutivo=$nom_fisicok_consecutivo_aux;
						
						/*Obtener consecutivo*/
						$elements=explode("_",$nom_fisicok_consecutivo);
						$consecutivo_aux=$elements[count($elements)-1];
						$consecutivo=(int)$consecutivo_aux;
						$valor_aux=trim($aDimFilteredValueInfo[2]);
						$valor_aux=str_replace("]","",$valor_aux);
						$valor=$valor_aux;
						
						if($consecutivo!=$this->Collection[$dimensionIndex]->consecutivo)
						{
							$dimensionIndex++;
						}
						
						if(!($posicionToken===false))
						{
							$this->Collection[$dimensionIndex]->except=true;
						}
						
						$this->Collection[$dimensionIndex]->values[]=$valor;
					}
				}
			}
		}
	}
	
	function GenerateStrFilterByDimensionCollection()
	{
		$numFilteredDimensions=count($this->Collection);
		
		$strFilter="";
		$strLevels="";
		
		for($i=0;$i<$numFilteredDimensions;$i++)
		{
			$nom_tabla=$this->Collection[$i]->nom_tabla;
			$nom_fisicok=$this->Collection[$i]->nom_fisicok;
			$consecutivo=$this->Collection[$i]->consecutivo;
			$except = $this->Collection[$i]->except;
			
			$strLevels.="|".$consecutivo;
			
			if($consecutivo<10)
			{
				$strConsecutivo='0'.$consecutivo;
			}
			else 
			{
				$strConsecutivo = ''.$consecutivo.'';
			}
			
			$filteredValues=$this->Collection[$i]->values;
			$numValues=count($filteredValues);
			$strDimensionValues="";
			for($j=0;$j<$numValues;$j++)
			{
				if($strDimensionValues!="")
				{
					$strDimensionValues.=" OR ";	
				}
				$strDimensionValues.="[".$nom_tabla."]."."[".$nom_fisicok."_".$strConsecutivo."]."."[".$filteredValues[$j]."]";
			}
			
			if($strFilter!="")
			{
				if( $except === true )
				{
				
					$strFilter.=" AND [EXCEPT] ";
					//$strFilter.=" AND ";
				}
				else 
				{
					$strFilter.=" AND ";
				}
			}
			else 
			{
				if( $except === true )
				{
				
					$strFilter.="[EXCEPT] ";
				}
			}
			
			$strFilter.=$strDimensionValues;
		
		}
		
		$this->Filter=$strFilter;
		$this->NumFilteredDimensions=$numFilteredDimensions;
		$this->Levels=$strLevels;
	}
	
	function GetArrayConsecutivos()
	{
		$arrayConsecutivos = array();
		
		foreach ($this->Collection as $key => $anElement) 
		{
   			$arrayConsecutivos[$key] = $anElement->consecutivo;
		}
		
		return $arrayConsecutivos;
	}
	
	function GetEASFilter()
	{
		$numFilteredDimensions=count($this->Collection);

		if($this->CubeID<10)
		{
			$strCubeID='0'.$this->CubeID;
		}
		else 
		{
			$strCubeID=''.$this->CubeID.'';
		}
		
		$strFilter='';
		for($i=0;$i<$numFilteredDimensions;$i++)
		{
			$consecutivo=$this->Collection[$i]->consecutivo;
			$except = $this->Collection[$i]->except;
			
			if($consecutivo<10)
			{
				$strConsecutivo='0'.$consecutivo;
			}
			else 
			{
				$strConsecutivo = ''.$consecutivo.'';
			}
			
			$filteredValues=$this->Collection[$i]->values;
			$numValues=count($filteredValues);
			$strFilterDef='DIM_'.$strCubeID.'_'.$strConsecutivo.'_';
			$strDimensionValues='';
			for($j=0;$j<$numValues;$j++)
			{
				if($strDimensionValues!='')
				{
					$strDimensionValues.='_AWDim_';
				}
				$strDimensionValues.=$strFilterDef.$filteredValues[$j];
			}
			if($strDimensionValues!='')
			{
				if( $except === true )
				{
					$strDimensionValues.='_AWDim_[EXCEPT]';
				}
				
				if($strFilter!='')
				{
					$strFilter.='_AWSep_';
				}
				$strFilter.=$strDimensionValues;
			}
		}
		
		return $strFilter;
	}
	
	function getPartialFilterCollection($includedDims)
	{	
		$partialCollection=null;
		
		$numIncludedDims=count($includedDims);
		if($numIncludedDims<=0)
		{
			return $partialCollection;
		}
		
		//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
		$partialCollection=BITAMFilteredSVCollection::NewInstance($this->Repository,"","",$this->CubeID,$this->CatalogID);
		//@JAPR
		$numFilterDims=count($this->Collection);
		$index=0;
		for($i=0;$i<$numFilterDims;$i++)
		{
			if(in_array($this->Collection[$i]->consecutivo,$includedDims))
			{
				$partialCollection->Collection[$index]=$this->Collection[$i];
				$index++;
			}
		}
		
		return $partialCollection;
	}
	
	/*
	Realiza una unión entre el filtro actual de la colección y el especificado como parámetro.
	En la unión, aquellas dimensiones que se encuentren filtrados juntarán sus valores filtrados
	para forzar un solo filtro, pero en el caso de que alguna dimensión se encuentre solo en
	una de las colecciones filtradas, desaparecerá del filtro final ya que representa el total
	de los elementos de la dimensión
	*/
	function mergeWithFilterCollection($aFilterCollectionToMerge)
	{
		$ResultFilterCollection = BITAMFilteredSVCollection::NewInstance($this->Repository);
		
		//Si la colección del parámetro o la instancia no tienen elementos, simplemente regresa una colección vacia
		//pues en ese caso todas las dimensiones utilizan todos sus valores
		if (count($aFilterCollectionToMerge->Collection) == 0 ||count($this->Collection) == 0)
		{
			return $ResultFilterCollection;
		}
		
		//Por lo menos hay un elemento filtrado en la colección del parámetro y en la instancia, hay que ver si se puede unir 
		//dicho filtro o regresar un filtro vacio si no
		$numElements = 0;
		$numFilterElements = count($this->Collection);
		$numFiltersToMerge = count($aFilterCollectionToMerge->Collection);
		for ($aFilterIdx = 0; $aFilterIdx < $numFilterElements; $aFilterIdx++)
		{
			$blnFilteredDimension = false;
			$aDimensionID = $this->Collection[$aFilterIdx]->consecutivo;
			//Buscamos esta misma dimensión en la colección de filtros del parámetro
			for($i=0; $i < $numFiltersToMerge; $i++)
			{
				//Verifica que el Except sea del mismo tipo, si no lo es no se puede hacer el Merge
				if (($aDimensionID == $aFilterCollectionToMerge->Collection[$i]->consecutivo) &&
					($this->Collection[$aFilterIdx]->except == $aFilterCollectionToMerge->Collection[$i]->except))
				{
					//Se encontró la dimensión, así que hay que unir ambos filtros
					$blnFilteredDimension = true;
					$ResultFilterCollection->Collection[$numElements] = clone( $this->Collection[$aFilterIdx] );
					//Hacemos le Merge de la colección de valores
					$arrMergedValues = array_values(array_unique(array_merge($this->Collection[$aFilterIdx]->values,$aFilterCollectionToMerge->Collection[$i]->values)));
					$ResultFilterCollection->Collection[$numElements]->values = $arrMergedValues;
					$numElements++;
					break;
				}
			}
		}
		
		return $ResultFilterCollection;
	}
}
?>