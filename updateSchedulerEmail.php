<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Obtener el CatalogID
$schedulerID = 0;
if (array_key_exists("SchedulerID", $_POST))
{
	$schedulerID = (int)$_POST["SchedulerID"];
}

//Obtenemos el catalogo, atributo y el surveyid
$sql = "SELECT CatalogID, AttributeID, SurveyID FROM SI_SV_SurveyScheduler WHERE SchedulerID = ".$schedulerID;

$aRS = $theRepository->DataADOConnection->Execute($sql);
if ($aRS === false)
{
    die(translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}

//Si se obtuvieron los datos correctamente del scheduler procedemos con lo siguiente
if (!$aRS->EOF) 
{
    $catalogID = (int)$aRS->fields["catalogid"];
    $attributeID = (int)$aRS->fields["attributeid"];
    $surveyID = (int)$aRS->fields["surveyid"];
    
	//Obtenemos el parentID del catalogo para poder establecer el nombre de la tabla del catalogo
	//y el parentID del atributo para hacer referencia al campo Email
	$sql = "SELECT A.ParentID AS ParentID, B.ParentID AS AttribParentID
			FROM SI_SV_Catalog A, SI_SV_CatalogMember B
			WHERE A.CatalogID = B.CatalogID AND B.CatalogID = ".$catalogID." 
			AND B.MemberID = ".$attributeID;
	
	$aRSParent = $theRepository->DataADOConnection->Execute($sql);
	if ($aRSParent === false)
	{
	    die(translate("Error accessing")." SI_SV_Catalog, SI_SV_CatalogMember ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	//Al obtener los datos correctamente estaremos tratando de insertar los correos nuevos
	//y se eliminarian los correos eliminados
	if (!$aRSParent->EOF) 
	{
	    $parentID = (int)$aRSParent->fields["parentid"];
	    $attribParentID = (int)$aRSParent->fields["attribparentid"];

		//Ahora obtenemos los correos que no se encuentran dados de alta en el scheduler y 
		//se insertan en la tabla SI_SV_SchedulerEmail
		$sql = "INSERT INTO SI_SV_SchedulerEmail (SchedulerID, Email) 
				SELECT ".$schedulerID.", A.DSC_".$attribParentID." FROM RIDIM_".$parentID." A WHERE NOT EXISTS
				(
		  			SELECT B.Email FROM SI_SV_SchedulerEmail B
		  			WHERE SchedulerID = ".$schedulerID." AND B.Email = A.DSC_".$attribParentID."
				) AND A.RIDIM_".$parentID."KEY <> 1;";
		
		if ($theRepository->DataADOConnection->Execute($sql) === false)
		{
			die(translate("Error accessing")." SI_SV_SchedulerEmail ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Ahora se eliminan los correos que estan en la tabla SI_SV_SchedulerEmail que no estan en el catalogo
		$sql = "DELETE A FROM SI_SV_SchedulerEmail A WHERE A.SchedulerID = ".$schedulerID." 
				AND NOT EXISTS
				(
		  			SELECT B.DSC_".$attribParentID." FROM RIDIM_".$parentID." B
		  			WHERE B.DSC_".$attribParentID." = A.Email
		  			AND B.RIDIM_".$parentID."KEY <> 1
				)";
		
		if ($theRepository->DataADOConnection->Execute($sql) === false)
		{
			die(translate("Error accessing")." SI_SV_SchedulerEmail ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		require_once("survey.inc.php");
		
		$surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $surveyID);
		$emailDimID = $surveyInstance->EmailDimID;
		
		//Obtenemos los correos que no se encuentran dados de alta en la dimension Email 
		//y se procede a insertarlos en la tabla de la dimension Email
		$sql = "INSERT INTO RIDIM_".$emailDimID." (DSC_".$emailDimID.") 
				SELECT A.DSC_".$attribParentID." FROM RIDIM_".$parentID." A WHERE NOT EXISTS 
				(
		  			SELECT B.DSC_".$emailDimID." FROM RIDIM_".$emailDimID." B 
		  			WHERE B.DSC_".$emailDimID." = A.DSC_".$attribParentID." 
				) AND A.RIDIM_".$parentID."KEY <> 1";
		
		if ($theRepository->DataADOConnection->Execute($sql) === false)
		{
			die(translate("Error accessing")." SI_SV_SchedulerEmail ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
}
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
<body>
<script language="JavaScript">
	window.parent.frames["body"].showSavingMessage(0);
</script>
</body>
</html>