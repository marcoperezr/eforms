<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("appuser.inc.php");
require_once("survey.inc.php");

class BITAMSurveyMenu extends BITAMObject
{
	public $MenuID;
	public $MenuName;
	public $SurveyIDs;
	public $VersionNum;
	public $MenuImageText;
	//@JAPR 2016-02-04: Agregados los LayOuts personalizados por forma (#FSMUZC)
	public $Width;					//El tamaño horizontal del elemento en la lista de formas del App definida en pixeles
	public $Height;					//El tamaño vertical del elemento en la lista de formas del App definida en pixeles
	public $TextPosition;			//El tipo de alineación del texto respecto a la imagen de la forma
	public $TextSize;				//Dependiendo del tipo de alineación, es el ancho/alto del área del texto en pixeles o porcentaje (la imagen usará el restante del área total de la forma definida en Width) definida en pixeles
	public $CustomLayout;			//El template HTML a utilizar como contenido de cada item en la lista de formas del App en caso de haberse personalizado. Si está vacío entonces se generará la vista default según otras configuraciones
	//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
	public $AutoFitImage;			//Indica si la imagen de la forma en el menú de formas del App se va a autoajustar al tamaño del parea especificada sin mantener la escala original
	
	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		$this->MenuID = -1;
		$this->MenuName= "";
		$this->VersionNum = 0;
		$this->MenuImageText= "";
		$this->SurveyIDs = array();
		//@JAPR 2016-02-04: Agregados los LayOuts personalizados por forma (#FSMUZC)
		$this->Width = 0;
		$this->Height = 0;
		$this->TextPosition = svtxpDefault;
		$this->TextSize = '';
		$this->CustomLayout = '';
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		$this->AutoFitImage = 0;
	}
	
	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstance($aRepository)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository);
//		return new BITAMSurveyMenu($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aMenuID)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		
		$anInstance = null;
		
		if (((int)  $aMenuID) < 0)
		{
			return $anInstance;
		}
		
		//$anInstance =& BITAMGlobalFormsInstance::GetCatalogInstanceWithID($aMenuID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
		$strAdditionalFields = "";
		if (getMDVersion() >= esvMenuImage) {
			$strAdditionalFields .= ', t1.MenuImageText';
		}
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$strAdditionalFields .= ', t1.Width, t1.Height, t1.TextPosition, t1.TextSize, t1.CustomLayout';
		}
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		if (getMDVersion() >= esvAutoFitFormsImage) {
			$strAdditionalFields .= ', t1.AutoFitImage';
		}
		//@JAPR
		$sql = "SELECT t1.MenuID, t1.MenuName, t1.VersionNum $strAdditionalFields FROM SI_SV_Menu t1 WHERE t1.MenuID = ".((int) $aMenuID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Menu ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		
		$anInstance = $strCalledClass::NewInstance($aRepository);
		$anInstance->MenuID = (int) $aRS->fields["menuid"];
		$anInstance->MenuName = @$aRS->fields["menuname"];
		$anInstance->VersionNum = (int) @$aRS->fields["versionnum"];
		$anInstance->readSurveyIDs();
		if (getMDVersion() >= esvMenuImage) {
			$anInstance->MenuImageText = @$aRS->fields["menuimagetext"];
		}
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		$anInstance->Width = (int) @$aRS->fields["width"];
		$anInstance->Height = (int) @$aRS->fields["height"];
		$anInstance->TextPosition = (int) @$aRS->fields["textposition"];
		$anInstance->TextSize = (string) @$aRS->fields["textsize"];
		$anInstance->CustomLayout = (string) @$aRS->fields["customlayout"];
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		$anInstance->AutoFitImage = (int) @$aRS->fields["autofitimage"];
		
		return $anInstance;
	}
	
	function readSurveyIDs()
	{
		$sql = "SELECT SurveyID FROM SI_SV_SurveyMenu WHERE MenuID = ".$this->MenuID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die(translate("Error accessing")." SI_SV_SurveyMenu ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		while(!$aRS->EOF)
		{
			$aSurveyID = (int)$aRS->fields["surveyid"];
			$this->SurveyIDs[] = $aSurveyID;
			$aRS->MoveNext();
		}
	}
	
	function getSurveys($aRepository) {
		return BITAMSurvey::getSurveys($this->Repository, true);
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		if (array_key_exists("MenuID", $aHTTPRequest->POST))
		{
			$aMenuID = $aHTTPRequest->POST["MenuID"];
			
			if (is_array($aMenuID))
			{
				
				$aCollection = BITAMSurveyMenuCollection::NewInstance($aRepository, $aMenuID);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aMenuID);
				if (is_null($anInstance))
				{
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}

				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = $strCalledClass::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("MenuID", $aHTTPRequest->GET))
		{
			$aMenuID = $aHTTPRequest->GET["MenuID"];
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository,(int) $aMenuID);
			if (is_null($anInstance))
			{
				$anInstance = $strCalledClass::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = $strCalledClass::NewInstance($aRepository);
		}
		$anInstance->generateForm();
		die();
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("MenuID", $anArray))
		{
			$this->MenuID = (int) $anArray["MenuID"];
		}
		
	 	if (array_key_exists("MenuName", $anArray))
		{
			$this->MenuName = rtrim($anArray["MenuName"]);
		}
		
		if (array_key_exists("SurveyIDs", $anArray))
		{
			$this->SurveyIDs = $anArray["SurveyIDs"];
		}
		
		if (getMDVersion() >= esvMenuImage) {
			if (array_key_exists("MenuImageText", $anArray)) {
				$this->MenuImageText = $anArray["MenuImageText"];
			}
		}		
		
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (array_key_exists("Width", $anArray))
		{
			$this->Width = (int) @$anArray["Width"];
		}
		if (array_key_exists("Height", $anArray))
		{
			$this->Height = (int) @$anArray["Height"];
		}
		if (array_key_exists("TextPosition", $anArray))
		{
			$this->TextPosition = (int) @$anArray["TextPosition"];
		}
		if (array_key_exists("TextSize", $anArray))
		{
			$this->TextSize = trim((string) $anArray["TextSize"]);
		}
		if (array_key_exists("CustomLayout", $anArray))
		{
			$this->CustomLayout = (string) $anArray["CustomLayout"];
		}
		
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		if (array_key_exists("AutoFitImage", $anArray))
		{
			$this->AutoFitImage = (int) $anArray["AutoFitImage"];
		}
		//@JAPR
		
		return $this;
	}
	
	//@LROUX 2012-11-23: Agregado para ser procesado a una subclase customizada (PCP API).
	/*function updateFromRS($aRS)
	{
		$this->MenuID = (int) $aRS->fields["menuid"];
		$this->MenuName= $aRS->fields["menuname"];
		//BITAMGlobalFormsInstance::AddMenuInstanceWithID($this->MenuID, $this);
		return $this;
	}*/
	
	function UpdateSurveyIDs () {
		$sql = "DELETE FROM SI_SV_SurveyMenu WHERE MenuID = ".$this->MenuID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_SurveyMenu ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		foreach($this->SurveyIDs as $aSurveyID) {
			$sqlIns = "INSERT INTO SI_SV_SurveyMenu (MenuID, SurveyID, VersionNum) VALUES (".$this->MenuID.", ".$aSurveyID.", 1)";
			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				die( translate("Error accessing")." SI_SV_SurveyMenu ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
		}
	}
	
	function save()
	{
		global $gblShowErrorSurvey;
	 	if ($this->isNewObject())
		{
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(MenuID)", "0")." + 1 AS MenuID".
						" FROM SI_SV_Menu";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Menu ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Menu ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$this->MenuID = (int) $aRS->fields["menuid"];
			$strAdditionalFields = "";
			$strAdditionalValues = "";
			if (getMDVersion() >= esvMenuImage) {
				$strAdditionalFields .= ", MenuImageText";
				$strAdditionalValues .= (", ".$this->Repository->DataADOConnection->Quote($this->MenuImageText));
			}
			//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
			if (getMDVersion() >= esvFormsLayouts) {
				$strAdditionalFields .= ', Width, Height, TextPosition, TextSize, CustomLayout';
				$strAdditionalValues .= ', '.$this->Width.', '.$this->Height.', '.$this->TextPosition.', '.
					$this->Repository->DataADOConnection->Quote($this->TextSize).', '.
					$this->Repository->DataADOConnection->Quote($this->CustomLayout);
			}
			//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
			if (getMDVersion() >= esvAutoFitFormsImage) {
				$strAdditionalFields .= ', AutoFitImage';
				$strAdditionalValues .= ', '.$this->AutoFitImage;
			}
			//@JAPR
			$sql = "INSERT INTO SI_SV_Menu (".
						" MenuID, MenuName, VersionNum".
			            $strAdditionalFields.
			            ") VALUES (".
			            $this->MenuID.
						",".$this->Repository->DataADOConnection->Quote($this->MenuName).
						",1".
						$strAdditionalValues.
						")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Menu ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Menu ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		} else {
			$strAdditionalValues = "";
			if (getMDVersion() >= esvMenuImage) {
				$strAdditionalValues .= (", MenuImageText = ".$this->Repository->DataADOConnection->Quote($this->MenuImageText));
			}
			//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
			if (getMDVersion() >= esvFormsLayouts) {
				$strAdditionalValues .= ', Width = '.$this->Width.
					', Height = '.$this->Height.
					', TextPosition = '.$this->TextPosition.
					', TextSize = '.$this->Repository->DataADOConnection->Quote($this->TextSize).
					', CustomLayout = '.$this->Repository->DataADOConnection->Quote($this->CustomLayout);
			}
			//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
			if (getMDVersion() >= esvAutoFitFormsImage) {
				$strAdditionalValues .= ', AutoFitImage = '.$this->AutoFitImage;
			}
			//@JAPR
			$sql = "UPDATE SI_SV_Menu SET MenuName = ".$this->Repository->DataADOConnection->Quote($this->MenuName).
				", VersionNum = ".$this->Repository->ADOConnection->IfNull("VersionNum", "0")." +1".
				$strAdditionalValues." WHERE MenuID = ".$this->MenuID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false) {
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Menu ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Menu ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		$this->UpdateSurveyIDs();
		return $this;
	}

	function remove() {
		$sql = "DELETE FROM SI_SV_SurveyMenu WHERE MenuID IN (
			SELECT MenuID FROM SI_SV_Menu WHERE MenuID = {$this->MenuID})";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		$sql = "DELETE FROM SI_SV_Menu WHERE MenuID IN ({$this->MenuID})";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->MenuID < 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Menu");
		}
		else
		{
			return $this->MenuName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SurveyMenu";
		}
		else
		{
			return "BITAM_PAGE=SurveyMenu&MenuID=".$this->MenuID;
		}
	}

	function get_Parent()
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($this->Repository);
//		return BITAMSurveyMenuCollection::NewInstance($this->Repository,null);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function generateBeforeFormCode($aUser)
 	{	
?>
 	<script language="JavaScript">
 		function verifyFieldsAndSave(target)
 		{
 			var strAlert = '';
 			
 			if(Trim(BITAMSurveyMenu_SaveForm.MenuName.value)=='')
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Name"))?>';
 			}
 			
 			//En esta parte se verifica si el AttributeName ha sido modificado por valores validos
			var attributeNameOld = '<?=$this->MenuName?>';
			var menuID = <?=$this->MenuID?>;
			var attributeName = BITAMSurveyMenu_SaveForm.MenuName.value;

			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('<?=translate('XML Object not supported')?>');
				return(new Array);
			}

			xmlObj.open('POST', 'verifyAttributeName.php', false);
			xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
	 		xmlObj.send('ObjectID='+menuID+'&AttributeName='+encodeURIComponent(attributeName)+'&AttributeNameOld='+encodeURIComponent(attributeNameOld));

	 		if(xmlObj.status == 404)
			{
				alert('<?=translate('Unable to launch validation process')?>');
				return false;
			}

	 		strResponseData = Trim(unescape(xmlObj.responseText));

	 		arrResponseData = strResponseData.split('<SVSEP>');
	 		if(arrResponseData.length == 0)
	 		{
	 			alert('<?=translate('Error found during data validation')?>');
				return false;
	 		}

	 		if(arrResponseData[0]!="OK")
	 		{
				strErrMsg = arrResponseData[0];
				if(arrResponseData.length > 1)
				{
					strErrMsg = arrResponseData[1];
				}
				
  				strAlert += '\n- ' + '<?=translate('Error found during data validation')?>: ' + strErrMsg;
	 		}
			
			var itemsArray=document.getElementsByName("SurveyIDs[]");
			var numItems = itemsArray.length;
			var itemsSelected = false;
			var i;
			
			for (i=0; i<numItems; i++)
			{
				if(itemsArray[i].checked)
				{
					itemsSelected = true;
					break;
				}
			}
			
			if(!itemsSelected)
  			{
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Surveys"))?>';
  			}

 			if(Trim(strAlert)!='')
 			{
 				alert(strAlert);
 			}
 			else
 			{
 				BITAMSurveyMenu_Ok(target);
 			}
 		}

 	</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
?>
	<script language="JavaScript">
		
		objBody= document.getElementsByTagName("body");
		objBody[0].onkeypress= function onc(event)
		{
			var event = (window.event) ? window.event : event;
			//if (window.event.keyCode ==13)
			if (event.keyCode ==13)
			return false ;
		}
		objOkSelfButton = document.getElementById("BITAMSurveyMenu_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMSurveyMenu_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMSurveyMenu_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
	</script>
<?
	}
	
	function generateInsideFormCode($aUser)
	{
		include("formattedTextMenu.php");
	}
	
	function insideEdit()
	{
		if (getMDVersion() >= esvMenuImage) {
		?>
			var qimage = document.getElementById("menuImageTextArea");
			var qimagedisable = document.getElementById("disableMenuImage");
			if (qimagedisable) {
				qimagedisable.style.zIndex = 0;
				qimagedisable.style.display = "none";
			}
		<?
		}
	}
	
	function insideCancel()
	{
		if (getMDVersion() >= esvMenuImage) {
		?>
			var qimage = document.getElementById("menuImageTextArea");
			var qimagedisable = document.getElementById("disableMenuImage");
			if (qimagedisable) {
				qimagedisable.style.zIndex = 100;
				qimagedisable.style.display = "block";
			}
		<?
		}
	}
	
	function get_FormIDFieldName()
	{
		return 'MenuID';
	}

	function get_FormFieldName() {
		return 'MenuName';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		$myFields = array();
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MenuName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$surveyField = BITAMFormField::NewFormField();
		$surveyField->Name = "SurveyIDs";
		$surveyField->Title = translate("Surveys");
		$surveyField->Type = "Array";
		$surveyField->Options = BITAMSurveyMenu::getSurveys($this->Repository);
		$myFields[$surveyField->Name] = $surveyField;		
		
		return $myFields;
	}
	
	function canEdit($aUser)
	{
		return true;
	}
	
	function canRemove($aUser)
	{
		return true;
	}

	function get_Children($aUser)
	{
		$myChildren = array();
		return $myChildren;
	}
	
	function getJSonDefinition($aRepository = null) {
		//@JAPR 2015-05-26: Rediseñado el Admin con DHTMLX
		global $gbDesignMode;
		global $appVersion;
		
		if ($aRepository == null && isset($this->Repository))
			$aRepository = $this->Repository;
		
		$arrDef = array();
		$arrDef['id'] = $this->MenuID;
		$arrDef['name'] = $this->MenuName;
		if (getMDVersion() >= esvMenuImage) {
			$strDisplayImage = trim($this->MenuImageText);
			if ($strDisplayImage != '') {
				//@JAPR 2015-09-15: Corregido un bug, al App se debe enviar la imagen como tag IMG pero en diseño debe ser directa como ruta relativa
				if ($gbDesignMode) {
					$arrDef['displayImageDes'] = $strDisplayImage;
				}
				//@JAPR
				
				$strDisplayImage = trim(str_replace(array('<p>', '</p>'), '', $strDisplayImage));
				global $blnWebMode;
				//@JAPR 2014-10-09: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
				if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
					$strDisplayImage = (string) @BITAMSection::convertImagesToBase64($strDisplayImage);	
				}
				elseif (!$blnWebMode) {
					//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
					//@JAPR 2015-08-06: Agregado el parámetro $bUseImgTag para indicar si se desea o no como tag <Img>
					$strDisplayImage = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $strDisplayImage, true, false);
				}
				else {
					//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
					//JAPR 2015-08-04: Modificada para utilizar DHTMLX
					if ($appVersion < esveFormsDHTMLX) {
						$strDisplayImage = '<img src="'.$strDisplayImage.'" />';
					}
					//@JAPR
				}
			}
			//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
			//@JAPR 2015-09-15: Corregido un bug, al App se debe enviar la imagen como tag IMG pero en diseño debe ser directa como ruta relativa
			$arrDef['displayImage'] = $strDisplayImage;
			//@JAPR
		}
		
		$arrDef['versionNum'] = $this->VersionNum;
		$arrDef['surveys'] = array();
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$arrDef['width'] = $this->Width;
			$arrDef['height'] = $this->Height;
			$arrDef['textPos'] = $this->TextPosition;
			$arrDef['textSize'] = trim($this->TextSize);
			$arrDef['customLayout'] = $this->CustomLayout;
		}
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		if (getMDVersion() >= esvAutoFitFormsImage) {
			$arrDef['autoFitImage'] = $this->AutoFitImage;
		}
		
		//@JAPR 2015-09-15: Corregido un bug, se había dañado la definición esperada por el App, así que ahora genera propiedades diferentes para diseño
		if ($gbDesignMode) {
			$arrDef['FormsAssct'] = array();
			$arrDef['formsIDs'] = array();
		}
		foreach($this->SurveyIDs as $aSurveyID) {
			$objSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
			//@JAPR 2014-07-31: Validado que si se queda una referencia inválida no cause problemas al descargar las definciones
			if (!is_null($objSurvey)) {
				//@JAPR 2015-09-15: Corregido un bug, se había dañado la definición esperada por el App, así que ahora genera propiedades diferentes para diseño
				if ($gbDesignMode) {
					$arrDef['FormsAssct'][((int) $objSurvey->SurveyID)] = $objSurvey->SurveyName;
					$arrDef['formsIDs'][] = (int) $objSurvey->SurveyID;
				}
				$arrDef['surveys'][((int) $objSurvey->SurveyID)] = $objSurvey->SurveyName;
			}
			//@JAPR
		}

		//@JAPR 2015-10-09: Corregido el despliegue de las formas de menus, estaba mostrando las formas fuera de su menu también porque los menús no indicaban las formas que
		//contenían sino hasta que eran cargados (#XW13KY)
		$arrDef['surveyIDs'] = array();
		if (count($arrDef['surveys'])) {
			$arrDef['surveyIDs'] = array_keys($arrDef['surveys']);
		}
		//@JAPR
		return $arrDef;
	}

}

class BITAMSurveyMenuCollection extends BITAMCollection
{
  function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository, $anArrayOfMenuIDs = null)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository);
		
		//GCRUZ 2015-08-25. Cambiar instancia para v5
//		$anInstance = new BITAMSurveyMenuCollection($aRepository);
		
		$where = "";
		if (!is_null($anArrayOfMenuIDs))
		{
			switch (count($anArrayOfMenuIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE t1.MenuID = ".((int) $anArrayOfMenuIDs[0]);
					break;
				default:
					foreach ($anArrayOfMenuIDs as $aMenuID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $aMenuID; 
					}
					if ($where != "")
					{
						$where = "WHERE t1.MenuID IN (".$where.")";
					}
					break;
			}
		}
		
		$strAdditionalFields = "";
		if (getMDVersion() >= esvMenuImage) {
			$strAdditionalFields .= ', t1.MenuImageText';
		}		
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$strAdditionalFields .= ', t1.Width, t1.Height, t1.TextPosition, t1.TextSize, t1.CustomLayout';
		}
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		if (getMDVersion() >= esvAutoFitFormsImage) {
			$strAdditionalFields .= ', t1.AutoFitImage';
		}
		//@JAPR
		//Conchita 2015-04-14 se agrega versionNum ya que no estaba
		$sql = "SELECT t1.MenuID, t1.MenuName, t1.VersionNum $strAdditionalFields FROM SI_SV_Menu t1 ".$where." ORDER BY t1.MenuName";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Menu ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Menu ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
/*
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMSurveyMenu::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
*/
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		while (!$aRS->EOF) {
			//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
			$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
//		return BITAMSurveyMenuCollection::NewInstance($aRepository, null);
		//@JAPR 2015-04-25: Rediseñado el Admin con DHTMLX
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($aRepository, null);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Menus");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=SurveyMenuCollection";
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_SECTION=SurveyMenu";
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=SurveyMenu";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'MenuID';
	}

	function get_FormFieldName() {
		return 'MenuName';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MenuName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}

	function canAdd($aUser)
	{
		return true;	
	}
	
	function addButtons($aUser)
	{
	}

	function generateBeforeFormCode($aUser)
 	{
	}

	function generateAfterFormCode($aUser)
	{
	}
}
?>