<?
/*############################################
Sample :
$thumb=new Thumbnail("./shiegege.jpg");		// generate image_file, set filename to resize
$thumb->size_width(100);					// set width for thumbnail, or
$thumb->size_height(300);					// set height for thumbnail, or
$thumb->size_auto(200);						// set the biggest width or height for thumbnail
$thumb->jpeg_quality(75);					// [OPTIONAL] set quality for jpeg only (0 - 100) (worst - best), default = 75
$thumb->show();								// show your thumbnail
$thumb->save("./image.jpg");				// save your thumbnail to file
############################################*/

class Thumbnail
{
	var $img;

	function Thumbnail($imgfile)
	{
		//detect image format
		$this->img["format"]=ereg_replace(".*\.(.*)$","\\1",$imgfile);
		$this->img["format"]=strtoupper($this->img["format"]);
		
		if ($this->img["format"]=="JPG" || $this->img["format"]=="JPEG") 
		{
			//JPEG
			$this->img["format"]="JPEG";
			$this->img["src"] = @ImageCreateFromJPEG ($imgfile);
		} 
		elseif ($this->img["format"]=="PNG") 
		{
			//PNG
			$this->img["format"]="PNG";
			$this->img["src"] = @ImageCreateFromPNG ($imgfile);
		}
		elseif ($this->img["format"]=="GIF") 
		{
			//GIF
			$this->img["format"]="GIF";
			$this->img["src"] = @ImageCreateFromGIF ($imgfile);
		} 
		elseif ($this->img["format"]=="WBMP") 
		{
			//WBMP
			$this->img["format"]="WBMP";
			$this->img["src"] = @ImageCreateFromWBMP ($imgfile);
		}
		else 
		{
			//DEFAULT
			//PNG
			$this->img["format"]="PNG";
			$this->img["src"] = @ImageCreateFromPNG ($imgfile);
		}
		
		@$this->img["lebar"] = @imagesx($this->img["src"]);
		@$this->img["tinggi"] = @imagesy($this->img["src"]);
		
		//default quality jpeg
		$this->img["quality"]=75;
	}
	
	function isCorrupted() {
		if(!$this->img["src"]) {
			return true;
		}
		return false;
	}

	function size_height($size=100)
	{
		//height
    	$this->img["tinggi_thumb"]=$size;
    	@$this->img["lebar_thumb"] = ($this->img["tinggi_thumb"]/$this->img["tinggi"])*$this->img["lebar"];
	}

	function size_width($size=100)
	{
		//width
		$this->img["lebar_thumb"]=$size;
    	@$this->img["tinggi_thumb"] = ($this->img["lebar_thumb"]/$this->img["lebar"])*$this->img["tinggi"];
	}

	function size_auto($size=100)
	{
		//size
		if ($this->img["lebar"]>=$this->img["tinggi"]) 
		{
    		$this->img["lebar_thumb"]=$size;
    		@$this->img["tinggi_thumb"] = ($this->img["lebar_thumb"]/$this->img["lebar"])*$this->img["tinggi"];
		}
		else 
		{
	    	$this->img["tinggi_thumb"]=$size;
    		@$this->img["lebar_thumb"] = ($this->img["tinggi_thumb"]/$this->img["tinggi"])*$this->img["lebar"];
 		}
	}
	
	function max_size_auto($size=200)
	{
		//size
		if ($this->img["lebar"]>=$this->img["tinggi"] && $this->img["lebar"]>$size) 
		{
    		$this->img["lebar_thumb"]=$size;
    		@$this->img["tinggi_thumb"] = ($this->img["lebar_thumb"]/$this->img["lebar"])*$this->img["tinggi"];
		}
		else if($this->img["tinggi"]>$this->img["lebar"] && $this->img["tinggi"]>$size)
		{
	    	$this->img["tinggi_thumb"]=$size;
    		@$this->img["lebar_thumb"] = ($this->img["tinggi_thumb"]/$this->img["tinggi"])*$this->img["lebar"];
 		}
 		else 
 		{
 			$this->img["lebar_thumb"]=$this->img["lebar"];
 			$this->img["tinggi_thumb"]=$this->img["tinggi"];
 		}
	}

	function jpeg_quality($quality=75)
	{
		//jpeg quality
		$this->img["quality"]=$quality;
	}

	function show()
	{
		//show thumb
		@Header("Content-Type: image/".$this->img["format"]);

		/*change ImageCreateTrueColor to ImageCreate if your GD not supported ImageCreateTrueColor function*/
		$this->img["des"] = ImageCreateTrueColor($this->img["lebar_thumb"],$this->img["tinggi_thumb"]);
    	@imagecopyresized ($this->img["des"], $this->img["src"], 0, 0, 0, 0, $this->img["lebar_thumb"], $this->img["tinggi_thumb"], $this->img["lebar"], $this->img["tinggi"]);

		if ($this->img["format"]=="JPG" || $this->img["format"]=="JPEG") 
		{
			//JPEG
			imageJPEG($this->img["des"],"",$this->img["quality"]);
		} 
		elseif ($this->img["format"]=="PNG") 
		{
			//PNG
			imagePNG($this->img["des"]);
		} 
		elseif ($this->img["format"]=="GIF") 
		{
			//GIF
			imageGIF($this->img["des"]);
		} 
		elseif ($this->img["format"]=="WBMP") 
		{
			//WBMP
			imageWBMP($this->img["des"]);
		}
	}

	function save($save="")
	{
		//save thumb
		if (empty($save)) 
		{
			$save=strtolower("./thumb.".$this->img["format"]);
		}
		
		if(!$this->img["src"]) {
		
		} else {

			/*change ImageCreateTrueColor to ImageCreate if your GD not supported ImageCreateTrueColor function*/
			$this->img["des"] = ImageCreateTrueColor($this->img["lebar_thumb"],$this->img["tinggi_thumb"]);
			
			if($this->img["format"]=="PNG" || $this->img["format"]=="GIF")
			{
				$this->setTransparency();
			}
			
			@imagecopyresized ($this->img["des"], $this->img["src"], 0, 0, 0, 0, $this->img["lebar_thumb"], $this->img["tinggi_thumb"], $this->img["lebar"], $this->img["tinggi"]);
			//@imagecopyresampled($this->img["des"], $this->img["src"], 0, 0, 0, 0, $this->img["lebar_thumb"], $this->img["tinggi_thumb"], $this->img["lebar"], $this->img["tinggi"]);

			if ($this->img["format"]=="JPG" || $this->img["format"]=="JPEG") 
			{
				//JPEG
				imageJPEG($this->img["des"],"$save",$this->img["quality"]);
			}
			elseif ($this->img["format"]=="PNG") 
			{
				//PNG
				imagePNG($this->img["des"],"$save");
			}
			elseif ($this->img["format"]=="GIF") 
			{
				//GIF
				imageGIF($this->img["des"],"$save");
			}
			elseif ($this->img["format"]=="WBMP") 
			{
				//WBMP
				imageWBMP($this->img["des"],"$save");
			}
		}
	}

	function setTransparency() 
    { 
		$transparencyIndex = imagecolortransparent($this->img["src"]); 
		$transparencyColor = array('red' => 255, 'green' => 255, 'blue' => 255); 
		
		if ($transparencyIndex >= 0)
		{ 
			//@JAPR 2012-07-24: Agregada una validación para casos donde la imagen no tiene transparencia
			//ya que generaban un Warning que provocaba un error en los móviles
			try {
				$transparencyColor = @imagecolorsforindex($this->img["src"], $transparencyIndex);    
			} catch (Exception $e) {
				//Ignorará el error (ocurría con imagenes .gif)
			}
			//@JAPR
		} 
		
		//@JAPR 2012-07-24: Agregada una validación para casos donde la imagen no tiene transparencia
		//ya que generaban un Warning que provocaba un error en los móviles
		if ($transparencyColor !== false)
		{
			$transparencyIndex = imagecolorallocate($this->img["des"], $transparencyColor['red'], $transparencyColor['green'], $transparencyColor['blue']); 
			imagefill($this->img["des"], 0, 0, $transparencyIndex); 
			imagecolortransparent($this->img["des"], $transparencyIndex); 
		}
		//@JAPR
    }
}
?>