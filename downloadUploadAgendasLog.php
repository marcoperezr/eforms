<?php
	$strLocalFileName = '';
	if (array_key_exists('filename', $_POST) && trim($_POST['filename']) != '')
	{
		$strLocalFileName = $_POST['filename'];
	}
	elseif (array_key_exists('filename', $_GET) && trim($_GET['filename']) != '')
	{
		$strLocalFileName = $_GET['filename'];
	}
	
	if (trim($strLocalFileName != ''))
	{
		$sfile = $strLocalFileName;
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$spathfile = GeteFormsLogPath().$sfile;
		//@JAPR
		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=\"".$sfile."\";");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($spathfile));
		readfile($spathfile);
		exit();
	}
	
?>
<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script language="javascript">
		  function pageloaded()
		  {	
			window.close();
		  }
		</script>
	</head>
<body onload="pageloaded()">
</body>
</html>
<?    

?>