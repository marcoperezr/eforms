<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$ObjectCatSel = -1;
if (array_key_exists("CatSel", $_GET))
{
	$ObjectCatSel = $_GET["CatSel"];
}

$strMembers = "";
$sql= "SELECT TableName from si_sv_catalog where CatalogID = ".$ObjectCatSel;
$x = $theRepository->DataADOConnection->Execute($sql);
if ($x === false)
{
	
	die( translate("Error accessing"));
}
else {	
	$tablename=  $x->fields["tablename"]; //nombre de tabla q se utilizara en la ultima consulta
	
}
			
$sql="SELECT ParentID from si_sv_catalogmember where CatalogID= ".$ObjectCatSel;
$a = $theRepository->DataADOConnection->Execute($sql);
if ($a === false)
{
	
	die( translate("Error accessing"));
}
else {	
	$parentID=  $a->fields["parentid"]; //numero de parentID que se utilizara en la ultima consulta
	
}

//ultima consulta
$sql="SELECT DSC_".$parentID." as p FROM ".$tablename." where DSC_".$parentID." <> ".$theRepository->DataADOConnection->Quote("*NA");

$w= $theRepository->DataADOConnection->Execute($sql);
if ($w === false)
{
	$strMembers="mal";
	die( translate("Error accessing"));
}
else 
{
	while(!$w->EOF)
	{
		$strMembers.= $w->fields["p"] . "\n";
		$w->MoveNext();
	}
}

header('Content-Type: text/plain; charset=utf-8');
echo $strMembers;
?>