<?php
/**
 * PHPMailer language file.
 * Versi�n en catal�n
 */

$PHPMAILER_LANG = array();

$PHPMAILER_LANG["provide_address"] = 'Ha de proveir almenys una ' .
                                       'adre�a de email com destinatari.';
$PHPMAILER_LANG["mailer_not_supported"] = ' mailer no est� suportat.';
$PHPMAILER_LANG["execute"] = 'No puc executar: ';
$PHPMAILER_LANG["instantiate"] = 'No s\'ha pogut crear una inst�ncia de la funci� Mail.';
$PHPMAILER_LANG["authenticate"] = 'Error SMTP: No s\'ha pogut autenticar.';
$PHPMAILER_LANG["from_failed"] = 'La (s) seg�ent (s) adreces de remitent han fallat: ';
$PHPMAILER_LANG["recipients_failed"] = 'Error SMTP: Els seg�ents ' .
                                        'destinataris han fallat: ';
$PHPMAILER_LANG["data_not_accepted"] = 'Error SMTP: Dades no acceptades.';
$PHPMAILER_LANG["connect_host"] = 'Error SMTP: No puc connectar al servidor SMTP.';
$PHPMAILER_LANG["file_access"] = 'No puc accedir a l\'arxiu: ';
$PHPMAILER_LANG["file_open"] = 'Error d\'Arxiu: No pot obrir l\'arxiu: ';
$PHPMAILER_LANG["encoding"] = 'Codificaci� desconeguda: ';
?>
