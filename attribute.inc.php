<?php

class BITAMAttribute extends BITAMObject
{
	public $consecutivo;
	public $orden;
	public $nom_logico;
	public $nom_fisico;
	public $nom_fisico_bd;
	public $tipo_dato;
	public $long_dato;
	public $cla_concepto;
	public $agrupador;
	public $valor_default;
	public $crea_indicador;
	public $formato;
	public $dimensions;
	public $dimensiones;
	public $cla_gpo_ind;
	//Utilizada solo en la generación de cubos a partir de la versión. Sirve para identificar de que indicador original provenía
	//el nuevo atributo creado durante el proceso, de forma que algunos datos se puedan heredar (la identificación del indicador
	//original se hace mediante la extracción del campo directamente de la fórmula, comparándolo con el NOM_FISICO del Atributo)
	public $cla_indicador;
	
	function __construct($aRepository, $aCubeID = -1)
	{	
		BITAMObject::__construct($aRepository);
		$this->consecutivo=-1;
		$this->orden=-1;
		$this->nom_logico="";
		$this->nom_fisico="";
		$this->nom_fisico_bd="";
		$this->tipo_dato="";
		$this->long_dato='';
		$this->cla_concepto=$aCubeID;
		$this->agrupador='SUM';
		$this->valor_default='';
		$this->crea_indicador=0;
		$this->formato='';
		$this->dimensions=array();
		$this->dimensiones='';
		$this->cla_gpo_ind=-1;
		$this->cla_indicador=-1;
	}
	
	static function NewInstance($aRepository, $aCubeID = -1)
	{
		return new BITAMAttribute($aRepository, $aCubeID);
	}
	
	static function NewInstanceWithID($aRepository, $anAttributeID, $aCubeID)
	{
		$anInstance = null;
		
		if (((int) $anAttributeID) < 0 || ((int) $aCubeID) < 0)
		{
			return $anInstance;
		}

		$anInstance =& BITAMGlobalInstance::GetAttributeInstanceWithID($aCubeID, $anAttributeID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}

		
		$sql = 
	"SELECT CLA_CONCEPTO, CONSECUTIVO, ORDEN, NOM_FISICO, NOM_LOGICO, TIPO_DATO, LONG_DATO, CREA_INDICADOR, ESTATUS, AGRUPADOR, VALOR_DEFAULT FROM SI_CPTO_ATRIB 
	WHERE CLA_CONCEPTO = ".$aCubeID." AND CONSECUTIVO = ".$anAttributeID;

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_CPTO_ATRIB ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMAttribute::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aCubeID=(int) $aRS->fields["cla_concepto"];
		$anAttributeID = (int) $aRS->fields["consecutivo"];
		$anInstance = null;
		$anInstance =& BITAMGlobalInstance::GetAttributeInstanceWithID($aCubeID, $anAttributeID);
		if(is_null($anInstance))
		{
			$anInstance = BITAMAttribute::NewInstance($aRepository);
			$anInstance->consecutivo = (int) $aRS->fields["consecutivo"];
			$anInstance->orden = (int) $aRS->fields["orden"];
			$anInstance->nom_logico = $aRS->fields["nom_logico"];
			$anInstance->nom_fisico = strtolower($aRS->fields["nom_fisico"]);
			$anInstance->nom_fisico_bd = $aRS->fields["nom_fisico"];
			$anInstance->tipo_dato = (int) $aRS->fields["tipo_dato"];
			$anInstance->long_dato = $aRS->fields["long_dato"];
		 	$anInstance->cla_concepto = (int) $aRS->fields["cla_concepto"];
		 	$anInstance->agrupador = strtoupper($aRS->fields["agrupador"]);
		 	$anInstance->valor_default = $aRS->fields["valor_default"];
		 	$anInstance->crea_indicador = (int)$aRS->fields["crea_indicador"];
		 	BITAMGlobalInstance::AddAttributeInstanceWithID($aCubeID, $anAttributeID, $anInstance);
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("CubeID", $aHTTPRequest->GET))
		{
			$aCubeID = (int) $aHTTPRequest->GET["CubeID"];
		}
		else
		{
			$aCubeID = -1;
		}

		if (array_key_exists("consecutivo", $aHTTPRequest->POST))
		{
			$aConsecutiveID = $aHTTPRequest->POST["consecutivo"];
			//$aCubeID = $aHTTPRequest->POST["CubeID"];
			
			if (is_array($aConsecutiveID))
			{
				/*
				$aCollection = BITAMAttributeCollection::NewInstance($aRepository, $aConsecutiveID, $aCubeID, true);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
				*/
			}
			else
			{
				$anInstance = BITAMAttribute::NewInstanceWithID($aRepository, (int) $aConsecutiveID, $aCubeID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMAttribute::NewInstance($aRepository, $aCubeID);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMAttribute::NewInstance($aRepository, $aCubeID);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("CubeID", $aHTTPRequest->GET) && array_key_exists("consecutivo", $aHTTPRequest->GET))
		{
			$aConsecutiveID = $aHTTPRequest->GET["consecutivo"];
			$anInstance = BITAMAttribute::NewInstanceWithID($aRepository,$aConsecutiveID,$aCubeID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMAttribute::NewInstance($aRepository, $aCubeID);
			}
		}
		else
		{
			$anInstance = BITAMAttribute::NewInstance($aRepository, $aCubeID);
		}
		return $anInstance;
	}

	function save()
	{
		global $queriesLogFile;
		
	 	if ($this->isNewObject())
		{
			$aCube = BITAMCube::NewInstanceWithID($this->Repository, $this->cla_concepto); 
			$aCubeID = $aCube->cla_concepto;

			$sql = 'SELECT TIPO_DATO, NOM_DATO FROM SI_BD_TIPOS 
				WHERE CLA_BD = '.$aCube->cla_bd.' AND TIPO_DATO = '.$this->tipo_dato;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_BD_TIPOS ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$strDataType = '';
			if (!$aRS->EOF)
			{
				$strDataType = $aRS->fields["nom_dato"];
				switch ($this->tipo_dato)
				{
		 			case 21:
		 			case 24:
		 				$strDataType .= '('.$this->long_dato.')';
		 				break;
				}
			}

			$strDefaultValue = '';
			if ($aCube->cla_bd == 8)
			{
				if ($this->valor_default != '')
				{
					$strDefaultValue = ' DEFAULT '.(float) $this->valor_default;
				}
				else 
				{
					$strDefaultValue = ' DEFAULT NULL';
				}
			}

			$strNewFieldDefinition = $this->nom_fisico_bd.' '.$strDataType.$strDefaultValue;
			// Agrega la columna del Atributo a la tabla de Hechos
			$sql =  "ALTER TABLE ".$aCube->nom_tabla.' ADD '.$strNewFieldDefinition;
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nBITAMAttribute->Save: \r\n";
			$aLogString.=$sql;
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error modifying")." ".$aCube->nom_tabla." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			// Agrega el nuevo Atributo a la definición del Cubo
			$sql = 'SELECT '.
						$this->Repository->ADOConnection->IfNull('MAX(CONSECUTIVO)', '0').' + 1 AS CONSECUTIVO FROM SI_CPTO_ATRIB 
						WHERE CLA_CONCEPTO = '.$aCubeID;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_CPTO_ATRIB ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$this->consecutivo = $aRS->fields["consecutivo"];
			$this->orden = $this->consecutivo;

			if (trim($this->valor_default) != '')
			{
				$strDefaultValue = (float) $this->valor_default;
			}
			else 
			{
				$strDefaultValue = 'NULL';
			}
			
			$sql = 'INSERT INTO SI_CPTO_ATRIB (ORDEN, CLA_CONCEPTO, NOM_FISICO, NOM_LOGICO, TIPO_DATO, 
					ESTATUS, CREA_INDICADOR, CONSECUTIVO, LONG_DATO, AGRUPADOR, VALOR_DEFAULT) 
			    VALUES ('.$this->orden.', '.$aCubeID.', '.
					$this->Repository->ADOConnection->Quote($this->nom_fisico_bd).', '.
					$this->Repository->ADOConnection->Quote($this->nom_logico).', '.
					$this->tipo_dato.', 1, '.$this->crea_indicador.', '.$this->consecutivo.', '.
					$this->Repository->ADOConnection->Quote($this->long_dato).', '.
					$this->Repository->ADOConnection->Quote($this->agrupador).', '.$strDefaultValue.')';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die(translate("Error inserting ")." SI_CPTO_ATRIB ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			// Agrega el nuevo Indicador Base a la definición del Cubo
			$sql = 'SELECT '.
						$this->Repository->ADOConnection->IfNull('MAX(CLA_INDICADOR)', '0').' + 1 AS CLA_INDICADOR FROM SI_INDICADOR ';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_CPTO_ATRIB ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$aIndicatorKey = $aRS->fields["cla_indicador"];

			$strUserFormula = '{'.$this->nom_logico.'}';
			$strDBFormula = $this->agrupador.'( t1.'.$this->nom_fisico_bd.' )';

			$strFormat = $this->formato;
			if ($strFormat === '\'0\'')
			{
				$strFormat = '0';
			}

			$sql = 'INSERT INTO SI_INDICADOR (CLA_INDICADOR, CLA_CONCEPTO, NOM_INDICADOR, FORMULA_USR, NOM_TABLA, 
					FORMULA_BD, LLAVES, TIPO_AGRUPACION, FORMATO, CLA_PADRE, TIPO_INDICADOR, ES_ATRIBUTO, CLA_OWNER,
					DIMENSIONES) 
				VALUES ('.$aIndicatorKey.', '.$aCubeID.', '.
					$this->Repository->ADOConnection->Quote($this->nom_logico).', '.
					$this->Repository->ADOConnection->Quote($strUserFormula).', '.
					$this->Repository->ADOConnection->Quote($aCube->nom_tabla).', '.
					$this->Repository->ADOConnection->Quote($strDBFormula).', '.
					$this->Repository->ADOConnection->Quote('').', '.
					$this->Repository->ADOConnection->Quote('NORMAL').', '.
					$this->Repository->ADOConnection->Quote($strFormat).', '.
					'-1, 1, 1, '.$_SESSION['PAtheUserID'].','.
					$this->Repository->ADOConnection->Quote($this->dimensiones).')';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_INDICADOR ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			// Agrega al grupo de indicadores
			if ($this->cla_gpo_ind > 0)
			{
				$sql = 'INSERT INTO SI_GPO_IND_DET (CLA_GPO_IND, CLA_INDICADOR)  
					VALUES ('.$this->cla_gpo_ind.', '.$aIndicatorKey.')';
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die( translate("Error inserting")." SI_GPO_IND_DET ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				/* @JAPRWarning: Funcionalidad Faltante
				//Primero obtenemos la información de los grupos de indicadores para actualizar su cache
				$sql = 'SELECT 1 AS ObjectType, A.CLA_USUARIO AS ObjectKey '.
					'FROM SI_USR_IND A WHERE A.CLA_GPO_IND = '.$this->cla_gpo_ind.
					' UNION '.
					'SELECT 2 AS ObjectType, A.CLA_ROL '.
					'FROM SI_ROL_IND A WHERE A.CLA_GPO_IND = '.$this->cla_gpo_ind;
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("Error accessing SI_USR_IND or SI_ROL_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
				}
				while (!$aRS->EOF)
				{
					$this->Repository->addPhPCacheRequest($aRS->fields["objecttype"], $aRS->fields["objectkey"], MDOBJECTDATA_All);
					$aRS->MoveNext();
				}
				*/
			}
			
			// Actualiza el Cache de Artus
			$sql = 'DELETE FROM SI_CACHE_METADATA 
				WHERE CLA_CACHE IN (1, 7)';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error deleting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = 'DELETE FROM SI_CACHE_METADATA 
				WHERE CLA_CACHE IN (2) AND CLA_CONCEPTO = '.$aCubeID;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error deleting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$strCacheDate = formatADateTime(date('Y-m-d H:i:s'),'yyyy-mm-dd hh:mm:ss');
			$sql = 'INSERT INTO SI_CACHE_METADATA (CLA_CACHE, CLA_CONCEPTO, FECHA) 
				VALUES (1, 0, '.
				$this->Repository->ADOConnection->Quote($strCacheDate).')';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error inserting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$sql = 'INSERT INTO SI_CACHE_METADATA (CLA_CACHE, CLA_CONCEPTO, FECHA) 
				VALUES (2, '.$aCubeID.', '.
				$this->Repository->ADOConnection->Quote($strCacheDate).')';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error inserting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = 'INSERT INTO SI_CACHE_METADATA (CLA_CACHE, CLA_CONCEPTO, FECHA) 
				VALUES (7, 0, '.
				$this->Repository->ADOConnection->Quote($strCacheDate).')';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error inserting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			// Agrega la columna del Atributo a todas las agregaciones (solo si el cubo usa Agregados)
			if ($aCube->usa_agregacion)
			{
				$aPeriodDimensionArray = array();
				$aAggregationArray = array();
				$aAggregationArray[] = array(0, $aCube->catalogo_olap);
				$blnUseNewAggregations = $aCube->ag_new;
				$aAggregationCollection = BITAMAggregationCollection::NewInstance($this->Repository, null, $aCubeID, true);
				if (!is_null($aAggregationCollection))
				{
					foreach ($aAggregationCollection as $aAggregation)
					{
						$aAggregationArray[] = array($aAggregation->consecutivo, $aAggregation->nom_fisico_bd);
					}
				}
				
				$aPeriodDimensionCollection = BITAMDimensionCollection::NewInstancePeriodDims($this->Repository, $this->cla_concepto, false);
				if (!is_null($aPeriodDimensionCollection))
				{
					foreach ($aPeriodDimensionCollection as $aPeriodDimension)
					{
						$aPeriodDimensionArray[] = $aPeriodDimension->cla_periodo;
					}
				}
				
				// Recorre cada Id de agregación
				$arrTableArray = array();
				foreach ($aAggregationArray as $aAggregation)
				{
					if (trim($aAggregation[1]) != '')
					{
						// Este es un agregado personalizado del usuario, solo hay una tabla sin importar la cantidad de periodos
						$arrTableArray[] = $aAggregation[1];
					}
					else
					{
						// Recorre cada periodo para terminar de formar el nombre de tabla, ya que se trata de agregados propietarios
						foreach ($aPeriodDimensionArray as $aPeriodDimension)
						{
							if ($blnUseNewAggregations)
							{
								$arrTableArray[] = 'AN_'.$aCubeID.'_'.$aPeriodDimension.'_'.$aAggregation[0];
							}
							else 
							{
								$arrTableArray[] = 'A_'.$aCubeID.'_'.$aPeriodDimension.'_'.$aAggregation[0];
							}
						}
					}
				}

				// Inserta el campo en las tablas de los agregados
				if (count($arrTableArray) > 0)
				{
					foreach ($arrTableArray as $strTableName)
					{
						$sql =  "ALTER TABLE ".$strTableName.' ADD '.$strNewFieldDefinition;
						$aLogString="\r\n--------------------------------------------------------------------------------------";
						$aLogString.="\r\nBITAMAttribute->Save: \r\n";
						$aLogString.=$sql;
						@updateQueriesLogFile($queriesLogFile,$aLogString);
						$aRS = $this->Repository->DataADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die( translate("Error modifying")." ".$strTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}
			}
			
		}
		else
		{
				
			/*
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_CPTO_ATRIB ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			*/
		}
		// @JAPRWarning: Funcionalidad Faltante
		//$this->Repository->addPhPCacheRequest(MDOBJECT_Cube, $this->cla_concepto, MDOBJECTDATA_All);
		return $this;
	}
	
	function get_FormIDFieldName()
	{
		return 'consecutivo';
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Attribute&CubeID=".$this->cla_concepto;
		}
		else
		{
			return "BITAM_PAGE=Attribute&CubeID=".$this->cla_concepto.'&consecutivo='.$this->consecutivo;
		}
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Metric");
		}
		else
		{  
			return $this->nom_logico;
		}
	}

	function isNewObject()
	{
		return ($this->consecutivo < 0);
	}

	function get_Parent()
	{
		if ($this->cla_concepto)
		{
			return BITAMCube::NewInstanceWithID($this->Repository,$this->cla_concepto);
		}
		else 
		{
			return $this->Repository;
		}
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		return false;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "nom_logico";
		$aField->Title = translate("Label");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "nom_fisico_bd";
		$aField->Title = translate("Short Name");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[$aField->Name] = $aField;

		$aDataTypesArray = array();
		$aCube = BITAMCube::NewInstanceWithID($this->Repository, $this->cla_concepto);
		if (!is_null($aCube))
		{
			$sql = 'SELECT TIPO_DATO, NOM_DATO FROM SI_BD_TIPOS 
				WHERE CLA_BD = '.$aCube->cla_bd.' AND TIPO_DATO IN (7,8)';	// (6,7,8,21,23,24)
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				//die("Error accessing SI_BD_TIPOS table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
				die( translate("Error accessing")." SI_BD_TIPOS ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			while (!$aRS->EOF)
			{
				$aDataTypesArray[(int) $aRS->fields["tipo_dato"]] = $aRS->fields["nom_dato"];
				$aRS->MoveNext();
			}
		}
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "tipo_dato";
		$aField->Title = translate("Data Type");
		$aField->Type = "Object";
		$aField->OnChange = "showLengthField()";             
		$aField->VisualComponent = "ComboBox";
		$aField->Options = $aDataTypesArray;
		$aField->Size = 20;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "long_dato";
		$aField->Title = translate("Length");
		$aField->Type = "String";
		$aField->Size = 10;
		//$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		$anAggregateFunctionArray = array();
		$anAggregateFunctionArray['SUM'] = 'SUM';
		$anAggregateFunctionArray['AVG'] = 'AVG';
		if (!$this->isNewObject())
		{
			$anAggregateFunctionArray['MAX'] = 'MAX';
			$anAggregateFunctionArray['MIN'] = 'MIN';
			$anAggregateFunctionArray['COUNT'] = 'COUNT';
		}
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "agrupador";
		$aField->Title = translate("Aggregate Function");
		$aField->Type = "Object";
		$aField->VisualComponent = "ComboBox";
		$aField->Options = $anAggregateFunctionArray;
		$aField->Size = 20;
		$myFields[$aField->Name] = $aField;

		/*
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "valor_default";
		$aField->Title = translate("Default Value");
		$aField->Type = "Float";
		$aField->Size = 20;
		$myFields[$aField->Name] = $aField;
		*/
		
		if ($this->formato === '0')
		{
			$this->formato = '\'0\'';
		}
		
		if ($this->isNewObject())
		{
			$aFormatArray = array();
			$aFormatArray['\'0\''] = '0';
			$aFormatArray['#'] = '#';
			$aFormatArray['#,##0'] = '#,##0';
			$aFormatArray['$#,##0'] = '$#,##0';
			$aFormatArray['#,##0.00'] = '#,##0.00';
			$aFormatArray['$#,##0.00'] = '$#,##0.00';
			$aFormatArray['# %'] = '# %';
			$aFormatArray['0 %'] = '0 %';
			$aFormatArray['0.0 %'] = '0.0 %';
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "formato";
			$aField->Title = translate("Format");
			$aField->Type = "Object";
			$aField->VisualComponent = "ComboBox";
			$aField->Options = $aFormatArray;
			$aField->Size = 30;
			$myFields[$aField->Name] = $aField;

			$aDimensionCollection = BITAMDimensionCollection::NewInstance($this->Repository,null,$this->cla_concepto,true,true,false);
			$aDimensionsArray = array();
			foreach ($aDimensionCollection as $aDimension)
			{
				$aDimensionsArray[$aDimension->consecutivo] = $aDimension->nom_logico;
			}

			$this->dimensions = array_keys($aDimensionsArray);
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "dimensions";
			$aField->Title = translate("Dimensions");
			$aField->Type = "Array";
			$aField->Options = $aDimensionsArray;
			$aField->OptionsSize = 10;
			$myFields[$aField->Name] = $aField;

			$anIndicatorGroupCollection = BITAMIndicatorGroupCollection::NewInstance($this->Repository, null, true);
			$anIndicatorGroupArray = array();
			$anIndicatorGroupArray[-1] = translate('None');
			if (!is_null($anIndicatorGroupCollection))
			{
				foreach ($anIndicatorGroupCollection as $anIndicatorGroup)
				{
					$anIndicatorGroupArray[$anIndicatorGroup->IndicatorGroupID] = $anIndicatorGroup->IndicatorGroupName;

					if ($anIndicatorGroup->IndicatorGroupName == $aCube->nom_concepto)
					{
						$this->cla_gpo_ind = $anIndicatorGroup->IndicatorGroupID;
					}
				}
			}
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "cla_gpo_ind";
			$aField->Title = translate("Indicator Group");
			$aField->Type = "Object";
			$aField->VisualComponent = "ComboBox";
			$aField->Options = $anIndicatorGroupArray;
			$aField->Size = 20;
			$myFields[$aField->Name] = $aField;
		}

		return $myFields;
	}

	function updateFromArray($anArray)
	{
		
		if (array_key_exists("consecutivo", $anArray))
		{
			$this->consecutivo = (int) $anArray["consecutivo"];
		}
		
	 	if (array_key_exists("nom_logico", $anArray))
		{
			$this->nom_logico = $anArray["nom_logico"];
		}
		
	 	if (array_key_exists("nom_fisico_bd", $anArray))
		{
			$this->nom_fisico_bd = $anArray["nom_fisico_bd"];
			$this->nom_fisico = strtolower($anArray["nom_fisico_bd"]);
		}
		
		if (array_key_exists("tipo_dato", $anArray))
		{
			$this->tipo_dato = (int) $anArray["tipo_dato"];
		}
		
	 	if (array_key_exists("long_dato", $anArray))
		{
			$this->long_dato = $anArray["long_dato"];
		}
		
		if (array_key_exists("agrupador", $anArray))
		{
			$this->agrupador = strtoupper($anArray["agrupador"]);
			$this->setAggregateFunctionID();
		}

		if (array_key_exists("valor_default", $anArray))
		{
			$this->valor_default = (float) $anArray["valor_default"];
		}

		if (array_key_exists("formato", $anArray))
		{
			$this->formato = $anArray["formato"];
		}

		if (array_key_exists("dimensions", $anArray))
		{
			$this->dimensions = $anArray["dimensions"];
			$this->setDimensionsMask();
		}

		if (array_key_exists("cla_gpo_ind", $anArray))
		{
			$this->cla_gpo_ind = (int) $anArray["cla_gpo_ind"];
		}
		
		return $this;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{	
?>
	 	<script language="JavaScript">

	 	function showLengthField()
	 	{
	 		objDataTypeCmb = BITAMAttribute_SaveForm.tipo_dato;
	 		objRowDataLength = document.getElementById("Row_long_dato");
	 		intDataType = parseInt(objDataTypeCmb.options(objDataTypeCmb.selectedIndex).value);
	 		switch (intDataType)
	 		{
	 			case 21:
	 			case 24:
	 				objRowDataLength.style.display = 'inline';
	 				break;
	 			default:
	 				objRowDataLength.style.display = 'none';
	 				break;
	 		}
	 	}
	 	
	 	function canSave(target)
	 	{
			if(Trim(BITAMAttribute_SaveForm.nom_logico.value)=='')
			{
				BITAMAttribute_SaveForm.nom_logico.focus();
				alert('<?=translate('Data in required field is missing')?>. <?=translate('Field')?>: \'<?=translate('Label')?>\'');
				return false;
			}
	 		
			if(Trim(BITAMAttribute_SaveForm.nom_fisico_bd.value)=='')
			{
				BITAMAttribute_SaveForm.nom_fisico_bd.focus();
				alert('<?=translate('Data in required field is missing')?>. <?=translate('Field')?>: \'<?=translate('Short Name')?>\'');
				return false;
			}

			objRegExp = /[\[{|}\]]/;
			arrMatches = objRegExp.exec(BITAMAttribute_SaveForm.nom_logico.value);
			anArrayLength = 0;
			if (arrMatches != null)
			{
				anArrayLength = arrMatches.length;
			}
			strMatches = '';
			for (i=0; i<anArrayLength; i++)
			{
				strMatches += arrMatches[i];
			}
			if (strMatches.length > 0)
			{
				alert('<?=translate('Invalid characters were found on a required field')?>: \''+strMatches+'\'. <?=translate('Field')?>: \'<?=translate('Label')?>\'');
				BITAMAttribute_SaveForm.nom_logico.focus();
				return false;
			}
			
			objRegExp = /\W/;
			arrMatches = objRegExp.exec(BITAMAttribute_SaveForm.nom_fisico_bd.value);
			anArrayLength = 0;
			if (arrMatches != null)
			{
				anArrayLength = arrMatches.length;
			}
			strMatches = '';
			for (i=0; i<anArrayLength; i++)
			{
				strMatches += arrMatches[i];
			}
			if (strMatches.length > 0)
			{
				BITAMAttribute_SaveForm.nom_fisico_bd.focus();
				alert('<?=translate('Invalid characters were found on a required field')?>: \''+strMatches+'\'. <?=translate('Field')?>: \'<?=translate('Short Name')?>\'');
				return false;
			}

	 		objDataTypeCmb = BITAMAttribute_SaveForm.tipo_dato;
	 		intDataType = parseInt(objDataTypeCmb.options(objDataTypeCmb.selectedIndex).value);
	 		switch (intDataType)
	 		{
	 			case 21:
	 			case 24:
	 				break;
	 			default:
	 				BITAMAttribute_SaveForm.long_dato.value = '';
	 				break;
	 		}
			
			if (Trim(BITAMAttribute_SaveForm.long_dato.value) != '')
			{
				objRegExp = /[^\d|\,]/;
				arrMatches = objRegExp.exec(BITAMAttribute_SaveForm.long_dato.value);
				anArrayLength = 0;
				if (arrMatches != null)
				{
					anArrayLength = arrMatches.length;
				}
				strMatches = '';
				for (i=0; i<anArrayLength; i++)
				{
					strMatches += arrMatches[i];
				}
				if (strMatches.length > 0)
				{
					BITAMAttribute_SaveForm.long_dato.focus();
					alert('<?=translate('Invalid characters were found on a required field')?>: \''+strMatches+'\'. <?=translate('Field')?>: \'<?=translate('Length')?>\'');
					return false;
				}
			}
			
	 		var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('<?=translate('XML Object not supported')?>');
				return false;
			}
			
			xmlObj.open('GET', 'validateAttribute.php?CubeID=<?=$this->cla_concepto?>&DataType='+intDataType+'&FieldName='+escape(BITAMAttribute_SaveForm.nom_fisico_bd.value), false);
	 		xmlObj.send(null);
			if(xmlObj.status == 404)
			{
				alert('<?=translate('Unable to launch validation process')?>');
				return false;
			}
			
	 		strResponseData = Trim(unescape(xmlObj.responseText));
	 		arrResponseData = strResponseData.split('<SEP>');
	 		if(arrResponseData.length == 0)
	 		{
	 			alert('<?=translate('Error found during data validation')?>: <?=translate('404 - File not found')?>');
				return false;
	 		}
	 		
	 		if(arrResponseData[0]!=0)
	 		{
				strErrMsg = arrResponseData[0];
				if(arrResponseData.length > 1)
				{
					strErrMsg = arrResponseData[1];
				}
				
	 			alert('<?=translate('Error found during data validation')?>: ' + strErrMsg);
	 		}
	 		else
	 		{	
	 			BITAMAttribute_Ok(target);
	 		}
	 	}
	 	
		</script> 		
<?
 	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
 	{
?>
	 	<script language="JavaScript">
 			objOkSelfButton = document.getElementById("BITAMAttribute_OkSelfButton");
 			objOkParentButton = document.getElementById("BITAMAttribute_OkParentButton");
 			objOkSelfButton.onclick=new Function("canSave('self');");
 			objOkParentButton.onclick=new Function("canSave('parent');");
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMAttribute_OkNewButton");
 			objOkNewButton.onclick=new Function("canSave('new');");
<?
		}
?>
		</script> 		
<?
	}	

	function setAggregateFunctionID()
	{
		switch (strtoupper($this->agrupador))
		{
			case 'AVG':
				$this->crea_indicador = 2;
				break;
			case 'COUNT':
				$this->crea_indicador = 5;
				break;
			case 'MAX':
				$this->crea_indicador = 4;
				break;
			case 'MIN':
				$this->crea_indicador = 3;
				break;
			default:
				$this->crea_indicador = 1;
				break;
		}
	}

	function setDimensionsMask()
	{
		$aDimensionCollection = BITAMDimensionCollection::NewInstance($this->Repository,null,$this->cla_concepto,true,true,false);
		$max = 0;
		$aDimensionsArray = array();
		$aSelectedDimensionsArray = array_flip($this->dimensions);
		foreach ($aDimensionCollection as $aDimension)
		{
			if ($aDimension->consecutivo > $max)
			{
				$max = $aDimension->consecutivo;
			}

			if (array_key_exists($aDimension->consecutivo, $aSelectedDimensionsArray))
			{
				$aDimensionsArray[$aDimension->consecutivo] = $aDimension->nom_logico;
			}
		}

		$strDimensions = '';
		for ($i=1; $i <= $max; $i++)
		{
			if(array_key_exists($i, $aDimensionsArray))
			{
				$strDimensions .= '1';
			}
			else 
			{
				$strDimensions .= '0';
			}
		}
		
		$this->dimensiones = $strDimensions;
	}
	
}

/***********************************************************************************************/

class BITAMAttributeCollection extends BITAMCollection
{
	public $cla_concepto;
	
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
		$this->cla_concepto = -1;
	}
	
	
	static function NewInstance($aRepository, $anArrayOfAttributeIDs = null, $aCubeID,$in)
	{
		$anInstance = new BITAMAttributeCollection($aRepository);
		$where = "";
		
		if (!is_null($anArrayOfAttributeIDs))
		{
			switch (count($anArrayOfAttributeIDs))
			{
				case 0:
					break;
				
				case 1:
				
					if($in==true)
					{
						$where = " AND CONSECUTIVO = ".((int) $anArrayOfAttributeIDs[0]);
					}
					else
					{
						$where = " AND CONSECUTIVO <> ".((int) $anArrayOfAttributeIDs[0]);
					}
					break;	
					
				default:
				
					foreach ($anArrayOfAttributeIDs as $anAttributeID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $anAttributeID; 
					}
					if ($where != "")
					{
						if($in==true)
						{
							$where = " AND CONSECUTIVO IN (".$where.")";
						}
						else
						{
							$where = " AND CONSECUTIVO NOT IN (".$where.")";
						}
					}
					break;
			}
		}

		$sql = 
		"SELECT CLA_CONCEPTO, CONSECUTIVO, ORDEN, NOM_FISICO, NOM_LOGICO, TIPO_DATO, LONG_DATO, CREA_INDICADOR, ESTATUS, AGRUPADOR, VALOR_DEFAULT FROM SI_CPTO_ATRIB 
		WHERE CLA_CONCEPTO = ".$aCubeID." ".$where. " ORDER BY NOM_LOGICO";
	
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			//die("Error accessing SI_CPTO_ATRIB table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			die( translate("Error accessing")." SI_CPTO_ATRIB ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMAttribute::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		$anInstance->cla_concepto=$aCubeID;
		return $anInstance;
	}
	
	static function NewInstanceEmpty($aRepository)
	{
		$anInstance = new BITAMAttributeCollection($aRepository);
		
		return $anInstance;
	}
	
	
	function getIndexByAttrID($atrID)
	{	
		$index=null;
		
		if($atrID==null)
		{
			return $index;
		}
	
		$sizeCollection=count($this->Collection);
		
		for($i=0;$i<$sizeCollection;$i++)
		{
			if($this->Collection[$i]->consecutivo==$atrID)
			{
			 	$index=$i;
			 	break;
			}
		}
		
		return $index;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("CubeID", $aHTTPRequest->GET))
		{
			$aCubeID = (int) $aHTTPRequest->GET["CubeID"];
		}
		else
		{
			$aCubeID = 0;
		}
		return BITAMAttributeCollection::NewInstance($aRepository,null,$aCubeID,true);
	}
	
	function get_Parent()
	{
		if ($this->cla_concepto > 0)
		{
			return BITAMCube::NewInstanceWithID($this->Repository,$this->cla_concepto);
		}
		else 
		{
			return BITAMCubeCollection::NewInstance($this->Repository,null,3);
		}
	}
	
	function get_Title()
	{	
		$title = translate("Metrics Definitions");
		
		return $title;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=Attribute&CubeID=".$this->cla_concepto;
	}
	
	function get_QueryString()
	{
		return "BITAM_SECTION=AttributeCollection";
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_SECTION=Attribute";
	}

	function get_FormIDFieldName()
	{
		return 'consecutivo';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "nom_logico";
		$aField->Title = translate("Label");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "nom_fisico_bd";
		$aField->Title = translate("Short Name");
		$aField->Type = "String";
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "agrupador";
		$aField->Title = translate("Aggregate Function");
		$aField->Type = "String";
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		$RepositoryType = $_SESSION["PABITAM_RepositoryType"];
		if (is_null($RepositoryType))
		{
			$RepositoryType = 2;
		}
		
		//Si no es de desarrollo, no podemos crear/editar/borrar
		if ($RepositoryType > 1)
		{
			return false;
		}
		
		//Si el usuario logeado no es el dueño y no es SUPERVISOR, no se puede continuar
		$aCube = BitamCube::NewInstanceWithID($this->Repository, $this->cla_concepto);
		if ($aUser->UserID != $aCube->OwnerID && $aUser->UserID != 0)
		{
			return false;
		}
		
		return true;
	}
	
	function canEdit($aUser)
	{
		return false;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

}

?>