<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Parámetro para indicar si el template de Agendas generado debe llenarse con las definiciones actuales o si simplemente se genera el esqueleto vacio
$blnFill = 0;
if (array_key_exists("fill", $_GET))
{
	$blnFill = (bool) (int) $_GET["fill"];
}
if (array_key_exists("fill", $_POST))
{
	$blnFill = (bool) (int) $_POST["fill"];
}

//GCRUZ 2015-09-03. Parámetros de fitlro del layout
$startDate = (isset($_REQUEST['startDate']))? $_REQUEST['startDate'] : null;
$endDate = (isset($_REQUEST['endDate']))? $_REQUEST['endDate'] : null;
$CheckPointIDs = (isset($_REQUEST['CheckPointIDs']))? explode(',', $_REQUEST['CheckPointIDs']) : null;
$UserIDs = (isset($_REQUEST['UserIDs']))? explode(',', $_REQUEST['UserIDs']) : null;
$StatusIDs = (isset($_REQUEST['StatusIDs']))? explode(',', $_REQUEST['StatusIDs']) : null;
$searchFilter = (isset($_REQUEST['searchFilter']))? $_REQUEST['searchFilter'] : null;

//GCRUZ 2016-02-25. Formas seleccionadas para mostrar en el template
$fillSurveys = (isset($_REQUEST['fillsurveys']) && trim($_REQUEST['fillsurveys']) != '')? explode(',', $_REQUEST['fillsurveys']) : array();
$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
if (count($fillSurveys) > 0)
{
	$theAppUser->updateFromArray(array('AgendasSurveysTemplate' => $_REQUEST['fillsurveys']));
	$theAppUser->save();
}

require_once("survey.inc.php");
require_once("surveyAgenda.inc.php");
require_once("surveyAgendaScheduler.inc.php");
require_once("user.inc.php");
require_once("catalog.inc.php");

$blnWriteFile = true;
$sfile = 'AgendasSchedulerTemplate-'.session_id().'-DTE'.date('YmdHis').'.xls';
//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
$strLogPath = GeteFormsLogPath();
$spathfile = $strLogPath.$sfile;
//@JAPR
$LN = chr(13).chr(10);

//Genera la cabecera del archivo
$aSurveyCollection = BITAMSurveyCollection::NewInstance($theRepository, null, false);

if (is_null($aSurveyCollection) || count($aSurveyCollection->Collection) == 0)
{
	$blnWriteFile = false;
	//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	error_log("\r\n"."There're not surveys available to schedule in an agenda", 3, $strLogPath.'ExportAgendas.txt');
	//@JAPR
}
else 
{
	//Obtiene el Array con los nombres de encuestas y otro indexado por IDs para marcarlas existentes
	$arrSurveyNames = array();
	$arrSurveyCheck = array();
	//@JAPR 2014-11-03: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
	$arrSurveyNamesByID = array();
	foreach ($aSurveyCollection as $aSurvey)
	{
		//GCRUZ 2016-02-25. Formas seleccionadas para mostrar en el template
		if (count($fillSurveys) > 0 && array_search($aSurvey->SurveyID, $fillSurveys) === false)
			continue;
		//@JAPR 2016-02-25: Corregido un bug, el nombre de la forma estaba generándose con caracteres inválidos
		$arrSurveyNames[] = '"'.str_replace('"', '""', utf8_decode($aSurvey->SurveyName)).'"';
		$arrSurveyCheck[$aSurvey->SurveyID] = 0;
		//@JAPR 2014-11-03: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
		//@JAPR 2016-02-25: Corregido un bug, el nombre de la forma estaba generándose con caracteres inválidos
		$arrSurveyNamesByID[$aSurvey->SurveyID] = str_replace('"', '""', utf8_decode($aSurvey->SurveyName));
	}
}

if ($blnWriteFile)
{
	//GCRUZ 2015-09-04. Generar la exportación en XLS
	require_once('../phpexcel/Classes/PHPExcel.php');
	// instantiate Excel.Application
	$bLoaded = false;
	try	
	{
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
	} 
	catch (Exception  $e) 
	{
		echo "Error creating xls object";
		exit();
		//return array(false, $e->getCode(), $e->getMessage(), $e->getLine());
	}

	// Set properties
	$objPHPExcel->getProperties()->setCreator("BITAM EForms");

	$sheetNames = $objPHPExcel->getSheetNames();
	$sheetCount = $objPHPExcel->getSheetCount();
	$objPHPExcel->setActiveSheetIndex(0);
	
	// create xls
	$o = $objPHPExcel;
	
	// get active sheet
	$a = $o->getActiveSheet();
	$a->setTitle('Agendas');
	
	// set default column width
	$a->getDefaultColumnDimension()->setWidth(20.71); // si ponemos 20, con los bordes queda en 19.29
	
	$ncounter = 1;
	$strFirst = "";
	$strCounter = "A";
	$intColumnCounter = 65;
	
	//GCRUZ 2015-09-04. Quitar columna de nombre de catálogo
	$xlsColumns = explode(',', 'Name,EMail,CheckPoint,Filter,Start,End,Time,Frequency,Each,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,Week #,Check-in Form,'.implode(',', $arrSurveyNames));
	
	//Columnas
	foreach ($xlsColumns as $fields)
	{
		$fields = str_replace('"', '', $fields);
		$strIndex = $strFirst.chr($intColumnCounter);
		$a->setCellValue($strIndex.$ncounter, utf8_encode($fields));
		if (chr($intColumnCounter) == "Z")
		{
			$strFirst = (strlen($strFirst) == 0 ? "A" : chr(ord($strFirst) + 1));
			$intColumnCounter = 65;
		}
		else 
		{
			$intColumnCounter++;;
		}
	}

	// set range alignment
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// set fill style
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF99');

	// set font style from array
	$arrStyle = array();
	$arrStyle["font"] = array();
	$arrStyle["font"]["name"] = "Arial";
	$arrStyle["font"]["size"] = 10;
	$arrStyle["font"]["bold"] = true;
	$arrStyle["font"]["color"] = array("rgb" => "0000FF");
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->applyFromArray($arrStyle);

	// set border style from array
	$arrBorderStyle = array();
	$arrBorderStyle["allborders"] = array();
	$arrBorderStyle["allborders"]["style"] = PHPExcel_Style_Border::BORDER_THIN;
	$arrBorderStyle["allborders"]["color"] = array("rgb" => "000000");
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getBorders()->applyFromArray($arrBorderStyle);
	//$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
}

if ($blnFill && $blnWriteFile)
{
	//GCRUZ 2015-09-04. Generar la exportación en XLS	

	// select A2!
	$ncounter++;
	$a->setSelectedCell(chr(65).$ncounter);
	$i = 1;
	
	//Llena todos los registros con las agendas existentes
	//GCRUZ 2015-09-03. Pasar los parámetros de filtro al generar la colección de agendas
	$anAgendaSchedulerCollection = BITAMSurveyAgendaSchedulerCollection::NewInstance($theRepository, null, $UserIDs, $endDate, null, $startDate, null, 100, false, null, $searchFilter, $StatusIDs, $CheckPointIDs);
	if (!is_null($anAgendaSchedulerCollection) && count($anAgendaSchedulerCollection->Collection) > 0)
	{
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$arrUsers = BITAMeFormsUser::GetUsers($theRepository, false);
		$arrCatalogs = BITAMCatalog::getCatalogs($theRepository, false);
		$arrCatalogMembers = BITAMSurveyAgenda::getCatMembersByCatalogID($theRepository);
		
		foreach ($anAgendaSchedulerCollection as $anAgendaScheduler)
		{
			$arrSurveyIDs = $arrSurveyCheck;
			foreach ($anAgendaScheduler->SurveyIDs as $intSurveyID)
			{
				if (isset($arrSurveyIDs[$intSurveyID]))
				{
					$arrSurveyIDs[$intSurveyID] = 1;
				}
			}
			
			$strName = str_replace('"', '""', (string) @$anAgendaScheduler->AgendaSchedName);
			$strEMail = str_replace('"', '""', (string) @$arrUsers[$anAgendaScheduler->UserID]);
			$strCheckPoint = str_replace('"', '""', (string) @$anAgendaScheduler->CheckPointName);
			$strCatalog = str_replace('"', '""', (string) @$arrCatalogs[$anAgendaScheduler->CatalogID]);
			$strFilterText = str_replace('"', '""', (string) @$anAgendaScheduler->FilterText);
			$arrDays = array_fill(0, 7, "");
			$strWeekNum = "";
			
			//Tipo de frecuencia, se estandariza a texto en inglés por ahora, aunque eventualmente se pretende que se pueda generar una Combo de MS Excel
			$strFrequency = "Day";
			$sOpc = (string) $anAgendaScheduler->RecurPatternOpt;
			switch ((int) $anAgendaScheduler->RecurPatternType) {
				case frecWeek:
					$strFrequency = "Week";
					$arrDiasSel = explode("|", $sOpc);
					foreach ($arrDiasSel as $intDaynum) {
						if (isset($arrDays[$intDaynum])) {
							$arrDays[$intDaynum] = "1";
						}
					}
					break;
				case frecWeekOfMonth:
					$strFrequency = "Week of the month";
                	$PatternOptions = explode("/", $sOpc);
                	//0 => nOrdinal, 1 => nDia, 2 => mensual (este ultmimo caso en particular no aplica)
					if (isset($PatternOptions[1])) {
						$PatternOptions = explode("|", $PatternOptions[1]);
						if (isset($PatternOptions[0])) {
							$strWeekNum = intval($PatternOptions[0]);
						}
						if (isset($PatternOptions[1])) {
							$intDaynum = intval($PatternOptions[1]) - 3; //Restamos tres para a justar los días a 0, 1, 2, ..., 6
							if (isset($arrDays[$intDaynum])) {
								$arrDays[$intDaynum] = "1";
							}
						}
					}
					break;
				case frecMonth:
					$strFrequency = "Month";
                    //RecurPatternOpt: [0]: tipo / [1]:opciones
                    $aMensualOpc = explode("/", $sOpc);
					switch($aMensualOpc[0]) {
						case "0"://dia- dias seleccionados
							//No aplica desde carga de XLS
							break;
						case "1"://el dia
							//aOpc- [0]: ordinal, [1] dia
							$aOpc = explode("|", $aMensualOpc[1]);
							//0first,1second,2third,3fourth,4last
							if (isset($aOpc[0])) {
								$strWeekNum = intval($aOpc[0]);
							}
							//0day, 1weekday, 2weekend day,3'sunday',4'monday',5'tuesday', 6'wednesday', 7'thursday', 8'friday', 9'saturday');
							if (isset($aOpc[1])) {
								$intDaynum = intval($aOpc[1]);
								if (isset($arrDays[$intDaynum])) {
									$arrDays[$intDaynum] = "1";
								}
							}
							break;
					}
					
					break;
				/*case frecYear:		//La frecuencia Anual se removió, para esos casos se tendrán que agendar directamente las fechas
					$strFrequency = "Year";
					break;*/
				case frecDay:
				default:
					$strFrequency = "Day";
					break;
			}
			
			$strDays = implode('","', $arrDays);
			
			//@JAPR 2014-11-03: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
			$strCheckInFormName = str_replace('"', '""', (string) @$arrSurveyNamesByID[$anAgendaScheduler->CheckinSurvey]);
			//@JAPR 2014-11-03: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
			//GCRUZ 2015-09-03. Agregada columna de CheckPoint
			//GCRUZ 2015-09-04. Quitar columna de nombre de catálogo
			$line = $LN.'"'.$strName.'","'.$strEMail.'","'.$strCheckPoint.'","'.$strFilterText.'","'.$anAgendaScheduler->getAgendaTime().'","'.$strFrequency.'","'.
				$anAgendaScheduler->RecurPatternNum.'","'.$strDays.'","'.$strWeekNum.'","'.$strCheckInFormName.'",';
			//@JAPR
			$line .= implode(',', $arrSurveyIDs);
			
			$xlsRows = explode(',', str_replace($LN, '', str_replace('"', '', $line)));
			$intColumnCounter = 65;
			$strFirst = "";
			foreach ($xlsRows as $field => $value)
			{
				$value = str_replace('"', '', $value);
				$strIndex = $strFirst.chr($intColumnCounter);
				$a->setCellValue($strIndex.$ncounter, utf8_encode($value));
				//$a->getStyle($strIndex.$ncounter)->getAlignment()->setWrapText(true);
				//$a->getStyle($strIndex.$ncounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
				if (chr($intColumnCounter) == "Z")
				{
					$strFirst = (strlen($strFirst) == 0 ? "A" : chr(ord($strFirst) + 1));
					$intColumnCounter = 65;
				}
				else 
				{
					$intColumnCounter++;;
				}
			}
			$ncounter++;
			$i++;
		}
	}
}

//Descarga el archivo o genera el error de exportación
if($blnWriteFile)
{
	//GCRUZ 2015-09-04. Generar la exportación en XLS	
	//	$a->getProtection()->setSheet(true);
	
	//unset($r);
	unset($a);
	unset($o);
	
	$fileName = str_replace('.csv', '.xls', $spathfile);
	$strXLSPath = $fileName.($ncounter > 255 ? "x" : "");

// save file
	if ($ncounter > 255)
	{
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$fileName .= "x";
	}
	else
	{
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	}

	try 
	{
		$objWriter->save($strXLSPath);
		$spathfile = $fileName;
		$sfile = str_replace('.csv', '.xls', $sfile);
		$sfile = $sfile.($ncounter > 255 ? "x" : "");
	}
	catch (Exception  $e) 
	{
		echo "Error file path:{$fileName}";
		exit();
		//return array(false, $e->getCode(), $e->getMessage(), $e->getLine());
	}

	unset($objWriter);
	
	if(!is_file($spathfile))
	{
		echo "Error file path:{$spathfile}";
		exit();
	}
	else
	{
		/*
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$sfile.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		*/
		if (getParamValue('DebugAbort', 'both', '(int)', true)) {
			echo ("<br>\r\nAborting process");
			die("Pathfile: {$spathfile} => '{$sfile}'");
		}
		
		//fix for IE catching or PHP bug issue
		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=\"".$sfile."\";");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($spathfile));
		readfile($spathfile);
		exit();
	}
}
else
{
	echo "Error file path:{$spathfile}";
	exit();
}
?>