<?PhP
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

$strOriginalWD = getcwd();

$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

echo("<BR>"."Converting database tables");

$blnErrors = false;
$strRepository = $_SESSION["PABITAM_RepositoryName"];
$strDWH = str_ireplace("_bmd_", "_dwh_", $strRepository);

echo("<BR>");
echo("<BR>*****************************************************************");
echo("<BR>***** ".$strRepository);
echo("<BR>*****************************************************************");
//Genera un ALTER TABLE por cada tabla que no se encuentre en el collation correspondiente
$sql = "SELECT TABLE_NAME, CONCAT('ALTER TABLE ', TABLE_NAME, ' CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;') AS SQLAlter 
	FROM information_schema.tables 
	WHERE TABLE_SCHEMA = '{$strRepository}' AND TABLE_TYPE <> 'VIEW' AND TABLE_COLLATION <> 'utf8_general_ci' 
	GROUP BY TABLE_NAME;";
echo("<BR>"."sql:"."<BR>".$sql."<BR>");
$aRS = $aRepository->ADOConnection->Execute($sql);
if (!$aRS) {
	//conchita 1-nov-2011 ocultar mensajes sql
	echo(sendUpgradeMessage("***** ".translate("Error accessing") . " information_schema.tables " . translate("table") . ": " . $aRepository->ADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql));
	$blnErrors = true;
}
else {
	while (!$aRS->EOF) {
		$strTableName = trim((string) @$aRS->fields["table_name"]);
		$sql = trim((string) @$aRS->fields["sqlalter"]);
		if (trim($strTableName) && trim($sql)) {
			echo("<BR><BR>"."Converting table '{$strTableName}'");
			echo(sendUpgradeMessage("{$sql}", "color: blue;"));
			if ($sql != '') {
				if (!$aRepository->ADOConnection->Execute($sql)) {
					echo(sendUpgradeMessage("***** ".translate("Error").": ".$aRepository->ADOConnection->ErrorMsg()));
					$blnErrors = true;
				}
				else {
					$intAffectedRows = (int) @$aRepository->ADOConnection->_affectedrows();
					if (!$intAffectedRows) {
						$sql = "ALTER TABLE {$strTableName} COLLATE utf8_general_ci;";
						echo(sendUpgradeMessage("{$sql}", "color: blue;"));
						if (!$aRepository->ADOConnection->Execute($sql)) {
							echo(sendUpgradeMessage("***** ".translate("Error").": ".$aRepository->ADOConnection->ErrorMsg()));
							$blnErrors = true;
						}
					}
				}
			}
		}
		$aRS->MoveNext();
	}
}

echo("<BR>");
echo("<BR>*****************************************************************");
echo("<BR>***** ".$strDWH);
echo("<BR>*****************************************************************");
//Genera un ALTER TABLE por cada tabla que no se encuentre en el collation correspondiente
$sql = "SELECT TABLE_NAME, CONCAT('ALTER TABLE ', TABLE_NAME, ' CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;') AS SQLAlter 
	FROM information_schema.tables 
	WHERE TABLE_SCHEMA = '{$strDWH}' AND TABLE_TYPE <> 'VIEW' AND TABLE_COLLATION <> 'utf8_general_ci' 
	GROUP BY TABLE_NAME;";
echo("<BR>"."sql:"."<BR>".$sql."<BR>");
$aRS = $aRepository->DataADOConnection->Execute($sql);
if (!$aRS) {
	//conchita 1-nov-2011 ocultar mensajes sql
	echo(sendUpgradeMessage("***** ".translate("Error accessing") . " information_schema.tables " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql));
	$blnErrors = true;
}
else {
	while (!$aRS->EOF) {
		$strTableName = trim((string) @$aRS->fields["table_name"]);
		$sql = trim((string) @$aRS->fields["sqlalter"]);
		if (trim($strTableName) && trim($sql)) {
			echo("<BR><BR>"."Converting table '{$strTableName}'");
			echo(sendUpgradeMessage("{$sql}", "color: blue;"));
			if ($sql != '') {
				if (!$aRepository->DataADOConnection->Execute($sql)) {
					echo(sendUpgradeMessage("***** ".translate("Error").": ".$aRepository->DataADOConnection->ErrorMsg()));
					$blnErrors = true;
				}
				else {
					$intAffectedRows = (int) @$aRepository->DataADOConnection->_affectedrows();
					if (!$intAffectedRows) {
						$sql = "ALTER TABLE {$strTableName} COLLATE utf8_general_ci;";
						echo(sendUpgradeMessage("{$sql}", "color: blue;"));
						if (!$aRepository->DataADOConnection->Execute($sql)) {
							echo(sendUpgradeMessage("***** ".translate("Error").": ".$aRepository->DataADOConnection->ErrorMsg()));
							$blnErrors = true;
						}
					}
				}
			}
		}
		$aRS->MoveNext();
	}
}

if ($blnErrors) {
	echo("<BR>"."Process finished with errors");
}
else {
	echo("<BR>"."Process finished");
}

//Genera un tag Span para incluir en el reporte de un proceso de migración. Permite recibir parámetros para variar directamente el estilo
if (!function_exists('sendUpgradeMessage')) {
	function sendUpgradeMessage($sErrorText, $aStyleSStr = '') {
		if ($aStyleSStr == '') {
			$aStyleSStr = 'color: red; font-weight:bold;';
		}
		$strHTML = "<br>\r\n<span style=\"$aStyleSStr\">".str_replace("\r\n", "<br>\r\n", $sErrorText)."</span>\r\n";
		
		return $strHTML;
	}
}
?>