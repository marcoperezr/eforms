<?php
//define('ADODB_ASSOC_CASE', 0);
require_once('adodb/adodb.inc.php');

global $ADODBLogFile;
global $ADODB_FETCH_MODE;

require_once('config.php');

$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$LanguageDictionary = array();
//@JAPR 2019-09-23: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
$gbLanguageAutoLoaded = false;
//@JAPR

function LoadLanguageWithName($aName)
{
	global $LanguageDictionary;
	
	$aLanguageFile = realpath("language_".AsTagName($aName).".php");
	if (file_exists($aLanguageFile))
	{
		require_once("Language_".$aName.".php");
	}
}

function translate($aString, $aParameter = null)
{
	global $LanguageDictionary;
	//@JAPR 2019-09-23: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
	global $gbLanguageAutoLoaded;
	
	//@JAPR 2019-09-23: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
	//Si es la primera vez que se invoca y no se había cargado el idioma previamente, entonces lo cargará en este momento (esto es útil únicamente en los errores iniciales y/o de login,
	//antes de que se cargue al usuario, ya que una vez cargado inmediatamente se carga el idioma del mismo, se usará por defaul el parámetro de idioma enviado por el app en el request si
	//es que existe, de lo contrario simplemente no cargará ningún archivo de idioma, en cualqueir caso, deja marcada una bandera para no repetir este proceso)
	if ( !$gbLanguageAutoLoaded && count($LanguageDictionary) == 0 ) {
		LoadLanguageWithName((string) @$_REQUEST["lang"]);
		$gbLanguageAutoLoaded = true;
	}
	//@JAPR
	
	$aTranslation = array_key_exists($aString, $LanguageDictionary) ? $LanguageDictionary[$aString] : $aString;
	if (is_null($aParameter))
	{
		return $aTranslation;
	}
	return sprintf($aTranslation, $aParameter);
}

function AsTagName($aString)
{
	$search = array("(^[^A-Za-z]+)", "([^A-Za-z0-9_])");
	$replace = array("", "");
	$tagName = preg_replace($search, $replace, $aString);
	if (strlen($tagName) == 0)
	{
		$tagName = "BIT".crc32($aString);
	}
	return $tagName;
}

function GetBitamInitialKey($aRepository)
{
	$sql = "SELECT CLAVE_INICIAL FROM SI_PROYECTO WHERE CLA_PROYECTO = 1";
	
	$aRS = $aRepository->ADOConnection->Execute($sql);
	
	if (!$aRS)
	{
		die( translate("Error accessing")." SI_PROYECTO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	$initialKey = 0;
	
	if(!$aRS->EOF)
	{
		$initialKey = (int) $aRS->fields["clave_inicial"];
	}
	
	return $initialKey;
}

function is_IE()
{
	if (!isset($_SERVER['HTTP_USER_AGENT'])) 
	{
		return true;
	}
    elseif(isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')!== false)) 
    {
        return true;
    }
    else 
    {
        return false;
    }
}
?>