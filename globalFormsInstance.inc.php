<?php

class BITAMGlobalFormsInstance
{
	// Colección de todos los OBJETOS DE FORMS para obtener datos de sus tablas
	static $arrAllUserNames=array();
	static $arrAllClaDescripsOfCatalog=array();
	static $arrAllClaDescripsOfCatMember=array();
	static $arrAllSurveyInstances=array();
	static $arrAllSectionInstances=array();
	static $arrAllDynSectionOfSurveys=array();
	static $arrAllCatalogInstances=array();
	//@JAPR 2015-07-21: Agregados los DataSource y DataSourceMembers
	static $arrAllDataSourceInstances=array();
	static $arrAllDataMemberInstances=array();
	//@JAPR 2018-07-13 Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	static $arrAllDataSourceColl=array();					//Colección de todos los DataSources del repositorio sin filtrar (utilizada para optimizar la carga durante la construcción de las definiciones y base de datos para dispositivo)
	//@JAPR 2012-06-20: Agregado el enlace al catálogo desde los valores de la dimensión
	static $arrAllCatalogInstancesByParentID=array();
	//@JAPR	
	static $arrAllCatMemberInstances=array();
	static $arrAllQuestionCollectionsBySection=array();
	static $arrAllQuestionCollectionsBySurvey=array();
	//@JAPR 2017-02-21: Agregado el método para caché de secciones
	static $arrAllSectionCollectionsBySurvey=array();
	//@JAPR 2015-08-02: Optimizada la carga de catálogos con el cache de objetos
	static $arrAllCatMemberCollectionsByCatalog=array();
	//@JAPR 2015-07-21: Agregados los DataSource y DataSourceMembers
	static $arrAllDataMembersCollsByDataSource=array();
	//@JAPR 2014-07-09: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
	static $arrAllProfileRestInstances=array();
	//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
	static $arrAllProfileInstances=array();
	//@JAPR 2014-05-14: Agregados los catálogos tipo eBavel
	// Colección de valores de dimensiones (clase BITAMDimensionValue) indexados por "CBO".idCubo."DIM".idDimension."KEY".idValor
	static $arrAllCatalogValues=array();
	//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
	static $arrAllMappedSections=array();
	//@JAPR 2018-06-22: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	static $arrAllSectionFilterCollectionsBySection=array();
	static $arrAllQuestionFilterCollectionsByQuestion=array();
	static $arrAllQuestionOptionsInstances=array();
	static $arrAllQuestionOptionCollectionsByQuestion=array();
	static $arrAllSkipRulesCollectionsByQuestion=array();
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	static $arrAllChildrenToFillByQuestion=array();
	static $arrAllChildrenToExcludeByQuestion=array();
	static $arrAllSectionToFillByQuestion=array();
	static $arrAllSectionToFillBySection=array();
	static $arrAllSectionQuestionsBySourceSection=array();
	static $arrAllShowQuestionCollectionsByQuestion=array();
	static $arrAllCatMembersListByQuestion=array();
	static $arrAllShowCondDepByQuestion=array();
	static $arrAllFormulaChildDepByQuestion=array();
	static $arrAllQuestionsToEvalByQuestion=array();
	static $arrAllRolesByUser=array();
	static $arrAllKeyCatMembersIDsByCatalogWithID=array();
	static $arrKeyCatMembersDefByCatalogWithID=array();
	//Indica si se aplicó la carga masiva de la forma para que otros objetos puedan usar los resultados aunque no existan en los arrays de carga masiva (Si la forma está almacenada aquí,
	//entonces significa que si el array masivo no da resultados es porque no existe ese tipo de elemento, no porque no se hubiera cargado)
	static $arrSurveysWithFullLoadFromDB=array();
	//Lo mismo que el anterior pero para Catálogos (BITAMCatalog)
	static $arrCatalogsWithFullLoadFromDB=array();
	//@JAPR

	static function ObjectCount($aArrayName)
	{
		$intCount=0;
		$strCode='';
		
		if(trim($aArrayName)=='')
		{
			return 0;
		}
		
		$strCode='$intCount=count(self::'.$aArrayName.');';
		if($strCode!='')
		{
			eval($strCode);
		}
		
		return $intCount;
	}
	
	static function ResetCollections()
	{
		self::$arrAllUserNames=array();
		self::$arrAllClaDescripsOfCatalog=array();
		self::$arrAllClaDescripsOfCatMember=array();
		self::$arrAllSurveyInstances=array();
		self::$arrAllSectionInstances=array();
		self::$arrAllDynSectionOfSurveys=array();
		self::$arrAllCatalogInstances=array();
		self::$arrAllCatalogInstancesByParentID=array();
		self::$arrAllCatMemberInstances=array();
		self::$arrAllQuestionCollectionsBySection=array();
		self::$arrAllQuestionCollectionsBySurvey=array();
		//@JAPR 2017-02-21: Agregado el método para caché de secciones
		self::$arrAllSectionCollectionsBySurvey=array();
		//@JAPR 2015-08-02: Optimizada la carga de catálogos con el cache de objetos
		self::$arrAllCatMemberCollectionsByCatalog=array();
		//@JAPR 2015-07-21: Agregados los DataSource y DataSourceMembers
		self::$arrAllDataSourceInstances=array();
		self::$arrAllDataMemberInstances=array();
		self::$arrAllDataMembersCollsByDataSource=array();
		//@JAPR 2018-07-13 Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		self::$arrAllDataSourceColl=array();
		//@JAPR 2014-07-09: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		self::$arrAllProfileRestInstances=array();
		//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		self::$arrAllProfileInstances=array();
		//@JAPR 2014-05-14: Agregados los catálogos tipo eBavel
		self::$arrAllCatalogValues=array();
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		self::$arrAllMappedSections=array();
		//@JAPR 2018-06-22: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		self::$arrAllSectionFilterCollectionsBySection=array();
		self::$arrAllQuestionFilterCollectionsByQuestion=array();
		self::$arrAllQuestionOptionsInstances=array();
		self::$arrAllQuestionOptionCollectionsByQuestion=array();
		self::$arrAllSkipRulesCollectionsByQuestion=array();
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		self::$arrAllChildrenToFillByQuestion=array();
		self::$arrAllChildrenToExcludeByQuestion=array();
		self::$arrAllSectionToFillByQuestion=array();
		self::$arrAllSectionToFillBySection=array();
		self::$arrAllSectionQuestionsBySourceSection=array();
		self::$arrAllShowQuestionCollectionsByQuestion=array();
		self::$arrAllCatMembersListByQuestion=array();
		self::$arrAllShowCondDepByQuestion=array();
		self::$arrAllFormulaChildDepByQuestion=array();
		self::$arrAllQuestionsToEvalByQuestion=array();
		self::$arrAllRolesByUser=array();
		self::$arrAllKeyCatMembersIDsByCatalogWithID=array();
		self::$arrKeyCatMembersDefByCatalogWithID=array();
		self::$arrSurveysWithFullLoadFromDB=array();
		self::$arrCatalogsWithFullLoadFromDB=array();
		//@JAPR
	}

	static function &GetSurveyInstanceWithID($aSurveyID)
	{
		$strInstanceKey='SUV'.$aSurveyID;
		$theInstance=null;
		if(isset(self::$arrAllSurveyInstances[$strInstanceKey]))
		{
			$theInstance=self::$arrAllSurveyInstances[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddSurveyInstanceWithID($aSurveyID, &$aObject)
	{
		$strInstanceKey='SUV'.$aSurveyID;
		if(!isset(self::$arrAllSurveyInstances[$strInstanceKey]))
		{
			self::$arrAllSurveyInstances[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetSectionInstanceWithID($aSectionID)
	{
		$strInstanceKey='SEC'.$aSectionID;
		$theInstance=null;
		if(isset(self::$arrAllSectionInstances[$strInstanceKey]))
		{
			$theInstance=self::$arrAllSectionInstances[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddSectionInstanceWithID($aSectionID, &$aObject)
	{
		$strInstanceKey='SEC'.$aSectionID;
		if(!isset(self::$arrAllSectionInstances[$strInstanceKey]))
		{
			self::$arrAllSectionInstances[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetDynSectionOfSurveyWithID($aSurveyID)
	{
		$strInstanceKey='DYNSECSUV'.$aSurveyID;
		$theInstance=null;
		if(isset(self::$arrAllDynSectionOfSurveys[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDynSectionOfSurveys[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDynSectionOfSurveyWithID($aSurveyID, &$aObject)
	{
		$strInstanceKey='DYNSECSUV'.$aSurveyID;
		if(!isset(self::$arrAllDynSectionOfSurveys[$strInstanceKey]))
		{
			self::$arrAllDynSectionOfSurveys[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetCatMemberInstanceWithID($aCatMemberID)
	{
		$strInstanceKey='CATMEM'.$aCatMemberID;
		$theInstance=null;
		if(isset(self::$arrAllCatMemberInstances[$strInstanceKey]))
		{
			$theInstance=self::$arrAllCatMemberInstances[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddCatMemberInstanceWithID($aCatMemberID, &$aObject)
	{
		$strInstanceKey='CATMEM'.$aCatMemberID;
		if(!isset(self::$arrAllCatMemberInstances[$strInstanceKey]))
		{
			self::$arrAllCatMemberInstances[$strInstanceKey] =& $aObject;
		}
	}
	
	//@JAPR 2014-07-09: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
	static function &GetProfileRestInstanceWithID($aProfileID)
	{
		$strInstanceKey='PROREST'.$aProfileID;
		$theInstance=null;
		if(isset(self::$arrAllProfileRestInstances[$strInstanceKey]))
		{
			$theInstance=self::$arrAllProfileRestInstances[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddProfileRestInstanceWithID($aProfileID, &$aObject)
	{
		$strInstanceKey='PROREST'.$aProfileID;
		if(!isset(self::$arrAllProfileRestInstances[$strInstanceKey]))
		{
			self::$arrAllProfileRestInstances[$strInstanceKey] =& $aObject;
		}
	}
	
	//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
	static function &GetProfileInstanceWithID($aProfileID)
	{
		$strInstanceKey='PRO'.$aProfileID;
		$theInstance=null;
		if(isset(self::$arrAllProfileInstances[$strInstanceKey]))
		{
			$theInstance=self::$arrAllProfileInstances[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddProfileInstanceWithID($aProfileID, &$aObject)
	{
		$strInstanceKey='PRO'.$aProfileID;
		if(!isset(self::$arrAllProfileInstances[$strInstanceKey]))
		{
			self::$arrAllProfileInstances[$strInstanceKey] =& $aObject;
		}
	}
	//@JAPR
	
	static function &GetCatalogInstanceWithID($aCatalogID)
	{
		$strInstanceKey='CAT'.$aCatalogID;
		$theInstance=null;
		if(isset(self::$arrAllCatalogInstances[$strInstanceKey]))
		{
			$theInstance=self::$arrAllCatalogInstances[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddCatalogInstanceWithID($aCatalogID, &$aObject)
	{
		$strInstanceKey='CAT'.$aCatalogID;
		if(!isset(self::$arrAllCatalogInstances[$strInstanceKey]))
		{
			self::$arrAllCatalogInstances[$strInstanceKey] =& $aObject;
		}
	}
	
	//@JAPR 2015-07-21: Agregados los DataSource y DataSourceMembers
	static function &GetDataSourceInstanceWithID($aDataSourceID)
	{
		$strInstanceKey='DAT'.$aDataSourceID;
		$theInstance=null;
		if(isset(self::$arrAllDataSourceInstances[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDataSourceInstances[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDataSourceInstanceWithID($aDataSourceID, &$aObject)
	{
		$strInstanceKey='DAT'.$aDataSourceID;
		if(!isset(self::$arrAllDataSourceInstances[$strInstanceKey]))
		{
			self::$arrAllDataSourceInstances[$strInstanceKey] =& $aObject;
		}
	}
	
	//@JAPR 2018-07-13 Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	static function &GetDataSourceCollectionAll()
	{
		$strInstanceKey='DSCOLL';
		$theInstance=null;
		if(isset(self::$arrAllDataSourceColl[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDataSourceColl[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDataSourceCollectionAll(&$aObject)
	{
		$strInstanceKey='DSCOLL';
		if(!isset(self::$arrAllDataSourceColl[$strInstanceKey]))
		{
			self::$arrAllDataSourceColl[$strInstanceKey] =& $aObject;
		}
	}
	//@JAPR
	
	static function &GetDataMemberInstanceWithID($aDataSourceMemberID)
	{
		$strInstanceKey='DATM'.$aDataSourceMemberID;
		$theInstance=null;
		if(isset(self::$arrAllDataMemberInstances[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDataMemberInstances[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDataMemberInstanceWithID($aDataSourceMemberID, &$aObject)
	{
		$strInstanceKey='DATM'.$aDataSourceMemberID;
		if(!isset(self::$arrAllDataMemberInstances[$strInstanceKey]))
		{
			self::$arrAllDataMemberInstances[$strInstanceKey] =& $aObject;
		}
	}
	
	//@JAPR 2014-05-14: Agregados los catálogos tipo eBavel
	static function &GetCatalogValueInstanceWithID($aCatalogID, $aValueID)
	{
		$strInstanceKey='CAT'.$aCatalogID.'KEY'.$aValueID;
		$theInstance=null;
		if(isset(self::$arrAllCatalogValues[$strInstanceKey]))
		{
			$theInstance=self::$arrAllCatalogValues[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddCatalogValueInstanceWithID($aCatalogID, $aValueID, &$aObject)
	{
		$strInstanceKey='CAT'.$aCatalogID.'KEY'.$aValueID;
		//@JAPR 2014-10-03: Corregido un bug, se estaba usando una referencia a una variable equivocada
		if(!isset(self::$arrAllCatalogValues[$strInstanceKey]))
		{
			self::$arrAllCatalogValues[$strInstanceKey] =& $aObject;
		}
	}
	//@JAPR
	
	//@JAPR 2012-06-20: Agregado el enlace al catálogo desde los valores de la dimensión
	static function &GetCatalogInstanceWithParentID($aParentID)
	{
		$strInstanceKey='CATPRNT'.$aParentID;
		$theInstance=null;
		if(isset(self::$arrAllCatalogInstancesByParentID[$strInstanceKey]))
		{
			$theInstance=self::$arrAllCatalogInstancesByParentID[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddCatalogInstanceWithParentID($aParentID, &$aObject)
	{
		$strInstanceKey='CATPRNT'.$aParentID;
		if(!isset(self::$arrAllCatalogInstancesByParentID[$strInstanceKey]))
		{
			self::$arrAllCatalogInstancesByParentID[$strInstanceKey] =& $aObject;
		}
	}
	//@JAPR
	
	static function &GetQuestionCollectionBySectionWithID($aSectionID)
	{
		$strInstanceKey='QCOLLBYSEC'.$aSectionID;
		$theInstance=null;
		if(isset(self::$arrAllQuestionCollectionsBySection[$strInstanceKey]))
		{
			$theInstance=self::$arrAllQuestionCollectionsBySection[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddQuestionCollectionBySectionWithID($aSectionID, &$aObject)
	{
		$strInstanceKey='QCOLLBYSEC'.$aSectionID;
		if(!isset(self::$arrAllQuestionCollectionsBySection[$strInstanceKey]))
		{
			self::$arrAllQuestionCollectionsBySection[$strInstanceKey] =& $aObject;
		}
	}
	
	//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	static function &GetQuestionOptionInstanceWithID($aQuestionOptionID)
	{
		$strInstanceKey='QO'.$aQuestionOptionID;
		$theInstance=null;
		if(isset(self::$arrAllQuestionOptionsInstances[$strInstanceKey]))
		{
			$theInstance=self::$arrAllQuestionOptionsInstances[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddQuestionOptionInstanceWithID($aQuestionOptionID, &$aObject)
	{
		$strInstanceKey='QO'.$aQuestionOptionID;
		if(!isset(self::$arrAllQuestionOptionsInstances[$strInstanceKey]))
		{
			self::$arrAllQuestionOptionsInstances[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetQuestionOptionCollectionByQuestionWithID($aQuestionID)
	{
		$strInstanceKey='QOCOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllQuestionOptionCollectionsByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllQuestionOptionCollectionsByQuestion[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddQuestionOptionCollectionByQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='QOCOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllQuestionOptionCollectionsByQuestion[$strInstanceKey]))
		{
			self::$arrAllQuestionOptionCollectionsByQuestion[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetSkipRulesCollectionByQuestionWithID($aQuestionID)
	{
		$strInstanceKey='SRCOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllSkipRulesCollectionsByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllSkipRulesCollectionsByQuestion[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddSkipRulesCollectionByQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='SRCOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllSkipRulesCollectionsByQuestion[$strInstanceKey]))
		{
			self::$arrAllSkipRulesCollectionsByQuestion[$strInstanceKey] =& $aObject;
		}
	}
	
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	static function &GetChildrenToFillByQuestionWithID($aQuestionID)
	{
		$strInstanceKey='CHILDCOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllChildrenToFillByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllChildrenToFillByQuestion[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddChildrenToFillByQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='CHILDCOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllChildrenToFillByQuestion[$strInstanceKey]))
		{
			self::$arrAllChildrenToFillByQuestion[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetChildrenToExcludeByQuestionWithID($aQuestionID)
	{
		$strInstanceKey='CHILDECOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllChildrenToExcludeByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllChildrenToExcludeByQuestion[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddChildrenToExcludeByQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='CHILDECOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllChildrenToExcludeByQuestion[$strInstanceKey]))
		{
			self::$arrAllChildrenToExcludeByQuestion[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetSectionsToFillByQuestionWithID($aQuestionID)
	{
		$strInstanceKey='SFILLCOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllSectionToFillByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllSectionToFillByQuestion[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddSectionsToFillByQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='SFILLCOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllSectionToFillByQuestion[$strInstanceKey]))
		{
			self::$arrAllSectionToFillByQuestion[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetSectionsToFillBySectionWithID($aSectionID)
	{
		$strInstanceKey='SFILLCOLLBYS'.$aSectionID;
		$theInstance=null;
		if(isset(self::$arrAllSectionToFillBySection[$strInstanceKey]))
		{
			$theInstance=self::$arrAllSectionToFillBySection[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddSectionsToFillBySectionWithID($aSectionID, &$aObject)
	{
		$strInstanceKey='SFILLCOLLBYS'.$aSectionID;
		if(!isset(self::$arrAllSectionToFillBySection[$strInstanceKey]))
		{
			self::$arrAllSectionToFillBySection[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetSectionQuestionsBySourceSectionWithID($aSectionID)
	{
		$strInstanceKey='SQCOLLBYS'.$aSectionID;
		$theInstance=null;
		if(isset(self::$arrAllSectionQuestionsBySourceSection[$strInstanceKey]))
		{
			$theInstance=self::$arrAllSectionQuestionsBySourceSection[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddSectionQuestionsBySourceSectionWithID($aSectionID, &$aObject)
	{
		$strInstanceKey='SQCOLLBYS'.$aSectionID;
		if(!isset(self::$arrAllSectionQuestionsBySourceSection[$strInstanceKey]))
		{
			self::$arrAllSectionQuestionsBySourceSection[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetShowQuestionCollectionByQuestionWithID($aQuestionID)
	{
		$strInstanceKey='SHQCOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllShowQuestionCollectionsByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllShowQuestionCollectionsByQuestion[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddShowQuestionCollectionByQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='SHQCOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllShowQuestionCollectionsByQuestion[$strInstanceKey]))
		{
			self::$arrAllShowQuestionCollectionsByQuestion[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetCatMembersListByQuestionWithID($aQuestionID)
	{
		$strInstanceKey='CMLSTCOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllCatMembersListByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllCatMembersListByQuestion[$strInstanceKey];
		}
		if ( is_null($theInstance) ) {
			$theInstance = array();
		}
		return $theInstance;
	}
	
	static function AddCatMembersListByQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='CMLSTCOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllCatMembersListByQuestion[$strInstanceKey]))
		{
			self::$arrAllCatMembersListByQuestion[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetShowCondDepByQuestionWithID($aQuestionID)
	{
		$strInstanceKey='SHCNDCOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllShowCondDepByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllShowCondDepByQuestion[$strInstanceKey];
		}
		if ( is_null($theInstance) ) {
			$theInstance = array();
		}
		return $theInstance;
	}
	
	static function AddShowCondDepByQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='SHCNDCOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllShowCondDepByQuestion[$strInstanceKey]))
		{
			self::$arrAllShowCondDepByQuestion[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetFormulaChildDepByQuestionWithID($aQuestionID)
	{
		$strInstanceKey='FORDCOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllFormulaChildDepByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllFormulaChildDepByQuestion[$strInstanceKey];
		}
		if ( is_null($theInstance) ) {
			$theInstance = array();
		}
		return $theInstance;
	}
	
	static function AddFormulaChildDepByQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='FORDCOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllFormulaChildDepByQuestion[$strInstanceKey]))
		{
			self::$arrAllFormulaChildDepByQuestion[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetQuestionToEvalQuestionWithID($aQuestionID)
	{
		$strInstanceKey='QEVALCOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllQuestionsToEvalByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllQuestionsToEvalByQuestion[$strInstanceKey];
		}
		if ( is_null($theInstance) ) {
			$theInstance = array();
		}
		return $theInstance;
	}
	
	static function AddQuestionToEvalQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='QEVALCOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllQuestionsToEvalByQuestion[$strInstanceKey]))
		{
			self::$arrAllQuestionsToEvalByQuestion[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetRolesByUserWithID($aUserID)
	{
		$strInstanceKey='ROLCOLLBYUSR'.$aUserID;
		$theInstance=null;
		if(isset(self::$arrAllRolesByUser[$strInstanceKey]))
		{
			$theInstance=self::$arrAllRolesByUser[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddRolesByUserWithID($aUserID, &$aObject)
	{
		$strInstanceKey='ROLCOLLBYUSR'.$aUserID;
		if(!isset(self::$arrAllRolesByUser[$strInstanceKey]))
		{
			self::$arrAllRolesByUser[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetKeyCatMembersIDsByCatalogWithID($aCatalogID)
	{
		$strInstanceKey='KATTRCOLLBYC'.$aCatalogID;
		$theInstance=null;
		if(isset(self::$arrAllKeyCatMembersIDsByCatalogWithID[$strInstanceKey]))
		{
			$theInstance=self::$arrAllKeyCatMembersIDsByCatalogWithID[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddKeyCatMembersIDsByCatalogWithID($aCatalogID, &$aObject)
	{
		$strInstanceKey='KATTRCOLLBYC'.$aCatalogID;
		if(!isset(self::$arrAllKeyCatMembersIDsByCatalogWithID[$strInstanceKey]))
		{
			self::$arrAllKeyCatMembersIDsByCatalogWithID[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetCatMembersDefByCatalogWithID($aCatalogID)
	{
		$strInstanceKey='CMDEFCOLLBYC'.$aCatalogID;
		$theInstance=null;
		if(isset(self::$arrKeyCatMembersDefByCatalogWithID[$strInstanceKey]))
		{
			$theInstance=self::$arrKeyCatMembersDefByCatalogWithID[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddCatMembersDefByCatalogWithID($aCatalogID, &$aObject)
	{
		$strInstanceKey='CMDEFCOLLBYC'.$aCatalogID;
		if(!isset(self::$arrKeyCatMembersDefByCatalogWithID[$strInstanceKey]))
		{
			self::$arrKeyCatMembersDefByCatalogWithID[$strInstanceKey] =& $aObject;
		}
	}
	
	static function IsSurveyFullyLoadedFromDB($aSurveyID)
	{
		$theInstance = false;
		if(isset(self::$arrSurveysWithFullLoadFromDB[$aSurveyID]))
		{
			$theInstance=true;
		}
		return $theInstance;
	}
	
	static function AddSurveyFullyLoadedFromDB($aSurveyID)
	{
		if(!isset(self::$arrSurveysWithFullLoadFromDB[$aSurveyID]))
		{
			self::$arrSurveysWithFullLoadFromDB[$aSurveyID] = $aSurveyID;
		}
	}
	
	static function IsCatalogFullyLoadedFromDB($aCatalogID)
	{
		$theInstance = false;
		if(isset(self::$arrCatalogsWithFullLoadFromDB[$aCatalogID]))
		{
			$theInstance=true;
		}
		return $theInstance;
	}
	
	static function AddCatalogFullyLoadedFromDB($aCatalogID)
	{
		if(!isset(self::$arrCatalogsWithFullLoadFromDB[$aCatalogID]))
		{
			self::$arrCatalogsWithFullLoadFromDB[$aCatalogID] = $aCatalogID;
		}
	}
	
	//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "."
	//@JAPR 2017-02-21: Agregado el método para caché de secciones
	static function &GetSectionCollectionBySurveyWithID($aSurveyID)
	{
		$strInstanceKey='SCOLLBYSV'.$aSurveyID;
		$theInstance=null;
		if(isset(self::$arrAllSectionCollectionsBySurvey[$strInstanceKey]))
		{
			$theInstance=self::$arrAllSectionCollectionsBySurvey[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddSectionCollectionBySurveyWithID($aSurveyID, &$aObject)
	{
		$strInstanceKey='SCOLLBYSV'.$aSurveyID;
		if(!isset(self::$arrAllSectionCollectionsBySurvey[$strInstanceKey]))
		{
			self::$arrAllSectionCollectionsBySurvey[$strInstanceKey] =& $aObject;
		}
	}
	
	//@JAPR 2018-06-22: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	static function &GetSectionFilterCollectionBySectionWithID($aSectionID)
	{
		$strInstanceKey='SFCOLLBYS'.$aSectionID;
		$theInstance=null;
		if(isset(self::$arrAllSectionFilterCollectionsBySection[$strInstanceKey]))
		{
			$theInstance=self::$arrAllSectionFilterCollectionsBySection[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddSectionFilterCollectionBySectionWithID($aSectionID, &$aObject)
	{
		$strInstanceKey='SFCOLLBYS'.$aSectionID;
		if(!isset(self::$arrAllSectionFilterCollectionsBySection[$strInstanceKey]))
		{
			self::$arrAllSectionFilterCollectionsBySection[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetQuestionFilterCollectionByQuestionWithID($aQuestionID)
	{
		$strInstanceKey='SFCOLLBYQ'.$aQuestionID;
		$theInstance=null;
		if(isset(self::$arrAllQuestionFilterCollectionsByQuestion[$strInstanceKey]))
		{
			$theInstance=self::$arrAllQuestionFilterCollectionsByQuestion[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddQuestionFilterCollectionByQuestionWithID($aQuestionID, &$aObject)
	{
		$strInstanceKey='SFCOLLBYQ'.$aQuestionID;
		if(!isset(self::$arrAllQuestionFilterCollectionsByQuestion[$strInstanceKey]))
		{
			self::$arrAllQuestionFilterCollectionsByQuestion[$strInstanceKey] =& $aObject;
		}
	}
	//@JAPR
	
	static function &GetQuestionCollectionBySurveyWithID($aSurveyID)
	{
		$strInstanceKey='QCOLLBYSV'.$aSurveyID;
		$theInstance=null;
		if(isset(self::$arrAllQuestionCollectionsBySurvey[$strInstanceKey]))
		{
			$theInstance=self::$arrAllQuestionCollectionsBySurvey[$strInstanceKey];
		}
		return $theInstance;
	}
	
	//@JAPR 2015-08-02: Optimizada la carga de catálogos con el cache de objetos
	static function AddCatMemberCollectionByCatalogWithID($aCatalogID, &$aObject)
	{
		$strInstanceKey='CMCOLLBYCAT'.$aCatalogID;
		if(!isset(self::$arrAllCatMemberCollectionsByCatalog[$strInstanceKey]))
		{
			self::$arrAllCatMemberCollectionsByCatalog[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetCatMemberCollectionByCatalogWithID($aCatalogID)
	{
		$strInstanceKey='CMCOLLBYCAT'.$aCatalogID;
		$theInstance=null;
		if(isset(self::$arrAllCatMemberCollectionsByCatalog[$strInstanceKey]))
		{
			$theInstance=self::$arrAllCatMemberCollectionsByCatalog[$strInstanceKey];
		}
		return $theInstance;
	}
	//@JAPR
	
	static function AddQuestionCollectionBySurveyWithID($aSurveyID, &$aObject)
	{
		$strInstanceKey='QCOLLBYSV'.$aSurveyID;
		if(!isset(self::$arrAllQuestionCollectionsBySurvey[$strInstanceKey]))
		{
			self::$arrAllQuestionCollectionsBySurvey[$strInstanceKey] =& $aObject;
		}
	}
	
	//@JAPR 2015-07-21: Agregados los DataSource y DataSourceMembers
	static function &GetDataMembersCollByDataSourceID($aDataSourceID)
	{
		$strInstanceKey='DATMCOLL'.$aDataSourceID;
		$theInstance=null;
		if(isset(self::$arrAllDataMembersCollsByDataSource[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDataMembersCollsByDataSource[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDataMembersCollByDataSourceID($aDataSourceID, &$aObject)
	{
		$strInstanceKey='DATMCOLL'.$aDataSourceID;
		if(!isset(self::$arrAllDataMembersCollsByDataSource[$strInstanceKey]))
		{
			self::$arrAllDataMembersCollsByDataSource[$strInstanceKey] =& $aObject;
		}
	}
	//@JAPR
	
	static function &GetUserNameWithID($anUserID)
	{
		$strInstanceKey='USNAME'.$anUserID;
		$theInstance=null;
		if(isset(self::$arrAllUserNames[$strInstanceKey]))
		{
			$theInstance=self::$arrAllUserNames[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddUserNameWithID($anUserID, &$aObject)
	{
		$strInstanceKey='USNAME'.$anUserID;
		if(!isset(self::$arrAllUserNames[$strInstanceKey]))
		{
			self::$arrAllUserNames[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetClaDescripWithCatalogID($aCatalogID)
	{
		$strInstanceKey='CAT'.$aCatalogID;
		$theInstance=null;
		if(isset(self::$arrAllClaDescripsOfCatalog[$strInstanceKey]))
		{
			$theInstance=self::$arrAllClaDescripsOfCatalog[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddClaDescripWithCatalogID($aCatalogID, &$aObject)
	{
		$strInstanceKey='CAT'.$aCatalogID;
		if(!isset(self::$arrAllClaDescripsOfCatalog[$strInstanceKey]))
		{
			self::$arrAllClaDescripsOfCatalog[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetClaDescripWithCatMemberID($aCatalogID, $aCatMemberID)
	{
		$strInstanceKey='CAT'.$aCatalogID.'CATMEM'.$aCatMemberID;
		$theInstance=null;
		if(isset(self::$arrAllClaDescripsOfCatMember[$strInstanceKey]))
		{
			$theInstance=self::$arrAllClaDescripsOfCatMember[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddClaDescripWithCatMemberID($aCatalogID, $aCatMemberID, &$aObject)
	{
		$strInstanceKey='CAT'.$aCatalogID.'CATMEM'.$aCatMemberID;
		if(!isset(self::$arrAllClaDescripsOfCatMember[$strInstanceKey]))
		{
			self::$arrAllClaDescripsOfCatMember[$strInstanceKey] =& $aObject;
		}
	}
	
	//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
	static function &GetMappedSectionsWithID($aSurveyID)
	{
		$strInstanceKey='SUV'.$aSurveyID;
		$theInstance=null;
		if(isset(self::$arrAllMappedSections[$strInstanceKey]))
		{
			$theInstance=self::$arrAllMappedSections[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddMappedSectionsWithID($aSurveyID, &$aObject)
	{
		$strInstanceKey='SUV'.$aSurveyID;
		if(!isset(self::$arrAllMappedSections[$strInstanceKey]))
		{
			self::$arrAllMappedSections[$strInstanceKey] =& $aObject;
		}
	}
	//@JAPR
}
?>