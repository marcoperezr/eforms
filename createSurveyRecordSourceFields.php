<?php
//Agrega las columnas con el ID de sección y ID de pregunta categoría de dimensión para la tabla paralela, con lo cual se podrá identificar
//si un registro capturado corresponde con alguno de esos elementos
//Campos EntrySectionID, EntryCatDimID
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	echo("<BR><BR>***** "."Survey: ".$surveyInstance->SurveyName);
	//Crear campo EntrySectionID
	$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD EntrySectionID INT NULL";
	echo("<BR>"."sql:"."<BR>".$sql);
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>Field already exists");
	}
	else {
		echo("<BR>EntrySectionID field added");
	}

	$sql = "UPDATE ".$surveyInstance->SurveyTable." SET EntrySectionID = -1 WHERE EntrySectionID IS NULL";
	echo("<BR>"."sql:"."<BR>".$sql);
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>Updating EntrySectionID for previos eForms versions");
	}
	
	//Crear campo EntryCatDimID
	$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD EntryCatDimID INT NULL";
	echo("<BR>"."sql:"."<BR>".$sql);
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>Field already exists");
	}
	else {
		echo("<BR>EntryCatDimID field added");
	}
	
	$sql = "UPDATE ".$surveyInstance->SurveyTable." SET EntryCatDimID = -1 WHERE EntryCatDimID IS NULL";
	echo("<BR>"."sql:"."<BR>".$sql);
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>Updating EntryCatDimID for previous eForms versions");
	}
	
	//@JAPR 2013-01-12: Agregado el campo para identificación del registro de página de una sección dinámica
	//Crear campo eFormsVersionNum
	$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD eFormsVersionNum VARCHAR(10) NULL";
	echo("<BR>"."sql:"."<BR>".$sql);
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>Field already exists");
	}
	else {
		echo("<BR>eFormsVersionNum field added");
	}
	
	$sql = "UPDATE ".$surveyInstance->SurveyTable." SET eFormsVersionNum = '' WHERE eFormsVersionNum IS NULL";
	echo("<BR>"."sql:"."<BR>".$sql);
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>Updating eFormsVersionNum for previous eForms versions");
	}
	
	//Crear campo eFormsVersionNum
	$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD DynamicPageDSC VARCHAR(255) NULL";
	echo("<BR>"."sql:"."<BR>".$sql);
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>Field already exists");
	}
	else {
		echo("<BR>DynamicPageDSC field added");
	}
	
	$sql = "UPDATE ".$surveyInstance->SurveyTable." SET DynamicPageDSC = '' WHERE DynamicPageDSC IS NULL";
	echo("<BR>"."sql:"."<BR>".$sql);
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>Updating DynamicPageDSC for previous eForms versions");
	}
}

echo("<BR><BR><BR>***** Process finished *****");
?>