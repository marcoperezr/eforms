<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

if(array_key_exists("surveyID",$_GET) && array_key_exists("questionID",$_GET))
{
    
        $surveyID = $_GET["surveyID"];
	$questionID = $_GET["questionID"];
    
    $strTMPName = "photo_".$surveyID."_".$questionID."_".$_SESSION["PABITAM_UserID"].".jpg";
    //$path = "surveytmpimages/".$theRepositoryName."/".$strPath;
    $path = "tmpsurveyimages/".$strTMPName;
	//$path = str_replace("\\", "/", $path);
	header("Location: ".$path);
}
?>