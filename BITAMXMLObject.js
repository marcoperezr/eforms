/**
* BITAMXMLObject 1.0 041206
* by Fernando Deutsch
* Copyright (c) 1997-2003 BITAM
*
* This software is provided "AS IS," without a warranty of any kind.
**/

function getXMLObject()
{
	var xmlhttp = null;
	/*@cc_on @*/
	/*@if (@_jscript_version >= 5)
  		try 
	  	{
	  		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
 		} 
 		catch (e) 
 		{
  			try 
  			{
    				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  			}
  			catch (E) 
  			{
  				xmlhttp = null;
  			}
 		}
	@else
 		xmlhttp = null;
	@end @*/

	if (xmlhttp == null && typeof XMLHttpRequest != 'undefined') 
	{
 		try 
 		{
  			xmlhttp = new XMLHttpRequest();
 		}
 		catch (e) 
 		{
  			xmlhttp = null
 		}
	}

	return(xmlhttp);
}