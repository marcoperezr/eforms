<?php
?>
&nbsp;
<br>

<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
				var tinyMCEmode = true; //Conchita 16-nov-2011 cambio para toggle tinymce
					tinyMCE.init({
						// General options
						mode : "exact",
						readonly : 0,
				        elements : "SurveyImageText,ajaxfilemanager",
				      	theme : "advanced",
				      	width: 150,
        				height: 150,
						file_browser_callback : "ajaxfilemanager",
						handle_event_callback : "formattedtextchanged",
						entity_encoding : "raw",
						object_resizing : true,
						theme_advanced_buttons1 : "image",
						theme_advanced_buttons2: "",
						theme_advanced_buttons3: "",
						theme_advanced_buttons4: "",		
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "none",
						theme_advanced_resizing : true,
						//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
						theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
        										"Arial=arial,helvetica,sans-serif;"+
        										"Arial Black=arial black,avant garde;"+
        										"Book Antiqua=book antiqua,palatino;"+
								                "Comic Sans MS=comic sans ms,sans-serif;"+
								                "Courier New=courier new,courier;"+
								                "Georgia=georgia,palatino;"+
								                "Helvetica=helvetica;"+
								                "Impact=impact,chicago;"+
								                "Symbol=symbol;"+
								                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
								                "Terminal=terminal,monaco;"+
								                "Times New Roman=times new roman,times;"+
								                "Trebuchet MS=trebuchet ms,geneva;"+
								                "Verdana=verdana,geneva;"+
								                "Webdings=webdings;"+
								                "Wingdings=wingdings,zapf dingbats",

   						relative_urls : false,
						content_css : "",
						convert_urls : false,
						editor_selector : "mceSimple",
						// Drop lists for link/image/media/template dialogs
						external_link_list_url : "lists/link_list.js",
						save_callback : "myCustomSaveContent",
						oninit: "myInitTinyMCE",
						cleanup: false,
						verify_html : false,
						forced_root_block : false
					});
					
					function myInitTinyMCE() {
						/*
						if (tinyMCE && tinyMCE.editors && tinyMCE.editors["SurveyImageText"]) {
							var htmlSrc = tinyMCE.editors["SurveyImageText"].getContent()
							
							if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
								toogleEditorMode('SurveyImageText');
							}
						}
						*/
					}
					
					function myCustomSaveContent(element_id, html, body) {
					        // Do some custom HTML cleanup
					        if (html) {
					        	html = html.replace(/\/\/ <!\[cdata\[/gi, '').replace(/\/\/ \]\]>/gi,'');
					        }
					
					        return html;
					}
					
					function formattedtextchanged(event) {
						
						var htmlSrc = tinyMCE.activeEditor.getContent();
			            
						if(event.type == "keydown") {
							
						}
						
						/*
						var rexp = [a-zA-z0-9\t\n ./<>?;:"'`!@#$%^&*()[]{}_+=-|\\];
			          
			            var cleanSrc = htmlSrc.replace(rexp,"");
			      		*/
			         
					}

					function ajaxfilemanager(field_name, url, type, win) 
					{
						var ajaxfilemanagerurl = "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
						var view = 'detail';
						switch (type) 
						{
							case "image":
								view = 'thumbnail';
								break;
							case "media":
								break;
							case "flash": 
								break;
							case "file":
								break;
							default:
								return false;
						}
    					
						tinyMCE.activeEditor.windowManager.open({
							url: "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
							width: 900,
        					height: 600,
							inline : "yes",
							close_previous : "no"
						},{
							window : win,
							input : field_name
						}); 
					}
				//Inicio conchita 16-nov-2011 toggle tinymce
            					
        function toogleEditorMode(sEditorID) { 
	        var x;
	        var tmp;
        
            try { 
                if(tinyMCEmode) { 
                	//x=removeHTML();
               		tinyMCE.execCommand('mceRemoveControl', false, sEditorID); 
                   	//document.getElementById(sEditorID).value=x; 
                    tinyMCEmode = false; 
                } else {   tinyMCE.execCommand('mceAddControl', false, sEditorID);
                    //tinyMCE.addMCEControl(document.getElementById(''), sEditorID); 
                    tinyMCEmode = true; 
                } 
            } catch(e) { 
            	alert('An error occur, try again. Error: '+ e);
                //error handling 
            } 
        }


        function removeHTML() {
            var htmlSrc = tinyMCE.activeEditor.getContent();
            var rexp = /<[^<>]+>/g;
          
            var cleanSrc = htmlSrc.replace(rexp,"");
      
            return cleanSrc;
        }
//Fin Conchita toggle tinymce 16-nov-2011	
</script>

<div style="position:absolute; display: block; width: 1357px;height: 157px; " id="disableSurveyImage"></div>

<div id="surveyImageTextArea" width="100%">
	<table class="object_table" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tbody>
		<tr>
			<td class="object_field_value_3" colspan="3">
				
				<textarea class="mceSimple" id="SurveyImageText" name="SurveyImageText" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
					<?=$this->SurveyImageText?>
				</textarea>
			</td>
		</tr>
		
	</tbody>
	</table>
</div>
