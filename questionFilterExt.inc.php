<?php
require_once("questionFilter.inc.php");
require_once("object.trait.php");

class BITAMQuestionFilterExt extends BITAMQuestionFilter
{
	use BITAMObjectExt;
	function __construct($aRepository, $aQuestionID, $aFilterType = null)
	{
		parent::__construct($aRepository, $aQuestionID, $aFilterType);
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
	}
	
	//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
	function hideBackButton($aUser) {
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=QuestionFilterExt&QuestionID=".$this->QuestionID;
		}
		else
		{
			return "BITAM_PAGE=QuestionFilterExt&QuestionID=".$this->QuestionID."&FilterID=".$this->FilterID;
		}
	}
	
	function generateAfterFormCode($aUser)
	{
 		parent::generateAfterFormCode($aUser);
		
		$myFormName = get_class($this);
		
		//Si se trata de un objeto nuevo, el grabado se debe hacer hacia un archivo alternativo que procese todos estos requests
		if ($this->isNewObject()) {
?>
	 	<script language="JavaScript">
			<?=$myFormName?>_SaveForm.action = '<?='processRequest.php?Process=Add&ObjectType='.otyQuestionFilter.'&'.$this->get_QueryString()?>&SelSurveyID=<?=$this->SurveyID?>';
		</script>
<?
		}
	}
}

class BITAMQuestionFilterExtCollection extends BITAMQuestionFilterCollection
{
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=QuestionFilterExt&QuestionID=".$this->QuestionID."&SelSurveyID=".$this->SurveyID;
	}
}
?>