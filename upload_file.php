<?php

require_once('config.php');


class UploadFile
{

	var $field_name;
	var $stored_file_name;
	var $original_file_name;
	var $temp_file_location;
	var $use_soap = false;
	var $file;
	var $file_ext;
	var $sError;
	
    function UploadFile ($field_name)
    {
		$this->field_name = $field_name;
		$this->sError = '';
    }

    function set_for_soap($filename, $file){
    	$this->stored_file_name = $filename;
    	$this->use_soap = true;
    	$this->file = $file;
    }

	function get_url($stored_file_name,$bean_id)
	{
		global $ekt_config;
		return UploadFile::get_file_path($stored_file_name,$bean_id);
	}
	
	function get_file_path($stored_file_name,$bean_id)
	{
		global $ekt_config;
		return $ekt_config['upload_dir'] . $bean_id . rawurlencode($stored_file_name);
	}

	function duplicate_file($old_id, $new_id, $file_name)
	{
		global $ekt_config;

		$source = $ekt_config['upload_dir'] . $old_id . $file_name;
		$destination = $ekt_config['upload_dir'] . $new_id . $file_name;
		copy($source, $destination);
	}

	function confirm_upload()
	{
		$intMaxUploadFileSize = getMaxUploadFileSize();
		
		if (!is_uploaded_file($_FILES[$this->field_name]['tmp_name']) )
		{
			return false;
		}
		else if ($_FILES[$this->field_name]['size'] > $intMaxUploadFileSize)
		{
			die("ERROR: uploaded file was too big: max filesize: {$ekt_config['upload_maxsize']}");
		}

		/*
		if(!is_writable($ekt_config['upload_dir']))
		{
			die ("ERROR: cannot write to directory: {$ekt_config['upload_dir']} for uploads");
		}
		*/

		$this->mime_type =$this->getMime($_FILES[$this->field_name]);
		$this->stored_file_name = $this->create_stored_filename();
		$this->temp_file_location = $_FILES[$this->field_name]['tmp_name'];

		return true;
	}

	function getMimeSoap($filename){

		if( function_exists( 'ext2mime' ) )
		{
			$mime = ext2mime($filename);
		}
		else
		{
			$mime = ' application/octet-stream';
		}
		return $mime;

	}
	function getMime(&$_FILES_element)
	{

		$filename = $_FILES_element['name'];

		if( $_FILES_element['type'] )
		{
			$mime = $_FILES_element['type'];
		}
		elseif( function_exists( 'mime_content_type' ) )
		{
			$mime = mime_content_type( $_FILES_element['tmp_name'] );
		}
		elseif( function_exists( 'ext2mime' ) )
		{
			$mime = ext2mime( $_FILES_element['name'] );
		}
		else
		{
			$mime = ' application/octet-stream';
		}
		return $mime;
	}

	function get_stored_file_name()
	{
		return $this->stored_file_name;
	}
	
	function set_stored_file_name($filename)
	{
		$this->stored_file_name = $filename;
	}
	

	function create_stored_filename()
	{
		global $ekt_config;
		if(!$this->use_soap){
           $stored_file_name = $_FILES[$this->field_name]['name'];
		}else{
			$stored_file_name = $this->stored_file_name;
		}
		$this->original_file_name = $stored_file_name;
        $ext_pos = strrpos($stored_file_name, ".");

		$this->file_ext = substr($stored_file_name, $ext_pos + 1);
		
		if(isset($ekt_config['upload_badext']))
		{
	        if (in_array($this->file_ext, $ekt_config['upload_badext']))
			{
	            $stored_file_name .= ".txt";
				$this->file_ext="txt";
	        }
		}
		elseif(isset($ekt_config['upload_goodext']))
		{
			if (!in_array($this->file_ext, $ekt_config['upload_goodext']))
			{
	            $stored_file_name .= ".txt";
				$this->file_ext="txt";
	        }
		}
		
		return $stored_file_name;
	}

	function final_move($bean_id)
	{
		global $ekt_config;

        $destination = $this->get_upload_path($bean_id);
        if($this->use_soap){
        	$fp = fopen($destination, 'wb');
        	if(!fwrite($fp, $this->file)){
        		die ("ERROR: can't save file to $destination");
        	}
        	fclose($fp);
        }else{
			if (!move_uploaded_file($_FILES[$this->field_name]['tmp_name'], $destination))
                {
					die ("ERROR: can't move_uploaded_file to $destination. You should try making the directory writable by the webserver");
                }
        }
		 return true;
	}

	function get_upload_path($bean_id){
			global $ekt_config;
			 $file_name = $bean_id.$this->stored_file_name;
			 return $ekt_config['upload_dir'].$file_name;
	}

	function unlink_file($bean_id,$file_name) {
		global $ekt_config;
        return unlink($ekt_config['upload_dir'].$bean_id.$file_name);
    }
}
?>