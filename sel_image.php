<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="JavaScript" src="js/EditorHTML/dialogs.js"></script>
<script language='JavaScript' src='js/EditorHTML/utils.js'></script>
<script language="JavaScript">

var window_dialogArguments = getDialogArgument();

document.writeln("<title>" + window_dialogArguments[1] + "</title>");
owner = window_dialogArguments[0];

</script>
<?php
$bGetTemplate = (isset($_GET['template']))?true:false;
$sAddUrlIFRAME = ($bGetTemplate) ? '?template=1' : '';
require_once('sel_image.inc.php');
global $url_images, $img_types, $img_names;
$sWidthDivs = '98%';
?>
<script language="JavaScript">
<?php
	echo $url_images."\n";
	echo $img_types."\n";
	echo $img_names."\n";
?>
</script>
<script language='JavaScript' src='js/EditorHTML/sel_image.js'></script>
<style type='text/css'>@import url('css/fudgeEditorHTML.css');</style>
	</head>
<body onload="OnStart();" class="NBody">
<div id="id_div_header" style="position:absolute;left:10px;top:5px;overflow:auto;width:<?=$sWidthDivs?>">
</div>
<div id="id_div" style="position:absolute;left:10px;top:30px;overflow:auto;height:290px;width:<?=$sWidthDivs?>">
</div>
<iframe id="iFrmImg" name="oFrmImg" src="loadimage.php<?=$sAddUrlIFRAME?>" frameborder="no" style="overflow:hidden; position:absolute; bottom: 50px; left: 5px; width:370px; height:80px;"></iframe>
<table width="<?=$sWidthDivs?>" cellpadding="0" cellspacing="0" id="tbButtons" border="0" style="position: absolute; bottom: 25px; left: 10px">
	<tr>
		<td align="right">
			<input id=botBack class="Nbutton" type="button" name="Back" onclick="OnPrevious();" style="display:none;">
			<input id=botOK class="Nbutton" type="button" name="OK" onclick="On_Ok();">
			&nbsp;
			<input id=botCancel class="Nbutton" type="button" name="Cancel" onclick="closeDialog();">
		</td>
	</tr>
</table>
</body>
</html>