<?
define("FBM_PATH", "e:\\inetpub\\wwwroot\\fbm");
$FBM_PATH = FBM_PATH;
if (!is_dir(FBM_PATH))
{
  $strFBMPath = realpath("/inetpub/wwwroot/fbm/");
  if (strlen($strFBMPath) > 0)
  {
    $FBM_PATH = $strFBMPath;
  }
}

include ("../error.inc.php");

//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

// session stuff
if (!array_key_exists("saasuser", $_SESSION) || !array_key_exists("dbid", $_SESSION))
{
	header("Location: "."/".$FBM_PATH."/session_lost.html");
	exit;
}

// get admin user id
$intAdminUserID = $_SESSION["saasuser"];

// set a timestamp for file names
$strTimeStamp = date("YmdHis");

// set global log file name
//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
$strLogFile = GeteFormsLogPath()."tmpuid".$intAdminUserID."_".$strTimeStamp.".txt";

// create connection
include($FBM_PATH."\\".'conns.inc.php');
////global $server, $server_user, $server_pwd, $masterdbname;
$mysql_Connection = mysql_connect($server, $server_user, $server_pwd);
if (!$mysql_Connection) 
{
	reportErrorToClient(translate("Could not establish connection to the admin database"));
	exit;
}
$db_selected = mysql_select_db($masterdbname, $mysql_Connection);
if (!$db_selected) 
{
	reportErrorToClient(translate("Could not select the admin database"));
	exit;
}

require_once($FBM_PATH."\\".'ESurvey.class.php');

// instantiate the host user
$hostUser = SAASUser::getSAASUserWithID($intAdminUserID, $mysql_Connection);
if (is_null($hostUser))
{
	reportErrorToClient(translate("Could not get host info"));
	exit;
}

// check his main project
if (is_null($hostUser->MainProject))
{
	reportErrorToClient(translate("Could not get host main project info"));
	exit;
}

// verify that host user main repository is equal to the one on the PABITAM_RepositoryName session var
if ($hostUser->MainProject["DatabaseBMD"] !== $_SESSION["PABITAM_RepositoryName"])  
{
	header("Location: "."/fbm/session_lost.html"); //the session vars have changed; this is just validation
	exit;
}

$blnXLSUploaded = (array_key_exists("xlsfile", $_FILES));
$blnSingleSaving = (array_key_exists("txtNewUserData", $_POST));
//@AAL 16/07/2015 Variable para reportar si hubo algun error durante el grabado de un nuevo usuario
$Errors = "";

if ($blnXLSUploaded)
{
	if ($_FILES['xlsfile']['error'] !== UPLOAD_ERR_OK)
	{
		reportErrorToClient("Upload error code: ".$_FILES['xlsfile']['error']);
		exit;
	}
	
	$type = strtolower(substr($_FILES['xlsfile']['name'], -4));
	$blnExcel5 = ($type == ".xls");
	$blnExcel2007 = ($type == "xlsx");
	if (!($blnExcel5 || $blnExcel2007))
	{
		reportErrorToClient("Invalid xls file");
		exit;
	}
	
	/** PHPExcel */
	require_once '../phpexcel/Classes/PHPExcel.php';
	try	
	{
  		// Create new PHPExcel object
		if ($blnExcel2007)
		{
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		}
		else
		{
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
		}
	} 
	catch (Exception $e) 
	{
		reportErrorToClient($e->getMessage());
		exit;
	}
	
	// move xls file
	//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strXLSFileName = GeteFormsLogPath()."tmpuid_".$intAdminUserID."_".$strTimeStamp."_".$_FILES['xlsfile']['name'];
	//@JAPR
	$strXLSFileTempName = $_FILES['xlsfile']['tmp_name'];
	move_uploaded_file($strXLSFileTempName, $strXLSFileName);
	
	// load xls file
	try 
	{
		$objPHPExcel = $objReader->load($strXLSFileName);
	}
	catch (Exception $e)
	{
		reportErrorToClient($e->getMessage());
		exit;
	}
	
	// delete the xls temp file
	@unlink($strXLSFileTempName);
	// delete the log\xls file
	@unlink($strXLSFileName);
	// get data
	$sheet = $objPHPExcel->getActiveSheet(); 
	$arrData = array(); 
	foreach($sheet->getRowIterator() as $row)
	{ 
		$rowIndex = $row->getRowIndex(); 
		if ($rowIndex == 1) { continue; }
		$strEmail = $sheet->getCell("A".$rowIndex)->getCalculatedValue();
		if (!checkEmail($strEmail)) { 
			$Errors = "Invalid email address";
			continue; 
		}
		$strPassword = $sheet->getCell("B".$rowIndex)->getCalculatedValue();
		$strFirstName = $sheet->getCell("C".$rowIndex)->getCalculatedValue();
		$strLastName = $sheet->getCell("D".$rowIndex)->getCalculatedValue();
		$strRole = $sheet->getCell("E".$rowIndex)->getCalculatedValue();
		// assign arrData values
		//@JAPR 2015-05-20: Corregido un bug, el EMail tenía que estar en minúsculas porque así es como se grabará en la tabla de usuarios, si no
		//se hace de esa manera, no podrá hacer match en el array y no insertará en la tabla de usuarios de eForms (#GR7N2W)
		$strEmail = strtolower($strEmail);
		//@JAPR
		$arrData[$strEmail]["email"] = $strEmail;
		$arrData[$strEmail]["password"] = $strPassword;
		$arrData[$strEmail]["firstname"] = $strFirstName;
		$arrData[$strEmail]["lastname"] = $strLastName;
		$arrData[$strEmail]["role"] = (strtolower($strRole) == "a" ? 1 : 0);
	} 
}
elseif ($blnSingleSaving)
{
	// syntax: email_©_pass_©_first_©_last_©_role_©_target
	$strNewUserData = $_POST["txtNewUserData"];
	$arrNewUserData = explode("_©_", $strNewUserData);
	$arrData = array();
	$strEmail = $arrNewUserData[0];
    //@JAPR 2015-05-20: Corregido un bug, el EMail tenía que estar en minúsculas porque así es como se grabará en la tabla de usuarios, si no
    //se hace de esa manera, no podrá hacer match en el array y no insertará en la tabla de usuarios de eForms (#GR7N2W)
    $strEmail = strtolower($strEmail);
    //@AAL 17/07/2015 No se estaaba validando la Dirección de correo
    if (!checkEmail($strEmail)) { 
		$Errors = translate("Invalid email address");
		continue; 
	}
    //@JAPR
	$arrData[$strEmail]["email"] = $strEmail;
	$arrData[$strEmail]["password"] = $arrNewUserData[1];
	$arrData[$strEmail]["firstname"] = $arrNewUserData[2];
	$arrData[$strEmail]["lastname"] = $arrNewUserData[3];
	$arrData[$strEmail]["role"] = $arrNewUserData[4];
	$arrData[$strEmail]["sendpdfbyemail"] = $arrNewUserData[5];
	$arrData[$strEmail]["supervisor"] = $arrNewUserData[6];
	$strTarget = $arrNewUserData[7];
	//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
	if (getMDVersion() >= esvAltEMailForNotif && isset($arrNewUserData[8])) {
		$arrData[$strEmail]["altemail"] = $arrNewUserData[8];
	}
	//@JAPR 2015-05-20: Corregido un bug, no estaba actualizando todos los campos existentes
	if (getMDVersion() >= esvDefaultSurvey && isset($arrNewUserData[9])) {
		$arrData[$strEmail]["defaultsurvey"] = $arrNewUserData[9];
	}
	//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
	if (getMDVersion() >= esvTemplateStyles && isset($arrNewUserData[10])) {
		$arrData[$strEmail]["colortemplate"] = $arrNewUserData[10];
	}
	//@JAPR
}

// $arrData could be set by saving a single user and requiring this file!
if (!isset($arrData))
{
	reportErrorToClient("Data not received (arrData)");
	$Errors = "Data not received (arrData)";
	exit;
}

// get the existing users
require_once("appUser.inc.php");
// sbrande: optimización -> en lugar de usar la colección, se tirará el query a si_usuario
//$objAppUserColl = BITAMAppUserCollection::NewInstance($theRepository);
$arrExistingUsers = get_SI_USUARIO_users($theRepository);
$blnKeepProcessing = true; 
// register each guest
foreach ($arrData as $guestInfo)
{
	if (array_key_exists($guestInfo["email"], $arrExistingUsers))
	{
		myErrorLog("Processing user" . " " .$guestInfo["email"] . ": " . translate("user already exists"));
		$Errors = translate("Processing user") . " " . $guestInfo["email"] . ": " . translate("user already exists");
		if ($blnSingleSaving) 
		{ 
		  $blnKeepProcessing = false; 
		}
		continue;
	}
	$strResult = registerGuest($mysql_Connection, $hostUser, $guestInfo);
	if ($strResult != "success")
	{
		myErrorLog(translate("Processing user") . " " . $guestInfo["email"] . ": " . $strResult);
		$Errors = translate("Processing user") . " " . $guestInfo["email"] . ": " . $strResult;
	}
}

// get the collection again
//$objAppUserColl = BITAMAppUserCollection::NewInstance($theRepository);
// load users again

if ($blnKeepProcessing)
{
	$arrExistingUsers = get_SI_USUARIO_users($theRepository);

	// update SI_SV_User users
	//foreach($objAppUserColl->Collection as $guest)
	foreach ($arrExistingUsers as $cuentaCorreo => $claUsuario)
	{
		//BITAMAppUser::addUserFromClaUsuario($theRepository, $guest->CLA_USUARIO, intval($arrData[$guest->Email]["role"]));
		if (array_key_exists($cuentaCorreo, $arrData))
		{
			// sbrande_03mar2012
			$intRole = intval($arrData[$cuentaCorreo]["role"]);
			BITAMAppUser::addUserFromClaUsuario($theRepository, $claUsuario, $intRole);
			//conchita actualizar los nuevos campos
			//@JAPR 2015-05-20: Modificado para hacer un único UPDATE con todos los campos necesarios
			$strAdditionalValues = "";
			$strAnd = '';
			if (array_key_exists("sendpdfbyemail", $arrData[$cuentaCorreo]))
			{
				$strAdditionalValues .= $strAnd."SendPDFByEmail = ".(int) $arrData[$cuentaCorreo]["sendpdfbyemail"];
				$strAnd = ', ';
			}
			if (array_key_exists("supervisor", $arrData[$cuentaCorreo]))
			{
				$strAdditionalValues .= $strAnd."Supervisor = ".(int) $arrData[$cuentaCorreo]["supervisor"];
				$strAnd = ', ';
			}
			// sbrande_23oct2012: agregar el nuevo campo AltEmail
			if (array_key_exists("altemail", $arrData[$cuentaCorreo]))
			{
				$strAdditionalValues .= $strAnd."AltEmail = ".$theRepository->ADOConnection->Quote($arrData[$cuentaCorreo]["altemail"]);
				$strAnd = ', ';
			}
			//@JAPR 2013-05-06: Agregada la posibilidad de editar el nombre largo en las cuentas de kpionline (sólo en modo de testing, IUSACELL)
			global $blnTestingServer;
			if ($blnTestingServer && array_key_exists("firstname", $arrData[$cuentaCorreo]))
			{
				$strLastName = trim((string) @$arrData[$cuentaCorreo]["lastname"]);
				$strAdditionalValues .= $strAnd."UserName = ".$theRepository->ADOConnection->Quote($arrData[$cuentaCorreo]["firstname"].(($strLastName != '')?' '.$strLastName:''));
				$strAnd = ', ';
			}
			//@JAPR 2015-05-20: Corregido un bug, no estaba actualizando todos los campos existentes
			if (getMDVersion() >= esvDefaultSurvey) {
				if (array_key_exists("defaultsurvey", $arrData[$cuentaCorreo]))
				{
					$strAdditionalValues .= $strAnd."DefaultSurvey = ".(int) $arrData[$cuentaCorreo]["defaultsurvey"];
					$strAnd = ', ';
				}
			}
			//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
			if (getMDVersion() >= esvTemplateStyles) {
				if (array_key_exists("colortemplate", $arrData[$cuentaCorreo]))
				{
					$strAdditionalValues .= $strAnd."TemplateID = ".(int) $arrData[$cuentaCorreo]["colortemplate"];
					$strAnd = ', ';
				}
			}
			
			if ($strAdditionalValues != '') {
				//@JAPR 2015-05-20: Corregido un bug, no estaba utilizando la conexión correcta para esta actualización
				$aSQL = "UPDATE $theRepositoryName.SI_SV_USERS SET {$strAdditionalValues} 
					WHERE Email = ".$theRepository->ADOConnection->Quote($cuentaCorreo);
				$theRepository->ADOConnection->Execute($aSQL);
			}
			//@JAPR
		}
	}

	// update user dim table!
	BITAMAppUser::addUsersFromClaUsuarioToUsersDim($theRepository);
}

// everything's right!
processTerminatedSuccessfully($theRepository);

function processTerminatedSuccessfully($theRepository)
{
	global $strLogFile, $blnXLSUploaded, $blnSingleSaving, $strTarget, $Errors;
	$strLogPath = "";
	if (file_exists($strLogFile))
	{
		if ($blnXLSUploaded)
		{
			$arrLogFileName = explode("\\", $strLogFile);
			$strLogFileName = end($arrLogFileName);
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			$strLogPath = GeteFormsLogPath().$strLogFileName;
			//@JAPR
		}
		elseif ($blnSingleSaving)
		{
			$strLogPath = file_get_contents($strLogFile);
			$strLogPath = str_replace(array(chr(13).chr(10), chr(13), chr(10)), "\n", trim($strLogPath));
		}
	}
	if ($blnSingleSaving)
	{
		// add target
		$strLogPath = $strTarget."_©_".$strLogPath;
	}
//@AAL 16/07/2015 Agregado para enviar solo el UserID del nuevo usuario agregado
	$UserID = -1;
	$Status = 500; //Hubo un error Interno
	$arrData = array();
	if($Errors == ""){
		$sql = "SELECT MAX(UserID) AS UserID FROM " . $theRepositoryName . ".SI_SV_USERS";
		$aRS = $theRepository->ADOConnection->Execute($sql);
		$Status = 200; //Todo estuvo Ok
		if (!$aRS || $aRS->EOF){
			$Errors = translate("Error accessing")." SI_SV_USERS ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			$Status = 500; //Hubo un error Interno
		}
		$UserID = (int) $aRS->fields["userid"];
		if ($UserID <= 0) {
			$Errors = translate("Failed to register user");
			$Status = 500; //Hubo un error Interno	
		}
	}

	$arrData = array();
	$arrData['Status'] = $Status;
	$arrData['Error'] = $Errors;
	$arrData['UserID'] = $UserID;
	header("Content-Type: text/javascript; charset=UTF-8");
	error_log("\narrData".print_r($arrData, true), 3, "errorlog.txt");
	echo(json_encode($arrData));
	die();
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script language="javascript">
		  function pageloaded()
		  {
			window.parent.refreshpage("<?=$strLogPath?>")
		  }
		</script>
	</head>
<body onload="pageloaded()">
</body>
</html>
<?    
}
  
function reportErrorToClient($errdesc)
{
?>    
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script language="javascript">
		  function pageloaded()
		  {
			alert("There was an error during the process: " + "<?=$errdesc?>")
		  }
		</script>
	</head>
<body onload="pageloaded()">
</body>
</html>
<?
}
  
function checkEmail($email) {
	//GRCUZ 2015-08-28. Remover validación de Email. Issue:BIDIAW
	return true;
	if(eregi("^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$" , $email)){
		//@JAPR 2015-02-06: Agregado soporte para php 5.6
		list($username,$domain)=explode('@',$email);
		//if(!customCheckDnsrr($domain)){
		 //return false;
		//}
		return true;
	}
	return false;
}
  
function registerGuest($mysql_Connection, $hostUser, $guestInfo)
{
  global $FBM_PATH;
	// create connection
	include($FBM_PATH."\\".'conns.inc.php');
	// require the main class
	require_once($FBM_PATH."\\".'ESurvey.class.php');
	
	/*    
	// save the current working directory
	$strCWD = getcwd();
	chdir(FBM_PATH);
	*/    
	
	// get the host main project
	$mainproject = $hostUser->MainProject;
	if (is_null($mainproject))
	{
		return (translate("Could not get the host main project info"));
	}
	
	// get some main project data
	$hostID = $hostUser->userID;
	$templateID = $mainproject["TemplateID"];
	$databaseID = $mainproject["DatabaseID"];
	$templateLanguageID = $mainproject["LanguageID"];
	
	// get guest data
	$strGuestEmail = $guestInfo["email"];
	$strGuestPassword = $guestInfo["password"];
	$strGuestFirstName = $guestInfo["firstname"];
	$strGuestLastName = $guestInfo["lastname"];
	//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
	$strAltEmail = null;
	if (getMDVersion() >= esvAltEMailForNotif && isset($guestInfo["altemail"])) {
		$strAltEmail = $guestInfo["altemail"];
	}
	//@JAPR
	//Conchita agregado sendpdfbyemail y supervisor
	if(isset($guestInfo["sendpdfbyemail"])){
		$strSendPdf = $guestInfo["sendpdfbyemail"];
	}
	if(isset($guestInfo["supervisor"])){
		$strSupervisor = $guestInfo["supervisor"];
	}
	
	// instantiate the new 
	$aSAASUser = SAASUser::getSAASUserWithEmail($strGuestEmail, $mysql_Connection);
	if (is_null($aSAASUser))
	{
		// check that the guest password is not missing
		if (strlen($strGuestPassword) == 0)
		{
			return (translate("Missing password"));
		}
		$aSAASUser = new SAASUser();
		$anArray['acc_name'] = $strGuestEmail;
		$anArray['email'] = $strGuestEmail;
		$anArray['pwd'] = $strGuestPassword;
		$anArray['first_name'] = $strGuestFirstName;
		$anArray['last_name'] = $strGuestLastName;
		$anArray['language'] = $templateLanguageID;
		//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
		if (!is_null($strAltEmail)) {
			$anArray['altemail'] = $strAltEmail;
		}
		//Conchita
		if (!is_null($strSendPdf)) {
			$anArray['sendpdfbyemail'] = $strSendPdf;
		}
		if (!is_null($strSupervisor)) {
			$anArray['supervisor'] = $strSupervisor;
		}
		//@JAPR
	}
	else 
	{
		$anArray = array(); //create empty array
		// check the received password
		if (strlen($strGuestPassword) > 0)
		{
			$anArray['pwd'] = $strGuestPassword;
		}
	}
	$dbid = $databaseID; //databaseid
	$hid = $hostID; //hostid
	$tid = $templateID; //templateID
	$pid = 1; //default: Artus
	$lid = $templateLanguageID; //languageid
	$rl = 0; //registerlevel
	
	// call register function
	$activation_key = $aSAASUser->register($mysql_Connection, $anArray, $dbid, $hid, $tid, $pid, $lid, $rl);
	
	if ($activation_key === false)
	{
		return ($aSAASUser->RegistrationErrorDesc);
	}
	
	// activateAcc
	$aSQL = sprintf("SELECT UserID, HostID, DatabaseID, TemplateID, LanguageID, ProductID, Status, RegisterLevel FROM $masterdbname.SAAS_REGS WHERE RandomKey = '%s'",
		    mysql_real_escape_string($activation_key, $mysql_Connection));
	
	$result = mysql_query($aSQL, $mysql_Connection);
	if (!$result || mysql_num_rows($result) == 0)
	{
		return (translate("Unable to retrieve user registration info"));
	}
	$row = mysql_fetch_assoc($result);
	
	$user_id = $row["UserID"];
	$hid = $row["HostID"];
	$dbid = $row["DatabaseID"];
	$tid = $row["TemplateID"];
	$lid = $row["LanguageID"];
	$pid = $row["ProductID"];
	$rl = (int) @$row["RegisterLevel"];
		
	if ($row["Status"] == 1)
	{
		return (translate("The email address has already been registered"));
	}
	
	$aUser = SAASUser::getSAASUserWithID($user_id, $mysql_Connection);
	
	//@JAPR 2010-03-31: Agregado el nivel de Registro a la Base de datos según el Template
	//Se asigna el tipo de Registro al usuario para que pueda ser procesado correctamente
	$aUser->RegisterLevel = $rl;
	if (!$aUser->confirmRegistration($mysql_Connection, $activation_key, $dbid, $hid, $tid, $pid, $lid, $rl))
	{
		return ($aUser->RegistrationErrorDesc);
	}
	
	//@JAPR
	// create Goyo's user in order to call ChangePass
	if (!defined('ADODB_ASSOC_CASE')) define('ADODB_ASSOC_CASE', 0);
	
	if (!defined('ARTUS')) define('ARTUS', 0);
	if (!defined('OLAP')) define('OLAP', 1);
	if (!defined('ESSBASE')) define('ESSBASE', 2);
	if (!defined('ROLAP')) define('ROLAP', 3);
	
	require_once($FBM_PATH."\\".'rolap/adodb/adodb.inc.php');
	require_once($FBM_PATH."\\".'SAASUser.class2.php');
	require_once($FBM_PATH."\\".'ETL.utils.inc.php');
	
	global $ADODB_FETCH_MODE;
	
	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
	
	$anADOMasterDBConnection = NewADOConnection($server_db_type);
	$anADOMasterDBConnection->Connect($server, $server_user, $server_pwd, $rep_db);
	if (!$anADOMasterDBConnection) 
	{
		return (translate("Could not establish connection to the ADO admin database"));
	}
	//$aUserTemp = SAASUserADODB::getSAASUserWithID($userID, $anADOMasterDBConnection);
	$aUserTemp = SAASUserADODB::getSAASUserWithID($user_id, $anADOMasterDBConnection);
	//@JAPR 2010-03-31: Agregado el nivel de Registro a la Base de datos según el Template
	$aUserTemp->RegisterLevel = $rl;
	// assign repository var!
	$strDatabaseBMD = (string) $aUserTemp->DatabaseBMD;
	
	// call it!
	//@GCRUZ 2010-04-14: Validar que el tid < 1000 para hacer uso de ChangeRepositoryData y changepass
	if ($tid < 1000)
	{
		//@JAPR 2010-04-14: Cambiado el orden de estas funciones, pues primero se deben habilitar los cubos y escenarios antes de exportar
		$aUserTemp->ChangeRepositoryData($anADOMasterDBConnection, $activation_key, $dbid, $hid, $tid, $pid, $lid, $rl);
		$aUserTemp->ChangePass($anADOMasterDBConnection, $aUser->Password);
		//@JAPR 2010-05-16: Agregada la reexportación de usuarios sólo en el proyecto donde se está haciendo el registro
		//Si se marcó el usuario para reexportar (inicialmente usado para BPA), se obtiene su ID de SI_USUARIO e invoca a CGenMetadata
		$aUserTemp->Reexport = $aUserTemp->Reexport || $aUser->Reexport;
		if ($aUserTemp->Reexport || true)
		{
			// Crear la conexion al repositorio
			$anADOUserDB_BMDConnection = NewADOConnection($server_db_type);
			$strDatabaseBMD = (string) $aUserTemp->DatabaseBMD;
			if ((int) $dbid > 0)
			{
				$strDatabaseBMD = $aUserTemp->Projects[$dbid]["DatabaseBMD"];
	      		$project = $aUserTemp->Projects[$dbid];
			}
			// @SBF - 26ene2011 - Tomar en cuenta que ahora las bds (bmd y dwh) pueden estar en otro dbserver 
			if (isset($project) && strlen($project["DBServer"]) == 0)
			{
				$anADOUserDB_BMDConnection->Connect($server, $server_user, $server_pwd, $strDatabaseBMD);
			}
			else 
			{
				$anADOUserDB_BMDConnection->Connect($project['DBServer'], $project['DBServerUsr'], BITAMDecryptPassword($project['DBServerPwd']), $strDatabaseBMD);
			}
			// @SBF - end
			//}
			
			$anADOUserDB_BMDConnection->SetFetchMode(ADODB_FETCH_ASSOC);
			$intClaUsuario = $aUserTemp->getSI_USUARIOClaUsuario($anADOUserDB_BMDConnection, $dbid);
			
		    // sbrande_09mar2012 - before reexporting...
		    // if the user role = 1 (forms admin), change the user from 2 (users) to 3 (forms admins)... a petición de Nub
		    if (intval($guestInfo["role"]) == 1)
		    {
		      $sql = "UPDATE SI_ROL_USUARIO SET CLA_ROL = 3 WHERE CLA_ROL = 2 AND CLA_USUARIO = $intClaUsuario;";
		      $anADOUserDB_BMDConnection->Execute($sql);
		    }
			
			$aUserTemp->ReexportToPhP($anADOMasterDBConnection, $intClaUsuario, $dbid);
			//Si el usuario a Reexportar es el Master, se reexporta también el SUPERVISOR
			if ($intClaUsuario == 1)
			{
				$aUserTemp->ReexportToPhP($anADOMasterDBConnection, 0, $dbid);
			}
		}
		//@JAPR
	}
	
	/*    
	// return to the original working directory
	chdir($strCWD);
	*/    
	return "success";
}
  
function get_SI_USUARIO_users($theRepository)
{
	$sql = "SELECT CLA_USUARIO, CUENTA_CORREO FROM SI_USUARIO WHERE CLA_USUARIO > 0";
	$aRS = $theRepository->ADOConnection->Execute($sql);
	if ($aRS === false)
	{
		reportErrorToClient(translate("Could not access users table"));
		exit;
	}
	$arrExistingUsers = array();
	//foreach($objAppUserColl->Collection as $guest)
	while (!$aRS->EOF)
	{
		//@JAPR 2015-05-20: Corregido un bug, el EMail tenía que estar en minúsculas porque así es como se grabará en la tabla de usuarios, si no
		//se hace de esa manera, no podrá hacer match en el array y no insertará en la tabla de usuarios de eForms (#GR7N2W)
		//Si bien en este punto no debería haber problema porque al insertar en la tabla de usuarios ya se agregaban los EMails en minúsculas,
		//se validará por si acaso se hizo una inserción o actualización manual donde se omitiera convertir a minúsculas
		$arrExistingUsers[strtolower($aRS->fields["cuenta_correo"])] = $aRS->fields["cla_usuario"];
		//@JAPR
		$aRS->MoveNext();
	}
	return ($arrExistingUsers);
}
  
function myErrorLog($errDesc)
{
	global $strLogFile;
	$dtNow = date("Y-m-d H:i:s");
	error_log($dtNow." - ".$errDesc."\r\n", 3, $strLogFile);
}
?>