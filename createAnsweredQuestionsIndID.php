<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("question.inc.php");
require_once("surveyScheduler.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

$sql = "SELECT GblSurveyModelID, AnsweredQuestionsIndID FROM SI_SV_GblSurveyModel";
		
$aRS = $aRepository->DataADOConnection->Execute($sql);
		
if ($aRS === false)
{
	die(translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}
		
if(!$aRS->EOF)
{
	$gblSurveyModelID = (int)$aRS->fields["gblsurveymodelid"];
	$gblAnsweredQuestionsIndID = (int)$aRS->fields["answeredquestionsindid"];
}

if($gblAnsweredQuestionsIndID<=0)
{
	$strOriginalWD = getcwd();
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
	require_once("../model_manager/filescreator.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	
	//se va a crear el indid en la tabla global
	require_once("../model_manager/indicatorkpi.inc.php");
	$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($aRepository);
	$anInstanceIndicator->ModelID = $gblSurveyModelID;
	$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($aRepository, $gblSurveyModelID);
	$anInstanceIndicator->IndicatorName = "Answered Questions";
	$anInstanceIndicator->formato = '#,##0';
	$anInstanceIndicator->agrupador = 'SUM';
	$anInstanceIndicator->sql_source = 'SUM';
	$anInstanceIndicator->save();
	chdir($strOriginalWD);
			
	//Obtenemos el Id del indicador creado y lo almacenamos en la tabla SI_SV_GblSurveyModel
	$gblAnsweredQuestionsIndID = $anInstanceIndicator->IndicatorID;
		
	//actualizar el indid en lat ablaglobal
	$sql = "UPDATE SI_SV_GblSurveyModel SET AnsweredQuestionsIndID = ".$gblAnsweredQuestionsIndID;
				
	if ($aRepository->DataADOConnection->Execute($sql) === false)
	{
		die( translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);	
	}
}

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	if(is_null($surveyInstance->AnsweredQuestionsIndID) || $surveyInstance->AnsweredQuestionsIndID<=0)
	{
		//se crea el indid en la tabla si_sv_survey
		$answeredQuestionsIndID = BITAMSurvey::createNewIndicator('AnsweredQuestions','SUM','#,##0',$surveyInstance->Repository, $surveyInstance->SurveyID, $surveyInstance->ModelID);
			
		//Se genera instancia de coleccion de preguntas dado un SurveyID
		$questionCollection = BITAMQuestionCollection::NewInstance($surveyInstance->Repository, $surveyInstance->SurveyID);
			
		//select a la tabla paralela para scar la info
		$sql = "SELECT FactKey";
		
		foreach($questionCollection as $question) {
			$sql .= ", ".$question->SurveyField;
		}
		
		$sql .= " FROM ".$surveyInstance->SurveyTable;
			
		$aRS = $surveyInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			echo( translate("Error accessing")." ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$factKeys = array();
		$countAnswered = array();
		$m = 0;
		while(!$aRS->EOF)
		{
			$factKeys[$m] = (int)$aRS->fields["factkey"];
			$countAnswered[$m] = 0;

			foreach($questionCollection as $question) 
			{
				$value = $aRS->fields[strtolower($question->SurveyField)];
				if(!is_null($value) && $value != "") 
				{
					$countAnswered[$m] = $countAnswered[$m] + 1;
				}
			}

			$m++;
			$aRS->MoveNext();
		}
		
		//actualizar tabla de hechos
		foreach($factKeys as $key=>$factkey) 
		{
			$sql = "UPDATE RIFACT_".$surveyInstance->ModelID." SET IND_".$answeredQuestionsIndID." = ";
			$sql .= $countAnswered[$key];
			$sql .=" WHERE FactKey = ".$factkey;
			
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				echo(translate("Error accessing")." RIFACT_".$surveyInstance->ModelID." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
			}
			else
			{
				echo('UPDATE: '.$sql.'<br>');
			}
		}
	}
}
?>