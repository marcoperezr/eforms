<?
define("FBM_PATH", "e:\\inetpub\\wwwroot\\fbm");
define("FBM_ROOT_PATH", "/fbm");

include ("../error.inc.php");

//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
require_once("uploadFileExt.php");
//@JAPR

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

// session stuff
if (!array_key_exists("saasuser", $_SESSION))
{
	//header("Location: "."/".FBM_PATH."/session_lost.html");
	$url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST']."/".FBM_ROOT_PATH."/session_lost.html";
	header("Location: ".$url);
	exit;
}

// get admin user id
$intAdminUserID = $_SESSION["saasuser"];

// set a timestamp for file names
$strTimeStamp = date("YmdHis");

// set global log file name
//@JAPR 2019-09-19: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
$strLogPath = GeteFormsLogPath();
$strLogFile = $strLogPath.$intAdminUserID."_".$strTimeStamp.".txt";
//@JAPR
$strLocalFileName = '';
if (array_key_exists('filename', $_GET) && trim($_GET['filename']) != '')
{
	$strLocalFileName = $_GET['filename'];
}
$blnTestLocalFile = ($strLocalFileName != '');

if (!$blnTestLocalFile)
{
  	$blnUploadedSuccessfully = (array_key_exists("xlsfile", $_FILES) && $_FILES['xlsfile']['error'] === UPLOAD_ERR_OK);
	if (!$blnUploadedSuccessfully)
	{
		reportErrorToClient("Upload error code: ".$_FILES['xlsfile']['error']);
		exit;
	}
	
	//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
	$strFileName = replaceInvalidFilenameChars($_FILES['xlsfile']['name']);
	$type = strtolower(substr($strFileName, -4));
	$blnExcel5 = ($type == ".xls");
	$blnExcel2007 = ($type == "xlsx");
	if (!($blnExcel5 || $blnExcel2007))
	{
		reportErrorToClient("Invalid xls file");
		exit;
	}
  	
	$basename = basename($strFileName);
	$filename = pathinfo ($strFileName, PATHINFO_FILENAME);
	//@JAPR
}

//Si el tipo de carga es nueva, entonces se hace truncate a la tabla de las agendas
//y con ello el $typeLoading = 0, pero si viene el parametro appendFile en el GET
//entonces el valor de $typeLoading = 1 y con ello no se hara truncate solo se 
//agregaran mas valores
$typeLoading = 0;
//Obtenemos el tipo de carga, verificamos si viene el parametro appendFile en el GET
if(isset($_GET["appendFile"]))
{
	$typeLoading = (int)$_GET["appendFile"];
}
//@JAPR 2012-06-05: Por instrucción de DAbdo, la carga incremental por el momento NO aplicará, así que se usará Append indistintamente de por
//cual enlace se accese
//@JAPR 2014-11-04: Modificado para siempre eliminar las agendas pre-existentes, ya que ahora se cargará el XLS completo pero sólo
//para las fechas posteriores a la actual, así que dichas agendas no podrían haber sido descargadas ni por ende capturadas por ningún
//usuario por no haberse cumplido aún la fecha
//Por ordenes de LRoux, ahora el proceso de importación primero eliminará todos los registros de fechas posteriores para simplemente insertar de
//nuevo lo que venga en el template, sin embargo esto se ajustó en el código de este archivo y se removió la asignación forzada, ahora se ocultará
//el botón que mandaba el método como Append para sólo dejar el de carga desde 0, así que se respetará el si viene o no el parámetro appendFile, el
//cual simplemente no se mandaría en este momento
//$typeLoading = 1;

// move xls file
if ($blnTestLocalFile)
{
	//@JAPR 2019-09-19: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strXLSFileName = $strLogPath.$strLocalFileName;
	//@JAPR
}
else 
{
	//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
	//@JAPR 2019-09-19: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strXLSFileName = $strLogPath."tmpuid_".$intAdminUserID."_".$strTimeStamp."_".$strFileName;
	//@JAPR
	$strXLSFileTempName = $_FILES['xlsfile']['tmp_name'];
	move_uploaded_file($strXLSFileTempName, $strXLSFileName);
}

// load xls file
include_once('excel.class.php');
$excel = new Excel($strXLSFileName);
$excel->open();

//obtenemos las hojas del excel
$error = '';
$sheets = $excel->getSheets($error);

if($error)
{
	if(is_object($error))
		reportErrorToClient($error->getMessage());
	else 
		reportErrorToClient($error);
	exit();
}

$AgendaSheet = -1;
$bAgendasSheetFound = false;
foreach ($sheets as $key => $aSheet)
{
	if ($aSheet == 'Agendas$')
	{
		$bAgendasSheetFound = true;
		$AgendaSheet = $key;
	}
}
if (!$bAgendasSheetFound)
{
	//@JAPR 2018-01-16: Corregido reporte de error
	//echo "<pre>";
	//echo translate('Could not find "Agendas" sheet on the workbook!');
	reportErrorToClient(translate('Could not find "Agendas" sheet on the workbook!'));
	//@JAPR
	exit;
}
	
$sheet = $sheets[$AgendaSheet];

$error = '';
$excelColumns = $excel->getColumns($sheet, $error);
if($error)
{
	if(is_object($error))
		reportErrorToClient($error->getMessage());
	else 
		reportErrorToClient($error);
	exit();
}

$intFirstSurveyCol = -1;
//@JAPR 2014-11-26: Corregido un bug, los índices de columna empiezan en 0 así que se cambió la validación por -1
$intEMailCol = -1;
$intCatalogCol = -1;
$intAttributeCol = -1;
$intFilterCol = -1;
$intDateCol = -1;
$intHourCol = -1;
//@JAPR 2014-11-04: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
$intCheckInSurveyCol = -1;
//@JAPR
//GCRUZ 2015-09-03. Agregada columna de CheckPoint
$intCheckPoint = -1;
$intNumCols = count($excelColumns);
for($i=0; $i < $intNumCols; $i++)
{
	$strColumnName = strtolower($excelColumns[$i]);
	//Cuando se encuentre la primer columna no reconocída como columna de definición de encuestas, se considerará que es la primer columna
	//correspondiente a las encuestas
	switch ($strColumnName)
	{
		case 'email':
			$intEMailCol = $i;
			break;
		case 'catalogname':
			$intCatalogCol = $i;
			break;
		case 'filter':
			$intFilterCol = $i;
			break;
		case 'date':
			$intDateCol = $i;
			break;
		case 'time':
			$intHourCol = $i;
			break;
		//@JAPR 2014-11-04: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
		case 'check-in form':
			$intCheckInSurveyCol = $i;
			break;
		//@JAPR
		//GCRUZ 2015-09-03. Agregada columna de CheckPoint
		case 'checkpoint':
			$intCheckPoint = $i;
			break;
		
		default:
			if ($intFirstSurveyCol < 0)
			{
				$intFirstSurveyCol = $i; 
			}
			break;
	}
}

//validamos que todas las columnas esten en el excel
//@JAPR 2014-11-03: Modificado el esquema de agendas
//Ya no se agregará el campo de CatalogAttribute, así que ya no necesita estar presente en el archivo y se ignorará si era un archivo viejo
//@JAPR 2014-11-04: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
//@JAPR 2014-11-26: Corregido un bug, los índices de columna empiezan en 0 así que se cambió la validación por -1
//GCRUZ 2015-09-03. Agregada columna de CheckPoint
//GCRUZ 2015-09-04. Quitar columna de nombre de catálogo
if($intEMailCol == -1 /*|| $intCatalogCol == -1*/ || $intFilterCol == -1 || $intDateCol == -1 || $intHourCol == -1 || $intCheckInSurveyCol == -1 || $intCheckPoint == -1)
{
	$strMissingCols = "";
	//@JAPR 2014-11-26: Corregido un bug, los índices de columna empiezan en 0 así que se cambió la validación por -1
	$strMissingCols .= ($intEMailCol == -1)?",EMail":"";
	//$strMissingCols .= ($intCatalogCol == -1)?",CatalogName":"";
	$strMissingCols .= ($intCheckPoint == -1)?",CheckPoint":"";
	$strMissingCols .= ($intFilterCol == -1)?",Filter":"";
	$strMissingCols .= ($intDateCol == -1)?",Date":"";
	$strMissingCols .= ($intHourCol == -1)?",Time":"";
	//@JAPR 2014-11-04: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
	$strMissingCols .= ($intCheckInSurveyCol == -1)?",Check-in Form":"";
	//@JAPR
	$strMissingCols = substr($strMissingCols, 1);
	reportErrorToClient("Invalid excel template format, the following columns are missing: [{$strMissingCols}]");
	exit();
}

//@JAPR 2016-02-15: Modificado el proceso para hacer el borrado exclusivamente de las combinaciones que están procesandose por usuario
//En este punto va a leer el DISTINCT del MAX/MIN de fechas agrupador por usuario, y lo dejará en un array en memoria (no podría consumir
//demasiado a menos que pretendieran cargar cientos de miles de usuarios en el archivo, lo cual ténicamente sería poco probable para
//una administración adecuada del XLS, sería demasiado grande, y probablemete habría sido generado a partir de un motor de BD, si es el caso
//convendría mejor hacer una generación directa a nuestras tablas)
//Activar para debugear el proceso
//$_GET['DebugBreak'] = 1;

//Movida esta asignación ya que se usará antes del punto original
require_once("survey.inc.php");
require_once('surveyAgenda.inc.php');
require_once("user.inc.php");
require_once("catalog.inc.php");

$strCurrDate = date('Ymd');
$arrUsers = array();
$arrUsers = BITAMeFormsUser::GetUsers($theRepository, false);
$arrUsers = array_change_key_case(array_flip($arrUsers), CASE_LOWER);
$strCurrDateFmt = "#2016-02-02#"; // "#".date('Y-m-d')."#";

//GCRUZ 2016-02-26. Utilizar tambla temporal para validar fechas de agendas.
$aSQL = "CREATE TABLE IF NOT EXISTS tmpsvuploadagendas (Email VARCHAR(255), Date DATETIME, LoadDate DATETIME)";
$theRepository->DataADOConnection->Execute($aSQL);
//Llenar la tabla temporal con los datos del excel
$aSQL = "SELECT Email, Date FROM [%s]";
$rs = $excel->execute($aSQL, $sheet, $error);
if($error)
{
	if(is_object($error))
		reportErrorToClient($error->getMessage());
	else 
		reportErrorToClient($error);
	exit();
}

$strLoadDate = date('Y-m-d H:i:s');
while (!$rs->EOF) {
	$strEMail = strtolower((string) @$rs->Fields(0)->Value);
	if (trim((string) @$rs->Fields(1)->Value) == '' || strtotime(trim((string) @$rs->Fields(1)->Value)) === false)
	{
		continue;
		$rs->MoveNext();
	}
	$strDate = date('Y-m-d H:i:s', strtotime(trim((string) @$rs->Fields(1)->Value)));
	$aSQL = "INSERT INTO tmpsvuploadagendas (Email, Date, LoadDate) VALUES (".$theRepository->DataADOConnection->Quote($strEMail).", ".$theRepository->DataADOConnection->Quote($strDate).", '".$strLoadDate."')";
	$theRepository->DataADOConnection->Execute($aSQL);
	$rs->MoveNext();
}

//Este array contendrá además una indicación sobre si ya había o no ejecutado el DELETE de este usuario, para no repetirlo y además
//ir eliminando sólo de aquellos donde ya procesó por lo menos una agenda en lugar de hacer el DELETE completo al iniciar el proceso
//GCRUZ 2016-02-26. Utilizar tambla temporal para validar fechas de agendas. Cambiar consulta de excel a tabla temporal
$arrDatesPerUser = array();
/*
$sql = "SELECT EMail, MIN(Date) AS MinDate, MAX(Date) AS MaxDate 
	FROM [%s] 
	GROUP BY EMail 
	ORDER BY EMail";
	//Por problemas dependiendo de si la columna era Fecha o Texto, no se pudo hacer el Filtro que funcionara para ambos casos, así que se obtendrá el rango de fechas a eliminar
	//mediante otro proceso
	//WHERE Date >= {$strCurrDateFmt} 
*/
$sql = "SELECT EMail, MIN(Date) AS MinDate, MAX(Date) AS MaxDate 
	FROM tmpsvuploadagendas 
	WHERE Date >= '".date('Y-m-d 00:00:00', strtotime('+1 days'))."' AND LoadDate = '".$strLoadDate."' 
	GROUP BY EMail 
	ORDER BY EMail";
//$rs = $excel->execute($sql, $sheet, $error);
$rs = $theRepository->DataADOConnection->Execute($sql);
//if($error)
if(!$rs || $rs->_numOfRows == 0)
{
	/*
	if(is_object($error))
		reportErrorToClient($error->getMessage());
	else 
		reportErrorToClient($error);
	*/
	reportErrorToClient('Error loading file: Could not fin any valid agenda date. All dates must be on future.');
	exit();
}
/*
if(!$rs)
{
	reportErrorToClient("Invalid query". ": {$sql}");
	exit();		
}
*/

while (!$rs->EOF) {
	/*
	$strEMail = strtolower((string) @$rs->Fields(0)->Value);
	$strMinDate = trim((string) @$rs->Fields(1)->Value);
	$strMaxDate = trim((string) @$rs->Fields(2)->Value);
	*/
	$strEMail = strtolower((string) @$rs->fields['EMail']);
	$strMinDate = trim((string) @$rs->fields['MinDate']);
	$strMaxDate = trim((string) @$rs->fields['MaxDate']);
	$intUserID = (int) @$arrUsers[$strEMail];
	if ($intUserID > 0) {
		//GCRUZ 2016-02-26. Remover validación de fechas, se hizo mediante query
		/*
		$blnValidMinDate = true; 
		$blnValidMaxDate = true; 
		//Valida que la fecha sea válida, es decir, que en realidad sea una fecha y que sea igual o posterior al día actual
		if ($strMinDate != '') {
			$strXLSDate = (int) date('Ymd', @strtotime($strMinDate));
			//@JAPR 2014-11-03: Validado que sólo se puedan importar Agendas que son posteriores al día de hoy, ya que las del día actual pudieran
			//encontrarse en proceso de captura y modificarlas afectaría lo ya capturado, y porque no se puede editar la definición histórica
			if ($strXLSDate <= (int) $strCurrDate)
			{
				$blnValidMinDate = false;
			}
			else {
				$strMinDate = date('Y-m-d', @strtotime($strMinDate));
			}
		}
		else {
			$blnValidMinDate = false;
		}
		
		if ($strMaxDate != '') {
			$strXLSDate = (int) date('Ymd', @strtotime($strMaxDate));
			//@JAPR 2014-11-03: Validado que sólo se puedan importar Agendas que son posteriores al día de hoy, ya que las del día actual pudieran
			//encontrarse en proceso de captura y modificarlas afectaría lo ya capturado, y porque no se puede editar la definición histórica
			if ($strXLSDate <= (int) $strCurrDate)
			{
				$blnValidMaxDate = false;
			}
			else {
				$strMaxDate = date('Y-m-d', @strtotime($strMaxDate));
			}
		}
		else {
			$blnValidMaxDate = false;
		}
		
		//Si alguna de las fechas no fue válida, se iguala a la que si hubiera sido
		if ($blnValidMinDate xor $blnValidMaxDate) {
			if (!$blnValidMinDate) {
				$strMinDate = $strMaxDate;
				$blnValidMinDate = true;
			}
			else {
				$strMaxDate = $strMinDate;
				$blnValidMaxDate = true;
			}
		}
		
		if ($blnValidMinDate && $blnValidMaxDate) {
			if (!isset($arrDatesPerUser[$intUserID])) {
				$arrDatesPerUser[$intUserID] = array('EMail' => $strEMail, 'MinDate' => $strMinDate, 'MaxDate' => $strMaxDate, 'Processed' => 0);
			}
		}
		*/
		$arrDatesPerUser[$intUserID] = array('EMail' => $strEMail, 'MinDate' => $strMinDate, 'MaxDate' => $strMaxDate, 'Processed' => 0);
	}
	
	$rs->MoveNext();
}
//@JAPR

//GCRUZ 2016-02-26. Borrar datos de la temporal
$aSQL = "DELETE FROM tmpsvuploadagendas WHERE LoadDate = '".$strLoadDate."'";
$theRepository->DataADOConnection->Execute($aSQL);

if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	echo("<br><br>\r\n\r\n Dates by user:");
	PrintMultiArray($arrDatesPerUser);
}

//$aQuery = "SELECT ". implode(", ", $excelColumns) ." FROM [%s]";
$aQuery = "SELECT [". implode("], [", $excelColumns) ."] FROM [%s]";
$error = "";
$rs = $excel->execute($aQuery, $sheet, $error);
if($error)
{
	if(is_object($error))
		reportErrorToClient($error->getMessage());
	else 
		reportErrorToClient($error);
	exit();
}

if(!$rs)
{
	reportErrorToClient("Invalid query". ": {$aQuery}");
	exit();		
}

//Antes de iniciar el proceso se cargan todos los posibles elementos a usar
$savedAgendas = 0;
//$arrUsers = array();
$arrCatalogs = array();
//@JAPR 2015-01-27: Agregada la validación de los datos del filtro antes de grabar la agenda (#IZJZ48)
$arrCatalogsCollByID = array();
$arrCatalogMembersByCat = array();
$arrAttributesCollByCat = array();
//Este array contiene sólo una referencia a si existe o no el dato con el filtro especificado dentro del catalogo correspondiente
$arrCatalogDataByFilterText = array();
$arrInvalidAgendasBySetting = array();
//@JAPR
$arrAttributes = array();
$arrSurveys = array();
$arrSurveysByCatMember = array();
if (!$rs->EOF)
{
	/*require_once("survey.inc.php");
	require_once('surveyAgenda.inc.php');
	require_once("user.inc.php");
	require_once("catalog.inc.php");
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
	$arrUsers = BITAMeFormsUser::GetUsers($theRepository, false);
	$arrUsers = array_change_key_case(array_flip($arrUsers), CASE_LOWER);
	*/
	$arrCatalogs = BITAMCatalog::getCatalogs($theRepository, false);
	$arrCatalogs = array_change_key_case(array_flip($arrCatalogs), CASE_LOWER);
	//GCRUZ 2015-09-03. Cambiando Catalog por DataSourceMember
	$arrCatalogMembersByCat = BITAMSurveyAgenda::getCatMembersByCatalogID($theRepository, true);
	$arrSurveys = BITAMSurvey::getSurveys($theRepository);
	//@JAPR 2016-02-25: Corregido un bug, los archivos de MS Excel no venían como utf8
	//Debe decodificar el nombre de las agendas dado a que el archivo en MS Excel no viene con soporte de utf8 pero en la metadata si se graban de esa manera
	foreach($arrSurveys as $intSurveyID => $strSurveyName) {
		$arrSurveys[$intSurveyID] = utf8_decode($strSurveyName);
	}
	//@JAPR
	
	$arrSurveys = array_change_key_case(array_flip($arrSurveys), CASE_LOWER);
	$arrSurveysByCatMember = BITAMSurveyAgenda::getSurveysByCatMemberID($theRepository);
	//GCRUZ 2015-09-03. Agregada columna de CheckPoint
	$objCheckPointsCollection = BITAMCheckpointCollection::NewInstance($theRepository);
}

//Si la carga es nueva entonces se hace truncate a la tabla y se borran los elementos
if ($typeLoading == 0) {
	//@JAPR 2014-11-04: Modificado para siempre eliminar las agendas pre-existentes, ya que ahora se cargará el XLS completo pero sólo
	//para las fechas posteriores a la actual, así que dichas agendas no podrían haber sido descargadas ni por ende capturadas por ningún
	//usuario por no haberse cumplido aún la fecha
	//reportErrorToClient("This method is not supported at this time");
	//exit();
	$strCurrentDate = date("Y-m-d")." ".date("H:i:s", @strtotime("23:59:59"));
	$sql = "DELETE FROM SI_SV_SurveyAgendaDet 
		WHERE AgendaID NOT IN (
			SELECT AgendaID 
			FROM SI_SV_SurveyAgenda)";
	$theRepository->DataADOConnection->Execute($sql);
	
	//@JAPR 2016-02-15: Modificado el proceso para hacer el borrado exclusivamente de las combinaciones que están procesandose por usuario
	/*
	//@JAPR 2014-11-26: Corregido un bug, se había dejado una fecha fija
	$sql = "DELETE FROM SI_SV_SurveyAgendaDet 
		WHERE AgendaID IN (
			SELECT AgendaID 
			FROM SI_SV_SurveyAgenda 
			WHERE NOT CaptureStartDate IS NULL AND CaptureStartDate > ".$theRepository->DataADOConnection->DBTimeStamp($strCurrentDate).")";
	$theRepository->DataADOConnection->Execute($sql);
	
	$sql = "DELETE FROM SI_SV_SurveyAgenda 
		WHERE NOT CaptureStartDate IS NULL AND CaptureStartDate > ".$theRepository->DataADOConnection->DBTimeStamp($strCurrentDate);
	$theRepository->DataADOConnection->Execute($sql);
	*/
	//@JAPR
}

//@JAPR 2014-11-04: Modificado para siempre eliminar las agendas pre-existentes, ya que ahora se cargará el XLS completo pero sólo
//para las fechas posteriores a la actual, así que dichas agendas no podrían haber sido descargadas ni por ende capturadas por ningún
//usuario por no haberse cumplido aún la fecha
//Ahora se utilizará el mismo proceso de grabado, simplemente se validará si debe o no obtener una referencia a una agenda pre-existente según el
//parámetro recibido
//else 
//{
	//El método de Append verificará si ya existe la Agenda especificada, si existe entonces remueve cualquier encuesta asociada sin importar 
	//su Status y sólo inserta las indicadas en el Template con Status de Open. Si la Agenda no existe simplemente se agrega como nueva
	//La llave para identificar Agendas es la combinación de sus Elementos "Usuario (Email)", Catálogo, Atributo, Filtro y Fecha, así que 
	//con un sólo registro que exista en ese día para dicha combinación sin importar su hora se asumirá que debe reemplazarlo (si hubieran
	//varios simplemente se elimitan todos excepto el último, el cual se sobreescribe como se explicó)
	$numRow = 1;
	//$strCurrDate = date('Ymd');
	while (!$rs->EOF)
	{
		//Estos valores cambian el case porque deben ser únicos en toda la metadata (ignoro si realmente el Model Manager lo valida de esta forma,
		//pero aunque no lo hiciera, desde el Template no se podría diferenciar entre un error de dedo en el Case o intencionalmente buscarlo
		//tal como se escribió si es que se soporta, así que por ahora deben ser únicos sin importar el case)
		$strEMail = strtolower((string) @$rs->Fields($intEMailCol)->Value);
		//GCRUZ 2015-09-04. Quitar columna de nombre de catálogo
		//$strCatalog = strtolower((string) @$rs->Fields($intCatalogCol)->Value);
		$strCatalog = '';
		//Este no cambia el case porque la BD puede ser Case Sensitive así que se usa tal cual ya que es un filtro
		$strFilterText = (string) @$rs->Fields($intFilterCol)->Value;
		$strDate = date('Y-m-d', strtotime(trim((string) @$rs->Fields($intDateCol)->Value)));
		$strHour = trim((string) @$rs->Fields($intHourCol)->Value);
		$strHour = date('H:i', @strtotime($strHour));
		//@JAPR 2014-11-04: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
		$strCheckInSurveyName = strtolower((string) @$rs->Fields($intCheckInSurveyCol)->Value);
		$intCheckInSurveyID = (int) @$arrSurveys[$strCheckInSurveyName];
		//@JAPR
		
		$intUserID = (int) @$arrUsers[$strEMail];
		//GCRUZ 2015-09-03. Usar el checkpoint para obtener el catalogid
		$strCheckPoint = strtolower((string) @$rs->Fields($intCheckPoint)->Value);
		$intCatalogID = 0;
		foreach ($objCheckPointsCollection->Collection as $aCheckPoint)
		{
			if ($strCheckPoint == strtolower($aCheckPoint->name))
			{
				$intCatalogID = $aCheckPoint->catalog_id;
				$intCheckPointID = $aCheckPoint->id;
			}
		}
		//$intCatalogID = (int) @$arrCatalogs[$strCatalog];
		//@JAPR 2014-11-03: Modificado el esquema de agendas
		//Ya no se agregará el campo de CatalogAttribute
		$intAttributeID = 0;
		//$intAttributeID = (int) @$arrAttributes[$strAttribute];
		//Verifica que el atributo especificado realmente pertenezca al catálogo
		/*
		if ($intCatalogID > 0 && $intAttributeID > 0)
		{
			if (!isset($arrCatalogMembersByCat[$intCatalogID][$intAttributeID]))
			{
				$intAttributeID = 0;
			}
		}
		*/
		//@JAPR
		
		$blnValidDate = true; 
		//Valida que la fecha sea válida, es decir, que en realidad sea una fecha y que sea igual o posterior al día actual
		if ($strDate != '')
		{
			$strXLSDate = date('Ymd', @strtotime($strDate));
			//@JAPR 2014-11-03: Validado que sólo se puedan importar Agendas que son posteriores al día de hoy, ya que las del día actual pudieran
			//encontrarse en proceso de captura y modificarlas afectaría lo ya capturado, y porque no se puede editar la definición histórica
			//@JAPR 2014-11-26: Corregido un bug, se había dejado el = a esta validación así que si se incluían las agendas del día de hoy
			//@JAPR 2016-02-18: Pese al comentario previo de mi parte, se regresó el "=" a esta validación, porque no se pueden generar
			//agendas del mismo día desde la interfaz, así que dejar abierto en este punto esa posibilidad era un bug
			if ((int) $strXLSDate <= (int) $strCurrDate)
			{
				$blnValidDate = false;
			}
		}
		else {
			$blnValidDate = false;
		}
		
		//Verifica que estén asignadas encuestas para esta agenda y que estas sean válidas
		$blnEmptySurveys = true;
		$arrSurveyIDs = array();
		$arrInvalidSurveys = array();
		//@JAPR 2015-01-27: Agregada la validación de los datos del filtro antes de grabar la agenda (#IZJZ48)
		$blnValidFilter = true;
		//@JAPR 2014-11-03: Modificado el esquema de agendas
		//Ya no se agregará el campo de CatalogAttribute
		//@JAPR 2016-02-18: Agregada la validación para no procesar todos los Filtros a menos que se consideren válidos la fecha y el usuario
		if ($blnValidDate && $intUserID > 0 && $intCatalogID > 0)	// && $intAttributeID > 0)
		{
			for ($i = $intFirstSurveyCol; $i < $intNumCols; $i++)
			{
				//Sólo se procesa esta encuesta si en el Template viene marcada como activada
				if (((int) @$rs->Fields[$i]->Value) == 1)
				{
					$strSurveyName = (string) @$rs->Fields[$i]->Name;
					$strSurveyNameLower = strtolower($strSurveyName);
					$blnValidSurveyName = true;
					switch ($strSurveyNameLower)
					{
						case 'email':
						case 'catalogname':
						case 'catalogattribute':
						case 'filter':
						case 'date':
						case 'time':
						//@JAPR 2014-11-04: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
						case 'check-in form':
						//@JAPR
							//Es un nombre de columna del Teamplate, así que no es válido
							$blnValidSurveyName = false;
							break;
					}
					
					if ($blnValidSurveyName)
					{
						$intSurveyID = (int) @$arrSurveys[$strSurveyNameLower];
						//@JAPR 2014-11-03: Modificado el esquema de agendas
						//Ya no se agregará el campo de CatalogAttribute
						if ($intSurveyID > 0) //&& isset($arrSurveysByCatMember[$intAttributeID]) && isset($arrSurveysByCatMember[$intAttributeID][$intSurveyID]))
						{
							$arrSurveyIDs[] = $arrSurveys[$strSurveyNameLower];
							$blnEmptySurveys = false;
						}
						else 
						{
							$arrInvalidSurveys[] = $strSurveyName;
						}
					}
				}
			}
			
			//@JAPR 2015-01-27: Agregada la validación de los datos del filtro antes de grabar la agenda (#IZJZ48)
			$blnValidFilter = true;
			if (!isset($arrCatalogDataByFilterText[$intCatalogID][$strFilterText])) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br><br>\r\n\r\n Checking Filter Text: {$strFilterText} for date: {$strDate}");
				}
				
				//En este caso el filtro especificado no ha sido verificado dentro de los valores del catálogo, así que hay que realizar la
				//consulta para dejarlo en el caché y así reutilizarlo en las siguientes agendas. Se grabará un 0 si no había valor o 1 si
				//existía, no se guardará el valor exacto pues es irrelevante (cualquier combinación con estas llaves es válida)
				//$anAgendaInstance = BITAMSurveyAgenda::NewInstance($theRepository);
				//$anAgendaInstance->FilterText = $strFilterText;
				$arrFilterText = explode("|", $strFilterText);
				$arrAttribDataColl = @$arrCatalogMembersByCat[$intCatalogID];
				if (!is_null($arrAttribDataColl) && is_array($arrAttribDataColl) && count($arrAttribDataColl) > 0) {
					$first = true;
					$curCatMember = 0;
					$strFilter = '';
					foreach ($arrAttribDataColl as $memberID => $arrAttribDef) {
						$memberValue = $theRepository->DataADOConnection->Quote((string) @$arrFilterText[$curCatMember]);
						if (!$first) {
							$strFilter .= ' AND ';
						}
						
						//@CMID66 = 'The Home Depot' AND @CMID65 <> 'Home Depot Tampico'
						//GCRUZ 2015-09-03. Cambiando Catalog por DataSourceMember
						//$strFilter .= ("@CMID".$memberID."=".$memberValue);
						$strFilter .= ("{@DMID".$memberID."}=".$memberValue);
						$curCatMember++;
						$first = false;
					}
					
					if (trim($strFilter) != '') {
					    //@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
					    $intMaxOrder = 0;
					    if (!isset($arrAttributesCollByCat[$intCatalogID])) {
							$arrAttributesColl = BITAMDataSource::GetCatalogAttributes($theRepository, $intCatalogID, $intMaxOrder);
					    	$arrAttributesCollByCat[$intCatalogID] = $arrAttributesColl;
					    }
					    $arrAttributesColl = @$arrAttributesCollByCat[$intCatalogID];
					    
					    if (!isset($arrCatalogsCollByID[$intCatalogID])) {
					    	$objCatalog = BITAMDataSource::NewInstanceWithID($theRepository, $intCatalogID);
					    	$arrCatalogsCollByID[$intCatalogID] = $objCatalog;
					    }
					    $objCatalog = @$arrCatalogsCollByID[$intCatalogID];
					    
					    if (!is_null($arrAttributesColl) && !is_null($objCatalog)) {
							$strFilter = BITAMDataSource::TranslateAttributesVarsToFieldNames($theRepository, $intCatalogID, $strFilter);
							//GCRUZ 2016-04-13. Enviar $intUserID a la función getAttributeValues (antes se enviaba -1)
							$objCatalogValues = $objCatalog->getAttributeValues(array_values($arrAttributesColl), $intUserID, utf8_encode($strFilter), null, null, false, true);
							
							if (!isset($arrCatalogDataByFilterText[$intCatalogID])) {
								$arrCatalogDataByFilterText[$intCatalogID] = array();
							}
							if (!is_null($objCatalogValues) && is_array($objCatalogValues) && count($objCatalogValues) > 0) {
								$arrCatalogDataByFilterText[$intCatalogID][$strFilterText] = 1;
							}
							else {
								$arrCatalogDataByFilterText[$intCatalogID][$strFilterText] = 0;
							}
					    }
					}
				}
			}
			
			$blnValidFilter = (int) @$arrCatalogDataByFilterText[$intCatalogID][$strFilterText];
			if (!$blnValidFilter) {
				if (!isset($arrInvalidAgendasBySetting['Filter'])) {
					$arrInvalidAgendasBySetting['Filter'] = array();
				}
				
				$strInvalidFilter = $strEMail.",".$strDate."=>".$strFilterText;
				myErrorLog(translate("Invalid filter").": ".$strInvalidFilter);
				$arrInvalidAgendasBySetting['Filter'][] = $strInvalidFilter;
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n Is Valid Filter '{$strFilterText}'?: {$blnValidFilter}<br>\r\n");
			}
			//@JAPR
		}
		else 
		{
			//Como en este caso ya se va a reportar un error con el catálogo o el atributo, no tiene caso también marcar error de surveys
			$blnEmptySurveys = false;
		}
		
		//@JAPR 2014-11-03: Modificado el esquema de agendas
		//Ya no se agregará el campo de CatalogAttribute
		//@JAPR 2014-11-04: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
		//@JAPR 2014-11-28: Corregido un bug, la forma de CheckIn debe ser opcional
		//@JAPR 2015-01-27: Agregada la validación de los datos del filtro antes de grabar la agenda (#IZJZ48)
		if ($intUserID > 0 && $intCatalogID > 0 /*&& $intCheckInSurveyID > 0*/ && $strDate != '' && $blnValidDate && !$blnEmptySurveys && trim($strFilterText) != '' && $blnValidFilter)	//$intAttributeID > 0 && 
		{
			//@JAPR 2016-02-15: Modificado el proceso para hacer el borrado exclusivamente de las combinaciones que están procesandose por usuario
			//En este caso se trata de una agenda válida, antes de insertarla debe eliminar las agendas de este usuario que están en el
			//rango de fechas a procesar, pero sólo si aún no se había llevado a cabo dicho borrado, así se reducen los casos de perdida
			//de definiciones por datos inválidos en el template XLS
			$arrUserDates = @$arrDatesPerUser[$intUserID];
			if (!is_null($arrUserDates) && is_array($arrUserDates) && isset($arrUserDates['Processed']) && !$arrUserDates['Processed']) {
				$strMinDate = (string) @$arrUserDates['MinDate'];
				$strMaxDate = (string) @$arrUserDates['MaxDate'];
				$strDeleteFilter = "CaptureStartDate IS NOT NULL 
							AND CaptureStartDate > ".$theRepository->DataADOConnection->DBTimeStamp($strCurrentDate)." 
							AND UserID = {$intUserID} 
							AND (CaptureStartDate BETWEEN "
								.$theRepository->DataADOConnection->DBTimeStamp($strMinDate)." AND "
								.$theRepository->DataADOConnection->DBTimeStamp($strMaxDate).") ";
				
				$sql = "DELETE FROM SI_SV_SurveyAgendaDet 
					WHERE AgendaID IN (
						SELECT AgendaID 
						FROM SI_SV_SurveyAgenda 
						WHERE ".$strDeleteFilter.")";
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n Deleting Agenda detail for user {$strEMail}: {$sql}");
				}
				$theRepository->DataADOConnection->Execute($sql);
				
				$sql = "DELETE FROM SI_SV_SurveyAgenda 
					WHERE ".$strDeleteFilter;
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n Deleting Agenda for user {$strEMail}: {$sql}");
					echo("<br>\r\n");
				}
				$theRepository->DataADOConnection->Execute($sql);
				$arrDatesPerUser[$intUserID]['Processed'] = 1;
			}
			//@JAPR
			
			//Obtiene la (s) agenda (s) que aplica para esta combinación
			//@JAPR 2014-11-04: Modificado para siempre eliminar las agendas pre-existentes, ya que ahora se cargará el XLS completo pero sólo
			//para las fechas posteriores a la actual, así que dichas agendas no podrían haber sido descargadas ni por ende capturadas por ningún
			//usuario por no haberse cumplido aún la fecha
			$anAgendaColl = null;
			//@JAPR 2014-11-04: Corregido un bug, aunque no hay edición, si se debe validar que en caso de intentar importar la misma combinación
			//de agenda con una definición diferente, sólo la última ocurrencia debe ser la que permanezca, así que siempre se hará el SELECT
			//pero según el tipo de captura se validará para omitirlo o la sobreescribirá
			//if ($typeLoading == 1) {
			$anAgendaColl = BITAMSurveyAgendaCollection::NewInstanceUniquePerDay($theRepository, $intUserID, $strDate, $intCatalogID, $intAttributeID, $strFilterText);
			//}
			//@JAPR
			//Por lo pronto sólo reemplaza la primer agenda encontrada con esta combinación
			if (!is_null($anAgendaColl) && count($anAgendaColl->Collection) > 0)
			{
				$anAgendaInstance = $anAgendaColl->Collection[0];
			}
			else 
			{
				//Crea una nueva agenda así que llena sus datos generales
				$anAgendaInstance = BITAMSurveyAgenda::NewInstance($theRepository);
				$anAgendaInstance->AgendaName = utf8_encode($strFilterText);
				$anAgendaInstance->UserID = $intUserID;
				$anAgendaInstance->CatalogID = $intCatalogID;
				$anAgendaInstance->CheckPointID = $intCheckPointID;
				//@JAPR 2014-11-03: Modificado el esquema de agendas
				//Ya no se agregará el campo de CatalogAttribute
				//$anAgendaInstance->AttributeID = $intAttributeID;
				//@JAPR
				$anAgendaInstance->FilterText = utf8_encode($strFilterText);
				$anAgendaInstance->CaptureStartDate = $strDate;
			}
			
			//Graba la definición de Agenda
			$anAgendaInstance->CaptureStartTime = explode(':', $strHour);
			$anAgendaInstance->CaptureStartTime_Hours = (int) @$anAgendaInstance->CaptureStartTime[0];
			$anAgendaInstance->CaptureStartTime_Minutes = (int) @$anAgendaInstance->CaptureStartTime[1];
			$anAgendaInstance->SurveyIDs = $arrSurveyIDs;
			//@JAPR 2014-11-04: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
			$anAgendaInstance->CheckinSurvey = $intCheckInSurveyID;
			//@JAPR
			$anAgendaInstance->Save();
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n Agenda stored for user {$strEMail}: Filter: '{$anAgendaInstance->FilterText}', Date: '{$anAgendaInstance->CaptureStartDate}'");
			}
			$savedAgendas++;
		}
		else 
		{
			if ($intUserID <= 0)
			{
				myErrorLog(translate("Invalid email").": ".$theRepository->DataADOConnection->Quote($strEMail).", ".translate("row #").$numRow);
			}
			if ($intCatalogID <= 0)
			{
				myErrorLog(translate("Invalid checkpoint name").": ".$theRepository->DataADOConnection->Quote($strCheckPoint).", ".translate("row #").$numRow);
			}
			//@JAPR 2014-11-03: Modificado el esquema de agendas
			//Ya no se agregará el campo de CatalogAttribute
			/*
			if ($intAttributeID <= 0)
			{
				myErrorLog(translate("Invalid attribute name").": ".$theRepository->DataADOConnection->Quote($strAttribute).", ".translate("row #").$numRow);
			}
			*/
			//@JAPR 2014-11-04: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
			//@JAPR 2014-11-28: Corregido un bug, la forma de CheckIn debe ser opcional
			/*
			if ($intCheckInSurveyID <= 0)
			{
				myErrorLog(translate("Invalid check-in survey").": ".$theRepository->DataADOConnection->Quote($strCheckInSurveyName).", ".translate("row #").$numRow);
			}
			*/
			//@JAPR
			if ($strDate == '' || !$blnValidDate)
			{
				myErrorLog(translate("Invalid date").": ".$theRepository->DataADOConnection->Quote($strDate).", ".translate("row #").$numRow);
			}
			if (trim($strFilterText) == '')
			{
				myErrorLog(translate("Invalid element filter").": ".$theRepository->DataADOConnection->Quote($strFilterText).", ".translate("row #").$numRow);
			}
			if ($blnEmptySurveys)
			{
				myErrorLog(translate("You must select at least one survey for the agenda").", ".translate("row #").$numRow);
			}
		}
		
		//Independientemente de si hubo o no otros errores, se indica cuales encuestas no eran válidas de acuerdo a la selección de catálogo
		if (count($arrInvalidSurveys) > 0)
		{
			myErrorLog(translate("Invalid survey selection").": ".implode(',', $arrInvalidSurveys).", ".translate("row #").$numRow);
		}
		
		$rs->MoveNext();
		$numRow++;
	}
//}
//@JAPR

// delete the xls temp file
if (!$blnTestLocalFile)
{
	unlink($strXLSFileTempName);
}

// everything's right!
processTerminatedSuccessfully($savedAgendas);

function processTerminatedSuccessfully($numRow)
{
	global $strLogFile;
	global $arrInvalidAgendasBySetting;
	$strLogPath = "";
	if (file_exists($strLogFile))
	{
		//GCRUZ 2015-09-04. En caso de error mostrar archivo de log.
		echo "<pre>";
		echo $numRow.' '.translate('records loaded').PHP_EOL.('There were some errors during the process').PHP_EOL;
		echo file_get_contents($strLogFile);
		exit();
		$arrLogFileName = explode("\\", $strLogFile);
		$strLogFileName = end($arrLogFileName);
		$strLogPath = "$strLogFileName";
	}
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<script language="javascript">
	function pageloaded()
	{	
<?
	if (trim($strLogPath) != '')
	{
?>
	  	if (confirm("<?=$numRow?> <?=translate('records loaded')?>\r\n<?=translate('There were some errors during the process, do you want to download the error log file?')?>"))
	  	{
	  		frmDownloadAgendasLog.submit();
	  	}
	  	else {
<?
		if (count($arrInvalidAgendasBySetting) > 0) {
  			$strErrorMsg = '';
  			foreach ($arrInvalidAgendasBySetting as $strSetting => $arrInvalidAgendas) {
  				$strErrorMsg .= "\\r\\n-".translate($strSetting).":".implode("\\r\\n", $arrInvalidAgendas);
  			}
  			if (trim($strErrorMsg) != '') {
?>
			alert("<?=str_ireplace("\"", "", translate("The following agendas cannot be added because they contain an invalid filter").$strErrorMsg)?>");
<?
  			}
  		}
?>
	  	}
<?
	}
	else
	{
?>
  		alert('<?=$numRow?> <?=translate('records loaded')?>');
<?
	}
	
?>
  	//document.location.href = 'main.php?BITAM_SECTION=SurveyAgendaCollection';
	//frmRefreshAgendas.submit();
  }
	</script>
	</head>
<body onload="pageloaded()">
	<form name="frmDownloadAgendasLog" method="POST" action="downloadUploadAgendasLog.php?dte=<?=date('YmdHis')?>" enctype="multipart/form-data" target="_blank" style="display:none">
		<input type="hidden" name="filename" value="<?=$strLogPath?>">
	</form>
	<form name="frmRefreshAgendas" method="POST" action="main.php?BITAM_SECTION=SurveyAgendaCollection" enctype="multipart/form-data" target="body" style="display:none">
	</form>
</body>
</html>
<?    
}
  
function reportErrorToClient($errdesc)
{
?>    
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<script language="javascript">
	  function pageloaded()
	  {
		alert("There was an error during the import process: " + "<?=str_replace("\\", "\\\\", str_replace("\n", "", $errdesc))?>")
	  }
	</script>
	</head>
<body onload="pageloaded()">
</body>
</html>
<?
}
  
function checkEmail($email) {
	if(eregi("^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$" , $email)){
		//@JAPR 2015-02-06: Agregado soporte para php 5.6
		list($username,$domain)=explode('@',$email);
		//if(!customCheckDnsrr($domain)){
		 //return false;
		//}
		return true;
	}
	return false;
}
  
function myErrorLog($errDesc)
{
	global $strLogFile;
	$dtNow = date("Y-m-d H:i:s");
	error_log($dtNow." - ".$errDesc."\r\n", 3, $strLogFile);
}
?>