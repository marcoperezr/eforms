<?php
/*Realiza la conversión de todos los catálogos y encuestas para utilizar Dimensiones independientes de los atributos de los catálogos
Este proceso se debe realizar mientras no existan sincronizaciones de capturas, ya que modificará la estructura de las tablas y podría
afectar el grabado de algunas capturas si estas se realizan cuando el proceso aun no esté completo
Al finalizar el proceso, se debería hacer una revisión de los reportes con la cuenta afectada para verificar que todo quedó correctamente,
además de realizar capturas de prueba de las encuestas que fueron alteradas
Dependiendo de los posibles errores de grabado durante la vida de las encuestas, pudiera ser necesario cierta cantidad de correcciones manuales
en los datos de las tablas al finalizar este proceso
*/
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$dteCurrDate = date("'Y-m-d H:i:s'");
$blnDebugBreak = getParamValue('DebugBreak', 'both', "(boolean)");

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
/*Desactiva el reporte de errores que termina la ejecución del proceso, con esto se permitirá reportar como resultado los casos de error
y continuar con aquello que sea posible*/
global $gblShowErrorSurvey;
$gblShowErrorSurvey = false;

//Si se agregan IDs, se realizará el proceso exclusivamente a estos catálogos/encuestas, de lo contrario se hará al total de la BD
$arrFilteredSurveys = array();
$strFilteredSurveys = getParamValue('SurveyFilter', 'both', "(string)");
$strFilteredSurveys = trim(str_replace(' ', '', $strFilteredSurveys));
if ($strFilteredSurveys != '') {
	$arrFilteredSurveys = explode(',', $strFilteredSurveys);
}

$objSurveyColl = BITAMSurveyCollection::NewInstance($aRepository, $arrFilteredSurveys, false);
if (!is_object($objSurveyColl)) {
	echo(sendUpgradeMessage("***** Error loading survey collection: ".(string) $objSurveyColl));
	exit();
}

//*************************************************************************************************************************
//Recorre todas las formas y agrega los campos y tablas de las preguntas que lo requieran
//*************************************************************************************************************************
echo("<BR><BR>***** Creating questions tables and fields");
foreach ($objSurveyColl as $objSurvey) {
	$intSurveyID = (int) @$objSurvey->SurveyID;
	if ($intSurveyID <= 0) {
		continue;
	}
	
	echo(sendUpgradeMessage("Processing Survey: {$objSurvey->SurveyName}", "color: blue;"));
	
	$objQuestionColl = BITAMQuestionCollection::NewInstance($aRepository, $intSurveyID);
	if (!is_object($objQuestionColl)) {
		echo(sendUpgradeMessage("***** Error: ".(string) $objQuestionColl));
		continue;
	}
	
	foreach ($objQuestionColl->Collection as $objQuestion) {
		$blnValid = false;
		switch ($objQuestion->QTypeID) {
			case qtpSingle:
			case qtpCallList:
			case qtpMulti:
				$blnValid = true;
				break;
		}
		
		if (!$blnValid) {
			continue;
		}
		echo(sendUpgradeMessage("Processing Question ({$objQuestion->QuestionID}): ".$objQuestion->getAttributeName(), "color: black;"));
		$objQuestion->createDataTablesAndFields();
	}
}

echo("<BR><BR><BR>***** Process finished *****");
?>