<?php
require_once("section.inc.php");
require_once("object.trait.php");

class BITAMSectionExt extends BITAMSection
{
	use BITAMObjectExt;
	function __construct($aRepository, $aSurveyID)
	{
		parent::__construct($aRepository, $aSurveyID);
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
	}
	
	//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
	function hideBackButton($aUser) {
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SectionExt&SurveyID=".$this->SurveyID;
		}
		else
		{
			return "BITAM_PAGE=SectionExt&SurveyID=".$this->SurveyID."&SectionID=".$this->SectionID;
		}
	}
	
	function generateAfterFormCode($aUser)
	{
		//@JAPR 2015-04-30: Corregido un bug, al integrar el nuevo archivo se desactivaron las validaciones previas al grabado
 		parent::generateAfterFormCode($aUser);
		//@JAPR
		
 		$myFormName = get_class($this);
		
		//Si se trata de un objeto nuevo, el grabado se debe hacer hacia un archivo alternativo que procese todos estos requests
		if ($this->isNewObject()) {
?>
	 	<script language="JavaScript">
			<?=$myFormName?>_SaveForm.action = '<?='processRequest.php?Process=Add&ObjectType='.otySection.'&'.$this->get_QueryString()?>&SelSurveyID=<?=$this->SurveyID?>';
		</script>
<?
		}
	}
}

class BITAMSectionExtCollection extends BITAMSectionCollection
{
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=SectionExt&SurveyID=".$this->SurveyID."&SelSurveyID=".$this->SurveyID;
	}
}
?>