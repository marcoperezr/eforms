<?php
    //@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
    require_once("checkCurrentSession.inc.php");
	
	$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
	$strScriptPath = $path_parts["dirname"];
	
    /*@AAL 29/06/2015: Se importan los archivos necesarios para obtener los datos correspondiente */
    require_once("report.inc.php");

    /*@AAL 29/06/2015: Extraemos los parámetros que se enviaron desde el archivo principal(report.inc.php) */
    $SurveyID = (isset($_POST["SurveyID"]) ? $_POST["SurveyID"] : NULL);
    $DateID = (isset($_POST["DateID"]) ? $_POST["DateID"] : NULL);
	//@JAPR 2015-08-08: Corregido un bug, no estaba cargando la instancia de reporte correcto pues no estaba considerando al usuario (#4PIE8N)
	$intUserID = getParamValue("UserID", 'both', '(int)');
	if ($intUserID <= 0) {
		$intUserID = 1;
	}
	$strReportRowID = getParamValue("RowID", 'both', '(string)');
    
	//@AAL 26/06/2015: Se obtienen los datos correspondientes a cada captura.
	$surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $SurveyID);
    $anInstance = BITAMReport::NewInstanceToFormsLogWithID($theRepository, $DateID, $SurveyID, $intUserID, false);
	//GCRUZ 2015-10-26. Escapar comillas. Issue: F5G2AY
	//@JAPR 2016-11-08: Corregidos los textos utilizados dentro de funciones de DHTMLX que generan JScript para escapar caracteres que podrían causar conflictos (#KPCWW3)
	//Se optó por unificar estos reemplazos con una función global en lugar de seguir haciéndolos manualmente en cada ocurrencia y diferentes según quien los programe
	$strSurveyName = GetValidJScriptText($anInstance->SurveyName);

	
    function getImage($theRepository, $aQuestion, $FactKeyDimVal, $anImage = null) {
		global $gblEFormsNA;
		
		//@JAPR 2015-08-11: Modificado para no mostrar etiqueta en caso de no existir este valor
		//GCRUZ 2015-09-02. Preguntas tipo Barcode no mostrarán imagen. Issue: 53NPOT
		if ($aQuestion->QTypeID == qtpBarCode)
			return '';
        $htmlString = '';	//translate("No image captured");
		//@JAPR
		$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
		$strPathImage = '';
		
		if (is_null($anImage)) {
			$sql = "SELECT FactKey, SurveyID, QuestionID, PathImage FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$aQuestion->SurveyID." AND FactKey = ".$FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID;
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			if ($aRS !== false && !$aRS->EOF) {
				$strPathImage = (string) @$aRS->fields["pathimage"];
			}
		}
		else {
			$strPathImage = $anImage;
			if (trim($strPathImage) == $gblEFormsNA) {
				$strPathImage = '';
			}
		}
		
		if (trim($strPathImage) != '') {
			$path = "surveyimages/".$theRepositoryName."/".$strPathImage;
			$path = str_replace("\\", "/", $path);
			$htmlString = "<img src='".$path."' style='width: 100px; height: 140px;' displayMe='1'>";
		}
        
        return $htmlString;
    }

    function getDocument($theRepository, $aQuestion, $FactKeyDimVal, $aDocument = null) {
		global $gblEFormsNA;
		
		//@JAPR 2015-08-11: Modificado para no mostrar etiqueta en caso de no existir este valor
        $htmlString = '';	//translate("No document attached");
		//@JAPR
		$blnDocument = false;
		$renglon = 0;
		$columna = 0;
		if (is_null($aDocument)) {
			$sql = "SELECT FactKey, SurveyID, QuestionID FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$aQuestion->SurveyID." AND FactKey = ".$FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID;
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			if ($aRS !== false && !$aRS->EOF) {
				$blnDocument = true;
			}
		}
		else {
			$blnDocument = ($aDocument != '' && $aDocument != $gblEFormsNA);
		}

		//GCRUZ 2015-09-08. Obtener el MainFactKey del documento para poder relacionarlo a su "renglon" correspondiente dentro del maestro detalle
		if (!is_null($aDocument))
		{
			$aSQL = "SELECT FactKey, MainFactKey, SurveyID, QuestionID FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$aQuestion->SurveyID." AND FactKey = ".$FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID." AND PathDocument = ".$theRepository->DataADOConnection->Quote($aDocument);
			$aRS = $theRepository->DataADOConnection->Execute($aSQL);
			if ($aRS && $aRS->_numOfRows != 0)
				$renglon = $aRS->fields['MainFactKey'];
		}
		
		if ($blnDocument) {
			switch ($aQuestion->QTypeID) {
				case qtpDocument:
					$strIcon="images/document.gif";
					$strTitle="View document";
					break;
				case qtpAudio:
					$strIcon="images/audio.gif";
					$strTitle="Listen audio";
					break;
				case qtpVideo:
					$strIcon="images/video.gif";
					$strTitle="View video";
					break;
				default:
					$blnDocument = false;
					break;
			}
		}
		
		if ($blnDocument) {
			$htmlString = "<img title='".$strTitle."' src='".$strIcon."' style='cursor:hand' onclick='javascript:OpenDoc(".$aQuestion->SurveyID.", ".$aQuestion->QuestionID.", ".$FactKeyDimVal.", ".$renglon.", ".$columna.")' displayMe='1'>";
		}
        
        return $htmlString;
    }
	
    function getComment($theRepository, $aQuestion, $FactKeyDimVal, $aComment = null,$strMainFactKey=0) {
		//@JAPR 2015-08-11: Modificado para no mostrar etiqueta en caso de no existir este valor
        $htmlString = ''; //translate("No comment");
		//@JAPR
		$strIcon="images/comment.gif";
		$renglon = $strMainFactKey;
		
		$columna = 0;
		$strComment = "";

		if (is_null($aComment)) {
			$sql = "SELECT FactKey, SurveyID, QuestionID, StrComment, MainFactKey FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$aQuestion->SurveyID." AND FactKey = ".$FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID;
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			if ($aRS !== false && !$aRS->EOF) {
				$strComment = (string) @$aRS->fields["strcomment"];
				$strMainFactKey = @$aRS->fields["mainfactkey"];
			}
		}
		else {
			$strComment = $aComment;
		}

		//GCRUZ 2015-10-13. Obtener el MainFactKey del comentario para poder relacionarlo a su "renglon" correspondiente dentro del maestro detalle
		if (!is_null($aComment))
		{
			$aSQL = "SELECT FactKey, MainFactKey, SurveyID, QuestionID FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$aQuestion->SurveyID." AND FactKey = ".$FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID." AND StrComment = ".$theRepository->DataADOConnection->Quote($aComment);
			$aRS = $theRepository->DataADOConnection->Execute($aSQL);
			if ($aRS && $aRS->_numOfRows != 0)
				$renglon = $aRS->fields['MainFactKey'];
		}
		
		if (trim($strComment)) {
			$htmlString = "<img title='View comment' src='".$strIcon."' style='cursor:hand' onclick='javascript:OpenComment(".$aQuestion->SurveyID.", ".$aQuestion->QuestionID.", ".$FactKeyDimVal.", ".$renglon.", ".$columna.")' displayMe='1'>";
		}
		
        return $htmlString;
    }

?>
<!-- AAL 29/06/2015: Se inicia a crear el HTML y los elementos que mostrara la página, pero antes
    en el encabezado se definen los estilos y el código JavaScrip emplado por los elemetos HTML -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
        <script src="js/codebase/dhtmlxFmtd.js"></script>
        <script type="text/javascript" src="js/json2.min.js"></script>
        <script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
<?		//@JAPR 2017-01-10: Agregada la posibilidad de editar capturas grabadas desde el Admin en la captura en línea, utilizando un draft pre-cargado como parámetro para simular
		//que se estuviera cargando un draft de archivo pero en browser (#J4HPY4)
?>
		<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>
        <title>"<?=translate('Capture log')?>"</title>
        <style>
            /*AAL 25/02/2015: Definición de algunos estilos empleados en algunos elementos HTML*/
           div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
                text-transform: none !important;
            }
			/* it's important to set width/height to 100% for full-screen init */
			html, body {
				width: 100%;
				height: 100%;
				margin: 0px;
				overflow: hidden;
			}
        </style>

		<!-- AAL 29/06/2015: Se inicia el apartado de texto javascript para validar los datos a mostrar en 
        cada uno de los elementos HTML -->
        <script type="text/javascript">
			var dhxGridDataCell = "a";
			var objReportLayout = undefined;
			var objReportToolbar = undefined;
			var objGrid = undefined;
			
            function OpenImg(surveyID, questionID, folioID, renglon, columna)
            {
                var aWindow = window.open('getSurveyImage.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'picWin', 'width=800, height=600, left=300, top=100, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
            }

            function OpenDoc(surveyID, questionID, folioID, renglon, columna)
            {
                window.open('getSurveyDocument.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'picWin', 'width=500, height=300, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
            }

            function OpenComment(surveyID, questionID, folioID, renglon, columna)
            {
				//GCRUZ 2015-10-01. Cambiar ventana de comentario de una pestaña a una ventana de dhtmlX. Issue: ASA36W
//                var aWindow = window.open('getSurveyComment.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'commentWin', 'width=400, height=150, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);

				var objWindowComment = new dhtmlXWindows({
					image_path:"images/"
				});

				var intDialogWidth = 450;
				var intDialogHeight = 200;
				objDialog = objWindowComment.createWindow({
					id:"wOpenComment",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialog.setText("<?=translate("Comment")?>");
				objDialog.denyPark();
				objDialog.denyResize();
				objDialog.setIconCss("without_icon");

				objDialog.attachURL('getSurveyComment.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna);
            }
			
			/* Envía información al servidor para procesar algo, ya sea un grabado, borrado o para obtener información
			Se llamó doSendDataAjax para no confundir con la función doSendData, aunque en esencia hacen lo mismo, sin embargo esta función no se basa en una Forma como la otra
			*/
			function doSendDataAjax(oFields, oCallbackFn, bRemoveProgress) {
				console.log("doSendDataAjax");
				
				if (bRemoveProgress === undefined) {
					bRemoveProgress = true;
				}
				
				//Prepara los parámetros con los datos a grabar
				var objFieldsData = oFields;
				if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
					return;
				}
				
				var objParams = $.extend({}, objFieldsData);
				if (!objParams) {
					return;
				}
				
				var strParams = '';
				var strAnd = '';
				for (var strProp in objParams) {
					strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
					strAnd = '&';
				}
				
				objReportLayout.cells(dhxGridDataCell).progressOn();
				window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
					doSaveConfirmation(loader, oCallbackFn, bRemoveProgress);
				});
			}
			
			/* Procesa la respuesta de una invocación al servidor */
			function doSaveConfirmation(loader, oCallbackFn, bRemoveProgress) {
				console.log('doSaveConfirmation');
				
				if (!loader || !loader.xmlDoc) {
					if (bRemoveProgress) {
						objReportLayout.cells(dhxGridDataCell).progressOff();
					}
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					if (bRemoveProgress) {
						objReportLayout.cells(dhxGridDataCell).progressOff();
					}
					return;
				}
				
				//Si llega a este punto es que se recibió una respuesta correctamente
				var response = loader.xmlDoc.responseText;
				try {
					var objResponse = JSON.parse(response);
				} catch(e) {
					alert(e + "\r\n" + response.substr(0, 1000));
					if (bRemoveProgress) {
						objReportLayout.cells(dhxGridDataCell).progressOff();
					}
					return;
				}
				
				if (objResponse.error) {
					alert(objResponse.error.desc);
					if (bRemoveProgress) {
						objReportLayout.cells(dhxGridDataCell).progressOff();
					}
					return;
				}
				else {
					if (objResponse.warning) {
						console.log(objResponse.warning.desc);
					}
				}
				
				//JAPR 2015-07-17: Agregado el borrado de objetos directamente desde Ajax, los cuales no regresan código desde el server sino sólo una referencia para que se
				//pueda ejecutar un código de callback
				if (objResponse.objectType) {
					if (objResponse.deleted) {
						if (oCallbackFn && typeof oCallbackFn == "function") {
							oCallbackFn(objResponse);
						}
						else {
							if (bRemoveProgress) {
								objReportLayout.cells(dhxGridDataCell).progressOff();
							}
						}
					}
					return;
				}
				else {
					if (objResponse.newObject) {
						if (oCallbackFn && typeof oCallbackFn == "function") {
							oCallbackFn(objResponse.newObject);
						}
					}
					return;
				}
				
				if (bRemoveProgress) {
					objReportLayout.cells(dhxGridDataCell).progressOff();
				}
			}
        </script>
    </head>

    <!-- AAL 29/06/2015: Se inicia el apartado del cuerpo HTML (BODY) asi como los elementos que contendra la página -->
    <body>
        <!--<div id="Title" style="width:100%; margin: 5px 0px 5px; text-align: center; color: #285768; font: bold 20px arial,sans-serif;"><?=translate('Capture log')?></div>-->
        <!--<div id="ShowData" style="width:624px; height: 405px; overflow:hidden"></div>-->
		<div id="ShowData" style="width:100%; height: 100%; overflow:hidden"></div>
        <script language="JavaScript">
<?
	if ($surveyInstance->CreationAdminVersion >= esveFormsv6) {
		//Si es una forma de V6, el reporte se debe generar agrupando las secciones multi-registro en un subgrid, las estándar y los datos base si se generan en el grid principal
?>
			//JAPR 2015-08-13: Agregado el LayOut para permitir la toolbar y corregido el resize de la ventana
			objReportLayout = new dhtmlXLayoutObject({
				parent: document.body,
				pattern:"1C"
			});
			objReportLayout.cells(dhxGridDataCell).hideHeader();
<?		//@JAPR 2015-09-03: validado que sólo los administradores puedan acceder a ciertas opciones de eForms
		//Sólo un administrador o el propio usuario que hizo la captura puede eliminarla
		if ((int) @$_SESSION["PABITAM_UserRole"]==1 || (!is_null($theUser) && (int) @$theUser->UserID == (int) @$anInstance->UserID)) {
?>
			var objReportToolbar = objReportLayout.cells(dhxGridDataCell).attachToolbar({
				parent:"questionsCreationCell"
			});
			objReportToolbar.setIconsPath("<?=$strScriptPath?>/images/admin/");
			var intPos = 0;
			objReportToolbar.addButton("remove", intPos++, "<?=translate("Remove")?>", "remove.png");
<?			//@JAPR 2017-01-10: Agregada la posibilidad de editar capturas grabadas desde el Admin en la captura en línea, utilizando un draft pre-cargado como parámetro para simular
			//que se estuviera cargando un draft de archivo pero en browser (#J4HPY4)
?>
			objReportToolbar.addButton("load", intPos++, "<?=translate("Load entry")?>", "loadEntry.png");
			objReportToolbar.attachEvent("onClick", function(id) {
				console.log('objReportToolbar.onClick ' + id);
				
				switch (id) {
					case "remove":
						var blnAnswer = confirm("<?=sprintf(translate("Do you want to remove this %s?"), translate("Entry"))?>");
						if (!blnAnswer) {
							return;
						}
						
						var objParams = {
							RequestType: <?=reqAjax?>,
							Process: "Delete",
							ObjectType: <?=otyReport?>,
							ObjectID: "<?=$anInstance->FactKey?>",
							SurveyID: <?=$SurveyID?>
						};
						
						doSendDataAjax(objParams, function(oResponse) {
							objReportLayout.cells(dhxGridDataCell).progressOff();
							//Cierra el diálogo de este reporte al terminar el borrado
							setTimeout(function () {
								//Elimina la row de este reporte
								if (parent.objLogGrid && parent.objLogGrid.deleteRow && '<?=$strReportRowID?>') {
									parent.objLogGrid.deleteRow('<?=$strReportRowID?>');
								}
								
								if (parent.doUnloadDialog) {
									parent.doUnloadDialog();
								}
							}, 100);
						});
						break;
					
<?					//@JAPR 2017-01-10: Agregada la posibilidad de editar capturas grabadas desde el Admin en la captura en línea, utilizando un draft pre-cargado como parámetro para simular
					//que se estuviera cargando un draft de archivo pero en browser (#J4HPY4)
?>
					case "load":
						d = new Date();
						dteNow = '' + d.getFullYear() + d.getMonth() + d.getDay() + d.getHours() + d.getMinutes() + d.getSeconds();
						openWindow('GenerateSurvey.php?dte=' + dteNow + '&surveyID=<?=$SurveyID?>&login=1&edit=1&ApplicatorID=<?=$anInstance->UserID?>&SurveyDate=<?=$DateID?>');
						break;
				}
			});
<?
		}
?>
			
            //var objGrid = new dhtmlXGridObject('ShowData');
			var objGrid = objReportLayout.cells(dhxGridDataCell).attachGrid();
			//JAPR
			
			//Attribute/Question Name, Value, Image/Photo, DocumentLink, CommentLink
            objGrid.setHeader(" , , , , , ");
            objGrid.setInitWidths("50, 200, 400, 150, 150, 150");
            objGrid.setStyle("", "font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);font-family:Arial;font-size:9pt;");
            //Para los scrollbars del gridbox
            objGrid.objBox.style.overflowX = "auto";
            objGrid.objBox.style.overflowY = "auto";
			objGrid.setColTypes("ro,ro,ro,ro,ro,ro");
			objGrid.setSizes();
			objGrid.setImagePath("images/");
			objGrid.enableMultiline(true);
			objGrid.enableColSpan(true);
            objGrid.init();
			
			var intRowNum = 1;
			var strPropColStyle = "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;";
			objGrid.addRow(intRowNum, ["", "<?=translate("Entry id")?>", "<?=$anInstance->ReportID?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("User Name")?>", "<?=$anInstance->UserName?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
			var intRowNumSurvey = intRowNum;
            /*objGrid.addRow(intRowNum, ["", "<?=translate("Survey")?>", "<?=$strSurveyName?>", "", "", ""]);*/
			objGrid.addRow(intRowNumSurvey, ["", "<?=translate("Survey")?>", "<?=$strSurveyName?>", "", "", ""]);
			objGrid.setColspan(intRowNumSurvey,2,4);			
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("Start date")?>", "<?=(string) @$anInstance->StartDate?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("Start time")?>", "<?=(string) @$anInstance->StartTime?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("End date")?>", "<?=(string) @$anInstance->EndDate?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("End time")?>", "<?=(string) @$anInstance->EndTime?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("Latitude")?>", "<?=(string) @$anInstance->Latitude?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("Longitude")?>", "<?=(string) @$anInstance->Longitude?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("Sync date")?>", "<?=(string) @$anInstance->SyncDate?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("Sync time")?>", "<?=(string) @$anInstance->SyncTime?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("Sync latitude")?>", "<?=(string) @$anInstance->SyncLatitude?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("Sync longitude")?>", "<?=(string) @$anInstance->SyncLongitude?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
<?
			//@JAPR 2016-11-08: Corregidos los textos utilizados dentro de funciones de DHTMLX que generan JScript para escapar caracteres que podrían causar conflictos (#KPCWW3)
			//Se optó por unificar estos reemplazos con una función global en lugar de seguir haciéndolos manualmente en cada ocurrencia y diferentes según quien los programe
?>
            objGrid.addRow(intRowNum, ["", "<?=translate("GPS country")?>", "<?=GetValidJScriptText((string) @$anInstance->Country)?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("GPS state")?>", "<?=GetValidJScriptText((string) @$anInstance->State)?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("GPS city")?>", "<?=GetValidJScriptText((string) @$anInstance->City)?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
            objGrid.addRow(intRowNum, ["", "<?=translate("GPS zip code")?>", "<?=GetValidJScriptText((string) @$anInstance->ZipCode)?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
			var intRowNumGPSAdrs = intRowNum;
            objGrid.addRow(intRowNumGPSAdrs, ["", "<?=translate("GPS address")?>", "<?=GetValidJScriptText((string) @$anInstance->Address)?>", "", "", ""]);
			objGrid.setColspan(intRowNumGPSAdrs,2,4);			
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
			
<?
	if ($anInstance->bHasTransferData)
	{
?>
			objGrid.addRow(intRowNum, ["", "<?=translate("Transfer Time (min)")?>", "<?=(double) sprintf('%1.2f', @$anInstance->TransferTime)?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
			objGrid.addRow(intRowNum, ["", "<?=translate("Transfer Distance (mts)")?>", "<?=(double) sprintf('%1.2f', @$anInstance->TransferDistance)?>", "", "", ""]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
<?
	}
			//Genera los registros de las secciones estándar
			$objQuestionColl = BITAMQuestionCollection::NewInstance($theRepository, $SurveyID);
			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->SectionType != sectNormal) {
					continue;
				}
				
				//GCRUZ 2015-09-02. Preguntas que ya no se van a mostrar. Issue: 53NPOT
				switch ($objQuestion->QTypeID) {
					case qtpPassword:
						continue 2;
						break;
					case qtpSkipSection:
						continue 2;
						break;
					case qtpMessage:
						continue 2;
						break;
					//RV Mayo2016: Las preguntas de tipo Actualizar Destinos, Salir, Sección, Sincronizar no deben mostrarse en el reporte
					case qtpUpdateDest:
					case qtpExit:
					case qtpSection:
					case qtpSync:
						continue 2;
						break;
					//RV Mayo2016:  La pregunta OCR sólo se mostrará si tiene configurado guardar foto, si no tiene activada esa configuración entonces
					//no se incluye en el reporte.
					case qtpOCR:
						if(!$objQuestion->SavePhoto)
						{
							continue 2;
						}
						break;
					default:
						break;
				}
				
				$intQuestionID = $objQuestion->QuestionID;
				$strPropName = $objQuestion->SurveyField;
				//GCRUZ 2015-11-13. Cambiar el nombre de la pregunta por el nombre corto
                $QuestionText = $objQuestion->getAttributeName();
				/*ears 2016-11-23 se quitan los puntos suspensivos al habilitar la función multilinea se pueden observar completos los nombres de preguntas o secciones*/
				/*$ShortText = (strlen($QuestionText) >= 24) ? substr($QuestionText, 0, 24) . "..." : $QuestionText;*/
				$ShortText = $QuestionText;
				//@JAPR 2015-10-09: Corregido un bug, debe escapar las comillas (#62XBCE)
				$ShortText = str_replace("\"", "\\\"", $ShortText);
				//@JAPR
				$strValue = (string) @$anInstance->{$strPropName}["value"];
				//GCRUZ 2015-10-26. Escapar comillas. Issue: F5G2AY
				$strValue = str_replace("\"", "\\\"", $strValue);
				$strImage = getImage($theRepository, $objQuestion, $anInstance->FactKeyDimVal, @$anInstance->{$strPropName}["image"]);
				$strDocument = getDocument($theRepository, $objQuestion, $anInstance->FactKeyDimVal, @$anInstance->{$strPropName}["document"]);
				$strComment = getComment($theRepository, $objQuestion, $anInstance->FactKeyDimVal, @$anInstance->{$strPropName}["comment"]);
?>
			objGrid.addRow(intRowNum, ["", "<?= $ShortText ?>", "<?=$strValue?>", "<?=$strImage?>", "<?=$strDocument?>", "<?=$strComment?>"]);
            objGrid.setCellTextStyle(intRowNum, 0, strPropColStyle);
            objGrid.setCellTextStyle(intRowNum++, 1, strPropColStyle);
<?
			}
			
			//Genera los registros de las secciones múltiples
			$objSectionColl = BITAMSectionCollection::NewInstance($theRepository, $SurveyID);
			foreach ($objSectionColl->Collection as $objSection) {
				if ($objSection->SectionType != sectMasterDet && $objSection->SectionType != sectInline) {
					continue;
				}
				
				$intSectionID = $objSection->SectionID;
			//@JAPR 2016-11-08: Corregidos los textos utilizados dentro de funciones de DHTMLX que generan JScript para escapar caracteres que podrían causar conflictos (#KPCWW3)
			//Se optó por unificar estos reemplazos con una función global en lugar de seguir haciéndolos manualmente en cada ocurrencia y diferentes según quien los programe
?>				
			var strRowID = "Section" + "<?=$intSectionID?>";
			objGrid.addRow(strRowID, ["<?=$intSectionID?>", "<?=GetValidJScriptText($objSection->SectionName)?>", "<?=GetValidJScriptText($objSection->SectionName)?>", "", "", ""]);
			objGrid.setCellTextStyle(strRowID, 0, strPropColStyle);
			objGrid.setCellTextStyle(strRowID, 1, strPropColStyle);
			objGrid.setCellExcellType(strRowID, 0, "sub_row_grid");						
<?
			}
?>			
			objGrid.attachEvent("onSubRowOpen", function(id,state){
				//GCRUZ 2015-10-02. Aplicar resize al row que contiene el subgrid para que no se encime sobre otros. Issue:P3B3R8
//				debugger;
				console.log("onSubRowOpen. id:"+id+" state:"+state);
				if (state == true)
				{
					var sectionTitle = this.cells(id, 1).getValue();
					console.log('Row Title:'+this.cells(id, 1).getValue());
					var subGridHeight = this.cells(id, 0).getSubGrid().entBox.style.height;
					console.log('SubGridHeigh='+subGridHeight);
					//Buscar el elemento seleccionado
/*
					var sectionRow = null;
					var arrRows = $('tr.odd_dhx_terrace ');
					for (var i=0; i<arrRows.length; i++)
					{
						if (arrRows[i].cells[1].innerHTML == sectionTitle)
						{
							sectionRow = arrRows[i];
						}
					}
					var arrRows = $('tr.ev_dhx_terrace ');
					for (var i=0; i<arrRows.length; i++)
					{
						if (arrRows[i].cells[1].innerHTML == sectionTitle)
						{
							sectionRow = arrRows[i];
						}
					}
*/
					var sectionRow = this.rowsAr[id];
					console.log("SectionHeight" + sectionRow.style.height);
					sectionRow.style.height = parseInt(subGridHeight) + 50 + 'px';
					console.log("New SectionHeight" + sectionRow.style.height);
				}
			});

			objGrid.attachEvent("onSubGridCreated", function(subgrid, rId, rInd) {
				var objSubGrid = subgrid;
				switch (rId) {
<?
			foreach ($objSectionColl->Collection as $objSection) {
				if ($objSection->SectionType != sectMasterDet && $objSection->SectionType != sectInline) {
					continue;
				}
				
				$intSectionID = $objSection->SectionID;
?>					
					case "Section<?=$intSectionID?>":
<?
				$strHeader = "";
				$strHeaderWidthPerc = "";
				$strHeaderWidth = "";
				$strHeaderAlign = "";
				$strHeaderTypes = "";
				$strHeaderResize = "";
				$arrHeaderStyle = array();
				$strAnd = "";
				$intPos = -1;
				
				//Genera el header del subgrid
				$objQuestionColl = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
				//GCRUZ 2015-11-12. Modificar el orden de las preguntas para poner selector como inicial
				//Modificar el orden de las preguntas para poner a selector como la inicial
				if ($objSection->SelectorQuestionID != 0)
				{
					//Buscar la pregunta selector y sacarla del arreglo
					$questionKey = -1;
					foreach ($objQuestionColl->Collection as $key => $objQuestion)
					{
						if ($objQuestion->QuestionID == $objSection->SelectorQuestionID)
						{
							$questionKey = $key;
						}
					}
					//Validar si en realidad se encontró la pregunta selector
					if ($questionKey != -1)
					{
						//Sacar la pregunta del arreglo
						$question = array_splice($objQuestionColl->Collection, $questionKey, 1);
						$question = $question[0];
						//Meter de nuevo la pregunta al inicio del arreglo
						array_unshift($objQuestionColl->Collection, $question);
					}
				}
				foreach ($objQuestionColl->Collection as $objQuestion) {
					if ($objQuestion->SectionID != $intSectionID) {
						continue;
					}
					
					//GCRUZ 2015-11-13. Cambiar el nombre de la pregunta por el nombre corto
	                $QuestionText = $objQuestion->getAttributeName();
					$ShortText = $QuestionText;
					/*$ShortText = (strlen($QuestionText) >= 24) ? substr($QuestionText, 0, 24) . "..." : $QuestionText;*/
					//@JAPR 2015-10-09: Corregido un bug, debe escapar las comillas (#62XBCE)
					$ShortText = str_replace("\"", "\\\"", $ShortText);
					//Valida si la pregunta tiene o no un valor que capturar como respuesta
					$valid = true;
					switch ($objQuestion->QTypeID) {
						case qtpPhoto:
						case qtpDocument:
						case qtpSignature:
						case qtpSketch:
						//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
						case qtpSketchPlus:
						//@JAPR
						case qtpSkipSection:
						case qtpMessage:
						case qtpSync:
						case qtpPassword:
						case qtpAudio:
						case qtpVideo:
						case qtpSection:
						//OMMC 2016-02-02: Agregado para las OCR
						case qtpOCR:
						//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
						case qtpExit:
						//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
						case qtpUpdateDest:
							$valid = false;
							break;
					}
					if($valid){
						//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
						//GCRUZ 2015-09-01. Manipulamos únicamente la primera pregunta para meter el campo SectionDesc
						//GCRUZ 2015-09-01. Si la sección inline tiene atributos de catálogo se agregarán todas las columnas
						$strPropName = $objQuestion->SurveyField;
						if ( $objSection->SectionType == sectInline && isset($anInstance->{$strPropName}["sectionDesc"]))
						{
							if ($objSection->ValuesSourceType == 1 && $objSection->DataSourceMemberID > 0 && isset($anInstance->{$strPropName}["sectionDescAttributesNames"]) && isset($anInstance->{$strPropName}["sectionDescAttributesValues"]))
							{
								foreach ($anInstance->{$strPropName}["sectionDescAttributesNames"][0] as $anAttributeName)
								{
									//@JAPR 2016-12-02: Corregido un bug con comillas y otros caracteres que rompen string en JScript (#KPCWW3)
									$strHeader .= $strAnd.GetValidJScriptText($anAttributeName);
									//@JAPR
									$strHeaderWidth .= $strAnd."200";
									$strHeaderAlign .= $strAnd."left";
									$strHeaderTypes .= $strAnd."ro";
									$strHeaderResize .= $strAnd."true";
									$arrHeaderStyle[] = "\"background-color:cccccc;\"";
									$strAnd = ",";	
								}
							}
							else
							{
								$strHeader .= $strAnd.translate('SectionDesc');
								$strHeaderWidth .= $strAnd."200";
								$strHeaderAlign .= $strAnd."left";
								$strHeaderTypes .= $strAnd."ro";
								$strHeaderResize .= $strAnd."true";
								$arrHeaderStyle[] = "\"background-color:cccccc;\"";
								$strAnd = ",";	
							}
						}

						$strHeader .= $strAnd.$ShortText;
						$strHeaderWidth .= $strAnd."200";
						$strHeaderAlign .= $strAnd."left";
						$strHeaderTypes .= $strAnd."ro";
						$strHeaderResize .= $strAnd."true";
						$arrHeaderStyle[] = "\"background-color:cccccc;\"";
						$strAnd = ",";	
					}
					
					//@AAL 20/07/2015 Si la pregunta contiene foto agregar su columna caorrespondiete
					//GCRUZ 2015-10-23. Preguntas tipo barcode no moestrarán la imagen.
					if ($objQuestion->HasReqPhoto > 0 && $objQuestion->QTypeID != qtpBarCode) {
						/*ears 2016-11-23 se quitan los puntos suspensivos al habilitar la función multilinea se pueden observar completos los nombres de preguntas o secciones*/
						/*$ShortText = (strlen($QuestionText) >= 8) ? substr($QuestionText, 0, 8) . "..." : $QuestionText;*/
						$ShortText = $QuestionText;
						//@JAPR 2015-10-09: Corregido un bug, debe escapar las comillas (#62XBCE)
						$ShortText = str_replace("\"", "\\\"", $ShortText);
						$strHeader .= $strAnd.$ShortText."_".translate("Photo");
						$strHeaderWidth .= $strAnd."200";
						$strHeaderAlign .= $strAnd."left";
						$strHeaderTypes .= $strAnd."ro";
						$strHeaderResize .= $strAnd."true";
						$arrHeaderStyle[] = "\"background-color:cccccc;\"";
						$strAnd = ",";
					}
					//@AAL 20/07/2015 Si la pregunta contiene Comentario agregar su columna caorrespondiete
					if ($objQuestion->HasReqComment > 0) {
						/*ears 2016-11-23 se quitan los puntos suspensivos al habilitar la función multilinea se pueden observar completos los nombres de preguntas o secciones*/
						/*$ShortText = (strlen($QuestionText) >= 8) ? substr($QuestionText, 0, 8) . "..." : $QuestionText;*/
						$ShortText = $QuestionText;
						//@JAPR 2015-10-09: Corregido un bug, debe escapar las comillas (#62XBCE)
						$ShortText = str_replace("\"", "\\\"", $ShortText);
						$strHeader .= $strAnd.$ShortText."_".translate("Comment");
						$strHeaderWidth .= $strAnd."200";
						$strHeaderAlign .= $strAnd."left";
						$strHeaderTypes .= $strAnd."ro";
						$strHeaderResize .= $strAnd."true";
						$arrHeaderStyle[] = "\"background-color:cccccc;\"";
						$strAnd = ",";
					}
					//@AAL 20/07/2015 Si la pregunta contiene Documento agregar su columna caorrespondiete
					if ($objQuestion->HasReqDocument > 0) {
						/*ears 2016-11-23 se quitan los puntos suspensivos al habilitar la función multilinea se pueden observar completos los nombres de preguntas o secciones*/
						/*$ShortText = (strlen($QuestionText) >= 8) ? substr($QuestionText, 0, 8) . "..." : $QuestionText;*/
						$ShortText = $QuestionText;
						//@JAPR 2015-10-09: Corregido un bug, debe escapar las comillas (#62XBCE)
						$ShortText = str_replace("\"", "\\\"", $ShortText);
						$strHeader .= $strAnd.$ShortText."_".translate("Document");
						$strHeaderWidth .= $strAnd."200";
						$strHeaderAlign .= $strAnd."left";
						$strHeaderTypes .= $strAnd."ro";
						$strHeaderResize .= $strAnd."true";
						$arrHeaderStyle[] = "\"background-color:cccccc;\"";
						$strAnd = ",";
					}
				}
				
				if (count($arrHeaderStyle)) {
?>
						//Genera el header del subgrid
						objSubGrid.setImagePath("images/");
						objSubGrid.setHeader("<?=$strHeader?>", null, [<?=implode(',', $arrHeaderStyle)?>]);
						objSubGrid.setInitWidths("<?=$strHeaderWidth?>");
						objSubGrid.setColAlign("<?=$strHeaderAlign?>");
						objSubGrid.setColTypes("<?=$strHeaderTypes?>");
						objSubGrid.enableResizing("<?=$strHeaderResize?>");
						objSubGrid.objBox.style.overflowX = "auto";
						objSubGrid.objBox.style.overflowY = "auto";
						objSubGrid.init();
						
						var intSubRowNum = 0;
<?
				
					//Genera las rows del subgrid
					$intRowCount = (int) @$anInstance->SectRowsCount[$intSectionID];
					for ($intRowIndex = 0; $intRowIndex < $intRowCount; $intRowIndex++) {
						$strValues = "";
						$strAnd = "";
						foreach ($objQuestionColl->Collection as $objQuestion) {
							if ($objQuestion->SectionID != $intSectionID) {
								continue;
							}
							
							$intQuestionID = $objQuestion->QuestionID;
							$strPropName = $objQuestion->SurveyField;

							//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
							//GCRUZ 2015-09-01. Manipulamos únicamente la primera pregunta para meter el campo SectionDesc
							//GCRUZ 2015-09-01. Si la sección inline tiene atributos de catálogo se agregarán todas las columnas
							$strValueSectionDesc = '';
							$arrValueSectionDescAttributes = array();
							if ( $objSection->SectionType == sectInline && isset($anInstance->{$strPropName}["sectionDesc"]))
							{
								if ($objSection->ValuesSourceType == 1 && $objSection->DataSourceMemberID > 0 && isset($anInstance->{$strPropName}["sectionDescAttributesNames"]) && isset($anInstance->{$strPropName}["sectionDescAttributesValues"]))
								{
									foreach ($anInstance->{$strPropName}["sectionDescAttributesValues"][$intRowIndex] as $anAttributeValue)
										$arrValueSectionDescAttributes[] = $anAttributeValue;
								}
								else {
									//@JAPR 2016-11-14: Corregidos los textos utilizados dentro de funciones de DHTMLX que generan JScript para escapar caracteres que podrían causar conflictos (#KPCWW3)
									//Se optó por unificar estos reemplazos con una función global en lugar de seguir haciéndolos manualmente en cada ocurrencia y diferentes según quien los programe
									$strValueSectionDesc = GetValidJScriptText((string) @$anInstance->{$strPropName}["sectionDesc"][$intRowIndex]);
								}
							}

							//GCRUZ 2015-11-13. Cambiar el nombre de la pregunta por el nombre corto
			                $QuestionText = $objQuestion->getAttributeName();
							/*ears 2016-11-23 se quitan los puntos suspensivos al habilitar la función multilinea se pueden observar completos los nombres de preguntas o secciones*/
							/*$ShortText = (strlen($QuestionText) >= 24) ? substr($QuestionText, 0, 24) . "..." : $QuestionText;*/
							$ShortText = $QuestionText;
							//@JAPR 2015-10-09: Corregido un bug, debe escapar las comillas (#62XBCE)
							$ShortText = str_replace("\"", "\\\"", $ShortText);
							$strValue = (string) @$anInstance->{$strPropName}["value"][$intRowIndex];
							//GCRUZ 2015-10-26. Escapar comillas. Issue: F5G2AY
							$strValue = str_replace("\"", "\\\"", $strValue);
							//@ears 2016-12-16 mostraba la frase Array = al consultar la información de la bitácora #A9EZGY
							$strValue = str_replace("Array =","", $strValue);
							//@AAL 30/07/2015 Comentado en esta parte no son utilizados. Se utilizan cuando cumplen la condicion de tener Img, Comment o Document.
							//$strImage = getImage($theRepository, $objQuestion, $anInstance->FactKeyDimVal, (string) @$anInstance->{$strPropName}["image"][$intRowIndex]);
							//$strDocument = getDocument($theRepository, $objQuestion, $anInstance->FactKeyDimVal, (string) @$anInstance->{$strPropName}["document"][$intRowIndex]);
							//$strComment = getComment($theRepository, $objQuestion, $anInstance->FactKeyDimVal, (string) @$anInstance->{$strPropName}["comment"][$intRowIndex]);
							//Valida si la pregunta tiene o no un valor que capturar como respuesta
							
							$valid = true;
							switch ($objQuestion->QTypeID) {
								case qtpPhoto:
								case qtpDocument:
								case qtpSignature:
								case qtpSketch:
								//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
								case qtpSketchPlus:
								//@JAPR
								case qtpSkipSection:
								case qtpMessage:
								case qtpSync:
								case qtpPassword:
								case qtpAudio:
								case qtpVideo:
								case qtpSection:
								//OMMC 2016-02-02: Agregado para las OCR
								case qtpOCR:
								//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
								case qtpExit:
								//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
								case qtpUpdateDest:
									$valid = false;
									break;
							}
							
							if($valid){										
								//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
								//GCRUZ 2015-09-01. Manipulamos únicamente la primera pregunta para meter el campo SectionDesc
								if ($strValueSectionDesc != '')
								{
									//@JAPR 2016-12-02: Corregido un bug con comillas y otros caracteres que rompen string en JScript (#KPCWW3)
									$strValues .= $strAnd."\"".GetValidJScriptText($strValueSectionDesc)."\"";
									//@JAPR
									$strAnd = ", ";	
								}
								//GCRUZ 2015-09-01. Si la sección inline tiene atributos de catálogo se agregarán todas las columnas
								if (count($arrValueSectionDescAttributes) > 0)
								{
									foreach ($arrValueSectionDescAttributes as $anAttributeValue)
									{
										//@JAPR 2016-12-02: Corregido un bug con comillas y otros caracteres que rompen string en JScript (#KPCWW3)
										$strValues .= $strAnd."\"".GetValidJScriptText($anAttributeValue)."\"";
										//@JAPR
										$strAnd = ", ";	
									}
								}

								//@JAPR 2016-12-02: Corregido un bug con comillas y otros caracteres que rompen string en JScript (#KPCWW3)
								$strValues .= $strAnd."\"".GetValidJScriptText($strValue)."\"";
								//@JAPR
								$strAnd = ", ";	
							}
							
							//@AAL 20/07/2015 Si la pregunta contiene foto agregar su columna caorrespondiete
							//OMMC 2016-02-02: Agregadas las OCR
							if ($objQuestion->HasReqPhoto > 0 || ($objQuestion->QTypeID == qtpOCR && $objQuestion->SavePhoto > 0)) {
								$strImage = getImage($theRepository, $objQuestion, $anInstance->FactKeyDimVal, (string) @$anInstance->{$strPropName}["image"][$intRowIndex]);
								$strImage = str_replace("100px","80px",$strImage);
								$strImage = str_replace("140px","80px",$strImage);
								$strValues .= $strAnd."\"{$strImage}\"";
								$strAnd = ", ";
							}
							//@AAL 20/07/2015 Si la pregunta contiene Comentario agregar su columna caorrespondiete
							if ($objQuestion->HasReqComment > 0) {
								$intSectionKey=(int)@$anInstance->SectRowsKeys[$objQuestion->SectionID][$intRowIndex];
								$strComment = getComment($theRepository, $objQuestion, $anInstance->FactKeyDimVal, (string) @$anInstance->{$strPropName}["comment"][$intRowIndex],$intSectionKey);
								$strValues .= $strAnd."\"{$strComment}\"";
								$strAnd = ", ";
							}
							//@AAL 20/07/2015 Si la pregunta contiene Documento agregar su columna caorrespondiete
							if ($objQuestion->HasReqDocument > 0){
								$strDocument = getDocument($theRepository, $objQuestion, $anInstance->FactKeyDimVal, (string) @$anInstance->{$strPropName}["document"][$intRowIndex]);
								$strValues .= $strAnd."\"{$strDocument}\"";
								$strAnd = ", ";
							}
						}
?>
						//Genera las rows del subgrid
						objSubGrid.addRow(intSubRowNum++, [<?=$strValues?>]);
						//objSubGrid.setCellTextStyle(intSubRowNum++, 1, strPropColStyle);
<?
					}
				}
				
				//Al final agrega una row vacia para que no obstruya el Scroll el contenido de la última row
?>				
						objSubGrid.addRow(intSubRowNum++, [""]);
						setTimeout(function() {
							objSubGrid.objBox.style.overflowX = "auto";
							objSubGrid.objBox.style.overflowY = "auto";
						}, 100);
						break;
<?
			}
?>
				}
			});
			
			$(window).resize(function() {
				doOnResizeScreen();
			});
			
			function doOnResizeScreen() {
				if (objGrid) {
					objGrid.setSizes();
				}
			}
<?
	} else {
?>
            var objGrid = new dhtmlXGridObject('ShowData');
            objGrid.setHeader(" , ");
            objGrid.setInitWidths("200");
            objGrid.setStyle("", "font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);font-family:Arial;font-size:9pt;");
            //objGrid.setStyle("background-color:rgb(254,204,98);font-family:Arial;font-weight:bold;font-size:10pt;", "background-color:rgb(240,240,240);color:rgb(51,51,51);font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);color:rgb(240,240,240);font-family:Arial;font-size:9pt;");
            //Para los scrollbars del gridbox
            objGrid.objBox.style.overflowX = "hidden";  
            objGrid.objBox.style.overflowY = "auto";
            objGrid.init();
            objGrid.addRow(1, ["<?=translate('User Name')?>", "<?= $anInstance->UserName ?>"]);
            objGrid.setCellTextStyle(1, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
            objGrid.addRow(2, ["<?=translate('Survey')?>", "<?= $anInstance->SurveyName ?>"]);
            objGrid.setCellTextStyle(2, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
            objGrid.addRow(3, ["<?=translate('Date')?>", "<?= $anInstance->DateID ?>"]);
            objGrid.setCellTextStyle(3, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
            var Idx = 4;
            var rowValues = [];
            
        <? 
            for($i = 0; $i < $anInstance->NumQuestionFields; $i++)
            {
                $fieldName = $anInstance->QuestionFields[$i];
                $QuestionText = $anInstance->Questions[$i]->QuestionText;
                $ShortText = (strlen($QuestionText) >= 24) ? substr($QuestionText, 0, 24) . "..." : $QuestionText;
				//@JAPR 2015-10-09: Corregido un bug, debe escapar las comillas (#62XBCE)
				$ShortText = str_replace("\"", "\\\"", $ShortText);
                if($anInstance->Questions[$i]->HasReqPhoto == 1 || $anInstance->Questions[$i]->HasReqPhoto == 2){ ?>
                    objGrid.addRow(Idx, ["<?= $ShortText ?>", "<?= getImage($theRepository, $anInstance->Questions[$i], $anInstance->FactKeyDimVal) ?>"]);
                    objGrid.cellById(Idx,0).setAttribute("title","<?= $QuestionText ?>");
                    objGrid.setCellTextStyle(Idx++, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
            <?  }elseif ($anInstance->Questions[$i]->HasReqDocument == 1 || $anInstance->Questions[$i]->HasReqDocument == 2){ ?>
                    objGrid.addRow(Idx, ["<?= $ShortText ?>", "<?= getDocument($theRepository, $anInstance->Questions[$i], $anInstance->FactKeyDimVal) ?>"]);
                    objGrid.cellById(Idx,0).setAttribute("title","<?= $QuestionText ?>");
                    objGrid.setCellTextStyle(Idx++, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
            <?  }elseif ($anInstance->Questions[$i]->HasReqComment == 1 || $anInstance->Questions[$i]->HasReqComment == 2){ ?>
                    objGrid.addRow(Idx, ["<?= $ShortText ?>", "<?= getComment($theRepository, $anInstance->Questions[$i], $anInstance->FactKeyDimVal) ?>"]);
                    objGrid.cellById(Idx,0).setAttribute("title","<?= $QuestionText ?>");
                    objGrid.setCellTextStyle(Idx++, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
            <?  }else{ ?>
                    objGrid.addRow(Idx, ["<?= $ShortText ?>", "<?= $anInstance->$fieldName ?>"]);
                    objGrid.cellById(Idx,0).setAttribute("title","<?= $QuestionText ?>");
                    objGrid.setCellTextStyle(Idx++, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
        <?      }
            } ?>
<?
	}
?>
        </script>
    </body>
</html>