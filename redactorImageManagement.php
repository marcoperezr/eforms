<?php
require_once("checkCurrentSession.inc.php");
require_once("utils.inc.php");
//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
require_once("uploadFileExt.php");
//@JAPR

$pos = strpos($_SESSION["PAHTTP_REFERER"],"build_page.php");
$pathImg = substr($_SESSION["PAHTTP_REFERER"],0,$pos);
$path = str_replace("\\", "/", realpath("."))."/customerimages/".$_SESSION["PABITAM_RepositoryName"];

ob_start();
if (!file_exists($path)) {
	if (!mkdir($path, null, true)) {
		$strErrDesc = trim(ob_get_contents());
		$strErrDesc = translate("File not uploaded").", ".translate("Error").": ".$strErrDesc;
		$blnError = true;
		//@JAPR 2018-02-08: Faltaba reportar los errores, la propiedad extra del objeto cuando no hay state o este es false, indica el error que se reportará 
		//en el evento onImageUploadFail (#JMK4PR)
		ob_end_clean();
		$array = array(
			'error' => true,
			'message' => translate("Invalid file extension")
		);
		echo json_encode($array); 
		die();
		//@JAPR
	}
}
//@JAPR 2018-02-08: Se debe desactivar el buffer al terminar de usarlo (#JMK4PR)
ob_end_clean();
//@JAPR

$_FILES['file']['type'] = strtolower($_FILES['file']['type']);
 
if ($_FILES['file']['type'] == 'image/png'
|| $_FILES['file']['type'] == 'image/jpg'
|| $_FILES['file']['type'] == 'image/gif'
//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
//Estandarizaros los tipos de archivos de imagen soportados en eForms
//@JAPR 2018-02-07: Corregido un bug, la librería redactor por alguna razón internamente convertía la extensión de los archivos .jpg a .jpeg, así que al validar
//solo los tipos de extensión de arriba y excluir .jpeg, se dejó de soportar inconscientemente a los .jpg. No se analizó exactamente por qué hace eso la librería,
//pero como es una librería externa se permitirá por ahora simplemente que suba las mismas extensiones documentadas aquí aunque en otros puntos del producto no sea
//permitido, para evitar que los procesos internos de la librería afecten a eForms (#JMK4PR)
|| $_FILES['file']['type'] == 'image/jpeg'
|| $_FILES['file']['type'] == 'image/pjpeg')
{
    //OMMC 2015-12-16: Agregada la función de StripSpecialCharsFromFileNames para reemplazar caracteres especiales del nombre de las fotografías del editor HTML. #3GQ6PE
    //@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
    $filename = replaceInvalidFilenameChars(StripSpecialCharsFromFileNames($_FILES['file']['name'], "photo"));
    $arrFileInfo = getExtendedFileInfo($filename);
    
    if ( $arrFileInfo && is_array($arrFileInfo) ) {
        if ( !checkValidExtension(@$arrFileInfo["fileExtension"], null, true) ) {
            //Faltaba reportar los errores, la propiedad extra del objeto cuando no hay state o este es false, indica el error que se reportará en el evento onImageUploadFail
            $blnError = true;
            $array = array(
                'error' => true,
                'message' => translate("Invalid file extension")
            );
            echo json_encode($array); 
            die();
        }
    }
    //@JAPR
    $file = "customerimages/".$_SESSION["PABITAM_RepositoryName"]."/".$filename;
	
    // copying
    //OMMC 2010-10-08 Se agrega decoding para caracteres especiales
    move_uploaded_file($_FILES['file']['tmp_name'], utf8_decode($file));
 
    // displaying file
    $array = array(
        'filelink' => $pathImg."customerimages/".$_SESSION["PABITAM_RepositoryName"]."/".$filename
    );
    
    echo json_encode($array); 
 
}
//@JAPR 2018-02-08: Faltaba reportar los errores, la propiedad extra del objeto cuando no hay state o este es false, indica el error que se reportará en el evento 
//onImageUploadFail (#JMK4PR)
else {
	$blnError = true;
	$array = array(
		'error' => true,
		'message' => translate("Invalid file extension")
	);
	echo json_encode($array); 
	die();
}
//@JAPR
?>