<?php
//@JAPR 2015-10-21: Agregada la validación de eliminación de mas tipos de objetos para v6, ahora este contiene la función que realiza la validación de uso del objeto especificado
//y se podrá invocar desde diferentes archivos (con sesión previamente iniciada) para validar y obtener una respuesta en el formato que sea necesaria
/* Esta función obtendrá todas las referencias el objeto indicado en la metadata de eForms en base a los parámetros especificados
El parámetro $iValidationType cambió su sentido, si no se envía o es 0, se asume que se quiere la revisión completa, si se envía con 1 se requiere buscar referencias de vairables,
mientras que si se envía como 2 se buscan las referencias directas mediante configuraciones con IDs
*/
function CheckObjectReferences($aRepository, $iObjectType, $iObjectID, $iValidationType, $aFieldsColl = null, $bReturArray = false, $bGetIDs = false) {
	$intObjectType = (int) @$iObjectType;
	$intObjectID = (int) @$iObjectID;
	$intValidationType = (int) @$iValidationType;
	
	$response = "OK";
	$strResponse = '';
	if ($bReturArray) {
		$response = array();
		$strResponse = array();
	}
	switch ($intObjectType) {
		case otySection:
			//Para las secciones hay dos tipos de validaciones
			require_once("section.inc.php");
			$objSection = BITAMSection::NewInstanceWithID($aRepository, $intObjectID);
			if (!is_null($objSection)) {
				//@JAPR 2015-10-22: Removido el uso del parámetro $iValidationType, ya que ahora el borrado sólo se puede hacer desde un único lugar, así que tiene que realizar
				//siempre las comprobaciones completas
				$strAnd = '';
				//En este caso se verifica si la sección es utilizada en alguna otra configuración (regresa el detalle completo)
				$strVarsUsed = SearchVariablePatternInMetadata($aRepository, $objSection->SurveyID, $intObjectID, $intObjectType, $aFieldsColl, $bReturArray, $bGetIDs);
				if ($bReturArray) {
					$strResponse = array_merge_recursive($strResponse, $strVarsUsed);
				}
				else {
					if (trim($strVarsUsed) != '') {
						$strResponse .= $strAnd.$strVarsUsed;
						$strAnd = "\r\n";
					}
				}
				
				//En este caso se verifica si alguna de las preguntas de esta sección es utilizada en alguna otra configuración (sólo regresa los
				//nombres de las preguntas que se utilizan en otras configuraciones)
				//@JAPR 2015-10-25: Agregada la validación de uso de objetos en propiedades que se pueden limpiar o que cuya referencia puede quedarse sin afectar al producto
				//Ahora el array se regresará como un texto directamente con los casos encontrados
				$arrUsedQuestions = $objSection->getQuestionsWithVarReferences($bGetIDs);
				if (!$bGetIDs) {
					if (!$bReturArray) {
						if (trim($arrUsedQuestions) != '') {
							$strResponse .= $strAnd.$arrUsedQuestions;
							$strAnd = "\r\n";
						}
					}
				}
				else {
					if (!is_null($arrUsedQuestions) && is_array($arrUsedQuestions)) {
						if (count($arrUsedQuestions) > 0) {
							if ($bReturArray) {
								$strResponse = array_merge_recursive($strResponse, $strVarsUsed);
							}
							else {
								$strResponse .= $strAnd.implode("\r\n", $arrUsedQuestions);
								$strAnd = "\r\n";
							}
						}
					}
				}
				//@JAPR
				
				//Verifica si existen configuraciones directas que apunten a esta sección como un ID
				$strVarsUsed = SearchObjectReferenceInMetadata($aRepository, $objSection->SurveyID, $intObjectID, $intObjectType, $aFieldsColl, $bReturArray, $bGetIDs);
				if ($bReturArray) {
					$strResponse = array_merge_recursive($strResponse, $strVarsUsed);
				}
				else {
					if (trim($strVarsUsed) != '') {
						$strResponse .= $strAnd.$strVarsUsed;
						$strAnd = "\r\n";
					}
				}
			}
			break;
		
		case otyQuestion:
			//Para las preguntas la única validación es si ella se utiliza en alguna otra configuración
			require_once("question.inc.php");
			$objQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $intObjectID);
			if (!is_null($objQuestion)) {
				$strAnd = '';
				$strVarsUsed = SearchVariablePatternInMetadata($aRepository, $objQuestion->SurveyID, $intObjectID, $intObjectType, $aFieldsColl, $bReturArray, $bGetIDs);
				if ($bReturArray) {
					$strResponse = array_merge_recursive($strResponse, $strVarsUsed);
				}
				else {
					if (trim($strVarsUsed) != '') {
						$strResponse .= $strAnd.$strVarsUsed;
						$strAnd = "\r\n";
					}
				}
				
				//Verifica si existen configuraciones directas que apunten a esta pregunta como un ID
				$strVarsUsed = SearchObjectReferenceInMetadata($aRepository, $objQuestion->SurveyID, $intObjectID, $intObjectType, $aFieldsColl, $bReturArray, $bGetIDs);
				if ($bReturArray) {
					$strResponse = array_merge_recursive($strResponse, $strVarsUsed);
				}
				else {
					if (trim($strVarsUsed) != '') {
						$strResponse .= $strAnd.$strVarsUsed;
						$strAnd = "\r\n";
					}
				}
			}
			
			break;
		
		case otyDataSource:
			//Para los DataSources la única validación es si ellos se utilizan en alguna otra configuración
			require_once("dataSource.inc.php");
			$objDataSource = BITAMDataSource::NewInstanceWithID($aRepository, $intObjectID);
			if (!is_null($objDataSource)) {
				//@JAPR 2015-10-22: Removido el uso del parámetro $iValidationType, ya que ahora el borrado sólo se puede hacer desde un único lugar, así que tiene que realizar
				//siempre las comprobaciones completas
				$strAnd = '';
				
				//Verifica si existen configuraciones directas que apunten a este DataSource como un ID
				$strVarsUsed = SearchObjectReferenceInMetadata($aRepository, -1, $intObjectID, $intObjectType, $aFieldsColl, $bReturArray, $bGetIDs);
				if ($bReturArray) {
					$strResponse = array_merge_recursive($strResponse, $strVarsUsed);
				}
				else {
					if (trim($strVarsUsed) != '') {
						$strResponse .= $strAnd.$strVarsUsed;
						$strAnd = "\r\n";
					}
				}
				
				//Verifica si alguno de los atributos es usado directamente por preguntas, ya que esos casos no deben permitir eliminar tampoco al DataSource
				$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $intObjectID);
				if (!is_null($objDataSourceMembersColl)) {
					foreach ($objDataSourceMembersColl as $objDataSourceMember) {
						$strVarsUsed = CheckObjectReferences($aRepository, otyDataSourceMember, $objDataSourceMember->MemberID, $iValidationType, null, $bReturArray, $bGetIDs);
						if (trim($strVarsUsed) != '') {
							$strVarsUsed = str_ireplace("ERROR<SVSEP>", "", $strVarsUsed);
							$strResponse .= $strAnd."\r\n".translate("Attribute").": ".$objDataSourceMember->MemberName.$strVarsUsed;
							$strAnd = "\r\n";
						}
					}
				}
			}
			break;
		
		case otyDataSourceMember:
			//Para los atributos la única validación es si ellos se utilizan en alguna otra configuración
			require_once("dataSourceMember.inc.php");
			$objDataSourceMember = BITAMDataSourceMember::NewInstanceWithID($aRepository, $intObjectID);
			if (!is_null($objDataSourceMember)) {
				$strAnd = '';
				$strVarsUsed = SearchVariablePatternInMetadata($aRepository, -1, $intObjectID, $intObjectType, $aFieldsColl, $bReturArray, $bGetIDs);
				if ($bReturArray) {
					$strResponse = array_merge_recursive($strResponse, $strVarsUsed);
				}
				else {
					if (trim($strVarsUsed) != '') {
						$strResponse .= $strAnd.$strVarsUsed;
						$strAnd = "\r\n";
					}
				}
				
				//Verifica si existen configuraciones directas que apunten a este atributo como un ID
				$strVarsUsed = SearchObjectReferenceInMetadata($aRepository, -1, $intObjectID, $intObjectType, $aFieldsColl, $bReturArray, $bGetIDs);
				if ($bReturArray) {
					$strResponse = array_merge_recursive($strResponse, $strVarsUsed);
				}
				else {
					if (trim($strVarsUsed) != '') {
						$strResponse .= $strAnd.$strVarsUsed;
						$strAnd = "\r\n";
					}
				}
			}
			break;
	}
	
	if ($bReturArray) {
		$response = $strResponse;
	}
	else {
		if ($strResponse != '') {
			$response = "ERROR<SVSEP>".$strResponse;
		}
	}
	
	return $response;
}
?>