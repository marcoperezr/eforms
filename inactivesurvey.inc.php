<?php
require_once("survey.inc.php");

class BITAMInactiveSurvey extends BITAMSurvey
{
	function __construct($aRepository)
	{
		BITAMSurvey::__construct($aRepository);
	}

	static function NewInstance($aRepository)
	{
		return new BITAMInactiveSurvey($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aSurveyID)
	{
		$anInstance = null;
		
		if (((int)  $aSurveyID) < 0)
		{
			return $anInstance;
		}

    //@LROUX 2011-10-18: Se agrega AllowPartialSave.		
		$sql = "SELECT t1.SurveyID, t1.SurveyName, t1.Status, t1.ModelID, t1.ClosingMsg, t1.CountIndID, t1.AnsweredQuestionsIndID, t1.DurationIndID, t1.LatitudeIndID,t1.LongitudeIndID, t1.UserDimID, 
				t1.SectionDimID, t1.CaptureStartTime, t1.CaptureEndTime, t1.HelpMsg, t1.CatalogID, t1.SurveyType, t1.HasSignature, t1.AllowPartialSave, t1.EmailDimID, t1.StatusDimID, t1.SchedulerDimID 
				FROM SI_SV_Survey t1 WHERE t1.SurveyID = ".((int)$aSurveyID);
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMInactiveSurvey::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMInactiveSurvey::NewInstance($aRepository);
		$anInstance->SurveyID = (int) $aRS->fields["surveyid"];
		$anInstance->SurveyName = $aRS->fields["surveyname"];
		$anInstance->SurveyType = (int) $aRS->fields["surveytype"];
		$anInstance->Status = (int)$aRS->fields["status"];
		$anInstance->ModelID = (int)$aRS->fields["modelid"];
		$anInstance->SurveyTable = "SVSurvey_".$anInstance->ModelID;
		$anInstance->ClosingMsg = $aRS->fields["closingmsg"];
		$anInstance->CountIndID = (int)$aRS->fields["countindid"];
		$anInstance->DurationIndID = (int)$aRS->fields["durationindid"];
		//Latitude y Longitude
		$anInstance->LatitudeIndID = (int)$aRS->fields["latitudeindid"];
		$anInstance->LongitudeIndID = (int)$aRS->fields["longitudeindid"];
		$anInstance->UserDimID = (int)$aRS->fields["userdimid"];
		$anInstance->SectionDimID = (int)$aRS->fields["sectiondimid"];
		$anInstance->CaptureStartTime = $aRS->fields["capturestarttime"];
		$anInstance->CaptureEndTime = $aRS->fields["captureendtime"];
		$anInstance->HelpMsg = $aRS->fields["helpmsg"];
		$anInstance->CatalogID = (int)$aRS->fields["catalogid"];
		$anInstance->OldCatalogID = (int)$aRS->fields["catalogid"];
		$anInstance->HasSignature = (int)$aRS->fields["hassignature"];
    //@LROUX 2011-10-18: Se agrega AllowPartialSave.
		$anInstance->AllowPartialSave = (int)$aRS->fields["allowpartialsave"];

		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("SurveyID", $aHTTPRequest->POST))
		{
			$aSurveyID = $aHTTPRequest->POST["SurveyID"];
			
			if (is_array($aSurveyID))
			{
				
				$aCollection = BITAMInactiveSurveyCollection::NewInstance($aRepository, $aSurveyID);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMInactiveSurvey::NewInstanceWithID($aRepository, (int)$aSurveyID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMInactiveSurvey::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMInactiveSurvey::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("SurveyID", $aHTTPRequest->GET))
		{
			$aSurveyID = $aHTTPRequest->GET["SurveyID"];

			$anInstance = BITAMInactiveSurvey::NewInstanceWithID($aRepository, (int)$aSurveyID);

			if (is_null($anInstance))
			{
				$anInstance = BITAMInactiveSurvey::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMInactiveSurvey::NewInstance($aRepository);
		}
		return $anInstance;
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=InactiveSurvey";
		}
		else
		{
			return "BITAM_PAGE=InactiveSurvey&SurveyID=".$this->SurveyID;
		}
	}
	
	function get_Parent()
	{
		return BITAMInactiveSurveyCollection::NewInstance($this->Repository, null);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
}

class BITAMInactiveSurveyCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}

	static function NewInstance($aRepository, $anArrayOfSurveyIDs = null)
	{
		$anInstance = new BITAMInactiveSurveyCollection($aRepository);

		$where = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$where = " AND t1.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$aSurveyID; 
					}
					if ($where != "")
					{
						$where = " AND t1.SurveyID IN (".$where.")";
					}
					break;
			}
		}
    //@LROUX 2011-10-18: Se agrega AllowPartialSave.
		$sql = "SELECT t1.SurveyID, t1.SurveyName, t1.Status, t1.ModelID, t1.ClosingMsg, t1.AnsweredQuestionsIndID, t1.CountIndID, t1.DurationIndID, t1.LatitudeIndID, t1.LongitudeIndID, t1.UserDimID, 
		t1.SectionDimID, t1.CaptureStartTime, t1.CaptureEndTime, t1.HelpMsg, t1.CatalogID, t1.SurveyType, t1.HasSignature, t1.AllowPartialSave, t1.EmailDimID, t1.StatusDimID, t1.SchedulerDimID 
		FROM SI_SV_Survey t1 WHERE t1.Status = 2 ".$where." ORDER BY t1.SurveyName";
		
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMInactiveSurvey::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}

		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("changeStatusToActive", $aHTTPRequest->GET) && array_key_exists("strSurveyIDs", $aHTTPRequest->GET))
		{	
			$strSurveyIDs = $aHTTPRequest->GET["strSurveyIDs"];
			
			BITAMInactiveSurveyCollection::changeToStatusToActive($aRepository, $strSurveyIDs);
		}

		return BITAMInactiveSurveyCollection::NewInstance($aRepository, null);
	}

	static function changeToStatusToActive($aRepository, $strSurveyIDs)
	{
		$arraySurveyIDs = explode("|", $strSurveyIDs);
		
		//Cambiar a estado Active
		if(count($arraySurveyIDs)>0)
		{
			$sql = "UPDATE SI_SV_Survey SET Status = 1 WHERE SurveyID IN (".implode(",", $arraySurveyIDs).")";
	
			if ($aRepository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}

	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		$title= translate("Inactive Surveys");
		
		return $title;
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=InactiveSurveyCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=InactiveSurvey";
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SurveyID';
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return false;	
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}

	function displayCheckBox($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}

 	function addButtons($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			if (count($this->Collection)>0)
			{
				$strChangeToStatusText = str_ireplace("{var1}", translate("Active"), translate("Change to status: {var1}"));
?>
			<!--<a href="#" id="changeSurveyStatusBtn" class="alinkescfav" onclick="changeSurveyStatus();"><img src="images/reopen.gif" displayMe="1">&nbsp;<?=$strChangeToStatusText?>&nbsp;&nbsp;</a>-->
			<button id="btnChangeSurveyStatus" class="alinkescfav" onclick="changeSurveyStatus();"><img src="images/reopen.gif" alt="<?=$strChangeToStatusText?>" title="<?=$strChangeToStatusText?>" displayMe="1" /> <?=$strChangeToStatusText?></button>
<?
			}
		}
	}

	function generateBeforeFormCode($aUser)
 	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
?>
 		<script language="JavaScript">
 		function changeSurveyStatus()
		{
			var cont = 0;
			var elements = BITAMInactiveSurveyCollection_SelectForm.elements;
			var strSurveyIDs="";
			var numElements = elements.length;
			for (var i = 0; i < numElements; i++)
			{
				if (elements[i].name == "SurveyID[]" && elements[i].checked==true)
				{
					if(strSurveyIDs!="")
					{
						strSurveyIDs+="|";
					}
					strSurveyIDs+=elements[i].value;
					cont++;
				}
			}
			
			if (cont == 0)
			{
				 alert('<?= sprintf(translate("Please select which %s to change status"), translate("Surveys"))?>');
				 return;
			}
			
 			var answer = confirm('<?= translate("Do you want to change to status Active all selected surveys?")?>');
	
			if(answer)
			{
				//C�digo para poder debuguear la copia de Surveys
				//document.getElementById("iFrameChangeSurveyStatus").style.display='inline';
				
				frmChangeSurveyStatus.strSurveyIDs.value = strSurveyIDs;
				frmChangeSurveyStatus.submit();
			}
		}
 		</script>
		
		<!--iframe para poder debuggear en el Zend el cambio de status de encuestas-->
		<!--
		<iframe id="iFrameChangeSurveyStatus" name="iFrameChangeSurveyStatus" style="display:none" width="100%" height="300px"></iframe>
		<form name="frmChangeSurveyStatus" action="main.php" method="get" target="iFrameCopySurveys">
			<input type="hidden" name="BITAM_SECTION" value="InactiveSurveyCollection">
			<input type="hidden" name="changeStatusToActive" id="changeStatusToActive">
			<input type="hidden" id="strSurveyIDs" name="strSurveyIDs" value="">
		</form>
		-->
		<form name="frmChangeSurveyStatus" action="main.php" method="GET" target="body">
			<input type="hidden" name="BITAM_SECTION" value="InactiveSurveyCollection">
			<input type="hidden" name="changeStatusToActive" id="changeStatusToActive">
			<input type="hidden" id="strSurveyIDs" name="strSurveyIDs" value="">
		</form>
 <?
		}
 	}
}
?>