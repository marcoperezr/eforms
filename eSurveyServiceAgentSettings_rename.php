<?php
//@JAPR 2018-02-16: Agregado el agente de reprocesamiento automático de destinos de datos, además del archivo de configuraciones local del agente (#E5YASV)
//*******************	Este Archivo NO se integrará a control de código, ya que contiene configuraciones locales de cada server	*******************
//Comentar las líneas siguientes cuando se desee desactivar la funcionalidad, o en su defecto renombrar/eliminar este archivo para dejar al agente con la configuración
//default

//Activar el modo de depuración para que se queden todos los archivos de procesamiento de las capturas en el directorio log incluso si no hay error, esto permitirá
//analizar capturas específicas en casos de error de manera mas detallada
$blnDebugAgentMode = false;
//@JAPR 2019-02-12: Modificado el comportamiento para permitir generar el Log del agente en tiempo de ejecución, enviando el parámetro para que se escriban los eventos mientras suceden
//y no hasta que ha terminado todo el proceso (#P5SE4W)
$blnSaveDebugLogOnTheFly = false;
?>