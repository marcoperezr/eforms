<?php
//Esta clase permite configurar la vista de la App, seleccionando entre los colores y varios elementos que se enviarán al App para cambiar el Look
require_once("repository.inc.php");
require_once("initialize.php");

class BITAMEFormsAppCustomization extends BITAMObject
{
	public $TemplateID;					//ID de definición de templates para permitir reutilizar en diferentes usuarios como si fuera un catálogo sin tener que perder antiguas definiciones
	public $UserID;						//ID del usuario que realiza la personalización, si no se especifica se asume que se trata de la configuración global (default para los usuarios que no tengan personalización)
	public $SettingID;					//ID único de la configuración
	public $SettingName;
	public $SettingValue;
	public $ArrayVariables;
	public $ArrayVariableValues;
	public $ArrayVariablesExist;
	public $TemplateName;				//Cuando se carga la instancia para grabar un template, contiene el nombre asignado al mismo
	public $isNew;
	
  	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->TemplateID = -1;
		$this->UserID = -1;
		$this->SettingID = -1;
		$this->SettingName= "";
		$this->SettingValue= "";
		$this->ArrayVariables=array();
		$this->ArrayVariableValues=array();
		$this->ArrayVariablesExist=array();
		$this->TemplateName = '';
		$this->isNew = true;
	}

	static function NewInstance($aRepository)
	{
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $aUserID = -1);
	}

	static function NewInstanceWithID($aRepository, $aSettingsVariableID, $aUserID = -1, $aTemplateID = -1)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		
		if (((int)  $aSettingsVariableID) < 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.TemplateID, A.UserID, A.SettingID, A.SettingName, A.SettingValue, B.TemplateName 
			FROM SI_SV_AppCustomization A 
				LEFT OUTER JOIN SI_SV_AppCustomizationTpls B ON A.TemplateID = B.TemplateID 
			WHERE A.TemplateID = {$aTemplateID} AND A.UserID = {$aUserID} AND A.SettingID = ".((int) $aSettingsVariableID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_AppCustomization ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewInstanceWithName($aRepository, $aSettingsVariableName, $aUserID = -1, $aTemplateID = -1)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		
		if (trim($aSettingsVariableName) == '')
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.TemplateID, A.UserID, A.SettingID, A.SettingName, A.SettingValue, B.TemplateName 
			FROM SI_SV_AppCustomization A 
				LEFT OUTER JOIN SI_SV_AppCustomizationTpls B ON (A.TemplateID = B.TemplateID) 
			WHERE A.TemplateID = {$aTemplateID} AND A.UserID = {$aUserID} AND A.SettingName = ".$aRepository->DataADOConnection->Quote($aSettingsVariableName);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_AppCustomization ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository);
		$anInstance->TemplateID = (int) @$aRS->fields["templateid"];
		$anInstance->TemplateName = (string) @$aRS->fields["templatename"];
		$anInstance->UserID = (int) @$aRS->fields["userid"];
		$anInstance->SettingID = (int) @$aRS->fields["settingid"];
		$anInstance->SettingName = (string) @$aRS->fields["settingname"];
		$anInstance->SettingValue = (string) @$aRS->fields["settingvalue"];
		$anInstance->isNew = false;
		return $anInstance;
	}
	
	//Llena la instancia de settings con los valores default
	function resetDefaultSettings()
	{
		$this->ArrayVariables = array();
		$this->ArrayVariableValues = array();
		
		//Esta definición de colores corresponde con los colores del eForms v4 original (tonos azules)
		$this->ArrayVariables[1] = 'CLRTMPLSectionTitleColor';	//Etiquetas H3 que funcuionan como el Título de inicio de las secciones
		$this->ArrayVariableValues['CLRTMPLSectionTitleColor'] = '333333';	//Section Title Color
		$this->ArrayVariables[2] = 'CLRTMPLOuterLabelColor';	//Etiquetas como el texto de las preguntas, generalmente están fuera de las combos, checkbox y demás
		$this->ArrayVariableValues['CLRTMPLOuterLabelColor'] = '333333';	//Outer Label Color
		$this->ArrayVariables[3] = 'CLRTMPLInnerLabelColor';	//Etiquetas dentro de componentes como checkbox, combos, etc.
		$this->ArrayVariableValues['CLRTMPLInnerLabelColor'] = '333333';	//Inner Label Color
		$this->ArrayVariables[4] = 'CLRTMPLButtonUpBorderColor';	//Color del border de los botones como los de configuración
		$this->ArrayVariableValues['CLRTMPLButtonUpBorderColor'] = '145072';	//Button Up Border Color
		$this->ArrayVariables[5] = 'CLRTMPLButtonUpBackgroundColorStart';	//Color de fondo de los botones como los de configuración
		$this->ArrayVariableValues['CLRTMPLButtonUpBackgroundColorStart'] = '5f9cc5';	//Button Up Background Color Start
		$this->ArrayVariables[6] = 'CLRTMPLButtonUpBackgroundColorEnd';	//Color de fondo de los botones como los de configuración
		$this->ArrayVariableValues['CLRTMPLButtonUpBackgroundColorEnd'] = '396b9e';	//Button Up Background Color End
		$this->ArrayVariables[7] = 'CLRTMPLButtonHooverBorderColor';	//Color del border de los botones como los de configuración (cuando se pasa el Mouse sobre el componente)
		$this->ArrayVariableValues['CLRTMPLButtonHooverBorderColor'] = '00516e';	//Button Hoover Border Color
		$this->ArrayVariables[8] = 'CLRTMPLButtonHooverBackgroundColorStart';	//Color de fondo de los botones como los de configuración
		$this->ArrayVariableValues['CLRTMPLButtonHooverBackgroundColorStart'] = '72b0d4';	//Button Hoover Background Color Start
		$this->ArrayVariables[9] = 'CLRTMPLButtonHooverBackgroundColorEnd';	//Color de fondo de los botones como los de configuración
		$this->ArrayVariableValues['CLRTMPLButtonHooverBackgroundColorEnd'] = '4b88b6';	//Button Hoover Background Color End
		$this->ArrayVariables[10] = 'CLRTMPLButtonDownBorderColor';	//Color del border de los botones como los de configuración (cuando se presiona)
		$this->ArrayVariableValues['CLRTMPLButtonDownBorderColor'] = '225377';	//Button Down Border Color
		$this->ArrayVariables[11] = 'CLRTMPLButtonDownBackgroundColorStart';	//Color de fondo de los botones como los de configuración
		$this->ArrayVariableValues['CLRTMPLButtonDownBackgroundColorStart'] = '396b9e';	//Button Down Background Color Start
		$this->ArrayVariables[12] = 'CLRTMPLButtonDownBackgroundColorEnd';	//Color de fondo de los botones como los de configuración
		$this->ArrayVariableValues['CLRTMPLButtonDownBackgroundColorEnd'] = '4e89c5';	//Button Down Background Color End
		$this->ArrayVariables[13] = 'CLRTMLPButtonLabelColor';	//Color de fondo del texto de las etiquetas en los botones como los de configuración
		$this->ArrayVariableValues['CLRTMLPButtonLabelColor'] = 'ffffff';	//Button Label Color
		$this->ArrayVariables[14] = 'CLRTMLPBarsBorderColor';	//Color del border de las barras de header y footer
		$this->ArrayVariableValues['CLRTMLPBarsBorderColor'] = '456f9a';	//Bars Border Color
		$this->ArrayVariables[15] = 'CLRTMLPBarsBackgroundColorStart';	//Color de fondo de las barras de header y footer
		$this->ArrayVariableValues['CLRTMLPBarsBackgroundColorStart'] = '2f5d95';	//Bars Background Color Start
		$this->ArrayVariables[16] = 'CLRTMLPBarsBackgroundColorEnd';	//Color de fondo de las barras de header y footer
		$this->ArrayVariableValues['CLRTMLPBarsBackgroundColorEnd'] = '193559';	//Bars Background Color End
		$this->ArrayVariables[17] = 'CLRTMLPMainButtonBackgroundColorStart';	//Color de fondo de los botones principales (como el de formas) para gradiente en 4 tonos
		$this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStart'] = '5E87B0';	//Main Button Background Color Start
		$this->ArrayVariables[18] = 'CLRTMPLMainButtonBackgroundColorEnd';	//Color de fondo de los botones principales (como el de formas) para gradiente en 4 tonos
		$this->ArrayVariableValues['CLRTMPLMainButtonBackgroundColorEnd'] = '5E87B0';	//Main Button Background Color End
		$this->ArrayVariables[19] = 'CLRTMLPMainButtonBackgroundColorStop1';	//Color de fondo de los botones principales (como el de formas) para gradiente en 4 tonos
		$this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStop1'] = '5E87B0';	//Main Button Background Color Stop1
		$this->ArrayVariables[20] = 'CLRTMLPMainButtonBackgroundColorStop2';	//Color de fondo de los botones principales (como el de formas) para gradiente en 4 tonos
		$this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStop2'] = '145072';	//Main Button Background Color Stop2
		$this->ArrayVariables[21] = 'CLRTMLPMainButtonBorderColor';	//Color de border de los botones principales (como el de formas)
		$this->ArrayVariableValues['CLRTMLPMainButtonBorderColor'] = '303030';	//Main Button Border Color
		$this->ArrayVariables[22] = 'CLRTMLPSectionBackgroundColorStart';	//Color de border de los botones principales (como el de formas)
		$this->ArrayVariableValues['CLRTMLPSectionBackgroundColorStart'] = 'eeeeee';	//Main Button Border Color
		$this->ArrayVariables[23] = 'CLRTMLPSectionBackgroundColorEnd';	//Color de border de los botones principales (como el de formas)
		$this->ArrayVariableValues['CLRTMLPSectionBackgroundColorEnd'] = 'dddddd';	//Main Button Border Color
		$this->isNew = true;
	}
	
	static function NewInstanceAll($aRepository, $aUserID = -1, $aTemplateID = -1)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		$strTemplateName = '';
		$sql = "SELECT TemplateName 
			FROM SI_SV_AppCustomizationTpls 
			WHERE TemplateID = {$aTemplateID}";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF) {
			$strTemplateName = (string) @$aRS->fields["templatename"];
		}
		
		$sql = "SELECT A.TemplateID, A.UserID, A.SettingID, A.SettingName, A.SettingValue, B.TemplateName 
			FROM SI_SV_AppCustomization A 
				LEFT OUTER JOIN SI_SV_AppCustomizationTpls B ON A.TemplateID = B.TemplateID 
			WHERE A.TemplateID = {$aTemplateID} AND A.UserID = {$aUserID} 
			ORDER BY SettingID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_AppCustomization ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$anInstanceTemp = $strCalledClass::NewInstance($aRepository);
		$anInstanceTemp->resetDefaultSettings();
		$ArrayVariables = &$anInstanceTemp->ArrayVariables;
		$ArrayVariableValues = &$anInstanceTemp->ArrayVariableValues;
		$ArrayVariablesExist = array();
		$blnIsNew = true;
		while(!$aRS->EOF)
		{
			$variableId = $aRS->fields["settingid"];
			$variableName = $aRS->fields["settingname"];
			$SettingValue = $aRS->fields["settingvalue"];
			$ArrayVariables[$variableId] = $variableName;
			$ArrayVariableValues[$variableName]=$SettingValue;
			$ArrayVariablesExist[$variableName]=true;
			$blnIsNew = false;
			$aRS->MoveNext();
		}
		
		$strtemp = 			'if (!class_exists("BITAMEFormsAppCustomizationExtended"))'."\n";
		$strtemp = $strtemp.'{'."\n";
		$strtemp = $strtemp.'	class BITAMEFormsAppCustomizationExtended extends '.$strCalledClass."\n";
		$strtemp = $strtemp.'	{'."\n";
		foreach($ArrayVariables as $variable)
		{   
			$variableValue=$ArrayVariableValues[$variable];
			$strtemp = $strtemp.'public $'.$variable.' = "'.$variableValue.'";'."\n";
			
		}
		$strtemp = $strtemp.'	}'."\n";
		$strtemp = $strtemp.'}'."\n";
		//@JAPR 2012-05-18: Validación para evitar que invocar múltiples veces al SaveData en la misma sesión provoque que esta clase dinámica
		//sea redeclarada, de todas formas, las configuraciones dificilmente cambiarán durante la ejecución de una llamada de grabado como para 
		//que esto pudiera ser un problema
		$strtemp=addcslashes( $strtemp, '\\');
		eval($strtemp);
		$strtemp = "";
		//@JAPR
		$strtemp = $strtemp.'$theobject = new BITAMEFormsAppCustomizationExtended($aRepository);'."\n";
		$strtemp=addcslashes( $strtemp, '\\');
		eval($strtemp);
		//$theobject->UserID = $aUserID;
		$theobject->TemplateID = $aTemplateID;
		$theobject->UserID = $aUserID;
		$theobject->TemplateName = $strTemplateName;
		$theobject->ArrayVariables = $ArrayVariables;
		$theobject->ArrayVariableValues = $ArrayVariableValues;
		$theobject->ArrayVariablesExist = $ArrayVariablesExist;
		$theobject->isNew = $blnIsNew;
		
		return($theobject);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{	$strCalledClass = static::class;
		$intTemplateID = getParamValue('TemplateID', 'both', '(int)');
		if ($intTemplateID <= 0) {
			$intTemplateID = -1;
		}
		
		$intUserID = getParamValue('UserID', 'both', '(int)');
		if ($intUserID <= 0) {
			$intUserID = -1;
		}
		
		if (array_key_exists("SettingID", $aHTTPRequest->POST))
		{
			/*Inicia: Grabar el nombre del template*/
			require_once("eformsappcustomizationtpl.inc.php");
			$anInstanceTPL = BITAMEFormsAppCustomizationTpl::NewInstanceWithID($aRepository, (int)$intTemplateID);
			if (is_null($anInstanceTPL))
			{
				$anInstanceTPL = BITAMEFormsAppCustomizationTpl::NewInstance($aRepository);
			}
			$anInstanceTPL->updateFromArray($aHTTPRequest->POST);
			$aResultTPL = $anInstanceTPL->save();
			/*Termina: Grabar el nombre del template*/
			
			$anInstance = $strCalledClass::NewInstanceAll($aRepository, $intUserID, $intTemplateID);
			$anInstance->updateFromArray($aHTTPRequest->POST);
			
			$aResult = $anInstance->save();
			
			if (array_key_exists("go", $_POST))
			{
				$go = $_POST["go"];
			}
			else
			{
				$go = "self";
			}
			
			if ($go == "parent")
			{
				$anInstance = $anInstance->get_Parent();
			}
			else
			{
				if ($go == "new")
				{
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}
			}
			
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		
		$anInstance = $strCalledClass::NewInstanceAll($aRepository, $intUserID, $intTemplateID);
		
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("TemplateID", $anArray)) {
			$this->TemplateID = $anArray["TemplateID"];
		}
		
		if (array_key_exists("UserID", $anArray))
		{
			$this->UserID = $anArray["UserID"];
		}
		
		if (array_key_exists("TemplateName", $anArray)) {
			$this->TemplateName = $anArray["TemplateName"];
		}
		
		foreach($this->ArrayVariables as $variable)
		{
			if (array_key_exists($variable, $anArray))
			{
				$this->$variable = $anArray[$variable];
			}
		}
		return $this;
	}

	function save()
	{
		foreach($this->ArrayVariables as $variableID => $variable)
		{
			$sql="";
			$variableValue=$this->$variable;
			
			if (isset($this->ArrayVariablesExist[$variable]))
			{
				$sql = "UPDATE SI_SV_AppCustomization SET SettingValue =".$this->Repository->DataADOConnection->Quote($variableValue).
					" WHERE TemplateID = {$this->TemplateID} AND UserID = {$this->UserID} AND SettingID = {$variableID}";
			}
			else 
			{
				$sql = "INSERT INTO SI_SV_AppCustomization (TemplateID, UserID, SettingID, SettingName, SettingValue) VALUES (".
					$this->TemplateID.",".$this->UserID.",".$variableID.", ".
					$this->Repository->DataADOConnection->Quote($variable).", ".
					$this->Repository->DataADOConnection->Quote($variableValue).")";
			}
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_AppCustomization ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$this->isNew = false;
		}
	}
	
	function isNewObject()
	{
		return $this->isNew;
	}

	function get_Title()
	{
		return translate("Color template definition");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=EFormsAppCustomization&TemplateID={$this->TemplateID}&UserID={$this->UserID}";
	}

	function get_Parent()
	{
		return $this;
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SettingID';
	}
	
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");

		$myFields = array();
		        
        //JCEM 03.11.2014 #MO80MU Se agrega esta funcionalidad para seccionar el formulario de 
        //configuracion de colores
        //--------------------------------
        //
        //Estructura para clasificar el valor DEFAULT es donde cae los que no tienen subSecciones
        
        $treeArr = array( 'Buttons' => array(
                                            'Text' => array(),
                                            'ButtonUp' => array(),
                                            'ButtonHover' => array(),
                                            'ButtonDown' => array(),
                                      ),
                          'ButtonBackToFormList' => array(
                                                        'DEFAULT' => array(),
                                                        'Border'=> array()
                                                    ),
                          'TopButtonBars' => array(
                                                 'DEFAULT' => array()
                                             ),
                          'SectionsAndQuestionsArea' => array(
                                                        'DEFAULT' => array(),
                                                        'Texts' => array()
                                                    ),
                          'Others' => array(
                                          'DEFAULT' => array()
                                      )
                        );

        
        
        //recorreo y clasifico      
        foreach($this->ArrayVariables as $variableID => $variable)
		{
            switch ($variable) {
                //seccion botones. subsec:textos-0-1
                case ('CLRTMLPButtonLabelColor'):
                
                    $treeArr['Buttons']['Text'][] = $variable;
                    break;
                
                //seccion botones, subsec:Botón deshabilitado, no utilizable-0-2
                case 'CLRTMPLButtonUpBorderColor':
                case 'CLRTMPLButtonUpBackgroundColorStart':
                case 'CLRTMPLButtonUpBackgroundColorEnd':
                    
                    $treeArr['Buttons']['ButtonUp'][] = $variable;
                    break;
                
                //sec:botones, subsec:Botón al pasar el puntero encima-0-3
                case 'CLRTMPLButtonHooverBorderColor':
                case 'CLRTMPLButtonHooverBackgroundColorStart':
                case 'CLRTMPLButtonHooverBackgroundColorEnd':
                
                    $treeArr['Buttons']['ButtonHover'][] = $variable;
                    break;
                
                //sec:botones, sub:Boton up estado normal-0-4
                case 'CLRTMPLButtonDownBorderColor':
                case 'CLRTMPLButtonDownBackgroundColorStart':
                case 'CLRTMPLButtonDownBackgroundColorEnd':
                
                    $treeArr['Buttons']['ButtonDown'][] = $variable;
                    break;
                
                //sec:Botón de regresar a la lista formas-1-0, sub:''
                case 'CLRTMLPMainButtonBackgroundColorStart':
                case 'CLRTMLPMainButtonBackgroundColorStop1':
                case 'CLRTMLPMainButtonBackgroundColorStop2':
                case 'CLRTMPLMainButtonBackgroundColorEnd':
                
                    $treeArr['ButtonBackToFormList']['DEFAULT'][] = $variable;
                    break;
                
                //sec:Botón de regresar a la lista formas, sub:Borde-1-1
                case ('CLRTMLPMainButtonBorderColor'):
                    
                    $treeArr['ButtonBackToFormList']['Border'][] = $variable;
                    break;
                
                //sec:Barra Superior e Inferior-2-0, sub:''
                case 'CLRTMLPBarsBorderColor':
                case 'CLRTMLPBarsBackgroundColorStart':
                case 'CLRTMLPBarsBackgroundColorEnd':
                    
                    $treeArr['TopButtonBars']['DEFAULT'][] = $variable;
                    break;
                    
                //sec:Secciones y área de preguntas3-0, sub:''
                case 'CLRTMLPSectionBackgroundColorStart':
                case 'CLRTMLPSectionBackgroundColorEnd':
                                
                    $treeArr['SectionsAndQuestionsArea']['DEFAULT'][] = $variable;
                    break;
                
                //sec:Textos-4-0, sub:''
                case 'CLRTMPLSectionTitleColor':
                case 'CLRTMPLOuterLabelColor':
                case 'CLRTMPLInnerLabelColor':
                    
                    $treeArr['SectionsAndQuestionsArea']['Texts'][] = $variable;
                    break;
                
                //no clasificados
                default:
                
                    $treeArr['Others']['DEFAULT'][] = $variable;
                    break;
            }
        }
        
        //si la seccion others no tiene elementos la quitamos
        if (count($treeArr['Others']['DEFAULT']) === 0){
            unset($treeArr['Others']);
        }
          
        $aField = BITAMFormField::NewFormField();
		$aField->Name = "TemplateID";
		$aField->Title = translate("Template");
		$aField->Type = "Object";
        $aField->TextAlign = 'left';
		$aField->VisualComponent = "Combobox";
		$aField->Options = array($this->TemplateID => 'Template');
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserID";
		$aField->Title = translate("User");
		$aField->Type = "Object"; 
        $aField->TextAlign = 'left';
		$aField->VisualComponent = "Combobox";
        $aField->Options = array($this->UserID => 'User');
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
        
        $aField->Type = "String";
        $tabsz= '4em;';
        
        //JCEM 03.11.2014 #MO80MU crear las entradas
        foreach($treeArr as $sections => $content) {
            $aField = BITAMFormField::NewFormField();
            $aField->Name = $sections;
            $aField->Type = "String";
            $aField->TextAlign = 'left';
            $aField->Title = translate($sections);
            $aField->Size = 255;
            $aField->IsDisplayOnly = true;
            //$aField->CustomizedClass = "color";
            $myFields[$aField->Name] = $aField;

            foreach($content as $subsection => $subcontent){
                if ($subsection !== 'DEFAULT'){
                    $aField = BITAMFormField::NewFormField();
                    $aField->Name = $subsection;
                    $aField->Type = "String";
                    //se agrega text-indent para indentar los textos, como la propiedad
                    //textAlign se concatena al ejecutar funciona correctamente
                    $aField->TextAlign = 'left; text-indent:' . $tabsz ;
                    $aField->Title = translate($subsection);
                    $aField->Size = 10;
                    $aField->IsDisplayOnly = true;
                    $myFields[$aField->Name] = $aField;
                    //si es opcion se tabula doble
                    $tabsz='8em;';
                }else{
                    //si es subseccion se tabula
                    $tabsz='4em;';
                }
                    
                foreach($subcontent as $options ){
                    $aField = BITAMFormField::NewFormField();
                    $aField->Name = $options;
                    $aField->Title = translate($options);
                    $aField->Type = "String";
                    $aField->TextAlign = 'left; text-indent:' . $tabsz ;
                    $aField->Size = 10;
                    $aField->CustomizedClass = "color";
                    $myFields[$aField->Name] = $aField;
                }
                
                $tabsz='4em;';
            }
        }
        
        //--------------------------
        
          return $myFields;
	}
    
   	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{
        //2014.11.12 JCEM #MO80MU agrega referencias necesarioas para la seccion de preview
?>
        <link type="text/css" rel="stylesheet" href="jquery.mobile-1.0/jquery.mobile-1.0.css">
		<link type="text/css" rel="stylesheet" href="jquery.mobile-forms.css">
        <style type="text/css" media="screen">
		   .ui-footer.ui-bar {
		       padding-left: 0px;
		       padding-right: 0px;
		   }
            
   
		    h3 {
				color: white;
			}
		</style>
		
		<script type="text/javascript" src="jquery.mobile-1.0/jquery.mobile-1.0.js"></script>
        <script type="text/javascript" src="js/jscolor/jscolor.js"></script>
        <script language="javascript">
            //funcion que aplica la vista previa al vuelo
            function BITAMEFormsAppCustomizationExtended_applyPreview(control_affected)
			{
				//alert("control_affected: " +control_affected);
                switch (control_affected) 
                {
                //seccion botones. color del texto de los botones
                case ('CLRTMLPButtonLabelColor'):
                    //para la clase .ui-btn-up-b
                    document.styleSheets['stylePreview'].cssRules[4].style.color = '#'+ document.getElementsByName('CLRTMLPButtonLabelColor')[0].value;
                    //para la clase .inbarlabel
                    document.styleSheets['stylePreview'].cssRules[2].style.color = '#'+ document.getElementsByName('CLRTMLPButtonLabelColor')[0].value;
                    break;
                
                //seccion botones, subsec:Botón deshabilitado, no utilizable-0-2
                case 'CLRTMPLButtonUpBorderColor':
                    //para la clase .ui-btn-up-b .style.border
                    document.styleSheets['stylePreview'].cssRules[4].style.border = '1px solid #' + document.getElementsByName('CLRTMPLButtonUpBorderColor')[0].value;
                    break;
                case 'CLRTMPLButtonUpBackgroundColorStart':
                case 'CLRTMPLButtonUpBackgroundColorEnd':
                     //para la clase .ui-btn-up-b .style.backgroundImage
                    document.styleSheets['stylePreview'].cssRules[4].style.backgroundImage = 'linear-gradient(#' + document.getElementsByName('CLRTMPLButtonUpBackgroundColorStart')[0].value + ', #' + document.getElementsByName('CLRTMPLButtonUpBackgroundColorEnd')[0].value + ')' ;
                    break;
                    
                //sec:botones, subsec:Botón al pasar el puntero encima-0-3
                case 'CLRTMPLButtonHooverBorderColor':
                     //para la clase .ui-btn-up-b:hover .style.border
                    document.styleSheets['stylePreview'].cssRules[5].style.border = '1px solid #' + document.getElementsByName('CLRTMPLButtonHooverBorderColor')[0].value;
                    break;
                case 'CLRTMPLButtonHooverBackgroundColorStart':
                case 'CLRTMPLButtonHooverBackgroundColorEnd':
                    //para la clase .ui-btn-up-b:hover .style.backgroundImage
                    document.styleSheets['stylePreview'].cssRules[5].style.backgroundImage = 'linear-gradient(#' + document.getElementsByName('CLRTMPLButtonHooverBackgroundColorStart')[0].value + ', #' + document.getElementsByName('CLRTMPLButtonHooverBackgroundColorEnd')[0].value + ')' ;
                    break;
                
                //sec:botones, sub:Boton up estado normal-0-4
                case 'CLRTMPLButtonDownBorderColor':
                    //para la clase .ui-btn-up-b:active .style.border
                    document.styleSheets['stylePreview'].cssRules[6].style.border = '1px solid #' + document.getElementsByName('CLRTMPLButtonDownBorderColor')[0].value;
                    break;
                case 'CLRTMPLButtonDownBackgroundColorStart':
                case 'CLRTMPLButtonDownBackgroundColorEnd':
                    //para la clase .ui-btn-up-b:active .style.backgroundImage
                    document.styleSheets['stylePreview'].cssRules[6].style.backgroundImage = 'linear-gradient(#' + document.getElementsByName('CLRTMPLButtonDownBackgroundColorStart')[0].value + ', #' + document.getElementsByName('CLRTMPLButtonDownBackgroundColorEnd')[0].value + ')' ;
                    break;
                
                //sec:Botón de regresar a la lista formas-1-0, sub:''
                case 'CLRTMLPMainButtonBackgroundColorStart':
                case 'CLRTMLPMainButtonBackgroundColorStop1':
                case 'CLRTMLPMainButtonBackgroundColorStop2':
                case 'CLRTMPLMainButtonBackgroundColorEnd':
                    //para la clase  uibtn .style.backgroundImage
                    document.styleSheets['stylePreview'].cssRules[7].style.backgroundImage =  '-webkit-gradient(linear, left top, left bottom, from(#' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStart')[0].value + '), to(#' + document.getElementsByName('CLRTMPLMainButtonBackgroundColorEnd')[0].value + '), color-stop(0.5, #' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStop1')[0].value  + ' ), color-stop(0.5, #' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStop2')[0].value + '))';
                    //para la clase  uibtn:hover .style.border --mientras no se cree una configuracion unica para esta opcion se invierten los valores
                    document.styleSheets['stylePreview'].cssRules[8].style.backgroundImage =  '-webkit-gradient(linear, left top, left bottom, from(#' + document.getElementsByName('CLRTMPLMainButtonBackgroundColorEnd')[0].value + '), to(#' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStart')[0].value + '), color-stop(0.5, #' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStop2')[0].value  + ' ), color-stop(0.5, #' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStop1')[0].value + '))';
                    break;
                
                //sec:Botón de regresar a la lista formas, sub:Borde-1-1
                case ('CLRTMLPMainButtonBorderColor'):
                    //para la clase  uibtn .style.border
                    document.styleSheets['stylePreview'].cssRules[7].style.border = '1px solid #' + document.getElementsByName('CLRTMLPMainButtonBorderColor')[0].value;
                    document.styleSheets['stylePreview'].cssRules[9].style.borderTopColor = '#' + document.getElementsByName('CLRTMLPMainButtonBorderColor')[0].value;
                    document.styleSheets['stylePreview'].cssRules[9].style.borderTopStyle = 'solid';
                    document.styleSheets['stylePreview'].cssRules[9].style.borderTopWidth = '1px';
                    document.styleSheets['stylePreview'].cssRules[9].style.borderRightColor = '#' + document.getElementsByName('CLRTMLPMainButtonBorderColor')[0].value;
                    document.styleSheets['stylePreview'].cssRules[9].style.borderRightStyle = 'solid';
                    document.styleSheets['stylePreview'].cssRules[9].style.borderRightWidth = '1px';
                    break;
                
                //sec:Barra Superior e Inferior-2-0, sub:''
                case 'CLRTMLPBarsBorderColor':
                    //para la clase ui-bar-f .style.border
                    document.styleSheets['stylePreview'].cssRules[10].style.border = '1px solid #' + document.getElementsByName('CLRTMLPBarsBorderColor')[0].value;
                    break;
                case 'CLRTMLPBarsBackgroundColorStart':
                case 'CLRTMLPBarsBackgroundColorEnd':
                    //para la clase ui-bar-f .style.backgroundImage
                    document.styleSheets['stylePreview'].cssRules[10].style.backgroundImage = "linear-gradient(#" + document.getElementsByName('CLRTMLPBarsBackgroundColorStart')[0].value + ", #" + document.getElementsByName('CLRTMLPBarsBackgroundColorEnd')[0].value + ")";
                    break;
			
                //sec:Secciones y área de preguntas3-0, sub:''
                case 'CLRTMLPSectionBackgroundColorStart':
                case 'CLRTMLPSectionBackgroundColorEnd':
                    //para la clase .ui-body-c, .ui-dialog.ui-overlay-c .style.backgroundImage
                    document.styleSheets['stylePreview'].cssRules[3].style.backgroundImage = "linear-gradient(#" + document.getElementsByName('CLRTMLPSectionBackgroundColorStart')[0].value + ", #" + document.getElementsByName('CLRTMLPSectionBackgroundColorEnd')[0].value + ")";
                    break;
                
                //sec:Textos-4-0, sub:''
                case 'CLRTMPLSectionTitleColor':
                    //par ala clase h3 style.color
                    document.styleSheets['stylePreview'].cssRules[0].style.color = '#'+ document.getElementsByName('CLRTMPLSectionTitleColor')[0].value;
                    break;
                case 'CLRTMPLOuterLabelColor':
                    // por el momento ambos controles toman el valor d ela misma propiedad
                     document.getElementById('label_out_section').style.color =  '#'+ document.getElementsByName('CLRTMPLOuterLabelColor')[0].value;
                    //document.styleSheets['stylePreview'].cssRules[1].style.color = '#'+ document.getElementsByName('CLRTMPLOuterLabelColor')[0].value;
                    break;
                case 'CLRTMPLInnerLabelColor':
                    //para la clase inSeclabel style.color
                    document.styleSheets['stylePreview'].cssRules[1].style.color = '#'+ document.getElementsByName('CLRTMPLInnerLabelColor')[0].value;
                    break;
                
                //no clasificados
                default:
                    break;
                }
    		}
            //funcion que resetea la vista previa cuando se cancela la opcion
            function BITAMEFormsAppCustomizationExtended_ResetPreview()
            {

                //seccion botones. color del texto de los botones
                //para la clase .ui-btn-up-b
                document.styleSheets['stylePreview'].cssRules[4].style.color = '#'+ document.getElementsByName('CLRTMLPButtonLabelColor')[0].value;
                //para la clase .inbarlabel
                document.styleSheets['stylePreview'].cssRules[2].style.color = '#'+ document.getElementsByName('CLRTMLPButtonLabelColor')[0].value;
                //para la clase .ui-btn-up-b .style.border
                document.styleSheets['stylePreview'].cssRules[4].style.border = '1px solid #' + document.getElementsByName('CLRTMPLButtonUpBorderColor')[0].value;
                 //para la clase .ui-btn-up-b .style.backgroundImage
                document.styleSheets['stylePreview'].cssRules[4].style.backgroundImage = 'linear-gradient(#' + document.getElementsByName('CLRTMPLButtonUpBackgroundColorStart')[0].value + ', #' + document.getElementsByName('CLRTMPLButtonUpBackgroundColorEnd')[0].value + ')' ;
                 //para la clase .ui-btn-up-b:hover .style.border
                document.styleSheets['stylePreview'].cssRules[5].style.border = '1px solid #' + document.getElementsByName('CLRTMPLButtonHooverBorderColor')[0].value;
                //para la clase .ui-btn-up-b:hover .style.backgroundImage
                document.styleSheets['stylePreview'].cssRules[5].style.backgroundImage = 'linear-gradient(#' + document.getElementsByName('CLRTMPLButtonHooverBackgroundColorStart')[0].value + ', #' + document.getElementsByName('CLRTMPLButtonHooverBackgroundColorEnd')[0].value + ')' ;
                //para la clase .ui-btn-up-b:active .style.border
                document.styleSheets['stylePreview'].cssRules[6].style.border = '1px solid #' + document.getElementsByName('CLRTMPLButtonDownBorderColor')[0].value;
                //para la clase .ui-btn-up-b:active .style.backgroundImage
                document.styleSheets['stylePreview'].cssRules[6].style.backgroundImage = 'linear-gradient(#' + document.getElementsByName('CLRTMPLButtonDownBackgroundColorStart')[0].value + ', #' + document.getElementsByName('CLRTMPLButtonDownBackgroundColorEnd')[0].value + ')' ;
                //para la clase  uibtn .style.backgroundImage
                document.styleSheets['stylePreview'].cssRules[7].style.backgroundImage =  '-webkit-gradient(linear, left top, left bottom, from(#' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStart')[0].value + '), to(#' + document.getElementsByName('CLRTMPLMainButtonBackgroundColorEnd')[0].value + '), color-stop(0.5, #' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStop1')[0].value  + ' ), color-stop(0.5, #' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStop2')[0].value + '))';
                //para la clase  uibtn:hover .style.border --mientras no se cree una configuracion unica para esta opcion se invierten los valores
                document.styleSheets['stylePreview'].cssRules[8].style.backgroundImage =  '-webkit-gradient(linear, left top, left bottom, from(#' + document.getElementsByName('CLRTMPLMainButtonBackgroundColorEnd')[0].value + '), to(#' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStart')[0].value + '), color-stop(0.5, #' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStop2')[0].value  + ' ), color-stop(0.5, #' + document.getElementsByName('CLRTMLPMainButtonBackgroundColorStop1')[0].value + '))';
                //para la clase  uibtn .style.border
                document.styleSheets['stylePreview'].cssRules[7].style.border = '1px solid #' + document.getElementsByName('CLRTMLPMainButtonBorderColor')[0].value;
                //para la clase  uibtn pivot .style.border
                document.styleSheets['stylePreview'].cssRules[9].style.borderTopColor = '#' + document.getElementsByName('CLRTMLPMainButtonBorderColor')[0].value;
                document.styleSheets['stylePreview'].cssRules[9].style.borderRightColor = '#' + document.getElementsByName('CLRTMLPMainButtonBorderColor')[0].value;
                //para la clase ui-bar-f .style.border
                document.styleSheets['stylePreview'].cssRules[10].style.border = '1px solid #' + document.getElementsByName('CLRTMLPBarsBorderColor')[0].value;
                //para la clase ui-bar-f .style.backgroundImage
                document.styleSheets['stylePreview'].cssRules[10].style.backgroundImage = "linear-gradient(#" + document.getElementsByName('CLRTMLPBarsBackgroundColorStart')[0].value + ", #" + document.getElementsByName('CLRTMLPBarsBackgroundColorEnd')[0].value + ")";
                //para la clase .ui-body-c, .ui-dialog.ui-overlay-c .style.backgroundImage
                document.styleSheets['stylePreview'].cssRules[3].style.backgroundImage = "linear-gradient(#" + document.getElementsByName('CLRTMLPSectionBackgroundColorStart')[0].value + ", #" + document.getElementsByName('CLRTMLPSectionBackgroundColorEnd')[0].value + ")";
                //par ala clase h3 style.color
                document.styleSheets['stylePreview'].cssRules[0].style.color = '#'+ document.getElementsByName('CLRTMPLSectionTitleColor')[0].value;
                // por el momento ambos controles toman el valor d ela misma propiedad
                //document.getElementById('label_out_section').style.color =  '#'+ document.getElementsByName('CLRTMPLOuterLabelColor')[0].value;
                //document.styleSheets['stylePreview'].cssRules[1].style.color = '#'+ document.getElementsByName('CLRTMPLOuterLabelColor')[0].value;
                //para la clase inSeclabel style.color
                document.styleSheets['stylePreview'].cssRules[1].style.color = '#'+ document.getElementsByName('CLRTMPLInnerLabelColor')[0].value;

            }
        </script>
        <style id="stylePreview" type="text/css">
            h3 
                {	color: #<?= $this->CLRTMPLSectionTitleColor ?>;}


            label.inSecLabel 
                {	color: #<?= $this->CLRTMPLInnerLabelColor ?>;}

            label.inBarLabel 
                {	color: #<?= $this->CLRTMLPButtonLabelColor ?>;}    

            .ui-body-c, .ui-dialog.ui-overlay-c 
                {	background-image: -webkit-gradient(linear, left top, left bottom, from(#<?= $this->CLRTMLPSectionBackgroundColorStart ?>), to( #<?= $this->CLRTMLPSectionBackgroundColorEnd ?>));
                    background-image: -webkit-linear-gradient(#<?= $this->CLRTMLPSectionBackgroundColorStart ?>, #<?= $this->CLRTMLPSectionBackgroundColorEnd ?>);
                    background-image:    -moz-linear-gradient(#<?= $this->CLRTMLPSectionBackgroundColorStart ?>, #<?= $this->CLRTMLPSectionBackgroundColorEnd ?>);
                    background-image:     -ms-linear-gradient(#<?= $this->CLRTMLPSectionBackgroundColorStart ?>, #<?= $this->CLRTMLPSectionBackgroundColorEnd ?>);
                    background-image:      -o-linear-gradient(#<?= $this->CLRTMLPSectionBackgroundColorStart ?>, #<?= $this->CLRTMLPSectionBackgroundColorEnd ?>);
                    background-image:         linear-gradient(#<?= $this->CLRTMLPSectionBackgroundColorStart ?>, #<?= $this->CLRTMLPSectionBackgroundColorEnd ?>);
                }

            .ui-btn-up-b
                {	border: 1px solid <?= $this->CLRTMPLButtonUpBorderColor ?>;
                    color: #<?= $this->CLRTMLPButtonLabelColor ?>;
                    background-image: -webkit-gradient(linear, left top, left bottom, from(#<?= $this->CLRTMPLButtonUpBackgroundColorStart ?>), to( #<?= $this->CLRTMPLButtonUpBackgroundColorEnd ?>));
                    background-image: -webkit-linear-gradient(#<?= $this->CLRTMPLButtonUpBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonUpBackgroundColorEnd ?>);
                    background-image:    -moz-linear-gradient(#<?= $this->CLRTMPLButtonUpBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonUpBackgroundColorEnd ?>);
                    background-image:     -ms-linear-gradient(#<?= $this->CLRTMPLButtonUpBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonUpBackgroundColorEnd ?>);
                    background-image:      -o-linear-gradient(#<?= $this->CLRTMPLButtonUpBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonUpBackgroundColorEnd ?>);
                    background-image:         linear-gradient(#<?= $this->CLRTMPLButtonUpBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonUpBackgroundColorEnd ?>);
                }

            .ui-btn-up-b:hover
                {	border: 1px solid #<?= $this->CLRTMPLButtonHooverBorderColor ?>;
                    color: #<?= $this->CLRTMLPButtonLabelColor ?>;
                    background-image: -webkit-gradient(linear, left top, left bottom, from(#<?= $this->CLRTMPLButtonHooverBackgroundColorStart ?>), to( #<?= $this->CLRTMPLButtonHooverBackgroundColorEnd ?>));
                    background-image: -webkit-linear-gradient(#<?= $this->CLRTMPLButtonHooverBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonHooverBackgroundColorEnd ?>);
                    background-image:    -moz-linear-gradient(#<?= $this->CLRTMPLButtonHooverBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonHooverBackgroundColorEnd ?>);
                    background-image:     -ms-linear-gradient(#<?= $this->CLRTMPLButtonHooverBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonHooverBackgroundColorEnd ?>);
                    background-image:      -o-linear-gradient(#<?= $this->CLRTMPLButtonHooverBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonHooverBackgroundColorEnd ?>);
                    background-image:         linear-gradient(#<?= $this->CLRTMPLButtonHooverBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonHooverBackgroundColorEnd ?>);
                }

            .ui-btn-up-b:active
                {	border: 1px solid #<?= $this->CLRTMPLButtonDownBorderColor ?>;
                    color: #<?= $this->CLRTMLPButtonLabelColor ?>;
                    background-image: -webkit-gradient(linear, left top, left bottom, from(#<?= $this->CLRTMPLButtonDownBackgroundColorStart ?>), to( #<?= $this->CLRTMPLButtonDownBackgroundColorEnd ?>));
                    background-image: -webkit-linear-gradient(#<?= $this->CLRTMPLButtonDownBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonDownBackgroundColorEnd ?>);
                    background-image:    -moz-linear-gradient(#<?= $this->CLRTMPLButtonDownBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonDownBackgroundColorEnd ?>);
                    background-image:     -ms-linear-gradient(#<?= $this->CLRTMPLButtonDownBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonDownBackgroundColorEnd ?>);
                    background-image:      -o-linear-gradient(#<?= $this->CLRTMPLButtonDownBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonDownBackgroundColorEnd ?>);
                    background-image:         linear-gradient(#<?= $this->CLRTMPLButtonDownBackgroundColorStart ?>, #<?= $this->CLRTMPLButtonDownBackgroundColorEnd ?>);
                }

            .UIButton 
                {	border: 1px solid #<?= $this->CLRTMLPMainButtonBorderColor ?>;
                    background: -webkit-gradient(linear, left top, left bottom, from(#<?= $this->CLRTMLPMainButtonBackgroundColorStart ?>), to(#<?= $this->CLRTMPLMainButtonBackgroundColorEnd ?>), color-stop(0.5, #<?= $this->CLRTMLPMainButtonBackgroundColorStop1 ?>), color-stop(0.5, #<?= $this->CLRTMLPMainButtonBackgroundColorStop2 ?>));
                }
            .UIButton:hover 
                {	border: 1px solid #<?= $this->CLRTMLPMainButtonBorderColor ?>;
                    background: -webkit-gradient(linear, left top, left bottom, from(#<?= $this->CLRTMPLMainButtonBackgroundColorEnd ?>), to(#<?= $this->CLRTMLPMainButtonBackgroundColorStart ?>), color-stop(0.5, #<?= $this->CLRTMLPMainButtonBackgroundColorStop2 ?>), color-stop(0.5, #<?= $this->CLRTMLPMainButtonBackgroundColorStop1 ?>));
                }
            .UIButton.left .pivot 
                {	
                    border-top-color: #<?= $this->CLRTMLPMainButtonBorderColor ?>;
                    border-top-style: solid;
                    border-top-width: 1px;
                    border-right-color: #<?= $this->CLRTMLPMainButtonBorderColor ?>;
                    border-right-style: solid;
                    border-right-width: 1px;
                    background: -webkit-gradient(linear, left top, left bottom, from(#<?= $this->CLRTMLPMainButtonBackgroundColorStart ?>), to(#<?= $this->CLRTMPLMainButtonBackgroundColorEnd ?>), color-stop(0.5, #<?= $this->CLRTMLPMainButtonBackgroundColorStop2 ?>), color-stop(0.5, #<?= $this->CLRTMLPMainButtonBackgroundColorStop1 ?>));
                }

            .ui-bar-f 
                {	border: 1px solid #<?= $this->CLRTMLPBarsBorderColor ?>;
                    background-image: -webkit-gradient(linear, left top, left bottom, from(#<?= $this->CLRTMLPBarsBackgroundColorStart ?>), to( #<?= $this->CLRTMLPBarsBackgroundColorEnd ?>));
                    background-image: -webkit-linear-gradient(#<?= $this->CLRTMLPBarsBackgroundColorStart ?>, #<?= $this->CLRTMLPBarsBackgroundColorEnd ?>);
                    background-image:    -moz-linear-gradient(#<?= $this->CLRTMLPBarsBackgroundColorStart ?>, #<?= $this->CLRTMLPBarsBackgroundColorEnd ?>);
                    background-image:     -ms-linear-gradient(#<?= $this->CLRTMLPBarsBackgroundColorStart ?>, #<?= $this->CLRTMLPBarsBackgroundColorEnd ?>);
                    background-image:      -o-linear-gradient(#<?= $this->CLRTMLPBarsBackgroundColorStart ?>, #<?= $this->CLRTMLPBarsBackgroundColorEnd ?>);
                    background-image:         linear-gradient(#<?= $this->CLRTMLPBarsBackgroundColorStart ?>, #<?= $this->CLRTMLPBarsBackgroundColorEnd ?>);
                }
            .ui-input-text
                {	color: #<?= $this->CLRTMPLOuterLabelColor ?>;}
        </style>
<?
 	}
 	
    function generateAfterFormCode($aUser) {
        //JCEM 2014.11.11  #MO80MU 
        //se crea la seccion preview para mostrar los cambios en los controles
?>
        <script language="javascript">
            var cellStyle = document.getElementsByTagName('td');

            for (i=0; i < cellStyle.length; ++i) {
                if (cellStyle[i].Id = 'tdPrincipal'){
                    cellStyle[i].style.whiteSpace = 'nowrap';
                }
            }
            
            var inputs = document.getElementsByTagName('input');
     
            for (i=0; i < inputs.length; ++i) {
                if (inputs[i].type == 'text'){
                    inputs[i].onchange = function onchange(event) {javascript: BITAMEFormsAppCustomizationExtended_applyPreview(this.name);} 
                }
            }
            
        </script>
        
            <div id="PreviewHTMLSection" style="width: 100%;height: 335px">
                <br>
                <div class="object_title"><?= translate("Preview") ?></div>
                <hr class="object_title_hr">
                             
                <div id="section_header" class="ui-header ui-bar-f" style="position: relative;  height: 45px;">
                    <a id="btn_mnuformas" class="UIButton ui-btn-up-b left ui-btn-left" style="position:absolute; top:8px;cursor:pointer">
                        <span class="pivot"></span>
                        Botón regresar menú formas
                    </a>
                     <div style="position:absolute; top:15px; left:45%">
                                <label id="top_bar_label" class="inBarLabel"> Barra Superior </label>
                    </div>
                    <a id="btn_nav_next" style="position:absolute;top:8px;right:10px;" class="ui-btn-right ui-btn ui-btn-up-b ui-btn-icon-left ui-btn-corner-all ui-shadow">
                        <span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible;">
                            <span class="ui-btn-text">Botón navegador</span>
                            <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span>
                        </span>
                    </a>
                </div>
                <div id="section_body" class="ui-content ui-body-c" style=" ">
                    <h3 id="section_body_title" style="font-weight:bold;font-size:18;">Ejemplo de título en sección</h3>
                    <hr>
                </div>
                <div id="qfield2_area" data-role="fieldcontain" style="width:100%; position: relative;margin: 0;" class="ui-content ui-field-contain ui-body-c ui-br">
                        <label id="section_body_label" class="inSecLabel" style="font-size: 14px;text-indent: 1em;">
                            Ejemplo de etiqueta dentro de sección</label>
                        <br>
                        <br>

                        <a id="btn_nav_next" style="position: relative;width: 200px;" class="ui-btn-right ui-btn ui-btn-up-b ui-btn-icon-left ui-btn-corner-all ui-shadow">
                            <span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible;">
                                <span class="ui-btn-text">Botón en sección</span>
                            </span>
                        </a>
                        <br>
                </div>
                <div id="no_section" data-role="fieldcontain" style="margin: 0; " class="ui-field-contain ui-body ui-br">
                    <div><label id="label_out_section" for="" style="position: relative;width:100%;font-size:14;" class="ui-input-text">Ejemplo de etiqueta fuera de sección</label></div>
                </div>
                <div id="section_footer" style="width: 100%;height:45px;position: relative;" class="ui-footer ui-bar-f" role="contentinfo">
                    <div style="position:absolute; top:15px; left:46%">
                       <label id="bottom_bar_label" class="inBarLabel"> Barra Inferior </label>
                    </div>

                    <a id="btn_nav_next" style="position:absolute;top:6px;right:10px;" class="ui-btn-right ui-btn ui-btn-up-b ui-btn-icon-left ui-btn-corner-all ui-shadow">
                        <span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible;">
                            <span class="ui-btn-text">Botón navegador</span>
                            <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span>
                        </span>
                    </a>
                </div>
            </div>
<?
        
    }
    
    //2014.11.13 JCEM funcion para insertar el comando para ejecutar la funcion que resetea el preview
    function generateOnCancelFormCode($aUser)
    {
?>
                BITAMEFormsAppCustomizationExtended_ResetPreview();
<?        
    }
	
	function getJSonDefinitionDesign()
	{
		$arrDef = array();
		$arrDef["id"]=$this->TemplateID;
		$arrDef["TemplateID"]=$this->TemplateID;
		$arrDef["UserID"]=$this->UserID;
		$arrDef["TemplateName"]=$this->TemplateName;
		foreach($this->ArrayVariables as $variable)
		{
			$arrDef[$variable]=$this->$variable;
		}
		return $arrDef;
	}
    
 	//Basado en las configuraciones de la instancia actual (que debe ser una instancia Extendida obtenida mediante la función NewInstanceAll)
 	//genera el JSon de estilo que debe ser enviado al App, el cual se procesará para sobreescribir o generar dinámicamente las clases que se 
 	//requieren en el App para modificar la apariencia
	function getJSonDefinition($desig=false) {
		
		if($desig==true)
		{	
			$strCalledClass = static::class;
			$arrDef = $strCalledClass::getJSonDefinitionDesign();
			return $arrDef;
		}
		
		global $blnWebMode;
		if (!isset($blnWebMode)) {
			$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
		}
		
		$arrDef = array();
		//Estilo de los títulos de las secciones
		if (isset($this->ArrayVariableValues['CLRTMPLSectionTitleColor'])) {
			$arrStyle = array();
			$arrStyle['name'] = 'h3';
			$arrStyle['style'] = "color: #{$this->ArrayVariableValues['CLRTMPLSectionTitleColor']};";
			$arrDef[] = $arrStyle;
		}
		
		//Estilo del fondo de las secciones
		if (isset($this->ArrayVariableValues['CLRTMLPSectionBackgroundColorStart']) ||
				isset($this->ArrayVariableValues['CLRTMLPSectionBackgroundColorEnd'])) {
			$arrStyle = array();
			$arrStyle['name'] = '.ui-body-c, .ui-dialog.ui-overlay-c';
			$strGradStart = (string) @$this->ArrayVariableValues['CLRTMLPSectionBackgroundColorStart'];
			$strGradEnd = (string) @$this->ArrayVariableValues['CLRTMLPSectionBackgroundColorEnd'];
			$strGradStart = ($strGradStart)?$strGradStart:$strGradEnd;
			$strGradEnd = ($strGradEnd)?$strGradEnd:$strGradStart;
			
			$strBackground = ($strGradStart || $strGradEnd)?"".
				"background-image: -webkit-gradient(linear, left top, left bottom, from(#{$strGradStart}), to( #{$strGradEnd}));".
				"background-image: -webkit-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:    -moz-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:     -ms-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:      -o-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:         linear-gradient(#{$strGradStart}, #{$strGradEnd});":"";
			$arrStyle['style'] = $strBackground;
			$arrDef[] = $arrStyle;
		}
		
		//Estilo de las etiquetas externas a los componentes (como las etiquetas de las preguntas)
		if (isset($this->ArrayVariableValues['CLRTMPLOuterLabelColor'])) {
			$arrStyle = array();
			$arrStyle['name'] = '.outLabel';
			$arrStyle['style'] = "color: #{$this->ArrayVariableValues['CLRTMPLOuterLabelColor']};";
			$arrDef[] = $arrStyle;
		}
		
		//Estilo de las etiquetas internas a los componentes (como las etiquetas de las opciones de respuesta de simple choice en radio buttons)
		if (isset($this->ArrayVariableValues['CLRTMPLInnerLabelColor'])) {
			$arrStyle = array();
			$arrStyle['name'] = '.inLabel';
			$arrStyle['style'] = "color: #{$this->ArrayVariableValues['CLRTMPLInnerLabelColor']};";
			$arrDef[] = $arrStyle;
		}
		
		//Temporalmente en lo que se modifican los js del App, todos los labels se cambiarán con el mismo color independientemente de si están o no
		//dentro de algún componente
		if (isset($this->ArrayVariableValues['CLRTMPLOuterLabelColor']) ||
				isset($this->ArrayVariableValues['CLRTMPLInnerLabelColor'])) {
			$arrStyle = array();
			$arrStyle['name'] = '.ui-body-c label';
			$strLabelColor = (string) @$this->ArrayVariableValues['CLRTMPLInnerLabelColor'];
			$strLabelColor = ($strLabelColor)?$strLabelColor:(string) @$this->ArrayVariableValues['CLRTMPLOuterLabelColor'];
			$strLabelColor = ($strLabelColor)?"color: #{$strLabelColor};":"";
			$arrStyle['style'] = $strLabelColor;
			$arrDef[] = $arrStyle;
		}
		
		//Estilo de los botones de JQuery Mobile cuando están sin presionar
		if (isset($this->ArrayVariableValues['CLRTMPLButtonUpBorderColor']) ||
				isset($this->ArrayVariableValues['CLRTMPLButtonUpBackgroundColorStart']) ||
				isset($this->ArrayVariableValues['CLRTMPLButtonUpBackgroundColorEnd']) ||
				isset($this->ArrayVariableValues['CLRTMLPButtonLabelColor'])) {
			$arrStyle = array();
			$arrStyle['name'] = '.ui-btn-up-b, .ui-btn-up-f';
			$strLabelColor = (string) @$this->ArrayVariableValues['CLRTMLPButtonLabelColor'];
			$strLabelColor = ($strLabelColor)?"color: #{$strLabelColor};":"";
			$strBorderColor = (string) @$this->ArrayVariableValues['CLRTMLPBarsBorderColor'];
			$strBorderColor = ($strBorderColor)?"border: 1px solid {$strBorderColor};":"";
			$strGradStart = (string) @$this->ArrayVariableValues['CLRTMPLButtonUpBackgroundColorStart'];
			$strGradEnd = (string) @$this->ArrayVariableValues['CLRTMPLButtonUpBackgroundColorEnd'];
			$strGradStart = ($strGradStart)?$strGradStart:$strGradEnd;
			$strGradEnd = ($strGradEnd)?$strGradEnd:$strGradStart;
			
			$strBackground = ($strGradStart || $strGradEnd)?"".
				"background-image: -webkit-gradient(linear, left top, left bottom, from(#{$strGradStart}), to( #{$strGradEnd}));".
				"background-image: -webkit-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:    -moz-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:     -ms-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:      -o-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:         linear-gradient(#{$strGradStart}, #{$strGradEnd});":"";
			$arrStyle['style'] = $strBorderColor.$strLabelColor.$strBackground;
			$arrDef[] = $arrStyle;
		}
		
		//Estilo de los botones de JQuery Mobile cuando están presionados
		if (isset($this->ArrayVariableValues['CLRTMPLButtonDownBorderColor']) ||
				isset($this->ArrayVariableValues['CLRTMPLButtonDownBackgroundColorStart']) ||
				isset($this->ArrayVariableValues['CLRTMPLButtonDownBackgroundColorEnd']) ||
				isset($this->ArrayVariableValues['CLRTMLPButtonLabelColor'])) {
			$arrStyle = array();
			//@JAPR 2016-02-17: Agregada la clase .ui-btn-hover-c debido a la separación de temas de conchita
			$arrStyle['name'] = '.ui-btn-hover-b, .ui-btn-hover-c, .ui-btn-hover-f';
			$strLabelColor = (string) @$this->ArrayVariableValues['CLRTMLPButtonLabelColor'];
			$strLabelColor = ($strLabelColor)?"color: #{$strLabelColor};":"";
			//@JAPR 2016-02-17: Agregadas nuevas configuraciones para color de fondo de opciones de respuesta y mostrar o no el borde
			$strBorderColor = "";
			if ((isset($this->ArrayVariableValues['CLRTMLPOptionNoBorder']) && $this->ArrayVariableValues['CLRTMLPOptionNoBorder'] == '1')) {
				$strBorderColor = "border: 0px solid transparent;";
			}
			else {
				$strBorderColor = (string) @$this->ArrayVariableValues['CLRTMPLButtonHooverBorderColor'];
				$strBorderColor = ($strBorderColor)?"border: 1px solid {$strBorderColor};":"";
			}
			$strGradStart = (string) @$this->ArrayVariableValues['CLRTMPLButtonHooverBackgroundColorStart'];
			$strGradEnd = (string) @$this->ArrayVariableValues['CLRTMPLButtonHooverBackgroundColorEnd'];
			$strGradStart = ($strGradStart)?$strGradStart:$strGradEnd;
			$strGradEnd = ($strGradEnd)?$strGradEnd:$strGradStart;
			
			$strBackground = ($strGradStart || $strGradEnd)?"".
				"background-image: -webkit-gradient(linear, left top, left bottom, from(#{$strGradStart}), to( #{$strGradEnd}));".
				"background-image: -webkit-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:    -moz-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:     -ms-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:      -o-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:         linear-gradient(#{$strGradStart}, #{$strGradEnd});":"";
			$arrStyle['style'] = $strBorderColor.$strLabelColor.$strBackground;
			$arrDef[] = $arrStyle;
		}
		
		//Estilo de los botones de JQuery Mobile cuando pasan el mouse sobre ellos
		if (isset($this->ArrayVariableValues['CLRTMPLButtonHooverBorderColor']) ||
				isset($this->ArrayVariableValues['CLRTMPLButtonHooverBackgroundColorStart']) ||
				isset($this->ArrayVariableValues['CLRTMPLButtonHooverBackgroundColorEnd']) ||
				isset($this->ArrayVariableValues['CLRTMLPButtonLabelColor'])) {
			$arrStyle = array();
			$arrStyle['name'] = '.ui-btn-down-b, .ui-btn-down-f';
			$strLabelColor = (string) @$this->ArrayVariableValues['CLRTMLPButtonLabelColor'];
			$strLabelColor = ($strLabelColor)?"color: #{$strLabelColor};":"";
			$strBorderColor = (string) @$this->ArrayVariableValues['CLRTMPLButtonHooverBorderColor'];
			$strBorderColor = ($strBorderColor)?"border: 1px solid {$strBorderColor};":"";
			$strGradStart = (string) @$this->ArrayVariableValues['CLRTMPLButtonHooverBackgroundColorStart'];
			$strGradEnd = (string) @$this->ArrayVariableValues['CLRTMPLButtonHooverBackgroundColorEnd'];
			$strGradStart = ($strGradStart)?$strGradStart:$strGradEnd;
			$strGradEnd = ($strGradEnd)?$strGradEnd:$strGradStart;
			
			$strBackground = ($strGradStart || $strGradEnd)?"".
				"background-image: -webkit-gradient(linear, left top, left bottom, from(#{$strGradStart}), to( #{$strGradEnd}));".
				"background-image: -webkit-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:    -moz-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:     -ms-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:      -o-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:         linear-gradient(#{$strGradStart}, #{$strGradEnd});":"";
			$arrStyle['style'] = $strBorderColor.$strLabelColor.$strBackground;
			$arrDef[] = $arrStyle;
		}
		
		//Estilo de los botones de JQuery Mobile para "Main" (son cuadrados a diferencia de los anteriores)
		if (isset($this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStart']) ||
				isset($this->ArrayVariableValues['CLRTMPLMainButtonBackgroundColorEnd']) ||
				isset($this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStop1']) ||
				isset($this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStop2']) ||
				isset($this->ArrayVariableValues['CLRTMLPMainButtonBorderColor'])) {
			$arrStyle = array();
            
            $arrStyle['name'] = '.UIButton';
			$strBorderColor = (string) @$this->ArrayVariableValues['CLRTMLPMainButtonBorderColor'];
			$strBorderColor = ($strBorderColor)?"border: 1px solid #{$strBorderColor};":"";
			$strGradStart = (string) @$this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStart'];
			$strGradEnd = (string) @$this->ArrayVariableValues['CLRTMPLMainButtonBackgroundColorEnd'];
			$strStop1 = (string) @$this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStop1'];
			$strStop2 = (string) @$this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStop2'];
			$strStop1 = ($strStop1)?$strStop1:$strStop2;
			$strStop2 = ($strStop2)?$strStop2:$strStop1;
			
			$strGradStart = ($strGradStart)?$strGradStart:$strGradEnd;
			$strGradEnd = ($strGradEnd)?$strGradEnd:$strGradStart;
			
			if ($strStop1 || $strStop2) {
				$strStop1 = ", color-stop(0.5, #{$strStop1})";
				$strStop2 = ", color-stop(0.5, #{$strStop2})";
			}
			
			$strBackground = ($strGradStart || $strGradEnd)?"".
				"background: -webkit-gradient(linear, left top, left bottom, from(#{$strGradStart}), to(#{$strGradEnd})".$strStop1.$strStop2.");":"";
			$arrStyle['style'] = $strBorderColor.$strBackground;
			$arrDef[] = $arrStyle;
            
            //JCEM 30.10.2014 creando el hover botones principales
            //falta crear una configuracion especial para esta propiedad y reemplazar colores
            $arrStyle = array();
			$arrStyle['name'] = '.UIButton:hover';
			$strBorderColor = (string) @$this->ArrayVariableValues['CLRTMLPMainButtonBorderColor'];
			$strBorderColor = ($strBorderColor)?"border: 1px solid #{$strBorderColor};":"";
			
            $strGradStart = (string) @$this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStart'];
			$strGradEnd = (string) @$this->ArrayVariableValues['CLRTMPLMainButtonBackgroundColorEnd'];
			$strStop1 = (string) @$this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStop1'];
			$strStop2 = (string) @$this->ArrayVariableValues['CLRTMLPMainButtonBackgroundColorStop2'];
				
            $strGradStart = ($strGradStart)?$strGradStart:$strGradEnd;
			$strGradEnd = ($strGradEnd)?$strGradEnd:$strGradStart;
			
            if ($strStop1 || $strStop2) {
				$strStop1 = ", color-stop(0.5, #{$strStop1})";
				$strStop2 = ", color-stop(0.5, #{$strStop2})";
                
            $strBackground = ($strGradStart || $strGradEnd)?"".
                "background: -webkit-gradient(linear, left top, left bottom, from(#{$strGradEnd}), to(#{$strGradStart})".$strStop2.$strStop1.");":"";
                
            $arrStyle['style'] = $strBorderColor.$strBackground;
			$arrDef[] = $arrStyle;    
                
			}
            //----
            
			
			//Hay un segundo y tercer estilo asociado con los mismos parámetros (realmente, el tercer estilo era un poco diferente en la versión
			//original, pero para el rediseño de Heineken y Barcel se dejaron igual)
            //JCEM 30.10.2014 faltaba un ; para el border-top lineas 670 y 668
			$arrStyle = array();
			$arrStyle['name'] = '.UIButton.left .pivot';
			$strBorderColor = (string) @$this->ArrayVariableValues['CLRTMLPMainButtonBorderColor'];
			$strBorderColor = ($strBorderColor)?"border-right: 1px solid #{$strBorderColor};border-top: 1px solid #{$strBorderColor};":"";
			$arrStyle['style'] = $strBorderColor.$strBackground;
			$arrDef[] = $arrStyle;
			
			$arrStyle = array();
			$arrStyle['name'] = '.UIButton.right .pivot';
			$strBorderColor = (string) @$this->ArrayVariableValues['CLRTMLPMainButtonBorderColor'];
			$strBorderColor = ($strBorderColor)?"border-right: 1px solid #{$strBorderColor};border-top: 1px solid #{$strBorderColor};":"";
			$arrStyle['style'] = $strBorderColor.$strBackground;
			$arrDef[] = $arrStyle;
		}
		
		//Estilo de las barras de Header y Footer de JQuery Mobile
		if (isset($this->ArrayVariableValues['CLRTMLPBarsBorderColor']) ||
				isset($this->ArrayVariableValues['CLRTMLPBarsBackgroundColorStart']) ||
				isset($this->ArrayVariableValues['CLRTMLPBarsBackgroundColorEnd'])) {
			$arrStyle = array();
			$arrStyle['name'] = '.ui-bar-f';
			$strBorderColor = (string) @$this->ArrayVariableValues['CLRTMLPBarsBorderColor'];
			//@JAPR 2016-02-16: Corregido un bug, faltaba aplicar el # al color
			$strBorderColor = ($strBorderColor)?"border: 1px solid #{$strBorderColor} !important;":"";
			$strGradStart = (string) @$this->ArrayVariableValues['CLRTMLPBarsBackgroundColorStart'];
			$strGradEnd = (string) @$this->ArrayVariableValues['CLRTMLPBarsBackgroundColorEnd'];
			$strGradStart = ($strGradStart)?$strGradStart:$strGradEnd;
			$strGradEnd = ($strGradEnd)?$strGradEnd:$strGradStart;
			
			$strBackground = ($strGradStart || $strGradEnd)?"".
				"background-image: -webkit-gradient(linear, left top, left bottom, from(#{$strGradStart}), to( #{$strGradEnd}));".
				"background-image: -webkit-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:    -moz-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:     -ms-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:      -o-linear-gradient(#{$strGradStart}, #{$strGradEnd});".
				"background-image:         linear-gradient(#{$strGradStart}, #{$strGradEnd});":"";
			$arrStyle['style'] = $strBorderColor.$strBackground;
			$arrDef[] = $arrStyle;
			//@JAPR 2016-02-16: Agregado el theme "g" dado a los cambios de Conchita en v6 para separar clases
			$arrStyle['name'] = '.ui-bar-g';
			$arrDef[] = $arrStyle;
		}
		
		//@JAPR 2016-02-17: Agregadas nuevas configuraciones para color de fondo de opciones de respuesta y mostrar o no el borde
		//Personalización para credere-integral (requerían que el color de fondo de las opciones de respuesta fuera igual que el color de fondo de la sección, así que se
		//agregaron configuraciones para personalizar color de fondo de las opciones, adicionalmente requerían que no hubiera padding por tanto también se agregó una configuración
		//para tal fin)
		//Color de fondo y padding de opciones de respuesta
		if ((isset($this->ArrayVariableValues['CLRTMLPOptionBackgroundColor']) && $this->ArrayVariableValues['CLRTMLPOptionBackgroundColor'] != '') ||
				(isset($this->ArrayVariableValues['CLRTMLPOptionPadding']) && $this->ArrayVariableValues['CLRTMLPOptionPadding'] != '')) {
			$arrStyle = array();
			$arrStyle['name'] = '.ui-radio .ui-btn .ui-btn-inner';
			$strBackColor = '';
			if ($this->ArrayVariableValues['CLRTMLPOptionBackgroundColor'] != '') {
				$strBackColor = "background-color: #".(string) @$this->ArrayVariableValues['CLRTMLPOptionBackgroundColor'].";";
			}
			
			//@JAPR 2016-02-16: Corregido un bug, faltaba aplicar el # al color
			$strPadding = '';
			if ($this->ArrayVariableValues['CLRTMLPOptionPadding'] != '') {
				$strPadding = "padding: ".$this->ArrayVariableValues['CLRTMLPOptionPadding']."px;";
			}
			$arrStyle['style'] = $strBackColor.$strPadding;
			$arrDef[] = $arrStyle;
		}
		
		//Borde de opciones de respuesta
		if ((isset($this->ArrayVariableValues['CLRTMLPOptionNoBorder']) && $this->ArrayVariableValues['CLRTMLPOptionNoBorder'] == '1')) {
			$arrStyle = array();
			$arrStyle['name'] = '.ui-btn-inner';
			
			//@JAPR 2016-02-16: Corregido un bug, faltaba aplicar el # al color
			$strBorderColor = "border: 0px solid transparent;";
			
			$arrStyle['style'] = $strBorderColor;
			$arrDef[] = $arrStyle;
			//@JAPR 2016-02-16: Agregado el theme "g" dado a los cambios de Conchita en v6 para separar clases
			$arrStyle['name'] = '.ui-btn-up-c';
			$arrDef[] = $arrStyle;
			
			//Quitar el borde inferior de la última opción de respuesta
			$arrStyle = array();
			$arrStyle['name'] = '.ui-btn-inner.ui-corner-right.ui-controlgroup-last';
			$strBorder = "border-bottom-width: 0px;";
			$arrStyle['style'] = $strBorder;
			$arrDef[] = $arrStyle;
		}
		
		//Altura del área de los mapas
		if ((isset($this->ArrayVariableValues['CLRTMLPSingleMapHeight']) && $this->ArrayVariableValues['CLRTMLPSingleMapHeight'] != '')) {
			$arrStyle = array();
			$arrStyle['name'] = '.singleChoiceMap';
			
			$strFixedStyle = "margin-top:2px;width:99%;";
			$strDivHeight = "height:".$this->ArrayVariableValues['CLRTMLPSingleMapHeight']."px;";
			
			$arrStyle['style'] = $strFixedStyle.$strDivHeight;
			$arrDef[] = $arrStyle;
		}
		
		if ((isset($this->ArrayVariableValues['CLRTMLPInlineNoBackground']) && $this->ArrayVariableValues['CLRTMLPInlineNoBackground'] == '1')) {
			$arrStyle = array();
			$arrStyle['name'] = 'table.inlineTab tr:nth-child(odd), table.inlineTab tr:nth-child(even)';
			
			$strBackground = "background-color: transparent;";
			
			$arrStyle['style'] = $strBackground;
			$arrDef[] = $arrStyle;
		}
		
		if (isset($this->ArrayVariableValues['CLRTMLPInlineInputsLabelColor']) || isset($this->ArrayVariableValues['CLRTMLPInlineInputsBackgroundColor']) || (
				(isset($this->ArrayVariableValues['CLRTMLPInlineInputsNoBorder']) && $this->ArrayVariableValues['CLRTMLPInlineInputsNoBorder'] == "1"))) {
			$arrStyle = array();
			$arrStyle['name'] = '.inlineTab input[type=text]';
			
			$strBackground = "";
			if (isset($this->ArrayVariableValues['CLRTMLPInlineInputsBackgroundColor'])) {
				$strBackground = "background-color: #".$this->ArrayVariableValues['CLRTMLPInlineInputsBackgroundColor'].";";
			}
			$strLabelColor = "";
			if (isset($this->ArrayVariableValues['CLRTMLPInlineInputsLabelColor'])) {
				$strLabelColor = "color: #".$this->ArrayVariableValues['CLRTMLPInlineInputsLabelColor'].";";
			}
			
			$strBorderColor = "";
			if (isset($this->ArrayVariableValues['CLRTMLPInlineInputsNoBorder']) && $this->ArrayVariableValues['CLRTMLPInlineInputsNoBorder'] == "1") {
				$strBorderColor = "border: 0px solid transparent;";
			}
			
			$arrStyle['style'] = $strBackground.$strLabelColor.$strBorderColor;
			$arrDef[] = $arrStyle;
		}
		
		if (isset($this->ArrayVariableValues['CLRTMLPInlineInputsLabelColor']) ||
				(isset($this->ArrayVariableValues['CLRTMLPInlineInputsDisabledNoBackground']) && $this->ArrayVariableValues['CLRTMLPInlineInputsDisabledNoBackground'] == "1") || 
				(isset($this->ArrayVariableValues['CLRTMLPInlineInputsDisabledBold']) && $this->ArrayVariableValues['CLRTMLPInlineInputsDisabledBold'] == "1") ||
				(isset($this->ArrayVariableValues['CLRTMLPInlineInputsNoBorder']) && $this->ArrayVariableValues['CLRTMLPInlineInputsNoBorder'] == "1")) {
			$arrStyle = array();
			$arrStyle['name'] = '.inlineTab input[type=text][disabled], .overflowCell';
			
			$strBackground = "";
			if (isset($this->ArrayVariableValues['CLRTMLPInlineInputsDisabledNoBackground']) && $this->ArrayVariableValues['CLRTMLPInlineInputsDisabledNoBackground'] == "1") {
				$strBackground = "background-color: transparent;";
			}
			$strLabelColor = "";
			if (isset($this->ArrayVariableValues['CLRTMLPInlineInputsLabelColor'])) {
				$strLabelColor = "color: #".$this->ArrayVariableValues['CLRTMLPInlineInputsLabelColor'].";";
			}
			$strLabelWeight = "";
			if (isset($this->ArrayVariableValues['CLRTMLPInlineInputsDisabledBold']) && $this->ArrayVariableValues['CLRTMLPInlineInputsDisabledBold'] == "1") {
				$strLabelWeight = "font-weight: bold;";
			}
			
			$strBorderColor = "";
			if (isset($this->ArrayVariableValues['CLRTMLPInlineInputsNoBorder']) && $this->ArrayVariableValues['CLRTMLPInlineInputsNoBorder'] == "1") {
				$strBorderColor = "border: 0px solid transparent;";
			}
			
			$arrStyle['style'] = $strBackground.$strLabelColor.$strLabelWeight.$strBorderColor;
			$arrDef[] = $arrStyle;
		}
		//@JAPR
		
		//OMMC 2015-12-15: Estilos para ocultar las descripciones de las formas
		/*if (isset($this->ArrayVariableValues['CLRTMLPDescriptionContainers'])){
			$arrStyle = array();
			$arrStyle['name'] = '.formDescTD, .formDescTDdiv';
			$arrStyle['style'] = "height: {$this->ArrayVariableValues['CLRTMLPDescriptionContainers']}px;";
			$arrDef[] = $arrStyle;

			$arrStyle = array();
			$arrStyle['name'] = '.formDescTDdivDesc';
			$arrStyle['style'] = "display: none;";
			$arrDef[] = $arrStyle;
		}*/

		return $arrDef;
	}
}
?>
