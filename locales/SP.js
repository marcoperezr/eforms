{
"Description" : "Descripción",
"Meeting"	: "Reunión",
"Responsible"	: "Responsable",
"Deadline" : "Fecha Límite",
"Status" : "Estatus",
"Finish" : "Terminar",
"Change" : "Cambiar",
"Insert" : "Insertar",
"Add comment" : "Agregar comentario",
"Change status" : "Cambiar estatus",
"Change responsible" : "Cambiar responsable",
"Change due date" : "Cambiar la fecha límite",
"Hide comments" : "Ocultar comentarios",
"Hide status" : "Ocultar estatus",
"Hide responsible" : "Ocultar responsable",
"Hide due date" : "Ocultar la fecha límite",
"Due date" : "Fecha límite",
"View comments" : "Ver los comentarios",
"View notes" : "Ver las notas",
"Finished" : "Terminado",
"In Progress" : "En progreso",
"Request" : "Solicitud",
"Stand by" : "En espera",
"Rejected" : "Rechazado",
"Source" : "Fuente",
"Category" : "Categoría",
"Add description" : "Agregar descripción",
"Add Resposible" : "Agregar Responsable",
"Add Deadline" : "Agregar Fecha Límite",
"Add category" : "Agregar categoría",
"Add title" : "Agregar título",
"Add notes" : "Añadir notas",
"My action items" : "Mis Acciones",
"New Action" : "Nueva Acción",
"Action Forms" : "Acciones de forma",
"Drop files here to upload" : "Dejar archivos aquí para subirlos",
"Actions" : "Acciones",
"Seach" : "Buscar",
"Submit" : "Enviar",
"Cancel" : "Cancelar",
"Failed" : "Fallado",
"Title" : "Título",
"Dead line" :"Fecha límite",
"Enable" : "Habilitada",
"Disable" : "Inhabilitada",
"Add " : "Agregar ",
"Add" : "Agregar",
"Save" : "Guardar",
"Save " : "Guardar ",
"Edit" : "Editar",
"Edit " : "Editar ",
"Add note" : "Añadir nota",
"Add new action item" : "Agregar nueva acción",
"Created by" : "Creado por",
"Finished" : "Terminado",
"In progress" : "En progreso",
"Request" : "Solicitud",
"Stand by" : "En espera",
"Rejected" : "Rechazado",
"Import" : "Importar",
"Import Excel" : "Importar a Excel",
"Export" : "Exportar",
"Export PDF" : "Exportar a PDF",
"Header (Optional):" : "Encabezado (Opcional):",
"Footer (Optional):" : "Pie de página (Opcional):",
"Export Excel" : "Exportar a Excel",
"Catalogs" : "Catálogos",
"Delete" : "Eliminar",
"Users Fields" : "Campos de Usuario",
"Save" : "Guardar",
"Save & new" : "Guardar & nuevo",
"New " : "Nuevo ",
"New" : "Nuevo",
"more..." : "más...",
"More" : "Más",
"Catalogs..." : "Catalogos...",
"Catalogs" : "Catalogos",
"Catalog" : "Catalogo",
"Search" : "Buscar",
"You must select at least one item" : "Debe seleccionar por lo menos un registro",
"You must select at least one record that meets the condition": "Debe seleccionar al menos un registro que cumpla la condición",
"Select a file to upload" : "Seleccione un archivo para cargar",
"[Only .xls or xlsx formats are supported]" : "[Sólo están soportados los formatos .xls o .xlsx]",
"Upload a file" : "Cargar un archivo",
"Select sheet." : "Seleccionar hoja",
"Mapping fields" : "Campos de mapeo",
"Sync data" : "Sincronización de datos",
"Loading" : "Cargando",
"Entries Per Page" : "Entradas por página",
"Page" : "Página",
"of" : "de",
"Logout" : "Cerrar sesión",
"Send Database to Server" : "Enviar base de datos a Servidor",
"Users" : "Usuarios",
"User" : "Usuario",
"System" : "Sistema",
"First page" : "Primera página",
"Last page" : "Última página",
"Artus dashboards / Artus reports" : "Escenarios Artus / Reportes Artus",
"Are you sure you want to export this data?" : "¿Está seguro de querer expotar estos datos?",
"Failure while saving the data, it is posible they were not saved correctly" : "Falla al guardar los datos, es posible que los datos no se hayan guardado correctamente",
"Setup" : "Configuración",
"Users & Permissions" : "Usuarios & Permisos",
"Dashboards & Artus reports" : "Escenarios & Reportes de Artus",
"Location" : "Localización",
"Security forms" : "Seguridad en formas",
"Security views" : "Seguridad en vistas",
"Views" : "Vistas",
"View" : "Vista",
"Dashboards & Artus reports access" : "Acceso a escenarios & reportes de Artus",
"Dashboards" : "Escenarios",
"Reports" : "Reportes",
"Language" : "Idioma",
"In this section you can select the language for the options and features of the product" : "En esta sección se puede seleccionar el idioma para mostrar las opciones y funcionalidades del producto",
"List of the available Dashboards and Reports you can select to be displayed by clicking the Artus Dashboards quick access button" : "Lista de los Escenarios y Reportes disponibles, los cuales se pueden seleccionar para mostrarse al hacer clic en el botón de acceso rápido de Artus Dashboards",
"Definition of modification privileges, exporting and importing permissions per role" : "Definición de los privilegios para modificar y de los permisos para exportar e importar para cada rol",
"Definition of application access and privileges for each user" : "Definición del acceso a las aplicaciones y privilegios para cada usuario",
"All roles": "Todos los roles",
"All users" :  "Todos los usuarios",
"Hide filter" : "Ocultar filtro",
"Show filter" : "Mostrar filtro",
"Change password" : "Cambiar contraseña",
"Applications" : "Aplicaciones",
"Password" : "Contraseña",
"Subordinates" : "Subordinados",
"New password: " : "Nueva contraseña: ",
"Repeat new password: " : "Repita la nueva contraseña: ",
"Current password: " : "Contraseña actual: ",
"Passwords do not match" : "Las contraseñas no coinciden",
"Your password is incorrect" : "Su contraseña es incorrecta",
"Please enter 1 more character" : "Por favor, ingrese 1 carácter más",
"You can only select 1 item" : "Únicamente puede seleccionar 1 elemento",
"Searching" : "Buscando",
"No matches found" : "No se han encontrado resultados",
"This field is required" : "Este campo es requerido",
"Please enter a valid email address" : "Por favor, ingrese un correo electrónico válido",
"Please enter a valid date" : "Por favor, ingrese una fecha válida",
"Please enter a valid number" : "Por favor, ingrese un número válido",
"Please enter only digits" : "Por favor, ingrese sólo números",
"Please enter the same value again" : "Por favor, ingrese de nuevo el mismo valor",
"Please enter no more than {0} characters" : "Por favor, no ingrese más de {0} caracteres",
"Please enter at least {0} characters" : "Por favor, ingrese al menos {0} caracteres",
"Please enter a value between {0} and {1} characters long" : "Por favor, ingrese un valor de entre {0} y {1} caracteres de largo",
"Please enter a value between {0} and {1}" : "Por favor, ingrese un valor entre {0} y {1}",
"Please enter a value less than or equal to {0}" : "Por favor, ingrese un valor menor o igual que {0}",
"Please enter a value greater than or equal to {0}" : "Por favor, ingrese un valor mayor o igual que {0}",
"Custom date range" : "Rango de fechas",
"From" : "De",
"To" : "A",
"Alarms" : "Alarmas",
"Specify criteria" : "Especificar criterio",
"Specify alarm" : "Especificar alarma",
"Business Process Report" : "Informe de Procesos de Negocio",
"Customization" : "Personalizar",
"Geolocation" : "Geolocalización",
"Photo" : "Foto",
"Action not allowed. There are detail records that should be deleted before." : "Acción no permitida. Hay registros de detalles que deben ser eliminados antes.",
"An error occurred while performing the action. Please report it to the system administrator." : "Ocurrió un error al realizar la acción. Favor de reportarlo con el administrador del sistema.",
"Records were not eliminated because they do not match the criteria." : "Registros no se eliminaron porque no cumplen con el criterio.",
"Business Process" : "Proceso de Negocios",
"Conditional formatting" : "Formato condicional",
"day(s) to answer" :  "dia(s) para responder",
"Notification"  :  "Notificacion",
"Notifications"  :  "Notificaciones",
"Actions not available" : "No hay acciones disponibles",
"There are no actions" : "No hay acciones",
"Application customization" : "Personalizar la Aplicación",
"In this section you can customize aspects of the implementation" : "En esta sección puedes personalizar aspectos de la aplicación",
"Application" : "Aplicación",
"Navigation" : "Navegación",
"Font Settings" : "Configuración de Fuente",
"Grouping of navigations" : "Agrupamiento de las navegaciones",
"Buttons" : "Botones",
"Forms" : "Formularios",
"Positioning" : "Posicionamiento",
"In this section you can set options and features of the devices gps" : "En esta sección puedes configurar las opciones y características del gps en el dispositivo",
"Maximum age" : "Limite máximo",
"Time out" : "Tiempo de espera",
"Enable high accuracy" : "Habilitar alta precisión",
"Camera" : "Cámara",
"In this section you can set options and features of the devices camera" : "En esta sección puedes configurar las opciones y características de la cámara en el dispositivo",
"Quality" : "Calidad",
"Destination type" : "Tipo de destino",
"Source type" : "Tipo de fuente",
"Allow edit" : "Permitir edición",
"Encoding type" : "Tipo de codificación",
"Target width" : "Ancho del destino",
"Target height" : "Alto del destino",
"Save to photo album" : "Guardar para álbum de fotos",
"Business Process Report" : "Informe sobre los procesos de negocio",
"Web Application" : "Aplicación Web",
"Mobile Application" : "Aplicación Móvil",
"Field" : "Campo",
"Fields" : "Campos",
"Condition" : "Condición",
"Value" : "Valor",
"All records" : "Todos los registros",
"Selected records" : "Registros seleccionados",
"Form-View permissions" : "Permisos en Formularios/Vistas",
"Add criteria" : "Agregar criterio",
"English" : "Inglés",
"Yes" : "Si",
"Customizing View" : "Personalizar la vista",
"In this section you can customize aspects of the view" : "En esta sección puedes personalizar aspectos de la vista",
"Select a view" : "Seleccione una vista",
"Select a form" : "Seleccione una forma",
"Color and font in views" : "Color y tipo de fuente en vistas",
"View headers" : "Visualización en los encabezados",
"Rows in the view" : "Visualización en las filas",
"Spacing and view colors" : "Espaciado y colores de vista",
"Patterned Colors" : "Modelo de colores",
"Primary color" : "Color primario",
"Secondary color" : "Color secundario",
"Width of each record and separation" : "Separación y anchura de cada registro",
"Width of each record" : "Ancho de cada registro",
"Line thickness" : "Grosor de la línea",
"Line Color" : "Color de línea",
"Publish" : "Publicar",
"Filter" : "Filtro",
"Filter properties" : "Propiedades de filtrado",
"Default status" : "Estado predeterminado",
"Filter appearance" : "Apariencia del filtro",
"Background color" : "Color de fondo",
"Space of each row" : "Espaciado de cada fila",
"Space between filters" : "Espaciado entre filtros",
"Font style" : "Estilo de fuente",
"Customizing Form" : "Personalización del Formulario",
"In this section you can customize aspects of the form" : "En esta sección puedes personalizar aspectos del formulario",
"Select forms" : "Seleccionar formulario",
"Select field" : "Seleccionar campo",
"Select a field:" : "Seleccione un campo:",
"All views" : "Todas las Vistas",
"All fields" : "Todos los campos",
"All forms" : "Todas las formas",
"Medium" : "Medio",
"Large" : "Grande",
"Hidden" : "Oculto",
"Show" : "Visible",
"Font Settings" : "Configuración de fuente",
"Latin text font" : "Fuente de texto latina",
"Size" : "Tamaño",
"Font color" : "Color de fuente",
"Add a grouper" : "Agregar un agrupador",
"Add group" : "Agregar grupo",
"No specify criteria" : "Sin especificar criterios",
"Each Button": "Cada Botón",
"Style all buttons" : "Estilo de todos los botones",
"Button color" : "Color de botón",
"Button New" : "Botón Nuevo",
"Button Delete" : "Botón Eliminar",
"Button Export" : "Botón Exportar",
"Button Import" : "Botón Importar",
"Button Save" : "Botón Guardar",
"Button Cancel" : "Botón Cancelar",
"Button Actions" : "Botón de Acciones",
"Button Workflow" : "Botón de Flujo de Trabajo",
"Create View" : "Crear Vista",
"Edit View" : "Editar Vista",
"Delete View" : "Eliminar Vista",
"Criteria" : "Criterios",
"Create" : "Crear",
"Entity" : "Entidad",
"file with passed id was not added, or already uploaded or cancelled" : "El archivo con este id no se añadio, ya se cargo o cancelo",
"{file} has invalid extension. Only {extensions} are allowed." : "{file} tiene una extensión invalida. Sólo extensiones {extensions} son permitidas.",
"{file} is too large, maximum file size is {sizeLimit}." : "{file} es demasiado grande, El tamaño máximo del archivo debe ser de {sizeLimit}.",
"{file} is too small, minimum file size is {minSizeLimit}." : "{file} es demasiado pequeño, El tamaño minimo del archivo debe ser de {minSizeLimit}.",
"{file} is empty, please select files again without it." : "{file} está vacío, Por favor seleccione nuevamente el archivo.",
"The files are being uploaded, if you leave now the upload will be cancelled." : "Los archivos se están cargando, si te sales ahora la carga se cancelará",
"Select sheet" : "Seleccione una hoja",
"Mapping fields" : "Mapear campos",
"By role" : "Por rol",
"By user" : "Por usuario",
"Note: " : "Nota: ",
"When the condition is true it will be possible to perform the selected actions." : "Cuando la condición sea verdadera, será posible llevar a cabo las acciones seleccionadas.",
"Security by role" : "Seguridad por rol",
"Security by user" : "Seguridad por usuario",
"Administrators" : "Administradores",
"Forms Administrators" : "Administradores de Formularios",
"Guests" : "Invitados",
"Nothing" : "Nada",
"Criteria name:" : "Nombre del criterio:",
"User Defined Fields" : "Campos Definidos por el Usuario",
"System Fields" : "Campos del Sistema",
"-Select-" : "-Seleccionar-",
"Add Condition Subgroup" : "Agregar Condición a Subgrupo",
"Assign security" : "Asignar seguridad",
"New criteria" : "Nuevo criterio",
"Equal" : "Igual a",
"Not equal to" : "No es igual a",
"Contains" : "Contiene",
"Doesn't contains" : "No contiene",
"Starts with" : "Inicia con",
"Ends with" : "Termina con",
"Is empty" : "Esta vacío",
"Is not empty" : "No esta vacío",
"Greater than" : "Mayor que",
"Less than" : "Menor que",
"equal to" : "igual a",
"not equal to" : "no es igual a",
"contains" : "contiene",
"doesn't contains" : "no contiene",
"starts with" : "inicia con",
"ends with" : "termina con",
"is empty" : "esta vacío",
"is not empty" : "no esta vacío",
"greater than" : "mayor que",
"less than" : "menor que",
"The grouping has no name" : "No se ha asignado un nombre al grupo",
"Form" : "Formulario",
"Business Process Steps" : " Pasos del Proceso de Negocio",
"Modificate Date" : "Fecha de Modificación",
"Process status" : "Estado del proceso",
"Comments" : "Comentarios",
"Detail Business Process Report" : "Informe Detallado del Proceso de Negocio",
"Please enter all fields" : "Por favor llene todos los campos",
"Show / Hide Filters" : "Mostrar / Ocultar Filtros",
"Open" : "Abrir",
"Location not found" : "Ubicación no encontrada",
"Export application to a file" : "Exportar aplicación a un archivo",
"Import application from file" : "Importar aplicación desde un archivo",
"Upload the file with the definition of the application to import" : "Cargar el archivo con la definición de la aplicación a importar",
"Download the file with the definition of the application" : "Descargar el archivo con la con la definición de la aplicación",
"Map the field names with the appropriate column names of the source file that you import." : "Asigne los nombres de los campos con los nombres de las columnas correspondiente al archivo fuente que se importa.",
"Main section" : "Sección principal",
"Form Field:" : "Campos de Forma:",
"Import Field" : "Importar Campo",
"Seccion Header" : "Sección de Encabezado",
"Previous" : "Previo",
"None" : "Ninguno",
"Duplicate records" : "Duplicar Registros",
"Restart sorting" : "Reiniciar ordenamiento",
"Following Columns in your file are not mapped with any of the form field. The data in these columns will not be imported if you try to continue the importation." : "Las siguientes columnas de su archivo no se relacionan con ningún campo del formulario. Los datos de estas columnas no se importarán si se intenta continuar.",
"Map Fields" : "Mapeo de Campos",
"An error occurred in the application" : "Se ha producido un error en la aplicación",
"Send report to technical support" : "Enviar informe a soporte técnico",
"Sending..." : "Enviando...",
"Service Unavailable" : "Servicio no disponible",
"Report sent" : "Informe enviado",
"Action not allowed. There are detail records that should be deleted before" : "Acción no permitida. Hay detalles de registros que sedeben eliminar antes de",
"It is not permitted to insert in the form the " : "No esta permitido Insertar en el formulario del ",
"It is not permitted to insert in the form" : "No está permitido insertar en la forma",
"The server is not responding or is not reachable. " : "El servidor no responde o no se puede acceder. ",
"You have lost your connection, try to connect again later. " : "Usted ha perdido su conexión, Intente conectarse nuevamente o mas tarde. ",
"There's no default navigation view for this application" : "No hay vista de navegación predeterminada para está aplicación",
"Your version of app needs updating" : "La versión de su aplicación necesita ser actualizada",
"There was an error parsing the server response to this action: " : "Se ha producido un error al analizar la respuesta del servidor para esta acción: ",
"There was an error sending the section template EMail" : "Se ha producido un error al enviar el email de la plantilla de la sección",
"Section template EMail sent successfully" : "Plantilla de la sección EMail enviada con éxito",
"Operation for documents is only available online" : "Operación de documentos sólo disponible en línea",
"Getting content length is not supported." : "La longitud del contenido obtenido no está soportado",
"Server error. Upload directory isn't writable." : "Error en el servidor. No se puede escribir en el directorio de carga",
"No files were uploaded." : "No hay archivos subidos",
"File is empty" : "El archivo esta vacío",
"File is too large" : "El archivo es demasiado grande",
"File has an invalid extension" : "El archivo tiene una extensión invalida",
"Could not save uploaded file." : "No se pudo guardar el archivo cargado.",
"Could not save uploaded file. The upload was cancelled, or server error encountered." : "No se pudo guardar el archivo cargado. La carga ha sido cancelada o se encontro un error en el servidor.",
"element not found " : "Elemento no encontrado ",
"Import Summary" : "Importar resumen",
"No. of Records overwrite: " : "No. de registros sobre-escritos: ",
"No. of Records ignored: " : "No. de registros omitidos: ",
"No. of Records added: " : "No. de registros agregados: ",
"Continue import" : "Continuar Importación",
"Back" : "Atras",
"Submit" : "Enviar",
"View Name:" : "Nombre de la Vista:",
"Choose Columns" : "Elegir Columnas",
"Drag from the available columns to the selected columns and drop the field." : "De los campos disponibles en las columnas seleccionadas arrastre y coloque en campos.",
"Selected Columns:" : "Columnas Seleccionadas:",
"Available Columns:" : "Columnas disponibles:",
"Format:" : "Formato:",
"Label column:" : "Etiqueta de la columna:",
"Save Field" : "Guardar Campo",
"Next" : "Siguiente",
"Creation Date" : "Fecha de Creación",
"Notification Type" : "Tipo de Notificación",
"Origin Form" : "Formulario de Origen",
"These changes take effect the next time you log in.":"Estos cambios tomaran efecto en la próxima vez que inicie sesión.",
"View does not have a name":"La Vista no tiene un nombre",
"Home" : "Inicio",
"User fields" : "Campos de usuario",
"Create view" : "Crear vista",
"Accept" : "Aceptar",
"Erase Alert" : "Alerta de borrado",
"Alert" : "Alerta",
"Selected records will be deleted. Do you agree?" : "Los registros seleccionados serán eliminados. ¿Está de acuerdo?",
"unread notifications" : "notificaciones sin leer",
"Select one record" : "Seleccione un registro",
"Recurrence" : "Recurrencia",
"Daily" : "Diariamente",
"Weekly" : "Semanalmente",
"Monthly" : "Mensualmente",
"Day" : "Dia",
"Hours" : "Hora",
"Minutes" : "Minuto",
"Monday" : "Lunes",
"Tuesday" : "Martes",
"Wednesday" : "Miércoles",
"Thursday" : "Jueves",
"Friday" : "Viernes",
"Saturday" : "Sábado",
"Sunday" : "Domingo",
"The selected record is not linked to a 'Business Process'":"El registro seleccionado no está ligado a un 'Proceso de Negocio'",
"Go to Record" : "Ir al registro",
"Go to 'Business Process'" : "Ir al 'Proceso de Negocio'",
"Record was not saved because it does not meet the security criteria, please contact the application manager": "El registro no se guardó porque no cumple los criterios de seguridad, por favor contacte al administrador de la aplicación",
"Groupers cannot be grouped." : "Los agrupadores no se pueden agrupar",
"Missing fields in the definition of the list type Gantt view":"Faltan campos en la definición de la vista de tipo Gantt",
"The data will be synchronized in a deferred way, you will be notified by email when they have been saved in your project." : "Los datos serán sincronizados en forma diferida, se le notificará por correo cuando se hayan guardado en su proyecto.",
"Restore application" : "Restaurar aplicación",
"Send database" : "Enviar base de datos",
"The database has been received to be reviewed." : "La base de datos ha sido recibida para su revisión.",
"No. of Records affected" : "No. de registros afectados",
"A web service is a technology that uses a set of protocols and standards to the end of exchanging data between applications." : "Un servicio web es una tecnología que utiliza un conjunto de protocolos y estándares que sirven para intercambiar datos entre aplicaciones.",
"{1:} records were ignored because they did not match the criteria." : "{1:} registros se han ignorado por no cumplir los criterios.",
"Customize login window" : "Personalizar la ventana de inicio de sesión",
"In this section you can customize the login window. The made changes take effect the next time you log in." : "En esta sección se puede personalizar la ventana de inicio de sesión. Los cambios realizados se aplicarán la próxima vez que inicie sesión.",
"Window login" : "Ventana de inicio de sesión",
"Select position" : "Seleccione una posición",
"Top left" : "Arriba a la izquierda",
"Top center" : "Arriba en el centro",
"Top right" : "Arriba a la derecha",
"Middle left" : "En medio a la izquierda",
"Center" : "Centrado",
"Middle right" : "En medio a la derecha",
"Bottom left" : "Abajo a la izquierda",
"Bottom center" : "Abajo en el centro",
"Bottom right" : "Abajo a la derecha",
"Background image login" : "Cargar imagen de fondo",
"Upload an image" : "Subir imagen",
"Maximum size" : "Tamaño máximo",
"Restore defaults" : "Restaurar valores",
"Restore" : "Restablecer",
"Preview window login" : "Vista previa",
"Email" : "Correo electrónico",
"Password" : "Contraseña",
"Log in" : "Iniciar sesión",
"Login" : "Inicio de sesión",
"Do you wish to exit the view without saving the changes?" : "¿Desea salir de la vista sin guardar los cambios?",
"Show more": "Mostrar más",
"Show less": "Mostrar menos",
"No. of Records duplicated: ":"No. de registros duplicados: ",
"Select the view you appear by default" : "Selecciona la vista que aparecera por default",
"There are no Artus cubes defined" : "No hay cubos de Artus definidos",
"In this section you will be able to create new Artus cubes from eBavel forms by selecting different fields between the form relationships" : "En esta seccion podras crear nuevos cubos de artus a partir de las formas de eBavel seleccionando diferentes campos entre las relaciones de forma",
"Manage security settings in Production application version" : "Administrar la seguridad en la aplicación de Producción",
"The application has finished its building process, you can now access the app or share it" : "La construccion de la aplicacion a finalizado, puedes ir a la aplicacion o compartirla",
"The application can be built in the development or production environment, while the app is being developed and changes are made, it must be built in the development environment. Once it’s finished and the changes tested, it can be built to production. The security may be handled independently from the environment or in development only, managing it in development indicates that when the app is built to production all the security settings will be transferred from development to production. Current security settings are in" : "La construcción de la aplicación puede ser en el ambiente de desarrollo o producción, mientras se esté desarrollando la aplicación y realizando cambios debe construirse en el ambiente de desarrollo. Una vez finalizado y probado los cambios se puede construir a producción. La seguridad se puede manejar independiente del ambiente o solo en desarrollo, manejarla en desarrollo indica que cuando se construya a producción toda la configuración de seguridad será pasada de desarrollo a producción. La configuración actual de seguridad es en",
"If you disable the option, when you build to production, the app’s security will be replaced by that of development" : "Si desactiva la opción al construir a producción la seguridad de la aplicación será reemplazada por la de desarrollo",
"This process replaces all you have defined in the current application. Do you agree?" : "Este proceso reemplaza todo lo que se ha definido en la aplicación actual. Estás de acuerdo?",
"There are no Batch Processes defined" : "No hay procesos por lotes definidos",
"In this section you can define Batch Process to conduct a series of processes on records stored in an application eBavel" : "En esta sección usted podrá definir Procesos por Lotes que realizarán una serie de procesos sobre registros almacenados en una aplicación de eBavel",
"Code" : "Código",
"Live Chat" : "Chat en Línea",
"Batch Processes" : "Procesos por Lotes",
"Execution of the batch process " : "Ejecución del proceso por lotes ",
"The following errors have been found during the execution of batch process: " : "Los siguientes errores han sido encontrados durante la ejecución del proceso por lotes: ",
"The record is in the Business process":"El registro se encuentra en el Proceso de negocio",
"step":"paso",
"pending user":"pendiente por el usuario",
"Basic info":"Información básica",
"Execution frequency":"Frecuencia de ejecución",
"Disabled":"Deshabilitada",
"Execution Time":"Hora de ejecución",
"Weekdays":"Días de la semana",
"Day of the month":"Día del mes",
"Today" : "Hoy",
"This Week" : "Esta Semana",
"This Month" : "Este Mes",
"This Year" : "Este Año",
"Search" : "Buscar",
"Date" : "Fecha",
"Custom date range" : "Rango de Fecha",
"From" : "Desde",
"To" : "Hasta",
"Groups" : "Grupos",
"Users" : "Usuarios",
"Forms" : "Formas"
}