{
"Today" : "Hoy",
"This Week" : "Esta Semana",
"This Month" : "Este Mes",
"This Year" : "Este Año",
"Search" : "Buscar",
"Date" : "Fecha",
"Custom date range" : "Rango de Fecha",
"From" : "Desde",
"To" : "Hasta",
"Groups" : "Grupos",
"Users" : "Usuarios",
"Forms" : "Formas"
}