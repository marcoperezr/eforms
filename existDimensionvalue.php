<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

header('Content-Type: text/xml; charset=utf-8');

$CubeID = -1;
if (array_key_exists("CubeID", $_GET))
{
	$CubeID = $_GET["CubeID"];
}

$aDimensionValueID = -1;
if (array_key_exists("ObjectID", $_GET))
{
	$aDimensionValueID = $_GET["ObjectID"];
}

//echo '-1<SEP>'.$aDimensionValueID;
//die();

//@JAPR 2010-02-19: Validado que al editar no se pueda dejar otro nombre de ID igual a uno existente
//Si se envía un DimensionValueID adicional, se asume que es el valor original antes de editar, por lo que el parámetro ObjectID trae el valor
//como se planea grabar, así que en caso de existir ya el ObjectId y ser diferente del DimensionValueID, se estaría duplicando la descripción
//por lo cual se marcará un error
$aSourceDimensionValueID = null;
if (array_key_exists("DimensionValueID", $_GET))
{
	$aSourceDimensionValueID = $_GET["DimensionValueID"];
}
//@JAPR

$DimensionID = -1;
if (array_key_exists("DimensionID", $_GET))
{
	$DimensionID = $_GET["DimensionID"];
}

$anInstanceDynamicDimensionValue = BITAMDimensionValue::NewInstanceWithID($theRepository, $aDimensionValueID, $DimensionID, $CubeID);
if(!is_null($anInstanceDynamicDimensionValue))
{
	if (is_null($aSourceDimensionValueID) || $aSourceDimensionValueID != $aDimensionValueID)
	{
		echo '-1<SEP>'.translate('This value id already exists, please enter other value');
		die();
	}
}

echo '0<SEP>'.translate('Ok');

?>