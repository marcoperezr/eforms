<?php

require_once("repository.inc.php");
require_once("initialize.php");

class BITAMSurveyActionMap extends BITAMObject
{
	public $FieldMapID;						//Identificador único de la tabla
	public $SurveyID;						//ID de la encuesta para la que se está configurando la columna
	public $SurveyFieldID;					//ID que identifica el tipo de columna de la que se trata (sólo los IDs negativos son los mismos ID entre todas las encuestas para el mismo tipo de columna, el resto de los IDs representan los QuestionIDs específicos de cada encuesta - no usados originalmente, pero por si alguna vez se solicitaban -. 0 (default) == No es un mapeo de columna)
	public $SurveyFieldText;				//Etiqueta para desplegar durante la configuración con el tipo de columna 
	public $eBavelFieldID;					//ID de FieldForm de eBavel al que se mapea esta columna
	public $eBavelAppID;					//ID de la aplicación de eBavel de la que se extraerán los campos para usar en el mapeo
	public $eBavelFormID;					//ID de la forma de eBavel de la que se extraerán los campos para usar en el mapeo
	public $ArrayVariables;					//Contenido de la tabla cuando se genera la instancia, utilizado para crear dinámicamente las propiedades cuando se carga como instancia para la página de configuración
	public $ArrayVariablesKeys;
	public $ArrayVariableValues;
	public $ArrayVariablesExist;
	static $ArrayQuestionData = array();	//Datos utilizados de las preguntas (sólo son el Nombre, Etiqueta y tipo). Se usa así para no tener que cargar la instancia completa de las preguntas
	static $SurveysLoaded = array();		//Lista de las encuestas que ya han cargado la información de las preguntas
	
  	function __construct($aRepository, $aSurveyID = 0)
	{
		BITAMObject::__construct($aRepository);
		$this->FieldMapID = -1;
		$this->SurveyID = -1;
		$this->SurveyFieldID = 0;
		$this->eBavelFieldID = -1;
		$this->eBavelAppID = -1;
		$this->eBavelFormID = -1;
		$this->ArrayVariables = array();
		$this->ArrayVariablesKeys = array();
		$this->ArrayVariableValues = array();
		$this->ArrayVariablesExist = array();
		if ($aSurveyID > 0) {
			self::GetQuestionData($aRepository, $aSurveyID);
		}
	}
	
	//Obtiene los nombres físicos y etiquetas de las preguntas de la encuesta especificada y llena los arrays estáticos con esa información
	static function GetQuestionData($aRepository, $aSurveyID) {
		if (isset(self::$SurveysLoaded[$aSurveyID])) {
			return;
		}
		
		self::$SurveysLoaded[$aSurveyID] = 1;
	}
	
	static function NewInstance($aRepository, $aSurveyID = 0)
	{
		return new BITAMSurveyActionMap($aRepository, $aSurveyID);
	}
	
	static function NewInstanceWithID($aRepository, $aFieldMapID)
	{
		$anInstance = null;
		
		if (((int) $aFieldMapID) < 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.FieldMapID, A.SurveyID, A.SurveyFieldID, A.eBavelFieldID 
			FROM SI_SV_SurveyActionFieldsMap A 
			WHERE A.FieldMapID = ".((int) $aFieldMapID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyActionFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMSurveyActionMap::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$intSurveyID = (int) @$aRS->fields["surveyid"];
		$anInstance = BITAMSurveyActionMap::NewInstance($aRepository, $intSurveyID);
		$anInstance->FieldMapID = (int) @$aRS->fields["fieldmapid"];
		$anInstance->SurveyID = (int) @$aRS->fields["surveyid"];
		$anInstance->SurveyFieldID = (int) @$aRS->fields["surveyfieldid"];
		$anInstance->eBavelFieldID = (int) @$aRS->fields["ebavelfieldid"];
		$anInstance->SurveyFieldText = BITAMSurveyActionMap::GetSurveyColumnText($this->SurveyFieldID);
		return $anInstance;
	}
	
	//Regresa la etiqueta correspondiente a la columna indicada
	static function GetSurveyColumnText($aSurveyFieldID) {
		$aSurveyFieldID = (int) $aSurveyFieldID;
		
		$strSurveyFieldText = '';
		switch ($aSurveyFieldID) {
			case safidDescription:
				$strSurveyFieldText = translate('Action description');
				break;
			case safidResponsible:
				$strSurveyFieldText = translate('Action responsible');
				break;
			case safidCategory:
				$strSurveyFieldText = translate('Action category');
				break;
			case safidDueDate:
				$strSurveyFieldText = translate('Action due date');
				break;
			case safidStatus:
				$strSurveyFieldText = translate('Action status');
				break;
			case safidSource:
				$strSurveyFieldText = translate('Action source');
				break;
			case safidSourceID:
				$strSurveyFieldText = translate('Action source id');
				break;
			case safidSourceRowKey:
				$strSurveyFieldText = translate('Action source entry id');
				break;
			//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
			case safidTitle:
				$strSurveyFieldText = translate('Action title');
				break;
			//@JAPR
			default:
				//El resto de los textos se obtendrían directamente de las etiquetas de las preguntas, ya que los FieldIDs > 0 son siempre preguntas
				$strQuestionField = 'qfield'.$aSurveyFieldID;
				$strSurveyFieldText = @BITAMSurveyActionMap::$ArrayQuestionData[$strQuestionField]['Label'];
				if (is_null($strSurveyFieldText)) {
					$strSurveyFieldText = translate('Unknown');
				}
				break;
		}
		
		return $strSurveyFieldText;
	}
	
	//Regresa el nombre identificador de la columna para permitir la captura usando el Framework. Si regresa vacio entonces se trata de una columna
	//no soportada en esta versión
	static function GetSurveyColumnName($aSurveyFieldID) {
		$aSurveyFieldID = (int) $aSurveyFieldID;
		
		$strSurveyFieldText = '';
		switch ($aSurveyFieldID) {
			case safidDescription:
				$strSurveyFieldText = 'ActFldDescription';
				break;
			case safidResponsible:
				$strSurveyFieldText = 'ActFldResponsible';
				break;
			case safidCategory:
				$strSurveyFieldText = 'ActFldCategory';
				break;
			case safidDueDate:
				$strSurveyFieldText = 'ActFldDueDate';
				break;
			case safidStatus:
				$strSurveyFieldText = 'ActFldStatus';
				break;
			case safidSource:
				$strSurveyFieldText = 'ActFldSource';
				break;
			case safidSourceID:
				$strSurveyFieldText = 'ActFldSourceID';
				break;
			case safidSourceRowKey:
				$strSurveyFieldText = 'ActFldSourceEntryID';
				break;
			//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
			case safidTitle:
				$strSurveyFieldText = 'ActFldTitle';
				break;
			//@JAPR
			default:
				//El resto de los textos se obtendrían directamente de las etiquetas de las preguntas, ya que los FieldIDs > 0 son siempre preguntas
				$strQuestionField = 'qfield'.$aSurveyFieldID;
				$strSurveyFieldText = @BITAMSurveyActionMap::$ArrayQuestionData[$strQuestionField]['Name'];
				if (is_null($strSurveyFieldText)) {
					$strSurveyFieldText = '';
				}
				break;
		}
		
		return $strSurveyFieldText;
	}

	//Regresa id de la columna a partir del nombre identificador
	static function GetSurveyColumnID($aSurveyFieldName) {
		$aSurveyFieldName = strtolower(trim((string) $aSurveyFieldName));
		
		$intSurveyFieldID = 0;
		switch ($aSurveyFieldName) {
			case 'actflddescription':
				$intSurveyFieldID = safidDescription;
				break;
			case 'actfldresponsible':
				$intSurveyFieldID = safidResponsible;
				break;
			case 'actfldcategory':
				$intSurveyFieldID = safidCategory;
				break;
			case 'actfldduedate':
				$intSurveyFieldID = safidDueDate;
				break;
			case 'actfldstatus':
				$intSurveyFieldID = safidStatus;
				break;
			case 'actfldsource':
				$intSurveyFieldID = safidSource;
				break;
			case 'actfldsourceid':
				$intSurveyFieldID = safidSourceID;
				break;
			case 'actfldsourceentryid':
				$intSurveyFieldID = safidSourceRowKey;
				break;
			//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
			case 'actfldtitle':
				$intSurveyFieldID = safidTitle;
				break;
			//@JAPR
			default:
				$intSurveyFieldID = (int) str_ireplace('qfield', '', $aSurveyFieldName);
				break;
		}
		
		return $intSurveyFieldID;
	}
	
	//Llena la instancia de campos de eBavel con los valores default
	function resetDefaultSettings()
	{
		$this->ArrayVariables = array();
		$this->ArrayVariablesKeys = array();
		$this->ArrayVariableValues = array();
		$this->ArrayVariablesExist = array();
		
		//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
		$arrDefaultActionFields = array(safidDescription, safidResponsible, safidCategory, safidDueDate, safidStatus, safidSource, safidSourceID, safidSourceRowKey, safidTitle);
		//for ($intSurveyFieldID = safidDescription; $intSurveyFieldID >= safidSourceRowKey; $intSurveyFieldID--) {
		foreach ($arrDefaultActionFields as $intSurveyFieldID) {
			$this->ArrayVariablesKeys[$intSurveyFieldID] = 0;
			$this->ArrayVariables[$intSurveyFieldID] = BITAMSurveyActionMap::GetSurveyColumnName($intSurveyFieldID);
			$this->ArrayVariableValues[$intSurveyFieldID] = 0;
			$this->ArrayVariablesExist[$intSurveyFieldID] = false;
		}
	}
	
	//Carga la configuración completa para una encuesta (esta es la instancia que se deberá usar para desplegar la ventana de configuración)
	static function NewInstanceWithSurvey($aRepository, $aSurveyID)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$anInstanceTemp = null;
		$sql = "SELECT FieldMapID, SurveyID, SurveyFieldID, eBavelFieldID FROM SI_SV_SurveyActionFieldsMap WHERE SurveyID = $aSurveyID Order By FieldMapID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyActionFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyActionFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		$anInstanceTemp = BITAMSurveyActionMap::NewInstance($aRepository, $aSurveyID);
		$anInstanceTemp->SurveyID = $aSurveyID;
		$anInstanceTemp->resetDefaultSettings();
		$ArrayVariables = &$anInstanceTemp->ArrayVariables;
		$ArrayVariablesKeys = &$anInstanceTemp->ArrayVariablesKeys;
		$ArrayVariableValues = &$anInstanceTemp->ArrayVariableValues;
		$ArrayVariablesExist = array();
		
		while(!$aRS->EOF)
		{
			$intFieldMapID = (int) @$aRS->fields["fieldmapid"];
			$intSurveyID = (int) @$aRS->fields["surveyid"];
			$intSurveyFieldID = (int) @$aRS->fields["surveyfieldid"];
			$inteBavelFieldID = $aRS->fields["ebavelfieldid"];
			$strVariableName = BITAMSurveyActionMap::GetSurveyColumnName($intSurveyFieldID);
			if ($strVariableName != '') {
				$ArrayVariables[$intSurveyFieldID] = $strVariableName;
				$ArrayVariablesKeys[$intSurveyFieldID] = $intFieldMapID;
				$ArrayVariableValues[$intSurveyFieldID] = $inteBavelFieldID;
				$ArrayVariablesExist[$intSurveyFieldID] = true;
			}
			$aRS->MoveNext();
		}
		
		$strtemp = 			'if (!class_exists("BITAMSurveyActionMapExtended"))'."\n";
		$strtemp = $strtemp.'{'."\n";
		$strtemp = $strtemp.'	class BITAMSurveyActionMapExtended extends BITAMSurveyActionMap'."\n";
		$strtemp = $strtemp.'	{'."\n";
		foreach($ArrayVariables as $intSurveyFieldID => $strVariableName)
		{   
			$intVariableValue = (int) @$ArrayVariableValues[$intSurveyFieldID];
			$strtemp = $strtemp.'public $'.$strVariableName.' = '.$intVariableValue.';'."\n";
			
		}
		$strtemp = $strtemp.'	}'."\n";
		$strtemp = $strtemp.'}'."\n";
		//@JAPR 2012-05-18: Validación para evitar que invocar múltiples veces al SaveData en la misma sesión provoque que esta clase dinámica
		//sea redeclarada, de todas formas, las configuraciones dificilmente cambiarán durante la ejecución de una llamada de grabado como para 
		//que esto pudiera ser un problema
		$strtemp = addcslashes( $strtemp, '\\');
		eval($strtemp);
		$strtemp = "";
		//@JAPR
		$strtemp = $strtemp.'$theobject = new BITAMSurveyActionMapExtended($aRepository);'."\n";
		$strtemp = addcslashes( $strtemp, '\\');
		eval($strtemp);
		$theobject->SurveyID = $aSurveyID;
		$theobject->ArrayVariables = $ArrayVariables;
		$theobject->ArrayVariablesKeys = $ArrayVariablesKeys;
		$theobject->ArrayVariableValues = $ArrayVariableValues;
		$theobject->ArrayVariablesExist = $ArrayVariablesExist;
		$theobject->IsDialog = true;
		
		return($theobject);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$inteBavelAppID = (int) @$aHTTPRequest->GET["eBavelAppID"];
		$inteBavelFormID = (int) @$aHTTPRequest->GET["eBavelFormID"];
		$aSurveyID = (int) @$aHTTPRequest->POST["SurveyID"];
		if ($aSurveyID > 0)
		{
			$anInstance = BITAMSurveyActionMap::NewInstanceWithSurvey($aRepository, $aSurveyID);
			$anInstance->updateFromArray($aHTTPRequest->POST);
			$aResult = $anInstance->save();
			
			if (array_key_exists("go", $_POST))
			{
				$go = $_POST["go"];
			}
			else
			{
				$go = "self";
			}
			
			if ($go == "parent")
			{
				$anInstance = $anInstance->get_Parent();
			}
			else
			{
				if ($go == "new")
				{
					$anInstance = BITAMSurveyActionMap::NewInstance($aRepository, $aSurveyID);
				}
			}
			
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		else {
			$aSurveyID = (int) @$aHTTPRequest->GET["SurveyID"];
		}
		
		$anInstance = BITAMSurveyActionMap::NewInstanceWithSurvey($aRepository, $aSurveyID);
		if (!is_null($anInstance)) {
			$anInstance->eBavelAppID = $inteBavelAppID;
			$anInstance->eBavelFormID = $inteBavelFormID;
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
		{
			if (array_key_exists($strVariableName, $anArray))
			{
				$this->$strVariableName = $anArray[$strVariableName];
				$this->ArrayVariableValues[$intSurveyFieldID] = $anArray[$strVariableName];
			}
		}
		return $this;
	}
	
	function save()
	{
		foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
		{
			$sql = "";
			$intVariableValue = (int) @$this->$strVariableName;
			if (isset($this->ArrayVariablesExist[$intSurveyFieldID]) && $this->ArrayVariablesExist[$intSurveyFieldID])
			{
				if ($intVariableValue > 0) {
					$sql = "UPDATE SI_SV_SurveyActionFieldsMap SET eBavelFieldID = ".$intVariableValue.
					" WHERE SurveyID = ".$this->SurveyID." AND SurveyFieldID = $intSurveyFieldID";
				}
				else {
					$sql = "DELETE FROM SI_SV_SurveyActionFieldsMap 
						WHERE SurveyID = ".$this->SurveyID." AND SurveyFieldID = $intSurveyFieldID";
				}
			}
			else 
			{
				$sql =  "SELECT ".
					$this->Repository->DataADOConnection->IfNull("MAX(FieldMapID)", "0")." + 1 AS FieldMapID".
					" FROM SI_SV_SurveyActionFieldsMap";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$intFieldMapID = (int) @$aRS->fields["fieldmapid"];
				
				if ($intVariableValue != 0) {
					$sql = "INSERT INTO SI_SV_SurveyActionFieldsMap (FieldMapID, SurveyID, SurveyFieldID, eBavelFieldID) VALUES (".
						$intFieldMapID.", ".$this->SurveyID.", ".$intSurveyFieldID.", ".$intVariableValue.")";
				}
				else {
					$sql = "";
				}
			}
			
			if ($sql != '' && $this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	
	function isNewObject()
	{
		return false;
	}
	
	function get_Title()
	{
		return translate("eBavel field mapping");
	}
	
	function get_QueryString()
	{
		return "BITAM_PAGE=SurveyFieldMap";
	}
	
	function get_Parent()
	{
		//return $this->Repository;
		return $this;
	}
	
	function get_PathArray()
	{
		//$parent = $this->get_Parent();
		//$pathArray = $parent->get_PathArray();
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'FieldMapID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		require_once('eBavelIntegration.inc.php');
		
		$myFields = array();
		$categoryCatalogField = null;
		
		//Agrega la encuesta ya que realmente la ventana graba en base a ella y no a un campo específico
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyID";
		$aField->Title = translate("Survey");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = array($this->SurveyID => 'Survey');
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		//Obtiene la lista de aplicaciones de eBavel creadas en el repositorio
		$arreBavelApps = array(0 => '('.translate('None').')');
		$arreBavelApps = GetEBavelCollectionByField(@GetEBavelApplications($this->Repository), 'applicationName', 'key', true);
		
		$aFieldApps = BITAMFormField::NewFormField();
		$aFieldApps->Name = "eBavelAppID";
		$aFieldApps->Title = translate("eBavel application");
		$aFieldApps->Type = "Object";
		$aFieldApps->VisualComponent = "Combobox";
		$aFieldApps->Options = $arreBavelApps;
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		$aFieldApps->InTitle = true;
		$aFieldApps->IsVisible = false;
		$myFields[$aFieldApps->Name] = $aFieldApps;
		
		//Obtiene la lista de formas de eBavel creadas en el repositorio
		$arreBavelForms = array('' => array('' => '('.translate('None').')'));
		$arreBavelForms = GetEBavelCollectionByField(@GetEBavelSections($this->Repository), 'sectionName', 'key', true, 'appId');
		
		$aFieldForms = BITAMFormField::NewFormField();
		$aFieldForms->Name = "eBavelFormID";
		$aFieldForms->Title = translate("eBavel form");
		$aFieldForms->Type = "Object";
		$aFieldForms->VisualComponent = "Combobox";
		$aFieldForms->Options = $arreBavelForms;
		$aFieldForms->InTitle = true;
		$aFieldForms->IsVisible = false;
		$myFields[$aFieldForms->Name] = $aFieldForms;
		$aFieldForms->Parent = $aFieldApps;
		$aFieldApps->Children[] = $aFieldForms;
		
		$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, null, null, null, true, '');
		//Array indexado por tipo de dato (de eForms) + EBavelSectionId + EBavelFieldID para obtener los arrays que se usarán al sincronizar
		//las combos de cada campo de mapeo de eForms
		$arrAllEBavelFormsFields = array();
		$arrEmptyeBavelForms = array('0' => array(0 => translate('None')));
		
		//Primero tiene que generar una serie de Arrays basados en los tipos de campos pero agrupados por FormID
		//ya que finalmente los campos se deben entrelazar conforme se van seleccionando las combos padre. La agrupación final será por
		//el tipo de campo para poder aplicar en cada combo la colección correcta
		foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
			if (!isset($arrAllEBavelFormsFields[$intQTypeID])) {
				$arrAllEBavelFormsFields[$intQTypeID] = array();
			}
			
			foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
				$intFormID = (int) @$arrFieldData['section_id'];
				if (!isset($arrAllEBavelFormsFields[$intQTypeID][$intFormID])) {
					$arrAllEBavelFormsFields[$intQTypeID][$intFormID] = array();
					$arrAllEBavelFormsFields[$intQTypeID][$intFormID][0] = translate('None');
				}
				
				if ($intFieldID > 0) {
					$arrAllEBavelFormsFields[$intQTypeID][$intFormID][$intFieldID] = $arrFieldData['label'];
				}
			}
		}
		
		$arreBavelAlphaFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpOpenAlpha])) {
			$arreBavelAlphaFieldsColl = $arrAllEBavelFormsFields[qtpOpenAlpha];
		}
		
		$arreBavelDateFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpOpenDate])) {
			$arreBavelDateFieldsColl = $arrAllEBavelFormsFields[qtpOpenDate];
		}
		
		$arreBavelTimeFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpOpenTime])) {
			$arreBavelTimeFieldsColl = $arrAllEBavelFormsFields[qtpOpenTime];
		}
		
		$arreBavelNumericFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpOpenNumeric])) {
			$arreBavelNumericFieldsColl = $arrAllEBavelFormsFields[qtpOpenNumeric];
		}
		
		$arreBavelLocationFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpLocation])) {
			$arreBavelLocationFieldsColl = $arrAllEBavelFormsFields[qtpLocation];
		}
		
		$arreBavelEmailFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpEMail])) {
			$arreBavelEmailFieldsColl = $arrAllEBavelFormsFields[qtpEMail];
		}
		
		//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
		$arreBavelTextFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpOpenString])) {
			$arreBavelTextFieldsColl = $arrAllEBavelFormsFields[qtpOpenString];
		}

		//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
		$arreBavelTextFieldsColl = $arrEmptyeBavelForms;
		if (isset($arrAllEBavelFormsFields[qtpBarCode])) {
			$arreBavelTextFieldsColl = $arrAllEBavelFormsFields[qtpBarCode];
		}
		
		//Genera todos los campos de cada columna a configurar, todos podrán configurarse hacia un campo alfanumérico de eBavel + el campo que
		//corresponde con el tipo específico de cada columna
		foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = $strVariableName;
			$aField->Title = BITAMSurveyActionMap::GetSurveyColumnText($intSurveyFieldID);
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			
			$arrMyeBavelFormsFields = array();
			switch($intSurveyFieldID)
			{
				case safidDueDate:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelDateFieldsColl, $arreBavelAlphaFieldsColl);
					break;
				/*
				case Hour:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelTimeFieldsColl, $arreBavelAlphaFieldsColl);
					break;
				
				case Location:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelLocationFieldsColl, $arreBavelAlphaFieldsColl);
					break;
				*/
				
				case safidSourceID:
				case safidSourceRowKey:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelAlphaFieldsColl, $arreBavelNumericFieldsColl);
					break;
				
				/*
				case EMail:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelEmailFieldsColl, $arreBavelAlphaFieldsColl);
					break;
				*/
				
				case safidCategory:
				case safidResponsible:
				case safidDescription:
				case safidStatus:
				case safidSource:
				//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
				case safidTitle:
					$arrMyeBavelFormsFields = array_multi_merge($arreBavelTextFieldsColl, $arreBavelAlphaFieldsColl);
					break;
				
				default:
					//En este caso, los campos no especificados en el switch se tratarían de campos de preguntas o de campos nuevos especificos
					//de acciones que en esta versión no se soportaban, si es lo segundo entonces se manerarán a alfanuméricos exclusivamente
					if ($intSurveyFieldID > 0) {
						//Dependiendo del tipo de pregunta, se podrá mapear a uno u otro tipo de campo
						$strQuestionField = 'qfield'.$aSurveyFieldID;
						$intQTypeID = (int) @BITAMSurveyActionMap::$ArrayQuestionData[$strQuestionField]['Type'];
						//@JAPR 2014-02-19: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
						//El tipo de campo qtpLocation se usó originalmente para identificar a campos tipo Geolocalización antes de que existieran las preguntas tipo
						//GPS en eForms, así que por compatibilidad hacia atrás se respetará dicho valor, sin embargo el tipo de campo qtpGPS creado precisamente
						//para las preguntas tipo GPS de eForms, se debe tratar igual que qtpLocation cuando es regresado por esta y otras funciones
						if ($intQTypeID == qtpGPS) {
							$intQTypeID = qtpLocation;
						}
						
						$arreBavelOwnTypeColl = $arrEmptyeBavelForms;
						if (isset($arrAllEBavelFormsFields[$intQTypeID])) {
							$arreBavelOwnTypeColl = $arrAllEBavelFormsFields[$intQTypeID];
						}
						$arrMyeBavelFormsFields = array_multi_merge($arreBavelOwnTypeColl, $arreBavelAlphaFieldsColl);
					}
					else {
						$arrMyeBavelFormsFields = $arreBavelAlphaFieldsColl;
					}
					break;
			}
			
			$aField->Options = $arrMyeBavelFormsFields;
			$myFields[$aField->Name] = $aField;
			$aField->Parent = $aFieldForms;
			$aFieldForms->Children[] = $aField;
		}
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{
 		$myFormName = get_class($this);
?>
	<script language="javascript" src="js/dialogs.js"></script>
 	<script language="JavaScript">
 		function returnFieldsData()
 		{
 			var objFieldNames = new Object();
<? 			
			foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
			{
?>
				objFieldNames['<?=$strVariableName?>'] = Trim(<?=$myFormName?>_SaveForm.<?=$strVariableName?>.value);
<?
			}
 ?>			
 			
 			var strAnd = '';
 			var sParamReturn = '';
 			var arrKeys = Object.keys(objFieldNames); 
 			var intLength = arrKeys.length;
			for (var intCont = intLength -1; intCont >= 0; intCont--) {
				var strFieldName = arrKeys[intCont];
				sParamReturn += strAnd + strFieldName + '=' + objFieldNames[strFieldName];
				strAnd = '|';
			}
 			
			try {
				if (window.setDialogReturnValue)
				{
					setDialogReturnValue(sParamReturn);
				}
			}
			catch (e) {
			}
			try {
				owner.returnValue = sParamReturn;
			}
			catch (e)
			{
			}
			
			//window.close();
			cerrar();
 		}
	</script>
<?
 	}
 	
	function generateAfterFormCode($aUser)
	{
		$myFormName = get_class($this);
		
?>
	<script language="JavaScript">
		objOkSelfButton = document.getElementById("<?=$myFormName?>_OkSelfButton");
		objOkParentButton = document.getElementById("<?=$myFormName?>_OkParentButton");
		
		if (objOkSelfButton != null) {
			objOkSelfButton.onclick = new Function("returnFieldsData();");
		}
		if (objOkParentButton != null) {
			objOkParentButton.style.display = 'none';
		}
		
		var streBavelFieldData = '';
		var objField = null;
		if (window.opener && window.opener.BITAMSurvey_SaveForm && window.opener.BITAMSurvey_SaveForm.eBavelFieldsData) {
			objField = window.opener.BITAMSurvey_SaveForm.eBavelFieldsData.value;
		}
		try {
			if (objField != null) {
				streBavelFieldData = window.opener.document.getElementsByName('eBavelFieldsData')[0].value;
			}
			
			if (streBavelFieldData) {
				var arrFieldData = streBavelFieldData.split('|');
				for (var intIndex in arrFieldData) {
					var strFieldInfo = Trim(arrFieldData[intIndex]);
					if (strFieldInfo) {
						var arrFieldInfo = strFieldInfo.split('=');
						if (arrFieldInfo.length >= 2) {
							var strFieldName = arrFieldInfo[0];
							var intFieldID = parseInt(arrFieldInfo[1]);
							if (intFieldID <= 0) {
								intFieldID = 0;
							}
							
							var objFieldColl = document.getElementsByName(strFieldName);
							if (objFieldColl.length) {
								objFieldColl[0].value = intFieldID;
							}
						}
					}
				}
			}
		}
		catch (e) {
		}
	</script>
<?
	}
}
?>