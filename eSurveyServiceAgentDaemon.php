<?php
session_start();

//@JAPR 2015-10-23: Agregado esta línea por instrucción de LRoux para intentar prevenir que llamadas al agente desde la tarea de windows se pudieran quedar en el limbo por problemas
//de timeout, esto a raiz de un caso que coincidió con la desconexión de KPI7 cuando el agente de KPI4 intentaba conectarse para obtener tareas, pero pese a que KPI7 ya estaba online
//el agente de nunca salió del intento de conexión y ninguna otra tarea fue procesada por él
set_time_limit(0);

/*
//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("eFormsRepositoryName");
$_SESSION["eFormsRepositoryName"] = "fbm_bmd_0081";
$_COOKIE['testing'] = 1;
*/

require_once('config.php');
require_once('cubeClasses.php');
require_once('eFormsAgent.inc.php');
//@JAPR 2013-07-11: Agregado el agente de eForms
require_once('eFormsAgentConfig.inc.php');
require_once('surveytask.inc.php');

//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Variable para indicar que el proceso se está ejecutando desde un agente de eForms en general, sea cual sea dicho agente
global $gbIsAneFormsAgent;
$gbIsAneFormsAgent = true;
//@JAPR

//@JAPR 2015-10-14: Corregido un bug, el identificador único sólo contenía el día y la hora incluyendo segundos, pero en un caso dos agentes si lograron entrar al mismos segundo así que
//se llegó a duplicar. En circunstancias normales eso no habría importado porque generalmente nunca dos agentes apuntarán al mismo repositorio, sin embargo hubo un error (ya corregido)
//donde el repositorio no se tomaba en cuenta para un SELECT, sino sólo el AgentID, así que se confundieron y dos agentes procesaron la misma tarea, con este uniqid + un prefijo enviado
//como parámetro en la URL de la tarea de Windows, se evitará repetir nuevamente este caso incluso si se deciden activar múltiples agentes para el mismo repositorio (en ese caso, el
//agentName sería obligatorio para asegurar que nunca se repetirá el ID, en cualqueir otro caso es opcional) (#NQ995V)
//@JAPR 2018-02-16: Agregado el agente de reprocesamiento automático de destinos de datos, además del archivo de configuraciones local del agente (#E5YASV)
//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Punto de entrada de un agente, deberá asignar el AgentName así como enviarlo a la función GeteFormsLogPath para que se registre en las llamadas subsecuentes
global $strAgentName;
$strAgentName = '';
//@JAPR

//@JAPR 2017-02-16: Agregadas configuraciones de línea de comando para limitar las acciones que el agente puede realizar (#0LC7NT)
//Configuraciones con valor default
global $blnEnableDataTasksProcessing;
$blnEnableDataTasksProcessing = true;
global $blnEnableDataSourcesTasksProcessing;
$blnEnableDataSourcesTasksProcessing = true;
global $blnEnableAgendasTasksProcessing;
$blnEnableAgendasTasksProcessing = true;
global $blnEnableIncDataDestinationsProcessing;
$blnEnableIncDataDestinationsProcessing = true;
global $blnEnableAgendaCubeTasksProcessing;
$blnEnableAgendaCubeTasksProcessing = true;
//Identifica las configuraciones ya sea por línea de comando o por parámetro de un request directo
if (isset($argv) && is_array($argv) && count($argv) > 2) {
	//Configuraciones por línea de comando
	foreach ($argv as $intParamNum => $strParamVal) {
		switch ($intParamNum) {
			case 0:		//Script name
				//Es irrelevante, es el nombre del archivo php del Agente
				break;
			case 1:		//Repository
				//No se procesa en este punto, así que será ignorado
				break;
			case 2:		//Switch para procesar tareas de datos
				$blnEnableDataTasksProcessing = (bool) $strParamVal;
				break;
			case 3:		//Switch para procesar tareas de DataSources
				$blnEnableDataSourcesTasksProcessing = (bool) $strParamVal;
				break;
			case 4:		//Switch para procesar tareas de generación de Agendas
				$blnEnableAgendasTasksProcessing = (bool) $strParamVal;
				break;
			case 5:		//Switch para procesar tareas de destinos incrementales
				$blnEnableIncDataDestinationsProcessing = (bool) $strParamVal;
				break;
			case 6:		//Switch para procesar tareas de generación del cubo de agendas
				$blnEnableAgendaCubeTasksProcessing = (bool) $strParamVal;
				break;
			//@JAPR 2015-10-14: Corregido un bug, el identificador único sólo contenía el día y la hora incluyendo segundos, pero en un caso dos agentes si lograron entrar al mismos segundo así que
			//se llegó a duplicar. En circunstancias normales eso no habría importado porque generalmente nunca dos agentes apuntarán al mismo repositorio, sin embargo hubo un error (ya corregido)
			//donde el repositorio no se tomaba en cuenta para un SELECT, sino sólo el AgentID, así que se confundieron y dos agentes procesaron la misma tarea, con este uniqid + un prefijo enviado
			//como parámetro en la URL de la tarea de Windows, se evitará repetir nuevamente este caso incluso si se deciden activar múltiples agentes para el mismo repositorio (en ese caso, el
			//agentName sería obligatorio para asegurar que nunca se repetirá el ID, en cualqueir otro caso es opcional) (#NQ995V)
			//@JAPR 2018-02-06: Faltaba agregar el parámetro vía tarea de windows desde el command prompt, así que se integró la posibilidad de variar
			//el nombre del archivo también desde línea de comando
			case 7:
				$strAgentName = (string) $strParamVal;
				break;
			//@JAPR
		}
	}
}
else {
	//Verifica si hay configuraciones por medio de un request
	if (getParamValue('EntryTasks', 'both', '(int)', true) !== null) {
		$blnEnableDataTasksProcessing = getParamValue('EntryTasks', 'both', '(bool)');
	}
	if (getParamValue('DataTasks', 'both', '(int)', true) !== null) {
		$blnEnableDataSourcesTasksProcessing = getParamValue('DataTasks', 'both', '(bool)');
	}
	if (getParamValue('AgendaTasks', 'both', '(int)', true) !== null) {
		$blnEnableAgendasTasksProcessing = getParamValue('AgendaTasks', 'both', '(bool)');
	}
	if (getParamValue('DestTasks', 'both', '(int)', true) !== null) {
		$blnEnableIncDataDestinationsProcessing = getParamValue('DestTasks', 'both', '(bool)');
	}
	if (getParamValue('AgendaModTasks', 'both', '(int)', true) !== null) {
		$blnEnableAgendaCubeTasksProcessing = getParamValue('AgendaModTasks', 'both', '(bool)');
	}
	//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strAgentName = getParamValue('AgentName', 'both', '(string)');
	//@JAPR
}

//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Integradas las funciones comunes para almacenamiento de log de eForms
$strLogPath = GeteFormsLogPath(null, $strAgentName);
//@JAPR

$strSessionID = "eFormsAgent_".date("YmdHis")."_".uniqid($strAgentName);

//Activar si se desea que los grabados de encuestas utilicen el modo de depuración, el cual arrojará muchos mas datos de las salidas del proceso
//de grabado enviando el parámetro DebugBreak 
$blnDebugAgentMode = false;
//@JAPR 2019-02-12: Modificado el comportamiento para permitir generar el Log del agente en tiempo de ejecución, enviando el parámetro para que se escriban los eventos mientras suceden
//y no hasta que ha terminado todo el proceso (#P5SE4W)
$blnSaveDebugLogOnTheFly = false;
//Nombre default del archivo de log de cada sesión del agente (es diferente en cada ejecución) y usado para todas las salidas a menos que en
//cierto proceso explícitamente se genere un archivo distinto (como en el resultado de un grabado de encuesta)
//@JAPR 2013-06-28: Modificado para que el log de procesamiento sea diario, mientras que el log de detalle de error si incluya el TimeStampt
$strDefaultLogStringFile = "eFormsAgent_{$strAgentName}".date("Ymd").'.log';
$strDefaultDetailLogStringFile = $strSessionID.'.log';
//Activar si se quiere registrar el archivo de Log cada vez que entra en ejecución el Agente de eForms, de lo contrario desactivada sólo generará
//dicho archivo en caso de error
$blnSaveLogStringFile = true;

//@JAPR 2018-02-07: Agregado el archivo de configuraciones semi-dinámico del agente para permitir variar su comportamiento sin tener que apagar la tarea de windows
//Se podrán asignar configuraciones forzadas modificando este archivo para no tener que cambiar el código y que por servicio se puedan hacer ajustes. Originalmente
//utilizado para activar el modo de depuración ($blnDebugAgentMode) (#E5YASV)
$strAgentSettings = "eSurveyServiceAgentSettings.php";
if (file_exists($strAgentSettings)) {
	require_once($strAgentSettings);
}

if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	ECHOString("eSurveyServiceAgentDaemon.php", 1, 1, "color:purple;");
}

logString(str_repeat('*', 80));
logString('Starting eForms agent');
if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	echo("<br>\r\n Agent session ID: {$strSessionID}<br>\r\n");
}

clearstatcache();

//Lee la configuración que utilizará este agente para procesar las tareas
@require_once("bpaemailconfiguration.inc.php");
$aConfiguration = @BPAEmailConfiguration::readConfiguration();
if (is_null($aConfiguration)) {
	logString('Unable to read eForms configuration file');
	exit;
}

if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	echo("<br>\r\n eForms Agent configuration:");
	PrintMultiArray($aConfiguration);
}

//Reinicia todos los valores del agente
//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//@JAPRDescontinuada, este archivo de seguridad NO se debe eliminar al iniciar cada ejecución del agente, es un archivo que manualmente debe ser removido por el usuario que lo creó para
//forzar a detener a los agentes (por ejemplo para una actualización), por lo que no se requiere una función para eliminarlo
//BPAEmailConfiguration::ResetAgent();

//Ahora se forzará a no iniciar el agente en caso de existir el archivo de seguridad
//Válvula de seguridad para abortar el agente en caso de que exista este archivo
$strStopFileName = $strLogPath.BPAEmailConfiguration::stopFile;
if ( file_exists($strStopFileName) ) {
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n eForms agent ({$strAgentName}) process was aborted by user action");
	}
	logString("eForms agent ({$strAgentName}) process was aborted by user action");
	die();
}
//@JAPR

$SMTPServer = (string) @$aConfiguration->fbm_email_server;	
$SMTPPort = (string) @$aConfiguration->fbm_email_port;
$SMTPUserName = (string) @$aConfiguration->fbm_email_username;
$SMTPPassword = (string) @$aConfiguration->fbm_email_pwd;
$SMTPEmailSource = (string) @$aConfiguration->fbm_email_source;

$activeAgents = array();
/*
foreach ($aConfiguration->agents as $eachAgent) {
	if ($eachAgent->active) {
		logString("Adding default Agent '{$aConfiguration->fbm_db_server}.fbm000'");
		$activeAgents[] = $eachAgent;
	}
}
*/

//@JAPR 2018-02-16: Agregado el agente de reprocesamiento automático de destinos de datos, además del archivo de configuraciones local del agente (#E5YASV)
$strAgentParams = "Agent parameters: Entry=".(int)$blnEnableDataTasksProcessing."; Data=".(int)$blnEnableDataSourcesTasksProcessing."; Agenda=".(int)$blnEnableAgendasTasksProcessing."; Dest=".(int)$blnEnableIncDataDestinationsProcessing."; AgendaMod=".(int)$blnEnableAgendaCubeTasksProcessing."; strAgentName=".$strAgentName;
//@JAPR
if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	ECHOString($strAgentParams, 1, 1, "color:purple;");
}
logString($strAgentParams);

//Obtiene la lista de repositorios a procesar en esta corrida del agente
$activeAgents = getFBMAgents($aConfiguration);
$intMaxAgents = count($activeAgents);

global $gblShowErrorSurvey;
$gblShowErrorSurvey = false;
global $intRequestNumber;
$intRequestNumber = 0;
//Si hay algunas tareas por ejecutar, recorre uno a uno los repositorios que tenían tareas pendientes y las procesa
if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	ECHOString("Recorre uno a uno los repositorios", 1, 1, "color:purple;");
}

$currentAgentIndex = 0;
try {
	$aBITAMConnection = null;
	//Recorre una a una las bases de datos identificadas
	while ($currentAgentIndex < $intMaxAgents) {
		//Recorre mientras aun existan procesos libres que puedan tomar nuevas tareas
		do {
			// get the agent
			$eachAgent = @$activeAgents[$currentAgentIndex];
			
			// execute the task if required
			if ($eachAgent) {
				logString("Check repository '{$eachAgent->repository}', server '{$eachAgent->server}'");
				//@JAPR 2017-02-16: Agregadas configuraciones de línea de comando para limitar las acciones que el agente puede realizar (#0LC7NT)
				if ($blnEnableDataTasksProcessing) {
					//Busca tareas hasta que se confirme que no hay una sola libre en este repositorio
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Busca tareas", 1, 1, "color:purple;");
					}
				}
				//@JAPR
				
				do {
					ob_start();
					try {
						//Cierra la conexión anterior si era de un server diferente
						if (!is_null($aBITAMConnection) && $aBITAMConnection->ADODBServer != $eachAgent->server) {
							@$aBITAMConnection->close();
							$aBITAMConnection = null;
						}
						
						if (is_null($aBITAMConnection)) {
							//@JAPR 2013-11-06: Modificado para reutilizar los archivos del Model Manager, los cuales no requieren el password desencriptado
							//Este password ya llega desencriptado en este punto
							//Ahora este password llega encriptado, pero así es como lo requiere BITAMConnection cuando se usa desde el
							//Model Manager
							$pass = $eachAgent->password;
							//@JAPR
							$aBITAMConnection = BITAMConnection::NewInstance($eachAgent->server, $eachAgent->username, $pass, "fbm000", 'mysql');
							if (is_null($aBITAMConnection) || is_null($aBITAMConnection->ADOConnection)) {
								$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
								ob_end_clean();
								logString("Unable to connect to '{$aConfiguration->fbm_db_server}': {$strErrorMsg}", '', true, true);
								return false;
							}
						}
					} catch (Exception $e){
						ob_end_clean();
						logString("Unable to connect to '{$aConfiguration->fbm_db_server}'", '', true, true);
						$currentAgentIndex++;
						//Brinca al siguiente agente de la lista
						continue;
					}
					ob_end_clean();
					
					//En este punto ya se tiene la conexión a la FBM000 del servidor del que se trate, así que se obtienen las tareas del
					//repositorio al que apunta el agente específico
					//Obtiene la siguiente tarea pendiente a ejecutar
					//@JAPR 2017-02-16: Agregadas configuraciones de línea de comando para limitar las acciones que el agente puede realizar (#0LC7NT)
					if ($blnEnableDataTasksProcessing) {
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("getNextTaskKeyToProcess", 1, 1, "color:purple;");
						}
						
						$intSurveyTaskID = getNextTaskKeyToProcess($aBITAMConnection, $eachAgent);
						
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("intSurveyTaskID: {$intSurveyTaskID}", 1, 1, "color:purple;");
						}
					}
					else {
						$intSurveyTaskID = 0;
					}
					//@JAPR
					
					//Si hay una tarea, invoca al proceso que la ejecuta
					if ($intSurveyTaskID > 0) {
						try {
							$blnStatus = executeSurveyTask($aBITAMConnection, $intSurveyTaskID);
						} catch (Exception $e) {
							logString($e->getMessage(), '', true, true);
						}
					}
					
					//Ahora se forzará a no iniciar el agente en caso de existir el archivo de seguridad
					//Válvula de seguridad para abortar el agente en caso de que exista este archivo
					clearstatcache();
					$strStopFileName = $strLogPath.BPAEmailConfiguration::stopFile;
					if ( file_exists($strStopFileName) ) {
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							echo("<br>\r\n eForms agent ({$strAgentName}) process was aborted by user action");
						}
						logString("eForms agent ({$strAgentName}) process was aborted by user action");
						die();
					}
					//@JAPR
				} while ($intSurveyTaskID > 0);
				
				//@JAPR 2015-09-07: Agregado el procesamiento de DataSources al agente, ahora independientemente de si tiene o no tareas que procesar, deberá incluir el repositorio para
				//procesar los DataSources
				//Al terminar de procesar las tareas, invoca al proceso que carga los datos de los DataSources
				//@JAPR 2017-02-16: Agregadas configuraciones de línea de comando para limitar las acciones que el agente puede realizar (#0LC7NT)
				if ($blnEnableDataSourcesTasksProcessing) {
					try {
						logString("Executing DataSources external upload", '', true, true);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Executing DataSources external upload", 1, 1, "color:blue;");
						}
						$blnStatus = executeDataSourcesUpload($aBITAMConnection, $eachAgent->repository);
					} catch (Exception $e) {
						logString($e->getMessage(), '', true, true);
					}
				}
				
				//@JAPR 2015-10-27: Agregado el proceso para generar todas las Agendas desde el Agente de eForms
				//Al terminar de procesar las dataSources, invoca al proceso que genera las agendas pendientes del día (se hace después de cargar los catálogos para tener sus
				//datos ya actualizados)
				//@JAPR 2017-02-16: Agregadas configuraciones de línea de comando para limitar las acciones que el agente puede realizar (#0LC7NT)
				if ($blnEnableAgendasTasksProcessing) {
					try {
						logString("Executing Agendas generation", '', true, true);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Executing Agendas generation", 1, 1, "color:blue;");
						}
						$blnStatus = executeAgendasGeneration($aBITAMConnection, $eachAgent->repository);
					} catch (Exception $e) {
						logString($e->getMessage(), '', true, true);
					}
				}
				//@JAPR

				//GCRUZ 2016-01-19: Agregado el proceso para generar Destinos incrementales desde el Agente de eForms
				//@JAPR 2017-02-16: Agregadas configuraciones de línea de comando para limitar las acciones que el agente puede realizar (#0LC7NT)
				if ($blnEnableIncDataDestinationsProcessing) {
					try {
						logString("Executing IncDataDestinations generation", '', true, true);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Executing IncDataDestinations generation", 1, 1, "color:blue;");
						}
						$blnStatus = executeIncrementalDataDestinations($aBITAMConnection, $eachAgent->repository);
					} catch (Exception $e) {
						logString($e->getMessage(), '', true, true);
					}
				}

				//GCRUZ 2016-06-06: Agregado el proceso para generar cubo de Agendas AM
				//@JAPR 2017-02-16: Agregadas configuraciones de línea de comando para limitar las acciones que el agente puede realizar (#0LC7NT)
				if ($blnEnableAgendaCubeTasksProcessing) {
					try {
						logString("Executing Agenda Cube generation AM", '', true, true);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Executing Agenda Cube generation", 1, 1, "color:blue;");
						}
						$blnStatus = executeAgendaCubeGeneration($aBITAMConnection, $eachAgent->repository, 'AM', efapAgendaCubeGenerationAM);
					} catch (Exception $e) {
						logString($e->getMessage(), '', true, true);
					}
					
					//GCRUZ 2016-06-06: Agregado el proceso para generar cubo de Agendas PM
					try {
						logString("Executing Agenda Cube generation PM", '', true, true);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Executing Agenda Cube generation", 1, 1, "color:blue;");
						}
						$blnStatus = executeAgendaCubeGeneration($aBITAMConnection, $eachAgent->repository, 'PM', efapAgendaCubeGenerationPM);
					} catch (Exception $e) {
						logString($e->getMessage(), '', true, true);
					}
				}
				//@JAPR
			}
			
			// wait a second, to avoid log file overwritting
			sleep(2);
			
			// go to the next agent on the list
			$currentAgentIndex++;
			
			//Ahora se forzará a no iniciar el agente en caso de existir el archivo de seguridad
			//Válvula de seguridad para abortar el agente en caso de que exista este archivo
			clearstatcache();
			$strStopFileName = $strLogPath.BPAEmailConfiguration::stopFile;
			if ( file_exists($strStopFileName) ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n eForms agent ({$strAgentName}) process was aborted by user action");
				}
				logString("eForms agent ({$strAgentName}) process was aborted by user action");
				die();
			}
			
			//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			//Removidas todas refrencias a los procesos, era un esquema de control de concurrencia que tiene años que ya no se utilizaba
		} while ($currentAgentIndex < count($activeAgents));
		
		//@JAPR 2017-02-16: Modificado el agente para no limitar la cantidad de agentes ejecutándose simultáneamente por petición de LRoux, para evitar que por cuestiones externas como IISRest,
		//reinicios del equipo, etc. el contador de agentes corriendo se quedara mal asignado y evitara que nuevas tareas del agente entraran a procesarse. Ahora siempre se regresará 0 para
		//que siempre entre el Agente mientras aún tenga repositorios que procesar (#U4AJ2X)
		//Como indica el comentario de arriba, ya no se verificarán los procesos corriendo, así que es irrelevante esperar el tiempo del ciclo de abajo puesto que este agente ejecutará
		//todos sus repositorios pendientes de principio a fin sin que la actividad de otro posible agente lo interrumpa, por tanto se remueve el sleep para que termine de inmediato la
		//ejecución del agente ya que el ciclo while previo seguramente al no importar los agentes corriendo ya habría terminado de procesar todos los repositorios (#0LC7NT)
		//@JAPR
	}
} catch (Exception $e) {
	logString("eForms Agent unhandled exception: ".$e->getMessage(), '', true, true);
}

logString('Finishing eForms agent');

/* Obtiene la lista de bases de datos que contienen por lo menos alguna tarea pendiente por ejecutar, incluyendo aquellas de servidores diferentes
al default que este agente procesa
*/
function getFBMAgents($aConfiguration)
{
	global $arrDBsEnabledForAgent;
	global $arrDBsExcludedFromAgent;
	global $BITAMRepositories;
	global $strDefaultLogStringFile;
	global $argv;
	global $strAgentName;
	
	if (!isset($arrDBsExcludedFromAgent) || !is_array($arrDBsExcludedFromAgent)) {
		$arrDBsExcludedFromAgent = array();
	}
	
	//@JAPR 2016-01-14: Modificado para que ahora el Agente sólo procese aquellas tareas que pertenecen exclusivamente a repositorios que están configurados en este
	//servicio donde está corriendo el archivo php del Agente, independientemente de si dicho repositorio existe en el array $arrDBsEnabledForAgent, dicho array sólo
	//servirá a partir de ahora para limitar las BDs que sean las únicas que se deban procesar con el Agente, lo cual en v6 es completamente irrelevante ya que todo
	//repositorio debería ser procesado precisamente por el agente, así que con este cambio se podrá dejar $arrDBsEnabledForAgent completamente vacío, sólo llenar
	//$arrDBsExcludedFromAgent con repositorios que por alguna extraña razón se quisieran poner en Stand By (es decir, no procesar sus capturas) y simplemente va a
	//procesar todos los repositorios que se encuentren en el archivo .ini (#WUH8DI)
	//Para facilitar las cosas, simplemente se sobreescribirá internamente el array $arrDBsEnabledForAgent con los repositorios del archivo EKT_cfg.ini, de tal manera
	//que lo configurado manualmente en el archivo eFormsAgentConfig.inc.php será completamente irrelevante, así todos los repositorios del archivo .ini se considerarán
	//válidos para procesar, excepto si están configurados en el array $arrDBsExcludedFromAgent
	logString("Loading Agents list from EKT_cfg.ini file");
	//Primero resetea cualquier posible configuración que hubiera estado indicada en el archivo eFormsAgentConfig.inc.php, ya que son obsoletas
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		if (isset($arrDBsEnabledForAgent)) {
			echo("<br>\r\n arrDBsEnabledForAgent original setting:");
			PrintMultiArray($arrDBsEnabledForAgent);
		}
	}
	
	$arrDBsEnabledForAgent = array();
	//En caso de haberse leído por lo menos un repositorio del archivo .ini, los agrega como repositoriso válidos tal como hubieran estado en el archivo eFormsAgentConfig.inc.php
	if (isset($BITAMRepositories) && is_array($BITAMRepositories)) {
		foreach ($BITAMRepositories as $aRepositoryName => $aRepository) {
			$strAgentRepositoryName = strtoupper((string) $aRepositoryName);
			$arrDBsEnabledForAgent[$strAgentRepositoryName] = 1;
		}
	}
	
	//@JAPR 2017-02-15: Modificado el código para recibir un parámetro con el repositorio exclusivo que esta tarea procesará  (#0LC7NT)
	//Si se recibe este parámetro, entonces se sobreescribe el array de repositorios habilitados para sólo utilizar este repositorio
	logString("Reading script arguments for single repository");
	$blnProcessSingleRepository = false;
	//Línea de comando:											Parámetros:		#1		   #2    #3    #4    #5    #6
	//Ejemplo: pathExe/php.exe pathAgente/eSurveyServiceAgentDaemon.php ["fbm_bmd_####"] [1|0] [1|0] [1|0] [1|0] [1|0]
	/* Formato: 
	[] Significa que es un parámetro opcional, si NO se quiere especificar ese parámetro en esa posición pero si uno posterior, entonces NO se puede omitir, se debe poner un default, por
		ejemplo ["fbm_bmd_####"] significa que pueden opcionalmente especificar un valor string, o no ponerlo, si no lo ponen pero deben poner un parámetro posterior, aunque sea opcional deben
		poner entonces el default adecuado según el tipo de dato esperado en ese parámetro que no mandarán, en este caso "". Si NO especifican el parámtro por completo, es decir, por ejemplo
		especifican hasta el segundo parámetro pero del 3ro en adelante ya no, entonces los parámetros no recibidos tomarán el default indicado
	| Significa que es un valor o el otro, ejemplo: 1|0 significa que pueden poner un valor de 1 o un valor de 0 pero no ambos ni deberían poner un valor diferente
		adecuado en la posición de ese parámetro
	Obligatorio: Ruta del Php.exe correspondiente al utilizado en eForms incluyendo el path completo
	Obligatorio: Ruta del servicio de eForms del agente a ejecutar
	Parámetro #1: Nombre de repositorio en formato fbm_bmd_####, el default adecuado es vacío. Este será el único repositorio que esta tarea procesará, si no se manda se procesarán todos
		los repositorios que estén configurados en el archivo .ini excepto los configurados en el php de configuración para ser excluídos
	Parámetro #2: Ejecutar tareas datos, el default adecuado es 1. Especifica si este agente procesará las tareas de capturas de los repositorios
	Parámetro #3: Ejecutar tareas de DataSources, el default adecuado es 1. Especifica si este agente procesará las cargas programadas de DataSources de los repositorios
	Parámetro #4: Ejecutar tareas de Agendas, el default adecuado es 1. Especifica si este agente procesará la generación programada de Agendas según los planificadores de los repositorios
	Parámetro #5: Ejecutar tareas de Destinos incrementales, el default adecuado es 1. Especifica si este agente procesará el envío incremental de destinos de los repositorios
	Parámetro #6: Ejecutar tareas de generación del cubo de agendas, el default adecuado es 1. Especifica si este agente procesará la generación del cubo de agendas de los repositorios (este 
	proceso se lleva a cabo en dos tiempos AM y PM, el parámetro NO hace diferencia entre los dos, así que ejecutará ambos si se activa o ninguno si se desactiva)
	*/
	if ((isset($argv) && is_array($argv) && count($argv) > 1) || getParamValue('Repository', 'both', '(string)')) {
		//En este caso si recibió al menos un argumento, el primero de los cuales debería ser un string en formato fbm_bmd_####, si no lo es, entonces no realiza este paso y funcionará
		//de manera normal
		$strAgentRepositoryName = getParamValue('Repository', 'both', '(string)');
		if (trim($strAgentRepositoryName) == '') {
			$strAgentRepositoryName = (string) @$argv[1];
		}
		$strAgentRepositoryName = strtoupper($strAgentRepositoryName);
		if (substr($strAgentRepositoryName, 0, 8) == 'FBM_BMD_') {
			//Es una configuración de repositorio válida, así que sobreescribe el array de repositorios
			$arrDBsEnabledForAgent = array();
			$arrDBsEnabledForAgent[$strAgentRepositoryName] = 1;
			$blnProcessSingleRepository = true;
			
			//Sobreescribe el nombre del archivo log del Agente para no mezclar esta sesión con la del agente normal
			$strDefaultLogStringFileTmp = "eFormsAgent_{$strAgentRepositoryName}_{$strAgentName}".date("Ymd").'.log';
			logString("Processing agent in single repository mode, switching log file to {$strDefaultLogStringFileTmp}");
			$strDefaultLogStringFile = "eFormsAgent_{$strAgentRepositoryName}_{$strAgentName}".date("Ymd").'.log';
			logString(str_repeat('*', 80));
			logString('Starting eForms agent in single repository mode');
			
			//En este caso se debe omitir del array de exclusiones a este repositorio dado a que se está forzando su ejecución directa, esto es así para permitir que algunos repositorios
			//tengan un agente particular configurandolos en el array de exclusiones de eFormsAgentConfig.inc.php y forzando a un agente mediante este parámetro nuevo, mientras que el resto 
			//de los repositorios que no están excluídos serían procesados por el agente default
			if (isset($arrDBsExcludedFromAgent[$strAgentRepositoryName])) {
				unset($arrDBsExcludedFromAgent[$strAgentRepositoryName]);
			}
		}
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		if ($blnProcessSingleRepository && isset($arrDBsEnabledForAgent)) {
			echo("<br>\r\n arrDBsEnabledForAgent setting for single repository:");
			PrintMultiArray($arrDBsEnabledForAgent);
			echo("<br>\r\n arrDBsExcludedFromAgent setting for single repository:");
			PrintMultiArray($arrDBsExcludedFromAgent);
		}
	}
	//@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		if (isset($arrDBsEnabledForAgent)) {
			echo("<br>\r\n arrDBsEnabledForAgent from ini file:");
			PrintMultiArray($arrDBsEnabledForAgent);
		}
		if (isset($arrDBsExcludedFromAgent)) {
			echo("<br>\r\n arrDBsExcludedFromAgent from ini file:");
			PrintMultiArray($arrDBsExcludedFromAgent);
		}
	}
	//@JAPR
	
	logString("Retrieving configured eForms Agents list");
	$activeAgents = array();
	
	ob_start();
	try {
		//@JAPR 2013-11-06: Modificado para reutilizar los archivos del Model Manager, los cuales no requieren el password desencriptado
		//@JAPR 2015-01-30: Es indispensable que los archivos cconnection.inc.php y ccreatequery.inc.php NO se encuentren en la carpeta del
		//servicio de eForms si va a estar ejecutandose el agente, de lo contrario dado al cambio mencionado arriba, fallará la conexión pues
		//dichos archivos si requerían el password desencriptado, al eliminarlos se forzará a reutilizar los de Model Manager y funcionará bien
		//$pass = BITAMDecryptPassword($aConfiguration->fbm_db_pwd);
		$pass = $aConfiguration->fbm_db_pwd;
		//@JAPR
		$aBITAMConnection = BITAMConnection::NewInstance($aConfiguration->fbm_db_server, $aConfiguration->fbm_db_user, $pass, "fbm000", 'mysql');
		if (is_null($aBITAMConnection) || is_null($aBITAMConnection->ADOConnection)) {
			$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
			ob_end_clean();
			logString("Unable to connect to '{$aConfiguration->fbm_db_server}': {$strErrorMsg}", '', true, true);
			return false;
		}
	} catch (Exception $e){
		ob_end_clean();
		logString("Unable to connect to '{$aConfiguration->fbm_db_server}'", '', true, true);
		return false;
	}
	ob_end_clean();

	$sqlCREATE = "CREATE TABLE SI_SV_SurveyTasks ( 
			SurveyTaskID INTEGER NOT NULL AUTO_INCREMENT, 
			CreationDateID DATETIME NOT NULL, 
			Repository VARCHAR(50) NOT NULL, 
			SurveyID INTEGER NOT NULL, 
			AgendaID INTEGER NOT NULL,
			UserID VARCHAR(255) NOT NULL, 
			WebMode INTEGER NULL, 
			Latitude DOUBLE NULL, 
			Longitude DOUBLE NULL, 
			SurveyDate DATETIME NULL, 
			TaskStatus INTEGER NULL, 
			TaskStartDate DATETIME NULL, 
			FileName VARCHAR(255) NULL, 
			FormsPath VARCHAR(255) NULL, 
			FormsVersion VARCHAR(50) NULL, 
			AgentID VARCHAR(50) NULL, 
			Executing INT NULL, 
			UserAgent TEXT NULL,
			SourceTaskID INTEGER NULL,
			SavingTries INTEGER NULL,
			SurveyKey INTEGER NULL,
			PRIMARY KEY (SurveyTaskID) 
		) AUTO_INCREMENT=1";
	
	$sqlCREATELog = "CREATE TABLE SI_SV_SurveyTasksLog ( 
			SurveyTaskLogID INTEGER NOT NULL AUTO_INCREMENT, 
			SurveyTaskID INTEGER NOT NULL, 
			CreationDateID DATETIME NOT NULL, 
			Repository VARCHAR(50) NOT NULL, 
			SurveyID INTEGER NOT NULL, 
			AgendaID INTEGER NOT NULL,
			UserID VARCHAR(255) NOT NULL, 
			WebMode INTEGER NULL, 
			Latitude DOUBLE NULL, 
			Longitude DOUBLE NULL, 
			SurveyDate DATETIME NULL, 
			TaskStatus INTEGER NULL, 
			TaskStartDate DATETIME NULL, 
			FileName VARCHAR(255) NULL, 
			FormsPath VARCHAR(255) NULL, 
			FormsVersion VARCHAR(50) NULL, 
			AgentID VARCHAR(50) NULL, 
			LogFileName VARCHAR(255) NULL, 
			UserAgent TEXT NULL,
			SourceTaskID INTEGER NULL,
			SavingTries INTEGER NULL,
			SurveyKey INTEGER NULL,
			PRIMARY KEY (SurveyTaskLogID) 
		) AUTO_INCREMENT=1";
	
	/*
	$sql = "SELECT DISTINCT Repository 
		FROM SI_SV_SurveyTasks 
		WHERE TaskStatus IS NULL OR TaskStatus = ".stsPending." 
		ORDER BY Repository";
	$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
	//$aBITAMConnection->close();
	if ($aRS === false) {
		logString("Error executing query in server '{$aConfiguration->fbm_db_server}': {$sql}. Error: ".@$aBITAMConnection->ADOConnection->ErrorMsg().", query: {$sql}", '', true, true);
		//En caso de error no terminará la ejecución, simplemente creará la tabla de tareas, pero obviamente no hay tareas en este server
		@$aBITAMConnection->ADOConnection->Execute($sqlCREATE);
		@$aBITAMConnection->ADOConnection->Execute($sqlCREATELog);
		//return false;
	}
	else {
		while (!$aRS->EOF) {
			$anArray = array($aConfiguration->fbm_db_server, 80, $aConfiguration->fbm_db_user, $aConfiguration->fbm_db_pwd, 1, $aRS->fields["repository"], 1);
			logString("Adding Agent '{$aConfiguration->fbm_db_server}.{$aRS->fields["repository"]}'");
			$activeAgents[] = eFormsAgent::fromArray($anArray);
			$aRS->MoveNext();
		}
	}
	
	if ($aRS) {
		$aRS->close();
	}
	*/
	
	//checamos las demas conexiones de FBM000 en otros servers que están configuradas como agentes en el archivo de configuraciones del servicio
	//(si no hubiera ningún agente configurado, significaría que este servicio sólo procesaría sus propias tareas, esto permitiría colocar 2 o mas
	//servicios, uno en cada servidor, y así cada servidor podría procesar sus propias tareas optimizando el proceso, ya que de lo contrario las
	//peticiones realmente se estaban realizando sincronamente y un sólo servicio hubiera tardado mucho mas en terminar de resolver las tareas de
	//todos los servidores)
	foreach ($aConfiguration->agents as $eachAgent) {
		$aBITAMConnection = null;
		ob_start();
    	try {
			//@JAPR 2013-11-06: Modificado para reutilizar los archivos del Model Manager, los cuales no requieren el password desencriptado
			//$pass = BITAMDecryptPassword($eachAgent->password);
			$pass = $eachAgent->password;
			//@JAPR
			$aBITAMConnection = BITAMConnection::NewInstance($eachAgent->server, $eachAgent->username, $pass, "fbm000", 'mysql');
			if (is_null($aBITAMConnection) || is_null($aBITAMConnection->ADOConnection)) {
				$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
				ob_end_clean();
				logString("Unable to connect to '{$eachAgent->server}': {$strErrorMsg}", '', true, true);
				return false;
			}
		} catch (Exception $e){
			ob_end_clean();
			logString("Unable to connect to '{$eachAgent->server}'", '', true, true);
      		$aBITAMConnection = NULL;
		}
		ob_end_clean();
    	
		if ($aBITAMConnection) {
			logString("Reading pending task from server '{$eachAgent->server}'");
			//@JAPR 2015-01-30: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
			$sql = "SELECT DISTINCT Repository 
				FROM SI_SV_SurveyTasks 
				WHERE TaskStatus IS NULL OR TaskStatus IN (".stsPending.",".stsRescheduled.",".stsManualUpload.") 
				ORDER BY Repository";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			//$aBITAMConnection->close();
			if ($aRS === false) {
				logString("Error executing query in server '{$eachAgent->server}': {$sql}. Error: ".@$aBITAMConnection->ADOConnection->ErrorMsg().", query: {$sql}", '', true, true);
				//En caso de error no terminará la ejecución, simplemente creará la tabla de tareas, pero obviamente no hay tareas en este server
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n sql: {$sqlCREATE}");
				}
				$aBITAMConnection->ADOConnection->Execute($sqlCREATE);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n sql: {$sqlCREATELog}");
				}
				$aBITAMConnection->ADOConnection->Execute($sqlCREATELog);
			}
			
			//@JAPR 2018-02-01: Debido a un incidente reciente, LRoux indicó que ya no deberían realizarse ALTER TABLEs continuos para evitar Table locks, así que se
			//comentarán todos los siguientes estatutos bajo el entendido que la única tabla de metadata de KPIOnline tendría siempre que estar al día manualmente,
			//de cualquier manera es buena idea seguir agregando los ALTERs en este bloque como un histórico de los cambios sufridos en el Agente de eForms
			/*//@JAPR 2015-02-03: Independientemente de si hubo o no error en la consulta, se intenta actualizar la metadata por si le hiciera
			//falta, mas abajo se validará si puede o no continuar con el proceso normal
			//@JAPR 2013-11-22: Agregada la información del dispositivo que realiza la petición
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD UserAgent TEXT NULL";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
			
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD UserAgent TEXT NULL";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
			
			//@JAPR 2015-02-03: Agregado el botón para recalendarizar las tareas del agente que hubieran fallado
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD SourceTaskID INTEGER NULL";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
			
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD SourceTaskID INTEGER NULL";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
			
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD SavingTries INTEGER NULL";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
			
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD SavingTries INTEGER NULL";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
			//@JAPR

			//@AAL 03/07/2015 Agregada la columna SessionID para EformsV6 y siguientes
			//A traves de este campo se agrupa y obtiene la duración de sincronización (Max(CreationDateID) - Min(CreationDateID))
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD SessionID VARCHAR(60) NULL";
			if (getParamValue('DebugBreak', 'both', '(string)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
			
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD SessionID VARCHAR(60) NULL";
			if (getParamValue('DebugBreak', 'both', '(string)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
			
			//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD DeviceName VARCHAR(60) NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD DeviceName VARCHAR(60) NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD UDID VARCHAR(100) NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD UDID VARCHAR(100) NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD Accuracy INTEGER NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD Accuracy INTEGER NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD DeviceID INTEGER NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD DeviceID INTEGER NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			//@JAPR
			//Campo para guardar el id del destino que falló
			$sql = "ALTER TABLE SI_SV_SurveyDestinationLog ADD DestinationID INTEGER NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);

			//Campo para guardar el status de procesamiento
			//Cuando el destino que falló no se ha reprocesado el campo ProcessStatus tendrá valor null
			//Cuando el destino que falló ya se mandó a reprocesar el campo ProcessStatus tendrá 1, independientemente si el
			//resultado fue exitoso o no
			$sql = "ALTER TABLE SI_SV_SurveyDestinationLog ADD ProcessStatus INTEGER NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			
			//DestinationIDs en SI_SV_SurveyTasks se utiliza para guardar cuales destinos serán procesados,
			//esto es en el caso en que la tarea haya sido agregada a la tabla SI_SV_SurveyTasks debido a que desde la pantalla de destinos
			//se seleccinaron destinos fallidos para reprocesarse, de esta manera sólo se procesarán los destinos especificados en el campo DestinationIDs 
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD DestinationIDs TEXT";
			$aBITAMConnection->ADOConnection->Execute($sql);

			//Reprocess en SI_SV_SurveyTasks tendrá el valor de 1 cuando la tarea haya sido agregaga a la tabla debido al reproceso manual de destinos
			//desde la pantalla de Bítacora de destinos
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD Reprocess INTEGER";
			$aBITAMConnection->ADOConnection->Execute($sql);
			
			//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
			//SurveyKey en SI_SV_SurveyTasks contiene el valor del Id de la captura, mientras que SurveyId contiene el valor del Id de la forma, este nuevo campo
			//SurveyKey es aplicable y servirá como join entre la tabla de tareas, log de tareas y log de destinos de datos
			$sql = "ALTER TABLE SI_SV_SurveyTasks ADD SurveyKey INTEGER NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD SurveyKey INTEGER NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			$sql = "ALTER TABLE SI_SV_SurveyDestinationLog ADD SurveyKey INTEGER NULL";
			$aBITAMConnection->ADOConnection->Execute($sql);
			//@JAPR

			//DestinationIDs en SI_SV_SurveyTasks se utiliza para guardar cuales destinos serán procesados,
			//esto es en el caso en que la tarea haya sido agregada a la tabla SI_SV_SurveyTasks debido a que desde la pantalla de destinos
			//se seleccinaron destinos fallidos para reprocesarse, de esta manera sólo se procesarán los destinos especificados en el campo DestinationIDs 
			//DestinationIDs  también se graba en SI_SV_SurveyTasksLog
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD DestinationIDs TEXT";
			$aBITAMConnection->ADOConnection->Execute($sql);

			//Reprocess en SI_SV_SurveyTasks tendrá el valor de 1 cuando la tarea haya sido agregaga a la tabla debido al reproceso manual de destinos
			//desde la pantalla de Bítacora de destinos, este valor también de graba en SI_SV_SurveyTasksLog
			$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD Reprocess INTEGER";
			$aBITAMConnection->ADOConnection->Execute($sql);
			*/
			//@JAPR
			
			if ($aRS !== false) {
				//@JAPR 2013-07-11: Ahora reutilizará las variables globales de configuración para determinar que repositorios debe procesar
				//este Agente, por si se configuran múltiples agentes en diferentes server, así cada server procesará sólo las cuentas que
				//si graban sus datos en él
				/*if (!isset($arrDBsExcludedFromAgent) || !is_array($arrDBsExcludedFromAgent)) {
					$arrDBsExcludedFromAgent = array();
				}*/
				
				if (!isset($arrDBsEnabledForAgent) || !is_array($arrDBsEnabledForAgent)) {
					$arrDBsEnabledForAgent = array();
				}
				//@JAPR 2015-09-07: Agregado el procesamiento de DataSources al agente, ahora independientemente de si tiene o no tareas que procesar, deberá incluir el repositorio para
				//procesar los DataSources
				$arrAgentsIncluded = array();
				//@JAPR
				
				while (!$aRS->EOF) {
					//@JAPR 2013-07-11: Ahora reutilizará las variables globales de configuración para determinar que repositorios debe procesar
					//este Agente, por si se configuran múltiples agentes en diferentes server, así cada server procesará sólo las cuentas que
					//si graban sus datos en él
					$blnAddAgent = true;
					$strAgentRepositoryName = strtoupper((string) @$aRS->fields["repository"]);
					if (isset($arrDBsExcludedFromAgent[$strAgentRepositoryName])) {
						//Se debe excluir este repositorio
						$blnAddAgent = false;
					}
					
					//@JAPR 2016-01-14: Modificado para que ahora el Agente sólo procese aquellas tareas que pertenecen exclusivamente a repositorios que están configurados en este
					//servicio donde está corriendo el archivo php del Agente, independientemente de si dicho repositorio existe en el array $arrDBsEnabledForAgent, dicho array sólo
					//servirá a partir de ahora para limitar las BDs que sean las únicas que se deban procesar con el Agente, lo cual en v6 es completamente irrelevante ya que todo
					//repositorio debería ser procesado precisamente por el agente, así que con este cambio se podrá dejar $arrDBsEnabledForAgent completamente vacío, sólo llenar
					//$arrDBsExcludedFromAgent con repositorios que por alguna extraña razón se quisieran poner en Stand By (es decir, no procesar sus capturas) y simplemente va a
					//procesar todos los repositorios que se encuentren en el archivo .ini (#WUH8DI)
					//Para facilitar las cosas, simplemente se sobreescribirá internamente el array $arrDBsEnabledForAgent con los repositorios del archivo EKT_cfg.ini, de tal manera
					//que lo configurado manualmente en el archivo eFormsAgentConfig.inc.php será completamente irrelevante, así todos los repositorios del archivo .ini se considerarán
					//válidos para procesar, excepto si están configurados en el array $arrDBsExcludedFromAgent
					if ($blnAddAgent) {
						if (count($arrDBsEnabledForAgent) > 0) {
							if (!isset($arrDBsEnabledForAgent[$strAgentRepositoryName])) {
								$blnAddAgent = false;
							}
						}
						else {
							//Ahora en caso de no existir repositorios configurados, NO se van a procesar con este agente las tareas
							$blnAddAgent = false;
						}
					}
					//@JAPR
					
					//Si no se debe agregar este agente, incrementa el recordset y salta este repositorio
					if (!$blnAddAgent) {
						$aRS->MoveNext();
						continue;
					}
					//@JAPR
					
					$anArray = array($eachAgent->server, 80, $eachAgent->username, $pass, 1, $aRS->fields["repository"], 1);
					$activeAgents[] = eFormsAgent::fromArray($anArray);
					//@JAPR 2015-09-07: Agregado el procesamiento de DataSources al agente, ahora independientemente de si tiene o no tareas que procesar, deberá incluir el repositorio para
					//procesar los DataSources
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Adding agent processed {$strAgentRepositoryName}", 1, 1, "color:blue;");
					}
					$arrAgentsIncluded[$strAgentRepositoryName] = $strAgentRepositoryName;
					//@JAPR
					$aRS->MoveNext();
				}
				
				if ($aRS) {
					$aRS->close();
				}
				
				//@JAPR 2015-09-07: Agregado el procesamiento de DataSources al agente, ahora independientemente de si tiene o no tareas que procesar, deberá incluir el repositorio para
				//procesar los DataSources
				//Si no se había agregado este repositorio por sus tareas, lo agrega por sus DataSources
				foreach ($arrDBsEnabledForAgent as $strAgentRepositoryName => $intActive) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Checking agent processed {$strAgentRepositoryName}", 1, 1, "color:blue;");
					}
					
					//@JAPR 2017-02-16: Corregido un bug, a pesar de que se hubieran configurado como excluídos los repositorios, cuando se implementaron las taresa de DataSources y demás
					//se habían agregado como repositorios válidos así que ignoraba el array de exclusiones (#0LC7NT)
					if (!isset($arrAgentsIncluded[$strAgentRepositoryName]) && !isset($arrDBsExcludedFromAgent[$strAgentRepositoryName])) {
						$anArray = array($eachAgent->server, 80, $eachAgent->username, $pass, 1, strtolower($strAgentRepositoryName), 1);
						$activeAgents[] = eFormsAgent::fromArray($anArray);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Adding new agent to process {$strAgentRepositoryName}", 1, 1, "color:blue;");
						}
					}
				}
				//@JAPR
			}
		}
	}
	
	if (count($activeAgents) == 0) {
		logString("There were no pending tasks found");
	}
	else {
		logString("There were ".count($activeAgents)." repositories found with pending tasks to process");
	}
	
	return $activeAgents;
}

/* Dada una conexión a una base de datos FBM000 y los datos de un agente, obtiene la siguiente clave de tarea aun no tomada en dicho repositorio,
para lo cual primero hace un UPDATE de la tabla utilizando el ID de sesión de este agente para permitirle reutilizar sólo tareas libres apartandola
para que algún otro agente no la tome al mismo tiempo
*/
function getNextTaskKeyToProcess($aBITAMConnection, $anAgent) {
	global $strSessionID;
	
	if (is_null($anAgent) || !is_object($anAgent)) {
		return 0;
	}
	
	logString("Adquiring a survey task to process in server '{$anAgent->server}': ".@$aBITAMConnection->ADOConnection->ErrorMsg());
	$intNumChecks = 0;
	$intMaxFreeTasksCheck = 10;
	$blnContinueCheck = true;
	$intSurveyTaskID = 0;
	$strAgentID = $aBITAMConnection->ADOConnection->Quote($strSessionID);
	$strRepository = $aBITAMConnection->ADOConnection->Quote($anAgent->repository);
	
	//Primero aparta una tarea para este agente, si no hubo registros afectados quiere decir que no hay tareas pendientes que no estén siendo
	//procesadas por lo que ya no es necesario
	while ($blnContinueCheck) {
		//@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
		//Ahora hay tareas configuradas que contienen un Executing == stsRescheduled que no son seleccionadas por este proceso, sin embargo
		//esas tareas tienen una fecha a partir de la cual se convierten en válidas, así que primero se cambia el Executing de esas tareas
		//sin importar que agente lo hizo, de tal manera que el proceso de identificación de tareas pendientes las pueda tomar, pero sólo si
		//la fecha actual ya es posterior a la mínima configurada para ejecutarla
		$currDate = date('Y-m-d H:i:s');
		$sql = "UPDATE SI_SV_SurveyTasks 
  				SET Executing = ".stsPending." 
			WHERE Repository = {$strRepository} AND TaskStatus = ".stsRescheduled." AND Executing = ".stsRescheduled." 
				AND TaskStartDate <= ".$aBITAMConnection->ADOConnection->DBTimeStamp($currDate);
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: {$sql}");
		}
		$aBITAMConnection->ADOConnection->Execute($sql);
		
		//@JAPR 2015-01-30: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
		$sql = "UPDATE SI_SV_SurveyTasks 
  				SET AgentID = {$strAgentID}, Executing = ".stsActive." 
			WHERE Repository = {$strRepository} AND (TaskStatus IS NULL OR TaskStatus IN (".stsPending.",".stsRescheduled.",".stsManualUpload.")) AND 
				(Executing IS NULL OR Executing = ".stsPending.") 
			LIMIT 1";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: {$sql}");
		}
		if ($aBITAMConnection->ADOConnection->Execute($sql)) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n Done");
			}
			$intAffectedRows = (int) @$aBITAMConnection->ADOConnection->_affectedrows();
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n Affected rows: {$intAffectedRows}");
			}
			
			if ($intAffectedRows > 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n Verifying task".str_repeat(' ', 2049));
				}
				//Hay por lo menos una tarea libre, así que ahora verifica que finalmente sea este el agente que la apartó, ya que si en el mismo
				//instante 2 o mas agentes hubieran hecho un UPDATE a este registro, sólo uno de ellos finalmente tendría asignado el AgentID y
				//debe verificar que sea este antes de usar esta tarea, si no hubiera alguna tarea apartada (Excuting) pero no procesada 
				//(TaskStatus == 0) que tenga asignado este AgentID, entonces se debe repetir el proceso
				//@JAPR 2015-01-30: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
				//@JAPR 2015-10-14: Corregido un bug, aunque se apartaba correctamente la tarea por parte de un agente, si hubieran entrado dos agentes (incluso de respositorios diferentes)
				//en el mismo instante a apartar una tarea, este SELECT no estaba consultando sólo las tareas del repositorio procesado por el agente, así que se pudo haber confundido
				//ejecutando una tarea de un repositorio que no le correspondía, específicamente en el caso de error reportado, el agente de producción tomó al mismo tiempo una tarea que
				//el agente de testing había apartado, porque el agente no verificaba por el repositorio, si lo hubiera hecho, producción no la habría tomado (#NQ995V)
				$sql = "SELECT SurveyTaskID 
					FROM SI_SV_SurveyTasks 
					WHERE Repository = {$strRepository} AND Executing = ".stsActive." AND (TaskStatus IS NULL OR TaskStatus IN (".stsPending.",".stsRescheduled.",".stsManualUpload.")) 
						AND AgentID = {$strAgentID}";
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n sql: {$sql}");
				}
				$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
				if ($aRS) {
					if (!$aRS->EOF) {
						//Si encontró la tarea que este servicio actualizó, así que regresa su ID para procesarla
						//Sólo debería haber un registro en este query, aunque si hubiera mas de uno de todas formas toma el primero
						$intSurveyTaskID = (int) @$aRS->fields["surveytaskid"];
						$blnContinueCheck = false;
					}
					else {
						//Si no se hubiera podido recuperar ningún registro, entonces quiere decir que alguien mas tomó la tarea que teóricamente
						//se había apartado, así que repite el proceso hasta "n" veces
						$intNumChecks++;
						if ($intNumChecks > $intMaxFreeTasksCheck) {
							$blnContinueCheck = false;
							break;
						}
					}
				}
				else {
					$blnContinueCheck = false;
					logString("Error adquiring a survey task to process in server '{$anAgent->server}': ".@$aBITAMConnection->ADOConnection->ErrorMsg(), '', true, true);
				}
			}
			else {
				//En este caso no se pudo actualizar ningún registro, así que no hay tareas libres por lo que ya no continua identificando alguna,
				//sin embargo verifica si hubiera algún tipo de error y previamente había quedado alguna tarea apartada sin terminar, para en 
				//dicho caso procesar esa tarea
				//@JAPRWarning: Por el momento no se ve como este caso sería posible, ya que los agentes usan como Sesión un TimeStampt así que
				//un agente que interrumpió abruptamente una carga en otra sesión no podría continuar en una sesión posterior
				$blnContinueCheck = false;
			}
		}
		else {
			//Si no se pudo ejecutar el UPDATE, entonces cancela el proceso de ejecución
			$blnContinueCheck = false;
			logString("Error adquiring a survey task to process: ".@$aBITAMConnection->ADOConnection->ErrorMsg(), '', true, true);
		}
	}
	
	if ($intSurveyTaskID > 0) {
		logString("Survey task ID : {$intSurveyTaskID}");
	}
	return $intSurveyTaskID;
}

//@JAPR 2015-10-27: Agregado el proceso para generar todas las Agendas desde el Agente de eForms
/* Obtiene el valor de una configuración global, en caso de no existir la tabla o la configuración, simplemente regresará null
Si no se especifica el parámetro $sRepositoryName se asumirá que se quiere la configuración global para todos, ya que esta tabla por encontrarse en el repositorio de KPIOnline FBM000,
requiere una referencia al repositorio que está procesando el Agente para obtener la configuración que aplica sólo a él
*/
//GCRUZ 2016-01-19. Agregado parámetro bRepositoryConnection, indicando que la conexión enviada YA es sobre el repositorio y no es necesario incluirlo en el query.
function GetGlobalSettingValueByName($aBITAMConnection, $sSettingName, $sRepositoryName = null, $bRepositoryConnection = false) {
	$strRepositoryFilter = " AND (Repository = ".$aBITAMConnection->ADOConnection->Quote('')." OR Repository IS NULL)";
	if (!is_null($sRepositoryName)) {
		$strRepositoryFilter = " AND Repository = ".$aBITAMConnection->ADOConnection->Quote(strtoupper($sRepositoryName));
	}
	if ($bRepositoryConnection)
	{
		$strRepositoryFilter = '';
	}
	
	$strSettingValue = null;
	$sql = "SELECT SettingID, SettingName, SettingValue 
		FROM SI_SV_Settings 
		WHERE SettingName = ".$aBITAMConnection->ADOConnection->Quote($sSettingName).$strRepositoryFilter;
	$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$strSettingValue = @$aRS->fields["settingvalue"];
	}
	
	return $strSettingValue;
}

/* Ejecuta la generación de todas las Agendas del día para todos los usuarios que tengan un Scheduler de Agendas configurado. Además generará las agendas del día siguiente si
los Schedulers se encuentran configurados para ello
El proceso sólo se ejecutará una vez al día por repositorio según la configuración global para ello, aunque se puede invocar directamente con la URL adecuada (la tabla de configuraciones
se encuentra en el repositorio de KPIOnline, tiene la misma estructura y utiliza los mismos IDs que la tabla de configuraciones normal de eForms, pero NO es la misma, ya que la de eForms
se encuentra en cada DWH)
*/
function executeAgendasGeneration($aBITAMConnection, $sRepositoryName) {
	global $aConfiguration;
	global $intRequestNumber;
	global $strDefaultLogStringFile;
	global $strDefaultDetailLogStringFile;
	global $blnDebugAgentMode;
	global $strAgentName;
	
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	$blnMultiPass = GetMultiPasswordEnabledState($aBITAMConnection, $sRepositoryName);
	//@JAPR
	
	//@JAPR 2019-01-30: Debido a un incidente reciente, LRoux indicó que ya no deberían realizarse ALTER TABLEs continuos para evitar Table locks, así que se
	//comentarán todos los siguientes estatutos bajo el entendido que la única tabla de metadata de KPIOnline tendría siempre que estar al día manualmente,
	//de cualquier manera es buena idea seguir agregando los ALTERs en este bloque como un histórico de los cambios sufridos en el Agente de eForms (#BBGFMH)
	//Primero verifica si debe o no ejecutar la tarea según la configuración global existente y si ya se había o no ejecutado el día actual
	/*$sql = "CREATE TABLE SI_SV_ProcessLog ( 
			ProcessLogID INTEGER NOT NULL AUTO_INCREMENT, 
			ProcessID INTEGER NOT NULL,
			CreationDateID DATETIME NOT NULL, 
			Repository VARCHAR(50) NOT NULL, 
			TaskStatus INTEGER NULL, 
			LogFileName VARCHAR(255) NULL, 
			PRIMARY KEY (ProcessLogID) 
		) AUTO_INCREMENT=1";
	$aBITAMConnection->ADOConnection->Execute($sql);*/
	//@JAPR
	
	$intAgendasStartTime = (int) @GetGlobalSettingValueByName($aBITAMConnection, "AUTOAGENDASGENERATIONTIME", $sRepositoryName);
	//@JAPR 2015-11-03: Corregido un bug, cuando la configuración se leía por repositorio, no estaba considerando al valor -1 (#MO8TBT)
	if ($intAgendasStartTime <= 0 && $intAgendasStartTime != -1) {
		$intAgendasStartTime = (int) @GetGlobalSettingValueByName($aBITAMConnection, "AUTOAGENDASGENERATIONTIME");
		if ($intAgendasStartTime <= 0 && $intAgendasStartTime != -1) {
			$intAgendasStartTime = 2200;
		}
	}
	
	$blnProcessed = false;
	$currDate = date('Y-m-d');
	//Si se configura la hora de ejecución con un valor de -1, quiere decir que todo el día se deberán generar agendas, así que no validará si ya se había o no procesado
	if ($intAgendasStartTime != -1) {
		$sql = "SELECT ProcessLogID, CreationDateID 
			FROM SI_SV_ProcessLog 
			WHERE ProcessID = ".efapAgendas." AND TaskStatus = ".stsCompleted." AND Repository = ".$aBITAMConnection->ADOConnection->Quote($sRepositoryName).
				" AND CreationDateID BETWEEN ".$aBITAMConnection->ADOConnection->DBTimeStamp($currDate." 00:00:00").
				" AND ".$aBITAMConnection->ADOConnection->DBTimeStamp($currDate." 23:59:59")." 
			ORDER BY CreationDateID";
		$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$blnProcessed = true;
		}
		
		//Si ya se había ejecutado la tarea, ya no la volverá a repetir el mismo día
		if ($blnProcessed) {
			logString("Agenda task was already processed today", '', true, true);
			return false;
		}
	}
	
	//Si no ha llegado la hora de ejecución de las tareas, no puede continuar con el proceso, una hora de -1 es un comodín para ejecutar el proceso durante todo el día
	if ($intAgendasStartTime > 0) {
		$intCurrentTime = (int) date('Hi');
		if ($intCurrentTime < $intAgendasStartTime) {
			logString("The execution time for the Agendas task has not been reached, the next run should be at {$intAgendasStartTime} hours", '', true, true);
			return false;
		}
	}
	
	//En este caso la tarea si es válida para procesar, así que prepara el request al servicio de eForms correspondiente para que se generen las Agendas necesarias, al finalizar
	//marcará la tarea como procesada
	$intRequestNumber++;
	$intConnectionTimeOut = $intRequestTimeOut = (int) $aConfiguration->execution_timeout;
	if ($intConnectionTimeOut < 0) {
		$intConnectionTimeOut = 0;
	}
	if ($intRequestTimeOut <= 0) {
		$intRequestTimeOut = 600;
	}
	
	$strDebugFileName = '';
	if (((string) @$strDefaultDetailLogStringFile) != '') {
		$strDebugFileName = $strDefaultDetailLogStringFile;
	}
	if (trim($strDebugFileName) == '') {
		$strDebugFileName = "eFormsAgent.log";
	}
	$strDebugFileName = str_ireplace('.log', '_'.$intRequestNumber.'.log', $strDebugFileName);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Agendas DebugFilename {$strDebugFileName}", 1, 1, "color:blue;");
	}
	
	$kpiUser = '';
	$kpiPass = '';
	$streFormsService = '';
	//Obtiene los parámetros de conexión directo del repositorio de KPIOnline, ya que sólo se cuenta con el nombre del repositorio
	$sql = "SELECT B.Email, B.AccPassword, A.KPIServer, A.ArtusPath, A.FormsPath 
		FROM saas_databases A 
			INNER JOIN saas_users B ON A.UserID = B.UserID 
		WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote($sRepositoryName);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Agendas Query {$sql}", 1, 1, "color:blue;");
	}
	$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$kpiUser = (string) @$aRS->fields["email"];
		$kpiPass = BITAMDecryptPassword((string) @$aRS->fields["accpassword"]);
		$strKPIServer = (string) @$aRS->fields["kpiserver"];
		$strArtusPath = (string) @$aRS->fields["artuspath"];
		$strArtusPath = str_ireplace("\\", "/", $strArtusPath);
		$strFormsPath = (string) @$aRS->fields["formspath"];
		$streFormsService = "http://{$strKPIServer}/{$strArtusPath}/{$strFormsPath}/";
	}
	else {
		logString("Unable to retrieve the account data, no repository was found '{$sRepositoryName}': ".$aBITAMConnection->ADOConnection->ErrorMsg(), '', true, true);
		return false;
	}
	
	if (trim($kpiUser) == '') {
		logString("Unable to retrieve the account data, no user admin account was found '{$sRepositoryName}': ", '', true, true);
		return false;
	}
	
	$intTaskStatus = stsFailed;
	$strProjectName = '';
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Ahora se comenzará a enviar el ProjectName ya que en base a eso se complementará la validación del password múltiple por repositorio, además sobreescribirá el password del usuario
	//con el específico configurado para este repositorio en caso de ya estar habilitada la funcionalidad
	if ($blnMultiPass) {
		$strProjectName = $sRepositoryName;
		$strkpiPass = GetMultiPasswordForRepository($aBITAMConnection, $sRepositoryName, $blnMultiPass);
		if (!is_null($strkpiPass) && $strkpiPass) {
			$kpiPass = $strkpiPass;
		}
	}
	//@JAPR
	
	$timeStamp = date('YmdHis');
	//@JAPR 2015-09-07: Validado que si el servicio viene como https, se cambie automáticamente a http
	$intPos = stripos($streFormsService, "https://");
	if ($intPos !== false) {
		$streFormsService = str_ireplace("https://", "http://", $streFormsService);
	}
	//@JAPR 2018-11-07: Corregido un bug, la comparación con "=" en MySQL sobre campos Varchar ignora los espacios en blanco al final, así que EMails que los tenían se dejaban pasar por
	//el proceso de login y terminaban provocando errores al procesar sus capturas al formarse URLs en el Agente sin escaparlos debidamente (#ZA1GUB)
	//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strURL = "{$streFormsService}eSurveyService.php?".
		"UserID=".rawurlencode($kpiUser)."&Password=".rawurlencode($kpiPass)."&projectName=".rawurlencode($strProjectName).
		"&action=syncagendas".
		"&isagent=1&DebugBreak=1&FAgentName=".$strAgentName;
	//@JAPR
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Agendas URL {$strURL}", 1, 1, "color:blue;");
	}
	logString("HTTP Request: ".$strURL);
	try {
		$ch = curl_init();
		if ($ch === false) {
			logString("There was an error starting the curl session", '', true, true);
			return false;
		}
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $intConnectionTimeOut);
		curl_setopt($ch, CURLOPT_TIMEOUT, $intRequestTimeOut);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_URL, $strURL);
		
		$strResult = curl_exec($ch);   
		$intLastCURLError = 0;
		$strLastCURLError = '';
		$strErrorDesc = '';
		if ($strResult === false) {
			$strErrorDesc = curl_error($ch);
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Agendas error {$strErrorDesc}", 1, 1, "color:red;");
			}
			logString("Error executing the Agendas task: $strErrorDesc", '', true, true);
			/* Errores comunes:
			- Error executing the Agendas task: Failed to open/read local data from file/application
				Esto sucede cuando la URL solicitada no existe
				http://php.net/manual/en/function.curl-errno.php
				http://php.net/manual/en/function.curl-getinfo.php
			*/
		}
		else {
			//Antes de liberar los recursos obtiene la información del último error detectado, esto cuando el error no es un HTTP 400+, ya que en ese
			//caso hubiera entrado por el If
			$intLastCURLError = @curl_errno($ch);
			$strLastCURLError = @curl_error($ch);
		}
		curl_close($ch);
		
		//Si no hubo error en el request, verifica si hubo error durante el grabado
		$strStatus = 'Agenda Schedulers were processed successfully.';
		if ($strResult !== false) {
			//@JAPR 2018-02-07: Agregado el archivo de configuraciones semi-dinámico del agente para permitir variar su comportamiento sin tener que apagar la tarea de windows
			//Se podrán asignar configuraciones forzadas modificando este archivo para no tener que cambiar el código y que por servicio se puedan hacer ajustes. Originalmente
			//utilizado para activar el modo de depuración ($blnDebugAgentMode) (#E5YASV)
			if ($blnDebugAgentMode) {
				logString("DebugFileName: {$strDebugFileName}");
			}
			//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			//Esta llamada reasignará el path completo hacia el repositorio para almacenar ahí el archivo de log del procesamiento de la tarea
			GeteFormsLogPath($sRepositoryName);
			//@JAPR
			$blnOk = stripos($strResult, $strStatus);
			if ($blnOk === false) {
				$strErrorDesc = "Error executing the Agendas task , Error num: {$intLastCURLError}, Error: '{$strLastCURLError}'. Check the error detail & debug information in this file: $strDebugFileName";
				//En este caso hubo un error y no se logró concretar el grabado de la encuesta (errores por eBavel no se reportarían, ya que esos
				//no impiden el grabado de la encuesta, en esos casos se deberá consultar el log de upload de la misma)
				logString($strResult, $strDebugFileName, true, true, false);
				//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
				GeteFormsLogPath('');
				//@JAPR
				logString($strErrorDesc, '', true, true);
			}
			else {
				if ($blnDebugAgentMode) {
					logString($strResult, $strDebugFileName, true, true, false);
				}
				//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
				GeteFormsLogPath('');
				//@JAPR
				
				//Todo estuvo bien
				logString($strStatus);
				$intTaskStatus = stsCompleted;
			}
		}
	} catch (Exception $e) {
		logString("Error executing the Agendas task (Exception): ".$e->getMessage(), '', true, true);
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n El resultado es: <br>\r\n".$strResult);
	}
	
	//Agrega un registro a la bitácora de procesos para que ya no se repita este día nuevamente
	$currDate = $aBITAMConnection->ADOConnection->DBTimeStamp(date('Y-m-d H:i:s'));
	$sql = "INSERT INTO SI_SV_ProcessLog (ProcessID, CreationDateID, Repository, TaskStatus, LogFileName) 
		VALUES (".efapAgendas.", {$currDate}, ".
			$aBITAMConnection->ADOConnection->Quote($sRepositoryName).", {$intTaskStatus}, ".
			$aBITAMConnection->ADOConnection->Quote($strDebugFileName).")";
	$aBITAMConnection->ADOConnection->Execute($sql);
	
	return true;
}
//@JAPR

//GCRUZ 2016-01-19. Agregado procesamiento incremental de Destinos de Datos
function executeIncrementalDataDestinations($aBITAMConnection, $sRepositoryName)
{
	global $aConfiguration;
	global $intRequestNumber;
	global $strDefaultLogStringFile;
	global $strDefaultDetailLogStringFile;
	global $blnDebugAgentMode;
	global $strAgentName;
	
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	$blnMultiPass = GetMultiPasswordEnabledState($aBITAMConnection, $sRepositoryName);
	//@JAPR
	
	//@JAPR 2019-01-30: Debido a un incidente reciente, LRoux indicó que ya no deberían realizarse ALTER TABLEs continuos para evitar Table locks, así que se
	//comentarán todos los siguientes estatutos bajo el entendido que la única tabla de metadata de KPIOnline tendría siempre que estar al día manualmente,
	//de cualquier manera es buena idea seguir agregando los ALTERs en este bloque como un histórico de los cambios sufridos en el Agente de eForms (#BBGFMH)
	//Primero verifica si debe o no ejecutar la tarea según la configuración global existente y si ya se había o no ejecutado el día actual
	/*$sql = "CREATE TABLE SI_SV_ProcessLog ( 
			ProcessLogID INTEGER NOT NULL AUTO_INCREMENT, 
			ProcessID INTEGER NOT NULL,
			CreationDateID DATETIME NOT NULL, 
			Repository VARCHAR(50) NOT NULL, 
			TaskStatus INTEGER NULL, 
			LogFileName VARCHAR(255) NULL, 
			PRIMARY KEY (ProcessLogID) 
		) AUTO_INCREMENT=1";
	$aBITAMConnection->ADOConnection->Execute($sql);*/
	//@JAPR

	//Conectarse al repositorio para obtener la información de la periodicidad de los destinos de datos
	$kpiUser = '';
	$kpiPass = '';
	$streFormsService = '';
	//Obtiene los parámetros de conexión directo del repositorio de KPIOnline, ya que sólo se cuenta con el nombre del repositorio
	$sql = "SELECT B.Email, B.AccPassword, A.KPIServer, A.ArtusPath, A.FormsPath, A.DBServer, A.DBServerUsr, A.DBServerPwd, A.DataWarehouse 
		FROM saas_databases A 
			INNER JOIN saas_users B ON A.UserID = B.UserID 
		WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote($sRepositoryName);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("IncDataDestinations Query {$sql}", 1, 1, "color:blue;");
	}
	$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$kpiUser = (string) @$aRS->fields["email"];
		$kpiPass = BITAMDecryptPassword((string) @$aRS->fields["accpassword"]);
		$strKPIServer = (string) @$aRS->fields["kpiserver"];
		$strArtusPath = (string) @$aRS->fields["artuspath"];
		$strArtusPath = str_ireplace("\\", "/", $strArtusPath);
		$strFormsPath = (string) @$aRS->fields["formspath"];
		//GCRUZ Test Only!
//		$strFormsPath = 'EsurveyV6GC';
		$streFormsService = "http://{$strKPIServer}/{$strArtusPath}/{$strFormsPath}/";
		$strDBServer = (string) @$aRS->fields["dbserver"];
		$strDBServerUsr = (string) @$aRS->fields["dbserverusr"];
		$strDBServerPwd = (string) @$aRS->fields["dbserverpwd"];
		$strDataWarehouse = (string) @$aRS->fields["datawarehouse"];
	}
	else {
		logString("Unable to retrieve the account data, no repository was found '{$sRepositoryName}': ".$aBITAMConnection->ADOConnection->ErrorMsg(), '', true, true);
		return false;
	}
	
	if (trim($kpiUser) == '') {
		logString("Unable to retrieve the account data, no user admin account was found '{$sRepositoryName}': ", '', true, true);
		return false;
	}

	$aRepositoryConnection = null;
	ob_start();
	try {
		//Cierra la conexión anterior si era de un server diferente
		if (!is_null($aRepositoryConnection)) {
			@$aRepositoryConnection->close();
			$aBITAMConnection = null;
		}
		
		if (is_null($aRepositoryConnection)) {
			//@JAPR 2013-11-06: Modificado para reutilizar los archivos del Model Manager, los cuales no requieren el password desencriptado
			//Este password ya llega desencriptado en este punto
			//Ahora este password llega encriptado, pero así es como lo requiere BITAMConnection cuando se usa desde el
			//Model Manager
			$pass = $strDBServerPwd;
			//@JAPR
			$aRepositoryConnection = BITAMConnection::NewInstance($strDBServer, $strDBServerUsr, $strDBServerPwd, $strDataWarehouse, 'mysql');
			if (is_null($aRepositoryConnection) || is_null($aRepositoryConnection->ADOConnection)) {
				$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
				ob_end_clean();
				logString("Unable to connect to '{$strDataWarehouse}': {$strErrorMsg}", '', true, true);
				return false;
			}
		}
	} catch (Exception $e){
		ob_end_clean();
		logString("Unable to connect to '{$strDataWarehouse}'", '', true, true);
		return false;
	}
	ob_end_clean();


	$intStartTime = (int) @GetGlobalSettingValueByName($aRepositoryConnection, "AUTOINCREMENTALGENERATIONTIME", null, true);
	logString('IncDataDestinations StartTime='.$intStartTime, '', true, true);
	if ($intStartTime <= 0) {
		$intStartTime = 2200;
	}

	$blnProcessed = false;
	$currDate = date('Y-m-d');
	//GCRUZ 2016-02-24. Agregar validación de hora al revisar SI_SV_ProcessLog
	$execTime = substr(''.$intStartTime, 0, 2).':'.substr(''.$intStartTime, -2).':00';
	//Si se configura la hora de ejecución con un valor de -1, quiere decir que todo el día se deberán generar destinos incrementales, así que no validará si ya se había o no procesado
	if ($intStartTime != -1) {
		$sql = "SELECT ProcessLogID, CreationDateID 
			FROM SI_SV_ProcessLog 
			WHERE ProcessID = ".efapIncrementalDataDestinations." AND TaskStatus = ".stsCompleted." AND Repository = ".$aBITAMConnection->ADOConnection->Quote($sRepositoryName).
				" AND CreationDateID BETWEEN ".$aBITAMConnection->ADOConnection->DBTimeStamp($currDate." ".$execTime).
				" AND ".$aBITAMConnection->ADOConnection->DBTimeStamp($currDate." 23:59:59")." 
			ORDER BY CreationDateID";
		$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$blnProcessed = true;
		}
		
		//Si ya se había ejecutado la tarea, ya no la volverá a repetir el mismo día
		if ($blnProcessed) {
			logString("IncDataDestinations task was already processed today", '', true, true);
			return false;
		}
	}
	else
	{
		logString("IncDataDestinations continous incremental process not allowed", '', true, true);
		return false;
	}
	
	//Si no ha llegado la hora de ejecución de las tareas, no puede continuar con el proceso, una hora de -1 es un comodín para ejecutar el proceso durante todo el día
	if ($intStartTime > 0) {
		$intCurrentTime = (int) date('Hi');
		if ($intCurrentTime < $intStartTime) {
			logString("The execution time for the IncDataDestinations task has not been reached, the next run should be at {$intStartTime} hours", '', true, true);
			return false;
		}
	}

	$intRequestNumber++;
	$intConnectionTimeOut = $intRequestTimeOut = (int) $aConfiguration->execution_timeout;
	if ($intConnectionTimeOut < 0) {
		$intConnectionTimeOut = 0;
	}
	if ($intRequestTimeOut <= 0) {
		$intRequestTimeOut = 600;
	}

	$strDebugFileName = '';
	if (((string) @$strDefaultDetailLogStringFile) != '') {
		$strDebugFileName = $strDefaultDetailLogStringFile;
	}
	if (trim($strDebugFileName) == '') {
		$strDebugFileName = "eFormsAgent.log";
	}
	$strDebugFileName = str_ireplace('.log', '_'.$intRequestNumber.'.log', $strDebugFileName);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("IncDataDestinations DebugFilename {$strDebugFileName}", 1, 1, "color:blue;");
	}

	$intTaskStatus = stsFailed;
	$strProjectName = '';
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Ahora se comenzará a enviar el ProjectName ya que en base a eso se complementará la validación del password múltiple por repositorio, además sobreescribirá el password del usuario
	//con el específico configurado para este repositorio en caso de ya estar habilitada la funcionalidad
	if ($blnMultiPass) {
		$strProjectName = $sRepositoryName;
		$strkpiPass = GetMultiPasswordForRepository($aBITAMConnection, $sRepositoryName, $blnMultiPass);
		if (!is_null($strkpiPass) && $strkpiPass) {
			$kpiPass = $strkpiPass;
		}
	}
	//@JAPR
	
	$timeStamp = date('YmdHis');
	//@JAPR 2015-09-07: Validado que si el servicio viene como https, se cambie automáticamente a http
	$intPos = stripos($streFormsService, "https://");
	if ($intPos !== false) {
		$streFormsService = str_ireplace("https://", "http://", $streFormsService);
	}
	//@JAPR 2018-11-07: Corregido un bug, la comparación con "=" en MySQL sobre campos Varchar ignora los espacios en blanco al final, así que EMails que los tenían se dejaban pasar por
	//el proceso de login y terminaban provocando errores al procesar sus capturas al formarse URLs en el Agente sin escaparlos debidamente (#ZA1GUB)
	//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strURL = "{$streFormsService}eSurveyService.php?".
		"UserID=".rawurlencode($kpiUser)."&Password=".rawurlencode($kpiPass)."&projectName=".rawurlencode($strProjectName).
		"&action=syncincdatadestinations".
		"&isagent=1&DebugBreak=1&autoincrementalgenerationtime=".$intStartTime."&FAgentName=".$strAgentName;
	//@JAPR
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("IncDataDestinations URL {$strURL}", 1, 1, "color:blue;");
	}
	logString("HTTP Request: ".$strURL);
	try {
		$ch = curl_init();
		if ($ch === false) {
			logString("There was an error starting the curl session", '', true, true);
			return false;
		}
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $intConnectionTimeOut);
		curl_setopt($ch, CURLOPT_TIMEOUT, $intRequestTimeOut);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_URL, $strURL);
		
		$strResult = curl_exec($ch);   
		$intLastCURLError = 0;
		$strLastCURLError = '';
		$strErrorDesc = '';
		if ($strResult === false) {
			$strErrorDesc = curl_error($ch);
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("IncDataDestinations error {$strErrorDesc}", 1, 1, "color:red;");
			}
			logString("Error executing the Incremental DataDestinations task: $strErrorDesc", '', true, true);
			/* Errores comunes:
			- Error executing the Agendas task: Failed to open/read local data from file/application
				Esto sucede cuando la URL solicitada no existe
				http://php.net/manual/en/function.curl-errno.php
				http://php.net/manual/en/function.curl-getinfo.php
			*/
		}
		else {
			//Antes de liberar los recursos obtiene la información del último error detectado, esto cuando el error no es un HTTP 400+, ya que en ese
			//caso hubiera entrado por el If
			$intLastCURLError = @curl_errno($ch);
			$strLastCURLError = @curl_error($ch);
		}
		curl_close($ch);
		
		//Si no hubo error en el request, verifica si hubo error durante el grabado
		$strStatus = 'SyncIncDataDestinations:Success';
		if ($strResult !== false) {
			//@JAPR 2018-02-07: Agregado el archivo de configuraciones semi-dinámico del agente para permitir variar su comportamiento sin tener que apagar la tarea de windows
			//Se podrán asignar configuraciones forzadas modificando este archivo para no tener que cambiar el código y que por servicio se puedan hacer ajustes. Originalmente
			//utilizado para activar el modo de depuración ($blnDebugAgentMode) (#E5YASV)
			if ($blnDebugAgentMode) {
				logString("DebugFileName: {$strDebugFileName}");
			}
			//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			//Esta llamada reasignará el path completo hacia el repositorio para almacenar ahí el archivo de log del procesamiento de la tarea
			GeteFormsLogPath($sRepositoryName);
			//@JAPR
			$blnOk = stripos($strResult, $strStatus);
			if ($blnOk === false) {
				$strErrorDesc = "Error executing the Incremental DataDestinations task , Error num: {$intLastCURLError}, Error: '{$strLastCURLError}'. Check the error detail & debug information in this file: $strDebugFileName";
				//En este caso hubo un error y no se logró concretar el grabado de la encuesta (errores por eBavel no se reportarían, ya que esos
				//no impiden el grabado de la encuesta, en esos casos se deberá consultar el log de upload de la misma)
				logString($strResult, $strDebugFileName, true, true, false);
				//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
				GeteFormsLogPath('');
				//@JAPR
				logString($strErrorDesc, '', true, true);
			}
			else {
				if ($blnDebugAgentMode) {
					logString($strResult, $strDebugFileName, true, true, false);
				}
				//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
				GeteFormsLogPath('');
				//@JAPR
				
				//Todo estuvo bien
				logString($strStatus);
				$intTaskStatus = stsCompleted;
			}
		}
	} catch (Exception $e) {
		logString("Error executing the Incremental DataDestinations task (Exception): ".$e->getMessage(), '', true, true);
	}

	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n El resultado es: <br>\r\n".$strResult);
	}
	
	//Agrega un registro a la bitácora de procesos para que ya no se repita este día nuevamente
	$currDate = $aBITAMConnection->ADOConnection->DBTimeStamp(date('Y-m-d H:i:s'));
	$sql = "INSERT INTO SI_SV_ProcessLog (ProcessID, CreationDateID, Repository, TaskStatus, LogFileName) 
		VALUES (".efapIncrementalDataDestinations.", {$currDate}, ".
			$aBITAMConnection->ADOConnection->Quote($sRepositoryName).", {$intTaskStatus}, ".
			$aBITAMConnection->ADOConnection->Quote($strDebugFileName).")";
	$aBITAMConnection->ADOConnection->Execute($sql);


	return true;
}

//GCRUZ 2016-06-06. Agregado generación de cubo de agendas
function executeAgendaCubeGeneration($aBITAMConnection, $sRepositoryName, $strTime, $intTime)
{
	global $aConfiguration;
	global $intRequestNumber;
	global $strDefaultLogStringFile;
	global $strDefaultDetailLogStringFile;
	global $blnDebugAgentMode;
	global $strAgentName;
	
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	$blnMultiPass = GetMultiPasswordEnabledState($aBITAMConnection, $sRepositoryName);
	//@JAPR
	
	//@JAPR 2019-01-30: Debido a un incidente reciente, LRoux indicó que ya no deberían realizarse ALTER TABLEs continuos para evitar Table locks, así que se
	//comentarán todos los siguientes estatutos bajo el entendido que la única tabla de metadata de KPIOnline tendría siempre que estar al día manualmente,
	//de cualquier manera es buena idea seguir agregando los ALTERs en este bloque como un histórico de los cambios sufridos en el Agente de eForms (#BBGFMH)
	//Primero verifica si debe o no ejecutar la tarea según la configuración global existente y si ya se había o no ejecutado el día actual
	/*$sql = "CREATE TABLE SI_SV_ProcessLog ( 
			ProcessLogID INTEGER NOT NULL AUTO_INCREMENT, 
			ProcessID INTEGER NOT NULL,
			CreationDateID DATETIME NOT NULL, 
			Repository VARCHAR(50) NOT NULL, 
			TaskStatus INTEGER NULL, 
			LogFileName VARCHAR(255) NULL, 
			PRIMARY KEY (ProcessLogID) 
		) AUTO_INCREMENT=1";
	$aBITAMConnection->ADOConnection->Execute($sql);*/
	//@JAPR

	//Conectarse al repositorio para obtener la información de la periodicidad de los destinos de datos
	$kpiUser = '';
	$kpiPass = '';
	$streFormsService = '';
	//Obtiene los parámetros de conexión directo del repositorio de KPIOnline, ya que sólo se cuenta con el nombre del repositorio
	$sql = "SELECT B.Email, B.AccPassword, A.KPIServer, A.ArtusPath, A.FormsPath, A.DBServer, A.DBServerUsr, A.DBServerPwd, A.DataWarehouse 
		FROM saas_databases A 
			INNER JOIN saas_users B ON A.UserID = B.UserID 
		WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote($sRepositoryName);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("AgendaCubeGeneration{$strTime} Query {$sql}", 1, 1, "color:blue;");
	}
	$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$kpiUser = (string) @$aRS->fields["email"];
		$kpiPass = BITAMDecryptPassword((string) @$aRS->fields["accpassword"]);
		$strKPIServer = (string) @$aRS->fields["kpiserver"];
		$strArtusPath = (string) @$aRS->fields["artuspath"];
		$strArtusPath = str_ireplace("\\", "/", $strArtusPath);
		$strFormsPath = (string) @$aRS->fields["formspath"];
		//GCRUZ Test Only!
		//$strFormsPath = 'EsurveyV6GC';
		$streFormsService = "http://{$strKPIServer}/{$strArtusPath}/{$strFormsPath}/";
		$strDBServer = (string) @$aRS->fields["dbserver"];
		$strDBServerUsr = (string) @$aRS->fields["dbserverusr"];
		$strDBServerPwd = (string) @$aRS->fields["dbserverpwd"];
		$strDataWarehouse = (string) @$aRS->fields["datawarehouse"];
	}
	else {
		logString("Unable to retrieve the account data, no repository was found '{$sRepositoryName}': ".$aBITAMConnection->ADOConnection->ErrorMsg(), '', true, true);
		return false;
	}
	
	if (trim($kpiUser) == '') {
		logString("Unable to retrieve the account data, no user admin account was found '{$sRepositoryName}': ", '', true, true);
		return false;
	}

	$aRepositoryConnection = null;
	ob_start();
	try {
		//Cierra la conexión anterior si era de un server diferente
		if (!is_null($aRepositoryConnection)) {
			@$aRepositoryConnection->close();
			$aBITAMConnection = null;
		}
		
		if (is_null($aRepositoryConnection)) {
			//@JAPR 2013-11-06: Modificado para reutilizar los archivos del Model Manager, los cuales no requieren el password desencriptado
			//Este password ya llega desencriptado en este punto
			//Ahora este password llega encriptado, pero así es como lo requiere BITAMConnection cuando se usa desde el
			//Model Manager
			$pass = $strDBServerPwd;
			//@JAPR
			$aRepositoryConnection = BITAMConnection::NewInstance($strDBServer, $strDBServerUsr, $strDBServerPwd, $strDataWarehouse, 'mysql');
			if (is_null($aRepositoryConnection) || is_null($aRepositoryConnection->ADOConnection)) {
				$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
				ob_end_clean();
				logString("Unable to connect to '{$strDataWarehouse}': {$strErrorMsg}", '', true, true);
				return false;
			}
		}
	} catch (Exception $e){
		ob_end_clean();
		logString("Unable to connect to '{$strDataWarehouse}'", '', true, true);
		return false;
	}
	ob_end_clean();


	$intStartTime = (int) @GetGlobalSettingValueByName($aRepositoryConnection, "AGENDASCUBEGENERATIONTIME{$strTime}", null, true);
	logString("AgendaCubeGenerationTime{$strTime} StartTime=".$intStartTime, '', true, true);
	if ($intStartTime <= 0) {
		if ($strTime == 'AM')
			$intStartTime = 900;
		elseif ($strTime == 'PM')
			$intStartTime = 2100;
		else
			$intStartTime = 1200;
	}

	$blnProcessed = false;
	$currDate = date('Y-m-d');
	//GCRUZ 2016-02-24. Agregar validación de hora al revisar SI_SV_ProcessLog
	$execTime = substr('0'.$intStartTime, -4, 2).':'.substr(''.$intStartTime, -2).':00';
	//Si se configura la hora de ejecución con un valor de -1, quiere decir que todo el día se deberán generar destinos incrementales, así que no validará si ya se había o no procesado
	if ($intStartTime != -1) {
		$sql = "SELECT ProcessLogID, CreationDateID 
			FROM SI_SV_ProcessLog 
			WHERE ProcessID = ".$intTime." AND TaskStatus = ".stsCompleted." AND Repository = ".$aBITAMConnection->ADOConnection->Quote($sRepositoryName).
				" AND CreationDateID BETWEEN ".$aBITAMConnection->ADOConnection->DBTimeStamp($currDate." ".$execTime).
				" AND ".$aBITAMConnection->ADOConnection->DBTimeStamp($currDate." 23:59:59")." 
			ORDER BY CreationDateID";
		$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$blnProcessed = true;
		}
		
		//Si ya se había ejecutado la tarea, ya no la volverá a repetir el mismo día
		if ($blnProcessed) {
			logString("AgendaCubeGeneration{$strTime} task was already processed today", '', true, true);
			return false;
		}
	}
	else
	{
		logString("AgendaCubeGeneration{$strTime} continous incremental process not allowed", '', true, true);
		return false;
	}
	
	//Si no ha llegado la hora de ejecución de las tareas, no puede continuar con el proceso, una hora de -1 es un comodín para ejecutar el proceso durante todo el día
	if ($intStartTime > 0) {
		$intCurrentTime = (int) date('Hi');
		if ($intCurrentTime < $intStartTime) {
			logString("The execution time for the AgendaCubeGeneration{$strTime} task has not been reached, the next run should be at {$intStartTime} hours", '', true, true);
			return false;
		}
	}

	$intRequestNumber++;
	$intConnectionTimeOut = $intRequestTimeOut = (int) $aConfiguration->execution_timeout;
	if ($intConnectionTimeOut < 0) {
		$intConnectionTimeOut = 0;
	}
	if ($intRequestTimeOut <= 0) {
		$intRequestTimeOut = 600;
	}

	$strDebugFileName = '';
	if (((string) @$strDefaultDetailLogStringFile) != '') {
		$strDebugFileName = $strDefaultDetailLogStringFile;
	}
	if (trim($strDebugFileName) == '') {
		$strDebugFileName = "eFormsAgent.log";
	}
	$strDebugFileName = str_ireplace('.log', '_'.$intRequestNumber.'.log', $strDebugFileName);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("AgendaCubeGeneration{$strTime} DebugFilename {$strDebugFileName}", 1, 1, "color:blue;");
	}

	$intTaskStatus = stsFailed;
	$strProjectName = '';
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Ahora se comenzará a enviar el ProjectName ya que en base a eso se complementará la validación del password múltiple por repositorio, además sobreescribirá el password del usuario
	//con el específico configurado para este repositorio en caso de ya estar habilitada la funcionalidad
	if ($blnMultiPass) {
		$strProjectName = $sRepositoryName;
		$strkpiPass = GetMultiPasswordForRepository($aBITAMConnection, $sRepositoryName, $blnMultiPass);
		if (!is_null($strkpiPass) && $strkpiPass) {
			$kpiPass = $strkpiPass;
		}
	}
	//@JAPR
	
	$timeStamp = date('YmdHis');
	//@JAPR 2015-09-07: Validado que si el servicio viene como https, se cambie automáticamente a http
	$intPos = stripos($streFormsService, "https://");
	if ($intPos !== false) {
		$streFormsService = str_ireplace("https://", "http://", $streFormsService);
	}
	//@JAPR 2018-11-07: Corregido un bug, la comparación con "=" en MySQL sobre campos Varchar ignora los espacios en blanco al final, así que EMails que los tenían se dejaban pasar por
	//el proceso de login y terminaban provocando errores al procesar sus capturas al formarse URLs en el Agente sin escaparlos debidamente (#ZA1GUB)
	//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strURL = "{$streFormsService}eSurveyService.php?".
		"UserID=".rawurlencode($kpiUser)."&Password=".rawurlencode($kpiPass)."&projectName=".rawurlencode($strProjectName).
		"&action=agendacubegeneration".
		"&isagent=1&DebugBreak=1&Time={$strTime}&agendacubegenerationtime=".$intStartTime."&FAgentName=".$strAgentName;
	//@JAPR
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("AgendaCubeGenerationTime URL {$strURL}", 1, 1, "color:blue;");
	}
	logString("HTTP Request: ".$strURL);
	try {
		$ch = curl_init();
		if ($ch === false) {
			logString("There was an error starting the curl session", '', true, true);
			return false;
		}
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $intConnectionTimeOut);
		curl_setopt($ch, CURLOPT_TIMEOUT, $intRequestTimeOut);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_URL, $strURL);
		
		$strResult = curl_exec($ch);   
		$intLastCURLError = 0;
		$strLastCURLError = '';
		$strErrorDesc = '';
		if ($strResult === false) {
			$strErrorDesc = curl_error($ch);
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("AgendaCubeGeneration{$strTime} error {$strErrorDesc}", 1, 1, "color:red;");
			}
			logString("Error executing the Agenda Cube Generation {$strTime} task: $strErrorDesc", '', true, true);
			/* Errores comunes:
			- Error executing the Agendas task: Failed to open/read local data from file/application
				Esto sucede cuando la URL solicitada no existe
				http://php.net/manual/en/function.curl-errno.php
				http://php.net/manual/en/function.curl-getinfo.php
			*/
		}
		else {
			//Antes de liberar los recursos obtiene la información del último error detectado, esto cuando el error no es un HTTP 400+, ya que en ese
			//caso hubiera entrado por el If
			$intLastCURLError = @curl_errno($ch);
			$strLastCURLError = @curl_error($ch);
		}
		curl_close($ch);
		
		//Si no hubo error en el request, verifica si hubo error durante el grabado
		$strStatus = 'AgendaCubeGeneration:Success';
		if ($strResult !== false) {
			//@JAPR 2018-02-07: Agregado el archivo de configuraciones semi-dinámico del agente para permitir variar su comportamiento sin tener que apagar la tarea de windows
			//Se podrán asignar configuraciones forzadas modificando este archivo para no tener que cambiar el código y que por servicio se puedan hacer ajustes. Originalmente
			//utilizado para activar el modo de depuración ($blnDebugAgentMode) (#E5YASV)
			if ($blnDebugAgentMode) {
				logString("DebugFileName: {$strDebugFileName}");
			}
			//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			//Esta llamada reasignará el path completo hacia el repositorio para almacenar ahí el archivo de log del procesamiento de la tarea
			GeteFormsLogPath($sRepositoryName);
			//@JAPR
			$blnOk = stripos($strResult, $strStatus);
			if ($blnOk === false) {
				$strErrorDesc = "Error executing the Agenda Cube Generation {$strTime} task , Error num: {$intLastCURLError}, Error: '{$strLastCURLError}'. Check the error detail & debug information in this file: $strDebugFileName";
				//En este caso hubo un error y no se logró concretar el grabado de la encuesta (errores por eBavel no se reportarían, ya que esos
				//no impiden el grabado de la encuesta, en esos casos se deberá consultar el log de upload de la misma)
				logString($strResult, $strDebugFileName, true, true, false);
				//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
				GeteFormsLogPath('');
				//@JAPR
				logString($strErrorDesc, '', true, true);
			}
			else {
				if ($blnDebugAgentMode) {
					logString($strResult, $strDebugFileName, true, true, false);
				}
				//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
				GeteFormsLogPath('');
				//@JAPR
				
				//Todo estuvo bien
				logString($strStatus);
				$intTaskStatus = stsCompleted;
			}
		}
	} catch (Exception $e) {
		logString("Error executing the Agenda Cube Generation {$strTime} task (Exception): ".$e->getMessage(), '', true, true);
	}

	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n El resultado es: <br>\r\n".$strResult);
	}
	
	//Agrega un registro a la bitácora de procesos para que ya no se repita este día nuevamente
	$currDate = $aBITAMConnection->ADOConnection->DBTimeStamp(date('Y-m-d H:i:s'));
	$sql = "INSERT INTO SI_SV_ProcessLog (ProcessID, CreationDateID, Repository, TaskStatus, LogFileName) 
		VALUES (".$intTime.", {$currDate}, ".
			$aBITAMConnection->ADOConnection->Quote($sRepositoryName).", {$intTaskStatus}, ".
			$aBITAMConnection->ADOConnection->Quote($strDebugFileName).")";
	$aBITAMConnection->ADOConnection->Execute($sql);


	return true;
}

//@JAPR 2015-09-07: Agregado el procesamiento de DataSources al agente, ahora independientemente de si tiene o no tareas que procesar, deberá incluir el repositorio para
//procesar los DataSources
/* Carga los datos de todos los DataSources que tienen una fuente externa mediante un request vía curl para controlar los errores
*/
function executeDataSourcesUpload($aBITAMConnection, $sRepositoryName) {
	global $aConfiguration;
	global $intRequestNumber;
	global $strDefaultLogStringFile;
	global $strDefaultDetailLogStringFile;
	global $blnDebugAgentMode;
	global $strAgentName;
	
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	$blnMultiPass = GetMultiPasswordEnabledState($aBITAMConnection, $sRepositoryName);
	//@JAPR
	
	$intRequestNumber++;
	$intConnectionTimeOut = $intRequestTimeOut = (int) $aConfiguration->execution_timeout;
	if ($intConnectionTimeOut < 0) {
		$intConnectionTimeOut = 0;
	}
	if ($intRequestTimeOut <= 0) {
		$intRequestTimeOut = 600;
	}
	
	$strDebugFileName = '';
	if (((string) @$strDefaultDetailLogStringFile) != '') {
		$strDebugFileName = $strDefaultDetailLogStringFile;
	}
	if (trim($strDebugFileName) == '') {
		$strDebugFileName = "eFormsAgent.log";
	}
	$strDebugFileName = str_ireplace('.log', '_'.$intRequestNumber.'.log', $strDebugFileName);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("DataSources DebugFilename {$strDebugFileName}", 1, 1, "color:blue;");
	}
	
	$kpiUser = '';
	$kpiPass = '';
	$streFormsService = '';
	//Obtiene los parámetros de conexión directo del repositorio de KPIOnline, ya que sólo se cuenta con el nombre del repositorio
	$sql = "SELECT B.Email, B.AccPassword, A.KPIServer, A.ArtusPath, A.FormsPath 
		FROM saas_databases A 
			INNER JOIN saas_users B ON A.UserID = B.UserID 
		WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote($sRepositoryName);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("DataSources Query {$sql}", 1, 1, "color:blue;");
	}
	$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$kpiUser = (string) @$aRS->fields["email"];
		$kpiPass = BITAMDecryptPassword((string) @$aRS->fields["accpassword"]);
		$strKPIServer = (string) @$aRS->fields["kpiserver"];
		$strArtusPath = (string) @$aRS->fields["artuspath"];
		$strArtusPath = str_ireplace("\\", "/", $strArtusPath);
		$strFormsPath = (string) @$aRS->fields["formspath"];
		$streFormsService = "http://{$strKPIServer}/{$strArtusPath}/{$strFormsPath}/";
	}
	else {
		logString("Unable to retrieve the account data, no repository was found '{$sRepositoryName}': ".$aBITAMConnection->ADOConnection->ErrorMsg(), '', true, true);
		return false;
	}
	
	if (trim($kpiUser) == '') {
		logString("Unable to retrieve the account data, no user admin account was found '{$sRepositoryName}': ", '', true, true);
		return false;
	}
	
	$strProjectName = '';
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Ahora se comenzará a enviar el ProjectName ya que en base a eso se complementará la validación del password múltiple por repositorio, además sobreescribirá el password del usuario
	//con el específico configurado para este repositorio en caso de ya estar habilitada la funcionalidad
	if ($blnMultiPass) {
		$strProjectName = $sRepositoryName;
		$strkpiPass = GetMultiPasswordForRepository($aBITAMConnection, $sRepositoryName, $blnMultiPass);
		if (!is_null($strkpiPass) && $strkpiPass) {
			$kpiPass = $strkpiPass;
		}
	}
	//@JAPR
	
	$timeStamp = date('YmdHis');
	//@JAPR 2015-09-07: Validado que si el servicio viene como https, se cambie automáticamente a http
	$intPos = stripos($streFormsService, "https://");
	if ($intPos !== false) {
		$streFormsService = str_ireplace("https://", "http://", $streFormsService);
	}
	//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$intFullBuild = getParamValue('FullBuild', 'both', '(int)', 0);
	//@JAPR
	
	$objTaskData = array();
	$objTaskData['RepositoryName'] = $sRepositoryName;
	$objTaskData['FormsPath'] = $streFormsService;
	$objTaskData['User'] = $kpiUser;
	$objTaskData['LogFileName'] = $strDebugFileName;
	//@JAPR 2018-05-16: Optimizada la carga de eForms (#G87C4W)
	//Agregado el parámetro appVersion, ya que las imágenes que ahora se procesarán con este request dependen de cierta versión. Se mandará siempre la última versión
	//del servicio ya que finalmente quien ejecuta el Agente es el servicio, y este proceso es independiente de una versión, por tanto es válido usar la última versión
	//@JAPR 2018-11-07: Corregido un bug, la comparación con "=" en MySQL sobre campos Varchar ignora los espacios en blanco al final, así que EMails que los tenían se dejaban pasar por
	//el proceso de login y terminaban provocando errores al procesar sus capturas al formarse URLs en el Agente sin escaparlos debidamente (#ZA1GUB)
	//@JAPR 2018-10-01: Agregado el soporte de catálogos offline (#IAJE6Q)
	//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strURL = "{$streFormsService}eSurveyService.php?".
		"UserID=".rawurlencode($kpiUser)."&Password=".rawurlencode($kpiPass)."&projectName=".rawurlencode($strProjectName).
		"&action=syncdatasources".
		"&isagent=1&appVersion=".ESURVEY_SERVICE_VERSION."&DebugBreak=1&FullBuild={$intFullBuild}&FAgentName=".$strAgentName;
	//Agregado el parámetro DebugLog para forzar a grabar las bitácoras en un archivo de paso (exclusivamente ejecución vía browser)
	if (getParamValue('DebugLog', 'both', '(string)', true)) {
		$strURL = $strURL."&DebugLog=".getParamValue('DebugLog', 'both', '(string)', true);
	}
	//@JAPR
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("DataSources URL {$strURL}", 1, 1, "color:blue;");
	}
	logString("HTTP Request: ".$strURL);
	try {
		$ch = curl_init();
		if ($ch === false) {
			logString("There was an error starting the curl session", '', true, true);
			return false;
		}
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $intConnectionTimeOut);
		curl_setopt($ch, CURLOPT_TIMEOUT, $intRequestTimeOut);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_URL, $strURL);
		
		$strResult = curl_exec($ch);   
		$intLastCURLError = 0;
		$strLastCURLError = '';
		$strErrorDesc = '';
		if ($strResult === false) {
			$strErrorDesc = curl_error($ch);
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("DataSources error {$strErrorDesc}", 1, 1, "color:red;");
			}
			logString("Error executing the DataSources task: $strErrorDesc", '', true, true);
			/* Errores comunes:
			- Error executing the DataSources task: Failed to open/read local data from file/application
				Esto sucede cuando la URL solicitada no existe
				http://php.net/manual/en/function.curl-errno.php
				http://php.net/manual/en/function.curl-getinfo.php
			*/
			
			//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
			//Este tipo de errores deben ser notificados ya que el proceso de sincronización ni siquiera inició, así que no hay ninguna otra notificación
			//vía EMail que se hubiera enviado
			reportDataSourceTaskError($aBITAMConnection, $strErrorDesc, null, $objTaskData);
			//@JAPR
		}
		else {
			//Antes de liberar los recursos obtiene la información del último error detectado, esto cuando el error no es un HTTP 400+, ya que en ese
			//caso hubiera entrado por el If
			$intLastCURLError = @curl_errno($ch);
			$strLastCURLError = @curl_error($ch);
		}
		curl_close($ch);
		
		//Si no hubo error en el request, verifica si hubo error durante el grabado
		$strStatus = 'DataSources were processed successfully.';
		if ($strResult !== false) {
			//@JAPR 2018-02-07: Agregado el archivo de configuraciones semi-dinámico del agente para permitir variar su comportamiento sin tener que apagar la tarea de windows
			//Se podrán asignar configuraciones forzadas modificando este archivo para no tener que cambiar el código y que por servicio se puedan hacer ajustes. Originalmente
			//utilizado para activar el modo de depuración ($blnDebugAgentMode) (#E5YASV)
			if ($blnDebugAgentMode) {
				logString("DebugFileName: {$strDebugFileName}");
			}
			//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			//Esta llamada reasignará el path completo hacia el repositorio para almacenar ahí el archivo de log del procesamiento de la tarea
			GeteFormsLogPath($sRepositoryName);
			//@JAPR
			$blnOk = stripos($strResult, $strStatus);
			if ($blnOk === false) {
				$strErrorDesc = "Error executing the DataSources task , Error num: {$intLastCURLError}, Error: '{$strLastCURLError}'. Check the error detail & debug information in this file: $strDebugFileName";
				//En este caso hubo un error y no se logró concretar el grabado de la encuesta (errores por eBavel no se reportarían, ya que esos
				//no impiden el grabado de la encuesta, en esos casos se deberá consultar el log de upload de la misma)
				logString($strResult, $strDebugFileName, true, true, false);
				//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
				GeteFormsLogPath('');
				//@JAPR
				logString($strErrorDesc, '', true, true);
				
				//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
				//Este tipo de errores deben ser notificados ya que el proceso de sincronización ni siquiera inició, así que no hay ninguna otra notificación
				//vía EMail que se hubiera enviado
				reportDataSourceTaskError($aBITAMConnection, $strErrorDesc, null, $objTaskData);
				//@JAPR
			}
			else {
				if ($blnDebugAgentMode) {
					logString($strResult, $strDebugFileName, true, true, false);
				}
				//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
				GeteFormsLogPath('');
				//@JAPR
				
				//Todo estuvo bien
				logString($strStatus);
			}
		}
	} catch (Exception $e) {
		logString("Error executing the DataSources task (Exception): ".$e->getMessage(), '', true, true);
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n El resultado es: <br>\r\n".$strResult);
	}
	
	return true;
}
//@JAPR

/* Obtiene los datos de la tarea especificada y hace una llamada a eForms para que cargue los datos de la captura asociada a ella
*/
function executeSurveyTask($aBITAMConnection, $aSurveyTaskID) {
	if (getParamValue('DebugBreak', 'both', '(int)', true)){
		ECHOString("executeSurveyTask", 1, 1, "color:purple;");
	}

	global $aConfiguration;
	global $intRequestNumber;
	global $strDefaultLogStringFile;
	global $strDefaultDetailLogStringFile;
	global $blnDebugAgentMode;
	//@JAPR 2019-02-12: Modificado el comportamiento para permitir generar el Log del agente en tiempo de ejecución, enviando el parámetro para que se escriban los eventos mientras suceden
	//y no hasta que ha terminado todo el proceso (#P5SE4W)
	global $blnSaveDebugLogOnTheFly;
	//@JAPR
	global $strAgentName;
	
	$currDate = date('Y-m-d H:i:s');
	$intRequestNumber++;
	$intConnectionTimeOut = $intRequestTimeOut = (int) $aConfiguration->execution_timeout;
	if ($intConnectionTimeOut < 0) {
		$intConnectionTimeOut = 0;
	}
	if ($intRequestTimeOut <= 0) {
		$intRequestTimeOut = 600;
	}
	
	//@JAPR 2013-11-22: Agregada la información del dispositivo que realiza la petición
	//@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
	//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
	//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
	//@JAPR 2018-02-08: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
	$sql = "SELECT CreationDateID, Repository, SurveyID, AgendaID, UserID, WebMode, Latitude, Longitude, 
			SurveyDate, TaskStatus, TaskStartDate, FileName, FormsPath, FormsVersion, AgentID, Executing, UserAgent, 
			SourceTaskID, SavingTries, SessionID, DeviceName, UDID, Accuracy, DeviceID, DestinationIDs, Reprocess, SurveyKey 
		FROM SI_SV_SurveyTasks 
		WHERE SurveyTaskID = {$aSurveyTaskID}";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n sql: {$sql}");
	}
	$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
	if ($aRS === false) {
		logString("Error reading the survey task definition in server '{$aConfiguration->fbm_db_server}': {$sql}. Error: ".@$aBITAMConnection->ADOConnection->ErrorMsg().", query: {$sql}", '', true, true);
		return false;
	}
	elseif ($aRS->EOF) {
		logString("The specified survey task no longer exists '{$aSurveyTaskID}'", '', true, true);
		return false;
	}
	
	//@JAPR 2015-01-30: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
	$intTaskStatus = (int) @$aRS->fields["taskstatus"];
	//@JAPR
	
	//Si llega aqui entonces está listo para ejecutar la tarea, pero antes la marca en estatus de ejecutando por si ocurre algún problema que
	//interrumpiera el proceso antes de terminarlo
	$sql = "UPDATE SI_SV_SurveyTasks SET 
			TaskStatus = ".stsActive.", 
			TaskStartDate = ".$aBITAMConnection->ADOConnection->DBTimeStamp($currDate)." 
		WHERE SurveyTaskID = {$aSurveyTaskID}";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n sql: {$sql}");
	}
	$aBITAMConnection->ADOConnection->Execute($sql);
	
	//Lee los datos para generar la URL de carga de la encuesta
	$streFormsService = (string) @$aRS->fields["formspath"];
	$strAppVersion = (string) @$aRS->fields["formsversion"];
	$kpiUser = (string) @$aRS->fields["userid"];
	$kpiPass = '';
	$strProjectName = (string) @$aRS->fields["repository"];
	//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$sRepositoryName = $strProjectName;
	//@JAPR
	$intWebMode = (int) @$aRS->fields["webmode"];
	$surveyID = (int) @$aRS->fields["surveyid"];
	$agendaID = (int) @$aRS->fields["agendaid"];
	if ($agendaID <= 0) {
		$agendaID = "undefined";
	}
	$surveyDate = (string) @$aRS->fields["surveydate"];
	$surveyHour = trim(substr($surveyDate, 11));
	$surveyDate = trim(substr($surveyDate, 0, 10));
	$strFileName = (string) @$aRS->fields["filename"];
	$dblSyncLat = (string) @$aRS->fields["latitude"];
	$dblSyncLong = (string) @$aRS->fields["longitude"];
	//@JAPR 2015-07-30: Corregido un bug, no estaba enviando el valor del Accuracy para el grabado en las tablas de datos (#TS6KG1)
	$dblSyncAcc = (string) @$aRS->fields["accuracy"];
	//@JAPR 2013-11-22: Agregada la información del dispositivo que realiza la petición
	$nav = (string) @$aRS->fields["useragent"];
	//@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
	$intSourceTaskID = (int) @$aRS->fields["sourcetaskid"];
	$intSavingTries = (int) @$aRS->fields["savingtries"];
	$intExecuting = (int) @$aRS->fields["executing"];
	$strCreationDateID = (string) @$aRS->fields["creationdateid"];
	//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
	$strAppSessionID = (string) @$aRS->fields["sessionid"];
	$strDeviceName = (string) @$aRS->fields["devicename"];
	$strUDID = (string) @$aRS->fields["udid"];
	//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
	$intDeviceID = (int) @$aRS->fields["deviceid"];
	$strDestinationIDs = (string) @$aRS->fields["destinationids"];
	$reprocess = (string) @$aRS->fields["reprocess"];
	//@JAPR 2018-02-08: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
	$surveyKey = (int) (int) @$aRS->fields["surveykey"];
	
	//@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
	//Crea una instancia de tarea para permitir enviar el reporte en caso de error (No se carga directa con los métodos, ya que la conexión no
	//se realizó al DWH como regularmente lo haría sino a la FBM000, así que se llena manualmente en este punto, de cualquier forma los métodos
	//que se invoquen no harán uso del Repository, por lo que es seguro enviarla como si fuera el válido)
	$objSurveyTask = BITAMSurveyTask::NewInstance($aBITAMConnection);
	$objSurveyTask->SurveyTaskID = $aSurveyTaskID;
	$objSurveyTask->CreationDateID = $strCreationDateID;
	$objSurveyTask->SurveyID = $surveyID;
	$objSurveyTask->User = $kpiUser;
	$objSurveyTask->SurveyDate = $surveyDate.' '.$surveyHour;
	$objSurveyTask->TaskStartDate = $currDate;
	$objSurveyTask->TaskStatus = $intTaskStatus;
	$objSurveyTask->SourceTaskID = $intSourceTaskID;
	$objSurveyTask->AgendaID = $agendaID;
	$objSurveyTask->WebMode = $intWebMode;
	$objSurveyTask->FileName = $strFileName;
	$objSurveyTask->RepositoryName = $strProjectName;
	$objSurveyTask->SyncLatitude = $dblSyncLat;
	$objSurveyTask->SyncLongitude = $dblSyncLong;
	//@JAPR 2015-07-30: Corregido un bug, no estaba enviando el valor del Accuracy para el grabado en las tablas de datos (#TS6KG1)
	$objSurveyTask->SyncAccuracy = $dblSyncAcc;
	//@JAPR
	$objSurveyTask->FormsPath = $streFormsService;
	$objSurveyTask->FormsVersion = $strAppVersion;
	$objSurveyTask->UserAgent = $nav;
	$objSurveyTask->SavingTries = $intSavingTries;
	$objSurveyTask->Executing = $intExecuting;
	//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
	$objSurveyTask->SessionID = $strAppSessionID;
	$objSurveyTask->DeviceName = $strDeviceName;
	$objSurveyTask->UDID = $strUDID;
	//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
	$objSurveyTask->DeviceID = $intDeviceID;
	$objSurveyTask->DestinationIDs = $strDestinationIDs;
	$objSurveyTask->Reprocess = $reprocess;
	//@JAPR 2018-02-08: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
	$objSurveyTask->SurveyKey = $surveyKey;
	
	//@JAPR
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n Task to reschedule:");
		PrintMultiArray($objSurveyTask);
	}
	
	$nav = $aBITAMConnection->ADOConnection->Quote($nav);
	//@JAPR
	
	$strDebugFileName = '';
	if (((string) @$strDefaultDetailLogStringFile) != '') {
		$strDebugFileName = $strDefaultDetailLogStringFile;
	}
	if (trim($strDebugFileName) == '') {
		$strDebugFileName = "eFormsAgent.log";
	}
	$strDebugFileName = str_ireplace('.log', '_'.$intRequestNumber.'.log', $strDebugFileName);
	$objSurveyTask->LogFileName = $strDebugFileName;
	
	//Ejecutar este INSERT en caso de falla (stsFailed) en la carga de la encuesta o si se completó (stsCompleted) correctamente
	//@JAPR 2013-11-22: Agregada la información del dispositivo que realiza la petición
	//@AAL 03/07/2015 Agregado para EformsV6 y siguientes, Este campo permite agrupar y obtener la duración de sincronización (Max(CreationDate) - Min(CreationDate))
	//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
	//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
	//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
	//Ene16: Se agregan los campos DestinationIDs y Reprocess
	//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
	//Agregado el campo SurveyKey con el id de la captura, -1 es el default o en caso de fallo sin posibilidad de reporte de Id (que el servicio no esté actualizado)
	$sqlINSERT = "INSERT INTO SI_SV_SurveyTasksLog (SurveyTaskID, CreationDateID, Repository, SurveyID, AgendaID, UserID, WebMode, 
			Latitude, Longitude, SurveyDate, TaskStatus, TaskStartDate, FileName, FormsPath, FormsVersion, AgentID, UserAgent, LogFileName, 
			SourceTaskID, SavingTries, SessionID, DeviceName, UDID, Accuracy, DeviceID, DestinationIDs, Reprocess, SurveyKey) 
		SELECT SurveyTaskID, CreationDateID, Repository, SurveyID, AgendaID, UserID, WebMode, 
			Latitude, Longitude, SurveyDate, %s, TaskStartDate, FileName, FormsPath, FormsVersion, AgentID, UserAgent, ".
			$aBITAMConnection->ADOConnection->Quote($strDebugFileName).", 
			SourceTaskID, SavingTries, SessionID, DeviceName, UDID, Accuracy, DeviceID, DestinationIDs, Reprocess, %d
		FROM SI_SV_SurveyTasks 
		WHERE SurveyTaskID = {$aSurveyTaskID}";
	
	//Ejecutar este DELETE si la tarea termina ok
	$sqlDELETE = "DELETE FROM SI_SV_SurveyTasks WHERE SurveyTaskID = {$aSurveyTaskID}";
	if (getParamValue('DebugBreak', 'both', '(int)', true)){
		ECHOString("Termina de armar insert y delete", 1, 1, "color:purple;");
	}
	
	//Para obtener el password del usuario (el cual pudiera haber cambiado desde que se creó la tarea hasta que entró el agente), como teóricamente
	//las tareas se crean en el mismo servidor de BD donde están el BMD y el DWH, se reutilizará la conexión hacia la FBM000 actual para accesar
	//a la base de datos del repositorio y así obtener el password
	//@JAPR 2014-02-17: No recordé porqué es que esto funcionó originalmente, pero en el server de la base de datos de KPIOnline ya no necesariamente
	//están también todas las bases de datos de repositorio y DWH (de hecho, sólo hay una al día de este comentario, la de Barcel), así que en lugar
	//de accesar a SI_USUARIO lo cual requeriría una conexión adicional, simplemente se extraerá el password de saas_users directamente
	$sql = "SELECT AccPassword 
		FROM saas_users 
		WHERE Email = ".$aBITAMConnection->ADOConnection->Quote($kpiUser);
	/*$sql = "SELECT PASSWORD 
		FROM {$strProjectName}.SI_USUARIO 
		WHERE CUENTA_CORREO = ".$aBITAMConnection->ADOConnection->Quote($kpiUser);*/
	$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$kpiPass = BITAMDecryptPassword((string) @$aRS->fields["accpassword"]);
	}
	else {
		logString("Unable to retrieve the password for the user '{$kpiUser}': ".$aBITAMConnection->ADOConnection->ErrorMsg(), '', true, true);
		
		//Debe marcar la tarea como fallida y eliminarla de la tabla de pendientes
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
			echo("<br>\r\n sql: ".str_ireplace('%d', -1, str_ireplace('%s', stsFailed, $sqlINSERT)));
			//@JAPR
		}
		//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
		if ($aBITAMConnection->ADOConnection->Execute(str_ireplace('%d', -1, str_ireplace('%s', stsFailed, $sqlINSERT)))) {
			//Si la pudo insertar en la tabla de log, entonces si la elimina de la tabla de pendientes
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sqlDELETE}");
			}
			$aBITAMConnection->ADOConnection->Execute($sqlDELETE);
		}
		else {
			//De lo contrario simplemente actualiza la tabla de pendientes para no perder la información
			$sql = "UPDATE SI_SV_SurveyTasks SET 
					TaskStatus = ".stsFailed." 
				WHERE SurveyTaskID = {$aSurveyTaskID}";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
		}
		
		return false;
	}
	
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	$blnMultiPass = GetMultiPasswordEnabledState($aBITAMConnection, $strProjectName, $kpiUser);
	
	//Ahora se comenzará a enviar el ProjectName ya que en base a eso se complementará la validación del password múltiple por repositorio
	if ($blnMultiPass) {
		$strkpiPass = GetMultiPasswordForRepository($aBITAMConnection, $strProjectName, $blnMultiPass, $kpiUser);
		if (!is_null($strkpiPass) && $strkpiPass) {
			$kpiPass = $strkpiPass;
		}
	}
	else {
		//El ProjectName ya está asignado desde que se leyó la tarea, así que sólo lo limpiará si no están habilitados los múltiples passwords por compatibilidad hacia atrás
		$strProjectName = '';
	}
	//@JAPR
	
	$timeStamp = date('YmdHis');
	//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
	//@JAPR 2015-07-30: Corregido un bug, estaba enviando el parámetro syncLat como SyncLat y no lo podía leer correctamente
	//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
	//@EVEG 2015-08-29: Se agrego el parametro del id del task para la bitacora de destinos
	//@JAPR 2015-09-07: Validado que si el servicio viene como https, se cambie automáticamente a http
	$intPos = stripos($streFormsService, "https://");
	if ($intPos !== false) {
		$streFormsService = str_ireplace("https://", "http://", $streFormsService);
	}
	//@JAPR 2018-11-07: Corregido un bug, la comparación con "=" en MySQL sobre campos Varchar ignora los espacios en blanco al final, así que EMails que los tenían se dejaban pasar por
	//el proceso de login y terminaban provocando errores al procesar sus capturas al formarse URLs en el Agente sin escaparlos debidamente (#ZA1GUB)
	//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strURL = "{$streFormsService}eSurveyService.php?appVersion={$strAppVersion}".
		"&UserID=".rawurlencode($kpiUser)."&Password=".rawurlencode($kpiPass)."&projectName=".rawurlencode($strProjectName)."&webMode={$intWebMode}&surveyID={$surveyID}".
		"&agendaID={$agendaID}&surveyDate={$surveyDate}&surveyHour={$surveyHour}&action=uploadsyncdata&ftype=outbox".
		"&filename=".rawurlencode($strFileName)."&DateID={$surveyDate}&HourID={$surveyHour}&syncLat={$dblSyncLat}&syncLong={$dblSyncLong}&syncAcc={$dblSyncAcc}".
		"&testurl=1&isagent=1&DebugBreak=1&sessionID=".rawurlencode($strAppSessionID)."&deviceName=".rawurlencode($strDeviceName)."&uuid=".rawurlencode($strUDID).
		"&dte={$timeStamp}&deviceID={$intDeviceID}&TaskID={$aSurveyTaskID}&FAgentName=".rawurlencode($strAgentName);
	//@JAPR
	//Agregado el parámetro DebugLog para forzar a grabar las bitácoras en un archivo de paso (exclusivamente ejecución vía browser)
	if (getParamValue('DebugLog', 'both', '(string)', true)) {
		$strURL = $strURL."&DebugLog=".getParamValue('DebugLog', 'both', '(string)', true);
	}
	//@JAPR 2019-02-12: Modificado el comportamiento para permitir generar el Log del agente en tiempo de ejecución, enviando el parámetro para que se escriban los eventos mientras suceden
	//y no hasta que ha terminado todo el proceso (#P5SE4W)
	elseif ( $blnDebugAgentMode && $blnSaveDebugLogOnTheFly && $strDebugFileName != '' ) {
		//En este caso con el modo de depuración activado, se grabará directamente en el archivo cuyo nombre ya quedó asociado a esta tarea
		$strURL = $strURL."&DebugLog=".$strDebugFileName;
	}
	//@JAPR
	logString("HTTP Request: ".$strURL);
	//$arrPostFields = array();
	try {
		$ch = curl_init();
		if ($ch === false) {
			logString("There was an error starting the curl session", '', true, true);
			return false;
		}
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $intConnectionTimeOut);
		curl_setopt($ch, CURLOPT_TIMEOUT, $intRequestTimeOut);
		//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
		//Agregada para que los errores HTTP 400+ generen un error en lugar de regresar el HTML de la página de error como si la petición hubiera
		//sido exitosa
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		//echo("<br>\r\n Request URL: {$strURL}");
		
		//curl_setopt($ch, CURLOPT_HEADER, true); 
		curl_setopt($ch, CURLOPT_URL, $strURL);
		//@JAPR 2015-05-04: Corregido un bug, con PhP 5.6 comenzó a marcar un error si se especificaba el request como POST pero sin datos, así que se
		//cambió para que se mandara como GET
		//curl_setopt($ch, CURLOPT_POST, true);
		//@JAPR
		//curl_setopt($ch, CURLOPT_REFERER, '');
		//curl_setopt($ch, CURLOPT_HEADER, $header OR $follow);
		//curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		//curl_setopt($ch, CURLOPT_USERPWD, "[jpuente:jpuente]");
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
		//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $arrPostFields);
		
		$strResult = curl_exec($ch);   
		//@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
		//Gracias a la opción CURLOPT_FAILONERROR, ahora los errores de HTTP si entran por este IF, así que es seguro asumir que si entró por
		//aquí significa que no pudo grabar absolutamente nada de la captura y se puede reintentar nuevamente, generando una tarea recalendarizada
		//para que se procese en otro momento
		$intLastCURLError = 0;
		$strLastCURLError = '';
		$strErrorDesc = '';
		if ($strResult === false) {
			$strErrorDesc = curl_error($ch);
			logString("Error executing the survey task #{$aSurveyTaskID}: $strErrorDesc", '', true, true);
			/* Errores comunes:
			- Error executing the survey task: Failed to open/read local data from file/application
				Esto sucede cuando la URL solicitada no existe
				http://php.net/manual/en/function.curl-errno.php
				http://php.net/manual/en/function.curl-getinfo.php
			*/
			
			//Debe marcar la tarea como fallida y eliminarla de la tabla de pendientes
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
				echo("<br>\r\n sql: ".str_ireplace('%d', -1, str_ireplace('%s', stsFailed, $sqlINSERT)));
				//@JAPR
			}
			//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
			if ($aBITAMConnection->ADOConnection->Execute(str_ireplace('%d', -1, str_ireplace('%s', stsFailed, $sqlINSERT)))) {
				//Si la pudo insertar en la tabla de log, entonces si la elimina de la tabla de pendientes
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n sql: {$sqlDELETE}");
				}
				$aBITAMConnection->ADOConnection->Execute($sqlDELETE);
				
				//@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
				//Después de mover la tarea a la tabla de log, crea la nueva tarea recalendarizada siempre y cuando aún no llegue al máximo de
				//intentos por procesarla
				$objSurveyTask->TaskStatus = stsFailed;
				$objSurveyTask->reschedule($aBITAMConnection);
				//@JAPR
			}
			else {
				//De lo contrario simplemente actualiza la tabla de pendientes para no perder la información
				$sql = "UPDATE SI_SV_SurveyTasks SET 
						TaskStatus = ".stsFailed." 
					WHERE SurveyTaskID = {$aSurveyTaskID}";
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n sql: {$sql}");
				}
				$aBITAMConnection->ADOConnection->Execute($sql);
			}
			
			//Este tipo de errores deben ser notificados ya que el proceso de grabado ni siquiera inició, así que no hay ninguna otra notificación
			//vía EMail que se hubiera enviado
			reportSurveyTaskError($aBITAMConnection, $strErrorDesc, null, $objSurveyTask);
		}
		else {
			//@JAPR 2015-02-05: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
			//Antes de liberar los recursos obtiene la información del último error detectado, esto cuando el error no es un HTTP 400+, ya que en ese
			//caso hubiera entrado por el If
			$intLastCURLError = @curl_errno($ch);
			$strLastCURLError = @curl_error($ch);
		}
		curl_close($ch);
		
		//Si no hubo error en el request, verifica si hubo error durante el grabado
		$strStatus = 'Survey data was saved successfully.';
		
		if ($strResult !== false) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("IF", 1, 1, "color:purple;");
			}
			
			//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
			//Identifica el posible id de la captura, el cual se generaría posterior al INSERT en la tabla de datos de eForms, independientemente de si la tarea
			//concluyó correctamente o falló en algún punto. Sólo los servicios adecuadamente actualizados contendrían las líneas en el siguiente formato para
			//permitir identificar el Id de la captura:
				//### Entry info ###
				//SurveyKey: ElIdDeLaCapturaNumérico
				//### Entry info ###
			//Por lo que en este punto asignará el Id correspondiente al texto encontrado en ese punto, o dejará -1 como valor default en caso de no haber alcanzado
			//a registrarlo o ser un servicio no actualizado
			$intEndPos = 0;
			$intStartPos = (int) stripos($strResult, "### Entry info ###");//1 =>18
			$factKeyDimVal = -1;
			//Nunca podría ser la posición inicial el primer caracter, ya que la respuesta contiene demasiada información de depuración
			if ( $intStartPos > 0 ) {
				$intEndPos = (int) stripos($strResult, "### Entry info ###", $intStartPos+1);//2 => 19 offset
				if ( $intEndPos > 0 ) {
					$strEntryData = substr($strResult, $intStartPos + strlen("### Entry info ###"), $intEndPos - $intStartPos - strlen("### Entry info ###"));
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("The Entry response data:\r\n'{$strEntryData}'", 1, 1, "color:purple;");
					}
					
					$arrEntryData = explode("\r\n", $strEntryData);
					$strSurveyKey = (string) @$arrEntryData[1];
					$arrData = explode(":", $strSurveyKey);
					//$arrData[3] contiene el texto en formato: Fecha + hora (así que son 2 ":") - SurveyKey: (otro ":" mas) EntryId (por tanto se busca el índice 4)
					$factKeyDimVal = (int) trim(@$arrData[3]);
					if ( $factKeyDimVal <= 0 ) {
						$factKeyDimVal = -1;
					}
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("The Entry id is '{$factKeyDimVal}'", 1, 1, "color:purple;");
					}
				}
			}
			
			//Corregido un bug, durante el reprocesamiento de destinos de datos, la salida del proceso NO incluye el valor del SurveyKey porque no se vuelve a
			//manipular en absoluto la tabla de datos de la forma, así que $factKeyDimVal no estaba siendo asignada y se reemplazaba por -1, por lo que en esos
			//casos se utilizará $surveyKey que corresponde al Id de captura previamente almacenado en la bitácora de destinos y que es hereado por la instancia
			//de BITAMSurveyTask, pero solo en caso de no haber podido identificarse en la salida del proceso y que $surveyKey > 0. En este caso $factKeyDimVal es
			//sólo una variable local, no es la misma variable del proceso de grabado ya que este archivo pho corre en el contexto del agente y no en el contexto
			//de processSurveyFunctionsV6.php que es quien procesa la captura
			if ( $factKeyDimVal <= 0 && $surveyKey > 0 ) {
				$factKeyDimVal = $surveyKey;
			}
			//@JAPR
			
			//@JAPR 2018-02-07: Agregado el archivo de configuraciones semi-dinámico del agente para permitir variar su comportamiento sin tener que apagar la tarea de windows
			//Se podrán asignar configuraciones forzadas modificando este archivo para no tener que cambiar el código y que por servicio se puedan hacer ajustes. Originalmente
			//utilizado para activar el modo de depuración ($blnDebugAgentMode) (#E5YASV)
			if ($blnDebugAgentMode) {
				logString("DebugFileName: {$strDebugFileName}");
			}
			//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			//Esta llamada reasignará el path completo hacia el repositorio para almacenar ahí el archivo de log del procesamiento de la tarea
			GeteFormsLogPath($sRepositoryName);
			//@JAPR
			$blnOk = stripos($strResult, $strStatus);
			if ($blnOk === false) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("blnOk es false", 1, 1, "color:purple;");
				}
				
				//@JAPR 2015-01-30: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
				$strErrorDesc = "Error executing the survey task #{$aSurveyTaskID}, Error num: {$intLastCURLError}, Error: '{$strLastCURLError}'. Check the error detail & debug information in this file: $strDebugFileName";
				//En este caso hubo un error y no se logró concretar el grabado de la encuesta (errores por eBavel no se reportarían, ya que esos
				//no impiden el grabado de la encuesta, en esos casos se deberá consultar el log de upload de la misma)
				logString($strResult, $strDebugFileName, true, true, false);
				//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
				GeteFormsLogPath('');
				//@JAPR
				logString($strErrorDesc, '', true, true);
				
				//Debe marcar la tarea como fallida y eliminarla de la tabla de pendientes
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
					echo("<br>\r\n sql: ".str_ireplace('%d', $factKeyDimVal, str_ireplace('%s', stsFailed, $sqlINSERT)));
					//@JAPR
				}
				//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
				if ($aBITAMConnection->ADOConnection->Execute(str_ireplace('%d', $factKeyDimVal, str_ireplace('%s', stsFailed, $sqlINSERT)))) {
					//Si la pudo insertar en la tabla de log, entonces si la elimina de la tabla de pendientes
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n sql: {$sqlDELETE}");
					}
					$aBITAMConnection->ADOConnection->Execute($sqlDELETE);
					
					//@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
					//Después de mover la tarea a la tabla de log, crea la nueva tarea recalendarizada siempre y cuando aún no llegue al máximo de
					//intentos por procesarla
					//$objSurveyTask->TaskStatus = stsFailed;
					//$objSurveyTask->reschedule($aBITAMConnection);
					//@JAPR
				}
				else {
					//De lo contrario simplemente actualiza la tabla de pendientes para no perder la información
					$sql = "UPDATE SI_SV_SurveyTasks SET 
							TaskStatus = ".stsFailed." 
						WHERE SurveyTaskID = {$aSurveyTaskID}";
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n sql: {$sql}");
					}
					$aBITAMConnection->ADOConnection->Execute($sql);
				}
				
				//@JAPR 2015-01-30: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
				//Enviará una notificación vía EMail del error ocurrido. Generalmente si llega aquí es porque ocurrió un error por tablas/campos
				//faltantes, un estatuto SQL mal construido, referencias a archivos que no pudo encontrar, o probablemente algún error externo
				//a eForms como una desconexión de la base de datos. También es factible que sea un problema generado por eBavel
				//En cualquiera de los escenarios, el hecho de que no encontrara el texto que indica que terminó correctamente, quiere decir que
				//no llegó al final del grabado de eForms (en el caso de que hubiera algún problema con eBavel, probablemente los datos de eForms
				//si estén correctamente grabados), y no se tiene la certeza de que el error se auto-corrija automáticamente, por lo tanto NO se
				//va a forzar a reintentar el grabado de la captura sino que simplemente se notificará vía EMail de esta situación
				//En algunos casos dependiendo del problema, el propio grabado ya hubiera notificado del problema vía EMail, sin embargo se va
				//a notificar nuevamente, en esta ocasión informando de la referencia del log específico generado
				reportSurveyTaskError($aBITAMConnection, $strErrorDesc, null, $objSurveyTask);
				//@JAPR
			}
			else {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("blnOk NO es false", 1, 1, "color:purple;");
				}
				if ($blnDebugAgentMode) {
					logString($strResult, $strDebugFileName, true, true, false);
				}
				//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
				GeteFormsLogPath('');
				//@JAPR
				
				//Todo estuvo bien, debe marcar la tarea como completada y eliminarla de la tabla de pendientes
				logString($strStatus);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
					echo("<br>\r\n sql: ".str_ireplace('%d', $factKeyDimVal, str_ireplace('%s', stsCompleted, $sqlINSERT)));
					//@JAPR
				}
				//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
				if ($aBITAMConnection->ADOConnection->Execute(str_ireplace('%d', $factKeyDimVal, str_ireplace('%s', stsCompleted, $sqlINSERT)))) {
					//Si la pudo insertar en la tabla de log, entonces si la elimina de la tabla de pendientes
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("IF ejecucion correcta de insert", 1, 1, "color:purple;");
					}
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n sql: {$sqlDELETE}");
					}
					$aBITAMConnection->ADOConnection->Execute($sqlDELETE);
				}
				else {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Ejecución INcorrecta de insert se ejecutará UPDATE", 1, 1, "color:purple;");
					}
					//De lo contrario simplemente actualiza la tabla de pendientes para no perder la información
					$sql = "UPDATE SI_SV_SurveyTasks SET 
							TaskStatus = ".stsCompleted." 
						WHERE SurveyTaskID = {$aSurveyTaskID}";
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n sql: {$sql}");
					}
					$aBITAMConnection->ADOConnection->Execute($sql);
				}
			}
		}
	} catch (Exception $e) {
		logString("Error executing the survey task #{$aSurveyTaskID} (Exception): ".$e->getMessage(), '', true, true);
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n El resultado es: <br>\r\n".$strResult);
	}
	
	return true;
}

//@JAPR 2013-02-28: Agregada la notificación vía eMail de errores de captura
/* Envía un eMail al personal especializado en resolver errores de captura de eForms cuando el servicio recibe una solicitud pero por alguna
causa no puede hacer el grabado correcto. Adicionalmente se enviará un eMail al sistema de soporte, aunque en ese caso el EMail que se envía
sólo ocurre una vez por día para no saturar de tickets de soporte al sistema
*/
function reportSurveyTaskError($aRepository, $strEntryError, $theUser = null, $aSurveyTask = null) {
	if (is_null($aSurveyTask)) {
		return false;
	}
	
	@require_once("bpaemailconfiguration.inc.php");
	
	$anInstanceEmailConf = @BPAEmailConfiguration::readConfiguration();
	if(is_null($anInstanceEmailConf)) {
		return false;
	}
	
	$SMTPServer = (string) @$anInstanceEmailConf->fbm_email_server;	
	$SMTPPort = (string) @$anInstanceEmailConf->fbm_email_port;
	$SMTPUserName = (string) @$anInstanceEmailConf->fbm_email_username;
	$SMTPPassword = (string) @$anInstanceEmailConf->fbm_email_pwd;
	$SMTPEmailSource = (string) @$anInstanceEmailConf->fbm_email_source;
	if (strlen($SMTPServer) == 0 && strlen($SMTPPort) == 0 && strlen($SMTPUserName) == 0) {
		return false;
	}
	
	//@JAPR 2012-10-10: Validada la integración con eBavel, como eBavel mandará eMails, incluye primero la clase PhPMailer así que se debe validar
	//que sólo incluya eForms su versión si no lo hizo ya eBavel
	if (!class_exists("PHPMailer")) {
		@require_once("mail/class.phpmailer.php");
		
		if (!class_exists("PHPMailer")) {
			return false;
		}
	}
	//@JAPR
	
	//Indentifica las cuentas a las que enviará los EMails basado en el tipo de cuenta maestra de la que se trata (si ocurre algún error, se
	//asumirá que se debe notificar a soporte interno por lo menos para que se verifique por qué sucede este error)
	$blnSendToSupport = false;
	$blnSendToQA = false;
	$blnSendToMasterAcc = false;
	$blnSendToBitamSupport = false;
	$blnSendToUserAcc = false;
	$blnSendToKPIAdmin = false;
	$blnSendToDevelopMent = true;
	//@JAPR 2015-12-10: Corregido un bug, esta es una variable global, así que no se debe asignar aquí sino hacer referencia a ella nada mas
	global $blnTestingServer;
	//$blnTestingServer = false;
	//@JAPR
	
	//Null significa que no se pudo determinar el CustomerType, por lo tanto se asumirá que se debe notificar solo a desarrollo para que verifique
	//por qué no se pudo determinar. Es diferente a que la tabla tenga un Null en cuyo caso se va a asumir que es Free Trial
	$intCustomerType = null;
	/* Customer Types:
		-9	- NA 
		1 	- Customer 
		2	- Free Trial 
		3	- Internal 
		4	- Partner 
		5	- POC 
		6	- Partner Prospect 
		7	- Free Trial Contactado 
		8	- Customer Dead 
		9	- Partner Dead 
		10	- Delete 
		11	- Demo 
		12	- Testing
	*/
	
	$strMasterAccount = '';
	$strRepositoryName = $aSurveyTask->RepositoryName;
	$gstrOutboxFileName = $aSurveyTask->FileName;
	$sql = "SELECT b.EMail, a.CustomerType 
		FROM saas_databases a, saas_users b 
		WHERE a.UserID = b.UserID AND a.Repository = ".$aRepository->ADOConnection->Quote($strRepositoryName);
	$aRS = $aRepository->ADOConnection->Execute($sql);
    if ($aRS && !$aRS->EOF) {
	    $strMasterAccount = (string) @$aRS->fields["email"];
	    $intCustomerType = @$aRS->fields["customertype"];
	    if (is_null($intCustomerType)) {
	    	//Se tiene que revisar por qué del error, así que envía a desarrollo
	    	$intCustomerType = cmrtFreeTrial;
	    }
	    else {
	    	//Se convierte a numérico y se desactiva el envío a desarrollo
	    	$intCustomerType = (int) $intCustomerType;
			$blnSendToDevelopMent = false;
	    }
    }
    
    //Según el tipo de Customer habilita el envío de EMail a diferentes cuentas
    switch ($intCustomerType) {
    	case cmrtInternal:
    		//Las cuentas Internal deben ser dirigidas al responsable de la cuenta que generalmente es el Administrador y a soporte de Bitam, 
    		//ya que no son tan prioritarias como los clientes así que no generarán ticket de KPIOnline (ellos lo podrían generar automáticamente, 
    		//pero no se considera prioritario como para hacerlo automático)
    		$blnSendToMasterAcc = true;
    		$blnSendToBitamSupport = true;
    		break;
		
    	case cmrtCustomerDead:
    	case cmrtPartnerDead:
    		//Las cuentas de Customer o Partner Dead teoricamente ya no importan, sin embargo se notificará a los encargados de KPIOnline por si
    		//se tratara de algún tipo de error ya que continua recibiendo peticiones de eForms y tal vez se necesite cambiar el Status o darla
    		//de baja en definitiva
    		$blnSendToKPIAdmin = true;
    		break;	
    		
    	case cmrtDelete:
    		//Las cuentas eliminadas no tienen por qué reportar, así que termina el proceso
    		return false;
    		break;
    		
    	case cmrtTesting:
    		//Las cuentas de testing se deben notificar exclusivamente al equipo de Control de Calidad. Generalmente la cuenta Administradora no
    		//importa, pero si la cuenta específica del tester
    		$blnSendToQA = true;
    		$blnSendToUserAcc = true;
    		break;
    		
    	//Se asumirá cualquier tipo no identificado como si fuera de máxima prioridad, esto para no cometer errores e ignorar a cuentas importantes
    	//que pudieran estar estrenando un CustomerType recientemente definido. Estas cuentas no notifican al usuario que captura, solo al
    	//responsable que es la cuenta Administradora (Generan un ticket de KPIOnline automáticamente)
    	default:
			$blnSendToSupport = true;
			$blnSendToMasterAcc = true;
    		break;
    }
    
    if ($blnTestingServer) {
		//$blnSendToSupport = false;
		$blnSendToQA = true;
		//$blnSendToMasterAcc = false;
		//$blnSendToBitamSupport = false;
		//$blnSendToUserAcc = false;
		//$blnSendToDevelopMent = false;
		//$blnSendToKPIAdmin = false;
		//$blnSendToDevelopMent = false;
    }
    
	//Obtiene los datos específicos del error. Para haber llegado aqui es que se trata de un error que pudo ser atrapado por eForms y que generó
	//un log y respaldó los archivos para restaurarlo, así que tenemos los datos exactamente de que archivos son y el detalle del error
	$streFormsService = $aSurveyTask->FormsPath;
	$strUserAccount = $aSurveyTask->User;
	$strSurveyDate = $aSurveyTask->SurveyDate;
	$intSurveyID = $aSurveyTask->SurveyID;
	$intAgendaID = $aSurveyTask->AgendaID;
	$strEFormsVersion = ESURVEY_SERVICE_VERSION;
	$strNav = $aSurveyTask->UserAgent;
	$strLogFileName = $aSurveyTask->LogFileName;
	$strSurveyName = $intSurveyID;
	//$surveyInstance = @BITAMSurvey::NewInstanceWithID($aRepository, $intSurveyID);
	//if (!is_null($surveyInstance)) {
	//	$strSurveyName .= '- '.$surveyInstance->SurveyName;
	//}
	$strAgendaName = $intAgendaID;
	//$agendaInstance = @BITAMSurveyAgenda::NewInstanceWithID($aRepository, $intAgendaID);
	//if (!is_null($agendaInstance)) {
	//	$strAgendaName = '- '.$agendaInstance->AgendaName;
	//}
	$appVersion = (float) $aSurveyTask->FormsVersion;
	
	$mail = new PHPMailer();
	$mail->Mailer = "smtp";
	$mail->Host = $SMTPServer;
	$mail->Port = $SMTPPort;
	$mail->SMTPAuth = TRUE;
	//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
	//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
	$mail->SMTPAutoTLS = false;
	//$mail->SMTPDebug = false;
	//$mail->Debugoutput = 'echo';
	//$mail->SMTPSecure = '';
	//$mail->CharSet = 'UTF-8';
	//@JAPR
	$mail->Username = $SMTPUserName;
	$mail->Password = $SMTPPassword;
	$mail->From = $SMTPEmailSource;
	$mail->IsHTML(TRUE);
	//@JAPR 2013-03-26: Reemplazados los <enters> porque en el EMail no se veía bien
	$strEntryError = str_ireplace("\r\n", "\r\n<br><br>", $strEntryError);
	//@JAPR 2015-11-23: Agregado el nombre del producto que reporta
	$strProductName = (($gbIsGeoControl)?"GeoControl":"eForms");
	//@JAPR
	$mail->Subject = $strProductName.": ".str_ireplace("{var2}", $strUserAccount, str_ireplace("{var1}", $strMasterAccount, translate("An error has been produced while saving a form for the {var1} account with the {var2} user")));
	//OMMC 2015-10-29: se añade decode al subject y htmlentities al body para corregir acentos
	$mail->Subject = utf8_decode($mail->Subject);
	$mail->Body = "
<h2>".translate("Error made on the following form during saving time").".</h2>
<b>".translate("Service release").": </b>{$strEFormsVersion}
<br>
<b>".translate("App version").": </b>{$appVersion}
<br>
<b>".translate("Mobile device").": </b>{$strNav}
<br>
<b>".translate("Account").": </b>{$strMasterAccount}
<br>
<b>".translate("User").": </b>{$strUserAccount}
<br>
<b>".translate("Repository").": </b>{$strRepositoryName}
<br>
<b>".translate("Survey").": </b>{$strSurveyName}
<br>
<b>".translate("Agenda").": </b>{$strAgendaName}
<br>
<b>".translate("Entry date").": </b>{$strSurveyDate}
<br>
<b>".translate("Service used").": </b>{$streFormsService}
<br>
<br>
<b>".translate("Error Detail").": </b>
<br>
{$strEntryError}
<br>
";
	$mail->Body = utf8_decode($mail->Body);

	//Agrega el archivo log de errores
	//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strFullLogFileName = GeteFormsLogPath($strRepositoryName).$strLogFileName;
	//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
	GeteFormsLogPath('');
	//@JAPR
	clearstatcache();
	if (file_exists($strFullLogFileName)) {
		$mail->AddAttachment($strFullLogFileName, $strLogFileName);
	}
	
	//@JAPR 2015-12-10: Removidas las cuentas del equipo de QA a petición de Claudia, sólo recibirán notificaciones si la cuenta es de testing
	//@$mail->AddAddress("jpuente@bitam.com", "Juan Puente");
	@$mail->AddAddress("psanchez@bitam.com", "Pedro Sanchez");
	//@$mail->AddAddress("cgil@bitam.com", "Claudia Gil");
	@$mail->AddAddress("jgonzalez@bitam.com", "J Gonzalez");
	@$mail->AddAddress("glopez@bitam.com", "Gustavo López");
	@$mail->AddAddress("aceballos@bitam.com", "Adrian Ceballos");
	//@$mail->AddAddress("jagonzalez@bitam.com", "Jose A Gonzalez");
	//@$mail->AddAddress("jcuellar@bitam.com", "J Cuellar");
	//@$mail->AddAddress("csanchez@bitam.com", "Carlos Sanchez");
	
	//Agrega las direcciones de las personas encargadas de resolver problemas de eForms
	if ($blnSendToDevelopMent) {
		@$mail->AddAddress(eFormsDeveloper1, "{$strProductName} Developer 1");
		@$mail->AddAddress(eFormsDeveloper2, "{$strProductName} Developer 2");
		@$mail->AddAddress(eFormsDeveloper3, "{$strProductName} Developer 3");
		//@JAPR 2015-12-08: Agregada la lista de distribución para desarrollo
		@$mail->AddAddress(eFormsDevelopmentTeam, "{$strProductName} Development team");
	}
	if ($blnSendToKPIAdmin) {
		@$mail->AddAddress(KPIAdmin1, "KPI Admin 1");
		@$mail->AddAddress(KPIAdmin2, "KPI Admin 2");
		@$mail->AddAddress(KPIOnlineAdminTeam, "{$strProductName} Administrators");
	}
	if ($blnSendToQA) {
		@$mail->AddAddress(QAStaff1, "QA Staff 1");
		@$mail->AddAddress(QAStaff2, "QA Staff 2");
		@$mail->AddAddress(QAStaff3, "QA Staff 3");
		@$mail->AddAddress(QAStaff4, "QA Staff 4");
		@$mail->AddAddress(QAStaff5, "QA Staff 5");
		//@JAPR 2015-12-08: Agregada la lista de distribución para calidad
		@$mail->AddAddress(eFormsQualityTeam, "Quality Staff");
	}
	if ($blnSendToBitamSupport) {
		@$mail->AddAddress(SupportStaff1, "Support Staff 1");
		@$mail->AddAddress(SupportStaff2, "Support Staff 2");
	}
	//@JAPR 2016-01-04: Deshabilitado el reporte de errores a las cuentas de los clientes, sólo se enviará a soporte o el resto de destinos según Customer Type (#UM9ZJU)
	//Este cambio se pidió a raiz que los clientes no desean recibir notificaciones de error, de manera permanente se notificará al equipo de soporte así que
	//eso cubrirá todo posible problema
	/*if (trim($strUserAccount) != '' && strpos($strUserAccount, '@') !== false) {
		if ($blnSendToUserAcc) {
			@$mail->AddAddress($strUserAccount, "{$strProductName} User");
		}
	}
	if (trim($strMasterAccount) != '' && strpos($strMasterAccount, '@') !== false) {
		if ($blnSendToMasterAcc) {
			@$mail->AddAddress($strMasterAccount, "{$strProductName} Account Admin");
		}
	}*/
	if ($blnSendToSupport) {
		//@JAPR 2015-11-19: Esta cuenta ya no está funcional, se remueve este reporte por ahora
		//@JAPR 2015-12-07: Modificada la cuenta de soporte ya que la original fue dada de baja (#NVL2WM)
		@$mail->AddAddress(KPIOnlineSupportTeam, "KPI Online Support Team");
		@$mail->AddAddress(SupportStaff1, "Support Staff 1");
		@$mail->AddAddress(SupportStaff2, "Support Staff 2");
	}
	//Siempre se manda a internal para que quede registro del evento
	@$mail->AddAddress(InternalEForms, "{$strProductName}");
	//@JAPR 2014-02-20: Agregado el reporte de error en las notificaciones vía EMail
	//@JAPR 2016-01-04: Deshabilitado el reporte de errores a las cuentas de los clientes, sólo se enviará a soporte o el resto de destinos según Customer Type (#UM9ZJU)
	//Este cambio se pidió a raiz que los clientes no desean recibir notificaciones de error, de manera permanente se notificará al equipo de soporte así que
	//eso cubrirá todo posible problema
	//AddCuentaCorreoAltToMail($theUser, $mail, $appVersion);
	//@JAPR
	
	//@$mail->Send();
	if (!@$mail->Send()) {
		//Enviamos al log el error generado
		global $SendMailLogFile;
		
		$aLogString = "\r\n\r\n Error sending report: ".date('Y-m-d H:i:s');
		$aLogString .= "\r\n".@$mail->ErrorInfo;
		$aLogString .= "\r\n Capture detail: \r\n".@$mail->Body."\r\n";
		//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		//No se estaba grabando el log de errores de envíos de correo
		@error_log($aLogString, 3, GeteFormsLogPath().((string) @$SendMailLogFile));
		//@JAPR
	}
	//@JAPR
	
	return true;
}

//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
/* Envía un eMail al personal especializado en resolver errores de sincronización de DataSources de eForms cuando el servicio recibe una solicitud pero por alguna
causa no puede hacer el grabado correcto. Adicionalmente se enviará un eMail al sistema de soporte, aunque en ese caso el EMail que se envía
sólo ocurre una vez por día para no saturar de tickets de soporte al sistema
*/
function reportDataSourceTaskError($aRepository, $strEntryError, $theUser = null, $oTaskData = array()) {
	global $gbIsGeoControl;
	
	@require_once("bpaemailconfiguration.inc.php");
	
	$anInstanceEmailConf = @BPAEmailConfiguration::readConfiguration();
	if(is_null($anInstanceEmailConf)) {
		return false;
	}
	
	$SMTPServer = (string) @$anInstanceEmailConf->fbm_email_server;	
	$SMTPPort = (string) @$anInstanceEmailConf->fbm_email_port;
	$SMTPUserName = (string) @$anInstanceEmailConf->fbm_email_username;
	$SMTPPassword = (string) @$anInstanceEmailConf->fbm_email_pwd;
	$SMTPEmailSource = (string) @$anInstanceEmailConf->fbm_email_source;
	if (strlen($SMTPServer) == 0 && strlen($SMTPPort) == 0 && strlen($SMTPUserName) == 0) {
		return false;
	}
	
	//@JAPR 2012-10-10: Validada la integración con eBavel, como eBavel mandará eMails, incluye primero la clase PhPMailer así que se debe validar
	//que sólo incluya eForms su versión si no lo hizo ya eBavel
	if (!class_exists("PHPMailer")) {
		@require_once("mail/class.phpmailer.php");
		
		if (!class_exists("PHPMailer")) {
			return false;
		}
	}
	//@JAPR
	
	//Indentifica las cuentas a las que enviará los EMails basado en el tipo de cuenta maestra de la que se trata (si ocurre algún error, se
	//asumirá que se debe notificar a soporte interno por lo menos para que se verifique por qué sucede este error)
	$blnSendToSupport = false;
	$blnSendToQA = false;
	$blnSendToMasterAcc = false;
	$blnSendToBitamSupport = false;
	$blnSendToUserAcc = false;
	$blnSendToKPIAdmin = false;
	$blnSendToDevelopMent = true;
	//@JAPR 2015-12-10: Corregido un bug, esta es una variable global, así que no se debe asignar aquí sino hacer referencia a ella nada mas
	global $blnTestingServer;
	//$blnTestingServer = false;
	//@JAPR
	
	//Null significa que no se pudo determinar el CustomerType, por lo tanto se asumirá que se debe notificar solo a desarrollo para que verifique
	//por qué no se pudo determinar. Es diferente a que la tabla tenga un Null en cuyo caso se va a asumir que es Free Trial
	$intCustomerType = null;
	/* Customer Types:
		-9	- NA 
		1 	- Customer 
		2	- Free Trial 
		3	- Internal 
		4	- Partner 
		5	- POC 
		6	- Partner Prospect 
		7	- Free Trial Contactado 
		8	- Customer Dead 
		9	- Partner Dead 
		10	- Delete 
		11	- Demo 
		12	- Testing
	*/
	
	$strMasterAccount = '';
	$strRepositoryName = (string) @$oTaskData['RepositoryName'];
	$sql = "SELECT b.EMail, a.CustomerType 
		FROM saas_databases a, saas_users b 
		WHERE a.UserID = b.UserID AND a.Repository = ".$aRepository->ADOConnection->Quote($strRepositoryName);
	$aRS = $aRepository->ADOConnection->Execute($sql);
    if ($aRS && !$aRS->EOF) {
	    $strMasterAccount = (string) @$aRS->fields["email"];
	    $intCustomerType = @$aRS->fields["customertype"];
	    if (is_null($intCustomerType)) {
	    	//Se tiene que revisar por qué del error, así que envía a desarrollo
	    	$intCustomerType = cmrtFreeTrial;
	    }
	    else {
	    	//Se convierte a numérico y se desactiva el envío a desarrollo
	    	$intCustomerType = (int) $intCustomerType;
			$blnSendToDevelopMent = false;
	    }
    }
    
    //Según el tipo de Customer habilita el envío de EMail a diferentes cuentas
    switch ($intCustomerType) {
    	case cmrtInternal:
    		//Las cuentas Internal deben ser dirigidas al responsable de la cuenta que generalmente es el Administrador y a soporte de Bitam, 
    		//ya que no son tan prioritarias como los clientes así que no generarán ticket de KPIOnline (ellos lo podrían generar automáticamente, 
    		//pero no se considera prioritario como para hacerlo automático)
    		$blnSendToMasterAcc = true;
    		$blnSendToBitamSupport = true;
    		break;
		
    	case cmrtCustomerDead:
    	case cmrtPartnerDead:
    		//Las cuentas de Customer o Partner Dead teoricamente ya no importan, sin embargo se notificará a los encargados de KPIOnline por si
    		//se tratara de algún tipo de error ya que continua recibiendo peticiones de eForms y tal vez se necesite cambiar el Status o darla
    		//de baja en definitiva
    		$blnSendToKPIAdmin = true;
    		break;	
    		
    	case cmrtDelete:
    		//Las cuentas eliminadas no tienen por qué reportar, así que termina el proceso
    		return false;
    		break;
    		
    	case cmrtTesting:
    		//Las cuentas de testing se deben notificar exclusivamente al equipo de Control de Calidad. Generalmente la cuenta Administradora no
    		//importa, pero si la cuenta específica del tester
    		$blnSendToQA = true;
    		$blnSendToUserAcc = true;
    		break;
    		
    	//Se asumirá cualquier tipo no identificado como si fuera de máxima prioridad, esto para no cometer errores e ignorar a cuentas importantes
    	//que pudieran estar estrenando un CustomerType recientemente definido. Estas cuentas no notifican al usuario que captura, solo al
    	//responsable que es la cuenta Administradora (Generan un ticket de KPIOnline automáticamente)
    	default:
			$blnSendToSupport = true;
			$blnSendToMasterAcc = true;
    		break;
    }
    
    if ($blnTestingServer) {
		//$blnSendToSupport = false;
		$blnSendToQA = true;
		//$blnSendToMasterAcc = false;
		//$blnSendToBitamSupport = false;
		//$blnSendToUserAcc = false;
		//$blnSendToDevelopMent = false;
		//$blnSendToKPIAdmin = false;
		//$blnSendToDevelopMent = false;
    }
	//@JAPR 2015-11-19: Por ahora se habilita a Desarrollo para efectos de pruebas
	//$blnSendToDevelopMent = true;
    
	//Obtiene los datos específicos del error. Para haber llegado aqui es que se trata de un error que pudo ser atrapado por eForms y que generó
	//un log y respaldó los archivos para restaurarlo, así que tenemos los datos exactamente de que archivos son y el detalle del error
	$streFormsService = (string) @$oTaskData['FormsPath'];
	$strEFormsVersion = ESURVEY_SERVICE_VERSION;
	$strLogFileName = (string) @$oTaskData['LogFileName'];
	
	$mail = new PHPMailer();
	$mail->Mailer = "smtp";
	$mail->Host = $SMTPServer;
	$mail->Port = $SMTPPort;
	$mail->SMTPAuth = TRUE;
	//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
	//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
	$mail->SMTPAutoTLS = false;
	//$mail->SMTPDebug = false;
	//$mail->Debugoutput = 'echo';
	//$mail->SMTPSecure = '';
	//$mail->CharSet = 'UTF-8';
	//@JAPR
	$mail->Username = $SMTPUserName;
	$mail->Password = $SMTPPassword;
	$mail->From = $SMTPEmailSource;
	$mail->IsHTML(TRUE);
	//@JAPR 2013-03-26: Reemplazados los <enters> porque en el EMail no se veía bien
	$strEntryError = str_ireplace("\r\n", "\r\n<br><br>", $strEntryError);
	//@JAPR 2015-11-23: Agregado el nombre del producto que reporta
	$strProductName = (($gbIsGeoControl)?"GeoControl":"eForms");
	//@JAPR
	$mail->Subject = $strProductName.": ".translate("An error has been made during catalog synchronization")." ({$strMasterAccount})";
	//OMMC 2015-10-29: se añade decode al subject y htmlentities al body para corregir acentos
	$mail->Subject = utf8_decode($mail->Subject);
	$mail->Body = "
<h2>".translate("The following catalog synchronization mistakes have occurred").".</h2>
<b>".translate("Service release").": </b>{$strEFormsVersion}
<br>
<b>".translate("User").": </b>{$strMasterAccount}
<br>
<b>".translate("Repository").": </b>{$strRepositoryName}
<br>
<b>".translate("Service used").": </b>{$streFormsService}
<br>
<br>
<b>".translate("Error Detail").": </b>
<br>
{$strEntryError}
<br>
";
	$mail->Body = utf8_decode($mail->Body);

	//Agrega el archivo log de errores
	//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$strFullLogFileName = GeteFormsLogPath($strRepositoryName).$strLogFileName;
	//Esta llamada limpiará la porción del repositorio para volver a grabar a partir de la ruta base de logs simplemente, pues ya es un registro en el log del agente solamente
	GeteFormsLogPath('');
	//@JAPR
	clearstatcache();
	if (file_exists($strFullLogFileName)) {
		$mail->AddAttachment($strFullLogFileName, $strLogFileName);
	}
	
	//@JAPR 2015-12-10: Removidas las cuentas del equipo de QA a petición de Claudia, sólo recibirán notificaciones si la cuenta es de testing
	//@$mail->AddAddress("jpuente@bitam.com", "Juan Puente");
	@$mail->AddAddress("psanchez@bitam.com", "Pedro Sanchez");
	//@$mail->AddAddress("cgil@bitam.com", "Claudia Gil");
	@$mail->AddAddress("jgonzalez@bitam.com", "J Gonzalez");
	@$mail->AddAddress("glopez@bitam.com", "Gustavo López");
	@$mail->AddAddress("aceballos@bitam.com", "Adrian Ceballos");
	//@$mail->AddAddress("jagonzalez@bitam.com", "Jose A Gonzalez");
	//@$mail->AddAddress("jcuellar@bitam.com", "J Cuellar");
	//@$mail->AddAddress("csanchez@bitam.com", "Carlos Sanchez");
	
	//Agrega las direcciones de las personas encargadas de resolver problemas de eForms
	if ($blnSendToDevelopMent) {
		@$mail->AddAddress(eFormsDeveloper1, "{$strProductName} Developer 1");
		@$mail->AddAddress(eFormsDeveloper2, "{$strProductName} Developer 2");
		@$mail->AddAddress(eFormsDeveloper3, "{$strProductName} Developer 3");
		//@JAPR 2015-12-08: Agregada la lista de distribución para desarrollo
		@$mail->AddAddress(eFormsDevelopmentTeam, "{$strProductName} Development team");
	}
	if ($blnSendToKPIAdmin) {
		@$mail->AddAddress(KPIAdmin1, "KPI Admin 1");
		@$mail->AddAddress(KPIAdmin2, "KPI Admin 2");
		@$mail->AddAddress(KPIOnlineAdminTeam, "{$strProductName} Administrators");
	}
	if ($blnSendToQA) {
		@$mail->AddAddress(QAStaff1, "QA Staff 1");
		@$mail->AddAddress(QAStaff2, "QA Staff 2");
		@$mail->AddAddress(QAStaff3, "QA Staff 3");
		@$mail->AddAddress(QAStaff4, "QA Staff 4");
		@$mail->AddAddress(QAStaff5, "QA Staff 5");
		//@JAPR 2015-12-08: Agregada la lista de distribución para calidad
		@$mail->AddAddress(eFormsQualityTeam, "Quality Staff");
	}
	if ($blnSendToBitamSupport) {
		@$mail->AddAddress(SupportStaff1, "Support Staff 1");
		@$mail->AddAddress(SupportStaff2, "Support Staff 2");
	}
	//@JAPR 2016-01-04: Deshabilitado el reporte de errores a las cuentas de los clientes, sólo se enviará a soporte o el resto de destinos según Customer Type (#UM9ZJU)
	//Este cambio se pidió a raiz que los clientes no desean recibir notificaciones de error, de manera permanente se notificará al equipo de soporte así que
	//eso cubrirá todo posible problema
	/*if (trim($strMasterAccount) != '' && strpos($strMasterAccount, '@') !== false) {
		if ($blnSendToMasterAcc) {
			@$mail->AddAddress($strMasterAccount, "{$strProductName} Account Admin");
		}
	}*/
	if ($blnSendToSupport) {
		//@JAPR 2015-11-19: Esta cuenta ya no está funcional, se remueve este reporte por ahora
		//@JAPR 2015-12-07: Modificada la cuenta de soporte ya que la original fue dada de baja (#NVL2WM)
		@$mail->AddAddress(KPIOnlineSupportTeam, "KPI Online Support Team");
		@$mail->AddAddress(SupportStaff1, "Support Staff 1");
		@$mail->AddAddress(SupportStaff2, "Support Staff 2");
	}
	//Siempre se manda a internal para que quede registro del evento
	@$mail->AddAddress(InternalEForms, "{$strProductName}");
	//@JAPR 2014-02-20: Agregado el reporte de error en las notificaciones vía EMail
	//@JAPR 2016-01-04: Deshabilitado el reporte de errores a las cuentas de los clientes, sólo se enviará a soporte o el resto de destinos según Customer Type (#UM9ZJU)
	//Este cambio se pidió a raiz que los clientes no desean recibir notificaciones de error, de manera permanente se notificará al equipo de soporte así que
	//eso cubrirá todo posible problema
	//AddCuentaCorreoAltToMail($theUser, $mail);
	//@JAPR
	
	//@$mail->Send();
	if (!@$mail->Send()) {
		//Enviamos al log el error generado
		global $SendMailLogFile;
		
		$aLogString = "\r\n\r\n Error sending report: ".date('Y-m-d H:i:s');
		$aLogString .= "\r\n".@$mail->ErrorInfo;
		$aLogString .= "\r\n Capture detail: \r\n".@$mail->Body."\r\n";
		//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		//No se estaba grabando el log de errores de envíos de correo
		@error_log($aLogString, 3, GeteFormsLogPath().((string) @$SendMailLogFile));
		//@JAPR
	}
	//@JAPR
	
	return true;
}

//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
/* Carga la configuración del uso de múltiples passwords en KPIOnline. Sólo funciona con una conexión hacia KPIOnline7->FBM000 (Repositorio de KPIOnline) */
function GetMultiPasswordEnabledState($aBITAMConnection, $sRepositoryName, $kpiUser = null) {
	$blnMultiPass = false;
	
	//Si no se recibió el usuario, se asume que se debe utilizar a la cuenta maestra del repositorio, así que se carga el UserID de dicha cuenta
	$intSAASUserID = 0;
	if (!$kpiUser) {
		$sql = "SELECT B.Email, A.UserID 
			FROM saas_databases A 
				INNER JOIN saas_users B ON A.UserID = B.UserID 
			WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote($sRepositoryName);
		$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$intSAASUserID = (int) @$aRS->fields["userid"];
			$kpiUser = (string) @$aRS->fields["email"];
		}
	}
	
	$aSQL = "SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = -2 AND settingUserID = -2 AND settingValue > 0";
	//Si se recibió un repositorio, primero identifica el DataBaseID correspondiente
	$strProjectName = $sRepositoryName;
	if ($strProjectName) {
		$intDBID = 0;
		$sql = "SELECT c.DatabaseID 
			FROM saas_databases c 
			WHERE c.Repository = ".$aBITAMConnection->ADOConnection->Quote($strProjectName);
		$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$intDBID = (int) @$aRS->fields["databaseid"];
		}
		
		if ($intDBID) {
			$aSQL .= "
				UNION 
				SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = {$intDBID} AND settingUserID = -2 AND settingValue > 0";
		}
	}
	
	//Si se recibió el EMail (que todo request siempre lo debe recibir) primero identifica el UserID correspondiente
	if ($kpiUser) {
		if ($intSAASUserID <= 0) {
			$sql = "SELECT UserID 
				FROM saas_users 
				WHERE Email = ".$aBITAMConnection->ADOConnection->Quote($kpiUser);
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				$intSAASUserID = (int) @$aRS->fields["userid"];
			}
		}
		
		if ($intSAASUserID) {
			$aSQL .= "
				UNION 
				SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = -2 AND settingUserID = {$intSAASUserID} AND settingValue > 0";
		}
	}
	
	$aRS = $aBITAMConnection->ADOConnection->Execute($aSQL);
	if ($aRS && !$aRS->EOF) {
		$blnMultiPass = (bool) @$aRS->fields["settingvalue"];
	}
	
	return $blnMultiPass;
}

/* Dado un repositorio específico, identifica si se encuentra habilitado el soporte de múltiples passwords y regresa el password específico asignado para este repositorio, o null en
caso de no haber un password asignado (vacío NO es un password válido, así que en ese caso regresará null también).
Sólo funciona con una conexión hacia KPIOnline7->FBM000 (Repositorio de KPIOnline)
Si se especifica el parámetro $kpiUser + $bGetUserPassword, entonces NO se asumirá que se bucará el password de la cuenta maestra del repositorio sino el password de la cuenta indicada 
dentro de ese repositorio. El parámetro $kpiUser sin embargo por si sólo se usará para validar si está o no activo el esquema de múlti passwords para ese usuario específicamente
*/
function GetMultiPasswordForRepository($aBITAMConnection, $sRepositoryName, $blnMultiPass = null, $kpiUser = null, $bGetUserPassword = false) {
	$strMultiPassword = null;
	
	//Si no se recibió el parámetro para especificar si está o no habilitado el soporte de múltiples passwords, entonces lo debe cargar antes de obtener el password
	if (is_null($blnMultiPass)) {
		$blnMultiPass = GetMultiPasswordEnabledState($aBITAMConnection, $sRepositoryName, $kpiUser);
	}
	
	if ($blnMultiPass) {
		$strProjectName = $sRepositoryName;
		$intDBID = 0;
		if (strlen($strProjectName) > 0) {
			$strProjectName = strtolower($strProjectName);
			$sql = "SELECT c.DatabaseID 
				FROM saas_databases c 
				WHERE c.Repository = ".$aBITAMConnection->ADOConnection->Quote($strProjectName);
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				$intDBID = (int) @$aRS->fields["databaseid"];
			}
			
			if ($intDBID) {
				if ($kpiUser) {
					$sql = "SELECT B.AccPassword 
						FROM saas_dbxusr B, saas_databases A, saas_users C 
						WHERE C.Email = ".$aBITAMConnection->ADOConnection->Quote($kpiUser)." 
							AND B.UserID = C.UserID 
							AND B.DatabaseID = A.DatabaseID AND A.DatabaseID = {$intDBID}";
				}
				else {
					//Obtiene el password específico del usuario Maestro del repositorio, ya que él es quien se encarga de este tipo de tareas
					$sql = "SELECT B.AccPassword 
						FROM saas_dbxusr B, saas_databases A 
						WHERE B.DatabaseID = A.DatabaseID AND B.UserID = A.UserID AND A.DatabaseID = {$intDBID}";
				}
				
				$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					$strkpiPass = (string) @$aRS->fields["accpassword"];
					if ($strkpiPass) {
						$strMultiPassword = BITAMDecryptPassword($strkpiPass);
					}
				}
			}
		}
	}
	
	return $strMultiPassword;
}
//@JAPR
?>