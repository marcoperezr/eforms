<?php 

require_once("repository.inc.php");
//require_once("indicator.inc.php");
require_once("indicatorgroupindicator.inc.php");

class BITAMIndicatorGroupCollection extends BITAMCollection
{
	function __construct($aRepository, $anArrayOfIndicatorGroupIDs)
	{
		BITAMCollection::__construct($aRepository);

		$filter = "";
		if (!is_null($anArrayOfIndicatorGroupIDs))
		{
			switch (count($anArrayOfIndicatorGroupIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "and t1.CLA_GPO_IND = ".((int) $anArrayOfIndicatorGroupIDs[0]);
					break;
				default:
					foreach ($anArrayOfIndicatorGroupIDs as $aIndicatorGroupID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aIndicatorGroupID; 
					}
					if ($filter != "")
					{
						$filter = "and t1.CLA_GPO_IND IN (".$filter.")";
					}
					break;
			}
		}
		
		if ($filter == "")
		{
			$filter = "and t1.CLA_GPO_IND > -1";
		}		

		$sql = 
		"SELECT t1.CLA_GPO_IND AS IndicatorGroupID, ".
		"		t1.NOM_GPO_IND AS IndicatorGroupName, ".
		"		t2.NOM_CORTO AS OwnerName ".
		"FROM 	SI_GPO_IND t1, SI_USUARIO t2 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER ".$filter." ".
		"	and t1.NOM_GPO_IND <> 'Dummy' ";		
		
// @EA 2010-02-04 Checar en tabla de modelos deshabilitados 
		if (InModeSAAS())
		{
			if ($this->Repository->ADOConnection->Execute('select count(CLA_CONCEPTO) from SI_CONCEPTO_DESHAB'))
			{
				$sqlGruposDeIndicadores = 'select LLAVES as grupo_indicador_id from SI_CONCEPTO where not LLAVES is NULL and RI_DEFINITION = 6 and CLA_CONCEPTO in ( select CLA_CONCEPTO from SI_CONCEPTO_DESHAB )';
				$aRSGruposDeIndicadores = $this->Repository->ADOConnection->Execute($sqlGruposDeIndicadores);
				if ($aRSGruposDeIndicadores)
				{
					$sListaGruposDeIndicadoresDeshabilitados = '0';
					while (!$aRSGruposDeIndicadores->EOF)
					{
						$sListaGruposDeIndicadoresDeshabilitados .= ','.trim($aRSGruposDeIndicadores->fields['grupo_indicador_id']);
						$aRSGruposDeIndicadores->MoveNext();
					}
					$sql .=	' and NOT t1.CLA_GPO_IND in ( '.$sListaGruposDeIndicadoresDeshabilitados.' )';
				}
				
			}
		}
//		
		$sql .= ' ORDER BY 2';
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_GPO_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$this->Collection[] = BITAMIndicatorGroup::NewIndicatorGroupFromRS($this->Repository, $aRS);
			$aRS->MoveNext();
		}
	}

	static function NewIndicatorGroupCollection($aRepository, $anArrayOfIndicatorGroupIDs = null)
	{
		return new BITAMIndicatorGroupCollection($aRepository, $anArrayOfIndicatorGroupIDs);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		return BITAMIndicatorGroupCollection::NewIndicatorGroupCollection($aRepository);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Image()
	{
		return '<img src="images/grupoindicadores.gif">';
	}
	
	function get_Title()
	{
		return translate("Indicator Models");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=IndicatorGroupCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=IndicatorGroup";
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'IndicatorGroupID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IndicatorGroupName";
		$aField->Title = translate("Indicator Model");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[$aField->Name] = $aField;
		


		if (!InModeSAAS())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "OwnerName";
			$aField->Title = translate("Owner");
			$aField->Type = "String";
			$aField->Size = 15;		
			$myFields[$aField->Name] = $aField;				
		}
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		// @EA 2005-06-22
		$RepositoryType = $_SESSION["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			$RepositoryType = $_COOKIE["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			// Produccion
			$RepositoryType = 2;		

		if ($RepositoryType == 1)
			return true;
		else 
			return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
}

class BITAMIndicatorGroup extends BITAMObject
{
	public $IndicatorGroupID;
	public $IndicatorGroupName;
	public $OwnerName;
// @EA 2006-03-3	
	public $SecurityLevel;
	public $SecurityFilter;
	public $IndicatorID;
	public $ModelID;
//	
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->IndicatorGroupID = -1;
		$this->IndicatorGroupName = "";

		// Obtenemos el nombre del usuario logeado, primero lo tomamos de la sesion
		//@JAPR 2015-07-10: Corregido un bug, estas clases deben usar la variable de sesión de eForms
		$UserNameOwner = @$_SESSION["PABITAM_UserName"];
		// Si no existe en la sesion, lo tomamos de la cookie			
		if (is_null($UserNameOwner))
			$UserNameOwner = @$_COOKIE["BITAM_UserName"];
		// Si aun asi es nulo, lo inicializamos en cero
		if (is_null($UserNameOwner))
			$UserNameOwner = "SUPERVISOR";		

		$this->OwnerName = $UserNameOwner;
		
	}

	static function NewIndicatorGroup($aRepository)
	{
		return new BITAMIndicatorGroup($aRepository);
	}

	static function NewIndicatorGroupWithID($aRepository, $aIndicatorGroupID)
	{
		$anInstance = null;
		if (((int) $aIndicatorGroupID) < 0)
		{
			return $anInstance;
		}
		$sql = 
		"SELECT t1.CLA_GPO_IND AS IndicatorGroupID".
		"		, t1.NOM_GPO_IND AS IndicatorGroupName".
		"		, t2.NOM_CORTO OwnerName ".
		"FROM 	SI_GPO_IND t1, SI_USUARIO t2 ".
		"WHERE 	t2.CLA_USUARIO = t1.CLA_OWNER and t1.CLA_GPO_IND = ".((int) $aIndicatorGroupID);
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_GPO_IND table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMIndicatorGroup::NewIndicatorGroupFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewIndicatorGroupFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMIndicatorGroup::NewIndicatorGroup($aRepository);
		$anInstance->IndicatorGroupID = (int) $aRS->fields["indicatorgroupid"];
		$anInstance->IndicatorGroupName = rtrim($aRS->fields["indicatorgroupname"]);
		$anInstance->OwnerName = $aRS->fields["ownername"];
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("IndicatorGroupID", $aHTTPRequest->POST))
		{
			$aIndicatorGroupID = $aHTTPRequest->POST["IndicatorGroupID"];
			if (is_array($aIndicatorGroupID))
			{
				$aCollection = BITAMIndicatorGroupCollection::NewIndicatorGroupCollection($aRepository, $aIndicatorGroupID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
					
					if (!($anInstanceToRemove->Message == ""))
					{
						$anInstanceToRemove->Message = "CanŽt delete this indicators models because was used in base models";
						return $anInstanceToRemove;
					}
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMIndicatorGroup::NewIndicatorGroupWithID($aRepository, (int) $aIndicatorGroupID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMIndicatorGroup::NewIndicatorGroup($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("IndicatorGroupID", $aHTTPRequest->GET))
		{
			$aIndicatorGroupID = $aHTTPRequest->GET["IndicatorGroupID"];
			$anInstance = BITAMIndicatorGroup::NewIndicatorGroupWithID($aRepository, (int) $aIndicatorGroupID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMIndicatorGroup::NewIndicatorGroup($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMIndicatorGroup::NewIndicatorGroup($aRepository);
		}
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
 		if (array_key_exists("IndicatorGroupID", $anArray))
		{
			$this->IndicatorGroupID = (int) $anArray["IndicatorGroupID"];
		}
 		if (array_key_exists("IndicatorGroupName", $anArray))
		{
			$this->IndicatorGroupName = stripslashes($anArray["IndicatorGroupName"]);
		}
 		if (array_key_exists("OwnerName", $anArray))
		{
			$this->OwnerName = stripslashes($anArray["OwnerName"]);
		}		
		return $this;
	}

	function save()
	{
		CheckMetadataDuplicated($this->Repository->ADOConnection, 'SI_GPO_IND', 'NOM_GPO_IND', $this->IndicatorGroupName, 'CLA_GPO_IND', $this->IndicatorGroupID, '');
		
	 	if ($this->isNewObject())
		{
/*			
			$sql =  "SELECT MAX(CLA_GPO_IND) As IndicatorGroupID".
						" FROM SI_GPO_IND";

			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("Error accessing SI_GPO_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}

			if (is_null($aRS->fields["indicatorgroupid"]))
				$this->IndicatorGroupID = 1;
			else
				$this->IndicatorGroupID = $aRS->fields["indicatorgroupid"] + 1;
*/
			$this->IndicatorGroupID = BITAMGetNextID($this->Repository->ADOConnection, 'CLA_GPO_IND', 'SI_GPO_IND', '');

			// Obtenemos el id del usuario logeado, primero lo tomamos de la sesion
			$UserIDOwner = $_SESSION["BITAM_UserID"];
			// Si no existe en la sesion, lo tomamos de la cookie			
			if (is_null($UserIDOwner))
				$UserIDOwner = $_COOKIE["BITAM_UserID"];
			// Si aun asi es nulo, lo inicializamos en cero
			if (is_null($UserIDOwner))
				$UserIDOwner = "0";
								
			$sql = "INSERT INTO SI_GPO_IND (".
			            "CLA_GPO_IND".
			            ",NOM_GPO_IND".
			            ",CLA_OWNER".
						") VALUES (".
						$this->IndicatorGroupID.
						",".$this->Repository->ADOConnection->Quote($this->IndicatorGroupName).
						",".$UserIDOwner.")";
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_GPO_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
		}
		else
		{
			$sql = "UPDATE SI_GPO_IND SET ".
						"NOM_GPO_IND = ".$this->Repository->ADOConnection->Quote($this->IndicatorGroupName).
					" WHERE CLA_GPO_IND = ".$this->IndicatorGroupID;
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_GPO_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
		}
		return $this;
	}

	function remove()
	{		
		/* ====================================================================================
			Garantizamos la integridad del repositorio
			SI_GPO_IND_DET -> Relacion con indicadores
			SI_ROL_IND     -> Relacion con grupos de usuarios 
			SI_USR_IND	   -> Relacion con usuarios
		   ==================================================================================== */

		// Validamos si no es un grupo de indicadores asociado a un modelo 
		
		if (! ExistsIndicatorGroupInModel($this->Repository->ADOConnection, $this->IndicatorGroupID))
		{
			// Relacion de indicadores
			$sql = "DELETE FROM SI_GPO_IND_DET WHERE CLA_GPO_IND = ".$this->IndicatorGroupID;
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_GPO_IND_DET table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}		
			
			// Relacion con grupos de usuarios
			$sql = "DELETE FROM SI_ROL_IND WHERE CLA_GPO_IND = ".$this->IndicatorGroupID;
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_ROL_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}		
			
			// Relacion con usuarios
			$sql = "DELETE FROM SI_USR_IND WHERE CLA_GPO_IND = ".$this->IndicatorGroupID;
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_USR_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}		
			
			// Finalmente los grupos de indicadores		
			$sql = "DELETE FROM SI_GPO_IND WHERE CLA_GPO_IND = ".$this->IndicatorGroupID;
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_GPO_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
		}
		else
			$this->Message = "0";
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->IndicatorGroupID < 0);
	}

	function get_Image()
	{
		return '<img src="images/models.gif">';
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New Indicator Group");
		}
		else
		{
			return $this->IndicatorGroupName;
		}
	}

	function get_QueryString()
	{
		if ($this->IndicatorGroupID < 0)
		{
			return "BITAM_PAGE=IndicatorGroup";
		}
		else
		{
			return "BITAM_PAGE=IndicatorGroup&IndicatorGroupID=".$this->IndicatorGroupID;
		}
	}

	function get_Parent()
	{
		return BITAMIndicatorGroupCollection::NewIndicatorGroupCollection($this->Repository, $this->IndicatorGroupID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'IndicatorGroupID';
	}

	function get_Children($aUser)
	{
		$myChildren = array();
		
		if (!$this->isNewObject())
		{
			$myChildren[] = BITAMIndicatorGroupIndicatorCollection::NewIndicatorGroupIndicatorCollection($this->Repository, $this->IndicatorGroupID);
		}
		
		return $myChildren;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IndicatorGroupName";
		$aField->Title = translate("Indicator Model");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[$aField->Name] = $aField;
		


		if (!InModeSAAS())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "OwnerName";
			$aField->Title = translate("Owner");
			$aField->Type = "String";
			$aField->Size = 15;		
			$aField->IsDisplayOnly = true;
			$myFields[$aField->Name] = $aField;		
		}
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		// @EA 2005-06-22
		$RepositoryType = $_SESSION["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			$RepositoryType = $_COOKIE["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			// Produccion
			$RepositoryType = 2;		

		if ($RepositoryType == 1)
		{
			return true;				
		}
		else 
			return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}		

}

?>