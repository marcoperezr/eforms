<?php

class BPAEmailConfiguration
{
  	public $fbm_email_server;
	public $fbm_email_port;
  	public $fbm_email_username;
  	public $fbm_email_pwd;
  	public $fbm_email_source;
  	public $bpa_email_source;
  	public $bpa_email_sourcename;
	public $fbm_db_server;
	public $fbm_db_user;
	public $fbm_db_pwd;
	public $fbm_db_name;
	public $agents = array();
	//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
	public $push_agent_sleep = 20;				//Tiempo en segundos que deberá dormir la tarea del Agente de notificaciones Push antes de revisar nuevamente si hay tareas pendientes
	public $push_agent_running_time = 60;		//Tiempo en minutos que debe durar en ejecución la tarea permanente del Agente de notificaciones Push, llegando a este valor será terminada y arrancará una nueva tarea de Windows con el Agente
	//@JAPR 2016-10-21: Agregadas configuraciones para depuración del Agente
	public $push_agent_debug_mode = 0;			//Indica si el agente entrará en modo de depuración, dejando el archivo de proceso de tareas sin eliminar cuando no ocurre un error
	public $push_agent_ignore_tasks = 0;		//Indica si el agente ignorará todas las tareas configuradas, con lo cual se podrá ejecutar directamente la URL del agente para depurar el proceso
	//@JAPR
  	
	const stopFile = "eFormsStop.ini";
	//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	const stopPushFile = "StopPushAgent.ini";
	//@JAPRDescontinuado, ya no se encontraron referencias a este archivo así que se removerá
	//const sessionFile = 'eFormsSession.xml';
	//@JAPR
  	
	function __construct()
	{
		$this->fbm_email_server = "";
		$this->fbm_email_port = "";
		$this->fbm_email_username = "";
		$this->fbm_email_pwd = "";
		$this->fbm_email_source = "";
		$this->bpa_email_source = "esurvey@bitam.com";
		$this->bpa_email_sourcename = "KPIOnline eSurvey";
		$this->fbm_db_server = '';
		$this->fbm_db_user = '';
		$this->fbm_db_pwd = '';
		$this->fbm_db_name = '';
	}
  
	static function readConfiguration()
	{
		$object = new BPAEmailConfiguration();

		$filename = getcwd()."\\"."config"."\\BPA_Agent.xml";

		if (!file_exists($filename))
		{
			return null;
		}

		$doc = new DOMDocument();

		try
		{
			$doc->load($filename);
		}
		catch (Exception $e)
		{
			throw new Exception('Could not open configuration file: ' . $e->getMessage());
		}
		
		$root = $doc->getElementsByTagName("CONFIGURATION");
		$root = $root->item(0);

		$tag = $root->getElementsByTagName("FBM_EMAIL_SERVER");
		if ($tag->item(0) != null)
		{
			$object->fbm_email_server = $tag->item(0)->nodeValue;
		}
			
		$tag = $root->getElementsByTagName("FBM_EMAIL_PORT");
		if ($tag->item(0) != null)
		{
			$object->fbm_email_port = $tag->item(0)->nodeValue;
		}
			
		$tag = $root->getElementsByTagName("FBM_EMAIL_USERNAME");
		if ($tag->item(0) != null)
		{
			$object->fbm_email_username = $tag->item(0)->nodeValue;
		}
		
		$tag = $root->getElementsByTagName("FBM_EMAIL_PWD");
		if ($tag->item(0) != null)
		{
			$object->fbm_email_pwd = $tag->item(0)->nodeValue;
		}
			
		$tag = $root->getElementsByTagName("FBM_EMAIL_SOURCE");
		if ($tag->item(0) != null)
		{
			$object->fbm_email_source = $tag->item(0)->nodeValue;
		}

		//@JAPR 2013-06-19: Agregado el agente de eForms
		$tag = $root->getElementsByTagName("FBM_DB_SERVER");
		if ($tag->item(0) != null) {
			$object->fbm_db_server = $tag->item(0)->nodeValue;
		}
		
		$tag = $root->getElementsByTagName("FBM_DB_USER");
		if ($tag->item(0) != null) {
			$object->fbm_db_user = $tag->item(0)->nodeValue;
		}
			
		$tag = $root->getElementsByTagName("FBM_DB_PWD");
		if ($tag->item(0) != null) {
			$object->fbm_db_pwd = $tag->item(0)->nodeValue;
		}
		
		$tag = $root->getElementsByTagName("FBM_DB_NAME");
		if ($tag->item(0) != null) {
			$object->fbm_db_name = $tag->item(0)->nodeValue;
		}

		$tag = $root->getElementsByTagName("MAX_PROCESSES");
		if ($tag->item(0) != null) {
			$object->max_processes = intval($tag->item(0)->nodeValue);
		}

		$tag = $root->getElementsByTagName("RETRY_TIME");
		if ($tag->item(0) != null) {
			$object->retry_time = intval($tag->item(0)->nodeValue);
		}

		$tag = $root->getElementsByTagName("EXECUTION_TIMEOUT");
		if ($tag->item(0) != null) {
			$object->execution_timeout = intval($tag->item(0)->nodeValue);
		}
		
		//@JAPR 2016-10-19: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
		$tag = $root->getElementsByTagName("PUSH_AGENT_SLEEP");
		if ($tag->item(0) != null) {
			$object->push_agent_sleep = intval($tag->item(0)->nodeValue);
		}
		
		$tag = $root->getElementsByTagName("PUSH_AGENT_RUNNING_TIME");
		if ($tag->item(0) != null) {
			$object->push_agent_running_time = intval($tag->item(0)->nodeValue);
		}
		
		//@JAPR 2016-10-21: Agregadas configuraciones para depuración del Agente
		$tag = $root->getElementsByTagName("PUSH_AGENT_DEBUG_MODE");
		if ($tag->item(0) != null) {
			$object->push_agent_debug_mode = intval($tag->item(0)->nodeValue);
		}
		
		$tag = $root->getElementsByTagName("PUSH_AGENT_IGNORE_TASKS");
		if ($tag->item(0) != null) {
			$object->push_agent_ignore_tasks = intval($tag->item(0)->nodeValue);
		}
		//@JAPR
		
		//Agrega la configuración default como un agente mas de la lista (implícito)
		if ($object->fbm_db_server != '') {
			require_once('eFormsAgent.inc.php');
			$anAgent = new eFormsAgent();
			$anAgent->server = $object->fbm_db_server;
			$anAgent->username = $object->fbm_db_user;
			$anAgent->password = $object->fbm_db_pwd;
			$anAgent->repository = $object->fbm_db_name;
			$anAgent->active = true;
			$anAgent->syncronized = true;
			$object->agents[] = $anAgent;
		}
		
		//Agrega el resto de agentes que hubiera configurados
		$col = $root->getElementsByTagName("AGENT");
		foreach ($col as $eachAgent) {
			$object->agents[] = BPAEmailConfiguration::fromXMLElement($eachAgent);
		}
		
		return($object);
	}
	
	static function fromXMLElement($anXMLElement)
	{
		require_once('eFormsAgent.inc.php');
		$anAgent = new eFormsAgent();

		$tag = $anXMLElement->getElementsByTagName("SERVER");
		if ($tag->item(0) != null) {
			$anAgent->server = $tag->item(0)->nodeValue;
		}

		$tag = $anXMLElement->getElementsByTagName("PORT");
		if ($tag->item(0) != null) {
			$anAgent->port = intval($tag->item(0)->nodeValue);
		}

		$tag = $anXMLElement->getElementsByTagName("USERNAME");
		if ($tag->item(0) != null) {
			$anAgent->username = ($tag->item(0)->nodeValue);
		}

		$tag = $anXMLElement->getElementsByTagName("PASSWORD");
		if ($tag->item(0) != null) {
			//$anAgent->password = BITAMDecryptPassword($tag->item(0)->nodeValue);
			$anAgent->password = $tag->item(0)->nodeValue;
		}

		$tag = $anXMLElement->getElementsByTagName("ACTIVE");
		if ($tag->item(0) != null) {
			$anAgent->active = $tag->item(0)->nodeValue == '1';
		}

		$tag = $anXMLElement->getElementsByTagName("REPOSITORY");
		if ($tag->item(0) != null) {
			$anAgent->repository = $tag->item(0)->nodeValue;
		}

		$tag = $anXMLElement->getElementsByTagName("SYNCRONIZED");
		if ($tag->item(0) != null) {
			$anAgent->syncronized = $tag->item(0)->nodeValue == '1';
		}
			
		return($anAgent);
	}
	
	static function check_email_address($email) 
	{
		// First, we check that there's one @ symbol, and that the lengths are right
		if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) 
		{
			// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
		}
	
		// Split it into sections to make life easier
		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);
		for ($i = 0; $i < sizeof($local_array); $i++) 
		{
			if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) 
			{
				return false;
			}
		}  
	
		if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) 
		{ 
			// Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2) 
			{
				return false; // Not enough parts to domain
			}
			for ($i = 0; $i < sizeof($domain_array); $i++) 
			{
				if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) 
				{
					return false;
				}
			}
		}
	
		return true;
	}
	
	//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	//@JAPRDescontinuada, este archivo de seguridad NO se debe eliminar al iniciar cada ejecución del agente, es un archivo que manualmente debe ser removido por el usuario que lo creó para
	//forzar a detener a los agentes (por ejemplo para una actualización), por lo que no se requiere una función para eliminarlo
	//Elimina el archivo de seguridad que forza a detener el agente, además de cualquier otra información temporal que pudiera haber generado
	/*static function ResetAgent() {
		$sFileName = substr(ADODB_DIR, 0, strlen(ADODB_DIR)-5).'log/'.BPAEmailConfiguration::stopFile;
		@unlink($sFileName);
	}*/
	//@JAPR
}
?>