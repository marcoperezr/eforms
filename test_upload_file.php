<?php
class UploadFile
{

	public $field_name;
	public $stored_file_name;
	public $original_file_name;
	public $temp_file_location;
	public $use_soap = false;
	public $file;
	public $file_ext;
	public $sError = '';
	public $winrar_path = 'c:\\Program Files\\WinRAR\\WinRAR';

    function UploadFile ($field_name)
    {
		$this->field_name = $field_name;
    }

    function set_for_soap($filename, $file){
    	$this->stored_file_name = $filename;
    	$this->use_soap = true;
    	$this->file = $file;
    }

	function get_url($stored_file_name,$bean_id)
	{
		global $upload_config;
		return UploadFile::get_file_path($stored_file_name,$bean_id);
	}
	function get_file_path($stored_file_name,$bean_id)
	{
		global $upload_config;
		return $upload_config['upload_dir'] . $bean_id . rawurlencode($stored_file_name);
	}

	function duplicate_file($old_id, $new_id, $file_name)
	{
		global $upload_config;

		$source = $upload_config['upload_dir'] . $old_id . $file_name;
		$destination = $upload_config['upload_dir'] . $new_id . $file_name;
		copy($source, $destination);
	}

	function confirm_upload()
	{
		global $upload_config;

		if (!is_uploaded_file($_FILES[$this->field_name]['tmp_name']) )
		{
			return false;
		}
		else if ($_FILES[$this->field_name]['size'] > $upload_config['upload_maxsize'])
		{
			$this->sError = "File was too big! Max 5 MB";
			return false;
		}

	
		$this->mime_type =$this->getMime($_FILES[$this->field_name]);
		$this->stored_file_name = $this->create_stored_filename();
		if (!in_array(strtolower($this->file_ext), $upload_config['upload_goodext'])) {
			$this->sError = "NOT A VALID FILE !";
			return false;
		}
		$this->temp_file_location = $_FILES[$this->field_name]['tmp_name'];

		return true;
	}

	function getMimeSoap($filename){

		if( function_exists( 'ext2mime' ) )
		{
			$mime = ext2mime($filename);
		}
		else
		{
			$mime = ' application/octet-stream';
		}
		return $mime;

	}
	function getMime(&$_FILES_element)
	{

		$filename = $_FILES_element['name'];

		if( $_FILES_element['type'] )
		{
			$mime = $_FILES_element['type'];
		}
		elseif( function_exists( 'mime_content_type' ) )
		{
			$mime = mime_content_type( $_FILES_element['tmp_name'] );
		}
		elseif( function_exists( 'ext2mime' ) )
		{
			$mime = ext2mime( $_FILES_element['name'] );
		}
		else
		{
			$mime = ' application/octet-stream';
		}
		return $mime;
	}

	function get_stored_file_name()
	{
		return $this->stored_file_name;
	}

	function create_stored_filename()
	{
		global $upload_config;
		if(!$this->use_soap){
           $stored_file_name = $_FILES[$this->field_name]['name'];
		}else{
			$stored_file_name = $this->stored_file_name;
		}
		$this->original_file_name = $stored_file_name;
        $ext_pos = strrpos($stored_file_name, ".");

		$this->file_ext = substr($stored_file_name, $ext_pos + 1);
        if (!in_array(strtolower($this->file_ext), $upload_config['upload_goodext']))
		{
            $stored_file_name .= ".txt";
			$this->file_ext="txt";
        }

		return $stored_file_name;
	}

	function final_move($bean_id)
	{
		global $upload_config;

        $destination = $this->get_upload_path($bean_id);
        if($this->use_soap){
        	$fp = fopen($destination, 'wb');
        	if(!fwrite($fp, $this->file)){
        		$this->sError = "Can't save file to:".$destination;
        	}
        	fclose($fp);
        }else{
			if (!move_uploaded_file($_FILES[$this->field_name]['tmp_name'], $destination))
            {
				$this->sError = "Can't move Uploaded File";
                //die ("ERROR: can't move_uploaded_file to $destination. You should try making the directory writable by the webserver");
             }
        }
	    return true;
	}

	function extractFiles($bean_id) {
		global $upload_config;
		if (!isset($upload_config['extract_dir'])) {
			$this->sError = "NO EXTRACT DIRECTORY SELECTED !";
			return false;
		}
		$source = $this->get_upload_path($bean_id);
		$destination = $upload_config['extract_dir'];

		$bAddQuotesSour = (strpos($source, ' ') !== false);
		$bAddQuotesDest = (strpos($upload_config['extract_dir'], ' ') !== false);

		

		$zip = new ZipArchive;
		if ($zip->open($source) === true) {
			$zip->extractTo($destination);
			$zip->close();
		} else {
			$this->sError = "Can not extract files...";
		}
		unset($zip);

		@unlink($source);

	}

	function getRealPathFile($sPathFile, $bIncludeQuotes=true, $bSource=false) {
		$sRetVal = (($bIncludeQuotes)?'"':'').getcwd().'/';
		$sRetVal .= $sPathFile;
		if ($bIncludeQuotes && !$bSource) {
			$sRetVal .= '/';
		}
		$sRetVal .= (($bIncludeQuotes)?'"':'');
		$sRetVal = str_replace('/', '\\', $sRetVal);
//echo $sRetVal."<br>\n";
		return $sRetVal;
	}
	function get_upload_path($bean_id){
		global $upload_config;
		$file_name = $bean_id.$this->stored_file_name;
		return $upload_config['upload_dir'].$file_name;
	}

	function unlink_file($bean_id,$file_name) {
		global $upload_config;
        return unlink($upload_config['upload_dir'].$bean_id.$file_name);
    }
}
?>