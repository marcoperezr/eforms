<?php
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
require_once("initialize.php");
session_start();
//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);
	LoadLanguageWithName("EN");
}

?>
	<div id="__menu__" class="menu">
<?
//if ($theUser->IsArtusAdministrator)
//{
?>
		<a href="main.php?BITAM_SECTION=BudgetCollection"><?=translate("Budgets")?></a>
		<!--&nbsp;|&nbsp; -->
<?
//}
?>
	</div>