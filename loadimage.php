<?php
session_start();
$repository=$_SESSION["PABITAM_RepositoryName"];
session_write_close();
$repository="customerimages/".$repository.'/';

$bAdded = false;
$sError = '';
if (array_key_exists("load_image", $_FILES))
{
	if (is_array($_FILES["load_image"]))
	{
		if ($_FILES['load_image']['error'] == 1){
			$sError = "ML[640]";
		}
		elseif ($_FILES['load_image']['error'] == 2){
			$sError = "ML[641]";
		}
		elseif ($_FILES['load_image']['error'] == 3){
			$sError = "ML[642]";
		}
		elseif ($_FILES['load_image']['error'] == 4){
			$sError = "ML[643]";
		}
		elseif (is_uploaded_file($_FILES["load_image"]["tmp_name"]))
		{
			$load_image = $_FILES["load_image"]["tmp_name"];

			require_once("upload_file.php");
			
			global $ekt_config;
			$ekt_config = array(
				'upload_goodext' =>
				  array (
				    0 => 'gif',
				    1 => 'png',
				    2 => 'bmp',
				    3 => 'jpg',
				    4 => 'jpeg',
				    5 => 'ico',
				  ),
				  'upload_dir' => $repository,
				  'upload_maxsize' => 10000000//5242880  // Soporta Hasta 5 MegasBytes (MB)
			);

			$upload_file = new UploadFile('load_image');

			if(trim($ekt_config['upload_dir'])!="")
			{	
				if ($upload_file->confirm_upload())
				{
					$filename = $upload_file->get_stored_file_name();
			        $file_mime_type = $upload_file->mime_type;
					$upload_file->final_move('');
					$bAdded = true;
				}
				if ($upload_file->sError != '' && !$bAdded) {
					$sError = $upload_file->sError;
				}
			}
			unset($upload_file);
		}
	}
}
$bGetTemplate = (isset($_GET['template']))?true:false;
$sAddUrlIFRAME = ($bGetTemplate) ? '?template=1' : '';
if ($bAdded) {
	require_once('sel_image.inc.php');
	global $url_images, $img_types, $img_names;
}
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script language='JavaScript' src='js/EditorHTML/<?=$_SESSION["PAuserLanguage"]?>.js'></script>
		<script language="Javascript">
		function OnStart(){
			document.getElementById('botOK').value = ML[631];
		<?php
		if ($bAdded) {
		?>
			window.parent.<?=$url_images?>
			window.parent.<?=$img_types?>
			window.parent.<?=$img_names?>
			window.parent.Show_Order(1, false);
		<?php
		}
		?>
		}
		</script>
	</head>
<body onload="OnStart();" class="NBody">
<style type='text/css'>@import url('css/fudgeEditorHTML.css');</style>
<form enctype="multipart/form-data" action="loadimage.php<?=$sAddUrlIFRAME?>" method="post" name="FLoadImage" target="_self">
<fieldset style="border-style:outset; position:absolute; top:0px; left:0px; width:350px;">
	<legend class="dataLabel"><script language="JavaScript">document.write(ML[632])</script></legend>
	<input type="file" size="50" tabindex="0" name="load_image" id="load_image">
	&nbsp;<input id=botOK class="button" type="submit" name="Load" value="Load">&nbsp;
	<font style="font-family:arial; font-size:10px; color:red;">
		<script language="Javascript">document.write(<?=$sError?>);</script>
	</font>
</fieldset>
</form>
</body>
</html>