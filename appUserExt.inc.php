<?php
require_once("appuser.inc.php");
require_once("object.trait.php");
require_once('kpiWebServiceEsurveyClient.inc.php');

class BITAMAppUserExt extends BITAMAppUser
{
	use BITAMObjectExt;
	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
	}
	
	//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
	function hideBackButton($aUser) {	
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=AppUserExt";
		}
		else
		{
			return "BITAM_PAGE=AppUserExt&UserID=".$this->UserID;
		}
	}
	
	//@JAPR 2015-06-10: Agregada la propiedad para obtener la imagen a usar en la vista tipo Metro
	function get_Image() {
		if($this->Image != "" && $this->Image != null)
			$strImage = "<img src=\'" . $this->Image . "\'/>";
		else
			$strImage = "<img src=\"images/user1.gif\" />";
		return $strImage;
	}

	/* Genera la ventana de definición de la forma específicada según el rediseño, la cual contempla el árbol con los elementos de la forma y la sección del
	preview
	*/
	/* Genera la ventana de definición de la forma específicada según el rediseño, la cual contempla el árbol con los elementos de la forma y la sección del
	preview
	*/
	function generateDesignWindow() {
		global $gbIsGeoControl;
		
		//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
		require_once('settingsvariable.inc.php');
		
		//GCRUZ 2015-08-18. Tab para permisos sobre formas
		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		$strScriptPath = $path_parts["dirname"];

		$schedulerIDs = array();
		$aSQL = "SELECT SchedulerID FROM si_sv_surveyscheduleruser WHERE UserID = ".$this->CLA_USUARIO;
		$aRS = $this->Repository->DataADOConnection->Execute($aSQL);
		if ($aRS && $aRS->_numOfRows != 0)
		{
			while (!$aRS->EOF)
			{
				$schedulerIDs[] = $aRS->fields['SchedulerID'];
				$aRS->MoveNext();
			}
		}
		else
			$schedulerIDs[] = 0;
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
		<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
		<link href="css/font/roboto/robotofont.css" rel="stylesheet" type="text/css">
		<script src="js/codebase/dhtmlxFmtd.js"></script>
      	<script type="text/javascript" src="js/json2.min.js"></script>
		<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
		<script type="text/javascript" data-main="js/_requireconfig.js" src="js/libs/require/require.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>
		<script type="text/javascript" src="js/variables.js"></script>
<?
//@JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
$intMapType = maptApple;
$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'MAPTYPE');
if (!is_null($objSetting) && trim($objSetting->SettingValue != '')) {
	$intMapType = (int) @$objSetting->SettingValue;
	if ( $intMapType < maptGoogle || $intMapType > maptApple ) {
		$intMapType = maptApple;
	}
}

//Carga el API de mapas según la configuración
switch ($intMapType) {
	case maptGoogle:
?>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places<?=googleServerAPIKey?>"></script>
<?
		break;
	case maptApple:
	default:
?>
		<script type="text/javascript" src="https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.js"></script>
<?
		break;
}
//@JAPR
?>
		<style>
			/* Estilos para ocultar el icono de las ventanas modales (todas las skins utilizadas) */
			.dhxwins_vp_dhx_skyblue div.dhxwin_hdr div.dhxwin_icon.without_icon {
				display: none;
			}
			.dhxwins_vp_dhx_web div.dhxwin_hdr div.dhxwin_icon.without_icon {
				display: none;
			}
			/* Estilo para los botones de búsqueda asociados a grids */
			.search_button input {
				border:1px solid #e1e1e1 !important;
			}
			.selection_container div.dhxform_container {
				border:1px solid #e1e1e1 !important;
				//height:90% !important;
				////overflow-y:auto;
			}
			.selection_container div.objbox {
				//overflow-y:auto;
			}
			.selection_list fieldset.dhxform_fs {
				border:1px solid #e1e1e1 !important;
				//height:95% !important;
			}
			div.dhxform_item_label_left.assoc_button div.dhxform_btn_txt {
				background-image:url(images/add_field.png);
				background-repeat:no-repeat;
				background-position: 0px 3px;
				padding-left: 22px;
				margin: 0px 15px 0px 12px;
			}
			div.dhxform_item_label_left.remove_button div.dhxform_btn_txt {
				background-image:url(images/del_field.png);
				background-repeat:no-repeat;
				background-position: 0px 3px;
				padding-left: 22px;
				margin: 0px 15px 0px 12px;
			}
			/* it's important to set width/height to 100% for full-screen init */
			html, body {
				width: 100%;
				height: 100%;
				margin: 0px;
				overflow: hidden;
			}
			span.tabFormsOpts {
				font-size:12px;
			}
			div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
				 text-transform: none !important;
			}
			/*
			
			
			/* Estilos para ocultar las tabs pero permitir ver sus celdas */
			.hiddenTabs div.dhx_cell_tabbar {
				position:none !important;	//Este estilo no existe, pero efectivamente bloquea el aplicado a la clase que sobreescribe
			}
			.hiddenTabs div.dhxtabbar_tabs {
				display:none !important;
			}
			.hiddenTabs div.dhx_cell_cont_tabbar {
				border: 0px !important;
			}
			.visibleTabs div.dhx_cell_tabbar {
				position:absolute !important;	//Utilizado para pintar tabs que estarían dentro de la tab principal oculta
			}
			.visibleTabs div.dhxtabbar_tabs {
				display:inline !important;
			}
			.visibleTabs div.dhx_cell_cont_tabbar {
				border: 0px !important;
			}
			/* Estilo para los enlaces de las opciones principales (tabs ocultos) de la ventana de diseño */
			.linkToolbar {
				background-color: #264B83;
			}
			.linktd {
				cursor: pointer;
				font-family: Roboto Regular;
				font-size: 13px;
				//font-weight: bold;
				//text-align: center;
				//text-decoration: underline;
				color: white;	//#333333;
				padding-left: 24px;
				padding-right: 24px;
				//width: 150px;
				white-space: nowrap;
			}
			.linktdSelected {
				text-decoration: underline;
			}
			/* Estilo para mantener seleccionado el botón de selección de la toolbar de preguntas */
			//Finalmente no sirvió esto, porque el evento MouseOver del list option cambia de clase eliminando la anterior aplicada, así que se perdía
			//la clase definida en este archivo, se optó por cambiar directamente el estilo
			.tbOptionSelected {
				  background-color: #fff3a1 !important;
			}
			/* Agrega el border a las celdas del grid */
			table.obj tr td {
				//border-left-width: 1px !important;
				border: 0px !important;
				padding-left: 0px !important;
				padding-right: 0px !important;
			}
			.dhx_textarea {
				margin-left: 0px;
			}
			/* Estilos para los subgrids de propiedades de las tabs de colecciones */
			.dhx_sub_row {
				//overflow: visible !important;
				border: 1px solid #808080 !important;
			}
			.dhx_sub_row table.obj tr td {
				border-left-width:1px solid #808080 !important;
			}
			.dhxdataview_placeholder div.dhx_dataview_item {
				border: 0px !important;
			}
			/* Estilos de los Layouts */
			.dhx_cell_hdr {
				background-color: #f5c862 !important;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>
		<script>
			var AdminObjectType = {
				otyItem: 0,
				otySurvey: 1,
				otySection: 2,
				otyQuestion: 3,
				otyAnswer: 4,
				otyAgenda: 5,
				otyCatalog: 6,
				otyAttribute: 7,
				otyDraft: 8,
				otyOutbox: 9,
				otyPhoto: 10,
				otySignature: 11,
				otySetting: 12,
				otySurveyFilter:13,
				otySectionFilter:14,
				otyMenu: 15,
				otyAudio: 16,
				otyVideo: 17,
				otyQuestionFilter: 18,
				otyStatusAgenda: 19,
				otyStatusDocument: 20,
				otySketch: 21,
				otyOption: 22,
				otyShowQuestion: 23,
				otyUser: 24,
				otyUserGroup: 25
			};
			var GFieldTypes = {
				alphanum: "edtxt",			//Texto en una sola linea editado inline
				text: "txttxt",				//Text area multiline
				combo: "coro",				//Combo simple (Combo methods && props: save(), restore(), size(), get(sidx), getKeys(), clear(), put(sidx, val), remove(sidx) .keys [array], .values [array]
				comboSync: "comboExt",		//Combo sincronizable con otras combos (Padres e hijas, por ejemplo, los atributos del catálogo seleccionado) (Personalizado)
				check: "ch",				//Checkbox
				editor: "editor",			//DHTMLXEditor
				number: "edn",				//Valores numéricos con máscara de captura
				date: "dhxCalendarA",		//Calendario con posibilidad de escribir la fecha
				hour: "time",				//Componente para capturar hora y minutos
				multiList: "clist",			//Lista con checkbox para seleccionar varios elementos
				color: "cp",				//Permite seleccionar el color de un conjunto predeterminado de posibilidades (personalizarlo al JColor que manejamos actualmente)
				image: "img",				//Una imagen con un click a una función
				uploadImg: "uploadImg",		//Subir imágenes al servidor
				grid: "sub_row_grid",		//Un grid dentro de otro con la funcionalidad default de propiedades
				readOnly: "rotxt"			//No contiene texto editable, sólo una etiqueta (opcional)
			};
			
			var optionsYesNo = {
				0:"<?=translate("No")?>",
				1:"<?=translate("Yes")?>"
			};
			
			var propTabType = {
				properties: 0,
				collection: 1
			}
			//Diálogos
			var objWindows;						//Referencia al componente que almacena todas las ventanas
			var objWindowsFn;
			var objDialog;						//Diálogo modal común para cualquier proceso que requiera captura de algún valor específico (cada proceso lo volverá a definir)
			//Forms
			var objUserFormPermissions;			//Ventana para asociar las formas a los usuarios
			//TabBars
			var objFormTabs;					//Cejillas de las configuraciones de la forma
			//LayOuts							//Opciones principales de la forma
			//JAPRWarning: Remover esto para regresar a Tabs como objeto principal
			var objMainLayout;					//LayOut que se agregó para separar la tabbar con el contenido de la página de un header HTML que represente a las Tabs
			//JAPRWarning: Remover esto para regresar a Tabs como objeto principal (End)
			var objPropsLayout;					//LayOut principal de la forma, contiene a los demás
			var objUserTabsColl = new Object();	//Array con la definición de campos para la forma (se separa ya que esta no está en la celda de propiedades, así que los métodos como getFieldDefinition no pueden usar objTabsColl)
			var UserID = <?=$this->UserID?>;
			var ClaUsuario = <?=$this->CLA_USUARIO?>;
			var Forms = {}; //Para las formas existentes
			var FormsOrder = [];
			var Templates = [];					//Guarda el nombre de los templates disponibles
			var tabProfile = "tabProfile";
		    var tabCatalog = "tabCatalog";
		    var tabModel = "tabModel";
		    var tabNotification = "tabNotification";
			//GCRUZ 2015-08-18. Tab para permisos sobre formas
			var tabFormPermissions = "tabFormPermissions";
		    var Users = JSON.parse("<?=addslashes(json_encode($this->getJSonDefinition()))?>");
		    /* Opciones para utilizar en la definición de las configuraciones en los grids */
			var objCheck = {type:GFieldTypes.combo, options:optionsYesNo, default:0};
			var objAlpha = {type:GFieldTypes.alphanum, length:255};
			var objInteger = {type:GFieldTypes.number};
			var reqAjax = 1;
			var GroupsList = [];
			var Order = [];
			//var gbFullSave = false;
			//GCRUZ 2016-05-05. Agregar idioma
			var Languages = [];
			Languages[1] = 'Español';
			Languages[2] = 'English';
			<?//JAPR 2016-11-10: Validado que por ahora no permita utilizar idiomas que no están totalmente traducidos (#SYJ8FX)?>
			//Languages[4] = 'Portugês';
			//Languages[6] = 'Portugês - Brasil';
			//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
			var miMapType = "<?=$intMapType?>";
			//JAPR
			
			//JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
			var objPortalProducts = {};
			objPortalProducts[<?=prodDefault?>] = "<?=translate("Default")?>";
			objPortalProducts[<?=prodARTUS?>] = "Artus web";
			objPortalProducts[<?=prodEFORMS?>] = "KPIForms designer";
			objPortalProducts[<?=prodEBAVEL?>] = "eBavel designer";
			
			//GCRUZ 2016-05-05. Agregar idioma
			var reloadOnLanguage = false;
			//GCRUZ 2016-06-09. Datos de la GeoCerca
<?
			if (getMDVersion() >= esvGeoFence) {
?>
				var GeoFenceRadius = <?=$this->GeoFenceRadius?>;
				var GeoFenceLat = <?=$this->GeoFenceLat?>;
				var GeoFenceLng = <?=$this->GeoFenceLng?>;
				var OriginalGeoFenceLat = <?=$this->GeoFenceLat?>;
				var OriginalGeoFenceLng = <?=$this->GeoFenceLng?>;
<?
			}
?>

			
<?
			require_once("usergroup.inc.php");
			$objUserGroupsColl = BITAMUserGroupCollection::NewUserGroupCollection($this->Repository);
			foreach ($objUserGroupsColl as $objRole) {
?>
				//OMMC 2015-11-26: Agregado escape de comillas para los nombres de grupos con comillas
				GroupsList.push("<?=addslashes($objRole->UserGroupName)?>");
				Order.push("<?=$objRole->UserGroupID?>");		
<?
			}
			require_once("survey.inc.php");
			$loadedSurveys = BITAMSurvey::getSurveys($this->Repository, true);
			foreach($loadedSurveys as $skey => $asurvey) {
				//GCRUZ 2015-11-05. Escapar comillas.
				$asurvey = str_replace("\"", "\\\"", $asurvey);
?>
				Forms[<?=$skey?>] = "<?=$asurvey?>";
				FormsOrder.push(<?=$skey?>);
<?			}

			//@JAPR 2016-02-26: Agregados los templates de estilos personalizados de v6
			//@JAPR 2016-03-22: DAbdo determinó que los nuevos templates no deberían aplicar en el esquema de seguridad (por usuario o grupo), sólo global o por forma
			/*if (getMDVersion() >= esvTemplateStyles) {
				$objColorTemplatesColl = BITAMEFormsAppCustomStylesTplCollection::NewInstance($this->Repository);
				foreach($objColorTemplatesColl->Collection as $objColorTemplate) {
?>
				Templates[<?=$objColorTemplate->TemplateID?>] = "<?=$objColorTemplate->TemplateName?>";
<?
				}
			}*/
?>
						
			//GCRUZ 2015-08-18. Tab para permisos sobre formas. Lista de formas disponibles (todas)
			var arrSurveys = new Object();
			var arrSelForms = new Object();
			//GCRUZ 2015-10-16. Establecer el orden de las formas
			var arrSurveysOrder = [];
			var arrSelFormsOrder = [];
<?
			$i=0;
			foreach($loadedSurveys as $skey => $asurvey) {
				//@JAPR 2015-12-17: Corregido un bug, la función htmlspecialchars requiere ENT_QUOTES para reemplazar comilla sencilla también (#OUHB07)
				echo("\t\t\t"."arrSurveys[{$skey}] = {id:{$skey}, name:'".htmlspecialchars($asurvey, ENT_QUOTES)."'};\r\n");
				echo("\t\t\t"."arrSurveysOrder[{$i}] = {$skey};\r\n");
				$i++;
			}

			require_once('surveyScheduler.inc.php');
			$objSurveySchedulerCol = BITAMSurveySchedulerCollection::NewInstance($this->Repository, $schedulerIDs);
			$i=0;
			foreach ($objSurveySchedulerCol as $objSurveyScheduler)
			{
				if (isset($loadedSurveys[$objSurveyScheduler->SurveyID]))
				{
					//@JAPR 2015-12-17: Corregido un bug, la función htmlspecialchars requiere ENT_QUOTES para reemplazar comilla sencilla también (#OUHB07)
					echo("\t\t\t"."arrSelForms[{$objSurveyScheduler->SurveyID}] = {id:{$objSurveyScheduler->SurveyID}, name:'".htmlspecialchars($loadedSurveys[$objSurveyScheduler->SurveyID], ENT_QUOTES)."'};\r\n");
					echo("\t\t\t"."arrSelFormsOrder[{$i}] = {$objSurveyScheduler->SurveyID};\r\n");
					$i++;
				}
			}
?>
						
			/* Invoca la inicialización del App de eForms y la interfaz del diseñador
			*/
			function doOnLoad() {
				console.log('doOnLoad');

				if (!Users) {
					alert('<?=translate("You can not access this Users.")?>');
					return;
				}
				//Agrega clases extendidas a los objetos de definición leídos
				addClassPrototype();
								
				//Agrega estilos adicionales para corregir issues estéticos del DHTMLX
				//Para la DataView de secciones, aparecía un borde azul a la derecha y se veía mal, así que se remueve dinámicamente
				//$("head").append("<style> .simpleClass{ display:none; } </style>");
				
				//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});

				objWindowsFn = new dhtmlXWindows({
					image_path:"images/"
				});
				
				//JAPRWarning: Remover esto para regresar a Tabs como objeto principal
				objMainLayout = new dhtmlXLayoutObject({
					parent: document.body,
					pattern:"1C"
					//pattern: "2E"
				});
				objMainLayout.cells("a").appendObject("divDesign");
				objMainLayout.cells("a").hideHeader();
				
				objMainLayout.setSizes();
				//JAPRWarning: Remover esto para regresar a Tabs como objeto principal (End)
				
				//Prepara la tabbar con las opciones principales de la forma
				//JAPRWarning: Remover esto para regresar a Tabs como objeto principal
				objFormTabs = new dhtmlXTabBar({
					parent:"divDesignBody"
				});
				//objFormTabs = new dhtmlXTabBar(document.body);
				//JAPRWarning: Remover esto para regresar a Tabs como objeto principal (End)
				objFormTabs.addTab(tabProfile, "<span class='tabFormsOpts'><?=translate("Profile")?></span>", null, null, true);
				objFormTabs.addTab(tabCatalog, "<span class='tabFormsOpts'><?=translate("Catalogs")?></span>");
				objFormTabs.addTab(tabModel, "<span class='tabFormsOpts'><?=translate("Models")?></span>");
				objFormTabs.addTab(tabNotification, "<span class='tabFormsOpts'><?=translate("Notification")?></span>");
				objFormTabs.addTab(tabFormPermissions, "<span class='tabFormsOpts'><?=translate("Forms")?></span>");
				objFormTabs.setArrowsMode("auto");

				generateUserProps(tabProfile);
				objPropsLayout = objFormTabs.tabs(tabCatalog).attachLayout({pattern: "1C"});
				objPropsLayout.cells("a").attachURL("DataSourceFilter.php?UserID=" + ClaUsuario);
				//objPropsLayout.cells("a").attachURL("DataSourceFilter.php");
				objPropsLayout.cells("a").hideHeader();
				//******************************************************************************************************
				//Prepara el Layout para mostrar el detalle de Usuario por default.
				//doChangeTab(tabProfile);	
				//GCRUZ 2015-08-18. Tab para permisos sobre formas
				generateUserFormPermissions(tabFormPermissions);

				//Resize completo de la ventana y prepara el evento para los resize posteriores
				doOnResizeScreen();

				$(window).resize(function() {
					doOnResizeScreen();
				});

			}

			/* Genera la ventana de propiedades de la forma
			*/
			function generateUserProps(tabProfile) {
				console.log('generateUserProps ' + tabProfile);
				//Generar todos los fields
				objUserTabsColl = {"generalTab": {id:"generalTab", name:"<?=translate("General")?>", activeOnLoad:true}
<?
	if (getMDVersion() >= esvGeoFence) {
?>
				, "geoFenceTab": {id:"geoFenceTab", name:"<?=translate("GeoFence")?>"}
<?
	}
?>
				};
				
				var objCommon = {id:UserID, parentType:AdminObjectType.otyUser, tab:"generalTab"};
<?
			//@JAPR 2015-10-06: Validado que en un servicio de GeoControl no se puedan crear usuarios ni editar sus datos de KPIOnline
			if ($gbIsGeoControl || true) {
?>
				var blnReadOnly = true;
				var objFieldType = {type:GFieldTypes.readOnly};
<?
			}
			else {
?>
				var blnReadOnly = false;
				var objFieldType = objAlpha;
<?
			}
			//@JAPR
			//JAPR 2015-11-18: Habilitado el nombre de usuario para edición en cuentas que no son GeoControl (#QQCMGH)
?>
				
				objUserTabsColl["generalTab"].fields =  new Array(
					new FieldCls($.extend(true, {}, objCommon, objFieldType, {name:"Email", label:"<?=translate("User")?>", readOnly:blnReadOnly})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"UserName", label:"<?=translate("Full Name")?>", readOnly:<?=($gbIsGeoControl)?1:0?>})),
					new FieldCls($.extend(true, {}, objCommon, objFieldType, {name:"LastsyncDate", label:"<?=translate("Last synchronization date")?>", readOnly:blnReadOnly})),
<?					
			//@JAPR 2015-10-06: Validado que en un servicio de GeoControl no se puedan crear usuarios ni editar sus datos de KPIOnline
			if (!$gbIsGeoControl && false) {
?>
					new FieldCls($.extend(true, {}, objCommon, objFieldType, {name:"Password", label:"<?=translate("Password")?>", readOnly:blnReadOnly})),
<?
			}
			//@JAPR
?>
					new FieldCls($.extend(true, {}, objCommon, objFieldType, {name:"FirstName", label:"<?=translate("First name")?>", readOnly:blnReadOnly})),
					new FieldCls($.extend(true, {}, objCommon, objFieldType, {name:"LastName", label:"<?=translate("Last name")?>", readOnly:blnReadOnly})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"AltEmail", label:"<?=translate("Alternative email")?>"})),
					new FieldCls($.extend(true, {}, objCommon, {name:"GroupsAssct", label:"<?=translate("User Groups")?>", type:GFieldTypes.multiList,
						options: GroupsList, optionsOrder: Order, readOnly:true})),
					new FieldCls($.extend(true, {}, objCommon, {name:"SendPDFByEmail", label:"<?=translate("Send PDF by email")?>", type:GFieldTypes.combo, options:optionsYesNo})),
					new FieldCls($.extend(true, {}, objCommon, {name:"DefaultSurvey", label:"<?=translate("Default form")?>", type:GFieldTypes.combo, options:Forms, optionsOrder:FormsOrder, empty:true, default:0})),
					//JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
					new FieldCls($.extend(true, {}, objCommon, {name:"DefaultKPIProduct", label:"<?=translate("Default portal")?>", type:GFieldTypes.combo, options:objPortalProducts, empty:true, default:0})),
					//JAPR 2015-10-06: Removido temporalmente en lo que se implementan cambios al esquema de colores
					//JAPR 2016-03-22: DAbdo determinó que los nuevos templates no deberían aplicar en el esquema de seguridad (por usuario o grupo), sólo global o por forma
					//new FieldCls($.extend(true, {}, objCommon, {name:"TemplateID", label:"<?=translate("Default colour template")?>", type:GFieldTypes.combo, options:Templates, empty:true, emptyId:-1, default:-1})),
					new FieldCls($.extend(true, {}, objCommon, {name:"Image", label:"<?=translate("Profile picture")?>", image:"<?=$this->Image?>", type:GFieldTypes.uploadImg, maxLength:255})),
					//GCRUZ 2016-05-05. Agregar idioma
					new FieldCls($.extend(true, {}, objCommon, {name:"UsrLanguage", label:"<?=translate("Language")?>", type:GFieldTypes.combo, options:Languages, empty:false, default:1}))
<?
	if (getMDVersion() >= esvUserParams) {
?>
					//GCRUZ 2016-07-19. Parámetros extra de usuario para fórmulas
					,new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"UserParam1", label:"<?=translate("User Parameter 1")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"UserParam2", label:"<?=translate("User Parameter 2")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"UserParam3", label:"<?=translate("User Parameter 3")?>"}))
<?
	}
?>
				);
				
				//Los detalles de la forma se muestran en el tab de propiedades
				generateFormDetail(objUserTabsColl, AdminObjectType.otyUser, UserID, tabProfile);
				
				//GCRUZ 2016-06-08. Generar el tab para la GeoCerca
<?
	if (getMDVersion() >= esvGeoFence) {
?>
				//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
				initializeMaps(tabProfile);
				//JAPR
<?
	}
?>
			}
			
			//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
			/* Inicializa el Apple Mapkit, en el caso de Google Maps simplemente continua con la función principal */
			function initializeMaps(tabProfile) {
				if ( miMapType != 0 ) {
					if ( !mapkit ) {
						alert("Unable to load mapkiy library");
						console.log("Unable to load mapkiy library");
						return;
					}
					
					mapkit.init({ authorizationCallback: function(done) {	
						fetch("https://kpionline.bitam.com/mapsService/mapKit/service.php", {method: "POST", headers: {'Product-Name' : 'KPIForms'}})
							.then(response => {
								return response.json();
							}).then(data => {
								if (data && data.token) {
									done(data.token);
								} else {
									console.log("No token generated at the site");
								}
							}).catch(error => {
								console.log("There was an error with the request")
							});
					}});
					mapkit.addEventListener("configuration-change", function(event) {
						switch(event.status) {
							case "Initialized":
								console.log("MapKit initialized correctly, ready to draw maps.");
								generateGeoFenceTab(tabProfile);
							break;
							case "Refreshed":
								console.log("Mapkit configuration or token changed correctly.");
							break;
						}
					});
					mapkit.addEventListener("error", function(event) {
						switch(event.status) {
							case "Unauthorized":
								console.log("MapKit token was invalid.");
								alert("Mapkit token was invalid.");
							break;
							default:
								console.log("Woops, Mapkit experienced an unexpected error" + event.status);
								alert("Woops, Mapkit experienced an unexpected error." + event.status);
								console.log("Unknown Mapkit error: " + event.status);
							break;
						}
					})
					return;
				}
				
				//Para Google Maps simplemente continua con la carga
				generateGeoFenceTab(tabProfile);
			}
			//JAPR
			
			//GCRUZ 2016-06-08. Generar el tab para la GeoCerca
			function generateGeoFenceTab(tabCell)
			{
				//Primero eliminar el grid ya aplicado desde generateFormDetail
				objFormTabs.tabs(tabCell).getAttachedObject().cells('a').getAttachedObject().cells('geoFenceTab').detachObject(true);
				var geoFenceLayout = objFormTabs.tabs(tabCell).getAttachedObject().cells('a').getAttachedObject().cells('geoFenceTab').attachLayout("2U");
				var geoFenceMapCell = geoFenceLayout.cells('a');
				var geoFenceDataCell = geoFenceLayout.cells('b');
				geoFenceMapCell.hideArrow();
				geoFenceDataCell.hideArrow();
				geoFenceMapCell.setText("<?=translate('Map')?>");
				geoFenceDataCell.setText("<?=translate('Position Data')?>");
				geoFenceDataCell.setWidth(400);
				//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
				if ( miMapType != <?=maptGoogle?> ) {
					var centerPoint = new mapkit.Coordinate(parseFloat(GeoFenceLat) || 0, parseFloat(GeoFenceLng) || 0), userSpan = new mapkit.CoordinateSpan(.05, .05),
						userRegion = new mapkit.CoordinateRegion(centerPoint, userSpan);
					var mapParams = {
						showMapTypeControl: false,
						showsCompass: false,
						showsZoomControl: true,
						center: centerPoint,
						region: userRegion,
						bitamMapType: <?=$intMapType?>
					};
				}
				else {
					var mapParams = {
						center: new google.maps.LatLng(GeoFenceLat,GeoFenceLng),
						zoom: 18,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						mapTypeControl: false,
						bitamMapType: <?=$intMapType?>
					};
				}
				//JAPR
				
				var geoFenceMap = geoFenceMapCell.attachMap(mapParams), geoFenceCircle = undefined, geoFenceMarker = undefined;
				//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
				if ( miMapType != <?=maptGoogle?> ) {
					var geoFenceCircle = new mapkit.CircleOverlay(centerPoint, GeoFenceRadius, {
						style: new mapkit.Style({
							lineWidth: 2,
							strokeColor: '#FF0000',
							fillColor: '#FF0000'
						})
					});
					
					geoFenceMap.addOverlays([geoFenceCircle]);
					
					var opts = {
						title: '<?=translate('GeoFence Center')?>',
						color: '#FF0000',
						draggable: true
					};
					var geoFenceMarker = new mapkit.MarkerAnnotation(centerPoint, opts);
					geoFenceMarker.addEventListener('drag-end', function(event) {
						geoFenceCircle.coordinate = event.target.coordinate;
						userRegion = new mapkit.CoordinateRegion(event.target.coordinate, geoFenceMap.region.span);
						geoFenceMap.setRegionAnimated(userRegion);
					});
					geoFenceMap.showItems([geoFenceMarker], { 
						animate: true,
						padding: new mapkit.Padding(60, 25, 60, 25)
					});
					
					// Try HTML5 geolocation.
					var bWarning = false;
					if (GeoFenceLat == 0 && GeoFenceLng == 0)
					{
						bWarning = true;
						if (navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(function(position) {
								GeoFenceLat = position.coords.latitude;
								GeoFenceLng = position.coords.longitude;
								var userPosition = new mapkit.Coordinate(GeoFenceLat, GeoFenceLng);
								if ( geoFenceCircle ) {
									geoFenceCircle.coordinate = userPosition;
								}
								if ( geoFenceMarker ) {
									geoFenceMarker.coordinate = userPosition;
								}
								if ( geoFenceMap.region && geoFenceMap.region.span ) {
									userRegion = new mapkit.CoordinateRegion(userPosition, geoFenceMap.region.span);
									geoFenceMap.setRegionAnimated(userRegion);
								}
							}, function() {
								console.log('Error: The Geolocation service failed.');
							});
						}
						else
						{
							console.log('Error: Browser doesn\'t support geolocation.');
						}
					}
				}
				else {
					var geoFenceCircle = new google.maps.Circle({
						strokeColor: '#FF0000',
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: '#FF0000',
						fillOpacity: 0.35,
						map: geoFenceMap,
						center: geoFenceMap.getCenter(),
						radius: GeoFenceRadius
					});
					var input = document.getElementById('geoFenceSearch');
					input.style.display = 'inline';
					input.style.width = 500;
					var searchBox = new google.maps.places.SearchBox(input);
					geoFenceMap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
					var geoFenceMarker = new google.maps.Marker({
						position: geoFenceMap.getCenter(),
						map: geoFenceMap,
						title: '<?=translate('GeoFence Center')?>',
						draggable:true
					});
					
					// Try HTML5 geolocation.
					var bWarning = false;
					if (GeoFenceLat == 0 && GeoFenceLng == 0)
					{
						bWarning = true;
						if (navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(function(position) {
								GeoFenceLat = position.coords.latitude;
								GeoFenceLng = position.coords.longitude;
								var userPosition = new google.maps.LatLng(GeoFenceLat, GeoFenceLng);
								geoFenceCircle.setCenter(userPosition);
								geoFenceMarker.setPosition(userPosition);
								geoFenceMap.setCenter(userPosition);
							}, function() {
								console.log('Error: The Geolocation service failed.');
							});
						}
						else
						{
							console.log('Error: Browser doesn\'t support geolocation.');
						}
					}
				}
				//JAPR

				var objFormData = [
					{type:"fieldset",  name:"fldsLatLng", label:"<?=translate("Current Position")?>", offsetLeft:10, list:[
						{type:"input", name:"txtLat", label:"<?=translate("Latitude")?>", value:GeoFenceLat, labelWidth:100, inputWidth:230, disabled:true, readonly:true},
						{type:"input", name:"txtLng", label:"<?=translate("Longitude")?>", value:GeoFenceLng, labelWidth:100, inputWidth:230, disabled:true, readonly:true}
					]},
					{type:"fieldset",  name:"fldsMap", label:"<?=translate("GeoFence Data")?>", offsetLeft:10, list:[
						{type:"input", name:"txtRadius", label:"<?=translate("Radius (mts)")?>", value:GeoFenceRadius, labelWidth:100, inputWidth:230, className:"search_button"},
						{type:"button", name: "btnSetCenter", value:"<?=translate('Set GeoFence Center on current position')?>", width:280, offsetLeft:30},
						{type:"button", name: "btnReturnCenter", value:"<?=translate('Return to GeoFence Center')?>", width:280, offsetLeft:30}
					]},
					{type:"block", name:'blockWarning', blockOffset:20, list:[
						{type:"label", name: "lblWarning", label:"<div style=\"color:#ea7208;\"><?=translate('warning_geofence_message')?></div>"},
					]},
					{type:"input", name:"ClaUsuario", value:ClaUsuario, hidden:true}
				];
				var objGeoFenceFormData = geoFenceDataCell.attachForm(objFormData);
				if (bWarning)
				{
					objGeoFenceFormData.showItem('blockWarning');
				}
				else
				{
					objGeoFenceFormData.hideItem('blockWarning');
				}

				//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
				if ( miMapType != <?=maptGoogle?> ) {
					geoFenceMap.addEventListener("zoom-end", function(event) {
						console.log('CENTER_CHANGED :: New position: Lat='+event.target.center.latitude+' Lng='+event.target.center.longitude);
						objGeoFenceFormData.getInput('txtLat').value = event.target.center.latitude;
						objGeoFenceFormData.getInput('txtLng').value = event.target.center.longitude;
						var userPosition = new mapkit.Coordinate(event.target.center.latitude, event.target.center.longitude);
						if ( geoFenceCircle ) {
							geoFenceCircle.coordinate = userPosition;
						}
						if ( geoFenceMarker ) {
							geoFenceMarker.coordinate = userPosition;
						}
					});
					geoFenceMap.addEventListener("scroll-end", function(event) {
						console.log('DRAGEND :: New position: Lat='+event.target.center.latitude+' Lng='+event.target.center.longitude);
						objGeoFenceFormData.getInput('txtLat').value = event.target.center.latitude;
						objGeoFenceFormData.getInput('txtLng').value = event.target.center.longitude;
						var userPosition = new mapkit.Coordinate(event.target.center.latitude, event.target.center.longitude);
						if ( geoFenceCircle ) {
							geoFenceCircle.coordinate = userPosition;
						}
						if ( geoFenceMarker ) {
							geoFenceMarker.coordinate = userPosition;
						}
					});
				}
				else {
					google.maps.event.addListener(geoFenceMap,'center_changed',function() {
						console.log('CENTER_CHANGED :: New position: Lat='+geoFenceMap.getCenter().lat()+' Lng='+geoFenceMap.getCenter().lng());
						objGeoFenceFormData.getInput('txtLat').value = geoFenceMap.getCenter().lat();
						objGeoFenceFormData.getInput('txtLng').value = geoFenceMap.getCenter().lng();
					});
					google.maps.event.addListener(geoFenceMap,'dragend',function() {
						console.log('DRAGEND :: New position: Lat='+geoFenceMap.getCenter().lat()+' Lng='+geoFenceMap.getCenter().lng());
						objGeoFenceFormData.getInput('txtLat').value = geoFenceMap.getCenter().lat();
						objGeoFenceFormData.getInput('txtLng').value = geoFenceMap.getCenter().lng();
						geoFenceCircle.setCenter(geoFenceMap.getCenter());
						geoFenceMarker.setPosition(geoFenceMap.getCenter());
					});
				}
				//JAPR

				objGeoFenceFormData.attachEvent('onInputChange', function (name, value, form){
					if (name == 'txtRadius')
					{
						//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
						if ( miMapType != <?=maptGoogle?> ) {
							if ( geoFenceCircle ) {
								geoFenceCircle.radius = parseInt(value);
							}
							doUpdateGeoFence(AdminObjectType.otyUser, parseInt(value), 0, 0);
						}
						else {
							geoFenceCircle.setRadius(parseInt(value));
							doUpdateGeoFence(AdminObjectType.otyUser, geoFenceCircle.getRadius(), 0, 0);
						}
						//JAPR
					}
				});

				objGeoFenceFormData.attachEvent("onButtonClick", function(name){
					if (name == 'btnSetCenter')
					{
						//geoFenceMarker.setPosition(geoFenceMap.getCenter());
						//geoFenceCircle.setCenter(geoFenceMap.getCenter());
						//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
						if ( miMapType != <?=maptGoogle?> ) {
							if ( geoFenceCircle && geoFenceMarker ) {
								doUpdateGeoFence(AdminObjectType.otyUser, geoFenceCircle.radius, geoFenceMarker.coordinate.latitude, geoFenceMarker.coordinate.longitude);
							}
						}
						else {
							doUpdateGeoFence(AdminObjectType.otyUser, geoFenceCircle.getRadius(), geoFenceMarker.getPosition().lat(), geoFenceMarker.getPosition().lng());
						}
						//JAPR
						objGeoFenceFormData.hideItem('blockWarning');
					}
					if (name == 'btnReturnCenter')
					{
						//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
						if ( miMapType != <?=maptGoogle?> ) {
							var userPosition = new mapkit.Coordinate(OriginalGeoFenceLat, OriginalGeoFenceLng);
							if ( geoFenceCircle ) {
								geoFenceCircle.coordinate = userPosition;
							}
							if ( geoFenceMarker ) {
								geoFenceMarker.coordinate = userPosition;
							}
							if ( geoFenceMap.region && geoFenceMap.region.span ) {
								userRegion = new mapkit.CoordinateRegion(userPosition, geoFenceMap.region.span);
								geoFenceMap.setRegionAnimated(userRegion);
							}
						}
						else {
							var originalPosition = new google.maps.LatLng(OriginalGeoFenceLat, OriginalGeoFenceLng);
							geoFenceMap.setCenter(originalPosition);
							geoFenceCircle.setCenter(originalPosition);
							geoFenceMarker.setPosition(originalPosition);
						}
						//JAPR
					}
				});

				//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
				if ( miMapType != <?=maptGoogle?> ) {
				}
				else {
					geoFenceMap.addListener('bounds_changed', function() {
						searchBox.setBounds(geoFenceMap.getBounds());
					});
					// Bias the SearchBox results towards current map's viewport.
					searchBox.addListener('places_changed', function() {
						var places = searchBox.getPlaces();
						
						if (places.length == 0) {
							return;
						}
						console.log(places);
						geoFenceMap.setCenter(places[0].geometry.location);
						objGeoFenceFormData.getInput('txtLat').value = geoFenceMap.getCenter().lat();
						objGeoFenceFormData.getInput('txtLng').value = geoFenceMap.getCenter().lng();
						//geoFenceCircle.setCenter(geoFenceMap.getCenter());
					});
					
					geoFenceMarker.addListener('dragend', function(){
						geoFenceCircle.setCenter(geoFenceMarker.getPosition());
					});
				}
				//JAPR
			}

			function doUpdateGeoFence(iAssocItemType, Radius, Lat, Lng)
			{
				console.log("doUpdateScheduler type == " + iAssocItemType);
				console.log("Radius = " + Radius);
				console.log("Latitude = " + Lat);
				console.log("Longitude = " + Lng);
				
				var objObject = getObject(AdminObjectType.otyUser, UserID);
				//Si está activado el grabado automático durante la edición, se invoca al mismo en este punto sólo con la propiedad modificada
				if (objObject && objObject.send) {
					var objParams = {};
					objParams["UserID"] = UserID;
					objParams["GeoFenceRadius"] = Radius;
					if (Lat != 0)
					{
						objParams["GeoFenceLat"] = Lat;
						//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
						//Corregido un bug, la coordenada no debe cambiar si solo está actualizando el radio
						OriginalGeoFenceLat = Lat;
						//JAPR
					}
					if (Lng != 0)
					{
						objParams["GeoFenceLng"] = Lng;
						//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
						//Corregido un bug, la coordenada no debe cambiar si solo está actualizando el radio
						OriginalGeoFenceLng = Lng;
						//JAPR
					}
					setTimeout(objObject.send(objParams), 100);
				}
			}

			//GCRUZ 2015-08-18
			/* Realiza el request para grabar permisos sobre formas
			*/
			function doUpdateScheduler(iAssocItemType, movingForms, operation) {
				console.log("doUpdateScheduler type == " + iAssocItemType);
				
				if (!movingForms) {
					return;
				}
				
				var strMovingForms = JSON.stringify(movingForms);
				var objObject = getObject(AdminObjectType.otyUser, UserID);
				//Si está activado el grabado automático durante la edición, se invoca al mismo en este punto sólo con la propiedad modificada
				if (objObject && objObject.send) {
					var objParams = {};
					objParams["StrForms"] = strMovingForms;
					objParams["Process"] = "FormPermission";
					objParams["Operation"] = operation;
					objParams["ObjectType"] = iAssocItemType;
					objParams["ClaUsuario"] = ClaUsuario;
					setTimeout(objObject.send(objParams), 100);
				}
			}
			
			//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
			/* Dado un objeto Grid, remueve el filtro aplicado para permitir hacer el borrado de elementos y evitar duplicidad de los elementos del grid */
			function removeGridFilter(oGrid, oForm) {
				if (!oGrid) {
					return;
				}
				
				oGrid.filterBy(0,"");
				oGrid._f_rowsBuffer = null;
			}
			
			/* Dado un objeto Grid, restaura el filtro aplicado para permitir hacer el borrado de elementos */
			function restoreGridFilter(oGrid, oForm) {
				if (!oGrid || !oGrid.bitSearchFieldName) {
					return;
				}
				
				var strLastSearch = oForm.getUserData(oGrid.bitSearchFieldName, "lastSearch");
				if (strLastSearch) {
					oGrid.filterBy(0, strLastSearch);
				}
			}
			
			//GCRUZ 2015-08-18. Tab para permisos sobre formas
			function generateUserFormPermissions(tabFormPermissions)
			{
				var objFormData = [
					{type:"fieldset",  name:"fldsAvailableForms", label:"<?=translate("Available Forms")?>", width:380, className:"selection_list", list:[
						{type:"input", name:"txtSearchAvailableForms", label:"<?=translate("Search")?>", value:"", labelWidth:100, inputWidth:230, className:"search_button"},
						{type:"container", name:"grdAvailableForms", inputWidth:330, inputHeight:200, className:"selection_container"}
					]},
					{type:"newcolumn", offset:20},
					{type:"block", blockOffset:0, offsetTop:200, list:[
						{type:"button", name: "btnAssocForm", value:"", className:"assoc_button"},
						{type:"button", name: "btnRemoveForm", value:"", className:"remove_button"}
					]},
					{type:"newcolumn", offset:20},
					{type:"fieldset",  name:"fldsAssocForms", label:"<?=translate("Selected Forms")?>", width:380, className:"selection_list", list:[
						{type:"input", name:"txtSearchAssocForms", label:"<?=translate("Search")?>", value:"", labelWidth:100, inputWidth:230, className:"search_button"},
						{type:"container", name:"grdAssocForms", inputWidth:330, inputHeight:400, className:"selection_container"}
					]},
					{type:"input", name:"ClaUsuario", value:ClaUsuario, hidden:true}
				];

				//Se tuvo que invocar a la creación de la forma mediante un div, ya que de lo contrario al hacer resize a la forma, sus fieldset y blocks se movían cuando no
				//cupiedan horizontalmente, así con el div se puede poner un Scroll y tenerlos siempre en la misma posición
				objFormTabs.tabs(tabFormPermissions).attachObject("divUserFormPermissionsParent");
				objUserFormPermissions = new dhtmlXForm("divUserFormPermissions", objFormData);
				$(objUserFormPermissions.cont).addClass('customGrid');
				
				//Contenido de la lista de usuarios disponibles
				var objAvailableFormsGrid = new dhtmlXGridObject(objUserFormPermissions.getContainer("grdAvailableForms"));
				objAvailableFormsGrid.setImagePath("<?=$strScriptPath?>/images/");
				objAvailableFormsGrid.setHeader("<?=translate("Forms")?>");
				objAvailableFormsGrid.setInitWidthsP("100");
				objAvailableFormsGrid.setColAlign("left");
				objAvailableFormsGrid.setColTypes("ro");
				objAvailableFormsGrid.enableEditEvents(false);
				objAvailableFormsGrid.enableDragAndDrop(true);
				objAvailableFormsGrid.setColSorting("str");
				objAvailableFormsGrid.enableMultiselect(true);
				objAvailableFormsGrid.init();
				objAvailableFormsGrid.sortRows(0,"str","asc");
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAvailableFormsGrid.bitSearchFieldName = "txtSearchAvailableForms";
				
				//Contenido de la lista de usuarios invitados
				var objAssocFormsGrid = new dhtmlXGridObject(objUserFormPermissions.getContainer("grdAssocForms"));
				objAssocFormsGrid.setImagePath("<?=$strScriptPath?>/images/");
				objAssocFormsGrid.setHeader("<?=translate("Forms")?>");
				objAssocFormsGrid.setInitWidthsP("100");
				objAssocFormsGrid.setColAlign("left");
				objAssocFormsGrid.setColTypes("ro");
				objAssocFormsGrid.enableEditEvents(false);
				objAssocFormsGrid.enableDragAndDrop(true);
				objAssocFormsGrid.setColSorting("str");
				objAssocFormsGrid.enableMultiselect(true);
				objAssocFormsGrid.init();
				objAssocFormsGrid.sortRows(0,"str","asc");
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAssocFormsGrid.bitSearchFieldName = "txtSearchAssocForms";
				
				for (var i=0; i<arrSurveysOrder.length; i++) {
					var surveyID = arrSurveysOrder[i];
					var strRowId = "form"+surveyID;
					var objObject = arrSurveys[surveyID];
					if (arrSelForms[objObject.id]) {
						objAssocFormsGrid.addRow(strRowId, objObject.name);
						objAssocFormsGrid.setUserData(strRowId, "id", objObject.id);
					}
					else {
						objAvailableFormsGrid.addRow(strRowId, objObject.name);
						objAvailableFormsGrid.setUserData(strRowId, "id", objObject.id);
					}
				}

				//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
				objAvailableFormsGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
					console.log('objAvailableFormsGrid.onDrop Forms');
					
					if (sObj == tObj) {
						return;
					}
					var arrSIds = sId.split(',');
					var movingForms = new Object();
					for (var intSIdIndex in arrSIds)
					{
						movingForms[arrSIds[intSIdIndex]] = {id:arrSIds[intSIdIndex], name:tObj.cells(arrSIds[intSIdIndex], 0).getValue()};
					}
					
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					//Durante el drop restaura el filtro que tuviera aplicado cada grid
					restoreGridFilter(sObj, objUserFormPermissions);
					restoreGridFilter(tObj, objUserFormPermissions);
					//JAPR
					
					doUpdateScheduler(AdminObjectType.otyUser, movingForms, 'remove');
				});
				
				//JAPR 2015-07-15: Agregado el evento del doble click
				objAvailableFormsGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
					console.log('objAvailableFormsGrid.onRowDblClicked Forms');
					var movingForms = new Object();
					movingForms[rId] = {id:rId, name:objAvailableFormsGrid.cells(rId, cInd).getValue()};
					
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					removeGridFilter(objAvailableFormsGrid, objUserFormPermissions);
					removeGridFilter(objAssocFormsGrid, objUserFormPermissions);
					this.moveRow(rId, "row_sibling", undefined, objAssocFormsGrid);
					restoreGridFilter(objAvailableFormsGrid, objUserFormPermissions);
					restoreGridFilter(objAssocFormsGrid, objUserFormPermissions);
					//JAPR
					doUpdateScheduler(AdminObjectType.otyUser, movingForms, 'add');
				});
				
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAvailableFormsGrid.attachEvent("onDrag", function(sId,tId,sObj,tObj,sInd,tInd) {
					//Durante el drag (antes de dejar caer el elemento) limpia el filtro aplicado a los grids
					removeGridFilter(sObj, objUserFormPermissions);
					removeGridFilter(tObj, objUserFormPermissions);
					
					return true;
				});
				//JAPR
				
				//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
				objAssocFormsGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
					console.log('objAssocFormsGrid.onDrop Users');
					
					if (sObj == tObj) {
						return;
					}
					var arrSIds = sId.split(',');
					var movingForms = new Object();
					for (var intSIdIndex in arrSIds)
					{
						movingForms[arrSIds[intSIdIndex]] = {id:arrSIds[intSIdIndex], name:tObj.cells(arrSIds[intSIdIndex], 0).getValue()};
					}
					
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					//Durante el drop restaura el filtro que tuviera aplicado cada grid
					restoreGridFilter(sObj, objUserFormPermissions);
					restoreGridFilter(tObj, objUserFormPermissions);
					//JAPR
					
					doUpdateScheduler(AdminObjectType.otyUser, movingForms, 'add');
				});
				
				//JAPR 2015-07-15: Agregado el evento del doble click
				objAssocFormsGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
					var movingForms = new Object();
					movingForms[rId] = {id:rId, name:objAssocFormsGrid.cells(rId, cInd).getValue()};

					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					removeGridFilter(objAvailableFormsGrid, objUserFormPermissions);
					removeGridFilter(objAssocFormsGrid, objUserFormPermissions);
					this.moveRow(rId, "row_sibling", undefined, objAvailableFormsGrid);
					restoreGridFilter(objAvailableFormsGrid, objUserFormPermissions);
					restoreGridFilter(objAssocFormsGrid, objUserFormPermissions);
					//JAPR
					doUpdateScheduler(AdminObjectType.otyUser, movingForms, 'remove');
				});
				
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAssocFormsGrid.attachEvent("onDrag", function(sId,tId,sObj,tObj,sInd,tInd) {
					//Durante el drag (antes de dejar caer el elemento) limpia el filtro aplicado a los grids
					removeGridFilter(sObj, objUserFormPermissions);
					removeGridFilter(tObj, objUserFormPermissions);
					
					return true;
				});
				//JAPR
				
				objUserFormPermissions.attachEvent("onKeyUp",function(inp, ev, name, value) {
					if (!name || !inp || !ev || !ev.keyCode) {
						return;
					}
					
					var objGrid = undefined;
					switch (name) {
						case "txtSearchAvailableForms":
							objGrid = objAvailableFormsGrid;
							break;
						case "txtSearchAssocForms":
							objGrid = objAssocFormsGrid;
							break;
					}
					
					if (!objGrid) {
						return;
					}
					
					//Verifica si ya se había aplicado esta búsqueda (debido por ejemplo a teclas que no se consideran caracteres y que por tanto no alterarían el contenido)
					var strValue = inp.value;
					var strLastSearch = objUserFormPermissions.getUserData(name, "lastSearch");
					if (strLastSearch == strValue) {
						return;
					}
					
					objUserFormPermissions.setUserData(name, "lastSearch", strValue);
					objGrid.filterBy(0, strValue);
				});
				
				//JAPR 2015-07-15: Agregada la funcionalidad para mover los elementos entre grids
				objUserFormPermissions.attachEvent("onButtonClick", function(name) {
					switch (name) {
						case "btnAssocForm":
							var strSelection = objAvailableFormsGrid.getSelectedRowId();
							if (strSelection) {
								var arrSelection = strSelection.split(",");
								if (arrSelection.length) {
									var blnMove = false;
									var movingForms = new Object();
									for (var intIdx in arrSelection) {
										var strId = arrSelection[intIdx];
										if (strId) {
											blnMove = true;
											movingForms[strId] = {id:strId, name:objAvailableFormsGrid.cells(strId, 0).getValue()};
											//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
											removeGridFilter(objAvailableFormsGrid, objUserFormPermissions);
											removeGridFilter(objAssocFormsGrid, objUserFormPermissions);
											objAvailableFormsGrid.moveRow(strId, "row_sibling", undefined, objAssocFormsGrid);
											restoreGridFilter(objAvailableFormsGrid, objUserFormPermissions);
											restoreGridFilter(objAssocFormsGrid, objUserFormPermissions);
											//JAPR
										}
									}
									
									if (blnMove) {
										doUpdateScheduler(AdminObjectType.otyUser, movingForms, 'add');
									}
								}
							}
							break;
						
						case "btnRemoveForm":
							var strSelection = objAssocFormsGrid.getSelectedRowId();
							if (strSelection) {
								var arrSelection = strSelection.split(",");
								if (arrSelection.length) {
									var blnMove = false;
									var movingForms = new Object();
									for (var intIdx in arrSelection) {
										var strId = arrSelection[intIdx];
										if (strId) {
											blnMove = true;
											movingForms[strId] = {id:strId, name:objAssocFormsGrid.cells(strId, 0).getValue()};
											//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
											removeGridFilter(objAvailableFormsGrid, objUserFormPermissions);
											removeGridFilter(objAssocFormsGrid, objUserFormPermissions);
											objAssocFormsGrid.moveRow(strId, "row_sibling", undefined, objAvailableFormsGrid);
											restoreGridFilter(objAvailableFormsGrid, objUserFormPermissions);
											restoreGridFilter(objAssocFormsGrid, objUserFormPermissions);
											//JAPR
										}
									}
									
									if (blnMove) {
										doUpdateScheduler(AdminObjectType.otyUser, movingForms, 'remove');
									}
								}
							}
							break;
					}
				});
			}

			/* Esta función pinta todas las rows de los campos definidos en aTabsColl
			//JAPR 2015-06-18: Agregados los parámetros iObjectType, iObjectID para identificar el objeto actualmente desplegado en las propiedades
			//JAPR 2015-06-20: Agregado el parámetro con el contenedor del grid de propiedades
			*/
			function generateFormDetail(aTabsColl, iObjectType, iObjectID, sTabId) {
				console.log('generateFormDetail');
				
				objPropsLayout = objFormTabs.tabs(sTabId).attachLayout({pattern: "1C"});
				objPropsTabs = objPropsLayout.cells("a");
				objPropsTabs.detachObject(true);
				$(objPropsTabs.cell).addClass("visibleTabs");
				objPropsTabs = objPropsTabs.attachTabbar();
				
				for (var strTabName in aTabsColl) {
					var objTab = aTabsColl[strTabName];
					var blnActive = (objTab.activeOnLoad == true);
					objPropsTabs.addTab(strTabName, objTab.name, null, null, blnActive);
					//Asigna la visibilidad por default de la cejilla
					//JAPR 2015-06-29: Modificada la visibilidad para usar el parámetro "self" para referirse a la propia tab y permitir ocultar campos de la misma
					/*if (!objTab.defaultVisibility || objTab.defaultVisibility["self"] === false) {
						objPropsTabs.tabs(strTabName).hide();
					}*/
					
					//Generar todos los fields del tab
					var objGrid = objPropsTabs.tabs(strTabName).attachGrid('gridbox');
					var strGridStyle = "font-family:Roboto Regular;font-size:13px;"
					objGrid.setStyle(strGridStyle, strGridStyle);
					doGenerateGrid(objGrid, objTab, iObjectType, iObjectID);
				}
			}

			/* Obtiene la referencia al objeto indicado por el tipo y id de los parámetros. Si se necesita un objeto específico de alguna colección que le pertenece, se utilizan 
			los parámetros child (esto se debería aplicar sólo si el objeto principal no tiene una forma directa de ser accesado, originalmente utilizado para las opciones de
			respuesta fijas de preguntas/secciones)
			*/
			function getObject(iObjectType, iObjectID, iChildObjectType, iChildObjectID) {
				var objObject = undefined;
				
				if (!Users) {
					return objObject;
				}

				return Users;
			}
			
			/* Permite hacer ajustes a la pantalla donde DHTMLX no lo hace correctamente */
			function doOnResizeScreen() {
				console.log('doOnResizeScreen');
				//Para permitir que se pinten correctamente las ventanas de Invitar usuario y grupos, es necesario ocultar momentáneamente los divs que las contienen
				//ya que DHTMLX no estaba redimensionando sus componentes al maximizar la ventana
				var intTopAdjust = 120;
				//$('#divInviteUsers .selection_container div.dhxform_container').height($(window).height() - intTopAdjust - 50);
				var intHeight = $(window).height();
				$('.selection_container div.dhxform_container').height(intHeight - intTopAdjust - 50);
				$('.selection_container div.objbox').height(intHeight - intTopAdjust - 90);
				$('.selection_list fieldset.dhxform_fs').height(intHeight - intTopAdjust);
				//JAPRWarning: Remover esto para regresar a Tabs como objeto principal
//				if (objMainLayout) {
//					objMainLayout.setSizes();
//				}
//				if (objFormTabs) {
//					objFormTabs.setSizes();
//				}
				//JAPRWarning: Remover esto para regresar a Tabs como objeto principal (End)
			}
			
			function doChangeTab(sTabId) {
				console.log('doChangeTab ' + sTabId);

				var objEvent = this.event || window.event;
				if (objEvent) {
					var objTarget = objEvent.target || objEvent.srcElement;
					if (objTarget) {
						$("#divDesignHeader td").removeClass("linktdSelected");
						$(objTarget).addClass("linktdSelected");
					}
				}
				if (!sTabId || !objFormTabs || !objFormTabs.tabs(sTabId)) {
					return;
				}

				switch (sTabId) {
			        case tabProfile:
			            break;
			        case tabCatalog:
			        	
			            break;
			        case tabModel:
			            break;
			        case tabNotification:
			            break;
					case tabFormPermissions:
						break;
			        
			        default:
			            break;
			    }
				
				objFormTabs.tabs(sTabId).setActive();
			}

			//******************************************************************************************************
			function FieldCls(sObjectDef) {
				//Constructor
				this.id = 0;											//ID numérico del objeto que se está editando (SurveyID/SectionID/QuestionID según el caso)
				this.name = "";											//Nombre único del campo
				this.label = "";										//Etiqueta para desplegar en la lista de configuraciones del objeto
				this.type = GFieldTypes.alphanum;						//Tipo de componente para captura
				this.parentType = AdminObjectType.otyUser;			//Tipo de objeto del que se extraen los valores (Survey/Section/Question)
				this.maxLength = 0;										//Sólo para tipos con texto, es la longitud del texto a capturar
				this.showWhenNew = true;								//Indica si el campo debe ser o no capturable cuando el objeto es nuevo (id == -1) o sólo al editarlo
				this.useOrder = true;									//Indica si está o no definido un array de optionsOrder para ordenar el contenido de la combo (se asigna automáticamente al ejecutar getOptions)
				this.options = new Object();							//Colección de los valores en los campos tipo Combo o Lista (puede ser una función que obtiene dicho array)
				this._options = new Object();							//Array interno posterior a evaluar getOptions
				this.optionsOrder = new Array();						//Array con los Ids de opciones ordenados tal como se deben ver (puede ser una función que obtiene dicho array)
				this._optionsOrder = new Array();						//Array interno posterior a evaluar getOptions
				this.parentField = undefined;							//Referencia al campo del que dependen las opciones de respuesta
				this.tab = "";											//Tab al cual pertenece el campo
				this.customButtoms = new Array;							//Definir cuales son los botones personalizados que se desplegarán en el editor
																		//Botones por default: bold, italic, underline
																		//Botones personalizados (configurables en customButtoms):
																		//backColor, fontName, fontSize, foreColor
				this.readOnly = false;									//Indica si se debe o no permitir editar el campo (si no se permite, se cambia a "ro" su tipo interno)
				this.resize = undefined;								//Sólo modo "collection". Indica si la columna permitirá el auto-resize o no (default == true)
				this.default = undefined;								//Valor default del campo (puede ser una función que regrese el valor a utilizar)
				
				//Extiende las propiedades default si es que fueron especificadas
				if (sObjectDef) {
					$.extend(this, sObjectDef);
				}
				
				/* En caso de que el objeto options sea una función, devuelve la lista real de objetos y actualiza el array optionsOrder utilizando dicha función */
				this.getOptions = function() {
					this.useOrder = true;
					this._options = this.options;
					this._optionsOrder = this.optionsOrder;
					if (!this._optionsOrder || ($.isArray(this._optionsOrder) && !this._optionsOrder.length)) {
						this.useOrder = false;
						this._optionsOrder = this._options;
					}
					
					//Si se trata de funciones, las ejecuta para obtener la colección real de valores
					if (typeof this.options == "function") {
						this._options = this.options();
						if (!this._options) {
							this._options = new Object();
						}
					}
					if (typeof this.optionsOrder == "function") {
						this._optionsOrder = this.optionsOrder();
						if (!this._optionsOrder) {
							this._optionsOrder = new Array();
						}
					}
					
					return this._options;
				}
				
				/* Obtiene el valor del campo directamente del objeto en memoria que representa a la forma (selSurvey)
				Si se especifica el parámetro bRaw, entonces se obtendrá el valor natural del campo (por ejemplo el integer con el id en caso de ser una combo), de lo contrario se
				obtendrá el valor string para ser presentado en el Grid, el cual es sólo una representación visual y no se debe usar para grabar nada
				//JAPR 2015-06-29: Agregados los parámetros iObjectType e iObjectID para permitir variar el tipo de objeto del que se extraen las propiedades. Utilizado durante la generación
				de tabs tipo collection en la que los valores representan objetos hijos del definido inicialmente en la tab. Si no se especifican, se usarán los defindos en el FieldCls
				*/
				this.getValue = function(bRaw, iObjectType, iObjectID) {
					console.log('FieldCls.getValue ' + bRaw);
					
					//Accesar al objeto selSurvey (al nivel del frame principal), al Survey/Section/Question correspondiente, para pedirle la propiedad del name
					var objObject = getObject(this.parentType, this.id, iObjectType, iObjectID);
					if (!objObject) {
						return "";
					}
					
					//El valor numérico en caso de tratarse de una propiedad tipo combo
					var varValue = objObject[this.name];
					if (bRaw) {
						//El valor raw es en que finalmente se graba en la metadata, así que se obtiene directamente del objeto en cuestión
						switch (this.type) {
							case GFieldTypes.grid:
								//Si se trata de un grid, se usará un valor dummy ya que se va a generar programáticamente
								varValue = 1;
								break;
						}
					}
					else {
						//El valor no raw es el que el componente requiere para pintarse correctamente en el grid, así que puede recibir tratamiento especial
						switch (this.type) {
							case GFieldTypes.multiList:
								//En el caso de una lista múltiple de selección, el valor por default es el conjunto de las descripciones del valor real del campo, el cual es un array
								var varTempValue = '';
								var strAnd = '';
								var blnUseOrder = true;
								var cmbOptions = new Object();
								var cmbOptionsOrder = new Array();
								if (this.getOptions) {
									var cmbOptions = this.getOptions();
									var cmbOptionsOrder = this._optionsOrder;
									var blnUseOrder = this.useOrder;
								}
								
								if (cmbOptions) {
									for (intIdx in varValue) {
										var intObjectID = varValue[intIdx];
										for (var intId in cmbOptionsOrder) {
											var intTmpObjectID = cmbOptionsOrder[intId];
											if (intTmpObjectID == intObjectID) {
												varTempValue += strAnd + cmbOptions[intId];
												strAnd = ',';
												break;
											}
										}
									}
								}
								varValue = varTempValue;
								break;
						
							case GFieldTypes.grid:
								//Si se trata de un grid, se usará un valor dummy ya que se va a generar programáticamente
								varValue = 1;
								break;
							case GFieldTypes.image:
								//Si se trata de una imagen, en caso de no venir asignado el valor directo con el nombre del campo, se verifica si está definida una imagen
								//en el campo y en ese caso se regresa directamente la imagen de la definición, la cual funciona como un default
								if (varValue === undefined && this.image) {
									varValue = this.image;
								}
								
								//Para el grid la imagen separa sus propiedades en el formato: urlImagen^alttext^javascript_OR_url^target
								var strLink = this.click;
								if (strLink) {
									varValue = varValue + "^^" + strLink;
								}
								break;
						
							//case GFieldTypes.comboSync:
							case GFieldTypes.combo:
								//Si se trata de una combo, debe buscar el valor dentro de las opciones disponibles del campo y regresar el texto que la representa
								//Los posibles valores de estas combos tienen que ser IDs enteros, así que en caso de no ser un valor entero, se asumirá 0
								//(esto puede pasar si el objeto en cuestión no tiene la propiedad indicada porque no aplicaba, y generalmente pasará sólo con
								//las combos que tienen opciones de Si/No)
								if (!$.isNumeric(varValue) && this.default !== undefined) {
									varValue = this.default;
								}
								
								var blnFound = false;
								var blnUseOrder = true;
								var cmbOptions = new Object();
								var cmbOptionsOrder = new Array();
								if (this.getOptions) {
									var cmbOptions = this.getOptions();
									var cmbOptionsOrder = this._optionsOrder;
									var blnUseOrder = this.useOrder;
								}
								
								if (cmbOptions) {
									for (var intId in cmbOptions) {
										if (intId == varValue) {
											var objValue = cmbOptions[intId];
											varValue = (objValue.name)?objValue.name:objValue;
											blnFound = true;
											break;
										}
									}
								}
								
								//Si se permite un elemento vacío, automáticamente usa esa descripción como el valor inicial
								if (!blnFound) {
									varValue = "(<?=translate("None")?>)";
									blnFound = true;
								}
								
								/*if (!blnFound) {
									varValue = "<?=translate("Unknown")?>";
								}*/
								break;
						}
					}
					
					//Dependiendo del tipo de columna, se 
					
					return  varValue;
				}

				this.setValue = function(iObjectType, iObjectID, val) {
					console.log('FieldCls.setValue ' + val);
					//Accesar al objeto selSurvey (al nivel del frame principal), al Survey/Section/Question correspondiente, para pedirle la propiedad del name
					var objObject = getObject(this.parentType, this.id, iObjectType, iObjectID);
					if (!objObject) {
						return "";
					}
					
					//El valor numérico en caso de tratarse de una propiedad tipo combo
					objObject[this.name] = val;
				}
				
				/* Obtiene el valor del campo directamente del grid donde se está definiendo la propiedad, así que es el valor que habría configurado el usuario en caso de haberlo
				cambiado
				Si se especifica el parámetro bRaw, entonces se obtendrá el valor natural del campo (por ejemplo el integer con el id en caso de ser una combo), de lo contrario se
				obtendrá el valor string para ser presentado en el Grid, el cual es sólo una representación visual y no se debe usar para grabar nada
				*/
				this.getPropValue = function(bRaw) {
					console.log('FieldCls.getPropValue ' + bRaw);
					
					if (!objPropsTabs) {
						return "";
					}
					
					var varValue = ""
					var objTempGrid = undefined;
					if (objPropsTabs.tabs(this.tab)) {
						objTempGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
						if (objTempGrid) {
							if (bRaw) {
								varValue = objTempGrid.getRowAttribute(this.name, "value");
							}
							else {
								if (objTempGrid.cells(this.name, 1)) {
									varValue = objTempGrid.cells(this.name, 1).getValue();
								}
							}
							
							if (varValue === undefined || varValue === null || varValue === NaN) {
								varValue = "";
							}
						}
					}
					
					return varValue;
				}
				
				// Utilizada para validar mínimos, máximos o cualquier otra cosa que se requiera en el field
				this.validate = function() {
					return true;
				}
				
				// Determina si es o no visible la configuración
				this.visible = function() {
					return true;
				}
				
				//Agrega la row que representa este campo/configuración en el grid correspondiente
				this.draw = function(oGrid, iObjectType, iObjectID) {
					console.log('FieldCls.draw');
					
					rowValues = new Array;
					rowValues[0] = this.label;
					rowValues[1] = this.getValue(undefined, iObjectType, iObjectID);
					//JAPR 2015-06-18: Validado que al forzar el llenado manual de los componentes, no se lance el evento de onCellChanged
					//agregando una bandera temporal al grid
					oGrid.bitDisableEvents = true;
					oGrid.addRow(this.name, rowValues);
					oGrid.setRowAttribute(this.name, "tabName", this.tab);
					oGrid.setRowAttribute(this.name, "fieldName", this.name);
					//oGrid.setRowAttribute(this.name, "options", this.options);
					oGrid.setRowAttribute(this.name, "type", this.type);
					oGrid.setRowAttribute(this.name, "objectType", this.parentType);
					oGrid.setRowAttribute(this.name, "value", this.getValue(true, iObjectType, iObjectID));
					if (this.hidden) {
						oGrid.setRowHidden(this.name, true);
					}
					oGrid.setCellExcellType(this.name, 1, this.type);
					if (this.type == GFieldTypes.multiList) {
						var arrOptions = new Array();
						for (var intOptionNum in this.options) {
							arrOptions.push(this.options[intOptionNum]);
						}
						oGrid.registerCList(1, arrOptions);
					}
					oGrid.bitDisableEvents = false;
					//JAPR
					
					return true;
				}
			}
			//******************************************************************************************************
			//Class definitions
			//******************************************************************************************************
			function GenericItemCls() {
			}
			GenericItemCls.prototype.getDataFields = function(oFields) {}
			
			//Regresa el arra POST con los datos genéricos que identifica el tipo de objeto del que se trata
			GenericItemCls.prototype.getObjFields = function() {
				var objParams = {
					UserID:UserID,
					RequestType:reqAjax,
					Process:"Edit",
					ObjectType:this.objectType
				};
				
				//Agrega el campo ID específico del objeto. Siempre debe existir una propiedad id correspondiente la instancia
				if (this.objectIDFieldName) {
					objParams[this.objectIDFieldName] = this.id;
				}
				//Agrega los campos padres del objeto. En este caso es una colección con la lista de padres en la forma {fieldName:propName, ...} y todos se deben
				//agregar a la lista de parámetros además de existir en la instancia con el nombre indicado
				if (this.parentFields) {
					for (var strFieldName in this.parentFields) {
						var strPropName = this.parentFields[strFieldName];
						if (strPropName && this[strPropName]) {
							objParams[strFieldName] = this[strPropName];
						}
					}
				}
				
				return objParams;
			}
			
			/* A partir de la definición del objeto (el cual ya debería estar creado en el servidor), prepara el array de datos a enviar para actualizar su
			información mediante Ajax, además de reportar cualquier error ocurrido en el proceso
			La actualización de información se realizará dependiendo del método configurado con la variable gbFullSave
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			*/
			GenericItemCls.prototype.save = function(oFields) {
				console.log("GenericItemCls.save");

				//Prepara los parámetros con los datos a grabar
				var objFieldsData = this.getDataFields(oFields);
				//@AAL Si se quiere actualizar el FirstName/LastName/Password, se agregara como parametro el Email. Esto se realizará al editar en el processRequest
				if (objFieldsData["FirstName"] || objFieldsData["LastName"] || objFieldsData["Password"]) {
					objFieldsData["Email"] = Users.Email;
				}

				if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
					return;
				}
				
				var objParams = $.extend({}, this.getObjFields(), objFieldsData);
				if (!objParams) {
					return;
				}

				//GCRUZ 2016-05-05. Agregar idioma
				reloadOnLanguage = false;
				if (objFieldsData['UsrLanguage'])
				{
					if (objParams['UserID'] == "<?= $_SESSION['PAtheAppUserID'] ?>")
					{
						reloadOnLanguage = true;
					}
				}
				
				var strParams = '';
				var strAnd = '';
				for (var strProp in objParams) {
					var varValue = objParams[strProp];
					if ($.isArray(varValue) || $.isPlainObject(varValue)) {
						//En este caso el parámetro se genera también como array
						for (var intIndex in varValue) {
							//JAPR 2018-11-01: Corregido un bug con el escape de caracteres en requests (#S0SRC1)
							strParams += strAnd + strProp + "[" + intIndex + "]=" + encodeURIComponent(varValue[intIndex]);
							//JAPR
							strAnd = '&';
						}
					}
					else {
						//JAPR 2018-11-01: Corregido un bug con el escape de caracteres en requests (#S0SRC1)
						strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
						//JAPR
					}
					strAnd = '&';
				}
				
				$('#divSaving').show();
				window.dhx4.ajax.post("processRequest.php", strParams, doSaveConfirmation);
			}
			
			/* Similar a la función save, a excepción que el parámetro oFields no representa propiedades de la instancia sino el conjunto de parámetros a enviar
			directamente, así que se puede hacer para basar el request en un tipo de objeto pero mandando datos que no pertenecen a él (originalmente usado para
			invitar usuarios/grupos a una forma). El parámetro oFields puede sobreescribir las propiedades defalt de getObjFields
			*/
			GenericItemCls.prototype.send = function(oFields) {
				console.log("GenericItemCls.send");
			
				//Prepara los parámetros con los datos a grabar
				var objFieldsData = oFields;
				if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
					return;
				}
				
				var objParams = $.extend({}, this.getObjFields(), objFieldsData);
				if (!objParams) {
					return;
				}
				
				var strParams = '';
				var strAnd = '';
				for (var strProp in objParams) {
					//GCRUZ 2015-12-15. Formas con & en el nombre. Issue: BSPRO2
					strParams += strAnd + strProp+"="+encodeURIComponent(objParams[strProp]);
					strAnd = '&';
				}
				
				$('#divSaving').show();
				window.dhx4.ajax.post("processRequest.php", strParams, doSaveConfirmation);
			}
			
			/* Remueve todo tag HTML del nombre del objeto para regresar un nombre que sea presentable a los usuarios, en caso de que el nombre quedara vacío, intenta utilizar
			el shortName del objeto si es que tiene uno
			*/
			GenericItemCls.prototype.getName = function() {
				var strName = (this.nameHTML)?this.nameHTML:"";
				strName = $.trim(strName.replace(new RegExp("<[^>]*>", "gi"), ''));
				if (!strName) {
					if (this.shortName) {
						strName = this.shortName;
					}
				}
				
				return strName;
			}
			
			//******************************************************************************************************
			UserCls.prototype = new GenericItemCls();
			UserCls.prototype.constructor = UserCls;
			function UserCls() {
				this.objectType = AdminObjectType.otyUser;
			}
			/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			*/
			UserCls.prototype.getDataFields = function(oFields) {
				var blnAllFields = false;
				//if (gbFullSave || !oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
				if (!oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
					blnAllFields = true;
				}
				
				var objParams = {};
				if (blnAllFields || oFields["UserID"]) {
					objParams["UserID"] = this.UserID;
				}
				if (blnAllFields || oFields["UserName"]) {
					objParams["UserName"] = this.UserName;
				}
				if (blnAllFields || oFields["Email"]) {
					objParams["Email"] = this.Email;
				}
				if (blnAllFields || oFields["AltEmail"]) {
					objParams["AltEmail"] = this.AltEmail;
				}
				if (blnAllFields || oFields["FullName"]) {
					objParams["FullName"] = this.FullName;
				}
				if (blnAllFields || oFields["FirstName"]) {
					objParams["FirstName"] = this.FirstName;
				}
				if (blnAllFields || oFields["LastName"]) {
					objParams["LastName"] = this.LastName;
				}
				if (blnAllFields || oFields["Password"]) {
					objParams["Password"] = this.Password;
				}
				if (blnAllFields || oFields["SendPDFByEmail"]) {
					objParams["SendPDFByEmail"] = this.SendPDFByEmail;
				}
				if (blnAllFields || oFields["DefaultSurvey"]) {
					objParams["DefaultSurvey"] = this.DefaultSurvey;
				}
				if (blnAllFields || oFields["TemplateID"]) {
					objParams["TemplateID"] = this.TemplateID;
				}
				/*if (blnAllFields || oFields["role"]) {
					objParams["Role"] = this.role;
				}*/
				if (blnAllFields || oFields["GroupsAssct"]) {
					objParams["GroupsAssct"] = this.GroupsAssct;
				}
				if (blnAllFields || oFields["DissociateAll"]) {
					objParams["DissociateAll"] = this.DissociateAll;
				}
				if (blnAllFields || oFields["Image"]) {
					objParams["Image"] = this.Image;
				}
				if (blnAllFields || oFields["UsrLanguage"]) {
					objParams["UsrLanguage"] = this.UsrLanguage;
				}
				if (blnAllFields || oFields["UserParam1"]) {
					objParams["UserParam1"] = this.UserParam1;
				}
				if (blnAllFields || oFields["UserParam2"]) {
					objParams["UserParam2"] = this.UserParam2;
				}
				if (blnAllFields || oFields["UserParam3"]) {
					objParams["UserParam3"] = this.UserParam3;
				}
				//JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
				if (blnAllFields || oFields["DefaultKPIProduct"]) {
					objParams["DefaultKPIProduct"] = this.DefaultKPIProduct;
				}
				//JAPR
				
				return objParams;
			}

			/* Configura un grid previamente generado basado en el objeto con el formato similar a un TabCls, de tal manera que se puedan generar subGrids e invocar a este
			método para generarlos. Si se necesita un objeto específico de alguna colección que le pertenece, se utilizan 
			los parámetros child (esto se debería aplicar sólo si el objeto principal no tiene una forma directa de ser accesado, originalmente utilizado para las opciones de
			respuesta fijas de preguntas/secciones)
			*/
			function doGenerateGrid(oGrid, oSettings, iObjectType, iObjectID, iChildObjectType, iChildObjectID) {
				var objGrid = oGrid;
				var objTab = oSettings;
				if (!objGrid || !objTab) {
					return;
				}
				
				var objValues = new Object();
				var objValuesOrder = new Array();
				switch (objTab.type) {
					case propTabType.collection:
						//Se debe generar el Grid sólo como una colección de valores con un mínimo de propiedades id y Name, siendo el id el rowId y el Name la columna default
						var strHeader = "";
						var strHeaderWidthPerc = "";
						var strHeaderWidth = "";
						var strHeaderAlign = "";
						var strHeaderTypes = "";
						var strHeaderResize = "";
						var arrHeaderStyle = new Array();
						var strAnd = "";
						var intPos = -1;
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							if (aField) {
								strHeader += strAnd + aField.label;
								var strWidth = $.trim(aField.width);
								if ((intPos = strWidth.indexOf("%")) != -1) {
									strWidth = strWidth.substr(0, intPos);
									strHeaderWidthPerc += strAnd + ((strWidth)?strWidth:10);
								}
								else {
									strHeaderWidth += strAnd + ((strWidth)?strWidth:10);
								}
								strHeaderAlign += strAnd + ((aField.align)?aField.align:"left");
								strHeaderTypes += strAnd + ((aField.type)?aField.type:"ro");
								strHeaderResize += strAnd + ((aField.resize !== undefined)?aField.resize:true);
								arrHeaderStyle.push("background-color:cccccc;");
								strAnd = ",";
							}
						}
						
						objGrid.setHeader(strHeader, null, arrHeaderStyle);
						if (strHeaderWidthPerc) {
							objGrid.setInitWidthsP(strHeaderWidthPerc);
						}
						else {
							objGrid.setInitWidths(strHeaderWidth);
						}
						objGrid.setColAlign(strHeaderAlign);
						objGrid.setColTypes(strHeaderTypes);
						objGrid.enableMarkedCells();
						objGrid.enableResizing(strHeaderResize);
						objGrid.bitObjectType = iObjectType;
						objGrid.bitObjectID = iObjectID;
						objGrid.bitTabName = objTab.id;
						
						objGrid.bitValuesRendered = 0;
						var objValues = new Object();
						var objValuesOrder = new Array();
						if (objTab.getValues) {
							objValues = objTab.getValues();
							objValuesOrder = objTab._valuesOrder;
						}
						objGrid.bitValuesLength = objValuesOrder.length;
						
						//Realiza ajustes específicos a las columnas según el tipo de cada una
						var intColIndex = 0;
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							if (aField) {
								switch (aField.type) {
									case GFieldTypes.multiList:
										var cmbOptions = new Object();
										var cmbOptionsOrder = new Array();
										if (aField.getOptions) {
											var cmbOptions = aField.getOptions();
											var cmbOptionsOrder = aField._optionsOrder;
											var blnUseOrder = aField.useOrder;
										}
										objGrid.registerCList(intColIndex, cmbOptions);
										break;
								
									case GFieldTypes.combo:
										//Obtiene la referencia a la combo y le asigna los valores por default que tiene el field
										var cmbOptions = new Object();
										var cmbOptionsOrder = new Array();
										if (aField.getOptions) {
											var cmbOptions = aField.getOptions();
											var cmbOptionsOrder = aField._optionsOrder;
											var blnUseOrder = aField.useOrder;
										}
										
										var objCombo = objGrid.getCombo(intColIndex);
										if (objCombo) {
											objCombo.clear();
											if (aField.empty) {
												var idOption = 0;
												if (aField.emptyId !== undefined) {
													var idOption = aField.emptyId;
												}
												var strName = "(<?=translate("None")?>)";
												objCombo.put(idOption, strName);
											}
											
											for (var idOption in cmbOptions) {
												var strName = cmbOptions[idOption].name;
												if (strName === undefined) {
													strName = cmbOptions[idOption];
												}
												objCombo.put(idOption, strName);
											}
										}
										break;
								}
								intColIndex++;
							}
						}
						
						objGrid.attachEvent("onCellMarked", function(rid, ind) {
							this.bitLastSelectedRowId = rid;
							this.bitLastSelectedColId = ind;
							if (rid == "new") {
								addNewOption(this, rid, ind);
							}
						});
						
						objGrid.attachEvent("onEditCell", function(stage,id,index) {
							var intObjectType = this.getRowAttribute(id, "objectType");
							var blnForForm = (intObjectType == AdminObjectType.otyUser);
							var strTabName = this.getRowAttribute(id, "tabName");
							var objField = getFieldDefinitionByIndex(strTabName, index, blnForForm);
							if (objField) {
								if (objField.readOnly) {
									//Desactiva la edición de la celda
									return false;
								}
							}
							
							//Habilita la edición de la celda
							return true;
						});
						
						objGrid.attachEvent("onRowCreated", function(rId,rObj,rXml) {
							this.bitValuesRendered += 1;
							if (this.entBox && this.bitValuesRendered >= this.bitValuesLength) {
								setTimeout(function() {
									$(objGrid.entBox).find('img').css('display', 'inline');
									//$(objGrid.entBox).find('img').css('width', '5px');
									//$(objGrid.entBox).find('img').css('height', '8px');
									$(objGrid.entBox).find('img').css('cursor', 'pointer');
									$(objGrid.entBox).find('img').parents('td').attr('align', 'center');
								}, 100);
							}
						});						
						
						objGrid.attachEvent("onSubGridCreated", function(subgrid, rId, rInd) {
							//Primero obtiene la definición de la estructura del grid de detalles mediante la combinación TabName + FieldIdx, ya que las colecciones son
							//las únicas que generan un subGrid
							var strTabName = this.bitTabName;
							if (!objTabsColl || !strTabName || !objTabsColl[strTabName]) {
								return false;
							}
							
							var objCollectionTab = objTabsColl[strTabName];
							if (!objCollectionTab.fields || !objCollectionTab.fields[rInd]) {
								return false;
							}
							
							var objGridField = objCollectionTab.fields[rInd];
							if (!objGridField.definition || !objTabsColl[objGridField.definition]) {
								return false;
							}
							var objDefinitionTab = objTabsColl[objGridField.definition];
							
							//Genera el grid utilizando la definición del tab de detalles
							var strGridStyle = "font-family:Roboto Regular;font-size:12px;"
							subgrid.setStyle(strGridStyle, strGridStyle);
							subgrid.setNoHeader(true);
							doGenerateGrid(subgrid, objDefinitionTab, this.bitObjectType, this.bitObjectID, objCollectionTab.childType, rId);
							if (subgrid.entBox) {
								//$(objPropsTabs.cells("valuesTab").getAttachedObject().entBox).find('img').css('display', 'inline');
								//$(objPropsTabs.cells("valuesTab").getAttachedObject().entBox).find('img').parents('td').attr('align', 'center');
								//$(subgrid.entBox).find('img').parent('td').attr('align', 'center');
								setTimeout(function() {
									var intTop = parseInt($(subgrid.entBox).css('top').replace('px', ''));
									if (!$.isNumeric(intTop)) {
										intTop = 0;
									}
									
									//$(subgrid.entBox).css('top', (intTop - 5)+'px')
									//$(subgrid.entBox).css('overflow', 'visible');
									
									if (subgrid.hdrBox) {
										var intHeight = $(subgrid.hdrBox).find('div.xhdr').css('height');
										$(subgrid.hdrBox).find('div.xhdr').css('height', '26px');
										$(subgrid.hdrBox).find('div.hdrcell').css('line-height', '26px');
									}
								}, 100);
							}
							//return true;
						});
						break;
					
					default:
						//Cada row representa una propiedad que se puede configurar de manera diferente según el tipo de campo
						objGrid.setHeader("<?=translate("Property")?>,<?=translate("Value")?>", null, ["background-color:cccccc;","background-color:cccccc;"]);
						objGrid.setInitWidthsP("30,70");	//50,50
						objGrid.setColAlign("left,left");
						objGrid.setColTypes("ro,ed");
						//objGrid.setNumberFormat("0,000.00",1,".",",");
						objGrid.enableEditEvents(true);
						objGrid.enableMarkedCells();
						objGrid.bitObjectType = iObjectType;
						objGrid.bitObjectID = iObjectID;
						objGrid.bitChildObjectType = iChildObjectType;
						objGrid.bitChildObjectID = iChildObjectID;
						
						objGrid.attachEvent("onCellChanged", function(rId, cInd, nValue) {
							console.log('onCellChanged rId == ' + rId + ', cInd == ' + cInd + ', nValue == ' + nValue);
							
							//JAPR 2015-06-18: Validado que al forzar el llenado manual de los componentes, no se lance el evento de onCellChanged
							//agregando una bandera temporal al grid
							if (this.bitDisableEvents) {
								return;
							}
							
							var varValue = nValue;
							try {
								//El componente GFieldTypes.multiList se actualiza con este evento, pero sus valores no se deben grabar como String sino como un array de IDs, así que
								//primero hace la conversión basado en el array de opciones que presentó el componente mientras estaba visible
								var blnUpdate = false;
								var strTabName = this.getRowAttribute(rId, "tabName");
								var intObjectType = this.getRowAttribute(rId, "objectType");
								var blnForForm = (intObjectType == AdminObjectType.otyUser);
								var objField = getFieldDefinition(strTabName, rId, blnForForm);
								if (objField) {
									switch (objField.type) {
										case GFieldTypes.multiList:
											var cmbOptions = new Object();
											var cmbOptionsOrder = new Array();
											if (objField.getOptions) {
												var cmbOptions = objField.getOptions();
												var cmbOptionsOrder = objField._optionsOrder;
												var blnUseOrder = objField.useOrder;
											}
											
											var arrValues =[];
											if (nValue) {
												var arrSelection = nValue.split(",");
												if (arrSelection && $.isArray(arrSelection) && arrSelection.length) {
													//Debe buscar la descripción (que para este componente debería ser única) en el array de opciones del campo para almacenar
													//internamente la colección de IDs seleccionados
													for (var intSelIdx in arrSelection) {
														var strDescription = $.trim(arrSelection[intSelIdx]);
														//JAPR 2016-07-29: Corregido un bug, removidos los doble espacios convertidos a &nbsp; por DHTMLX (#W34WOC) (#QHYOU7)
														strDescription = html_entity_decode($.trim(strDescription)); //strDescription.replace(/&nbsp;/gi, ' ');
														//JAPR
														for (var intIdx in cmbOptions) {
															//JAPR 2016-11-15: Corregido un bug, para estandarizar se hace el mismo trim que con el texto a comparar (#W34WOC)
															var strOptionName = $.trim(cmbOptions[intIdx]);
															if (strDescription == strOptionName) {
																//Esta opción fue seleccionada, debe grabar internamente el ID
																var ValueID = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
																arrValues.push(ValueID);
																break;
															}
														}
													}
												}
											}
											
											blnUpdate = true;
											varValue = arrValues.join();
											//this.setRowAttribute(rId, "value", arrShowQuestionIDs);
										break;
										case GFieldTypes.uploadImg:
											blnUpdate = false;
										break;

										default:
											blnUpdate = true;
											break;
									}
								}
								
								if (blnUpdate) {
									doUpdateProperty(this, iObjectType, iObjectID, rId, cInd, varValue);
								}
							} catch(e) {
								console.log ("Error updating property value (type=" + iObjectType + ", id=" + iObjectID + ", prop=" + rId + "):" + e);
								debugger;
							}
						});
						
						objGrid.attachEvent("onCellMarked", function(rid, ind) {
							this.bitLastSelectedRowId = rid;
							this.bitLastSelectedColId = ind;
							if (ind != 1) {
								this.mark(rid, ind, false);
								this.mark(rid, 1, true);
							}
						});
						
						objGrid.attachEvent("onEditCell", /*$.proxy(*/function(stage,id,index) {
							//AAL 19/07/2015 Agregado para ajustar el Height del componente MiltipleList, cuado hay una lista grande, ésta desbordaba.
							if ($(".dhx_clist").height() > 100) {
								$(".dhx_clist").css({"height": "200px", "overflow-y": "scroll"});
							}

							var ind = this.getRowIndex(id);	//this.objGrid == this with Proxy							
							if (index != 1) {
								return true;
							}
							
							var intObjectType = this.getRowAttribute(id, "objectType");
							var blnForForm = (intObjectType == AdminObjectType.otyUser);
							switch (this.getRowAttribute(id, "type")) {
								case GFieldTypes.multiList:
									var strTabName = this.getRowAttribute(id, "tabName");
									var objField = getFieldDefinition(strTabName, id, blnForForm);
									
									var cmbOptions = new Object();
									var cmbOptionsOrder = new Array();
									var objField = getFieldDefinition(strTabName, id, blnForForm);
									if (objField && objField.getOptions) {
										var cmbOptions = objField.getOptions();
										var cmbOptionsOrder = objField._optionsOrder;
										var blnUseOrder = objField.useOrder;
									}
									
									switch (stage) {
										case 0:	//Before start
											//Antes de iniciar, se debe llenar la columna con la lista de opciones corresponiente utilizando sólo las descripciones, ya que este
											//componente no soporta uso de ids
											this.registerCList(1, cmbOptions);
											break;
										
										case 1:
											$('.dhx_clist input:button').before('<img src="images/admin/selectall.jpg" style="width:22px; height:22px;" onclick="javascript:fnSelectAllCListItems($(this).parents(\'.dhx_clist\')[0], true);" />');
											$('.dhx_clist input:button').before('<img src="images/admin/unselect.jpg" style="width:22px; height:22px;" onclick="javascript:fnSelectAllCListItems($(this).parents(\'.dhx_clist\')[0], false);" />');
											$('.dhx_clist input:button').attr('value', '<?=translate("Apply")?>')
											break;
										
										case 2:	//Editor closed
											
											break;
									}
									break;
							
								case GFieldTypes.alphanum:
									switch (stage) {
										case 1:	//The editor is opened
											//Cuando se muestra el componente para captura, verifica si hay un límite de caracteres y dinámicamente modifica el input
											var strTabName = this.getRowAttribute(id, "tabName");
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.maxLength > 0) {
												$(this.editor.obj).attr("maxlength", objField.maxLength);
											}
											break;
									}
									break;
								
								//if (this.getRowAttribute(id, "type") == GFieldTypes.combo) {
								case GFieldTypes.combo:
									switch (stage) {
										case 0:	//Before start
											//Al iniciar la edición, se debe modificar el contenido del combo a los valores válidos para esta Row, además se debe
											//asignar como valor de la row el Id en lugar del texto correspondiente para que la combo pueda seleccionar correctamente
											//su valor al abrir la lista
											var combo = this.getCombo(index);
											combo.clear();
											var strTabName = this.getRowAttribute(id, "tabName");
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.empty) {
												var idOption = 0;
												if (objField.emptyId !== undefined) {
													var idOption = objField.emptyId;
												}
												var strName = "(<?=translate("None")?>)";
												combo.put(idOption, strName);
											}
											
											var blnUseOrder = true;
											var cmbOptions = new Object();
											var cmbOptionsOrder = new Array();
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.getOptions) {
												var cmbOptions = objField.getOptions();
												var cmbOptionsOrder = objField._optionsOrder;
												var blnUseOrder = objField.useOrder;
											}
											
											if (objField && objField.parent) {
												var intParentValue = this.getRowAttribute(objField.parent, "value");
												var objParentField = getFieldDefinition(strTabName, objField.parent, blnForForm);
												if (objParentField && objParentField.options) {
													if (!$.isNumeric(intParentValue)) {
														if (objParentField.default !== undefined) {
															intParentValue = objParentField.default;
														}
													}
													var objParentOptions = objParentField.options[intParentValue];
													if (objParentOptions && objParentOptions.children) {
														cmbOptions = objParentOptions.children;
														//Si hay una configuración de orden, se utiliza para agregar los elementos en dicha secuencia, de lo contrario se usa el orden recibido
														var cmbOptionsOrder = objParentOptions.childrenOrder;
														if (cmbOptionsOrder) {
															blnUseOrder = true;
														}
														else {
															blnUseOrder = false;
															cmbOptionsOrder = cmbOptions;
														}
													}
												}
											}
											
											for (var intIdx in cmbOptionsOrder) {
												var idOption = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
												var strName = cmbOptions[idOption].name;
												if (strName === undefined) {
													strName = cmbOptions[idOption];
												}
												combo.put(idOption, strName);
											}
											
											var intValue = this.getRowAttribute(id, "value");
											if (!$.isNumeric(intValue)) {
												if (objField && objField.default !== undefined) {
													intValue = objField.default;
												}
											}
											
											if ($.isNumeric(intValue)) {
												//JAPR 2015-06-18: Validado que al forzar el llenado manual de los componentes, no se lance el evento de onCellChanged
												//agregando una bandera temporal al grid
												//En este caso se está llenando así debido a como funciona la combo, pero realmente no se está asignando un valor
												this.bitDisableEvents = true;
												this.cells(id, 1).setValue(intValue.toString());
												this.bitDisableEvents = false;
											}
											break;
										
										case 2:	//Editor closed
											//Al terminar la edición hay que actualizar el atributo con el nuevo valor seleccionado
											var intValue = this.cells(id, 1).getValue();
											if ($.isNumeric(intValue)) {
												//Si hubo un cambio, entonces esta combo habría regresado un Id, de lo contrario regresaría el texto de la celda del grid
												//así que se valida para que sólo los datos numéricos actualicen el valor real de la row (incluso un 0 es válido como ID,
												//así que no deberían agregarse posibles textos con descripción numérica para no causar problemas)
												this.setRowAttribute(id, "value", intValue);
												
												//Se deben aplicar las reglas de visibilidad de la opción de respuesta seleccionada, ocultando todos los campos/tabs que otras
												//opciones debían mostrar, y mostrando sólo los que está configurada para ver
												var strTabName = this.getRowAttribute(id, "tabName");
												var blnUseOrder = true;
												var cmbOptions = new Object();
												var cmbOptionsOrder = new Array();
												var objField = getFieldDefinition(strTabName, id, blnForForm);
												//var objField = getFieldDefinition(strTabName, id, blnForForm);
												//if (objField && objField.options && objField.options[intValue]) {
												if (objField) {
													if (objField.getOptions) {
														var cmbOptions = objField.getOptions();
														var cmbOptionsOrder = objField._optionsOrder;
														var blnUseOrder = objField.useOrder;
													}
													
													//Prepara una función para mandar el option y que se ejecute el proceso correspodiente de show/Hide
													var fnShowHideItems = function(oOption, bShow) {
														if (!oOption) {
															return;
														}
														
														if (oOption && oOption.visibilityRules) {
															var objVisibilityRules = oOption.visibilityRules;
															//Oculta las tabs indicadas
															if (objVisibilityRules.tabs && objVisibilityRules.tabs.length) {
																for (var intIndex in objVisibilityRules.tabs) {
																	var strTabId = objVisibilityRules.tabs[intIndex];
																	if (strTabId && objPropsTabs.tabs(strTabId)) {
																		try {
																			if (bShow) {
																				objPropsTabs.tabs(strTabId).show();
																			}
																			else {
																				objPropsTabs.tabs(strTabId).hide();
																			}
																		} catch(e) {};
																	}
																}
															}
															
															//Oculta los campos indicados
															if (objVisibilityRules.fields && objVisibilityRules.fields.length) {
																for (var intIndex in objVisibilityRules.fields) {
																	var objHideField = objVisibilityRules.fields[intIndex];
																	var strTabId = objHideField['tabId'];
																	var strFieldId = objHideField['fieldId'];
																	try {
																		var objTempGrid = undefined;
																		if (objPropsTabs.tabs(strTabId)) {
																			objTempGrid = objPropsTabs.tabs(strTabId).getAttachedObject();
																		}
																		
																		if (objTempGrid && strFieldId) {
																			objTempGrid.setRowHidden(strFieldId, ((bShow)?false:true));
																		}
																	} catch(e) {};
																}
															}
														}
													}
													
													//Primero oculta todos los elementos que otras opciones deben mostrar
													var objSelectedOption = undefined;
													for (var intOption in cmbOptions) {
														var objOption = undefined;
														if (intOption == intValue) {
															objSelectedOption = cmbOptions[intOption];
														}
														else {
															var objOption = cmbOptions[intOption];
														}
														
														if (objOption && objOption.visibilityRules && objOption.visibilityRules) {
															fnShowHideItems(objOption, false);
														}
													}
													
													//Al terminar muestra los elementos de la opción seleccionada
													if (objSelectedOption && objSelectedOption.visibilityRules) {
														fnShowHideItems(objSelectedOption, true);
													}
												}
											}
											break;
									}
									break;
							}
							
							//Return true permite la edición, false la cancela
							return true;
						}/*, {objGrid:objGrid})*/);
						break;
				}
				
				objGrid.init();
				
				//Dependiendo del tipo de grid, configura las rows
				switch (objTab.type) {
					case propTabType.collection:
						$(objGrid.hdr).find('td div').css('padding-left', '0px');
						
						//Se debe generar el Grid sólo como una colección de valores con un mínimo de propiedades id y Name, siendo el id el rowId y el Name la columna default
						//var objValues = new Object();
						//var objValuesOrder = new Array();
						//if (objTab.getValues) {
						if (objValues && objValuesOrder && objValuesOrder.length) {
							//var objValues = objTab.getValues();
							//var objValuesOrder = objTab._valuesOrder;
							var blnUseOrder = objTab.useOrder;
							for (var intIdx in objValuesOrder) {
								var intValueID = (blnUseOrder)?objValuesOrder[intIdx]:intIdx;
								var objValue = objValues[intValueID];
								if (objValue) {
									var rowValues = new Array();
									for (var iField in objTab.fields) {
										var aField = objTab.fields[iField];
										if (aField) {
											var varValue = aField.getValue(undefined, objTab.childType, intValueID);
											if (varValue === undefined) {
												varValue = '';
											}
											rowValues.push(varValue);
										}
									}
									
									objGrid.bitDisableEvents = true;
									objGrid.addRow(intValueID, rowValues);
									objGrid.setRowAttribute(intValueID, "tabName", objTab.id);
									//objGrid.setRowAttribute(intValueID, "fieldName", intValueID);
									//objGrid.setRowAttribute(intValueID, "options", this.options);
									//objGrid.setRowAttribute(intValueID, "type", this.type);
									objGrid.setRowAttribute(intValueID, "objectType", objTab.childType);
									//objGrid.setRowAttribute(intValueID, "value", this.getValue(true));
									//if (this.hidden) {
									//	objGrid.setRowHidden(intValueID, true);
									//}
									//objGrid.setCellExcellType(intValueID, 1, this.type);
									objGrid.bitDisableEvents = false;
								}
							}
							
							//Al final agrega la row para crear un nuevo elemento
							var strNewItemID = "new";
							var rowValues = new Array();
							rowValues.push("images/admin/new.gif");
							rowValues.push("(<?=translate("Add")?>)");
							rowValues.push("");
							objGrid.addRow(strNewItemID, rowValues);
							objGrid.setCellExcellType(strNewItemID, 0, GFieldTypes.image);
							objGrid.setCellExcellType(strNewItemID, 2, GFieldTypes.readOnly);
							objGrid.setRowTextStyle(strNewItemID, "cursor:pointer;");
							objGrid.cells(strNewItemID, 2).setValue("");
						}
						
						break;
					
					default:
						//Cada row representa una propiedad que se puede configurar de manera diferente según el tipo de campo
						//objGeneralGrid.setColSorting("int,str,str,int");
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							aField.draw(objGrid, iChildObjectType, iChildObjectID);
							//Asigna la visibilidad por default del campo
							/*if (objTab.defaultVisibility && !objTab.defaultVisibility[aField.name]) {
								objGrid.setRowHidden(aField.name, true);
							}*/
						}
						break;
				}
			}

			<?
			$pos = strpos($_SESSION["PAHTTP_REFERER"],"build_page.php");
			$pathImg = substr($_SESSION["PAHTTP_REFERER"],0,$pos);
			?>
			//Permite definir celdas para poder subir imágenes al servidor
			function eXcell_uploadImg(cell) { //the eXcell name is defined here
				if (cell) {                // the default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function(){}  //read-only cell doesn't have edit method
				this.isDisabled = function(){ return false; } // the cell is read-only, so it's always in the disabled state
				this.setValue = function(val) {
					var strRowId = this.cell.parentNode.idd;
					var strId = "cellUploadImg_"+strRowId+'_'+this.cell.cellIndex;
					if (val!="") {
						srcImg='<?=$pathImg?>'+val;
					}
					else {
						srcImg='<?=$pathImg?>images/user1.gif';
					}
					var intObjectType = this.grid.getRowAttribute(strRowId, "objectType");
					var intObjectID = this.grid.bitObjectID;
					var intChildObjectType = this.grid.bitChildObjectType;
					var intChildObjectID = this.grid.bitChildObjectID;
					var blnForForm = (intObjectType == AdminObjectType.otySurvey);
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var strColTabName = this.grid.bitCollectionTabName;
					var objField = getFieldDefinition(tabName, strRowId,blnForForm);
					var fieldHTML='<div id="divFile'+strId+'" style="border:none">'+
						'<form name="frmUploadFile'+strId+'" method="POST" action="uploadUserImage.php?objectType='+objField.parentType+'&strId='+strId+'&tabName='+tabName+'&strRowId='+strRowId+'&objectID='+intObjectID+'&childType='+intChildObjectType+'&childID='+intChildObjectID+'&collTabName='+strColTabName+'" enctype="multipart/form-data" style="display:inline" target="frameUploadFile">'+
						'<input type="file" id="file'+strId+'" name="file'+strId+'" value="" style="opacity:0;width:0px;height:0px;" onchange="document.getElementById(\'txtsubmit'+strId+'\').click();">'+
						'<button type="button" onclick="document.getElementById(\'file'+strId+'\').click();"><?=translate("Select file")?></button>'+
						'<input type="submit" id ="txtsubmit'+strId+'" name="txtsubmit'+strId+'" value="<?=translate("Upload file")?>"style="display: none; visibility: hidden;">'+'<img name="deleteImg" id="deleteImg" src="images/admin/delete.png" style="display:inline" onClick=\'removeImg("'+strId+'")\'>'+
						'<div>'+
						'<img name ="img'+strId+'" id="img'+strId+'" src="'+srcImg+'">'+
						'</div>'+
						'</form>'+
						'</div>'+
						'<form id="frmDeleteFile'+strId+'" name="frmDeleteFile'+strId+'" method="POST" action="uploadUserImage.php?action=delete&objectType='+objField.parentType+'&strId='+strId+'&tabName='+tabName+'&strRowId='+strRowId+'&objectID='+intObjectID+'&childType='+intChildObjectType+'&childID='+intChildObjectID+'&collTabName='+strColTabName+'" enctype="multipart/form-data" style="display:inline" target="frameUploadFile">'+
						'</form>'
					this.setCValue(fieldHTML);
					
					var objGrid = this.grid;
				}
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var objField = getFieldDefinition(tabName, strRowId);
					var strValue = "";
					if (objField)
					{
						strValue = objField.getValue();
					}
					return strValue;
				}
			}
			
			function removeImg(strId)
			{
				//Forma para borrar imagen
				document.forms['frmDeleteFile'+strId].submit();
			}
			eXcell_uploadImg.prototype = new eXcell;// nests all other methods from the base class
			//JAPR 2015-07-19: Agregados los parámetros iObjectID, iChildObjectType, iChildObjectID, sCollectionName para identificar el elemento hijo si se tratara de un subgrid,
			//ya que en las opciones de respuesta el tabName representa un detalle pero no directo de objPropsTabs, así que se debe navegar un nivel mas abajo para obtener la referencia
			//al grid correcto, además tabName representa a un tab oculto que sólo se usa como definición de tantos tabs detalle como rows tenga el sCollectionName, que es desde donde
			//se obtendrá la verdadera referencia al SubGrid donde se modificó la propiedad. Esto es así exclusivamente cuando hay subGrids de collecciones (hasta un nivel) y porque este
			//componente tiene que invocar a Javascript desde una respuesta del server, si hubiera podido invocarlo en el cliente directamente, se hubiera pasado la referencia al grid
			//en lugra de hacer todo esto
			//En otras propiedades que son directas de un grid que no es de colección, basta con enviar los parámetros hasta iObjectType y el grid recuperado será el adecuado
			//@APR 2017-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
			//Faltaba implementar el siguiente ajuste para reporte de errores
			//Corrección Ticket #QND4OY: Sólo se aceptan imagenes JPG, GIF y PNG			
			var errorMsg;
			errorMsg="";
			var blnError;
			blnError=false;
			
			function setUserImageForObject(strId, tabName, pathImg, strRowId, iObjectType, iObjectID)
			{
			   <?
				$pos = strpos($_SESSION["PAHTTP_REFERER"],"build_page.php");
				$pathImg = substr($_SESSION["PAHTTP_REFERER"],0,$pos);
			   ?>
			   
				//JAPR 2017-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
				//JAPR 2015-07-22: Validado el caso donde se reciba un mensaje de error
				if (blnError) {
					if(errorMsg!="")
					{
						alert(errorMsg);
						errorMsg="";
					}
					else
					{
						alert("<?=translate("There was an error uploading the image file, please contact Technical Support")?>: \n" + pathImg);
					}
					//OMMC 2015-11-23: Limpiar el valor del input file ya que prevenía que se subiera la misma imagen 2 veces de manera sucesiva.
					//O que si se borraba la imagen y se deseaba subir una con el mismo nombre, no se podía.
					if($('#file'+strId).val() != ""){
						$('#file'+strId).val("");
					}
					blnError=false;
					return;
				}
				//JAPR
				if(pathImg!="")
				{	//Ticket #IYR03T: Se agregó un parámetro extra de fecha para que no tome la imagen de caché
					//ya que si se subía una imagen con el mismo nombre de la anterior pero diferente contenido no la actualizaba
					$('img[name="img' + strId + '"]').attr('src', '<?=$pathImg?>'+pathImg+ "?dte=" + getFullDate(undefined, true));
					//OMMC 2015-11-23: Limpiar el valor del input file ya que prevenía que se subiera la misma imagen 2 veces de manera sucesiva.
					//O que si se borraba la imagen y se deseaba subir una con el mismo nombre, no se podía.
					if($('#file'+strId).val() != ""){
						$('#file'+strId).val("");
					}
				}
				else
				{
					$('img[name="img' + strId + '"]').attr('src', '<?=$pathImg?>'+'images/user1.gif');
				}
				propName=strRowId;
				if(iObjectType==AdminObjectType.otyUser)
				{
					var objGrid = objFormTabs.tabs(tabDetail).getAttachedObject().tabs(tabName).getAttachedObject();
				}
				else
				{
					var objGrid = objPropsTabs.tabs(tabName).getAttachedObject();
					//Si se recibieron los parámetros de sCollectionName y los demás, quiere decir que esta propiedad es de un objeto detalle dentro de una colección, así que el grid
					//a modificar es el de dicho detalle, porque tabName representa sólo un grid dummy para generar a los demás (se asumirá por ahora que el subgrid nace a partir de la
					//columna 0)
					try {
						var objTab = objPropsTabs.tabs(sCollectionName);
						if (objTab) {
							var objCollGrid = objTab.getAttachedObject();
							if (objCollGrid && iChildObjectType && iChildObjectID) {
								if (objCollGrid.cells(iChildObjectID, 0)) {
									var objSubGrid = objCollGrid.cells(iChildObjectID, 0).getSubGrid();
									if (objSubGrid) {
										var objGrid = objSubGrid;
									}
								}
							}
						}
					} catch (e) {}
				}
				
				var blnForForm = (iObjectType == AdminObjectType.otyUser);
				var objField = getFieldDefinition(tabName, propName,blnForForm);
				if (objField) {
					objField.setValue(null, null, pathImg);
					doUpdateProperty(objGrid, objField.parentType, objField.id, strRowId, 1, pathImg);
				}
			}

			/* Regresa la definición del campo a partir de la combinación tab/field. Se asume que siempre se hace referencia a los campos del tipo de objeto
			que se está desplegando en el momento en que se solicita, ya que esos son los que están almacenados en el array
			//JAPR 2015-06-25: Agregado el parámetro bForForm para indicar que se solicitan los campos directos del detalle de la forma, de lo contrario se asume que se trata del objeto
			que se está desplegando en la celda de propiedades
			*/
			function getFieldDefinition(sTabName, sFieldName, bForForm) {
				console.log('getFieldDefinition ' + sTabName + '.' + sFieldName);
				var objField = undefined;
				
				var objTmpTabsColl = objUserTabsColl;
				if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
					var objTab = objTmpTabsColl[sTabName];
					if (objTab && objTab.fields) {
						for (var intCont in objTab.fields) {
							var objFieldTmp = objTab.fields[intCont];
							if (objFieldTmp && objFieldTmp.name == sFieldName) {
								objField = objFieldTmp;
								break;
							}
						}
					}
				}
				
				return objField;
			}
			
			/* Idéntica a getFieldDefinition pero en lugar del nombre de campo el cual es útil en un grid de propiedades, se utiliza el índice del campo, el cual es útil en
			un grid de valores */
			function getFieldDefinitionByIndex(sTabName, iFieldIdx, bForForm) {
				console.log('getFieldDefinitionByIndex ' + sTabName + '.' + iFieldIdx);
				var objField = undefined;
				
				var objTmpTabsColl = objUserTabsColl;
				if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
					var objTab = objTmpTabsColl[sTabName];
					if (objTab && objTab.fields) {
						objField = objTab.fields[iFieldIdx];
					}
				}
				
				return objField;
			}

			/* Realiza el cambio de propiedades del objeto en el Previw según el elemento actualmente seleccionado
			*/
			function doUpdateProperty(oGrid, iObjectType, iObjectID, rId, cInd, nValue) {
				console.log('doUpdateProperty iObjectType == ' + iObjectType + ', iObjectID == ' + iObjectID + ', rId == ' + rId + ', cInd == ' + cInd + ', nValue == ' + nValue);
				
				if (!oGrid) {
					return;
				}
				
				if (iObjectType === undefined) {
					iObjectType = oGrid.bitObjectType;
				}
				if (iObjectID === undefined) {
					iObjectID = oGrid.bitObjectID;
				}
				if (!iObjectType || !iObjectID) {
					return;
				}
				//JAPR 2015-06-29: Agregado el subgrid de propiedades de los valores
				var intChildObjectType = oGrid.bitChildObjectType;
				var intChildObjectID = oGrid.bitChildObjectID;
				
				//Verifica que realmente hubiera un cambio en el valor de esta propiedad antes de realizar el proceso
				var varOldValue = oGrid.getRowAttribute(rId, "value");
				if (JSON.stringify(varOldValue) == JSON.stringify(nValue)) {
					return;
				}
				
				oGrid.setRowAttribute(rId, "value", nValue);
				var objParams = {};
				var objObject = getObject(iObjectType, iObjectID, intChildObjectType, intChildObjectID);
				if (objObject) {
					//AAL 19/07/2015 Agregado el parametro DissociateAll para saber cuando se desasocian todos los usuarios del grupo
					//Y no volverlos a insertar. Esto lo sabemos si el texto de la celda del Row es None o "".
					if(rId == "GroupsAssct" && (nValue == "(<?=translate("None")?>)" || nValue == "")){
						objObject[rId] = objObject.GroupsAssct.join();
						objObject["DissociateAll"] = true;
						objParams["DissociateAll"] = 1;
					}
					else{
						objObject[rId] = nValue;
					}
				}
				
				//Si está activado el grabado automático durante la edición, se invoca al mismo en este punto sólo con la propiedad modificada
				if (objObject && objObject.save) {
					objParams[rId] = 1;
					setTimeout(objObject.save(objParams), 100);
				}
			}

			/* Procesa la respuesta de un request de actualización de datos asíncrona y presenta el error en pantalla en caso de existir alguno
			Esta función está pensada como un callback de un request Ajax que regresará un contenido en JSon con infomación adicional de error
			*/
			function doSaveConfirmation(loader) {
				console.log('doSaveConfirmation');
				
				$('#divSaving').hide();
				
				if (!loader || !loader.xmlDoc) {
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					return;
				}
				
				//Si llega a este punto es que se recibió una respuesta correctamente
				var response = loader.xmlDoc.responseText;
				try {
					var objResponse = JSON.parse(response);
				} catch(e) {
					alert(e + "\r\n" + response.substr(0, 1000));
					return;
				}
				
				if (objResponse.error) {
					alert(objResponse.error.desc);
					return;
				}
				else {
					if (objResponse.warning) {
						console.log(objResponse.warning.desc);
					}
				}

				if (reloadOnLanguage)
				{
					window.parent.location.reload();
				}
				
				//La respuesta no tiene error, debió haber terminado de grabar correctamente
				//alert('Todo actualizado ok: ' + response);
			}
			
			/* Manda los datos al server para ser procesados
			El parámetro oSuccess permite especificar la función que se debe invocar al terminar el proceso de grabado en caso de éxito
			*/
			function doSendData(oSuccess) {
				console.log('doSendData');
				if (!objDialog) {
					return;
				}
				
				
				var objForm = objDialog.getAttachedObject();
				var objData = objForm.getFormData();
				objDialog.progressOn();
				objForm.lock();
				objForm.send("processRequest.php", "post", function(loader, response) {
					if (!loader || !response) {
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					if (loader.xmlDoc && loader.xmlDoc.status != 200) {
						alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					setTimeout(function () {
						try {
							var objResponse = JSON.parse(response);
						} catch(e) {
							alert(e + "\r\n" + response.substr(0, 1000));
							objDialog.progressOff();
							objForm.unlock();
							return;
						}
						
						if (objResponse.error) {
							alert(objResponse.error.desc);
							objDialog.progressOff();
							objForm.unlock();
							return;
						}
						else {
							if (objResponse.warning) {
								alert(objResponse.warning.desc);
							}
						}
						
						//Sólo si todo sale Ok cierra el diálogo y continua cargando el diseño de la forma recien creada
						doUnloadDialog();
						if (oSuccess) {
							try {
								//Se enviará la respuesta obtenida, ya que ella traería la nueva definición del objeto a utilizar
								oSuccess(objResponse);
							} catch(e) {
								console.log("Error executing doSendData callback: " + e);
							}
						}
					}, 100);
				});
			}

			/* Agrega la clase correspondiente a cada objeto descargado de la definición de la forma */
			function addClassPrototype() {
				if (!Users) {
					return;
				}
				$.extend(Users, new UserCls());
			}

			function fnSelectAllCListItems(oCList, bCheck) {
				if (!oCList) {
					return;
				}
				
				$(oCList).find(':checkbox').prop('checked', (bCheck?true:false));
			}

			//Descarga la instancia de la forma de la memoria
			function doUnloadDialog() {
				console.log('doUnloadDialog');
				if (!objWindows || !objDialog) {
					return;
				}
				
				var objForm = objDialog.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
				
				if (objWindows.unload) {
					objWindows.unload();
					objWindows = null;
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				objDialog = null;
			}
						
			/* Descarga las ventanas adicionales creadas así como cualquier otro componente */
			function doOnUnload() {
				console.log('doOnUnload');
				if (objWindows && objWindows.unload) {
					objWindows.unload();
				}
			}
		</script>
	</head>
	<style>
		.controls {
		  margin-top: 10px;
		  border: 1px solid transparent;
		  border-radius: 2px 0 0 2px;
		  box-sizing: border-box;
		  -moz-box-sizing: border-box;
		  height: 32px;
		  outline: none;
		  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		}
		#geoFenceSearch {
		  background-color: #fff;
		  font-family: Roboto;
		  font-size: 15px;
		  font-weight: 300;
		  margin-left: 12px;
		  padding: 0 11px 0 13px;
		  text-overflow: ellipsis;
		  width: 300px;
		}

		#geoFenceSearch:focus {
		  border-color: #4d90fe;
		}
	</style>
	<body onload="doOnLoad();" onunload="doOnUnload();">
		<input id="geoFenceSearch" class="controls" type="text" placeholder="Search Box" style="display:none">
		<iframe id="frameRequests" name="frameRequests" style="display:none;">
		</iframe>
		<form id="frmRequests" style="display:none;" method="post" target="frameRequests">
		</form>
		<!--<div id="objectId" style="width:100%;height:100%;background-color:blue;">abc</div>-->
		<div id="divUserFormPermissionsParent" style="display:none;height:100%;width:100%;overflow-y:auto;">
			<div id="divUserFormPermissions" style="height:100%;width:900px;overflow-y:auto;"></div>
		</div>
		<div id="divInviteGroupsParent" style="display:none;height:100%;width:100%;overflow-y:auto;">
			<div id="divInviteGroups" style="height:100%;width:900px;overflow-y:auto;"></div>
		</div>
		<div id="divDesign" style="display:none;height:100%;width:100%;">
			<div id="divDesignHeader" class="linkToolbar" style="height:30px;width:100%">
				<table style="/*width:100%;*/height:100%;">
					<tbody>
						<tr>
						<td class="linkTD" tabName="tabProfile" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Profile")?></td>
							<td class="linkTD" tabName="tabCatalog" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Catalogs")?></td>
							<!--<td class="linkTD" tabName="tabModel" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Models")?></td>-->
							<!--<td class="linkTD" tabName="tabNotification" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Notifications")?></td>-->
							<td class="linkTD" tabName="tabFormPermissions" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Forms")?></td>
							<td class="linkTD" tabName="save" style="text-decoration: none !important; cursor:auto;width:150px;">
								<div id="divSaving" class="dhx_cell_progress_img" style="display:none;position:relative !important;background-size:20px 20px;">&nbsp;</div>
								<div id="divSavingError" style="display:none;position:relative !important;background-size:20px 20px;cursor:pointer;" onclick="doShowErrors()">&nbsp;</div>
							</td>
						</tr>
				  </tbody>
				</table>
			</div>
			<div id="divDesignBody" class="hiddenTabs" style="height:95%;width:100%;overflow:hidden;">
			</div>
		</div>
		<iframe id="frameUploadFile" name="frameUploadFile" style="display:none; position:absolute; left:100, top:150">
		</iframe>
	</body>
</html>
<?
	}
}

class BITAMAppUserExtCollection extends BITAMAppUserCollection
{
	use BITAMCollectionExt;
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=AppUserExt";
	}
	
	function get_CustomButtons() {
		global $gbIsGeoControl;
	
		$arrButtons = array();
		//@JAPR 2015-10-06: Validado que en un servicio de GeoControl no se puedan crear usuarios ni editar sus datos de KPIOnline
		if (!$gbIsGeoControl) {
			$arrButtons[] = array("id" => "add", "url" => "", "onclick" => "addNewObject();", "isDefault" => 1);	//*"label" => translate("Add"), "image" => "add.png", */
		}
		//GCRUZ 2015-11-11. Agregar manejo de suscripción de usuarios
		if (!$gbIsGeoControl) {
		$arrButtons[] = array("id" => "suscriptions", 'url' => 'Suscriptions', "label" => translate('Manage Suscriptions'), 'image' => 'menu.png');	//*"label" => translate("Add"), "image" => "add.png", */
		}
		return $arrButtons;
	}

	//GCRUZ 2015-09-17. Agregar botones custom al objeto de comportamientos
	function get_CustomButtonsBehaviour(){
		foreach (@$this->get_CustomButtons() as $aCustomButton)
		{
			$strButtonID = $aCustomButton['id'];
?>
		objCustomBehaviour["<?=$strButtonID?>"] = {customized:true};
<?
		}
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		return parent::PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser);
	}
	
	/* Agrega el código para permitir la creación flotante de formas así como el copiado */
	function generateAfterFormCode($aUser) {
		//Cálculo de licencias disponibles (código tomado de manageSuscriptions.inc.php)
		require_once('eSurveyServiceMod.inc.php');
		$aBITAMConnection = connectToKPIRepository();
		$strRepository = $this->Repository->RepositoryName;
		// get the databaseid
		$aSQL = "SELECT DatabaseID FROM saas_databases WHERE Repository = ".$aBITAMConnection->ADOConnection->Quote($strRepository);
		$rsAdminData = $aBITAMConnection->ADOConnection->Execute($aSQL);
		if ($rsAdminData && $rsAdminData->_numOfRows != 0)
		{
			$intDatabaseID = $rsAdminData->fields['DatabaseID'];
		}
		else
			die('kpiWebService.Error: Failed to obtain databaseid Data!');
		// get the admin email and productid
		$aSQL = "SELECT A.ProductID, B.Email FROM saas_regs A, saas_users B WHERE A.databaseid = ".$intDatabaseID." AND A.hostid = 0 AND A.userid = B.userid";
		$rsAdminData = $aBITAMConnection->ADOConnection->Execute($aSQL);
		if ($rsAdminData && $rsAdminData->_numOfRows != 0)
		{
			$strAdminEmail = $rsAdminData->fields['Email'];
			$intProductID = 5;
		}
		else
			die('kpiWebService.Error: Failed to obtain host Data!');
		
		$kpiWebServiceClient = new getKpiWebServiceClient();
		$users = $kpiWebServiceClient->getUsers($strAdminEmail, $intProductID);
		//Obtener usuarios de SI_SV_USERS
		$aSQL = "SELECT Email, SV_Admin, SV_User FROM SI_SV_USERS ORDER BY Email";
		$rsUsers = $this->Repository->ADOConnection->Execute($aSQL);
		$arrEsurveyUsers = array();
		$arrEsurveyUsers['Admin'] = array();
		$arrEsurveyUsers['Guest'] = array();
		if ($rsUsers && $rsUsers->_numOfRows != 0)
		{
			while(!$rsUsers->EOF)
			{
				if (intval($rsUsers->fields['SV_Admin']))
					$arrEsurveyUsers['Admin'][] = strtolower($rsUsers->fields['Email']);
				if (intval($rsUsers->fields['SV_User']))
					$arrEsurveyUsers['Guest'][] = strtolower($rsUsers->fields['Email']);
				$rsUsers->MoveNext();
			}
		}
		$intExcessAdmins = 0;
		$intExcessGuests = 0;
		$intMaxAdmins = 0;
		$intMaxGuests = 0;
		if (!is_array($users))
		{
			$intAvailableAdmins = 0;
			$intAvailableGuests = 0;
			$arrAdminSuscritpions = array();
			$arrGuestSuscritpions = array();
		}
		else
		{
			if (intval($users[5]['maxAdmins']) == -1)
			{
				//GCRUZ 2016-07-05. Ilimitados
				$intAvailableAdmins = -1;
				$arrAdminSuscritpions = $arrEsurveyUsers['Admin'];
				$intExcessAdmins = 0;
				$intMaxAdmins = -1;
			}
			else
			{
//				$intAvailableAdmins = intval($users[5]['maxAdmins']) - count($users[5]['currentAdmins']);
				$intMaxAdmins = intval($users[5]['maxAdmins']);
				$intAvailableAdmins = intval($users[5]['maxAdmins']) - count($arrEsurveyUsers['Admin']);
				if ($intAvailableAdmins < 0)
				{
					$intExcessAdmins = $intAvailableAdmins * -1;
					$intAvailableAdmins = 0;
				}
//				$arrAdminSuscritpions = $users[5]['currentAdmins'];
				$arrAdminSuscritpions = $arrEsurveyUsers['Admin'];
			}
			if (intval($users[5]['maxGuests']) == -1)
			{
				//GCRUZ 2016-07-05. Ilimitados
				$intAvailableGuests = -1;
				$arrGuestSuscritpions = $arrEsurveyUsers['Guest'];
				$intExcessGuests = 0;
				$intMaxGuests = -1;
			}
			else
			{
//				$intAvailableGuests = intval($users[5]['maxGuests']) - count($users[5]['currentGuests']);;
				$intMaxGuests = intval($users[5]['maxGuests']);
				$intAvailableGuests = intval($users[5]['maxGuests']) - count($arrEsurveyUsers['Guest']);
				if ($intAvailableGuests < 0)
				{
					$intExcessGuests = $intAvailableGuests * -1;
					$intAvailableGuests = 0;
				}
//				$arrGuestSuscritpions = $users[5]['currentGuests'];
				$arrGuestSuscritpions = $arrEsurveyUsers['Guest'];
			}
		}
?>
	<script language="JavaScript">
		var objWindows;
		var objDialog;
		var intDialogWidth = 450;
		var intDialogHeight = 500;
		var reqAjax = 1;
		var Supervisor = "<?=$aUser->EMail?>";
		
		//Descarga la instancia de la forma de la memoria
		function doUnloadDialog() {
			if (objDialog) {
				var objForm = objDialog.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
			}
			
			if (objWindows) {
				if (objWindows.unload) {
					objWindows.unload();
					objWindows = null;
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
			}
			
			objDialog = null;
		}
		
		//Manda los datos al server para ser procesados
		function doSendData() {
			if (!objDialog) {
				return;
			}
			
			var objForm = objDialog.getAttachedObject();
			var objData = objForm.getFormData();
			//Parametros por default, concatenados con los capturados por el usuario
			var Role = objData.Role, TemplateID = -1, DefSurvey = 0, Sendpdfbyemail = 0, AltEmail = "", target = "parent";
			var strParams = objData.Email + "_©_" + objData.Password +  "_©_" +  objData.FirstName + "_©_" + objData.LastName + "_©_" + Role + "_©_" + Sendpdfbyemail + "_©_" + Supervisor + "_©_" + target + "_©_" + AltEmail + "_©_" + DefSurvey + "_©_" + TemplateID;
			objDialog.progressOn();
			objForm.lock();
			//@AAL En la parte de grabado de un Usuario se requiere de invocar primero el archivo uploadUsersFile, ya que guarda ciertos campos a la tabla de usuarios
//			window.dhx4.ajax.post("uploadUsersFile.php", "txtNewUserData=" + strParams, function(strParams){
			//GCRUZ 2015-11-04. Cambiar método de agregar usuarios, ahora usar el webservice de kpi
			window.dhx4.ajax.post("kpiWebServiceEsurveyClient.php", "txtNewUserData=" + strParams, function(strParams) {
				//{status: "ok", data: "value", data2: "value2"}

//				var Response = window.dhx4.s2j(strParams.xmlDoc.responseText); // convert response to json object
//			    if (Response && Response.UserID < 0 && Response.Status != 200) {
//			        alert(Response.Error);
//			        objDialog.progressOff();
//					objForm.unlock();
//					doUnloadDialog();
//			        return;
//			    }
//			    objForm.setItemValue("UserID", Response.UserID);
				if (strParams.xmlDoc.responseText.search('kpiWebService.Success') == -1)
				{
					alert(strParams.xmlDoc.responseText);
			        objDialog.progressOff();
					objForm.unlock();
					doUnloadDialog();
			        return;
			    }
				var newUserID = strParams.xmlDoc.responseText.replace('kpiWebService.Success:', '');
				objForm.setItemValue("UserID", newUserID);
				objForm.send("processRequest.php", "post", function(loader, response) {
					if (!loader || !response) {
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					if (loader.xmlDoc && loader.xmlDoc.status != 200) {
						alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					setTimeout(function () {
						try {
							var objResponse = JSON.parse(response);
						} catch(e) {
							alert(e + "\r\n" + response.substr(0, 1000));
							objDialog.progressOff();
							objForm.unlock();
							return;
						}
						
						if (objResponse.error) {
							alert(objResponse.error.desc);
							objDialog.progressOff();
							objForm.unlock();
							return;
						}
						else {
							if (objResponse.warning) {
								alert(objResponse.warning.desc);
							}
						}
						
						//Sólo si todo sale Ok cierra el diálogo y continua cargando el diseño de la forma recien creada
						doUnloadDialog();
						var strURL = objResponse.url;
						if (strURL && parent.doExecuteURL) {
							parent.doExecuteURL("", undefined, undefined, strURL);
						}
					}, 100);
				});    
			});	
		}
		
		//Muestra el diálogo para crear una nueva forma
		function addNewObject() {
			if (!objWindows) {
				//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
			}
			
			objDialog = objWindows.createWindow({
				id:"newObject",
				left:0,
				top:0,
				width:intDialogWidth,
				height:intDialogHeight,
				center:true,
				modal:true
			});
			
			objDialog.setText("<?=translate("New user")?>");
			objDialog.denyPark();
			objDialog.denyResize();
			objDialog.setIconCss("without_icon");
			$(".dhxwin_hdr").attr("style", "z-index: 0; background-color: wheat;");
			//Al cerrar realizará el cambio de sección de la pregunta
			objDialog.attachEvent("onClose", function() {
				return true;
			});
				optionRole = new Array();
				optionRole = [
				<?
				if($intAvailableGuests==-1 || $intAvailableGuests>0) {
					if($intAvailableAdmins==-1 || $intAvailableAdmins>0) {
				?>
						{text: "<?=translate("Administrator")?>", value: "1"},
						{text: "<?=translate("User")?>", value: "2", selected: true}
				<?
					}
					else {
				?>		
						{text: "<?=translate("User")?>", value: "2", selected: true}
				<?
					}
				}
				else {
					if($intAvailableAdmins==-1 || $intAvailableAdmins>0) {
				?>
					{text: "<?=translate("Administrator")?>", value: "1", selected: true}
				<?
					}
				}
				?>	
				];
			/* El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma */
			var objFormData = [
				{type:"settings"/*, offsetLeft:20*/},
				//GRCUZ 2015-08-28. Remover validación de Email. Issue:BIDIAW
				{type:"input", name:"Email", label:"<?=translate("Email")?>", labelAlign:"left", value:"", labelWidth:150, inputWidth:200, required:true, /*GRCUZ 2015-08-28. Código original: validate:"NotEmpty,ValidEmail"}*/validate:"NotEmpty"},
				{type:"input", name:"FirstName", label:"<?=translate("First name")?>", labelAlign:"left", value:"", labelWidth:150, inputWidth:200},
				{type:"input", name:"LastName", label:"<?=translate("Last name")?>", labelAlign:"left", value:"", labelWidth:150, inputWidth:200},
				{type:"password", name:"Password", label:"<?=translate("Password")?>", labelAlign:"left", value:"", labelWidth:150, inputWidth:200, required:true, validate:"NotEmpty"},
				<?
				if ($intAvailableGuests == 0 && $intAvailableAdmins == 0) {
				?>
					{type: "label", label: "<?=translate("No licenses available")?>", labelWidth:150},
					{type:"input", name:"Role", value:"0", hidden:true},
				<?
				}
				else {		
				?>
					{type:"combo", name:"Role", label:"<?=translate("Role")?>", labelAlign:"left", labelWidth:150, inputWidth:200, required:false,  options:optionRole},
				<?
				}
				?>	
				//El correo alternativo no debe mostrarse en la ventana principal
				//{type:"input", name:"AltEmail", label:"<?=translate("Alternative email")?>", labelAlign:"left", value:"", labelWidth:150, inputWidth:200},
				{type:"block", blockOffset:50, offsetLeft:100, list:[
					{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
					{type:"newcolumn"},
					{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
				]},
				{type:"input", name:"UserID", value:"", hidden:true},
				{type:"input", name:"RequestType", value:reqAjax, hidden:true},
				{type:"input", name:"Process", value:"Add", hidden:true},
				{type:"input", name:"ObjectType", value:<?=otyUser?>, hidden:true}
			];
			
			var objForm = objDialog.attachForm(objFormData);
			objForm.adjustParentSize();
			objForm.setItemFocus("UserName");
			objForm.attachEvent("onBeforeValidate", function (id) {
				console.log('Before validating the form: id == ' + id);
			});
			
			objForm.attachEvent("onAfterValidate", function (status) {
				console.log('After validating the form: status == ' + status);
			});
			
			objForm.attachEvent("onValidateSuccess", function (name, value, result) {
				console.log('Validating rule ok: name == ' + name + ', value == ' + value + ', result == ' + result);
			});
			
			objForm.attachEvent("onValidateError", function (name, value, result) {
				console.log('Error validating the form: name == ' + name + ', value == ' + value + ', result == ' + result);
			});
			
			objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
				if (!name || !inp || !ev || !ev.keyCode) {
					return;
				}
				
				switch (ev.keyCode) {
					case 13:
						//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
							if(objDialog.bitProcessing === undefined){
								objDialog.bitProcessing = true;
								setTimeout(function() {
									objForm.callEvent("onButtonClick", ["btnOk"]);
									objDialog.bitProcesing = undefined;
								}, 100);
							}
						break;
					case 27:
						setTimeout(function() {
							objForm.callEvent("onButtonClick", ["btnCancel"])
						}, 100);
						break;
				}
			});
			
			objForm.attachEvent("onButtonClick", function(name) {
				switch (name) {
					case "btnCancel":
						setTimeout(function () {
							doUnloadDialog();
						}, 100);
						break;
						
					case "btnOk":
						setTimeout(function() {
							if (objForm.validate()) {
								doSendData();
							}
						}, 100);
						break;
				}
			});
		}
	</script>
<?
	}
}
?>