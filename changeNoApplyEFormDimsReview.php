<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("question.inc.php");

global $gblEFormsNA;

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

//Obtenemos los datos del cubo global de encuestas a partir de la primera encuesta de la coleccion
//Para obtener las dimensiones globales de cubo Surveys y modificar valor de No Aplica
if(isset($surveyCollection->Collection[0]))
{
	$surveyInstance = $surveyCollection->Collection[0];
	$strOriginalWD = getcwd();
	
	require_once("cubeClasses.php");
	require_once("dimension.inc.php");
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");

	$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->GblSurveyModelID);
	chdir($strOriginalWD);

	$descriptKeysCollection = BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository, $anInstanceModel->ModelID);
	
	$aSurveyInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyDimID);
	chdir($strOriginalWD);
	$aStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStartDateDimID);
	chdir($strOriginalWD);
	$anEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblEndDateDimID);
	chdir($strOriginalWD);
	$aStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStartTimeDimID);
	chdir($strOriginalWD);
	$anEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblEndTimeDimID);
	chdir($strOriginalWD);
	
	//Obtenemos la instancia de la dimension User
	$userDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblUserDimID);
	chdir($strOriginalWD);
	
	//Obtenemos los campos del INSERT a la tabla de hechos de encuestas globales
	$factTable = $anInstanceModel->nom_tabla;

	$surveyDimField = $aSurveyInstanceDim->Dimension->TableName."KEY";
	$userDimField = $userDimension->Dimension->TableName."KEY";
	$startDateDimField = $aStartDateInstanceDim->Dimension->TableName."KEY";
	$endDateDimField = $anEndDateInstanceDim->Dimension->TableName."KEY";
	$startTimeDimField = $aStartTimeInstanceDim->Dimension->TableName."KEY";
	$endTimeDimField = $anEndTimeInstanceDim->Dimension->TableName."KEY";
	
	//Modificamos valor de No Aplica de Dimension User
	$tableName = $userDimension->Dimension->TableName;
	$fieldDesc = "DSC_".$surveyInstance->GblUserDimID;
	
	$sql = "SELECT ".$userDimField." FROM ".$tableName." 
			WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$userDimField." <> -1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
	}
	
	if(!$aRS->EOF)
	{
		$clave = (int)$aRS->fields[strtolower($userDimField)];
		echo('Dimension User('.$surveyInstance->GblUserDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
	}
	else 
	{
		$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$userDimField." = -1";
		echo($sql.'<br>');
	}
	
	//Modificamos valor de No Aplica de Dimension Surveys
	$tableName = $aSurveyInstanceDim->Dimension->TableName;
	$fieldDesc = "DSC_".$surveyInstance->GblSurveyDimID;
	
	$sql = "SELECT ".$surveyDimField." FROM ".$tableName." 
			WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$surveyDimField." <> 1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
	}
	
	if(!$aRS->EOF)
	{
		$clave = (int)$aRS->fields[strtolower($surveyDimField)];
		echo('Dimension Surveys('.$surveyInstance->GblSurveyDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
	}
	else 
	{
		$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$surveyDimField." = 1";
		echo($sql.'<br>');
	}

	//Modificamos valor de No Aplica de Dimension StartDate
	$tableName = $aStartDateInstanceDim->Dimension->TableName;
	$fieldDesc = "DSC_".$surveyInstance->GblStartDateDimID;
	
	$sql = "SELECT ".$startDateDimField." FROM ".$tableName." 
			WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$startDateDimField." <> 1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
	}
	
	if(!$aRS->EOF)
	{
		$clave = (int)$aRS->fields[strtolower($startDateDimField)];
		echo('Dimension StartDate('.$surveyInstance->GblStartDateDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
	}
	else 
	{
		$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$startDateDimField." = 1";
		echo($sql.'<br>');
	}
	
	//Modificamos valor de No Aplica de Dimension EndDate
	$tableName = $anEndDateInstanceDim->Dimension->TableName;
	$fieldDesc = "DSC_".$surveyInstance->GblEndDateDimID;
	
	$sql = "SELECT ".$endDateDimField." FROM ".$tableName." 
			WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$endDateDimField." <> 1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
	}
	
	if(!$aRS->EOF)
	{
		$clave = (int)$aRS->fields[strtolower($endDateDimField)];
		echo('Dimension EndDate('.$surveyInstance->GblEndDateDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
	}
	else 
	{
		$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$endDateDimField." = 1";
		echo($sql.'<br>');
	}
	
	//Modificamos valor de No Aplica de Dimension StartTime
	$tableName = $aStartTimeInstanceDim->Dimension->TableName;
	$fieldDesc = "DSC_".$surveyInstance->GblStartTimeDimID;
	
	$sql = "SELECT ".$startTimeDimField." FROM ".$tableName." 
			WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$startTimeDimField." <> 1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
	}
	
	if(!$aRS->EOF)
	{
		$clave = (int)$aRS->fields[strtolower($startTimeDimField)];
		echo('Dimension StartTime('.$surveyInstance->GblStartTimeDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
	}
	else 
	{
		$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$startTimeDimField." = 1";
		echo($sql.'<br>');
	}
	
	//Modificamos valor de No Aplica de Dimension EndTime
	$tableName = $anEndTimeInstanceDim->Dimension->TableName;
	$fieldDesc = "DSC_".$surveyInstance->GblEndTimeDimID;
	
	$sql = "SELECT ".$endTimeDimField." FROM ".$tableName." 
			WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$endTimeDimField." <> 1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
	}
	
	if(!$aRS->EOF)
	{
		$clave = (int)$aRS->fields[strtolower($endTimeDimField)];
		echo('Dimension EndTime('.$surveyInstance->GblEndTimeDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
	}
	else 
	{
		$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$endTimeDimField." = 1";
		echo($sql.'<br>');
	}
	
	//Modificamos valores de No Aplica en las dimensiones comunes a todas las encuestas creadas
	
	//Modificamos valor de No Aplica de Dimension Email
	$tableName = "RIDIM_".$surveyInstance->EmailDimID;
	$fieldDesc = "DSC_".$surveyInstance->EmailDimID;
	$fieldKey = "RIDIM_".$surveyInstance->EmailDimID."KEY";
	
	$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
			WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
	}
	
	if(!$aRS->EOF)
	{
		$clave = (int)$aRS->fields[strtolower($fieldKey)];
		echo('Dimension Email('.$surveyInstance->EmailDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
	}
	else 
	{
		$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
		echo($sql.'<br>');
	}

	//Modificamos valor de No Aplica de Dimension Status
	$tableName = "RIDIM_".$surveyInstance->StatusDimID;
	$fieldDesc = "DSC_".$surveyInstance->StatusDimID;
	$fieldKey = "RIDIM_".$surveyInstance->StatusDimID."KEY";
	
	$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
			WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
	}
	
	if(!$aRS->EOF)
	{
		$clave = (int)$aRS->fields[strtolower($fieldKey)];
		echo('Dimension Status('.$surveyInstance->StatusDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
	}
	else 
	{
		$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
		echo($sql.'<br>');
	}
	
	//Modificamos valor de No Aplica de Dimension Scheduler
	$tableName = "RIDIM_".$surveyInstance->SchedulerDimID;
	$fieldDesc = "DSC_".$surveyInstance->SchedulerDimID;
	$fieldKey = "RIDIM_".$surveyInstance->SchedulerDimID."KEY";
	
	$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
			WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
	}
	
	if(!$aRS->EOF)
	{
		$clave = (int)$aRS->fields[strtolower($fieldKey)];
		echo('Dimension Scheduler('.$surveyInstance->SchedulerDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
	}
	else 
	{
		$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
		echo($sql.'<br>');
	}
}

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	//Modificamos valor de No Aplica de Dimension EncuestaID
	$tableName = "RIDIM_".$surveyInstance->FactKeyDimID;
	$fieldDesc = "DSC_".$surveyInstance->FactKeyDimID;
	$fieldKey = "RIDIM_".$surveyInstance->FactKeyDimID."KEY";
	
	$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
			WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> -1";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
	}
	
	if(!$aRS->EOF)
	{
		$clave = (int)$aRS->fields[strtolower($fieldKey)];
		echo('Dimension EncuestaID('.$surveyInstance->FactKeyDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
	}
	else 
	{
		$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = -1";
		echo($sql.'<br>');
	}

	//Se genera instancia de coleccion de preguntas dado un SurveyID pero se ignoraran los datos de tipo 3 Seleccion Multiple
	$anArrayTypes = array();
	$anArrayTypes[0] = 3;
	$questionCollection = BITAMQuestionCollection::NewInstanceWithExceptType($aRepository, $surveyInstance->SurveyID, $anArrayTypes);

	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		switch ($aQuestion->QTypeID) 
		{
			case 1:
			case 4:
			case 5:
			case 6:
				//Modificamos valor de No Aplica de la Pregunta
				$tableName = "RIDIM_".$aQuestion->IndDimID;
				$fieldDesc = "DSC_".$aQuestion->IndDimID;
				$fieldKey = "RIDIM_".$aQuestion->IndDimID."KEY";
				
				$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
						WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
				
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				
				if(!$aRS)
				{
					echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
				}
				
				if(!$aRS->EOF)
				{
					$clave = (int)$aRS->fields[strtolower($fieldKey)];
					echo('Dimension Question('.$aQuestion->QuestionID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
				}
				else 
				{
					$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
					echo($sql.'<br>');
				}
				break;
			case 2:
				if($aQuestion->UseCatalog==0 && $aQuestion->QDisplayMode!=dspMatrix)
				{
					//Modificamos valor de No Aplica de la Pregunta
					$tableName = "RIDIM_".$aQuestion->IndDimID;
					$fieldDesc = "DSC_".$aQuestion->IndDimID;
					$fieldKey = "RIDIM_".$aQuestion->IndDimID."KEY";
					
					$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
							WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
					
					$aRS = $aRepository->DataADOConnection->Execute($sql);
					
					if(!$aRS)
					{
						echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
					}
					
					if(!$aRS->EOF)
					{
						$clave = (int)$aRS->fields[strtolower($fieldKey)];
						echo('Dimension Question('.$aQuestion->QuestionID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
					}
					else 
					{
						$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
						echo($sql.'<br>');
					}
				}
				else if($aQuestion->UseCatalog==0 && $aQuestion->QDisplayMode==dspMatrix) 
				{
					if($aQuestion->OneChoicePer==0)
					{
						$arrayOptions = $aQuestion->SelectOptions;
						$strQIndDimIds = "QIndDimIds";
					}
					else 
					{
						$arrayOptions = $aQuestion->SelectOptionsCol;
						$strQIndDimIds = "QIndDimIdsCol";
					}
					
					//Recorremos las dimensiones
					foreach ($aQuestion->$strQIndDimIds as $optionKey=>$dimensionid)
					{
						if(!is_null($dimensionid) && $dimensionid>0)
						{
							//Modificamos valor de No Aplica de la Pregunta
							$tableName = "RIDIM_".$dimensionid;
							$fieldDesc = "DSC_".$dimensionid;
							$fieldKey = "RIDIM_".$dimensionid."KEY";
							
							$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
									WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
							
							$aRS = $aRepository->DataADOConnection->Execute($sql);
							
							if(!$aRS)
							{
								echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
							}
							
							if(!$aRS->EOF)
							{
								$clave = (int)$aRS->fields[strtolower($fieldKey)];
								echo('Dimension Matrix Question-OptionDim('.$aQuestion->QuestionID.'-'.$dimensionid.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
							}
							else 
							{
								$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
								echo($sql.'<br>');
							}
						}
					}
				}
				break;		
			default:
				//Pareciera que no entraria a este caso
				break;
		}
		
		//Preguntamos si la pregunta tiene foto opcional o requerida y que tenga 
		//valor de dimension valido
		if($aQuestion->HasReqPhoto>0 && !is_null($aQuestion->ImgDimID) && $aQuestion->ImgDimID>0)
		{
			$tableName = "RIDIM_".$aQuestion->ImgDimID;
			$fieldDesc = "DSC_".$aQuestion->ImgDimID;
			$fieldKey = "RIDIM_".$aQuestion->ImgDimID."KEY";
			
			$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
					WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
			
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			
			if(!$aRS)
			{
				echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
			}
			
			if(!$aRS->EOF)
			{
				$clave = (int)$aRS->fields[strtolower($fieldKey)];
				echo('Dimension ImagenQuestion('.$aQuestion->QuestionID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
			}
			else 
			{
				$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
				echo($sql.'<br>');
			}
		}
	}
	
	//Generamos una coleccion de solo las preguntas que sean de selección múltiple 
	//y q pertenezcan a secciones dinamicas y esten dentro de ellas
	$uniqueMultiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceUniqueDim($aRepository, $surveyInstance->SurveyID);
	
	foreach ($uniqueMultiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
	{
		//Modificamos valor de No Aplica de la Pregunta
		$tableName = "RIDIM_".$aQuestion->IndDimID;
		$fieldDesc = "DSC_".$aQuestion->IndDimID;
		$fieldKey = "RIDIM_".$aQuestion->IndDimID."KEY";
		
		$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
				WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if(!$aRS)
		{
			echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
		}
		
		if(!$aRS->EOF)
		{
			$clave = (int)$aRS->fields[strtolower($fieldKey)];
			echo('Dimension Question('.$aQuestion->QuestionID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
		}
		else 
		{
			$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
			echo($sql.'<br>');
		}
		
		if($aQuestion->HasReqPhoto>0 && !is_null($aQuestion->ImgDimID) && $aQuestion->ImgDimID>0)
		{
			$tableName = "RIDIM_".$aQuestion->ImgDimID;
			$fieldDesc = "DSC_".$aQuestion->ImgDimID;
			$fieldKey = "RIDIM_".$aQuestion->ImgDimID."KEY";
			
			$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
					WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
			
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			
			if(!$aRS)
			{
				echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
			}
			
			if(!$aRS->EOF)
			{
				$clave = (int)$aRS->fields[strtolower($fieldKey)];
				echo('Dimension ImagenQuestion('.$aQuestion->QuestionID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
			}
			else 
			{
				$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
				echo($sql.'<br>');
			}
		}
	}

	//Generamos una coleccion de solo las preguntas que sean de selección múltiple 
	//y q no pertenezcan a dimensiones dinamicas o bien esten fuera de ellas
	$multiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceDims($aRepository, $surveyInstance->SurveyID);
	
	//Recorremos las preguntas de seleccion multiple para obtener sus valores multiples
	foreach ($multiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
	{
		$QIndDimIDs = $aQuestion->QIndDimIds;
	
		//Obtenemos las instancias de las dimensiones de las opciones
		foreach ($QIndDimIDs as $qKey=>$qDimID) 
		{
			//Modificamos valor de No Aplica de la Pregunta
			$tableName = "RIDIM_".$qDimID;
			$fieldDesc = "DSC_".$qDimID;
			$fieldKey = "RIDIM_".$qDimID."KEY";
			
			$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
					WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
			
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			
			if(!$aRS)
			{
				echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
			}
			
			if(!$aRS->EOF)
			{
				$clave = (int)$aRS->fields[strtolower($fieldKey)];
				echo('Dimension Question-OptionDim('.$aQuestion->QuestionID.'-'.$qDimID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
			}
			else 
			{
				$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
				echo($sql.'<br>');
			}
		}
		
		if($aQuestion->HasReqPhoto>0 && !is_null($aQuestion->ImgDimID) && $aQuestion->ImgDimID>0)
		{
			$tableName = "RIDIM_".$aQuestion->ImgDimID;
			$fieldDesc = "DSC_".$aQuestion->ImgDimID;
			$fieldKey = "RIDIM_".$aQuestion->ImgDimID."KEY";
			
			$sql = "SELECT ".$fieldKey." FROM ".$tableName." 
					WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." AND ".$fieldKey." <> 1";
			
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			
			if(!$aRS)
			{
				echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
			}
			
			if(!$aRS->EOF)
			{
				$clave = (int)$aRS->fields[strtolower($fieldKey)];
				echo('Dimension ImagenQuestion('.$aQuestion->QuestionID.') no puede ser modificado Valor de No Aplica porque ya existe dicha descripcion en la clave '.$clave.'<br>');
			}
			else 
			{
				$sql = "UPDATE ".$tableName." SET ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA)." WHERE ".$fieldKey." = 1";
				echo($sql.'<br>');
			}
		}
	}
}
?>