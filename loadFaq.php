<?php
//@JAPR 2009-02-11: Agregado parche para que IExplorer permita descargar archivos de Office cuando se trabaja utilizando SSL
session_cache_limiter("public");
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//$strdoc = "\\\\maquina\ArchivosCompartidos\Junio.pdf";
//$strdoc = "\\\\maquina\ArchivosCompartidos\LluviaIdeas.doc";
$sqlFaq = "SELECT DocumentName FROM SI_EKT_GlobalDocuments WHERE GlobalDocumentID = 1";
$aRSFaq = $theRepository->ADOConnection->Execute($sqlFaq);
if(!$aRSFaq)
{
	$strdoc = "about.html";
}
else 
{
	if($aRSFaq->EOF)
	{
		$strdoc = "about.html";
	}
	else 
	{
		$tempdoc = $aRSFaq->fields["documentname"];
		
		if(trim($tempdoc)=="")
		{
			$strdoc = "about.html";
		}
		else 
		{
			$strdoc =  "./documents/".$tempdoc;
		}
	}
}
if (strlen($strdoc) == 0)
{
	exit;
}
$strBaseName = basename($strdoc);
$arrBaseName = explode(".", $strBaseName);
$strFileExt = trim($arrBaseName[count($arrBaseName) - 1]);
$strMIMEType = GetMIMEType($strFileExt);
header('Content-Description: File Transfer'); 
header('Content-Type: '.$strMIMEType); 
header('Content-Length: '.filesize($strdoc)); 
header('Content-Disposition: inline; filename='.$strBaseName); 
readfile($strdoc);
function GetMIMEType($sExt)
{
	$strMIMEType = "application/octet-stream";
	switch(strtolower($sExt))
	{
		case "": 
			$strMIMEType = "application/octet-stream";
			break;
		case "323": 
			$strMIMEType = "text/h323";
			break;
		case "acx": 
			$strMIMEType = "application/internet-property-stream";
			break;
		case "ai": 
			$strMIMEType = "application/postscript";
			break;
		case "aif": 
			$strMIMEType = "audio/x-aiff";
			break;
		case "aifc": 
			$strMIMEType = "audio/x-aiff";
			break;
		case "aiff": 
			$strMIMEType = "audio/x-aiff";
			break;
		case "asf": 
			$strMIMEType = "video/x-ms-asf";
			break;
		case "asr": 
			$strMIMEType = "video/x-ms-asf";
			break;
		case "asx": 
			$strMIMEType = "video/x-ms-asf";
			break;
		case "au": 
			$strMIMEType = "audio/basic";
			break;
		case "avi": 
			$strMIMEType = "video/x-msvideo";
			break;
		case "axs": 
			$strMIMEType = "application/olescript";
			break;
		case "bas": 
			$strMIMEType = "text/plain";
			break;
		case "bcpio": 
			$strMIMEType = "application/x-bcpio";
			break;
		case "bin": 
			$strMIMEType = "application/octet-stream";
			break;
		case "bmp": 
			$strMIMEType = "image/bmp";
			break;
		case "c": 
			$strMIMEType = "text/plain";
			break;
		case "cat": 
			$strMIMEType = "application/vnd.ms-pkiseccat";
			break;
		case "cdf": 
			$strMIMEType = "application/x-cdf";
			break;
		case "cer": 
			$strMIMEType = "application/x-x509-ca-cert";
			break;
		case "class": 
			$strMIMEType = "application/octet-stream";
			break;
		case "clp": 
			$strMIMEType = "application/x-msclip";
			break;
		case "cmx": 
			$strMIMEType = "image/x-cmx";
			break;
		case "cod": 
			$strMIMEType = "image/cis-cod";
			break;
		case "cpio": 
			$strMIMEType = "application/x-cpio";
			break;
		case "crd": 
			$strMIMEType = "application/x-mscardfile";
			break;
		case "crl": 
			$strMIMEType = "application/pkix-crl";
			break;
		case "crt": 
			$strMIMEType = "application/x-x509-ca-cert";
			break;
		case "csh": 
			$strMIMEType = "application/x-csh";
			break;
		case "css": 
			$strMIMEType = "text/css";
			break;
		case "dcr": 
			$strMIMEType = "application/x-director";
			break;
		case "der": 
			$strMIMEType = "application/x-x509-ca-cert";
			break;
		case "dir": 
			$strMIMEType = "application/x-director";
			break;
		case "dll": 
			$strMIMEType = "application/x-msdownload";
			break;
		case "dms": 
			$strMIMEType = "application/octet-stream";
			break;
		case "doc": 
			$strMIMEType = "application/msword";
			break;
		case "dot": 
			$strMIMEType = "application/msword";
			break;
		case "dvi": 
			$strMIMEType = "application/x-dvi";
			break;
		case "dxr": 
			$strMIMEType = "application/x-director";
			break;
		case "eps": 
			$strMIMEType = "application/postscript";
			break;
		case "etx": 
			$strMIMEType = "text/x-setext";
			break;
		case "evy": 
			$strMIMEType = "application/envoy";
			break;
		case "exe": 
			$strMIMEType = "application/octet-stream";
			break;
		case "fif": 
			$strMIMEType = "application/fractals";
			break;
		case "flr": 
			$strMIMEType = "x-world/x-vrml";
			break;
		case "gif": 
			$strMIMEType = "image/gif";
			break;
		case "gtar": 
			$strMIMEType = "application/x-gtar";
			break;
		case "gz": 
			$strMIMEType = "application/x-gzip";
			break;
		case "h": 
			$strMIMEType = "text/plain";
			break;
		case "hdf": 
			$strMIMEType = "application/x-hdf";
			break;
		case "hlp": 
			$strMIMEType = "application/winhlp";
			break;
		case "hqx": 
			$strMIMEType = "application/mac-binhex40";
			break;
		case "hta": 
			$strMIMEType = "application/hta";
			break;
		case "htc": 
			$strMIMEType = "text/x-component";
			break;
		case "htm": 
			$strMIMEType = "text/html";
			break;
		case "html": 
			$strMIMEType = "text/html";
			break;
		case "htt": 
			$strMIMEType = "text/webviewhtml";
			break;
		case "ico": 
			$strMIMEType = "image/x-icon";
			break;
		case "ief": 
			$strMIMEType = "image/ief";
			break;
		case "iii": 
			$strMIMEType = "application/x-iphone";
			break;
		case "ins": 
			$strMIMEType = "application/x-internet-signup";
			break;
		case "isp": 
			$strMIMEType = "application/x-internet-signup";
			break;
		case "jfif": 
			$strMIMEType = "image/pipeg";
			break;
		case "jpe": 
			$strMIMEType = "image/jpeg";
			break;
		case "jpeg": 
			$strMIMEType = "image/jpeg";
			break;
		case "jpg": 
			$strMIMEType = "image/jpeg";
			break;
		case "js": 
			$strMIMEType = "application/x-javascript";
			break;
		case "latex": 
			$strMIMEType = "application/x-latex";
			break;
		case "lha": 
			$strMIMEType = "application/octet-stream";
			break;
		case "lsf": 
			$strMIMEType = "video/x-la-asf";
			break;
		case "lsx": 
			$strMIMEType = "video/x-la-asf";
			break;
		case "lzh": 
			$strMIMEType = "application/octet-stream";
			break;
		case "m13": 
			$strMIMEType = "application/x-msmediaview";
			break;
		case "m14": 
			$strMIMEType = "application/x-msmediaview";
			break;
		case "m3u": 
			$strMIMEType = "audio/x-mpegurl";
			break;
		case "man": 
			$strMIMEType = "application/x-troff-man";
			break;
		case "mdb": 
			$strMIMEType = "application/x-msaccess";
			break;
		case "me": 
			$strMIMEType = "application/x-troff-me";
			break;
		case "mht": 
			$strMIMEType = "message/rfc822";
			break;
		case "mhtml": 
			$strMIMEType = "message/rfc822";
			break;
		case "mid": 
			$strMIMEType = "audio/mid";
			break;
		case "mny": 
			$strMIMEType = "application/x-msmoney";
			break;
		case "mov": 
			$strMIMEType = "video/quicktime";
			break;
		case "movie": 
			$strMIMEType = "video/x-sgi-movie";
			break;
		case "mp2": 
			$strMIMEType = "video/mpeg";
			break;
		case "mp3": 
			$strMIMEType = "audio/mpeg";
			break;
		case "mpa": 
			$strMIMEType = "video/mpeg";
			break;
		case "mpe": 
			$strMIMEType = "video/mpeg";
			break;
		case "mpeg": 
			$strMIMEType = "video/mpeg";
			break;
		case "mpg": 
			$strMIMEType = "video/mpeg";
			break;
		case "mpp": 
			$strMIMEType = "application/vnd.ms-project";
			break;
		case "mpv2": 
			$strMIMEType = "video/mpeg";
			break;
		case "ms": 
			$strMIMEType = "application/x-troff-ms";
			break;
		case "mvb": 
			$strMIMEType = "application/x-msmediaview";
			break;
		case "nws": 
			$strMIMEType = "message/rfc822";
			break;
		case "oda": 
			$strMIMEType = "application/oda";
			break;
		case "p10": 
			$strMIMEType = "application/pkcs10";
			break;
		case "p12": 
			$strMIMEType = "application/x-pkcs12";
			break;
		case "p7b": 
			$strMIMEType = "application/x-pkcs7-certificates";
			break;
		case "p7c": 
			$strMIMEType = "application/x-pkcs7-mime";
			break;
		case "p7m": 
			$strMIMEType = "application/x-pkcs7-mime";
			break;
		case "p7r": 
			$strMIMEType = "application/x-pkcs7-certreqresp";
			break;
		case "p7s": 
			$strMIMEType = "application/x-pkcs7-signature";
			break;
		case "pbm": 
			$strMIMEType = "image/x-portable-bitmap";
			break;
		case "pdf": 
			$strMIMEType = "application/pdf";
			break;
		case "pfx": 
			$strMIMEType = "application/x-pkcs12";
			break;
		case "pgm": 
			$strMIMEType = "image/x-portable-graymap";
			break;
		case "pko": 
			$strMIMEType = "application/ynd.ms-pkipko";
			break;
		case "pma": 
			$strMIMEType = "application/x-perfmon";
			break;
		case "pmc": 
			$strMIMEType = "application/x-perfmon";
			break;
		case "pml": 
			$strMIMEType = "application/x-perfmon";
			break;
		case "pmr": 
			$strMIMEType = "application/x-perfmon";
			break;
		case "pmw": 
			$strMIMEType = "application/x-perfmon";
			break;
		case "pnm": 
			$strMIMEType = "image/x-portable-anymap";
			break;
		case "pot": 
			$strMIMEType = "application/vnd.ms-powerpoint";
			break;
		case "ppm": 
			$strMIMEType = "image/x-portable-pixmap";
			break;
		case "pps": 
			$strMIMEType = "application/vnd.ms-powerpoint";
			break;
		case "ppt": 
			$strMIMEType = "application/vnd.ms-powerpoint";
			break;
		case "prf": 
			$strMIMEType = "application/pics-rules";
			break;
		case "ps": 
			$strMIMEType = "application/postscript";
			break;
		case "pub": 
			$strMIMEType = "application/x-mspublisher";
			break;
		case "qt": 
			$strMIMEType = "video/quicktime";
			break;
		case "ra": 
			$strMIMEType = "audio/x-pn-realaudio";
			break;
		case "ram": 
			$strMIMEType = "audio/x-pn-realaudio";
			break;
		case "ras": 
			$strMIMEType = "image/x-cmu-raster";
			break;
		case "rgb": 
			$strMIMEType = "image/x-rgb";
			break;
		case "rmi": 
			$strMIMEType = "audio/mid";
			break;
		case "roff ": 
			$strMIMEType = "application/x-troff";
			break;
		case "rtf": 
			$strMIMEType = "application/rtf";
			break;
		case "rtx": 
			$strMIMEType = "text/richtext";
			break;
		case "scd": 
			$strMIMEType = "application/x-msschedule";
			break;
		case "sct": 
			$strMIMEType = "text/scriptlet";
			break;
		case "setpay": 
			$strMIMEType = "application/set-payment-initiation";
			break;
		case "setreg": 
			$strMIMEType = "application/set-registration-initiation";
			break;
		case "sh": 
			$strMIMEType = "application/x-sh";
			break;
		case "shar ": 
			$strMIMEType = "application/x-shar";
			break;
		case "sit": 
			$strMIMEType = "application/x-stuffit";
			break;
		case "snd": 
			$strMIMEType = "audio/basic";
			break;
		case "spc": 
			$strMIMEType = "application/x-pkcs7-certificates";
			break;
		case "spl": 
			$strMIMEType = "application/futuresplash";
			break;
		case "src": 
			$strMIMEType = "application/x-wais-source";
			break;
		case "sst": 
			$strMIMEType = "application/vnd.ms-pkicertstore";
			break;
		case "stl": 
			$strMIMEType = "application/vnd.ms-pkistl";
			break;
		case "stm": 
			$strMIMEType = "text/html";
			break;
		case "sv4cpio": 
			$strMIMEType = "application/x-sv4cpio";
			break;
		case "sv4crc": 
			$strMIMEType = "application/x-sv4crc";
			break;
		case "t": 
			$strMIMEType = "application/x-troff";
			break;
		case "tar": 
			$strMIMEType = "application/x-tar";
			break;
		case "tcl": 
			$strMIMEType = "application/x-tcl";
			break;
		case "tex": 
			$strMIMEType = "application/x-tex";
			break;
		case "texi ": 
			$strMIMEType = "application/x-texinfo";
			break;
		case "texinfo": 
			$strMIMEType = "application/x-texinfo";
			break;
		case "tgz": 
			$strMIMEType = "application/x-compressed";
			break;
		case "tif": 
			$strMIMEType = "image/tiff";
			break;
		case "tiff ": 
			$strMIMEType = "image/tiff";
			break;
		case "tr": 
			$strMIMEType = "application/x-troff";
			break;
		case "trm": 
			$strMIMEType = "application/x-msterminal";
			break;
		case "tsv": 
			$strMIMEType = "text/tab-separated-values";
			break;
		case "txt": 
			$strMIMEType = "text/plain";
			break;
		case "uls": 
			$strMIMEType = "text/iuls";
			break;
		case "ustar": 
			$strMIMEType = "application/x-ustar";
			break;
		case "vcf": 
			$strMIMEType = "text/x-vcard";
			break;
		case "vrml ": 
			$strMIMEType = "x-world/x-vrml";
			break;
		case "wav": 
			$strMIMEType = "audio/x-wav";
			break;
		case "wcm": 
			$strMIMEType = "application/vnd.ms-works";
			break;
		case "wdb": 
			$strMIMEType = "application/vnd.ms-works";
			break;
		case "wks": 
			$strMIMEType = "application/vnd.ms-works";
			break;
		case "wmf": 
			$strMIMEType = "application/x-msmetafile";
			break;
		case "wps": 
			$strMIMEType = "application/vnd.ms-works";
			break;
		case "wri": 
			$strMIMEType = "application/x-mswrite";
			break;
		case "wrl": 
			$strMIMEType = "x-world/x-vrml";
			break;
		case "wrz": 
			$strMIMEType = "x-world/x-vrml";
			break;
		case "xaf": 
			$strMIMEType = "x-world/x-vrml";
			break;
		case "xbm": 
			$strMIMEType = "image/x-xbitmap";
			break;
		case "xla": 
			$strMIMEType = "application/vnd.ms-excel";
			break;
		case "xlc": 
			$strMIMEType = "application/vnd.ms-excel";
			break;
		case "xlm": 
			$strMIMEType = "application/vnd.ms-excel";
			break;
		case "xls": 
			$strMIMEType = "application/vnd.ms-excel";
			break;
		case "xlt": 
			$strMIMEType = "application/vnd.ms-excel";
			break;
		case "xlw": 
			$strMIMEType = "application/vnd.ms-excel";
			break;
		case "xof": 
			$strMIMEType = "x-world/x-vrml";
			break;
		case "xpm": 
			$strMIMEType = "image/x-xpixmap";
			break;
		case "xwd": 
			$strMIMEType = "image/x-xwindowdump";
			break;
		case "z": 
			$strMIMEType = "application/x-compress";
			break;
		case "zip": 
			$strMIMEType = "application/zip";
			break;
			
		case "docm": 
			$strMIMEType = "application/vnd.ms-word.document.macroEnabled.12";
			break;
		case "docx": 
			$strMIMEType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
			break;
		case "dotm": 
			$strMIMEType = "application/vnd.ms-word.template.macroEnabled.12";
			break;
		case "dotx": 
			$strMIMEType = "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
			break;
		case "potm": 
			$strMIMEType = "application/vnd.ms-powerpoint.template.macroEnabled.12";
			break;
		case "potx": 
			$strMIMEType = "application/vnd.openxmlformats-officedocument.presentationml.template";
			break;
		case "ppam": 
			$strMIMEType = "application/vnd.ms-powerpoint.addin.macroEnabled.12";
			break;
		case "ppsm": 
			$strMIMEType = "application/vnd.ms-powerpoint.slideshow.macroEnabled.12";
			break;
		case "ppsx": 
			$strMIMEType = "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
			break;
		case "pptm": 
			$strMIMEType = "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
			break;
		case "pptx": 
			$strMIMEType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
			break;
		case "xlam": 
			$strMIMEType = "application/vnd.ms-excel.addin.macroEnabled.12";
			break;
		case "xlsb": 
			$strMIMEType = "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
			break;
		case "xlsm": 
			$strMIMEType = "application/vnd.ms-excel.sheet.macroEnabled.12";
			break;
		case "xlsx": 
			$strMIMEType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			break;
		case "xltm": 
			$strMIMEType = "application/vnd.ms-excel.template.macroEnabled.12";
			break;
		case "xltx": 
			$strMIMEType = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
			break;
	}
	return ($strMIMEType);
}
?>