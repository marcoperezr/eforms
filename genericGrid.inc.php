<?php	
		require_once("eSurveyServiceMod.inc.php");
		require_once("appuser.inc.php");
		require_once("usergroup.inc.php");
		require_once("catalog.inc.php");
		require_once("surveyScheduler.inc.php");
		require_once("dataSource.inc.php");
		require_once("dataSourceMember.inc.php");
		
		$objCatalogColl = BITAMCatalogCollection::NewInstance($this->Repository);
		$objAttributesByCatalog = array();
		foreach ($objCatalogColl->Collection as $objCatalog) {
			$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($this->Repository, $objCatalog->CatalogID);
			$objAttributesByCatalog[$objCatalog->CatalogID] = $objCatalogMembersColl;
		}
		
		$objDataSourceColl = BITAMDataSourceCollection::NewInstance($this->Repository);
		$objAttributesByDataSource = array();
		foreach ($objDataSourceColl->Collection as $objDataSource) {
			$objDataSourceMembersColl = @BITAMDataSourceMemberCollection::NewInstance($this->Repository, $objDataSource->DataSourceID);
			$objAttributesByDataSource[$objDataSource->DataSourceID] = $objDataSourceMembersColl;
		}
		
		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		$strScriptPath = $path_parts["dirname"];
?>
		<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
		<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
		<link href="css/font/roboto/robotofont.css" rel="stylesheet" type="text/css">
		<!--<link rel="stylesheet" type="text/css" href="js/codebase/skins/bitam2/dhtmlx.css"/>-->
		<!--<script src="js/codebase/dhtmlx.js"></script>-->
		<script src="js/codebase/dhtmlxFmtd.js"></script>
      	<script type="text/javascript" src="js/json2.min.js"></script>
		<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
		<script type="text/javascript" data-main="js/_requireconfig.js" src="js/libs/require/require.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>
		
		<!--------------------------------------------------------------------->
		<!-- Inician librerías para el nuevo editor HTML!-->
			<!--Librerias de Redactor-->
			<link rel="stylesheet" href="js/redactor/redactor/redactor.css" />
			<script src="js/redactor/redactor/redactor.js"></script>
			<!--Librería de font-awesome-->
			<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"-->
			
			<!--Plugins para la toolbar-->
			<script src="js/redactor/underline.js"></script>
			<script src="js/redactor/fontsize.js"></script>
			<script src="js/redactor/fontfamily.js"></script>
			<!--En el archivo "fontcolor.js" buscar la línea que dice: $dropdown.width(242); 
			Cambiar el valor de (242) por: (287) Esto hace que se puedan alinear los colores de la paleta-->
			<script src="js/redactor/fontcolor.js"></script>
			
			<!--Selección del lenguaje-->
			<script src="js/redactor/lang/es.js"></script>
			<script src="js/redactor/lang/en.js"></script>
		<!-- Terminan librerías para el nuevo editor HTML!-->
		<!--------------------------------------------------------------------->
		
		<style>
			/* Estilos para ocultar el icono de las ventanas modales (todas las skins utilizadas) */
			.dhxwins_vp_dhx_skyblue div.dhxwin_hdr div.dhxwin_icon.without_icon {
				display: none;
			}
			.dhxwins_vp_dhx_web div.dhxwin_hdr div.dhxwin_icon.without_icon {
				display: none;
			}
			/* Estilo para los botones de búsqueda asociados a grids */
			.search_button input {
				border:1px solid #e1e1e1 !important;
			}
			.selection_container div.dhxform_container {
				border:1px solid #e1e1e1 !important;
				//height:90% !important;
				////overflow-y:auto;
			}
			.selection_container div.objbox {
				//overflow-y:auto;
			}
			.selection_list fieldset.dhxform_fs {
				border:1px solid #e1e1e1 !important;
				//height:95% !important;
			}
			div.dhxform_item_label_left.assoc_button div.dhxform_btn_txt {
				background-image:url(images/add_field.png);
				background-repeat:no-repeat;
				background-position: 0px 3px;
				padding-left: 22px;
				margin: 0px 15px 0px 12px;
				cursor: pointer !important;
			}
			div.dhxform_item_label_left.remove_button div.dhxform_btn_txt {
				background-image:url(images/del_field.png);
				background-repeat:no-repeat;
				background-position: 0px 3px;
				padding-left: 22px;
				margin: 0px 15px 0px 12px;
				cursor: pointer !important;
			}
			/* it's important to set width/height to 100% for full-screen init */
			html, body {
				width: 100%;
				height: 100%;
				margin: 0px;
				overflow: hidden;
			}
			span.tabFormsOpts {
				font-size:12px;
			}
			div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
				 text-transform: none !important;
			}
			/*
			.dhx_dataview_customSection_item_selected{
				border:0px !important;
			}
			.dhx_dataview_customSection_item{
				border:0px !important;
			}
			*/
			/* Estilos para el editor HTML */
			.dhx_cell_cont_editor {
				width: 100% !important;
				border: 1px solid !important;
			}
			div.dhxform_item_label_right div.dhxform_control {
				margin-right: 10px !important;
			}
			.editor_compact div.dhx_cell_stb {
				height:20px !important;
			}
			/*
			.editor_compact div.dhx_cell_cont_editor {
				top:21px !important;
			}
			*/
			.editor_compact div.dhx_cell_stb_button {
				margin-top:2px !important;
			}
			.editor_compact div.dhx_cell_toolbar_def {
				padding:0px !important;
				height:20px !important;
			}
			.editor_compact div.dhx_toolbar_btn {
				border: 0px !important;
				margin-top:0px !important;
				padding: 0px !important;
			}
			.editor_compact div.dhx_cell_editor {
			  width: 100% !important;
			}
			.editor_compact iframe.dhxeditor_mainiframe {
			  width: 100% !important;
			}
			.dhx_cell_editor div.dhx_cell_toolbar_def {
				border: 0px !important;
			}
			.editor_compact div.dhx_toolbar_text {
				padding:0px !important;
				margin:0px !important;
				height: 18px !important;
			}
			/* Estilos para ocultar las tabs pero permitir ver sus celdas */
			.hiddenTabs div.dhx_cell_tabbar {
				position:none !important;	//Este estilo no existe, pero efectivamente bloquea el aplicado a la clase que sobreescribe
			}
			.hiddenTabs div.dhxtabbar_tabs {
				display:none !important;
			}
			.hiddenTabs div.dhx_cell_cont_tabbar {
				border: 0px !important;
			}
			.visibleTabs div.dhx_cell_tabbar {
				position:absolute !important;	//Utilizado para pintar tabs que estarían dentro de la tab principal oculta
			}
			.visibleTabs div.dhxtabbar_tabs {
				display:inline !important;
			}
			.visibleTabs div.dhx_cell_cont_tabbar {
				border: 0px !important;
			}
			/* Estilo para los enlaces de las opciones principales (tabs ocultos) de la ventana de diseño */
			.linkToolbar {
				background-color: #264B83;
			}
			.linktd {
				cursor: pointer;
				font-family: Roboto Regular;
				font-size: 13px;
				//font-weight: bold;
				//text-align: center;
				//text-decoration: underline;
				color: white;	//#333333;
				padding-left: 24px;
				padding-right: 24px;
				//width: 150px;
				white-space: nowrap;
			}
			.linktdSelected {
				text-decoration: underline;
			}
			/* Estilo para mantener seleccionado el botón de selección de la toolbar de preguntas */
			//Finalmente no sirvió esto, porque el evento MouseOver del list option cambia de clase eliminando la anterior aplicada, así que se perdía
			//la clase definida en este archivo, se optó por cambiar directamente el estilo
			.tbOptionSelected {
				  background-color: #fff3a1 !important;
			}
			/* Agrega el border a las celdas del grid */
			table.obj tr td {
				//border-left-width: 1px !important;
				border: 0px !important;
				padding-left: 0px !important;
				padding-right: 0px !important;
			}
			.dhx_textarea {
				margin-left: 0px;
			}
			/* Estilos para los subgrids de propiedades de las tabs de colecciones */
			.dhx_sub_row {
				//overflow: visible !important;
				border: 1px solid #808080 !important;
			}
			.dhx_sub_row table.obj tr td {
				border-left-width:1px solid #808080 !important;
			}
			.dhxdataview_placeholder div.dhx_dataview_item {
				border: 0px !important;
			}
			/* Estilos de los Layouts */
			.dhx_cell_hdr {
				background-color: #f5c862 !important;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>
		<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
		<script>
			//******************************************************************************************************
			//Types definitions
			//******************************************************************************************************
			//Tipos de objetos (basados en el config.php del Administrator, no en el App)
			AdminObjectType = {
				otyItem: 0,
				otySurvey: 1,
				otySection: 2,
				otyQuestion: 3,
				otyAnswer: 4,
				otyAgenda: 5,
				otyCatalog: 6,
				otyAttribute: 7,
				otyDraft: 8,
				otyOutbox: 9,
				otyPhoto: 10,
				otySignature: 11,
				otySetting: 12,
				otySurveyFilter:13,
				otySectionFilter:14,
				otyMenu: 15,
				otyAudio: 16,
				otyVideo: 17,
				otyQuestionFilter: 18,
				otyStatusAgenda: 19,
				otyStatusDocument: 20,
				otySketch: 21,
				otyOption: 22,
				otyShowQuestion: 23,
				otyUser: 24,
				otyUserGroup: 25,
				otyAppCustomization:31,
				otyLink:35
			};
			
			//Tipos de secciones
			SectionType = {
				Standard: 0,
				Dynamic: 1,
				Formatted: 2,
				Multiple: 3,
				Inline: 4,
				Recap: 5
			};
		
			//Tipos de preguntas
			<?//JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
			Type = {
				number: 1,
				simplechoice: 2,
				multiplechoice: 3,
				date: 4,
				text: 5,
				alfanum: 6,
				photo: 8,
				action: 9,
				signature: 10,
				message: 11,
				time: 12,
				skipsection: 13,
				calc: 14,
				document: 15,
				sync: 16,
				callList: 17,
				gps: 18,
				password: 19,
				mapped: 20,
				audio: 21,
				video: 22,
				sketch: 23,
				section: 24
				, barcode: 25
				, ocr: 26
				, exit: 27
				, updateDest: 28
				, myLocation: 29
				, sketchPlus: 30
			};
			
			//Tipos extendidos de preguntas para utilizar en los Fields de las propiedades (Ids para subclasificar los tipos de Open, Simple y demás)
			//Utilizan la numeración básica para las que son identificables con el objeto Type, pero para las que no tenían subclasificación usará un ID nuevo
			<?//JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
			QTypesExt = {
				openNumber: Type.number,
				simpleMenu: Type.simplechoice,
				simpleVert: 201,
				simpleHoriz: 202,
				simpleAuto: 203,
				simpleMap: 204,
				simpleGetData: 205,
				simpleMetro: 206,
				simpleMetroHoriz: 207,
				multiMenu: Type.multiplechoice,
				multiVert: 301,
				multiHoriz: 302,
				multiAuto: 303,
				multiRank: 304,
				openDate: Type.date,
				openText: Type.text,
				openAlpha: Type.alfanum,
				photo: Type.photo,
				signature: Type.signature,
				message: Type.message,
				html: 1100,
				openTime: Type.time,
				skipsection: Type.skipsection,
				calc: Type.calc,
				document: Type.document,
				sync: Type.sync,
				callList: Type.callList,
				gps: Type.gps,
				openPassword: Type.password,
				audio: Type.audio,
				video: Type.video,
				sketch: Type.sketch,
				section: Type.section,
				barCode: Type.barCode,
				ocr: Type.ocr,
				exit: Type.exit,
				updateDest: Type.updateDest,
				myLocation: Type.myLocation,
				menuMenu: 401,
				menuVert: 402,
				menuHoriz: 403
				, sketchPlus: Type.sketchPlus
			}
			
			//Tipos de despliegue
			DisplayMode = {
				dspVertical: 0,
				dspHorizontal: 1,
				dspMenu: 2,
				dspMatrix: 3,
				dspEntry: 4,
				dspAutocomplete: 5,
				dspLabelnum: 6,
				dspDefault: 4,
				dspCustomized: 7,
				dspMap: 8
			};
			
			//JAPR 2015-07-15: Agregado el componente selList
			GFieldTypes = {
				alphanum: "edtxt",			//Texto en una sola linea editado inline
				text: "txttxt",				//Text area multiline
				combo: "coro",				//Combo simple (Combo methods && props: save(), restore(), size(), get(sidx), getKeys(), clear(), put(sidx, val), remove(sidx) .keys [array], .values [array]
				comboSync: "comboExt",		//Combo sincronizable con otras combos (Padres e hijas, por ejemplo, los atributos del catálogo seleccionado) (Personalizado)
				comboEdit: "combo",			//Combo editable (es un componente DHTMLX Combo)
				check: "ch",				//Checkbox
				editor: "editor",			//DHTMLXEditor
				number: "edn",				//Valores numéricos con máscara de captura
				date: "dhxCalendarA",		//Calendario con posibilidad de escribir la fecha
				hour: "time",				//Componente para capturar hora y minutos
				multiList: "clist",			//Lista con checkbox para seleccionar varios elementos
				color: "cp",				//Permite seleccionar el color de un conjunto predeterminado de posibilidades (personalizarlo al JColor que manejamos actualmente)
				image: "img",				//Una imagen con un click a una función
				grid: "sub_row_grid",		//Un grid dentro de otro con la funcionalidad default de propiedades
				readOnly: "rotxt",			//No contiene texto editable, sólo una etiqueta (opcional)
				uploadImg: "uploadImg",		//Subir imágenes al servidor
				selList: "selList",			//Un par de listas para mover elementos desde los disponibles hacia los seleccionados, permitiendo un ordenamiento
				formula: "edFormula",		//Un texto editable pero con un botón de editor
				editorHTML: "editorHTML",	//Nuevo editor HTML				
				editorHTMLDialog: "editorHTMLDialog", //Editor en diálogo
				colorSelector: "colorSelector" //Selector de color
			};
			
			optionsYesNo = {
				0:"<?=translate("No")?>",
				1:"<?=translate("Yes")?>"
			};
			
			optionsYesNoOpt = {
				0:"<?=translate("No")?>",
				1:"<?=translate("Yes")?>",
				2:"<?=translate("Optional")?>",
			};
			
			SectionType = {
				Standard: 0,
				Dynamic: 1,
				Formatted: 2,
				Multiple: 3,
				Inline: 4,
				Recap: 5
			};
			
			propTabType = {
				properties: 0,
				collection: 1
			}
			
			/* Opciones para utilizar en la definición de las configuraciones en los grids */
			var objCheck = {type:GFieldTypes.combo, options:optionsYesNo, default:0};
			var objCheckPlusOpt = {type:GFieldTypes.combo, options:optionsYesNoOpt, default:0};
			var objAlpha = {type:GFieldTypes.alphanum, length:255};
			var objInteger = {type:GFieldTypes.alphanum};
			var opSectionsType = {
				<?=sectNormal?>:{name:"<?=translate('Standard question & answer')?>"},
				<?=sectDynamic?>:{name:"<?=translate('List-based')?>", visibilityRules:{
						tabs:["valuesTab"]
					}
				},
				<?=sectFormatted?>:{name:"<?=translate('Formatted text')?>", visibilityRules:{
						fields:[{tabId:"advTab", fieldId:"autoRedraw"},
							{tabId:"generalTab", fieldId:"formattedText"}
						]
					}
				},
				<?=sectMasterDet?>:{name:"<?=translate('Master-Detail')?>", visibilityRules:{
						fields:[{tabId:"advTab", fieldId:"summaryInMDSection"}]
					}
				},
				<?=sectInline?>:{name:"<?=translate('Inline')?>", visibilityRules:{
						fields:[{tabId:"advTab", fieldId:"displayMode"},
							{tabId:"advTab", fieldId:"showSelector"},
							{tabId:"advTab", fieldId:"switchBehaviour"},
							{tabId:"advTab", fieldId:"MinRecordsToSelect"}
						],
						tabs:["valuesTab"]
					}
				},
				<?=sectRecap?>:{name:"<?=translate('Recap')?>"}
			};
			//Ahora existirá un campo para indicar el tipo de sección como repeticiones de registros, así que adapta esa configuracion al tipo de sección definido
			var opSectionsRepetitionsType = {
				<?=srptSingle?>:{name:"<?=translate("One")?>"},
				<?=srptMulti?>:{name:"<?=translate("Multiples")?>", visibilityRules:{
						fields:[{tabId:"advTab", fieldId:"summaryInMDSection"}]
					}
				},
				<?=srptCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:"advTab", fieldId:"displayMode"},
							{tabId:"advTab", fieldId:"showSelector"},
							{tabId:"advTab", fieldId:"switchBehaviour"},
							{tabId:"advTab", fieldId:"MinRecordsToSelect"}
						]
					}
				}
			};
			
			//JAPR 2015-07-16: Agregada la combo para el formato
			var strOptTabName = "generalTab";
			var opQuestionNumericFormat = {
				"mask1":'',
				"mask2":'0',
				"mask3":'#',
				"mask4":'#,##0',
				"mask5":'$#,##0',
				"mask6":'#,##0.00',
				"mask7":'$#,##0.00',
				"mask8":'# %',
				"mask9":'0 %',
				"mask10":'0.0 %'
			};
			
			var opQuestionDateFormat = {
				"mask0":'dd/mmm/yyyy',
				"mask1":'dd/yyyy/mmm',
				"mask2":'mmm/dd/yyyy',
				"mask3":'mmm/yyyy/dd',
				"mask4":'yyyy/mmm/dd',
				"mask5":'yyyy/dd/mmm',
				"mask6":'dd/yyyy/mm',
				"mask7":'dd/mm/yyyy',
				"mask8":'mm/dd/yyyy',
				"mask9":'mm/yyyy/dd',
				"mask10":'yyyy/mm/dd',
				"mask11":'yyyy/dd/mm',
				"mask12":'dd/yyyy/mmmm',
				"mask13":'dd/mmmm/yyyy',
				"mask14":'mmmm/dd/yyyy',
				"mask15":'mmmm/yyyy/dd',
				"mask16":'yyyy/mmmm/dd',
				"mask17":'yyyy/dd/mmmm',
				"mask18":'dd/yy/mmm',
				"mask19":'dd/mmm/yy',
				"mask20":'mmm/dd/yy',
				"mask21":'mmm/yy/dd',
				"mask22":'yy/mmm/dd',
				"mask23":'yy/dd/mmm',
				"mask24":'dd/yy/mm',
				"mask25":'dd/mm/yy',
				"mask26":'mm/dd/yy',
				"mask27":'mm/yy/dd',
				"mask28":'yy/mm/dd',
				"mask29":'yy/dd/mm',
				"mask30":'dd/yy/mmmm',
				"mask31":'dd/mmmm/yy',
				"mask32":'mmmm/dd/yy',
				"mask33":'mmmm/yy/dd',
				"mask34":'yy/mmmm/dd',
				"mask35":'yy/dd/mmmm'
			};
			
			var strOptTabName = "valuesTab";
			//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
			var opSectionTypeOfFilling = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"}]
					}
				},
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							//{tabId:strOptTabName, fieldId:"catalogID"},
							//{tabId:strOptTabName, fieldId:"catMemberID"}
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"}
						]
					}
				},
				<?=tofMCHQuestion?>:{name:"<?=translate("From another multiple choice's answers")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceQuestionID"}]
					}
				},
				<?=tofInlineSection?>:{name:"<?=translate("From the responses of an Inline section")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceSectionID"}]
					}
				}
			};
			
			var strOptTabName = "advTab";
			var opQuestionDisplayMode = {
				<?=dspVertical?>: {name:"<?=translate("Vertical")?>"},
				<?=dspHorizontal?>: {name:"<?=translate("Horizontal")?>"},
				<?=dspMenu?>: {name:"<?=translate("Menu")?>"},
				<?=dspAutocomplete?>: {name:"<?=translate("Autocomplete")?>"}
			}
			
			//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
			var opQuestionTypeOfFillingMenu = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"}],
						tabs:["valuesTab"]
					}
				},
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							//{tabId:strOptTabName, fieldId:"catalogID"},
							//{tabId:strOptTabName, fieldId:"catMemberID"},
							{tabId:strOptTabName, fieldId:"catMembersList"}
						]
					}
				},
				<?=tofMCHQuestion?>:{name:"<?=translate("From another multiple choice's answers")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceQuestionID"}]
					}
				},
				<?=tofSharedAnswers?>:{name:"<?=translate("Share with another question")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sharedQuestionID"}]
					}
				}
			};
			
			var opQuestionTypeOfFillingMap = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"}],
						tabs:["valuesTab"]
					}
				},
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							//{tabId:strOptTabName, fieldId:"catalogID"},
							//{tabId:strOptTabName, fieldId:"catMemberID"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"},
							{tabId:strOptTabName, fieldId:"dataMemberLatitudeID"},
							{tabId:strOptTabName, fieldId:"dataMemberLongitudeID"}
						]
					}
				},
				<?=tofMCHQuestion?>:{name:"<?=translate("From another multiple choice's answers")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceQuestionID"}]
					}
				},
				<?=tofSharedAnswers?>:{name:"<?=translate("Share with another question")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sharedQuestionID"}]
					}
				}
			};
			
			var opQuestionTypeOfFilling = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"}],
						tabs:["valuesTab"]
					}
				},
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							//{tabId:strOptTabName, fieldId:"catalogID"},
							//{tabId:strOptTabName, fieldId:"catMemberID"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"}
						]
					}
				},
				<?=tofMCHQuestion?>:{name:"<?=translate("From another multiple choice's answers")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceQuestionID"}]
					}
				},
				<?=tofSharedAnswers?>:{name:"<?=translate("Share with another question")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sharedQuestionID"}]
					}
				}
			};
			
			var opMultiTypeOfFilling = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"}],
						tabs:["valuesTab"]
					}
				},
				//JAPR 2016-04-21: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							//{tabId:strOptTabName, fieldId:"catalogID"},
							//{tabId:strOptTabName, fieldId:"catMemberID"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"}
						]
					}
				},
				<?=tofMCHQuestion?>:{name:"<?=translate("From another multiple choice's answers")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceQuestionID"}]
					}
				},
				<?=tofSharedAnswers?>:{name:"<?=translate("Share with another question")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sharedQuestionID"}]
					}
				}
			};
			
			var opDisplayModes = {
				<?=sdspDefault?>:"<?=translate("Default")?>",
				<?=sdspInline?>:"<?=translate("Inline")?>",
				<?=sdspPages?>:"<?=translate("Pages")?>"
			};
			
			var gc_ListOptionSelected = "#fff3a1";
			//True = Envía todas las propiedades del objeto sin importar cual cambió (el grabado puede ser en un botón luego de actualizar varias cosas)
			//False = Sólo se envía la propiedad que acaba de ser modificada (el grabado es en línea)
			var gbFullSave = false;
			var intDialogWidth = 380;
			var intDialogHeight = 350;
			var reqAjax = 1;
			var intMinCellWidth = 300;
			var intSurveyID = 1;
			var giSectionID = 0;
			var giQuestionID = 0;
			var gsLastQGroupSelected = "";		//Indica cual fue el último grupo de tipos de pregunta del cual se creó uno o intentó crear, para abrir ese mismo la siguiente ocasión que se intente crear una pregunta
			var goErrors = new Array();
			//Variables para funciones dinámicas
			var doCancelDragDropOperation;		//Utilizado cuando falla el Drag&Drop de una sección y/o pregunta para restaurarlas a su posición original
			
			//var strSkin = "skyblue";
			//var strSkin = "web";
			//Diálogos
			var objWindows;						//Referencia al componente que almacena todas las ventanas
			var objDialog;						//Diálogo modal común para cualquier proceso que requiera captura de algún valor específico (cada proceso lo volverá a definir)
			//Sólo para el editor de fórmulas
			var objWindowsFn;					//Referencia al componente que almacena la ventana del editor de Formulas
			var objDialogFn;					//Diálogo modal sólo para el editor de fórmulas
			//Para el editor HTML
			var objWindowsEd;					//Referencia al componente que almacena la ventana del resize de imágenes
			var objDialogEd;					//Diálogo modal sólo para la ventana del resize de imágenes
			//Forms
			var objUsersForm;					//Ventana para asociar los usuarios a la Forma
			var objUserGroupsForm;				//Ventana para asociar los grupos de usuarios a la Forma
			//TabBars
			var objFormTabs;					//Cejillas de las configuraciones de la forma
			var tabDesign = "form";
			var tabUsers = "users";
			var tabGroups = "usergroups";
			var tabeBavel = "ebavel";
			var tabModel = "model";
			var tabDetail = "details";
			var objPropsTabs;					//Cejillas de la ventana de propiedades
			//Ribbons
			var objNewQuestionRib;				//Ribbon para seleccionar los tipos de preguntas
			//Accordions
			var objNewQuestionsAcc;				//Selector de la clasificación de preguntas
			//LayOuts							//Opciones principales de la forma
			//JAPRWarning: Remover esto para regresar a Tabs como objeto principal
			var objMainLayout;					//LayOut que se agregó para separar la tabbar con el contenido de la página de un header HTML que represente a las Tabs
			//JAPRWarning: Remover esto para regresar a Tabs como objeto principal (End)
			var objFormsLayout;					//LayOut principal de la forma, contiene a los demás
			var objTreeLayout;					//LayOut de secciones, contiene el DataView de secciones y anteriormente el arbol completo de la forma
			var objNewSectionLayOut;			//LayOut para seleccionar el tipo de sección a agregar
			var objPropsLayout;					//Layout para mostrar el detalle de las propiedades de un objeto
			//Toolbars
			var objSectionsToolbar;					//Toolbar con las opciones para manipular secciones
			var objQuestionsToolbar;			//Toolbar con las opciones para manipular las preguntas
			//PopUps
			var objMousePopUp;					//PopUp para usar con el movimiento del mouse
			var objSectionPopUp;				//PopUp para seleccionar los tipos de secciones
			var objQuestionPopUp;				//PopUp para seleccionar los tipos de preguntas
			//DataViews
			var objSectionsDataView;			//DataView para selecionar la sección desplegada
			//JAPR 2015-05-22: Se agrega nuevamente el DataView de secciones y ahora una toolbar con tipos de preguntas
			//Celdas del LayOut principal de la forma
			var dhxLayOutCell = "a";
			var dhxPreviewCell = "b";
			var dhxPropsCell = "a";
			//Celdas del Layout de propiedades
			var dhxPropsDetailCell = "a";
			
			var dhxTreeCell = "a";
			//var dhxQTypeIDsCell = "c";
			var objTabsColl = new Object();		//Array con la definición de campos para el objeto del cual se está mostrando el detalle (siempre contiene el último desplegado en la celda de propiedades)
			var objSurveyTabsColl = new Object();	//Array con la definición de campos para la forma (se separa ya que esta no está en la celda de propiedades, así que los métodos como getFieldDefinition no pueden usar objTabsColl)
			var intPropsTBPos;
			
			var objOrientationsColl = {
				"Portrait": {},
				"Landscape": {}
			};
			var objTreeState = {};		//Contiene el estado de los nodos del árbol antes de invocar al método de pintado para restaurarlo en el mismo estado
			
			var arrSelUsers = new Object();
			var arrSelRoles = new Object();
			var arrCatalogsOrder = new Array();
			var arrCatalogs = new Object();
			var arrAttributes = new Object();
<?
			//@JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
			foreach ($objDataSourceColl as $objDataSource) {
				$intCatalogID = $objDataSource->DataSourceID;
				echo("\t\t\t"."arrCatalogsOrder.push({$intCatalogID});\r\n");
				echo("\t\t\t"."arrCatalogs[{$intCatalogID}] = {id:{$intCatalogID}, name:'".addslashes($objDataSource->DataSourceName)."', childrenOrder:[], children:{}};\r\n");
				$objDataSourceMembersColl = @$objAttributesByDataSource[$intCatalogID];
				if (!is_null($objDataSourceMembersColl)) {
					foreach ($objDataSourceMembersColl as $objCatMember) {
						$intMemberID = $objCatMember->MemberID;
						echo("\t\t\t"."arrAttributes[{$intMemberID}] = {id:{$intMemberID}, name:'".addslashes($objCatMember->MemberName)."'};\r\n");
						echo("\t\t\t"."arrCatalogs[{$intCatalogID}].childrenOrder.push({$intMemberID});");
						echo("\t\t\t"."arrCatalogs[{$intCatalogID}].children[{$intMemberID}] = arrAttributes[{$intMemberID}];");
					}
				}
			}
			
			/*
			foreach ($objCatalogColl as $objCatalog) {
				echo("\t\t\t"."arrCatalogsOrder.push({$objCatalog->CatalogID});\r\n");
				echo("\t\t\t"."arrCatalogs[{$objCatalog->CatalogID}] = {id:{$objCatalog->CatalogID}, name:'".htmlspecialchars($objCatalog->CatalogName)."', childrenOrder:[], children:{}};\r\n");
				$objCatalogMembersColl = $objAttributesByCatalog[$objCatalog->CatalogID];
				foreach ($objCatalogMembersColl as $objCatMember) {
					echo("\t\t\t"."arrAttributes[{$objCatMember->MemberID}] = {id:{$objCatMember->MemberID}, name:'".htmlspecialchars($objCatMember->MemberName)."'};\r\n");
					echo("\t\t\t"."arrCatalogs[{$objCatalog->CatalogID}].childrenOrder.push({$objCatMember->MemberID});");
					echo("\t\t\t"."arrCatalogs[{$objCatalog->CatalogID}].children[{$objCatMember->MemberID}] = arrAttributes[{$objCatMember->MemberID}];");
				}
			}
			*/
?>
			
			//JAPR 2015-07-30: Agregados botones para seleccionar/deseleccionar todos los elementos
			/* Selecciona todos los elementos (bCheck=true) o ninguno (bCheck=false) del componente CList indicado en el primer parámetro
			*/
			function fnSelectAllCListItems(oCList, bCheck) {
				if (!oCList) {
					return;
				}
				
				$(oCList).find(':checkbox').prop('checked', (bCheck?true:false));
			}
			
			/* Actualiza el valor de campos hijos del que invoca esta función cuando cambia algún valor, de esa manera se puede refrescar el componente de los hijos según el nuevo
			valor del campo padre
			*/
			function fnSetChildrenValues() {
				if (!this.children || !$.isArray(this.children)) {
					return;
				}
				
				for (var intIdx in this.children) {
					var strFieldName = this.children[intIdx];
					if (strFieldName) {
						var blnForForm = (this.parentType == AdminObjectType.otySurvey);
						var objChildField = getFieldDefinition(this.tab, strFieldName, blnForForm);
						if (!objChildField || !objChildField.refreshValue) {
							continue;
						}
						
						objChildField.refreshValue();
					}
				}
			}
			
			/* Abre el editor de fórmula con el texto por default sText, y sobreescribe el objeto oObj al terminar si no se cancela la edición
			*/
			function fnOpenFormulaEditor(oObj, sText, iDataSourceID, htmlRange, sourceID, sourceType) {
				var intDialogWidth = 643;
				var intDialogHeight = 519;
				objDialogFn = objWindowsFn.createWindow({
					id:"editFormula",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialogFn.setText("<?=translate("Edit formula")?>");
				objDialogFn.denyPark();
				objDialogFn.denyResize();
				objDialogFn.setIconCss("without_icon");
				
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialogFn.attachEvent("onClose", function() {
					return true;
				});
				
				//Para permitir accesar a la celda que se recibió como parámetro, se tuvo que agregar una propiedad dinámica al diálogo, ya que se perdía la referencia dentro del
				//evento onContentLoaded
				objDialogFn.bitCellObj = oObj;
				
				//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
				if (iDataSourceID === undefined || !$.isNumeric(iDataSourceID)) {
					iDataSourceID = 0;
				}
				//Entrada por editor HTML
				if(htmlRange != undefined){
					objDialogFn.attachURL("formulaEditor.php?SurveyID=" + intSurveyID + "&aTextEditor=" + encodeURIComponent('') + "&DataSourceID=" + iDataSourceID);
				}else{
					objDialogFn.attachURL("formulaEditor.php?SurveyID=" + intSurveyID + "&aTextEditor=" + encodeURIComponent(sText) + "&DataSourceID=" + iDataSourceID + "&sourceID=" + sourceID + "&sourceType=" + sourceType);
				}
				
				objDialogFn.attachEvent("onContentLoaded", function(win) {
					var objFrame = objDialogFn.getFrame();
					if (objFrame && objFrame.contentWindow && objFrame.contentWindow.document) {
						var objButton = $(objDialogFn.getFrame().contentWindow.document).find("#closeButton");
						if (objButton && objButton.length) {
							//objButton.attr('click', '');
							objButton.on('click', function() {
								var strValue = '';
								if (objDialogFn.bitCellObj) {
									try {
										strValue = objFrame.contentWindow.sParamReturn;
										//Si es un editor HTML el que llama al editor de Fórmulas
										if(htmlRange != undefined){
											//Obtiene el texto que se va a agregar y se lo concatena al final.
											var htmlValue = objDialogFn.bitCellObj[0].value;
											htmlValue = htmlValue.concat(strValue);
											objDialogFn.bitCellObj[0].value = htmlValue;
										}else{
											//Para un input dentro de un componente GFieldTypes.formula de un grid de propiedades
											if (objDialogFn.bitCellObj.childNodes[1]) {
												objDialogFn.bitCellObj.childNodes[1].value = strValue;
											}
											else {
												//Para un input dentro de un div cuando es su primer elemento hijo
												if (objDialogFn.bitCellObj.childNodes[0]) {
													objDialogFn.bitCellObj.childNodes[0].value = strValue;
												}
												else {
													//Para un input directo (por ejemplo, en el diálogo de preguntas, el input de la fórmula)
													objDialogFn.bitCellObj.value = strValue;
													$(objDialogFn.bitCellObj).focus();
												}
											}
										}
									} catch(e) {
										console.log("Error updating the formula : " + e);
									}
								}
								
								setTimeout(function () {
									var strRowId = "";
									var objGrid = undefined;
									if (objDialogFn.bitCellObj && objDialogFn.bitCellObj.parentNode && objDialogFn.bitCellObj.parentNode.grid) {
										var strRowId = objDialogFn.bitCellObj.parentNode.idd;
										objGrid = objDialogFn.bitCellObj.parentNode.grid;
									}
									
									if (objGrid) {
										//Por la manera en que funcionan el grid, al aceptar la fórmula se va a forzar un editStop, esto automáticamente cancelaría el evento blur porque
										//el input que ya no tenía el foco de todas maneras no habría detectado el evento keydown con <enter> así que no se consideraría pérdida de foco,
										//por el contrario se hará el grabado directamente en este punto. En resumen, NO se pondrá el foco en el input para delegarle el grabado a él, sino
										//que cancelará el modo edición y se grabará aquí mismo
										//$(objDialogFn.bitCellObj).find("input:first").focus();
										doUpdateProperty(objGrid, objGrid.bitObjectType, objGrid.bitObjectID, strRowId, 1, strValue);
										//Se envía el parámetro en true, porque de lo contrario el grid revierte el valor a lo que él consideraba el último "grabado"
										objGrid.editStop(true);
									}
									objDialogFn.bitCellObj = undefined;
									//OMMC 2015/09/08: Si el editor de fórmulas fue llamado del editor HTML, antes de cerrarse, regresa el foco al editor HTML lo que permite al valor refrescarse.
									//OMMC 2015/10/15: Validación modificada.
									if(htmlRange != undefined){
										doUnloadDialogFn();
										var redactorArea = $(htmlRange.startContainer).parent('div').last();
										var areaCount = redactorArea.find('div').length;
										if(areaCount && areaCount > 0){
											redactorArea = redactorArea.find(".redactor-editor").focus();
										}else{
											redactorArea.focus();
										}
										objDialogEd.bringToTop();
									}else{
										doUnloadDialogFn();
									}
								}, 100);
							});
						}
					}
				});
			}
			
			function fnOpenEditorHTML(oObj, sText, qTypeID) {
				var intDialogWidth = 690;
				var intDialogHeight = 480;
				if(!objWindows.Ed){
					objWindowsEd = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				objDialogEd = objWindowsEd.createWindow({
					id:"editHTML",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialogEd.setText("<?=translate("Editor")?>");
				objDialogEd.denyPark();
				objDialogEd.denyResize();
				objDialogEd.setIconCss("without_icon");
				
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialogEd.attachEvent("onClose", function() {
					return true;
				});
				
				//Para permitir accesar a la celda que se recibió como parámetro, se tuvo que agregar una propiedad dinámica al diálogo, ya que se perdía la referencia dentro del
				//evento onContentLoaded
				objDialogEd.bitCellObj = oObj;
					
				/*
				//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
				if (iDataSourceID === undefined || !$.isNumeric(iDataSourceID)) {
					iDataSourceID = 0;
				}
				*/
				//OMMC 2016-01-19: Se obtiene la referencia del objeto que se va a editar.
				//Puede ser header o footer de sección o pregunta HTML
				//OMMC 2016-01-20: Agregado el caso para cuando el editor HTML se manda llamar desde la creación de la pregunta.
				//El parámetro se envía como 0 para que no haya conflicto (el estado del editor no se guarda ya que no existe la pregunta en la base de datos).
				//Ya que se unificaron los editores para las preguntas mensaje y HTML, se envía como parámetro el tipo de pregunta que es.
				//Esto le dice al editorHTML que oculte o no el ícono de HTML.
				if(oObj.parentNode.grid == undefined){
					if(qTypeID == QTypesExt.html){
						objDialogEd.attachURL("editorHTML.php?aTextEditor=" + encodeURIComponent(sText) + "&HTMLEditorState=0");
					}else{
						objDialogEd.attachURL("editorHTML.php?aTextEditor=" + encodeURIComponent(sText) + "&HTMLEditorState=0&qTypeID=message");
					}
				}else{
					var aElement = getObject(oObj.parentNode.grid.bitObjectType, oObj.parentNode.grid.bitObjectID);
					if(aElement){
						var editorState;
						switch(aElement.objectType){
							case AdminObjectType.otySection:
								if(oObj.parentNode.grid.bitLastSelectedRowId == 'htmlHeaderDes'){
									editorState = aElement.HTMLHEditorState;
								}else if(oObj.parentNode.grid.bitLastSelectedRowId == 'htmlFooterDes'){
									editorState = aElement.HTMLFEditorState;
								}
							break;
							case AdminObjectType.otyQuestion:
								if(qTypeID == QTypesExt.html){
									editorState = aElement.HTMLEditorState;
								}
							break;
						}
						if(editorState != undefined){
							objDialogEd.attachURL("editorHTML.php?aTextEditor=" + encodeURIComponent(sText) + "&HTMLEditorState=" + editorState);
						}else{
							//OMMC 2016-04-14: Corregido para que aparezca el botón de HTML en diseño de HTML para forma
							if(qTypeID == QTypesExt.html  || aElement.objectType == AdminObjectType.otySurvey){
								objDialogEd.attachURL("editorHTML.php?aTextEditor=" + encodeURIComponent(sText) + "&HTMLEditorState=" + editorState);
							}else{
								objDialogEd.attachURL("editorHTML.php?aTextEditor=" + encodeURIComponent(sText) + "&HTMLEditorState=0&qTypeID=message");
							}
						}
					}
				}
				objDialogEd.attachEvent("onContentLoaded", function(win) {
					var objFrame = objDialogEd.getFrame();
					if (objFrame && objFrame.contentWindow && objFrame.contentWindow.document) {
						var objButton = $(objDialogEd.getFrame().contentWindow.document).find("#closeButton");
						if (objButton && objButton.length) {
							//objButton.attr('click', '');
							objButton.on('click', function() {
								var strValue = '';
								var intState = 0;
								
								//OMMC 2015-01-19: Validado el estado del editor HTML antes de cerrar la ventana.
								if(objFrame.contentWindow.aState){
									intState = parseInt(objFrame.contentWindow.aState);
								}
								
								//JAPR 2015-09-01: Validado que se capture algo en el editor antes de perder el foco si el campo está marcado como requerido
								strValue = objFrame.contentWindow.sParamReturn;
								
								if ($.trim(strValue) == '') {
									var strValue = "<?=translate("You must enter a value for this field")?>";
									alert(strValue);
									return;
								}
								//JAPR
								
								if (objDialogEd.bitCellObj) {
									try {
										//Para un input dentro de un componente GFieldTypes.formula de un grid de propiedades
										if (objDialogEd.bitCellObj.childNodes[1]) {
											objDialogEd.bitCellObj.childNodes[1].value = strValue;
										}
										else {
											//Para un input dentro de un div cuando es su primer elemento hijo
											if (objDialogEd.bitCellObj.childNodes[0]) {
												objDialogEd.bitCellObj.childNodes[0].value = strValue;
											}
											else {
												//Para un input directo (por ejemplo, en el diálogo de preguntas, el input de la fórmula)
												objDialogEd.bitCellObj.value = strValue;
												$(objDialogEd.bitCellObj).focus();
											}
										}
									} catch(e) {
										console.log("Error updating the formula : " + e);
									}
								}
								
								setTimeout(function () {
									var strRowId = "";
									var objGrid = undefined;
									if (objDialogEd.bitCellObj && objDialogEd.bitCellObj.parentNode && objDialogEd.bitCellObj.parentNode.grid) {
										var strRowId = objDialogEd.bitCellObj.parentNode.idd;
										objGrid = objDialogEd.bitCellObj.parentNode.grid;
									}
									
									if (objGrid) {
										//OMMC 2016-01-19: Se actualiza el estado de el editor HTML.
										var aElement = getObject(objGrid.bitObjectType, objGrid.bitObjectID);
										if(aElement){
											switch(aElement.objectType){
												case AdminObjectType.otySection:
													if(objGrid.bitLastSelectedRowId == 'htmlHeaderDes'){
														aElement.HTMLHEditorState = intState;
													}else if(objGrid.bitLastSelectedRowId == 'htmlFooterDes'){
														aElement.HTMLFEditorState = intState;
													}
												break;
												case AdminObjectType.otyQuestion:
													switch(aElement.type){
														case Type.message:
															aElement.HTMLEditorState = intState;
														break;
													}
												break;
											}
										}
										//Por la manera en que funcionan el grid, al aceptar la fórmula se va a forzar un editStop, esto automáticamente cancelaría el evento blur porque
										//el input que ya no tenía el foco de todas maneras no habría detectado el evento keydown con <enter> así que no se consideraría pérdida de foco,
										//por el contrario se hará el grabado directamente en este punto. En resumen, NO se pondrá el foco en el input para delegarle el grabado a él, sino
										//que cancelará el modo edición y se grabará aquí mismo
										//$(objDialogEd.bitCellObj).find("input:first").focus();
										//console.log("doUpdateProperty:"+strValue);
										//debugger;
										doUpdateProperty(objGrid, objGrid.bitObjectType, objGrid.bitObjectID, strRowId, 1, strValue);
										//Se envía el parámetro en true, porque de lo contrario el grid revierte el valor a lo que él consideraba el último "grabado"
										//objGrid.editStop(true);
									}
									objDialogEd.bitCellObj = undefined;
									doUnloadDialogEd();
								}, 100);
							});
						}
					}
				});
			}
			
			//Prepara una función para mandar el option y que se ejecute el proceso correspodiente de show/Hide
			//var fnShowHideItems = function(oOption, bShow) {
			function fnShowHideItems(oOption, bShow) {
				if (!oOption) {
					return;
				}
				
				if (oOption && oOption.visibilityRules) {
					var objVisibilityRules = oOption.visibilityRules;
					//Oculta las tabs indicadas
					if (objVisibilityRules.tabs && objVisibilityRules.tabs.length) {
						for (var intIndex in objVisibilityRules.tabs) {
							var strTabId = objVisibilityRules.tabs[intIndex];
							if (strTabId && objPropsTabs.tabs(strTabId)) {
								try {
									if (bShow) {
										objPropsTabs.tabs(strTabId).show();
									}
									else {
										objPropsTabs.tabs(strTabId).hide();
									}
								} catch(e) {};
							}
						}
					}
					
					//Oculta los campos indicados
					if (objVisibilityRules.fields && objVisibilityRules.fields.length) {
						for (var intIndex in objVisibilityRules.fields) {
							var objHideField = objVisibilityRules.fields[intIndex];
							var strTabId = objHideField['tabId'];
							var strFieldId = objHideField['fieldId'];
							try {
								var objTempGrid = undefined;
								if (objPropsTabs.tabs(strTabId)) {
									objTempGrid = objPropsTabs.tabs(strTabId).getAttachedObject();
								}
								
								if (objTempGrid && strFieldId) {
									objTempGrid.setRowHidden(strFieldId, ((bShow)?false:true));
								}
							} catch(e) {};
						}
					}
				}
			}
						
			//******************************************************************************************************
			//Class definitions
			//******************************************************************************************************
			function GenericItemCls() {
			}
			GenericItemCls.prototype.getDataFields = function(oFields) {}
			
			//Regresa el arra POST con los datos genéricos que identifica el tipo de objeto del que se trata
			GenericItemCls.prototype.getObjFields = function() {
				var objParams = {
					SurveyID:intSurveyID,
					Design:1,
					RequestType:reqAjax,
					Process:"Edit",
					ObjectType:this.objectType
				};
				
				//Agrega el campo ID específico del objeto. Siempre debe existir una propiedad id correspondiente la instancia
				//alert('this.objectIDFieldName:'+this.objectIDFieldName);
				if (this.objectIDFieldName) {
					objParams[this.objectIDFieldName] = this.id;
				}
				//Agrega los campos padres del objeto. En este caso es una colección con la lista de padres en la forma {fieldName:propName, ...} y todos se deben
				//agregar a la lista de parámetros además de existir en la instancia con el nombre indicado
				if (this.parentFields) {
					for (var strFieldName in this.parentFields) {
						var strPropName = this.parentFields[strFieldName];
						if (strPropName && this[strPropName]) {
							objParams[strFieldName] = this[strPropName];
						}
					}
				}
				
				return objParams;
			}
			
			/* Invoca el método de reordenamiento de datos en el server, el cual en caso de haberse ejecutado correctamente, deberá hacer un refresh de definiciones
			y del previo para reflejar el cambio en la información. En caso de error, el proceso simplemente desactivará el icono de espera ya que el Drag&Drop se
			cancela antes de llegar aquí, así que no habría que cambiar nada realmente
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			*/
			GenericItemCls.prototype.reorder = function(oFields) {
				console.log("GenericItemCls.reorder");
				
				//Prepara los parámetros con los datos a grabar
				var objFieldsData = this.getDataFields(oFields);
				if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					return;
				}
				
				var objParams = $.extend({}, this.getObjFields(), objFieldsData);
				if (!objParams) {
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					return;
				}
				objParams["Process"] = "Reorder";
				
				var strParams = '';
				var strAnd = '';
				for (var strProp in objParams) {
					//JAPR 2018-11-01: Corregido un bug con el escape de caracteres en requests (#S0SRC1)
					strParams += strAnd + strProp+"="+encodeURIComponent(objParams[strProp]);
					//JAPR
					strAnd = '&';
				}
				
				$('#divSaving').show();
				window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
					doReorderConfirmation(loader);
				});
			}
			
			/* A partir de la definición del objeto (el cual ya debería estar creado en el servidor), prepara el array de datos a enviar para actualizar su
			información mediante Ajax, además de reportar cualquier error ocurrido en el proceso
			La actualización de información se realizará dependiendo del método configurado con la variable gbFullSave
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			*/
			GenericItemCls.prototype.save = function(oFields, oCallbackFn, bRemoveProgress) {
				console.log("GenericItemCls.save");
				//Prepara los parámetros con los datos a grabar
				var objFieldsData = this.getDataFields(oFields);
				if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
					return;
				}
				
				var objParams = $.extend({}, this.getObjFields(), objFieldsData);
				if (!objParams) {
					return;
				}
				
				var strParams = '';
				var strAnd = '';
				for (var strProp in objParams) {
					var varValue = objParams[strProp];
					if ($.isArray(varValue) || $.isPlainObject(varValue)) {
						//En este caso el parámetro se genera también como array
						for (var intIndex in varValue) {
							strParams += strAnd + strProp + "[" + intIndex + "]=" + encodeURIComponent(varValue[intIndex]);
							strAnd = '&';
						}
					}
					else {
						strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
					}
					strAnd = '&';
				}
				
				$('#divSaving').show();
				//JAPR 2015-08-10: Agregado el callback de la función de grabado
				if (oCallbackFn && typeof oCallbackFn == "function") {
					window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
						doSaveConfirmation(loader, oCallbackFn, bRemoveProgress);
					});
				}
				else {
					window.dhx4.ajax.post("processRequest.php", strParams, doSaveConfirmation);
				}
				//JAPR
			}
			
			/* Similar a la función save, a excepción que el parámetro oFields no representa propiedades de la instancia sino el conjunto de parámetros a enviar
			directamente, así que se puede usar para basar el request en un tipo de objeto pero mandando datos que no pertenecen a él (originalmente usado para
			invitar usuarios/grupos a una forma). El parámetro oFields puede sobreescribir las propiedades defalt de getObjFields
			El parámetro oCallbackFn permite ejecutar dicha función al terminar el proceso
			*/
			GenericItemCls.prototype.send = function(oFields, oCallbackFn, bRemoveProgress) {
				console.log("GenericItemCls.send");
			
				//Prepara los parámetros con los datos a grabar
				var objFieldsData = oFields;
				if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
					return;
				}
				
				var objParams = $.extend({}, this.getObjFields(), objFieldsData);
				if (!objParams) {
					return;
				}
				
				var strParams = '';
				var strAnd = '';
				for (var strProp in objParams) {
					strParams += strAnd + strProp+"="+encodeURIComponent(objParams[strProp]);
					strAnd = '&';
				}
				
				$('#divSaving').show();
				window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
					doSaveConfirmation(loader, oCallbackFn, bRemoveProgress);
				});
			}
			
			/* Remueve todo tag HTML del nombre del objeto para regresar un nombre que sea presentable a los usuarios, en caso de que el nombre quedara vacío, intenta utilizar
			el shortName del objeto si es que tiene uno
			*/
			GenericItemCls.prototype.getName = function() {
				var strName = (this.nameHTML)?this.nameHTML:"";
				strName = $.trim(strName.replace(new RegExp("<[^>]*>", "gi"), ''));
				if (!strName) {
					if (this.shortName) {
						strName = this.shortName;
					}
				}
				
				return strName;
			}
			
			//******************************************************************************************************
			function TabCls(sObjectDef) {
				this.values = new Object();							//Colección de los valores en los campos tipo Combo o Lista
				this._values = new Object();						//Array interno posterior a evaluar getvalues
				this.valuesOrder = new Array();						//Array con los Ids de opciones ordenados tal como se deben ver
				this._valuesOrder = new Array();					//Array interno posterior a evaluar getvalues
				
				//Extiende las propiedades default si es que fueron especificadas
				if (sObjectDef) {
					$.extend(this, sObjectDef);
				}
				
				/* En caso de que el objeto values sea una función, devuelve la lista real de valores y actualiza el array valuesOrder utilizando dicha función */
				this.getValues = function() {
					this.useOrder = true;
					this._values = this.values;
					this._valuesOrder = this.valuesOrder;
					if (!this._valuesOrder || ($.isArray(this._valuesOrder) && !this._valuesOrder.length)) {
						this.useOrder = false;
						this._valuesOrder = this._values;
					}
					
					//Si se trata de funciones, las ejecuta para obtener la colección real de valores
					if (typeof this.values == "function") {
						this._values = this.values();
						if (!this._values) {
							this._values = new Object();
						}
					}
					if (typeof this.valuesOrder == "function") {
						this._valuesOrder = this.valuesOrder();
						if (!this._valuesOrder) {
							this._valuesOrder = new Array();
						}
					}
					
					return this._values;
				}
			}
			
			//******************************************************************************************************
			function FieldCls(sObjectDef) {
				//Constructor
				this.id = 0;											//ID numérico del objeto que se está editando (SurveyID/SectionID/QuestionID según el caso)
				this.name = "";											//Nombre único del campo
				this.label = "";										//Etiqueta para desplegar en la lista de configuraciones del objeto
				this.type = GFieldTypes.alphanum;						//Tipo de componente para captura
				this.parentType = AdminObjectType.otyQuestion;			//Tipo de objeto del que se extraen los valores (Survey/Section/Question)
				this.maxLength = 0;										//Sólo para tipos con texto, es la longitud del texto a capturar
				this.showWhenNew = true;								//Indica si el campo debe ser o no capturable cuando el objeto es nuevo (id == -1) o sólo al editarlo
				this.useOrder = true;									//Indica si está o no definido un array de optionsOrder para ordenar el contenido de la combo (se asigna automáticamente al ejecutar getOptions)
				this.options = new Object();							//Colección de los valores en los campos tipo Combo o Lista (puede ser una función que obtiene dicho array)
				this._options = new Object();							//Array interno posterior a evaluar getOptions
				this.optionsOrder = new Array();						//Array con los Ids de opciones ordenados tal como se deben ver (puede ser una función que obtiene dicho array)
				this._optionsOrder = new Array();						//Array interno posterior a evaluar getOptions
				this.parentField = undefined;							//Referencia al campo del que dependen las opciones de respuesta
				this.tab = "";											//Tab al cual pertenece el campo
				this.customButtoms = new Array;							//Definir cuales son los botones personalizados que se desplegarán en el editor
																		//Botones por default: bold, italic, underline
																		//Botones personalizados (configurables en customButtoms):
																		//backColor, fontName, fontSize, foreColor
				this.readOnly = false;									//Indica si se debe o no permitir editar el campo (si no se permite, se cambia a "ro" su tipo interno)
				this.resize = undefined;								//Sólo modo "collection". Indica si la columna permitirá el auto-resize o no (default == true)
				this.default = undefined;								//Valor default del campo (puede ser una función que regrese el valor a utilizar)
				
				//Extiende las propiedades default si es que fueron especificadas
				if (sObjectDef) {
					$.extend(this, sObjectDef);
				}
				
				/* En caso de que el objeto options sea una función, devuelve la lista real de objetos y actualiza el array optionsOrder utilizando dicha función
				JAPR 2015-07-15: Agregado el caso en el que el campo tiene un campo padre del cual depende para obtener sus options (sólo aplica a ciertos tipos de campos como combo o 
				selList)
				*/
				this.getOptions = function() {
					this.useOrder = true;
					this._options = this.options;
					this._optionsOrder = this.optionsOrder;
					
					//JAPR 2015-07-15: Agregado el caso en el que el campo tiene un campo padre del cual depende para obtener sus options (sólo aplica a ciertos tipos de campos)
					//Si hubiera un campo padre configurado, entonces tiene que obtener las opciones directamente de él, por lo que no aplica la parte de ejecutar la función de options
					//que se encuentra mas abajo
					if (this.parent) {
						var blnForForm = (this.parentType == AdminObjectType.otySurvey);
						var objParentField = getFieldDefinition(this.tab, this.parent, blnForForm);
						if (objParentField && objParentField.options) {
							var intParentValue = objParentField.getValue(true);
							if (!$.isNumeric(intParentValue)) {
								if (objParentField.default !== undefined) {
									intParentValue = objParentField.default;
								}
							}
							
							var blnUseOrder = true;
							var objParentOptions = new Object();
							var objParentOptionsOrder = new Array();
							if (objParentField.getOptions) {
								var objParentOptions = objParentField.getOptions();
								var objParentOptionsOrder = objParentField._optionsOrder;
								var blnUseOrder = objParentField.useOrder;
							}
							
							var objParentOption = objParentOptions[intParentValue];
							if (objParentOption && objParentOption.children) {
								this._options = objParentOption.children;
								if (!this._options) {
									this._options = new Object();
								}
								
								//Si hay una configuración de orden, se utiliza para agregar los elementos en dicha secuencia, de lo contrario se usa el orden recibido
								this._optionsOrder = objParentOption.childrenOrder;
								if (!this._optionsOrder || ($.isArray(this._optionsOrder) && !this._optionsOrder.length)) {
									this.useOrder = false;
									this._optionsOrder = this._options;
								}
							}
						}
					}
					else {
						if (!this._optionsOrder || ($.isArray(this._optionsOrder) && !this._optionsOrder.length)) {
							this.useOrder = false;
							this._optionsOrder = this._options;
						}
						
						//Si se trata de funciones, las ejecuta para obtener la colección real de valores
						if (typeof this.options == "function") {
							this._options = this.options();
							if (!this._options) {
								this._options = new Object();
							}
						}
						if (typeof this.optionsOrder == "function") {
							this._optionsOrder = this.optionsOrder();
							if (!this._optionsOrder) {
								this._optionsOrder = new Array();
							}
						}
					}
					
					return this._options;
				}
				
				/* Obtiene el valor del campo directamente del objeto en memoria que representa a la forma (selSurvey)
				Si se especifica el parámetro bRaw, entonces se obtendrá el valor natural del campo (por ejemplo el integer con el id en caso de ser una combo), de lo contrario se
				obtendrá el valor string para ser presentado en el Grid, el cual es sólo una representación visual y no se debe usar para grabar nada
				//JAPR 2015-06-29: Agregados los parámetros iObjectType e iObjectID para permitir variar el tipo de objeto del que se extraen las propiedades. Utilizado durante la generación
				de tabs tipo collection en la que los valores representan objetos hijos del definido inicialmente en la tab. Si no se especifican, se usarán los defindos en el FieldCls
				*/
				this.getValue = function(bRaw, iObjectType, iObjectID) {
					console.log('FieldCls.getValue ' + bRaw);
					
					//Accesar al objeto selSurvey (al nivel del frame principal), al Survey/Section/Question correspondiente, para pedirle la propiedad del name
					var objObject = getObject(this.parentType, this.id, iObjectType, iObjectID);
					if (!objObject) {
						return "";
					}
					
					//El valor numérico en caso de tratarse de una propiedad tipo combo
					var varValue = objObject[this.name];
					if (bRaw) {
						//El valor raw es en que finalmente se graba en la metadata, así que se obtiene directamente del objeto en cuestión
						switch (this.type) {
							case GFieldTypes.grid:
								//Si se trata de un grid, se usará un valor dummy ya que se va a generar programáticamente
								varValue = 1;
								break;
						}
					}
					else {
						//El valor no raw es el que el componente requiere para pintarse correctamente en el grid, así que puede recibir tratamiento especial
						switch (this.type) {
							//JAPR 2015-07-15: Agregado el componente selList
							case GFieldTypes.selList:
								//En el caso de una lista de selección, el valor por default es un array con los Ids de cada elemento de la lista de seleccionados, así que es igual que
								//el valor real del campo por lo que no es necesario hacer ajustes (es un array para permitir la lista ordenada según lo que define el usuario, las descripciones
								//se obtienen a partir de las opciones del campo)
								/*
								var varTempValue = new Object();
								var blnUseOrder = true;
								var cmbOptions = new Object();
								var cmbOptionsOrder = new Array();
								if (this.getOptions) {
									var cmbOptions = this.getOptions();
									var cmbOptionsOrder = this._optionsOrder;
									var blnUseOrder = this.useOrder;
								}
								
								if (cmbOptions) {
									for (var intIdx in varValue) {
										var intObjectID = varValue[intIdx];
										for (var intId in cmbOptionsOrder) {
											var intTmpObjectID = cmbOptionsOrder[intId];
											if (intTmpObjectID == intObjectID) {
												varTempValue[intObjectID] = cmbOptions[intObjectID];
												break;
											}
										}
									}
								}
								varValue = varTempValue;
								*/
								break;
						
							case GFieldTypes.multiList:
								//En el caso de una lista múltiple de selección, el valor por default es el conjunto de las descripciones del valor real del campo, el cual es un array
								var varTempValue = '';
								var strAnd = '';
								var blnUseOrder = true;
								var cmbOptions = new Object();
								var cmbOptionsOrder = new Array();
								if (this.getOptions) {
									var cmbOptions = this.getOptions();
									var cmbOptionsOrder = this._optionsOrder;
									var blnUseOrder = this.useOrder;
								}
								
								if (cmbOptions) {
									for (var intIdx in varValue) {
										var intObjectID = varValue[intIdx];
										for (var intId in cmbOptionsOrder) {
											var intTmpObjectID = cmbOptionsOrder[intId];
											if (intTmpObjectID == intObjectID) {
												varTempValue += strAnd + cmbOptions[intId];
												strAnd = ',';
												break;
											}
										}
									}
								}
								varValue = varTempValue;
								break;
						
							case GFieldTypes.grid:
								//Si se trata de un grid, se usará un valor dummy ya que se va a generar programáticamente
								varValue = 1;
								break;
						
							case GFieldTypes.image:
								//Si se trata de una imagen, en caso de no venir asignado el valor directo con el nombre del campo, se verifica si está definida una imagen
								//en el campo y en ese caso se regresa directamente la imagen de la definición, la cual funciona como un default
								if (varValue === undefined && this.image) {
									varValue = this.image;
								}
								
								//Para el grid la imagen separa sus propiedades en el formato: urlImagen^alttext^javascript_OR_url^target
								var strLink = this.click;
								if (strLink) {
									varValue = varValue + "^^" + strLink;
								}
								break;
						
							//case GFieldTypes.comboSync:
							case GFieldTypes.combo:
								//Si se trata de una combo, debe buscar el valor dentro de las opciones disponibles del campo y regresar el texto que la representa
								//Los posibles valores de estas combos tienen que ser IDs enteros, así que en caso de no ser un valor entero, se asumirá 0
								//(esto puede pasar si el objeto en cuestión no tiene la propiedad indicada porque no aplicaba, y generalmente pasará sólo con
								//las combos que tienen opciones de Si/No)
								if (!$.isNumeric(varValue) && this.default !== undefined) {
									varValue = this.default;
								}
								
								var blnFound = false;
								var blnUseOrder = true;
								var cmbOptions = new Object();
								var cmbOptionsOrder = new Array();
								if (this.getOptions) {
									var cmbOptions = this.getOptions();
									var cmbOptionsOrder = this._optionsOrder;
									var blnUseOrder = this.useOrder;
								}
								
								if (cmbOptions) {
									for (var intId in cmbOptions) {
										if (intId == varValue) {
											var objValue = cmbOptions[intId];
											varValue = (objValue.name)?objValue.name:objValue;
											blnFound = true;
											break;
										}
									}
								}
								
								//Si se permite un elemento vacío, automáticamente usa esa descripción como el valor inicial
								if (!blnFound && this.empty) {
									varValue = "(<?=translate("None")?>)";
									blnFound = true;
								}
								
								if (!blnFound) {
									varValue = "<?=translate("Unknown")?>";
								}
								break;
						}
					}
					
					return  varValue;
				}
				
				this.setValue = function(iObjectType, iObjectID, val) {
					console.log('FieldCls.setValue ' + val);
					//Accesar al objeto selSurvey (al nivel del frame principal), al Survey/Section/Question correspondiente, para pedirle la propiedad del name
					var objObject = getObject(this.parentType, this.id, iObjectType, iObjectID);
					if (!objObject) {
						return "";
					}
					
					//El valor numérico en caso de tratarse de una propiedad tipo combo
					objObject[this.name] = val;
				}

				/* Obtiene el valor del campo directamente del grid donde se está definiendo la propiedad, así que es el valor que habría configurado el usuario en caso de haberlo
				cambiado
				Si se especifica el parámetro bRaw, entonces se obtendrá el valor natural del campo (por ejemplo el integer con el id en caso de ser una combo), de lo contrario se
				obtendrá el valor string para ser presentado en el Grid, el cual es sólo una representación visual y no se debe usar para grabar nada
				*/
				this.getPropValue = function(bRaw) {
					console.log('FieldCls.getPropValue ' + bRaw);
					
					if (!objPropsTabs) {
						return "";
					}
					
					var varValue = ""
					var objTempGrid = undefined;
					if (objPropsTabs.tabs(this.tab)) {
						objTempGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
						if (objTempGrid) {
							if (bRaw) {
								varValue = objTempGrid.getRowAttribute(this.name, "value");
							}
							else {
								if (objTempGrid.cells(this.name, 1)) {
									varValue = objTempGrid.cells(this.name, 1).getValue();
								}
							}
							
							if (varValue === undefined || varValue === null || varValue === NaN) {
								varValue = "";
							}
						}
					}
					
					return varValue;
				}
				
				/* Basado en el valor del campo y el tipo, recorre las opciones para aplicar visibilidad encadenada de otros campos si tiene alguna regla configurada*/
				this.applyVisibilityRules = function() {
					console.log('FieldCls.applyVisibilityRules ' + this.name);
					
					var intValue = this.getValue(true);
					var blnUseOrder = true;
					var cmbOptions = new Object();
					var cmbOptionsOrder = new Array();
					if (this.getOptions) {
						var cmbOptions = this.getOptions();
						var cmbOptionsOrder = this._optionsOrder;
						var blnUseOrder = this.useOrder;
					}
					
					//Primero oculta todos los elementos que otras opciones deben mostrar
					var objSelectedOption = undefined;
					for (var intOption in cmbOptions) {
						var objOption = undefined;
						if (intOption == intValue) {
							objSelectedOption = cmbOptions[intOption];
						}
						else {
							var objOption = cmbOptions[intOption];
						}
						
						if (objOption && objOption.visibilityRules && objOption.visibilityRules) {
							fnShowHideItems(objOption, false);
						}
					}
					
					//Al terminar muestra los elementos de la opción seleccionada
					if (objSelectedOption && objSelectedOption.visibilityRules) {
						fnShowHideItems(objSelectedOption, true);
					}
				}
				
				// Utilizada para validar mínimos, máximos o cualquier otra cosa que se requiera en el field
				this.validate = function() {
					return true;
				}
				
				// Determina si es o no visible la configuración
				this.visible = function() {
					return true;
				}
				
/*
			GFieldTypes = {
				alphanum: "ed",				//Texto en una sola linea editado inline
				text: "edtxt",				//Text area multiline
				combo: "coro",				//Combo simple (Combo methods && props: save(), restore(), size(), get(sidx), getKeys(), clear(), put(sidx, val), remove(sidx) .keys [array], .values [array]
				comboSync: "comboExt",		//Combo sincronizable con otras combos (Padres e hijas, por ejemplo, los atributos del catálogo seleccionado) (Personalizado)
				check: "ch",				//Checkbox
				editor: "editor",			//DHTMLXEditor
				number: "edn",				//Valores numéricos con máscara de captura
				date: "dhxCalendarA",		//Calendario con posibilidad de escribir la fecha
				hour: "time",				//Componente para capturar hora y minutos
				multiList: "clist",			//Lista con checkbox para seleccionar varios elementos
				color: "cp"					//Permite seleccionar el color de un conjunto predeterminado de posibilidades (personalizarlo al JColor que manejamos actualmente)
				image: "img"				//Una imagen con un click a una función
			};
*/
				//Agrega la row que representa este campo/configuración en el grid correspondiente
				this.draw = function(oGrid, iObjectType, iObjectID) {
					console.log('FieldCls.draw');
					
					rowValues = new Array;
					//JAPR 2015-07-15: Agregado el componente con la lista seleccionable ordenada
					//Si el componente es una lista seleccionable, se debe colocar en la primer celda y no en la segunda como los demás, además se aplica un colSpan para que se vea a lo
					//largo de las dos columnas
                    //JAPR 2015-11-12: Corregido un bug, por la manera en que funciona DHTMLX, si se va a cambiar el tipo de la celda, no se puede asignar primero un valor pues al
                    //invocar al cambio del tipo, primero se leerá el valor con el tipo anterior y tal cual se va a sobreescribir en el campo con el tipo nuevo, así que podría quedar
                    //dañado (esto es especialmente evidente cuando el tipo original permitía HTML, se le asigna un HTML Entity, y luego se cambia a un tipo solo texto, la HTML Entity
                    //quedará visible en lugar de mostrar el caracter que representa (#WN65X7)
					if (this.type == GFieldTypes.selList) {
						rowValues[0] = this.getValue(undefined, iObjectType, iObjectID);
						rowValues[0] = "";
					}
					else {
						rowValues[0] = this.label;
                        //JAPR 2015-11-12: Para corregir el bug identificado en el comentario de este día, ahora se asignará el valor luego de cambiar el tipo (#WN65X7)
                        rowValues[1] = "";    //this.getValue(undefined, iObjectType, iObjectID);
                        //JAPR
					}
					
					//JAPR 2015-06-18: Validado que al forzar el llenado manual de los componentes, no se lance el evento de onCellChanged
					//agregando una bandera temporal al grid
					oGrid.bitDisableEvents = true;
					if(typeof(rowValues[1])=="string" && rowValues[1].indexOf("&")!=-1)
					{
						rowValues[1] = rowValues[1].replace("&", "&amp;");
					}
					oGrid.addRow(this.name, rowValues);
					oGrid.setRowAttribute(this.name, "tabName", this.tab);
					oGrid.setRowAttribute(this.name, "fieldName", this.name);
					//oGrid.setRowAttribute(this.name, "options", this.options);
					oGrid.setRowAttribute(this.name, "type", this.type);
					oGrid.setRowAttribute(this.name, "objectType", this.parentType);
					oGrid.setRowAttribute(this.name, "value", this.getValue(true, iObjectType, iObjectID));
					if (this.hidden) {
						oGrid.setRowHidden(this.name, true);
					}
					
					//JAPR 2015-07-15: Agregado el componente con la lista seleccionable ordenada
					//Si el componente es una lista seleccionable, se debe colocar en la primer celda y no en la segunda como los demás, además se aplica un colSpan para que se vea a lo
					//largo de las dos columnas
					if (this.type == GFieldTypes.selList) {
						oGrid.enableColSpan(true);
						oGrid.setCellExcellType(this.name, 0, this.type);
						oGrid.setColspan(this.name, 0, 2);
					}
					else {
						oGrid.setCellExcellType(this.name, 1, this.type);
						//JAPR 2015-11-12: Para corregir el bug identificado en el comentario de este día, ahora se asignará el valor luego de cambiar el tipo (#WN65X7)
						oGrid.cells(this.name, 1).setValue(this.getValue(undefined, iObjectType, iObjectID));
						//JAPR
					}
					if (this.type == GFieldTypes.multiList) {
						var arrOptions = new Array();
						for (var intOptionNum in this.options) {
							arrOptions.push(this.options[intOptionNum]);
						}
						oGrid.registerCList(1, arrOptions);
					}
					oGrid.bitDisableEvents = false;
					//JAPR
					
					return true;
				}
			}
			
			/* Permite hacer ajustes a la pantalla donde DHTMLX no lo hace correctamente */
			function doOnResizeScreen() {
				console.log('doOnResizeScreen');
				//Para permitir que se pinten correctamente las ventanas de Invitar usuario y grupos, es necesario ocultar momentáneamente los divs que las contienen
				//ya que DHTMLX no estaba redimensionando sus componentes al maximizar la ventana
				var intTopAdjust = 120;
				//$('#divInviteUsers .selection_container div.dhxform_container').height($(window).height() - intTopAdjust - 50);
				var intHeight = $(window).height();
				$('.selection_container div.dhxform_container').height(intHeight - intTopAdjust - 50);
				$('.selection_container div.objbox').height(intHeight - intTopAdjust - 50);
				$('.selection_list fieldset.dhxform_fs').height(intHeight - intTopAdjust);
				//JAPRWarning: Remover esto para regresar a Tabs como objeto principal
				if (objMainLayout) {
					objMainLayout.setSizes();
				}
				if (objFormTabs) {
					objFormTabs.setSizes();
				}
				//JAPRWarning: Remover esto para regresar a Tabs como objeto principal (End)
			}
			
			/* Descarga las ventanas adicionales creadas así como cualquier otro componente */
			function doOnUnload() {
				console.log('doOnUnload');
				if (objWindows && objWindows.unload) {
					objWindows.unload();
				}
			}
			
			/* Dado un grid, regresa la lista de elementos que contiene en la propiedad id (sólo aplica obviamente para grids que se generan de dicha manera)
			El valor devuelto es un array, o bien se puede pedir directamente el string con el valor bGetArray = false
			*/
			function doGetGridSelectedIDs(oGrid, sSep, bGetArray) {
				console.log("doGetGridSelectedIDs");
				
				if (!oGrid) {
					return;
				}
				if (!sSep) {
					sSep = ', ';
				}
				if (bGetArray === undefined) {
					bGetArray = true;
				}
				
				var intRows = oGrid.getRowsNum();
				var strAnd = "";
				var varList = "";
				for (var intIndex = 0; intIndex < intRows; intIndex++) {
					var strRowId = oGrid.getRowId(intIndex);
					if (strRowId) {
						var intObjectID = oGrid.getUserData(strRowId, "id");
						if (intObjectID) {
							varList += strAnd + intObjectID;
							strAnd = sSep;
						}
					}
				}
				
				if (bGetArray) {
					varList = varList.split(sSep);
				}
				
				return varList;
			}
			
			
			/* Configura un grid previamente generado basado en el objeto con el formato similar a un TabCls, de tal manera que se puedan generar subGrids e invocar a este
			método para generarlos. Si se necesita un objeto específico de alguna colección que le pertenece, se utilizan 
			los parámetros child (esto se debería aplicar sólo si el objeto principal no tiene una forma directa de ser accesado, originalmente utilizado para las opciones de
			respuesta fijas de preguntas/secciones)
			*/
			function doGenerateGrid(oGrid, oSettings, iObjectType, iObjectID, iChildObjectType, iChildObjectID) {
				var objGrid = oGrid;
				var objTab = oSettings;
				if (!objGrid || !objTab) {
					return;
				}
				
				var objValues = new Object();
				var objValuesOrder = new Array();
				objGrid.setImagePath("<?=$strScriptPath?>/images/");          //the path to images required by grid
				switch (objTab.type) {
					case propTabType.collection:
						//Se debe generar el Grid sólo como una colección de valores con un mínimo de propiedades id y Name, siendo el id el rowId y el Name la columna default
						var strHeader = "";
						var strHeaderWidthPerc = "";
						var strHeaderWidth = "";
						var strHeaderAlign = "";
						var strHeaderTypes = "";
						var strHeaderResize = "";
						var arrHeaderStyle = new Array();
						var strAnd = "";
						var intPos = -1;
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							if (aField) {
								strHeader += strAnd + aField.label;
								var strWidth = $.trim(aField.width);
								if ((intPos = strWidth.indexOf("%")) != -1) {
									strWidth = strWidth.substr(0, intPos);
									strHeaderWidthPerc += strAnd + ((strWidth)?strWidth:10);
								}
								else {
									strHeaderWidth += strAnd + ((strWidth)?strWidth:10);
								}
								strHeaderAlign += strAnd + ((aField.align)?aField.align:"left");
								strHeaderTypes += strAnd + ((aField.type)?aField.type:"ro");
								strHeaderResize += strAnd + ((aField.resize !== undefined)?aField.resize:true);
								arrHeaderStyle.push("background-color:cccccc;");
								strAnd = ",";
							}
						}
						
						objGrid.setHeader(strHeader, null, arrHeaderStyle);
						if (strHeaderWidthPerc) {
							objGrid.setInitWidthsP(strHeaderWidthPerc);
						}
						else {
							objGrid.setInitWidths(strHeaderWidth);
						}
						objGrid.setColAlign(strHeaderAlign);
						objGrid.setColTypes(strHeaderTypes);
						objGrid.enableMarkedCells();
						objGrid.enableResizing(strHeaderResize);
						objGrid.bitObjectType = iObjectType;
						objGrid.bitObjectID = iObjectID;
						objGrid.bitTabName = objTab.id;
						
						objGrid.bitValuesRendered = 0;
						var objValues = new Object();
						var objValuesOrder = new Array();
						if (objTab.getValues) {
							objValues = objTab.getValues();
							objValuesOrder = objTab._valuesOrder;
						}
						objGrid.bitValuesLength = objValuesOrder.length;
						
						//Realiza ajustes específicos a las columnas según el tipo de cada una
						var intColIndex = 0;
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							if (aField) {
								switch (aField.type) {
									case GFieldTypes.multiList:
										var cmbOptions = new Object();
										var cmbOptionsOrder = new Array();
										if (aField.getOptions) {
											var cmbOptions = aField.getOptions();
											var cmbOptionsOrder = aField._optionsOrder;
											var blnUseOrder = aField.useOrder;
										}
										objGrid.registerCList(intColIndex, cmbOptions);
										break;
								
									case GFieldTypes.combo:
										//Obtiene la referencia a la combo y le asigna los valores por default que tiene el field
										var cmbOptions = new Object();
										var cmbOptionsOrder = new Array();
										if (aField.getOptions) {
											var cmbOptions = aField.getOptions();
											var cmbOptionsOrder = aField._optionsOrder;
											var blnUseOrder = aField.useOrder;
										}
										
										var objCombo = objGrid.getCombo(intColIndex);
										if (objCombo) {
											objCombo.clear();
											if (aField.empty) {
												var idOption = 0;
												if (aField.emptyId !== undefined) {
													var idOption = aField.emptyId;
												}
												var strName = "(<?=translate("None")?>)";
												objCombo.put(idOption, strName);
											}
											
											for (var idOption in cmbOptions) {
												var strName = cmbOptions[idOption].name;
												if (strName === undefined) {
													strName = cmbOptions[idOption];
												}
												objCombo.put(idOption, strName);
											}
										}
										break;
								}
								intColIndex++;
							}
						}
						
						objGrid.attachEvent("onCellMarked", function(rid, ind) {
							this.bitLastSelectedRowId = rid;
							this.bitLastSelectedColId = ind;
							if (rid == "new") {
								//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
								var intObjectType = AdminObjectType.otyOption;
								var strTabName = this.bitTabName;
								var objTab = objTabsColl[strTabName];
								if (objTab) {
									intObjectType = objTab.childType;
								}
								
								switch(intObjectType) {
									case AdminObjectType.otySkipRule:
										addNewSkipRule(this, rid, ind);
										break;
									default:
										addNewOption(this, rid, ind);
										break;
								}
								//JAPR
							}
						});
						
						objGrid.attachEvent("onEditCell", function(stage,id,index) {
							var intObjectType = this.getRowAttribute(id, "objectType");
							var blnForForm = (intObjectType == AdminObjectType.otySurvey);
							var strTabName = this.getRowAttribute(id, "tabName");
							var objField = getFieldDefinitionByIndex(strTabName, index, blnForForm);
							if (objField) {
								if (objField.readOnly) {
									//Desactiva la edición de la celda
									return false;
								}
							}
							
							//Habilita la edición de la celda
							return true;
						});
						
						objGrid.attachEvent("onRowCreated", function(rId,rObj,rXml) {
							this.bitValuesRendered += 1;
							if (this.entBox && this.bitValuesRendered >= this.bitValuesLength) {
								setTimeout(function() {
									$(objGrid.entBox).find('img').css('display', 'inline');
									//$(objGrid.entBox).find('img').css('width', '5px');
									//$(objGrid.entBox).find('img').css('height', '8px');
									$(objGrid.entBox).find('img').css('cursor', 'pointer');
									$(objGrid.entBox).find('img').parents('td').attr('align', 'center');
								}, 100);
							}
						});						
						
						objGrid.attachEvent("onSubGridCreated", function(subgrid, rId, rInd) {
							//Primero obtiene la definición de la estructura del grid de detalles mediante la combinación TabName + FieldIdx, ya que las colecciones son
							//las únicas que generan un subGrid
							var strTabName = this.bitTabName;
							if (!objTabsColl || !strTabName || !objTabsColl[strTabName]) {
								return false;
							}
							
							var objCollectionTab = objTabsColl[strTabName];
							if (!objCollectionTab.fields || !objCollectionTab.fields[rInd]) {
								return false;
							}
							
							var objGridField = objCollectionTab.fields[rInd];
							if (!objGridField.definition || !objTabsColl[objGridField.definition]) {
								return false;
							}
							var objDefinitionTab = objTabsColl[objGridField.definition];
							
							//Genera el grid utilizando la definición del tab de detalles
							var strGridStyle = "font-family:Roboto Regular;font-size:12px;"
							subgrid.setStyle(strGridStyle, strGridStyle);
							subgrid.setNoHeader(true);
							subgrid.bitCollectionTabName = objGridField.tab;
							doGenerateGrid(subgrid, objDefinitionTab, this.bitObjectType, this.bitObjectID, objCollectionTab.childType, rId);
							if (subgrid.entBox) {
								//$(objPropsTabs.cells("valuesTab").getAttachedObject().entBox).find('img').css('display', 'inline');
								//$(objPropsTabs.cells("valuesTab").getAttachedObject().entBox).find('img').parents('td').attr('align', 'center');
								//$(subgrid.entBox).find('img').parent('td').attr('align', 'center');
								setTimeout(function() {
									var intTop = parseInt($(subgrid.entBox).css('top').replace('px', ''));
									if (!$.isNumeric(intTop)) {
										intTop = 0;
									}
									
									//$(subgrid.entBox).css('top', (intTop - 5)+'px')
									//$(subgrid.entBox).css('overflow', 'visible');
									
									if (subgrid.hdrBox) {
										var intHeight = $(subgrid.hdrBox).find('div.xhdr').css('height');
										$(subgrid.hdrBox).find('div.xhdr').css('height', '26px');
										$(subgrid.hdrBox).find('div.hdrcell').css('line-height', '26px');
									}
								}, 100);
							}
							//return true;
						});
						break;
					
					default:
						//Cada row representa una propiedad que se puede configurar de manera diferente según el tipo de campo
						objGrid.setHeader("<?=translate("Property")?>,<?=translate("Value")?>", null, ["background-color:cccccc;","background-color:cccccc;"]);
						
						if(iObjectType==AdminObjectType.otyAppCustomization)
						{
							objGrid.setInitWidthsP("70,30");
						}
						else
						{
							objGrid.setInitWidthsP("30,70");	//50,50
						}
						
						//objGrid.setInitWidthsP("30,70");	//50,50
						objGrid.setColAlign("left,left");
						objGrid.setColTypes("ro,ed");
						//objGrid.setNumberFormat("0",1,".",",");
						objGrid.enableEditEvents(true);
						objGrid.enableMarkedCells();
						objGrid.bitObjectType = iObjectType;
						objGrid.bitObjectID = iObjectID;
						objGrid.bitChildObjectType = iChildObjectType;
						objGrid.bitChildObjectID = iChildObjectID;
						objGrid.bitTabName = objTab.id;
						
						objGrid.attachEvent("onCellChanged", function(rId, cInd, nValue) {
							console.log('onCellChanged rId == ' + rId + ', cInd == ' + cInd + ', nValue == ' + nValue);
							
							//JAPR 2015-06-18: Validado que al forzar el llenado manual de los componentes, no se lance el evento de onCellChanged
							//agregando una bandera temporal al grid
							if (this.bitDisableEvents) {
								return;
							}
							
							var varValue = nValue;
							var varCollValue = nValue;
							try {
								//El componente GFieldTypes.multiList se actualiza con este evento, pero sus valores no se deben grabar como String sino como un array de IDs, así que
								//primero hace la conversión basado en el array de opciones que presentó el componente mientras estaba visible
								var blnUpdate = false;
								var strTabName = this.getRowAttribute(rId, "tabName");
								var intObjectType = this.getRowAttribute(rId, "objectType");
								var blnForForm = (intObjectType == AdminObjectType.otySurvey);
								var objField = getFieldDefinition(strTabName, rId, blnForForm);
								if (objField) {
									switch (objField.type) {
										case GFieldTypes.comboEdit:
											//Si se trata de un combo editable, hay dos posibilidades, se está recibiendo un ID de elemento del options, en cuyo caso es numérico (aunque
											//no se debe basar en ese hecho, simplemente hay que buscar el Id en las opciones disponibles) o se trata de un texto personalizado, así que
											//identifica el caso para grabar sólo el texto personalizado, ya que la combo es Editable
											var cmbOptions = new Object();
											var cmbOptionsOrder = new Array();
											if (objField.getOptions) {
												var cmbOptions = objField.getOptions();
												var cmbOptionsOrder = objField._optionsOrder;
												var blnUseOrder = objField.useOrder;
											}
											
											for (var intIdx in cmbOptions) {
												if (intIdx == nValue) {
													varValue = cmbOptions[intIdx];
													break;
												}
											}
											blnUpdate = true;
											//@OMMC 2015/09/02: Agregado para evitar que se agregue &nbsp en campos numéricos al seleccionar formato vacío
											if(varValue == '&nbsp;'){
												varValue = "";
											}
											break;
									
										case GFieldTypes.multiList:
											var cmbOptions = new Object();
											var cmbOptionsOrder = new Array();
											if (objField.getOptions) {
												var cmbOptions = objField.getOptions();
												var cmbOptionsOrder = objField._optionsOrder;
												var blnUseOrder = objField.useOrder;
											}
											
											var arrShowQuestionIDs = new Object();
											if (nValue) {
												var arrSelection = nValue.split(",");
												if (arrSelection && $.isArray(arrSelection) && arrSelection.length) {
													//Debe buscar la descripción (que para este componente debería ser única) en el array de opciones del campo para almacenar
													//internamente la colección de IDs seleccionados
													for (var intSelIdx in arrSelection) {
														var strDescription = $.trim(arrSelection[intSelIdx]);
														//JAPR 2016-07-29: Corregido un bug, removidos los doble espacios convertidos a &nbsp; por DHTMLX (#W34WOC) (#QHYOU7)
														strDescription = html_entity_decode($.trim(strDescription)); //strDescription.replace(/&nbsp;/gi, ' ');
														//JAPR
														for (var intIdx in cmbOptions) {
															//JAPR 2016-11-15: Corregido un bug, para estandarizar se hace el mismo trim que con el texto a comparar (#W34WOC)
															var strOptionName = $.trim(cmbOptions[intIdx]);
															if (strDescription == strOptionName) {
																//Esta opción fue seleccionada, debe grabar internamente el ID
																var intQuestionID = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
																arrShowQuestionIDs[intQuestionID] = intQuestionID;
																break;
															}
														}
													}
												}
											}
											
											blnUpdate = true;
											//JAPR 2015-07-30: Corregido un bug, si no se selecciona algún elemento sino que se quiere remover la selección, envía un elemento inválido
											//para permitir que el request procese algo, ya que de lo contrario considera que no hay valor y lo ignora (#65Z4K7)
											if ($.isEmptyObject(arrShowQuestionIDs)) {
												arrShowQuestionIDs[-1] = -1;
											}
											//JAPR
											varValue = arrShowQuestionIDs;
											//JAPR 2016-07-29: Corregido un bug, removidos los doble espacios convertidos a &nbsp; por DHTMLX (#W34WOC) (#QHYOU7)
											varCollValue = html_entity_decode($.trim(varCollValue)); //varCollValue.replace(/&nbsp;/gi, ' ');
											//JAPR
											//this.setRowAttribute(rId, "value", arrShowQuestionIDs);
											break;
										case GFieldTypes.uploadImg:
											blnUpdate = false;
											break;
										default:
											blnUpdate = true;
											break;
									}
								}
								
								if (blnUpdate) {
									doUpdateProperty(this, iObjectType, iObjectID, rId, cInd, varValue);
								}
								
								//Independientemente de si se mandó o no a grabar la configuración, si esto fuera un subgrid, debe actualizar la columna correspondiente en caso de
								//tener un detalle desplegado también en ella
								var strCollectionTabName = this.bitCollectionTabName;
								if (objField && strCollectionTabName && this.bitChildObjectID) {
									var blnForForm = (objField.parentType == AdminObjectType.otySurvey);
									var objTabComponent = undefined;
									try {
										if (blnForForm) {
											var objTabComponent = objFormTabs.tabs(tabDetail).getAttachedObject();
										}
										else {
											var objTabComponent = objPropsTabs;
										}
										
										if (objTabComponent && objTabComponent.tabs && objTabComponent.tabs(strCollectionTabName)) {
											var objCollGrid = objTabComponent.tabs(strCollectionTabName).getAttachedObject();
											//Obtiene la referencia al grid y tab que contienen la definición, para determinar si hay o no un campo que sea equivalente al editado,
											//si existe entonces si actualiza el valor en la columna correspondiente
											var objDefinitionTab = objTabsColl[strCollectionTabName];
											if (objCollGrid && objDefinitionTab && objDefinitionTab.fields) {
												var objCollField = getFieldDefinition(strCollectionTabName, rId);
												if (objCollField && objCollField.columnNum !== undefined) {		//columnNum
													objCollGrid.cells(this.bitChildObjectID, parseInt(objCollField.columnNum) -1).setValue(varCollValue);
												}
											}
										}
									} catch (e) {
										debugger;
									}
								}
							} catch(e) {
								console.log ("Error updating property value (type=" + iObjectType + ", id=" + iObjectID + ", prop=" + rId + "):" + e);
								debugger;
							}
						});
						
						objGrid.attachEvent("onCellMarked", function(rid, ind) {
							this.bitLastSelectedRowId = rid;
							this.bitLastSelectedColId = ind;
							if (ind != 1) {
								this.mark(rid, ind, false);
								this.mark(rid, 1, true);
							}
						});
						
						objGrid.attachEvent("onEditCell", /*$.proxy(*/function(stage,id,index) {
							var ind = this.getRowIndex(id);	//this.objGrid == this with Proxy
							if (index != 1) {
								return true;
							}
							
							var intObjectType = this.getRowAttribute(id, "objectType");
							var blnForForm = (intObjectType == AdminObjectType.otySurvey);
							switch (this.getRowAttribute(id, "type")) {
								case GFieldTypes.multiList:
									var strTabName = this.getRowAttribute(id, "tabName");
									var objField = getFieldDefinition(strTabName, id, blnForForm);
									
									var cmbOptions = new Object();
									var cmbOptionsOrder = new Array();
									var objField = getFieldDefinition(strTabName, id, blnForForm);
									if (objField && objField.getOptions) {
										var cmbOptions = objField.getOptions();
										var cmbOptionsOrder = objField._optionsOrder;
										var blnUseOrder = objField.useOrder;
									}
									
									switch (stage) {
										case 0:	//Before start
											//Antes de iniciar, se debe llenar la columna con la lista de opciones corresponiente utilizando sólo las descripciones, ya que este
											//componente no soporta uso de ids
											this.registerCList(1, cmbOptions);
											break;
										
										case 1:
											//JAPR 2015-07-30: Agregados botones para seleccionar/deseleccionar todos los elementos
											$('.dhx_clist input:button').before('<img src="images/admin/selectall.jpg" style="width:22px; height:22px;" onclick="javascript:fnSelectAllCListItems($(this).parents(\'.dhx_clist\')[0], true);" />');
											$('.dhx_clist input:button').before('<img src="images/admin/unselect.jpg" style="width:22px; height:22px;" onclick="javascript:fnSelectAllCListItems($(this).parents(\'.dhx_clist\')[0], false);" />');
											//JAPR
											$('.dhx_clist input:button').attr('value', '<?=translate("Apply")?>')
											break;
										
										case 2:	//Editor closed
											//Al terminar la edición hay que actualizar el atributo con el nuevo valor seleccionado
											/*
											var strSelection = this.cells(id, 1).getValue();
											if (strSelection) {
												var arrShowQuestionIDs = new Object();
												var arrSelection = strSelection.split(",");
												if (arrSelection && $.isArray(arrSelection) && arrSelection.length) {
													//Debe buscar la descripción (que para este componente debería ser única) en el array de opciones del campo para almacenar
													//internamente la colección de IDs seleccionados
													for (var intSelIdx in arrSelection) {
														var strDescription = arrSelection[intSelIdx];
														for (var intIdx in cmbOptions) {
															var strOptionName = cmbOptions[intIdx];
															if (strDescription == strOptionName) {
																//Esta opción fue seleccionada, debe grabar internamente el ID
																var intQuestionID = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
																arrShowQuestionIDs[intQuestionID] = intQuestionID;
																break;
															}
														}
													}
												}
												else {
													this.cells(id, 1).setValue('');
													this.setRowAttribute(id, "value", arrShowQuestionIDs);
												}
											}
											*/
											break;
									}
									break;
							
								case GFieldTypes.alphanum:
									switch (stage) {
										case 1:	//The editor is opened
											//Cuando se muestra el componente para captura, verifica si hay un límite de caracteres y dinámicamente modifica el input
											var strTabName = this.getRowAttribute(id, "tabName");
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.maxLength > 0) {
												$(this.editor.obj).attr("maxlength", objField.maxLength);
											}
											break;
									}
									break;
								
								case GFieldTypes.comboEdit:
									//Se trata de un DHTMLCombo
									switch (stage) {
										case 0:	//Before start
											var objCombo = objGrid.cells(id, 1).getCellCombo()
											var strTabName = this.getRowAttribute(id, "tabName");
											
											var blnUseOrder = true;
											var cmbOptions = new Object();
											var cmbOptionsOrder = new Array();
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.getOptions) {
												var cmbOptions = objField.getOptions();
												var cmbOptionsOrder = objField._optionsOrder;
												var blnUseOrder = objField.useOrder;
											}
											
											objCombo.clearAll();
											var intSelectedIdx = -1;
											var strValue = this.getRowAttribute(id, "value");
											for (var intIdx in cmbOptionsOrder) {
												var idOption = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
												var strName = cmbOptions[idOption].name;
												if (strName === undefined) {
													strName = cmbOptions[idOption];
												}
												
												objCombo.addOption(idOption, strName);
												if (strName == strValue) {
													intSelectedIdx = intIdx;
												}
											}
											
											//Selecciona la opción del combo que corresponde con el valor asignado a esta propiedad, si no hay alguno, deja el texto de la propiedad
											if (intSelectedIdx >= 0) {
												objCombo.selectOption(intSelectedIdx);
											}
											else {
												objCombo.setComboText(strValue);
											}
											break;
											
										case 2:	//Editor closed
											//Al terminar la edición hay que actualizar el atributo con el nuevo valor seleccionado
											var strValue = this.cells(id, 1).getValue();
											
											//Si hubo un cambio, entonces esta combo habría regresado un Id, de lo contrario regresaría el texto de la celda del grid
											//así que se valida para que sólo los ids actualicen el valor real de la row
											var strTabName = this.getRowAttribute(id, "tabName");
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField) {
												var cmbOptions = new Object();
												var cmbOptionsOrder = new Array();
												if (objField.getOptions) {
													var cmbOptions = objField.getOptions();
													var cmbOptionsOrder = objField._optionsOrder;
													var blnUseOrder = objField.useOrder;
												}
												
												for (var intIdx in cmbOptions) {
													if (intIdx == strValue) {
														strValue = cmbOptions[intIdx];
														break;
													}
												}
											}
											
											this.setRowAttribute(id, "value", strValue);
											break;
									}
									break;
								
								case GFieldTypes.combo:
									switch (stage) {
										case 0:	//Before start
											//Al iniciar la edición, se debe modificar el contenido del combo a los valores válidos para esta Row, además se debe
											//asignar como valor de la row el Id en lugar del texto correspondiente para que la combo pueda seleccionar correctamente
											//su valor al abrir la lista
											var combo = this.getCombo(index);
											combo.clear();
											var strTabName = this.getRowAttribute(id, "tabName");
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.empty) {
												var idOption = 0;
												if (objField.emptyId !== undefined) {
													var idOption = objField.emptyId;
												}
												var strName = "(<?=translate("None")?>)";
												combo.put(idOption, strName);
											}
											
											var blnUseOrder = true;
											var cmbOptions = new Object();
											var cmbOptionsOrder = new Array();
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.getOptions) {
												var cmbOptions = objField.getOptions();
												var cmbOptionsOrder = objField._optionsOrder;
												var blnUseOrder = objField.useOrder;
											}
											
											if (objField && objField.parent) {
												var intParentValue = this.getRowAttribute(objField.parent, "value");
												var objParentField = getFieldDefinition(strTabName, objField.parent, blnForForm);
												if (objParentField && objParentField.options) {
													if (!$.isNumeric(intParentValue)) {
														if (objParentField.default !== undefined) {
															intParentValue = objParentField.default;
														}
													}
													var objParentOptions = objParentField.options[intParentValue];
													if (objParentOptions && objParentOptions.children) {
														cmbOptions = objParentOptions.children;
														//Si hay una configuración de orden, se utiliza para agregar los elementos en dicha secuencia, de lo contrario se usa el orden recibido
														var cmbOptionsOrder = objParentOptions.childrenOrder;
														if (cmbOptionsOrder) {
															blnUseOrder = true;
														}
														else {
															blnUseOrder = false;
															cmbOptionsOrder = cmbOptions;
														}
													}
												}
											}
											
											for (var intIdx in cmbOptionsOrder) {
												var idOption = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
												var strName = cmbOptions[idOption].name;
												if (strName === undefined) {
													strName = cmbOptions[idOption];
												}
												combo.put(idOption, strName);
											}
											
											var intValue = this.getRowAttribute(id, "value");
											if (!$.isNumeric(intValue)) {
												if (objField && objField.default !== undefined) {
													intValue = objField.default;
												}
											}
											
											if ($.isNumeric(intValue)) {
												//JAPR 2015-06-18: Validado que al forzar el llenado manual de los componentes, no se lance el evento de onCellChanged
												//agregando una bandera temporal al grid
												//En este caso se está llenando así debido a como funciona la combo, pero realmente no se está asignando un valor
												this.bitDisableEvents = true;
												this.cells(id, 1).setValue(intValue.toString());
												this.bitDisableEvents = false;
											}
											break;
										
										case 2:	//Editor closed
											//Al terminar la edición hay que actualizar el atributo con el nuevo valor seleccionado
											var intValue = this.cells(id, 1).getValue();
											if ($.isNumeric(intValue)) {
												//Si hubo un cambio, entonces esta combo habría regresado un Id, de lo contrario regresaría el texto de la celda del grid
												//así que se valida para que sólo los datos numéricos actualicen el valor real de la row (incluso un 0 es válido como ID,
												//así que no deberían agregarse posibles textos con descripción numérica para no causar problemas)
												this.setRowAttribute(id, "value", intValue);
												
												//Se deben aplicar las reglas de visibilidad de la opción de respuesta seleccionada, ocultando todos los campos/tabs que otras
												//opciones debían mostrar, y mostrando sólo los que está configurada para ver
												var strTabName = this.getRowAttribute(id, "tabName");
												var objField = getFieldDefinition(strTabName, id, blnForForm);
												//Invoca al proceso que aplica la visbilidad de las opciones del campo
												objField.applyVisibilityRules();
												if (objField.setValuesFn) {
													objField.setValuesFn();
												}
											}
											break;
									}
									break;
							}
							
							//Return true permite la edición, false la cancela
							return true;
						}/*, {objGrid:objGrid})*/);
						break;
				}
				
				objGrid.init();
				
				//Dependiendo del tipo de grid, configura las rows
				switch (objTab.type) {
					case propTabType.collection:
						$(objGrid.hdr).find('td div').css('padding-left', '0px');
						
						//Se debe generar el Grid sólo como una colección de valores con un mínimo de propiedades id y Name, siendo el id el rowId y el Name la columna default
						//var objValues = new Object();
						//var objValuesOrder = new Array();
						//if (objTab.getValues) {
						if (objValues && objValuesOrder && objValuesOrder.length) {
							//var objValues = objTab.getValues();
							//var objValuesOrder = objTab._valuesOrder;
							var blnUseOrder = objTab.useOrder;
							for (var intIdx in objValuesOrder) {
								var intValueID = (blnUseOrder)?objValuesOrder[intIdx]:intIdx;
								var objValue = objValues[intValueID];
								if (objValue) {
									var rowValues = new Array();
									for (var iField in objTab.fields) {
										var aField = objTab.fields[iField];
										if (aField) {
											var varValue = aField.getValue(undefined, objTab.childType, intValueID);
											if (varValue === undefined) {
												varValue = '';
											}
											rowValues.push(varValue);
										}
									}
									
									objGrid.bitDisableEvents = true;
									objGrid.addRow(intValueID, rowValues);
									objGrid.setRowAttribute(intValueID, "tabName", objTab.id);
									//objGrid.setRowAttribute(intValueID, "fieldName", intValueID);
									//objGrid.setRowAttribute(intValueID, "options", this.options);
									//objGrid.setRowAttribute(intValueID, "type", this.type);
									objGrid.setRowAttribute(intValueID, "objectType", objTab.childType);
									//objGrid.setRowAttribute(intValueID, "value", this.getValue(true));
									//if (this.hidden) {
									//	objGrid.setRowHidden(intValueID, true);
									//}
									//objGrid.setCellExcellType(intValueID, 1, this.type);
									objGrid.bitDisableEvents = false;
								}
							}
						}
						
						//Al final agrega la row para crear un nuevo elemento
						var strNewItemID = "new";
						var rowValues = new Array();
						rowValues.push("images/admin/new.gif");
						rowValues.push("(<?=translate("Add")?>)");
						rowValues.push("");
						objGrid.addRow(strNewItemID, rowValues);
						objGrid.setCellExcellType(strNewItemID, 0, GFieldTypes.image);
						objGrid.setCellExcellType(strNewItemID, 2, GFieldTypes.readOnly);
						objGrid.setRowTextStyle(strNewItemID, "cursor:pointer;");
						objGrid.cells(strNewItemID, 2).setValue("");
						break;
					
					default:
						//Cada row representa una propiedad que se puede configurar de manera diferente según el tipo de campo
						//objGeneralGrid.setColSorting("int,str,str,int");
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							aField.draw(objGrid, iChildObjectType, iChildObjectID);
							//Asigna la visibilidad por default del campo
							if (objTab.defaultVisibility && !objTab.defaultVisibility[aField.name]) {
								objGrid.setRowHidden(aField.name, true);
							}
						}
						
						//Al terminar de generar el grid, aplica la visibilidad encadenada de los componentes que la tengan definida y cuya visibilidad default sea visible
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							//Asigna la visibilidad por default del campo
							if (objTab.defaultVisibility && objTab.defaultVisibility[aField.name]) {
								aField.applyVisibilityRules();
							}
						}
						break;
				}
			}
			
			/* Realiza el cambio de propiedades del objeto en el Previw según el elemento actualmente seleccionado
			*/
			function doUpdateProperty(oGrid, iObjectType, iObjectID, rId, cInd, nValue) {
				console.log('doUpdateProperty iObjectType == ' + iObjectType + ', iObjectID == ' + iObjectID + ', rId == ' + rId + ', cInd == ' + cInd + ', nValue == ' + nValue);
				
				if (!oGrid) {
					return;
				}
				
				if (iObjectType === undefined) {
					iObjectType = oGrid.bitObjectType;
				}
				if (iObjectID === undefined) {
					iObjectID = oGrid.bitObjectID;
				}
				if (!iObjectType || !iObjectID) {
					return;
				}
				//JAPR 2015-06-29: Agregado el subgrid de propiedades de los valores
				var intChildObjectType = oGrid.bitChildObjectType;
				var intChildObjectID = oGrid.bitChildObjectID;
				
				//Verifica que realmente hubiera un cambio en el valor de esta propiedad antes de realizar el proceso
				var varOldValue = oGrid.getRowAttribute(rId, "value");
				if (JSON.stringify(varOldValue) == JSON.stringify(nValue)) {
					return;
				}
				
				oGrid.setRowAttribute(rId, "value", nValue);
				var objObject = getObject(iObjectType, iObjectID, intChildObjectType, intChildObjectID);
				if (objObject) {
					objObject[rId] = nValue;
				}
				
				objCallbackFn=undefined;
				//Refrescar el menú principal donde están los enlazadores
				if (iObjectType == AdminObjectType.otyLink) {
					console.log('Asignar función');
					objCallbackFn = function() {
						parent.parent.refreshDataViewMenu(objLink);
					}
				}
				
				//RV Nov2016: Cuando desde la pantalla de configuraciones se cambia el tipo de despliegue es necesario actualizar la variable global en JS que lo almacena
				//para que los enlaces se abran de acuerdo al último valor asignado sin necesidad de refrescar el frame de menús.
				if (iObjectType == AdminObjectType.otySetting && rId=="LINKSDISPLAYMODE") {
					parent.parent.window.defLinksDisplayMode=parseInt(nValue);
				}
				//JAPR
				//Si está activado el grabado automático durante la edición, se invoca al mismo en este punto sólo con la propiedad modificada
				if (!gbFullSave && objObject && objObject.save) {
					var objParams = {};
					objParams[rId] = 1;
					setTimeout(objObject.save(objParams,objCallbackFn), 100);
				}
			}
			
			/* Realiza el cambio de posición del objeto indicado diretamente en el server
			*/
			function doReorderObject(iObjectType, iObjectID, iNewPos) {
				console.log('doUpdateProperty iObjectType == ' + iObjectType + ', iObjectID == ' + iObjectID + ', iNewPos == ' + iNewPos);
				
				if (!iObjectType || !iObjectID) {
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					return;
				}
				
				var intOldPos = 0;
				var objObject = getObject(iObjectType, iObjectID);
				if (objObject) {
					intOldPos = objObject["number"];
					objObject["number"] = iNewPos;
				}
				
				//Si se trata de una pregunta, se sobreescribe la función que cancela el Drop para evitar que el iFrame del preview tenga uqe hacerlo
				if (iObjectType == AdminObjectType.otyQuestion) {
					doCancelDragDropOperation = function() {
						doRestoreDroppedItem(AdminObjectType.otyQuestion, iObjectID, intOldPos);
					};
				}
				
				//Si está activado el grabado automático durante la edición, se invoca al mismo en este punto sólo con la propiedad modificada
				if (!gbFullSave && objObject && objObject.save) {
					var objParams = {};
					objParams["number"] = 1;
					setTimeout(objObject.reorder(objParams), 100);
				}
			}
			
			
			/* Esta función pinta todas las rows de los campos definidos en aTabsColl
			//JAPR 2015-06-18: Agregados los parámetros iObjectType, iObjectID para identificar el objeto actualmente desplegado en las propiedades
			//JAPR 2015-06-20: Agregado el parámetro con el contenedor del grid de propiedades
			*/
			function generateFormDetail(aTabsColl, iObjectType, iObjectID, oContainer) {
				console.log('generateFormDetail');
				
				//JAPR 2015-06-20: Agregado el parámetro con el contenedor del grid de propiedades
				if (oContainer === undefined) {
					oContainer = objPropsLayout.cells(dhxPropsDetailCell);
				}
				oContainer.detachObject(true);
				
				//Si el contenedor tiene una propiedad cell, debe ser una celda de Layout o Tabbar (en ese caso es directamente el div con el contenido)
				//o algún componente que funciona de manera similar, en cualquier caso se le aplica el estilo de visibilidad a dicho div, ya que debido a 
				//que el Tabbar principal se creó con un estilo para ocultarlo, y como toda la ventana se construye a partir del mismo div base (excepto 
				//el preview que existe en un iframe), todos los tabbars internos se ven afectados por el estilo del div base, por lo que hay que cambiar 
				//el estilo a visible para los que si se desean con la parte de Tabs en pantalla, esto se hace sobre la celda que contendrá al tabbar y 
				//antes de pintarlo
				if (oContainer.cell) {
					$(oContainer.cell).addClass("visibleTabs");
				}
				objPropsTabs = oContainer.attachTabbar();
				//JAPR
				
				//Se agregá para configurar que las flechas del Tabbar se desplieguen sólo cuando es necesario, ya que anteriormente siempre se desplegaban 
				//aunque fueran pocos tabs y al dar click sobre ellas no se realizaba ninguna acción.
				objPropsTabs.setArrowsMode("auto");
				
				for (var strTabName in aTabsColl) {
					var objTab = aTabsColl[strTabName];
					var blnActive = (objTab.activeOnLoad == true);
					objPropsTabs.addTab(strTabName, objTab.name, null, null, blnActive);
					//Asigna la visibilidad por default de la cejilla
					//JAPR 2015-06-29: Modificada la visibilidad para usar el parámetro "self" para referirse a la propia tab y permitir ocultar campos de la misma
					if (!objTab.defaultVisibility || objTab.defaultVisibility["self"] === false) {
						objPropsTabs.tabs(strTabName).hide();
					}
					
					//Generar todos los fields del tab
					var objGrid = objPropsTabs.tabs(strTabName).attachGrid('gridbox');
					var strGridStyle = "font-family:Roboto Regular;font-size:13px;"
					objGrid.setStyle(strGridStyle, strGridStyle);
					doGenerateGrid(objGrid, objTab, iObjectType, iObjectID);
				}
			}
			
			/* Permite capturar un texto simple pero abrir un editor de fórmulas si se desea un texto mas complejo con ayuda de dicho editor
			*/
			function eXcell_edFormula(cell) {
				if (cell) {
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function() {
					this.val = this.getValue();
					var strRowId = this.cell.parentNode.idd;
					this.cell.innerHTML = "<img src='images/admin/editFormula.gif' style='width:18px; height:18px; float:left; cursor:pointer;'>"
						+ "<input autocomplete='off' class='dhx_combo_edit' style='height: 28px;'>";
					this.cell.childNodes[1].value = this.val;
					var objCell = this.cell;
					this.cell.childNodes[0].onclick = function(e) {
						fnOpenFormulaEditor(objCell, objCell.childNodes[1].value);
						(e||event).cancelBubble = true;	//blocks onclick event
					}
					this.cell.childNodes[1].onclick = function(e) {(e||event).cancelBubble = true;} //blocks onclick event
				}
				this.setValue = function(val) {
					//JAPR 2015-11-12: Corregido un bug, este campo no contiene HTML así que debe usarse la versión de la función que sólo asigna texto (#WN65X7)
					//this.setCValue(val);
					this.setCTxtValue(val);
					//JAPR
				}
				this.getValue = function() {
                    //JAPR 2015-11-12: Corregido un bug, este campo no contiene HTML así que debe usarse la versión de la función que sólo asigna texto (#WN65X7)
                    return this.cell.innerText;
                    //return this.cell.innerHTML;
                    //JAPR
				}
				this.detach = function() {
					this.setValue(this.cell.childNodes[1].value); //sets the new value
					return this.val != this.getValue(); // compares the new and the old values
				}
			}
			eXcell_edFormula.prototype = new eXcell;    // nests all other methods from base class
			
			/* Permite definir celdas de tipo DHTMLXEditor en el grid
			*/
			function eXcell_editor(cell) { //the eXcell name is defined here
				if (cell) {                // the default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function(){}  //read-only cell doesn't have edit method
				this.isDisabled = function(){ return false; } // the cell is read-only, so it's always in the disabled state
				this.setValue = function(val) {
					var strRowId = this.cell.parentNode.idd;
					var strId = "cellEditor_"+strRowId+'_'+this.cell.cellIndex;
					this.setCValue("<div id='"+strId+"' class='editor_compact' style='height:100px;width:100%;overflow:visible;'>"+val+"</div>",val);
					var myEditor = new dhtmlXEditor({parent:strId, toolbar:true, iconsPath:"images/editor"});
					myEditor.setContent(val);
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var objField = getFieldDefinition(tabName, strRowId);					
					changeEditor(myEditor, objField.customButtoms);
					
					var objGrid = this.grid;
					myEditor.attachEvent("onAccess", function(eventName, evObj){
						if (eventName == "blur") {
							//Al perder el foco debe actualizar el valor en el atributo de la row
							if (objGrid) {
								doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, this.getContent());
							}
						}
					});
					
					if (this.grid) {
						this.grid.setUserData(strRowId, "editor", myEditor);
						//JAPRWarning
						//myEditor.tb es la DHTMLX Toolbar que se puede personalizar en este punto, obtener de this.grid.getUserData/getRowAttribute los botones personalizados
						//y la configuración de botones a ocultar como uyn array que debe modificat myEditor.tb
					}
				}
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var strValue = "";
					if (this.grid) {
						myEditor = this.grid.getUserData(strRowId, "editor");
						strValue = myEditor.getContent();
					}
					return strValue;
				}
			}
			eXcell_editor.prototype = new eXcell;// nests all other methods from the base class
			
			function eXcell_editorHTML(cell) { //the eXcell name is defined here
				if (cell) {                // the default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function(){
					this.val = this.getValue();
					var strValue = this.val;
					var strRowId = this.cell.parentNode.idd;
					var strId = "editorHTML_"+strRowId+'_'+this.cell.cellIndex;
					//this.setCValue("<div id='"+strId+"' style='height:150px;width:100%;overflow:visible;'><textarea id='txt"+strId+"' name='txt"+strId+"'>"+val+"</textarea></div>",val);
					this.cell.innerHTML = "<div id='"+strId+"' style='height:150px;width:100%;overflow:visible;'><textarea id='txt"+strId+"' name='txt"+strId+"'></textarea></div>";
					
					var objGrid = this.grid;
					var objEditorHTML = undefined;
					$('#txt'+strId).redactor({
						focus: true,
						lang: 'es',
						//Definición de la altura del textarea
						minHeight: 150,
						maxHeight: 150,
						//Botones que se van a diujar
						buttons: ['html', 'bold', 'italic', 'image', 'file'],
						
						//Otros botones que se pueden dibujar
						//'formatting', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'alignment', 'horizontalrule'
						//Botones que se pueden ocultar
						//buttonsHide: ['formatting', 'deleted', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'alignment', 'horizontalrule'],
						
						//Plugins de subrayar, subir imágenes, tamaño de letra, tipo de leta y color de letra
						plugins:[
							'underline',
							'imagemanager',
							'fontsize',
							'fontfamily',
							'fontcolor'
						],
						//Incluir la ruta del archivo encargado de subir las imágenes al servidor
						imageUpload: 'images/image.php',
						
						initCallback: function()
						{
							objEditorHTML = this;
							this.code.set(strValue);
						},
						//JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
						/* Atrapa los errores de upload de imágenes, el parámetro contendrá la descripción del error en la propiedad message */
						imageUploadErrorCallback: function(e) {
							if ( e.error ) {
								alert(e.message || '');
							}
						},
						/*,
						blurCallback: function(e)
						{
							//alert('EVENTO BLUR:'+this.code.get());
							//alert("BLUR objGrid:"+objGrid);
							if (objGrid) {
								//alert("this.code.get:"+this.code.get());
								doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, this.code.get());
							}
						}*/
					});
					
					$('#'+strId).on('click', function(e) {
						debugger;
						(e||event).cancelBubble = true;
					})
					//this.cell.childNodes[1].onclick = function(e) {(e||event).cancelBubble = true;} //blocks onclick e
					
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var objField = getFieldDefinition(tabName, strRowId);
					objEditorHTML.bitFieldName = strRowId;
					objEditorHTML.bitEditorDivID = strId;
					
					if (this.grid) {
						this.grid.setUserData(strRowId, "editor", objEditorHTML);
					}
				}  //read-only cell doesn't have edit method
				//this.isDisabled = function(){ return false; } // the cell is read-only, so it's always in the disabled state
				this.setValue = function(val) {
					if (val === null) {
						return;
					}
					//debugger;
					console.log ("editor.setValue " + val);
					
					this.setCValue(val);                                     
				}
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var strValue = "";
					if (this.grid) {
						myEditor = this.grid.getUserData(strRowId, "editor");
						if (myEditor && myEditor.code) {
							strValue = myEditor.code.get();
						}
					}
					
					return strValue;
				}
				this.detach = function() {
					var strRowId = this.cell.parentNode.idd;
					if (this.grid) {
						myEditor = this.grid.getUserData(strRowId, "editor");
						strValue = myEditor.code.get();
					}
					
					this.setValue(strValue); //sets the new value
					return this.val != this.getValue(); // compares the new and the old values
				}
			}
			eXcell_editorHTML.prototype = new eXcell;// nests all other methods from the base class
			
			function eXcell_editorHTMLDialog(cell) {
				if (cell) {
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function() {
					this.val = this.getValue();
					var strRowId = this.cell.parentNode.idd;
					this.cell.innerHTML = "<img src='images/admin/editorhtml.gif' style='width:18px; height:18px; float:left; cursor:pointer;'>"
						+ "<input autocomplete='off' class='dhx_combo_edit' style='height: 28px;'>";
					//alert(this.cell.innerHTML);
					this.cell.childNodes[1].value = this.val;
					var objCell = this.cell;
					//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
					/*
					var intDataSourceID = 0;
					var intObjectType = this.grid.bitObjectType;
					var intObjectID = this.grid.bitObjectID;
					var objObject = getObject(intObjectType, intObjectID);
					
					if (objObject && objObject.dataSourceID && (
							(objObject.objectType == AdminObjectType.otyQuestion && objObject.type == Type.simplechoice && objObject.displayMode == DisplayMode.dspGetData) || 
							(objObject.objectType == AdminObjectType.otySection && objObject.valuesSourceType == TypeOfFill.tofCatalog)
							)) {
						intDataSourceID = objObject.dataSourceID;
					}
					*/
					//JAPR
					
					this.cell.childNodes[0].onclick = function(e) {
						var aElement = getObject(objCell.parentNode.grid.bitObjectType, objCell.parentNode.grid.bitObjectID);
						//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
						//fnOpenFormulaEditor(objCell, objCell.childNodes[1].value, intDataSourceID);
						fnOpenEditorHTML(objCell, objCell.childNodes[1].value, aElement.typeExt);
						(e||event).cancelBubble = true;	//blocks onclick event
					}
					//Agrega el evento blur para actualizar el valor al perder el foco en el input
					//OMMC 2015-10-05: Se agrega validación para prevenir que pregunta HTML quede vacía.
					var objGrid = this.grid;
					var blnRequired = false;
					intObjectType = objGrid.bitObjectType;
					var tabName = objGrid.getRowAttribute(strRowId, "tabName");
					var blnForForm = (intObjectType == AdminObjectType.otySurvey);					
					var objField = getFieldDefinition(tabName, strRowId,blnForForm);
					if (objField) {
						blnRequired = objField.required;
					}
					//Prepara el evento para recibir el <enter>, ya que internamente la celda manda al mismo evento editStop ya sea con <enter> (aceptar la captura) o <escape> (cancelar
					//la captura) y no da indicios de por cual de los dos exactamente se lanzó, así que se invocará al detach de la celda y ambos cancelarían el onblur, por lo que hay que
					//detectar desde el propio input cual de las teclas provocó el editStop y prevenir que en caso de <enter> se descarte el contenido
					//OMMC 2015-10-07 Cambio del del evento del <enter> ahora si se presiona en la celda del grid y el valor está vació se llenará con un valor default.
					this.cell.childNodes[1].onkeydown = function(e) {
						if (e && e.keyCode == 13) {
						if($.trim(this.value) == '' && blnRequired == true){
								var strContent = this.value;
								strContent = "<?=translate("You must enter a value for this field")?>";
								this.value = strContent;
								alert(strContent);
								$(this).focus();
								return;
							}
							//this.bitPreventDisableBlur = true;
						}
					}
					this.cell.childNodes[1].onblur = function(e) {
						//El evento se controlará con este atributo, ya que la manera en que funciona el Grid con una celda editable (como este componente) es que lanza el evento editStop
						//el cual invoca a la función detach y ella sobreescribe la celda provocando el onBlur, para posteriormente hacer el setValue con el valor anterior a lo que se
						//estaba editando, pero para ese punto ya el onblur habría enviado el valor cancelado al server para grabar y visualmente se habría quedado el valor original,
						//así que el evento detach va a generar el atributo de esta validación en true para evitar el onblur en ese punto
						if (!this.bitDisableBlur || this.bitPreventDisableBlur) {
							//JAPR 2015-09-01: Validado que se capture algo en el editor antes de perder el foco si el campo está marcado como requerido
							var strContent = this.value;
							//OMMC 2015-10-02: Validado para que los encabezados y pies de sección puedan ser nulos y no marque error (ISSUE: FZUWI5)
							if ($.trim(strContent) == '' && blnRequired == true) {
								var strContent = "<?=translate("You must enter a value for this field")?>";
								this.value = strContent;
								$(this).focus();
								alert(strContent);
								return;
							}
							//JAPR
							
							doUpdateProperty(objGrid, objGrid.bitObjectType, objGrid.bitObjectID, strRowId, 1, this.value);
						}
					}
					this.cell.childNodes[1].onclick = function(e) {(e||event).cancelBubble = true;} //blocks onclick event
				}
				this.setValue = function(val) {
					if (val === null) {
						return;
					}
					
					console.log ("edFormula.setValue " + val);
					this.setCValue(val);                                     
				}
				this.getValue = function() {
					return this.cell.innerHTML;
				}
				this.detach = function() {
					if (this.cell && this.cell.childNodes && this.cell.childNodes[1]) {
						this.cell.childNodes[1].bitDisableBlur = true;
					}
					
					this.setValue(this.cell.childNodes[1].value); //sets the new value
					return this.val != this.getValue(); // compares the new and the old values
				}
			}
			eXcell_editorHTMLDialog.prototype = new eXcell;    // nests all other methods from base class
			
			<?
			$pos = strpos($_SESSION["PAHTTP_REFERER"],"build_page.php");
			$pathImg = substr($_SESSION["PAHTTP_REFERER"],0,$pos);
			?>
			//Permite definir celdas para poder subir imágenes al servidor
			function eXcell_uploadImg(cell) { //the eXcell name is defined here
				if (cell) {                // the default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function(){}  //read-only cell doesn't have edit method
				this.isDisabled = function(){ return false; } // the cell is read-only, so it's always in the disabled state
				this.setValue = function(val) {
					var strRowId = this.cell.parentNode.idd;
					var strId = "cellUploadImg_"+strRowId+'_'+this.cell.cellIndex;
					//JAPR 2016-06-07: Corregida la asignación para no considerar undefined (#DEWE31)
					if (val!="" && val!=undefined) {
						srcImg='<?=$pathImg?>'+val;
					}
					else {
						srcImg='<?=$pathImg?>images/admin/emptyimg.png';
					}
					var intObjectType = this.grid.getRowAttribute(strRowId, "objectType");
					var intObjectID = this.grid.bitObjectID;
					var intChildObjectType = this.grid.bitChildObjectType;
					var intChildObjectID = this.grid.bitChildObjectID;
					var blnForForm = (intObjectType == AdminObjectType.otySurvey);
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var strColTabName = this.grid.bitCollectionTabName;
					var objField = getFieldDefinition(tabName, strRowId,blnForForm);
					var fieldHTML='<div id="divFile'+strId+'" style="border:none">'+
						'<form name="frmUploadFile'+strId+'" method="POST" action="uploadUserImage.php?objectType='+objField.parentType+'&strId='+strId+'&tabName='+tabName+'&strRowId='+strRowId+'&objectID='+intObjectID+'&childType='+intChildObjectType+'&childID='+intChildObjectID+'&collTabName='+strColTabName+'" enctype="multipart/form-data" style="display:inline" target="frameUploadFile">'+
						'<input type="file" id="file'+strId+'" name="file'+strId+'" value="" style="opacity:0;width:0px;height:0px;" onchange="document.getElementById(\'txtsubmit'+strId+'\').click();">'+
						'<button type="button" onclick="document.getElementById(\'file'+strId+'\').click();"><?=translate("Select file")?></button>'+
						'<input type="submit" id ="txtsubmit'+strId+'" name="txtsubmit'+strId+'" value="<?=translate("Upload file")?>"style="display: none; visibility: hidden;">'+'<img name="deleteImg" id="deleteImg" src="images/admin/delete.png" style="display:inline" onClick=\'removeImg("'+strId+'")\'>'+
						'<div>'+
						'<img name ="img'+strId+'" id="img'+strId+'" src="'+srcImg+'">'+
						'</div>'+
						'</form>'+
						'</div>'+
						'<form id="frmDeleteFile'+strId+'" name="frmDeleteFile'+strId+'" method="POST" action="uploadUserImage.php?action=delete&objectType='+objField.parentType+'&strId='+strId+'&tabName='+tabName+'&strRowId='+strRowId+'&objectID='+intObjectID+'&childType='+intChildObjectType+'&childID='+intChildObjectID+'&collTabName='+strColTabName+'" enctype="multipart/form-data" style="display:inline" target="frameUploadFile">'+
						'</form>'
					this.setCValue(fieldHTML);
					
					var objGrid = this.grid;
				}
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var objField = getFieldDefinition(tabName, strRowId);
					var strValue = "";
					if (objField)
					{
						strValue = objField.getValue();
					}
					return strValue;
				}
			}
			
			function removeImg(strId)
			{
				//Forma para borrar imagen
				document.forms['frmDeleteFile'+strId].submit();
			}
			eXcell_uploadImg.prototype = new eXcell;// nests all other methods from the base class
			
			function eXcell_colorSelector(cell) { //the eXcell name is defined here
				if (cell) {                // the default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function(){}  //read-only cell doesn't have edit method
				this.isDisabled = function(){ return false; } // the cell is read-only, so it's always in the disabled state
				this.setValue = function(val) {
					//alert("setValue"+val);
					var strRowId = this.cell.parentNode.idd;
					var strId = "color_"+strRowId+'_'+this.cell.cellIndex;
					var inputName = strRowId; 
					/*
					if (val!="") {
						srcImg='<?=$pathImg?>'+val;
					}
					else {
						srcImg='<?=$pathImg?>images/admin/emptyimg.png';
					}
					*/
					var intObjectType = this.grid.getRowAttribute(strRowId, "objectType");
					var intObjectID = this.grid.bitObjectID;
					var intChildObjectType = this.grid.bitChildObjectType;
					var intChildObjectID = this.grid.bitChildObjectID;
					var blnForForm = (intObjectType == AdminObjectType.otySurvey);
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var strColTabName = this.grid.bitCollectionTabName;
					var objField = getFieldDefinition(tabName, strRowId,blnForForm);
					var styleInput='style="background-color:#'+val+'"';
					var fieldHTML='<div id="div'+strId+'" style="border:none">'+
							'<input id="input' + strId + '" name="' + inputName + '" type="text" '+styleInput+' value="#'+val+'">'+
							'<div id="d' + strId + '"></div>'+
						'</div>'
					this.setCValue(fieldHTML);
					/*
					myCP = new dhtmlXColorPicker([
					{input: "button1", target_color: "cv1", target_value: "inp1", color: "#05ff50"},
					*/
					myCP = dhtmlXColorPicker(["input" + strId]);
					//myCP.setColor("#"+val);
					
					var objGrid = this.grid;
					myCP.attachEvent("onSelect", function(color,node){
						strColor=color.replace("#", "");
						//alert("onSelect color:"+color+"\r\n"+"strColor: "+strColor);
						doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, strColor);
						//Asignarle el valor sin el caracter #
						node.value=strColor;
						BITAMEFormsAppCustomizationExtended_applyPreview(node.name);
					});
					/*
					myCP.attachEvent("onAccess", function(eventName, evObj){
						if (eventName == "blur") {
							//Al perder el foco debe actualizar el valor en el atributo de la row
							if (objGrid) {
								var arrayColors = this.getSelectedColor();
								alert("onAccess arrayColors[0]: "+arrayColors[0]);
								doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrayColors[0]);
							}
						}
					});
					*/
					if (this.grid) {
						this.grid.setUserData(strRowId, "colorPicker", myCP);
					}
					
				}
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var strValue = "";
					if (this.grid) {
						myColorPicker = this.grid.getUserData(strRowId, "colorPicker");
						var arrayColors = myColorPicker.getSelectedColor();
						strValue = arrayColors[0];
						//alert("getValue arrayColors[0]: "+arrayColors[0]);
					}
					return strValue;
				}
				/*
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var objField = getFieldDefinition(tabName, strRowId);
					var strValue = "";
					if (objField)
					{
						strValue = objField.getValue();
					}
					alert("getValue:"+strValue);
					return strValue;
				}
				*/
			}
			eXcell_colorSelector.prototype = new eXcell;// nests all other methods from the base class
			
			//JAPR 2015-07-15: Agregado el componente con la lista seleccionable ordenada
			/* Componente con dos listas, una de opciones disponibles y otra de opciones seleccionadas, esta última permite definir un orden en los elementos seleccionados mediante
			Drag & Drop de las opciones
			*/
			function eXcell_selList(cell) { //the eXcell name is defined here
				if (cell) {                // the default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.detach = function() {
					//this.setCValue("");
				}
				this.edit = function() {}  //read-only cell doesn't have edit method
				this.isDisabled = function(){ return false; } // the cell is read-only, so it's always in the disabled state
				this.setValue = function(val) {
					var strRowId = this.cell.parentNode.idd;
					var strId = "cellSelList_"+strRowId+'_'+this.cell.cellIndex;
					this.setCValue("<div id='"+strId+"' style='height:220px;width:100%;overflow:visible;'>"+val+"</div>",val);
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var objField = getFieldDefinition(tabName, strRowId);					
					var objGrid = this.grid;
					
					var objFormData = [
						{type:"container", name:strId+"grdAvail", inputWidth:230, inputHeight:200, className:"selection_container"},
						{type:"newcolumn", offset:20},
						{type:"block", blockOffset:0, offsetTop:20, list:[
							{type:"button", name:strId+"btnAssoc", value:"", className:"assoc_button"},
							{type:"button", name:strId+"btnRemove", value:"", className:"remove_button"}
						]},
						{type:"newcolumn", offset:20},
						{type:"container", name:strId+"grdAssoc", inputWidth:230, inputHeight:200, className:"selection_container"},
						{type:"input", name:"SurveyID", value:intSurveyID, hidden:true}
					];
					
					objSelForm = new dhtmlXForm(strId, objFormData);
					$(objSelForm.cont).addClass('customGrid')
					
					//Contenido de la lista de usuarios disponibles
					var objAvailGrid = new dhtmlXGridObject(objSelForm.getContainer(strId+"grdAvail"));
					objAvailGrid.setImagePath("<?=$strScriptPath?>/images/");
					objAvailGrid.setHeader("<?=translate("Available")?>");
					objAvailGrid.setInitWidthsP("100");
					objAvailGrid.setColAlign("left");
					objAvailGrid.setColTypes("ro");
					objAvailGrid.enableEditEvents(false);
					objAvailGrid.enableDragAndDrop(true);
					objAvailGrid.setColSorting("str");
					objAvailGrid.enableMultiselect(true);
					objAvailGrid.init();
					objAvailGrid.sortRows(0,"str","asc");
					
					//Contenido de la lista de usuarios invitados
					var objAssocGrid = new dhtmlXGridObject(objSelForm.getContainer(strId+"grdAssoc"));
					objAssocGrid.setImagePath("<?=$strScriptPath?>/images/");
					objAssocGrid.setHeader("<?=translate("Used")?>");
					objAssocGrid.setInitWidthsP("100");
					objAssocGrid.setColAlign("left");
					objAssocGrid.setColTypes("ro");
					objAssocGrid.enableEditEvents(false);
					objAssocGrid.enableDragAndDrop(true);
					objAssocGrid.setColSorting("str");
					objAssocGrid.enableMultiselect(true);
					objAssocGrid.init();
					objAssocGrid.sortRows(0,"str","asc");
					
					//Llena la lista con las opciones disponibles (options) y las seleccionadas (value)
					var blnUseOrder = true;
					var cmbOptions = new Object();
					var cmbOptionsOrder = new Array();
					var arrSelected = new Array();
					if (objField && objField.getOptions) {
						var cmbOptions = objField.getOptions();
						var cmbOptionsOrder = objField._optionsOrder;
						var blnUseOrder = objField.useOrder;
						arrSelected = objField.getValue();
					}
					
					//Primero llena la lista de opciones disponibles
					for (var intIdx in cmbOptionsOrder) {
						var idOption = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
						//Debe excluir a las que ya se encuentren seleccionadas
						if (arrSelected.indexOf(idOption) == -1) {
							var strName = cmbOptions[idOption].name;
							if (strName === undefined) {
								strName = cmbOptions[idOption];
							}
							
							var strItemId = "i" + idOption;
							objAvailGrid.addRow(strItemId, strName);
							objAvailGrid.setUserData(strItemId, "id", idOption);
						}
					}
					
					//Posteriormente llena las opciones seleccionadas, las cuales deben respetar el orden de selección del usuario
					//Se valida en caso de que los options no contengan a todos los elementos seleccionados, lo cual puede suceder si se cambia al padre teniendo ya configurado todo
					if ($.isArray(arrSelected)) {
						for (var intIdx in arrSelected) {
							var idOption = arrSelected[intIdx];
							if (!idOption) {
								continue;
							}
							
							if (!cmbOptions[idOption]) {
								continue;
							}
							
							var strName = cmbOptions[idOption].name;
							if (strName === undefined) {
								strName = cmbOptions[idOption];
							}
							
							var strItemId = "i" + idOption;
							objAssocGrid.addRow(strItemId, strName);
							objAssocGrid.setUserData(strItemId, "id", idOption);
						}
					}
					
					objAvailGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
						console.log('objAvailGrid.onDrop Property');
						
						if (sObj == tObj) {
							return;
						}
						
						var arrIDs = doGetGridSelectedIDs(objAssocGrid);
						doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
					});
					
					objAvailGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
						this.moveRow(rId, "row_sibling", undefined, objAssocGrid);
						
						var arrIDs = doGetGridSelectedIDs(objAssocGrid);
						doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
					});
					
					objAssocGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
						console.log('objAssocGrid.onDrop Property');
						
						//JAPR 2015-07-18: Corregido un bug, el Drop en el mismo grid de asociación si se permite para cambiar el orden de los elementos
						//if (sObj == tObj) {
						//	return;
						//}
						
						var arrIDs = doGetGridSelectedIDs(this);
						doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
					});
					
					objAssocGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
						this.moveRow(rId, "row_sibling", undefined, objAvailGrid);
						
						var arrIDs = doGetGridSelectedIDs(this);
						doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
					});
					
					objSelForm.attachEvent("onButtonClick", function(name) {
						switch (name) {
							case strId+"btnAssoc":
								var strSelection = objAvailGrid.getSelectedRowId();
								if (strSelection) {
									var arrSelection = strSelection.split(",");
									if (arrSelection.length) {
										var blnMove = false;
										for (var intIdx in arrSelection) {
											var strItemId = arrSelection[intIdx];
											if (strItemId) {
												blnMove = true;
												objAvailGrid.moveRow(strItemId, "row_sibling", undefined, objAssocGrid);
											}
										}
										
										if (blnMove) {
											var arrIDs = doGetGridSelectedIDs(objAssocGrid);
											doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
										}
									}
								}
								break;
							
							case strId+"btnRemove":
								var strSelection = objAssocGrid.getSelectedRowId();
								if (strSelection) {
									var arrSelection = strSelection.split(",");
									if (arrSelection.length) {
										var blnMove = false;
										for (var intIdx in arrSelection) {
											var strItemId = arrSelection[intIdx];
											if (strItemId) {
												blnMove = true;
												objAssocGrid.moveRow(strItemId, "row_sibling", undefined, objAvailGrid);
											}
										}
										
										if (blnMove) {
											var arrIDs = doGetGridSelectedIDs(objAssocGrid);
											doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
										}
									}
								}
								break;
						}
					});
					
					if (this.grid) {
						this.grid.setUserData(strRowId, "selGrid", objAssocGrid);
					}
				}
				
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var strValue = "";
					if (this.grid) {
						var oGrid = this.grid.getUserData(strRowId, "selGrid");
						if (oGrid) {
							var tabName = this.grid.getRowAttribute(strRowId, "tabName");
							var objField = getFieldDefinition(tabName, strRowId);					
							if (objField) {
								var intRows = oGrid.getRowsNum();
								var blnUseOrder = true;
								var cmbOptions = new Object();
								var cmbOptionsOrder = new Array();
								if (objField && objField.getOptions) {
									var cmbOptions = objField.getOptions();
									var cmbOptionsOrder = objField._optionsOrder;
									var blnUseOrder = objField.useOrder;
								}
								
								strAnd = "";
								for (var intIndex = 0; intIndex < intRows; intIndex++) {
									var strRowId = oGrid.getRowId(intIndex);
									if (strRowId) {
										var intObjectID = oGrid.getUserData(strRowId, "id");
										if (intObjectID && cmbOptions[intObjectID]) {
											var strName = cmbOptions[intObjectID].name;
											if (strName === undefined) {
												strName = cmbOptions[intObjectID];
											}
											
											strValue += strAnd + strName;
											strAnd = ", ";
										}
									}
								}
							}
						}
					}
					
					return strValue;
				}
			}
			eXcell_selList.prototype = new eXcell;// nests all other methods from the base class
			
			//JAPR 2015-07-19: Agregados los parámetros iObjectID, iChildObjectType, iChildObjectID, sCollectionName para identificar el elemento hijo si se tratara de un subgrid,
			//ya que en las opciones de respuesta el tabName representa un detalle pero no directo de objPropsTabs, así que se debe navegar un nivel mas abajo para obtener la referencia
			//al grid correcto, además tabName representa a un tab oculto que sólo se usa como definición de tantos tabs detalle como rows tenga el sCollectionName, que es desde donde
			//se obtendrá la verdadera referencia al SubGrid donde se modificó la propiedad. Esto es así exclusivamente cuando hay subGrids de collecciones (hasta un nivel) y porque este
			//componente tiene que invocar a Javascript desde una respuesta del server, si hubiera podido invocarlo en el cliente directamente, se hubiera pasado la referencia al grid
			//en lugra de hacer todo esto
			//En otras propiedades que son directas de un grid que no es de colección, basta con enviar los parámetros hasta iObjectType y el grid recuperado será el adecuado
			
			//Corrección Ticket #QND4OY: Sólo se aceptan imagenes JPG, GIF y PNG
			var errorMsg;
			errorMsg="";
			var blnError;
			blnError=false;
			
			function setUserImageForObject(strId, tabName, pathImg, strRowId, iObjectType, iObjectID)
			{
			   <?
				$pos = strpos($_SESSION["PAHTTP_REFERER"],"build_page.php");
				$pathImg = substr($_SESSION["PAHTTP_REFERER"],0,$pos);
			   ?>
			   
				if (blnError) {
					if(errorMsg!="")
					{
						alert(errorMsg);
						errorMsg="";
					}
					else
					{
						alert("<?=translate("There was an error uploading the image file, please contact Technical Support")?>: \n" + pathImg);
					}
					//OMMC 2015-11-23: Limpiar el valor del input file ya que prevenía que se subiera la misma imagen 2 veces de manera sucesiva.
					//O que si se borraba la imagen y se deseaba subir una con el mismo nombre, no se podía.
					if($('#file'+strId).val() != ""){
						$('#file'+strId).val("");
					}
					blnError=false;
					return;
				}
			   //Ticket #IYR03T: Se agregó un parámetro extra de fecha para que no tome la imagen de caché
				//ya que si se subía una imagen con el mismo nombre de la anterior pero diferente contenido no la actualizaba
				$('img[name="img' + strId + '"]').attr('src', '<?=$pathImg?>'+pathImg+ "?dte=" + getFullDate(undefined, true));
				//OMMC 2015-11-23: Limpiar el valor del input file ya que prevenía que se subiera la misma imagen 2 veces de manera sucesiva.
				//O que si se borraba la imagen y se deseaba subir una con el mismo nombre, no se podía.
				if($('#file'+strId).val() != ""){
					$('#file'+strId).val("");
				}
				propName=strRowId;
				if (objFormTabs) {
					var objGrid = objFormTabs.tabs(tabDetail).getAttachedObject().tabs(tabName).getAttachedObject();
				}
				else {
					var objGrid = objPropsTabs.tabs(tabName).getAttachedObject();
				}
				
				var blnForForm = (this.parentType == AdminObjectType.otySurvey);
				var objField = getFieldDefinition(tabName, propName,blnForForm);
				if (objField) {
					objField.setValue(null, null, pathImg);
					doUpdateProperty(objGrid, objField.parentType, objField.id, strRowId, 1, pathImg);
				}
			}
			
			/* Permite capturar una hora */
			function eXcell_time(cell){                        //excell name is defined here
				if (cell) {                                       //default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.setValue = function(val) {
					this.setCValue(val);                                     
				}
				this.getValue = function() {
					return this.cell.innerHTML; // get value
				}
				this.edit = function() {
					if (this.cell.innerText == '')
					{
						return;
					}
					this.val = this.getValue(); //save current value
					var strCellHTML = "";
					//GCRUZ 2016-02-23. Pendiente modificar de manera correcta los select para que el objeto funcione de manera genérica y no solo para la configuración de hora de detinos incrementales
					strCellHTML = "<select id='idSettingHour' style='width:50px;' onchange='updateHourTime()'>";
					for (var i=0; i<24; i++)
					{
						var strHour = "";
						if (i < 10)
						{
							strHour = "0"+i;
						}
						else
						{
							strHour = i;
						}
						strCellHTML += "<option value='"+strHour+"'>"+strHour;
					}
					//GCRUZ 2016-02-23. Pendiente modificar de manera correcta los select para que el objeto funcione de manera genérica y no solo para la configuración de hora de detinos incrementales
					strCellHTML += "</select><select id='idSettingMinute' style='width:50px;' onchange='updateHourTime()'>"
					for (var i=0; i<60; i++)
					{
						var strMins = "";
						if (i < 10)
						{
							strMins = "0"+i;
						}
						else
						{
							strMins = i;
						}
						strCellHTML += "<option value='"+strMins+"'>"+strMins;
					}
					strCellHTML += "</select>"; // editor's html
					this.cell.innerHTML = strCellHTML;
					var strTime = '0000'+parseInt('0'+this.val);
					strTime = strTime.slice(-4);
					this.cell.firstChild.value=strTime.slice(0,2); //set the hour
					this.cell.childNodes[1].value=strTime.slice(2); //set the minute
					//if (this.val.indexOf("PM")!=-1) this.cell.childNodes[1].value="PM";
					this.cell.childNodes[0].onclick=function(e){ (e||event).cancelBubble=true;} //blocks onclick event
					this.cell.childNodes[1].onclick=function(e){ (e||event).cancelBubble=true;} //blocks onclick event
				}
				this.detach = function() {
					//debugger;
					//Only apply if the user press ENTER
					if (event.keyCode == 13)
					{
						this.setValue(this.cell.childNodes[0].value+this.cell.childNodes[1].value); //sets the new value
						return this.val!=this.getValue(); // compares the new and the old values
					}
					else
					{
						return false;
					}
				}
			}
			eXcell_time.prototype = new eXcell;    // nests all other methods from base class

			//GCRUZ 2016-02-23. Pendiente modificar de manera correcta los select para que el objeto funcione de manera genérica y no solo para la configuración de hora de detinos incrementales
			function updateHourTime()
			{
				//debugger;
				var SettingsGrid = objPropsTabs.tabs('generalTab').getAttachedObject();
				var SettingHour = document.getElementById('idSettingHour');
				var SettingMinute = document.getElementById('idSettingMinute');
				objPropsTabs.tabs('generalTab').getAttachedObject().cells('AUTOINCREMENTALGENERATIONTIME', 1).setValue(SettingHour.value+SettingMinute.value)
				//doUpdateProperty(SettingsGrid, 12, 1, 'AUTOINCREMENTALGENERATIONTIME', 1, SettingHour.value+SettingMinute.value);
			}

			function doChangeTab(sTabId) {
				console.log('doChangeTab ' + sTabId);
				
				var objEvent = this.event || window.event;
				if (objEvent) {
					var objTarget = objEvent.target || objEvent.srcElement;
					if (objTarget) {
						$("#divDesignHeader td").removeClass("linktdSelected");
						$(objTarget).addClass("linktdSelected");
					}
				}
				if (!sTabId || !objFormTabs || !objFormTabs.tabs(sTabId)) {
					return;
				}

				if( sTabId == 'model' )
				{
					/*require(['main.php?BITAM_SECTION=eFormsModelsCollection&ext=js'], function( DHTMLX ) {

						DHTMLX( objFormTabs.tabs(sTabId).cell );

					});*/

					require(['views/models/model'], function( Page ) {

						new Page().setElement( objFormTabs.tabs(sTabId).cell ).render();

					});
				}
				
				objFormTabs.tabs(sTabId).setActive();
			}
			
			//Descarga la instancia de la forma de la memoria
			function doUnloadDialog() {
				console.log('doUnloadDialog');
				if (!objWindows || !objDialog) {
					return;
				}
				
				var objForm = objDialog.getAttachedObject();
				//JAPR 2015-10-13: Agregado el cierre de los posibles PopUps que pudiera contener la forma 
				if (objForm) {
					objForm.forEachItem(function (name) {
						var strType = objForm.getItemType(name);
						if (strType == "editor") {
							var objEditor = objForm.getEditor(name);
							if (objEditor && objEditor.tb && objEditor.tb.objPopUpsColl) {
								for (var strPopUpName in objEditor.tb.objPopUpsColl) {
									var objPopUp = objEditor.tb.objPopUpsColl[strPopUpName];
									if (objPopUp) {
										if (objPopUp.bitEventHandler) {
											objPopUp.detachEvent(objPopUp.bitEventHandler);
										}
										objPopUp.hide();
									}				
								}
							}
						}
					});
				}
				//JAPR
				
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
				
				if (objWindows.unload) {
					objWindows.unload();
					objWindows = null;
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				objDialog = null;
			}
			
			function doUnloadDialogFn() {
				console.log('doUnloadDialogFn');
				if (!objWindowsFn || !objDialogFn) {
					return;
				}
				
				var objForm = objDialogFn.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
				
				if (objWindowsFn.unload) {
					objWindowsFn.unload();
					objWindowsFn = null;
					objWindowsFn = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				objDialogFn = null;
			}
			
			function doUnloadDialogEd() {
				console.log('doUnloadDialogEd');
				if (!objWindowsEd || !objDialogEd) {
					return;
				}
				
				var objForm = objDialogEd.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
				
				if (objWindowsEd.unload) {
					objWindowsEd.unload();
					objWindowsEd = null;
					objWindowsEd = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				objDialogEd = null;
			}
			
			/* Agrega la clase correspondiente a cada objeto descargado de la definición de la forma */
			function addClassPrototype() {
				if (!selSurvey) {
					return;
				}
				
				$.extend(selSurvey, new SurveyCls());
				if (selSurvey.sections) {
					for (var intSectionID in selSurvey.sections) {
						$.extend(selSurvey.sections[intSectionID], new SectionCls());
						var objSection = selSurvey.sections[intSectionID];
						if (objSection.questions) {
							for (var intQuestionID in objSection.questions) {
								$.extend(objSection.questions[intQuestionID], new QuestionCls());
								//Agrega una nueva colección de options indexadas por el consecutiveID, ya que el preview las requiere indexadas por el name
								var objQuestion = objSection.questions[intQuestionID];
								if (objQuestion) {
									objQuestion.optionsByID = new Object();
									objQuestion.optionsOrderByID = new Array();
									for (var intOptionNum in objQuestion.optionsOrder) {
										var strOptionName = objQuestion.optionsOrder[intOptionNum];
										$.extend(objQuestion.options[strOptionName], new OptionCls());
										var objOption = objQuestion.options[strOptionName];
										if (objOption) {
											objQuestion.optionsByID[objOption.id] = objOption;
											objQuestion.optionsOrderByID.push(objOption.id);
										}
									}
									
									//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
									for (var intSkipRuleID in objQuestion.skipRules) {
										$.extend(objQuestion.skipRules[intSkipRuleID], new SkipRuleCls());
									}
									//JAPR
									
									//Sobreescribe la instancia de selSurvey para mantener una referencia y permitir modificar sólo en un lugar
									if (selSurvey.questions && selSurvey.questions[objQuestion.id]) {
										selSurvey.questions[objQuestion.id] = objQuestion;
									}
								}
							}
						}
					}
				}
			}
			
			/* Procesa la respuesta de un request de actualización de datos asíncrona y presenta el error en pantalla en caso de existir alguno
			Esta función está pensada como un callback de un request Ajax que regresará un contenido en JSon con infomación adicional de error, sin embargo se puede recibir
			un callback adicional en el parámetro oCallbackFn
			El parámetro bRemoveProgress se usa generalmente sólo en requests Ajax y permite restablecer la ventana de diseño en caso de error, removiendo la progres bar del TabBar
			*/
			function doSaveConfirmation(loader, oCallbackFn, bRemoveProgress) {
				console.log('doSaveConfirmation');
				
				$('#divSaving').hide();
				
				if (!loader || !loader.xmlDoc) {
					if (bRemoveProgress) {
						objFormTabs.tabs(tabDesign).progressOff();
					}
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					if (bRemoveProgress) {
						objFormTabs.tabs(tabDesign).progressOff();
					}
					return;
				}
				
				//Si llega a este punto es que se recibió una respuesta correctamente
				var response = loader.xmlDoc.responseText;
				try {
					var objResponse = JSON.parse(response);
				} catch(e) {
					alert(e + "\r\n" + response.substr(0, 1000));
					if (bRemoveProgress) {
						objFormTabs.tabs(tabDesign).progressOff();
					}
					return;
				}
				
				if (objResponse.error) {
					alert(objResponse.error.desc);
					if (bRemoveProgress) {
						objFormTabs.tabs(tabDesign).progressOff();
					}
					return;
				}
				else {
					//JAPR 2015-10-25: Agregado el proceso de confirmación de borrado cuando existen referencias
					if (objResponse.confirm && objResponse.confirm.desc) {
						//Si se pidió confirmación, pide al usuario autorización y si se acepta, repite el proceso de borrado pero esta vez ya sin validaciones
						var answer = confirm(objResponse.confirm.desc);
						if (answer) {
							if (goObjectToDelete) {
								goObjectToDelete.SkipValidation = 1;
							}
							
							setTimeout(function() {
								doDeleteObject(goObjectToDelete.objectType, goObjectToDelete.objectID, goObjectToDelete.selObjectType, goObjectToDelete.selObjectID, goObjectToDelete.requestType, goObjectToDelete.callbackFn, goObjectToDelete.removeProcess);
							}, 100);
							return;
						}
						
						//En este caso simplemente quita la barra de progreso para que todo regrese a la normalidad y ya no continua con el proceso
						if (bRemoveProgress) {
							objFormTabs.tabs(tabDesign).progressOff();
						}
						return;
					}
					else {
						if (objResponse.warning) {
							console.log(objResponse.warning.desc);
						}
					}
					//JAPR
				}
				
				//La respuesta no tiene error, debió haber terminado de grabar correctamente
				//JAPR 2015-07-16: Agregado el grabado de nuevas opciones de respuesta
				//Si el server respondió que se agregó un nuevo objeto, invoca al método que continua con el proceso de carga (por ahora sólo aplica para opciones de respuesta)
				//El valor respondido es el ID de la Row donde se debe actualizar la opción de respuesta
				//JAPRDescontinuada: Finalmente se cambió el proceso para que abriera una nueva ventana para crear las opciones de respuesta, así que esto ya no es necesario,
				//lo hubiera sido si en doCreateNewOption se invocara a un GenericItemCls.send o save que terminara en este proceso de manera automática, pero como se abre la nueva
				//ventana con una forma, ya no es necesario este proceso
				/*
				if (objResponse.newObjectRowID && objResponse.objectType) {
					switch (objResponse.objectType) {
						case AdminObjectType.otyOption:
							doUpdateNewOption(objResponse);
							break;
						default:
							if (bRemoveProgress) {
								objFormTabs.tabs(tabDesign).progressOff();
							}
							break;
					}
					return;
				}
				*/
				
				//JAPR 2015-07-17: Agregado el borrado de objetos directamente desde Ajax, los cuales no regresan código desde el server sino sólo una referencia para que se
				//pueda ejecutar un código de callback
				if (objResponse.objectType) {
					if (objResponse.deleted) {
						if (oCallbackFn && typeof oCallbackFn == "function") {
							oCallbackFn(objResponse);
						}
						else {
							if (bRemoveProgress) {
								objFormTabs.tabs(tabDesign).progressOff();
							}
						}
					}
					return;
				}
				else {
					if (objResponse.newObject) {
						if (oCallbackFn && typeof oCallbackFn == "function") {
							oCallbackFn(objResponse.newObject);
						}
					}
				}
				/*
				console.log('************antes de if de AdminObjectType.otyLink');
				if(AdminObjectType.otyLink)
				{	console.log('AdminObjectType.otyLink');
					debugger;
					if (oCallbackFn && typeof oCallbackFn == "function") {
						console.log('parent.oCallbackFn');
						parent.oCallbackFn(objResponse);
					}
				}
				*/
			}
			
			/* Manda los datos al server para ser procesados
			El parámetro oSuccess permite especificar la función que se debe invocar al terminar el proceso de grabado en caso de éxito
			*/
			function doSendData(oSuccess) {
				console.log('doSendData');
				if (!objDialog) {
					return;
				}
				
				objDialog.progressOn();
				var objForm = objDialog.getAttachedObject();
				var objData = objForm.getFormData();
				objForm.lock();
				objForm.send("processRequest.php", "post", function(loader, response) {
					if (!loader || !response) {
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					if (loader.xmlDoc && loader.xmlDoc.status != 200) {
						alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					setTimeout(function () {
						try {
							var objResponse = JSON.parse(response);
						} catch(e) {
							alert(e + "\r\n" + response.substr(0, 1000));
							objDialog.progressOff();
							objForm.unlock();
							return;
						}
						
						if (objResponse.error) {
							alert(objResponse.error.desc);
							objDialog.progressOff();
							objForm.unlock();
							return;
						}
						else {
							if (objResponse.warning) {
								alert(objResponse.warning.desc);
							}
						}
						
						//Sólo si todo sale Ok cierra el diálogo y continua cargando el diseño de la forma recien creada
						doUnloadDialog();
						if (oSuccess) {
							try {
								//Se enviará la respuesta obtenida, ya que ella traería la nueva definición del objeto a utilizar
								oSuccess(objResponse);
							} catch(e) {
								console.log("Error executing doSendData callback: " + e);
							}
						}
					}, 100);
				});
			}
			
			/* Imprime un objeto de una forma mas legible que Json en la consola. Si se indica el parámetro bIsFormObject, se asumirá que se trata de una
			colección de objetos genéricos que representan a una forma, así que se les dará nombres según el tipo y sólo se asumirá de un nivel la colección
			*/
			function PrintObject(oCollection, sTabSpcs, bIsFormObject) {
				if (!oCollection) {
					console.log('Empty');
					return;
				}
				if (!sTabSpcs) {
					sTabSpcs = '';
				}
				
				for (var strKey in oCollection) {
					var objObject = oCollection[strKey];
					if (bIsFormObject) {
						var intObjectType = objObject.type;
						var intObjectID = objObject.id;
						var strTypeStr = '';
						switch (intObjectType) {
							case AdminObjectType.otySurvey:
								strTypeStr = 'Survey';
								break;
							case AdminObjectType.otySection:
								strTypeStr = 'Section';
								break;
							case AdminObjectType.otyQuestion:
								strTypeStr = 'Question';
								break;
							case AdminObjectType.otyOption:
								var intParentType = objObject.parentType;
								strTypeStr = 'Option';
								switch (intParentType) {
									case AdminObjectType.otySection:
										strTypeStr = 'Section ' + strTypeStr;
										break;
									case AdminObjectType.otyQuestion:
									strTypeStr = 'Question ' + strTypeStr;
										break;
								}
								break;
							case AdminObjectType.otySurveyFilter:
								strTypeStr = 'Survey Filter';
								break;
							case AdminObjectType.otyQuestionFilter:
								strTypeStr = 'Question Filter';
								break;
							case AdminObjectType.otyShowQuestion:
								strTypeStr = 'Show Question';
								break;
						}
						
						console.log(strTypeStr + ': ' + intObjectID);
					}
					else {
						if (typeof objObject == "object") {
							console.log(sTabSpcs + strKey);
							PrintObject(objObject, sTabSpcs + '   ', bIsFormObject);
						}
						else {
							console.log(sTabSpcs + strKey + ' = ' + objObject);
						}
					}
				}
			}
			function changeEditor(objEditor, customButtoms)
			{
				objEditor.tb.hideItem("applyH1");
				objEditor.tb.hideItem("applyH1");
				objEditor.tb.hideItem("applyH2");
				objEditor.tb.hideItem("applyH3");
				objEditor.tb.hideItem("applyH4");
				objEditor.tb.hideItem("separ01");
				objEditor.tb.hideItem("applyStrikethrough");
				objEditor.tb.hideItem("alignLeft");
				objEditor.tb.hideItem("alignCenter");
				objEditor.tb.hideItem("alignRight");
				objEditor.tb.hideItem("alignJustify");
				objEditor.tb.hideItem("separ03");
				objEditor.tb.hideItem("applySub");
				objEditor.tb.hideItem("applySuper");
				objEditor.tb.hideItem("separ04");
				objEditor.tb.hideItem("createNumList");
				objEditor.tb.hideItem("createBulList");
				objEditor.tb.hideItem("separ05");
				objEditor.tb.hideItem("increaseIndent");
				objEditor.tb.hideItem("decreaseIndent");
				objEditor.tb.hideItem("separ06");
				objEditor.tb.hideItem("clearFormatting");
				var buttonPos = objEditor.tb.getPosition("clearFormatting");
				buttonPos++;
				var myCP;
				objEditor.tb.objPopUpsColl = new Object();
				for (var strCustomButton in customButtoms)
				{	
					var objEditorButton=customButtoms[strCustomButton];
					
					switch (strCustomButton)
					{	
						case "fontName":
						{
							//Agrega la combo con los tipos de letra
							//objEditor.tb.addText("lblFontName", 40, "Font:");
							objEditor.tb.addText("fontName", 100, "");
							var objComboDiv = objEditor.tb.objPull[objEditor.tb.idPrefix+"fontName"].obj;
							objComboDiv.style.margin = '2px';
							objEditor.tb.objPull[objEditor.tb.idPrefix+"fontName"].obj.innerHTML = "";
							var objCombo = new dhtmlXCombo(objComboDiv, "cbFontName", 140);
							objCombo.addOption("Arial", "Arial", "font-family:Arial");
							objCombo.addOption("Georgia", "Georgia", "font-family:Georgia");
							objCombo.addOption("Palatino Linotype", "Palatino Linotype", "font-family:Palatino Linotype");
							objCombo.addOption("Times New Roman", "Times New Roman", "font-family:Times New Roman");
							objCombo.addOption("Verdana", "Verdana", "font-family:Verdana");
							objCombo.selectOption(0);
							objCombo.attachEvent("onChange", function() {
								objEditor._runCommand("fontName", objCombo.getSelectedText());
							});

							break;
						}
						case "fontSize":
						{
							objEditor.tb.addText("fontSize", 100, "");
							var objComboDivFS = objEditor.tb.objPull[objEditor.tb.idPrefix+"fontSize"].obj;
							objComboDivFS.style.margin = '2px';
							objEditor.tb.objPull[objEditor.tb.idPrefix+"fontSize"].obj.innerHTML = "";
							var objComboFS = new dhtmlXCombo(objComboDivFS, "cbFontSize", 40);
							objComboFS.addOption("1", "1");
							objComboFS.addOption("2", "2");
							objComboFS.addOption("3", "3");
							objComboFS.addOption("4", "4");
							objComboFS.addOption("5", "5");
							objComboFS.addOption("6", "6");
							objComboFS.addOption("7", "7");
							objComboFS.selectOption(0);
							objComboFS.attachEvent("onChange", function() {
							objEditor._runCommand("fontSize", objComboFS.getSelectedText());
							});
							break;
						}
						default:
						{
							switch (objEditorButton.type)
							{
								case "buttonColor":
								{
									objEditor.tb.addButton(strCustomButton, buttonPos, "", objEditorButton.img, objEditorButton.img);
									buttonPos++;
									var objColorPopUp = new dhtmlXPopup({ 
										toolbar: objEditor.tb,
										id: strCustomButton
									});
									objColorPopUp.bitButtonId = strCustomButton;
									objEditor.tb.objPopUpsColl[strCustomButton] = objColorPopUp;
									
									
									objColorPopUp.attachEvent("onShow", function() {
										var objPopUp = objEditor.tb.objPopUpsColl[this.bitButtonId];
										myCP = objPopUp.attachColorPicker({
											color: "#2a87eb",
											custom_colors: ["#ff9f29","#a3ff2b","#57cdff","#fb26ff","#9f96ff"]
										});
										myCP.attachEvent("onCancel",function(color){
											// do not allow to hide colorpicker, hide popup instead
											objPopUp.hide();
											return false;
										});
										myCP.attachEvent("onSelect",function(color){
											objEditor._runCommand(objPopUp.bitButtonId, color);
											objPopUp.hide();
										});
										
										//Traduce los textos del colorPicker
										setTimeout(function() {
											try {
												var objLabels = $('.dhxcp_inputs_cont td.dhxcp_label_hsl');
												if (objLabels && objLabels.length) {
													for (var intCont = 0; intCont < objLabels.length; intCont++) {
														var strLabel = "";
														switch (intCont % 3) {
															case 0:
																strLabel = "<?=translate("Hue")?>";
																break;
															case 1:
																strLabel = "<?=translate("Sat")?>";
																break;
															case 2:
																strLabel = "<?=translate("Lum")?>";
																break;
														}
														
														if (objLabels[intCont]) {
															objLabels[intCont].innerText = strLabel;
														}
													}
												}
												
												var objLabels = $('.dhxcp_inputs_cont td.dhxcp_label_rgb');
												if (objLabels && objLabels.length) {
													for (var intCont = 0; intCont < objLabels.length; intCont++) {
														var strLabel = "";
														switch (intCont % 3) {
															case 0:
																strLabel = "<?=translate("Red")?>";
																break;
															case 1:
																strLabel = "<?=translate("Green")?>";
																break;
															case 2:
																strLabel = "<?=translate("Blue")?>";
																break;
														}
														
														if (objLabels[intCont]) {
															objLabels[intCont].innerText = strLabel;
														}
													}
												}
												
												var objLabels = $('.dhxcp_g_memory_area button div.dhxcp_label_bm');
												if (objLabels && objLabels.length) {
													for (var intCont = 0; intCont < objLabels.length; intCont++) {
														objLabels[intCont].innerText = "<?=translate("Save the color")?>";
													}
												}
												
												var objLabels = $('.dhxcp_buttons_area button.dhx_button_save');
												if (objLabels && objLabels.length) {
													for (var intCont = 0; intCont < objLabels.length; intCont++) {
														objLabels[intCont].innerText = "<?=translate("Select")?>";
													}
												}
												
												var objLabels = $('.dhxcp_buttons_area button.dhx_button_cancel');
												if (objLabels && objLabels.length) {
													for (var intCont = 0; intCont < objLabels.length; intCont++) {
														objLabels[intCont].innerText = "<?=translate("Cancel")?>";
													}
												}
											} catch(e) {
												debugger;
											}
										}, 100);
									});
									break;
								}
								case "input":
								{
									break;
								}
								default:
								{
									break;
								}
							}
							break;
						}
					}
				}
			}
		</script>
		</head>
<?
