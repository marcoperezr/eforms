<?php

require_once('surveyMenu.inc.php');

class BITAMSurveyMenuExt extends BITAMSurveyMenu
{
	use BITAMObjectExt;

	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		//@JAPR 2015-04-06: Redise�ado el Admin con DHTMLX
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
	}
	
	//@JAPR 2015-04-06: Redise�ado el Admin con DHTMLX
	function hideBackButton($aUser) {	
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SurveyMenuExt";
		}
		else
		{
			return "BITAM_PAGE=SurveyMenuExt&MenuID=".$this->MenuID;
		}
	}
	
	//@JAPR 2015-06-10: Agregada la propiedad para obtener la imagen a usar en la vista tipo Metro
	function get_Image() {
		$strImage = $this->MenuImageText;
		//@JAPR 2015-07-27: Corregida la ruta de las imagenes, ahora ya no es un Tag IMG as� que se le debe agregar manualmente (#7TONG4)
		if ($strImage) {
			$strImage = "<img src=\"{$strImage}\" />";
		} else {
			$strImage = "<img src=\"images/admin/taskgroup.png\" />";
		}
		
		return $strImage;
	}
	
	function generateForm($aUser = null) {
		global $gbDesignMode;
		$gbDesignMode = true;
	
		//Contiene lo necesario para el manejo del grid
		require_once("genericgrid.inc.php");
		$strDivSaving = "<div id='divSaving' class='dhx_cell_progress_img' style='display:none;background-size:20px 20px;'>&nbsp;</div>";

?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
		<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
		<link href="css/font/roboto/robotofont.css" rel="stylesheet" type="text/css">
		<script src="js/codebase/dhtmlxFmtd.js"></script>
      	<script type="text/javascript" src="js/json2.min.js"></script>
		<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
		<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
		<script type="text/javascript" data-main="js/_requireconfig.js" src="js/libs/require/require.js"></script>
		<style>
			/* Estilos para ocultar el icono de las ventanas modales (todas las skins utilizadas) */
			.dhxwins_vp_dhx_skyblue div.dhxwin_hdr div.dhxwin_icon.without_icon {
				display: none;
			}
			.dhxwins_vp_dhx_web div.dhxwin_hdr div.dhxwin_icon.without_icon {
				display: none;
			}
			/* Estilo para los botones de b�squeda asociados a grids */
			.search_button input {
				border:1px solid #e1e1e1 !important;
			}
			.selection_container div.dhxform_container {
				border:1px solid #e1e1e1 !important;
				//height:90% !important;
				////overflow-y:auto;
			}
			.selection_container div.objbox {
				//overflow-y:auto;
			}
			.selection_list fieldset.dhxform_fs {
				border:1px solid #e1e1e1 !important;
				//height:95% !important;
			}
			div.dhxform_item_label_left.assoc_button div.dhxform_btn_txt {
				background-image:url(images/add_field.png);
				background-repeat:no-repeat;
				background-position: 0px 3px;
				padding-left: 22px;
				margin: 0px 15px 0px 12px;
			}
			div.dhxform_item_label_left.remove_button div.dhxform_btn_txt {
				background-image:url(images/del_field.png);
				background-repeat:no-repeat;
				background-position: 0px 3px;
				padding-left: 22px;
				margin: 0px 15px 0px 12px;
			}
			/* it's important to set width/height to 100% for full-screen init */
			html, body {
				width: 100%;
				height: 100%;
				margin: 0px;
				overflow: hidden;
			}
			span.tabFormsOpts {
				font-size:12px;
			}
			div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
				 text-transform: none !important;
			}
			/*
			
			
			/* Estilos para ocultar las tabs pero permitir ver sus celdas */
			.hiddenTabs div.dhx_cell_tabbar {
				position:none !important;	//Este estilo no existe, pero efectivamente bloquea el aplicado a la clase que sobreescribe
			}
			.hiddenTabs div.dhxtabbar_tabs {
				display:none !important;
			}
			.hiddenTabs div.dhx_cell_cont_tabbar {
				border: 0px !important;
			}
			.visibleTabs div.dhx_cell_tabbar {
				position:absolute !important;	//Utilizado para pintar tabs que estar�an dentro de la tab principal oculta
			}
			.visibleTabs div.dhxtabbar_tabs {
				display:inline !important;
			}
			.visibleTabs div.dhx_cell_cont_tabbar {
				border: 0px !important;
			}
			/* Estilo para los enlaces de las opciones principales (tabs ocultos) de la ventana de dise�o */
			.linkToolbar {
				background-color: #264B83;
			}
			.linktd {
				cursor: pointer;
				font-family: Roboto Regular;
				font-size: 13px;
				//font-weight: bold;
				//text-align: center;
				//text-decoration: underline;
				color: white;	//#333333;
				padding-left: 24px;
				padding-right: 24px;
				//width: 150px;
				white-space: nowrap;
			}
			.linktdSelected {
				text-decoration: underline;
			}
			/* Estilo para mantener seleccionado el bot�n de selecci�n de la toolbar de preguntas */
			//Finalmente no sirvi� esto, porque el evento MouseOver del list option cambia de clase eliminando la anterior aplicada, as� que se perd�a
			//la clase definida en este archivo, se opt� por cambiar directamente el estilo
			.tbOptionSelected {
				  background-color: #fff3a1 !important;
			}
			/* Agrega el border a las celdas del grid */
			table.obj tr td {
				//border-left-width: 1px !important;
				border: 0px !important;
				padding-left: 0px !important;
				padding-right: 0px !important;
			}
			.dhx_textarea {
				margin-left: 0px;
			}
			/* Estilos para los subgrids de propiedades de las tabs de colecciones */
			.dhx_sub_row {
				//overflow: visible !important;
				border: 1px solid #808080 !important;
			}
			.dhx_sub_row table.obj tr td {
				border-left-width:1px solid #808080 !important;
			}
			.dhxdataview_placeholder div.dhx_dataview_item {
				border: 0px !important;
			}
			/* Estilos de los Layouts */
			.dhx_cell_hdr {
				background-color: #f5c862 !important;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>
		<script>
			var AdminObjectType = {
				otyItem: 0,
				otySurvey: 1,
				otySection: 2,
				otyQuestion: 3,
				otyAnswer: 4,
				otyAgenda: 5,
				otyCatalog: 6,
				otyAttribute: 7,
				otyDraft: 8,
				otyOutbox: 9,
				otyPhoto: 10,
				otySignature: 11,
				otySetting: 12,
				otySurveyFilter:13,
				otySectionFilter:14,
				otyMenu: 15,
				otyAudio: 16,
				otyVideo: 17,
				otyQuestionFilter: 18,
				otyStatusAgenda: 19,
				otyStatusDocument: 20,
				otySketch: 21,
				otyOption: 22,
				otyShowQuestion: 23,
				otyUser: 24,
				otyUserGroup: 25
			};

			
			var optionsYesNo = {
				0:"<?=translate("No")?>",
				1:"<?=translate("Yes")?>"
			};
			
			optionsYesNoOpt = {
				0:"<?=translate("No")?>",
				1:"<?=translate("Yes")?>",
				2:"<?=translate("Optional")?>",
			};
			
			optionsYesNoOptPhoto = {
				0: {name:"<?=translate("No")?>", visibilityRules:{fields:[{tabId:"generalTab", fieldId:"allowGallery", show:false}]}},
				1: {name:"<?=translate("Yes")?>", visibilityRules:{fields:[{tabId:"generalTab", fieldId:"allowGallery", show:true}]}},
				2: {name:"<?=translate("Optional")?>", visibilityRules:{fields:[{tabId:"generalTab", fieldId:"allowGallery", show:true}]}}
			};
			
			//OMMC 2016-04-05: Agregado para mostrar los n�meros de preguntas
			optionsYesNoDef = {
				"-1": "<?=translate("Default")?>",
				"0": "<?=translate("No")?>",
				"1": "<?=translate("Yes")?>"
			};
			
			var propTabType = {
				properties: 0,
				collection: 1
			}
			
			var optionsFormsSizes = new Object;
			optionsFormsSizes[0] = "<?=translate("Default")?>";
			optionsFormsSizes[100] = "<?=translate("Size")." x1"?>";
			optionsFormsSizes[200] = "<?=translate("Size")." x2"?>";
			optionsFormsSizes[300] = "<?=translate("Size")." x3"?>";
			
			<?//JAPR 2016-11-10: Corregido un bug, no estaba habilitada la configuraci�n de s�lo imagen y el orden era incorrecto (#FSMUZC)?>
			var optionsFormsLayOutTextPos = new Object;
			optionsFormsLayOutTextPos[<?=svtxpDefault?>] = "<?=translate("Default")?>";
			optionsFormsLayOutTextPos[<?=svtxpNone?>] = "<?=translate("Image only")?>";
			optionsFormsLayOutTextPos[<?=svtxpBottom?>] = "<?=translate("Text below image")?>";
			optionsFormsLayOutTextPos[<?=svtxpLeft?>] = "<?=translate("Text before image")?>";
			optionsFormsLayOutTextPos[<?=svtxpRight?>] = "<?=translate("Text after image")?>";
			optionsFormsLayOutTextPos[<?=svtxpTop?>] = "<?=translate("Text above image")?>";
			optionsFormsLayOutTextPos[<?=svtxOverTop?>] = "<?=translate("Text inside and above image")?>";
			optionsFormsLayOutTextPos[<?=svtxOverMiddle?>] = "<?=translate("Text inside and center image")?>";
			optionsFormsLayOutTextPos[<?=svtxOverBottom?>] = "<?=translate("Text inside and below image")?>";
			optionsFormsLayOutTextPos[<?=svtxpCustom?>] = "<?=translate("Form custom layout html")?>";
			var optionsFormsLayOutTextPosOrder = new Array;
			optionsFormsLayOutTextPosOrder.push(<?=svtxpDefault?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpNone?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpBottom?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpLeft?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpRight?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpTop?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxOverTop?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxOverMiddle?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxOverBottom?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpCustom?>);
			
			//Di�logos
			var objWindows;						//Referencia al componente que almacena todas las ventanas
			var objDialog;						//Di�logo modal com�n para cualquier proceso que requiera captura de alg�n valor espec�fico (cada proceso lo volver� a definir)
			//Forms
			var objUsersForm;					//Ventana para asociar los usuarios a la Forma
			var objUserGroupsForm;				//Ventana para asociar los grupos de usuarios a la Forma
			//TabBars
			var objFormTabs;					//Cejillas de las configuraciones de la forma
			//LayOuts							//Opciones principales de la forma
			//JAPRWarning: Remover esto para regresar a Tabs como objeto principal
			var objMainLayout;					//LayOut que se agreg� para separar la tabbar con el contenido de la p�gina de un header HTML que represente a las Tabs
			//JAPRWarning: Remover esto para regresar a Tabs como objeto principal (End)
			var objPropsLayout;					//LayOut principal de la forma, contiene a los dem�s
			var objUserTabsColl = new Object();	//Array con la definici�n de campos para la forma (se separa ya que esta no est� en la celda de propiedades, as� que los m�todos como getFieldDefinition no pueden usar objTabsColl)
			var MenuID = "<?=$this->MenuID?>";
			//JAPR 2016-07-27: Corregido un bug, el objeto para los campos GFieldTypes.multiList debe ser un array de nombres, no un object (#7UMIYL)
			var Forms = []; //Para las formas existentes
			var FormsOrder = [];
			/* Opciones para utilizar en la definici�n de las configuraciones en los grids */
			var objCheck = {type:GFieldTypes.combo, options:optionsYesNo, default:0};
			var objCheckPlusOpt = {type:GFieldTypes.combo, options:optionsYesNoOpt, default:0};
			var objCheckPlusDef = {type:GFieldTypes.combo, options:optionsYesNoDef, default:"-1"};
			var objAlpha = {type:GFieldTypes.alphanum, length:255};
			var objInteger = {type:GFieldTypes.number};
			var reqAjax = 1;

			var Order = [];

<?			
			require_once("survey.inc.php");
			$loadedSurveys = BITAMSurvey::getSurveys($this->Repository, true);
			foreach($loadedSurveys as $skey => $asurvey) {
				//GCRUZ 2015-11-05. Escapar comillas.
				$asurvey = str_replace("\"", "\\\"", $asurvey);
				//@JAPR 2016-07-27: Corregido un bug, el objeto para los campos GFieldTypes.multiList debe ser un array de nombres, no un object (#7UMIYL)
?>
				Forms.push("<?=$asurvey?>");
				FormsOrder.push(<?=$skey?>);
<?			}

		$arrJSON = $this->getJSonDefinition($this->Repository);
		if (is_null($arrJSON) || !is_array($arrJSON)) {
			$arrJSON = array('data' => array());
		}
		else {
			$arrJSON = array('data' => $arrJSON);
		}
		$strJSONDefinitions = json_encode($arrJSON);
?>		
		var objFormsDefs = JSON.parse("<?=addslashes($strJSONDefinitions)?>");
		var objSetttings = objFormsDefs.data;
		
		//******************************************************************************************************
		SettingsCls.prototype = new GenericItemCls();
		SettingsCls.prototype.constructor = SettingsCls;
		function SettingsCls() {
			this.objectType = AdminObjectType.otyMenu;
			this.objectIDFieldName = "MenuID";
		}
		
		/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
		//El par�metro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
		*/
		SettingsCls.prototype.getDataFields = function(oFields) {
			var blnAllFields = false;
				//if (gbFullSave || !oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
				if (!oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
					blnAllFields = true;
				}
				
				var objParams = {};
				if (blnAllFields || oFields["MenuID"]) {
					objParams["MenuID"] = MenuID;
				}
				if (blnAllFields || oFields["name"]) {
					objParams["MenuName"] = this.name;
				}
				if (blnAllFields || oFields["formsIDs"]) {
					objParams["FormsAssct"] = this.formsIDs;
				}
				if (blnAllFields || oFields["DissociateAll"]) {
					objParams["DissociateAll"] = this.DissociateAll;
				}
				if (blnAllFields || oFields["displayImageDes"]) {
					objParams["MenuImageText"] = this.displayImageDes;
				}
				//JAPR 2016-02-09: Modificada la lista de formas para permitir Layouts personalizados y diferentes tama�os por cada forma (#FSMUZC)
				if (blnAllFields || oFields["width"]) {
					objParams["Width"] = this.width;
				}
				if (blnAllFields || oFields["height"]) {
					objParams["Height"] = this.height;
				}
				if (blnAllFields || oFields["textPos"]) {
					objParams["TextPosition"] = this.textPos;
				}
				if (blnAllFields || oFields["textSize"]) {
					objParams["TextSize"] = this.textSize;
				}
				if (blnAllFields || oFields["customLayout"]) {
					objParams["CustomLayout"] = this.customLayout;
				}
				//JAPR 2016-08-22: Agregada la configuraci�n para ajustar las imagenes del men� de formas al total del �rea usada sin importar la escala (#Y3ATJH)
				if (blnAllFields || oFields["autoFitImage"]) {
					objParams["AutoFitImage"] = this.autoFitImage;
				}
				
				return objParams;
		}
			
			/* Invoca la inicializaci�n del App de eForms y la interfaz del dise�ador
			*/
			function doOnLoad() {
				console.log('doOnLoad');

				if (!objSetttings) {
					alert('<?=translate("You can not access this Groups.")?>');
					return;
				}
				//Agrega clases extendidas a los objetos de definici�n le�dos
				addClassPrototype();

				//Agrega estilos adicionales para corregir issues est�ticos del DHTMLX
				//Para la DataView de secciones, aparec�a un borde azul a la derecha y se ve�a mal, as� que se remueve din�micamente
				//$("head").append("<style> .simpleClass{ display:none; } </style>");
				
				//Prepara el controlador de ventanas que permitir� crear di�logos de captura o ventanas flotantes auxiliares
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
				//JAPRWarning: Remover esto para regresar a Tabs como objeto principal
				objMainLayout = new dhtmlXLayoutObject({
					parent: document.body,
					pattern:"1C",
					parent:"divDesignBody"
					//pattern: "2E"
				});
				objMainLayout.cells("a").appendObject("divDesign");
				objMainLayout.cells("a").setText("<?=translate('Menu').$strDivSaving?>");
				
				objMainLayout.setSizes();
				//JAPRWarning: Remover esto para regresar a Tabs como objeto principal (End)

				generateMenuProps(objMainLayout);
				objMainLayout.cells("a").showHeader();
			}

			function generateMenuProps(objLayout)
			{
				console.log('generateMenuProps ' + objLayout);
				//Generar todos los fields
				//JAPR 2016-02-09: Modificada la lista de formas para permitir Layouts personalizados y diferentes tama�os por cada forma (#FSMUZC)
				objUserTabsColl = {
					"generalTab": {id:"generalTab", name:"<?=translate("General")?>", activeOnLoad:true}, defaultVisibility:{
						name:true, formsIDs:true, displayImageDes:true, width:true, height:true, textPos:true, textSize:true, customLayout:true
					}<?if (getMDVersion() >= esvTemplateStyles) {?>,
					"layoutTab": {id:"layoutTab", name:"<?=translate("Format")?>", defaultVisibility:{
							width:true, height:true, textPos:true, textSize:true, customLayout:true, autoFitImage:true
						}
					}<?}?>
				};
				
				var objCommon = {id:MenuID, parentType:AdminObjectType.otyMenu, tab:"generalTab"};
				objUserTabsColl["generalTab"].fields =  new Array(
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"name", label:"<?=translate("Name")?>"})),
					new FieldCls($.extend(true, {}, objCommon, {name:"formsIDs", label:"<?=translate("Forms")?>", type:GFieldTypes.multiList,
						options:Forms, optionsOrder:FormsOrder, readOnly:true})),
					new FieldCls($.extend(true, {}, objCommon, {name:"displayImageDes", label:"<?=translate("Menu Image")?>", type:GFieldTypes.uploadImg, maxLength:255}))
				);
				
				//JAPR 2016-02-09: Modificada la lista de formas para permitir Layouts personalizados y diferentes tama�os por cada forma (#FSMUZC)
				<?if (getMDVersion() >= esvTemplateStyles) {?>
				var objCommon = {id:intSurveyID, parentType:AdminObjectType.otySurvey, tab:"layoutTab"};
				objUserTabsColl["layoutTab"].fields =  new Array(
					new FieldCls($.extend(true, {}, objCommon, {name:"width", label:"<?=translate("Form layout width")?>", type:GFieldTypes.combo, options:optionsFormsSizes, default:300})),
					new FieldCls($.extend(true, {}, objCommon, {name:"height", label:"<?=translate("Form layout height")?>", type:GFieldTypes.combo, options:optionsFormsSizes, default:100})),
					new FieldCls($.extend(true, {}, objCommon, {name:"textPos", label:"<?=translate("Form layout text pos")?>", type:GFieldTypes.combo, options:optionsFormsLayOutTextPos, optionsOrder:optionsFormsLayOutTextPosOrder, default:<?=svtxpDefault?>})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"textSize", label:"<?=translate("Form layout text size")?>"})),
					new FieldCls($.extend(true, {}, objCommon, {name:"customLayout", label:"<?=translate("Form custom layout html")?>", empty:true, type:GFieldTypes.editorHTMLDialog, size:5000}))
				);
				<?}?>
				//JAPR 2016-08-22: Agregada la configuraci�n para ajustar las imagenes del men� de formas al total del �rea usada sin importar la escala (#Y3ATJH)
				<?if (getMDVersion() >= esvAutoFitFormsImage) {?>
				objUserTabsColl["layoutTab"].fields.push(new FieldCls($.extend(true, {}, objCommon, objCheckPlusDef, {name:"autoFitImage", label:"<?=translate("Autofit image")?>"})));
				<?}?>
				
				//Los detalles de la forma se muestran en el tab de propiedades
				generateFormDetail(getSettingsFieldsDefaulVisibility(objUserTabsColl), AdminObjectType.otyMenu, MenuID, objLayout.cells("a"));
			}

			/* Modifica el objeto recibido con el conjunto de los campos (propiedades) que son visibles por default*/
			function getSettingsFieldsDefaulVisibility(oTabsColl) {
				console.log("getSettingsFieldsDefaulVisibility ");
				
				var strTabId = "generalTab";
				if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
					oTabsColl[strTabId].defaultVisibility = {};

					oTabsColl[strTabId].defaultVisibility["name"] = true;
					oTabsColl[strTabId].defaultVisibility["formsIDs"] = true;
					oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
					//JAPR 2016-02-09: Modificada la lista de formas para permitir Layouts personalizados y diferentes tama�os por cada forma (#FSMUZC)
					oTabsColl[strTabId].defaultVisibility["width"] = true;
					oTabsColl[strTabId].defaultVisibility["height"] = true;
					oTabsColl[strTabId].defaultVisibility["textPos"] = true;
					oTabsColl[strTabId].defaultVisibility["textSize"] = true;
					oTabsColl[strTabId].defaultVisibility["customLayout"] = true;
				}
				return oTabsColl;
			}


			/* Regresa la definici�n del campo a partir de la combinaci�n tab/field. Se asume que siempre se hace referencia a los campos del tipo de objeto
			que se est� desplegando en el momento en que se solicita, ya que esos son los que est�n almacenados en el array
			//JAPR 2015-06-25: Agregado el par�metro bForForm para indicar que se solicitan los campos directos del detalle de la forma, de lo contrario se asume que se trata del objeto
			que se est� desplegando en la celda de propiedades
			*/
			function getFieldDefinition(sTabName, sFieldName, bForForm) {
				console.log('getFieldDefinition ' + sTabName + '.' + sFieldName);
				var objField = undefined;
				
				var objTmpTabsColl = objUserTabsColl;
				if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
					var objTab = objTmpTabsColl[sTabName];
					if (objTab && objTab.fields) {
						for (var intCont in objTab.fields) {
							var objFieldTmp = objTab.fields[intCont];
							if (objFieldTmp && objFieldTmp.name == sFieldName) {
								objField = objFieldTmp;
								break;
							}
						}
					}
				}
				
				return objField;
			}
			
			/* Id�ntica a getFieldDefinition pero en lugar del nombre de campo el cual es �til en un grid de propiedades, se utiliza el �ndice del campo, el cual es �til en
			un grid de valores */
			function getFieldDefinitionByIndex(sTabName, iFieldIdx, bForForm) {
				console.log('getFieldDefinitionByIndex ' + sTabName + '.' + iFieldIdx);
				var objField = undefined;
				
				var objTmpTabsColl = objUserTabsColl;
				if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
					var objTab = objTmpTabsColl[sTabName];
					if (objTab && objTab.fields) {
						objField = objTab.fields[iFieldIdx];
					}
				}
				
				return objField;
			}


			/* Agrega la clase correspondiente a cada objeto descargado de la definici�n de la forma */
			function addClassPrototype() {
				if (!objSetttings) {
					return;
				}
			
			$.extend(objSetttings, new SettingsCls());
		}

		/* Obtiene la referencia al objeto indicado por el tipo y id de los par�metros. Si se necesita un objeto espec�fico de alguna colecci�n que le pertenece, se utilizan 
			los par�metros child (esto se deber�a aplicar s�lo si el objeto principal no tiene una forma directa de ser accesado, originalmente utilizado para las opciones de
			respuesta fijas de preguntas/secciones)
			*/
			function getObject(iObjectType, iObjectID, iChildObjectType, iChildObjectID) {
				var objObject = objSetttings;
				
				return objObject;
			}

			//Descarga la instancia de la forma de la memoria
			function doUnloadDialog() {
				console.log('doUnloadDialog');
				if (!objWindows || !objDialog) {
					return;
				}
				
				var objForm = objDialog.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
				
				if (objWindows.unload) {
					objWindows.unload();
					objWindows = null;
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				objDialog = null;
			}
			
			/* Descarga las ventanas adicionales creadas as� como cualquier otro componente */
			function doOnUnload() {
				console.log('doOnUnload');
				if (objWindows && objWindows.unload) {
					objWindows.unload();
				}
			}
		</script>
	</head>
	<body onload="doOnLoad();" onunload="doOnUnload();">
		<iframe id="frameRequests" name="frameRequests" style="display:none;">
		</iframe>
		<form id="frmRequests" style="display:none;" method="post" target="frameRequests">
		</form>
		<div id="divDesign" style="height:100%;width:100%;">
		</div>
		<div id="divDesignBody" class="hiddenTabs" style="height:95%;width:100%;overflow:hidden;">
		</div>
		<iframe id="frameUploadFile" name="frameUploadFile" style="display:none; position:absolute; left:100, top:150">
		</iframe>
	</body>
</html>
<?
		die();
	}
}

class BITAMSurveyMenuExtCollection extends BITAMSurveyMenuCollection
{
	use BITAMCollectionExt;
	public $ObjectType = otyMenu;
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=SurveyMenuExt";
	}
	
	function get_CustomButtons() {
		$arrButtons = array();
		$arrButtons[] = array("id" => "add", "url" => "", "onclick" => "addNewObject();", "isDefault" => 1);	//*"label" => translate("Add"), "image" => "add.png", */
		return $arrButtons;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		return parent::PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser);
	}
	
	/* Agrega el c�digo para permitir la creaci�n flotante de formas as� como el copiado */
	function generateAfterFormCode($aUser) {
?>
	<script language="JavaScript">
		var objWindows;
		var objDialog;
		var intDialogWidth = 450;
		var intDialogHeight = 450;
		var reqAjax = 1;
		var FormsList = [];
		
<?
		$objFormsColl = BITAMSurveyCollection::NewInstance($this->Repository);
		foreach ($objFormsColl as $objForm) {
				//@JAPR 2015-12-17: Corregido un bug, la funci�n htmlspecialchars requiere ENT_QUOTES para reemplazar comilla sencilla tambi�n (#OUHB07)
				echo("\t\t\t"."FormsList.push({value:{$objForm->SurveyID}, text:'".htmlspecialchars($objForm->SurveyName, ENT_QUOTES)."'});\r\n");
		}
?>

		//Descarga la instancia de la forma de la memoria
		function doUnloadDialog() {
			if (objDialog) {
				var objForm = objDialog.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
			}
			
			if (objWindows) {
				if (objWindows.unload) {
					objWindows.unload();
					objWindows = null;
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
			}
			
			objDialog = null;
		}
		
		//Manda los datos al server para ser procesados
		function doSendData() {
			if (!objDialog) {
				return;
			}
			
			
			var objForm = objDialog.getAttachedObject();
			var objData = objForm.getFormData();
			var Combx = objForm.getCombo('FormsAssct');
			var Values = Combx.getChecked();
			//if(Values.indexOf(Combx.getSelected()) == -1)
			//	Values.push(Combx.getSelected());
			var objData = objForm.getFormData();
			objForm.setItemValue("FormsAssct", Values.join()); //Si Values es [] se envia ""
			objDialog.progressOn();
			objForm.lock();
			objForm.send("processRequest.php", "post", function(loader, response) {
				if (!loader || !response) {
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				setTimeout(function () {
					try {
						var objResponse = JSON.parse(response);
					} catch(e) {
						alert(e + "\r\n" + response.substr(0, 1000));
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					if (objResponse.error) {
						alert(objResponse.error.desc);
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					else {
						if (objResponse.warning) {
							alert(objResponse.warning.desc);
						}
					}
					
					//S�lo si todo sale Ok cierra el di�logo y continua cargando el dise�o de la forma recien creada
					doUnloadDialog();
					var strURL = objResponse.url;
					if (strURL && parent.doExecuteURL) {
						parent.doExecuteURL("", undefined, undefined, strURL);
					}
				}, 100);
			});
		}
		
		//Muestra el di�logo para crear una nueva forma
		function addNewObject() {
			if (!objWindows) {
				//Prepara el controlador de ventanas que permitir� crear di�logos de captura o ventanas flotantes auxiliares
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
			}
			
			objDialog = objWindows.createWindow({
				id:"newObject",
				left:0,
				top:0,
				width:intDialogWidth,
				height:intDialogHeight,
				center:true,
				modal:true
			});
			
			objDialog.setText("<?=translate('New menu')?>");
			objDialog.denyPark();
			objDialog.denyResize();
			objDialog.setIconCss("without_icon");
			$(".dhxwin_hdr").attr("style", "z-index: 0; background-color: wheat;");
			//Al cerrar realizar� el cambio de secci�n de la pregunta
			objDialog.attachEvent("onClose", function() {
				return true;
			});
								
			/* El di�logo para crear las formas simplemente pide el nombre que se le asignar� a la nueva forma */
			var objFormData = [
				{type:"settings"/*, offsetLeft:20*/},
				{type:"input", name:"MenuName", label:"<?=translate("Name")?>", labelAlign:"left", value:"", labelWidth:150, inputWidth:250, required:true, validate:"NotEmpty"},
				{type:"combo", comboType: "checkbox", name:"FormsAssct", label:"<?=translate("Forms")?>", labelAlign:"left", options:FormsList, labelWidth:150, inputWidth:250, required:true, validate:"NotEmpty"},
				{type:"block", blockOffset:50, offsetLeft:100, list:[
					{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
					{type:"newcolumn"},
					{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
				]},
				{type:"input", name:"MenuID", value:-1, hidden:true},
				{type:"input", name:"RequestType", value:reqAjax, hidden:true},
				{type:"input", name:"Process", value:"Add", hidden:true},
				{type:"input", name:"ObjectType", value:<?=otyMenu?>, hidden:true}
			];
			
			var objForm = objDialog.attachForm(objFormData);
			objForm.adjustParentSize();
			objForm.setItemFocus("UserGroupName");
			objForm.attachEvent("onBeforeValidate", function (id) {
				console.log('Before validating the form: id == ' + id);
			});
			
			objForm.attachEvent("onAfterValidate", function (status) {
				console.log('After validating the form: status == ' + status);
			});
			
			objForm.attachEvent("onValidateSuccess", function (name, value, result) {
				console.log('Validating rule ok: name == ' + name + ', value == ' + value + ', result == ' + result);
			});
			
			objForm.attachEvent("onValidateError", function (name, value, result) {
				console.log('Error validating the form: name == ' + name + ', value == ' + value + ', result == ' + result);
			});
			
			objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
				if (!name || !inp || !ev || !ev.keyCode) {
					return;
				}
				
				switch (ev.keyCode) {
					case 13:
						//OMMC 2015-11-12: Agregada validaci�n para prevenir la creaci�n de varios objetos derivado de m�ltiples <enter> 
						if(objDialog.bitProcessing === undefined){
							objDialog.bitProcessing = true;
							setTimeout(function() {
								objForm.callEvent("onButtonClick", ["btnOk"]);
								objDialog.bitProcesing = undefined;
							}, 100);
						}
						break;
					case 27:
						setTimeout(function() {
							objForm.callEvent("onButtonClick", ["btnCancel"])
						}, 100);
						break;
				}
			});
			
			objForm.attachEvent("onButtonClick", function(name) {
				switch (name) {
					case "btnCancel":
						setTimeout(function () {
							doUnloadDialog();
						}, 100);
						break;
						
					case "btnOk":
						setTimeout(function() {
							if (objForm.validate()) {
								doSendData();
							}
						}, 100);
						break;
				}
			});
		}
	</script>
<?
	}
}

?>