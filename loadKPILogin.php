<?php
//@JAPR 2013-02-08: Modificado el LogOut para que delege a KPIOnline el lugar correcto al que debe dirigirse
//@JAPR 2015-10-05: Modificada la redirección en caso de pérdida de sesión para que dependa del servicio donde se encuentra (originalmente usado por GeoControl)
require_once("config.php");

$intErrorNum = getParamValue('error', 'both', '(int)');
$strErrorParam = "";
if ($intErrorNum > 0) {
	$strErrorParam = "?error={$intErrorNum}";
}

global $loginKpiOnline;
if (!isset($loginKpiOnline) || trim($loginKpiOnline) == '') {
	//@JAPR 2017-06-09: Modificada la URL de redirección cuando se pierde la sesión
	$loginKpiOnline = "https://www.bitam.com/";
}
//@JAPR
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script language="JavaScript">
		function redirectKPILogin()
		{
			javascript:top.location.href='<?=$loginKpiOnline?><?=$strErrorParam?>';
		}
		</script>
	</head>
	<body onload="javascript:redirectKPILogin();">
	</body>
</html>