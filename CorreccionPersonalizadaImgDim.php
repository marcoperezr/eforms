<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("question.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

//Obtenemos el prefijo de la ruta requerida en la dimension imagen
$script = $_SERVER["REQUEST_URI"];
//Eliminamos la primera diagonal que separa el archivo php de la carpeta
$position = strrpos($script, "/");
$dir = substr($script, 0, $position);
	
//Obtenemos el siguiente diagonal para de ahi obtener la carpeta principal de trabajo
$position = strrpos($dir, "/");
$mainDir = substr($dir, ($position+1));

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	//Se genera instancia de coleccion de preguntas dado un SurveyID
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyInstance->SurveyID);
	
	foreach ($questionCollection->Collection as $questionInstance) 
	{
		if(!is_null($questionInstance->HasReqPhoto) && $questionInstance->HasReqPhoto>0)
		{
			if(!is_null($questionInstance->ImgDimID) && $questionInstance->ImgDimID>0)
			{
				$dimTableName = "RIDIM_".$questionInstance->ImgDimID;
				$dimFieldDesc = "DSC_".$questionInstance->ImgDimID;
				$oldString = "c:\\inetpub\\wwwroot\\artus\\genviusa\\esurveytmp/";
				$newString = $mainDir."/";

				//Reemplazamos el valor anterior por el valor nuevo
				$sql = "UPDATE ".$dimTableName." SET ".$dimFieldDesc." = REPLACE(".$dimFieldDesc.", ".$aRepository->DataADOConnection->Quote($oldString).", ".$aRepository->DataADOConnection->Quote($newString).")";
				
				if ($aRepository->DataADOConnection->Execute($sql) === false)
				{
					echo( translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
				}
				else 
				{
					echo('UPDATE: '.$sql.'<br>');
				}
			}
		}
	}
}
?>