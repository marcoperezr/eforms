<?php

require_once('eFormsDataDestinations.inc.php');
require_once('syncDataDestination.class.php');
require_once('survey.inc.php');
require_once('csvExport/csvExport.inc.php');

function syncIncDataDestinations($theRepository, $theAppUser)
{
	// kpi7 fbm000.SI_SV_SurveyTasksLog.CreationDateID = syncDate, fbm000.SI_SV_SurveyTasksLog.SurveyDate = fecha de captura
	// repository sveforms_xxxx.SyncDate + sveforms_xxxx.SyncTime = fbm000.SI_SV_SurveyTasksLog.CreationDateID
	$bsyncIncDataDestinations = true;

	require_once('eSurveyServiceMod.inc.php');
	$aBITAMConnection = connectToKPIRepository();

	$aSQL = "SELECT GROUP_CONCAT(DISTINCT(surveyid)) AS 'syncsurveys' FROM si_sv_incdatadestinations WHERE status IN (".stsPending.", ".stsFailed.")";
	$rsSyncSurveys = $theRepository->DataADOConnection->Execute($aSQL);
	$strSyncSurveys = '';
	if ($rsSyncSurveys && $rsSyncSurveys->_numOfRows != 0)
		$strSyncSurveys = $rsSyncSurveys->fields['syncsurveys'];
	echo PHP_EOL.'strSyncSurveys='.$strSyncSurveys.PHP_EOL;
	if (trim($strSyncSurveys) != '')
		$syncSurveys = explode(',', $strSyncSurveys);
	else
		$syncSurveys = array();
	syncLogString('Get the surveyIDs to process...');
	syncLogString($aSQL);
	syncLogString('strSyncSurveys='.$strSyncSurveys);

	foreach ($syncSurveys as $aSurveyID)
	{
		syncLogString('processing SurveyID='.$aSurveyID);
		$survey = BITAMSurvey::NewInstanceWithID($theRepository, $aSurveyID);
		$sections = BITAMSectionCollection::NewInstance($theRepository, $aSurveyID);
		$questions = BITAMQuestionCollection::NewInstance($theRepository, $aSurveyID);

		$dataDestinations =  BITAMeFormsDataDestinationsCollection::NewInstance($theRepository, $aSurveyID);
		foreach ($dataDestinations->Collection as $aDataDestination)
		{
			if (($aDataDestination->type == 2 || $aDataDestination->type == 3 || $aDataDestination->type == 5) && $aDataDestination->isIncremental)
			{
				syncLogString('Incremental DataDestination found: '.$aDataDestination->connection['name']);
				$aSQLDataDestination = "SELECT surveykey FROM si_sv_incdatadestinations WHERE datadestination_id = ".$aDataDestination->id." AND surveyid = ".$aSurveyID." AND status IN (".stsPending.", ".stsFailed.")";
				syncLogString('Get SurveyKeys For surveyID='.$aSurveyID);
				syncLogString($aSQLDataDestination);
				$rsSurveyKeys = $theRepository->DataADOConnection->Execute($aSQLDataDestination);
				$arrSurveyKeys = array();
				$strSurveyKeys = '';
				if ($rsSurveyKeys && $rsSurveyKeys->_numOfRows != 0)
				{
					while(!$rsSurveyKeys->EOF)
					{
						$arrSurveyKeys[] = $rsSurveyKeys->fields['surveykey'];
						$rsSurveyKeys->MoveNext();
					}
					$strSurveyKeys = implode(',', $arrSurveyKeys);
					syncLogString('SurveyKeys='.$strSurveyKeys);
				}
				if (count($arrSurveyKeys) == 0)
				{
					syncLogString('No surveykeys found for this destination, continue...');
					continue;
				}
				syncLogString('Loading data to export...');
				$data = EsurveyGetDataToExport($theRepository, $survey, $sections, $questions, null, null, array(), false, $aSQLDataDestination);
				$aSQL = "UPDATE si_sv_incdatadestinations SET status = ".stsActive." WHERE datadestination_id = ".$aDataDestination->id." AND surveyid = ".$aSurveyID." AND surveykey IN (".$strSurveyKeys.")";
				syncLogString('Set Incremental DataDestination as Active');
				syncLogString($aSQL);
				$theRepository->DataADOConnection->Execute($aSQL);
				$fileFormat = '';
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				$fileName = GeteFormsLogPath().date('YmdHis').'_SurveyExport_'.$aSurveyID;
				//@JAPR
				switch($aDataDestination->fileformat)
				{
					case 1:
						$fileFormat = 'CSV';
						$fileName .= '.csv';
						EsurveyCSVExport($data, $fileName);
						break;
					case 2:
						$fileFormat = 'XLS';
						$fileName .= '.xlsx';
						EsurveyXLSExport($data, $fileName);
						break;
				}
				syncLogString('Export file: '.$fileName.'. File Format:'.$fileFormat);
				switch($aDataDestination->type)
				{
					//DropBox
					case 2:
						syncLogString('Path Export file: ' . $fileName);

						//GCRUZ 2016-01-13. En cargas incrementales antes de subir el archivo hay que descargar el archivo de datos
						$incrementalFile = '';
						if (trim($survey->incrementalFile) == '')
							$survey->incrementalFile = 'SurveyReport_'.$aSurveyID;
						$incrementalFile = $survey->incrementalFile. '.' . pathinfo( $fileName, PATHINFO_EXTENSION );

						syncLogString('Incremental file: ' . $incrementalFile);
						$incrementalTempPath = "loadexcel/uploads/Datasources/dropbox/";
						//Borrar el archivo antes de bajarlo al folder temporal
						@unlink($incrementalTempPath.$incrementalFile);
						//Descargar archivo
						syncLogString('Attempt to download file...');
						$success = connectionCloud::downloadFile('dropBox',$aDataDestination->id_connection,$theRepository,$aDataDestination->path.$incrementalFile);
						if ($success['success'])
						{
							//Archivo descargado correctamente, procede a hacer incremental
							syncLogString('Procees to do incremental. Source File:' . $incrementalTempPath.$incrementalFile . ' Incremental file: ' . $fileName);
							switch($aDataDestination->fileformat)
							{
								//CSV
								case 1:
									EsurveyCSVIncremental($incrementalTempPath.$incrementalFile, $fileName);
									break;
								//XLS
								case 2:
									$resultIncremental = @EsurveyXLSIncremental($incrementalTempPath.$incrementalFile, $fileName);
									if (!$resultIncremental[0])
										syncLogString('Failed to make a XLS incremental file!');
									break;
							}
						}
						else
							syncLogString('Failed to download file...');

						$success=connectionCloud::uploadFile('dropBox', $aDataDestination->id_connection, $fileName, $theRepository, $survey, $incrementalFile, 'syncLogString' ,$aDataDestination->path);
						if ($success['error'])
						{
							$aSQL = "UPDATE si_sv_incdatadestinations SET status = ".stsFailed." WHERE datadestination_id = ".$aDataDestination->id." AND surveyid = ".$aSurveyID." AND surveykey IN (".$strSurveyKeys.")";;
							syncLogString('Set Incremental DataDestination as Failed');
							syncLogString('Error msg:'.$success['msg']);
							syncLogString($aSQL);
							$theRepository->DataADOConnection->Execute($aSQL);
							$bsyncIncDataDestinations = false;
						}
						else
						{
							$aSQL = "UPDATE si_sv_incdatadestinations SET status = ".stsCompleted." WHERE datadestination_id = ".$aDataDestination->id." AND surveyid = ".$aSurveyID." AND surveykey IN (".$strSurveyKeys.")";;
							syncLogString('Set Incremental DataDestination as Completed');
							syncLogString($aSQL);
							$theRepository->DataADOConnection->Execute($aSQL);
						}

						syncLogString('Finish dropbox');
						break;
					//Google Drive
					case 3:
						syncLogString('Path Export file: ' . $fileName);

						//GCRUZ 2016-01-13. En cargas incrementales antes de subir el archivo hay que descargar el archivo de datos
						$incrementalFile = '';
						if (trim($survey->incrementalFile) == '')
							$survey->incrementalFile = 'SurveyReport_'.$aSurveyID;
						$incrementalFile = $survey->incrementalFile. '.' . pathinfo( $fileName, PATHINFO_EXTENSION );

						syncLogString('Incremental file: ' . $incrementalFile);
						$incrementalTempPath = "loadexcel/uploads/Datasources/googledrive/";
						//Borrar el archivo antes de bajarlo al folder temporal
						@unlink($incrementalTempPath.$incrementalFile);
						//Descargar archivo
						syncLogString('Attempt to download file...');
						$success = connectionCloud::downloadFile('googleDrive',$aDataDestination->id_connection,$theRepository,$incrementalFile, null, $aDataDestination->realPath);
						if ($success['success'])
						{
							//Archivo descargado correctamente, procede a hacer incremental
							syncLogString('Procees to do incremental. Source File:' . $incrementalTempPath.$incrementalFile . ' Incremental file: ' . $fileName);
							switch($aDataDestination->fileformat)
							{
								//CSV
								case 1:
									EsurveyCSVIncremental($incrementalTempPath.$incrementalFile, $fileName);
									break;
								//XLS
								case 2:
									$resultIncremental = @EsurveyXLSIncremental($incrementalTempPath.$incrementalFile, $fileName);
									if (!$resultIncremental[0])
										syncLogString('Failed to make a XLS incremental file!');
									break;
							}
						}
						else
							syncLogString('Failed to download file...');

						$success=connectionCloud::uploadFile('googleDrive', $aDataDestination->id_connection, $fileName, $theRepository, $survey, $incrementalFile, 'syncLogString' ,$aDataDestination->realPath);
						if ($success['error'])
						{
							$aSQL = "UPDATE si_sv_incdatadestinations SET status = ".stsFailed." WHERE datadestination_id = ".$aDataDestination->id." AND surveyid = ".$aSurveyID." AND surveykey IN (".$strSurveyKeys.")";;
							syncLogString('Set Incremental DataDestination as Failed');
							syncLogString('Error msg:'.$success['msg']);
							syncLogString($aSQL);
							$theRepository->DataADOConnection->Execute($aSQL);
							$bsyncIncDataDestinations = false;
						}
						else
						{
							$aSQL = "UPDATE si_sv_incdatadestinations SET status = ".stsCompleted." WHERE datadestination_id = ".$aDataDestination->id." AND surveyid = ".$aSurveyID." AND surveykey IN (".$strSurveyKeys.")";;
							syncLogString('Set Incremental DataDestination as Completed');
							syncLogString($aSQL);
							$theRepository->DataADOConnection->Execute($aSQL);
						}

						syncLogString('Finish GoogleDrive');
						break;
					//FTP
					case 5:
						syncLogString('Path Export file: ' . $fileName);

						//GCRUZ 2016-01-13. En cargas incrementales antes de subir el archivo hay que descargar el archivo de datos
						$incrementalFile = '';
						if (trim($survey->incrementalFile) == '')
							$survey->incrementalFile = 'SurveyReport_'.$aSurveyID;
						$incrementalFile = $survey->incrementalFile. '.' . pathinfo( $fileName, PATHINFO_EXTENSION );

						syncLogString('Incremental file: ' . $incrementalFile);
						$incrementalTempPath = "loadexcel/uploads/Datasources/FTP/";
						//Borrar el archivo antes de bajarlo al folder temporal
						@unlink($incrementalTempPath.$incrementalFile);
						//Descargar archivo
						syncLogString('Attempt to download file...');
						$success = connectionCloud::downloadFile('FTP',$aDataDestination->connection,$theRepository,$aDataDestination->path.$incrementalFile);
						if ($success['success'])
						{
							//Archivo descargado correctamente, procede a hacer incremental
							syncLogString('Procees to do incremental. Source File:' . $incrementalTempPath.$incrementalFile . ' Incremental file: ' . $fileName);
							switch($aDataDestination->fileformat)
							{
								//CSV
								case 1:
									EsurveyCSVIncremental($incrementalTempPath.$incrementalFile, $fileName);
									break;
								//XLS
								case 2:
									$resultIncremental = @EsurveyXLSIncremental($incrementalTempPath.$incrementalFile, $fileName);
									if (!$resultIncremental[0])
										syncLogString('Failed to make a XLS incremental file!');
									break;
							}
						}
						else
							syncLogString('Failed to download file...');
						
						$success=connectionCloud::uploadFile('FTP', $aDataDestination->connection, $fileName, $theRepository, $survey, $incrementalFile, 'syncLogString' ,$aDataDestination->path);
						if ($success['error'])
						{
							$aSQL = "UPDATE si_sv_incdatadestinations SET status = ".stsFailed." WHERE datadestination_id = ".$aDataDestination->id." AND surveyid = ".$aSurveyID." AND surveykey IN (".$strSurveyKeys.")";;
							syncLogString('Set Incremental DataDestination as Failed');
							syncLogString($aSQL);
							$theRepository->DataADOConnection->Execute($aSQL);
							$bsyncIncDataDestinations = false;
						}
						else
						{
							$aSQL = "UPDATE si_sv_incdatadestinations SET status = ".stsCompleted." WHERE datadestination_id = ".$aDataDestination->id." AND surveyid = ".$aSurveyID." AND surveykey IN (".$strSurveyKeys.")";;
							syncLogString('Set Incremental DataDestination as Completed');
							syncLogString($aSQL);
							$theRepository->DataADOConnection->Execute($aSQL);
						}

						syncLogString('Finish FTP');
						break;
				}
			}
		}
	}

	if ($bsyncIncDataDestinations)
	{
		$aResponse = array();
		$aResponse['error'] = false;
		$aResponse['errmsg'] = "SyncIncDataDestinations:Success";
	}
	else
	{
		$aResponse = array();
		$aResponse['error'] = true;
		$aResponse['errmsg'] = "SyncIncDataDestinations:Failed";
	}
	return $aResponse;

}

function syncLogString($msg)
{
	//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	//Integradas las funciones comunes para almacenamiento de log de eForms
	$sFilePath = GeteFormsLogPath();
	error_log(date('Y-m-d H:i:s').' :: '.$msg.PHP_EOL, 3, $sFilePath.'/syncIncDataDestinations.log');
	//@JAPR
}

?>