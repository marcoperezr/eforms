<?php
error_reporting(E_ALL);
session_start();

if (session_is_registered("PABITAM_RepositoryName") && array_key_exists("PABITAM_RepositoryName", $_SESSION) &&
	session_is_registered("PABITAM_UserName") && array_key_exists("PABITAM_UserName", $_SESSION))
{
	require_once("repository.inc.php");
	require_once('config.php');
	require_once('cubeClasses.php');
	require_once("headerUtils.php");

	global $BITAMRepositories;

	$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
	
	if (!array_key_exists($theRepositoryName, $BITAMRepositories))
	{
		header("Location: index.php?error=1");
		exit();
	}
	
	$theRepository = $BITAMRepositories[$theRepositoryName];
	if (!$theRepository->open())
	{
		header("Location: index.php?error=2");
		exit();
	}	
	
	$theUserName = $_SESSION["PABITAM_UserName"];
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
	$theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
	if (is_null($theUser))
	{
		header("Location: index.php?error=3");
		exit();
	}
} 
else
{
	//Identificar si nos encontramos en modo FBM
	$fbmMode = (int)$_COOKIE["PAFBM_Mode"];
		
	if($fbmMode!=1)
	{
		if (array_key_exists("QUERY_STRING", $_SERVER))
		{
			$goto = base64_encode($_SERVER["PHP_SELF"]."?".$_SERVER["QUERY_STRING"]);
		}
		else
		{
			$goto = base64_encode($_SERVER["PHP_SELF"]);
		}
		header("Location: index.php?error=4&goto=".$goto);
	}
	else 
	{
		header("Location: loadKPILogin.php");
	}
	exit();
}

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$theSchedulerID = 0;

if (array_key_exists("SchedulerID", $_GET))
{
	$theSchedulerID = $_GET["SchedulerID"];
}

	//Obtenemos todos los usuarios de la tabla de usuarios de ESurvey
	$arrUsers = array();

	$sql = "SELECT CLA_USUARIO, Email FROM SI_SV_Users";
	
	if($_SESSION["PAFBM_Mode"])
	{
		//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
		//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
		$sql.= " WHERE CLA_USUARIO > 0";	
		//@JAPR
	}

	$sql.=" ORDER BY Email";
	
	
	$aRS = $theRepository->ADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	while (!$aRS->EOF)
	{
		$intUserID = (int)$aRS->fields["cla_usuario"];
		$strUserName = $aRS->fields["email"];

		$arrUsers[$intUserID]["cla_usuario"] = $intUserID;
		$arrUsers[$intUserID]["name"] = $strUserName;
		$aRS->MoveNext();
	}
	
	//Obtenemos los usuarios seleccionados en el scheduler
	$arrSurveyUsers = array();

	$sql = "SELECT UserID FROM SI_SV_SurveySchedulerUser WHERE SchedulerID = ".$theSchedulerID;
	
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		die( translate("Error accessing")." SI_SV_SurveySchedulerUser ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	while (!$aRS->EOF)
	{
		$intUserID = (int)$aRS->fields["userid"];
		$strUserName = $arrUsers[$intUserID]["name"];

		$arrSurveyUsers[$intUserID]["cla_usuario"] = $intUserID;
		$arrSurveyUsers[$intUserID]["name"] = $strUserName;

		$aRS->MoveNext();
	}

	/*******************************************************************************************************************************************************/
	
	//Obtenemos todos los roles
	$arrRoles = array();
	$sql = "SELECT CLA_ROL, NOM_ROL FROM SI_ROL WHERE CLA_ROL <> -1 AND NOM_ROL NOT LIKE (".$theRepository->ADOConnection->Quote("dummy").")";

	$aRS = $theRepository->ADOConnection->Execute($sql);
	if ($aRS === false)
	{
		die( translate("Error accessing")." SI_ROL ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	while (!$aRS->EOF)
	{
		$intRolID = (int)$aRS->fields["cla_rol"];
		$strRolName = $aRS->fields["nom_rol"];

		$arrRoles[$intRolID]["cla_rol"] = $intRolID;
		$arrRoles[$intRolID]["name"] = $strRolName;
		$aRS->MoveNext();
	}
	
	$arrSurveyRoles = array();
	$sql = "SELECT RolID FROM SI_SV_SurveySchedulerRol WHERE SchedulerID = ".$theSchedulerID;

	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS === false)
	{
		die( translate("Error accessing")." SI_SV_SurveySchedulerRol ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	while (!$aRS->EOF)
	{
		$intRolID = (int)$aRS->fields["rolid"];
		$strRolName = $arrRoles[$intRolID]["name"];

		$arrSurveyRoles[$intRolID]["cla_rol"] = $intRolID;
		$arrSurveyRoles[$intRolID]["name"] = $strRolName;
		$aRS->MoveNext();
	}
	
	$strAllElements = "";
	
	foreach ($arrUsers as $anElement)
	{
		if($strAllElements!="")
		{
			$strAllElements.="_AWSepElement_";
		}
		
		$strAllElements.="User_".$anElement["cla_usuario"]."_AWSepName_".$anElement["name"];
	}
	
	foreach ($arrRoles as $anElement)
	{
		if($strAllElements!="")
		{
			$strAllElements.="_AWSepElement_";
		}
		
		$strAllElements.="Rol_".$anElement["cla_rol"]."_AWSepName_".$anElement["name"];
	}
	
	$strSelectedElements = "";

	foreach ($arrSurveyUsers as $anElement)
	{
		if($strSelectedElements!="")
		{
			$strSelectedElements.="_AWElem_";
		}
		
		$strSelectedElements.="User_".$anElement["cla_usuario"];
	}
	
	foreach ($arrSurveyRoles as $anElement)
	{
		if($strSelectedElements!="")
		{
			$strSelectedElements.="_AWElem_";
		}
		
		$strSelectedElements.="Rol_".$anElement["cla_rol"];
	}
?>
<html>
<head>
<title>eSurvey</title>
<script language='JavaScript' src='js/utils.js'></script>
<script language="javascript" src="js/dialogs.js"></script>


<link rel="stylesheet" type="text/css" href="css/default.css">
	<link rel="stylesheet" type="text/css" href="css/calendar.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/default2.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/buildformula.css">
	<link rel="stylesheet" type="text/css" href="css/tabs.css">
	<link rel="stylesheet" type="text/css" href="css/base.css">
	<link rel="stylesheet" type="text/css" href="css/tabbedUl.css">
	
	<script language="JavaScript" type="text/javascript" src="js/browserSniffer.js"></script>
	<script language="JavaScript">
<?
	global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
	global $MONTH_NAMES, $MONTH_ABBR;
	global $DAY_NAMES, $DAY_ABBR;

	$first = true;
	echo("		var MONTH_NAMES = new Array(");
	foreach ($MONTH_NAMES as $aName)
	{
		if (!$first) { echo(","); }
		$first = false;
		echo("\"".$aName."\"");
	}
	echo(");\n");
	$first = true;
	echo("		var MONTH_ABBR = new Array(");
	foreach ($MONTH_ABBR as $anAbbr)
	{
		if (!$first) { echo(","); }
		$first = false;
		echo("\"".$anAbbr."\"");
	}
	echo(");\n");
	$first = true;
	echo("		var DAY_NAMES = new Array(");
	foreach ($DAY_NAMES as $aName)
	{
		if (!$first) { echo(","); }
		$first = false;
		echo("\"".$aName."\"");
	}
	echo(");\n");
	$first = true;
	echo("		var DAY_ABBR = new Array(");
	foreach ($DAY_ABBR as $anAbbr)
	{
		if (!$first) { echo(","); }
		$first = false;
		echo("\"".$anAbbr."\"");
	}
	echo(");\n");
?>
		var MONTH_REGEXP = '(';
		for (var _i = MONTH_NAMES.length - 1; _i >= 0; _i--)
		{
			if (_i == MONTH_NAMES.length - 1)
			{
				MONTH_REGEXP += MONTH_NAMES[_i];
			}
			else
			{
				MONTH_REGEXP += ('|' + MONTH_NAMES[_i]);
			}
			MONTH_REGEXP += ('|' + MONTH_ABBR[_i]);
			if ((_i + 1) < 10)
			{
				MONTH_REGEXP += ('|0' + (_i + 1));
			}
			MONTH_REGEXP += ('|' + (_i + 1));
		}
		MONTH_REGEXP += ')?';
		
		var DECIMAL_SYMBOL = '<?= $DECIMAL_SYMBOL ?>';
		var DIGIT_GROUPING_SYMBOL = '<?= $DIGIT_GROUPING_SYMBOL ?>';
	</script>

	<script language="JavaScript" type="text/javascript" src="js/browserSniffer.js"></script>
<script language="JavaScript" type="text/javascript" src="js/calendar.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/main.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/autofill.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/common.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/popup.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/toggleDiv.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/CmbMultiSel.js"></script>


<script language="JavaScript">
//var window_dialogArguments = window.dialogArguments;
var window_dialogArguments = getDialogArgument();
owner = window_dialogArguments[0];
owner.returnValue = null;

var strAllElements = '<?=$strAllElements?>';
var strSelectedElements = '<?=$strSelectedElements?>';

function displayUsers()
{
	document.getElementById('Columns').style.display = 'block';
	document.getElementById('Roles').style.display = 'none';
}

function displayRoles()
{
	document.getElementById('Roles').style.display = 'block';
	document.getElementById('Columns').style.display = 'none';
}

function myCustomFunc() {
	//OnStart(strAllElements, strSelectedElements);
	//alert(1);
	debugger;
}

</script>
<script language='JavaScript' src='js/manageLists.js'></script>
<style type="text/css">@import url("css/fudge.css");</style>
</head>
<body onload="myCustomFunc();" class="NBody">
<!--<div id='divHS' style="position:relative;" class="HeaderStep">
	<img style="position:relative;left:10px; top:5px; width:112px; height:90%;" src="images/lineasfondo.png">
	<table cellpadding="0" cellspacing="1" border="0" class="TBStep">
		<tr>
			<td width="20px"><img src="images/step_icon.gif"></td>
			<td width="270px" id="lbStep" class="Step"><?=translate("Change agenda")?></td>
		</tr>
	</table>
</div>-->
<!--<div style="width:100%;position:absolute;top:80px;bottom:25px;">-->
	<div style="width:100%;" >
		<div style="position:relative:left; height:20px;">
		
		</div>
		<div id="" style="position:relative;width:100%;height:20px;margin: 10 10 10 10" class="">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr style="width:100%;height:25px;float:left">
					<td><input style="float:left;" type="text" id="CaptureStartDate" name="CaptureStartDate" size="22" maxlength="19" value="" onfocus="javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}" onkeypress="javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);" onblur="javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0) {year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureStartDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureStartDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');">
						<script language="JavaScript">
							function calendarCallback_BITAMSurvey_CaptureStartDate(day, month, year)
							{
								if (String(month).length == 1)
								{
									month = '0' + month;
								}
								if (String(day).length == 1)
								{
									day = '0' + day;
								}

								var obj = document.getElementById('CaptureStartDate');
								var v = obj.value;
								v = v.substr(10);
								v = year + '-' + month + '-' + day + v;
								obj.value = v;
								obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
							}
							calendar_BITAMSurvey_CaptureStartDate = new calendar('calendar_BITAMSurvey_CaptureStartDate', 'calendarCallback_BITAMSurvey_CaptureStartDate');
						</script>
					</td>
				</tr>
			</table>
		</div>
	</div>
<!--</div>-->
<!--<br>
<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tbButtons" style="position:absolute; bottom:5px;right:10px;">
	<tr>
		
	</tr>
</table>-->
</body>
</html>