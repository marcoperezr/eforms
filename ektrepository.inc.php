<?php

require_once("repository.inc.php");
require_once("initialize.php");

class BITAMEKTRepository extends BITAMObject
{
	public $RepositoryName;
	public $ADODBDriver; // "mssql" or "oci8" or "odbc";
	public $ADODBServer;
	public $ADODBUser;
	public $ADODBPassword;
	public $ADODBDatabase; // "database name" (mssql) or "service name" (oci8)
	public $ADOConnection;
	public $ADODataDictionary;
	public $ArtusWebHtmlURL;
	public $DataADODBDriver; // "mssql" or "oci8" or "odbc";
	public $DataADODBServer;
	public $DataADODBUser;
	public $DataADODBPassword;
	public $DataADODBDatabase; // "database name" (mssql) or "service name" (oci8)
	public $DataADOConnection;
	public $IniArray;
	public $DifferentDBs;
	public $ChangeADODBPassword;
	public $ChangeDataADODBPassword;
			
	function __construct($anIniArray, $aRepositoryName)
	{
		BITAMObject::__construct(null);
		$this->IniArray=$anIniArray;
		$this->RepositoryName = $aRepositoryName;
		$this->ADODBDriver = ""; // "mssql" or "oci8" or "odbc";
		$this->ADODBServer = "";
		$this->ADODBUser = "";
		$this->ADODBPassword = "";
		$this->ADODBDatabase = ""; // "database name" (mssql) or "service name" (oci8)
		
		$this->DataADODBDriver = ""; // "mssql" or "oci8" or "odbc";
		$this->DataADODBServer = "";
		$this->DataADODBUser = "";
		$this->DataADODBPassword = "";
		$this->DataADODBDatabase = ""; // "database name" (mssql) or "service name" (oci8)
		$this->DifferentDBs=0;
		$this->ChangeADODBPassword=0;
		$this->ChangeDataADODBPassword=0;
	}

	static function NewInstance($anIniArray, $aRepositoryName)
	{
		return new BITAMEKTRepository($anIniArray, $aRepositoryName);
	}

	static function NewInstanceWithID($anIniArray, $aRepositoryName, $withPasswords = true)
	{
		$anInstance = null;
		if ($aRepositoryName=='' || $aRepositoryName==null)
		{
			return $anInstance;
		}
		$allRepositories = BITAMEKTRepository::LoadAllRepositories($anIniArray, $withPasswords);
		if (array_key_exists($aRepositoryName, $allRepositories))
		{
			$anInstance = $allRepositories[$aRepositoryName];
		}
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$anIniArray=BITAMEKTRepository::getIniArray();
		
		if (array_key_exists("RepositoryName", $aHTTPRequest->POST))
		{
			$aRepositoryName = $aHTTPRequest->POST["RepositoryName"];
			
			if (is_array($aRepositoryName))
			{
				$aCollection = BITAMEKTRepositoryCollection::NewInstance($anIniArray, $aRepositoryName);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->IniArray=BITAMEKTRepository::getIniArray();
					$anInstanceToRemove->remove();
				}
				
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMEKTRepository::NewInstanceWithID($anIniArray, $aRepositoryName);
				
				if (is_null($anInstance))
				{
					$anInstance = BITAMEKTRepository::NewInstance($anIniArray, $aRepositoryName);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMEKTRepository::NewInstance($anIniArray, '');
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("RepositoryName", $aHTTPRequest->GET))
		{
			$aRepositoryName = $aHTTPRequest->GET['RepositoryName'];

			$anInstance = BITAMEKTRepository::NewInstanceWithID($anIniArray, $aRepositoryName, false);
			
			if (is_null($anInstance))
			{
				$anInstance = BITAMEKTRepository::NewInstance($anIniArray, '');
			}
		}
		else
		{
			$anInstance = BITAMEKTRepository::NewInstance($anIniArray, '');
		}
		
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("RepositoryName", $anArray))
		{
			$this->RepositoryName = $anArray["RepositoryName"];
		}
		
	 	if (array_key_exists("ADODBDriver", $anArray))
		{
			$this->ADODBDriver = $anArray["ADODBDriver"];
		}
		
		if (array_key_exists("ADODBServer", $anArray))
		{
			$this->ADODBServer = $anArray["ADODBServer"];
		}
		
		if (array_key_exists("ADODBDatabase", $anArray))
		{
			$this->ADODBDatabase = $anArray["ADODBDatabase"];
		}

		if (array_key_exists("ADODBUser", $anArray))
		{
			$this->ADODBUser = $anArray["ADODBUser"];
		}
		
		if (array_key_exists("ADODBPassword", $anArray))
		{
			$this->ADODBPassword = $anArray["ADODBPassword"];
		}
		
		if (array_key_exists("DifferentDBs", $anArray))
		{
			$this->DifferentDBs = $anArray["DifferentDBs"];
		}

		if($this->DifferentDBs==0)
		{
			$this->DataADODBDriver = "";
			$this->DataADODBServer = "";
			$this->DataADODBDatabase = "";
			$this->DataADODBUser = "";
			$this->DataADODBPassword = "";
		}
		else
		{
			if (array_key_exists("DataADODBDriver", $anArray))
			{
				$this->DataADODBDriver = $anArray["DataADODBDriver"];
			}
			
			if (array_key_exists("DataADODBServer", $anArray))
			{
				$this->DataADODBServer = $anArray["DataADODBServer"];
			}
	
			if($this->DataADODBDriver!="odbc")
			{
				if (array_key_exists("DataADODBDatabase", $anArray))
				{
					$this->DataADODBDatabase = $anArray["DataADODBDatabase"];
				}
			}
			else 
			{
				$this->DataADODBDatabase = "";
			}

			if (array_key_exists("DataADODBUser", $anArray))
			{
				$this->DataADODBUser = $anArray["DataADODBUser"];
			}
	
			if (array_key_exists("DataADODBPassword", $anArray))
			{
				$this->DataADODBPassword = $anArray["DataADODBPassword"];
			}
		}
		
	 	if (array_key_exists("ChangeADODBPassword", $anArray))
		{
			$this->ChangeADODBPassword = (int) $anArray["ChangeADODBPassword"];
		}
		
	 	if (array_key_exists("ChangeDataADODBPassword", $anArray))
		{
			$this->ChangeDataADODBPassword = (int) $anArray["ChangeDataADODBPassword"];
		}

		return $this;
	}
	
	function save()
	{
	 	if (!array_key_exists($this->RepositoryName, $this->IniArray))
		{
			$this->IniArray[$this->RepositoryName] = array();
		}
		$this->IniArray[$this->RepositoryName]['ADODBDriver'] = $this->ADODBDriver;
		$this->IniArray[$this->RepositoryName]['ADODBServer'] = $this->ADODBServer;
		$this->IniArray[$this->RepositoryName]['ADODBDatabase'] = $this->ADODBDatabase;
		$this->IniArray[$this->RepositoryName]['ADODBUser'] = $this->ADODBUser;
		
		if($this->ChangeADODBPassword)
		{
			$this->IniArray[$this->RepositoryName]['ADODBPassword'] = BITAMEncryptPassword($this->ADODBPassword);			
		}
		
		$this->IniArray[$this->RepositoryName]['DataADODBDriver'] = $this->DataADODBDriver;
		$this->IniArray[$this->RepositoryName]['DataADODBServer'] = $this->DataADODBServer;
		$this->IniArray[$this->RepositoryName]['DataADODBDatabase'] = $this->DataADODBDatabase;
		$this->IniArray[$this->RepositoryName]['DataADODBUser'] = $this->DataADODBUser;
		
		if($this->ChangeDataADODBPassword)
		{
			$this->IniArray[$this->RepositoryName]['DataADODBPassword'] = BITAMEncryptPassword($this->DataADODBPassword);
		}
				
		global $EKTCfgPath;
		write_ini_file($EKTCfgPath, $this->IniArray);		
		
		if(array_key_exists("PABITAM_RepositoryName", $_SESSION) && $this->RepositoryName==$_SESSION["PABITAM_RepositoryName"])
		{
			exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php\">\n</head>\n<body>\n</body>\n</html>\n");
		}

		return $this;
	}
	
	function remove()
	{
	 	if (array_key_exists($this->RepositoryName, $this->IniArray))
		{
			unset($this->IniArray[$this->RepositoryName]);
			global $EKTCfgPath;
			write_ini_file($EKTCfgPath, $this->IniArray);
		}
	}

	function isNewObject()
	{	
		//return ($this->RepositoryName == '');
		return !array_key_exists($this->RepositoryName, $this->IniArray);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Repository");
		}
		else
		{  
			return $this->RepositoryName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=EKTRepository";
		}
		else
		{
			return "BITAM_PAGE=EKTRepository&RepositoryName=".$this->RepositoryName;
		}
	}

	function get_Parent()
	{
		$anIniArray=$this->IniArray;
		return BITAMEKTRepositoryCollection::NewInstance($anIniArray);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function generateBeforeFormCode($aUser)
 	{	?>
 		<script language="JavaScript">
 		function verifyFieldsAndSave(target)
 		{
 			//debugger;
 			var RepositoryName=Trim(BITAMEKTRepository_SaveForm.RepositoryName.value);
 			var allRepositories = new Array;
 			
 			<?		
 			if (array_key_exists("PABITAM_RepositoryName", $_SESSION))
 			{
			?>
			//repositorio de la conexión actual
			var currentRepository='<?=strtoupper($_SESSION["PABITAM_RepositoryName"])?>';
			<?
 			}
 			else
 			{
			?>
				var currentRepository=null;
 			<?
 			}
 			?>
 			
 			if(RepositoryName=='')
 			{	
 				alert ('<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Name"))?>');
 			}
 			else
 			{
 				<? 
				$nI = 0;
				foreach ($this->IniArray as $keyIniArray => $valueIniArray)
				{ 					
 				?>
					allRepositories[<?=$nI?>] = '<?=strtoupper($keyIniArray)?>';
			 	<?
					$nI++;
				}
			 	?>				
 				
			 	RepositoryName = RepositoryName.toUpperCase();
			 	<?
			 	//Si es repositorio nuevo verifica que el nombre de repositorio no exista
			 	if($this->isNewObject())
 				{
			 	?>
				 	if (!inArray(allRepositories, RepositoryName))
				 	{
	 					BITAMEKTRepository_Ok(target);
				 	}
	 				else
	 				{
	 					alert('<?=translate("%s already exists",translate("This Repository Name"))?>');
	 				}
 				<?
 				}
 				else
 				{
 				?>
 					//debugger;
	 				if(currentRepository!=null && RepositoryName==currentRepository)
	 				{
	 						//Si es el repositorio usado actualmente se pide confirmación para grabar los cambios
	 						var answer = confirm("<?=translate("This is the current repository, If you save the changes is necessary to aunthetify to the application again, Do you want to save the changes?")?>");
	 						
	 						if(answer)
	 						{
	 							BITAMEKTRepository_Ok(target);
	 						}
	 				}
 					else
 					{
 						BITAMEKTRepository_Ok(target);
 					}
 				<?
 				}
 				?>
 			}
 		}
 		
	 	function showHideDataFields()
	 	{
	 		var objCmb = BITAMEKTRepository_SaveForm.DifferentDBs;
	 		differentDBsValue = parseInt(objCmb.options[objCmb.selectedIndex].value);
	 		rowDataADOBDDriver = document.getElementById("Row_DataADODBDriver");
	 		rowDataADODBServer = document.getElementById("Row_DataADODBServer");
	 		rowDataADODBDatabase = document.getElementById("Row_DataADODBDatabase");
	 		rowDataADODBUser = document.getElementById("Row_DataADODBUser");
	 		rowDataADODBPassword = document.getElementById("Row_DataADODBPassword");
	 		
	 		if (differentDBsValue==0)
	 		{	//El repositorio y la fuente están en la misma DB
				rowDataADOBDDriver.style.display = 'none';
		 		rowDataADODBServer.style.display = 'none';
		 		rowDataADODBDatabase.style.display = 'none';
		 		rowDataADODBUser.style.display = 'none';
		 		rowDataADODBPassword.style.display = 'none';
	 		}
	 		else
	 		{
	 			//El repositorio y la fuente están en diferentes DBs
				if(bIsIE)
	 			{
		 			
	 				rowDataADOBDDriver.style.display = 'inline';
			 		rowDataADODBServer.style.display = 'inline';
			 		rowDataADODBDatabase.style.display = 'inline';
			 		rowDataADODBUser.style.display = 'inline';
			 		rowDataADODBPassword.style.display = 'inline';
	 			}
	 			else
	 			{
	 				rowDataADOBDDriver.style.display = 'table-row';
			 		rowDataADODBServer.style.display = 'table-row';
			 		rowDataADODBDatabase.style.display = 'table-row';
			 		rowDataADODBUser.style.display = 'table-row';
			 		rowDataADODBPassword.style.display = 'table-row';
	 			}
		 		//Se requiere verificar si se ocultará el campo
		 		//de la base de datos de la fuente cuando es odbc
		 		showHideDataDB();
	 		}
	 	}
	 	
	 	function showHideDataDB()
	 	{
	 		var objCmb = BITAMEKTRepository_SaveForm.DataADODBDriver;
	 		var dataADODBDriverValue = objCmb.options[objCmb.selectedIndex].value;
	 		rowDataADODBDatabase = document.getElementById("Row_DataADODBDatabase");
	 		
	 		if (dataADODBDriverValue=='odbc')
	 		{	
	 			//El driver es odbc entonces no se despliega el campo
	 			//que pide el nombre de la base de datos
				rowDataADODBDatabase.style.display = 'none';
				//Y se elimina el contenido para cuando se haga submit a la forma
				//se vaya con un valor vacío en dicho campo
				BITAMEKTRepository_SaveForm.DataADODBDatabase.value = '';
	 		}
	 		else
	 		{	
	 			//El driver es diferente de odbc entonces se despliega el campo
	 			//que pide el nombre de la base de datos
 				if(bIsIE)
 				{
		 			rowDataADODBDatabase.style.display = 'inline';
 				}
 				else
 				{
 					rowDataADODBDatabase.style.display = 'table-row';
 				}
	 		}
	 	}

	 	//Refresca el frame del menú del encabezado
	 	function UpdateHeader()	
		{
	 		var objFrameHeader=window.parent.frames("header");
	 		objFrameHeader.location.reload();
	 	}
	 	
	 	function openChangePassword(passwordType)
	 	{
	 		//debugger;
			repName = BITAMEKTRepository_SaveForm.RepositoryName.value;
			strPassword=window.showModalDialog('dialog.html', 'changePassword.php?repName='+ repName + '&passwordType='+passwordType, 'help: no; status: no; scroll: yes; resizable: no; dialogHeight:200px; dialogWidth:270px;');

			if(strPassword!=null && strPassword!= undefined)
			{
				if(passwordType == 1)
				{
					objChangePassword = BITAMEKTRepository_SaveForm.ChangeADODBPassword;
					objChangePassword.value = 1;
					objPassword = BITAMEKTRepository_SaveForm.ADODBPassword;
					objPassword.value = strPassword;
				}
				else
				{
					objChangePassword = BITAMEKTRepository_SaveForm.ChangeDataADODBPassword;
					objChangePassword.value = 1;
					objPassword = BITAMEKTRepository_SaveForm.DataADODBPassword;
					objPassword.value = strPassword;
				}
			}
	 	}
	 	
 		</script>
 		<?
	}

	function generateAfterFormCode($aUser)
	{
		?>
	<script language="JavaScript">
		
		objBody= document.getElementsByTagName("body");
		objBody[0].onkeypress= function onc(event)
		{
			var event = (window.event) ? window.event : event;
			//if (window.event.keyCode == 13)
			if (event.keyCode ==13)
			return false;
		}		
		objOkSelfButton = document.getElementById("BITAMEKTRepository_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMEKTRepository_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMEKTRepository_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
		</script>	
<?
	}
	
	static function getTitleSection($title)
	{
		$htmlString='<div style="font-weight:bold; font-size:12">'.translate($title).'</div>';
        return $htmlString; 
	}	

	function get_FormIDFieldName()
	{
		return 'RepositoryName';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "RepositoryName";
		$aField->Title = translate("Repository Name");
		$aField->Type = "String";
		$aField->Size = 24;
		$aField->IsDisplayOnly=!($this->isNewObject());
		$aField->CodeBeforeField=BITAMEKTRepository::getTitleSection("MetaData");
		$myFields[$aField->Name] = $aField;

		$driverOptions=array();
		$driverOptions["mssql"]="mssql";
		$driverOptions["mysql"]="mysql";
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ADODBDriver";
		$aField->Title = translate("Database Driver");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options=$driverOptions;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ADODBServer";
		$aField->Title = translate("Server");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ADODBDatabase";
		$aField->Title = translate("Database");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ADODBUser";
		$aField->Title = translate("User");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ADODBPassword";
		$aField->Title = translate("Password");
		$aField->Type = "String";
		$aField->IsVisible = false;
		$aField->AfterMessage = "<span id=spanChangeADODBPassword unselectable=on style=\"padding-top:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);backgroun-position:center center;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openChangePassword(1)\">"
		.translate("Change").
		"</span>";
		$myFields[$aField->Name] = $aField;
		
		$options = array();
		$options[0] = translate("Metadata equal to data");
		$options[1] = translate("Metadata different from data");
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DifferentDBs";
		$aField->Title = translate("Data");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options=$options;
		$aField->OnChange="showHideDataFields()";
		$aField->CodeBeforeField=BITAMEKTRepository::getTitleSection("Data");
		$myFields[$aField->Name] = $aField;

		$driverOptions=array();
		$driverOptions["odbc"]="odbc";
		$driverOptions["mysql"]="mysql";

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataADODBDriver";
		$aField->Title = translate("Driver");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options=$driverOptions;
		$aField->OnChange="showHideDataDB()";
		//$aField->IsDisplayOnly=true;
		$myFields[$aField->Name] = $aField;

		//Nombre del ODBC o nombre del server si es conexion por driver
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataADODBServer";
		$aField->Title = translate("Data Source Name")." / ".translate("Server");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataADODBDatabase";
		$aField->Title = translate("Database");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataADODBUser";
		$aField->Title = translate("User");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataADODBPassword";
		$aField->Title = translate("Password");
		$aField->Type = "String";
		$aField->IsVisible = false;
		$aField->AfterMessage = "<span id=spanChangeDataADODBPassword unselectable=on style=\"padding-top:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);backgroun-position:center center;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openChangePassword(2)\">"
		.translate("Change").
		"</span>";
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ChangeADODBPassword";
		$aField->Title = "";
		$aField->Type = "Integer";
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ChangeDataADODBPassword";
		$aField->Title = "";
		$aField->Type = "Integer";
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function insideEdit()
	{
?>
		objSpanChange = document.getElementById("spanChangeADODBPassword");
		objSpanChange.style.backgroundImage = 'url(images\\btnBackground.gif)';
		objSpanChange.style.cursor = 'pointer';
		objSpanChange.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChange.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChange.onclick = new Function("openChangePassword(1);");
		
		objSpanChange = document.getElementById("spanChangeDataADODBPassword");
		objSpanChange.style.backgroundImage = 'url(images\\btnBackground.gif)';
		objSpanChange.style.cursor = 'pointer';
		objSpanChange.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChange.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChange.onclick = new Function("openChangePassword(2);");

<?
	}

	function insideCancel()
	{
?>
		objSpanChange = document.getElementById("spanChangeADODBPassword");
		objSpanChange.style.backgroundImage = 'url(images\\btnBackground_off.gif)';
		objSpanChange.style.cursor = 'default';
		objSpanChange.onmouseover = "";
		objSpanChange.onmouseout = "";
		objSpanChange.onclick = "";
		
		objSpanChange = document.getElementById("spanChangeDataADODBPassword");
		objSpanChange.style.backgroundImage = 'url(images\\btnBackground_off.gif)';
		objSpanChange.style.cursor = 'default';
		objSpanChange.onmouseover = "";
		objSpanChange.onmouseout = "";
		objSpanChange.onclick = "";
<?
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if (array_key_exists("PABITAM_RepositoryName", $_SESSION))
		{
			if($this->RepositoryName!=$_SESSION["PABITAM_RepositoryName"])
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return true;
		}
	}
	
	static function LoadAllRepositories($anIniArray, $withPasswords=true)
	{   
		global $AllRepositories;
		if (is_null($AllRepositories))
		{
			$AllRepositories = array();
			
			foreach ($anIniArray as $aRepositoryName => $aSectionArray)
			{
				if ($aRepositoryName != '@MAIN')
				{
					$aRepository = BITAMEKTRepository::NewInstance($anIniArray, $aRepositoryName);
					if (array_key_exists('ADODBDriver', $aSectionArray))
					{
						$aRepository->ADODBDriver = $aSectionArray['ADODBDriver'];
					}
					if (array_key_exists('ADODBServer', $aSectionArray))
					{
						$aRepository->ADODBServer = $aSectionArray['ADODBServer'];
					}

					if (array_key_exists('ADODBDatabase', $aSectionArray))
					{
						$aRepository->ADODBDatabase = $aSectionArray['ADODBDatabase'];
					}

					if (array_key_exists('ADODBUser', $aSectionArray))
					{
						$aRepository->ADODBUser = $aSectionArray['ADODBUser'];
					}
					
					if($withPasswords)
					{
						if (array_key_exists('ADODBPassword', $aSectionArray))
						{
							$aRepository->ADODBPassword = BITAMDecryptPassword($aSectionArray['ADODBPassword']);
						}
					}
					
					//Fuente
					if (array_key_exists('DataADODBDriver', $aSectionArray))
					{
						$aRepository->DataADODBDriver = $aSectionArray['DataADODBDriver'];
					}
					
					if($aRepository->DataADODBDriver=='')
					{
						$aRepository->DifferentDBs=0;
					}
					else
					{
						$aRepository->DifferentDBs=1;
					}
					
					if (array_key_exists('DataADODBServer', $aSectionArray))
					{
						$aRepository->DataADODBServer = $aSectionArray['DataADODBServer'];
					}
					
					if (array_key_exists('DataADODBDatabase', $aSectionArray))
					{
						$aRepository->DataADODBDatabase = $aSectionArray['DataADODBDatabase'];
					}
					
					if (array_key_exists('DataADODBUser', $aSectionArray))
					{
						$aRepository->DataADODBUser = $aSectionArray['DataADODBUser'];
					}
					
					if($withPasswords)
					{
						if (array_key_exists('DataADODBPassword', $aSectionArray))
						{
							$aRepository->DataADODBPassword = BITAMDecryptPassword($aSectionArray['DataADODBPassword']);
						}
					}
					
					$AllRepositories[$aRepositoryName] = $aRepository;
				}
			}
		}
		return $AllRepositories;
	}
	
	static function getIniArray()
	{		
		global $EKTCfgPath;
		$anIniArray = read_ini_file($EKTCfgPath);
		ksort($anIniArray);
		return $anIniArray;
	}
	
	static function loadBITAMRepositories()
	{
		$BITAMRepositories = array();

		$anIniArray = BITAMEKTRepository::getIniArray();
	
		foreach ($anIniArray as $aRepositoryName => $aSectionArray)
		{
			if ($aRepositoryName != '@MAIN')
			{
				$aRepository = BITAMRepository::NewInstance($aRepositoryName);
				
				if (array_key_exists('ADODBDriver', $aSectionArray))
				{
					$aRepository->ADODBDriver = $aSectionArray['ADODBDriver'];
				}
				if (array_key_exists('ADODBServer', $aSectionArray))
				{
					$aRepository->ADODBServer = $aSectionArray['ADODBServer'];
				}
				if (array_key_exists('ADODBDatabase', $aSectionArray))
				{
					$aRepository->ADODBDatabase = $aSectionArray['ADODBDatabase'];
				}
				if (array_key_exists('ADODBUser', $aSectionArray))
				{
					$aRepository->ADODBUser = $aSectionArray['ADODBUser'];
				}
				if (array_key_exists('ADODBPassword', $aSectionArray))
				{
					$aRepository->ADODBPassword = BITAMDecryptPassword($aSectionArray['ADODBPassword']);
				}
				//Fuente
				if (array_key_exists('DataADODBDriver', $aSectionArray))
				{
					$aRepository->DataADODBDriver = $aSectionArray['DataADODBDriver'];
				}
				if (array_key_exists('DataADODBServer', $aSectionArray))
				{
					$aRepository->DataADODBServer = $aSectionArray['DataADODBServer'];
				}
				if (array_key_exists('DataADODBDatabase', $aSectionArray))
				{
					$aRepository->DataADODBDatabase = $aSectionArray['DataADODBDatabase'];
				}
				if (array_key_exists('DataADODBUser', $aSectionArray))
				{
					$aRepository->DataADODBUser = $aSectionArray['DataADODBUser'];
				}
				if (array_key_exists('DataADODBPassword', $aSectionArray))
				{
					$aRepository->DataADODBPassword = BITAMDecryptPassword($aSectionArray['DataADODBPassword']);
				}
				
				$BITAMRepositories[$aRepository->RepositoryName] = $aRepository;
			}
		}
		
		return $BITAMRepositories;
	}
}

class BITAMEKTRepositoryCollection extends BITAMCollection
{
	function __construct()
	{
		BITAMCollection::__construct(null);
	}

	static function NewInstance($anIniArray, $anArrayOfRepositoryNames = null)
	{
		$anInstance = new BITAMEKTRepositoryCollection();
		
		if (!is_null($anArrayOfRepositoryNames))
		{
			switch (count($anArrayOfRepositoryNames))
			{
				case 0:
					break;
				case 1:
					$allRepositories = BITAMEKTRepository::LoadAllRepositories($anIniArray);
					$aRepositoryName = $anArrayOfRepositoryNames[0];
					if (array_key_exists($aRepositoryName, $allRepositories))
					{
						$anInstance->Collection[] = $allRepositories[$aRepositoryName];
					}
					break;
				default:
					$allRepositories = BITAMEKTRepository::LoadAllRepositories($anIniArray);
					foreach ($anArrayOfRepositoryNames as $aRepositoryName)
					{
						$aRepositoryName = $aRepositoryName;
						if (array_key_exists($aRepositoryName, $allRepositories))
						{
							$anInstance->Collection[] = $allRepositories[$aRepositoryName];
						}
					}
					break;
			}
		}
		else
		{
			$allRepositories = BITAMEKTRepository::LoadAllRepositories($anIniArray);
			foreach ($allRepositories as $aRepository)
			{
				$anInstance->Collection[] = $aRepository;
			}
		}
		return $anInstance;
	}	
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$anIniArray=BITAMEKTRepository::getIniArray();
		return BITAMEKTRepositoryCollection::NewInstance($anIniArray);
	}
	
	function get_Parent()
	{
		return null;
	}
	
	function get_Title()
	{	
		return translate("Repositories");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=EKTRepositoryCollection";
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_SECTION=EKTRepository";
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=EKTRepository";
	}
	
	function get_PathArray()
	{
		/*
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
		*/
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;

	}

	function get_FormIDFieldName()
	{
		return 'RepositoryName';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");

		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "RepositoryName";
		$aField->Title = translate("Repository Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsDisplayOnly=!($this->isNewObject());
		$aField->CodeBeforeField=BITAMEKTRepository::getTitleSection("MetaData");
		$myFields[$aField->Name] = $aField;

		$driverOptions=array();
		$driverOptions["mssql"]="mssql";
		$driverOptions["mysql"]="mysql";
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ADODBDriver";
		$aField->Title = translate("Database Driver");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options=$driverOptions;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ADODBServer";
		$aField->Title = translate("Server");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ADODBDatabase";
		$aField->Title = translate("Database");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		/*
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ADODBUser";
		$aField->Title = translate("User");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ADODBPassword";
		$aField->Title = translate("Password");
		$aField->Type = "Password";
		$aField->Size = 15;
		$aField->FormatMask = "N";
		$myFields[$aField->Name] = $aField;
		*/
		
		/*
		$driverOptions=array();
		$driverOptions["odbc"]="odbc";
		$driverOptions["mysql"]="mysql";

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataADODBDriver";
		$aField->Title = translate("Driver");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options=$driverOptions;
		$aField->CodeBeforeField=BITAMEKTRepository::getTitleSection("Data");
		//$aField->IsDisplayOnly=true;
		$myFields[$aField->Name] = $aField;

		//Nombre del ODBC o del servidor de la bd cuando es
		//conexion por provider
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataADODBServer";
		$aField->Title = translate("Data Source Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataADODBDatabase";
		$aField->Title = translate("Database");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataADODBUser";
		$aField->Title = translate("User");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataADODBPassword";
		$aField->Title = translate("Password");
		$aField->Type = "Password";
		$aField->Size = 15;
		$aField->FormatMask = "N";
		$myFields[$aField->Name] = $aField;
		*/
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return true;
	}
}
?>