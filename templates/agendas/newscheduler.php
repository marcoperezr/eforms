<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../');
	require 'checkCurrentSession.inc.php';
	require 'survey.inc.php';
	chdir($chdirCurrent);

	$rs = $theRepository->ADOConnection->Execute( "SELECT CLA_USUARIO, NOM_LARGO FROM SI_USUARIO WHERE cla_usuario > 0 order by NOM_LARGO ASC" );

	$sUserOptions = '<option value=""></option>';
	while($rs && !$rs->EOF)
	{
		$sUserOptions .= '<option value="'. $rs->fields['CLA_USUARIO'] .'">'. $rs->fields['NOM_LARGO'] .'</option>';
		$rs->MoveNext();
	}

	$rs = $theRepository->DataADOConnection->Execute( "SELECT id, name FROM si_sv_checkpoint order by name" );

	$sCheckpointOptions = '<option value=""></option>';
	while($rs && !$rs->EOF)
	{
		$sCheckpointOptions .= '<option value="'. $rs->fields['id'] .'">'. $rs->fields['name'] .'</option>';
		$rs->MoveNext();
	}

	$aSurveys = BITAMSurveyCollection::NewInstance($theRepository);

	$nCount = count( $aSurveys->Collection );

	$sSurveys = '';
	for($i=0; $i < $nCount; $i++)
	{
		$sSurveys .= '<option value="'. $aSurveys->Collection[$i]->SurveyID .'">'. $aSurveys->Collection[$i]->SurveyName .'</option>';
	}	

?><form role="form">
	<style>
		
		.modal.material form {
			overflow: inherit;
		}

		.alert.alert-danger {
			display: none;
		}
		
		.modal > .modal-dialog > .modal-content > .content {
			min-height: 310px;
		}
		.selectize-input {
			min-height: 28px;
			padding: 3px 10px;
		}

		.modal-backdrop {
			z-index: 104;
		}

		.modal {
			z-index: 104;
		}

		.modal > .modal-dialog > .modal-content .selectize-dropdown-content span {
		    display: inline;
		    text-align: inherit;
		    font-size: 14px;
		}

		.modal > .modal-dialog > .modal-content .selectize-dropdown-content span.caption {
			font-size: 12px;
		}

		.errorinput {
			border-color: red !important;
    		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    		box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    		border-width: 1px;
    		border-style: solid;
		}

		.object_table {
			display: inline-block;
		    float: left;
		    margin-left: 5px;
		    margin-right: 5px;
		}

		.object_table table {
			font-size: 12px;
		    border-style: solid;
		    border-width: 1px;
		    border-collapse: separate;
		    background-color: #FFFFFF;
		}

		.object_table td {
		    padding: 3px 5px;
		    border: solid #ebebeb;
		    border-width: 0 1px 1px 0;
		}

		.object_table td.selected {
			background-color: rgb(186, 210, 224);
		}

		div[data-recurtype] .row {
			margin-top: 10px;
		}

		section[data-section=scheduler] .selectize-control, section[data-section=scheduler] input.form-control {
			margin-right: 5px;
		}

		.modal input.form-control {
			height: 28px !important;
		}

		div.keys .form-group label {
			display: block !important;
    		text-align: left !important;
    		float: left;
    		width: 100%;
		}

		div.keys .selectize-control.single {
			display: inline-block;
    		width: 800px;
    		float: left;
		}

		div.keys div.clear {
			height: 28px;
		    width: 30px;
		    float: left;
		    text-align: center;
		    padding-top: 6px;
		    cursor: pointer;
		}


	</style>
	<div style="position: relative;  top: -59px;  left: 0px;height: 0px;"><div class="alert alert-danger" style="">Error!</div></div>
    <h1><?= translate("Basic info") ?></h1>
    <section data-section="basicinfo">
    	<input type="hidden" id="id" name="id">
		<div class="form-group">
			<label for="AgendaSchedName">* <?= translate('Name') ?></label>
		    <input required type="text" class="form-control" id="AgendaSchedName" name="AgendaSchedName" maxlength="250" required>
		</div>
		<div class="form-group">
			<label for="UserID">* <?= translate('User') ?></label>
		    <select required name="UserID"><?= $sUserOptions ?></select>
		</div>
		<div class="form-group">
			<label for="CheckPointID">* <?= translate('Check point') ?></label>
		    <select required name="CheckPointID"><?= $sCheckpointOptions ?></select>
		</div>
    </section>
    <h1><?= translate('Filters and forms') ?></h1>
    <section data-section="filtersforms">
    	<div class="form-group">
    		<label for="SurveyIDs">* <?= translate('Surveys') ?></label>
    		<select required name="SurveyIDs" multiple><?= $sSurveys; ?></select>
    	</div>
    	<div class="keys"></div>
    </section>
    <h1><?= translate("Scheduler") ?></h1>
    <section data-section="scheduler" class="scheduler">
    	<div class="form-group capture_start_time">
    		<label><?= translate('Capture Start Time') ?></label>
	    	<div>
	    		<select class="pull-left" id="" name="CaptureStartTime_Hours" class="" style="width: 200px;"><option selected="" value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
	    		<select class="pull-left" id="" name="CaptureStartTime_Minutes" class="" style="width: 200px;"><option selected="" value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>
	    		<label>(HH:mm)</label>
	    	</div>
    	</div>
    	<div class="form-group range_of_recurrence">
    		<label><?= translate('Range of recurrence') ?></label>
	    	<div>
	    		<label class="pull-left"><?= translate('Start') ?> </label><input readonly="readonly" style="cursor: pointer;width: 150px;background-color: #FFFFFF;" required type="text" class="form-control pull-left" id="RangeRecurrenceStart" name="RangeRecurrenceStart" maxlength="250" required>
	    		<select class="pull-left" id="RangeRecurOpt" style="width: 200px;" name="RangeRecurOpt"><option selected="" value="0"><?= translate('No end date') ?></option><option value="2"><?= translate('End by') ?></option></select>
	    		<label style="display: none" class=" labelEnd pull-left"><?= translate('End') ?> </label><input readonly="readonly" style="display: none; cursor: pointer;width: 150px;background-color: #FFFFFF;" required type="text" class="form-control pull-left" id="RangeRecurrenceEnd" name="RangeRecurrenceEnd" maxlength="250" required>
	    		<span>&nbsp;</span>
	    	</div>
    	</div>
    	<div class="form-group generate_next_day">
    		<label><?= translate('Generate agendas of next day after this time') ?></label>
	    	<div>
	    		<select class="pull-left" id="" name="GenerateAgendasNextDayTime_Hours" class="" style="width: 200px;"><option selected="" value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
	    		<select class="pull-left" id="" name="GenerateAgendasNextDayTime_Minutes" class="" style="width: 200px;"><option selected="" value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>
	    		<label>(HH:mm)</label>
	    	</div>
    	</div>
    	<div class="form-group">
    		<label for="frecuency"><?= translate('Recurrence pattern') ?></label>
    		<select required class="form-control" id="RecurPatternType" name="RecurPatternType">
    			<option value=""></option>
    			<option value="1"><?=translate('Daily')?></option>
    			<option value="2"><?=translate('Weekly')?></option>
    			<option value="8"><?=translate('Weeks of the month')?></option>
    			<option value="4"><?=translate('Monthly')?></option>
    			<option value="5"><?=translate('Yearly')?></option>
    		</select>
		</div>
		<div class="row" data-recurtype="1" style="display: none">
			<div class="col-md-4">
				<select id="DailyOpt" name="DailyOpt"><option selected="" value="0"><?= translate('Every') ?></option><option value="1"><?= translate('Every weekday') ?></option></select>
			</div>
			<div class="col-md-5 DNumDays">
				<input required class="form-control" title="1-364" type="text" id="DNumDays" name="DNumDays" value="1" style="display: inline-block;width: auto;"><label style="display: inline-block;"> <?= translate('day(s)') ?></label>
			</div>
			<div class="col-md-3"></div>
		</div>
		<div data-recurtype="2" style="display: none">
			<div class="row">
				<div class="col-md-9"><?= translate("Recur every") ?> <input type="number" min="1" max="52" style="width: 70px;display: inline-block;font-size: 13px;" class="form-control" title="1-52" type="text" required id="WNumWeeks" name="WNumWeeks" value="1"> <?= translate('week(s) on') ?>:</div>
				<div class="col-md-3"></div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<label class="checkbox-inline">
						<input required type="checkbox" data-name="WDays" name="WDaysM" value="1"><?=translate('Monday')?>
					</label>
					<label class="checkbox-inline">
						<input required type="checkbox" data-name="WDays" name="WDaysTu" value="2"><?=translate('Tuesday')?>
					</label>
					<label class="checkbox-inline">
						<input required type="checkbox" data-name="WDays" name="WDaysW" value="3"><?=translate('Wednesday')?>
					</label>
					<label class="checkbox-inline">
						<input required type="checkbox" data-name="WDays" name="WDaysTh" value="4"><?=translate('Thursday')?>
					</label>
					<label class="checkbox-inline">
						<input required type="checkbox" data-name="WDays" name="WDaysF" value="5"><?=translate('Friday')?>
					</label>
					<label class="checkbox-inline">
						<input required type="checkbox" data-name="WDays" name="WDaysSa" value="6"><?=translate('Saturday')?>
					</label>
					<label class="checkbox-inline">
						<input required type="checkbox" data-name="WDays" name="WDaysSu" value="0"><?=translate('Sunday')?>
					</label>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div data-recurtype="8" style="display: none">
			<div class="row">
				<select id="The" style="width: 200px;" class="pull-left" name="The"><option value="1"><?= translate('The') ?></option></select>
				<select id="The_DaysWeek" class="pull-left" style="width: 97px;" name="The_DaysWeek">
					<option value="3"><?= translate('Sunday') ?></option>
					<option value="4"><?= translate('Monday') ?></option>
					<option value="5"><?= translate('Tuesday') ?></option>
					<option value="6"><?= translate('Wednesday') ?></option>
					<option value="7"><?= translate('Thursday') ?></option>
					<option value="8"><?= translate('Friday') ?></option>
					<option value="9"><?= translate('Saturday') ?></option>
				</select><span class="pull-left">&nbsp;<?= translate('Of The') ?>&nbsp;</span>
				<select id="The_FreqOrdinal" style="width: 100px;" name="The_FreqOrdinal" class="pull-left">
					<option value="0"><?= translate('First') ?></option>
					<option value="1"><?= translate('Second') ?></option>
					<option value="2"><?= translate('Third') ?></option>
					<option value="3"><?= translate('Fourth') ?></option>
					<option value="4"><?= translate('Last') ?></option>
				</select>&nbsp;<span class="pull-left">&nbsp;<?= translate('Week') ?></span>
			</div>
<?			//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)?>
			<div class="row<?=(getMDVersion() < esvAgendaFirsDayWeek)?" hidden":''?>">
				<label for="FirstDayOfWeek"><?= translate("First day of week") ?></label>
				<select id="FirstDayOfWeek" style="width: 200px;" class="form-control" name="FirstDayOfWeek">
					<option value="<?=asfwdSunday?>"><?=translate("Sunday")?></option>
					<option value="<?=asfwdMonday?>"><?=translate("Monday")?></option>
				</select>
			</div>
		</div>
		<div data-recurtype="4" style="display: none">
			<div class="row">
				<div class="col-md-9">
					<select class="pull-left" style="display: inline-block;width: 80px;" id="MonthlyOpt" name="MonthlyOpt"><option selected="" value="0"><?= translate("Day") ?></option><option value="1"><?= translate("The") ?></option></select>
					<div id="boxcalendar" class="object_table">
						<input type="text" id="tDaysOfMonthSel" name="tDaysOfMonthSel" value="" style="display:none">
						<table id="tDaysOfMonth" name="tDaysOfMonth" style="">
                            <thead>
                                <tr>
                                    <th colspan="7" class="datepicker-switch" style="text-align: center;background-color: rgb(64, 120, 153);color: white;">
                                        <?= translate('Days of month') ?></th>
                                </tr>
                            </thead>
                            <tbody style="cursor:pointer;border:1px;border-style:solid;">
                                <tr>
                                    <td class="day" id="d1">1</td>
                                    <td class="day" id="d2">2</td>
                                    <td class="day" id="d3">3</td>
                                    <td class="day" id="d4">4</td>
                                    <td class="day" id="d5">5</td>
                                    <td class="day" id="d6">6</td>
                                    <td class="day" id="d7">7</td>
                                </tr>
                                <tr>
                                    <td class="day" id="d8">8</td>
                                    <td class="day" id="d9">9</td>
                                    <td class="day" id="d10">10</td>
                                    <td class="day" id="d11">11</td>
                                    <td class="day" id="d12">12</td>
                                    <td class="day" id="d13">13</td>
                                    <td class="day" id="d14">14</td>
                                </tr>
                                <tr>
                                    <td class="day" id="d15">15</td>
                                    <td class="day" id="d16">16</td>
                                    <td class="day" id="d17">17</td>
                                    <td class="day" id="d18">18</td>
                                    <td class="day" id="d19">19</td>
                                    <td class="day" id="d20">20</td>
                                    <td class="day" id="d21">21</td>
                                </tr>
                                <tr>
                                    <td class="day" id="d22">22</td>
                                    <td class="day" id="d23">23</td>
                                    <td class="day" id="d24">24</td>
                                    <td class="day" id="d25">25</td>
                                    <td class="day" id="d26">26</td>
                                    <td class="day" id="d27">27</td>
                                    <td class="day" id="d28">28</td>
                                </tr>
                                <tr>
                                    <td class="day" id="d29">29</td>
                                    <td class="day" id="d30">30</td>
                                    <td class="day" id="d31">31</td>
                                    <td class="dayoff" id="n0"></td>
                                    <td class="dayoff" id="n1"></td>
                                    <td class="dayoff" id="n2"></td>
                                    <td class="dayoff" id="n3"></td>
                                </tr>
                            </tbody>
                        </table>
					</div>
					<div class="MonthlyOpt01" style="display: none">
						<select class="pull-left" style="display: inline-block;width: 100px;" id="M_The_FreqOrdinal" name="M_The_FreqOrdinal"><option selected="" value="0"><?= translate('First') ?></option><option value="1"><?= translate('Second ') ?></option><option value="2"><?=translate('Third ')?></option><option value="3"><?=translate('Fourth ')?></option><option value="4"><?=translate('Last ')?></option></select>
						<select class="pull-left" style="display: inline-block;width: 140px;" id="M_The_DaysWeek" name="M_The_DaysWeek"><option selected="" value="0"><?= translate('Day') ?></option><option value="1"><?= translate('Weekday') ?></option><option value="2"><?= translate('Weekend day') ?></option><option value="3"><?= translate('Sunday') ?></option><option value="4"><?= translate('Monday') ?></option><option value="5"><?= translate('Tuesday') ?></option><option value="6"><?= translate('Wednesday') ?></option><option value="7"><?= translate('Thursday') ?></option><option value="8"><?= translate('Friday') ?></option><option value="9"><?= translate('Saturday') ?></option></select>
						<label style="width: 70px;text-align: center;" class="pull-left" > <?= translate("of each")?> </label>
						<input class="form-control pull-left" class="form-control" type="text" required id="M_The_NumMonth" name="M_The_NumMonth" value="1" style="width:30px;text-align: center;" class="form-control">
					</div>
					<div class="MonthlyOpt00">
						<label style="width: 70px;text-align: center;" class="pull-left" > <?= translate("of each")?> </label>
						<input class="form-control pull-left" class="form-control" type="text" required id="M_Day_NumMonth" name="M_Day_NumMonth" value="1" style="width:50px;text-align: center;" class="form-control">
					</div>
					<label class="pull-left"> <?= translate("month(s)")?></label>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div data-recurtype="5" style="display: none">
			<div class="row">
				<div class="col-md-12">
					<label class="pull-left"><?= translate("Recur every") ?></label> <input required style="width: 50px;display: inline-block;" class="form-control pull-left" type="text" id="YNumYears" name="YNumYears" value="1"> <label class="pull-left"><?= translate('year(s)') ?></label>
					<select class="pull-left" id="YearlyOpt" style="width: 80px;" name="YearlyOpt"><option selected="" value="0"><?= translate('On') ?></option><option value="1"><?= translate('On the') ?></option></select>
					<div class="YearlyOpt0">
						<input class="form-control pull-left" type="number" required id="Y_On_NumDay" min="1" max="31" name="Y_On_NumDay" value="1" style="width:70px;font-size: 13px;text-align: center;">
						<select class="form-control pull-left" id="Y_On_Months" style="width: 117px;" name="Y_On_Months"><option selected="" value="0"><?= translate('January') ?></option><option value="1"><?= translate('February') ?></option><option value="2"><?= translate('March') ?></option><option value="3"><?= translate('April') ?></option><option value="4"><?= translate('May') ?></option><option value="5"><?= translate('June') ?></option><option value="6"><?= translate('July') ?></option><option value="7"><?= translate('August') ?></option><option value="8"><?= translate('September') ?></option><option value="9"><?= translate('October') ?></option><option value="10"><?= translate('November') ?></option><option value="11"><?= translate('December') ?></option></select>
					</div>
					<div class="YearlyOpt1" style="display: none">
						<select class="form-control pull-left" id="Y_OnThe_FreqOrdinal" style="width: 100px;" name="Y_OnThe_FreqOrdinal"><option selected="" value="0"><?= translate('First') ?></option><option value="1"><?=translate('Second ')?></option><option value="2"><?=translate('Third ')?></option><option value="3"><?=translate('Fourth ')?></option><option value="4"><?=translate('Last ')?></option></select>
						<select class="pull-left" style="display: inline-block;width: 140px;" id="Y_OnThe_Days" name="Y_OnThe_Days"><option selected="" value="0"><?= translate('Day') ?></option><option value="1"><?= translate('Weekday') ?></option><option value="2"><?= translate('Weekend day') ?></option><option value="3"><?= translate('Sunday') ?></option><option value="4"><?= translate('Monday') ?></option><option value="5"><?= translate('Tuesday') ?></option><option value="6"><?= translate('Wednesday') ?></option><option value="7"><?= translate('Thursday') ?></option><option value="8"><?= translate('Friday') ?></option><option value="9"><?= translate('Saturday') ?></option></select>
						<label class="pull-left"> <?= translate('Of') ?> </label>
						<select class="pull-left" id="Y_OnThe_Months" style="width: 117px;" name="Y_OnThe_Months"><option selected="" value="0"><?= translate('January') ?></option><option value="1"><?= translate('February') ?></option><option value="2"><?= translate('March') ?></option><option value="3"><?= translate('April') ?></option><option value="4"><?= translate('May') ?></option><option value="5"><?= translate('June') ?></option><option value="6"><?= translate('July') ?></option><option value="7"><?= translate('August') ?></option><option value="8"><?= translate('September') ?></option><option value="9"><?= translate('October') ?></option><option value="10"><?= translate('November') ?></option><option value="11"><?= translate('December') ?></option></select>
					</div>
				</div>
			</div>
		</div>
    </section>
</form>
