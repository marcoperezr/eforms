<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);

	//$rs = $theRepository->DataADOConnection->Execute("SELECT datasourceID, datasourceName FROM si_sv_datasource WHERE datasourceID NOT IN ( SELECT catalog_id FROM si_sv_checkpoint ) ORDER BY datasourceName ASC");
	$rs = $theRepository->DataADOConnection->Execute("SELECT datasourceID, datasourceName FROM si_sv_datasource ORDER BY datasourceName ASC");

	$sOptions = '<option value=""></option>';

	while($rs && !$rs->EOF)
	{
		$sOptions .= '<option value="'. $rs->fields['datasourceID'] .'">'. $rs->fields['datasourceName'] .'</option>';
		$rs->MoveNext();
	}

?><form role="form">
	<style>
		
		.modal.material form {
			overflow: inherit;
		}

		.alert.alert-danger {
			display: none;
		}
		
		.modal > .modal-dialog > .modal-content .wizard > .content {
			min-height: 310px;
		}
		.selectize-input {
			min-height: 28px;
			padding: 3px 10px;
		}

		.modal-backdrop {
			z-index: 104;
		}

		.modal {
			z-index: 104;
		}

		.modal > .modal-dialog > .modal-content .wizard .selectize-dropdown-content span {
		    display: inline;
		    text-align: inherit;
		    font-size: 14px;
		}

		.modal > .modal-dialog > .modal-content .wizard .selectize-dropdown-content span.caption {
			font-size: 12px;
		}

		.errorinput {
			border-color: #3c763d !important;
    		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    		box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
		}


	</style>
	<div style="position: relative;  top: -59px;  left: 0px;height: 0px;"><div class="alert alert-danger" style="">Error!</div></div>
    <h1><?= translate("Basic info") ?></h1>
    <section data-section="basicinfo">
    	<input type="hidden" id="id" name="id">
		<div class="form-group">
			<label for="name">* <?= translate('Name') ?></label>
		    <input required type="text" class="form-control" id="name" name="name" maxlength="250" required>
		</div>
		<div class="form-group">
			<label for="catalog">* <?= translate('Catalog') ?></label>
		    <select required name="catalog"><?= $sOptions ?></select>
		</div>
    </section>
    <h1><?= translate("Attributes for agendas") ?></h1>
    <section data-section="selectattrs" class="">
    	<div class="table_attrs">
    		<table class="table table-condensed">
    			<thead><tr><th><?= translate('Attributes') ?></th><th><?= translate('Key') ?></th><th><?= translate('Description') ?><th><?= translate('Latitude') ?></th><th><?= translate('Longitude') ?></th></th></tr></thead>
    			<tbody></tbody>
    		</table>
    	</div>    	
    </section>
</form>
