<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../');
	require 'checkCurrentSession.inc.php';
	require 'survey.inc.php';
	chdir($chdirCurrent);

	$rs = $theRepository->ADOConnection->Execute( "SELECT CLA_USUARIO, NOM_LARGO FROM SI_USUARIO WHERE cla_usuario > 0" );

	$sUserOptions = '<option value=""></option>';
	while($rs && !$rs->EOF)
	{
		$sUserOptions .= '<option value="'. $rs->fields['CLA_USUARIO'] .'">'. $rs->fields['NOM_LARGO'] .'</option>';
		$rs->MoveNext();
	}

	$rs = $theRepository->DataADOConnection->Execute( "SELECT id, name FROM si_sv_checkpoint" );

	$sCheckpointOptions = '<option value=""></option>';
	while($rs && !$rs->EOF)
	{
		$sCheckpointOptions .= '<option value="'. $rs->fields['id'] .'">'. $rs->fields['name'] .'</option>';
		$rs->MoveNext();
	}

	$aSurveys = BITAMSurveyCollection::NewInstance($theRepository);

	$nCount = count( $aSurveys->Collection );

	$sSurveys = '';
	for($i=0; $i < $nCount; $i++)
	{
		$sSurveys .= '<option value="'. $aSurveys->Collection[$i]->SurveyID .'">'. $aSurveys->Collection[$i]->SurveyName .'</option>';
	}	

?><form class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-4 control-label" for="newStartDate">* <?= translate('Capture Start Date') ?></label>
		<div class="col-sm-8">
	    	<input readonly="readonly" style="cursor: pointer;width: 403px;background-color: #FFFFFF;" required type="text" class="form-control" id="newStartDate" name="newStartDate" maxlength="250" required>
	    </div>
	</div>
	<div class="form-group">
		<label class="col-sm-4 control-label">* <?= translate('Capture Start Time') ?></label>
		<div class="col-sm-8">
	    	<select id="" name="newStartHour" class="form-control input-sm pull-left" style="width: 200px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select><span class="pull-left"> : </span><select id="newStartMinutes" name="newStartMinutes" class="form-control input-sm pull-left" style="width: 200px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>
	    </div>
	</div>
</form>