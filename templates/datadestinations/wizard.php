<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../');
	require_once 'checkCurrentSession.inc.php';
	require_once 'utils.inc.php';
	chdir($chdirCurrent);

?><form role="form">
	<style>
		.httpRight{
			float: right;
    		width: 45%;
    		border-style: solid;
		    border-width: 2px;
		    border-color: #4285f4;
		    padding: 5px;
		}
		.httpLeft{
			float: left;
    		width: 45%;
    		border-style: solid;
		    border-width: 2px;
		    border-color: #4285f4;
		    padding: 5px;
		}
		.labelTree{
			padding-top: 2%;
		}
		.fa {
		    display: inline-block;
		    font: normal normal normal 14px/1 FontAwesome;
		    font-size: inherit;
		    text-rendering: auto;
		    margin: 3px;
		}
		.fa-folder-o:before {
	    	content: "\f114";
		}
		.selectedTree > div {
			overflow: auto;
			float: left;
			height: 230px;
			width: 700px;
			border: 1px solid #FFFFFF;
			border-radius: 3px;
			margin-right: 10px !important;
		}
		.selectedTree > div > ul {			
			padding: 10px !important;
			margin-right: 10px !important;
		}
		.metisFolder .active > a > .icon-expand-alt:before {
			content: "\f117";
		}

		.metisFolder a, .selectedTree > div > .selectedList {
			.overflow: auto;	
		}

		.metisFolder a, .selectedTree > div > .selectedList > div {
			cursor: pointer;
		}

		.selectedTree > div > .selectedList > div {
			padding: 4px 10px 4px 10px;
			border-bottom: 1px solid #FFFFFF;
		}

		.selectedTree > div > .selectedList > div.old {
			background-color: rgb(229, 229, 229);
		}

		.selectedTree > div > .selectedList > div:hover {
			background-color: rgba(0, 136, 204, 0.66);
			color: #FFFFFF;
		}

		.form-group > .selectedTree .icon-indicator {
			background-image: url('images/indicadordimension.png');
			width: 16px;
			height: 16px;
			display: inline-block;
			vertical-align: middle;
			background-position: 16px; */
		}

		.form-group > .selectedTree .icon-dimension {
			background-image: url('images/indicadordimension.png');
			width: 16px;
			height: 16px;
			display: inline-block;
			vertical-align: middle;
		}

		.selectedTree > div > .selectedList > div i.menu {
			display: none;
		}

		.selectedTree > div > .selectedList > div:hover i.menu {
			background-color: #FFFFFF; 
			border-radius: 50%; 
			color: rgba(0, 0, 0, 1);
			display: block;
		}

		.selectedTree > div > .selectedList > div i.menu:before {
			height: 20px;
			width: 20px;
			text-align: center;
			padding-top: 3px;
		}
		.modal.material form {
			overflow: inherit;
		}

		.alert.alert-danger {
			display: none;
		}
		
		.modal > .modal-dialog > .modal-content .wizard > .content {
			min-height: 310px;
		}
		.selectize-input {
			min-height: 28px;
			padding: 3px 10px;
		}

		.mappingquestions .input-group .form-control {
			width: 140px;
			height: 28px !important;
			border-radius: 4px !important;
			border-bottom-right-radius: 0px !important;
  			border-top-right-radius: 0px !important;
		}
		.mappingquestions .input-group-btn button {
			height: 28px;
		}

		.modal-backdrop {
			z-index: 104;
		}

		.modal {
			z-index: 104;
		}

		.typedestinations label > input {
			visibility: hidden;
			position: absolute;
		}
		.typedestinations label > input + img {
			cursor:pointer;
			border:2px solid transparent;
			padding: 2px;
		}
		.typedestinations label > input:checked + img {
			border: 2px solid #ffcd61;
			border-radius: 4px;
		}

		.signin2 {
			cursor: pointer;
			background-color: #427fed;
		    width: 150px;
		    padding: 5px 10px;
		    color: #FFFFFF;
		    border-radius: 3px;
		}

		.signin2 i {
			margin-right: 10px;
		}

		.errorinput {
			border-color: #F70645 !important;
    		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    		box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
		}

		.modal > .modal-dialog > .modal-content .wizard .selectize-dropdown-content span {
		    display: inline;
		    text-align: inherit;
		    font-size: 14px;
		}

		.modal > .modal-dialog > .modal-content .wizard .selectize-dropdown-content span.caption {
			font-size: 12px;
		}

		.selectize-control.eBavelAppsFields {
			width: 225px;
			display: inline-block;
		}


	</style>
	<div style="position: relative;  top: -59px;  left: 0px;height: 0px;"><div class="alert alert-danger" style="">Error!</div></div>
    <h1><?= translate("Basic info") ?></h1>
    <section data-section="basicinfo">
    	<input type="hidden" id="id_modelartus" name="id_modelartus">
		<div class="form-group">
			<label for="name">* <?= translate('Name') ?></label>
		    <input type="text" class="form-control" id="name" name="name" maxlength="250" required>
		</div>
		<div class="form-group">
			<label for="type">* <?= translate('Type') ?></label>
		    <div class="typedestinations">
				<label>
				  	<input required type="radio" id="type" name="type" value="1" />
					<img src="images/datadestinations/ebavel.png" title="eBavel" >
				</label>
				<label>
				  	<input required type="radio" id="type" name="type" value="3" />
					<img src="images/datadestinations/drive.png" title="Google Drive" >
				</label>
				<label>
				  	<input required type="radio" id="type" name="type" value="2" />
					<img src="images/datadestinations/dropbox.png" title="Dropbox" >
				</label>
				<label>
				  	<input required type="radio" id="type" name="type" value="4" />
					<img src="images/datadestinations/email.png" title="eMail" >
				</label>
				<label>
				  	<input required type="radio" id="type" name="type" value="5" />
					<img src="images/datadestinations/ftp.png" title="FTP" >
				</label>
				<label>
				  	<input required type="radio" id="type" name="type" value="6" />
					<img src="images/datadestinations/http.png" title="HTTP" >
				</label>
			</div>
		</div>
    </section>
    <h1><?= translate("Mapping eBavel") ?></h1>
    <section data-section="mappingebavel" class="mapping_ebavel">
    	<p><?= translate("Relate a question with an eBavel form and field") ?>.</p>
    	<div class="form-group">
			<label for="eBavelAppForm">* <?= translate('Applications and forms eBavel') ?></label>
		    <select required class="form-control" id="eBavelAppForm" name="eBavelAppForm"><option value=""></select>
		</div>
    	<div class="row">
			<div class="col-sm-4"><?= translate('eBavel field') ?></div><div class="col-sm-4"><?= translate('Question') ?></div><div class="col-sm-3"></div><div class="col-sm-1"></div>
		</div>
    	<div class="mappingquestions">
    	</div>
    	<div class="form-horizontal">
	    	
		</div>
    </section>
    <h1><?= translate("Dropbox") ?></h1>
    <section data-section="dropbox">
    	<p><?= translate('Select an Dropbox account or create a new connection') ?></p>
    	<select name="dpxaccount"></select>
    	<br>
    	<input type="hidden" name="access_token">
		<div class="loadingDiv" style="display: none;">
	   		<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
		</div>
    </section>
    <h1><?= translate("Google Drive") ?></h1>
    <section data-section="gdrive">
    	<p><?= translate('Select an GoogleDrive account or create a new connection') ?></p>
    	<select name="gaccount"></select>
    	<input type="hidden" name="access_token">
    	<div class="loadingDiv" style="display: none;">
	   		<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
		</div>
    </section>
    <h1><?= translate("FTP") ?></h1>
    <section data-section="ftpstep">
    	<p><?= translate('Select an FTP account') ?></p>
    	<select name="ftpaccount"></select>
    	<br>
    	<div style="display: none" class="ftpnew">
	    	<p><?= translate('Type a FTP server, user name and password') ?></p>
<?	 	if (getMDVersion()>=esveServicesConnectionName) { ?>
			<div class="form-group">
				<label for="ftpName">* <?= translate('Name') ?></label>
				<input type="text" autocomplete="off"  required requiredisnull="ftpaccount" class="form-control" id="ftpName" name="ftpName">
			</div>
<? 		} ?>
	    	<div class="form-group">
				<label for="ftpServer">* <?= translate('FTP Server') ?></label>
			    <input type="text" pattern="^(ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$" required requiredisnull="ftpaccount" class="form-control" id="ftpServer" name="ftpServer">
			</div>
	    	<div class="form-group">
				<label for="ftpUsername">* <?= translate('User Name') ?></label>
			    <input type="text" required requiredisnull="ftpaccount" class="form-control" id="ftpUsername" name="ftpUsername">
			</div>
			<div class="form-group">
				<label for="ftpPassword"><?= translate('Password') ?></label>
			    <input type="password" class="form-control" id="ftpPassword" name="ftpPassword">
			</div>
		</div>
		<div class="loadingDiv" style="display: none;">
	   		<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
		</div>
    </section>
    <h1><?= translate("HTTP") ?></h1>
    <section data-section="httpstep">
	<p><?= translate('Select an HTTP account') ?></p>
		<select name="httpaccount"></select>
    	<br>
		<div style="display: none" class="httpnew">
			<p><?= translate('Type a HTTP server, user name and password') ?></p>
<?		if (getMDVersion()>=esveServicesConnectionName) { ?>
			<div class="form-group">
				<label for="BDName">* <?= translate('Name') ?></label>
				<input type="text" autocomplete="off"  required requiredisnull="httpaccount" class="form-control" id="httpName" name="httpName">
			</div>
<? 		} ?>
	    	<div class="form-group">
				<label for="httpServer">* <?= translate('Server') ?></label>
			    <input type="text" pattern="^(http)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$" required requiredisnull="ftpaccount" class="form-control" id="httpServer" name="httpServer">
			</div>
	    	<div class="form-group">
				<label for="httpUsername"><?= translate('User Name') ?></label>
			    <input type="text" autocomplete="off" class="form-control" id="httpUsername" name="httpUsername">
			</div>
			<div class="form-group">
				<label for="httpPassword"><?= translate('Password') ?></label>
			    <input type="password" autocomplete="off" class="form-control" id="httpPassword" name="httpPassword">
			</div>
			<div class="form-group">
				<label for="Parameters"><?= translate('Parameters') ?></label>
			    <input type="text" autocomplete="off" class="form-control" id="Parameters" name="Parameters">
			</div>	    	
		</div>
	    <div class="loadingDiv" style="display: none;">
	    	<i class="icon-spinner icon-spin icon-large"></i> <?php echo translate( 'Loading' ) ?>
		</div>
    </section>
    <h1><?= translate("File format") ?></h1>
    <section data-section="fileformatstep">
    	<p>* <?= translate('Select the file format to send') ?></p>
    	<select required name="fileformat">
    		<option value=""></option>
    		<option value="1"><?= translate('CSV File') ?></option>
    		<option value="2"><?= translate('Excel File') ?></option>
    		<option value="3"><?= translate('PDF File') ?></option>
    	</select>
    	<br>
    	<p>* <?= translate('Destination path') ?></p>
    	<input type="text" class="form-control" required name="path" id="path" maxlength="250">
    	<label class="labelTree"><?= translate('Folder tree') ?></label>
    	<div class="selectedTree">
			<div><ul class="metisFolder" data-text="<?= translate('Building folder tree') ?>...", data-textnoopen="<?= translate('You must select a dimension or indicator before opening the next level') ?>"><?= translate( 'Building folder tree' ) ?>...</ul></div>
		</div>
		<div class="type-request" style="display: none;">
			<p><?= translate('Select the type request') ?></p>
	    	<select name="selectRequest">
	    		<option value=""></option>
	    		<option value="1"><?= translate('POST') ?></option>
	    		<option value="2"><?= translate('PUT') ?></option>
	    	</select>
			<? if (getMDVersion()>=esvDataDestHTTPSinglePOST) { ?>
			<div class="checkbox httpSendSinglePOST" style="display: none">
				<label> <input type="checkbox" name="httpSendSinglePOST" value="0"> <?= translate('Send all files on a single POST') ?>. </label>
			</div>
			<? } ?>
		</div>
	</section>
	<? if (getMDVersion()>=esvDataDestination) { ?>
	<h1><?= translate("eMail format") ?></h1>
	<section data-section="emailformatstep">
    	<p>* <?= translate('Select the file format to send') ?></p>
    	<select required name="emailformat">
    		<option value=""></option>
			<option value="1"><?= translate('CSV File') ?></option>
    		<option value="2"><?= translate('Excel File') ?></option>
    		<option value="<?=ddeftPDF?>"><?= translate('PDF File') ?></option>
    		<? if (getMDVersion()>=esvDataDestination) { ?>
    		<option value="<?=ddeftDashboard?>"><?= translate('Dashboard File') ?></option>
    		<option value="<?=ddeftARS?>"><?= translate('ARS File') ?></option>
    		<? } ?>
    	</select>
    	<br>
		<div class="Dashboardemail" style="display: none;">
			<p><?= translate('Select the dashboard') ?></p>
	    	<select name="Dashboardemail"></select>
		</div>
		<div class="arsemail" style="display: none;">
			<p><?= translate('Select the ARS report') ?></p>
	    	<select name="arsemail"></select>
		</div>
		<? if (getMDVersion()>=esvadditionaleMail) { ?>
		<label for="alteMail"> <?= translate('Alternative mail') ?></label>
		<textarea name="alteMail" id="alteMail" rows="4" cols="117"></textarea>
		<? if (getMDVersion()>=esvDataDestEmailComposite) { //MAPR 2019-07-22: Se a?ade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
		?>
		<label for="alteMail"> <?= translate('Form alternative emails from survey answers') ?></label>
		<br>
		<!-- input type="text" autocomplete="off" class="form-control" id="emailcomposite" name="emailcomposite" -->
		<textarea name="emailcomposite" id="emailcomposite" rows="4" cols="117"></textarea>
		<? } //MAPR 2019-07-22: Se a?ade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6) ?>
		<? } ?>
		<input type="text" name="cla_dimension" id="cla_dimension" maxlength="250" style="display: none">
	</section>
	<? } ?>
	<? //@JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
	if (getMDVersion()>=esvDataDestEmailDetail) { ?>
	<h1><?= translate("eMail detail") ?></h1>
	<section data-section="emaildetailstep">
    	<p>* <?= translate('eMail detail') ?></p>
		<div class="form-group">
			<label for="emailsubject"><?= translate('Subject') ?></label>
			<input type="text" autocomplete="off" class="form-control" id="emailsubject" name="emailsubject">
		</div>
    	<br>
		<p><?= translate('Body') ?></p>
		<div class="bodyContainer"><span></span><textarea class="form-control" rows="10" name="emailbody"></textarea>
		</div>
	</section>
	<? } ?>
    <h1><?= translate("Conditions") ?></h1>
    <section data-section="conditions">
    	<p></p>
    	<div class="checkbox">
			<label> <input type="checkbox" name="active" value="1" checked> <?= translate('Enable sending data to the destination source') ?>. </label>
		</div>
		<p><?= translate('Sending condition') ?></p>
		<div class="conditionContainer"><span></span><textarea class="form-control" rows="3" name="condition"></textarea>
		</div>
<? if (getMDVersion()>=esvDestinationIncremental) { ?>
		<div class="checkbox incrementalLoad" style="display: none">
			<label> <input type="checkbox" name="incrementalLoad" value="1"> <?= translate('Activate incremental load') ?>. </label>
		</div>
<? } ?>
    </section>
</form>
