<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);

?><form role="form">
	<style>
		.selectedTree > div {
			overflow: auto;
			float: left;
			height: 230px;
			width: 400px;
			border: 1px solid #FFFFFF;
			border-radius: 3px;
			margin-right: 10px !important;
		}
		.selectedTree > div > ul {			
			padding: 10px !important;
			margin-right: 10px !important;
		}
		.metisFolder .active > a > .icon-expand-alt:before {
			content: "\f117";
		}

		.metisFolder a, .selectedTree > div > .selectedList {
			.overflow: auto;	
		}

		.metisFolder a, .selectedTree > div > .selectedList > div {
			cursor: pointer;
		}

		.selectedTree > div > .selectedList > div {
			padding: 4px 10px 4px 10px;
			border-bottom: 1px solid #FFFFFF;
		}

		.selectedTree > div > .selectedList > div.old {
			background-color: rgb(229, 229, 229);
		}

		.selectedTree > div > .selectedList > div:hover {
			background-color: rgba(0, 136, 204, 0.66);
			color: #FFFFFF;
		}

		.form-group > .selectedTree .icon-indicator {
			background-image: url('images/indicadordimension.png');
			width: 16px;
			height: 16px;
			display: inline-block;
			vertical-align: middle;
			background-position: 16px; */
		}

		.form-group > .selectedTree .icon-dimension {
			background-image: url('images/indicadordimension.png');
			width: 16px;
			height: 16px;
			display: inline-block;
			vertical-align: middle;
		}

		.selectedTree > div > .selectedList > div i.menu {
			display: none;
		}

		.selectedTree > div > .selectedList > div:hover i.menu {
			background-color: #FFFFFF; 
			border-radius: 50%; 
			color: rgba(0, 0, 0, 1);
			display: block;
		}

		.selectedTree > div > .selectedList > div i.menu:before {
			height: 20px;
			width: 20px;
			text-align: center;
			padding-top: 3px;
		}
		.alert.alert-danger {
			display: none;
		}
		
		.modal > .modal-dialog > .modal-content .wizard > .content {
			min-height: 310px;
		}
		.selectize-input {
			min-height: 28px;
			padding: 3px 10px;
		}
		.fields .form-group {
			margin-bottom: 10px;
			width: 50%;
			float: left;
		}
		.modal.material .modal-dialog .modal-content .modal-body .form-group>label {
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
		.modal.material form {
			overflow: inherit;
		}

		.modal .mapping_ebavel h4{
			clear: both;
			margin-left: -22px;
			background-color: rgba(0, 0, 0, 0.03);
			padding: 8px;
			padding-left: 20px;
			margin-right: -70px;
		}
		.form-group > .selectedTree .icon-indicator {
			background-image: url('images/indicadordimension.png');
			width: 16px;
			height: 16px;
			display: inline-block;
			vertical-align: middle;
			background-position: 16px; */
		}

		.form-group > .selectedTree .icon-dimension {
			background-image: url('images/indicadordimension.png');
			width: 16px;
			height: 16px;
			display: inline-block;
			vertical-align: middle;
		}

		.icon-expand-alt.eforms, .icon-expand-alt.ebavel {
			background-repeat: no-repeat;
			background-position-x: 100%;
			background-size: contain;
		}

		.icon-expand-alt.eforms {
			background-image: url(images/iconoform18px.png);
		}

		.icon-expand-alt.ebavel {
			background-image: url(images/iconoebavel18px.png);
		}

		.icon-expand-alt.eforms:before, .icon-expand-alt.ebavel:before {
			width: 32px;
		}

	</style>
	<div style="position: relative;  top: -59px;  left: 0px;height: 0px;"><div class="alert alert-danger" style="">Error!</div></div>
    <h1><?= translate("Basic info") ?></h1>
    <section>
    	<input type="hidden" id="id_modelartus" name="id_modelartus">
		<div class="form-group">
			<label for="name">* <?= translate('Name') ?></label>
		    <input type="text" class="form-control" id="name" name="name">
		</div>
		<div class="form-group">
			<label for="description">* <?= translate('Description') ?></label>
		    <textarea  class="form-control" id="description" name="description"></textarea >
		</div>
    </section>
    <h1><?= translate("Mapping eBavel") ?></h1>
    <section class="mapping_ebavel">
    	<p><?= translate("Optional. Relate a question with an eBavel form and field to be able to select eBavel attributes in the Artus model.") ?></p>
    	<div class="row">
			<div class="col-sm-4"><?= translate('Question') ?></div><div class="col-sm-3"><?= translate('eBavel app') ?></div><div class="col-sm-4"><?= translate('eBavel field') ?></div><div class="col-sm-1"></div>
		</div>
    	<div class="mappingquestions">
    	</div>
    	<div class="form-horizontal">
	    	
		</div>
    </section>
    <h1><?= translate("Dimensions / Indicators") ?></h1>
    <section>
    	<div class="form-group">
			<label>* <?= translate('Indicators and dimensions') ?></label>
			<div class="selectedTree">
				<div><ul class="metisFolder" data-text="<?= translate('Building indicator and dimension tree') ?>...", data-textnoopen="<?= translate('You must select a dimension or indicator before opening the next level') ?>"><?= translate( 'Building indicator and dimension tree' ) ?>...</ul></div>
	            <div><div class="selectedList"></div></div>
			</div>		    
		</div>
		<ul id="contextMenu" class="dropdown-menu" role="menu" style="display:none; width: 200px" >  			
		    <li>
		    	<a tabindex="-1" href="#"><?= translate('Rename')?></a>
		    	<ul class="dropdown-menu">
		    		<li style=""><a tabindex="-1" href="#"><i class="icon-arrow-left"></i> <?= translate('Rename attribute') ?></a></li>
		    		<li style="" role="presentation" class="divider"></li>
		    		<li style="">
						<div style="padding-left: 10px;margin-right: 10px;">
							<input type="text" placeholder="<?= translate('Attribute name')?>" class="form-control input-sm" style="box-sizing: border-box !important;"/>
								<button data-action="1" class="btn btn-basic pull-right" style="background-color: transparent;"><?= translate('Save')?></button>
						</div>
					</li>
		    	</ul>
		    </li>
		    <li><a tabindex="-1" data-action="2" href="#"><?= translate('Change to dimension')?></a></li>
		</ul>
    </section>
</form>
