<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$data = "";
$strPath = "";
$surveyID = -1;
$folioID = -1;
$questionID = -1;
//GCRUZ 2015-09-08. Usar secuencia_reng para identificar si es un renglón de una maestro detalle
$secuencia_reng = (isset($_REQUEST['secuencia_reng']))? intval($_REQUEST['secuencia_reng']) : 0;
$secuencia_col = -1;

//GCRUZ 2015-08-28. Soporte para descarga de múltiples documentos, todos en un ZIP
$arrDocuments = array();
$surveyDocumentsPath = "surveydocuments/".$theRepositoryName."/";
$syncDocumentsPath = "syncdata/".$theRepositoryName."/survey_".$surveyID."/";
if(array_key_exists("surveyID",$_GET) && array_key_exists("folioID",$_GET) && array_key_exists("questionID",$_GET))
{
	$surveyID = $_GET["surveyID"];
	$folioID = $_GET["folioID"];
	$questionID = $_GET["questionID"];
	
	if(isset($_GET["mainFactKey"])) {
	   $theMainFactKey = $_GET["mainFactKey"];
	}
	//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sï¿œla tabla de encuestas, ahora la llave es FactKey
	//$sql = "SELECT escape_foto FROM enc_res_imagen WHERE id_folio = ".$folioID." AND id_encuesta = ".$surveyID." AND id_registro = ".$questionID;
	$sql = "SELECT PathDocument FROM SI_SV_SurveyAnswerDocument WHERE FactKey = ".$folioID." AND SurveyID = ".$surveyID." AND QuestionID = ".$questionID;
	if (isset($_GET["mainFactKey"])) {
		$sql .= " AND MainFactKey = ".$theMainFactKey;
	}
	//GCRUZ 2015-09-08. Usar secuencia_reng para identificar si es un renglón de una maestro detalle
	if ($secuencia_reng > 0)
		$sql .= " AND MainFactKey = ".$secuencia_reng;

	$aRS = $theRepository->DataADOConnection->Execute($sql);
	
	if($aRS) {
		while (!$aRS->EOF) {
			$strPath = $aRS->fields["pathdocument"];
			if (file_exists($surveyDocumentsPath.$strPath)) {
				$document = array();
				$fileName = explode('/', $strPath);
				$document['fullPath'] = $surveyDocumentsPath.$strPath;
				$document['fileName'] = end($fileName);
				$arrDocuments[] = $document;
			}
			elseif (file_exists($syncDocumentsPath.$strPath)) {
				$document = array();
				$fileName = explode('/', $strPath);
				$document['fullPath'] = $syncDocumentsPath.$strPath;
				$document['fileName'] = end($fileName);
				$arrDocuments[] = $document;
			}
			$aRS->MoveNext();
		}
	}
}
//@JAPR 2015-08-31: Corregido un bug, estaba condicionado a que existiera mas de un documento, así que no funcionaba para todos los casos
//GCRUZ 2015-09-08. Si es un sólo documento, no se mete en un zip
if (count($arrDocuments) == 1)
{
	header("Pragma: public");
	header("Expires: 0"); // set expiration time
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
//	if (count($arrCSVFiles) > 1)
//		header("Content-Type: application/zip");
//	else
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Disposition: attachment; filename=\"".$arrDocuments[0]['fileName']."\";");
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ".filesize($arrDocuments[0]['fullPath']));
	readfile($arrDocuments[0]['fullPath']);
}
elseif (count($arrDocuments) > 0) {
	$zip = new ZipArchive;
	//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$zipPath = GeteFormsLogPath();
	//@JAPR
	$zipFile = date('YmdHis').'_DownloadDocuments.zip';
	$zipRes = $zip->open($zipPath.$zipFile, ZipArchive::CREATE);
	if ($zipRes === TRUE) {
		foreach ($arrDocuments as $aDocument)
			$zip->addFile($aDocument['fullPath'], $aDocument['fileName']);
		$zip->close();
	} else {
		die ('failed to open Zip File. ErrCode:'.$zipRes);
	}
	$sfile = $zipFile;
	$spathfile = $zipPath.$zipFile;

	header("Pragma: public");
	header("Expires: 0"); // set expiration time
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
//	if (count($arrCSVFiles) > 1)
//		header("Content-Type: application/zip");
//	else
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Disposition: attachment; filename=\"".$sfile."\";");
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ".filesize($spathfile));
	readfile($spathfile);
}
else {
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
<body style="text-align: center; vertical-align: middle;">
<span><b><?=translate("File not found")?></b></span>
</body>
</html>
<?
}
exit();

?>