<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	if(!is_null($surveyInstance->FactKeyDimID) && $surveyInstance->FactKeyDimID>0)
	{
		//Insertar el campo que representa la dimension FactKey en tabla paralela
		$sql = "ALTER TABLE ".$surveyInstance->SurveyTable." ADD FactKeyDimVal BIGINT AFTER FactKey";
		
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			echo(translate("Error accessing")." ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
		}
		else 
		{
			echo('ALTER TABLE: '.$sql.'<br>');
		}

		$tableDimFactKey = "RIDIM_".$surveyInstance->FactKeyDimID;
		$fieldDimFactKeyKey = $tableDimFactKey."KEY";

		//Ahora se llena el campo FactKeyDimVal con los valores correspondientes de acuerdo a la tabla de hechos
		$sql = "UPDATE ".$surveyInstance->SurveyTable." A, RIFACT_".$surveyInstance->ModelID." B SET A.FactKeyDimVal = B.".$fieldDimFactKeyKey." 
				WHERE A.FactKey = B.FactKey AND A.FactKeyDimVal IS NULL";

		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			echo(translate("Error accessing")." ".$surveyInstance->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
		}
		else 
		{
			echo('UPDATE: '.$sql.'<br>');
		}
	}
}
?>