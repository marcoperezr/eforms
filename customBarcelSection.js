<style type="text/css">
 
.CSSTableGenerator {
 margin:0px;padding:0px;
 width:100%;
 border:1px solid #dadada;
 
 -moz-border-radius-bottomleft:10px;
 -webkit-border-bottom-left-radius:10px;
 border-bottom-left-radius:10px;
 
 -moz-border-radius-bottomright:10px;
 -webkit-border-bottom-right-radius:10px;
 border-bottom-right-radius:10px;
 
 -moz-border-radius-topright:10px;
 -webkit-border-top-right-radius:10px;
 border-top-right-radius:10px;
 
 -moz-border-radius-topleft:10px;
 -webkit-border-top-left-radius:10px;
 border-top-left-radius:10px;
}
.CSSTableGenerator table{
    border-collapse: collapse;
        border-spacing: 0;
 width:100%;
 height:100%;
 margin:0px;padding:0px;
}

.CSSTableGenerator th {
	background-color: #555555;
	border:0px solid #dadada;
	text-align:center;
	border-width:0px 0px 1px 1px;
	font-size:16px;
	font-family:Helvetica;
	font-weight:normal;
	color:#ffffff;
	text-shadow: '';
	height: 41px;
	font-family: Helvetica, Arial, sans-serif /*{global-font-family}*/;
	text-shadow: white 0px 1px 0px;
}

.CSSTableGenerator tr:nth-child(odd) {
	background-color: #d1d8e9;
}

.CSSTableGenerator tr:nth-child(even) {
	background-color: #eaeef4;
}

.CSSTableGenerator tr:last-child td:last-child {
 -moz-border-radius-bottomright:10px;
 -webkit-border-bottom-right-radius:10px;
 border-bottom-right-radius:10px;
}
.CSSTableGenerator table tr:first-child td:first-child {
 -moz-border-radius-topleft:10px;
 -webkit-border-top-left-radius:10px;
 border-top-left-radius:10px;
}
.CSSTableGenerator table tr:first-child td:last-child {
 -moz-border-radius-topright:10px;
 -webkit-border-top-right-radius:10px;
 border-top-right-radius:10px;
}
.CSSTableGenerator tr:last-child td:first-child{
 -moz-border-radius-bottomleft:10px;
 -webkit-border-bottom-left-radius:10px;
 border-bottom-left-radius:10px;
}
.CSSTableGenerator tr:hover td{
 background-color:#cccccc;
}
.CSSTableGenerator td{
 vertical-align:middle;
 /*background-color:#ffffff;*/
 border:1px solid #dadada;
 border-width:0px 1px 1px 0px;
 text-align:left;
 padding:8px;
 font-size:12px;
 font-family:Arial;
 font-weight:normal;
 color:#000000;
}
.CSSTableGenerator tr:last-child td{
 border-width:0px 1px 0px 0px;
}
.CSSTableGenerator tr td:last-child{
 border-width:0px 0px 1px 0px;
}
.CSSTableGenerator tr:last-child td:last-child{
 border-width:0px 0px 0px 0px;
}

.CSSTableGenerator tr:first-child td{
  /*background:-o-linear-gradient(bottom, #999999 5%, #fff 100%); background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #999999), color-stop(1, #fff) );*/
 /*background:-moz-linear-gradient( center top, #999999 5%, #fff 100% );*/
 filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#999999", endColorstr="#fff"); background: -o-linear-gradient(top,#999999,fff);
 
 /*background-color:#999999;*/
 border:0px solid #dadada;
 text-align:center;
 border-width:0px 0px 1px 1px;
 font-size:16px;
 font-family:Helvetica;
 font-weight:normal;
 color:#000000;
}
.CSSTableGenerator tr:first-child:hover td{
 /*background:-o-linear-gradient(bottom, #999999 5%, #fff 100%); background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #999999), color-stop(1, #fff) );*/
 /*background:-moz-linear-gradient( center top, #999999 5%, #fff 100% );*/
 filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#999999", endColorstr="#fff"); background: -o-linear-gradient(top,#999999,fff);
 
 /*background-color:#999999;*/
}
.CSSTableGenerator tr:first-child td:first-child{
 border-width:0px 0px 1px 0px;
}
.CSSTableGenerator tr:first-child td:last-child{
 border-width:0px 0px 1px 1px;
}
</style>
 
<script language="javascript" type="text/javascript">
 
if (objCustomTableBarcel) {
    objCustomTableBarcel.rebuildTable();
} else {

    function CustomTableBarcelCls() {
        this.init();
    }

    CustomTableBarcelCls.prototype.init = function() {
        this.setUseChecked();
        this.setTypes();
        this.opportunitySectionNumber = 19;
        this.setQuestionNumbers();
        this.setOpportunityMapQuestions();
        this.setOptionsScores();
    }

    CustomTableBarcelCls.prototype.setUseChecked = function() {
        this.useChecked = false;
        this.useUnchecked = !this.useChecked;
    }

    CustomTableBarcelCls.prototype.setQuestionNumbers = function() {
        this.qnum = new Array();
        this.qnum[this.Type.Services] = 11;
        this.qnum[this.Type.Category] = new Array(13, 14, 18, 22, 26);
        this.qnum[this.Type.Displays] = new Array(31, 32, 33, 34, 35, 36);
        this.qnum[this.Type.Execution] = new Array(37, 40);
        this.qnum[this.Type.OutOfShock] = 40;
        this.qnum[this.Type.VisibilityInternal] = 42;
        this.qnum[this.Type.VisibilityExternal] = 43;
    }

    CustomTableBarcelCls.prototype.setOptionsScores = function() {
        this.Score = new Array();
        // type of customer
		this.Score[10] = new Array();
		this.Score[10]['Walmart'] = 1;
		this.Score[10]['National accounts'] = 2;
		this.Score[10]['Regional accounts'] = 3;
		this.Score[10]['Clubs'] = 4;
		this.Score[10]['Drugstore'] = 5;
		this.Score[10]['C Store'] = 6;
		this.Score[10]['Gas station'] = 7;
		this.Score[10]['Independant'] = 8;
		this.Score[10]['Dollar family'] = 9;
		//services
		this.Score[11] = new Array();
		//this.Score[11]['Kindly attention'] = 1;
		this.Score[11]['Promotions & new products communication'] = 2;
		this.Score[11]['Execution'] = 3;
		this.Score[11]['Frequency'] = 4;
		this.Score[11]['Product availability'] = 5;
		// barcel
		this.Score[14] = new Array();
		this.Score[14]['Barcel - Cacahuate'] = 1;
		this.Score[14]['Barcel - Extruidos'] = 2;
		this.Score[14]['Barcel - Maiz'] = 3;
		this.Score[14]['Barcel - Papa'] = 4;
		//riconlino
		this.Score[18] = new Array();
		this.Score[18]['Ricolino - Chocolates'] = 1;
		this.Score[18]['Ricolino - Confitados'] = 2;
		this.Score[18]['Ricolino - Gomitas'] = 3;
		this.Score[18]['Ricolino - Untable'] = 4;
		//coronado
		this.Score[22] = new Array();
		this.Score[22]['Coronado - Cajeta'] = 1;
		this.Score[22]['Coronado - D. Trad.'] = 2;
		//vero
		this.Score[26] = new Array();
		this.Score[26]['Vero - Caramelo'] = 1;
		this.Score[26]['Vero - Gomitas'] = 2;
		//displays
		this.Score[31] = new Array();
		this.Score[31]['PME - End Cap'] = 1;
		this.Score[31]['PME - Perimeter'] = 2;
		this.Score[31]['PME - Checkout'] = 3;
		this.Score[31]['PME - In line'] = 4;

		this.Score[32] = new Array();
		this.Score[32]['P. Payaso - End Cap'] = 1;
		this.Score[32]['P. Payaso - Perimeter'] = 2;
		this.Score[32]['P. Payaso - Checkout'] = 3;
		this.Score[32]['P. Payaso - In line'] = 4;

		this.Score[33] = new Array();
		this.Score[33]['Paleton - End Cap'] = 1;
		this.Score[33]['Paleton - Perimeter'] = 2;
		this.Score[33]['Paleton - Checkout'] = 3;
		this.Score[33]['Paleton - In line'] = 4;

		this.Score[34] = new Array();
		this.Score[34]['Cacahuate - End Cap'] = 1;
		this.Score[34]['Cacahuate - Perimeter'] = 2;
		this.Score[34]['Cacahuate - Checkout'] = 3;
		this.Score[34]['Cacahuate - In line'] = 4;

		this.Score[35] = new Array();
		this.Score[35]['BBB - End Cap'] = 1;
		this.Score[35]['BBB - Perimeter'] = 2;
		this.Score[35]['BBB - Checkout'] = 3;
		this.Score[35]['BBB - In line'] = 4;

		this.Score[36] = new Array();
		this.Score[36]['Ric-Vero - End Cap'] = 1;
		this.Score[36]['Ric-Vero - Perimeter'] = 2;
		this.Score[36]['Ric-Vero - Checkout'] = 3;
		this.Score[36]['Ric-Vero - In line'] = 4;
		//execution
		this.Score[37] = new Array();
		this.Score[37]['Cleaning'] = 1;
		this.Score[37]['Out of date'] = 2;
		this.Score[37]['Out of stock'] = 3;
		this.Score[37]['Rotation'] = 4;
		this.Score[37]['Schematics'] = 5;
		//visibility
		this.Score[42] = new Array();
		this.Score[42]['Display'] = 1;
		this.Score[42]['Stopper'] = 2;
		this.Score[42]['Copete'] = 3;
		this.Score[42]['Stickers'] = 4;
		this.Score[42]['Cenefas'] = 5;
		
		this.Score[43] = new Array();
		this.Score[43]['Poster'] = 1;
		this.Score[43]['Placas'] = 2;
		this.Score[43]['Stoppers'] = 3;
		this.Score[43]['Stickers'] = 4;

    }

    CustomTableBarcelCls.prototype.setOpportunityMapQuestions = function() {
        this.MapNum = {
            Type: 0,
            Opportunity: 1,
            Price: 2,
            Responsible: 3,
            Comments: 4,
            SendToActions: 5,
        };
    }

    CustomTableBarcelCls.prototype.setTypes = function() {
        this.Type = {
            Services: 'Services',
            Category: 'Category',
            Displays: 'Displays',
            Execution: 'Execution',
            OutOfShock: 'Out of Shock',
            VisibilityInternal: 'Visibility Internal',
            VisibilityExternal: 'Visibility External'
        };
    }

    CustomTableBarcelCls.prototype.onload = function(anErrorID) {
        this.rebuildTable();
    }

    CustomTableBarcelCls.prototype.rebuildTable = function() {
		try {
            this.buildTableTemplate();
            this.buildTableWithData();
            //this.buildTableWithNewData();
            this.buildAddButton();
            this.refreshMasterDetailData();
			this.paintTable();
        } catch (e) {
            console.log(e.message);
        }
    }

    CustomTableBarcelCls.prototype.buildTableWithData = function() {
        this.arrayOportunities = new Array();
		if(!this.newOpportunities) {
			this.newOpportunities = new Array();
		}
        for (var type in this.Type) {
            var opts = {'questionCheckbox': this.qnum[this.Type[type]], 'type': this.Type[type]};
            var arrayNewTR = new Array();
            arrayNewTR = this.rebuildType(opts);
            if (arrayNewTR.length) {
				var oplength = 0;
                for (nkey in arrayNewTR) {
					arrayNewTR[nkey].gIndex = oplength++;
					aIndex = arrayNewTR[nkey].id + '_' + arrayNewTR[nkey].index
                    this.arrayOportunities[aIndex] = arrayNewTR[nkey];
                }
            }
        }
    }
	
	CustomTableBarcelCls.prototype.paintTable = function() {
		this.paintOportunities(this.arrayOportunities);
		this.paintOportunities(this.newOpportunities, true);
	}
	
	CustomTableBarcelCls.prototype.paintOportunities = function(arrayOportunities, isNewOpportunity) {
		var arrayNewTR = arrayOportunities;
		var $customTable = $('#CustomTableBarcel');
		var isNew = (isNewOpportunity) ? isNewOpportunity : false;
		for(var ktr in arrayNewTR) {
			var type = arrayNewTR[ktr].type;
			var opportunity = arrayNewTR[ktr].opportunity;
			var responsible = arrayNewTR[ktr].responsible;
			var price = arrayNewTR[ktr].price;
			var comments = arrayNewTR[ktr].comments;
			var qID = arrayNewTR[ktr].id;
			arrayNewTR[ktr].index = ktr;
			$customTable.append(this.getNewTR({id: qID, index: ktr, type: type, opportunity: opportunity, responsible: responsible, price: price, comments: comments, isNew: isNew}));
		}
	}

	/*
    CustomTableBarcelCls.prototype.buildTableWithNewData = function() {
		if(!this.newOpportunities) {
			this.newOpportunities = new Array();
		}
        for (var nkey in this.newOpportunities) {
            var opts = {'questionCheckbox': this.qnum[this.Type[type]], 'type': this.Type[type]};
            var arrayNewTR = new Array();
            var newOp = this.newOpportunities[nkey];
			arrayNewTR[nkey] = {};
			arrayNewTR[nkey].type = newOp.type;
			arrayNewTR[nkey].opportunity = newOp.opportunity;
			arrayNewTR[nkey].price = newOp.price;
			arrayNewTR[nkey].responsible = newOp.responsible;
			arrayNewTR[nkey].comments = newOp.comments;
			arrayNewTR[nkey].index = newOp.index;
			arrayNewTR[nkey].id = newOp.id;
            if (arrayNewTR.length) {
				var oplength = 0;
                for (nkey in arrayNewTR) {
					arrayNewTR[nkey].gIndex = oplength++;
					aIndex = arrayNewTR[nkey].id + '_' + arrayNewTR[nkey].index
                    this.newOportunities[aIndex] = arrayNewTR[nkey];
                }
            }
        }
    }*/
	
	CustomTableBarcelCls.prototype.addNewTR = function() {
		var $customTable = $('#CustomTableBarcel');
		if(!this.newOpportunities) {
			this.newOpportunities = new Array();
		}
		var newid = this.newOpportunities.length;
		var qID = newid;
		var ktr = newid;
		this.newOpportunities[newid] = {};		
		$customTable.append(this.getNewTR({id: qID, index: ktr, type: '', opportunity: '', responsible: objSettings.user, price: '', comments: '', isNew: true}));
		$('#Section_' + (this.opportunitySectionNumber - 1) + '_html').trigger('create');
		var mdIndex = (Object.keys(this.arrayOportunities).length)+newid;
		var objMDSection = selSurvey.sections[selSurvey.sectionsOrder[(this.opportunitySectionNumber - 1)]];
		//objMDSection.clearData();
		objMDSection.setActive(mdIndex);
	}

    CustomTableBarcelCls.prototype.buildAddButton = function() {
		var $addButton = $('<button/>', {'type': 'button', 'id': 'customAdd', 'class': 'addBtn', 'text': 'Agregar', 'data-inline':'true' });
		var $addDiv = $('<div/>', { 'style':'float:right;' }).append($addButton);
		$addButton.on('click', $.proxy(function() {
			this.customTable.addNewTR();
		}, { customTable: this} ));
		$addDiv.insertAfter('#CustomTableBarcel');
    }

	CustomTableBarcelCls.prototype.refreshOpportunities = function(arrayOportunities, objMDSection, isNewOpportunity) {
		var opportunitySection = selSurvey.sections[selSurvey.sectionsOrder[(this.opportunitySectionNumber - 1)]];
        var opQuestions = opportunitySection.questions;
        //por cada oportunidad debe existir un registro en la maestro - detalle
		var isNew = (isNewOpportunity) ? isNewOpportunity : false;
		var iOp = 0;
        for (var akey in arrayOportunities) {
			if(isNew) {
				var iOp =(Object.keys(this.arrayOportunities).length)+parseInt(akey);
			}
            //Crea el nuevo registro de la maestro-detalle
            objMDSection.setActive(iOp);
            var aOpportunity = arrayOportunities[akey];
            var type = aOpportunity.type;
            var opportunity = aOpportunity.opportunity;
            var price = aOpportunity.price;
            var responsible = aOpportunity.responsible;
            var comments = aOpportunity.comments;
            var sendToActions = true;
            //por cada campo de la maestro-detalle
            for (var mkey in this.MapNum) {
                var qOrder = this.MapNum[mkey];
                var objMapQuestion = opportunitySection.questions[opportunitySection.questionsOrder[qOrder]];
                if (objMapQuestion) {
                    var value = '';
                    switch (qOrder) {
                        case this.MapNum.Type:
                            value = type;
                            break;
                        case this.MapNum.Opportunity:
                            value = opportunity;
                            break;
                        case this.MapNum.Price:
                            value = price;
                            break;
                        case this.MapNum.Responsible:
                            value = responsible;
                            break;
                        case this.MapNum.Comments:
                            value = comments;
                            break;
                        case this.MapNum.SendToActions:
                            value = 'Si';
                            break;
                    }
                    objMapQuestion.answers[iOp].setValue(value);
                }
            }
			iOp++;
        }
	}
	
    CustomTableBarcelCls.prototype.refreshMasterDetailData = function() {
		//Primero hace la limpieza de la sección
        var objMDSection = selSurvey.sections[selSurvey.sectionsOrder[(this.opportunitySectionNumber - 1)]];
        objMDSection.clearData();
		this.refreshOpportunities(this.arrayOportunities, objMDSection);
		this.refreshOpportunities(this.newOpportunities, objMDSection, true);
		if(((Object.keys(this.arrayOportunities).length) + this.newOpportunities.length) > 0) {
			var objMDSection = selSurvey.sections[selSurvey.sectionsOrder[(this.opportunitySectionNumber - 1)]];
			objMDSection.newRecord = false;
			objMDSection.empty = 0;
		}
    }
	
    CustomTableBarcelCls.prototype.buildTableTemplate = function() {
        var divTable = $('<div/>').append(
			$('<table/>', {'id': 'CustomTableBarcel', 'class': 'CSSTableGenerator'}).append(
				$('<tbody/>').append(
					$('<tr/>').append(
						$('<th/>'),
						$('<th/>', {'text': 'Type'}),
						$('<th/>', {'text': 'Opportunity'}),
						$('<th/>', {'text': 'Value'}),
						$('<th/>', {'text': 'Responsible'}),
						$('<th/>', {'text': 'Comments'})
					)
				)
			)
		);
        $('#Section_' + (this.opportunitySectionNumber - 1) + '_html').html();
        $('#Section_' + (this.opportunitySectionNumber - 1) + '_html').append(divTable);
    }
	
	CustomTableBarcelCls.prototype.remove = function(target) {
		var elimbtn = $(target);
		var deleteIndex = elimbtn.attr('opindex');
		var objMDSection = selSurvey.sections[selSurvey.sectionsOrder[(this.opportunitySectionNumber - 1)]];
		var irecord = -1;
		for(var key in this.arrayOportunities) {
			if(deleteIndex == key) {
				irecord = key;
				break;
			}
		}
		if(irecord == -1) {
			for(var key in this.newOpportunities) {
				if(deleteIndex == key) {
					irecord = (Object.keys(this.arrayOportunities).length)+parseInt(key);
					break;
				}
			}
			this.newOpportunities.splice(deleteIndex,1);
		} else {
			//delete this.arrayOportunities[deleteIndex];
		}
		objMDSection.removeRecord(irecord);
		elimbtn.parent().remove();
		
		if(((Object.keys(this.arrayOportunities).length) + this.newOpportunities.length) < 0) {
			objMDSection.empty = 1;
		}
	}
	
	CustomTableBarcelCls.prototype.edit = function(target) {
		var editInput = $(target);
		var editIndex = editInput.attr('opindex');
		var newValue = editInput.val();
		var column = editInput.attr('col');
		this.newOpportunities[editIndex][column] = newValue;
		this.refreshMasterDetailData();
	}
	
	CustomTableBarcelCls.prototype.attachEditEvent = function($target) {
		$target.on('focusout', $.proxy(function() {
			this.customTable.edit(event.target);
		}, { customTable: this }));
	}

    CustomTableBarcelCls.prototype.getNewTR = function(opts) {
        var qID = (opts.id) ? opts.id : 0;
        var i = opts.index;
        var type = (opts.type) ? opts.type : '';
        var opportunity = (opts.opportunity) ? opts.opportunity : '';
        var responsible = (opts.responsible) ? opts.responsible : '';
        var comments = (opts.comments) ? opts.comments : '';
        var price = (opts.price) ? opts.price : '';
		var isNew = (opts.isNew) ? opts.isNew : false;
		
		if(isNew) {
			var $elimBtn = $('<td/>', { 'opIndex': i, 'id': ('q' + qID + 'elim' + i) }).append(
				$('<button/>', {'type': 'button', 'id': ('btn' + qID + 'elim' + i), 'class': 'elimBtn', 'text': 'Eliminar'})
			);
			$elimBtn.on('click', $.proxy(function() {
				this.customTable.remove(event.currentTarget);
			}, { customTable: this }));
		} else {
			var $elimBtn = $('<td/>');
		}
        
        var $type = $('<td/>', {'id': ('q' + qID + 'type' + i), 'text': ((isNew) ? '' : type) });
        var $opportunity = $('<td/>', {'id': ('q' + qID + 'oportunity' + i), 'style': 'text-align: right;', 'text': ((isNew) ? '' : opportunity) });
		var $price = $('<td/>', {'id': ('q' + qID + 'price' + i), 'style': 'text-align: right;', 'text': ((isNew) ? '' : price) });
        var $responsible = $('<td/>', {'id': ('q' + qID + 'responsible' + i), 'style': 'text-align: right;', 'text': ((isNew) ? '' : responsible) });
        var $comments = $('<td/>', {'id': ('q' + qID + 'comments' + i), 'style': 'text-align: right;', 'text': ((isNew) ? '' : comments) });
		
		if(isNew) {
			var style = 'width: 94%; height: 100%; text-align: right;';
			$type.append($('<input/>', { 'col': 'type', 'opIndex': i, 'type': 'text', 'id': ('q'+qID+'type'+i), 'style': style, 'value': type }));
			$opportunity.append($('<input/>', { 'col': 'opportunity', 'opIndex': i, 'type': 'text', 'id': ('q'+qID+'oportunity'+i), 'style': style, 'value': opportunity }));
			$price.append($('<input/>', { 'col': 'price', 'opIndex': i, 'type': 'number', 'id': ('q'+qID+'price'+i), 'style': style, 'value': price }));
			$comments.append($('<input/>', { 'col': 'comments', 'opIndex': i, 'type': 'text', 'id': ('q'+qID+'comments'+i), 'style': style, 'value': comments }));
			//attach events
			this.attachEditEvent($type);
			this.attachEditEvent($opportunity);
			this.attachEditEvent($price);
			this.attachEditEvent($comments);
		}
		
        var newTR = $('<tr/>', { 'id': ('q' + qID + 'tr' + i)}).append(
			$elimBtn, $type, $opportunity, $price, $responsible, $comments
		);
        return newTR;
    }
	
	CustomTableBarcelCls.prototype.rebuildTypeExecution = function(opts) {
		var arrayNewTR = new Array();
		if (opts.type == this.Type.Execution) {
			var arrUnselected = new Array();
			var arrOptionsUnselectedLength = 0;
			var qID = selSurvey.questions[selSurvey.questionsOrder[(opts.questionCheckbox[0] - 1)]].id;
			var qnum = opts.questionCheckbox[0];
			var section = selSurvey.questions[qID].section;
            var qAnswers = selSurvey.questions[qID].answers;
			var arrOptions = section.dynamicValues;
			for(var akey in qAnswers) {
				if(section.dynamicValues.length) {
					var anAnswer = qAnswers[akey].answer;
					if(section.dynamicValues[akey].value == 'Out of stock' && (anAnswer == 'Yes' || anAnswer == 'Sí')) {
						var arrStockAnswers = selSurvey.questions[selSurvey.questionsOrder[(this.qnum[this.Type.OutOfShock]-1)]].answer.multiAnswer;
						for(var skey in arrStockAnswers) {
							arrUnselected[arrOptionsUnselectedLength] = {};
							arrUnselected[arrOptionsUnselectedLength].type = this.Type.Execution;
							arrUnselected[arrOptionsUnselectedLength].opportunity = skey;
							var score = (arrStockAnswers[skey]) ? arrStockAnswers[skey] : '';
							arrUnselected[arrOptionsUnselectedLength].price = score;
							arrUnselected[arrOptionsUnselectedLength].responsible = objSettings.user;
							arrUnselected[arrOptionsUnselectedLength].comments = '';
							arrUnselected[arrOptionsUnselectedLength].index = i;
							arrUnselected[arrOptionsUnselectedLength].id = qID;
							arrayNewTR.push(arrUnselected[arrOptionsUnselectedLength]);
							arrOptionsUnselectedLength++;
						}
					} else {
						if(anAnswer == 'Yes' || anAnswer == 'Sí') {
							var opportunity = section.dynamicValues[akey].value;
							arrUnselected[arrOptionsUnselectedLength] = {};
							arrUnselected[arrOptionsUnselectedLength].type = opts.type;
							arrUnselected[arrOptionsUnselectedLength].opportunity = opportunity;
							var score = (this.Score[qnum] && this.Score[qnum][opportunity]) ? (this.Score[qnum][opportunity]) : '';
							arrUnselected[arrOptionsUnselectedLength].price = score;
							arrUnselected[arrOptionsUnselectedLength].responsible = objSettings.user;
							arrUnselected[arrOptionsUnselectedLength].comments = selSurvey.questions[selSurvey.questionsOrder[(qnum)]].answers[akey].answer;
							arrUnselected[arrOptionsUnselectedLength].index = i;
							arrUnselected[arrOptionsUnselectedLength].id = qID;
							arrayNewTR.push(arrUnselected[arrOptionsUnselectedLength]);
							arrOptionsUnselectedLength++;
						}
					}
				}
			}
		}
		return arrayNewTR;
	}

    CustomTableBarcelCls.prototype.rebuildType = function(opts) {
        //Obtenemos la pregunta que vamos a evaluar para sacar los elementos de la pregunta que no esten checados
        //questionCheckbox representa cada una de las variables (this.servicesQuestionNumber, etc.) para obtener los checkbox
        //si lo que se obtiene es un array (como this.categoryQuestionNumber) eso significa que se debe evaluar cada una de
        //esas preguntas para obtener los TR a mostrar para ese type
        var arrayNewTR = new Array();
        if (opts.type == this.Type.Execution || opts.type == this.Type.OutOfShock) {
            return this.rebuildTypeExecution(opts);
        }
        if ($.isArray(opts.questionCheckbox)) {
            var qID = selSurvey.questions[selSurvey.questionsOrder[(opts.questionCheckbox[0] - 1)]].id;
			var qnum = opts.questionCheckbox[0];
        } else {
            var qID = selSurvey.questions[selSurvey.questionsOrder[(opts.questionCheckbox - 1)]].id;
			var qnum = opts.questionCheckbox;
        }
        var arrUnselected = new Array();
        var arrOptionsUnselectedLength = 0;
        var arrOptions = Object.keys(selSurvey.questions[qID].options);
        var arrAnswers = new Array();
        if (selSurvey.questions[qID].type == Type.multiplechoice) {
            arrAnswers = Object.keys(selSurvey.questions[qID].answer.multiAnswer);
        }
        for (var i = 0; i < arrOptions.length; i++) {
            var option = arrOptions[i];
            var isValidOption = false;
            if (selSurvey.questions[qID].type == Type.multiplechoice) {
                isValidOption = (this.useChecked) ? ($.inArray(option, arrAnswers) != -1) : ($.inArray(option, arrAnswers) == -1);
            } else {
                if (selSurvey.questions[qID].answer == 'No') {
                    isValidOption = true;
                }
				if(opts.type == this.Type.Execution) {
					if (selSurvey.questions[qID].section.type == SectionType.Inline && (selSurvey.questions[qID].answer.answer == 'Yes' || selSurvey.questions[qID].answer.answer == 'Sí')) {
						isValidOption = true;
					}
				}
            }
            if (isValidOption) {
                var aType = opts.type;
                if (aType == this.Type.OutOfShock) {
                    aType = this.Type.Execution;
                }
                var score = (this.Score[qnum] && this.Score[qnum][option]) ? (this.Score[qnum][option]) : '';
                var objQuestion = selSurvey.questions[qID];
                arrUnselected[arrOptionsUnselectedLength] = {};
                arrUnselected[arrOptionsUnselectedLength].type = aType;
                if (aType == this.Type.Displays) {
                    arrUnselected[arrOptionsUnselectedLength].opportunity = selSurvey.questions[qID].name + ' - ' + option;
                } if(aType == this.Type.Execution) {
					option = selSurvey.questions[qID].section.dynamicValues[i].value;
					arrUnselected[arrOptionsUnselectedLength].opportunity = option;
				} else {
                    arrUnselected[arrOptionsUnselectedLength].opportunity = option;
                }
                arrUnselected[arrOptionsUnselectedLength].price = score;
                arrUnselected[arrOptionsUnselectedLength].responsible = objSettings.user;
                arrUnselected[arrOptionsUnselectedLength].comments = '';
                arrUnselected[arrOptionsUnselectedLength].index = i;
				arrUnselected[arrOptionsUnselectedLength].id = qID;
                arrOptionsUnselectedLength++;
            }
        }
        //arrUnselected tiene los elementos desmarcados del unico o primer pregunta de checkbox
        for (var i = 0; i < arrOptionsUnselectedLength; i++) {
            //Esta parte aplica solo para secciones inline (Category, Execution)
            if (opts.type == this.Type.Category) {
                //Si se trata de un array de preguntas entonces debemos recorrer las siguientes preguntas
                //p.ej. (Para el type Category, para la primera pregunta = 13 puede llegar en arrUnselected las opciones desmarcadas de
                //Barcel y Ricolino, por lo que cuando j = 1 entonces se obtiene que el nextQID sera 14
                //p.ej. (Para Execution
                for (var j = 1; j < opts.questionCheckbox.length; j++) {
					if(opts.type == this.Type.Category && arrUnselected[i].index != (j-1)) {
						continue;
					}
                    var nextQID = selSurvey.questions[selSurvey.questionsOrder[(opts.questionCheckbox[j] - 1)]].id;
					var qnum = opts.questionCheckbox[j];
                    //Si se trata del type Category, el index de arrUnselected, debe coincidir con la posicion en el array de questionCheckBox
                    //por lo que opts.questionCheckbox[1] representa los checkbox que se deben validar en la seccion Barcel category
                    //y si existe arrUnselected[X].index en el questionCheckBox significa que hay que checar esa pregunta
                    var aType = opts.type;
                    //recorremos la respuesta de la inline para saber que checkbox de la seccion (Barcel, Ricolino, etc) estan desmarcadas
                    var qAnswers = selSurvey.questions[nextQID].answers;
                    for (var qkey in qAnswers) {
                        //la pregunta se muestra como un checkbox pero internamente se comporta como un radio
                        //Si la respuesta esta desmarcada se guarda un Yes, si esta marcado un No
                        //if (qAnswers[qkey] && qAnswers[qkey].answer == qAnswers[qkey].options[0]) {
						if (qAnswers[qkey] && qAnswers[qkey].answer == 'Yes') {
                            //Si esta desmarcado entonces se debe generar un TR por esa opcion
                            var section = selSurvey.questions[nextQID].section;
                            var aOpportunity = section.dynamicValues[qkey].value;
                            if (aType == this.Type.Execution) {
                                if (aOpportunity == 'Out of stock') {
                                    this.doOutOfShock = true;
                                    continue;
                                }
                            }
							var responsible = '';
							var comments = '';
                            if (aType == this.Type.Category) {
								aOpportunity = (arrUnselected[i].opportunity + ' - ' + aOpportunity);
								responsible = selSurvey.questions[selSurvey.questionsOrder[(qnum+1)]].answers[qkey].answer;
								//if(selSurvey.questions[selSurvey.questionsOrder[(qnum+1)]].answers[qkey] && responsible == selSurvey.questions[selSurvey.questionsOrder[(qnum+2)]].answers[qkey].answer) {
								comments = selSurvey.questions[selSurvey.questionsOrder[(qnum+2)]].answers[qkey].answer;
                            }
							var score = (this.Score[qnum] && this.Score[qnum][aOpportunity]) ? (this.Score[qnum][aOpportunity]) : '';
                            arrayNewTR.push({
                                'type': aType, 'opportunity': aOpportunity, 'responsible': responsible,
                                'comments': comments, 'price': score
                            });
                        }
                    }
                }
            } else {
                arrayNewTR.push(arrUnselected[i]);
            }
        }
        //En el paso anterior solo la primer pregunta de Displays fue procesada, por lo que se debe hacer lo mismo para las demas preguntas
        //del array, ya que no es una inline pero si se trata como un array de preguntas ya que son varias las que conforman el type Displays
        if (opts.type == this.Type.Displays) {
            for (var j = 1; j < opts.questionCheckbox.length; j++) {
                var nextQID = selSurvey.questions[selSurvey.questionsOrder[(opts.questionCheckbox[j] - 1)]].id;
                var objQuestion = selSurvey.questions[nextQID];
                var arrUnselected = new Array();
                var arrOptionsUnselectedLength = 0;
                var arrOptions = Object.keys(objQuestion.options);
                if (selSurvey.questions[nextQID].type == Type.multiplechoice) {
                    arrAnswers = Object.keys(selSurvey.questions[nextQID].answer.multiAnswer);
                }
                for (var i = 0; i < arrOptions.length; i++) {
                    var option = arrOptions[i];
                    var isValidOption = false;
                    if (selSurvey.questions[qID].type == Type.multiplechoice) {
                        isValidOption = (this.useChecked) ? ($.inArray(option, arrAnswers) != -1) : ($.inArray(option, arrAnswers) == -1);
                    } else {
                        if (selSurvey.questions[qID].answer == 'No') {
                            isValidOption = true;
                        }
                    }
                    if (isValidOption) {
                        arrUnselected[arrOptionsUnselectedLength] = {};
                        arrUnselected[arrOptionsUnselectedLength].type = this.Type.Displays;
                        arrUnselected[arrOptionsUnselectedLength].opportunity = objQuestion.name + ' - ' + option;
                        arrUnselected[arrOptionsUnselectedLength].responsible = objSettings.user;
                        arrUnselected[arrOptionsUnselectedLength].comments = '';
                        arrUnselected[arrOptionsUnselectedLength].index = i;
						arrUnselected[arrOptionsUnselectedLength].id = qID;
                        arrayNewTR.push(arrUnselected[arrOptionsUnselectedLength]);
                        arrOptionsUnselectedLength++;
                    }
                }
            }
        }
        return arrayNewTR;
    }

    var objCustomTableBarcel = new CustomTableBarcelCls();
    objCustomTableBarcel.onload();
}
 
</script>