	//Variable booleana que indica si el navegador es IE
	var bIsIE = navigator.userAgent.indexOf("MSIE") != -1;
	
	var MONTH_REGEXP = '(';
	for (var _i = MONTH_NAMES.length - 1; _i >= 0; _i--)
	{
		if (_i == MONTH_NAMES.length - 1)
		{
			MONTH_REGEXP += MONTH_NAMES[_i];
		}
		else
		{
			MONTH_REGEXP += ('|' + MONTH_NAMES[_i]);
		}
		MONTH_REGEXP += ('|' + MONTH_ABBR[_i]);
		if ((_i + 1) < 10)
		{
			MONTH_REGEXP += ('|0' + (_i + 1));
		}
		MONTH_REGEXP += ('|' + (_i + 1));
	}
	MONTH_REGEXP += ')?';
	
	function GetElementsByName(aForm, aName)
	{
		 var elements = new Array();
		 var n = aForm.elements.length;
		 for (var i = 0; i < n; i++)
		 {
		  if (aForm.elements[i].name == aName)
		  {
		   elements[elements.length] = aForm.elements[i];
		  }
		 }
		 return elements;
	}
	 
	function GetElementsById(aForm, anID)
	{
		 var elements = new Array();
		 var n = aForm.elements.length;
		 for (var i = 0; i < n; i++)
		 {
		  if (aForm.elements[i].id == anID)
		  {
		   elements[elements.length] = aForm.elements[i];
		  }
		 }
		 return elements;
	}

	function DateFromDateString(aDateString, aFormat)
	{
		var unformatedString = unformatDate(aDateString, aFormat);
		var year = new Number(unformatedString.substring(0, 3));
		var month = new Number(unformatedString.substring(5, 6)) - 1;
		var day = new Number(unformatedString.substring(8, 9));
		return new Date(year, month, day);
	}

	function DateAsDateString(aDate, aFormat)
	{
		var aDateString = aDate.getFullYear().toString() + '-' +
						  (aDate.getMonth() + 1).toString() + '-' +
						  aDate.getDate().toString();
		return formatDate(aDateString, aFormat);
	}

	function DateFromDateTimeString(aDateTimeString, aFormat)
	{
		var unformatedString = unformatDateTime(aDateTimeString, aFormat);
		var year = new Number(unformatedString.substring(0, 3));
		var month = new Number(unformatedString.substring(5, 6)) - 1;
		var day = new Number(unformatedString.substring(8, 9));
		var hours = new Number(unformatedString.substring(11, 12));
		var minutes = new Number(unformatedString.substring(14, 15));
		var seconds = new Number(unformatedString.substring(17, 18));
		return new Date(year, month, day, hours, minutes, seconds);
	}

	function DateAsDateTimeString(aDate, aFormat)
	{
		var aDateTimeString = aDate.getFullYear().toString() + '-' +
						  (aDate.getMonth() + 1).toString() + '-' +
						  aDate.getDate().toString() + ' ' +
						  aDate.getHours().toString() + ':' +
						  aDate.getMinutes().toString() + ':' +
						  aDate.getSeconds().toString();
		return formatDateTime(aDateTimeString, aFormat);
	}

	function NumberFromNumberString(aNumberString, aFormat)
	{
		var unformatedNumber = unformatNumber(aNumberString, aFormat);
		return new Number(unformatedNumber);
	}

	function NumberAsNumberString(aNumber, aFormat)
	{
		return formatNumber(aNumber, aFormat);
	}

	function validateFormat(anElement, aRegExpStr, aKeyCode)
	{
		if (aRegExpStr == '')
		{
			return true;
		}
	   	var source;
	   	var textRange = anElement.createTextRange;
		if (textRange == null)
  		{
	    	source = anElement.value + String.fromCharCode(aKeyCode);
		}
  		else
		{
    		textRange = document.selection.createRange().duplicate();
			var oldValue = anElement.value;
			var bookmark = textRange.getBookmark();
			textRange.text = textRange.text.charAt(textRange.text.length - 1) != ' ' ? String.fromCharCode(aKeyCode) : String.fromCharCode(aKeyCode) + ' ';
			source = anElement.value;
			anElement.value = oldValue;
			textRange.moveToBookmark(bookmark);
			textRange.select();
  		}
		var regExp = new RegExp(aRegExpStr, 'i');
	   	var result = source.match(regExp);
	   	return ((result != null) && (result[0].length == source.length)) ;
	}
	function unformatStringRegExpStr(aFormat)
	{
 		var regExpStr = '';
		for (var i = 0; i < aFormat.length; i++)
		{
			var ch = aFormat.charAt(i);
			if (ch == '\\')
			{
				i++;
				if (i < aFormat.length)
				{
					ch = aFormat.charAt(i);
					switch (ch)
					{
						case '\\':
							regExpStr += '(\\\\?)';
							break;
						case '$':
							regExpStr += '(\\' + ch + '?)';
						    break;
						case '^':
						case '*':
						case '+':
						case '?':
						case '{':
						case '}':
						case '!':
						case '(':
						case ')':
						case '[':
						case ']':
						case '.':
							regExpStr += '(\\' + ch + '?)';
						    break;
						default:
							regExpStr += '(' + ch + '?)';
						    break;
					}
				}
			}
			else
			{
				switch (ch)
				{
					case '$':
						regExpStr += '(\\' + ch + '?)';
					    break;
					case '^':
					case '*':
					case '+':
					case '?':
					case '{':
					case '}':
					case '!':
					case '(':
					case ')':
					case '[':
					case ']':
					case '.':
						regExpStr += '(\\' + ch + '?)';
					    break;
					case '#':
					    regExpStr += '([0-9]*)';
						break;
					case '0':
						regExpStr += '([0-9]?)';
						break;
					case 'A':
					    regExpStr += '([A-Z]*)';
						break;
					case 'a':
						regExpStr += '([A-Z]?)';
						break;
					case 'N':
					    regExpStr += '([0-9A-Z]*)';
						break;
					case 'n':
					    regExpStr += '([0-9A-Z]?)';
						break;
					case 'W':
					    regExpStr += '([0-9A-Z_]*)';
						break;
					case 'w':
						regExpStr += '([0-9A-Z_]?)';
						break;
					case 'F':
					    regExpStr += '([\]\[\!\¡\#\$\%\&\'\(\)\+\,\-\.\;\=\@\\\\\ 0-9A-ZñÑáÁéÉíÍóÓúÚ_]*)';
						break;
					case 'f':
					    regExpStr += '([\]\[\!\¡\#\$\%\&\'\(\)\+\,\-\.\;\=\@\\\\\ 0-9A-ZñÑáÁéÉíÍóÓúÚ_]?)';
						break;
					default:
						regExpStr += '(' + ch + '?)';
					    break;
				}
			}
		}
		return regExpStr;
	}
	function formatStringRegExpStr(aFormat)
	{
 		var regExpStr = '';
 		var inWord = false;
		for (var i = 0; i < aFormat.length; i++)
		{
			var ch = aFormat.charAt(i);
			if (ch == '\\')
			{
				i++;
				if (i < aFormat.length)
				{
					ch = aFormat.charAt(i);
					switch (ch)
					{
						case '\\':
						    if (!inWord)
						    {
								regExpStr += '(?:';
								inWord = true;
							}
							regExpStr += '\\\\';
							break;
						case '$':
						case '^':
						case '*':
						case '+':
						case '?':
						case '{':
						case '}':
						case '!':
						case '(':
						case ')':
						case '[':
						case ']':
						case '.':
						    if (!inWord)
						    {
								regExpStr += '(?:';
								inWord = true;
							}
							regExpStr += '\\' + ch;
						    break;
						default:
						    if (!inWord)
						    {
								regExpStr += '(?:';
								inWord = true;
							}
							regExpStr += ch;
						    break;
					}
				}
			}
			else
			{
				switch (ch)
				{
					case '$':
					case '^':
					case '*':
					case '+':
					case '?':
					case '{':
					case '}':
					case '!':
					case '(':
					case ')':
					case '[':
					case ']':
					case '.':
					    if (!inWord)
					    {
							regExpStr += '(?:';
							inWord = true;
						}
						regExpStr += '\\' + ch;
					    break;
					case '#':
					    if (inWord)
					    {
					    	regExpStr += ')?';
					    	inWord = false;
					    }
					    regExpStr += '([0-9]*)';
						break;
					case '0':
					    if (inWord)
					    {
					    	regExpStr += ')?';
					    	inWord = false;
					    }
						regExpStr += '([0-9]?)';
						break;
					case 'A':
					    if (inWord)
					    {
					    	regExpStr += ')?';
					    	inWord = false;
					    }
					    regExpStr += '([A-Z]*)';
						break;
					case 'a':
					    if (inWord)
					    {
					    	regExpStr += ')?';
					    	inWord = false;
					    }
						regExpStr += '([A-Z]?)';
						break;
					case 'N':
					    if (inWord)
					    {
					    	regExpStr += ')?';
					    	inWord = false;
					    }
					    regExpStr += '([0-9A-Z]*)';
						break;
					case 'n':
					    if (inWord)
					    {
					    	regExpStr += ')?';
					    	inWord = false;
					    }
					    regExpStr += '([0-9A-Z]?)';
						break;
					case 'W':
					    if (inWord)
					    {
					    	regExpStr += ')?';
					    	inWord = false;
					    }
					    regExpStr += '([0-9A-Z_]*)';
						break;
					case 'w':
					    if (inWord)
					    {
					    	regExpStr += ')?';
					    	inWord = false;
					    }
						regExpStr += '([0-9A-Z_]?)';
						break;
					case 'F':
					    if (inWord)
					    {
					    	regExpStr += ')?';
					    	inWord = false;
					    }
					    regExpStr += '([\]\[\!\¡\#\$\%\&\'\(\)\+\,\-\.\;\=\@\\\\\ 0-9A-ZñÑáÁéÉíÍóÓúÚ_]*)';
						break;
					case 'f':
					    if (inWord)
					    {
					    	regExpStr += ')?';
					    	inWord = false;
					    }
						regExpStr += '([\]\[\!\¡\#\$\%\&\'\(\)\+\,\-\.\;\=\@\\\\\ 0-9A-ZñÑáÁéÉíÍóÓúÚ_]?)';
						break;
					default:
					    if (!inWord)
					    {
							regExpStr += '(?:';
							inWord = true;
						}
						regExpStr += ch;
					    break;
				}
			}
		}
		if (inWord)
		{
			regExpStr += ')?';
			inWord = false;
		}
		return regExpStr;
	}
	function unformatString(aString, aFormat)
	{
		aString = aString.toString();
		if (aFormat == '')
		{
			return aString;
		}
		var regExpStr = unformatStringRegExpStr(aFormat);
		var regExp = new RegExp(regExpStr, 'i');
 	    var matchArray = aString.match(regExp);
 	    if (matchArray == null)
 	    {
 	    	return '';
 	    }
 	    if (matchArray.index > 0 || regExp.lastIndex < 0)
 	    {
 	    	return '';
 	    }
 	    var unformatedString = '';
 	    for (var i = 1; i < matchArray.length; i++)
 	    {
 	    	unformatedString += matchArray[i];
 	    }
 	    return unformatedString;
	}
	
	function formatString(aString, aFormat)
	{
		aString = aString.toString();
		if (aFormat == '')
		{
			return aString;
		}
 		var regExpStr = formatStringRegExpStr(aFormat);
		var regExp = new RegExp(regExpStr, 'i');
 	    var matchArray = aString.match(regExp);
 	    if (matchArray == null)
 	    {
 	    	return '';
 	    }
 	    if (matchArray.index > 0 || regExp.lastIndex < 0)
 	    {
 	    	return '';
 	    }
 	    var formatedString = '';
 	    var matchPos = 1;
 	    var matchLen = matchArray.length;
 	    while (matchLen > 1 && (matchArray[matchLen - 1] == ''))
 	    {
 	    	matchLen--;
 	    }
		for (var i = 0; i < aFormat.length && matchPos < matchLen; i++)
		{
			var ch = aFormat.charAt(i);
			if (ch == '\\')
			{
				i++;
				if (i < aFormat.length)
				{
					ch = aFormat.charAt(i);
					formatedString += ch;
				}
			}
			else
			{
				switch (ch)
				{
					case '#':
					case '0':
					case 'A':
					case 'a':
					case 'N':
					case 'n':
					case 'W':
					case 'w':
					case 'F':
					case 'f':
					    formatedString += matchArray[matchPos];
					    matchPos++;
						break;
					default:
						formatedString += ch;
					    break;
				}
			}
		}
 	    return formatedString;
	}
	function indexOfChar(aString, aChar)
	{
		for (var i = 0; i < aString.length; i++)
		{
			var ch = aString.charAt(i);
			if (ch == '\\')
			{
				i++;
			}
			else
			{
				if (ch == aChar)
				{
					return i;
				}
			}
		}
		return -1;
	}
	function removeChar(aString, aChar)
	{
		for (var i = aString.length - 1; i >= 0; i--)
		{
			var ch = aString.charAt(i);
			if (ch == '\\')
			{
				i--;
			}
			else
			{
				if (ch == aChar)
				{
					aString = aString.substr(0, i) + aString.substr(i + 1);
				}
			}
		}
		return aString;
	}
	function unformatNumberRegExpStr(aFormat, anInfoArray)
	{
 		var regExpStr = '(\-)?';
		var digitMatchCapture = false;
		var decimalPointMatchCapture = false;
		var colons = 0;
		var percentSigns = 0;
		for (var i = 0; i < aFormat.length; i++)
		{
			var ch = aFormat.charAt(i);
			if (ch == '\\')
			{
				digitMatchCapture = false;
				i++;
				if (i < aFormat.length)
				{
					ch = aFormat.charAt(i);
					switch (ch)
					{
						case '\\':
							regExpStr += '\\\\?';
							break;
						case '^':
						case '$':
						case '*':
						case '+':
						case '?':
						case '{':
						case '}':
						case '!':
						case '(':
						case ')':
						case '[':
						case ']':
						    regExpStr += '\\' + ch + '?';
						    break;
						default:
						    if ((ch >= '0' && ch <= '9') || ch == '-')
						    {
						    	regExpStr += ch;
						    }
						    else
						    {
						    	regExpStr += ch + '?';
						    }
						    break;
					}
				}
			}
			else
			{
				switch (ch)
				{
					case '^':
					case '$':
					case '*':
					case '+':
					case '?':
					case '{':
					case '}':
					case '!':
					case '(':
					case ')':
					case '[':
					case ']':
					    regExpStr += '\\' + ch + '?';
						digitMatchCapture = false
					    break;
					case ',':
					    if (!decimalPointMatchCapture)
					    {
					    	colons++;
						}
						break;
					case '#':
					case '0':
					    if (!decimalPointMatchCapture)
					    {
					    	if (!digitMatchCapture)
					    	{
					    		regExpStr += '([0-9,]*)';
					    	}
							colons = 0;
					    }
					    else
					    {
					    	if (!digitMatchCapture)
					    	{
					    		regExpStr += '([0-9]*)';
					    	}
					    }
						digitMatchCapture = true;
						break;
					case '.':
					    if (!decimalPointMatchCapture)
					    {
					    	regExpStr += '(\\.?)';
					    }
					    else
					    {
							regExpStr += '\\.?';
						}
						digitMatchCapture = false;
						decimalPointMatchCapture = true;
					    break;
					case '%':
					    percentSigns++;
					default:
						if ((ch >= '0' && ch <= '9') || ch == '-')
						{
						  	regExpStr += ch;
						}
						else
						{
						   	regExpStr += ch + '?';
						}
						digitMatchCapture = false;
					    break;
				}
			}
		}
		if (anInfoArray != null)
		{
			anInfoArray[0] = colons;
			anInfoArray[1] = percentSigns;
		}
		return regExpStr;
	}
	function unformatNumber(aNumber, aFormat)
	{
		aNumber = aNumber.toString();
		if (aFormat == '')
		{
			var unformatedNumber = aNumber.toString().replace(/([^0-9.\-])/g, '');
			if(unformatedNumber == '' || isNaN(unformatedNumber))
			{
				unformatedNumber = '0';
			}
			return new Number(unformatedNumber);
		}
		var semicolonPos = indexOfChar(aFormat, ';');
		if (semicolonPos >= 0)
		{
			var positiveFormat = aFormat.substr(0, semicolonPos);
			var negativeFormat = aFormat.substr(semicolonPos + 1);
			var zeroFormat = null;
			semicolonPos = indexOfChar(negativeFormat, ';');
			if (semicolonPos >= 0)
			{
				zeroFormat = negativeFormat.substr(semicolonPos + 1);
				negativeFormat = negativeFormat.substr(0, semicolonPos);
			}
			var anArray = new Array(3);
			anArray[0] = privateUnformatNumber(aNumber, positiveFormat);
			anArray[1] = privateUnformatNumber(aNumber, negativeFormat);
			if (anArray[1] != null)
			{
				anArray[1][0] = -1 * anArray[1][0];
			}
			anArray[2] = null;
			if (zeroFormat != null)
			{
				anArray[2] = privateUnformatNumber(aNumber, zeroFormat);
				if (anArray[2] != null)
				{
					anArray[2][0] = 0;
				}
			}
			var max_i = -1;
			var max_len = -1;
			for (var i = 0; i < anArray.length; i++)
			{
				if (anArray[i] != null)
				{
					if (anArray[i][1] > max_len)
					{
						max_i = i;
						max_len = anArray[i][1];
					}
				}
			}
			return (max_i < 0 ? 0 : anArray[max_i][0]);
		}
 	    anArray = privateUnformatNumber(aNumber, aFormat);
		return (anArray == null ? 0 : anArray[0]);
	}
	function privateUnformatNumber(aNumber, aFormat)
	{
		var infoArray = new Array();
		var regExpStr = unformatNumberRegExpStr(aFormat, infoArray);
		var colons = infoArray[0];
		var percentSigns = infoArray[1];
		var regExp = new RegExp(regExpStr, 'i');
 	    var matchArray = aNumber.match(regExp);
 	    if (matchArray == null)
 	    {
 	    	return null;
 	    }
 	    if (matchArray.index > 0 || regExp.lastIndex < 0)
 	    {
 	    	return null;
 	    }
 	    var unformatedNumber = '';
 	    for (var i = 1; i < matchArray.length; i++)
 	    {
 	    	if(matchArray[i]!=null && matchArray[i]!=undefined)
 	    	{
 	    		unformatedNumber += matchArray[i];
 	    	}
 	    }
 	    unformatedNumber = unformatedNumber.replace(/\,/g, '');
		if (isNaN(unformatedNumber))
		{
			unformatedNumber = '0';
		}
		unformatedNumber = unformatedNumber * (Math.pow(1000, colons) / Math.pow(100, percentSigns));
 	    return new Array(unformatedNumber, regExp.lastIndex);
	}
	function formatNumber(aNumber, aFormat)
	{
		aNumber = aNumber.toString();
		if (aFormat == '')
		{
			var formatedNumber = unformatNumber(aNumber, aFormat);
			return formatedNumber.toString();
		}
		var semicolonPos = indexOfChar(aFormat, ';');
		if (semicolonPos >= 0)
		{
			var positiveFormat = aFormat.substr(0, semicolonPos);
			var negativeFormat = aFormat.substr(semicolonPos + 1);
			var zeroFormat = null;
			semicolonPos = indexOfChar(negativeFormat, ';');
			if (semicolonPos >= 0)
			{
				zeroFormat = negativeFormat.substr(semicolonPos + 1);
				negativeFormat = negativeFormat.substr(0, semicolonPos);
			}
			if (aNumber < 0)
			{
				return formatNumber(Math.abs(aNumber), negativeFormat);
			}
			else
			{
                if (aNumber > 0 || zeroFormat == null)
				{
					return formatNumber(aNumber, positiveFormat);
				}
				else
				{
					return formatNumber(aNumber, zeroFormat);
				}
			}
		}
		var firstPart, leftPart, rightPart;
		var decimalPos = indexOfChar(aFormat, '.');
		if (decimalPos >= 0)
		{
  			leftPart = aFormat.substr(0, decimalPos);
  			rightPart = aFormat.substr(decimalPos + 1);
  			hasDecimalPoint = true;
		}
		else
  		{
			leftPart = aFormat;
			rightPart = '';
			hasDecimalPoint = false;
  		}
  		firstPart = '';
	  	var i = 0;
		while (i < leftPart.length)
		{
			var ch = leftPart.charAt(i);
			if (ch == '\\')
			{
				i++;
			}
			else
			{
				if (ch == '#') break;
				if (ch == '0') break;
			}
			i++;
		}
		firstPart = leftPart.substr(0, i);
		firstPart = firstPart.replace('\\', '');
		leftPart = leftPart.substr(i);
		if (leftPart == '')
		{
			leftPart = '#';
		}
  		var thousandDivisor = 0;
  		var hasThousandSeparator = (indexOfChar(leftPart, ',') != -1);
  		if (hasThousandSeparator)
  		{
	  		var i = leftPart.length - 1;
			while (i >= 0)
			{
				if (i > 0 && leftPart.charAt(i - 1) == '\\')
				{
					i--;
				}
				else
				{
					var ch = leftPart.charAt(i);
					if (ch == '#') break;
					if (ch == '0') break;
					if (ch == ',') thousandDivisor++;
				}
				i--;
			}
   			leftPart = removeChar(leftPart, ',');
  		}
		thousandDivisor = Math.pow(1000, thousandDivisor);
  		var percentageMultiplier = 0;
	  	for (var i = 0; i < leftPart.length; i++)
	  	{
			var ch = leftPart.charAt(i);
			if (ch == '\\')
			{
				i++;
			}
			else
			{
				if (ch == '%')
				{
					percentageMultiplier++;
				}
			}
  		}
	  	for (var i = 0; i < rightPart.length; i++)
	  	{
			var ch = rightPart.charAt(i);
			if (ch == '\\')
			{
				i++;
			}
			else
			{
				if (ch == '%')
				{
					percentageMultiplier++;
				}
			}
  		}
		percentageMultiplier = Math.pow(100, percentageMultiplier);
  		var leftZeroDigits = 0;
		for (var i = 0; i < leftPart.length; i++)
		{
			var ch = leftPart.charAt(i);
			if (ch == '\\')
			{
				i++;
			}
			else
			{
				if (ch == '0') leftZeroDigits++;
				if (ch == '#' && leftZeroDigits > 0) leftZeroDigits++;
			}
		}
		var rightZeroDigits = 0;
		var rightDigits = 0;
		for (var i = rightPart.length - 1; i >= 0; i--)
		{
			if (i > 0 && rightPart.charAt(i - 1) == '\\')
			{
				i--;
			}
			else
			{
				var ch = rightPart.charAt(i);
				if (ch == '0')
				{
				 	rightZeroDigits++;
					rightDigits++;
				}
				else
				{
					if (ch == '#')
					{
            	    	if (rightZeroDigits > 0) rightZeroDigits++;
                		rightDigits++;
            		}
				}
			}
		}
		var formatedNumber = unformatNumber(aNumber, '');
		// formatedNumber = formatedNumber * (percentageMultiplier / thousandDivisor);
		formatedNumber = formatedNumber * (percentageMultiplier / thousandDivisor);
		var isNegative = false;
		if (formatedNumber < 0)
		{
			isNegative = true;
			formatedNumber = Math.abs(formatedNumber);
		}
		divisor = Math.pow(10, rightDigits);
		formatedNumber = Math.floor(formatedNumber * divisor + 0.50000000001);
		var fraction = (formatedNumber % divisor).toString();
		formatedNumber = Math.floor(formatedNumber / divisor).toString();
		var rightZeros = '';
		for (var i = 0; i < (rightDigits - fraction.length); i++)
		{
				rightZeros = rightZeros + '0';
		}
		fraction = rightZeros + fraction;
		fraction = fraction.substr(fraction.length - rightDigits, rightDigits);
		var i = fraction.length;
		while (i > rightZeroDigits && fraction.charAt(i - 1) == '0')
		{
			i--;
		}
		fraction = fraction.substr(0, i);
 		var leftZeros = '';
		for (var i = 0; i < leftZeroDigits - formatedNumber.length; i ++)
		{
				leftZeros = leftZeros + '0';
		}
		formatedNumber = leftZeros + formatedNumber;
 		var i = formatedNumber.length;
		while (i > leftZeroDigits && formatedNumber.charAt(formatedNumber.length - i) == '0')
		{
			i--;
		}
		formatedNumber = formatedNumber.substr(formatedNumber.length - i);
		var digits = 0;
		var formatPos = leftPart.length - 1;
		var numberPos = formatedNumber.length - 1;
		for (var i = 0; i < leftPart.length; i++)
		{
			if (formatPos > 0 && leftPart.charAt(formatPos - 1) == '\\')
			{
				var ch = leftPart.charAt(formatPos);
				formatedNumber = formatedNumber.substr(0, numberPos + 1) + ch + formatedNumber.substr(numberPos + 1);
				formatPos--;
			}
			else
			{
				var ch = leftPart.charAt(formatPos);
				if (ch == '#' || ch == '0')
				{
					if (digits == 3)
					{
						if (hasThousandSeparator && numberPos >= 0)
						{
							formatedNumber = formatedNumber.substr(0, numberPos + 1) + ',' + formatedNumber.substr(numberPos + 1);
						}
						digits = 0;
					}
					digits++;
					numberPos--;
				}
				else
				{
					formatedNumber = formatedNumber.substr(0, numberPos + 1) + ch + formatedNumber.substr(numberPos + 1);
				}
			}
			formatPos--;
		}
		if (hasThousandSeparator)
		{
			while (numberPos >= 0)
			{
				if (digits == 3)
				{
					formatedNumber = formatedNumber.substr(0, numberPos + 1) + ',' + formatedNumber.substr(numberPos + 1);
					digits = 0;
				}
				digits++;
				numberPos--;
			}
		}
 		var formatPos = 0;
		var numberPos = 0;
		for (var i = 0; i < rightPart.length; i++)
		{
			var ch = rightPart.charAt(formatPos);
			if (ch == '\\')
			{
				formatPos++;
				var ch = rightPart.charAt(formatPos);
				fraction = fraction.substr(0, numberPos) + ch + fraction.substr(numberPos);
			}
			else
			{
				if (ch != '#' && ch != '0')
				{
					fraction = fraction.substr(0, numberPos) + ch + fraction.substr(numberPos);
				}
			}
			formatPos++;
			numberPos++;
		}
		if (hasDecimalPoint)
  		{
			return (((isNegative) ? '-' : '') + firstPart + formatedNumber + '.' + fraction);
		}
		else
		{
			return (((isNegative) ? '-' : '') + firstPart + formatedNumber);
		}
	}
	function monthFromName(aString)
	{
		if(aString==undefined)
		{
			return "";
		}

		for (var i = 0; i < MONTH_NAMES.length; i++)
		{
			if (MONTH_NAMES[i].toLowerCase() == aString.toLowerCase())
			{
				return (i + 1).toString();
			}
		}
		return aString;
	}
	function monthFromAbbr(aString)
	{
		for (var i = 0; i < MONTH_ABBR.length; i++)
		{
			if (MONTH_ABBR[i].toLowerCase() == aString.toLowerCase())
			{
				return (i + 1).toString();
			}
		}
		return aString;
	}
	function unformatDateTimeRegExpStrPart(aFormatArray, aTypeArray)
	{
		var regExpStr = '';
 		if (aFormatArray[0] == 4)
		{
			aTypeArray[aTypeArray.length] = 'yyyy';
			regExpStr = '([0-9]{0,4})';
		}
		if (aFormatArray[0] == 3)
		{
			aTypeArray[aTypeArray.length] = 'yy';
			aTypeArray[aTypeArray.length] = 'y';
			regExpStr = '([0-9]{0,4})([0-9]{0,4})';
		}
		if (aFormatArray[0] == 2)
		{
			aTypeArray[aTypeArray.length] = 'yy';
			regExpStr = '([0-9]{0,4})';
		}
		if (aFormatArray[0] == 1)
		{
			aTypeArray[aTypeArray.length] = 'y';
			regExpStr = '([0-9]{0,4})';
		}
		if (aFormatArray[1] == 4)
		{
			aTypeArray[aTypeArray.length] = 'MMMM';
			regExpStr = MONTH_REGEXP;
		}
		if (aFormatArray[1] == 3)
		{
			aTypeArray[aTypeArray.length] = 'MMM';
			regExpStr = MONTH_REGEXP;
		}
		if (aFormatArray[1] == 2)
		{
			aTypeArray[aTypeArray.length] = 'MM';
			regExpStr = MONTH_REGEXP;
		}
		if (aFormatArray[1] == 1)
		{
			aTypeArray[aTypeArray.length] = 'M';
			regExpStr = MONTH_REGEXP;
		}
		if (aFormatArray[2] == 2)
		{
			aTypeArray[aTypeArray.length] = 'dd';
			regExpStr = '([0-9]{0,2})';
		}
		if (aFormatArray[2] == 1)
		{
			aTypeArray[aTypeArray.length] = 'd';
			regExpStr = '([0-9]{0,2})';
		}
		if (aFormatArray[3] == 2)
		{
			aTypeArray[aTypeArray.length] = 'hh';
			regExpStr = '([0-9]{0,2})';
		}
		if (aFormatArray[3] == 1)
		{
			aTypeArray[aTypeArray.length] = 'h';
			regExpStr = '([0-9]{0,2})';
		}
		if (aFormatArray[4] == 2)
		{
			aTypeArray[aTypeArray.length] = 'mm';
			regExpStr = '([0-9]{0,2})';
		}
		if (aFormatArray[4] == 1)
		{
			aTypeArray[aTypeArray.length] = 'm';
			regExpStr = '([0-9]{0,2})';
		}
		if (aFormatArray[5] == 2)
		{
			aTypeArray[aTypeArray.length] = 'ss';
			regExpStr = '([0-9]{0,2})';
		}
		if (aFormatArray[5] == 1)
		{
			aTypeArray[aTypeArray.length] = 's';
			regExpStr = '([0-9]{0,2})';
		}
		for (var i = 0; i < aFormatArray.length; i++)
		{
			aFormatArray[i] = 0;
		}
		return regExpStr;
	}
	function unformatDateTimeRegExpStr(aFormat, aTypeArray)
	{
		if (aTypeArray == null)
		{
			aTypeArray = new Array();
		}
 		var regExpStr = '';
 		var formatArray = new Array(6);
 		for (var i = 0; i < formatArray.length; i++)
		{
			formatArray[i] = 0;
		}
		for (var i = 0; i < aFormat.length; i++)
		{
			var ch = aFormat.charAt(i);
			if (ch == '\\')
			{
				regExpStr += unformatDateTimeRegExpStrPart(formatArray, aTypeArray);
				i++;
				if (i < aFormat.length)
				{
					ch = aFormat.charAt(i);
					switch (ch)
					{
						case '\\':
							regExpStr += '\\\\?';
							break;
						case '^':
						case '$':
						case '*':
						case '+':
						case '?':
						case '{':
						case '}':
						case '!':
						case '(':
						case ')':
						case '[':
						case ']':
						case '.':
						    regExpStr += '\\' + ch + '?';
						    break;
						default:
							if ((ch >= '0' && ch <= '9'))
						    {
						    	regExpStr += ch;
						    }
						    else
						    {
						    	regExpStr += ch + '?';
						    }
						    break;
					}
				}
			}
			else
			{
				switch (ch)
				{
					case '^':
					case '$':
					case '*':
					case '+':
					case '?':
					case '{':
					case '}':
					case '!':
					case '(':
					case ')':
					case '[':
					case ']':
					case '.':
						regExpStr += unformatDateTimeRegExpStrPart(formatArray, aTypeArray);
					    regExpStr += '\\' + ch + '?';
					    break;
					case 'y':
						if (formatArray[0] == 4)
						{
							regExpStr += unformatDateTimeRegExpStrPart(formatArray, aTypeArray);
						}
						formatArray[0]++;
						break;
					case 'M':
						if (formatArray[1] == 4)
						{
							regExpStr += unformatDateTimeRegExpStrPart(formatArray, aTypeArray);
						}
						formatArray[1]++;
						break;
					case 'd':
						if (formatArray[2] == 2)
						{
							regExpStr += unformatDateTimeRegExpStrPart(formatArray, aTypeArray);
						}
						formatArray[2]++;
						break;
					case 'h':
						if (formatArray[3] == 2)
						{
							regExpStr += unformatDateTimeRegExpStrPart(formatArray, aTypeArray);
						}
						formatArray[3]++;
						break;
					case 'm':
						if (formatArray[4] == 2)
						{
							regExpStr += unformatDateTimeRegExpStrPart(formatArray, aTypeArray);
						}
						formatArray[4]++;
						break;
					case 's':
						if (formatArray[5] == 2)
						{
							regExpStr += unformatDateTimeRegExpStrPart(formatArray, aTypeArray);
						}
						formatArray[5]++;
						break;
					default:
						regExpStr += unformatDateTimeRegExpStrPart(formatArray, aTypeArray);
					    if ((ch >= '0' && ch <= '9'))
					    {
					    	regExpStr += ch;
					    }
					    else
					    {
					    	regExpStr += ch + '?';
					    }
					    break;
				}
			}
		}
		regExpStr += unformatDateTimeRegExpStrPart(formatArray, aTypeArray);
		return regExpStr;
	}
	function privateUnformatDateTime(aDateTime, aFormat)
	{
		if (aFormat == '')
		{
			aFormat = 'yyyy-MM-dd hh:mm:ss';
		}
		aDateTime = aDateTime.toString();
 		var typeArray = new Array();
 		var regExpStr = unformatDateTimeRegExpStr(aFormat, typeArray);
		var regExp = new RegExp(regExpStr, 'i');
 	    var matchArray = aDateTime.match(regExp);
 	    if (matchArray == null)
 	    {
	 	    return new Array('0000', '00', '00', '00', '00', '00');
 	    }
 	    if (matchArray.index > 0 || regExp.lastIndex < 0)
 	    {
	 	    return new Array('0000', '00', '00', '00', '00', '00');
 	    }
		var year = null;
		var month = null;
		var day = null;
		var hours = null;
		var minutes = null;
		var seconds = null;
 	    for (var i = 1; i < matchArray.length; i++)
 	    {
 	    	switch (typeArray[i - 1])
 	    	{
 	    	    case 'yyyy':
 	    	    case 'yy':
 	    	    case 'y':
 	    	    	if (year == null)
 	    			{
						year = matchArray[i];
						if (year == '' || isNaN(year))
						{
							year = null;
						}
						else
						{
							if (year < 1)
							{
								year = null;
							}
							else
							{
								if (year < 100)
								{
									today = new Date();
									year = Math.floor(today.getFullYear() / 100) * 100 + (year - 0);
								}
							}
						}
					}
					break;
				case 'MMMM':
				case 'MMM':
 	    	    case 'MM':
 	    	    case 'M':
 	    	    	if (month == null)
 	    			{
						month = matchArray[i];
						month = monthFromName(month);
						month = monthFromAbbr(month);
						if (month == '' || isNaN(month))
						{
							month = null;
						}
						else
						{
							if (month < 1 || month > 12)
							{
								month = null;
							}
						}
					}
					break;
 	    	    case 'hh':
 	    	    case 'h':
 	    	    	if (hours == null)
 	    			{
						hours = matchArray[i];
						if (hours == '' || isNaN(hours))
						{
							hours = null;
						}
						else
						{
							if (hours > 23)
							{
								hours = null;
							}
						}
					}
					break;
 	    	    case 'mm':
 	    	    case 'm':
 	    	    	if (minutes == null)
 	    			{
						minutes = matchArray[i];
						if (minutes == '' || isNaN(minutes))
						{
							minutes = null;
						}
						else
						{
							if (minutes > 59)
							{
								minutes = null;
							}
						}
					}
					break;
 	    	    case 'ss':
 	    	    case 's':
 	    	    	if (seconds == null)
 	    			{
						seconds = matchArray[i];
						if (seconds == '' || isNaN(seconds))
						{
							seconds = null;
						}
						else
						{
							if (seconds > 59)
							{
								seconds = null;
							}
						}
					}
					break;
				default:
				    break;
			}
		}
 	    for (var i = 1; i < matchArray.length; i++)
 	    {
 	    	if (typeArray[i - 1] == 'dd' || typeArray[i - 1] == 'd')
 	    	{
 	    	    if (day == null)
 	    		{
					day = matchArray[i];
					if (day == '' || isNaN(day))
					{
						day = null;
					}
					else
	 				{
	 					if (day < 1)
	 					{
	 						day = null;
	 					}
	 					else
	 					{
	 						if (month == null)
	 						{
	 							if (day > 31)
	 							{
	 								day = null;
	 							}
	 						}
		 					else
		 					{
								if (month == 2)
								{
									if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
									{
										if (day > 29)
										{
											day = 29;
										}
									}
									else
									{
										if (day > 28)
										{
											day = 28;
										}
									}
								}
								else
								{
									if ((month == 4) || (month == 6) ||
										(month == 9) || (month == 11))
									{
										if (day > 30)
										{
											day = 30;
										}
									}
									else
									{
										if (day > 31)
										{
											day = 31;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (year == null)
		{
			year = '0000';
			month = '00';
			day = '00';
		}
		else
		{
			year = '0000' + (year - 0).toString();
			year = year.substr(year.length - 4);
			if (month == null)
			{
				month = '01';
				day = '01';
			}
			else
			{
				if (month < 10)
				{
					month = '0' + (month - 0).toString();
				}
				if (day == null)
				{
					day = '01';
				}
				else
				{
					if (day < 10)
					{
						day = '0' + (day - 0).toString();
					}
				}
			}
		}
		if (hours == null)
		{
			hours = '00';
			minutes = '00';
			seconds = '00';
		}
		else
		{
			if (hours < 10)
			{
				hours = '0' + (hours - 0).toString();
			}
			if (minutes == null)
			{
				minutes = '00';
				seconds = '00';
			}
			else
			{
				if (minutes < 10)
				{
					minutes = '0' + (minutes - 0).toString();
				}
				if (seconds == null)
				{
					seconds = '00';
				}
				else
				{
					if (seconds < 10)
					{
						seconds = '0' + (seconds - 0).toString();
					}
				}
			}
		}
 	    return new Array(year, month, day, hours, minutes, seconds);
	}
	function unformatDateTime(aDateTime, aFormat)
	{
		var dateTimeArray = privateUnformatDateTime(aDateTime, aFormat);
		return dateTimeArray[0] + '-' + dateTimeArray[1] + '-' + dateTimeArray[2] + ' ' + dateTimeArray[3] + ':' + dateTimeArray[4] + ':' + dateTimeArray[5];
	}
	function unformatDate(aDateTime, aFormat)
	{
		var dateTimeArray = privateUnformatDateTime(aDateTime, aFormat);
		return dateTimeArray[0] + '-' + dateTimeArray[1] + '-' + dateTimeArray[2];
	}
	function formatDateTimePart(dateTimeArray, formatArray)
	{
		var formatedDateTimePart = '';
 		if (formatArray[0] == 4)
		{
			formatedDateTimePart = dateTimeArray[0];
		}
		if (formatArray[0] == 3)
		{
			formatedDateTimePart = dateTimeArray[0].substr(2) + dateTimeArray[0];
		}
		if (formatArray[0] == 2)
		{
			formatedDateTimePart = dateTimeArray[0].substr(2);
		}
		if (formatArray[0] == 1)
		{
			formatedDateTimePart = dateTimeArray[0];
		}
		if (formatArray[1] == 4)
		{
			if (dateTimeArray[1] > 0)
			{
				formatedDateTimePart = MONTH_NAMES[dateTimeArray[1] - 1];
			}
			else
			{
				formatedDateTimePart = dateTimeArray[1];
			}
		}
		if (formatArray[1] == 3)
		{
			if (dateTimeArray[1] > 0)
			{
				formatedDateTimePart = MONTH_ABBR[dateTimeArray[1] - 1];
			}
			else
			{
				formatedDateTimePart = dateTimeArray[1];
			}
		}
		if (formatArray[1] == 2)
		{
			formatedDateTimePart = dateTimeArray[1];
		}
		if (formatArray[1] == 1)
		{
			formatedDateTimePart = (dateTimeArray[1] - 0).toString();
		}
		if (formatArray[2] == 2)
		{
			formatedDateTimePart = dateTimeArray[2];
		}
		if (formatArray[2] == 1)
		{
			formatedDateTimePart = (dateTimeArray[2] - 0).toString();
		}
		if (formatArray[3] == 2)
		{
			formatedDateTimePart = dateTimeArray[3];
		}
		if (formatArray[3] == 1)
		{
			formatedDateTimePart = (dateTimeArray[3] - 0).toString();
		}
		if (formatArray[4] == 2)
		{
			formatedDateTimePart = dateTimeArray[4];
		}
		if (formatArray[4] == 1)
		{
			formatedDateTimePart = (dateTimeArray[4] - 0).toString();
		}
		if (formatArray[5] == 2)
		{
			formatedDateTimePart = dateTimeArray[5];
		}
		if (formatArray[5] == 1)
		{
			formatedDateTimePart = (dateTimeArray[5] - 0).toString();
		}
		for (var i = 0; i < formatArray.length; i++)
		{
			formatArray[i] = 0;
		}
		return formatedDateTimePart;
	}
	function formatDateTime(aDateTime, aFormat)
	{
		var dateTimeArray = privateUnformatDateTime(aDateTime, '');
  		if (aFormat == '')
		{
			return dateTimeArray[0] + '-' + dateTimeArray[1] + '-' + dateTimeArray[2] + ' ' + dateTimeArray[3] + ':' + dateTimeArray[4] + ':' + dateTimeArray[5];
		}
		
		var formatedDateTime = '';
  		var formatArray = new Array(6);
 		for (var i = 0; i < formatArray.length; i++)
		{
			formatArray[i] = 0;
		}
		
		for (var i = 0; i < aFormat.length; i++)
		{
			var ch = aFormat.charAt(i);
			if (ch == '\\')
			{
				formatedDateTime += formatDateTimePart(dateTimeArray, formatArray);
				i++;
				if (i < aFormat.length)
				{
					ch = aFormat.charAt(i);
					formatedDateTime += ch;
				}
			}
			else
			{
				switch (ch)
				{
					case 'y':
						if (formatArray[0] == 4)
						{
							formatedDateTime += formatDateTimePart(dateTimeArray, formatArray);
						}
						formatArray[0]++;
						break;
					case 'M':
						if (formatArray[1] == 4)
						{
							formatedDateTime += formatDateTimePart(dateTimeArray, formatArray);
						}
						formatArray[1]++;
						break;
					case 'd':
						if (formatArray[2] == 2)
						{
							formatedDateTime += formatDateTimePart(dateTimeArray, formatArray);
						}
						formatArray[2]++;
						break;
					case 'h':
						if (formatArray[3] == 2)
						{
							formatedDateTime += formatDateTimePart(dateTimeArray, formatArray);
						}
						formatArray[3]++;
						break;
					case 'm':
						if (formatArray[4] == 2)
						{
							formatedDateTime += formatDateTimePart(dateTimeArray, formatArray);
						}
						formatArray[4]++;
						break;
					case 's':
						if (formatArray[5] == 2)
						{
							formatedDateTime += formatDateTimePart(dateTimeArray, formatArray);
						}
						formatArray[5]++;
						break;
					default:
						formatedDateTime += formatDateTimePart(dateTimeArray, formatArray);
						formatedDateTime += ch;
					    break;
				}
			}
		}
		formatedDateTime += formatDateTimePart(dateTimeArray, formatArray);
		return formatedDateTime;
	}

	function displayIDs(aTable, iCol)
	{
		var str = '';
		for (var i = 1; i < aTable.rows.length; i++)
		{
			str += ("\n" + aTable.rows[i].cells[iCol].id);
		}
		alert(str);
	}

	function MoveRow(aTable, lBound, hBound)
	{
		var objFrstRow, objScndRow;
		var lMaxIndex, lAdd;
		
		if (lBound == hBound)
		{
			return ;
		}
		
		if (aTable.moveRow)
		{
			aTable.moveRow (lBound, hBound);
		}
		else
		{
			objFrstRow = aTable.rows[lBound];
			lMaxIndex = aTable.rows.length - 1;
			if (hBound == lMaxIndex)
			{
				objFrstRow.parentNode.appendChild (objFrstRow);
			}
			else
			{
				lAdd = 1;
				if (lBound > hBound)
				{
					lAdd = 0;
				}
				objScndRow = aTable.rows[hBound + lAdd];
				objFrstRow.parentNode.insertBefore (objFrstRow, objScndRow);
			}
		}
	}
	
	function StringQuicksortTable(aTable, iCol)
	{
		var order;
		if (aTable.rows[0].cells[iCol].id == 'asc')
		{
			StringQuicksortTableDesc(aTable, iCol, 1, aTable.rows.length - 1);
			order = 'desc';
		}
		else
		{
			StringQuicksortTableAsc(aTable, iCol, 1, aTable.rows.length - 1);
			order = 'asc';
		}
		for (var i = 0; i < aTable.rows[0].cells.length; i++)
		{
			aTable.rows[0].cells[i].id = (i == iCol) ? order : '';
		}
	}

	function NumberQuicksortTable(aTable, iCol)
	{
		var order;
		if (aTable.rows[0].cells[iCol].id == 'asc')
		{
			NumberQuicksortTableDesc(aTable, iCol, 1, aTable.rows.length - 1);
			order = 'desc';
		}
		else
		{
			NumberQuicksortTableAsc(aTable, iCol, 1, aTable.rows.length - 1);
			order = 'asc';
		}
		for (var i = 0; i < aTable.rows[0].cells.length; i++)
		{
			aTable.rows[0].cells[i].id = (i == iCol) ? order : '';
		}
	}

	function StringQuicksortTableAsc(aTable, iCol, loBound, hiBound)
	{
		var pivot, loSwap, hiSwap;

		if ((hiBound - loBound) <= 1)
		{
			if (aTable.rows[loBound].cells[iCol].id > aTable.rows[hiBound].cells[iCol].id)
			{
			    MoveRow (aTable, loBound, hiBound);
			}
			return;
		}
		pivot = parseInt((loBound + hiBound) / 2);
		MoveRow (aTable, loBound, pivot);
		if ((pivot - loBound) > 1)
		{
			MoveRow (aTable, pivot - 1, loBound);
		}
		loSwap = loBound + 1;
		hiSwap = hiBound;
		do
		{
			while (loSwap <= hiSwap && aTable.rows[loSwap].cells[iCol].id <= aTable.rows[loBound].cells[iCol].id)
   			{
				loSwap++;
			}
			while (aTable.rows[hiSwap].cells[iCol].id > aTable.rows[loBound].cells[iCol].id)
			{
				hiSwap--;
			}
			if (loSwap < hiSwap)
			{
			    MoveRow (aTable, loSwap, hiSwap);
			    if ((hiSwap - loSwap) > 1)
			    {
			    	MoveRow (aTable, hiSwap - 1, loSwap);
			    }
			}
		} while (loSwap < hiSwap);

		MoveRow (aTable, loBound, hiSwap);
		if ((hiSwap - loBound) > 1)
		{
			MoveRow (aTable, hiSwap - 1, loBound);
		}
		if (loBound < hiSwap - 1)
		{
			StringQuicksortTableAsc(aTable, iCol, loBound, hiSwap - 1);
		}
		if (hiSwap + 1 < hiBound)
		{
			StringQuicksortTableAsc(aTable, iCol, hiSwap + 1, hiBound);
		}
	}
	function StringQuicksortTableDesc(aTable, iCol, loBound, hiBound)
	{
		var pivot, loSwap, hiSwap;

		if ((hiBound - loBound) <= 1)
		{
			if (aTable.rows[loBound].cells[iCol].id < aTable.rows[hiBound].cells[iCol].id)
			{
			    MoveRow (aTable, loBound, hiBound);
			}
			return;
		}
		pivot = parseInt((loBound + hiBound) / 2);
		MoveRow (aTable, loBound, pivot);
		if ((pivot - loBound) > 1)
		{
			MoveRow (aTable, pivot - 1, loBound);
		}
		loSwap = loBound + 1;
		hiSwap = hiBound;
		do
		{
			while (loSwap <= hiSwap && aTable.rows[loSwap].cells[iCol].id >= aTable.rows[loBound].cells[iCol].id)
   			{
				loSwap++;
			}
			while (aTable.rows[hiSwap].cells[iCol].id < aTable.rows[loBound].cells[iCol].id)
			{
				hiSwap--;
			}
			if (loSwap < hiSwap)
			{
			    MoveRow (aTable, loSwap, hiSwap);
			    if ((hiSwap - loSwap) > 1)
			    {
			    	MoveRow (aTable, hiSwap - 1, loSwap);
			    }
			}
		} while (loSwap < hiSwap);

		MoveRow (aTable, loBound, hiSwap);
		if ((hiSwap - loBound) > 1)
		{
			MoveRow (aTable, hiSwap - 1, loBound);
		}
		if (loBound < hiSwap - 1)
		{
			StringQuicksortTableDesc(aTable, iCol, loBound, hiSwap - 1);
		}
		if (hiSwap + 1 < hiBound)
		{
			StringQuicksortTableDesc(aTable, iCol, hiSwap + 1, hiBound);
		}
	}

	function NumberQuicksortTableAsc(aTable, iCol, loBound, hiBound)
	{
		var pivot, loSwap, hiSwap;

		if ((hiBound - loBound) <= 1)
		{
			if ((aTable.rows[loBound].cells[iCol].id - 0) > (aTable.rows[hiBound].cells[iCol].id - 0))
			{
			    MoveRow (aTable, loBound, hiBound);
			}
			return;
		}
		pivot = parseInt((loBound + hiBound) / 2);
		MoveRow (aTable, loBound, pivot);
		if ((pivot - loBound) > 1)
		{
			MoveRow (aTable, pivot - 1, loBound);
		}
		loSwap = loBound + 1;
		hiSwap = hiBound;
		do
		{
			while (loSwap <= hiSwap && (aTable.rows[loSwap].cells[iCol].id - 0) <= (aTable.rows[loBound].cells[iCol].id - 0))
   			{
				loSwap++;
			}
			while ((aTable.rows[hiSwap].cells[iCol].id - 0) > (aTable.rows[loBound].cells[iCol].id - 0))
			{
				hiSwap--;
			}
			if (loSwap < hiSwap)
			{
			    MoveRow (aTable, loSwap, hiSwap);
			    if ((hiSwap - loSwap) > 1)
			    {
			    	MoveRow (aTable, hiSwap - 1, loSwap);
			    }
			}
		} while (loSwap < hiSwap);

		MoveRow (aTable, loBound, hiSwap);
		if ((hiSwap - loBound) > 1)
		{
			MoveRow (aTable, hiSwap - 1, loBound);
		}
		if (loBound < hiSwap - 1)
		{
			NumberQuicksortTableAsc(aTable, iCol, loBound, hiSwap - 1);
		}
		if (hiSwap + 1 < hiBound)
		{
			NumberQuicksortTableAsc(aTable, iCol, hiSwap + 1, hiBound);
		}
	}

	function NumberQuicksortTableDesc(aTable, iCol, loBound, hiBound)
	{
		var pivot, loSwap, hiSwap;

		if ((hiBound - loBound) <= 1)
		{
			if ((aTable.rows[loBound].cells[iCol].id - 0) < (aTable.rows[hiBound].cells[iCol].id - 0))
			{
			    MoveRow (aTable, loBound, hiBound);
			}
			return;
		}
		pivot = parseInt((loBound + hiBound) / 2);
		MoveRow (aTable, loBound, pivot);
		if ((pivot - loBound) > 1)
		{
			MoveRow (aTable, pivot - 1, loBound);
		}
		loSwap = loBound + 1;
		hiSwap = hiBound;
		do
		{
			while (loSwap <= hiSwap && (aTable.rows[loSwap].cells[iCol].id - 0) >= (aTable.rows[loBound].cells[iCol].id - 0))
   			{
				loSwap++;
			}
			while ((aTable.rows[hiSwap].cells[iCol].id - 0) < (aTable.rows[loBound].cells[iCol].id - 0))
			{
				hiSwap--;
			}
			if (loSwap < hiSwap)
			{
			    MoveRow (aTable, loSwap, hiSwap);
			    if ((hiSwap - loSwap) > 1)
			    {
			    	MoveRow (aTable, hiSwap - 1, loSwap);
			    }
			}
		} while (loSwap < hiSwap);

		MoveRow (aTable, loBound, hiSwap);
		if ((hiSwap - loBound) > 1)
		{
			MoveRow (aTable, hiSwap - 1, loBound);
		}
		if (loBound < hiSwap - 1)
		{
			NumberQuicksortTableDesc(aTable, iCol, loBound, hiSwap - 1);
		}
		if (hiSwap + 1 < hiBound)
		{
			NumberQuicksortTableDesc(aTable, iCol, hiSwap + 1, hiBound);
		}
	}

	function TextArea_onkeydown(aTextArea)
	{
   		var tabKeyCode = 9;
   		if (event.keyCode == tabKeyCode && event.srcElement == aTextArea)
		{
    		aTextArea.selection = document.selection.createRange();
    		aTextArea.selection.text = String.fromCharCode(tabKeyCode);
     		event.returnValue = false;
   		}
	}
	
	function aChange(elemento, aClass)
	{
			elemento.className = aClass;
	}


