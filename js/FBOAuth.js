var FBAPILanguage = '';

if (objSettings && objSettings.language) {
	switch (objSettings.language) {
		case 'sp':
			FBAPILanguage = 'es_LA';
		break;
		case 'en':
		default:
			FBAPILanguage = 'en_US';
		break;
	}
} else {
	FBAPILanguage = 'en_US';
}

// Carga asíncrona del API de Facebook.
// Se tiene que invocar window.fbAsyncInit() al finalizar el cargado del script, para poder renderear el botón.
// El script tiene que ser llamado ANTES de que el botón si quiera exista.
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "http://connect.facebook.net/"+FBAPILanguage+"/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

//El appId NO debe cambiarse, es el ID que está registrada en el perfil de desarrollador
window.fbAsyncInit = function() {
	FB.init({
		appId      : '1021872614528915',
		cookie     : true,  // enable cookies to allow the server to access the session
		xfbml      : true,  // parse social plugins on this page
		version    : 'v2.6' // use graph api version 2.6
	});

	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});

};

//Función que registra el cambio en el estado del usuario
function statusChangeCallback(response) {
	Debugger.register('FB_statusChangeCallback');

	if (response && response.status) {
		if (response.status === 'connected') {
			// Logged into your app and Facebook.
			if (objApp && objApp.FBObject) {
				objApp.FBObject.response = response;
				FB_getUserData(response.authResponse.userID);
			}
		} else if (response.status === 'not_authorized') {
			// The person is logged into Facebook, but not your app.
		} else {
			// The person is not logged into Facebook, so we're not sure if they are logged into this app or not.
		}
	}
}

//Sentinela en el botón, se encarga de saber qué hizo el usuario dentro de la ventana.
function checkLoginState() {
	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});
}

//Función encargada de obtener la información del usuario, asignará los campos al formulario.
function FB_getUserData(userID) {
	FB.api("/"+ userID +"", {fields: 'id,name,first_name,middle_name,last_name,email,birthday,gender,location,hometown,languages,work'}, function(response) {
      if (response && !response.error && objApp && objApp.FBObject) {
        objApp.FBObject.userData = response;
        //Fill form fields
      } else {
      	//Send message with error on response
      }
   });
}

//Al destruir la sesión se tiene que limpiar el objeto en memoria con toda la info del usuario.
function FB_logout(userID) {
	Debugger.register("FB_logout");
	if (userID && objApp && objApp.FBObject) {
		objApp.FBObject = {};
	}
}