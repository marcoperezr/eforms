﻿<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);

	header("Content-Type:text/javascript; charset=UTF-8");

?>define([
	'underscore',
	'backbone',
	'text!templates/artusmodels/wizard.php',
	'views/modal/wizardModal',
	'libs/selectize.js/dist/js/standalone/selectize',
	'bootstrap',
	'css!libs/metisMenu/src/metisFolder.css',
	'libs/metisMenu/src/metisMenu',
	'css!libs/selectize.js/dist/css/selectize.bootstrap3.css',
	'css!cssFolder/materialModal.css',
	'libs/misc/contextMenu',
	'css!cssFolder/bootstrap.min.css'
], function(_, Backbone, Template, WizardModal){
	
	var wizard, aModal, arrSelected;

	var saveModel = function( idSurvey, callBackSave ) {

		if( wizard.find('a[href="#finish"]').parent().hasClass('disabled') ) return;

		var data = {
			'idModel': wizard.find('[name="id_modelartus"]').val(),
			'idSurvey': idSurvey,
			'name': wizard.find('[name="name"]').val(),
			'description': wizard.find('[name="description"]').val(),
			'attributes': [],
			'mapping': [],
			'action': 'saveArtusModel'
		};
		//@JRPP 2016-01-26: Se cambia la validacion si no se cumple la condicion [this.getAttribute('data-pathori').indexOf('[CAT]')] entonces tomo el valor de data-path anteriormente mandaba valor nullo.
		//		El mandar valor nullo ocacionaba que siempre se insertaran y eliminaran todos los atributos lo cual era incorrecto
		wizard.find('div.selectedList > div').map(function() { data.attributes.push( {path: this.getAttribute('data-path'), questionid: this.getAttribute('data-questionid'), key: this.getAttribute('data-key'), pathori: this.getAttribute('data-pathori'), idModelArtusQuestion: this.getAttribute('data-idModelArtusQuestion') } ); });
		//wizard.find('div.selectedList > div').map(function() { data.attributes.push( {path: this.getAttribute('data-path'), questionid: this.getAttribute('data-questionid'), key: this.getAttribute('data-key'), pathori: this.getAttribute('data-pathori').indexOf('[CAT]') > 0 ? this.getAttribute('data-pathori') : null} ); });

		wizard.find('.mapping_ebavel select.eBavelAppsFields').map(function() { if( this.value ) { data.mapping.push( {idquestion: this.getAttribute('data-idquestion'), 'field': this.value.split('|')[0]} )	}});

		wizard.find('a[href="#finish"]').parent().addClass('disabled');
		/** Aqui envia la informacion del modelo creado / editado a la pagina para guardarlo */
		$.post('main.php?BITAM_SECTION=eFormsModelsCollection', data).done(function( r ) {
			//Cerramos la ventana modal
			callBackSave( r );

			aModal.remove();
		}).fail(function() {
			wizard.find('a[href="#finish"]').parent().removeClass('disabled');
		});

	}

	var createTree = function(idSurvey, dataList, selected) {

		/** Limpiamos los atrbutos seleccionados */
		wizard.find('.selectedTree .selectedList').empty();

		wizard.find('.selectedTree .metisFolder').html( wizard.find('.selectedTree .metisFolder').attr('data-text') );

		wizard.find('.metisFolder').off('beforeopen').on('beforeopen', function(e, anObject) {

      		if(anObject.el.getAttribute('data-path') != '' && !anObject.el.getAttribute('data-isquestioncatalog')) {
      			if( $(anObject.el.parentElement.parentElement).find('> li:hidden').length == 0 ) {
      				e.preventDefault();
      				alert(this.getAttribute('data-textnoopen'));
      			}
      		}

      	});

		if(dataList) {

			wizard.find('.metisFolder').html(dataList).metisMenu({ toggle: false }).find('.attrField').on('click', function() {
	      		addAttr(this.getAttribute('data-pathlabel'), this.getAttribute('data-label'), this.getAttribute('data-path'), this.getAttribute('data-pathnumeric'), this.parentElement);
	      	});

			if(selected) {
				for(var i = 0; i < selected.length; i++) {
					var label = selected[i].match(/{(.*)}/), liSelected = wizard.find('.metisFolder a[data-path="'+ selected[i].replace(/{.*}/, '') +'"]');
					if(label)
						liSelected.attr('data-label', label[1]);

					liSelected.length && addAttr(liSelected.attr('data-pathlabel'), liSelected.attr('data-label'), selected[i], liSelected.attr('data-pathnumeric'), liSelected.parent()[0]);
				}
			}

		} else {

			var arrMappings = []

			aModal.$el.find('.mapping_ebavel select.eBavelAppsFields').each(function(){
				if( this.value ) {
					arrMappings.push( this.getAttribute('data-idquestion') + '|' + this.value );
				}
			})

			$.get('main.php?BITAM_SECTION=eFormsModelsCollection', {action: 'buildTree', mapping: arrMappings, idSurvey: idSurvey}).done(function( r ) {

				var $content = aModal.$el.find('ul.metisFolder');

				$content.empty();

				for(var key in r.tree) {
					$content.append( r.tree[key] );
				}

				$content.metisMenu({ toggle: false }).find('.attrField').on('click', function() {
		      		addAttr(this.getAttribute('data-pathlabel'), this.getAttribute('data-label'), this.getAttribute('data-path'), this.getAttribute('data-pathnumeric'), this.parentElement);
		      	});

		      	if( arrSelected && arrSelected.length ) {
		      		for(var i = 0; i < arrSelected.length; i++) {

						var label = arrSelected[i].path.match(/{(.*)}/), 
						sQuerySelector = 'a[data-path="' + arrSelected[i].path.replace(/{.*}/, '') +'"]', liSelected;

						if( arrSelected[i].questionid ) {

							if( arrSelected[i].isEBavel )
								sQuerySelector = '[data-key^="'+ arrSelected[i].questionid +'|"] ' + sQuerySelector;
							else
								sQuerySelector += '[data-questionid="'+ arrSelected[i].questionid +'"]';
						}

						liSelected = wizard.find( '.metisFolder ' + sQuerySelector );

						if(label)
							liSelected.attr('data-label', label[1]);
						liSelected.length && addAttr(liSelected.attr('data-pathlabel'), liSelected.attr('data-label'), arrSelected[i].path, liSelected.attr('data-pathnumeric'), liSelected.parent()[0], true, arrSelected[i].id);
					}
		      	}
				
			});

		}
		
	};

	var addAttr = function(pathlabel, label, path, pathnumeric, li, old, idModelArtusQuestion) {

		li.style.display = 'none';

		var dataKey = $(li).parents('li[data-key]').attr('data-key');
		if (idModelArtusQuestion === undefined) {
			idModelArtusQuestion = -1;
		}
		var $item = $('<div class="'+ ( old ? 'old' : '' ) +'" data-key="'+ (dataKey ? dataKey : '') +'" data-questionid="' + li.firstChild.getAttribute('data-questionid') + '" data-pathlabel="' + pathlabel + '" data-label="' + label + '" data-idModelArtusQuestion="'+ idModelArtusQuestion + '" data-pathori="'+ path.replace(/{.*}/, '') +'" data-ori="'+ (path.indexOf('[IND]') > 0 ? 'IND' : 'DIM') +'" data-pathnumeric="'+ pathnumeric +'" data-path="'+ path +'"><div class="icon-'+ (path.indexOf('[IND]') > 0 ? 'indicator' : 'dimension') +'"></div> <span>'+ pathlabel + label +'</span><i class="menu icon-ellipsis-vertical pull-right" style=""></i></div>');

		if( $item.attr('data-pathori').indexOf('[CAT]') > 0 ) {
			
			var rename = /{.*}/.exec($item.attr('data-path'));

			if( rename && rename[0] ) {
				rename = rename[0];
			} else {
				rename = '';
			}

			$item.attr('data-path', $item.attr('data-pathori').replace('[CAT]', '[DIM]') + rename);
		}

		$item.on('click', function() {
			removeAttr(this.getAttribute('data-pathori'), this);
		}).find('i.menu').contextMenu({
			onEvent: 'click',
    		menuSelector: "#contextMenu",
    		menuSelected: function (invokedOn, selectedMenu) {
    			var a = selectedMenu.attr('data-action'), data = invokedOn.parent().attr('data-path');
    			if( a == 1 ) {
    				//Rename
    				if( invokedOn.parent().attr('data-label') == selectedMenu.prev().val()) return;
    				if(data.indexOf('{') > 0) {
    					data = data.substring(0, data.indexOf('{'));
    				}
    				invokedOn.parent().attr('data-path', data + '{'+ selectedMenu.prev().val() +'}').attr('data-label', selectedMenu.prev().val());
    				invokedOn.prev().html( invokedOn.parent().attr('data-pathlabel') + selectedMenu.prev().val());
    				selectedMenu.prev().val('');
    			} else if (a == 2) {
    				//Change to dimension / indicator
    				//@JRPP 2016-01-26: Si cambia de indicador a dimencion o biseversa se insertara un nuevo registro por lo cual
    				//seteamos el atributo idModelArtusQuestion con el valor de -1
    				invokedOn.parent().attr('data-idModelArtusQuestion', -1);
    				if( data.indexOf('[IND]') > 0 ) {
    					invokedOn.parent().attr('data-pathori', data.replace('[IND]', '[DIM]'));
    					invokedOn.parent().attr('data-path', data.replace('[IND]', '[DIM]'));
    					invokedOn.prev().prev().removeClass('icon-indicator').addClass('icon-dimension');
    				} else {
    					invokedOn.parent().attr('data-pathori', data.replace('[DIM]', '[IND]'));
    					invokedOn.parent().attr('data-path', data.replace('[DIM]', '[IND]'));
    					invokedOn.prev().prev().removeClass('icon-indicator').addClass('icon-indicator');
    				}
    				
    			}
    		}
		}).on('bshow', function(e, data) {
			$('ul.dropdown-menu.dl-menu').hide();
			if( path.indexOf('[DIM]') > 0 ) {
				data.contextMenu.find('[data-action="2"]').parent().addClass('disabled');
			} else {
				data.contextMenu.find('[data-action="2"]').parent().removeClass('disabled');
				//si es dimension, cambiamos el texto a 'change to indicator'
				if( data.invokedOn.parent().attr('data-path').indexOf('[IND]') > 0 ) 
					data.contextMenu.find('[data-action="2"]').html( '<?= translate('Change to dimension') ?>' );
				else
					data.contextMenu.find('[data-action="2"]').html( '<?= translate('Change to indicator') ?>' );
			}
			data.contextMenu.find('input[type="text"]').val( data.invokedOn.parent().attr('data-label') );
		});
		
		wizard.find('.selectedList').append( $item );
	}

	var removeAttr = function(path, li) {
		
		var pathNumeric = li.getAttribute('data-pathnumeric');

		if( pathNumeric && wizard.find('.selectedList > div[data-pathnumeric^="' + pathNumeric + '."]').length || !pathNumeric &&  wizard.find('.selectedList > div[data-pathnumeric=""]').length == 1 &&  wizard.find('.selectedList > div').length != 1 ) {
			return false;
		}

		li.remove();

		wizard.find('ul.metisFolder a[data-path="'+ path +'"]').parent().show();
	}

	var addRowMapping = function( r, data ) {

		var $row = $('<div class="row"><div class="form-group col-sm-4"><select class="question"></select></div><div class="form-group col-sm-3"><select class="ebavelListApps"></select></div><div class="form-group col-sm-4"><select class="eBavelAppsFields"></select></div><div class="form-group col-sm-1"><a href="javascript:;" class="lremove" style="display: none"><?= translate('Remove') ?></a></div></div>');

		$row.find('select.question').on('change', function() {

			var self = this, last = this.getAttribute('data-last');

			this.setAttribute('data-last', this.value);

			$row.find('select.eBavelAppsFields').attr('data-idquestion', this.value);

			aModal.$el.find('.mapping_ebavel select.question').each(function() {

				if( self != this ) {

					last && (this.selectize.options[last].disabled = false, this.selectize.updateOption(last, this.selectize.options[last]));
					this.selectize.options[self.value].disabled = true;
					this.selectize.updateOption(self.value, this.selectize.options[self.value]);

					this.selectize.refreshOptions( false );
				}

			});

		}).html( r.questionSelect ).val( data && data.id_question );

		/*aModal.$el.find('.mapping_ebavel select.question').each(function() {
			$row.find('select.question option[value="'+ this.value +'"]').prop('disabled', true);
		});*/

		$row.find('select.question').selectize();

		$row.find('select.ebavelListApps').html( r.ebavelListApps ).val( data && data.app ).on('change', function() {

			var $select = $row.find('select.eBavelAppsFields');
			
			$select[0].selectize.destroy();

			$select.html( r.eBavelApps[this.value] ).val( data && data.value ).selectize();

		}).selectize();

		$row.find('select.eBavelAppsFields').on('change', function() {

			if( this.getAttribute('data-row') != 'true' ) {
				this.setAttribute('data-row', 'true');
				
				$row.find('a.lremove').show().on('click', function() {

					var i = 0, questionid = $row.find('select.question').val();

					if( r && r.infoModel && r.infoModel.attributes ) {
						for(i = 0; i < r.infoModel.attributes.length; i++ ) {
							if( r.infoModel.attributes[i].isEBavel && r.infoModel.attributes[i].questionid == questionid ) {
								alert('<?= translate("Action not allowed. There are dimension / indicator that should be deleted before") ?>');
								return false;
							}
						}
					}

					$row.remove();
				});

				if( !data ) addRowMapping( r );
			}		

		});

		if( data && data.app ) {
			$row.find('select.eBavelAppsFields').html( r.eBavelApps[ data.app ] ).val( data && data.value ).attr('data-idquestion', data.id_question).change().selectize();
		} else {
			$row.find('select.eBavelAppsFields').selectize();
		}



		if( data ) {
			aModal.$el.find('.mapping_ebavel .mappingquestions').prepend( $row );
		} else {
			aModal.$el.find('.mapping_ebavel .mappingquestions').append( $row );	
		}	

	}
	
	/** Recibe el ID de la definicion del modelo de Artus a cargar, si esta vacio crea uno nuevo */
	return function( idSurvey, idModel, callBackSave ) {

		wizard = $(_.template(Template)( {} ));

		var params = {'action': 'getInfoCreateArtusModel', 'idSurvey': idSurvey, 'idModel': idModel};

		aModal = new WizardModal({
			modalTitle: (idModel ? '<?= translate( 'Edit Artus model' ) ?>' : '<?= translate('Create Artus model') ?>'),
			className: 'material'
		});

		require(['selectize'], function(a) {
			/** Obtenemos informacion */
			$.get('main.php?BITAM_SECTION=eFormsModelsCollection', params).done(function( r ) {

					var $contentMapping;
					
					/** Agregamos el contenido para la ventana modal tipo wizard */
					aModal.setContent(wizard);

					wizard.find('a[href="#previous"]').html('<?= translate('Previous') ?>');
					wizard.find('a[href="#next"]').html('<?= translate('Next') ?>');
					wizard.find('a[href="#finish"]').html('<?= translate('Finish') ?>');

					arrSelected = null;

					if( idModel ) {
						
						wizard.find('#name').val( r.infoModel.name );

						wizard.find('#description').val( r.infoModel.description );

						arrSelected = r.infoModel.attributes;
					}

					/** Agregamos el evento al momento de terminar el wizard */
					aModal.on('onFinished', function(res) { 
						saveModel( idSurvey, callBackSave ) 
					});

					/** Agregamos las validaciones para los campos */
					aModal.on('onStepChanging onFinishing', function(e, currentIndex, newIndex) { 
						if( currentIndex > newIndex ) return true;

						if(
							currentIndex == 0 && ( !wizard.find('[name="name"]').val() || !wizard.find('[name="description"]').val() ) ||
							currentIndex == 2 && wizard.find('div.selectedList > div').length == 0
						) {
							e.stopPropagation()
							return false;
						}

						if( newIndex == 2 ) {

							createTree( idSurvey );

						}

						return true;
					});

					$contentMapping = aModal.$el.find('.mapping_ebavel .form-horizontal');

					for(var sSection in r.questions) {

						$contentMapping.append('<h4>'+ sSection +'</h4>');
						
						for( var i = 0; i < r.questions[sSection].length; i++ ) {
							
							$contentMapping.append( '<div class="form-group col-sm-6"><label class="col-sm-4 control-label" title="' + r.questions[sSection][i].text + '">' + r.questions[sSection][i].text + '</label><div class="col-sm-8"><select placeholder="<?= translate("eBavel field") ?>" data-idquestion="'+ r.questions[sSection][i].value +'" class="form-control" id="description" name="description"></select></div></div>' );
						}

						$contentMapping.find('select').html( r.eBavelApps );
					}

					if( idSurvey && idModel ) {
						wizard.find('[name="name"]').val( r.infoModel.name );
						wizard.find('[name="description"]').val( r.infoModel.description );
						wizard.find('[name="id_modelartus"]').val( idModel );
						//createPeriod(data.payload.dateList, data.payload.dataInfoModel.fieldperiod);
						//createTree(data.payload.dataInfoModel.form, data.payload.list, data.payload.dataInfoModel.attributes);

						if( r.infoModel.mapping && r.infoModel.mapping.length ) {
							for(var i = r.infoModel.mapping.length-1; i > -1; i-- ) {
								//$contentMapping.find('select[data-idquestion='+ r.infoModel.mapping[i].id_question +']').val( r.infoModel.mapping[i].value );
								addRowMapping( r, r.infoModel.mapping[i] );
							}
						}
					}

					addRowMapping( r );

					aModal.$el.find('.mapping_ebavel select.question').each(function() {

						for( var i = 0; i < r.infoModel.mapping.length; i++ ) {

							if( r.infoModel.mapping[i].id_question != this.value ) {
								this.selectize.options[ r.infoModel.mapping[i].id_question ].disabled = true;
								this.selectize.updateOption(r.infoModel.mapping[i].id_question, this.selectize.options[ r.infoModel.mapping[i].id_question ]);
							}

						}

						this.selectize.refreshOptions( false );

					});

					$contentMapping.find('select').selectize();

					//createTree(this.value);

				});	
		});

		

		$('body').append(aModal.render().width(990).$el);

		aModal.$el.find('.modal-body').html('<?= translate("Loading") ?>...');


		
	};

});
