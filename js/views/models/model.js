define([
	'underscore',
	'backbone',
	'js/views/base/basePageCollection.php?'
], function(_, Backbone, Base) {

	var loading = false;

	var Page = Base.extend({

		initialize: function() {

		},

		options: {
			myFormIDFieldName: 'idModel',
			objectType: 29,
			picture: 'images/dataconnect.png'
		},

		_urlCollection: function() {
			
			return 'main.php?BITAM_SECTION=eFormsModelsCollection&action=getCollection&idSurvey=' + intSurveyID;
		},

		/** Funcion de acciones para la botonera */
		_btn_add: function() {
			
			/** Cargamos el wizard para la creacion de modelos */
			this._loadWizard(intSurveyID, null);
		},

		/**
		 * Wizard para creacion / edicion de modelos de Artus
		 * @param  {[type]} idModel  [description]
		 * @param  {[type]} idSurvey [description]
		 * @return {[type]}          [description]
		 */
		_loadWizard: function( idSurvey, idModel ) {

			var self = this;

			loading = true;

			require(['js/views/models/wizardArtusModels.php?'], function( Wizard ) {

				Wizard( idSurvey, idModel, function( r ) {

					for(var i = 0; i < r.collection.length; i++) {
						self._addItem( r.collection[i] );
					}

				} );

				loading = false;

			});

		},

		_itemClick: function(id, ev, html) {

			!loading && this._loadWizard( intSurveyID, id );

		}

	});

	return Page;
		
});