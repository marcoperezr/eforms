define([
	'underscore',
	'backbone',
	'js/views/base/basePageCollection.php?'
], function(_, Backbone, Base) {

	var loading = false;

	var Page = Base.extend({

		initialize: function() {

		},

		options: {
			myFormIDFieldName: 'id',
			objectType: 28,
			picture: 'images/dataconnect.png'
		},

		_urlCollection: function() {
			
			return 'main.php?BITAM_SECTION=eFormsDataDestinationsCollection&action=getCollection&idSurvey=' + intSurveyID;
		},

		/** Funcion de acciones para la botonera */
		_btn_add: function() {
			
			/** Cargamos el wizard para la creacion de modelos */
			this._loadWizard(intSurveyID, null);
		},

		/**
		 * Wizard para creacion / edicion de conexiones a datos destino
		 * @param  {[type]} id  		Id de la conexion
		 * @param  {[type]} idSurvey 	Id de la encuesta
		 * @return {[type]}          	[description]
		 */
		_loadWizard: function( idSurvey, id ) {
			$(parent.window.document.getElementById('fondo')).show();
			var self = this;

			loading = true;

			require(['js/views/datadestinations/wizardDataDestinations.php?'], function( Wizard ) {

				Wizard( idSurvey, id, function( r ) {

					for(var i = 0; i < r.collection.length; i++) {
						self._addItem( r.collection[i] );
					}

				} );
				$(parent.window.document.getElementById('fondo')).hide();
				loading = false;

			});

		},
		
		_itemClick: function(id, ev, html) {

			!loading && this._loadWizard( intSurveyID, id );

		},
		
		//JAPR 2016-12-21: Validado que al terminar de eliminar ciertos objetos se puedan realizar acciones como redireccionar a otra página (#37X3EU)
		/* Para el caso de destinos de datos, como pueden ser parte de la configuración del diseño de una forma, es necesario actualizar el array de destinos en memoria para reflejar los 
		cambios realizados
		Ejecutada después de agregar un objeto de la colección que invoca a esta vista
		*/
		_afterAdd: function(oData) {
			if (!oData || oData.type != 1 || !window['arrDataDestinations'] || !window['arrDataDestinationsOrder']) {
				return;
			}
			
			var intNewDataDestinationPos = -1;
			var intNewDataDestinationID = parseInt(oData.id);
			var strNewDataDestinationName = String(oData.name).toLowerCase();
			//Validación para no duplicar los elementos originales cuando se está generando la vista de captura
			if (intNewDataDestinationID && !arrDataDestinations[intNewDataDestinationID]) {
				for (var intIndex in arrDataDestinationsOrder) {
					var intDataDestinationID = arrDataDestinationsOrder[intIndex];
					var strDestinationName = arrDataDestinations[intDataDestinationID];
					if (strDestinationName && strDestinationName.name && (String(strDestinationName.name).toLowerCase() > strNewDataDestinationName)) {
						//Dado a que el array está ordenado en forma ascendente, el primer nombre > que el nuevo indica la posición en la que se debe agregar el nuevo elemento
						intNewDataDestinationPos = intIndex;
						break;
					}
				}
				
				//Agrega el elemento en la posición identificada (si no hubiera elementos, el índice -1 se cambia a length para que se agregue simplemente como el último del array, ya que
				//eso significaría que ninguno de los nombres existentes era mayor que el que se está insertando o bien no hay elementos pre-existentes)
				if (intNewDataDestinationPos == -1) {
					intNewDataDestinationPos = arrDataDestinationsOrder.length;
				}
				
				arrDataDestinationsOrder.splice(intNewDataDestinationPos, 0, intNewDataDestinationID);
				arrDataDestinations[intNewDataDestinationID] = {id:intNewDataDestinationID, name:oData.name};
				//Actualiza la definición del campo si es que se encontraba cargado
				var objField = getFieldDefinition("advTab", "dataDestinationID");
				if (objField && objField.parentType == AdminObjectType.otyQuestion) {
					var objParentObject = getObject(objField.parentType, objField.id);
					if (objParentObject && objParentObject.type == Type.updateDest) {
						objField.options = arrDataDestinations;
						objField.optionsOrder = arrDataDestinationsOrder;
						if (objField.refreshValue) {
							objField.refreshValue();
						}
					}
				}
			}
		},
		
		//JAPR 2016-12-21: Validado que al terminar de eliminar ciertos objetos se puedan realizar acciones como redireccionar a otra página (#37X3EU)
		/* Para el caso de destinos de datos, como pueden ser parte de la configuración del diseño de una forma, es necesario actualizar el array de destinos en memoria para reflejar los 
		cambios realizados
		Ejecutada después de editar un objeto de la colección que invoca a esta vista
		*/
		_afterEdit: function(oData) {
			if (!oData || oData.type != 1 || !window['arrDataDestinations'] || !window['arrDataDestinationsOrder']) {
				return;
			}
			
			var intDataDestinationID = oData.id;
			if (intDataDestinationID && arrDataDestinations[intDataDestinationID]) {
				arrDataDestinations[intDataDestinationID].name = oData.name;
				//Actualiza la definición del campo si es que se encontraba cargado
				var objField = getFieldDefinition("advTab", "dataDestinationID");
				if (objField && objField.parentType == AdminObjectType.otyQuestion) {
					var objParentObject = getObject(objField.parentType, objField.id);
					if (objParentObject && objParentObject.type == Type.updateDest) {
						objField.options = arrDataDestinations;
						objField.optionsOrder = arrDataDestinationsOrder;
						if (objField.refreshValue) {
							objField.refreshValue();
						}
					}
				}
			}
		},
		
		//JAPR 2016-12-21: Validado que al terminar de eliminar ciertos objetos se puedan realizar acciones como redireccionar a otra página (#37X3EU)
		/* Para el caso de destinos de datos, como pueden ser parte de la configuración del diseño de una forma, es necesario actualizar el array de destinos en memoria para reflejar los 
		cambios realizados
		Ejecutada después de eliminar un objeto de la colección que invoca a esta vista
		*/
		_afterRemove: function(arrRemovedIDs) {
			//if (parent.doExecuteURL) {
			//alert('en el padre inmediato');
			//}
			if (!arrRemovedIDs || !window['arrDataDestinations'] || !window['arrDataDestinationsOrder']) {
				return;
			}
			
			var blnDataDestinationDeleted = false;
			for (var intIndex in arrRemovedIDs) {
				var intDataDestinationID = arrRemovedIDs[intIndex];
				if (intDataDestinationID) {
					if (arrDataDestinations[intDataDestinationID]) {
						for (var intDeleteIdx in arrDataDestinationsOrder) {
							var intDeleteDataDestinationID = arrDataDestinationsOrder[intDeleteIdx];
							if (intDeleteDataDestinationID == intDataDestinationID) {
								//Remueve el destino de la lista
								arrDataDestinationsOrder.splice(intDeleteIdx, 1);
								delete arrDataDestinations[intDataDestinationID];
								blnDataDestinationDeleted = true;
								break;
							}
						}
					}
				}
			}
			
			if (blnDataDestinationDeleted) {
				//Actualiza la definición del campo si es que se encontraba cargado
				var objField = getFieldDefinition("advTab", "dataDestinationID");
				if (objField && objField.parentType == AdminObjectType.otyQuestion) {
					var objParentObject = getObject(objField.parentType, objField.id);
					if (objParentObject && objParentObject.type == Type.updateDest) {
						objField.options = arrDataDestinations;
						objField.optionsOrder = arrDataDestinationsOrder;
						if (objField.refreshValue) {
							objField.refreshValue();
						}
					}
				}
			}
		}
		//JAPR
	});

	return Page;
		
});