﻿<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);

	header("Content-Type:text/javascript; charset=UTF-8");

	/** Leemos la informacion del cliente de Google */
	$sJson = file_get_contents('../../../datadestinations/libs/Google/client_secret_gDrive.json');

	$gInfoClient = json_decode( $sJson );

?>define([
	'underscore',
	'backbone',
	'text!templates/datadestinations/wizard.php',
	'views/modal/wizardModal',
	'libs/selectize.js/dist/js/standalone/selectize',
	'bootstrap',
	'libs/metisMenu/src/metisMenu'
], function(_, Backbone, Template, WizardModal){
	
	var self = this, wizard, aModal, arrSelected, arrSteps = [], arrScripts = { '1': ['basicinfo', 'mappingebavel', 'conditions'], '2': ['basicinfo', 'dropbox', 'fileformatstep', 'conditions'], '3': ['basicinfo', 'gdrive', 'fileformatstep', 'conditions'], '4': ['basicinfo','conditions'], '5': ['basicinfo', 'ftpstep', 'fileformatstep', 'conditions'],'6':['basicinfo','httpstep','fileformatstep','conditions']},
	infoDestination = {}, urlPath = 'loadexcel/';
	<? if (getMDVersion()>=esvDataDestination) { ?>
		arrScripts[4] = ['basicinfo', 'emailformatstep'<? if (getMDVersion()>=esvDataDestEmailDetail) { ?>, 'emaildetailstep'<? } ?>, 'conditions'];
	<? } ?>
	first = true;
	<?//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
	var objDefaultItemOptsArr = [{text:'<?=translate('Value')?>', value:0}];
	var objSketchItemOptsArr = [{text:'<?=translate('Image')?>', value:0},
		{text:'<?=translate('Strokes')?>', value:1},
		{text:'<?=translate('Source image')?>', value:2}];
	<?//@JAPR?>
	
	window.renderButtonsGoogle = function() {

		gapi.load('auth2', function() {
			gapi.auth2.getAuthInstance() || gapi.auth2.init({
				client_id: '<?= $gInfoClient->web->client_id; ?>',
				scope: 'https://www.googleapis.com/auth/drive'
			});

			/*gapi.auth2.getAuthInstance().attachClickHandler(document.getElementById('my-connect'), {},
    			function(user) {
      				fHandleSuccess( user.getBasicProfile().getEmail(), user.getAuthResponse().access_token );
    			}, function(error) {
    		});*/

			/*gapi.signin2.render('my-signin2', {
				'scope': 'https://www.googleapis.com/auth/drive',
				'width': 195,
				'height': 50,
				'theme': 'dark',
				'onsuccess': function( user ) {
					fHandleSuccess( user.getBasicProfile().getEmail(), user.getAuthResponse().access_token );
				},
				'onfailure': function( user ) {
					fHandleFailure( user );
			}});*/
		}); 

		loadConnections( 'gaccount' );

	}

	var fHandleFailure = function() {
		debugger;
	}

	var save = function( idSurvey, id, callBackSave ) {
		var isIncremental = 0;
		if (wizard.find(".incrementalLoad label input")){
			var isIncremental = (wizard.find(".incrementalLoad label input").prop('checked')? 1 : 0);
		}
		//GCRUZ 2016-12-13. Envío de HTTP en un solo POST
		var httpSendSinglePOST = 0;
		if (wizard.find(".incrementalLoad label input")){
			var httpSendSinglePOST = (wizard.find(".httpSendSinglePOST label input").prop('checked')? 1 : 0);
		}
		var additionaleMail = '';
		var arrAdditionaleMail = [];
		//debugger;
		//var re = new RegExp('([._a-zA-Z0-9\-]+)@([._a-zA-Z0-9-]+)', 'g')
		//var matches = re.exec(wizard.find('[name="alteMail"]').val());
		matches = new String(wizard.find('[name="alteMail"]').val()).match(/([._a-zA-Z0-9\-]+)@([._a-zA-Z0-9-]+)/g);		
		if($.isArray(matches)){
/*				debugger;
				for(i=0; i<matches.length; i++){
					arrAdditionaleMail.push(matches[i]);
				}*/
			additionaleMail = matches.join(";");
		}
		var data = {
			'id': id,
			'idSurvey': idSurvey,
			'name': wizard.find('[name="name"]').val(),
			'mapping': [],
			'active': ( wizard.find('[name="active"]').prop('checked') ? 1 : 0 ),
			'condition': wizard.find('[name="condition"]').val(),
			'type': wizard.find('[name="type"]:checked').val(),
			'eBavelAppForm': wizard.find('[name="eBavelAppForm"]').val(),
			'access_token': wizard.find('[name="access_token"]').val(),
			'gaccount': wizard.find('[name="gaccount"]').val(),
			'ftpName': wizard.find('[name="ftpName"]').val(),
			'ftpServer': wizard.find('[name="ftpServer"]').val(),
			'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
			'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
			'ftpaccount': wizard.find('[name="ftpaccount"]').val(),
			'path':wizard.find('[name="path"]').val(),
			'fileformat': wizard.find('[name="fileformat"]').val(),
			'action': 'save',
			'dpxaccount':wizard.find('[name="dpxaccount"]').val(),
			'httpaccount':wizard.find('[name="httpaccount"]').val(),
			'httpName': wizard.find('[name="httpName"]').val(),
			'httpServer': wizard.find('[name="httpServer"]').val(),
			'httpUsername': wizard.find('[name="httpUsername"]').val(),
			'httpPassword': wizard.find('[name="httpPassword"]').val(),
			'Parameters': wizard.find('[name="Parameters"]').val(),
			'httpUsernameParameter': wizard.find('[name="httpUsernameParameter"]').val(),
			'httpPasswordParameter': wizard.find('[name="httpPasswordParameter"]').val(),
			'httpRequest': wizard.find('[name="selectRequest"]').val(),
			'emailformat': wizard.find('[name="emailformat"]').val(),
			'cla_dimension': wizard.find('[name="cla_dimension"]').val(),
			'cla_ars': wizard.find('[name="arsemail"]').val(),
			'cla_dashboar': wizard.find('[name="Dashboardemail"]').val(),
			'isIncremental': isIncremental,
			'httpSendSinglePOST': httpSendSinglePOST,
			'additionaleMail': additionaleMail
			//MAPR 2019-07-22: Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
			, 'additionaleMailComposite': wizard.find('[name="emailcomposite"]').val()
			//JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
			, 'emailsubject': wizard.find('[name="emailsubject"]').val()
			, 'emailbody': wizard.find('[name="emailbody"]').val()
			//JAPR
		};

		if(data.type==3){
			data.realPath=wizard.find('[name="path"]').data("idFolderDrive")
		}		
		wizard.find('.mappingquestions .row').map(function() {

			var $row = $(this), questionid;

			if( questionid = $row.find('select.question').val() ) {
				<?//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
				data.mapping.push( { fieldformid: $row.find('select.eBavelAppsFields').val(), 'idquestion': questionid, 'formula': $row.find('input[name="formula"]').val(),
					additionalData: $row.find('select.item').val()} );
				<?//@JAPR?>
			}

		});

		/** Aqui envia la informacion del modelo creado / editado a la pagina para guardarlo */
		$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
			//Cerramos la ventana modal
			callBackSave( r );

			aModal.remove();
		});
	}
	var addButtonFormulate=function(){
		var $button=$('<button class="btn btn-default" type="button" style="width: 14px"><i><img src="images/admin/editFormula.gif" style="margin-left: -9px"/></i></button>');
		$button.on('click', function() {
			fnOpenFormulaEditor(this.parentElement, aModal.$el.find('.conditionContainer textarea').val());
		});
		aModal.$el.find('.conditionContainer').append( $button );

		//JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
		var $button2=$('<button class="btn btn-default" type="button" style="width: 14px"><i><img src="images/admin/editFormula.gif" style="margin-left: -9px"/></i></button>');
		$button2.on('click', function() {
			fnOpenFormulaEditor(aModal.$el.find('#emailsubject')[0], aModal.$el.find('#emailsubject').val());
		});
		aModal.$el.find('#emailsubject').after( $button2 );
		
		var $button3=$('<button class="btn btn-default" type="button" style="width: 14px"><i><img src="images/admin/editFormula.gif" style="margin-left: -9px"/></i></button>');
		$button3.on('click', function() {
			fnOpenEditorHTML(this.parentElement, aModal.$el.find('.bodyContainer textarea').val(), QTypesExt.html);
		});
		aModal.$el.find('.bodyContainer').append( $button3 );
		//JAPR
		//MAPR 2019-07-22: Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
		var $button4=$('<button class="btn btn-default" type="button" style="width: 14px"><i><img src="images/admin/editFormula.gif" style="margin-left: -9px"/></i></button>');
		$button4.on('click', function() {
			fnOpenFormulaEditor(aModal.$el.find('#emailcomposite')[0], aModal.$el.find('#emailcomposite').val());
		});
		aModal.$el.find('#emailcomposite').after( $button4 );
	}
	var createTreeFolder = function(idFolder) {
		if(idFolder==null){
			if(wizard.find('[name="path"]').val().length==0)
				wizard.find('[name="path"]').val("/");
				
			var data = {
				'gaccount': wizard.find('[name="gaccount"]').val(),
				'action': 'getTreeFolder',
				'type': wizard.find('[name="type"]:checked').val(),
				'ftpServer': wizard.find('[name="ftpServer"]').val(),
				'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
				'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
				'ftpaccount': wizard.find('[name="ftpaccount"]').val(),
				'dpxaccount':wizard.find('[name="dpxaccount"]').val()
			};
			$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
				if (typeof r == "object") {
						wizard.find(".loadingDiv").hide();
						wizard.find('.nextclass').attr("href",'#next');
						wizard.find('a[href="#next"]').removeClass("nextclass");
						aModal.showError( r.msg );
				}else {
					wizard.find(".loadingDiv").hide();
					first = false;
					wizard.find(".selectedTree").find("ul").empty();
					wizard.find(".selectedTree").find("ul").append(r);
					wizard.find(".selectedTree").find("ul").metisMenu({ toggle: false });
					wizard.steps('next');
				}
			});
			<? //@JAPR 2018-06-07: Corregido un bug, se estaba ejecutando esta creación de evento cada vez que ingresaba a la ventana de archivos (#VLMNXW)
				//Movido este código para que sólo se ejecute una vez la creación del evento, así no se agregará cada vez que se muestren carpetas/archivos ?>
			<? //@JAPR ?>
		}else{
			var data = {
				'gaccount': wizard.find('[name="gaccount"]').val(),
				'action': 'getTreeFolder',
				'idFolder':idFolder,
				'type': wizard.find('[name="type"]:checked').val(),
				'ftpServer': wizard.find('[name="ftpServer"]').val(),
				'ftpUsername': wizard.find('[name="ftpUsername"]').val(),
				'ftpPassword': wizard.find('[name="ftpPassword"]').val(),
				'ftpaccount': wizard.find('[name="ftpaccount"]').val(),
				'dpxaccount':wizard.find('[name="dpxaccount"]').val()
			};
			$.post('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', data).done(function( r ) {
				wizard.find(".selectedTree [name='"+idFolder+"'] ul").remove();
				wizard.find(".selectedTree [name='"+idFolder+"']").append("<ul class='collapse in' aria-expanded='false'>"+r+"</ul>");
				//wizard.find(".selectedTree").find(".metisFolder").metisMenu({ toggle: false });
				wizard.find(".selectedTree [name='"+idFolder+"'] li").on( "click", function(e) {
					e.stopPropagation();
					if(this.id.length > 0){
						wizard.find('[name="path"]').data("idFolderDrive",this.id);
						wizard.find('[name="path"]').val("");
						var liElement=$(this);
						while (liElement[0].tagName!="DIV") {
							var pathFolder=wizard.find('[name="path"]').val();
							wizard.find('[name="path"]').val(liElement.find('a')[0].text+"/"+pathFolder);
							liElement=liElement.parent().parent();
						}
						if($(this).attr('class')==undefined || $(this).attr('class').indexOf('active')< 0){
							if(this.id.length>0){
								$(this).addClass('active');
								if(wizard.find('[name="type"]:checked').val()==5){
									$(this).attr('name',wizard.find('[name="path"]').val())
									createTreeFolder(wizard.find('[name="path"]').val());
								}else{
									createTreeFolder(this.id);
								}
							}
						}else{
							$(this).removeClass('active');
							$(this).find('ul').removeClass('in');
						}
					}
				});
				
			});
		}
	}
	/* fieldformid id del filtro a configurar en esta row, questionSelect es el HTML con las opciones agrupadas de los campos de eBavel, fieldsSelect es el array con los
	pares [text, value] de los filtros a configurar, questionid es el valor actualmente seleccionado de este mapeo, formula es el texto adicional constante o fórmula según lo que
	se eligió para mapear siempre y cuando questionid == cierto valor que lo soporte */
	<?//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
	var addRowMapping = function( questionSelect, fieldsSelect, fieldformid, questionid, formula, bRemove, bRequired, oQuestions, iAdditionalData ) {

		if( typeof bRemove != "boolean" ) bRemove = true;

		<?//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Se agregó la combo para poder especificar que parte de la pregunta Sketch se debe utilizar?>
		var $row = $('<div class="row"><div class="form-group col-sm-4"><span>'+ ( bRequired ? '*' : '&nbsp;' ) +'</span><select style="" class="eBavelAppsFields"></select></div><div class="form-group col-sm-4"><select class="question '+ ( bRequired ? ' required ' : '' ) +'"></select></div><div style="display: none;" class="form-group col-sm-3"><div style="display: none; width: 100%;" class="input-group item"><select class="item '+ ( bRequired ? ' required ' : '' ) +'"></select></div></div><div class="form-group col-sm-3"><div style="display: none" class="input-group formulate"><span></span><input type="text" name="formula" class="form-control" placeholder="<?= translate('Formula') ?>"><span class="input-group-btn"><button class="btn btn-default" type="button"><i class="icon-superscript"></i></button></span></div></div><div class="form-group col-sm-1"><a href="javascript:;" class="lremove" style="display: none"><?= translate('Remove') ?></a></div></div>');
		<?//@JAPR?>

		$row.find('select.question').on('change', function() {

			if( this.getAttribute('data-row') != 'true' ) {
				this.setAttribute('data-row', 'true');

				addRowMapping( questionSelect, fieldsSelect );

				bRemove && $row.find('a.lremove').show()
			}

			$row.find('.input-group.formulate').hide();
			<?//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
			$row.find('.input-group.item').hide();
			$row.find('.input-group.item').parent().hide();
			<?//@JAPR?>

			if( this.value == '-39' || this.value == '-38' ) {
				$row.find('.input-group.formulate').show();
			}
			<?//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
			else {
				var objSelectize = $row.find('select.item')[0].selectize;
				if ( objSelectize ) {
					objSelectize.clearOptions();
					if ( <?=((getMDVersion() >= esvSketchPlus)?1:0) ?> && oQuestions[this.value] && oQuestions[this.value].qtypeid == <?=qtpSketchPlus?> ) {
						objSelectize.addOption(objSketchItemOptsArr);
						$row.find('.input-group.item').parent().show();
						$row.find('.input-group.item').show();
					}
					else {
						objSelectize.addOption(objDefaultItemOptsArr);
					}
					
					//Al hacer cambio de pregunta no se debe conservar el valor específico a mapear, así que se resetea al primero según el tipo de pregunta
					objSelectize.addItem(0, true);
				}
			}
			<?//@JAPR?>

		}).html( questionSelect ).selectize({items: [questionid]});
		
		<?//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
		$row.find('select.item').selectize();
		<?//@JAPR?>
		
		$row.find('a.lremove').on('click', function() {
			$row.remove();
		});

		$row.find('select.eBavelAppsFields').on('change', function() {

			$row.find('select.question').attr('data-fieldformid', this.value);

		}).selectize({
			options: fieldsSelect,
			items: [fieldformid]
		});

		$row.find('.input-group.formulate button').on('click', function() {
			fnOpenFormulaEditor(this.parentElement.parentElement, $row.find('.input-group.formulate input').val());
		});

		if( fieldformid ) {
			$row.find('select.eBavelAppsFields')[0].selectize.lock();
			$row.find('select.question').attr('data-fieldformid', fieldformid).attr('data-row', 'true');
		}

		if( questionid ) {
			bRemove && $row.find('a.lremove').show();
			if( questionid == '-39' || questionid == '-38' ) {
				$row.find('.input-group.formulate').show();

				$row.find('.input-group.formulate input').val( formula );
			}
			<?//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
			else {
				var objSelectize = $row.find('select.item')[0].selectize;
				if ( objSelectize ) {
					objSelectize.clearOptions();
					if ( <?=((getMDVersion() >= esvSketchPlus)?1:0) ?> && oQuestions[questionid] && oQuestions[questionid].qtypeid == <?=qtpSketchPlus?> ) {
						objSelectize.addOption(objSketchItemOptsArr);
						$row.find('.input-group.item').parent().show();
						$row.find('.input-group.item').show();
					}
					else {
						objSelectize.addOption(objDefaultItemOptsArr);
					}
					
					objSelectize.addItem(((!iAdditionalData)?0:iAdditionalData), true);
				}
			}
			<?//@JAPR?>
		}

		aModal.$el.find('.mapping_ebavel .mappingquestions').append( $row );
	}

	var gdrive = function() {
		
		(function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			po.src = 'https://apis.google.com/js/platform.js?onload=renderButtonsGoogle';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();	

	}

	var ftpstep = function() {
		
		loadConnections( 'ftpaccount' );
	}
	var dropbox=function() {

		loadConnections( 'dpxaccount' );
	}
	var httpstep=function() {

		loadConnections( 'httpaccount' );
	}

	var loadConnections = function( sEl ) {
		var type = wizard.find('[name="type"]:checked').val();
		if(wizard.reloadConnections || wizard.reloadConnections===undefined){
			$.get('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'getInfoSrvConnection', type: type}).done(function( r ) {
				
				wizard.find('[name="'+ sEl +'"]')[0].selectize.destroy();

				wizard.find('[name="'+ sEl +'"]').selectize({
					options: [ {text: '<?= translate( '(New connection)' ) ?>', value: 0} ].concat( r ),
					render: {
						option: function(item, escape) {
							var label = item.text
							var caption = item.value == 0 ? ( type == 2 ? '<?= translate('Creates a new connection to') ?> DropBox' : 
								( type == 3 ? '<?= translate('Creates a new connection to') ?> Google Drive' :
									( type == 5 ? '<?= translate('Creates a new connection to') ?> FTP' : ( type == 6 ? '<?= translate('Creates a new connection to') ?> HTTP':null )
								 ) )) : null;

							return '<div>' +
								'<span class="label">' + escape(label) + '</span>' +
								(caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
								'</div>';
						}
					},
					onItemAdd: function( v ) {
						if( v == 0 ) {
							
							if( type == 3 ) {
								var windowValidate = window.open("main.php?BITAM_SECTION=eFormsDataDestinationsCollection&action=validateGoogle", 'authGoogle', "width=800,height=436");
									
								window.fnSendCode=function( data ){
									windowValidate.close();

									var aSelectize = aModal.$el.find('[name="gaccount"]')[0].selectize;

									if( aSelectize ) {
										aSelectize.addOption([{ text: data.email, value: data.id }]);
										aSelectize.refreshOptions();
										aSelectize.addItem( data.id );
										aSelectize.refreshItems();
									}

								};
								
								<?//@JAPR 2018-06-08: Corregido un bug con las conexiones a Google Drive y Dropbox, no terminaba el proceso por bloqueo de cross-origin (#VAHJXY) ?>
								window.fnGoogleDriveCallback = function(event) {
									<?// IMPORTANT: Check the origin of the data! 
									/*Si el origen del mensaje no es el mismo servicio, no se debe continuar. Esto previene el error por Same-origin security policy
									que impedía que se agregara y refrescara el DataSource recien agregado debido al bloqueo de la ejecución de la función fnSendCode
									ejecutada por el código de callback de la llamada de GoogleDrive/DropBox, con este método siempre y cuando quien origina dicho
									código y quien lo reciben sean el mismo server (KPIOnline en este caso) se puede ejecutar sin problema
									*/
									?>
									if (~event.origin.indexOf('<?='http' . ( $_SERVER['SERVER_NAME'] == 'localhost' ? '' : 's' ) . '://' . $_SERVER['SERVER_NAME']?>')) {
										<?// The data has been sent from your site
										// The data sent with postMessage is stored in event.data ?>
										if ( event && event.data && event.data.email && event.data.id ) {
											fnSendCode(event.data);
										}
									} else {
										<?// The data hasn't been sent from your site!
										// Be careful! Do not use it. ?>
										console.log("Be careful! cross-origin data received");
									}
									
									window.removeEventListener('message', window.fnGoogleDriveCallback);
								}
								
								window.addEventListener('message', window.fnGoogleDriveCallback);
								<?//@JAPR ?>
							}
							if(type==2){
								var windowValidate=window.open("main.php?BITAM_SECTION=eFormsDataDestinationsCollection&action=validateDropBox", 'authDropBox', "width=800,height=436");
								window.fnSendCode=function(codeValidate){
									$.post( 'main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'validateDropBox', 'code': codeValidate} ).done(function(r) {
										var aSelectize = wizard.find('[name="dpxaccount"]')[0].selectize;

										if( aSelectize ) {

											aSelectize.addOption([{text: r.email, value: r.id}]);
											aSelectize.refreshOptions();
											aSelectize.addItem(r.id);
											aSelectize.refreshItems();
										}
										windowValidate.close();
									});
								};
								
								<?//@JAPR 2018-06-08: Corregido un bug con las conexiones a Google Drive y Dropbox, no terminaba el proceso por bloqueo de cross-origin (#VAHJXY) ?>
								window.fnDropboxCallback = function(event) {
									<?// IMPORTANT: Check the origin of the data! 
									/*Si el origen del mensaje no es el mismo servicio, no se debe continuar. Esto previene el error por Same-origin security policy
									que impedía que se agregara y refrescara el DataSource recien agregado debido al bloqueo de la ejecución de la función fnSendCode
									ejecutada por el código de callback de la llamada de GoogleDrive/DropBox, con este método siempre y cuando quien origina dicho
									código y quien lo reciben sean el mismo server (KPIOnline en este caso) se puede ejecutar sin problema
									*/
									?>
									if (~event.origin.indexOf('<?='http' . ( $_SERVER['SERVER_NAME'] == 'localhost' ? '' : 's' ) . '://' . $_SERVER['SERVER_NAME']?>')) {
										<?// The data has been sent from your site
										// The data sent with postMessage is stored in event.data ?>
										if ( event && event.data ) {
											fnSendCode(event.data);
										}
									} else {
										<?// The data hasn't been sent from your site!
										// Be careful! Do not use it. ?>
										console.log("Be careful! cross-origin data received");
									}
									
									window.removeEventListener('message', window.fnDropboxCallback);
								}
								
								window.addEventListener('message', window.fnDropboxCallback);
								<?//@JAPR ?>
							}
							if( type == 5 ) {
								wizard.find('#ftpName').val("");
								wizard.find('#ftpServer').val("");
								wizard.find('#ftpUsername').val("");
								wizard.find('#ftpPassword').val("");
								wizard.find('div.ftpnew').show();
								if (wizard.find('#ftpName').val()==""){
									wizard.find('#ftpName').focus();
								}else {
									wizard.find('[name="ftpServer"]').focus();
								}
							}
							if( type == 6 ) {
								wizard.find('#httpName').val("");
								wizard.find('#httpServer').val("");
								wizard.find('#httpUsername').val("");
								wizard.find('#httpPassword').val("");
								wizard.find('div.httpnew').show();
								if (wizard.find('#httpName').val()==""){
									wizard.find('#httpName').focus();
								}else {
									wizard.find('[name="httpServer"]').focus();
								}
							}
							
							wizard.find('[name="'+ sEl +'"]')[0].selectize.clear( true );
						}
					}
				});
				if( type == 5 ) {
					wizard.find('[name="'+ sEl +'"]').selectize().on('change', function() {
						if(wizard.find('[name="'+ sEl +'"]').val()!= "") {
							$.get(urlPath +'action.php?action=getConexion&idConnection=' + wizard.find('[name="'+ sEl +'"]').val()).done(function( response ) {
								if (response.success) {
									wizard.find('div.ftpnew').show();
									wizard.find('#ftpServer').val(response.ftp);
									wizard.find('#ftpUsername').val(response.user);
									wizard.find('#ftpPassword').val(response.pass);
									if(response.servicename){
										wizard.find('#ftpName').val(response.servicename);
									}
								}
							});
						}
					});
				}
				if( type == 6 ) {
					wizard.find('[name="'+ sEl +'"]').selectize().on('change', function() {
						if(wizard.find('[name="'+ sEl +'"]').val()!= "") {
							$.get(urlPath +'action.php?action=getHttpConexion&idConnection=' + wizard.find('[name="'+ sEl +'"]').val()).done(function( response ) {
								if (response.success) {
									wizard.find('div.httpnew').show();
									wizard.find('#httpServer').val(response.http);
									wizard.find('#httpUsername').val(response.user);
									wizard.find('#httpPassword').val(response.pass);
									if(response.servicename){
										wizard.find('#httpName').val(response.servicename);
									}
								}
							});
						}
					});
				}
				if( infoDestination.connection ) {
					selectConnection( sEl, infoDestination.connection.id );
				}
				wizard.reloadConnections=false;
			});
		}
	}

	var selectConnection = function( sEl, value ) {

		var aSelectize = wizard.find('[name="'+ sEl +'"]')[0].selectize;

		if( aSelectize ) {
			aSelectize.addItem( value );
			aSelectize.refreshItems();
		}

	}
	var selectDashboar = function(idSurvey, cla_dashboar) {
		$.get(urlPath +'action.php?action=loadDashboar&idSurvey='+idSurvey).done(function( response ) {
			if( response.success ) {
				wizard.find('[name="cla_dimension"]').val(response.cladimension);
				var selectDashboardemail = wizard.find('[name="Dashboardemail"]')[0].selectize;
				if( selectDashboardemail ) {
					//@JRPP 2015-12-01: Se agrega la opcion inicial
					//JAPR 2016-08-10: Removida la opción "ninguno" para forzar a seleccionar escenarios (#XMK1CP)
					//selectDashboardemail.addOption({value: 0, text: '(<?= translate( 'None' ) ?>)'});
					for( var key in  response.arrDashboards) {
						selectDashboardemail.addOption({value: key, text: response.arrDashboards[key]});
					}
					selectDashboardemail.refreshOptions();
					for( var key in  response.arrDashboards) {
						selectDashboardemail.addItem({key});
					}
					selectDashboardemail.refreshItems();
				}
				if (cla_dashboar) {
					selectDashboardemail.setValue(cla_dashboar);
				}
			} else {
				aModal.$el.find('.error-msg').html('Error: ' + response.msg);  
			}		
		});
	}
	var selectArs = function(idSurvey, cla_ars) {
		$.get(urlPath +'action.php?action=loadArs&idSurvey='+idSurvey).done(function( response ) {
			if( response.success ) {
				wizard.find('[name="cla_dimension"]').val(response.cladimension);
				var selectarsemail = wizard.find('[name="arsemail"]')[0].selectize;
				if( selectarsemail ) {
					//@JRPP 2015-12-01: Se agrega la opcion inicial
					//JAPR 2016-08-10: Removida la opción "ninguno" para forzar a seleccionar escenarios (#XMK1CP)
					//selectarsemail.addOption({value: 0, text: '(<?= translate( 'None' ) ?>)'});
					for( var key in  response.arrARS) {
						selectarsemail.addOption({value: key, text: response.arrARS[key]});
					}
					selectarsemail.refreshOptions();
					for( var key in  response.arrARS) {
						selectarsemail.addItem({key});
					}
					selectarsemail.refreshItems();
				}
				if (cla_ars) {
					selectarsemail.setValue(cla_ars);
				}
			} else {
				aModal.$el.find('.error-msg').html('Error: ' + response.msg);  
			}		
		});
	}
	/** Recibe el ID de la definicion de la conexion destino, si esta vacio crea uno nuevo */
	return function( idSurvey, id, callBackSave ) {
		infoDestination={};
		wizard = $(_.template(Template)( {} ));

		wizard.find('section').each(function(i) {
			arrSteps.push( this.getAttribute('data-section') );
		})

		aModal = new WizardModal({
			modalTitle: (id ? '<?= translate( 'Edit data destination' ) ?>' : '<?= translate('Create data destination') ?>'),
			className: 'material'
		});
		require(['selectize'], function(a) {

			$.get('main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'getInfo', idSurvey: idSurvey, id: id}).done(function( r ) {

				aModal.setContent( wizard );
				
				<? //@JAPR 2018-06-07: Corregido un bug, se estaba ejecutando esta creación de evento cada vez que ingresaba a la ventana de archivos (#VLMNXW)
					//Movido este código para que sólo se ejecute una vez la creación del evento, así no se agregará cada vez que se muestren carpetas/archivos ?>
				wizard.find('.metisFolder').on('beforeopen', function(e, anObject) {
					wizard.find('[name="path"]').val($(anObject.el).text()+"/");
					wizard.find('[name="path"]').data("idFolderDrive",anObject.el.parentElement.id);
					if($(anObject.el).parent().attr('aria-expanded')==undefined || $(anObject.el).parent().attr('aria-expanded')=="false"){
						$(anObject.el).parent().attr("aria-expanded","true");
						$(anObject.el).parent().parent().attr("aria-expanded","true");
						createTreeFolder($(anObject.el).parent().attr('id'));
					}else{
						$(anObject.el).parent().attr("aria-expanded","false");
						$(anObject.el).parent().parent().attr("aria-expanded","false");
					}
				});
				<? //@JAPR ?>
				
				addButtonFormulate();
				wizard.find('[name="eBavelAppForm"]').html( r.appFormsList ).on('change', function() {
					aModal.$el.find('.mapping_ebavel .mappingquestions').empty();

					var sForm = this.value;

					$.get( 'main.php?BITAM_SECTION=eFormsDataDestinationsCollection', {action: 'getAppFormFields', id: sForm} ).done(function( p ) {

						var nr = 0, arrMapping = {}, i;

						if( r.info && r.info.mapping && r.info.eBavelForm == sForm ) {
							for(var i = 0; i < r.info.mapping.length; i++) {
								arrMapping[ r.info.mapping[i].fieldformid ] = i;								
							}							
						}
						
						for(i = 0; i < p.fieldsSelect.length; i++) {
							<?//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
							if( arrMapping[p.fieldsSelect[i].value] >= 0  ) {
								addRowMapping( r.questionSelect, p.fieldsSelect, p.fieldsSelect[i].value, r.info.mapping[ arrMapping[p.fieldsSelect[i].value] ].questionid, 
									r.info.mapping[ arrMapping[p.fieldsSelect[i].value] ].formula, 
									false, p.fieldsSelect[i].required, r.questions, r.info.mapping[ arrMapping[p.fieldsSelect[i].value] ].additionalData );
							} else {
								addRowMapping( r.questionSelect, p.fieldsSelect, p.fieldsSelect[i].value, null, null, false, p.fieldsSelect[i].required, r.questions, 0 );	
							}
							<?//@JAPR?>
							nr ++;
						}

						/*if( nr < p.fieldsSelect.length ) {
							addRowMapping( r.questionSelect, p.fieldsSelect);
						}*/
					});
					
				});
				wizard.find(".typedestinations").on('change', function() {
					var type = wizard.find('[name="type"]:checked').val();
					wizard.reloadConnections=true;
					switch(type) {
						case "3": // Googledrive
						case "2":  //Dropbox
						case "5":   //FTP
							if (wizard.find('[name="fileformat"]').val() == "3") {
								wizard.find(".incrementalLoad label input").prop('checked', false);
								wizard.find(".incrementalLoad").hide();
							} else {
								wizard.find(".incrementalLoad").show();
							}
							
						break;
						default:
							wizard.find(".incrementalLoad label input").prop('checked', false);
							wizard.find(".incrementalLoad").hide();
							wizard.find(".httpSendSinglePOST label input").prop('checked', false);
							wizard.find(".httpSendSinglePOST").hide();
						break
					}
				});
				wizard.find('[name="fileformat"]').on('change', function() {
					var type = wizard.find('[name="type"]:checked').val();
					switch(type) {
						case "3": // Googledrive
						case "2":  //Dropbox
						case "5":   //FTP
							if (wizard.find('[name="fileformat"]').val() == "3") {
								wizard.find(".incrementalLoad label input").prop('checked', false);
								wizard.find(".incrementalLoad").hide();
							} else {
								wizard.find(".incrementalLoad").show();
							}
							
						break;
						default:
							wizard.find(".incrementalLoad label input").prop('checked', false);
							wizard.find(".incrementalLoad").hide();
						break
					}
				});
				wizard.find('[name="selectRequest"]').on('change', function() {
					var type = wizard.find('[name="type"]:checked').val();
					switch(type) {
						case "6": // HTTP
							if (wizard.find('[name="selectRequest"]').val() == "2") {
								wizard.find(".httpSendSinglePOST label input").prop('checked', false);
								wizard.find(".httpSendSinglePOST").hide();
							} else {
								wizard.find(".httpSendSinglePOST").show();
							}
						break;
						default:
							wizard.find(".httpSendSinglePOST label input").prop('checked', false);
							wizard.find(".httpSendSinglePOST").hide();
						break
					}
				});
				wizard.find('[name="emailformat"]').on('change', function() {
					switch(this.value) {
						case "1":
							//CSV File
							wizard.find(".arsemail").hide();
							wizard.find(".Dashboardemail").hide();
							wizard.find('[name="arsemail"]')[0].selectize.clear(true)
							wizard.find('[name="Dashboardemail"]')[0].selectize.clear( true );
						break;
						case "2":
							//XLS File
							wizard.find(".arsemail").hide();
							wizard.find(".Dashboardemail").hide();
							wizard.find('[name="arsemail"]')[0].selectize.clear(true)
							wizard.find('[name="Dashboardemail"]')[0].selectize.clear( true );
						break;
						case "<?=ddeftPDF?>":
							//PDF File
							wizard.find(".arsemail").hide();
							wizard.find(".Dashboardemail").hide();
							wizard.find('[name="arsemail"]')[0].selectize.clear(true)
							wizard.find('[name="Dashboardemail"]')[0].selectize.clear( true );
						break;
						case "<?=ddeftDashboard?>":
							//Dashboard File
							wizard.find(".arsemail").hide();
							wizard.find('[name="arsemail"]')[0].selectize.clear( true );
							selectDashboar(idSurvey);
							wizard.find(".Dashboardemail").show();
						break;
						case "<?=ddeftARS?>":
							//ARS File
							wizard.find(".Dashboardemail").hide();
							wizard.find('[name="Dashboardemail"]')[0].selectize.clear( true );
							selectArs(idSurvey);
							wizard.find(".arsemail").show();
						break;
					}
				});
				if( id && r.info ) {
					infoDestination = r.info;

					wizard.find('[name="name"]').val( r.info.name );
					
					wizard.find('[name="condition"]').val( r.info.condition );
					<?//@JAPR 2016-07-19: Corregido el valor default del checkbox de activo, siempre estaba como activado incluso cuando se había desactivado en un grabado previo (#A36JO4) ?>
					//if( r.info.active )
					wizard.find('[name="active"]').prop('checked', ((r.info.active)?true:false));

					wizard.find('[name="type"][value="'+ r.info.type +'"]').prop( 'checked', true );
					
					wizard.find('[name="eBavelAppForm"]').val( r.info.eBavelForm ).change();

					wizard.find('[name="fileformat"]').val( r.info.fileformat );
					<?//@JAPR 2016-07-20: Corregido el valor del tipo de request ya que no estaba seleccionandolo al editar (#N5DUR8) ?>
					wizard.find('[name="selectRequest"]').val( r.info.httpRequest );
					
					wizard.find('[name="path"]').val( r.info.path );

					wizard.find('[name="emailformat"]').val(r.info.emailformat);

					wizard.find('[name="alteMail"]').val(r.info.additionaleMail)
					switch(r.info.emailformat) {
						case "1":
							//CSV File
							wizard.find(".arsemail").hide();
							wizard.find(".Dashboardemail").hide();
						break;
						case "2":
							//XLS File
							wizard.find(".arsemail").hide();
							wizard.find(".Dashboardemail").hide();
						break;
						case "<?=ddeftPDF?>":
							//PDF File
							wizard.find(".arsemail").hide();
							wizard.find(".Dashboardemail").hide();
						break;
						case "<?=ddeftDashboard?>":
							//Dashboard File
							wizard.find(".arsemail").hide();
							selectDashboar(idSurvey,r.info.cla_dashboar );
							wizard.find(".Dashboardemail").show();
						break;
						case "<?=ddeftARS?>":
							//ARS File
							wizard.find(".Dashboardemail").hide();
							selectArs(idSurvey, r.info.cla_ars);
							wizard.find(".arsemail").show();
						break;
					}
					if ((r.info.type == "2" || r.info.type == "3" || r.info.type == "5") && r.info.fileformat != "3"){
						wizard.find(".incrementalLoad").show();
					}else {
						wizard.find(".incrementalLoad").hide();
					}
					if (r.info.type == "6" && r.info.httpRequest == "1"){
						wizard.find(".httpSendSinglePOST").show();
					}else {
						wizard.find(".httpSendSinglePOST").hide();
					}
					wizard.find('[name="cla_dimension"]').val(r.info.cla_dimension);
					if (r.info.parameters){
						wizard.find('[name="Parameters"]').val(r.info.parameters);
					}
					if (r.info.isIncremental){
						wizard.find(".incrementalLoad label input").prop('checked', r.info.isIncremental);
					}
					if (r.info.httpSendSinglePOST){
						wizard.find(".httpSendSinglePOST label input").prop('checked', r.info.httpSendSinglePOST);
					}
					//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
					wizard.find('[name="emailcomposite"]').val(r.info.additionaleMailComposite);
					//JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
					wizard.find('[name="emailsubject"]').val( r.info.emailsubject );
					wizard.find('[name="emailbody"]').val( r.info.emailbody );
					//JAPR
				}

				wizard.find('.steps.clearfix').hide();

				wizard.find('a[href="#previous"]').html('<?= translate('Previous') ?>');
				wizard.find('a[href="#next"]').html('<?= translate('Next') ?>');
				wizard.find('a[href="#finish"]').html('<?= translate('Finish') ?>');
				aModal.on('onStepChanged', function(o, a, b) {
					var type = wizard.find('[name="type"]:checked').val();
					aModal.hideError();
					if((type=="3" && a==6) || (type=="5" && a==6) || (type=="2" && a==6)) {
						first = true;
						aModal.$el.find('.nextclass').attr("href",'#next');
						aModal.$el.find('a[href="#next"]').removeClass("nextclass");
					}
				});
				aModal.on('onStepChanging', function( e, currentIndex, newIndex ) {
					var error = '<?= translate('In order to continue the information must be fully completed') ?>';
					var type = wizard.find('[name="type"]:checked').val(), numMapping = 0, newStep, fn;
					bValid = true;

					if( !type ) bValid = false;

					wizard.find('[id$="-p-'+ currentIndex +'"]').find('select[required], input[required]').each(function() {
						var iValid = true;

						if( this.getAttribute('requiredisnull') && wizard.find('[name="'+ this.getAttribute('requiredisnull') +'"]').val() ) {
							return;
						}

						if(!$(this).val()) {
							iValid = false;
							if (currentIndex == 4) {
								error = '<?= translate('The connection cannot be successful. There is some missing information at the FTP service') ?>';
							}
							
						} else if( this.getAttribute('pattern')  ) {
							iValid = new RegExp(this.getAttribute('pattern')).test( this.value );
							if (!iValid) {
								error = '<?= translate('Invalid FTP link service(ftp:\/\/your_FTP.domain_extension)') ?>';
							}
						}

						if( !iValid ) {
							bValid = false;
							if( this.tagName == 'SELECT' && this.selectize ) {
								this.selectize.$control.addClass('errorinput');
							} else if( this.tagName == 'INPUT' ) {
								this.classList.add('errorinput');
							}
						}

					});
					if( currentIndex == 1 ) {
						wizard.find('[id$="-p-'+ currentIndex +'"] select.question[data-fieldformid] option[value!=""]').each(function() {

							numMapping ++;

							if(!$(this).val() && this.classList.contains('required')) {
								bValid = false;
							}

						});

						if( bValid && numMapping == 0 ) bValid = false;
					}
					if( !bValid && currentIndex < newIndex ) {
						e.stopPropagation();
						aModal.showError(error);
						return false;
					}
					if (currentIndex == 0) {
						first = true;
					}
					//@JRPP 2015-12-01: Se agrega validacion par que no pueda avazar al siguiente paso sin antes llenar 
					//					correctamente las opciones de eMail
					if (currentIndex == 7 && type=="4" && newIndex==8){
						//Dasborad case
						if(wizard.find('[name="emailformat"]').val()=="<?=ddeftDashboard?>") {
							if (wizard.find('[name="Dashboardemail"]').val() == "" || wizard.find('[name="Dashboardemail"]').val() == "0") {
								e.stopPropagation();
								aModal.showError(error);
								return false;
							}
						}
						//ARS case
						if(wizard.find('[name="emailformat"]').val()=="<?=ddeftARS?>") {
							if (wizard.find('[name="arsemail"]').val() == "" || wizard.find('[name="arsemail"]').val() == "0") {
								e.stopPropagation();
								aModal.showError(error);
								return false;
							}
						}
					}
					if(((currentIndex == 3 && type==3 && newIndex==4) || (currentIndex == 4 && type==5 && newIndex==5) || (currentIndex == 2 && type==2 && newIndex==3)) && first){
						aModal.$el.find('a[href="#next"]').addClass("nextclass");
						aModal.$el.find('a[href="#next"]').removeAttr("href");
						aModal.$el.find(".selectedTree").find("ul").empty();
						aModal.$el.find(".loadingDiv").show();
						createTreeFolder(null);
						e.stopPropagation();
						return false;
					}else if(type==6 && newIndex==6){
						wizard.find(".selectedTree").hide();
						wizard.find(".labelTree").hide();
						wizard.find("#path").hide();
						wizard.find("#path").removeAttr('required');
						wizard.find(".selectedTree").parent().find('p')[1].hidden=true;
						wizard.find(".type-request").show();
					}

					newStep = $.inArray(arrSteps[ currentIndex ], arrScripts[ type ] );

					if( currentIndex < newIndex ){
						newStep++;
					}else{
						newStep--;
						//wizard.find(".selectedTree").empty();
						//wizard.find(".selectedTree").append('<div><ul class="metisFolder"><?= translate("Building folder tree") ?></ul></div>');
					}
					
					if( newIndex !=  $.inArray( arrScripts[ type ][ newStep ], arrSteps ) ) {
						wizard.steps('goto', $.inArray( arrScripts[ type ][ newStep ], arrSteps ));

						e.stopPropagation();

						return;
					}

					try{ fn =  eval(arrScripts[ type ][ newStep ])} catch(err){}

					if( typeof fn == 'function' ) {
						fn();
					}

				});

				/** Agregamos el evento al momento de terminar el wizard */
				aModal.on('onFinished', function(res) {
					save( idSurvey, id, callBackSave );
				});

				wizard.find('select').selectize({});

			});

		});
		/*@ears 2016-07-25 se aplica progressOn antes de invocar el dialog de destino de datos (issue GNFIZE)*/
		objFormTabs.tabs('datadestinations').progressOn();
		$('body').append(aModal.render().width(990).$el);
		aModal.$el.find('.modal-body').html('<?= translate("Loading") ?>...');
		objFormTabs.tabs('datadestinations').progressOff();	
	};

});
