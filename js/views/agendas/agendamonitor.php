﻿<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../../');
	require 'checkCurrentSession.inc.php';

	//GCRUZ 2016-02-25. Preguntar por formas al generar template de agendas
	require_once("appuser.inc.php");
	$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
	$arrSurveys = $theAppUser->getSurveys();
	if (trim($theAppUser->AgendasSurveysTemplate) != '')
		$arrSelectedSurveys = explode(',', trim($theAppUser->AgendasSurveysTemplate));
	else
		$arrSelectedSurveys = array();
	//Quitar "None" de la lista de formas
	unset($arrSurveys[0]);

	$besvAgendaExportTemplate = 0;
	if (getMDVersion() >= esvAgendaExportTemplate) {
		$besvAgendaExportTemplate = 1;
	}

	chdir($chdirCurrent);

	header("Content-Type:text/javascript; charset=UTF-8");

?>define([
	'underscore',
	'backbone',
	'js/views/base/listPageCollection.php?',
	'views/modal/basicModal',
	'views/modal/wizardModal',
	'views/agendas/schedulerlogic',
	'css!cssFolder/materialModal.css',
	'libs/selectize.js/dist/js/standalone/selectize',
	'css!libs/selectize.js/dist/css/selectize.bootstrap3.css'
], function(_, Backbone, Base, BasicModal, WizardModal, ScheduleFunction) {
	
	var loading = false;
	//GCRUZ 2016-02-25. Preguntar por formas al generar template de agendas
	var TemplateForms = [];
	var objWindows;
	var objDialog;
	var intDialogWidth = 450;
	var intDialogHeight = 500;
<?
	foreach ($arrSurveys as $surveyID => $surveyName)
	{
		if (count($arrSelectedSurveys) > 0 && array_search($surveyID, $arrSelectedSurveys) !== false)
		{
?>
			TemplateForms.push({text: "<?=htmlspecialchars($surveyName, ENT_QUOTES)?>", value: "<?=$surveyID?>", selected: true, checked: "1"});
<?
		}
		else
		{
?>
			TemplateForms.push({text: "<?=htmlspecialchars($surveyName, ENT_QUOTES)?>", value: "<?=$surveyID?>", checked: "0"});
<?
		}
	}
?>

	//Descarga la instancia de la forma de la memoria
	function doUnloadDialog() {
		console.log('doUnloadDialog');
		if (!objWindows || !objDialog) {
			return;
		}
		
		var objForm = objDialog.getAttachedObject();
		if (objForm && objForm.unload) {
			objForm.unload();
			objForm = null;
		}
		
		if (objWindows.unload) {
			objWindows.unload();
			objWindows = null;
			objWindows = new dhtmlXWindows({
				image_path:"images/"
			});
		}
		objDialog = null;
	}

	var Page = Base.extend({

		options: {
			myFormIDFieldName: 'AgendaID',
			objectType: 34,
			toolBarItems: [
				{id: "downloadAgendasTemplate", type: "button", text: "<?=translate('Download agendas template')?>", img: "../exportagenda.gif"},
				{id: "import", type: "button", text: "<?=translate('Import')?>", img: "../uploadagenda.gif"},
				{id: "export", type: "button", text: "<?=translate('Export')?>", img: "../donwloadagenda.gif"}/*, //GCRUZ 2015-09-04. No mostrar recalendarizar hasta que funcione
				{id: "changeSchedule", type: "button", text: "<?=translate('Reschedule')?>", img: "edit.png"}*/
			],
			header: {
				header: "<?=translate('Name')?>,<?=translate('User Name')?>,<?=translate('Check point')?>,<?=translate('Key')?>,<?=translate('Status')?>,<?=translate('Date')?>,<?=translate('Time')?>",
				colAlign: "left,left,left,left,left,left,left",
				colTypes: "ro,ro,ro,ro,ro,ro,ro",
				colSorting: "str,str,str,str,str,str,str",
				initWidths: "*,*,*,*,*,*,*",
				enableResizing: "false,false,false,false,false,false,false",
				headerFields: ['AgendaName', 'UserName', 'CheckpointName', 'FilterText', 'Status', 'CaptureStartDate', 'CaptureStartTime']
			},
			enableMultiselect: true,
			filterDate: true
		},

		_onRowSelect: function(id, pos) {

			var self = this, selected = id, data = this._collection[ id ];

			if( data.canEdit && !this._bRemoving ) {
				if( _.isString( selected ) ) selected = selected.split(',');

				$.get('main.php?BITAM_SECTION=SurveyAgendaExtCollection', {'action': 'edit', 'selected': selected}).done(function(r) {
					
					self._btn_add( r );

				});	
			}

			if( !this._bRemoving )
				this._objGrid.clearSelection();
			
		},

		_btn_add: function( data ) {

			var self = this;

			aModal = new WizardModal({
				modalTitle: '<?= translate('New agenda') ?>',
				className: 'material',
				labels: {
					cancel: "<?= translate('Cancel') ?>",
			        finish: "<?= translate('Finish') ?>",
			        next: "<?= translate('Next') ?>",
			        previous: "<?= translate('Previous') ?>"
				}
			});

			aModal.on('onStepChanging onFinished', function( e, currentIndex, newIndex ) {
				ScheduleFunction.validScheduler( $wizard.find('[id$="-p-'+ currentIndex +'"]'), e, currentIndex, newIndex );
			});

			aModal.on('onFinished', function(res) {

				if( res.isPropagationStopped() ) return;

				var params = {action: 'save'
					,AgendaID: aModal.$el.find('[name="id"]').val()
					,AgendaName: aModal.$el.find('[name="AgendaSchedName"]').val()
					,UserID: aModal.$el.find('[name="UserID"]').val()
					,CheckPointID: aModal.$el.find('[name="CheckPointID"]').val()
					,SurveyIDs: aModal.$el.find('[name="SurveyIDs"]').val()
					,CaptureStartDate: aModal.$el.find('[name="CaptureStartDate"]').val()
					,CaptureStartTime_Hours: aModal.$el.find('[name="CaptureStartTime_Hours"]').val()
					,CaptureStartTime_Minutes: aModal.$el.find('[name="CaptureStartTime_Minutes"]').val()						
				};

				aModal.$el.find('[name*="DSC_"]').each(function() {
					params[ this.name ] = this.value;
				});
				
				$.post('main.php?BITAM_SECTION=SurveyAgendaExtCollection', params).done(function(r){
					aModal.remove();

					for(var i = 0; i < r.collection.length; i++) {
						self._addItem( r.collection[i] );
					}
				});				
			});

			require(['text!templates/agendas/newscheduler.php', 'selectize'], function( Template ) {

				$wizard = $(_.template( Template )({}));

				$wizard.find('section.scheduler').html('<div class="form-group"><label><?= translate('Capture Start Date') ?></label><div><input readonly="readonly" style="cursor: pointer;width: 150px;background-color: #FFFFFF;" required type="text" class="form-control" id="CaptureStartDate" name="CaptureStartDate" maxlength="250" required></div></div><div class="form-group"><label><?= translate('Capture Start Time') ?></label><div><select class="pull-left" id="" name="CaptureStartTime_Hours" class="" style="width: 200px;"><option selected="" value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select><select class="pull-left" id="" name="CaptureStartTime_Minutes" class="" style="width: 200px;"><option selected="" value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select><label>(HH:mm)</label></div></div>');

				aModal.width(1000).setContent( $wizard );

				ScheduleFunction.schedulerLogic( $wizard, data );

			});

			$('body').append(aModal.render().$el);
		},

		_btn_changeSchedule: function() {

			var self = this, selected;

			if( (selected = this._objGrid.getSelectedRowId()) == null ) {
				selected = this._objGrid.getAllRowIds();
			}
			
			var aModal = new BasicModal({
				modalTitle: '<?= translate( 'Reschedule' ) ?>',
				className: 'material',
				labels: {
					close: '<?= translate('Close')?>'
				}
			}).addButton({
				'label': '<?= translate("Save") ?>',
				'callback': function() {
					
					$.post('main.php?BITAM_SECTION=SurveyAgendaExtCollection', {action: 'reschedule'
						,newStartDate: aModal.$el.find('[name="newStartDate"]').val()
						,newStartHour: aModal.$el.find('[name="newStartHour"]').val()
						,newStartMinutes: aModal.$el.find('[name="newStartMinutes"]').val()
						,'agendaRescheduleID': selected
						,'reschedule': 1
					}).done(function(r){
						aModal.remove();

						for(var i = 0; i < r.collection.length; i++) {

							self._addItem( r.collection[i] );
						}

					});
				}
			});

			require(['text!templates/agendas/agendamonitor.php'], function( Template ) {

				aModal.setContent( Template );

				var myCalendar = new dhtmlXCalendarObject("newStartDate");
				myCalendar.attachEvent("onShow", function(date){
				   this.base.style.zIndex = "10000";
				});

			});

			$('body').append(aModal.render().$el);
		},

		_btn_import: function() {
			var aModal = new BasicModal({
				modalTitle: '<?= translate( 'Import' ) ?>',
				className: 'material',
				labels: {
					close: '<?= translate('Close')?>'
				}
			});

			require(['libs/misc/SimpleAjaxUploader.min'], function() {

				aModal.setContent( '<form name="frmuploadAgendasFile" method="POST" action="uploadAgendas.php" enctype="multipart/form-data" target="frameuploadAgendasFile"><div class="form-group"><label for="exampleInputFile"><?= translate('Upload agendas file') ?></label><div class="row"><div class="col-md-6"><input style="height: 34px;" type="file" name="xlsfile"></div><div class="col-md-5"><button class="btn" type="submit" name="txtsubmit"><?= translate('Upload file') ?></button></div></div></div></form><iframe id="frameuploadAgendasFile" style="display: none"></iframe>' );

			});

			$('body').append(aModal.render().$el);
		},

		_btn_downloadAgendasTemplate: function() {

			var objButton = this;
			var esvAgendaExportTemplate = <?=$besvAgendaExportTemplate?>;
			
			if (esvAgendaExportTemplate)
			{
				//GCRUZ 2016-02-25. Preguntar por formas al generar template de agendas
				if (!objWindows) {
					//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				
				objDialog = objWindows.createWindow({
					id:"newObject",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialog.setText("<?=translate("Download agendas template")?>");
				objDialog.denyPark();
				objDialog.denyResize();
				objDialog.setIconCss("without_icon");
				$(".dhxwin_hdr").attr("style", "z-index: 0; background-color: wheat;");
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialog.attachEvent("onClose", function() {
					return true;
				});

				/* El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma */
				var objFormData = [
					{type:"settings"/*, offsetLeft:20*/},
					{type:"combo", comboType: "checkbox", name:"TemplateForms", label:"<?=translate("Forms")?>", labelAlign:"left", options:TemplateForms, labelWidth:150, inputWidth:250},
					{type:"block", blockOffset:50, offsetLeft:100, list:[
						{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
						{type:"newcolumn"},
						{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
					]},
					{type:"input", name:"UserID", value:"<?=$theAppUser->UserID?>", hidden:true}
				];

				var objForm = objDialog.attachForm(objFormData);
				objForm.adjustParentSize();
				objForm.setItemFocus("TemplateForms");
				objForm.attachEvent("onBeforeValidate", function (id) {
					console.log('Before validating the form: id == ' + id);
				});

				objForm.attachEvent("onAfterValidate", function (status) {
					console.log('After validating the form: status == ' + status);
				});
				
				objForm.attachEvent("onValidateSuccess", function (name, value, result) {
					console.log('Validating rule ok: name == ' + name + ', value == ' + value + ', result == ' + result);
				});
				
				objForm.attachEvent("onValidateError", function (name, value, result) {
					console.log('Error validating the form: name == ' + name + ', value == ' + value + ', result == ' + result);
				});

				objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
					if (!name || !inp || !ev || !ev.keyCode) {
						return;
					}
					
					switch (ev.keyCode) {
						case 13:
							//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
							if(objDialog.bitProcessing === undefined){
								objDialog.bitProcessing = true;
								setTimeout(function() {
									objForm.callEvent("onButtonClick", ["btnOk"]);
									objDialog.bitProcesing = undefined;
								}, 100);
							}
							break;
						case 27:
							setTimeout(function() {
								objForm.callEvent("onButtonClick", ["btnCancel"])
							}, 100);
							break;
					}
				});
				
				objForm.attachEvent("onButtonClick", function(name) {
					switch (name) {
						case "btnCancel":
							setTimeout(function () {
								doUnloadDialog();
							}, 100);
							break;
							
						case "btnOk":
							setTimeout(function() {
								if (objForm.validate()) {
									//doSendData();
									var selectedSurveys = objForm.getCombo("TemplateForms").getChecked();
									var userID = objForm.getItemValue("UserID");
									for (var i=0; i < TemplateForms.length; i++)
									{
										var option = objForm.getCombo("TemplateForms").getOptionByIndex(i);
										if (option.checked)
										{
											TemplateForms[option.index].checked = "1";
										}
										else
										{
											TemplateForms[option.index].checked = "0";
										}
									}
									objButton._getFrame().attr('src', "downloadAgendasTemplate.php?fill=0&appUserID="+userID+"&fillsurveys="+selectedSurveys.toString());
									doUnloadDialog();
								}
							}, 100);
							break;
					}
				});
			}
			else
			{
				objButton._getFrame().attr('src', "downloadAgendasTemplate.php?fill=0");
			}

		},

		_btn_export: function() {

//			debugger;
			//GCRUZ 2015-09-03. Agregar filtros del layout
			var CheckPointsIDs = [], startDate = "", endDate = "", UserID = [], StatusIDs = [], searchFilter = '', strParams = '&';
			//verificamos si hemos filtrado por estartDate y endDate
			var strDate = $("input[data-fieldname='date']")[0].value;
			if (strDate != "") {
				strDate = strDate.split('@');
				startDate = strDate[0];
				endDate = strDate[1];
			}
			if (startDate != '' && endDate != '')
			{
				strParams = strParams + 'startDate=' + startDate + '&endDate=' + endDate;
			}
			//Buscar filtro de CheckPoints
			$el = $("li.tagsCheckPointID").find('input');
			$el.each(function(index) {
				if($(this)[0].checked)
					CheckPointsIDs.push($(this).attr('data-description-id'));
			});
			if (CheckPointsIDs.length > 0)
			{
				var strCheckPointsIDs = CheckPointsIDs.toString();
				if (strParams != '&')
				{
					strParams = strParams + '&';
				}
				strParams = strParams + 'CheckPointIDs=' + strCheckPointsIDs;
			}
			//Buscamos si hemos seleccionado un usuario y obtenemos su Email
			$el = $("li.tagsUserID").find('input')
			$el.each(function(index) {
				if($(this)[0].checked)
					UserID.push($(this).attr('data-description-id'));
			});
			if (UserID.length > 0)
			{
				var strUserIDs = UserID.toString();
				if (strParams != '&')
				{
					strParams = strParams + '&';
				}
				strParams = strParams + 'UserIDs=' + strUserIDs;
			}
			//Buscar filtro de Estado
			$el = $("li.tagsstatus").find('input');
			$el.each(function(index) {
				if($(this)[0].checked)
					StatusIDs.push($(this).attr('data-description-id'));
			});
			if (StatusIDs.length > 0)
			{
				var strStatusIDs = StatusIDs.toString();
				if (strParams != '&')
				{
					strParams = strParams + '&';
				}
				strParams = strParams + 'StatusIDs=' + strStatusIDs;
			}
			//Filtro de búsqueda
			if ($("#Search")[0].value != 'search' && $("#Search")[0].value != '')
			{
				searchFilter = $("#Search")[0].value;
			}
			if (searchFilter != '')
			{
				if (strParams != '&')
				{
					strParams = strParams + '&';
				}
				strParams = strParams + 'searchFilter=' + searchFilter;
			}
			if (strParams == '&')
			{
				strParams = '';
			}
			this._getFrame().attr('src', "downloadAgendasTemplate.php?fill=1"+strParams);

		},

		_getFrame: function() {

			var $iframe = $('body iframe#upload_download');

			if( $iframe.length == 0 ) {
				$iframe = $( '<iframe style="display:none" id="upload_download">' );

				$('body').append( $iframe );
			}

			return $iframe;

		},

		_urlCollection: function( data ) {
			
			return {
				'url': 'main.php?BITAM_SECTION=SurveyAgendaExtCollection&action=getCollection',
				'data': data
			};

		},

		_applyFilter: function(filter) {
			
			this._objGrid.clearAll();

			this._load( {filter: filter} );
		}

	});

	var serializeJSON = function( form ) {
		var resData = {};
		
		var json = {};
		
		jQuery.map(form.serializeArray(), function(n, i) {
			if(json[n['name']]) {
				if($.isArray(json[n['name']])) {
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = new Array(json[n['name']]);
					json[n['name']].push(n['value']);
				}
			}
			else {
				if($('#' + n['name']).attr('multiple') == 'multiple') {
					json[n['name']] = new Array();
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = n['value'];
				}
			}
		});

		resData = json;

		return resData;	
	}

	return Page;
});
