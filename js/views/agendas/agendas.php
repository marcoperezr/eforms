﻿<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent); 

	header("Content-Type:text/javascript; charset=UTF-8");

?>define([
	'underscore',
	'backbone',
	'js/views/base/basePageCollection.php?'
], function(_, Backbone, Base) {
	
	var loading = false;

	var Page = Base.extend({

		options: {
			idSelected: null,
			myFormIDFieldName: 'id',
			objectType: 32,
			picture: ''
		},

		_btn_add: function() {

			!loading && this._loadWizard();
		},

		_itemClick: function( id ) {

			if( this.options.idSelected ) {
				
				if( this.options.idSelected == 1 ) {
					this._loadWizard( id );	
				}				
				return;
			}

			var self =  this;

			if( id == 1 ) {
				this.setTitle('<?= translate('Check point') ?>');
			} else if( id == 2 ) {
				this.setTitle('<?= translate('Scheduler') ?>');
			} else if( id == 3 ) {
				this.setTitle('<?= translate('Visits monitor') ?>');
			}

			this.options.idSelected = id;

			if( this.options.idSelected == 2 ) {
				/*@ears 2016-07-25 progressOn, al seleccionar Planificador (issue GNFIZE)*/
				/*barra de progreso en forma circular*/
				window.parent.objFormsLayoutMenu.cells("b").progressOn(); 
				require(['js/views/agendas/agendascheduler.php?'], function( Page ) {					
					window.parent.objFormsLayoutMenu.cells("b").progressOff();
					$('body').html('');
					new Page().render();
					
				});

				return;
			}

			if( this.options.idSelected == 3 ) {
				/*@ears 2016-07-25 progressOn, al seleccionar Monitor de visitas (issue GNFIZE)*/
				window.parent.objFormsLayoutMenu.cells("b").progressOn();
				require(['js/views/agendas/agendamonitor.php?'], function( Page ) {
					window.parent.objFormsLayoutMenu.cells("b").progressOff();
					$('body').html('');
					new Page().render();
				});
				return;
			}
                                                                                                                                                                                                              
			/*@ears 2016-07-25 progressOn, al seleccionar Punto de visita (issue GNFIZE)*/
			if( this.options.idSelected == 1 ) {
				window.parent.objFormsLayoutMenu.cells("b").progressOn();
			}	
			
			this.displayBtnAdd();
			this.displayBtnRemove();

			this._objListDataView.clearAll();

			$.get('main.php?BITAM_SECTION=AgendasCollection', {action: 'getcollection', type: id}).done(function( r ) {
				
				if( id == 1 ) {
					self.options.picture = "images/agendas/checkpointmaps.png";
					window.parent.objFormsLayoutMenu.cells("b").progressOff();
				}	

				for(var i = 0; i < r.collection.length; i++) {
					self._addItem( r.collection[i] );
				}
			});
		},

		_loadWizard: function( id ) {

			var self = this;

			loading = true;					
			
			require(['js/views/agendas/wizard'+ ( self.options.idSelected == 1 ? 'Checkpoint' : '' ) +'.php?'], function( Wizard ) {

				Wizard( id, function( r ) {
					for(var i = 0; i < r.collection.length; i++) {
						self._addItem( r.collection[i] );
					}

				} );

				loading = false;

			});

		}

	});

	return Page;
});
