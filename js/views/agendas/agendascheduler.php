﻿<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../../');
	require 'checkCurrentSession.inc.php';

	//GCRUZ 2016-02-25. Preguntar por formas al generar template de agendas
	require_once("appuser.inc.php");
	$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
	$arrSurveys = $theAppUser->getSurveys();
	if (trim($theAppUser->AgendasSurveysTemplate) != '')
		$arrSelectedSurveys = explode(',', trim($theAppUser->AgendasSurveysTemplate));
	else
		$arrSelectedSurveys = array();
	//Quitar "None" de la lista de formas
	unset($arrSurveys[0]);

	$besvAgendaExportTemplate = 0;
	if (getMDVersion() >= esvAgendaExportTemplate) {
		$besvAgendaExportTemplate = 1;
	}

	chdir($chdirCurrent);

	header("Content-Type:text/javascript; charset=UTF-8");

?>define([
	'underscore',
	'backbone',
	'js/views/base/listPageCollection.php?',
	'views/modal/basicModal',
	'views/modal/wizardModal',
	'views/agendas/schedulerlogic',
	'css!cssFolder/materialModal.css',
	'libs/selectize.js/dist/js/standalone/selectize',
	'css!libs/selectize.js/dist/css/selectize.bootstrap3.css'
], function(_, Backbone, Base, BasicModal, WizardModal, ScheduleFunction) {
	
	var loading = false;

	//GCRUZ 2016-02-25. Preguntar por formas al generar template de agendas
	var TemplateForms = [];
	var objWindows;
	var objDialog;
	var intDialogWidth = 450;
	var intDialogHeight = 500;
<?
	foreach ($arrSurveys as $surveyID => $surveyName)
	{
		if (count($arrSelectedSurveys) > 0 && array_search($surveyID, $arrSelectedSurveys) !== false)
		{
?>
			TemplateForms.push({text: "<?=htmlspecialchars($surveyName, ENT_QUOTES)?>", value: "<?=$surveyID?>", selected: true, checked: "1"});
<?
		}
		else
		{
?>
			TemplateForms.push({text: "<?=htmlspecialchars($surveyName, ENT_QUOTES)?>", value: "<?=$surveyID?>", checked: "0"});
<?
		}
	}
?>

	//Descarga la instancia de la forma de la memoria
	function doUnloadDialog() {
		console.log('doUnloadDialog');
		if (!objWindows || !objDialog) {
			return;
		}
		
		var objForm = objDialog.getAttachedObject();
		if (objForm && objForm.unload) {
			objForm.unload();
			objForm = null;
		}
		
		if (objWindows.unload) {
			objWindows.unload();
			objWindows = null;
			objWindows = new dhtmlXWindows({
				image_path:"images/"
			});
		}
		objDialog = null;
	}

	var Page = Base.extend({
<?
		//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
		//Agrega la versión de la metadata para poder condicionar los campos a mostrar en el Wizard
		//@JAPR 2016-02-25: Habilitada la importación de Schedulers de Agendas desde XLS
?>
		options: {
			myFormIDFieldName: 'AgendaSchedID',
			objectType: 33,
			toolBarItems: [
				{id: "downloadAgendasTemplate", type: "button", text: "<?=translate('Download agenda schedulers template')?>", img: "../exportagenda.gif"},
				{id: "import", type: "button", text: "<?=translate('Import')?>", img: "../uploadagenda.gif"},
				{id: "changeuser", type: "button", text: "<?=translate('Change user')?>", img: "edit.png"},
				{id: "changefrequency", type: "button", text: "<?=translate('Change frequency')?>", img: "edit.png"}
			],
			header: {
				header: "<?=translate('Name')?>,<?=translate('User Name')?>,<?=translate('Check point')?>,<?=translate('Key')?>",
				colAlign: "left,left,left,left",
				colTypes: "ro,ro,ro,ro",
				colSorting: "str,str,str,str",
				initWidths: "*,*,*,*",
				enableResizing: "false,false,false,false",
				headerFields: ['AgendaSchedName', 'UserName', 'CheckpointName', 'FilterText']
			},
			enableMultiselect: true,
			versions: {
				mdVersion: <?=getMDVersion()?>,
				versions: {esvAgendaFirsDayWeek: <?=esvAgendaFirsDayWeek?>}
			}
		},

		_onRowSelect: function(id, pos) {
			var self = this;

			if( Base.prototype._onRowSelect.apply(this, arguments) ) {

				$.get('main.php?BITAM_SECTION=SurveyAgendaSchedulerExtCollection', {'action': 'edit', 'selected': id}).done(function(r) {
					
					self._btn_add( r );

				});	
			}
		},

		_btn_add: function( data ) {

			var self = this, aModal, saveAgendaScheduler, $wizard;
<?
			//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
			//Agrega la versión de metadata, ya que al crear un nuevo elemento los campos se deben validar también en lugar de mostrarse todos
?>
			if (this.options && this.options.versions) {
				if (!data) {
					data = {};
				}
				$.extend(data, this.options.versions);
			}
			
			aModal = new WizardModal({
				modalTitle: '<?= translate( 'Scheduler' ) ?>',
				className: 'material',
				labels: {
					cancel: "<?= translate('Cancel') ?>",
			        finish: "<?= translate('Finish') ?>",
			        next: "<?= translate('Next') ?>",
			        previous: "<?= translate('Previous') ?>"
		    	}
			});

			saveAgendaScheduler = function(id, callback) {

				$.post('main.php?BITAM_SECTION=SurveyAgendaSchedulerExtCollection', _.extend( {action: 'save'}, serializeJSON($wizard))).done(function( r ) {
					callback( r );
				});
			}

			require(['text!templates/agendas/newscheduler.php', 'selectize'], function( Template ) {

				$wizard = $(_.template( Template )({}));

				aModal.width(1000).setContent( $wizard );
				//debugger;
				ScheduleFunction.schedulerLogic( $wizard, data );

			});

			aModal.on('onStepChanging onFinished', function( e, currentIndex, newIndex ) {
				ScheduleFunction.validScheduler( $wizard.find('[id$="-p-'+ currentIndex +'"]'), e, currentIndex, newIndex );
			});

			aModal.on('onFinished', function(res) {
				!res.isPropagationStopped() && saveAgendaScheduler( null, function( r ) {
					aModal.remove();
					for(var i = 0; i < r.collection.length; i++) {
						self._addItem( r.collection[i] );
					}
				} );
			});

			$('body').append(aModal.render().$el);

		},

		_btn_changeuser: function() {
			var self = this, selected;

			if( (selected = this._objGrid.getSelectedRowId()) == null ) {
				selected = this._objGrid.getAllRowIds();
			}

			var aModal = new BasicModal({
				modalTitle: '<?= translate( 'Change user' ) ?>',
				className: 'material',
				labels: {
					close: '<?= translate('Close')?>'
				}
			}).addButton({
				'label': '<?= translate("Save") ?>',
				'callback': function() {

					if( aModal.$el.find('select').val() ) {
						aModal.remove();
						$.post('main.php?BITAM_SECTION=SurveyAgendaSchedulerExtCollection', {action: 'changeUser', id: aModal.$el.find('select').val(), 'selected': selected}).done(function(r){

							self._objGrid.clearSelection();

							for(var i = 0; i < r.collection.length; i++) {
								self._addItem( r.collection[i] );
							}

						});
					} else {
						aModal.$el.find('select + div.selectize-control .selectize-input').addClass('errorinput');
					}					
				}
			});

			require(['selectize'], function(a) {
				$.get('main.php?BITAM_SECTION=SurveyAgendaSchedulerExtCollection&action=getUserList').done(function( r ) {
				
					aModal.setContent(
						$('<select>')
					);
					
					aModal.$el.find('select').selectize({
						options: r
					})

				});
			});

			$('body').append(aModal.render().$el);

		},

		_btn_changefrequency: function() {

			var self = this, selected, $scheduler;

			if( (selected = this._objGrid.getSelectedRowId()) == null ) {
				selected = this._objGrid.getAllRowIds();
			}

			$.ajax( this._urlCollection( {'arrAgendaSchedIDs': selected.split(',')} ) ).done( function( r ) {
				require(['text!templates/agendas/newscheduler.php', 'selectize'], function( Template ) {

					r = r.collection[0] || {};

					$scheduler = $(_.template( Template )({}));

					$scheduler.find('[name="CaptureStartTime_Hours"]').val( r.CaptureStartTime_Hours < 10 ? '0' + r.CaptureStartTime_Hours : r.CaptureStartTime_Hours );
					$scheduler.find('[name="CaptureStartTime_Minutes"]').val( r.CaptureStartTime_Minutes < 10 ? '0' + r.CaptureStartTime_Minutes : r.CaptureStartTime_Minutes );
					$scheduler.find('[name="GenerateAgendasNextDayTime_Hours"]').val( r.GenerateAgendasNextDayTime_Hours < 10 ? '0' + r.GenerateAgendasNextDayTime_Hours : r.GenerateAgendasNextDayTime_Hours );
					$scheduler.find('[name="GenerateAgendasNextDayTime_Minutes"]').val( r.GenerateAgendasNextDayTime_Minutes < 10 ? '0' + r.GenerateAgendasNextDayTime_Minutes : r.GenerateAgendasNextDayTime_Minutes );

					$scheduler.find('section.scheduler').attr('id', "a-p-0").wrap('<form class="scheduler">').find('[name="RecurPatternType"]').removeAttr('required');

					$scheduler = $scheduler.find('style, form.scheduler');

					aModal.width(1000).setContent( $scheduler );

					ScheduleFunction.schedulerLogic( $scheduler );

				});
			});

			var aModal = new BasicModal({
				modalTitle: '<?= translate( 'Change frequency' ) ?>',
				className: 'material',
				labels: {
					close: '<?= translate('Close')?>'
				}
			}).addButton({
				'label': '<?= translate("Save") ?>',
				'callback': function() {
					var anEvent = $.Event(), data = serializeJSON( $scheduler.filter('form.scheduler') );
					
					ScheduleFunction.validScheduler( $scheduler.filter('form.scheduler'), anEvent, 0 );

					if( anEvent.isPropagationStopped() ) return;

					if( data['RecurPatternType'] ) {
						aModal.remove();

						$.post('main.php?BITAM_SECTION=SurveyAgendaSchedulerExtCollection', _.extend( {action: 'save', id: selected}, data )).done(function(r){

							self._objGrid.clearSelection();

							for(var i = 0; i < r.collection.length; i++) {
								self._addItem( r.collection[i] );
							}

						});
					} else {
						$scheduler.find('[name="RecurPatternType"] + div.selectize-control .selectize-input').addClass('errorinput');
					}
				}
			});

			$('body').append(aModal.render().$el);
		},
		
		_btn_import: function() {
			var aModal = new BasicModal({
				modalTitle: '<?= translate( 'Import' ) ?>',
				className: 'material',
				labels: {
					close: '<?= translate('Close')?>'
				}
			});

			require(['libs/misc/SimpleAjaxUploader.min'], function() {

				aModal.setContent( '<form name="frmuploadAgendasFile" method="POST" action="uploadAgendasScheduler.php" enctype="multipart/form-data" target="frameuploadAgendasFile"><div class="form-group"><label for="exampleInputFile"><?= translate('Upload agendas scheduler file') ?></label><div class="row"><div class="col-md-6"><input style="height: 34px;" type="file" name="xlsfile"></div><div class="col-md-5"><button class="btn" type="submit" name="txtsubmit"><?= translate('Upload file') ?></button></div></div></div></form><iframe id="frameuploadAgendasFile" style="display: none"></iframe>' );

			});

			$('body').append(aModal.render().$el);
		},
		
		_btn_downloadAgendasTemplate: function() {

			//debugger;
			var objButton = this;
			var esvAgendaExportTemplate = <?=$besvAgendaExportTemplate?>;
			
			if (esvAgendaExportTemplate)
			{
				//GCRUZ 2016-02-25. Preguntar por formas al generar template de agendas
				if (!objWindows) {
					//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				
				objDialog = objWindows.createWindow({
					id:"newObject",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialog.setText("<?=translate("Download agenda schedulers template")?>");
				objDialog.denyPark();
				objDialog.denyResize();
				objDialog.setIconCss("without_icon");
				$(".dhxwin_hdr").attr("style", "z-index: 0; background-color: wheat;");
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialog.attachEvent("onClose", function() {
					return true;
				});

				/* El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma */
				var objFormData = [
					{type:"settings"/*, offsetLeft:20*/},
					{type:"combo", comboType: "checkbox", name:"TemplateForms", label:"<?=translate("Forms")?>", labelAlign:"left", options:TemplateForms, labelWidth:150, inputWidth:250},
					{type:"block", blockOffset:50, offsetLeft:100, list:[
						{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
						{type:"newcolumn"},
						{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
					]},
					{type:"input", name:"UserID", value:"<?=$theAppUser->UserID?>", hidden:true}
				];

				var objForm = objDialog.attachForm(objFormData);
				objForm.adjustParentSize();
				objForm.setItemFocus("TemplateForms");
				objForm.attachEvent("onBeforeValidate", function (id) {
					console.log('Before validating the form: id == ' + id);
				});

				objForm.attachEvent("onAfterValidate", function (status) {
					console.log('After validating the form: status == ' + status);
				});
				
				objForm.attachEvent("onValidateSuccess", function (name, value, result) {
					console.log('Validating rule ok: name == ' + name + ', value == ' + value + ', result == ' + result);
				});
				
				objForm.attachEvent("onValidateError", function (name, value, result) {
					console.log('Error validating the form: name == ' + name + ', value == ' + value + ', result == ' + result);
				});

				objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
					if (!name || !inp || !ev || !ev.keyCode) {
						return;
					}
					
					switch (ev.keyCode) {
						case 13:
							//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
							if(objDialog.bitProcessing === undefined){
								objDialog.bitProcessing = true;
								setTimeout(function() {
									objForm.callEvent("onButtonClick", ["btnOk"]);
									objDialog.bitProcesing = undefined;
								}, 100);
							}
							break;
						case 27:
							setTimeout(function() {
								objForm.callEvent("onButtonClick", ["btnCancel"])
							}, 100);
							break;
					}
				});
				
				objForm.attachEvent("onButtonClick", function(name) {
					switch (name) {
						case "btnCancel":
							setTimeout(function () {
								doUnloadDialog();
							}, 100);
							break;
							
						case "btnOk":
							setTimeout(function() {
								if (objForm.validate()) {
									//doSendData();
									var selectedSurveys = objForm.getCombo("TemplateForms").getChecked();
									var userID = objForm.getItemValue("UserID");
									for (var i=0; i < TemplateForms.length; i++)
									{
										var option = objForm.getCombo("TemplateForms").getOptionByIndex(i);
										if (option.checked)
										{
											TemplateForms[option.index].checked = "1";
										}
										else
										{
											TemplateForms[option.index].checked = "0";
										}
									}
									objButton._getFrame().attr('src', "downloadAgendasSchedulersTemplate.php?fill=0&appUserID="+userID+"&fillsurveys="+selectedSurveys.toString());
									doUnloadDialog();
								}
							}, 100);
							break;
					}
				});
			}
			else
			{
				objButton._getFrame().attr('src', "downloadAgendasSchedulersTemplate.php?fill=0");
			}

		},
		
		_getFrame: function() {

			var $iframe = $('body iframe#upload_download');

			if( $iframe.length == 0 ) {
				$iframe = $( '<iframe style="display:none" id="upload_download">' );

				$('body').append( $iframe );
			}

			return $iframe;

		},
		
		_urlCollection: function( data ) {
			
			return {
				'url': 'main.php?BITAM_SECTION=SurveyAgendaSchedulerExtCollection&action=getCollection',
				'data': data
			};

		},

		_applyFilter: function(filter, json, lastClick) {
			
			this._objGrid.clearAll();

			this._load( {filter: filter, lastClick: lastClick} );
		}

	});

	var serializeJSON = function( form ) {
		var resData = {};
		
		var json = {};
		
		jQuery.map(form.serializeArray(), function(n, i) {
			if(json[n['name']]) {
				if($.isArray(json[n['name']])) {
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = new Array(json[n['name']]);
					json[n['name']].push(n['value']);
				}
			}
			else {
				if($('#' + n['name']).attr('multiple') == 'multiple') {
					json[n['name']] = new Array();
					json[n['name']].push(n['value']);
				}
				else {
					json[n['name']] = n['value'];
				}
			}
		});

		resData = json;

		return resData;	
	}

	return Page;
});
