define([], function() {
	var schedulerLogic = function( $wizard, data ) {
		var values, i;
		data = data || {};

		if( data ) {
			for(var name in data) {
				if( !_.isArray( data[name] ) ) {
					$wizard.find('[name="'+ name +'"]').val( data[name] );
				} else if ( _.isArray( data[name] ) ) {
					for(var key in data[name]) {
						$wizard.find('[name="'+ name +'"] option[value="' + data[name][key] + '"]').prop("selected", true);
					}
				}
			}
		}

		$wizard.find('select[name="CheckPointID"]').on('change', function() {

			var catalogID = this.value;

			$.get('main.php?BITAM_SECTION=AgendasCollection', {action: 'getAttr', attrKeys: true, catalogid: catalogID}).done(function( r ) {

				var arrFields = [], selects, filters = {}, selected = '';

				for(var i = 0; i < r.length; i++) {

					selected = '';

					if( data['FilterTextValue'] && data['FilterTextValue'][ r[i].value ] ) {
						selected = '<option value="'+ data['FilterTextValue'][ r[i].value ] +'" selected>'+ data['FilterTextValue'][ r[i].value ] +'</option>';
					}

					arrFields.push('<div class="form-group"><label for="catalog">* '+ r[i].text +'</label><select data-attrid="'+ r[i].value +'" required name="DSC_'+ r[i].value +'">'+ selected +'</select><div class="clear" data-select="DSC_'+ r[i].value +'"><i class="icon-remove"></i></div></div>');
				}

				selects = $wizard.find('section[data-section="filtersforms"] div.keys').html( arrFields.join('') ).find('select');

				$wizard.find('div.clear').on('click', function() {
					$wizard.find( '[name="'+ this.getAttribute('data-select') +'"]' )[0].selectize.clear(true);
				});

				selects.selectize( {
					preload: 'focus',
					sortField: '$score'/*,
					load: function(v, callback) {						
						if( v != '' ) return;
						var self = this, attrid = this.$input.data('attrid'), filters = {};
						this.$input.data('loading', true);
						selects.each(function() {
							if( attrid != this.getAttribute('data-attrid') && this.value != '' ) filters[ this.getAttribute('data-attrid') ] = [ this.value ];
						});
						$.get('main.php?BITAM_SECTION=AgendasCollection', {action: 'getValueAttrs', attrKey: this.$input.data('attrid'), catalogid: catalogID, filters: filters}).done(function( r ) {
							callback( r )
							setTimeout(function() {
								self.$input.data('loading', false);
							})
						});
					}*/
				});

				selects.each(function() {
					var select = $(this);
					this.selectize.on('focus', function() {
						//if( select.data('loading') ) return;
						//this.clearOptions();
						if( select.data('loading') ) return;
						var value = this.getValue();
						select.data('loading', true);
						this.clearOptions();
						var self = this, attrid = this.$input.data('attrid'), filters = {};
						selects.each(function() {
							if( attrid != this.getAttribute('data-attrid') && this.value != '' ) filters[ this.getAttribute('data-attrid') ] = [ this.value ];
						});
						$.get('main.php?BITAM_SECTION=AgendasCollection', {action: 'getValueAttrs', attrKey: this.$input.data('attrid'), catalogid: catalogID, filters: filters}).done(function( r ){
							self.addOption( r );
							self.refreshOptions();
							self.setValue(value, true);
							setTimeout(function() {
								select.data('loading', false);
							})
							//self.open();
						});
					})
				})
			});

		});

		$wizard.find('[name="RecurPatternType"]').on('change', function() {
			$wizard.find('[data-recurtype]').hide();
			$wizard.find('[data-recurtype="'+ this.value +'"]').show();
		});

		$wizard.find('[name="DailyOpt"]').on('change', function() {
			if(this.value == 0)
				$wizard.find('[data-recurtype="0"] div.DNumDays').show();
			else
				$wizard.find('[data-recurtype="0"] div.DNumDays').hide();
		});

		$wizard.find('[name="MonthlyOpt"]').on('change', function() {
			if(this.value == 1) {
				$wizard.find('div#boxcalendar').hide();
				$wizard.find('div.MonthlyOpt00').hide();
				$wizard.find('div.MonthlyOpt01').show();
			}
			else {
				$wizard.find('div.MonthlyOpt01').hide();
				$wizard.find('div#boxcalendar').show();
				$wizard.find('div.MonthlyOpt00').show();
			}
		})

		$wizard.find('[name="YearlyOpt"]').on('change', function() {
			if(this.value == 0) {
				$wizard.find('div.YearlyOpt0').show();
				$wizard.find('div.YearlyOpt1').hide();
			} else {
				$wizard.find('div.YearlyOpt0').hide();
				$wizard.find('div.YearlyOpt1').show();
			}
		});

		$wizard.find('#tDaysOfMonth td').on('click', function() {
			if( !this.classList.contains('selected') ) this.classList.add('selected');
			else this.classList.remove('selected');

			var res = [];

			$wizard.find('#tDaysOfMonth td.selected').each(function() {
				res.push( this.id.replace('d', '') );						
			});

			$wizard.find('#boxcalendar #tDaysOfMonthSel').val( res.join('|') );

		});

		$wizard.find('[name="RangeRecurOpt"]').on('change', function() {
			if( this.value == 0 ) {
				$wizard.find('label.labelEnd').hide();
				$wizard.find('[name="RangeRecurrenceEnd"]').hide();
			} else {
				$wizard.find('label.labelEnd').show();
				$wizard.find('[name="RangeRecurrenceEnd"]').show();
			}
		});

		$wizard.find('[name="RangeRecurrenceStart"]').val( data['RangeRecurrenceStart'] || new Date().toString('yyyy-MM-dd') );
		var myCalendar1 = new dhtmlXCalendarObject("RangeRecurrenceStart");
		myCalendar1.attachEvent("onShow", function(date){
		   this.base.style.zIndex = "10000";
		});


		$wizard.find('[name="RangeRecurrenceEnd"]').val( data['RangeRecurrenceEnd'] || new Date().toString('yyyy-MM-dd') );
		var myCalendar2 = new dhtmlXCalendarObject("RangeRecurrenceEnd");
		myCalendar2.attachEvent("onShow", function(date){
		   this.base.style.zIndex = "10000";
		});

		$wizard.find('[name="CaptureStartDate"]').val( data['CaptureStartDate'] || new Date().toString('yyyy-MM-dd'));
		var myCalendar3 = new dhtmlXCalendarObject("CaptureStartDate");
		myCalendar3.attachEvent("onShow", function(date){
		   this.base.style.zIndex = "10000";
		});

		if( data['CheckPointID'] ) $wizard.find('select[name="CheckPointID"]').change();
		
		if( data['RecurPatternType'] !== null ) $wizard.find('select[name="RecurPatternType"]').change();

		if( data['RecurRangeOpt'] ) {
			values = data['RecurRangeOpt'].split('|');

			$wizard.find('[name="RangeRecurrenceStart"]').val( values[0] )
			$wizard.find('[name="RangeRecurOpt"]').val( values[1] ).change();
			$wizard.find('[name="RangeRecurrenceEnd"]').val( values[2] )
		}

		switch( data['RecurPatternType'] ) 
		{
			case 1:

				$wizard.find('[name="DailyOpt"]').val( data['RecurPatternOpt'] ).change();
				$wizard.find('[name="DNumDays"]').val( data['RecurPatternNum'] );

			case 2:
				values = data['RecurPatternOpt'].split('|');
				for(i = 0; i < values.length; i++) {					
					$wizard.find('[data-name="WDays"][value="'+ values[i] +'"]').prop('checked', true);
				}

				$wizard.find('[name="WNumWeeks"]').val( data['RecurPatternNum'] );
				break;

			case 4:
				values = data['RecurPatternOpt'].split('/');
				values[1] = values[1] && values[1].split('|') || [];

				$wizard.find('[name="MonthlyOpt"]').val( values[0] ).change();
				
				$wizard.find('[name="M_Day_NumMonth"]').val( data['RecurPatternNum'] );

				if( values[0] == '1' ) {
					$wizard.find('[name="M_The_FreqOrdinal"]').val(values[1][0]);
					$wizard.find('[name="M_The_DaysWeek"]').val(values[1][1]);
					$wizard.find('[name="M_The_NumMonth"]').val(values[1][2]);
				} else {
					for(i = 0; i < values[1].length; i++) {
						$wizard.find('table#tDaysOfMonth  td#d' + values[1][i]).click()/*addClass('selected')*/;
					}
				}
				
				break;
			case 5:
				values = data['RecurPatternOpt'].split('/');
				values[1] = values[1] && values[1].split('|') || [];

				$wizard.find('[name="YearlyOpt"]').val( values[0] ).change();

				if( values[0] == '1' ) {
					$wizard.find('[name="Y_OnThe_FreqOrdinal"]').val( values[1][0] );
					$wizard.find('[name="Y_OnThe_Days"]').val( values[1][1] );
					$wizard.find('[name="Y_OnThe_Months"]').val( values[1][2] );
				} else {
					$wizard.find('[name="Y_On_NumDay"]').val( values[1][0] );	
					$wizard.find('[name="Y_On_Months"]').val( values[1][1] );	
				}			
				
				$wizard.find('[name="YNumYears"]').val( data['RecurPatternNum'] );
				break;
			case 8:
				values = data['RecurPatternOpt'].split('/');
				values[1] = values[1] && values[1].split('|') || [];

				$wizard.find('[name="The"]').val( values[0] ).change();

				$wizard.find('[name="The_DaysWeek"]').val( values[1][1] );
				$wizard.find('[name="The_FreqOrdinal"]').val( values[1][0] );
				//JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
				var dblMDVersion = parseFloat(data['mdVersion']) || 0;
				var dblReqVersion = parseFloat(data.versions && data.versions.esvAgendaFirsDayWeek) || 0;
				if (dblReqVersion && dblMDVersion >= dblReqVersion) {
					values = parseInt(data['FirstDayOfWeek']) || 0;
					$wizard.find('[name="FirstDayOfWeek"]').val( values );
				}
				//JAPR
				break;
		}

		$wizard.find('select').selectize();
	}

	var validScheduler = function( $wizard, e, currentIndex, newIndex ) {
		var bValid = true;

		if( currentIndex > newIndex ) return true;

		$wizard.find('select[required], input[required]:visible').each(function() {

			var iValid = true;

			if( this.type == 'checkbox' ) {
				if( !$wizard.find('[data-name="'+ this.getAttribute('data-name') +'"]:checked').length ) iValid = false;
				else this.parentElement.parentElement.classList.remove('errorinput');

			} else if(!$(this).val()) {
				iValid = false;
			} else {
				if( this.tagName == 'SELECT' && this.selectize ) {
					this.selectize.$control.removeClass('errorinput');
				} else if( this.tagName == 'INPUT' ) {
					this.classList.remove('errorinput');
				}
			}

			if( this.type == 'number' && ( this.getAttribute('min') || this.getAttribute('max') ) ) {
				if( this.getAttribute('min') && this.value < this.getAttribute('min') )
					iValid = false;
				else if( this.getAttribute('max') && this.value > this.getAttribute('max') )
					iValid = false;
			}

			if( !iValid ) {
				bValid = false;
				if( this.tagName == 'SELECT' && this.selectize ) {
					this.selectize.$control.addClass('errorinput');
				} else if( this.type == 'checkbox' ) {
					this.parentElement.parentElement.classList.add('errorinput');
				} else if( this.tagName == 'INPUT' ) {
					this.classList.add('errorinput');
				}
			}

		});

		if( !bValid ) {

			e.stopPropagation();
			
			return false;
		}
	}

	return {'schedulerLogic': schedulerLogic, 'validScheduler': validScheduler};
});

