﻿<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);

	header("Content-Type:text/javascript; charset=UTF-8");

?>define([
	'underscore',
	'backbone',
	'text!templates/agendas/checkpoint.php',
	'views/modal/wizardModal',
	'libs/selectize.js/dist/js/standalone/selectize',
	'css!libs/selectize.js/dist/css/selectize.bootstrap3.css',
	'css!cssFolder/materialModal.css',
	'bootstrap'
], function(_, Backbone, Template, WizardModal){
	
	var self = this, wizard, aModal, aCheckpoint;

	var save = function( id, callBackSave ) {
		var data = {
			'id': id,
			'name': wizard.find('input[name="name"]').val(),
			'catalog': wizard.find('select[name="catalog"]').val(),
			'keys': [],
			'description': [],
			'latitude': wizard.find('input[name="latitude"]:checked').val(),
			'longitude': wizard.find('input[name="longitude"]:checked').val(),
			'action': 'saveCheckpoint'
		};

		wizard.find('input[name="key"]:checked').each(function() {
			data['keys'].push( this.value );
		});

		wizard.find('input[name="description"]:checked').each(function() {
			data['description'].push( this.value );
		});		

		/** Aqui envia la informacion del modelo creado / editado a la pagina para guardarlo */
		$.post('main.php?BITAM_SECTION=AgendasCollection', data).done(function( r ) {
			
			if( r === false ) {
				alert( '<?= translate('The catalog already exists, please select another') ?>' );
				return;
			}

			//Cerramos la ventana modal
			callBackSave( r );

			aModal.remove();
		});
	}

	var selectattrs = function() {

		$.get('main.php?BITAM_SECTION=AgendasCollection', { action: 'getAttr', 'catalogid': wizard.find('select[name="catalog"]').val() }).done(function( r ) {

			var $tBody = wizard.find('div.table_attrs table tbody').empty();

			for(var i = 0; i < r.length; i++) {
				$tBody.append( '<tr><td>'+ r[i].text +'</td><td><input type="checkbox" data-type="1" name="key" value="'+ r[i].value +'"></td><td><input type="checkbox" data-type="2" name="description" value="'+ r[i].value +'"></td><td><input type="radio" data-type="3" name="latitude" value="'+ r[i].value +'"></td><td><input type="radio" data-type="4" name="longitude" value="'+ r[i].value +'"></td></tr>' );
			}

			if( aCheckpoint ) {
				for( var i = 0; i < aCheckpoint.attributes.length; i++ ) {

					$tBody.find('[data-type="'+ aCheckpoint.attributes[i].type +'"][value="'+ aCheckpoint.attributes[i].id +'"]').prop('checked', true);

					if( aCheckpoint.attributes[i].type == 3 ) $tBody.find('input[name="longitude"][value="'+ aCheckpoint.attributes[i].id +'"]').prop('disabled', true);
					if( aCheckpoint.attributes[i].type == 4 ) $tBody.find('input[name="latitude"][value="'+ aCheckpoint.attributes[i].id +'"]').prop('disabled', true);
				}
			}

			$tBody.find('input[name="latitude"]').on('click', function() {
				$tBody.find('input[name="longitude"]').prop('disabled', false);
				$tBody.find('input[name="longitude"][value="'+ this.value +'"]').prop('disabled', true).prop('checked', false);
			});

			$tBody.find('input[name="longitude"]').on('click', function() {
				$tBody.find('input[name="latitude"]').prop('disabled', false);
				$tBody.find('input[name="latitude"][value="'+ this.value +'"]').prop('disabled', true).prop('checked', false);
			});

		}).fail(function() {

		});

	}
	

	/** Recibe el ID de la definicion de la conexion destino, si esta vacio crea uno nuevo */
	return function( id, callBackSave ) {

		wizard = $(_.template(Template)( {} ));

		aModal = new WizardModal({
			modalTitle: (id ? '<?= translate( 'Edit check point' ) ?>' : '<?= translate('Create check point') ?>'),
			className: 'material',
			labels: {
				cancel: "<?= translate('Cancel') ?>",
		        finish: "<?= translate('Finish') ?>",
		        next: "<?= translate('Next') ?>",
		        previous: "<?= translate('Previous') ?>"
			}
		});

		aModal.on('onStepChanging onFinished', function( e, currentIndex, newIndex ) {
			var bValid = true, fn;

			wizard.find('li[role="tab"]').removeClass('error');

			if( newIndex <  currentIndex) return;

			if(currentIndex == 1 ) {

				bValid = wizard.find('[data-type="1"]:checked').length > 0 && wizard.find('[data-type="2"]:checked').length > 0 && wizard.find('[data-type="3"]:checked').length > 0 && wizard.find('[data-type="4"]:checked').length > 0;
			}

			wizard.find('[id$="-p-'+ currentIndex +'"]').find('select[required], input[required]').each(function() {

				var iValid = true;	

				if(!$(this).val()) {
					iValid = false;
				}

				if( !iValid ) {
					bValid = false;
					if( this.tagName == 'SELECT' && this.selectize ) {
						this.selectize.$control.addClass('errorinput');
					} else if( this.tagName == 'INPUT' ) {
						this.classList.add('errorinput');
					}
				}

			});

			if( !bValid ) {

				wizard.find('li[role="tab"].current').addClass('error');

				e.stopPropagation();
				
				return false;
			}
			
			try{ fn =  eval( wizard.find('[id$="-p-'+ newIndex +'"]').attr('data-section') )} catch(err){}

			if( typeof fn == 'function' ) {
				fn( id );
			}
		});

		aModal.on('onFinished', function(e) {
			if( e.isPropagationStopped() ) return;
			save( id, callBackSave );
		});

		require(['selectize'], function(a) {

			$.when( id ? $.get('main.php?BITAM_SECTION=AgendasCollection', {'id': id, action: 'getCheckpoint'}) : [] ).done(function( r ) {

				aModal.setContent( wizard );

				if( r && id ) {
					aCheckpoint = r;
					wizard.find('input[name="name"]').val( r.name );
					wizard.find('select[name="catalog"]').val( r.catalog_id );
				}

				wizard.find('select').selectize();
			});

		});

		$('body').append(aModal.render().width(990).$el);
		
		aModal.$el.find('.modal-body').html('<?= translate("Loading") ?>...');
		
	};

});
