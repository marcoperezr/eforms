﻿define([
	'underscore',
	'backbone',
	'text!templates/modal/basicModal.html',
	'bootstrap',
	'libs/jquery-steps/build/jquery.steps',
	'css!libs/jquery-steps/build/jquery.steps.css',
	'css!../../../css/wizardModal.css'
], function(_, Backbone, Template, BasicModal){
	
	var WizardModal = Backbone.View.extend({

		initialize: function() {
			
			this.model = new Backbone.Model().set( { modalTitle: this.options.modalTitle, modalBody: '', bsModalSize:'' } );

			this.options.labels = _.extend( { labelClose: 'Close' }, this.options.labels );
			
		},

		events: {
		},

		setContent: function(content) {

			var self = this;

			content.steps({
				headerTag: "h1",
    			bodyTag: "section",
    			transitionEffect: "slideLeft",
    			onFinished: function (event, currentIndex) {
    				
    				self.trigger('onFinished', event, currentIndex);

    				return !event.isPropagationStopped();
			    },
			    onStepChanging: function (event, currentIndex, newIndex) {
			    	
			    	self.trigger('onStepChanging', event, currentIndex, newIndex);

			    	return !event.isPropagationStopped();
			    },
			    onFinishing: function (event, currentIndex) {
			    	
			    	self.trigger('onFinishing', event, currentIndex);

			    	return !event.isPropagationStopped();
			    },
			    onStepChanged: function(event, currentIndex, priorIndex) {
			    	
			    	self.trigger('onStepChanged', event, currentIndex, priorIndex);

			    	return !event.isPropagationStopped();
			    },
			    labels: self.options.labels
			});
	      
	      	this.$el.find('.modal-body').html(content);

	      	this.$el.find('.modal-footer').remove()//.addClass('wizard'); //.empty().html(content.find('.actions.clearfix').detach());

	      	if( this.options.height ) {
	      		this.$el.find('> .modal-dialog > .modal-content > .modal-body .content.clearfix').height( this.options.height );
	      	}
	      
	      	return this;
	    },

	    width: function(px) {

	    	this.$el.find('> .modal-dialog').width(px);

	    	return this;

	    },

	    height: function(px) {
	    	
	    	this.$el.find('> .modal-dialog > .modal-content > .modal-body .content.clearfix').height(px);

	    	this.options.height = px;

	    	return this;
	    },

		render: function() {
			
			var self = this;

			self.setElement($(_.template(Template)( _.extend( self.model.toJSON(), self.options.labels ) )));

			self.$el.modal({backdrop: 'static'}).on('hidden.bs.modal', function (e) {
				/** Cambiado a remove de jquery para que puedan ser llamados los eventos de destroy de los widgets */
				DatasourceFirst = false;
				$(this).remove();
			});

			if(self.options.className) {
				self.$el.addClass(self.options.className);
			}		

			return this;
		},

		showError: function( msg ) {
		   this.$el.find('.alert').show().html( msg );
		},
		
		hideError: function() {
			this.$el.find('.alert').hide();
		},
		
		remove: function() {
			DatasourceFirst = false;
			this.$el.modal('hide');
	    }
 	});
	
	return WizardModal;

});
