<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);

?>define([
	'js/views/base/basePageCollection.php?',
	'jquery.widgets',
	'eBavel/date',
	'css!cssFolder/eBavel.css',
	'css!cssFolder/listPageCollection.css'
], function( Base ) {

	var Page = Base.extend({

		initialize: function() {

		},

		_collection: {},

		options: {
			myFormIDFieldName: '',
			objectType: 0,
			enableMultiselect: false
		},

		_canRemove: function() {
			return true;
		},

		render: function() {

			var self = this, boxFilter = document.createElement('DIV');

			boxFilter.setAttribute('id', 'boxFilter');
			boxFilter.classList.add('boxFilter');

			self._content = new dhtmlXLayoutObject({
	            parent: document.body,
    			pattern: "2U"
	        });

	        self._content.cont.classList.add('layout-lPColl');

	        self._objToolbar = self._content.attachToolbar({
				icons_path: "images/admin/",
				items: self._getToolbarItems()
			});

			//self._objToolbar.setIconsPath("/images/admin/");

			self._objToolbar.addSpacer('removeCancel');

			self._objToolbar.hideItem('removeCancel');

			self._objToolbar.cont.firstChild.style.marginLeft = 210;

			self._boxFilter = self._content.cells("a").attachLayout("1C");
			self._boxFilter.cells("a").hideHeader();
			self._boxFilter.cells("a").setWidth(200);
			self._boxFilter.cells("a").attachObject( boxFilter );

			self._objGrid = self._content.cells("b").attachGrid();

			self._objGrid.entBox.classList.add('customTable');

        	self._content.cells("b").hideHeader();

        	self._content.cells("a").fixSize(true, true);
        	self._content.cells("b").fixSize(true, true);

        	self._objGrid.setHeader( self.options.header.header );
        	self.options.header.colAlign && self._objGrid.setColAlign(self.options.header.colAlign);
        	self.options.header.colTypes && self._objGrid.setColTypes(self.options.header.colTypes);
			self.options.header.colSorting && self._objGrid.setColSorting(self.options.header.colSorting);
			self.options.header.initWidths && self._objGrid.setInitWidths(self.options.header.initWidths);
			self.options.header.enableResizing && self._objGrid.enableResizing(self.options.header.enableResizing);

			self._objGrid.enableMultiselect( self.options.enableMultiselect );

			typeof self._onRowSelect == 'function' && self._objGrid.attachEvent("onRowSelect", function() {
				self._onRowSelect.apply( self, arguments );
			});

			self._objToolbar.attachEvent("onClick", function (id) {

				if( typeof self[ '_btn_' + id ] == 'function' ) {
					self[ '_btn_' + id ]();
				}

			});

			self._objGrid.init();

			self._load();

		},

		_load: function() {

			var self = this, params = _.extend( {type: 'post'}, ( typeof this._urlCollection == 'function' ? this._urlCollection.apply(self, arguments) : { url: this._urlCollection } ) );

			if (self && self._content && self._content.cells && self._content.cells("b")){
                self._content.cells("b").progressOn();
            }				
				
			this._urlCollection && $.ajax( params ).done(function( r ) {

				for(var i = 0; i < r.collection.length; i++) {

					self._addItem( r.collection[i] );
				}

				if (self && self._content && self._content.cells && self._content.cells("b")){
					self._content.cells("b").progressOff();
				}				
				
				if( r.filter ) {
					self._buildFilter( r.filter, r.orderFilter );
				}

			});

		},

		/*_btn_remove: function() {

		},*/

		_onRowSelect: function(id, pos) {

			var self = this, selected = id, data = this._collection[ id ];

			if( !this._bRemoving )
				this._objGrid.clearSelection();

			if( this._bRemoving ) {
				return false;
			}

			if(!data.canEdit) {
				return false;
			}

			return true;
		},

		_btn_removeCancel: function() {

			this._bRemoving = false;

			this._objToolbar.setItemText('remove', '<?= translate("Edit") ?>');

			this._objToolbar.setItemImage('remove', 'edit.png');

			this._objToolbar.hideItem('removeCancel');

			this._objGrid.clearSelection();
		},

		_addItem: function( data ) {

			var arrData =[], i, id = data[this.options.myFormIDFieldName];

			for( i = 0; i < this.options.header.headerFields.length; i++ ) {
				arrData.push( data[ this.options.header.headerFields[i] ] );
			}

			this._collection[ id ] = data;

			if( this._objGrid.doesRowExist( id ) ) {

				for( var i = 0; i < arrData.length; i++ ) {
					this._objGrid.cells(id,i).setValue( arrData[i] );
				}			
				
			} else {
				this._objGrid.addRow( id, arrData );
			}
		},

		_doShowPopUp: function(id, ev, html, tooltip) {		

		},

		_doHidePopUp: function() {
			
		},

		_itemClick: function(id, ev, html) {

		},

		displayBtnAdd: function( b ) {
			if( typeof b != 'boolean') b = true;
			if( b ) {
				this._objToolbar.showItem('add');	
			} else {
				this._objToolbar.hideItem('add');
			}
			
		},

		displayBtnRemove: function( b ) {
			if( typeof b != 'boolean') b = true;
			if( b ) {
				this._objToolbar.showItem('remove');	
			} else {
				this._objToolbar.hideItem('remove');
			}
			
		},

		_getToolbarItems: function() {

			if( _.isArray( this.options.toolBarItems )  ) {
				return [{id: "add", type: "button", text: "<?=translate('Add')?>", img: "add.png"}, {id: "remove", type: "button", text: "<?=translate('Edit')?>", img: "edit.png"}, {id: "removeCancel", type: "button", text: "<?=translate('Cancel')?>", img: "edit.png"}].concat( this.options.toolBarItems );
			}

			return [{id: "add", type: "button", text: "<?=translate('Add')?>", img: "add.png"}, {id: "remove", type: "button", text: "<?=translate('Edit')?>", img: "edit.png"}];

		},

		_buildFilter: function( descriptionTags, orderFilter ) {

			var self = this, checkedElemenTags = null, searchString = '', filterDate = '', $boxFilter = $(this._boxFilter.cont.getElementsByClassName('boxFilter')), $checkeds = [];

			var options = {
				'descriptionTags': descriptionTags,
				'checked': checkedElemenTags,
				'searchString': searchString,
				'filterDate': filterDate,
				'isAgenda': self.options.filterDate,
				'orderFilter': orderFilter,
				'applyFilterAttrib': function() {
					self._applyFilter.apply(self, arguments );
				}
			};

			if( !$.t ) {
				$.t = function(s) { 
					if( s == 'Date' ) return '<?= translate('Date') ?>';
					if( s == 'Custom date range' ) return '<?= translate('Custom date range') ?>';
					if( s == 'From' ) return '<?= translate('From') ?>';
					if( s == 'To' ) return '<?= translate('To') ?>';
					if( s == 'Clear range' ) return '<?= translate('Clear range') ?>';
					if( s == 'Search' ) return '<?= translate('Search') ?>';
					return s 
				};
			}

			if( $boxFilter.data('bitam-boxfilter') ) {
				$checkeds = $boxFilter.find('[type="checkbox"]:checked');
				$boxFilter.boxfilter('destroy').empty();
			}

			$boxFilter.boxfilter(options);

			if( $checkeds.length ) {

				$checkeds.each(function() {
					$boxFilter.find('[data-description-id="'+ this.getAttribute('data-description-id') +'"]').prop('checked', true).parent().addClass('checked');
				});
			}

		},

		_callbackRemove: function( arrValid ) {

			var self = this;

			self._objGrid.deleteSelectedRows();

			self._objGrid.clearSelection();

			self._load();
							
		},

		_applyFilter: function() {

		}

	});

	return Page;
		
});