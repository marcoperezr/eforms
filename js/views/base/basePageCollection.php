<?php
	
	$chdirCurrent = getcwd ();
	chdir('../../../');
	require 'checkCurrentSession.inc.php';
	chdir($chdirCurrent);

?>define([
	'underscore',
	'backbone'/*,
	'css!cssFolder/default.css',
	'css!cssFolder/default2.css'*/
], function(_, Backbone) {

	var Page = Backbone.View.extend({

		_objListDataView: null,
		_objCollectionLayOut: null,

		_objToolbar: null,

		initialize: function() {

		},

		options: {
			myFormIDFieldName: '',
			objectType: 0,
			picture: 'images/dataconnect.png'
		},

		_canRemove: function( data ) {

			if( data && typeof data.canRemove == 'boolean' ) return data.canRemove;

			return true;
		},

		render: function() {

			var self = this, objCollectionLayOut, dhxList = 'a', objCustomizedButtons = [], objCustomBehaviour = {}, objCustomizedFns = {}, intButtons = 0, blnCanAdd = 1;

			intButtons += (blnCanAdd)?1:0;
			intButtons += this._canRemove()?1:0;

			self._objCollectionLayOut = objCollectionLayOut = new dhtmlXLayoutObject({
				parent: this.el,
				pattern: "1C"
			});

			objCollectionLayOut.cells(dhxList).hideHeader();
			
			objCollectionLayOut.setOffsets({top:0, left:0, right:0, bottom:0});
			
			this._objListDataView = objListDataView = objCollectionLayOut.cells(dhxList).attachDataView({
				container:"listCell",
				drag:false, 
				select: 'multiselect',
				type:{
					//template:"<div style='text-align: center; padding:10px'>#picture#<div style='font-family:Roboto Regular;font-size:13px;font-weight:normal; text-align: center;' data-tooltip='#tooltip#'>#description#</div></div>",
					template:"<img style='height:95; width:85;' src='#picture#'><div style='font-family:Roboto Regular;font-size:13px;font-weight:normal; text-align: center;' data-tooltip='#tooltip#'>#description#</div><div class='checkContainer'><div><i class='icon-check-sign'></i></div></div>",
					css:"custom",
					width:160,
					border:0,
					padding:10,
					height:140
				}
			});

			var intCustomizedButtons = 0;
			
			intButtons += intCustomizedButtons;

			if (intButtons) {
				objCollectionLayOut.cells(dhxList).attachToolbar({
					parent:"objectsCollection"
				});
				
				var objToolbar = this._objToolbar = objCollectionLayOut.cells(dhxList).getAttachedToolbar();
				
				objToolbar.setIconsPath("images/admin/");
				
				var intPos = 0;
				
				if (blnCanAdd) {
					var strId = "add";
					var strLabel = "<?php echo translate('Add') ?>";
					var strImage = "add.png";
					if (objCustomBehaviour[strId]) {
						var objButton = objCustomBehaviour[strId];
						if (objButton.label) {
							strLabel = objButton.label;
						}
						if (objButton.image) {
							strImage = objButton.image;
						}
					}
					
					objToolbar.addButton(strId, intPos++, strLabel, strImage);
				}
				
				if ( this._canRemove() ) {
					var strId = "remove";
					var strLabel = "<?php echo translate('Edit') ?>";
					var strImage = "edit.png";
					if (objCustomBehaviour[strId]) {
						var objButton = objCustomBehaviour[strId];
						if (objButton.label) {
							strLabel = objButton.label;
						}
						if (objButton.image) {
							strImage = objButton.image;
						}
					}
					objToolbar.addButton(strId, intPos++, strLabel, strImage);

					objToolbar.addButton('removeCancel', intPos++, "<?=translate("Cancel")?>", strImage);

					objToolbar.hideItem('removeCancel');
				}
				
				//Agrega todos los botones personalizados, excepto si son considerados default, ya que en ese caso se personalizó sólo su diseño pero no debe
				//duplicar el botón (debe ser un array)
				if (intCustomizedButtons) {
					var intCont = objCustomizedButtons.length;
					for (var intIndex = 0; intIndex < intCont; intIndex++) {
						var objButton = objCustomizedButtons[intIndex];
						if (objButton && !objButton.isDefault) {
							objToolbar.addButton(objButton.id, intPos++, objButton.label, objButton.image);
						}
					}
				}
				
				//Prepara los eventos a utilizar, en el caso de los botones personalizados, el evento puede ser la invocación a código o a una URL, para los botones
				//fijos generalmente es una URL. Los IDs de los botones fijos también son fijos, con esto se puede sobreescribir el comportamiento de dicho botón
				//tal como si fuera personalizado
				objToolbar.attachEvent("onClick", function(id) {

					if( typeof self[ '_btn_' + id ] == 'function' ) {
						self[ '_btn_' + id ]();
					}
					
				});

				objListDataView.attachEvent("onMouseMove", function (id, ev, html) {
					self._doShowPopUp(id, ev, html, $(html).find('div[data-tooltip]').attr('data-tooltip'));
				});
				

				objListDataView.attachEvent("onMouseOut", function (ev) {
					self._doHidePopUp();
				});
			}

			this._urlCollection && $.get( ( typeof this._urlCollection == 'function' ? this._urlCollection() : this._urlCollection ) ).done(function( r ) {

				for(var i = 0; i < r.collection.length; i++) {

					self._addItem( r.collection[i] );
				}

			});

			objListDataView.attachEvent("onItemClick", function (id, ev, html) {

				if( self._bRemoving ) {
					
					objListDataView.select(id, true);

					return false;					
				}

				self._itemClick(id, ev, html);

				return false;

			});

		},

		_btn_remove: function() {

			if( !this._bRemoving ) {
				
				this._bRemoving = true;

				this._objToolbar.setItemText('remove', '<?= translate("Remove") ?>');

				this._objToolbar.setItemImage('remove', 'remove.png');

				this._objToolbar.showItem('removeCancel');

				this._objListDataView && this._objListDataView.$view.classList.add('dhx_dataview_selecting');

				return;
			}

			this._bRemoving = false;

			var self = this, selected = 0, toignore = 0, arrSelected = (this._objListDataView && this._objListDataView.getSelected() || this._objGrid && this._objGrid.getSelected()), arrValid = new Array(), arrInvalid = new Array(), blnMulti = $.isArray(arrSelected), strId, i, objItem, answer, params;

			if( !arrSelected ) arrSelected = [];

			if( _.isString( arrSelected ) ) arrSelected = arrSelected.split(',');
				
			/*if (!blnMulti) {
				strId = arrSelected;
				arrSelected = new Array();
				if (strId) {
					arrSelected.push(strId);
				}
			}*/
				
			for (i = 0; i < arrSelected.length; i++) {
				selected++;
				
				strId = arrSelected[i];
				
				objItem =  this._objListDataView && this._objListDataView.get(strId) || this._collection[ strId ];

				if (!objItem || !objItem.canRemove) {
					toignore++;
					arrInvalid.push(strId);
				}
				else {
					arrValid.push(strId);
				}
			}
			
			if (selected == 0) {
				
				alert('<?= translate("Please select which item(s) to remove") ?>');
				self._btn_removeCancel();
				return;
			}

			if (selected == toignore) {
				alert('<?= translate("No selected item(s) can be removed") ?>');
				self._btn_removeCancel();
				return;
			}

			if (toignore != 0) {
				alert('<?= translate("Some item(s) can not be removed, they will be deselected") ?>');

				for (i = 0; i < arrInvalid.length; i++) {
					strId = arrInvalid[i];
					if (strId) {
						self._objListDataView.unselect(strId);
					}
				}
			}

			answer = confirm('<?= translate("Do you want to remove all selected item(s)?") ?>');

			if (answer) {
				//Primero desmarca todos los checkboxes de elementos para no enviarlos al borrado

				params = { 'Process': 'Delete', 'RequestType': 1, 'responseformat': 'json', 'ObjectType': self.options.objectType };

				params[ self.options.myFormIDFieldName ] = arrValid;
				
				$.post('processRequest.php', params).then( function( r ) {
						
						if( !r.error ) {

							self._callbackRemove( arrValid );

						} else {

							alert( r.error.desc );

						}

						self._btn_removeCancel();
						
					}, function(){debugger;} );

				} else {
					self._btn_removeCancel();
				}


		},

		_callbackRemove: function( arrValid ) {

			var self = this;

			self._objListDataView.remove( arrValid );
							
			self._objListDataView.refresh();
			
			//JAPR 2016-12-21: Validado que al terminar de eliminar ciertos objetos se puedan realizar acciones como redireccionar a otra página (#37X3EU)
			if (this._afterRemove) {
				this._afterRemove(arrValid);
			}
			//JAPR
		},

		_btn_removeCancel: function() {

			this._bRemoving = false;

			this._objListDataView.$view.classList.remove('dhx_dataview_selecting');

			this._objToolbar.setItemText('remove', '<?= translate("Edit") ?>');

			this._objToolbar.setItemImage('remove', 'edit.png');

			this._objToolbar.hideItem('removeCancel');

			this._objListDataView.unselectAll();
		},

		_addItem: function( data ) {

			/** Si el elemento ya existe lo actualizamos */
			if( this._objListDataView.get( data.id ) ) {

				this._objListDataView.update( data.id, { id: data.id , description: data.name, picture: this.options.picture,
					canRemove: this._canRemove( data ), tooltip: ( data.description ? data.description : '' )
				} );
				
				//JAPR 2016-12-21: Validado que al terminar de eliminar ciertos objetos se puedan realizar acciones como redireccionar a otra página (#37X3EU)
				if (this._afterEdit) {
					this._afterEdit(data);
				}
				//JAPR
				return;
			}
			
			this._objListDataView.add({id: data.id , description: data.name, picture: this.options.picture, 
				canRemove: this._canRemove( data ), tooltip: ( data.description ? data.description : '' )
			});
			
			//JAPR 2016-12-21: Validado que al terminar de eliminar ciertos objetos se puedan realizar acciones como redireccionar a otra página (#37X3EU)
			if (this._afterAdd) {
				this._afterAdd(data);
			}
			//JAPR
		},

		_doShowPopUp: function(id, ev, html, tooltip) {
			
			
			if (!this._objListDataView || !tooltip) {
				return;
			}
			
			var objOffset = $(ev.target).offset();
			var objElement = $(ev.target)[0];
			
			//Primero crea el PopUp en el objeto seleccionado
			if (!this._objMousePopUp) {
				this._objMousePopUp = new dhtmlXPopup();
			}
			this._objMousePopUp.clear();
			this._objMousePopUp.attachHTML(tooltip);
			//objMousePopUp.show(objOffset.left, objOffset.top, 300, 50);
			this._objMousePopUp.show(window.dhx4.absLeft(objElement), window.dhx4.absTop(objElement), objElement.offsetWidth, objElement.offsetHeight);
		},

		_doHidePopUp: function() {
			if (!this._objMousePopUp) {
				return;
			}
			
			this._objMousePopUp.hide();
		},

		_itemClick: function(id, ev, html) {

		},

		displayBtnAdd: function( b ) {
			if( typeof b != 'boolean') b = true;
			if( b ) {
				this._objToolbar.showItem('add');	
			} else {
				this._objToolbar.hideItem('add');
			}
			
		},

		displayBtnRemove: function( b ) {
			if( typeof b != 'boolean') b = true;
			if( b ) {
				this._objToolbar.showItem('remove');	
			} else {
				this._objToolbar.hideItem('remove');
			}
			
		},

		setTitle: function( msg ) {
			parent.$('div.divInfo span#displayLoc').html( msg );
		}

	});

	return Page;
		
});