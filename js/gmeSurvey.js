var map;
var iterator		 = 0;
var markersArray	 = [];
//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
var markersBounds 	 = undefined;
//JAPR
var aMarker  		 = [];

var aMarkersInf      = new Array;
var aMarkersDataDesc = new Array;
var infowindow       = null;
var aIcons			 = new Array;
//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
var miMapType		 = 1;
//JAPR

//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
//Crear los markers con los colores que tenemos
var pinsColors 		= new Array;
//Removida la asignación del icono para Google Maps dentro de la función de inicialización
//JAPR
//Marker hotpoint
var shape = {
      coord: [1,1, 16,16],
      type: 'rect'
  };

function Marker(idx, desc, lon, lat, datas, action, img)
{
  this.index    = idx;
  this.desc     = desc;
  this.longitud = lon;
  this.latitud  = lat;
  this.datas    = datas.split(',');
  this.action   = action;
  this.img      = img;
}


function readData()
{	//                0=Desc           1=Longitud               2=Latitud           3=Vals
	//$sMarkerInf.='Marker'.$i.$sep.$sMarkerLong[$i-1].$sep.$sMarkerLat[$i-1].$sep.$sDataVals.$sepMarker;
	var aMarkers=sMarkerInf.split(sepMarker);
	for(i=0; i<aMarkers.length; i++)
	{
		if(aMarkers[i]!='')
		{
			aMarkerInf     = aMarkers[i].split(sep);
			aMarkersInf[i] = new Marker(i, aMarkerInf[0], aMarkerInf[1], aMarkerInf[2], aMarkerInf[3], '', '');
		}
	}
	aMarkerInf=null;
	//Descriptores de los Datos
	aMarkersDataDesc=sDataDesc.split(sep);
}

function setMarkers()
{
	for(i=0; i<aMarkersInf.length; i++)
	{
		var latlng = new google.maps.LatLng(aMarkersInf[i].longitud, aMarkersInf[i].latitud);
		addMarker(latlng, i);
	}
}

//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
function setMarkersMapkit(objOptions)
{
	if ( !objOptions ) {
		objOptions = {};
	}
	for(i=0; i<aMarkersInf.length; i++)
	{
		//Por alguna razón para Google Maps las propiedades se asignaban en orden inverso en el constructor (Long/Lat) y quedaban bien en el objeto Marker, pero para Mapkit que reutiliza
		//el mismo método para generar los markadores internos pero la creación del punto del mapa lo pide en el orden adecuado (Lat/Long) hay que invertir las propiedades
		var latlng = new mapkit.Coordinate(parseFloat(aMarkersInf[i].longitud) || 0, parseFloat(aMarkersInf[i].latitud) || 0);
		addMarkerMapkit(latlng, i, objOptions);
	}
}
//JAPR

function addMarker(location, index) {
  var marker = new google.maps.Marker({
      position: location,
	  draggable: false,
	  icon : aIcons[pinsColors[parseInt(Math.random()*pinsColors.length)]],
	  shape: shape,
      title: aMarkersInf[index].desc,
	  animation: google.maps.Animation.DROP,
      map: map
  });

  google.maps.event.addListener(marker, 'click', function()
  	{
  		showMarkerInfo(index);
  	}
  );

  markersArray.push(marker);
  markersBounds.extend(location);
}

//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
function addMarkerMapkit(location, index, objOptions) {
	//Info de la Ventana
	var sInf='<div style="width:260px;height:150px;background-color:#ffffff;border: solid 1px grey;padding:10px;"><b><center>'+aMarkersInf[index].desc+'</center></b>';
	for(j=0; j<aMarkersInf[index].datas.length; j++)
	{
		sInf+='<br><b>'+aMarkersDataDesc[j]+':&nbsp;</b>'+aMarkersInf[index].datas[j];
	}
	sInf+='</div>';
	
	var calloutDelegate = {
		calloutElementForAnnotation: function() {
			return $(sInf)[0];
		}
	}
	
	var opts = {
		/*title: aMarkersInf[index].desc,
		subtitle: aMarkersInf[index].desc,*/
		color: pinsColors[parseInt(Math.random()*pinsColors.length)],
		callout: calloutDelegate
	};
	if ( objOptions ) {
		$.extend(opts, objOptions);
	}
	
	var marker = new mapkit.MarkerAnnotation(location, opts);
	
	markersArray.push(marker);
}
//JAPR

//Crear Ventana InfoWindow
function showMarkerInfo(i)
{
	//Info de la Ventana
	var sInf='<b><center>'+aMarkersInf[i].desc+'</center></b>';
	for(j=0; j<aMarkersInf[i].datas.length; j++)
	{
		sInf+='<br><b>'+aMarkersDataDesc[j]+':&nbsp;</b>'+aMarkersInf[i].datas[j];
	}
	sInf+='';

	var zoom='';
	zoom += "<h6>";
  	zoom += "<div>";
    zoom += "   <a href=\"javascript:zoomOnPoint(-1,"+i+")\"><b>Zoom Out</b></a>";
    zoom += " - <a href=\"javascript:zoomOnPoint(+1,"+i+")\"><b>Zoom In</b></a>";
    zoom += " | <a href=\"javascript:zoomOnPoint(-3,"+i+")\"><b>Quick Zoom Out</b></a>";
    zoom += " - <a href=\"javascript:zoomOnPoint(+3,"+i+")\"><b>Quick Zoom In</b></a>";
    zoom += "</div>";
    zoom += "<div>";
    zoom += "   <center><a href='javascript:closeInfoWindo(); map.fitBounds(markersBounds);'><b>Center Map</b></a></center>";
    zoom += "</div>";
    zoom += "</h6>";

    //Zoom
    sInf += zoom;

	closeInfoWindo();
	infowindow = new google.maps.InfoWindow(
	{   content: sInf,
	    size: new google.maps.Size(50, 50),
	    position: markersArray[i].getPosition()
	});
	infowindow.open(map);
}

function closeInfoWindo()
{
	if(infowindow) infowindow.close();
}

function zoomOnPoint(a, MakerIdx) {
  var newZoom = map.getZoom() + a;
  if (newZoom<= 2) newZoom =  2;
  if (newZoom>=17) newZoom = 17;

  map.setZoom(newZoom);
  map.panTo(map.getCenter(), newZoom);
  map.setCenter(markersArray[MakerIdx].getPosition());
}

function initialize() {
	readData();
	if ( miMapType != 0 ) {
		initializeMapkit();
		return;
	}
	
	initializeGoogleMap();
}

//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
function initializeGoogleMap() {
	pinsColors = 'rojo,azul,violeta,gris,verde,amarillo'.split(',');
	markersBounds 	 = new google.maps.LatLngBounds();
	for(i=0; i<pinsColors.length; i++)
	{
		aIcons[pinsColors[i]] = new google.maps.MarkerImage('images/pin_'+pinsColors[i]+'.png', new google.maps.Size(38, 36), new google.maps.Point(0, 0),  new google.maps.Point(8, 36));
	}
	
	var Tampico = new google.maps.LatLng(22.249183, -97.868183);
	//center: latlng,
	//Parametrizar el Mapa
	var myOptions = {
		zoom: 15,
		disableDefaultUI: false,
		panControl: true,
		zoomControl: true,
		zoomControlOptions:
		{
			style: google.maps.ZoomControlStyle.SMALL
		},
		mapTypeControl: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
		},
		scaleControl: true,
		streetViewControl: true,
		overviewMapControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	//Crear el Mapa y Cargarlo con las opciones descritas
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	//Si no existen Markers, unicar y centrar en Tampico
	if(aMarkersInf.length==0)
	{
		map.setCenter(Tampico);
		map.setZoom(4);
	}
	else
	{
		setMarkers();
		if(aMarkersInf.length>10)
		{
			map.fitBounds(markersBounds);
		}
		else
		{
			if(aMarkersInf.length>0)
			{
				map.setCenter(markersArray[0].getPosition());
				map.setZoom(5);
			}
		}
	}
}

function initializeMapkit() {
	pinsColors = 'red,blue,fucsia,grey,green,yellow'.split(',');
	//markersBounds 	 = new google.maps.LatLngBounds();
	var Tampico = new mapkit.Coordinate(22.249183, -97.868183), centerPoint = {}, 
		userSpan = new mapkit.CoordinateSpan(.05, .05), userRegion = {};
	//Parametrizar el Mapa
	var myOptions = {
		showMapTypeControl: false,
		showsCompass: false,
		showsZoomControl: true
	};
	
	//Si no existen Markers, unicar y centrar en Tampico
	if(aMarkersInf.length==0)
	{
		centerPoint = Tampico;
	}
	else
	{
		setMarkersMapkit();
		if(aMarkersInf.length>0)
		{
			//Por alguna razón para Google Maps las propiedades se asignaban en orden inverso en el constructor (Long/Lat) y quedaban bien en el objeto Marker, pero para Mapkit que reutiliza
			//el mismo método para generar los markadores internos pero la creación del punto del mapa lo pide en el orden adecuado (Lat/Long) hay que invertir las propiedades
			centerPoint = new mapkit.Coordinate(parseFloat(aMarkersInf[0].longitud) || 0, parseFloat(aMarkersInf[0].latitud) || 0);
		}
	}
	
	userRegion = new mapkit.CoordinateRegion(centerPoint, userSpan);
	
	myOptions = $.extend(myOptions, {center: centerPoint, region: userRegion});
	
	//Crear el Mapa y Cargarlo con las opciones descritas
	map = new mapkit.Map(document.getElementById("map_canvas"), myOptions);
	map.setRegionAnimated(userRegion);
	/*for ( var intCont = 0; intCont < markersArray.length; intCont++) {
		var marker = markersArray[intCont];
		map.addAnnotation(marker);
	}*/
	
	map.showItems(markersArray, { 
		animate: true,
		padding: new mapkit.Padding(60, 25, 60, 25)
	});
}

/* Inicializa el Apple Mapkit, en el caso de Google Maps simplemente continua con la función principal */
function initializeMaps(iMapType) {
	miMapType = iMapType;
	
	if ( miMapType != 0 ) {
		if ( !mapkit ) {
			alert("Unable to load mapkiy library");
			console.log("Unable to load mapkiy library");
			return;
		}
		
		mapkit.init({ authorizationCallback: function(done) {	
			fetch("https://kpionline.bitam.com/mapsService/mapKit/service.php", {method: "POST", headers: {'Product-Name' : 'KPIForms'}})
				.then(response => {
					return response.json();
				}).then(data => {
					if (data && data.token) {
						done(data.token);
					} else {
						console.log("No token generated at the site");
					}
				}).catch(error => {
					console.log("There was an error with the request")
				});
		}});
		mapkit.addEventListener("configuration-change", function(event) {
			switch(event.status) {
				case "Initialized":
					console.log("MapKit initialized correctly, ready to draw maps.");
					initialize();
				break;
				case "Refreshed":
					console.log("Mapkit configuration or token changed correctly.");
				break;
			}
		});
		mapkit.addEventListener("error", function(event) {
			switch(event.status) {
				case "Unauthorized":
					console.log("MapKit token was invalid.");
					alert("Mapkit token was invalid.");
				break;
				default:
					console.log("Woops, Mapkit experienced an unexpected error" + event.status);
					alert("Woops, Mapkit experienced an unexpected error." + event.status);
					console.log("Unknown Mapkit error: " + event.status);
				break;
			}
		})
		return;
	}
	
	//Para Google Maps simplemente continua con la carga
	initialize();
}
//JAPR