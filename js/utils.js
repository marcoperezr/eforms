//Variable booleana que indica si el navegador es IE
var bIsIE = navigator.userAgent.indexOf("MSIE") != -1;

//****************************************************************************************************************************************
//********************************************************** Funciones de fecha **********************************************************
//****************************************************************************************************************************************
/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/datevalidation.asp)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh = '/';
var minYear = 1900;
var maxYear = 2200;

//Variables globales para ancho y alto de dispositivos
//Se utilizan en el nuevo diseñador HTML
var defaultWidth = '1024px';
var defaultHeight = '768px';
//iPod: 320 X 480
var iPodWidth = '320px';
var iPodHeight = '480px';
//iPad: 1024 X 768 (iPad pantalla de retina: 2048 x 1536)
var iPadWidth = '768px';
var iPadHeight = '1024px';
//iPad mini 1024 x 768 (iPad mini pantalla de retina: 2048 x 1536)
var iPadMiniWidth = '768px';
var iPadMiniHeight = '1024px';
//IPhone: 1136 x 640
var iPhoneWidth = '640px';
var iPhoneHeight = '1136px';
//Cel android:
var celAndroidWidth = '360px';//'640px';
var celAndroidHeight = '640px';//'1136px';
//Tableta Android: 800 x 1280
var tabletAndroidWidth = '600px';//'800px';
var tabletAndroidHeight = '1024px';//'1280px';

function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31
        if (i == 4 || i == 6 || i == 9 || i == 11) {
            this[i] = 30
        }
        if (i == 2) {
            this[i] = 29
        }
    }
    return this
}

function isInteger(s) {
    var i;
    for (i = 0; i < s.length; i++) {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < '0') || (c > '9'))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag) {
    var i;
    var returnString = '';
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary(year) {
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
}

function isDate(dtStr) {
    var daysInMonth = DaysArray(12);
    var pos1 = dtStr.indexOf(dtCh);
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1);
    var strMonth = dtStr.substring(0, pos1);
    var strDay = dtStr.substring(pos1 + 1, pos2);
    var strYear = dtStr.substring(pos2 + 1);
    strYr = strYear;
    if (strDay.charAt(0) == '0' && strDay.length > 1) strDay = strDay.substring(1);
    if (strMonth.charAt(0) == '0' && strMonth.length > 1) strMonth = strMonth.substring(1);
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == '0' && strYr.length > 1) strYr = strYr.substring(1);
    }
    month = parseInt(strMonth);
    day = parseInt(strDay);
    year = parseInt(strYr);
    if (pos1 == -1 || pos2 == -1) {
        return false;
    }
    if (strMonth.length < 1 || month < 1 || month > 12) {
        return false;
    }
    if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        return false;
    }
    if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
        return false;
    }
    if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
        return false;
    }
    return true;
}

//MABH 2012-05-09
/* Dado un objeto Date, formatea la fecha para que los valores de mes y de día tengan 2 dígitos. Adicionalmente con el parámetro bNumeric se puede pedir regresar la fecha como un número
o con el separador universal "-" 
*/
function getDate(date, bNumeric) {
	var dateStr = '';
	var strDel = (bNumeric)?'':'-';
	dateStr += (date.getFullYear()+strDel);
	dateStr += (padLeft(date.getMonth()+1,2)+strDel);
	dateStr += (padLeft(date.getDate(),2));
	return dateStr;
}

function getFullDate(date, bNumeric) {
	if (date === undefined) {
		date = new Date();
	}
	var dateStr = getDate(date, bNumeric) + ((bNumeric)?'':' ') + getTime(date, bNumeric);
	
	return dateStr;
}

function getTime(date, bNumeric) {
	var timeStr = '';
	var strDel = (bNumeric)?'':':';
	timeStr += (padLeft(date.getHours(),2)+strDel);
	timeStr += (padLeft(date.getMinutes(),2)+strDel);
	timeStr += (padLeft(date.getSeconds(),2));
	return timeStr;
}

function getTimeZone() {
    var current_date = new Date( );
    var gmt = current_date.getTimezoneOffset()/60;
    return gmt;
}

//Convierte la fecha especificada en formato YYYY-MM-DD hh:mm:ss a una fecha de JavaScript (instancia de Date)
function myToDate(strDate) {
    var datetime = strDate.split(' ');
    var date = datetime[0];
    var time = datetime[1];
    var date_ymd = date.split('-');
    var time_hms = time.split(':');
    var year = date_ymd[0];
    var month = parseInt(date_ymd[1]);
    //JAPR 2013-11-26: Corregido un bug, esta función espera meses en el rango 0 a 11 para Ene a Dic respectivamente, pero las fechas se reciben
    //en rango de 1 a 12 así que debe restar un mes
    month = (month >= 1 && month <= 12)?month -1:0;
    var day = date_ymd[2];
    var hour = time_hms[0];
    var min = time_hms[1];
    var seg = time_hms[2];
    var conv_date = new Date(year, month, day, hour, min, seg);
    return conv_date;
}

/* Regresa la fecha especificada (en formato yyyymmdd) a una fecha en formato estándar (yyyy-mm-dd) 
*/
function formatNumericDate(aNumericDate) {
	aNumericDate = aNumericDate + '';
	if (!aNumericDate) {
		return aNumericDate;
	}
	
	return aNumericDate.substr(0, 4) + '-' + aNumericDate.substr(4, 2) + '-' + aNumericDate.substr(6, 2);
}

/* Regresa la hora especificada (en formato hhmmss) a una hora en formato estándar (hh:mm:ss) 
*/
function formatNumericTime(aNumericTime) {
	aNumericTime = aNumericTime + '';
	if (!aNumericTime) {
		return aNumericTime;
	}
	if (aNumericTime.length < 6) {
		aNumericTime = '0' + aNumericTime;
	}
	
	return aNumericTime.substr(0, 2) + ':' + aNumericTime.substr(2, 2) + ':' + aNumericTime.substr(4, 2);
}

/* Regresa la fecha especificada (en formato yyyy-mm-dd) a una fecha en formato numérico (yyyymmdd) 
*/
function unformatNumericDate(aDate) {
	if (!aDate) {
		return '';
	}
	return aDate.replace(/-/gi, '')
}

/* Regresa la hora especificada (en formato hh:mm:ss) a una hora en formato numérico (hhmmss) 
*/
function unformatNumericTime(aTime) {
	if (!aTime) {
		return '';
	}
	return aTime.replace(/:/gi, '');
}

//****************************************************************************************************************************************
//********************************************************** Funciones de texto **********************************************************
//****************************************************************************************************************************************
function TrimLeft( str ) 
{
	var resultStr = "";
	var i = len = 0;
	
	if ( str + "" == "undefined" || str == null)
		return null;

	str += "";
	
	if (str.length == 0)
	{
		resultStr = "";
	}
	else 
	{
		len = str.length;

		while ( ((i <= len) && (str.charAt(i) == " ")) || ((i <= len) && (str.charAt(i) == "\n")) || ((i <= len) && (str.charAt(i) == "\r")) || ((i <= len) && (str.charAt(i) == "\t")))
		{
			i++;
		}
		resultStr = str.substring(i, len);
	}
	return resultStr;
}

//MABH 2012-05-21
function padLeft(n, totalDigits) {
	//Debugger.message('function padLeft('+n+', '+totalDigits+')'); 
	n = n.toString(); 
	var pd = ''; 
	if (totalDigits > n.length) { 
		for (i=0; i < (totalDigits-n.length); i++) { 
			pd += '0'; 
		} 
	} 
	return pd + n.toString(); 
}

function TrimRight( str ) 
{
	var resultStr = "";
	var i = 0;
	
	if ( str + "" == "undefined" || str == null )
	{	return null;
	}
	
	str += "";

	if (str.length == 0)
	{
		resultStr = "";
	}
	else 
	{
		i = str.length - 1;
		while (	((i >= 0) && (str.charAt(i) == " ")) || ((i >= 0) && (str.charAt(i) == "\n")) || ((i >= 0) && (str.charAt(i) == "\r")) || ((i >= 0) && (str.charAt(i) == "\t")))
		{
				i--;
		}

		resultStr = str.substring(0, i + 1);
	}

	return resultStr;
}		

function Trim( str ) 
{
	var resultStr = "";

	resultStr = TrimLeft(str);
	resultStr = TrimRight(resultStr);

	return resultStr;
}

function htmlspecialchars(string, quote_style) 
{
    string = string.toString();
    
    //Codificacion de los siguientes caracteres
    string = string.replace(/&/g, '&amp;');
    string = string.replace(/</g, '&lt;');
    string = string.replace(/>/g, '&gt;');
    
    //Codificacion 
    if (quote_style == 'ENT_QUOTES') 
    {
        string = string.replace(/"/g, '&quot;');
        string = string.replace(/'/g, '&#039;');
    }
    else if (quote_style != 'ENT_NOQUOTES')
    {
        // Para los demas casos (ENT_COMPAT, por defecto, pero no ENT_NOQUOTES)
        string = string.replace(/"/g, '&quot;');
    }
    
    return string;
}

//****************************************************************************************************************************************
//********************************************************** Funciones de arrays **********************************************************
//****************************************************************************************************************************************
//MABH 2012-05-09
function array_flip (trans) {
	//Debugger.message('function array_flip (trans)');
    var key, tmp_ar = {};
    for (key in trans) {
        tmp_ar[trans[key]] = key;
    }
    return tmp_ar;
}

function inArray(anArray, anElement)
{
	var exist=false;
	var i=0;
	var arrayLength=anArray.length;
	for(i=0;i<arrayLength;i++)
	{
		if(anArray[i]==anElement)
		{
			exist=true;
			break;
		}
	}
	return exist;
}

function searchInArray(anArray, anElement)
{	var idx=-1;
	var arrayLength=anArray.length;
	var i;
	for(i=0;i<arrayLength;i++)
	{
		if(anArray[i]==anElement)
		{
			idx=i;
			break;
		}
	}
	
	return idx;
}

function getArrayByString(aString, sepRecord, sepField, firstFieldAsKey, isAssociative, arrayKeysAssoc)
{
	var arrayAux1 = new Array();
	var countAux1;
	var arrayAux2 = new Array();
	var countAux2;
	var i;
	var j;
	var arrayResult = new Array();
	var key;
	var key2;
	var arrayResult;
	
	arrayAux1 = aString.split(sepRecord);
	countAux1 = arrayAux1.length;
	for(i=0; i<countAux1;i++)
	{
		arrayAux2 = new Array();
		arrayAux2 = arrayAux1[i].split(sepField);
		countAux2 = arrayAux2.length;
		
		if(countAux2 > 0)
		{
			if(firstFieldAsKey)
			{
				key = arrayAux2[0];
				
				if(arrayResult[key]==undefined)
				{
					arrayResult[key] = new Array();
				}
				
				key2 = arrayAux2[1];
				
				if(arrayResult[key][key2]==undefined)
				{
					arrayResult[key][key2] = new Array();
				}
			}
			else
			{
				arrayResult[i] = new Array();
				key2 = arrayAux2[1];
				arrayResult[1][key2] = new Array();
			}
			
			for(j=2; j<countAux2; j++)
			{
				if(firstFieldAsKey)
				{
					if(isAssociative)
					{
						arrayResult[key][key2][arrayKeysAssoc[j]] = arrayAux2[j];
					}
					else
					{
						arrayResult[key][key2][j] = arrayAux2[j];
					}
				}
				else
				{
					if(isAssociative)
					{
						arrayResult[i][arrayKeysAssoc[j]] = arrayAux2[j];
					}
					else
					{
						arrayResult[i][j] = arrayAux2[j];
					}
				}
			}
		}
	}
	
	return arrayResult;
	
}

function array_search(nValueSearch, aValues) 
{
	var nTop = aValues.length;
	var nPos = -1;
	for (var i=0; i<nTop; i++) {
		if (aValues[i] == nValueSearch) {
			nPos = i;
			break;
		}
	}
	return nPos;
}

//****************************************************************************************************************************************
//********************************************************** Funciones de Misc ***********************************************************
//****************************************************************************************************************************************
function addScript(src) {
//	Debugger.message('function addScript('+src+')');
	var js=document.createElement('script');
	js.src=src;
	js.type='text/javascript';
	js.async=false;
	document.getElementsByTagName('head').appendChild(js);
}

function addCSS(src) {
//	Debugger.message('function addCSS('+src+')');
	var css=document.createElement('link');
	css.rel='stylesheet';
	css.type='text/css';
	css.media='screen';
	css.href=src;
	document.getElementsByTagName('head').appendChild(css);
}

function cerrar() 
{
	if(bIsIE || window.parent.hidePopWin==undefined) 
	{
		var ventana = window.self;
		ventana.opener = window.self;
		ventana.close();
		try {
			if (window.closeDialog)
			{
				closeDialog();
			}
		}
		catch (e) {
		}
	}
	else 
	{
		window.parent.hidePopWin();
	}
}

function IsThatVersion(sVer)
{
	var bIsVersion = false;
	if (navigator.userAgent.indexOf(sVer) != -1) 
	{
		bIsVersion = true;
	}
	return bIsVersion;
}

function openWindow(URL) 
{
	day = new Date();
	id = day.getTime();
	eval("page" + id + " = window.open(URL, '" + id + "','toolbar=1,scrollbars=1,location=1,statusbar=1,menubar=1,resizable=1');");
}

function formatNumber(aNumber, aFormat)
{

    aNumber = aNumber.toString();
	if (aFormat == '')
	{
		var formatedNumber = unformatNumber(aNumber, aFormat);
		return formatedNumber.toString();
	}
	var semicolonPos = indexOfChar(aFormat, ';');
	if (semicolonPos >= 0)
	{
		var positiveFormat = aFormat.substr(0, semicolonPos);
		var negativeFormat = aFormat.substr(semicolonPos + 1);
		var zeroFormat = null;
		semicolonPos = indexOfChar(negativeFormat, ';');
		if (semicolonPos >= 0)
		{
			zeroFormat = negativeFormat.substr(semicolonPos + 1);
			negativeFormat = negativeFormat.substr(0, semicolonPos);
		}
		if (aNumber < 0)
		{
			return formatNumber(Math.abs(aNumber), negativeFormat);
		}
		else
		{
			if (aNumber > 0 || zeroFormat == null)
			{
				return formatNumber(aNumber, positiveFormat);
			}
			else
			{
				return formatNumber(aNumber, zeroFormat);
			}
		}
	}
	var firstPart, leftPart, rightPart;
	var decimalPos = indexOfChar(aFormat, '.');
	if (decimalPos >= 0)
	{
		leftPart = aFormat.substr(0, decimalPos);
		rightPart = aFormat.substr(decimalPos + 1);
		hasDecimalPoint = true;
	}
	else
	{
		leftPart = aFormat;
		rightPart = '';
		hasDecimalPoint = false;
	}
	firstPart = '';
	var i = 0;
	while (i < leftPart.length)
	{
		var ch = leftPart.charAt(i);
		if (ch == '\\')
		{
			i++;
		}
		else
		{
			if (ch == '#') break;
			if (ch == '0') break;
		}
		i++;
	}
	firstPart = leftPart.substr(0, i);
	firstPart = firstPart.replace('\\', '');
	leftPart = leftPart.substr(i);
	if (leftPart == '')
	{
		leftPart = '#';
	}
	var thousandDivisor = 0;
	var hasThousandSeparator = (indexOfChar(leftPart, ',') != -1);
	if (hasThousandSeparator)
	{
		var i = leftPart.length - 1;
		while (i >= 0)
		{
			if (i > 0 && leftPart.charAt(i - 1) == '\\')
			{
				i--;
			}
			else
			{
				var ch = leftPart.charAt(i);
				if (ch == '#') break;
				if (ch == '0') break;
				if (ch == ',') thousandDivisor++;
			}
			i--;
		}
		leftPart = removeChar(leftPart, ',');
	}
	thousandDivisor = Math.pow(1000, thousandDivisor);
	var percentageMultiplier = 0;
	for (var i = 0; i < leftPart.length; i++)
	{
		var ch = leftPart.charAt(i);
		if (ch == '\\')
		{
			i++;
		}
		else
		{
			if (ch == '%')
			{
				percentageMultiplier++;
			}
		}
	}
	for (var i = 0; i < rightPart.length; i++)
	{
		var ch = rightPart.charAt(i);
		if (ch == '\\')
		{
			i++;
		}
		else
		{
			if (ch == '%')
			{
				percentageMultiplier++;
			}
		}
	}
	
	//MABH 20120807
	//percentageMultiplier = Math.pow(1, percentageMultiplier);
	
	percentageMultiplier = Math.pow(100, percentageMultiplier);
	var leftZeroDigits = 0;
	for (var i = 0; i < leftPart.length; i++)
	{
		var ch = leftPart.charAt(i);
		if (ch == '\\')
		{
			i++;
		}
		else
		{
			if (ch == '0') leftZeroDigits++;
			if (ch == '#' && leftZeroDigits > 0) leftZeroDigits++;
		}
	}
	var rightZeroDigits = 0;
	var rightDigits = 0;
	for (var i = rightPart.length - 1; i >= 0; i--)
	{
		if (i > 0 && rightPart.charAt(i - 1) == '\\')
		{
			i--;
		}
		else
		{
			var ch = rightPart.charAt(i);
			if (ch == '0')
			{
				rightZeroDigits++;
				rightDigits++;
			}
			else
			{
				if (ch == '#')
				{
					if (rightZeroDigits > 0) rightZeroDigits++;
					rightDigits++;
				}
			}
		}
	}
	var formatedNumber = unformatNumber(aNumber, '');
	// formatedNumber = formatedNumber * (percentageMultiplier / thousandDivisor);
	formatedNumber = formatedNumber * (percentageMultiplier / thousandDivisor);
	var isNegative = false;

	if (formatedNumber < 0)
	{
		isNegative = true;
		formatedNumber = Math.abs(formatedNumber);
	}
	divisor = Math.pow(10, rightDigits);
	formatedNumber = Math.floor(formatedNumber * divisor + 0.50000000001);
	var fraction = (formatedNumber % divisor).toString();
	formatedNumber = Math.floor(formatedNumber / divisor).toString();
	var rightZeros = '';
	for (var i = 0; i < (rightDigits - fraction.length); i++)
	{
			rightZeros = rightZeros + '0';
	}
	fraction = rightZeros + fraction;
	fraction = fraction.substr(fraction.length - rightDigits, rightDigits);
	var i = fraction.length;
	while (i > rightZeroDigits && fraction.charAt(i - 1) == '0')
	{
		i--;
	}
	fraction = fraction.substr(0, i);
	var leftZeros = '';
	for (var i = 0; i < leftZeroDigits - formatedNumber.length; i ++)
	{
			leftZeros = leftZeros + '0';
	}
	formatedNumber = leftZeros + formatedNumber;
	var i = formatedNumber.length;
	while (i > leftZeroDigits && formatedNumber.charAt(formatedNumber.length - i) == '0')
	{
		i--;
	}
	formatedNumber = formatedNumber.substr(formatedNumber.length - i);
	var digits = 0;
	var formatPos = leftPart.length - 1;
	var numberPos = formatedNumber.length - 1;
	for (var i = 0; i < leftPart.length; i++)
	{
		if (formatPos > 0 && leftPart.charAt(formatPos - 1) == '\\')
		{
			var ch = leftPart.charAt(formatPos);
			formatedNumber = formatedNumber.substr(0, numberPos + 1) + ch + formatedNumber.substr(numberPos + 1);
			formatPos--;
		}
		else
		{
			var ch = leftPart.charAt(formatPos);
			if (ch == '#' || ch == '0')
			{
				if (digits == 3)
				{
					if (hasThousandSeparator && numberPos >= 0)
					{
						formatedNumber = formatedNumber.substr(0, numberPos + 1) + ',' + formatedNumber.substr(numberPos + 1);
					}
					digits = 0;
				}
				digits++;
				numberPos--;
			}
			else
			{
				formatedNumber = formatedNumber.substr(0, numberPos + 1) + ch + formatedNumber.substr(numberPos + 1);
			}
		}
		formatPos--;
	}

	if (hasThousandSeparator)
	{
		while (numberPos >= 0)
		{
			if (digits == 3)
			{
				formatedNumber = formatedNumber.substr(0, numberPos + 1) + ',' + formatedNumber.substr(numberPos + 1);
				digits = 0;
			}
			digits++;
			numberPos--;
		}
	}
	var formatPos = 0;
	var numberPos = 0;
	for (var i = 0; i < rightPart.length; i++)
	{
		var ch = rightPart.charAt(formatPos);
		if (ch == '\\')
		{
			formatPos++;
			var ch = rightPart.charAt(formatPos);
			fraction = fraction.substr(0, numberPos) + ch + fraction.substr(numberPos);
		}
		else
		{
			if (ch != '#' && ch != '0')
			{
				fraction = fraction.substr(0, numberPos) + ch + fraction.substr(numberPos);
			}
		}
		formatPos++;
		numberPos++;
	}

	if (hasDecimalPoint)
	{
		return (((isNegative) ? '-' : '') + firstPart + formatedNumber + '.' + fraction);
	}
	else
	{
		return (((isNegative) ? '-' : '') + firstPart + formatedNumber);
	}
}

function unformatNumber(aNumber, aFormat)
{
	aNumber = aNumber.toString();

	if (aFormat == '')
	{
		var unformatedNumber = aNumber.toString().replace(/([^0-9.\-])/g, '');
		if(unformatedNumber == '' || isNaN(unformatedNumber))
		{
			unformatedNumber = '0';
		}
		return new Number(unformatedNumber);
	}
	var semicolonPos = indexOfChar(aFormat, ';');

	if (semicolonPos >= 0)
	{
		var positiveFormat = aFormat.substr(0, semicolonPos);
		var negativeFormat = aFormat.substr(semicolonPos + 1);
		var zeroFormat = null;
		semicolonPos = indexOfChar(negativeFormat, ';');
		if (semicolonPos >= 0)
		{
			zeroFormat = negativeFormat.substr(semicolonPos + 1);
			negativeFormat = negativeFormat.substr(0, semicolonPos);
		}
		var anArray = new Array(3);
		anArray[0] = privateUnformatNumber(aNumber, positiveFormat);
		anArray[1] = privateUnformatNumber(aNumber, negativeFormat);
		if (anArray[1] != null)
		{
			anArray[1][0] = -1 * anArray[1][0];
		}
		anArray[2] = null;
		if (zeroFormat != null)
		{
			anArray[2] = privateUnformatNumber(aNumber, zeroFormat);
			if (anArray[2] != null)
			{
				anArray[2][0] = 0;
			}
		}
		var max_i = -1;
		var max_len = -1;
		for (var i = 0; i < anArray.length; i++)
		{
			if (anArray[i] != null)
			{
				if (anArray[i][1] > max_len)
				{
					max_i = i;
					max_len = anArray[i][1];
				}
			}
		}
		return (max_i < 0 ? 0 : anArray[max_i][0]);
	}

	    anArray = privateUnformatNumber(aNumber, aFormat);
	return (anArray == null ? 0 : anArray[0]);
}
	
function privateUnformatNumber(aNumber, aFormat)
{


	var infoArray = new Array();
	var regExpStr = unformatNumberRegExpStr(aFormat, infoArray);

	var colons = infoArray[0];
	var percentSigns = infoArray[1];
	var regExp = new RegExp(regExpStr, 'i');
	    var matchArray = aNumber.match(regExp);
	    if (matchArray == null)
	    {
	    	return null;
	    }
	    if (matchArray.index > 0 || regExp.lastIndex < 0)
	    {
	    	return null;
	    }
	    var unformatedNumber = '';
	    if (aNumber[0]=='-') {
		    //for (var i = 2; i < matchArray.length; i++)
		    for (var i = 1; i < matchArray.length; i++)
		    {
		    	unformatedNumber += (matchArray[i] !== undefined)?matchArray[i]:'';
		    }
	    }
	    else
	    {
	    	//positivo
		    for (var i = 2; i < matchArray.length; i++)
		    //for (var i = 1; i < matchArray.length; i++)
		    {
		    	unformatedNumber += (matchArray[i] !== undefined)?matchArray[i]:'';
		    }
	    }
	    
	    unformatedNumber = unformatedNumber.replace(/\,/g, '');

	if (isNaN(unformatedNumber))
	{
		unformatedNumber = '0';
	}
	
	//MABH 20120807
	//unformatedNumber = unformatedNumber * (Math.pow(1000, colons) / Math.pow(1, percentSigns));
	
	unformatedNumber = unformatedNumber * (Math.pow(1000, colons) / Math.pow(100, percentSigns));

	    return new Array(unformatedNumber, regExp.lastIndex);
}

function unformatNumberRegExpStr(aFormat, anInfoArray)
{
		var regExpStr = '(\-)?';
	var digitMatchCapture = false;
	var decimalPointMatchCapture = false;
	var colons = 0;
	var percentSigns = 0;
	for (var i = 0; i < aFormat.length; i++)
	{
		var ch = aFormat.charAt(i);
		if (ch == '\\')
		{
			digitMatchCapture = false;
			i++;
			if (i < aFormat.length)
			{
				ch = aFormat.charAt(i);
				switch (ch)
				{
					case '\\':
						regExpStr += '\\\\?';
						break;
					case '^':
					case '$':
					case '*':
					case '+':
					case '?':
					case '{':
					case '}':
					case '!':
					case '(':
					case ')':
					case '[':
					case ']':
					    regExpStr += '\\' + ch + '?';
					    break;
					default:
					    if ((ch >= '0' && ch <= '9') || ch == '-')
					    {
					    	regExpStr += ch;
					    }
					    else
					    {
					    	regExpStr += ch + '?';
					    }
					    break;
				}
			}
		}
		else
		{
			switch (ch)
			{
				case '^':
				case '$':
				case '*':
				case '+':
				case '?':
				case '{':
				case '}':
				case '!':
				case '(':
				case ')':
				case '[':
				case ']':
				    regExpStr += '\\' + ch + '?';
					digitMatchCapture = false
				    break;
				case ',':
				    if (!decimalPointMatchCapture)
				    {
				    	colons++;
					}
					break;
				case '#':
				case '0':
				    if (!decimalPointMatchCapture)
				    {
				    	if (!digitMatchCapture)
				    	{
				    		regExpStr += '([0-9,]*)';
				    	}
						colons = 0;
				    }
				    else
				    {
				    	if (!digitMatchCapture)
				    	{
				    		regExpStr += '([0-9]*)';
				    	}
				    }
					digitMatchCapture = true;
					break;
				case '.':
				    if (!decimalPointMatchCapture)
				    {
				    	regExpStr += '(\\.?)';
				    }
				    else
				    {
						regExpStr += '\\.?';
					}
					digitMatchCapture = false;
					decimalPointMatchCapture = true;
				    break;
				case '%':
				    percentSigns++;
				default:
					if ((ch >= '0' && ch <= '9') || ch == '-')
					{
					  	regExpStr += ch;
					}
					else
					{
					   	regExpStr += ch + '?';
					}
					digitMatchCapture = false;
				    break;
			}
		}
	}
	if (anInfoArray != null)
	{
		anInfoArray[0] = colons;
		anInfoArray[1] = percentSigns;
	}
	return regExpStr;
}
function indexOfChar(aString, aChar)
{
	for (var i = 0; i < aString.length; i++)
	{
		var ch = aString.charAt(i);
		if (ch == '\\')
		{
			i++;
		}
		else
		{
			if (ch == aChar)
			{
				return i;
			}
		}
	}
	return -1;
}
function removeChar(aString, aChar)
{
	for (var i = aString.length - 1; i >= 0; i--)
	{
		var ch = aString.charAt(i);
		if (ch == '\\')
		{
			i--;
		}
		else
		{
			if (ch == aChar)
			{
				aString = aString.substr(0, i) + aString.substr(i + 1);
			}
		}
	}
	return aString;
}
	
//MABH20130221
//JAPR 2016-05-19: Corregido el proceso que determina la fecha y hora del servidor (#LR773Y)
function formatServerDate(aDate) {
	var y = aDate.getFullYear();
	var m = aDate.getMonth()+1;
	var d = aDate.getDate();
	var h = aDate.getHours();
	var n = aDate.getMinutes();
	var s = aDate.getSeconds();
	return '' + y +'-'+ (m<=9?'0'+m:m) +'-'+ (d<=9?'0'+d:d) +' '+ (h<=9?'0'+h:h) +':'+ (n<=9?'0'+n:n) +':'+ (s<=9?'0'+s:s);
}

/* Dado un string fecha en posible formato universal YYYY-MM-DD, la convierte al formato con / para separar los elementos de fecha (esto no debería ser necesario, el objeto Date funciona con 
ambos formatos, sin embargo lo dejaré como estaba porque a falta de documentación, no puedo saber si hicieron esto por alguna razón o simplemente era algo innecesario) y regresa un Date()
*/
function unformatServerDate(aDateStr) {
	var newDateStr = ('' + aDateStr).replace(/-/g,"/");
	return new Date(newDateStr);
}

/* Dada una fecha completa, determina la cantidad de milisegundos desde el primero registrado por los objetos Date que han transcurrido hasta la fecha indicada, para permitir incrementar 
cada segundo dicha cantidad mediante un interval controlado por gblServerTimeInterval y así obtener la nueva fecha en cualquier momento en base a estos milisegundos */
function setNewServerDate(aServerDateStr) {
	//Al volver a entrar a esta función se está reasignando la fecha directamente desde el server, así que primero limpia el interval pues va a reiniciarlo
    if (gblServerTimeIntervalID) {
		clearInterval(gblServerTimeIntervalID);
		gblServerTimeIntervalID = undefined;
	}
	
	//Reasigna la fecha/hora actual según el server en milisegundos
	gblServerTime = unformatServerDate(aServerDateStr).getTime();
	//Reinicia el intervalo para actualización de la fecha/hora del server cada segundo
	gblServerTimeIntervalID = setInterval(function() {
		//Incrementa en 1 segundo la fecha/hora del server usando milisegundos
		gblServerTime += 1000;
	}, 1000);
}

//JAPR 2016-05-19: Corregido el proceso que determina la fecha y hora del servidor (#LR773Y)
//JAPRDescontinuada
/* Esta función era el equivalente a recibir la fecha y hora del server desde requestHello, sin embargo limpiaba y reactivaba el intervalo con el request para refrescar la fecha cada
cierto tiempo, así que como ese proceso ya no será necesario, esta función tampoco lo es
*/
function setNewServerDateFromInterval(data) {
	if (data.serverSurveyDate && data.serverSurveyHour) {
		var newServerFullDate = data.serverSurveyDate + " " + data.serverSurveyHour;
        
		setNewServerDate(newServerFullDate);
		clearInterval(gblServerVerifierID);
		gblServerVerifierID = undefined;
		//JAPR 2016-05-19: Corregido el proceso que determina la fecha y hora del servidor (#LR773Y)
		//JAPRDescontinuada: Ver comentario de la función
		//setServerVerifier();
	}
}

//JAPR 2016-05-19: Corregido el proceso que determina la fecha y hora del servidor (#LR773Y)
//JAPRDescontinuada
/* Crea un intervalo de requests al servidor para volver a actualizar la fecha y hora desde ese lugar. Este intervalo se controlaba con la variable gblServerTimeInterval y estaba
configurado como default para ejecutarse cada 60 segundos, sin embargo se optó por utilizar el mismo request que actualiza la posición GPS del App y así no tener que crear un segundo
tipo de request constante 
*/
function setServerVerifier() {
	//JAPR 2016-05-19: Corregido el proceso que determina la fecha y hora del servidor (#LR773Y)
	//Había errores en esta función, NO se van a corregir, simplemente si por alguna razón llegara a invocarse, se va a salir automáticamente
	return;
	
	/*
	gblServerVerifierID = setInterval(function() { 
		if (isOnline) {
			if(bitam_mode) {
				urlService = artusweb + 'esurvey/'+file_esurveyservice;
				ajaxParams = {
					serverVerifyMessage: null,
					UserID: user,
					Password: password,
					projectName: project
				};
			} else {
				urlService = 'http://'+esurveyFolder+'/'+file_esurveyservice;
				ajaxParams = {
					serverVerifyMessage: null,
					UserID: user,
					appVersion: appFormsVersion,
					Password: password
				};
			}   
			$.ajax({
				cache: false,
				crossDomain : true,
				timeout: (gblHelloTimeout*1000),
				type : 'GET',
				url : urlService,
				dataType: 'jsonp',
				jsonp : 'jsonpCallback',
				jsonpCallback: 'setNewServerDateFromInterval',
				data: ajaxParams,
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					
				}
			});
		}
	}, gblServerTimeInterval);
	*/
	//JAPR
}

//JAPR 2016-05-19: Corregido el proceso que determina la fecha y hora del servidor (#LR773Y)
/* Regresa la fecha según el servidor formateada en formato universal como YYYY-MM-DD
*/
function getServerDate() {
	if (!gblServerTime) {
		return '';
	}
	
	return getDate(new Date(gblServerTime));
	/*
	var formattedServerDate = "";
	if (gblServerTime) {
		var newDate = new Date(gblServerTime);
		var y = newDate.getFullYear();
		var m = newDate.getMonth()+1;
		var d = newDate.getDate();
		formattedServerDate = y+"-"+m+"-"+d;
	}
	return formattedServerDate;*/
}

//JAPR 2016-05-19: Corregido el proceso que determina la fecha y hora del servidor (#LR773Y)
/* Regresa la hora según el servidor formateada en formato universal como HH:MM:SS
*/
function getServerTime() {
	if (!gblServerTime) {
		return '';
	}
	
	return getTime(new Date(gblServerTime));
	/*var formattedServerTime = "";
	if (gblServerTime) {
		var newTime = new Date(gblServerTime);
		var h = newTime.getHours();
		var n = newTime.getMinutes();
		var s = newTime.getSeconds();
		formattedServerTime = h+":"+n+":"+s;
	}
	return formattedServerTime;*/
}

function getServerEndDate() {
	return getServerDate();
}

function getServerEndTime() {
	return getServerTime();
}

//Funciones tomadas del utils.js del reporteador
var bIsIE            = navigator.userAgent.indexOf("MSIE") != -1;
var sLogginFrom      = 'ARTUS';
var nAddToResize = 40;
var sStyWidth = '';
var gbFBM_WMode = false;
var bAddBodyEvtReportTB = true;
var bIsMobile = IsMobile(navigator.userAgent); //'blackberry'
var bWaitAjax = false;

function GetElementWithId(anId) {
	return document.getElementById(anId);
}
function GetIFrameWithId(anID)
{
	if (document.frames)
	{
		return document.frames[anID];
	}
	else if (GetElementWithId(anID))
	{
		return GetElementWithId(anID).contentDocument;
	}
}
function GetIFrameSonWithId(anIframe, anID)
{
	if (document.frames)
	{
		if (GetIFrameWithId(anIframe)) {
			if (GetIFrameWithId(anIframe).GetElementWithId) {
				if (GetIFrameWithId(anIframe).GetElementWithId(anID)) {
					return GetIFrameWithId(anIframe).GetElementWithId(anID);
				}
			}
		}
	}
	else
	{
		if (GetIFrameWithId(anIframe))
		{
			if (GetIFrameWithId(anIframe).getElementById(anID)) {
				return GetIFrameWithId(anIframe).getElementById(anID);
			}
		}
	}
}
function myReplace(sSource, sFind, sNew)
{
	if(sFind.length==0) return sSource;
	ipos=sSource.indexOf(sFind, 0);
	while(ipos >= 0)
	{
		sSource = sSource.replace(sFind, sNew)
		ipos=sSource.indexOf(sFind, ipos+sNew.length);
	}
	return sSource;
}
//Coockies
function Get_Cookie(name){
   var start = document.cookie.indexOf(name+"=");
   var len = start+name.length+1;
   if ((!start) && (name != document.cookie.substring(0,name.length))) return null;
   if (start == -1) return null;
   var end = document.cookie.indexOf(";",len);
   if (end == -1) end = document.cookie.length;
   return unescape(document.cookie.substring(len,end));
}

function Set_Cookie(name,value,expires,path,domain,secure) {
    var cookieString = name + "=" +escape(value) +
       ( (expires) ? ";expires=" + expires.toGMTString() : "") +
       ( (path) ? ";path=" + path : "") +
       ( (domain) ? ";domain=" + domain : "") +
       ( (secure) ? ";secure" : "");
    document.cookie = cookieString;
}

function Delete_Cookie(name,path,domain) {
   if (Get_Cookie(name)) document.cookie = name + "=" +
      ( (path) ? ";path=" + path : "") +
      ( (domain) ? ";domain=" + domain : "") +
      ";expires=Sun, 01-Jan-70 00:00:01 GMT";
}

function RGBToHexConvert(str) {
	if (str.indexOf('rgb(')==-1) {
		return str;
	}
	str = str.replace(/rgb\(|\)/g, "").split(",");
	str[0] = parseInt(str[0], 10).toString(16).toLowerCase();
	str[1] = parseInt(str[1], 10).toString(16).toLowerCase();
	str[2] = parseInt(str[2], 10).toString(16).toLowerCase();
	str[0] = (str[0].length == 1) ? '0' + str[0] : str[0];
	str[1] = (str[1].length == 1) ? '0' + str[1] : str[1];
	str[2] = (str[2].length == 1) ? '0' + str[2] : str[2];
	return ('#' + str.join(""));
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//JAPR 2016-08-09: Agregado el parámetro para permitir reemplazar los caracteres ASCII 160 (&nbsp convertidos a espacio, pero no ASCII 32) generados por componentes DHTMLX
function html_entity_decode( string, bClearNbsp ) {
    var ret, tarea = document.createElement('textarea');
    tarea.innerHTML = string;
    ret = tarea.value;
	//JAPR 2016-08-09: Agregado el parámetro para permitir reemplazar los caracteres ASCII 160 (&nbsp convertidos a espacio, pero no ASCII 32) generados por componentes DHTMLX
	if (bClearNbsp || bClearNbsp === undefined) {
		ret = ret.replace(/\u00a0/g, ' ');
	}
	//JAPR
    return ret;
}
function getBoolStr(bVal){
	if (bVal){ return '1'; }
	else     { return '0'; }
}
function getStrBool(sVal){
	if (sVal == '0' || sVal == 0){ return false; }
	else            			 { return true;  }
}
function getRestoreElem(aMD, Top, i, sDefault){
	if (i<=Top ){
		if ((aMD[i] != undefined)){
			return aMD[i];
		}
		else{
			return sDefault;
		}
	}
	else{
		return sDefault;
	}
}
function BITAMGetSiguiente(aParams, Token)
{
	var npos = aParams[0].indexOf(Token);
	if (npos != -1)
	{
		stmp = aParams[0].substr(0, npos);
		aParams[0] = aParams[0].substr(npos + Token.length);
		return stmp;
	}
	else
	{
        if (Trim(aParams[0]).length > 0)
        {
        	tmpCadena = aParams[0];
        	aParams[0] = '';
            return tmpCadena;
        }
		else
		{
            return '';
		}
	}
}
function specialcharstohtml(sCadToConvert){
	sCadToConvert = sCadToConvert.replace(/&lt;/gi,"<");
	sCadToConvert = sCadToConvert.replace(/&gt;/gi,">");
	sCadToConvert = sCadToConvert.replace(/&quot;/gi,'"');
	sCadToConvert = sCadToConvert.replace(/&#039;/gi,"'");
	sCadToConvert = sCadToConvert.replace(/&amp;/gi,"&");
	sCadToConvert = sCadToConvert.replace(/<br>/gi,"\n");
	sCadToConvert = sCadToConvert.replace(/_AWNL_/gi,"\n");
	sCadToConvert = sCadToConvert.replace(/&#42/gi,"*");
	return sCadToConvert;
}
function RemoveHtmlNonChar(sText){
	var regexp = new RegExp('\t',"gi");
	sText  = sText.replace(regexp,'_AWSS_');
	regexp = new RegExp(' ',"gi");
	sText  = sText.replace(regexp,'_AWSP_');
	regexp = new RegExp('&',"gi");
	sText  = sText.replace(regexp,'_AWAM_');
	regexp = new RegExp('"',"gi");
	sText  = sText.replace(regexp,'_AWComDob_');
	regexp = new RegExp("'","gi");
	sText  = sText.replace(regexp,'_AWComSim_');
	regexp = new RegExp('<',"gi");
	sText  = sText.replace(regexp,'_AWMin_');
	regexp = new RegExp('>',"gi");
	sText  = sText.replace(regexp,'_AWMay_');
	regexp = new RegExp('\\\\',"gi");
	sText  = sText.replace(regexp,'_AWBS_');
	return sText;
}
function PutHtmlNonChar(sCadToConvert){
	var regexp = new RegExp('_AWSS_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,"\t");
	regexp = new RegExp('_AWSP_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp," ");
	regexp = new RegExp('_AWAM_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,"&");
	regexp = new RegExp('_AWComDob_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,'"');
	regexp = new RegExp('_AWComSim_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,"'");
	regexp = new RegExp('_AWMin_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,"<");
	regexp = new RegExp('_AWMay_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,">");
	regexp = new RegExp('_AWBS_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,"\\");
	return sCadToConvert;
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function Convert_Hex_To_Dec(Hex)
{
	if(parseInt(Hex)==-1) return(-1);
	if(Hex.substr(0,1)=='#') Hex=Hex.substr(1);

	Hex=Hex.substr(4,2)+Hex.substr(2,2)+Hex.substr(0,2);
	Dec=parseInt(Hex, 16);
	if(isNaN(parseInt(Hex, 16)))
		return 0;
	else
		return Dec;
}

function copy(origen,copia,applyMultLanFrase)
{
    if (applyMultLanFrase == undefined) { applyMultLanFrase = false; }

	for(var clave in origen)
	{
        if(typeof origen[clave] == 'object'){
            var elementos = origen[clave].length;
            copia[clave] = new Array(elementos);
            copy(origen[clave],copia[clave],applyMultLanFrase);
        }
        else {
            if (applyMultLanFrase)
            {
            	copia[clave] = getMultLanFrase(origen[clave]);
            }
            else
            {
        		copia[clave] = origen[clave];
            }
        }
    }
}
function copy_WReplace(origen,copia,sReplace){
    for(var clave in origen){
        if(typeof origen[clave] == 'object'){
            var elementos = origen[clave].length;
            copia[clave] = new Array(elementos);
            copy_WReplace(origen[clave],copia[clave],sReplace);
        }
        else {
            copia[clave] = sReplace+origen[clave];
        }
    }
}

function array_search(nValueSearch, aValues) {
	var nTop = aValues.length;
	var nPos = -1;
	for (var i=0; i<nTop; i++) {
		if (aValues[i] == nValueSearch) {
			nPos = i;
			break;
		}
	}
	return nPos;
}

function getValidImageSrc(src) {
	src = myReplace(src, '%', '%25');
	src = myReplace(src, '#', '%23');
	src = myReplace(src, ' ', '%20');
	return src;
}

function str_repeat(x,n)
{
    var y = '';
    while(true)
    {
        if(n & 1) y += x;
        n >>= 1;
        if(n) x += x; else break;
    }
    return y;
}

function compararAsc(a, b)
{
    if (a[1] == b[1])
    {
        return (a[0] < b[0]) ? -1 : 1;
    }

    return (a[1] < b[1]) ? -1 : 1;
}

function compararDesc(a, b) {
    if (a[1] == b[1]) {
        return (a[0] < b[0]) ? -1 : 1;
    }

    return (a[1] < b[1]) ? 1 : -1;
}

function uasort(inputArr, sorter) {
  // http://kevin.vanzonneveld.net
  // %        note 1: This function deviates from PHP in returning a copy of the array instead
  // %        note 1: of acting by reference and returning true; this was necessary because
  // %        note 1: IE does not allow deleting and re-adding of properties without caching
  // %        note 1: of property position; you can set the ini of "phpjs.strictForIn" to true to
  // %        note 1: get the PHP behavior, but use this only if you are in an environment
  // %        note 1: such as Firefox extensions where for-in iteration order is fixed and true
  // %        note 1: property deletion is supported. Note that we intend to implement the PHP
  // %        note 1: behavior by default if IE ever does allow it; only gives shallow copy since
  // %        note 1: is by reference in PHP anyways
  // *     example 1: fruits = {d: 'lemon', a: 'orange', b: 'banana', c: 'apple'};
  // *     example 1: fruits = uasort(fruits, function (a, b) { if (a > b) {return 1;}if (a < b) {return -1;} return 0;});
  // *     results 1: fruits == {c: 'apple', b: 'banana', d: 'lemon', a: 'orange'}
  var valArr = [],
    tempKeyVal, tempValue, ret, k = '',
    i = 0,
    strictForIn = false,
    populateArr = {};

  if (typeof sorter === 'string') {
    sorter = this[sorter];
  } else if (Object.prototype.toString.call(sorter) === '[object Array]') {
    sorter = this[sorter[0]][sorter[1]];
  }

  // BEGIN REDUNDANT
  this.php_js = this.php_js || {};
  this.php_js.ini = this.php_js.ini || {};
  // END REDUNDANT
  strictForIn = this.php_js.ini['phpjs.strictForIn'] && this.php_js.ini['phpjs.strictForIn'].local_value && this.php_js.ini['phpjs.strictForIn'].local_value !== 'off';
  populateArr = strictForIn ? inputArr : populateArr;


  for (k in inputArr) { // Get key and value arrays
    if (inputArr.hasOwnProperty(k)) {
      valArr.push([k, inputArr[k]]);
      if (strictForIn) {
        delete inputArr[k];
      }
    }
  }
  valArr.sort(function (a, b) {
    return sorter(a[1], b[1]);
  });

  for (i = 0; i < valArr.length; i++) { // Repopulate the old array
    populateArr[valArr[i][0]] = valArr[i][1];
  }

  //return strictForIn || populateArr;
  return strictForIn || valArr;
}

function IsMobile(a)
{
	return (/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
}
function strpos (haystack, needle, offset) {
  var i = (haystack + '').indexOf(needle, (offset || 0));
  return i === -1 ? false : i;
}
function isset () {
  var a = arguments,
    l = a.length,
    i = 0,
    undef;

  if (l === 0) {
    throw new Error('Empty isset');
  }

  while (i !== l) {
    if (a[i] === undef || a[i] === null) {
      return false;
    }
    i++;
  }
  return true;
}
function is_array (mixed_var) {
  var ini,
    _getFuncName = function (fn) {
      var name = (/\W*function\s+([\w\$]+)\s*\(/).exec(fn);
      if (!name) {
        return '(Anonymous)';
      }
      return name[1];
    },
    _isArray = function (mixed_var) {
      // return Object.prototype.toString.call(mixed_var) === '[object Array]';
      // The above works, but let's do the even more stringent approach: (since Object.prototype.toString could be overridden)
      // Null, Not an object, no length property so couldn't be an Array (or String)
      if (!mixed_var || typeof mixed_var !== 'object' || typeof mixed_var.length !== 'number') {
        return false;
      }
      var len = mixed_var.length;
      mixed_var[mixed_var.length] = 'bogus';
      // The only way I can think of to get around this (or where there would be trouble) would be to have an object defined
      // with a custom "length" getter which changed behavior on each call (or a setter to mess up the following below) or a custom
      // setter for numeric properties, but even that would need to listen for specific indexes; but there should be no false negatives
      // and such a false positive would need to rely on later JavaScript innovations like __defineSetter__
      if (len !== mixed_var.length) { // We know it's an array since length auto-changed with the addition of a
      // numeric property at its length end, so safely get rid of our bogus element
        mixed_var.length -= 1;
        return true;
      }
      // Get rid of the property we added onto a non-array object; only possible
      // side-effect is if the user adds back the property later, it will iterate
      // this property in the older order placement in IE (an order which should not
      // be depended on anyways)
      delete mixed_var[mixed_var.length];
      return false;
    };

  if (!mixed_var || typeof mixed_var !== 'object') {
    return false;
  }

  // BEGIN REDUNDANT
  this.php_js = this.php_js || {};
  this.php_js.ini = this.php_js.ini || {};
  // END REDUNDANT

  ini = this.php_js.ini['phpjs.objectsAsArrays'];

  return _isArray(mixed_var) ||
    // Allow returning true unless user has called
    // ini_set('phpjs.objectsAsArrays', 0) to disallow objects as arrays
    ((!ini || ( // if it's not set to 0 and it's not 'off', check for objects as arrays
    (parseInt(ini.local_value, 10) !== 0 && (!ini.local_value.toLowerCase || ini.local_value.toLowerCase() !== 'off')))
    ) && (
    Object.prototype.toString.call(mixed_var) === '[object Object]' && _getFuncName(mixed_var.constructor) === 'Object' // Most likely a literal and intended as assoc. array
    ));
}
function explode (delimiter, string, limit) {

  if ( arguments.length < 2 || typeof delimiter == 'undefined' || typeof string == 'undefined' ) return null;
  if ( delimiter === '' || delimiter === false || delimiter === null) return false;
  if ( typeof delimiter == 'function' || typeof delimiter == 'object' || typeof string == 'function' || typeof string == 'object'){
    return { 0: '' };
  }
  if ( delimiter === true ) delimiter = '1';

  // Here we go...
  delimiter += '';
  string += '';

  var s = string.split( delimiter );

  if ( typeof limit === 'undefined' ) return s;
  // Support for limit
  if ( limit === 0 ) limit = 1;
  // Positive limit
  if ( limit > 0 ){
    if ( limit >= s.length ) return s;
    return s.slice( 0, limit - 1 ).concat( [ s.slice( limit - 1 ).join( delimiter ) ] );
  }

  // Negative limit
  if ( -limit >= s.length ) return [];

  s.splice( s.length + limit );
  return s;
}
function count (mixed_var, mode) {
  var key, cnt = 0;

  if (mixed_var === null || typeof mixed_var === 'undefined') {
    return 0;
  } else if (mixed_var.constructor !== Array && mixed_var.constructor !== Object) {
    return 1;
  }

  if (mode === 'COUNT_RECURSIVE') {
    mode = 1;
  }
  if (mode != 1) {
    mode = 0;
  }

  for (key in mixed_var) {
    if (mixed_var.hasOwnProperty(key)) {
      cnt++;
      if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
        cnt += this.count(mixed_var[key], 1);
      }
    }
  }

  return cnt;
}
function str_replace (search, replace, subject, count) {
  var i = 0,
    j = 0,
    temp = '',
    repl = '',
    sl = 0,
    fl = 0,
    f = [].concat(search),
    r = [].concat(replace),
    s = subject,
    ra = Object.prototype.toString.call(r) === '[object Array]',
    sa = Object.prototype.toString.call(s) === '[object Array]';
    s = [].concat(s);
  if (count) {
    this.window[count] = 0;
  }

  for (i = 0, sl = s.length; i < sl; i++) {
    if (s[i] === '') {
      continue;
    }
    for (j = 0, fl = f.length; j < fl; j++) {
      temp = s[i] + '';
      repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
      s[i] = (temp).split(f[j]).join(repl);
      if (count && s[i] !== temp) {
        this.window[count] += (temp.length - s[i].length) / f[j].length;
      }
    }
  }
  return sa ? s : s[0];
}
function strlen (string) {
   var str = string + '';
   var i = 0,
    chr = '',
    lgth = 0;

  if (typeof(string)=='string' && (!this.php_js || !this.php_js.ini || !this.php_js.ini['unicode.semantics'] || this.php_js.ini['unicode.semantics'].local_value.toLowerCase() !== 'on')) {
    return string.length;
  }

  var getWholeChar = function (str, i) {
    var code = str.charCodeAt(i);
    var next = '',
      prev = '';
    if (0xD800 <= code && code <= 0xDBFF) { // High surrogate (could change last hex to 0xDB7F to treat high private surrogates as single characters)
      if (str.length <= (i + 1)) {
        throw 'High surrogate without following low surrogate';
      }
      next = str.charCodeAt(i + 1);
      if (0xDC00 > next || next > 0xDFFF) {
        throw 'High surrogate without following low surrogate';
      }
      return str.charAt(i) + str.charAt(i + 1);
    } else if (0xDC00 <= code && code <= 0xDFFF) { // Low surrogate
      if (i === 0) {
        throw 'Low surrogate without preceding high surrogate';
      }
      prev = str.charCodeAt(i - 1);
      if (0xD800 > prev || prev > 0xDBFF) { //(could change last hex to 0xDB7F to treat high private surrogates as single characters)
        throw 'Low surrogate without preceding high surrogate';
      }
      return false; // We can pass over low surrogates now as the second component in a pair which we have already processed
    }
    return str.charAt(i);
  };

  for (i = 0, lgth = 0; i < str.length; i++) {
    if ((chr = getWholeChar(str, i)) === false) {
      continue;
    } // Adapt this line at the top of any loop, passing in the whole string and the current iteration and returning a variable to represent the individual character; purpose is to treat the first part of a surrogate pair as the whole character and then ignore the second part
    lgth++;
  }
  return lgth;
}
function mktime () {
  var d = new Date(),
    r = arguments,
    i = 0,
    e = ['Hours', 'Minutes', 'Seconds', 'Month', 'Date', 'FullYear'];

  for (i = 0; i < e.length; i++) {
    if (typeof r[i] === 'undefined') {
      r[i] = d['get' + e[i]]();
      r[i] += (i === 3); // +1 to fix JS months.
    } else {
      r[i] = parseInt(r[i], 10);
      if (isNaN(r[i])) {
        return false;
      }
    }
  }

  // Map years 0-69 to 2000-2069 and years 70-100 to 1970-2000.
  r[5] += (r[5] >= 0 ? (r[5] <= 69 ? 2e3 : (r[5] <= 100 ? 1900 : 0)) : 0);

  // Set year, month (-1 to fix JS months), and date.
  // !This must come before the call to setHours!
  d.setFullYear(r[5], r[3] - 1, r[4]);

  // Set hours, minutes, and seconds.
  d.setHours(r[0], r[1], r[2]);

  // Divide milliseconds by 1000 to return seconds and drop decimal.
  // Add 1 second if negative or it'll be off from PHP by 1 second.
  return d.getTime();
  //return (d.getTime() / 1e3 >> 0) - (d.getTime() < 0);
}

function roundPHP(value, precision, mode) {
  var m, f, isHalf, sgn; // helper variables
  precision |= 0; // making sure precision is integer
  m = Math.pow(10, precision);
  value *= m;
  sgn = (value > 0) | -(value < 0); // sign of the number
  isHalf = value % 1 === 0.5 * sgn;
  f = Math.floor(value);

  if (isHalf) {
    switch (mode) {
    case 'PHP_ROUND_HALF_DOWN':
      value = f + (sgn < 0); // rounds .5 toward zero
      break;
    case 'PHP_ROUND_HALF_EVEN':
      value = f + (f % 2 * sgn); // rouds .5 towards the next even integer
      break;
    case 'PHP_ROUND_HALF_ODD':
      value = f + !(f % 2); // rounds .5 towards the next odd integer
      break;
    default:
      value = f + (sgn > 0); // rounds .5 away from zero
    }
  }

  return (isHalf ? value : Math.round(value)) / m;
}

function array_reverse (array, preserve_keys) {
  var isArray = Object.prototype.toString.call(array) === "[object Array]",
    tmp_arr = preserve_keys ? {} : [],  key;

  if (isArray && !preserve_keys) {   return array.slice(0).reverse(); }

  if (preserve_keys) {
    var keys = [];
    for (key in array) {
      // if (array.hasOwnProperty(key)) {
      keys.push(key);
      // }
    }

    var i = keys.length;
    while (i--) {
      key = keys[i];
      // FIXME: don't rely on browsers keeping keys in insertion order
      // it's implementation specific
      // eg. the result will differ from expected in Google Chrome
      tmp_arr[key] = array[key];
    }
  } else {
    for (key in array) {
      // if (array.hasOwnProperty(key)) {
      tmp_arr.unshift(array[key]);
      // }
    }
  }
  return tmp_arr;
}

function array_splice (arr, offst, lgth, replacement) {
  // http://kevin.vanzonneveld.net
  // +   original by: Brett Zamir (http://brett-zamir.me)
  // +   input by: Theriault
  // %        note 1: Order does get shifted in associative array input with numeric indices,
  // %        note 1: since PHP behavior doesn't preserve keys, but I understand order is
  // %        note 1: not reliable anyways
  // %        note 2: Note also that IE retains information about property position even
  // %        note 2: after being supposedly deleted, so use of this function may produce
  // %        note 2: unexpected results in IE if you later attempt to add back properties
  // %        note 2: with the same keys that had been deleted
  // -    depends on: is_int
  // *     example 1: input = {4: "red", 'abc': "green", 2: "blue", 'dud': "yellow"};
  // *     example 1: array_splice(input, 2);
  // *     returns 1: {0: "blue", 'dud': "yellow"}
  // *     results 1: input == {'abc':"green", 0:"red"}
  // *     example 2: input = ["red", "green", "blue", "yellow"];
  // *     example 2: array_splice(input, 3, 0, "purple");
  // *     returns 2: []
  // *     results 2: input == ["red", "green", "blue", "purple", "yellow"]
  // *     example 3: input = ["red", "green", "blue", "yellow"]
  // *     example 3: array_splice(input, -1, 1, ["black", "maroon"]);
  // *     returns 3: ["yellow"]
  // *     results 3: input == ["red", "green", "blue", "black", "maroon"]
  var _checkToUpIndices = function (arr, ct, key) {
    // Deal with situation, e.g., if encounter index 4 and try to set it to 0, but 0 exists later in loop (need to
    // increment all subsequent (skipping current key, since we need its value below) until find unused)
    if (arr[ct] !== undefined) {
      var tmp = ct;
      ct += 1;
      if (ct === key) {
        ct += 1;
      }
      ct = _checkToUpIndices(arr, ct, key);
      arr[ct] = arr[tmp];
      delete arr[tmp];
    }
    return ct;
  };

  if (replacement && typeof replacement !== 'object') {
    replacement = [replacement];
  }
  if (lgth === undefined) {
    lgth = offst >= 0 ? arr.length - offst : -offst;
  } else if (lgth < 0) {
    lgth = (offst >= 0 ? arr.length - offst : -offst) + lgth;
  }

  if (Object.prototype.toString.call(arr) !== '[object Array]') {
/*if (arr.length !== undefined) { // Deal with array-like objects as input
    delete arr.length;
    }*/
    var lgt = 0,
      ct = -1,
      rmvd = [],
      rmvdObj = {},
      repl_ct = -1,
      int_ct = -1;
    var returnArr = true,
      rmvd_ct = 0,
      rmvd_lgth = 0,
      key = '';
    // rmvdObj.length = 0;
    for (key in arr) { // Can do arr.__count__ in some browsers
      lgt += 1;
    }
    offst = (offst >= 0) ? offst : lgt + offst;
    for (key in arr) {
      ct += 1;
      if (ct < offst) {
        if (this.is_int(key)) {
          int_ct += 1;
          if (parseInt(key, 10) === int_ct) { // Key is already numbered ok, so don't need to change key for value
            continue;
          }
          _checkToUpIndices(arr, int_ct, key); // Deal with situation, e.g.,
          // if encounter index 4 and try to set it to 0, but 0 exists later in loop
          arr[int_ct] = arr[key];
          delete arr[key];
        }
        continue;
      }
      if (returnArr && this.is_int(key)) {
        rmvd.push(arr[key]);
        rmvdObj[rmvd_ct++] = arr[key]; // PHP starts over here too
      } else {
        rmvdObj[key] = arr[key];
        returnArr = false;
      }
      rmvd_lgth += 1;
      // rmvdObj.length += 1;
      if (replacement && replacement[++repl_ct]) {
        arr[key] = replacement[repl_ct];
      } else {
        delete arr[key];
      }
    }
    // arr.length = lgt - rmvd_lgth + (replacement ? replacement.length : 0); // Make (back) into an array-like object
    return returnArr ? rmvd : rmvdObj;
  }

  if (replacement) {
    replacement.unshift(offst, lgth);
    return Array.prototype.splice.apply(arr, replacement);
  }
  return arr.splice(offst, lgth);
}

function fmod(X, Y) { return X%Y }

function print_r (array, return_val) {
  // http://kevin.vanzonneveld.net
  // +   original by: Michael White (http://getsprink.com)
  // +   improved by: Ben Bryan
  // +      input by: Brett Zamir (http://brett-zamir.me)
  // +      improved by: Brett Zamir (http://brett-zamir.me)
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // -    depends on: echo
  // *     example 1: print_r(1, true);
  // *     returns 1: 1
  var output = '',
    pad_char = ' ',
    pad_val = 4,
    d = this.window.document,
    getFuncName = function (fn) {
      var name = (/\W*function\s+([\w\$]+)\s*\(/).exec(fn);
      if (!name) {
        return '(Anonymous)';
      }
      return name[1];
    },
    repeat_char = function (len, pad_char) {
      var str = '';
      for (var i = 0; i < len; i++) {
        str += pad_char;
      }
      return str;
    },
    formatArray = function (obj, cur_depth, pad_val, pad_char) {
      if (cur_depth > 0) {
        cur_depth++;
      }

      var base_pad = repeat_char(pad_val * cur_depth, pad_char);
      var thick_pad = repeat_char(pad_val * (cur_depth + 1), pad_char);
      var str = '';

      if (typeof obj === 'object' && obj !== null && obj.constructor && getFuncName(obj.constructor) !== 'PHPJS_Resource') {
        str += 'Array\n' + base_pad + '(\n';
        for (var key in obj) {
          if (Object.prototype.toString.call(obj[key]) === '[object Array]') {
            str += thick_pad + '[' + key + '] => ' + formatArray(obj[key], cur_depth + 1, pad_val, pad_char);
          }
          else {
            str += thick_pad + '[' + key + '] => ' + obj[key] + '\n';
          }
        }
        str += base_pad + ')\n';
      }
      else if (obj === null || obj === undefined) {
        str = '';
      }
      else { // for our "resource" class
        str = obj.toString();
      }

      return str;
    };

  output = formatArray(array, 0, pad_val, pad_char);

  if (return_val !== true) {
    if (d.body) {
      this.echo(output);
    }
    else {
      try {
        d = XULDocument; // We're in XUL, so appending as plain text won't work; trigger an error out of XUL
        this.echo('<pre xmlns="http://www.w3.org/1999/xhtml" style="white-space:pre;">' + output + '</pre>');
      } catch (e) {
        this.echo(output); // Outputting as plain text may work in some plain XML
      }
    }
    return true;
  }
  return output;
}

function array_sum (array) {
  var key, sum = 0;

  if (array && typeof array === 'object' && array.change_key_case) { // Duck-type check for our own array()-created PHPJS_Array
    return array.sum.apply(array, Array.prototype.slice.call(arguments, 0));
  }

   if (typeof array !== 'object') {
    return null;
  }

  for (key in array) {
    if (!isNaN(parseFloat(array[key]))) {
      sum += parseFloat(array[key]);
    }
  }

  return sum;
}

function iif(bCondition, rTrue, rFalse)
{
	if (bCondition)
		return rTrue;
	else
		return rFalse;
}
function IIF(bCondition, rTrue, rFalse)
{
	if (bCondition)
		return rTrue;
	else
		return rFalse;
}

function ParsePathImg(sImgPath) {
	var sImgRet = sImgPath;
	var nPosProjects = sImgPath.indexOf('projects/');
	if (nPosProjects != -1) {
		var nPosImages = sImgPath.indexOf('/images/', nPosProjects);
		if (nPosImages != -1) {
			sImgRet = repository+'/images/'+sImgPath.substring(nPosImages+8);
		}
	}
	return sImgRet;
}

function specialhtmltochars(sCadToConvert){
	sCadToConvert = sCadToConvert.replace(/</gi,"&lt;");
	sCadToConvert = sCadToConvert.replace(/>/gi,"&gt;");
	sCadToConvert = sCadToConvert.replace(/"/gi,'&quot;');
	return sCadToConvert;
}

//JAPR 2019-02-15: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
//Obtiene la porción del atributo src de un tag IMG proporcionado como html (NO como elemento del DOM, utilizar outerHTML del tag IMG real)
function GetImageSrcFromImageTag(sTagStr) {
	var objMatches = String(sTagStr).match(/(img|src)\=(\"|\')[^\"\'\>]+/g);
	if ( !objMatches || !objMatches.length ) {
		return '';
	}
	
	var strSrc = objMatches[0].replace("src=", "");
	var intFirst = strSrc.indexOf('"'), intLast = strSrc.lastIndexOf('"');
	if ( intFirst != intLast ) {
		strSrc = strSrc.substr(intFirst+1, intLast-1);
	}
	else if ( intFirst != -1 ) {
		strSrc = strSrc.substr(intFirst+1);
	}
	
	return strSrc;
}

//JAPR 2019-04-29: Optimizada la carga de múltiples secciones tabla en la misma forma (#EZ06TY)
/* Rellena la cadena dada en el inicio hasta la longitud especificada, utilizando la otra cadena de relleno. Agregada por compatibilidad debido a que en algunos dispositivos no se encuentra
disponible la función de Strings.padStart) */
function padStart(sSourceString, iTargetLength, sFillString) {
	if ( !sFillString ) {
		sFillString = ' ';
	}
	
	if ( !iTargetLength || iTargetLength < 0 ) {
		iTargetLength = 0;
	}
	
	var intLength = String(sSourceString).length;
	if ( iTargetLength < intLength ) {
		return String(sSourceString);
	}
	
	var strText = String(sFillString).repeat(iTargetLength);
	return strText.substr(0, iTargetLength - intLength) + String(sSourceString);
}