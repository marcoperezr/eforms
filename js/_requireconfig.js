require.config({
	//2013-10-22@ARMZ: Agregado el parametro bust para evitar cache de los archivos, esto por mala configuracion del server
	//OMMC 2019-07-19: Corregido detalle con redactor, ya que los wizards de eBavel estaban sobreescribiendo jQuery
	urlArgs: "bust=" +  (new Date()).getTime(),
	waitSeconds: 200,
	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: ["underscore", "jquery"],
			exports: "Backbone"
		},
		jquery: {
			deps: ["jqueryui"],
			exports: "$"
		},
		redactorEditor: {
			deps: ["jquery"],
			exports: "redactor"
		}
	},
	paths: {
		underscore: 'libs/underscore/underscore-min',
		bootstrap: 'libs/bootstrap/bootstrap.min',
		backbone: 'libs/backbone/backbone-min',	
		jqueryui: 'jquery_1.9.1/jquery-1.9.1.min',
		//jquery: 'jquery/ui/jquery-ui-1.8.23.custom',
		jquery: 'eBavel/jquery-ui-1.10.1.custom.min',
		redactorEditor: 'redactor/redactor/redactor.min',
		dhtmlx: 'codebase/dhtmlxFmtd',
		// Require.js plugins
		text: 'libs/require/text',		
		json: 'libs/require/json',
		css: 'libs/require/css',
		// Just a short cut so we can put our html outside the js dir
		// When you have HTML/CSS designers this aids in keeping them out of the js directory
		templates: '../templates',
		cssFolder: '../css'
	}	
});