var arrInd = new Array();
var arrRol = new Array();
var arrIndSort = new Array();
var arrVal = new Array();
var col_active = '';
var sBCKGColorActive = '#899db8';
var sColorActive = '#ffffff';
var sBCKGColorOver = '#e1e4e9';
var sMLIdStep = 604;

function OnStart(strAllElements, strSelectedElements)
{
	if (bIsIE) 
	{
		document.getElementById('divHS').style.width = document.body.offsetWidth;
		document.getElementById('id_Step1').style.width = document.body.offsetWidth-25;
		
		if (document.getElementById('botOK')) 
		{
			document.getElementById('botOK').className = 'NbuttonIE';
		}
		
		if (document.getElementById('botCancel')) 
		{
			document.getElementById('botCancel').className = 'NbuttonIE';
		}
	}
	else  
	{
		document.getElementById('id_Step1').style.width = "565px";
	}
	
	if (IsThatVersion("Chrome")) 
	{
		document.getElementById('tbButtons').style.width = document.body.offsetWidth-25;
	}
	else 
	{
		document.getElementById('tbButtons').style.width = document.body.offsetWidth-15;
	}
	
	//construct_arrays(window_dialogArguments[3]);
	construct_arrays(strAllElements, strSelectedElements);
	fill_Cols();
}

//function construct_arrays(sSelected){
function construct_arrays(strAllElements, sSelected)
{
	var arrCols = new Array();
	var arrInfo = new Array();
	//arrCols = window_dialogArguments[2];
	arrCols = strAllElements;
	arrCols = arrCols.split('_AWSepElement_');
	var sSelectedSearch = '_AWElem_'+sSelected+'_AWElem_';
	var bAll = false;
	
	var itot = arrCols.length;
	for (var i=0; i<itot; i++)
	{
		if (arrCols[i] != '')
		{
			arrInfo = arrCols[i].split('_AWSepName_');
			arrVal[arrInfo[0]] = arrInfo[1];
			if (bAll)
			{
				arrIndSort.push(arrInfo[0]);	// Agregarlos a la derecha
			}
			else if (sSelectedSearch.indexOf('_AWElem_'+arrInfo[0]+'_AWElem_') == -1) 
			{
				//Aqui se tiene que identificar a donde se tiene q agregar
				//si al arreglo de arrInd de usuarios o al arreglo arrRol de roles
				arrWho = arrInfo[0].split('_');
				
				if(arrWho[0]=='User')
				{
					arrInd.push(arrInfo[0]);	// Agregarlo a los de la izquierda	
				}
				else
				{
					arrRol.push(arrInfo[0]);	// Agregarlo a los de la izquierda	
				}
			}
		}
	}
	
	if (!bAll) 
	{
		arrCols = sSelected.split('_AWElem_');
		itot = arrCols.length;
		for (i=0; i<itot; i++)
		{
			if (arrVal[arrCols[i]] != undefined) 
			{
				arrIndSort.push(arrCols[i]);	// Agregarlos a la derecha
			}
		}
	}

	arrCols = null;
	arrInfo = null;
}

function setarrInds(type)
{
	var bFound = false;
	var pointer = new Array();
	var arrInfo = new Array();

	if(type=='User')
	{
		arrInd = new Array();
	}
	else
	{
		arrRol = new Array();
	}
	
	for (idElement in arrVal) 
	{
		arrInfo = idElement.split('_');
		
		if(arrInfo[0]==type)
		{
			bFound = false;
			for (var i=0; i<arrIndSort.length; i++) 
			{
				if (arrIndSort[i] == idElement) 
				{
					bFound = true;
					break;
				}
			}
			
			if (!bFound) 
			{
				pointer.push(idElement);
			}
		}
	}
	
	if(type=='User')
	{
		arrInd = pointer;
	}
	else
	{
		arrRol = pointer;
	}
}

function fill_Cols()
{
	var table_info = Array();
	var table_infoRol = Array();
	var table_Sort_info = Array();
	var onmouse_ov_out = 'onmouseover="change_color_over(this);" onmouseout="change_color_out(this,\'transparent\');" onclick="set_color(this);"';
	var sstyle = 'style="border-bottom:1px solid #F3F3F3;"';
	
	// los del lado izquierdo
	var itot = arrInd.length;
	
	for (var i=0; i<itot; i++) 
	{
		table_info.push('<tr '+onmouse_ov_out+' id="'+arrInd[i]+'"><td '+sstyle+'>'+arrVal[arrInd[i]]+'</td></tr>');
	}
	
	var table_HTML = '<table cellspading=0 cellspacing=0 width=100% style="font-size:11px;">' + table_info.join('') + '</table>';
	document.getElementById("Columns").innerHTML = table_HTML;
	
	// los del lado izquierdo pero que son roles
	var itot = arrRol.length;
	
	for (var i=0; i<itot; i++) 
	{
		table_infoRol.push('<tr '+onmouse_ov_out+' id="'+arrRol[i]+'"><td '+sstyle+'>'+arrVal[arrRol[i]]+'</td></tr>');
	}
	
	var table_HTML = '<table cellspading=0 cellspacing=0 width=100% style="font-size:11px;">' + table_infoRol.join('') + '</table>';
	document.getElementById("Roles").innerHTML = table_HTML;

	// los del lado derecho
	itot = arrIndSort.length;
	for (i=0; i<itot; i++) 
	{
		table_Sort_info.push('<tr '+onmouse_ov_out+' id="'+arrIndSort[i]+'"><td '+sstyle+'>'+arrVal[arrIndSort[i]]+'</td></tr>');
	}
	table_Sort_HTML = '<table cellspading=0 cellspacing=0 width=100% style="font-size:11px;">' + table_Sort_info.join('') + '</table>';
	document.getElementById("SortColumns").innerHTML = table_Sort_HTML;
}

function change_color_over(celda)
{
	if (celda.id != col_active)
		celda.style.backgroundColor = sBCKGColorOver;
}

function change_color_out(celda, color)
{
	if (celda.id != col_active)
		celda.style.backgroundColor=color;
}

function set_color(celda){
	if (celda.id != col_active)
	{
		celda.style.backgroundColor = sBCKGColorActive;
		celda.style.color = sColorActive;
		if (col_active != '')
		{
			document.getElementById(col_active).style.backgroundColor = "transparent";
			document.getElementById(col_active).style.color = "#000000";
		}
		col_active = celda.id;
	}
}

function Add_to_Order()
{
	var arrayInfo = col_active.split('_');
	var pointer;
	
	if(arrayInfo[0]=='User')
	{
		pointer = arrInd;
	}
	else
	{
		pointer = arrRol;
	}
	
	if (col_active != '' && pointer.length>0 && array_search(col_active, arrIndSort) == -1)
	{
		arrIndSort.push(col_active);
		
		setarrInds(arrayInfo[0]);
		fill_Cols();
		document.getElementById(col_active).style.backgroundColor = sBCKGColorActive;
		document.getElementById(col_active).style.color = sColorActive;
	}
}

function Del_to_Order()
{
	var arrayInfo = col_active.split('_');
	if (col_active != '' && arrIndSort.length>0)
	{
		DelElemArray();
		setarrInds(arrayInfo[0]);
		fill_Cols();
		document.getElementById(col_active).style.backgroundColor = sBCKGColorActive;
		document.getElementById(col_active).style.color = sColorActive;
		
		//llamamos la funcion para desplegar la columna de usuarios o roles 
		//a la cual se le esta regresando el elemento para que se vea el movimiento
		if(arrayInfo[0]=='User')
		{
			displayUsers();
		}
		else
		{
			displayRoles();
		}
	}
}

function DelElemArray()
{
	if (col_active != '')
	{
		var tam_arr = arrIndSort.length;
		for (i=0; i<tam_arr; i++)
		{
			if (arrIndSort[i] == col_active)
			{
				for (j=i; j<tam_arr-1; j++)
				{
					arrIndSort[j] = arrIndSort[j+1];
				}
				arrIndSort.pop();
				break;
			}
		}
	}
}

function On_Ok()
{
    if (arrIndSort.length == 0) 
	{
    	alert('Invalid Users and Roles');
    	return;
    }
    var sParamReturn = GetReturnedValue().join('_AWElem_');
	try {
		if (window.setDialogReturnValue)
		{
			setDialogReturnValue(sParamReturn);
		}
	}
	catch (e) {
	}
	try {
		owner.returnValue = sParamReturn;
	}
	catch (e)
	{
	}
	cerrar();
}

function GetReturnedValue() 
{
	var aRetVal = new Array();  
	var sRetVal = '';
	for (idElement in arrIndSort) 
	{
		sRetVal = arrIndSort[idElement];
		if (arrVal[sRetVal] != undefined) 
		{
			//sRetVal = sRetVal + '_AWSepName_' + arrVal[sRetVal];
			sRetVal = sRetVal;
		}
		aRetVal.push(sRetVal);
	}
	return aRetVal;
}