var fileWorker = new Worker('js/filemanagerWorker.js');

fileWorker.addEventListener('message', function(e) {
	//Desde modo Web no se pueden leer archivos, así que simplemente regresa un texto vacio y llama al callback
	if (e.data.errNum && e.data.errNum != 0) {
		if (e.data.errorFn && e.data.errorFn != '') {
			var objErrorFn = getFunctionRefFromName(e.data.errorFn);
			if (objErrorFn) {
				objErrorFn();
			}
		}
	}
	else {
		if (e.data.successFn && e.data.successFn != '') {
			var objSuccessFn = getFunctionRefFromName(e.data.successFn);
			if (objSuccessFn) {
				objSuccessFn();
			}
		}
	}
}, false);

/* Warning: Por alguna razón en los móviles cuando se invoca a una función de clase como Callback, "this" NO hace referencia a la instancia sobre
la que se pidió la función, así que hay que utilizar objetos globales en lugar de this en estos casos, si realmente se necesita usar this, entonces
se debe de pasar la función de callback tal cual y no como nombre de función
*/
ESFile = {
    prefixFile : '',
    lastFileName : '',				//Nombre del último archivo sujeto a cualquiera de los método de esta clase
    init : function (prefixFile) {
		Debugger.register('ESFile.init ' + prefixFile);
        this.prefixFile = prefixFile;
    },
	//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	/* Agregado el parámetro sProjectFolder para concatenar la subcarpeta indicada en este punto al ESFile.prefixFile, ya que con la actualización de múltiples passwords, automáticamente se
	usará la carpeta del proyecto definido para el usuario (objSettings.project) y sólo mediante este parámetro se podría forzar a una ruta diferente o incluso al root de prefixFile
	para casos de archivos como el de auto-login que se seguirían leyendo desde el path de trabajo de versiones anteriores que era precisamente el root. Este parámetro no requiere
	la "/" final ya que se le concatenará en este punto
	*/
    deleteFile : function (filename, success, fail, sProjectFolder) {
		Debugger.register('ESFile.deleteFile ' + filename);
    	this.lastFileName = filename;
		//En Modo Web no hay borrado de archivos locales
		if (objSettings && objSettings.webMode) {
			fileWorker.postMessage({cmd: 'deleteFile', fileName: filename, successFn: success, errorFn: fail});
			return;
		}
		
		success = getFunctionRefFromName(success);
		if (!success) {
			success = noCallbackSuccess;
		}
		fail = getFunctionRefFromName(fail);
		if (!fail) {
			fail = noCallbackError;
		}
		
		//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		sProjectFolder = ESFile.getRootFilesPath(sProjectFolder);
		filename = this.prefixFile + sProjectFolder + filename;
		//JAPR
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, 
            function gotFS(filesystem) {
                filesystem.root.getFile(filename, null, 
                    function gotFileEntry(fileEntry) {
                        fileEntry.remove(success,fail);
                    }, 
                    function fail2(err) {
                        fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to find the file: ' + filename});
                    });
            }, 
            function fail3(err) {
				fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get a fileSystem object'});
            });
    },
	//JAPR 2019-03-28: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
	/* Elimia el directorio especificado. Si se indica la bandera bRecursively se eliminará junto a todos los archivos y directorios que contenga
	*/
	deleteDir : function (dirpath, success, fail, bRecursively) {
		Debugger.register('ESFile.deleteDir ' + dirpath + ', bRecursively == ' + bRecursively);
    	
		//En Modo Web no hay borrado de archivos locales
		if (objSettings && objSettings.webMode) {
			fileWorker.postMessage({cmd: 'deleteDir', dirpath: dirpath, successFn: success, errorFn: fail});
			return;
		}
		
		success = getFunctionRefFromName(success);
		if (!success) {
			success = noCallbackSuccess;
		}
		fail = getFunctionRefFromName(fail);
		if (!fail) {
			fail = noCallbackError;
		}
		
		if (objSettings.webMode) {
			//Por lo pronto en el browser se usará almacenamiento temporal
			var intPERSISTENT = window.TEMPORARY;
		}
		else {
			var intPERSISTENT = LocalFileSystem.PERSISTENT;
		}
		//JAPR 2018-11-14: Corregido un bug con la actualización de Cordova, ahora el fileSystem por default utiliaba los paths en la memoria interna en lugar del SDCard (#DP45ID)
		//blnExistsFileSystem(intPERSISTENT, 0, function gotFS(filesystem) {
		//window.resolveLocalFileSystemURL(objSettings.prefixTrans, 
		ESFile.resolveLocalFileSystemURL(objSettings.prefixTrans, 
		function gotFS(filesystem) {
			//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
			var directoryentry = filesystem.root.getDirectory(ESFile.prefixFile + dirpath, {create: false, exclusive: false}, 
				function(entry) {
					if ( bRecursively ) {
						entry.removeRecursively(success, fail);
					}
					else {
						entry.remove(success, fail);
					}
				}, 
				function fail2(err) {
					//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
					fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory object for: ' + ESFile.prefixFile + dirpath});
				});
		}, 
		function fail3(err) {
			fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the fileSystem object'});
		});
	},
	//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	/* Agregado el parámetro sProjectFolder para concatenar la subcarpeta indicada en este punto al ESFile.prefixFile, ya que con la actualización de múltiples passwords, automáticamente se
	usará la carpeta del proyecto definido para el usuario (objSettings.project) y sólo mediante este parámetro se podría forzar a una ruta diferente o incluso al root de prefixFile
	para casos de archivos como el de auto-login que se seguirían leyendo desde el path de trabajo de versiones anteriores que era precisamente el root. Este parámetro no requiere
	la "/" final ya que se le concatenará en este punto
	*/
    writeFile : function (filename, text, success, fail, params, sProjectFolder) {
		Debugger.register('ESFile.writeFile ' + filename);
    	this.lastFileName = filename;
		//En Modo Web no hay grabado de archivos locales, pero se simulará con la utilería para dejar los mismos archivos que en el App
		if (objSettings && objSettings.webMode) {
			fileWorker.postMessage({cmd: 'writeFile', fileName: filename, successFn: success, errorFn: fail, msg: text});
			return;
		}
		
		var successFn = success;
		success = getFunctionRefFromName(success);
		if (!success) {
			success = noCallbackSuccess;
		}
		var failFn = fail;
		fail = getFunctionRefFromName(fail);
		if (!fail) {
			fail = noCallbackError;
		}
		
		//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		sProjectFolder = ESFile.getRootFilesPath(sProjectFolder);
		filename = this.prefixFile + sProjectFolder + filename;
		//JAPR
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, 
            function gotFS(filesystem) {
                filesystem.root.getFile(filename, null, 
                    function gotFileEntry(fileEntry) {
                        fileEntry.remove(
                        	function() {
		                    	Debugger.message('Overwriting file ' + filename);
								//Despues de borrar manda a escribir directamente
								//JAPR 2014-04-30: Corregido un bug, no estaba enviando el parámetro para Append
                        		ESFile.writeFileDirect(filename, text, successFn, failFn, params)
                        	}, fail);
                    }, 
                    function fail2(err) {
                    	Debugger.message('Writing new file ' + filename);
						//Si no encontró el archivo no hay problema, simplemente lo manda a escribir
						//JAPR 2014-04-30: Corregido un bug, no estaba enviando el parámetro para Append
                		ESFile.writeFileDirect(filename, text, successFn, failFn, params)
                    });
            }, 
            function fail3(err) {
				fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get a fileSystem object'});
            });
    },
    /* Agregada porque en Android por alguna razón no estaba truncando el contenido del archivo cuando el contenido era mas pequeño, ni siquiera
    usando la función truncate, así que ahora se eliminará primero el archivo y posterior a eso se invocará al grabado con este método cuando
    se invoque un writeFile
	//JAPR 2015-08-05: Estandarizadas las variables independientemente del operativo utilizado
	//Esta función asume que filename ya tiene la ruta de archivo completa según el dispositivo, así que no es necesario concatenar nada mas
    */
    writeFileDirect : function (filename, text, success, fail, params) {
		Debugger.register('ESFile.writeFileDirect ' + filename);
    	this.lastFileName = filename;
		//En Modo Web no hay grabado de archivos locales, pero se simulará con la utilería para dejar los mismos archivos que en el App
		if (objSettings && objSettings.webMode) {
			fileWorker.postMessage({cmd: 'writeFile', fileName: filename, successFn: success, errorFn: fail, msg: text});
			return;
		}
		
		success = getFunctionRefFromName(success);
		if (!success) {
			success = noCallbackSuccess;
		}
		fail = getFunctionRefFromName(fail);
		if (!fail) {
			fail = noCallbackError;
		}
		
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, 
            function gotFS(filesystem) {
                filesystem.root.getFile(filename, {
                    'create':true,
                    'exclusive':false
                }, 
                function gotFileEntry(fileEntry) {
                    fileEntry.createWriter(
                        function gotFileWriter(writer) {
                            writer.onwrite = success;
                            writer.onerror = fail;
                            //JAPR 2012-09-15: Usado el objeto de parámetros
                            if (params && params.append) {
                            	Debugger.message('File append ' + writer.length);
                            	//Si se especificó que se quería agregar al final, se posiciona en el último caracter antes de escribir
                            	writer.seek(writer.length);
                            }
                            
                            //JAPR 2014-08-27: Modificado el funcionamiento de este código, ya que tanto el write como el truncate aplican los mismos
                            //callbacks previamente configurados, de hecho se tendría que invocar al truncate hasta que se termine de hacer el write
                            //y no como está aquí, sin embargo el código funciona porque pese a que el truncate falla en todos los casos, cuando se hace
                            //un truncate se debe estar omitiendo el parámetro append (y por tanto se invoca directo a writeFileDirect en lugar de invocar
                            //a writeFile), por lo que primero dentro de writeFile se hace un borrado del archivo, así que es realmente inncesario el
                            //truncate ya que queda vacio. Este código se adaptará eventualmente a la modificación realizada para el app de CheckList,
                            //la cual hace el uso correcto de los callbacks y se invoca al truncate o append según los parámetros sin hacer un borrado
                            //previo y por tanto sin importar por cual de las dos funciones entró
                            //Por lo pronto, simplemente se va a remover la parte del truncate debido a lo explicado arriba, si se requiere dejar sólo
                            //el contenido recien escrito, se debe invocar a writeFile, si se desea agregar al final del archivo existente entonces se
                            //debe invocar a writeFileDirect con el parámetro append activado
                            writer.write(text);
                            
                            /*
                            //JAPR 2012-09-15: Usado el objeto de parámetros
                            if (!params || params.truncate) {
                            	Debugger.message('File truncate ' + text.length);
                            	//Si se especificó que se quería eliminar el contenido previo, hace una limpieza completa (por default se
                            	//sobreescribirán completamente los archivos, ya que en un caso graban del LocalStorage con menos contenido se
                            	//notó que se corrompía el JSon guardado porque no limpiaba el contenido de los drafts, tal vez para fotos que
                            	//siempre se crean nuevas no es un problema, pero para archivos de definiciones o drafts se requiere limpieza previa
                            	writer.truncate(text.length);
                            }
                            */
                            //JAPR
                        }, 
                        function fail1(err){
	                        fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to write the file data for: ' + filename});
                        });
                }, 
                function fail2(err) {
                    fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to find the file: ' + filename});
                    });
            }, 
            function fail3(err) {
				fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get a fileSystem object'});
            });
	},
	//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	/* Agregado el parámetro sProjectFolder para concatenar la subcarpeta indicada en este punto al ESFile.prefixFile, ya que con la actualización de múltiples passwords, automáticamente se
	usará la carpeta del proyecto definido para el usuario (objSettings.project) y sólo mediante este parámetro se podría forzar a una ruta diferente o incluso al root de prefixFile
	para casos de archivos como el de auto-login que se seguirían leyendo desde el path de trabajo de versiones anteriores que era precisamente el root. Este parámetro no requiere
	la "/" final ya que se le concatenará en este punto
	*/
    readFile : function(filename, success, fail, sProjectFolder) {
		Debugger.register('ESFile.readFile ' + filename);
    	this.lastFileName = filename;
		//En Modo Web no hay lectura de archivos locales
		if (objSettings && objSettings.webMode) {
			fileWorker.postMessage({cmd: 'readFile', fileName: filename, successFn: success, errorFn: fail});
			return;
		}
		
		success = getFunctionRefFromName(success);
		if (!success) {
			success = noCallbackSuccess;
		}
		fail = getFunctionRefFromName(fail);
		if (!fail) {
			fail = noCallbackError;
		}
		//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		sProjectFolder = ESFile.getRootFilesPath(sProjectFolder);
		filename = this.prefixFile + sProjectFolder + filename;
		//JAPR
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, 
            function gotFS(filesystem) {
                filesystem.root.getFile(filename, null, 
                    function gotFileEntry(fileEntry) {
                        fileEntry.file(
                            function gotFile(file) {
                                var reader = new FileReader();
                                reader.onload = success;
                                reader.readAsText(file);
                            }, 
                            function fail1(err){
		                        fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to read the file data for: ' + filename});
                            });
                    }, 
                    function fail2(err) {
                        fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to find the file: ' + filename});
                    });
            }, 
            function fail3(err) {
				fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get a fileSystem object'});
            });
    },
    /* Lee la lista de archivos del directorio raiz comparandolos con el patrón especificado y regresa un array con todos los nombres que coinciden
    a través de la función de callback success especificada (para webMode simplemente invoca a dicha función en forma asíncrona regresando un
    array vacio. El parámetro bAddToAppArray indica que se deben agregar los archivos leídos al array del objApp, además que utilizará un método
    de busqueda extensivo porque en teoría no basta con un solo readEntries para recuperar todos los objetos
	//JAPR 2014-09-09: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
    Agregado el parámetro bReadImageFiles para indicar que al terminar de leer los archivos de la raiz, deberá leer los archivos dentro de la
    carpeta images\fbm_bmd_#### de la cuenta logeada (objSettings.project) y hasta terminar dicho proceso deberá ejecutar el callback
    */
	//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	/* Agregado el parámetro sProjectFolder para concatenar la subcarpeta indicada en este punto al ESFile.prefixFile, ya que con la actualización de múltiples passwords, automáticamente se
	usará la carpeta del proyecto definido para el usuario (objSettings.project) y sólo mediante este parámetro se podría forzar a una ruta diferente o incluso al root de prefixFile
	para casos de archivos como el de auto-login que se seguirían leyendo desde el path de trabajo de versiones anteriores que era precisamente el root. Este parámetro no requiere
	la "/" final ya que se le concatenará en este punto
	*/
    getFolderFiles: function(aPattern, success, fail, bAddToAppArray, bReadImageFiles, sProjectFolder) {
		Debugger.register('ESFile.getFolderFiles bAddToAppArray == ' + bAddToAppArray + ', bReadImageFiles == ' + bReadImageFiles);
    	var blnExistsFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
		if (objSettings && objSettings.webMode) {
			//Si es modo Web y no existe forma de accesar a archivos, simplemente hace un readFile
			fileWorker.postMessage({cmd: 'getFolderFiles', successFn: success, errorFn: fail});
			return;
		}
		else {
			success = getFunctionRefFromName(success);
			if (!success) {
				success = noCallbackSuccess;
			}
			fail = getFunctionRefFromName(fail);
			if (!fail) {
				fail = noCallbackError;
			}
			
			//Si es un app o se soporta acceso a archivos en el browser, lee todas las entradas en forma asíncrona y verifica que cumplan con
			//el patrón especificado (si no se especifica un string se asume que se quieren todas)
			//aPattern puede ser un String en cuyo caso se crea una expresión regular para buscarlo, o bien puede ser un objeto en cuyo caso
			//se asume que ya es una expresión regular
			if (!aPattern) {
				aPattern = '';
			}
			if ((typeof aPattern) == "string") {
				if (!aPattern) {
					//Si viene una cadena vacia se asume que se quieren obtener todos los archivos así que especifica null
					aPattern = null;
				}
				else {
					//Si viene cualquier otro string se asume que se busca dicho archivo (o parte del mismo sin importar donde)
					aPattern = new RegExp(RegExp.quote(aPattern), "gi");
				}
			}
			
			//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
			sProjectFolder = ESFile.getRootFilesPath(sProjectFolder);
			//JAPR
			
			if (objSettings.webMode) {
				//Por lo pronto en el browser se usará almacenamiento temporal
				var intPERSISTENT = window.TEMPORARY;
			}
			else {
				var intPERSISTENT = LocalFileSystem.PERSISTENT;
			}
            blnExistsFileSystem(intPERSISTENT, 0, function gotFS(filesystem) {
				//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
				var directoryentry = filesystem.root.getDirectory(ESFile.prefixFile + sProjectFolder, {create: true, exclusive: false}, 
					function(directoryentry) {
						var directoryReader = directoryentry.createReader();
						
						if (bAddToAppArray) {
							//En este caso se agregan los archivos leídos al array del objApp
							//No se puede garantizar que una sola llamada a readEntries regresará todos los archivos, así que se invoca recursivamente
							//hasta que ya no se regresan mas, para eso se crea la variable fnReadEntries
							var fnReadEntries = function() {
								directoryReader.readEntries(function (entries) {
									var intEntriesLength = entries.length;
									if (!intEntriesLength) {
										//Ya no se encontraron mas archivos, así que ejecuta el callback de success
										//JAPR 2014-09-09: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
										if (bReadImageFiles && ESFile) {
											try {
												ESFile.getImagesFolderFiles(aPattern, success, fail, bAddToAppArray);
											} catch(e) {
												//En caso de error se invoca al callback de fallo
												fail({errCode:-1, errDesc: 'Error reading images folder: ' + e});
											}
										}
										else {
											success();
										}
										//JAPR
									}
									else {
										//Se encontraron mas archivos, se invoca al método que los agrega a la lista pero además se invoca a la lectura
										//del resto de los archivos
										if (objApp && objApp.addFilesRead) {
											objApp.addFilesRead(entries);
										}
										
										//JAPRWarning: Se había leído en la red que se tenía que llamar recursivamente al readEntries, pero al
										//probar en Android provocó un ciclo infinito así que posiblemente sólo aplique para un Browser. Se hará
										//sólo una llamada hasta que se compruebe que hay problemas por hacerlo así en móviles
										//fnReadEntries();
										//JAPR 2014-09-09: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
										if (bReadImageFiles && ESFile) {
											try {
												ESFile.getImagesFolderFiles(aPattern, success, fail, bAddToAppArray);
											} catch(e) {
												//En caso de error se invoca al callback de fallo
												fail({errCode:-1, errDesc: 'Error reading images folder: ' + e});
											}
										}
										else {
											success();
										}
										//JAPR
									}
								}, 
								function fail1(err) {
									//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
									fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory list for: ' + ESFile.prefixFile + sProjectFolder});
								});
							};
							
							//Inicia la lectura de archivos
							fnReadEntries();
						}
						else {
							//Se usa el método original y simplemente se llama a success con la primer ocurrencia de lectura de archivos
							directoryReader.readEntries(function (entries) {
								var arrFiles = new Array();
								var intEntriesLength = entries.length;
								var intLength = 1;
								for (var intIndex = 0; intIndex < intEntriesLength; intIndex++) {
									entry = entries[intIndex];
									if (!entry.isDirectory) {
										var strFile = entry.name;
										if (aPattern) {
											//Busca el patrón especificado y si hay por lo menos una ocurrencia considera válido el nombre de archivo
											var arrMatch = strFile.match(aPattern);
											intLength = (arrMatch === null)?0:arrMatch.length;
										}
										else {
											//Si no hay un patrón, como ya se asignó una longitud cualquier nombre de archivo será regresado
										}
										
										if (intLength > 0) {
											arrFiles.push(strFile);
										}
									}
								}
								
								//Invoca a la función de Callback correspondiente
								//JAPR 2014-09-09: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
								//En este caso no se puede invocar en secuencia a la función getImagesFolderFiles, ya que se necesita enviar el array
								//con los archivos leídos como parte del callback, así que en esta situación quien invocó deberá continuar con la
								//ejecución de dicha función posteriormente
								success(arrFiles);
							}, 
							function fail1(err) {
								//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
								fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory list for: ' + ESFile.prefixFile + sProjectFolder});
							});
						}
					}, 
					function fail2(err) {
						//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
						fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory object for: ' + ESFile.prefixFile + sProjectFolder});
					});
            }, 
            function fail3(err) {
                fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the fileSystem object'});
            });
		}
    },
	//JAPR 2014-09-09: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
    /* Lee la lista de archivos de imagenes del directorio images\fbm_bmd_#### (objSettings.project) y regresa un array con todos los nombres 
    a través de la función de callback success especificada (para webMode simplemente invoca a dicha función en forma asíncrona regresando un
    array vacio. El parámetro bAddToAppArray indica que se deben agregar los archivos leídos al array del objApp
    Esta función por el momento no hará uso del parámetro aPattern, ya que todo archivo dentro de la carpeta indicada se considera archivo de imagen,
    pero se dejará el parámetro por consistencia con la función ofiginal getFolderFiles
    */
    getImagesFolderFiles: function(aPattern, success, fail, bAddToAppArray) {
		Debugger.register('ESFile.getImagesFolderFiles bAddToAppArray == ' + bAddToAppArray);
    	var blnExistsFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
		if (objSettings && objSettings.webMode) {
			//Si es modo Web y no existe forma de accesar a archivos, simplemente hace un readFile
			fileWorker.postMessage({cmd: 'getImagesFolderFiles', successFn: success, errorFn: fail});
			return;
		}
		else {
			success = getFunctionRefFromName(success);
			if (!success) {
				success = noCallbackSuccess;
			}
			fail = getFunctionRefFromName(fail);
			if (!fail) {
				fail = noCallbackError;
			}
			
			//Si es un app o se soporta acceso a archivos en el browser, lee todas las entradas en forma asíncrona y verifica que cumplan con
			//el patrón especificado (si no se especifica un string se asume que se quieren todas)
			//aPattern puede ser un String en cuyo caso se crea una expresión regular para buscarlo, o bien puede ser un objeto en cuyo caso
			//se asume que ya es una expresión regular
			if (!aPattern) {
				aPattern = '';
			}
			if ((typeof aPattern) == "string") {
				if (!aPattern) {
					//Si viene una cadena vacia se asume que se quieren obtener todos los archivos así que especifica null
					aPattern = null;
				}
				else {
					//Si viene cualquier otro string se asume que se busca dicho archivo (o parte del mismo sin importar donde)
					aPattern = new RegExp(RegExp.quote(aPattern), "gi");
				}
			}
			
			if (objSettings.webMode) {
				//Por lo pronto en el browser se usará almacenamiento temporal
				var intPERSISTENT = window.TEMPORARY;
			}
			else {
				var intPERSISTENT = LocalFileSystem.PERSISTENT;
			}
        	Debugger.message('getImagesFolderFiles before getting FileSystem');
            blnExistsFileSystem(intPERSISTENT, 0, function gotFS(filesystem) {
				Debugger.message('getImagesFolderFiles FileSystem got');
            	//Esta función siempre lee de la carpeta de imagenes para el projecto asociado a la cuenta logeada
            	var strDirectory = ESFile.prefixFile+"images/";
            	if (objSettings && objSettings.project) {
            		strDirectory += objSettings.project+"/";
            	}
				
            	Debugger.message('getImagesFolderFiles path: ' + strDirectory);
				var directoryentry = filesystem.root.getDirectory(strDirectory, {create: true, exclusive: false}, 
					function(directoryentry) {
	                    var directoryReader = directoryentry.createReader();
	                    if (bAddToAppArray) {
	                    	//En este caso se agregan los archivos leídos al array del objApp
		                    //No se puede garantizar que una sola llamada a readEntries regresará todos los archivos, así que se invoca recursivamente
		                    //hasta que ya no se regresan mas, para eso se crea la variable fnReadEntries
							var fnReadEntries = function() { 
								directoryReader.readEntries(function (entries) { 
			                        var intEntriesLength = entries.length; 
			                    	if (!intEntriesLength) {
			                    		//Ya no se encontraron mas archivos, así que ejecuta el callback de success
				                        success();
			                    	}
			                    	else {
			                    		//Se encontraron mas archivos, se invoca al método que los agrega a la lista pero además se invoca a la lectura
			                    		//del resto de los archivos
										//JAPR 2015-11-25: Corregido un bug, estaba preguntando por la función addFilesRead en lugar de addImagesRead, si bien no causaba problema porque
										//ambas funciones existen en ese objeto y por tanto hubiera entrado de todas maneras, no estaba correcta esa validación
										if (objApp && objApp.addImagesRead) { 
			                    			objApp.addImagesRead(entries);
			                    		}
										
			                    		//JAPRWarning: Se había leído en la red que se tenía que llamar recursivamente al readEntries, pero al
			                    		//probar en Android provocó un ciclo infinito así que posiblemente sólo aplique para un Browser. Se hará
			                    		//sólo una llamada hasta que se compruebe que hay problemas por hacerlo así en móviles
			                    		//fnReadEntries();
			                    		success();
			                    	}
			                    }, 
								function fail1(err) {
					                fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory list for: ' + strDirectory});
					            });
		                    };
		                    
		                    //Inicia la lectura de archivos
		                    fnReadEntries();
	                    }
						else { 
	                    	//Se usa el método original y simplemente se llama a success con la primer ocurrencia de lectura de archivos
							directoryReader.readEntries(function (entries) { 
		                        var arrFiles = new Array();
		                        var intEntriesLength = entries.length;
	                        	var intLength = 1;
		                        for (var intIndex = 0; intIndex < intEntriesLength; intIndex++) {
									entry = entries[intIndex];
		                        	if (!entry.isDirectory) {
			                            var strFile = entry.name;
			                            if (aPattern) {
			                            	//Busca el patrón especificado y si hay por lo menos una ocurrencia considera válido el nombre de archivo
			                        		var arrMatch = strFile.match(aPattern);
			                        		intLength = (arrMatch === null)?0:arrMatch.length;
			                            }
			                            else {
			                            	//Si no hay un patrón, como ya se asignó una longitud cualquier nombre de archivo será regresado
			                            }
			                            
			                            if (intLength > 0) {
			                            	arrFiles.push(strFile);
			                            }
		                        	}
		                        }
		                        
		                        //Invoca a la función de Callback correspondiente
		                        success(arrFiles);
		                    }, 
							function fail1(err) { 
								fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory list for: ' + strDirectory});
				            });
	                    }
	                }, 
					function fail2(err) {
		                fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory object for: ' + strDirectory});
		            });
            }, 
			function fail3(err) {
                fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the fileSystem object'});
            });
		}
    },
	/* Lee la lista de subdirectorios del directorio de modelos y regresa un array con todos los modelos identificados incluyendo número de versión
	a través de la función de callback success especificada (para webMode simplemente invoca a dicha función en forma asíncrona regresando un
	array vacio).
	Este método se invoca después de leer los demás tipos de archivos y no se encadena con ningún otro método, por lo cual no son necesarios parámetros adicionales, pero
	eventualmente pudiera ser necesario si se agregan mas tipos de archivos que deban ser leídos desde startRefreshDefinitionsProcess, getNewDefinitionsVersion o 
	saveDefinitionVersionsBeforeDeleteque son los procesos que hacen uso de este parámetro (se dejará el parámetro bReadNextFiles para tal situación futura)
	El parámetro sProjectFolder funcionará como en los casos de las otras funciones similares a esta
	El parámetro aPattern no tiene uso en esta función, sin embargo se dejara el parámetro por consistencia de definición con otras funciones similares
	*/
	getFolderModels: function(aPattern, success, fail, bAddToAppArray, bReadNextFiles, sProjectFolder) {
		Debugger.register('ESFile.getFolderModels bAddToAppArray == ' + bAddToAppArray + ', bReadNextFiles == ' + bReadNextFiles);
		//JAPR 2018-11-14: Corregido un bug con la actualización de Cordova, ahora el fileSystem por default utiliaba los paths en la memoria interna en lugar del SDCard (#DP45ID)
		//var blnExistsFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
		if (objSettings && objSettings.webMode) {
			//Si es modo Web y no existe forma de accesar a archivos, simplemente hace un readFile
			fileWorker.postMessage({cmd: 'getFolderModels', successFn: success, errorFn: fail});
			return;
		}
		else {
			success = getFunctionRefFromName(success);
			if (!success) {
				success = noCallbackSuccess;
			}
			fail = getFunctionRefFromName(fail);
			if (!fail) {
				fail = noCallbackError;
			}
			
			//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
			sProjectFolder = ESFile.getRootFilesPath(sProjectFolder);
			//JAPR
			
			if (objSettings.webMode) {
				//Por lo pronto en el browser se usará almacenamiento temporal
				var intPERSISTENT = window.TEMPORARY;
			}
			else {
				var intPERSISTENT = LocalFileSystem.PERSISTENT;
			}
			//JAPR 2018-11-14: Corregido un bug con la actualización de Cordova, ahora el fileSystem por default utiliaba los paths en la memoria interna en lugar del SDCard (#DP45ID)
			//blnExistsFileSystem(intPERSISTENT, 0, function gotFS(filesystem) {
			//window.resolveLocalFileSystemURL(objSettings.prefixTrans, 
			ESFile.resolveLocalFileSystemURL(objSettings.prefixTrans, 
			function gotFS(filesystem) {
				//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
				var directoryentry = filesystem.root.getDirectory(ESFile.prefixFile + sProjectFolder + "models", {create: true, exclusive: false}, 
					function(direntry) {
						//direntry representa el path hacia la carpeta de modelos, se necesitará crear un nuevo directoryEntry para navegar por cada carpeta de catálogo
						var directoryReader = direntry.createReader();
						
						//Se usa el método original y simplemente se llama a success con la primer ocurrencia de lectura de archivos
						directoryReader.readEntries(function (entries) {
							var arrCatalogFolders = new Array();
							var objLocalModels = new Object();
							var intEntriesLength = entries.length;
							for (var intIndex = 0; intIndex < intEntriesLength; intIndex++) {
								entry = entries[intIndex];
								if (entry.isDirectory) {
									var strFile = entry.name;
									
									//La estructura de subcarpetas de modelos es la siguiente: catalog_#/modelName_versionNum
									//donde catalog realmente es el DataSourceID, NO confundir con instancias específicas de CatalogCls, ya que cada CatalogCls apunta finalmente a un único
									//DataSource por lo que todos los valores del los Catalog del mismo DataSource serían técnicamente los mismos modelos si coinciden en el resto de la
									//estructura de subcarpetas
									
									//Al finalizar el proceso se tiene un array con folders que debe ser recorrido recursivamente hasta eliminar todo sus paths, 
									arrCatalogFolders.push(strFile);
								}
							}
							
							//Prepara una función para procesar recursivamente la colección de carpetas de catálogos identificadas, ya que se deberán leer los archivos de cada una
							//y no se puede hacer en un solo paso, usando el array local se irá procesando cada uno hasta que éste se encuentre vacío
							var fnReadModelsFromCatalogFolder = function() {
								var strCatalogFolder = arrCatalogFolders.shift();
								if ( !strCatalogFolder ) {
									//Cuando ya no existan mas carpetas que procesar terminará ejecutando el callback con los modelos leídos
									_.delay(function() {
										if ( bAddToAppArray ) {
											success();
										}
										else {
											success(objLocalModels);
										}
									}, 50);
									return;
								}
								
								//Lee los archivos de modelos de esta carpeta de catalogo
								Debugger.register('fnReadModelsFromCatalogFolder ' + strCatalogFolder);
								
								var arrFileParts = strCatalogFolder.split('_'), intCatalogID = parseInt(arrFileParts[arrFileParts.length -1]);
								var catDirEntry = filesystem.root.getDirectory(ESFile.prefixFile + sProjectFolder + "models/" + strCatalogFolder, {create: true, exclusive: false}, 
									function(catdirectoryentry) {
										//catdirectoryentry representa el path hacia la carpeta de uno de los catálogos que contienen modelos
										var catdirectoryReader = catdirectoryentry.createReader();
										catdirectoryReader.readEntries(function (entries) {
											var intEntriesLength = entries.length;
											for (var intIndex = 0; intIndex < intEntriesLength; intIndex++) {
												entry = entries[intIndex];
												if (entry.isDirectory) {
													var strFile = entry.name;
													
													//La estructura de subcarpetas de modelos es la siguiente: catalog_#/modelName_versionNum
													//donde catalog realmente es el DataSourceID, NO confundir con instancias específicas de CatalogCls, ya que cada CatalogCls apunta finalmente a un único
													//DataSource por lo que todos los valores del los Catalog del mismo DataSource serían técnicamente los mismos modelos si coinciden en el resto de la
													//estructura de subcarpetas
													if ( strFile ) {
														var arrFileParts = strFile.split('_');
														var strModelName = arrFileParts[0] || '', intModelVersion = arrFileParts[arrFileParts.length -1] || 0;
														if ( strModelName && intModelVersion ) {
															var objModelData = {name: strModelName, version: intModelVersion, dataSourceID: intCatalogID,
																key: strCatalogFolder + "/" + strFile, path: ESFile.prefixFile + sProjectFolder + "models/" + strCatalogFolder + "/" + strFile};
															if ( bAddToAppArray ) {
																objApp.modelsDef[objModelData.key] = objModelData;
																//Agrega el modelo indexado por su nombre para identificar fácilmente la versión previa
																objApp.modelsDefByName['catalog_' + objModelData.dataSourceID + "/" + objModelData.name] = objModelData.version;
															}
															else {
																objLocalModels[objModelData.key] = objModelData;
															}
														}
													}
												}
											}
											
											//Al terminar de procesar todos los modelos invoca al callback para terminar, de lo constrario se invoca recursivamente para continuar con el
											//siguiente catálogo
											if ( arrCatalogFolders.length ) {
												_.delay(fnReadModelsFromCatalogFolder, 50);
											}
											else {
												if ( bAddToAppArray ) {
													success();
												}
												else {
													success(objLocalModels);
												}
											}
										}, 
										function failCDR(err) {
											//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
											failCDR({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory list for: ' + ESFile.prefixFile + sProjectFolder + "models/" + strCatalogFolder});
										});
									},
									function failF(err) {
										//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
										failF({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory object for: ' + ESFile.prefixFile + sProjectFolder + "models/" + strCatalogFolder});
									});
							}
							
							//Inicia el proceso de ejecución de la lectura de modelos, en caso de no haber ninguno simplemente terminará tan pronto de ejecute la función
							_.delay(fnReadModelsFromCatalogFolder, 50);
						}, 
						function fail1(err) {
							//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
							fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory list for: ' + ESFile.prefixFile + sProjectFolder});
						});
					}, 
					function fail2(err) {
						//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
						fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory object for: ' + ESFile.prefixFile + sProjectFolder});
					});
			}, 
			function fail3(err) {
				fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the fileSystem object'});
			});
		}
	},
    /* Lee la lista de archivos del directorio raiz comparandolos con el patrón especificado elimina cada archivo encontrado. No utiliza las 
    funciones de callback a menos que sea al intentar leer el directorio, pero no por intentar eliminar los archivos
    */
	//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	/* Agregado el parámetro sProjectFolder para concatenar la subcarpeta indicada en este punto al ESFile.prefixFile, ya que con la actualización de múltiples passwords, automáticamente se
	usará la carpeta del proyecto definido para el usuario (objSettings.project) y sólo mediante este parámetro se podría forzar a una ruta diferente o incluso al root de prefixFile
	para casos de archivos como el de auto-login que se seguirían leyendo desde el path de trabajo de versiones anteriores que era precisamente el root. Este parámetro no requiere
	la "/" final ya que se le concatenará en este punto
	*/
    deleteFilesByPattern: function(aPattern, success, fail, sProjectFolder) {
		Debugger.register('ESFile.deleteFilesByPattern');
    	var blnExistsFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
		if (objSettings && objSettings.webMode) {
			//Si es modo Web y no existe forma de accesar a archivos, simplemente hace un readFile
			//ESFile.readFile('', success, fail);
			fileWorker.postMessage({cmd: 'deleteFilesByPattern', successFn: success, errorFn: fail});
			return;
		}
		else {
			success = getFunctionRefFromName(success);
			if (!success) {
				success = noCallbackSuccess;
			}
			fail = getFunctionRefFromName(fail);
			if (!fail) {
				fail = noCallbackError;
			}

			//Si es un app o se soporta acceso a archivos en el browser, lee todas las entradas en forma asíncrona y verifica que cumplan con
			//el patrón especificado (si no se especifica un string se asume que se quieren todas)
			//aPattern puede ser un String en cuyo caso se crea una expresión regular para buscarlo, o bien puede ser un objeto en cuyo caso
			//se asume que ya es una expresión regular
			if (!aPattern) {
				aPattern = '';
			}
			if ((typeof aPattern) == "string") {
				if (!aPattern) {
					//Si viene una cadena vacia se asume que se quieren obtener todos los archivos así que especifica null
					aPattern = null;
				}
				else {
					//Si viene cualquier otro string se asume que se busca dicho archivo (o parte del mismo sin importar donde)
					aPattern = new RegExp(RegExp.quote(aPattern), "gi");
				}
			}
			
			//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
			sProjectFolder = ESFile.getRootFilesPath(sProjectFolder);
			//JAPR
			
			if (objSettings.webMode) {
				//Por lo pronto en el browser se usará almacenamiento temporal
				var intPERSISTENT = window.TEMPORARY;
			}
			else {
				var intPERSISTENT = LocalFileSystem.PERSISTENT;
			}
            blnExistsFileSystem(intPERSISTENT, 0, function gotFS(filesystem) {
				//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
				var directoryentry = filesystem.root.getDirectory(ESFile.prefixFile + sProjectFolder, {create: true, exclusive: false}, 
	                function(directoryentry) { 
	                    var directoryReader = directoryentry.createReader();
	                    directoryReader.readEntries(function (entries) {
	                        var intEntriesLength = entries.length;
                        	var intLength = 1;
	                        for (var intIndex = 0; intIndex < intEntriesLength; intIndex++) {
								var entry = entries[intIndex];
	                        	if (!entry.isDirectory) {
		                            var strFile = entry.name;
		                            if (aPattern) {
		                            	//Busca el patrón especificado y si hay por lo menos una ocurrencia considera válido el nombre de archivo
		                        		var arrMatch = strFile.match(aPattern);
		                        		intLength = (arrMatch === null)?0:arrMatch.length;
		                            }
		                            else {
		                            	//Si no hay un patrón, como ya se asignó una longitud cualquier nombre de archivo será regresado
		                            }
		                            
		                            if (intLength > 0) {
		                            	//Elimina el archivo identificado pero sin Callback
										//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		                            	ESFile.deleteFile(strFile, undefined, undefined, sProjectFolder);
										//JAPR
		                            }
	                        	}
	                        }
	                        
	                        //Invoca a la función de Callback correspondiente
	                        success();
	                    }, 
			            function fail1(err) {
							//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
			                fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory list for: ' + ESFile.prefixFile + sProjectFolder});
			            });
	                }, 
		            function fail2(err) {
						//JAPR 2016-11-29: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		                fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory object for: ' + ESFile.prefixFile + sProjectFolder});
		            });
            }, 
            function fail3(err) {
                fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the fileSystem object'});
            });
		}
    },
	//JAPR 2016-11-30: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	/* Mueve el archivo especificado (incluyendo el path a partir de la carpeta root de eForms) hacia el directorio especificado (o root si se especifica vacío) a partir de la carpeta
	root de eForms. Utilizado originalmente para el proceso de compatibilidad hacia atrás cuando se implementaron múltiples passwords por usuario, ya que era necesario mover los archivos
	de la carpeta root a la subcarpeta del proyecto correspondiente a la cuenta del usuario logeado
	*/
    moveFile : function(filename, sNewFolder, success, fail, sProjectFolder) {
		Debugger.register('ESFile.moveFile ' + filename + ', sNewFolder == ' + sNewFolder + ', sProjectFolder == ' + sProjectFolder);
		
    	this.lastFileName = filename;
		//En Modo Web no hay lectura de archivos locales
		if (objSettings && objSettings.webMode) {
			fileWorker.postMessage({cmd: 'moveFile', fileName: filename, successFn: success, errorFn: fail});
			return;
		}
		
		success = getFunctionRefFromName(success);
		if (!success) {
			success = noCallbackSuccess;
		}
		fail = getFunctionRefFromName(fail);
		if (!fail) {
			fail = noCallbackError;
		}
		
		sProjectFolder = ESFile.getRootFilesPath(sProjectFolder);
		filename = ESFile.prefixFile + sProjectFolder + filename;
		
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, 
			function gotFS(filesystem) {
				Debugger.message('Getting file: ' + filename);
				filesystem.root.getFile(filename, null, 
					function gotFileEntry(fileEntry) {
						Debugger.message('Getting directory: ' + ESFile.prefixFile + sNewFolder);
						filesystem.root.getDirectory(ESFile.prefixFile + sNewFolder, {create: true, exclusive: false}, 
							function(directoryentry) {
								fileEntry.moveTo(directoryentry, undefined, success, fail);
						}, 
						function fail2(err) {
							Debugger.message('Unable to get the directory object (' + ESFile.prefixFile + sNewFolder + '): ' + JSON.stringify(err));
							fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory object (' + ESFile.prefixFile + sNewFolder + '): ' + JSON.stringify(err)});
						});
					}, 
					function fail2(err) {
						Debugger.message('Unable to find the file (' + filename + '): ' + JSON.stringify(err));
						fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to find the file (' + filename + '): ' + JSON.stringify(err)});
					});
			}, 
			function fail3(err) {
				Debugger.message('Unable to get a fileSystem object: ' + JSON.stringify(err));
				fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get a fileSystem object: ' + JSON.stringify(err)});
			});
    },
	//JAPR 2016-11-30: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	/* Crea la subcarpeta de proyecto que se encuentra configurado en objSettings a partir de la carpeta del App que se está ejecutando, y al terminar tanto con error como sin error,
	continua con el callback especificado en el parámetro según el caso (generalmente un error podría significar simplemente que la carpeta ya se encuentra creada, así que es recomendable
	enviar el mismo callback en ambos casos)
	*/
	createProjectFolder : function(success, fail) {
		Debugger.register('ESFile.createProjectFolder');
		
		//En Modo Web no hay subcarpetas
		if (objSettings && objSettings.webMode) {
			fileWorker.postMessage({cmd: 'createProjectFolder', fileName: '', successFn: success, errorFn: fail});
			return;
		}
		
    	var blnExistsFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
		success = getFunctionRefFromName(success);
		if (!success) {
			success = noCallbackSuccess;
		}
		fail = getFunctionRefFromName(fail);
		if (!fail) {
			fail = noCallbackError;
		}
		
		var strMainDir = "";
		switch(gsProductName){
			case "KPI Online Forms":
				strMainDir = "KPIOnlineForms6";
			break;
			case "Geocontrol":
				strMainDir = "Geocontrol";
			break;
			default:
				strMainDir = gsProductName;
			break;
		}
		
		var strProjectFolder = '';
		if (objSettings && objSettings.project) {
			strProjectFolder = objSettings.project;
		}
		
		//Si por alguna razón no estuviera asignado el proyecto, no tiene caso continuar con el proceso e invoca al callback de éxito
		if (!strProjectFolder) {
			setTimeout(function () {
				success();
			}, 100);
			return;
		}
		
		var intPERSISTENT = LocalFileSystem.PERSISTENT;
		//Para Android el folder principal ya se encuentra creado desde la función objApp.initMobile, así que sólo obtiene una referencia a dicho folder (ESFile.prefixFile) y crea el subFolder
		//Para IOS el directorio del App es automáticamente generado y exclusivo para el App, por lo que no existe ni riesgo de ser reutilizado por otra App ni necesidad de darle
		//un nombre como tal, simplemente crea la subcarpeta del proyecto correspondiente a partir de la instancia de almacenamiento persistente (ESFile.prefixFile == '' en ese caso)
		blnExistsFileSystem(intPERSISTENT, 0, function gotFS(filesystem) {
			var directoryentry = filesystem.root.getDirectory(ESFile.prefixFile + strProjectFolder, {create: true, exclusive: false}, 
				function(directoryentry) {
					//JAPR 2019-03-25: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
					//Crea la carpeta de modelos por si eventualmente se requiere utilizar
					directoryentry.getDirectory("models", {create: true, exclusive: false}, function(modelsDirEntry) {
						//Se logró crear el folder, invoca al callback de éxito
						setTimeout(function () {
							success();
						}, 100);
					}, function fail4(err) {
						fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the models directory object for: ' + ESFile.prefixFile + sProjectFolder + "/models"});
					});
					//JAPR
				}, 
				function fail2(err) {
					fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the directory object for: ' + ESFile.prefixFile + sProjectFolder});
				});
		}, 
		function fail3(err) {
			fail({errCode: ((err && err.code)?err.code:-1), errDesc: 'Unable to get the fileSystem object'});
		});
	},
	/* Dado un path específico, obtiene la ruta desde la raiz del dispositivo asignada para el App donde se deberán grabar los archivos de definiciones y demás. En versiones anteriores
	la raiz (KPIOnlineFormsV6 o bien el nombre del App) era el lugar, pero a partir de la actualización de multi-passwords se debe utilizar una subcarpeta de proyecto según la cuenta
	que se autenticó, sin embargo se puede forzar a la raiz o a otro path según el parámetro sProjectFolder recibido
	*/
	getRootFilesPath : function(sProjectFolder) {
		//Vacío es un path válido para forzar al root original de trabajo, así que se debe comparar contra undefined para no enviar el parámetro si se quiere accesar siempre al path
		//variable por proyecto al que se hizo login
		if (sProjectFolder === undefined) {
			//En este caso usa el proyecto del usuario si es que está asignado
			if (objSettings && objSettings.project) {
				sProjectFolder = objSettings.project;
			}
		}
		
		//Si el path no es vacío entonces debe concatenar la última "/", de lo contrario ya incluye el separador de prefixFile
		if (sProjectFolder) {
			sProjectFolder += '/';
		}
		
		return sProjectFolder;
	},
	
	//JAPR 2018-11-14: Corregido un bug con la actualización de Cordova, ahora el fileSystem por default utiliaba los paths en la memoria interna en lugar del SDCard (#DP45ID)
	/* Función que obtiene el path del SD Card a utilizar en eForms, invocada debido al cambio de Cordova a 8.1 en el que 
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function success(fileSystem) {
	ya no regresaba un path del SDCard como antes, sino un path de la memoria interna (file:///data/data/com.bitam.eforms/files/files/) y al hacer el cambio a 
		window.resolveLocalFileSystemURL(strPrefixTrans, function(fileSystem){
	ahora el objeto fileSystem regresaba un subobjecto filesystem que era el que contiene al root, no estaba directo como cuando se invocaba a window.requestFileSystem, por tanto esta
	función encapsulará estas llamadas para futuros cambiso en el Cordova y regresará ya directamente el objeto correcto
	//JAPR 2018-11-28: Corregido un bug con la implementación de esta función, se identificó que realmente NO era necesario usar root, de las versiones anteriores que utilizaban
	window.requestFileSystem, por alguna razón utilizaban el objeto "root" para aplicar las funciones de copiado y movimiento y demás, pero en Android usar root NO regresaba la referencia
	a la carpeta de la cual se había pedido la función sino hacia el SDCard, entonces cuando se usa esta función para obtener la referencia a un archivo, será necesario enviar el parámetro
	"useDirect" para indicar que NO se regrese root sino directamente el fileSystem obtenido. Antes de este parámetro, TODAS las llamadas a esta función invariablemente usaban el root y
	funcionaban porque al usarlo, los nombres de archivos y/o carpetas contenían ESFile.prefixFile o prefixTrans que finalmente en Android contienen la referencia a la carpeta del App, así
	que aunque las búsquedas se hacían desde el root, encontraban dicha carpeta y todo funcionaba bien. NO se modificó este comportamiento, utilizar la opción useDirect cuando se requiera
	obtener refencias directas a la carpeta y/o archivos pedidos (se dejó así por posible compatibilidad con IOS donde usar root si pudiera ser necesario)
	*/
	resolveLocalFileSystemURL: function(sPath, oCallBackFn, oErrorFn, oOptions) {
		try {
			window.resolveLocalFileSystemURL(sPath, function(fileSystem) {
				if ( fileSystem ) {
					var objFileSystem = (fileSystem.filesystem && (!oOptions || !oOptions.useDirect))?fileSystem.filesystem:fileSystem;
					oCallBackFn(objFileSystem);
				}
				else {
					oErrorFn({code: -2, errDesc: e});
				}
			}, oErrorFn);
		} catch(e) {
			if ( oErrorFn ) {
				oErrorFn({code: -1, errDesc: e});
			}
		}
	}
	//JAPR
};