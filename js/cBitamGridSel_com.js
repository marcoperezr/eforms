var tempor;
var div_id_visible='none';
var div_id_padre='none';
var div_id_hijo='none';
var div_id_nieto='none';
var tempor_padre;
var bUpdateColor = false;

function show_hide_sel(div_id, ObjClick, sAlign)
{
	if (document.getElementById(div_id).style.display == 'none')
	{
		iDelW = 18;
		// Si el que se va a mostrar es un padre entonces cerrar los hijos, nietos y divs existentes junto con su padre anterior
		if (div_id.search('id_padre') >= 0)
		{
			if (div_id_visible != 'none'){
				document.getElementById(div_id_visible).style.display = 'none';
				change_up_down_image(div_id_visible+'_img_up_down');
				div_id_visible = 'none';
			}
			if (div_id_nieto != 'none'){
				document.getElementById(div_id_nieto).style.display = 'none';
				change_up_down_image(div_id_nieto+'_img_up_down');
				div_id_nieto = 'none';
			}

			if (div_id_hijo != 'none'){
				document.getElementById(div_id_hijo).style.display = 'none';
				change_up_down_image(div_id_hijo+'_img_up_down');
				div_id_hijo = 'none';
			}

			if (div_id_padre != 'none'){
				document.getElementById(div_id_padre).style.display = 'none';
				change_up_down_image(div_id_padre+'_img_up_down');
			}
			div_id_padre = div_id;
		}
		//	Pero si es un hijo entonces revisar si existía algun hijo y/o nieto abierto y cerrarlo entonces
		else
			if (div_id.search('id_div_hijo') >= 0){
				iDelW = 20;
				if (div_id_nieto != 'none'){
					document.getElementById(div_id_nieto).style.display = 'none';
					change_up_down_image(div_id_nieto+'_img_up_down');
					div_id_nieto = 'none';
				}
		 		if (div_id_hijo != 'none'){
					document.getElementById(div_id_hijo).style.display = 'none';
					change_up_down_image(div_id_hijo+'_img_up_down');
		 		}
		 		div_id_hijo = div_id;
			}
			//	Pero si es un hijo entonces revisar si existía algun hijo y/o nieto abierto y cerrarlo entonces
			else
			 	if (div_id.search('id_div_nieto') >= 0){
			 		iDelW = 22;
					if (div_id_nieto != 'none'){
						document.getElementById(div_id_nieto).style.display = 'none';
						change_up_down_image(div_id_nieto+'_img_up_down');
					}
		 			div_id_nieto = div_id;
				}
				//	Pero si lo que se va a mostrar no tiene nada que ver con padre e hijo entonces ocultar padres, hijos y nietos
				else{
					if (div_id_visible != 'none'){
						document.getElementById(div_id_visible).style.display = 'none';
						change_up_down_image(div_id_visible+'_img_up_down');
						div_id_visible = 'none';
					}
					if (div_id_nieto != 'none'){
						document.getElementById(div_id_nieto).style.display = 'none';
						change_up_down_image(div_id_nieto+'_img_up_down');
						div_id_nieto = 'none';
					}

					if (div_id_hijo != 'none'){
						document.getElementById(div_id_hijo).style.display = 'none';
						change_up_down_image(div_id_hijo+'_img_up_down');
						div_id_hijo = 'none';
					}
					if (div_id_padre != 'none'){
						document.getElementById(div_id_padre).style.display = 'none';
						change_up_down_image(div_id_padre+'_img_up_down');
						div_id_padre = 'none';
					}
					div_id_visible = div_id;
				}
		clearTimeout(tempor);
		clearTimeout(tempor_padre);
		pos = getAbsoluteElementPosition(ObjClick);
		//alert("top="+pos.top+" left="+pos.left + " CWidth="+ObjClick.clientHeight);
		document.getElementById(div_id).style.display = 'inline';
		if (sAlign == 'right')
		{
			pLeft = pos.left-iDelW;
		}
		else
		{
					// Pos Img 	- 	Tam Div		+	Tam Img + borde
			if(ObjClick.clientWidth==undefined)
			   pLeft = pos.left-document.getElementById(div_id).clientWidth+(20-iDelW);
			else
			   pLeft = pos.left-document.getElementById(div_id).clientWidth+ObjClick.clientWidth+(20-iDelW);
		}
		if(ObjClick.clientHeight==undefined)
		   document.getElementById(div_id).style.top = (pos.top +1) + 'px';
		else
  	  	   document.getElementById(div_id).style.top = (pos.top + ObjClick.clientHeight+1) + 'px';

  	  	document.getElementById(div_id).style.left = pLeft + 'px';
		change_up_down_image(div_id+'_img_up_down');
		set_Tempor_2000(div_id);
	}
	else{
		// Si lo que se va a ocultar es un padre entonces cerrar los hijos y nietos abiertos si es que tiene
		if (div_id.search('id_padre') >= 0){
			if (div_id_hijo != 'none'){
				document.getElementById(div_id_hijo).style.display = 'none';
				change_up_down_image(div_id_hijo+'_img_up_down');
				div_id_hijo = 'none';
			}
			if (div_id_nieto != 'none'){
				document.getElementById(div_id_nieto).style.display = 'none';
				change_up_down_image(div_id_nieto+'_img_up_down');
				div_id_nieto = 'none';
			}
			div_id_padre = 'none';
		}
		//	Si lo que se oculta es un hijo entonces cerrar los nietos
		else
			if (div_id.search('id_div_hijo') >= 0){
				if (div_id_nieto != 'none'){
					document.getElementById(div_id_nieto).style.display = 'none';
					change_up_down_image(div_id_nieto+'_img_up_down');
					div_id_nieto = 'none';
				}
				div_id_hijo = 'none';
			}
			// Si lo que se oculta es un nieto entonces asignar a none la variable
			else
				if (div_id.search('id_div_nieto') >= 0)
					div_id_nieto = 'none';
				else
					div_id_visible = 'none';
		document.getElementById(div_id).style.display = 'none';
		change_up_down_image(div_id+'_img_up_down');
		clearTimeout(tempor);
		clearTimeout(tempor_padre);
	}
}
function change_up_down_image(id_img_up_down){
	vaux = document.getElementById(id_img_up_down).src;
	if (vaux.search('images/advanced_search.gif') >= 0)
		document.getElementById(id_img_up_down).src = 'images/basic_search2.gif';
	else
		document.getElementById(id_img_up_down).src = 'images/advanced_search.gif';
}
function change_image(src_image, img_selector, hint_sel, input_id, key_input){
	 document.getElementById(img_selector).src   = src_image;
	 document.getElementById(img_selector).title = hint_sel;
	 document.getElementById(input_id).value     = key_input;
	 if (input_id.indexOf('gallery_sel_') == 0)
	 {
		var serie_key = input_id.substring(12);
		switch(key_input)
		{
 			case 'line':
 			case 'lineCurve':
 			case 'lineStep':
 				document.getElementById('tr_id_marker_sel_'+serie_key).style.display  = 'inline';
 				document.getElementById('tr_id_marker_size_'+serie_key).style.display = 'inline';
 				document.getElementById('tr_id_marker_step_'+serie_key).style.display = 'inline';
 				document.getElementById('tr_id_style_sel_'+serie_key).style.display   = 'none';
 				break;
 			case 'scatter':
 			case 'bubble':
 				document.getElementById('tr_id_marker_sel_'+serie_key).style.display  = 'none';
 				document.getElementById('tr_id_marker_size_'+serie_key).style.display = 'none';
 				document.getElementById('tr_id_marker_step_'+serie_key).style.display = 'none';
 				document.getElementById('tr_id_style_sel_'+serie_key).style.display   = 'none';
 				break;
 			case 'bar':
 			case 'gantt':
 				document.getElementById('tr_id_marker_sel_'+serie_key).style.display  = 'none';
 				document.getElementById('tr_id_marker_size_'+serie_key).style.display = 'none';
 				document.getElementById('tr_id_marker_step_'+serie_key).style.display = 'none';
 				document.getElementById('tr_id_style_sel_'+serie_key).style.display   = 'inline';
 				break;
 			default:
 			    document.getElementById('tr_id_marker_size_'+serie_key).style.display = 'none';
 				document.getElementById('tr_id_marker_sel_'+serie_key).style.display  = 'none';
 				document.getElementById('tr_id_marker_step_'+serie_key).style.display = 'none';
 				document.getElementById('tr_id_style_sel_'+serie_key).style.display   = 'none';
 			break;
		}
	}
	if (input_id.indexOf('hijo_grid_sel') == 0)
 	{
		switch(key_input)
		{
 			case '0':
 				document.getElementById('id_width_hor').disabled            = true;
 				document.getElementById('id_width_hor').options[0].selected = true;
 				document.getElementById('id_width_ver').disabled            = true;
 				document.getElementById('id_width_ver').options[0].selected = true;
 				break;
 			case '1':
 				document.getElementById('id_width_hor').disabled            = false;
 				document.getElementById('id_width_hor').options[1].selected = true;
 				document.getElementById('id_width_ver').disabled            = true;
 				document.getElementById('id_width_ver').options[0].selected = true;
 				break;
 			case '2':
 				document.getElementById('id_width_ver').disabled            = false;
 				document.getElementById('id_width_ver').options[1].selected = true;
 				document.getElementById('id_width_hor').disabled            = true;
 				document.getElementById('id_width_hor').options[0].selected = true;
 				break;
 			case '3':
 				document.getElementById('id_width_hor').disabled            = false;
 				document.getElementById('id_width_hor').options[1].selected = true;
 				document.getElementById('id_width_ver').disabled            = false;
 				document.getElementById('id_width_ver').options[1].selected = true;
 				break;
 			break;
		}
	}
}
function change_color(src_image, img_selector, hint_sel, input_id, key_input){
	 document.getElementById(img_selector).style.background = src_image;
	 document.getElementById(img_selector).title = hint_sel;
	 document.getElementById(input_id).value = key_input;
	 if (bUpdateColor) {
	 	onclickBGModal();
	 	onclickFCModal();
	 }
}
function set_Tempor(div_id){
  	div_hidden = div_id;
	if (document.getElementById(div_id).style.display == 'inline')
		if (div_id.search('id_padre') >= 0)
	      	tempor_padre = setTimeout("show_hide_sel(div_hidden, this, '');",500);
		else
			tempor = setTimeout("show_hide_sel(div_hidden, this, '');",500);
}
function set_Tempor_2000(div_id){
  	div_hidden = div_id;
	if (div_id.search('id_padre') >= 0)
	   	tempor_padre = setTimeout("show_hide_sel(div_hidden, this, '');",2000);
	else
	   	tempor = setTimeout("show_hide_sel(div_hidden, this, '');",2000);
}
function clear_Tempor(){
	clearTimeout(tempor);
	clearTimeout(tempor_padre);
}
function fnShowChooseColorDlg(color,param,path){
	if (document.all) { //IE4 and up
		var args = new Array(3);
		args[0] = color;
		args[1] = window;
		args[2] = param;
		retVal = window.showModalDialog(
				path+'color_dialog.php',args,
				'dialogHeight: 380px; dialogWidth: 245px; center: yes; scroll: No; help:  No; resizable: No; status:no;');
	} else if (document.layers) {
	} else if (document.getElementById) { //mozilla
		var winRef;
		winRef = window.open(
				path+'color_dialog.php?'+escape(color)+'&'+param,"_blank",
				'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=245, height=325');
	}
}
function OnChangeColor(color, other){
	if (color != '')
		if (color == -1)
			change_color('transparent', 'id_img_selection_'+other, 'Transparent', other, color);
		else
			change_color(color, 'id_img_selection_'+other, color, other, color);
}
function getAbsoluteElementPosition(element) {
	if (typeof element == "string")
	element = document.getElementById(element)

	if (!element) return { top:0,left:0 };

	var y = 0;
	var x = 0;
	while (element.offsetParent) {
		x += element.offsetLeft;
		y += element.offsetTop;
		element = element.offsetParent;
		if (element.tagName == 'DIV')
			break;
	}
	return {top:y,left:x};
}