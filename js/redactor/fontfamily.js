(function($)
{
	$.Redactor.prototype.fontfamily = function()
	{
		return {
			init: function ()
			{
				//JAPR 2019-08-07: Ajustes a las fuentes utilizadas para estandarizar entre los editores y reducir complejidad en el diseño (#BT0NMN)
				var fonts = [ 'Arial', 'Lato', 'Monospace', 'Palatino Linotype' /*'Helvetica', 'Georgia', 'Times New Roman', 'Verdana'*/ ];
				var that = this;
				var dropdown = {};

				$.each(fonts, function(i, s)
				{
					dropdown['s' + i] = { title: s, func: function() { that.fontfamily.set(s); }};
				});

				if(redactorLang == 'es'){
					dropdown.remove = { title: 'Remover tipo de fuente', func: that.fontfamily.reset };
					var button = this.button.add('fontfamily', 'Tipo de fuente');
				}else{
					dropdown.remove = { title: 'Remove font family', func: that.fontfamily.reset };
					var button = this.button.add('fontfamily', 'Change font family');
				}
				this.button.addDropdown(button, dropdown);

			},
			set: function (value)
			{
				this.inline.format('span', 'style', 'font-family:' + value + ';');
			},
			reset: function()
			{
				this.inline.removeStyleRule('font-family');
			}
		};
	};
})(jQuery);