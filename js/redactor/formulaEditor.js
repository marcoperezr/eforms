$.Redactor.prototype.formulaEditor = function()
{
    return {
        init: function()
        {
            var button = this.button.add('formulaEditor', 'Editor de F&oacute;mulas');
 
            this.button.changeIcon('formulaEditor', 'fEditor');
            // make your added button as Font Awesome's icon
            //this.button.setAwesome('formulaEditor', 'fa-calculator');
 
            this.button.addCallback(button, this.formulaEditor.openFormula);
        },
        openFormula: function(buttonName)
        {
			//JAPR 2016-06-20: Integrado el EditorHTML directo en la forma de creación en lugar de en un diálogo nuevo (#TY8HOB)
			if (this.opts && this.opts.bitEmbedded) {
				fnOpenFormulaEditor(this.$textarea, this.$textarea.val(), 0, this.range);
			}
			else {
				parent.window.fnOpenFormulaEditor(this.$textarea, this.$textarea.val(), 0, this.range);
			}
			//JAPR
        }
    };
};