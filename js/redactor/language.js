$.Redactor.prototype.lenguaje = function()
{
	return {
		init: function()
		{
			var button = this.button.add('lenguaje', 'Lenguaje');
            this.button.setAwesome('lenguaje', 'fa-language');
 
			var dropdown = {};
 
            dropdown.l1 = { title: 'Espa&ntilde;ol', func: this.lenguaje.setEspanol };
            dropdown.l2 = { title: 'Ingl&eacute;s', func: this.lenguaje.setIngles };
 
            this.button.addDropdown(button, dropdown);
		},
		setEspanol: function(){
			//var lengu = this.lang.get('html');
			$.Redactor.opts.langs = 'es';
			$.Redactor.opts.lang = 'es';
			//debugger;
			//this.toolbar.loadButtons();
        },
        setIngles: function(){
            $.Redactor.opts.langs = 'en';
			$.Redactor.opts.lang = 'en';
			//debugger;
			//this.toolbar.loadButtons();
        }
	};
};