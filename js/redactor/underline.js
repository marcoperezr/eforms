//Underline button implementation
$(function(){
	$.Redactor.prototype.underline = function(){
		return {
			init: function(){
				if(redactorLang == 'es'){
					var button = this.button.addAfter('italic', 'underline', 'Subrayar');
					this.button.addCallback(button, this.underline.format);
				}else{
					var button = this.button.addAfter('italic', 'underline', 'Underline');
					this.button.addCallback(button, this.underline.format);
					}
			},
			format: function(){
				this.inline.format('u');
			}
		};
	};
});