(function($)
{
	$.Redactor.prototype.fontsize = function()
	{
		return {
			init: function()
			{
				//JAPR 2016-05-17: Incrementada la cantidad de tamaños de fuente disponibles
				//var fonts = [10, 11, 12, 14, 16, 18, 20, 24, 28, 30];
				var fonts = [];
				for (var intMinFS = 10; intMinFS <= 80; intMinFS++) {
					fonts.push(intMinFS);
				}
				//JAPR
				var that = this;
				var dropdown = {};

				$.each(fonts, function(i, s)
				{
					dropdown['s' + i] = { title: s + 'px', func: function() { that.fontsize.set(s); } };
				});

				if(redactorLang == 'es'){
					dropdown.remove = { title: 'Remover tamaño de fuente', func: that.fontsize.reset };
					var button = this.button.add('fontsize', 'Tama&ntilde;o de fuente');
				}else{
					dropdown.remove = { title: 'Remove font size', func: that.fontsize.reset };
					var button = this.button.add('fontsize', 'Change font size');
				}

				this.button.addDropdown(button, dropdown);
			},
			set: function(size)
			{
				this.inline.format('span', 'style', 'font-size: ' + size + 'px;');
			},
			reset: function()
			{
				this.inline.removeStyleRule('font-size');
			}
		};
	};
})(jQuery);