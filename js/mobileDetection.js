var gbForceMobile = false;
var gbDesignMode = true;
//Variable que controla si el app es para liga interna (0), la tienda (1) o desarrollo (2)
var gsStoreApp = 1;
//Variable que controla el ambiente donde se manejará el app: Tienda/producción (default: vacío), Liga (test), Testing/desarrollo (dev), desarrollo/xcode (xcode)
var gsEnvironmentPath = "";
//Variable para permitir login en blanco incluso cuando es app de Forms ya que no se permitía esto anteriormente.
var gsBlankLogin = 1;
//Nombre del producto, define el nombre de la carpeta: KPIForms = KPI Online Forms, Geocontrol = Geocontrol, Personalizada = Algún otro nombre
var gsProductName = "KPI Online Forms";
//Nombre del app, necesaria para registro de usuarios
var gsAppName = "";
//Cambiar para permitir registro de usuarios desde aplicación, cambiar en conjunto con gsAppName
var gsAllowRegister = false;
//Cambiar para saltar pantalla de login y entrar directamente al menú de formas, debe haber una cuenta y password fija
var gsBypassLogin = false;
//Cambiar para ocultar el logo de la pantalla de login.
var gsHideLoginBan = false;
//Permite entrar con un usuario que no necesariamente sea un correo (Valor = true: necesita correo obligatorio).
var gsByUserMail = false;
//JAPR 2016-06-24: Modificada la generación de URLs de Google Maps debido al requerimiento de llave a partir de Junio 2016, para poder cambiar la URL desde el server (#F3M0A7)
var gsMapsProtocol = "https://";
var gsMapsGeocodeURL = "maps.googleapis.com/maps/api/geocode/json";
var gsMapsAPIURL = "maps.googleapis.com/maps/api/js";
var gsMapsChartURL = "chart.googleapis.com/chart";
//Esta llave se debe asignar según las especificaciones del App que se va a generar, ya que hay un límite de requests de aprox. 25,000 por día combinados entre todos los productos,
//todas las versiones y todas las Apps generadas que utilicen esta llave, así que en caso de ser requerido, se deben generar llaves adicionales que se pueden modificar en este archivo
//por App generada para hacerlas específicas (la llave incluirá el parámetro por si llegase a cambiar la implementación del request, o por si se debieran invocar parámetros adicionales)
var gsMapsAPIKey = "&client=gme-bitamdemexicosade&channel=kpiforms";
//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM) (OMMC)
var gsAppleMapkitProtocol = "https://";
var gsAppleMapkitAPIURL = "cdn2.apple-mapkit.com/mk/5.x.x/mapkit.js";
//OMMC 2016-07-26: Agregada la generación de URLs de la pregunta OCR debido al requerimiento de una key para permitir más requests, se deja esto para estandarizar con Google Maps
var gsOCRKeyParam = "apikey";
var gsOCRKey = "2968ae18d788957";
var gsOCRService = "api.ocr.space/Parse/Image";
var gsOCRProtocol = "https://";
//MAPR 2019-03-28: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
var gsARextiOS = '.scn';
var gsARextAndroid = '.sfb';
