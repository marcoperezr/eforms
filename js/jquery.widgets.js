(function($){
	$.widget("bitam.boxfilter", {
		
		filter: {},
		descriptionsTags: {},
		searchString: '',
		filterDate: '',
		
		options: {

		},

		_create: function(options) {
			var self = this;
			var options = self.options;
			var list = $("<ul/>", {"class":"list-filter"});
			var $btnli = $('<i/>', { 'data-parent': 'SearchString', class: 'icon-search'});
			var $btnSearch = $('<button/>', {class: "btn btn-default" }).append($btnli);
			$btnSearch.on('click', $.proxy(this.submitFilter, { boxfilter: this }));
			var $div = $('<div/>', { class: 'input-group' });
			var $divSpan = $('<span/>', { class:"input-group-btn" });
			this.searchString = options.searchString;
			var $searchInput = $('<input/>', {id:'Search', value: this.searchString, name: "search", type:"search", placeholder: $.t('Search') , class:"form-control input-sm", style:"height: 28px;-webkit-appearance: none;" });
			$searchInput.keypress(function (e) {
				if (e.which == 13){
				   $btnSearch.trigger( "click" );
				   event.preventDefault();
				}
			});

			$div.append(
				$searchInput,
				$divSpan.append(
					$btnSearch
				)
			);
			var myFormSearch = $div;
						
			var myDivContainer = $("<div/>", {"id":"filterBox", "class":"filterbox grid_2"}).append(
				myFormSearch,
				$("<form/>", {}).append(list)
			);
			this.element.prepend(myDivContainer);
			$.extend(this.filter, options.checked);
			$.extend(this.descriptionsTags, options.descriptionTags);
			this.filterDate = options.filterDate;
			this.updateBoxFilter(list, options.descriptionTags, options.checked);
			
			//JAPR 2016-08-25: Implementada la eliminaci�n m�ltiple de reportes
			//JAPR 2016-08-25: Dado a que se necesita permitir invocar al submit de este widget (submitFilter) pero desde fuera, y el submit lo que hace es invocar a una funci�n
			//de fuera que se pasa como par�metro o aqu� mismo se obtiene su referencia global (applyFilterAttrib), a la cual le manda como primeros 3 par�metros el conjunto de
			//elementos marcados del Widget, para no repetir el c�digo que analiza la selecci�n del Widget, se verificar� si en options se recibi� una funci�n "setRefreshFn" la cual
			//recibir� como par�metro precisamente a la funci�n submitFilter para permitir invocarla desde fuera, ya sea especificando un objeto que simule al event de uno de los
			//componentes del widget, o no mandando nada con lo cual se simular� que se presion� el bot�n de Search, y el �nico uso de esta funci�n ser� simular un refresh completo
			//de la ventana tal como lo har�a el widget con su estado actual
			if (options.setRefreshFn) {
				options.setRefreshFn($.proxy(this.submitFilter, { boxfilter: this }));
			}
			//JAPR
		},
		
		updateBoxFilter: function (list, descriptionTags, arrFilter) {

			/*@AAL 24/04/2015: Modificado para que el filtrado por fecha solo se muestre en agendas. AgendaScheduler no aplica
			variable definida en surveyAgenda.inc.php (True) y surveyAgendaScheduler.in.php (False) */

			try { this.options.isAgenda = isAgenda; } catch(err) {};

			if(this.options.isAgenda){
				title = $("<li/>", {"class":"description-tag title"+'Fecha'}).on('click', {descrTags:".tags"+'Fecha'}, function(e){
						$(e.data.descrTags).slideToggle();
					});
				title.html($.t('Date'));
				list.append(title);
				//var listTag = $("<ul/>", {"class":"list-filter-tag "+descrTag, "id":"listFilterTag-"+descrTag});
				var listTag = $("<ul/>", {"class":"list-filter-tag "+'Fecha', "id":"listFilterTag-"+'Fecha'});
				this.appendDateFilter(listTag, arrFilter);
				descrTags = $("<li/>", {"class":"tags"+'Fecha'});
				descrTags.append(listTag);
				list.append(descrTags);
			}

			if( !this.options.orderFilter ) {
				this.options.orderFilter = Object.keys( descriptionTags );
			}

			//for (var keyDesc in descriptionTags) {
			for( var i = 0; i < this.options.orderFilter.length; i++ ) {
				var keyDesc = this.options.orderFilter[i];
				var descrTag = keyDesc;
				var title = list.find(".title"+descrTag).empty();
				var descrTags = list.find(".tags"+descrTag).empty();
				var listTag = $("<ul/>", {"class":"list-filter-tag "+descrTag, "id":"listFilterTag-"+descrTag});

				//GCRUZ 2015-10-15. Indicar un orden espec�fico a los elementos del filtro
				if (!descriptionTags[keyDesc].order)
				{
					descriptionTags[keyDesc].order = [];
					var okey = 0;
					for (var elkey in descriptionTags[keyDesc].elements )
					{
						descriptionTags[keyDesc].order[okey] = elkey;
						okey++;
					}
				}

				//GCRUZ 2015-10-15. Indicar un orden espec�fico a los elementos del filtro
				for (var okey = 0 ; okey < descriptionTags[keyDesc].order.length; okey++) {
					var dkey = descriptionTags[keyDesc].order[okey];
					var label = $("<label/>");
					var elementTag = dkey;
					var elementName = descriptionTags[keyDesc].elements[dkey].id;
					var descrTagNumItems = descriptionTags[keyDesc].elements[dkey].num;
					var descrTagID = descriptionTags[keyDesc].elements[dkey].id;
					var input = $("<input/>", {"data-role":"none", "data-description-id": elementTag, "data-parent": descrTag,"data-description":descrTagID,"data-fieldname":elementTag,"type":"checkbox","value":elementTag});
					input.on('click', $.proxy(this.submitFilter, { boxfilter: this } ));
					label.append(input, $("<span/>", {"title":elementName}).html(elementName));
					label.append($('<div/>', {'class':'tagnumitems'}).html(descrTagNumItems));
					listTag.append($("<li/>", {"class":"tagname"}).append(label));
					if (arrFilter && arrFilter[keyDesc] && $.inArray(elementTag, arrFilter[keyDesc]) != -1) {
						input[0].checked = true;
						input.attr('checked', true);
					}
					input.checkbox();
				}
				if(!title.length) {
					title = $("<li/>", {"class":"description-tag title"+descrTag}).on('click', {descrTags:".tags"+descrTag}, function(e){
						$(e.data.descrTags).slideToggle();
					})
					list.append(title);
				}					
				if(!descrTags.length){
					descrTags = $("<li/>", {"class":"tags"+descrTag});
					list.append(descrTags);
				}
				title.html( descriptionTags[keyDesc].description );
				descrTags.append(listTag);
			}
		},
		
		appendDateFilter: function (listTag, arrFilter) {
			var eachFilter = {};
			eachFilter.id = 'date';
			var filterDate = function (fieldId) {
				var dateFrom = Date.parse($('#from_'+fieldId).val());
				var dateTo = Date.parse($('#to_'+fieldId).val());
				if(dateFrom  && dateTo) {
					var msg = dateFrom.toString('d MMM yyyy')  + ' - ' + dateTo.toString('d MMM yyyy');
					var sRange = dateFrom.toString("yyyy-MM-dd") + '@' + dateTo.toString("yyyy-MM-dd");
					$('[data-fieldname="'+fieldId+'"]').val(sRange).prop('checked', false).trigger('click');
				}
				$('.tagsFecha .clear.ico').show();
			}
			
			var tmpArray = (this.filterDate) ? this.filterDate.split('@') : '';
			var dateFrom = undefined;
			var dateTo = undefined;
			
			if (tmpArray) {
				dateFrom = Date.parse(tmpArray[0]);
				dateTo = Date.parse(tmpArray[1]);
			}

			var input = $("<input/>", {"data-role":"none","data-description":'',"data-fieldname":eachFilter.id,"type":"checkbox","value":''}).hide();
			
			input.on('click', $.proxy(this.submitFilter, { boxfilter: this } ));
			
			var sRange = $.t('Any time');
			var sValueFrom = '', sValueTo = '';
			//if($.isArray(arrFilter[eachFilter.id]) && arrFilter[eachFilter.id][0] && arrFilter[eachFilter.id][0]!=''){
				//var dateFrom = Date.parse(arrFilter[eachFilter.id][0].split('@')[0]);
				//var dateTo = Date.parse(arrFilter[eachFilter.id][0].split('@')[1]);

			if (dateFrom && dateTo) {
				sRange = dateFrom.toString('d MMM yyyy')  + ' - ' + dateTo.toString('d MMM yyyy');
				input.val(this.filterDate).attr('checked', true);
				var sValueFrom = dateFrom.toString('MM/dd/yyyy');
				var sValueTo = dateTo.toString('MM/dd/yyyy');
			}
			//}
			listTag.append($("<li/>", {"class":"tagname", 'style':'position:relative'}).append(input, 
				$('<div/>', {'class':'customRange'}).append(
					$("<div/>", {'class':'customRange title'}).append($('<label/>', {'id':'CustomDateRange'}).html($.t('Custom date range')), $('<img/>', {'title':$.t('Clear range'),'class':'clear ico',src:"images/clear_ico.png"}).on('click', $.proxy(function(){
						$( "#from_"+this.idField ).val('');
						$( "#to_"+this.idField ).val('');
						$('[data-fieldname="'+this.idField+'"]').val('').prop('checked', true).trigger('click');
					}, {idField:eachFilter.id})).hide()),
					$('<label/>', {'id':'From', 'class':'customRange label'}).html($.t('From')),
					$('<input type="text" style="background-color:white;" readOnly="readOnly" id="from_'+eachFilter.id+'" name="from" class="form-control input-sm" />').datepicker({
						changeMonth: true,
						changeYear: true,
						yearRange:"c-40:c+4",
						showButtonPanel: true,
						onClose: $.proxy(function( selectedDate ) {
							$( "#to_"+this.idField ).datepicker( "option", "minDate", selectedDate );
							filterDate(this.idField);
						}, {idField:eachFilter.id})
					}).val(sValueFrom),
					$('<label/>', {'id':'To','class':'customRange label'}).html($.t('To')),
					$('<input type="text" style="background-color:white;" readOnly="readOnly" id="to_'+eachFilter.id+'" name="to" class="form-control input-sm" />').datepicker({
						changeMonth: true,
						changeYear: true,
						yearRange:"c-40:c+4",
						showButtonPanel: true,
						onClose: $.proxy(function( selectedDate ) {
							$( "#from_"+this.idField ).datepicker( "option", "maxDate", selectedDate );
							filterDate(this.idField);
						}, {idField:eachFilter.id})
					}).val(sValueTo)
				)
			));
		},
		
		submitFilter: function (event) {
			//JAPR 2016-08-25: Implementada la eliminaci�n m�ltiple de reportes
			//JAPR 2016-08-25: Dado a que se necesita permitir invocar al submit de este widget (submitFilter) pero desde fuera, y el submit lo que hace es invocar a una funci�n
			//de fuera que se pasa como par�metro o aqu� mismo se obtiene su referencia global (applyFilterAttrib), a la cual le manda como primeros 3 par�metros el conjunto de
			//elementos marcados del Widget, para no repetir el c�digo que analiza la selecci�n del Widget, se verificar� si en options se recibi� una funci�n "setRefreshFn" la cual
			//recibir� como par�metro precisamente a la funci�n submitFilter para permitir invocarla desde fuera, ya sea especificando un objeto que simule al event de uno de los
			//componentes del widget, o no mandando nada con lo cual se simular� que se presion� el bot�n de Search, y el �nico uso de esta funci�n ser� simular un refresh completo
			//de la ventana tal como lo har�a el widget con su estado actual
			//Si no se recibi� event, se asume que se desea aplicar sobre el bot�n de search
			if (!event) {
				//Este no es un evento real, pero a la fecha de implementaci�n no se necesitaba ninguna propiedad adicional de event, s�lo el componente target sobre el que se ejecutaba
				event = {target:this.boxfilter.element.find(".btn.btn-default")};
			}
			//JAPR
			
			var applyFilter = this.boxfilter.options.applyFilterAttrib || applyFilterAttrib;

			var descriptionsTags = this.boxfilter.descriptionsTags;
			var filter = this.boxfilter.filter;
			var $el = $(event.target);
			var descrTag = $el.attr('data-parent');
			if (!descrTag) {
				descrTag = 'Date';
			}
			var attrDescrTag = '';
			var elementTag = $el.attr('data-description-id');
			var searchString = this.boxfilter.element.find('input[name|="search"]').val();
			if (searchString != '') {
				filter['SearchString'] = searchString;
				descrTag = "SearchString";
			} else {
				if (filter['SearchString']) {
					delete filter['SearchString'];
				}
			}

			if (descrTag.indexOf('AttribID_') != -1) {
				attrDescrTag = descrTag;
				descrTag = 'Attributes';
				for (var fkey in filter) {
					if (fkey.indexOf('AttribID_') != -1) {
						
						if (!filter['Attributes']) {
							filter['Attributes'] = {};
						}

						if (!filter['Attributes'] && !filter['Attributes'][fkey]) {
							filter['Attributes'][fkey] = new Array();
						}
						filter['Attributes'][fkey] = filter[fkey];
						delete filter[fkey];
					}
				}
			}
			Tag = 'Forms';
			if (descrTag == $.t('Catalogo') || descrTag == $.t('Forms')) { 
				var catinput = $('#listFilterTag-Catalogo input:checked.').not($(event.target));
				if (catinput.length) {
					catinput.attr('checked', false)
					catinput.parent().removeClass('checked');
				}
				Tag = descrTag;
			}
			
			if (!filter) {
				filter = {};
			}
			var arrDescrTag = undefined;
			if (filter[descrTag]) {
				if (filter[descrTag][attrDescrTag]) {
					arrDescrTag = filter[descrTag][attrDescrTag];
				} else {
					arrDescrTag = filter[descrTag];
				}
			}
			
			if (arrDescrTag) {
				//if (filter[descrTag][elementTag]) {
				if ($.inArray(elementTag, arrDescrTag) != -1) {
					arrDescrTag.splice($.inArray(elementTag, arrDescrTag), 1);
					if (arrDescrTag.length == 0) {
						delete arrDescrTag;
					}
				} else {
					if (descrTag == $.t('Catalogo')) {
						//arrDescrTag = new Array();
						/**/
						if (filter[descrTag]) {
							if (filter[descrTag][attrDescrTag]) {
								filter[descrTag][attrDescrTag] = new Array();
							} else {
								filter[descrTag] = new Array(elementTag);
							}
						}
						/**/
					}
					//filter[descrTag].push(descriptionsTags[descrTag][elementTag]);
					//arrDescrTag[arrDescrTag.length] = elementTag;
					/**/
					if (filter[descrTag]) {
						if (filter[descrTag][attrDescrTag]) {
							filter[descrTag][attrDescrTag][filter[descrTag][attrDescrTag].length] = elementTag;
						} else if(attrDescrTag != ""){
							//@AAL 24/04/2015: Modificado, cuando se trataba de un nuevo atributo, no se estaba agregando al filtro.
							filter[descrTag][attrDescrTag] =  new Array(elementTag);
						}
						else {
							filter[descrTag][filter[descrTag].length] = elementTag;
						}
					}
					/**/
				}
			} else {
				if (attrDescrTag) {
					filter[descrTag] = {};
					filter[descrTag][attrDescrTag] = new Array();
					filter[descrTag][attrDescrTag][(filter[descrTag][attrDescrTag].length)] = elementTag;
				} else {
					filter[descrTag] = new Array();
					//filter[descrTag].push(descriptionsTags[descrTag][elementTag]);
					filter[descrTag][filter[descrTag].length] = elementTag;
				}
			}
			if (attrDescrTag) {
				var tmpOldTags = {};
				tmpOldTags[attrDescrTag] = descriptionsTags[attrDescrTag];
				var oldTags = JSON.stringify(tmpOldTags);
			} else {
				if (descrTag == 'SearchString') {
					var oldTags = {};
				} else {
					if (descrTag != 'Date') {
						var oldTags = JSON.stringify(descriptionsTags[descrTag].elements);
					}
				}
			}
			
			if ($('[data-fieldname="'+'date'+'"]').val() != '') {
				filter['Date'] = $('[data-fieldname="'+'date'+'"]').val();
			}
			
			if (descrTag == $.t('Catalogo')) {
				if (!filter[descrTag]) {
					delete filter['Attributes'];
				}
				if (filter[descrTag] && filter[descrTag].length == 0) {
					delete filter['Attributes'];
				}
			}
			
			//var oldTags = JSON.stringify(descriptionsTags[descrTag].elements);
			var currentTag = ((attrDescrTag) ? attrDescrTag : descrTag);
			if (!descriptionsTags['Catalogo'] && !descriptionsTags['Forms']) {
				if (currentTag == 'Catalogo') {
					descriptionsTags['Catalogo'] = {};
					descriptionsTags['Catalogo'].description = 'Catalogo';
					descriptionsTags['Catalogo'].elements = {};
				} 
				else{
					descriptionsTags['Forms'] = {};
					descriptionsTags['Forms'].description = 'Forms';
					descriptionsTags['Forms'].elements = {};
				}
			}

			applyFilter(filter, oldTags, currentTag, JSON.stringify(descriptionsTags[Tag].elements));
		}
	});

	$.widget("bitam.checkbox", {
		options: {
			
		},

		_create: function(){
			var self = this;
			var options = self.options;
			self.idContainerCheckboxGroup = $("<div/>").uniqueId().attr('id');
			if(options.query){
				options.query({
					callback:function(res){
						var bFirst = true;
						self.element.wrap('<div id="'+self.idContainerCheckboxGroup+'" class="checkbox-group '+(options.vertical?'checkboxgroup vertical':'')+'" style="'+(options.modal? 'display:none':'')+'"/>');
						self.element.find('span').html('');
						for(var key in res.results){
							if(bFirst){
								self.element.find('span').html(res.results[key].text);
								self.element.find('input').val(res.results[key]['id']);
								self.element.find('input').attr('data-jquerywidget', 'checkbox').attr('data-text', res.results[key].text).attr('data-description', res.results[key].text);
								if($.isPlainObject(self.options.value)) {
									
									if(res.results[0].id == (self.element.find('input').attr('name') + '|q1')) {
										for(var oKey in self.options.value) {
											if(self.options.value[oKey] == res.results[key].text) {
												//self.element.find('input').attr('checked',self.options.value[oKey]);
												self.element.find('input').prop('checked',true);												
											}
										}
									} else {
										self.element.find('input').prop('checked', self.options.value[res.results[key]['id']] != undefined);
									}
									
									//self.element.find('input').attr('checked', self.options.value[res.results[key]['id']] != undefined)
								}
								for(var key in res.events){
									self.element.find('input').on('change', res.events[key]);
								}
							}else{
								var inputCheck;
								var aInputCheck = $('<label/>').append(
									inputCheck = $('<input/>', {type:'checkbox', value:res.results[key]['id'], 'name':self.element.find('input').attr('name'), 'data-role':'none'}),
									$('<span/>').html(res.results[key].text)
								)
								inputCheck.attr('data-text', res.results[key].text).attr('data-description', res.results[key].text).attr('data-jquerywidget', 'checkbox');
								if(options.modal){
									aInputCheck = $("<div/>", {'class':'item-checkbox'}).append(aInputCheck);
								}							
								//aInputCheck.insertAfter(self.element);
								$('#'+self.idContainerCheckboxGroup).append(aInputCheck);
								if($.isPlainObject(self.options.value)){
									if(res.results[0].id == (inputCheck.attr('name') + '|q1')) {
										for(var oKey in self.options.value) {
											if(self.options.value[oKey] == res.results[key].text) {
												inputCheck.attr('checked',self.options.value[oKey]);
											}
										}
									} else {
									inputCheck.attr('checked', self.options.value[res.results[key]['id']] != undefined)
								}
							}						
								for(var key in res.events){
									inputCheck.on('change', res.events[key]);
								}
							}						
							bFirst = false;
						}
						
						if(options.modal){
							var bttn = $('<button data-inline="true" data-theme="g">'+$.t('Change')+'</button>');
							self.element.wrap("<div class='item-checkbox' />");
							self.element.parent().parent().parent().append(bttn.on('click', function(){
								var myWindowModal = $("#windowModal_widget");
								if(myWindowModal.length > 0){
									myWindowModal.remove();
								}
								var myWindowModal = $("<div/>", {"id":"windowModal_widget", "class":"ui-body-c"});
								var containerCheckBox = $('#'+self.idContainerCheckboxGroup).replaceWith($("<div id='"+self.idContainerCheckboxGroup+"_cpy'/>"));
								myWindowModal.append(containerCheckBox.show());
								$('body').append(myWindowModal);							
								width = (options.width ? options.width : 560); //690			
								myWindowModal.dialogWidget({
									title: options.title, 
									minHeight: 250, 
									minWidth: width, 
									height: 350,
									modal: true,
									buttons: {
										"Save": function() {
											$('#'+self.idContainerCheckboxGroup+"_cpy").replaceWith(containerCheckBox.hide());
											$( this ).dialogWidget( "close" );
										},
										Cancel: function() {
											$('#'+self.idContainerCheckboxGroup+"_cpy").replaceWith(containerCheckBox.hide());
											$( this ).dialogWidget( "close" );
										}
									}
								});
								
								return false;
							}));
						}
					}
				})
			}else{
				self.element.wrap('<div role="checkbox" aria-checked="'+(self.element.prop('checked') ? 'true':'false')+'" class="chkbox-box" style="float:left;margin-top: 2px;"><div class="chkbox-box-inner '+(self.element.prop('checked') ? 'checked':'')+'"></div></div>');
				self.element.on('click', function(){
					if(this.checked){
						$(this).parent().addClass('checked')
						$(this).parent().parent().attr('aria-checked', 'true');
					}else{
						$(this).parent().removeClass('checked')
						$(this).parent().parent().attr('aria-checked', 'false');
					}
				})
				self.element.css({
					/*'visibility': 'hidden',*/
					'opacity': '0',
					'position': 'absolute',
					'top': '-5px',
					'left': '-1px'
				})
			}
		},
		//aramirez@22/01/2013
		//Este metodo no debe lanzar el evento change/click, si se desea debemos implementar un metodo aparte
		checked: function(aBoolean){
			if(typeof aBoolean != "boolean")
				aBoolean = this.element.prop('checked');
			if(aBoolean){
				this.element.prop('checked', true);
				this.element.parent().addClass('checked');
				this.element.parent().parent().attr('aria-checked', 'true');
			}else{
				this.element.prop('checked', false);
				this.element.parent().removeClass('checked');
				this.element.parent().parent().attr('aria-checked', 'false');
			}
		},
		
		disabled: function(aBoolean){
			this.element.parent().parent().addClass('chkbox-box-disable');
			this.element.attr('disabled','disabled');
		},
		
		enable: function(aBoolean){
			this.element.parent().parent().removeClass('chkbox-box-disable');
			this.element.removeAttr('disabled');
		},
		
		val: function(value){
			if(value==undefined){
				var res = [];
				var aCheckboxs = $('#'+this.idContainerCheckboxGroup + " input:checked");
				if(aCheckboxs.length>1){
					res = [];
					for(var i=0; i<aCheckboxs.length; i++){
						res.push({id:$(aCheckboxs[i]).val(), text:$(aCheckboxs[i]).data('text')});
					}
				}else{
					if(aCheckboxs.length==1)
						res = {id:aCheckboxs.val(), text:aCheckboxs.data('text')}
				}				
				return res;
			}else{
				this.options.value = value;
			}
		}
	});

	$.widget("bitam.uniqueid", {
		options: {},
		
		_create: function(){
			var self = this;
			options = self.options,
			initValue = self.element.val();
			if($.trim(initValue) == '') {
				//var uniqueidVal = AlphabeticID.encode(new Date().getTime(), 10);
				var length = undefined;
				if(self.options.length) {
					length = parseInt(self.options.length);
				} else {
					length = 6;
				}
				var uniqueidVal = AlphabeticID.encode(length);
				self.element.val(uniqueidVal);
			}
		},
		
		destroy: function(){
			var self = this;
			$.Widget.prototype.destroy.call( self );
		}
	});
	
})(jQuery);
