function Start(){
	GetElementWithId('botOK').value = ML[69];
	GetElementWithId('botCancel').value = ML[70];
	GetElementWithId('textFontColor').innerHTML = ML[359];
	scomment = window_dialogArguments[1];
	scomment = scomment.replace(/&lt;/gi,"<");
	scomment = scomment.replace(/<br>/gi,"\n");
	GetElementWithId('comment').value = scomment;
	Add_Events();
	if (!bIsIE) { bUpdateColor = true; }
}
function Add_Events(){
	onmouseoverEvent =
		function() {
			clear_Tempor();
		};
	if (bIsIE) {
		GetElementWithId('id_div_padre_comment').attachEvent("onmouseover",onmouseoverEvent);
	}
	else {
		GetElementWithId('id_div_padre_comment').addEventListener('mouseover', onmouseoverEvent, false);
		GetElementWithId('id_div_padre_comment').addEventListener('click', onmouseoverEvent, false);
	}

	onchangeFamilyEvent =
		function() {
			sfont = GetElementWithId('id_font_family_comment').options[GetElementWithId('id_font_family_comment').selectedIndex].value;
			GetElementWithId('comment').style.fontFamily = sfont;
		};
	if (bIsIE) {
		GetElementWithId('id_font_family_comment').attachEvent("onchange",onchangeFamilyEvent);
	}
	else {
		GetElementWithId('id_font_family_comment').addEventListener("change",onchangeFamilyEvent,false);
	}

	onchangeSizeEvent =
		function() {
			sfont = GetElementWithId('id_font_size_comment').options[GetElementWithId('id_font_size_comment').selectedIndex].value;
			GetElementWithId('comment').style.fontSize = sfont;
		};
	if (bIsIE) {
		GetElementWithId('id_font_size_comment').attachEvent("onchange",onchangeSizeEvent);
	}
	else {
		GetElementWithId('id_font_size_comment').addEventListener("change",onchangeSizeEvent,false);
	}

	onclickBoldEvent =
		function() {
			if (GetElementWithId('id_font_bold_comment').checked){
				GetElementWithId('comment').style.fontWeight = 'bold';
			}
			else {
				GetElementWithId('comment').style.fontWeight = 'normal';
			}
		};
	if (bIsIE) {
		GetElementWithId('id_font_bold_comment').attachEvent("onclick",onclickBoldEvent);
	}
	else {
		GetElementWithId('id_font_bold_comment').addEventListener("click",onclickBoldEvent,false);
	}

	onclickItalicEvent =
		function() {
			if (GetElementWithId('id_font_italic_comment').checked){
				GetElementWithId('comment').style.fontStyle = 'italic';
			}
			else {
				GetElementWithId('comment').style.fontStyle = 'normal';
			}
		};
	if (bIsIE) {
		GetElementWithId('id_font_italic_comment').attachEvent("onclick",onclickItalicEvent);
	}
	else {
		GetElementWithId('id_font_italic_comment').addEventListener("click",onclickItalicEvent,false);
	}

	onclickUnderlineEvent =
		function() {
			if (GetElementWithId('id_font_underline_comment').checked){
				GetElementWithId('comment').style.textDecoration = 'underline';
			}
			else {
				GetElementWithId('comment').style.textDecoration = 'none';
			}
		};
	if (bIsIE) {
		GetElementWithId('id_font_underline_comment').attachEvent("onclick",onclickUnderlineEvent);
	}
	else {
		GetElementWithId('id_font_underline_comment').addEventListener("click",onclickUnderlineEvent,false);
	}

	onclickBGEvent =
		function() {
			scolor = GetElementWithId('nieto_font_bgcolor_comment').value;
			if (scolor == '-1'){
				scolor = 'transparent';
			}
			GetElementWithId('comment').style.backgroundColor = scolor;
		};
	if (bIsIE) {
		GetElementWithId('id_choose_color_nieto_font_bgcolor_comment').attachEvent("onclick",onclickBGEvent);
	}
	else {
		GetElementWithId('id_choose_color_nieto_font_bgcolor_comment').addEventListener("click",onclickBGEvent,false);
	}

	onclickFCEvent =
		function() {
			scolor = GetElementWithId('nieto_font_forecolor_comment').value;
			if (scolor == '-1'){
				scolor = 'transparent';
			}
			GetElementWithId('comment').style.color = scolor;
		};
	if (bIsIE) {
		GetElementWithId('id_choose_color_nieto_font_forecolor_comment').attachEvent("onclick",onclickFCEvent);
	}
	else {
		GetElementWithId('id_choose_color_nieto_font_forecolor_comment').addEventListener("click",onclickFCEvent,false);
	}
}
function onclickBGModal() {
	scolor = GetElementWithId('nieto_font_bgcolor_comment').value;
	if (scolor == '-1'){
		scolor = 'transparent';
	}
	GetElementWithId('comment').style.backgroundColor = scolor;
}
function onclickFCModal() {
	scolor = GetElementWithId('nieto_font_forecolor_comment').value;
	if (scolor == '-1'){
		scolor = 'transparent';
	}
	GetElementWithId('comment').style.color = scolor;
}
function Get_Font(font_id) {	// Name|Bold|Italic|Underline|Size|BG|FC
    //Name
	FN = GetElementWithId('id_font_family_' + font_id).value + '_';

	if(GetElementWithId('id_font_bold_'    + font_id).checked == true){
		FN = FN + '1_';
	}
	else {
		FN = FN + '0_';
	}
	if (GetElementWithId('id_font_italic_'  + font_id).checked == true) {
	    FN = FN + '1_';
	}
	else {
    	FN = FN + '0_';
	}
	if (GetElementWithId('id_font_underline_'  + font_id).checked == true) {
	    FN = FN + '1';
	}
	else {
    	FN = FN + '0';
	}
	//Size
	FS = GetElementWithId('id_font_size_' + font_id).value;
	//Color
	FC = GetElementWithId('nieto_font_forecolor_' + font_id).value;
	if (FC == '-1') {
		FC = 'transparent';
	}
	//BG color
	FBG = GetElementWithId('nieto_font_bgcolor_' + font_id).value;
	if (FBG == '-1')
		FBG = 'transparent';

	Font = FN + '_' + FS + '_' + FBG + '_' + FC;
	return Font;
}
function on_ok(){
	// Validacion del nombre de la tarea
	//debugger;
	var sName
	sName = GetElementWithId('comment').value;
	if ( (sName.lastIndexOf("\\") >= 0) || (sName.lastIndexOf("<b>") >= 0) ||
         (sName.lastIndexOf("|") >= 0) || (sName.lastIndexOf("@") >= 0) || (sName.lastIndexOf("_AWC_") >= 0) ||
         (sName.lastIndexOf("<?") >= 0))
	{
		alert(ML[435] + '. '+ML[444]+' \\ , | @ <? <b>');
		GetElementWithId('comment').focus();
		return false;
	}

	sFFamily = Get_Font('comment');
	sRetVal = sFFamily + '_AWStyle_' + GetElementWithId('comment').value;
	if (bIsIE) {
		owner.returnValue = sRetVal;
	}
	else {
		window.parent.SetCommentModalValues(sRetVal+sParamsReturn);
	}
	cerrar();
}