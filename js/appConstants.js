/* Definición de constantes utilizadas en los archivos eSurveyServiceMod.js y eSurveyServiceModFns.js (cualquier otro archivo del App de eForms)
*/

//****************************************************************************************************************************************
//****************************************************************************************************************************************
/* Enumerado con los tipos de objeto que usará el eForms
*/
//****************************************************************************************************************************************
//****************************************************************************************************************************************
ObjectType = {
	otyItem: 0,
	otySurvey: 1,
	otySection: 2,
	otyQuestion: 3,
	otyAnswer: 4,
	otyAgenda: 5,
	otyCatalog: 6,
	otyAttribute: 7,
	otyDraft: 8,
	otyOutbox: 9,
	otyPhoto: 10,
	otyMenu: 11,
	otyAudio: 12,
	otyVideo: 13,
	otyQuestionFilter: 18,
	otyStatusAgenda: 19,
	otyStatusDocument: 20,
	otySketch: 21,
	otyDocument: 22
};

//JAPR 2014-05-20: Agregado el tipo de sección Inline
SectionDisplayMode = {
	//JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
	//Ahora esta propiedad puede configurar el tipo de despliegue de Maestro-detalle. sdspDefault correspondería a dicho comportamiento
	sdspDefault: 0,		//Es el modo de despliegue normal hasta v4. A partir de V6 este despliegue representa a las secciones maestro-detalle exclusivamente, aunque SectionCls.type debería seguir siendo sectMasterDet
	sdspInline: 1,		//Modo de despliegue inline sólo aplicable para las secciones Inline (se ven en una única página)
	sdspPage: 2			//Modo de despliegue como página sólo aplicable para las secciones Inline (se ven como secciones dinámicas)
};
//JAPR

//Tipos de despliegue
DisplayMode = {
	dspVertical: 0,
	dspHorizontal: 1,
	dspMenu: 2,
	dspMatrix: 3,
	dspEntry: 4,
	dspAutocomplete: 5,
	dspLabelnum: 6,
	//JAPR 2013-04-12: Agregada la opción de visualización de preguntas tipo Foto
	dspDefault: 4,
	dspCustomized: 7,
	dspMap: 8,
	dspGetData: 9, 
	//JAPR 2016-04-28: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
	dspMetro: 10,
	//OMMC 2017-01-11: Agregado el despliegue Imagen para maximizar previos de fotos
	dspImage: 11,
	//MAPR 2019-03-28: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
	dspAR: 12
};

//Tipos de llenado de preguntas/secciones
ValuesSourceType = {
	tofFixedAnswers: 0,
	tofCatalog: 1,
	tofMCHQuestion: 2,
	tofSharedAnswers: 3,
	tofInlineSection: 4
}

MCInputType = {
	mpcCheckBox: 0,
	mpcNumeric: 1,
	mpcText: 2
};

//Tipos de preguntas
Type = {
	number: 1,
	simplechoice: 2,
	multiplechoice: 3,
	date: 4,
	text: 5,
	alfanum: 6,
	photo: 8,
	action: 9,
	signature: 10,
	message: 11,
	time: 12,
	skipsection: 13,
	calc: 14,
	document: 15,
	//JAPR 2013-04-12: Agregados nuevos tipos de pregunta Sincronización
	sync: 16,
	//Conchita 2013-04-26 Agregado tipo de pregunta callList (similar a simplechoice pero con un num de tel)
	callList: 17,
	//JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
	gps: 18,
	//JAPR 2014-03-10: Agregado el tipo de pregunta Password
	password: 19,
	mapped: 20,
	audio: 21,
	video: 22,
	//2014-09-05 agregado el nuevo tipo sketch (similar a la firma pero con imagen de fondo tomada o subida al server)
	sketch: 23,
	//JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
	section: 24,
	//AAL 06/05/2015: Agregado para soportar preguntas tipo Código de barras
	barcode: 25,
	//OMMC 2015-12-01: Agregado para las preguntas OCR
	ocr: 26,
	//JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
	exit: 27,
    //OMMC 2016-03-30: Agregado el tipo de pregunta Destino de datos
	updateDest: 28,
	//OMMC 2016-04-18: Agregada la pregunta Mi localización
	myLocation: 29
	//JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
	, sketchPlus: 30
};

//Tipos de estilo de componentes
ComponentStyle = {
	Native: 0,
	JQM: 1
};

//Tipos de sección
SectionType = {
	All: -1,
	Standard: 0,
	Dynamic: 1,
	Formatted: 2,
	Multiple: 3,
	//JAPR 2014-05-20: Agregado el tipo de sección Inline
	Inline: 4,
	//JAPR
	Recap: 5
};

//Nivel de captura de información
EntryLevel = {
	entNone: 0,
	entRequired: 1,
	entOptional: 2
}

//Tipos de eventos en el log del usuario
UserLogEvents = {
	uleMessage: 0,									//Un dato simplemente que se graba en el log del server
	uleLogin: 1,									//Un evento relacionado con el login (ver UserLogSteps)
		//Flags: Offline=true|false, Auto:true|false, Conn=Wifi|3Gs
	uleAutoDownloadDefinitions: 2,					//Inició la descarca de definiciones del servidor
	uleRefreshDefinitions: 3,						//Se invocó a la actualización manual de definiciones (botón de Refresh)
	uleFormCapture: 4,								//Un evento de captura de una encuesta
		//Flags: Agenda=Id, Draft=true|false, Outbox=true|false, New=true|false
	uleAppExit: 5,									//Se decidió salir del App manualmente
	uleFormRecovery: 6,								//Un evento de recuperación de una captura del Local Storage
	uleSynchronization: 7,							//Se inició el proceso de sincronización de las capturas
	uleServerCheck: 8								//Petición para verificar el estado del servidor
		//Flags: Total=###, Failed=###
}

//Pasos de ciertos eventos en el log del usuario
UserLogSteps = {
	ulsFlag: 0,										//Genérica. Indica que se debe grabar el dato en el campo Flags para complementar la info del log (por ejemplo una llave de agenda)
	ulsError: -1,									//Genérica. Indica que ocurrió algún error durante el proceso (se graba como un Flag especial pero además se actualiza el campo Status del error) 
	ulsStart: 1,									//Genérica. Indica el el proceso inició
	ulsDone: 2,										//Genérica. Indica que el proceso terminó
	ulsFormLoad: 2,									//Se cargó la forma para captura
	ulsFormCaptureStart: 3,							//Inicia la captura de una encuesta
	ulsFormCaptureEnd: 4,							//Se realizó el grabado de la encuesta (anexar si se grabó como Draft o como Outbox en un flag)
	ulsFormCaptureAbort: 5							//Se canceló la captura de la encuesta
}

//JAPR 2014-03-11: Agregados los tipos de posicionamiento del texto en MCH horizontal
PictureLayOut = {
	Left: 0,
	Top: 1,
	Behind: 2
}

QuestionFilterType = {
	qftpMarkerColors: 0
}

AgendaStatus = {
	agdOpen: 0,
	agdAnswered: 1,
	agdRescheduled: 2,
	agdFinished: 3,
	agdCanceled: 4
}

//JAPR 2016-01-28: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
FormsListViewTypes = {
	flvtDHTMLX: 0,
	flvtMasonry: 1
}

FormsTextLayOutPos = {
	svtxpNone: -1,				//No se desplegará nombre/descripción (texto), sólo la imagen asociada a la forma (se requiere tener una imagen personalizada o no habría manera de identificar una forma de otra, si no hay imagen personalizada se asume que el valor es el default o svtxpRight)
	svtxpDefault: 0,			//Se desplegará el texto abajo de la imagen
	svtxpBottom: 1,				//Se desplegará el texto abajo de la imagen
	svtxpLeft: 2,				//Se desplegará el texto a la izquierda de la imagen
	svtxpRight: 3,				//Se desplegará el texto a la derecha de la imagen
	svtxpTop: 4,				//Se desplegará el texto arriba de la imagen
	svtxOverTop: 5,				//Se desplegará el texto sobre la imagen en la parte superior
	svtxOverMiddle: 6,			//Se desplegará el texto sobre la imagen a la mitad
	svtxOverBottom: 7,			//Se desplegará el texto sobre la imagen en la parte inferior
	svtxpCustom: 8				//Se desplegará un HTML personalizado definido por objeto/global (durante el proceso de refreshFormsListMasonry se auto-asignará cuando se tiene un HTML personalizado en la forma)
}

//OMMC 2016-02-26: Agregada la lista de elementos de las secciones, para definir las clases que pintan los estilos
SElemTypes = {
	selemAll: -1,
	selemPage: 1,
	selemHdrBar: 2,
	selemHdrLab: 3,
	selemBdy: 4,
	selemFtrBar: 5,
	selemFtrLab: 6,
	selemLeftBtn: 7,
	selemRigthBtn: 8,
	selemHdrTbl: 9,
	selemLabelFirstCol:10,
	selemTotalTbl:11,
	selemSecTable:12,
	selemRowOdd:13,
	selemRowEven:14,
	selemSummary:15,
	selemSummaryFirstAns:16,
	selemSummaryAnswers:17,
	selemAddBtn: 18,
	selemRemoveBtn: 19,
	selemFinishBtn:20,
	selemGroupBtn: 21
}

//OMMC 2016-02-12: Agregada la lista para elementos de las preguntas, sirven para definir las clases para pintar los estilos
QElemTypes = {
	qelemLabTab: 1,			//Table del label de la pregunta
	qelemLabTR: 2,			//TR del label de la pregunta
	qelemLabTD: 3,			//TD del label de la pregunta
	qelemLabCont: 4,		//Div contenedor del label de la pregunta
	qelemLabElem: 5,		//El label de la pregunta
	qelemQtnTab: 6,			//Table del input/select/area de la pregunta
	qelemQtnTR: 7,			//TR del input/select/area de la pregunta
	qelemQtnTD: 8,			//TD del input/select/area de la pregunta
	qelemQtnCont: 9,		//Contenedor del input/select/area de la pregunta
	qelemQtnElem: 10,		//La pregunta en sí
	qelemPicCont: 11,		//Contenedor de la fotografía opcional
	qelemCmtCont: 12,		//Contenedor del comentario
	qelemCmtLab: 13,		//El label del comentario
	qelemCmtElem: 14, 		//El comentario en sí
	qelemArea: 15,
	qelemQtnOpSelec:16,		//Opción seleccionada 
	qelemQtnInput:17		//Input adicional de captura en preguntas de selección múltiple (vertical y horizontal)
}

//JAPR 2016-03-07: Agregada la lista para los elementos del resto de las ventanas y objetos en general
/* Todos los elementos que se refieren a una página específica utilizan como selector el id de la página, que convenientemente es el mismo que el objeto que contiene sus elements
Utiliza en general los mismos valores base de SElemTypes, pero luego de ellos los extiende para los elementos específicos de cada ventana a partir del índice 50 para dejar un margen
por si se agregan nuevos elementos a las partes comunes de la sección
*/
QElementGen = {
	loginPage: {			//Página de login
		logoImg: 1,			//El logo que se visualiza en la ventana
		frameList: 2,		//La <li> que envuelve a los inputs y tiene borde
		signInLabel: 3,		//La etiqueta de Login
		userLabel: 4,		//La etiqueta de Usuario
		userInput: 5,		//El campo de usuario
		pwdLabel: 6,		//La etiqueta de password
		pwdInput: 7,		//El capmpo de password
		signInButton: 8,	//El botón para Login
	}
}

//JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
/* Agregado el enumerado para configuraciones que usarán los valores de Si, No o Predeterminado (heredar de la configuración global), de manera que se puedan estandarizar y buscar
fácilmente en el código. Este enumerado sólo aplica a configuraciones que generalmente son un CheckBox, ya que las que son combos con múltiples valores no se pueden estandarizar de
esta manera
*/
SettingValues = {
	Default: -1,			//La configuración se tomará de las configuraciones default, ya que no fue personalizada en el objeto, así que depende del global si se habilita/deshabilita
	No: 0,					//La configuración de este objeto deshabilita la funcionalidad
	Yes: 1					//La configuraciónd e este objeto habilita la funcionalidad
}

//OMMC 2016-10-11: Agregados los estados para los mensajes push, estos estados son idénticos a los que tiene el server, deben ser iguales ya que las notificaciones cambiarán de estado
//de manera constante
PushMsgStatus = {
	Pending: 0,
	Sent: 1,
	Error: 2,
	Reprocess: 3,
	Read: 4,
	Deleted: 5
}

//JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
DataSourceTypes = {
	Manual: 1,
	Excel: 2,
	eBavel: 3,
	DropBox: 4,
	GoogleDrive: 5,
	FTP: 6,
	HTTP: 7,
	Table: 8
}

//JAPR 2018-10-18: Integrado los Mapas de Apple (#64ISFM)
MapsAPIType = {
	Google: 0,
	Mapkit: 1
}
//JAPR

//****************************************************************************************************************************************
//****************************************************************************************************************************************
/* Constantes y configuraciones default
*/
//****************************************************************************************************************************************
//****************************************************************************************************************************************

//Constantes
var myGreen = '#80c46d';
var myOrange = '#ee7600';
var myGray = '#949494';
var myBlue = '#5E87B0';
var myBlack = '#000000';
//JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
var formProduction = 0;
var formDevelopment = 1;
var formProdWithDev = 2;

//JAPR 2015-05-26: Rediseñado el Admin con DHTMLX
var myQuestionSelCol = '#A1CEED';
//JAPR
var lsPrefix = 'EForms';
var lsLogPrefix = 'logEForms';
//var clTabBorder = '#006600';
var clTabBorder = 'transparent';
var msDefaultDateFmt = 'yyyy/MM/dd';
//JAPR 2013-11-26: Modificado el timeout de refresh del GPS a 30 segundos (300000 -> 30000)
var mlGPSCheckInterval = 33000;					//Tiempo para el SetInterval en milisegundos
//JAPR 2013-12-09: Modificado para que el tiempo sin actualizar el GPS sea diferente al intervalo de revisión del mismo
var mlGPSCheckAfterNSecondsWOChange = 30000;	//Tiempo mínimo debe transcurrir sin haber actualizaro los datos del GPS para forzar a reiniciar el GPS Watch (verificado cada mlGPSCheckInterval segundos)
var myDefaultSurveyWidth = 100; //Conchita 2015-03-25 se cambio a 480 el default //Tamaño de cada menu de formas (cada recuadro)
var myDefaultSurveyHeight = 200;
var myDefaultSyncWidth = 150;
//JAPR
var mySyncButtonColor = '#b30000';
//JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
var myDefaultFormsWidth = 300;
var myDefaultFormsHeight = 200;
var myDefaultFormsTextPos = FormsTextLayOutPos.svtxOverBottom;
var myDefaultFormsTextSize = '30%';
//JAPR 2014-11-26: Validado que se respeten los valores previamente capturados cuando se vuelven a cargar los valores
//del catálogo a partir de filtros dinámicos, tanto en preguntas Simple choice como en secciones Inline y dinámicas
var msCFSep = '_eFSep_';
//JAPR 2016-05-03: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
//Se genera el template default que contiene una referencia a una posible imagen (si no existe, se reemplazará por cadena vacía) y la etiqueta de la opción de respuesta principal, así
//como una variable para substitución de tantos atributos adicionales como sea necesario para que dinámicamente se genere según la configuración de cada pregunta
var msDefaultOptionsTemplate = "<div class='metroTable'><div class='metroImgCell' style='display:#imageDisplay#;#imageSize#'>#displayImage#</div><div class='metroDataCell'><label class='metroMainAttr'>#name#</label>#additionalAttr#</div></div>";
//JAPR

//JAPR 2016-03-07: Agregada la variable/constante para generación de previews (sólo puede estar activada desde la consola para generación de HTML que será reemplazado como preview
//default del App, una aplicación de producción NO debe tener esta variable activada pues agregará clases y atributos que no son requeridos (variable strPreview dentro de la generación de HTML)
var gbGeneratePreview = false;
//JAPR 2016-06-27: Agregada la variable del modo de depuración, cuyo primer uso será impedir que el GPSWatch se reinicie cada vez que se cumple el intervalo de revisión de actividad,
//ya que eso es un problema generado en ciertos dispositivos pero no aplica para modo Web, si bien la funcionalidad debe ser así, para depurar errores es molesto estar viendo la misma
//serie de mensaje pasar por la consola, en especial cuando no está relacionado con el funcionamiento del GPS, así que se podrá activar esta variable para bloquear la reactivación del
//GPS Watch sólo si se está depurando (NO cambiar nunca el valor default, siempre deberá ser false y se deben hacer ajustes para depuración sólo cuando sea true manualmente desde un
//ambiente de desarrollo con la consola)
var gbDebugMode = false;
//OMMC 2016-11-02: Agregada la variable  para revisar notificaciones push si el app es arranacada por la notificación misma
var gbColdStartInit = false;
//JAPR 2016-11-02: Agregada la lectura del locale del dispositivo
var gsLocale = '';								//Idioma del dispositivo leído mediante el plug-in de globalization, se auto-asigna durante OnBodyLoad antes de invocar a startUpApp
//JAPR 2017-03-02: Corregido un bug, hay un límite para la cantidad de archivos que se pueden mantener referenciados, así que si se intenta el fileTransfer de una gran cantidad
//de archivos puede llegar a un punto donde se sobrepase el límite y eso causa que el App se muera, dependiendo del dispositivo el App se cierra o se queda pasmada dando el efecto
//visualmente como si no terminara de descargar aún las definiciones (#EGLFHM)
var giDefaultMaxAsyncFileProcessOpened = 30;	//Indica cuántos archivos máximos pueden estar siendo procesados al mismo tiempo para limitar las posibilidades de exceder el máximo permitido por el dispositivo/operativo y evitar que el App se muera
//JAPR 2018-12-26: Modificado el proceso de carga de definiciones para invocar a un proceso recursivo que serializará la escritura de los archivos locales tal como sucede con las
//imágenes para evitar que ocurran errores de escritura (#P1GL0L)
var giDefaultMaxAsyncFileWriteOpened = 30;
var giDefaultDefinitionDownloadAttempts = 3;
//JAPR 2019-02-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
var giDefaultSketchWidth = 500;
var giDefaultSketchHeight = 500;
var giDefaultSketchItemWidth = 32;
var giDefaultSketchItemHeight = 32;

//@JAPR 2018-09-26: Validado que todo upload realizado al server busque explícitamente la respuesta de que todo ok, de lo contrario se asumirá que hubo algún tipo de error, esto
//debido a que se habían perdido imágenes al realizar uploads con display_errors = 0, cuando ocurrían errores de php pero aún así regresaban un http code 200 en lugar de un 500 (#961S7M)
WebServiceVersions = {
	esvHTTPCode200RealOkCheck: '6.02028', 
	mustCheckOkResponse: function() {
		if ( !objSettings || !objSettings.webServiceVersion ) {
			return false;
		}
		
		return ( parseFloat(objSettings.webServiceVersion) >= parseFloat(this.esvHTTPCode200RealOkCheck) );
	}
}

//Array de colores posibles a utilizar, se usarán en diferentes puntos para el rendering del OpenLayer, por lo tanto NO pueden ser infinitos
var goDefaultSketchColorsArray = [
	{color: '0060ff', colorArray: [0, 96, 255, 1]},	//azul
	{color: '000000', colorArray: [0, 0, 0, 1]},	//negro
	{color: 'ffffff', colorArray: [255, 255, 255, 1]},	//blanco
	{color: 'fe0000', colorArray: [254, 0, 0, 1]},	//rojo
	{color: 'ffde00', colorArray: [255, 222, 0, 1]},	//amarillo
	{color: '727272', colorArray: [114, 114, 114, 1]},	//gris
	{color: 'ff8a00', colorArray: [255, 138, 0, 1]},	//naranja
	{color: '2fc501', colorArray: [47, 197, 1, 1]}	//verde
];
gbDefaultSketchColorsArrayWithStyles = false;

var garrSketchPopUpPosition = [30, 190, 30, 30];
var gsSketchItemsTemplate = "<div class='metroTable'><div class='metroSketchItems' style='display:#imageDisplay#;#imageSize#'>#displayImage#</div><div class='metroDataCell'><label class='metroMainAttr'>#name#</label></div></div>";
//JAPR
