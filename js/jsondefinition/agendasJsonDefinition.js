var bitamApp = bitamApp || {};
$.extend(bitamApp.formsSections, {
	
	Agendas: {
		
		id: 'FRM_AGENDAS',
		section_id: '1',
		sectionView: 'FRMW_AGENDAS',
		
		getSectionDefinition: function () {
			var self = this;
			//var sectionDefinition = self._getStaticSectionDefinition();
			var sectionDefinition = self._getSectionDefinition();
			return sectionDefinition;
		},
		
		_getSectionDefinition: function () {
			var self = this;
			var sectionDefinition = JSON.parse('{ \
				"FRM_AGENDAS": { \
					"id": "FRM_AGENDAS", \
					"id_section": "1", \
					"name": "FRM_AGENDAS", \
					"sectionName": "Application", \
					"description": "Application", \
					"item": "FRM_AGENDAS", \
					"filtered": true, \
					"isAppSection": false, \
					"formsTemplate": "", \
					"payload": { \
					} \
				} \
			}');
			var payload = self._getSectionDefinitionPayload();
			$.extend(sectionDefinition.FRM_AGENDAS.payload, payload);
			return sectionDefinition;
		},
		
		_getSectionDefinitionPayload: function () {
			var self = this;
			var payload = {};
			var fieldsForms = self._getSectionDefinitionFieldsForms();
			var views = self._getSectionDefinitionViews();
			$.extend(payload, fieldsForms, views);
			return payload;
		},
		
		_getSectionDefinitionFieldsForms: function () {
			var self = this;
			var fieldsFilterBox = self._getSectionDefinitionViewFieldsFilterBox();
			var fieldsForm = {
				"fieldsForm": fieldsFilterBox.fieldsFilterBox
			};
			return fieldsForm;
		},
		
		_getSectionDefinitionViews: function () {
			var self = this;
			var views = JSON.parse(
				'{"views": { \
					"FRMW_AGENDAS": { \
						"id": "FRMW_AGENDAS", \
						"id_view": "1", \
						"name": "FRMW_AGENDAS", \
						"label": "Application", \
						"boxFilter": false, \
						"filtered": true \
					} \
				}}'
			);
			var viewsFieldsList = self._getSectionDefinitionViewFieldsList();
			var viewsFieldsFilterBox = self._getSectionDefinitionViewFieldsFilterBox();
			$.extend(views.views.FRMW_AGENDAS, viewsFieldsList, viewsFieldsFilterBox);
			return views;
		},
		
		_getSectionDefinitionViewFieldsList: function () {
			var self = this;
			var fieldsFilterBox = self._getSectionDefinitionViewFieldsFilterBox();
			var fieldsList = {
				"fieldsList": fieldsFilterBox.fieldsFilterBox
			};
			return fieldsList;
		},
		
		_getSectionDefinitionViewFieldsFilterBox: function () {
			var self = this;
			var fieldsFilterBox = {
				"fieldsFilterBox": new Array()
			};
			fieldsFilterBox.fieldsFilterBox[0] = self._getUsersDefinition();
			fieldsFilterBox.fieldsFilterBox[1] = self._getFechaDefinition();
			fieldsFilterBox.fieldsFilterBox[2] = self._getHoraDefinition();
			fieldsFilterBox.fieldsFilterBox[3] = self._getCatalogDefinition();
			fieldsFilterBox.fieldsFilterBox[4] = self._getAttributoDefinition();
			return fieldsFilterBox;
		},
		
		//Users
		_getUsersDefinition: function () {
			return bitamApp.getFormsFieldFormDefinition({
				id_fieldform: 101,
				label: 'Users',
				section_name: this.id,
				section_id: this.section_id
			});
		},
		
		//fecha
		_getFechaDefinition: function () {
			return bitamApp.getFormsFieldFormDefinition({
				id_fieldform: 102,
				label: 'Users',
				section_name: this.id,
				section_id: this.section_id
			});
		},
		
		//hora
		_getHoraDefinition: function () {
			return bitamApp.getFormsFieldFormDefinition({
				id_fieldform: 103,
				label: 'Users',
				section_name: this.id,
				section_id: this.section_id
			});
		},
		
		//catalogo
		_getCatalogDefinition: function () {
			return bitamApp.getFormsFieldFormDefinition({
				id_fieldform: 104,
				label: 'Users',
				section_name: this.id,
				section_id: this.section_id
			});
		},
		
		//atributo
		_getAttributoDefinition: function () {
			return bitamApp.getFormsFieldFormDefinition({
				id_fieldform: 105,
				label: 'Users',
				section_name: this.id,
				section_id: this.section_id
			});
		},
		
		/*
		_getStaticSectionDefinition: function () {
			return '{ \
				"FRM_AGENDAS": { \
					"id": "FRM_AGENDAS", \
					"id_section": "1", \
					"name": "FRM_AGENDAS", \
					"sectionName": "Application", \
					"description": "Application", \
					"item": "FRM_AGENDAS", \
					"filtered": true, \
					"isAppSection": false, \
					"formsTemplate": "", \
					"payload": { \
						"fieldsForm": [{ \
							"id_fieldform": "1", \
							"id": "createdUser_FRM_AGENDAS", \
							"name": "createdUser_FRM_AGENDAS", \
							"label": "createdUser", \
							"tagname": "widget_field_alphanumeric", \
							"saveHistory": 0, \
							"keyfield": 0, \
							"owner": 0, \
							"section_id": "1", \
							"sectionName": "FRM_AGENDAS", \
							"description": "" \
						}], \
						"views": { \
							"FRMW_AGENDAS": { \
								"id": "FRMW_AGENDAS", \
								"id_view": "1", \
								"name": "FRMW_AGENDAS", \
								"label": "Application", \
								"boxFilter": false, \
								"filtered": true, \
								"fieldsFilterBox": [], \
								"fieldsList": [{ \
									"id": "FFRMS_52EA8233356B2", \
									"name": "FFRMS_52EA8233356B2", \
									"label": "Application", \
									"type": "text", \
									"contentEditable": true, \
									"format": "", \
									"position": { \
										"position": "" \
									}, \
									"sortableKey": "1", \
									"order": "1", \
									"section_id": "1", \
									"sectionName": "FRM_AGENDAS", \
									"id_fieldlist": "36514", \
									"customField": "", \
									"isCalculated": false, \
									"tagname": "widget_field_alphanumeric" \
								}] \
							} \
						} \
					} \
				} \
			}'
		}
		*/
	}
	
})