var bitamApp = bitamApp || {};
$.extend(bitamApp, {

	formsSections: {},

	getAppFormsDefinition: function () {
		var self = this;
		self._getAppFormsDefinition(self._getformsDefinition());
	},
	
	_getAppFormsDefinition: function (data) {
		var self = this;
		var res = new BuilderAppResponse(this);
		$.extend(res, data);
		self.application = res;
		self.application.appVersion = self.appVersion;
		var formsSections = self.formsSections;
		for (var skey in formsSections) {
			var aSection = {};
			$.extend(aSection, { 'FRM_AGENDAS': new Section(formsSections[skey].getSectionDefinition()[(formsSections[skey].id)]) });
			$.extend(self.application.payload.sections, aSection);
		}
	},
	
	_getformsDefinition: function () {
		var jsonDefinition = '{ \
			"applicationName": "Forms", \
			"codeName": "BAPP000000000000", \
			"id_app": "2", \
			"description": "Forms", \
			"version": 3.50002, \
			"db_origin": "FBM_BMD_1469", \
			"options": { \
				"mode": "kpi" \
			}, \
			"mdVersion": 4, \
			"language": "english", \
			"server": "http:\/\/kpionline4.bitam.com\/\/eBavel\/", \
			"payload": { \
				"sections": { \
				} \
			} \
		}';
		var definitionObj = JSON.parse(jsonDefinition);
		return definitionObj;
	},
	
	getFormsFieldFormDefinition: function (opts) {
		var self = this;
		var fieldFormCustomDefinition = {};
		var tmpO = {};
		tmpO.id_fieldform = opts.id_fieldform;
		tmpO.label = opts.label;
		tmpO.id = ("FFRMS_" + tmpO.label);
		tmpO.name = tmpO.id;
		tmpO.section_name = opts.section_name;
		tmpO.section_id = opts.section_id;
		tmpO.tagname = (opts.tagname) ? opts.tagname : WidgetType.Alphanumeric;
		if (opts.element) {
			tmpO.element = {};
			if (opts.element.defaultValue) {
				tmpO.element.defaultValue = opts.element.defaultValue;
			}
			if (opts.element.format) {
				tmpO.element.format = opts.element.format;
			}
		}
		fieldFormCustomDefinition = tmpO;
		var formsFieldForm = {};
		$.extend(formsFieldForm, self.templateFieldFormObject, fieldFormCustomDefinition);
		return formsFieldForm;
	},
	
	templateFieldFormObject: {
		"id_fieldform": "",
		"id": "",
		"name": "",
		"label": "",
		"tagname": "",
		"saveHistory": 0,
		"keyfield": 0,
		"owner": 1,
		"section_id": "",
		"sectionName": "",
		"description": "",
		"valid": {
			"required": 0
		},
		"element": {
			"defaultValue": "",
			"format": "",
			"edit": "0",
			"insert": "0"
		},
		"propagateParent": [],
		"wizPageId": "",
		"wizPageNum": 1,
		"columnsystem": false,
		"idCopy": 0,
		"showif_hide": [],
		"showif": [],
		"jumptoifq": [],
		"jumptoif": [],
		"sortableKey": "1",
		"ObserveInput": [],
		"CustomizationField": []
	}
	
});

var WidgetType = {
	Alphanumeric : 'widget_field_alphanumeric',
	Date : 'widget_field_date',
	Singlechoicelist : 'widget_selectlist',
	Multiplechoicehorizontal : 'widget_checkbox_multiple-horizontal',
	Multiplechoicevertical : 'widget_checkbox_multiple-vertical',
	Numeric : 'widget_field_numeric',
	Text : 'widget_textarea',
	Time : 'widget_field_time',
	Singlechoicehorizontal : 'widget_radiobutton_single-horizontal',
	Singlechoicevertical : 'widget_radiobutton_single-vertical',
	Sectionheadertitle : 'widget_secheader',
	Document : 'widget_file',
	SelectionHierarchy : 'widget_selectionhierarchy',
	Email : 'widget_field_email',
	Multiplechoicelist : 'widget_selectlist_multiple',
	Geolocation : 'widget_geolocation',
	Photo : 'widget_photo',
	Signature : 'widget_signature',
	UniqueID : 'widget_uniqueid',
	Phone : 'widget_callphone',
	Grid : 'widget_grid',
	URL : 'widget_url',
	Calculatedfield : 'widget_calculatedfield',
	Rating : 'widget_field_rating'
};