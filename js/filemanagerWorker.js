self.addEventListener('message', function(e) {
		var data = e.data;
		var objResponse = {errNum: 0, errDesc: ''};

		var strFileName = '';
		if (data.filename) {
			strFileName = data.filename;
		}
		
		switch (data.cmd) {
			case 'deleteFile':
				//No hay borrado de archivos, así que simplemente regresa vacio
				objResponse.responseText = strFileName + ' deleted';
				if (data.successFn) {
					objResponse.successFn = data.successFn;
				}
				if (data.errorFn) {
					objResponse.errorFn = data.errorFn;
				}
				self.postMessage(objResponse);
				break;

			case 'readFile':
				objResponse.responseText = strFileName + ' read';
				if (data.successFn) {
					objResponse.successFn = data.successFn;
				}
				if (data.errorFn) {
					objResponse.errorFn = data.errorFn;
				}
				//No hay lectura de archivos, así que simplemente regresa vacio
				self.postMessage(objResponse);
				break;
			
			case 'writeFile':
				objResponse.responseText = strFileName + ' wrote';
				if (data.successFn) {
					objResponse.successFn = data.successFn;
				}
				if (data.errorFn) {
					objResponse.errorFn = data.errorFn;
				}
				
				strMsg = '';
				if (data.msg) {
					strMsg = data.msg;
				}
				
				//Se escribe el archivo usando la librería de FileSaver, no regresa datos
				//Probablemente no funcione ya que no se tiene acceso a document desde un Worker
				try {
					var bb = new BlobBuilder();
					bb.append(strMsg);
					var blob = bb.getBlob("text/html;charset=" + document.characterSet);
					saveAs(blob, strFileName);
				} catch(e) {
					//Ignora este error
				}
				
				self.postMessage(objResponse);
				break;
			
			case 'getFolderFiles':
				objResponse.responseText = 'Files read';
				if (data.successFn) {
					objResponse.successFn = data.successFn;
				}
				if (data.errorFn) {
					objResponse.errorFn = data.errorFn;
				}
				self.postMessage(objResponse);
				break;
				
			default:
				objResponse.responseText = strFileName + ' not processed (unknown command)';
				if (data.successFn) {
					objResponse.successFn = data.successFn;
				}
				if (data.errorFn) {
					objResponse.errorFn = data.errorFn;
				}
				//No hay mas servicios
				self.postMessage(objResponse);
		};
	}, false);
