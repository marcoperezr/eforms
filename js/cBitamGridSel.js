var tempor;
var div_id_visible='none';
var div_id_padre='none';
var div_id_hijo='none';
var div_id_nieto='none';
var tempor_padre;
var xPos = 0;
var yPos = 0;

function set_Position(obj){
	xPos = findPosX(obj); 
	yPos = findPosY(obj);
}
function show_hide_sel(div_id){		
	
	if (document.getElementById(div_id).style.display == 'none')
	{
		// Si el que se va a mostrar es un padre entonces cerrar los hijos, nietos y divs existentes junto con su padre anterior
		if (div_id.search('id_padre') >= 0)
		{
			if (div_id_visible != 'none'){
				document.getElementById(div_id_visible).style.display = 'none';
				change_up_down_image(div_id_visible+'_img_up_down');
				div_id_visible = 'none';
			}
			if (div_id_nieto != 'none'){
				document.getElementById(div_id_nieto).style.display = 'none';
				change_up_down_image(div_id_nieto+'_img_up_down');
				div_id_nieto = 'none';
			}
			
			if (div_id_hijo != 'none'){
				document.getElementById(div_id_hijo).style.display = 'none';
				change_up_down_image(div_id_hijo+'_img_up_down');
				div_id_hijo = 'none';
			}
			
			if (div_id_padre != 'none'){
				document.getElementById(div_id_padre).style.display = 'none';
				change_up_down_image(div_id_padre+'_img_up_down');
			}
			div_id_padre = div_id;						
		}
		//	Pero si es un hijo entonces revisar si existía algun hijo y/o nieto abierto y cerrarlo entonces
		else 
			if (div_id.search('id_div_hijo') >= 0){
				if (div_id_nieto != 'none'){
					document.getElementById(div_id_nieto).style.display = 'none';
					change_up_down_image(div_id_nieto+'_img_up_down');
					div_id_nieto = 'none';
				}
		 		if (div_id_hijo != 'none'){
					document.getElementById(div_id_hijo).style.display = 'none';
					change_up_down_image(div_id_hijo+'_img_up_down');
		 		}
		 		div_id_hijo = div_id;
			}
			//	Pero si es un hijo entonces revisar si existía algun hijo y/o nieto abierto y cerrarlo entonces
			else
			 	if (div_id.search('id_div_nieto') >= 0){
					if (div_id_nieto != 'none'){
						document.getElementById(div_id_nieto).style.display = 'none';
						change_up_down_image(div_id_nieto+'_img_up_down');
					}
		 			div_id_nieto = div_id;
				}		 	
				//	Pero si lo que se va a mostrar no tiene nada que ver con padre e hijo entonces ocultar padres, hijos y nietos
				else{
					if (div_id_visible != 'none'){
						document.getElementById(div_id_visible).style.display = 'none';
						change_up_down_image(div_id_visible+'_img_up_down');
						div_id_visible = 'none';
					}
					if (div_id_nieto != 'none'){
						document.getElementById(div_id_nieto).style.display = 'none';
						change_up_down_image(div_id_nieto+'_img_up_down');
						div_id_nieto = 'none';
					}
			
					if (div_id_hijo != 'none'){
						document.getElementById(div_id_hijo).style.display = 'none';
						change_up_down_image(div_id_hijo+'_img_up_down');
						div_id_hijo = 'none';
					}
					if (div_id_padre != 'none'){
						document.getElementById(div_id_padre).style.display = 'none';
						change_up_down_image(div_id_padre+'_img_up_down');
						div_id_padre = 'none';
					}
					div_id_visible = div_id;
				}
		clearTimeout(tempor);
		clearTimeout(tempor_padre);
		
		document.getElementById(div_id).style.top = (yPos+17)+'px';
		document.getElementById(div_id).style.left = (xPos-134)+'px';				
		document.getElementById(div_id).style.display = 'inline';
		change_up_down_image(div_id+'_img_up_down');
		set_Tempor_2000(div_id);
	}
	else{
		// Si lo que se va a ocultar es un padre entonces cerrar los hijos y nietos abiertos si es que tiene					
		if (div_id.search('id_padre') >= 0){
			if (div_id_hijo != 'none'){
				document.getElementById(div_id_hijo).style.display = 'none';
				change_up_down_image(div_id_hijo+'_img_up_down');
				div_id_hijo = 'none';
			}
			if (div_id_nieto != 'none'){
				document.getElementById(div_id_nieto).style.display = 'none';
				change_up_down_image(div_id_nieto+'_img_up_down');
				div_id_nieto = 'none';
			}
			div_id_padre = 'none';
		}
		//	Si lo que se oculta es un hijo entonces cerrar los nietos
		else
			if (div_id.search('id_div_hijo') >= 0){
				if (div_id_nieto != 'none'){
					document.getElementById(div_id_nieto).style.display = 'none';
					change_up_down_image(div_id_nieto+'_img_up_down');
					div_id_nieto = 'none';
				}
				div_id_hijo = 'none';
			}
			// Si lo que se oculta es un nieto entonces asignar a none la variable
			else
				if (div_id.search('id_div_nieto') >= 0)
					div_id_nieto = 'none';
				else
					div_id_visible = 'none';
		document.getElementById(div_id).style.display = 'none';
		change_up_down_image(div_id+'_img_up_down');
		clearTimeout(tempor);
		clearTimeout(tempor_padre);
	}
}

function change_up_down_image(id_img_up_down){
	vaux = document.getElementById(id_img_up_down).src;
	if (vaux.search('images/basic_search.gif') >= 0)
		document.getElementById(id_img_up_down).src = 'images/basic_search.gif';
	else
		document.getElementById(id_img_up_down).src = 'images/basic_search.gif';
}

function change_image(src_image, img_selector, hint_sel, input_id, key_input){
	 document.getElementById(img_selector).src = src_image;
	 document.getElementById(img_selector).title = hint_sel;
	 document.getElementById(input_id).value = key_input;
	 if (input_id.indexOf('gallery_sel_') == 0)
	 {
		var serie_key = input_id.substring(12);					
		switch(key_input) 
		{
 			case 'line':
 			case 'lineCurve':
 			case 'lineStep':
 			case 'scatter': 			  			
 			case 'bubble':
 				document.getElementById('tr_id_marker_sel_'+serie_key).style.display = 'inline';
 				document.getElementById('tr_id_style_sel_'+serie_key).style.display = 'none';
 				break;
 			case 'bar': 			  			
 			case 'gantt':
 				document.getElementById('tr_id_marker_sel_'+serie_key).style.display = 'none';
 				document.getElementById('tr_id_style_sel_'+serie_key).style.display = 'inline';
 				break;
 			default:
 				document.getElementById('tr_id_marker_sel_'+serie_key).style.display = 'none';
 				document.getElementById('tr_id_style_sel_'+serie_key).style.display = 'none';
 			break; 			  			
		}
	}
	if (input_id.indexOf('hijo_grid_sel') == 0)
 	{		
		switch(key_input) 
		{
 			case '0':
 				document.getElementById('id_width_hor').disabled = true;
 				document.getElementById('id_width_hor').options[0].selected = true;
 				document.getElementById('id_width_ver').disabled = true;
 				document.getElementById('id_width_ver').options[0].selected = true;
 				break;
 			case '1':
 				document.getElementById('id_width_hor').disabled = false;
 				document.getElementById('id_width_hor').options[1].selected = true;
 				document.getElementById('id_width_ver').disabled = true;
 				document.getElementById('id_width_ver').options[0].selected = true;
 				break; 			 			
 			case '2':
 				document.getElementById('id_width_ver').disabled = false;
 				document.getElementById('id_width_ver').options[1].selected = true;
 				document.getElementById('id_width_hor').disabled = true;
 				document.getElementById('id_width_hor').options[0].selected = true;
 				break;
 			case '3':
 				document.getElementById('id_width_hor').disabled = false;
 				document.getElementById('id_width_hor').options[1].selected = true;
 				document.getElementById('id_width_ver').disabled = false;
 				document.getElementById('id_width_ver').options[1].selected = true;
 				break;
 			break; 			  			
		}
	}
}

function change_color(src_image, img_selector, hint_sel, input_id, key_input){
	 document.getElementById(img_selector).style.background = src_image;
	 document.getElementById(img_selector).title = hint_sel;
	 document.getElementById(input_id).value = key_input;
}


function set_Tempor(div_id){
  	div_hidden = div_id;
	if (document.getElementById(div_id).style.display == 'inline')
		if (div_id.search('id_padre') >= 0)
	      	tempor_padre = setTimeout("show_hide_sel(div_hidden);",500);
		else
			tempor = setTimeout("show_hide_sel(div_hidden);",500);
}

function set_Tempor_2000(div_id){
  	div_hidden = div_id;
	if (div_id.search('id_padre') >= 0)
	   	tempor_padre = setTimeout("show_hide_sel(div_hidden);",2000);
	else
	   	tempor = setTimeout("show_hide_sel(div_hidden);",2000);
}

function clear_Tempor(){
	clearTimeout(tempor);
	clearTimeout(tempor_padre);
}

function fnShowChooseColorDlg(color,param,path){	
	if (document.all) { //IE4 and up
		var args = new Array(3);
		args[0] = color;
		args[1] = window;
		args[2] = param;
		retVal = window.showModalDialog(
				path+'color_dialog.htm',args,
				'dialogHeight: 380px; dialogWidth: 245px; center: yes; scroll: No; help:  No; resizable: No; status:no;');		
	} else if (document.layers) {
	} else if (document.getElementById) { //mozilla
		var winRef;
		winRef = window.open(
				path+'color_dialog.htm?'+escape(color)+'&'+param,"_blank",
				'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=245, height=325');
	}
}

function OnChangeColor(color, other){
	if (color != '')
		if (color == -1)
			change_color('transparent', 'id_img_selection_'+other, 'Transparent', other, color);
		else
			change_color(color, 'id_img_selection_'+other, color, other, color);
}

function change_input(value_sel, input_id){
	document.getElementById(input_id).title = value_sel;
	document.getElementById(input_id).value = value_sel;
}


function asignItemToCombobox(value,key,id)
{	
	window.document.getElementById(id).value=key;
	var x_id= id.substring(1);
	var dim_id = id.substring(13);
	change_input(value, x_id); 
	show_hide_sel('id_div_'+dim_id);
}


function findPosX(obj)
  {
  	//alert('entro');
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    
    return curleft;
  }


 function findPosY(obj)
  {
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    
    return curtop;
  }
