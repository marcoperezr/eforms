//Returns an object given an id
function getObjectFromID(id){
	var theObject;
	if(document.getElementById)
		theObject=document.getElementById(id);
	else
		theObject=document.all[id];
	return theObject;
}


// This Function returns the Top position of an object
function getTop(theitem){
	var offsetTrail = theitem;
	var offsetTop = 0;
	while (offsetTrail) {
		offsetTop += offsetTrail.offsetTop;
		offsetTrail = offsetTrail.offsetParent;
	}
	if (navigator.userAgent.indexOf("Mac") != -1 && typeof document.body.leftMargin != "undefined") 
		offsetLeft += document.body.TopMargin;
	return offsetTop;
}

// This Function returns the Left position of an object
function getLeft(theitem){
	var offsetTrail = theitem;
	var offsetLeft = 0;
	while (offsetTrail) {
		offsetLeft += offsetTrail.offsetLeft;
		offsetTrail = offsetTrail.offsetParent;
	}
	if (navigator.userAgent.indexOf("Mac") != -1 && typeof document.body.leftMargin != "undefined") 
		offsetLeft += document.body.leftMargin;
	return offsetLeft;
}

function loadXMLDoc(url,readyStateFunction,async) 
{
	// branch for native XMLHttpRequest object
	if (window.XMLHttpRequest) {
		req = new XMLHttpRequest();
		req.onreadystatechange = readyStateFunction;
		req.open("GET", url, async);
		req.send(null);
	// branch for IE/Windows ActiveX version
	} else if (window.ActiveXObject) {
		req = new ActiveXObject("Microsoft.XMLHTTP");
		if (req) {
			if(readyStateFunction) req.onreadystatechange = readyStateFunction;
			req.open("GET", url, async);
			req.send();
		}
	}
}

function addEvent(obj, evType, fn){
 if (obj.addEventListener){
    obj.addEventListener(evType, fn, true);
    return true;
 } else if (obj.attachEvent){
    var r = obj.attachEvent("on"+evType, fn);
    return r;
 } else {
    return false;
 }
}
function removeEvent(obj, evType, fn, useCapture){
  if (obj.removeEventListener){
    obj.removeEventListener(evType, fn, useCapture);
    return true;
  } else if (obj.detachEvent){
    var r = obj.detachEvent("on"+evType, fn);
    return r;
  } else {
    window.status=("Handler could not be removed");
  }
}

function getViewportHeight() {
	if (window.innerHeight!=window.undefined) {
		return window.innerHeight;
		//var i=document.body.clientHeight;
		//return i;
	}
	if (document.compatMode=='CSS1Compat') return document.documentElement.clientHeight;
	if (document.body) return document.body.clientHeight; 
	return window.undefined; 
}
function getViewportWidth() {
	if (window.innerWidth!=window.undefined) {
		return window.innerWidth;
		//var i=document.body.offsetWidth;
		//return i;
	}
	if (document.compatMode=='CSS1Compat') return document.documentElement.clientWidth; 
	if (document.body) return document.body.clientWidth; 
	return window.undefined; 
}

function disableSave(){
	var tempButton=getObjectFromID("saveButton1");
	if(tempButton)
		tempButton.disabled=true;
	tempButton=getObjectFromID("saveButton2");
	if(tempButton)
		tempButton.disabled=true;		
}

function englishTime(thedate){
			var ampm = " AM";
			var hours = thedate.getHours()
			if(hours==0) hours=12;
			if (hours>12){
				var ampm = " PM";
				hours=hours-12
			}
			var minutes=thedate.getMinutes();
			if(minutes<10)
				minutes="0"+minutes;
			return hours+":"+minutes+ampm;
}

function englishDate(thedate){
	return (thedate.getMonth()+1)+"/"+thedate.getDate()+"/"+thedate.getFullYear();	
}

function dateFromField(englishdate,englishtime){
	var theyear= parseInt(englishdate.substring(englishdate.lastIndexOf("/")+1));
	var themonth= parseInt(englishdate.substring(0,englishdate.indexOf("/")))-1;
	var theday= parseInt(englishdate.substring(englishdate.indexOf("/")+1,englishdate.lastIndexOf("/")));
	var thedate= new Date(theyear,themonth,theday);
	if(englishtime){
		var thehour=parseInt(englishtime.substring(0,englishtime.indexOf(":")));
		var theminute=parseInt(englishtime.substring(englishtime.indexOf(":")+1,englishtime.indexOf(" ")));;
		var AMPM=englishtime.substring(englishtime.indexOf(" ")+1);
		if(AMPM=="PM")
			thehour+=12;
		else if (thehour==12)
			thehour=0;
		thedate.setHours(thehour,theminute);
	}
	return thedate;	
}

/* ----------------------------------------------------------------------------- */
