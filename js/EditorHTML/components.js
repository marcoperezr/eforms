  /*****************  LABEL  **************************/
  function cLabel (nTypeElement, sElementId)
  {
  	genericElement.apply(this, arguments);

  	// métodos (begin)
	this.restore = function (sParams, bRestore)
	{
		var regexp = new RegExp(AWComa,"gi");
		var bWSepProps = (sParams.indexOf(AWProps) != -1);
		if (bWSepProps) {
			var aMDM = sParams.split(AWProps);
			var aMD = aMDM[0].split(',');
			var top = aMD.length - 1;
		}
		else {
			var aMD = sParams.split(',');
		}
		if (bRestore) {
			this.element.Rep_id = aMD[0];
			this.element.id = this.element.Rep_id;
		}
		else {
			this.element.Rep_id = this.element.id;
		}

		var iPos = 1;
		if (bRestore) {
			this.element.Rep_name = aMD[iPos++].replace(regexp,',');
		}
		else {
			this.element.Rep_name = this.element.id;
			iPos++;
		}
		this.element.Rep_caption = aMD[iPos++].replace(regexp,',');

		if (bWSepProps) {
			aMD = aMDM[1].split(',');
			iPos = 0;
		}
		this.restoreProps(aMD, iPos);
		aMD = null;
	}

	this.save = function ()
	{
		var regexp = new RegExp(',',"gi");
		var sSave = this.element.Rep_id;
		sSave = sSave + ',' + this.element.Rep_name.replace(regexp,AWComa);
		sSave = sSave + ',' + this.element.Rep_caption.replace(regexp,AWComa);
		sSave = sSave + AWProps + this.saveProps();
		return sSave;
	}

	this.saveHTML = function ()
    {
		if (!this.element.Rep_visible)
		{
			return '';
		}
		
	   	var innerHTML='';
		if (this.element.Rep_caption != undefined)
		{
			innerHTML = this.element.Rep_caption;
		}
		
    	//HTML
    	var borderNone;
    	borderNone=true;
    	var strHTML = '<div id="'+this.element.id +
    					'" style="'+this.getStyle(borderNone) + '">'+
    					innerHTML +
    					'</div>';

    	return strHTML;
    }
  }
  cLabel.prototype = new genericElement();
  
/*****************  IMAGE  **************************/
function cImage (nTypeElement, sElementId)
{
  	genericElement.apply(this, arguments);

  	var oimg = document.createElement("img")
	oimg.style.height = "100%"
	oimg.style.width = "100%"
	oimg.id  = "Image"
	oimg.src = "images/transparent.gif"
	oimg.galleryImg = "no"
	oimg.setAttribute("displayMe", 1);
	oimg.style.zIndex = 0

	if (isIE) {
		oimg.attachEvent("ondragstart", function(evt){evt.returnValue = false;})
	}
	else {
		oimg.addEventListener("mousedown", function(evt){ if (bStopPropagation) evt.preventDefault()}, true)
	}
	this.element.innerHTML = '';
	this.element.style.zIndex = 0;
	this.element.appendChild(oimg)
	this.element.graph = oimg

  	// métodos (begin)
	this.restore = function (sParams, bRestore)
	{
		var regexp = new RegExp(AWComa,"gi");
		var bWSepProps = (sParams.indexOf(AWProps) != -1);
		if (bWSepProps) {
			var aMDM = sParams.split(AWProps);
			var aMD = aMDM[0].split(',');
			var top = aMD.length - 1;
		}
		else {
			var aMD = sParams.split(',');
		}
		if (bRestore) {
			this.element.Rep_id = aMD[0];
			this.element.id = this.element.Rep_id;
		}
		else {
			this.element.Rep_id = this.element.id;
		}

		var iPos = 1;
		if (bRestore) {
			this.element.Rep_name = aMD[iPos++].replace(regexp,',');
		}
		else {
			this.element.Rep_name = this.element.id;
			iPos++;
		}
		this.element.Rep_image = ParsePathImg(aMD[iPos++].replace(regexp,','));
		this.element.Rep_picstretched = getStrBool(aMD[iPos++]);
		this.element.Rep_sLinker = (bWSepProps) ? getRestoreElem(aMD,top,iPos++,'').replace(regexp,',') : '';

		if (bWSepProps) {
			aMD = aMDM[1].split(',');
			iPos = 0;
		}
		this.restoreProps(aMD, iPos);
		this.setImage();
		aMD = null;
	}

	this.save = function ()
	{
		var regexp = new RegExp(',',"gi");
		var sSave = this.element.Rep_id;
		sSave = sSave + ',' + this.element.Rep_name.replace(regexp,AWComa);
		sSave = sSave + ',' + this.element.Rep_image.replace(regexp,AWComa);
		sSave = sSave + ',' + getBoolStr(this.element.Rep_picstretched);
		sSave = sSave + ',' + this.element.Rep_sLinker.replace(regexp,AWComa);
		sSave = sSave + AWProps + this.saveProps();
		return sSave;
	}

	this.setImage = function () {
		this.element.graph.src = getValidImageSrc(this.element.Rep_image);
		if (this.element.Rep_picstretched) {
			this.element.graph.style.height = "100%"
			this.element.graph.style.width = "100%"
		}
		else {

			this.element.graph.style.height = "auto"
			this.element.graph.style.width = "auto"
		}
	}
	
	this.saveHTML = function ()
    {
		if (!this.element.Rep_visible)
		{
			return '';
		}
		
    	//HTML
    	var borderNone;
    	borderNone=true;
    	if (this.element.Rep_picstretched)
    	{
    		var strHTML = '<div id="'+this.element.id + '" style="'+this.getStyle(borderNone) + '"><img src="' + getValidImageSrc(this.element.Rep_image) +
    		'" style="height:100%; width:100%; z-index: 0;" ></div>';
    	}
    	else
    	{
    		var strHTML = '<div id="'+this.element.id + '" style="'+this.getStyle(borderNone) + '"><img src="' + getValidImageSrc(this.element.Rep_image) +
    		'" style="height:auto; width:auto; z-index: 0;" ></div>';
    	}
    	return strHTML;
    }
  }
  cImage.prototype = new genericElement();

  /*****************  Line  **************************/
  function cLine (nTypeElement, sElementId)
  {
  	genericElement.apply(this, arguments);

  	var oimg = document.createElement("img")
	oimg.style.height = "100%"
	oimg.style.width = "100%"
	oimg.id  = "Line"
	oimg.src = "images/transparent.gif"
	oimg.galleryImg = "no"

	if (isIE) {
		oimg.attachEvent("ondragstart", function(evt){evt.returnValue = false;})
	}
	else {
		oimg.addEventListener("mousedown", function(evt){ if (bStopPropagation) evt.preventDefault()}, true)
	}
	this.element.innerHTML = '';
	this.element.style.zIndex = 0;
	this.element.appendChild(oimg)
	this.element.graph = oimg
	this.element.style.backgroundColor    = "#FFFFFF";
	this.element.style.backgroundPosition = "top";
	this.element.style.backgroundRepeat   = "repeat";
	var myvar = this
	this.element.afterResize = function ()
	{
		myvar.makeImage();
	}

  	// métodos (begin)
	this.restore = function (sParams, bRestore)
	{
		var regexp = new RegExp(AWComa,"gi");
		var bWSepProps = (sParams.indexOf(AWProps) != -1);
		if (bWSepProps) {
			var aMDM = sParams.split(AWProps);
			var aMD = aMDM[0].split(',');
			var top = aMD.length - 1;
		}
		else {
			var aMD = sParams.split(',');
		}
		if (bRestore) {
			this.element.Rep_id = aMD[0];
			this.element.id = this.element.Rep_id;
		}
		else {
			this.element.Rep_id = this.element.id;
		}

		var iPos = 1;
		if (bRestore) {
			this.element.Rep_name = aMD[iPos++].replace(regexp,',');
		}
		else {
			this.element.Rep_name = this.element.id;
			iPos++;
		}
		//this.element.Rep_image = aMD[iPos++].replace(regexp,',');
		d = new Date();
		this.element.Rep_image = repository+'/temp/'+'lineImg'+Math.round(Math.random()*10000)+'_'+Math.round(Math.random()*10000)+'_'+d.getMilliseconds()+'.png';
		this.element.Rep_picstretched = getStrBool(aMD[iPos++]);
		this.element.Rep_sLinker = (bWSepProps) ? getRestoreElem(aMD,top,iPos++,'').replace(regexp,',') : '';

		if (bWSepProps) {
			aMD = aMDM[1].split(',');
			iPos = 0;
		}
		this.restoreProps(aMD, iPos);
		this.makeImage();
		//this.setImage();
		aMD = null;
	}

	this.makeImage = function ()
	{
		var param = '';
		param = this.element.Rep_width+'@'+this.element.Rep_height+'@';
		param+= Convert_Hex_To_Dec(this.element.Rep_BackColor)+'@'+Convert_Hex_To_Dec(this.element.Rep_ForeColor)+'@'+this.element.Rep_FontName;
		var fondo = "URL(makeLineImage.php?file="+this.element.Rep_image+"&param="+param+")";
		this.element.style.backgroundImage    = fondo;
	}

	this.save = function ()
	{
		var regexp = new RegExp(',',"gi");
		var sSave = this.element.Rep_id;
		sSave = sSave + ',' + this.element.Rep_name.replace(regexp,AWComa);
		sSave = sSave + ',' + this.element.Rep_image.replace(regexp,AWComa);
		sSave = sSave + ',' + getBoolStr(this.element.Rep_picstretched);
		sSave = sSave + ',' + this.element.Rep_sLinker.replace(regexp,AWComa);
		sSave = sSave + AWProps + this.saveProps();
		return sSave;
	}

	this.setImage = function () {
		this.element.graph.src = getValidImageSrc(this.element.Rep_image);
		if (this.element.Rep_picstretched) {
			this.element.graph.style.height = "100%"
			this.element.graph.style.width = "100%"
		}
		else {

			this.element.graph.style.height = "auto"
			this.element.graph.style.width = "auto"
		}
	}
  }
  cLine.prototype = new genericElement();
  
  /*****************  BUTTON  **************************/
  function cButton (nTypeElement, sElementId)
  {
  	genericElement.apply(this, arguments);

  	// métodos (begin)
	this.restore = function (sParams, bRestore)
	{
		var regexp = new RegExp(AWComa,"gi");
		var bWSepProps = (sParams.indexOf(AWProps) != -1);
		if (bWSepProps) {
			var aMDM = sParams.split(AWProps);
			var aMD = aMDM[0].split(',');
			var top = aMD.length - 1;
		}
		else {
			var aMD = sParams.split(',');
		}
		if (bRestore) {
			this.element.Rep_id = aMD[0];
			this.element.id = this.element.Rep_id;
		}
		else {
			this.element.Rep_id = this.element.id;
		}

		var iPos = 1;
		if (bRestore) {
			this.element.Rep_name = aMD[iPos++].replace(regexp,',');
		}
		else {
			this.element.Rep_name = this.element.id;
			iPos++;
		}
		this.element.Rep_caption = aMD[iPos++].replace(regexp,',');
		this.element.Rep_pivotvalue = aMD[iPos++].replace(regexp,',');
		this.element.Rep_optionid  = aMD[iPos++].replace(regexp,',');
		if (bWSepProps) {
			aMD = aMDM[1].split(',');
			iPos = 0;
		}
		this.restoreProps(aMD, iPos);
		aMD = null;
	}

	this.save = function ()
	{
		var regexp = new RegExp(',',"gi");
		var sSave = this.element.Rep_id;
		sSave = sSave + ',' + this.element.Rep_name.replace(regexp,AWComa);
		sSave = sSave + ',' + this.element.Rep_caption.replace(regexp,AWComa);
		sSave = sSave + ',' + this.element.Rep_pivotvalue.replace(regexp,AWComa);
		sSave = sSave + ',' + this.element.Rep_optionid.replace(regexp,AWComa);
		sSave = sSave + AWProps + this.saveProps();
		return sSave;
	}
	
	this.saveHTML = function ()
    {
		if (!this.element.Rep_visible)
		{
			return '';
		}
		
	   	var innerHTML='';
		if (this.element.Rep_caption != undefined)
		{
			innerHTML = this.element.Rep_caption;
		}
		
    	//HTML
    	//debugger;
    	if(this.element.Rep_optionid==0)
    	{
	    	if(this.element.Rep_Picture!="")
	    	{
    		var strHTML = '<img id="'+ this.element.id +
    			'" style="'+this.getStyle() + '" title='+innerHTML+' onclick=objApp.shutDownApp()>';
	    	}
	    	else
	    	{
    		var strHTML = '<input type=button id="'+ this.element.id +
    			'" style="'+this.getStyle() + '" value="'+innerHTML+'" data-role="none" onclick=objApp.shutDownApp()>';
	    	}
    	}
    	else
    	{
	    	if(this.element.Rep_Picture!="")
	    	{
    		var strHTML = '<img id="'+ this.element.id +
    			'" style="'+this.getStyle() + '" title='+innerHTML+' onclick=selSurvey.changeSectionByID('+this.element.Rep_optionid+')>';
	    	}
	    	else
	    	{
    		var strHTML = '<input type=button id="'+ this.element.id +
    			'" style="'+this.getStyle() + '" value='+innerHTML+' data-role="none" onclick=selSurvey.changeSectionByID('+this.element.Rep_optionid+')>';
	    	}
    	}
    	
    	return strHTML;
    }
  }
  cButton.prototype = new genericElement();
