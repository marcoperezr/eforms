var aSort = Array();
var img_active = '';
var bNewComp = false;
var bValidImg = true;

function OnStart(){
	GetElementWithId('botOK').value = ML[69];
	GetElementWithId('botCancel').value = ML[70];
	if (window_dialogArguments[2] != undefined) {
		bNewComp = window_dialogArguments[2];
	}
	if (bNewComp) {
		GetElementWithId('botBack').value = ML[174];
		GetElementWithId('botBack').style.display = 'inline';
	}
	if (bIsIE) {
		if (GetElementWithId('botBack')) { GetElementWithId('botBack').className = 'NbuttonIE'; }
		if (GetElementWithId('botOK')) { GetElementWithId('botOK').className = 'NbuttonIE'; }
		if (GetElementWithId('botCancel')) { GetElementWithId('botCancel').className = 'NbuttonIE'; }
	}
	if (IsThatVersion("Chrome")) {
		GetElementWithId('tbButtons').style.width = document.body.offsetWidth-25;
		GetElementWithId('id_div_header').style.width = document.body.offsetWidth-25;
		GetElementWithId('id_div').style.width = document.body.offsetWidth-25;
	}
	else {
		GetElementWithId('tbButtons').style.width = document.body.offsetWidth-15;
		GetElementWithId('id_div_header').style.width = document.body.offsetWidth-15;
		GetElementWithId('id_div').style.width = document.body.offsetWidth-15;
	}
	if (window_dialogArguments[3] != undefined) {
		bValidImg = window_dialogArguments[3];
	}
	Show_Order(-1, true);
}
function Show_Order(key_order, is_desc){
	reset_values();
	var what_dir, sPosImgTB=(bIsIE)?'middle':'';;
	img_ad_type = '<img src="images/arrow.gif" align="'+sPosImgTB+'">';
	img_ad_name = '<img src="images/arrow.gif" align="'+sPosImgTB+'">';
	if (is_desc == true)
	{
		img_ad = '<img src="images/arrow_down.gif" align="'+sPosImgTB+'">';
		what_dir = 'false';
	}
	else
	{
		img_ad = '<img src="images/arrow_up.gif" align="'+sPosImgTB+'">';
		what_dir = 'true';
	}
	switch (key_order){
		case 1:
			img_ad_name = img_ad;
			break;
		case 2:
			img_ad_type = img_ad;
			break;
	}
	var Name_Type = ML[360];
	var Name_Img = ML[74];
	var table_enc_HTML = '<thead><tr><td width=10% style="cursor:pointer; background-color:#dfd2c0;" class=dataLabel>&nbsp;</td><td width=70% style="cursor:pointer; background-color:#dfd2c0;" class=dataLabel onclick="Show_Order(1,'+what_dir+')">'+Name_Img+' '+img_ad_name+'</td><td width=20% style="cursor:pointer; background-color:#dfd2c0;" class=dataLabel onclick="Show_Order(2,'+what_dir+')">'+Name_Type+' '+img_ad_type+'</td></tr></thead>';
	table_HTML = '<table cellspading=0 cellspacing=0 width=100% class="tabForm">' + table_enc_HTML + '</table>';
	GetElementWithId("id_div_header").innerHTML = table_HTML;
	var table_info_HTML = Sort_by_Col(key_order, is_desc);
	table_HTML = '<table cellspading=0 cellspacing=0 width=100% class="tabForm">' + table_info_HTML + '</table>';
	GetElementWithId("id_div").innerHTML = table_HTML;
}
function Sort_by_Col(key_sort, is_desc){
	var tam_ar = img_names.length;
	var table_info = Array();
	var table_sub_info = Array();
	// Ordenar
	switch (key_sort){
		case 1:	//	Name / Nombre
			aSort = Array();
			for (i=0; i < tam_ar; i++) {
				aSort[i] = img_names[i]+'|REI|'+img_types[i]+'|REI|'+url_images[i];
			}
			aSort.sort();
			if (is_desc == true)
				aSort.reverse();
			var sort_len = aSort.length;
			for (i=0; i < sort_len; i++) {
				var kd = aSort[i].split('|REI|');
				img_names[i] = kd[0];
				img_types[i] = kd[1];
				url_images[i] = kd[2];
			}
			break;
		case 2:	//	Type	/	Tipo de Archivo
			aSort = Array();
			for (i=0; i < tam_ar; i++) {
				aSort[i] = img_types[i]+'|REI|'+img_names[i]+'|REI|'+url_images[i];
			}
			aSort.sort();
			if (is_desc == true)
				aSort.reverse();
			var sort_len = aSort.length;
			for (i=0; i < sort_len; i++) {
				var kd = aSort[i].split('|REI|');
				img_names[i] = kd[1];
				img_types[i] = kd[0];
				url_images[i] = kd[2];
			}
			break;
	}
	// Cargar datos
	for (i=0; i<tam_ar;i++)
	{
		onmouse_ov_out = 'onmouseover="change_color_over(this);" onclick="set_color(this);" onmouseout="change_color_out(this,';
		if (i%2 == 0 )
		{
			class_td = 'message_color_1';
			onmouse_ov_out = onmouse_ov_out + "'transparent');"+'"';
		}
		else
		{
			class_td = 'message_color_2';
			onmouse_ov_out = onmouse_ov_out + "'#FFFFFF');"+'"';
		}

		table_info[i] = '<tr '+onmouse_ov_out+' class='+class_td+' id=img_'+i+
						'><td width=11% align=center><img src="'+getValidImageSrc(url_images[i])+
						'" width="15px" height="15px"></td><td width=72%>'+img_names[i]+
						'</td><td width=17%>'+img_types[i]+'</td></tr>';
	}
	return table_info.join('');
}
function change_color_over(celda){
	if (celda.id != img_active)
		celda.style.backgroundColor="#dfd2c0";
}
function change_color_out(celda, color){
	if (celda.id != img_active)
		celda.style.backgroundColor=color;
}
function set_color(celda){
	if (celda.id != img_active)
	{
		// Asignar color a renglon donde se dio click
		celda.style.backgroundColor="brown";
		celda.style.color="white";
		if (img_active != '')
		{
			// Asignar el color ke tenia el renglon anterior
			if (document.getElementById(img_active).className == 'message_color_1')
				document.getElementById(img_active).style.backgroundColor="transparent";
			else
				document.getElementById(img_active).style.backgroundColor="#FFFFFF";
			document.getElementById(img_active).style.color="#000000";
		}
		img_active = celda.id;
	}
}
function reset_values(){
	img_active = '';
}

function On_Ok(){
	if (img_active == '' && bValidImg) {
		alert(ML[364]);
	}
	else{
		var sRetVal = '';
		if (img_active != '') {
			sRetVal = url_images[img_active.split('_')[1]];
		}

    setDialogReturnValue(sRetVal);
    closeDialog();
	}
}

function OnPrevious(){
  setDialogReturnValue('Previous');
  closeDialog();
}