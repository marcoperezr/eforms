var isIE = navigator.userAgent.indexOf("MSIE") != -1;
var nMovingBorderWidth = 7
var oMovingObject
var oDivOrigValid = null;
var nResizeMinSize = 3
var bRightClick = false;
var sWhatSource = 'report';
var bCtrlPressed = false;
var aMovingObject = new Array();
var bStopPropagation = true;

function registerMovingObject(odiv)
{
  if (isIE)
  {
    odiv.attachEvent("onmousedown", prepareMovingObject)
  }
  else
  {
    odiv.addEventListener("mousedown", prepareMovingObject, true)
  }
}

function prepareMovingObject(evt)
{
//window.parent.document.title = 'prepareMovingObject';
  bRightClick = false;
  if (isIE)
  {
    evt.cancelBubble = true
    if (evt.button != 1 && evt.button != 2) { return }
    if (evt.button == 2) { bRightClick = true; }
    if (!bRightClick) {
    	if (bitamRootMenu != null) {
			bitamRootMenu.close();
    	}
    }
    evt.returnValue = false
  }
  else
  {
    if (evt.button == 2) { bRightClick = true; }
    if (!bRightClick) {
    	if (bitamRootMenu != null) {
			bitamRootMenu.close();
    	}
    }
  }
//debugger;
  var odiv = evt.srcElement || evt.target
  bStopPropagation = !((odiv.tagName == "TH" || odiv.tagName == "TD" || odiv.tagName == "SPAN") &&
  						odiv.onclick && sWhatSource=='webdesigner');
  if (navigator.userAgent.indexOf("Mac OS") != -1 && bRightClick) {
  	bStopPropagation = false;
  }

  if (
  	  bStopPropagation && odiv.tagName == "IMG" &&
  	   (
  	     odiv.id.substring(0,3) == "OPE" || odiv.id.substring(0,3) == "OPD" ||
  	     odiv.id.substring(0,3) == "DFI" || odiv.id.substring(0,3) == "DFC"
  	   )
  	 )
  {
    bStopPropagation = false;
  }
  //alert('odiv.tagName:'+odiv.tagName+' | bStopPropagation:'+bStopPropagation);
//window.parent.document.title = odiv.tagName+'|'+odiv.onclick+'|'+window.parent.document.title;
  if (odiv.tagName == "IMG" &&
  	 (odiv.id == "Graph" || odiv.id == "Image" || odiv.id == "Gauge" || odiv.id == "Line" || odiv.id == "PGBar"))
  {
  	odiv = odiv.parentElement || odiv.parentNode
  }
  else {
	while(odiv.tagName!='DIV' && typeof(odiv.Rep_type)=="undefined" && odiv.tagName!="BODY")
	{
		odiv = odiv.parentElement || odiv.parentNode
	}
	/*  	
  	var attScroll = odiv.getAttribute("attScroll");
  	if (attScroll != undefined && attScroll != null) {
		return;
  	}
  	odiv = getObjectHaveGoParent(odiv);
  	if (odiv == null) { return; }
  	odiv = goToParentObject(odiv);
  	*/
  	if (odiv == null) { return; }
  }

  if (oMovingObject != null)
  {
    if (oMovingObject.divOrig == odiv)
    {
      oMovingObject.divOrig_mousedown(evt)
      return
    }
    oMovingObject.releaseMovingObject()
    oMovingObject = null
    oDivOrigValid = null
  }

  if (sWhatSource=='report') {
  	  if (!bCtrlPressed) {
		releaseaMovObjs();
	  	aMovingObject = new Array();
	  }
  }
  setSelMultEfect();

  oMovingObject = new MovingObject(odiv)

  if (sWhatSource=='report') {
	  var sIdMovObj = oMovingObject.divParent.id + '_AWSep_' + oMovingObject.divOrig.id;
	  if (aMovingObject[sIdMovObj] != undefined && aMovingObject[sIdMovObj] != null) {
		var bTempPressed = bCtrlPressed; var oMovingObjectTemp = oMovingObject;
		bCtrlPressed = false;
		oMovingObject = aMovingObject[sIdMovObj];
	  	oMovingObject.releaseMovingObject();
	  	bCtrlPressed = bTempPressed;
	  	oMovingObject = oMovingObjectTemp;
	  }
	  aMovingObject[sIdMovObj] = oMovingObject;
//window.parent.document.title = 'B:'+getLength(aMovingObject);
  }

  if (CanMoveResizeComp()) {
  	oMovingObject.divOrig_mousedown(evt)
  }
  else if (!isIE) {
	evt.preventDefault() // prevent selection
    evt.stopPropagation()
  }
}
function getObjectHaveGoParent(odiv){
	var sGoParent = odiv.getAttribute("sGoPar");
  	if (sGoParent == undefined || sGoParent == null) {
  		if (odiv.tagName=='HTML') {
	  		return null;
	  	}
  		odiv = odiv.parentElement || odiv.parentNode
  		while (odiv != null && odiv != undefined) {
  			sGoParent = odiv.getAttribute("sGoPar");
		  	if (sGoParent != undefined && sGoParent != null) {
		  		break;
		  	}
		  	else if (odiv.tagName=='HTML') {
		  		odiv = null;
		  		break;
		  	}
		  	odiv = odiv.parentElement || odiv.parentNode
  		}
  	}
  	return odiv;
}
function goToParentObject(odiv){
  	var sGoParent = odiv.getAttribute("sGoPar");
  	if (sGoParent != undefined && sGoParent != null) {
  		sGoParent = parseInt(sGoParent);
  		for (var iPar=0; iPar<sGoParent; iPar++) {
  			var odivPar = odiv.parentElement || odiv.parentNode;
  			if (odivPar != undefined && odivPar != null) {
  				odiv = odivPar;
  			}
  			else {
  				break;
  			}
  		}
  	}
  	else {
  		return null;
  	}
  	return odiv;
}
function setSelMultEfect() {
	if (sWhatSource=='report') {
		for (var i in aMovingObject) {
			if (aMovingObject[i] != undefined && aMovingObject != null) {
				var oMovObjTempE = aMovingObject[i];
				var sColorBorderDisabled = '#d8d8d8';
				oMovObjTempE.divBack.divUL.style.backgroundColor = sDisabledColor
				oMovObjTempE.divBack.divUL.style.border = "1px solid " + sColorBorderDisabled
				oMovObjTempE.divBack.divUM.style.backgroundColor = sDisabledColor
				oMovObjTempE.divBack.divUM.style.border = "1px solid " + sColorBorderDisabled
				oMovObjTempE.divBack.divUR.style.backgroundColor = sDisabledColor
				oMovObjTempE.divBack.divUR.style.border = "1px solid " + sColorBorderDisabled
				oMovObjTempE.divBack.divML.style.backgroundColor = sDisabledColor
				oMovObjTempE.divBack.divML.style.border = "1px solid " + sColorBorderDisabled
				oMovObjTempE.divBack.divMR.style.backgroundColor = sDisabledColor
				oMovObjTempE.divBack.divMR.style.border = "1px solid " + sColorBorderDisabled
				oMovObjTempE.divBack.divLL.style.backgroundColor = sDisabledColor
				oMovObjTempE.divBack.divLL.style.border = "1px solid " + sColorBorderDisabled
				oMovObjTempE.divBack.divLM.style.backgroundColor = sDisabledColor
				oMovObjTempE.divBack.divLM.style.border = "1px solid " + sColorBorderDisabled
				oMovObjTempE.divBack.divLR.style.backgroundColor = sDisabledColor
				oMovObjTempE.divBack.divLR.style.border = "1px solid " + sColorBorderDisabled
			}
		}
	}
}
function releaseaMovObjs() {
	if (sWhatSource=='report') {
		var sIdMovObj = '';
		if (oMovingObject != null) {
			var sIdMovObj = oMovingObject.divParent.id + '_AWSep_' + oMovingObject.divOrig.id;
		}
		for (var iaux in aMovingObject) {
			if (aMovingObject[iaux] != undefined && aMovingObject != null) {
				if (sIdMovObj != iaux) {
					if (aMovingObject[iaux].divOrig != null) {
						oMovingObject = aMovingObject[iaux];
						oMovingObject.setParentRelease();
						oMovingObject.releaseMovingObject();
					}
				}
			}
		}
		aMovingObject = new Array();
	}
}
// class MovingObject()
function MovingObject(odiv)
{
  var oParent
  var oParentRect
  var oMovingDiv
  var oResizingDiv
  var nMouseDownX = 0
  var nMouseDownY = 0
  var sLastResizingDivClicked

  // oDivOrig
  var oDivOrig = odiv
  oDivOrigValid = oDivOrig

  // oParent
  var oParent = oDivOrig.parentElement || oDivOrig.parentNode

  // oDivBack
  var oDivBack = document.createElement("div")
  oDivBack.style.position = "absolute"
  oDivBack.style.backgroundColor = "transparent"
  oDivBack.style.left = (oDivOrig.offsetLeft - nMovingBorderWidth) + "px"
  oDivBack.style.top = (oDivOrig.offsetTop - nMovingBorderWidth) + "px"
  oDivBack.style.width = (oDivOrig.offsetWidth + nMovingBorderWidth * 2) + "px"
  oDivBack.style.height = (oDivOrig.offsetHeight + nMovingBorderWidth * 2) + "px"
  oDivBack.unselectable = "on"

  // resize divs (8)
  var divUL = oDivBack.appendChild(getDivResize("ul"))
  var divUM = oDivBack.appendChild(getDivResize("um"))
  var divUR = oDivBack.appendChild(getDivResize("ur"))
  var divML = oDivBack.appendChild(getDivResize("ml"))
  var divMR = oDivBack.appendChild(getDivResize("mr"))
  var divLL = oDivBack.appendChild(getDivResize("ll"))
  var divLM = oDivBack.appendChild(getDivResize("lm"))
  var divLR = oDivBack.appendChild(getDivResize("lr"))
  setHorizontalResizeDivsPos()
  setVerticalResizeDivsPos()

  // back up original div last props and set new
  var sLastCursor = oDivOrig.style.cursor
  var sLastUnselectable = oParent.unselectable
  oDivOrig.style.cursor = "move"

  oParent.unselectable = "on"
  if (isIE)
  {
    var oLastSelectStart = null
    if (document.body.onselectstart != null)
    {
      oLastSelectStart = document.body.onselectstart
    }
    document.body.onselectstart = function() { window.event.returnValue = false }
  }

  // get max zIndex
  nMaxIndex = 0
  for (var i = 0; i < oParent.childNodes.length; i++)
  {
    if (oParent.childNodes[i].style)
    {
      if (parseInt(oParent.childNodes[i].style.zIndex) > nMaxIndex)
      {
        nMaxIndex = parseInt(oParent.childNodes[i].style.zIndex)
      }
    }
  }
  oDivBack.style.zIndex = nMaxIndex
  oDivOrig.style.zIndex = nMaxIndex + 1
  if (sWhatSource=='webdesigner') {
  	setZIndexAllComponent(oDivOrig);
  	bitamMenuZIndex = nMaxIndex + 2;
  }

  // append oDivBack to container
  oDivBack = oParent.appendChild(oDivBack)
  oDivBack.divUL = divUL
  oDivBack.divUM = divUM
  oDivBack.divUR = divUR
  oDivBack.divML = divML
  oDivBack.divMR = divMR
  oDivBack.divLL = divLL
  oDivBack.divLM = divLM
  oDivBack.divLR = divLR
  // public pointer to original div
  this.divOrig = oDivOrig
  this.divParent = oParent
  this.divBack = oDivBack

  // atach body event
  if (isIE)
  {
    oParent.attachEvent("onmousedown", parent_mousedown)
  }
  else
  {
    oParent.addEventListener("mousedown", parent_mousedown, false)
  }

  this.setParentRelease = function(evt) {
  	oParent = this.divParent;
  }

  function setNewPos(nLeft, nTop, nWidth, nHeight) // send -1 for no change
  {
  	var bGoRefresh = false;
  	if (sWhatSource=='webdesigner') {
		if (oDivOrig == null) { return; }
  		if (nLeft != parseInt(oDivOrig.style.left)) {
			bGoRefresh = true;
		}
		else if (nTop != parseInt(oDivOrig.style.top)) {
			bGoRefresh = true;
		}
		else if (nWidth != -1 || nHeight != -1) {
			bGoRefresh = true;
		}
  	}
    if (nLeft >= 0)
    {
      oDivBack.style.left = (nLeft - nMovingBorderWidth) + "px"
      oDivOrig.style.left = nLeft + "px"
      oDivOrig.Rep_left = nLeft
    }
    if (nTop >= 0)
    {
      oDivBack.style.top = (nTop - nMovingBorderWidth) + "px"
      oDivOrig.style.top = nTop + "px"
      oDivOrig.Rep_top = nTop
    }
    if (nWidth >= 0)
    {
      oDivBack.style.width = (nWidth + nMovingBorderWidth * 2) + "px"
      oDivOrig.style.width = (nWidth - (isIE ? 0 : (oDivOrig.offsetWidth - oDivOrig.clientWidth))) + "px"
      oDivOrig.Rep_width = nWidth
      setHorizontalResizeDivsPos()
    }
    if (nHeight >= 0)
    {
      oDivBack.style.height = (nHeight + nMovingBorderWidth * 2) + "px"
      oDivOrig.style.height = (nHeight - (isIE ? 0 : (oDivOrig.offsetHeight - oDivOrig.clientHeight)))  + "px"
      oDivOrig.Rep_height = nHeight
      setVerticalResizeDivsPos()
    }
    if (sWhatSource=='webdesigner' && bGoRefresh) {
    	var nWP = (nWidth!=-1) ? nWidth : parseInt(oDivOrig.style.width);
    	var nHP = (nHeight!=-1) ? nHeight : parseInt(oDivOrig.style.height);
    	var sCompId = getFT_Component(oDivOrig.id);
		var aAux = sCompId.split('_');
		var sCompType = parseInt(aAux[0]);
		var sOption = 'Move';
		if (sCompType == FT_CHART)
		{
			sOption = 'MoveChart';
		}
		if (sCompType == FT_LINE)
		{   //Actualizar la posicion de la linea en su Metadata para que el Wizard la pueda tomar correctamente
			aAux=aCompsInfo['INFO'][sCompId].split('_');
			var newSource=nLeft+'_'+nTop+'_'+nWP+'_'+nHP;
			for(var i=4; i<aAux.length; i++)
				newSource=newSource+'_'+aAux[i];

			aCompsInfo['INFO'][sCompId]=newSource;
		}
		var RefreshStg = (nWidth==-1 && nHeight==-1) ? 0 : 1;
    	StgParams(sCompId,sOption,nLeft+','+nTop+','+nWP+','+nHP,RefreshStg);
    	setNewPosAllComponent(oDivOrig);
    }
  }

  this.divOrig_mousedown = function(evt)
  {
//window.parent.document.title = '|divOrig_mousedown';
    oMovingDiv = getTempMovingDiv()
    oParent.appendChild(oMovingDiv)
    // get parent bounding client rect
    oParentRect = oParent.getBoundingClientRect()
    if (isIE)
    {
      nMouseDownX = evt.offsetX
      nMouseDownY = evt.offsetY
      // atach events
      if (CanMoveResizeComp()) {
	      document.body.attachEvent("onmousemove", movingObject)
      }
      oParent.attachEvent("onmouseout", parent_mouseout_moving)
      oParent.attachEvent("onmouseup", stopMovingObject)
      evt.cancelBubble = true
    }
    else
    {
      nMouseDownX = evt.layerX
      nMouseDownY = evt.layerY
      // atach events
      if (CanMoveResizeComp()) {
	      document.documentElement.addEventListener("mousemove", movingObject, true)
      }
      oParent.addEventListener("mouseout", parent_mouseout_moving, true)
      oParent.addEventListener("mouseup", stopMovingObject, true)
      if (bStopPropagation) {
      	evt.preventDefault() // prevent selection
      	evt.stopPropagation()
      }
      else {
      	stopMovingObject();
      }
    }
  }

  function movingObject(evt)
  {
//window.parent.document.title = '|movingObject';
	if (sWhatSource=='webdesigner') { bShowTB = false; }
  	if (bRightClick || oParentRect == null || oMovingDiv == null || oParent == null) { return; }
    nNewX = evt.clientX - oParentRect.left - nMouseDownX
    nNewY = evt.clientY - oParentRect.top - nMouseDownY
    if (nNewX < 0) { nNewX = 0 }
    if (nNewX + oMovingDiv.offsetWidth > oParent.offsetWidth) { nNewX = oParent.offsetWidth - oMovingDiv.offsetWidth }
    if (nNewY < 0) { nNewY = 0 }
    if (nNewY + oMovingDiv.offsetHeight > oParent.offsetHeight) { nNewY = oParent.offsetHeight - oMovingDiv.offsetHeight }
    oMovingDiv.style.left = nNewX + "px"
    oMovingDiv.style.top = nNewY + "px"
    if (isIE) {
    	evt.cancelBubble = true;
		evt.returnValue = false;
    }
  }

  function stopMovingObject()
  {
//window.parent.document.title = '|stopMovingObject';
	if (sWhatSource=='webdesigner') { bShowTB = true; }
  	if (oMovingDiv == null || oParent == null) { return; }
    setNewPos(oMovingDiv.offsetLeft, oMovingDiv.offsetTop, -1, -1)
    oParent.removeChild(oMovingDiv)
    oMovingDiv = null
    if (isIE)
    {
      if (CanMoveResizeComp() || true) {
      	document.body.detachEvent("onmousemove", movingObject)
      }
      oParent.detachEvent("onmouseout", parent_mouseout_moving)
      oParent.detachEvent("onmouseup", stopMovingObject)
    }
    else
    {
      if (CanMoveResizeComp() || true) {
      	document.documentElement.removeEventListener("mousemove", movingObject, true)
      }
      oParent.removeEventListener("mouseout", parent_mouseout_moving, true)
      oParent.removeEventListener("mouseup", stopMovingObject, true)
    }
  }

  function parent_mouseout_moving(evt)
  {
//window.parent.document.title = '|parent_mouseout_moving';
  	if (oParent == null) { return; }
    if (isIE)
    {
      if ((evt.toElement == null) || (evt.toElement != oParent && !oParent.contains(evt.toElement)))
      {
        stopMovingObject()
      }
    }
    else
    {
      if ((evt.relatedTarget == null) || (evt.relatedTarget != oParent && !(oParent.compareDocumentPosition(evt.relatedTarget) & 16)))
      {
        stopMovingObject()
      }
    }
  }

  function parent_mousedown(evt)
  {
//window.parent.document.title = '|parent_mousedown';
//debugger;
    //if (oMovingObject == null) { return }
    oMovingObject.releaseMovingObject()
    oMovingObject = null
    oDivOrigValid = null
    if (!bCtrlPressed && sWhatSource=='report') {
    	releaseaMovObjs();
    	aMovingObject = new Array();
	}
  }

  function setHorizontalResizeDivsPos()
  {
    divUM.style.left = parseInt(parseInt(oDivBack.style.width) / 2 - nMovingBorderWidth / 2) + "px"
    divUR.style.left = parseInt(parseInt(oDivBack.style.width) - nMovingBorderWidth) + "px"
    divMR.style.left = parseInt(parseInt(oDivBack.style.width) - nMovingBorderWidth) + "px"
    divLM.style.left = parseInt(parseInt(oDivBack.style.width) / 2 - nMovingBorderWidth / 2) + "px"
    divLR.style.left = parseInt(parseInt(oDivBack.style.width) - nMovingBorderWidth) + "px"
  }

  function setVerticalResizeDivsPos()
  {
    divML.style.top = parseInt(parseInt(oDivBack.style.height) / 2 - nMovingBorderWidth / 2) + "px"
    divMR.style.top = parseInt(parseInt(oDivBack.style.height) / 2 - nMovingBorderWidth / 2) + "px"
    divLL.style.top = parseInt(parseInt(oDivBack.style.height) - nMovingBorderWidth) + "px"
    divLM.style.top = parseInt(parseInt(oDivBack.style.height) - nMovingBorderWidth) + "px"
    divLR.style.top = parseInt(parseInt(oDivBack.style.height) - nMovingBorderWidth) + "px"
  }

  this.releaseMovingObject = function()
  {
//window.parent.document.title += '|releaseMovingObject';
//debugger;
  	// detach parent mousedown
  	if (oParent != null) {
	    if (isIE)
	    {
	      oParent.detachEvent("onmousedown", parent_mousedown)
	    }
	    else
	    {
	      oParent.removeEventListener("mousedown", parent_mousedown, false)
	    }
  	}

  	if (sWhatSource=='report') {
		if (bCtrlPressed) {
			return;
		}
	}

    // parche para el ie =/
    if (isIE)
    {
      if (oMovingObject != null)
      {
      	if (oMovingObject.divOrig.parentElement.id != 'DivDash') {
	        oMovingObject.divOrig.parentElement.style.top = "1px"
	        oMovingObject.divOrig.parentElement.style.top = ""
      	}
      }
    }

    // set odiv cursor back to original
    if (oDivOrig != null) {
    	oDivOrig.style.cursor = sLastCursor
    }
    if (oParent != null) {
	    if (oDivBack != null) {
    		oParent.removeChild(oDivBack)
	    }
	    oParent.unselectable = sLastUnselectable
    }
    if (isIE)
    {
      document.body.onselectstart = oLastSelectStart
    }
    divUL = null
    divUM = null
    divUR = null
    divML = null
    divMR = null
    divLL = null
    divLM = null
    divLR = null
    oDivOrig = null
    oDivBack = null
    oMovingObject = null
    oDivOrigValid = null
    oResizingObject = null
    oParent = null
    oParentRect = null
    this.divOrig = null
  }

  function getDivResize(sWhich)
  {
    var oResizeDiv = document.createElement("div")
    oResizeDiv.id = sWhich
    oResizeDiv.style.position = "absolute"
    oResizeDiv.style.backgroundColor = "#ffffff"
    oResizeDiv.style.fontSize = "1px"
    oResizeDiv.style.left = "0px"
    oResizeDiv.style.top = "0px"
    if (isIE)
    {
      oResizeDiv.style.width = nMovingBorderWidth + "px"
      oResizeDiv.style.height = nMovingBorderWidth + "px"
    }
    else
    {
      oResizeDiv.style.width = (nMovingBorderWidth - 2) + "px"
      oResizeDiv.style.height = (nMovingBorderWidth - 2) + "px"
    }
    oResizeDiv.style.border = "1px solid #000000"
    if (CanMoveResizeComp()) {
	    switch(sWhich)
	    {
	      case "ul":
	        oResizeDiv.style.cursor = "nw-resize"
	        break
	      case "um":
	        oResizeDiv.style.cursor = "n-resize"
	        break
	      case "ur":
	        oResizeDiv.style.cursor = "ne-resize"
	        break
	      case "ml":
	        oResizeDiv.style.cursor = "e-resize"
	        break
	      case "mr":
	        oResizeDiv.style.cursor = "w-resize"
	        break
	      case "ll":
	        oResizeDiv.style.cursor = "sw-resize"
	        break
	      case "lm":
	        oResizeDiv.style.cursor = "s-resize"
	        break
	      case "lr":
	        oResizeDiv.style.cursor = "se-resize"
	        break
	    }
	    // attach mousedown event
	    if (isIE)
	    {
	      oResizeDiv.attachEvent("onmousedown", divResize_mousedown)
	    }
	    else
	    {
	      oResizeDiv.addEventListener("mousedown", divResize_mousedown, true)
	    }
    }
    return (oResizeDiv)
  }

  function resizingObject(evt)
  {
  	if (bRightClick) { return; }
    var nNewX = evt.clientX - oParentRect.left
    var nNewY = evt.clientY - oParentRect.top
    var nNewWidth, nNewHeight
    switch(sLastResizingDivClicked)
    {
      case "ul":
        nNewX = nNewX + (nMovingBorderWidth - nMouseDownX)
        nNewY = nNewY + (nMovingBorderWidth - nMouseDownY)
        if (nNewX < 0) { nNewX = 0 }
        if (nNewX > oResizingDiv.offsetLeft + oResizingDiv.offsetWidth - nResizeMinSize) { nNewX = oResizingDiv.offsetLeft + oResizingDiv.offsetWidth - nResizeMinSize }
        if (nNewY < 0) { nNewY = 0 }
        if (nNewY > oResizingDiv.offsetTop + oResizingDiv.offsetHeight - nResizeMinSize) { nNewY = oResizingDiv.offsetTop + oResizingDiv.offsetHeight - nResizeMinSize }
        nNewWidth = oResizingDiv.offsetLeft + oResizingDiv.offsetWidth - nNewX - (isIE ? 0 : 2)
        nNewHeight = oResizingDiv.offsetTop + oResizingDiv.offsetHeight - nNewY - (isIE ? 0 : 2)
        oResizingDiv.style.left = nNewX + "px"
        oResizingDiv.style.top = nNewY + "px"
        oResizingDiv.style.width = nNewWidth + "px"
        oResizingDiv.style.height = nNewHeight + "px"
        break
      case "um":
        nNewY = nNewY + (nMovingBorderWidth - nMouseDownY)
        if (nNewY < 0) { nNewY = 0 }
        if (nNewY > oResizingDiv.offsetTop + oResizingDiv.offsetHeight - nResizeMinSize) { nNewY = oResizingDiv.offsetTop + oResizingDiv.offsetHeight - nResizeMinSize }
        nNewHeight = oResizingDiv.offsetTop + oResizingDiv.offsetHeight - nNewY - (isIE ? 0 : 2)
        oResizingDiv.style.top = nNewY + "px"
        oResizingDiv.style.height = nNewHeight + "px"
        break
      case "ur":
        nNewX = nNewX - nMouseDownX
        nNewY = nNewY + (nMovingBorderWidth - nMouseDownY)
        if (nNewX < oResizingDiv.offsetLeft + nResizeMinSize) { nNewX = oResizingDiv.offsetLeft + nResizeMinSize }
        if (nNewX > oParent.offsetWidth) { nNewX = oParent.offsetWidth }
        if (nNewY < 0) { nNewY = 0 }
        if (nNewY > oResizingDiv.offsetTop + oResizingDiv.offsetHeight - nResizeMinSize) { nNewY = oResizingDiv.offsetTop + oResizingDiv.offsetHeight - nResizeMinSize }
        nNewWidth = nNewX - oResizingDiv.offsetLeft - (isIE ? 0 : 2)
        nNewHeight = oResizingDiv.offsetTop + oResizingDiv.offsetHeight - nNewY - (isIE ? 0 : 2)
        oResizingDiv.style.top = nNewY + "px"
        oResizingDiv.style.width = nNewWidth + "px"
        oResizingDiv.style.height = nNewHeight + "px"
        break
      case "ml":
        nNewX = nNewX + (nMovingBorderWidth - nMouseDownX)
        if (nNewX < 0) { nNewX = 0 }
        if (nNewX > oResizingDiv.offsetLeft + oResizingDiv.offsetWidth - nResizeMinSize)
        {
          nNewX = oResizingDiv.offsetLeft + oResizingDiv.offsetWidth - nResizeMinSize
        }
        nNewWidth = oResizingDiv.offsetLeft + oResizingDiv.offsetWidth - nNewX - (isIE ? 0 : 2)
        oResizingDiv.style.left = nNewX + "px"
        oResizingDiv.style.width = nNewWidth + "px"
        break
      case "mr":
        nNewX = nNewX - nMouseDownX
        if (nNewX < oResizingDiv.offsetLeft + nResizeMinSize) { nNewX = oResizingDiv.offsetLeft + nResizeMinSize }
        if (nNewX > oParent.offsetWidth) { nNewX = oParent.offsetWidth }
        nNewWidth = nNewX - oResizingDiv.offsetLeft - (isIE ? 0 : 2)
        oResizingDiv.style.width = nNewWidth + "px"
        break
      case "ll":
        nNewX = nNewX + (nMovingBorderWidth - nMouseDownX)
        nNewY = nNewY - nMouseDownY
        if (nNewX < 0) { nNewX = 0 }
        if (nNewX > oResizingDiv.offsetLeft + oResizingDiv.offsetWidth - nResizeMinSize) { nNewX = oResizingDiv.offsetLeft + oResizingDiv.offsetWidth - nResizeMinSize }
        if (nNewY < oResizingDiv.offsetTop + nResizeMinSize) { nNewY = oResizingDiv.offsetTop + nResizeMinSize }
        if (nNewY > oParent.offsetHeight) { nNewY = oParent.offsetHeight }
        nNewWidth = oResizingDiv.offsetLeft + oResizingDiv.offsetWidth - nNewX - (isIE ? 0 : 2)
        nNewHeight = nNewY - oResizingDiv.offsetTop - (isIE ? 0 : 2)
        oResizingDiv.style.left = nNewX + "px"
        oResizingDiv.style.width = nNewWidth + "px"
        oResizingDiv.style.height = nNewHeight + "px"
        break
      case "lm":
        nNewY = nNewY - nMouseDownY
        if (nNewY < oResizingDiv.offsetTop + nResizeMinSize) { nNewY = oResizingDiv.offsetTop + nResizeMinSize }
        if (nNewY > oParent.offsetHeight) { nNewY = oParent.offsetHeight }
        nNewHeight = nNewY - oResizingDiv.offsetTop - (isIE ? 0 : 2)
        oResizingDiv.style.height = nNewHeight + "px"
        break
      case "lr":
        nNewX = nNewX - nMouseDownX
        nNewY = nNewY - nMouseDownY
        if (nNewX < oResizingDiv.offsetLeft + nResizeMinSize) { nNewX = oResizingDiv.offsetLeft + nResizeMinSize }
        if (nNewX > oParent.offsetWidth) { nNewX = oParent.offsetWidth }
        if (nNewY < oResizingDiv.offsetTop + nResizeMinSize) { nNewY = oResizingDiv.offsetTop + nResizeMinSize }
        if (nNewY > oParent.offsetHeight) { nNewY = oParent.offsetHeight }
        nNewWidth = nNewX - oResizingDiv.offsetLeft - (isIE ? 0 : 2)
        nNewHeight = nNewY - oResizingDiv.offsetTop - (isIE ? 0 : 2)
        oResizingDiv.style.width = nNewWidth + "px"
        oResizingDiv.style.height = nNewHeight + "px"
        break
    }
  }

  function stopResizingObject()
  {
    setNewPos(oResizingDiv.offsetLeft, oResizingDiv.offsetTop, oResizingDiv.offsetWidth, oResizingDiv.offsetHeight)
    if (oParent) { oParent.removeChild(oResizingDiv); }
    if (CanMoveResizeComp() || true) {
	    if (isIE)
	    {
	      document.body.detachEvent("onmousemove", resizingObject)
	      if (oParent) { oParent.detachEvent("onmouseout", parent_mouseout_resizing); }
	      if (oParent) { oParent.detachEvent("onmouseup", stopResizingObject); }
	    }
	    else
	    {
	      document.documentElement.removeEventListener("mousemove", resizingObject, true)
	      if (oParent) { oParent.removeEventListener("mouseout", parent_mouseout_resizing, true); }
	      if (oParent) { oParent.removeEventListener("mouseup", stopResizingObject, true); }
	    }
    }
    if(oDivOrig && oDivOrig.afterResize) oDivOrig.afterResize();
  }

  function parent_mouseout_resizing(evt)
  {
  	if (oParent) {
	    if (isIE)
	    {
	      if ((evt.toElement == null) || (evt.toElement != oParent && !oParent.contains(evt.toElement)))
	      {
	        stopResizingObject()
	      }
	    }
	    else
	    {
	      if ((evt.relatedTarget == null) || (evt.relatedTarget != oParent && !(oParent.compareDocumentPosition(evt.relatedTarget) & 16)))
	      {
	        stopResizingObject()
	      }
	    }
  	}
  }

  function divResize_mousedown(evt)
  {
    osrc = evt.srcElement || evt.target
    sLastResizingDivClicked = osrc.id
    oResizingDiv = getTempMovingDiv()
    oParent.appendChild(oResizingDiv)
    // get parent bounding client rect
    oParentRect = oParent.getBoundingClientRect()
    if (isIE)
    {
      nMouseDownX = evt.offsetX + 1 + 1 // border and offset
      nMouseDownY = evt.offsetY + 1 + 1
      // atach events
      if (CanMoveResizeComp()) {
	      document.body.attachEvent("onmousemove", resizingObject)
	      oParent.attachEvent("onmouseout", parent_mouseout_resizing)
	      oParent.attachEvent("onmouseup", stopResizingObject)
      }
      evt.cancelBubble = true
    }
    else
    {
      nMouseDownX = evt.layerX + 1
      nMouseDownY = evt.layerY + 1
      // atach events
      if (CanMoveResizeComp()) {
	      document.documentElement.addEventListener("mousemove", resizingObject, true)
	      oParent.addEventListener("mouseout", parent_mouseout_resizing, true)
	      oParent.addEventListener("mouseup", stopResizingObject, true)
      }
      evt.preventDefault() // prevent selection
      evt.stopPropagation()
    }
  }

  function getTempMovingDiv()
  {
    var oTempDiv = document.createElement("div")
    oTempDiv.style.position = "absolute"
    oTempDiv.style.backgroundColor = "transparent"
    oTempDiv.style.border = "1px dotted #000000"
    oTempDiv.style.fontSize = "1px"
    oTempDiv.style.left = oDivOrig.style.left
    oTempDiv.style.top = oDivOrig.style.top
    oTempDiv.style.width = isIE ? oDivOrig.style.width : (oDivOrig.offsetWidth - 2) + "px"
    oTempDiv.style.height = isIE ? oDivOrig.style.height : (oDivOrig.offsetHeight - 2) + "px"
    oTempDiv.style.zIndex = parseInt(oDivOrig.style.zIndex) + 1
    return (oTempDiv)
  }

  this.setDivBackPos = function()
  {
  	oDivBack.style.left = (oDivOrig.offsetLeft - nMovingBorderWidth) + "px"
  	oDivBack.style.top = (oDivOrig.offsetTop - nMovingBorderWidth) + "px"
  	oDivBack.style.width = (oDivOrig.offsetWidth + nMovingBorderWidth * 2 - (isIE ? 0 : 2)) + "px"
  	oDivBack.style.height = (oDivOrig.offsetHeight + nMovingBorderWidth * 2 - (isIE ? 0 : 2)) + "px"
  	setHorizontalResizeDivsPos();
  	setVerticalResizeDivsPos();
  }
}
// end MovingObject
// others functions
function setNewPosAllComponent(oDivOrig){
	var sCompId = getFT_Component(oDivOrig.id);
	if (sCompId == '') { return; }
	var aAux = sCompId.split('_');
	var sCompType = parseInt(aAux[0]);
	var sCompConsec = aAux[1];
//debugger;
	switch(sCompType) {
		case FT_DIMENSION:
			var objDataDiv = GetElementWithId('DataDiv'+sCompConsec);
			if (objDataDiv != null && objDataDiv != undefined) {
				objDataDiv.style.left = oDivOrig.style.left;
				objDataDiv.style.top = (parseInt(oDivOrig.style.height) + parseInt(oDivOrig.style.top)) + "px";
			}
			objDataDiv = GetElementWithId('LikeDataDiv'+sCompConsec);
			if (objDataDiv != null && objDataDiv != undefined) {
				objDataDiv.style.left = oDivOrig.style.left;
				objDataDiv.style.top = (parseInt(oDivOrig.style.height) + parseInt(oDivOrig.style.top)) + "px";
			}
			break;
	}
}
function setZIndexAllComponent(oDivOrig){
	var sCompId = getFT_Component(oDivOrig.id);
	if (sCompId == '') { return; }
	var aAux = sCompId.split('_');
	var sCompType = parseInt(aAux[0]);
	var sCompConsec = aAux[1];
//debugger;
	switch(sCompType) {
		case FT_DIMENSION:
			var objDataDiv = GetElementWithId('DataDiv'+sCompConsec);
			if (objDataDiv != null && objDataDiv != undefined) {
				objDataDiv.style.zIndex = oDivOrig.style.zIndex
			}
			objDataDiv = GetElementWithId('LikeDataDiv'+sCompConsec);
			if (objDataDiv != null && objDataDiv != undefined) {
				objDataDiv.style.zIndex = oDivOrig.style.zIndex
			}
			break;
	}
}
function MoveElement(oElementAct, oMovingObjectAct, key){
//debugger;
	var bOk = false;
	switch(key) {
		case 37:
			if (sWhatSource=='webdesigner') {
				var nLeft = parseInt(oMovingObjectAct.divOrig.style.left);
				if (nLeft > 0) {
					oMovingObjectAct.divOrig.style.left = (nLeft-1)+'px';
					bOk = true;
				}
			}
			else {
				if (oElementAct.element.Rep_left > 0) {
					oElementAct.element.Rep_left = oElementAct.element.Rep_left - 1;
					bOk = true;
				}
			}
			break;
		case 38:
			if (sWhatSource=='webdesigner') {
				var nTop = parseInt(oMovingObjectAct.divOrig.style.top);
				if (nTop > 0) {
					oMovingObjectAct.divOrig.style.top = (nTop-1)+'px';
					bOk = true;
				}
			}
			else {
				if (oElementAct.element.Rep_top > 0) {
					oElementAct.element.Rep_top = oElementAct.element.Rep_top - 1;
					bOk = true;
				}
			}
			break;
		case 39:
			if (sWhatSource=='webdesigner') {
				var nLeft = parseInt(oMovingObjectAct.divOrig.style.left);
				var nWidth = parseInt(oMovingObjectAct.divOrig.style.width);
				if ((nLeft + nWidth) < oMovingObjectAct.divParent.offsetWidth) {
					oMovingObjectAct.divOrig.style.left = (nLeft+1)+'px';
					bOk = true;
				}
			}
			else {
				if ((oElementAct.element.Rep_left+oElementAct.element.Rep_width) < oMovingObjectAct.divParent.offsetWidth) {
					oElementAct.element.Rep_left = oElementAct.element.Rep_left + 1;
					bOk = true;
				}
			}
			break;
		case 40:
			if (sWhatSource=='webdesigner') {
				var nTop = parseInt(oMovingObjectAct.divOrig.style.top);
				var nHeight = parseInt(oMovingObjectAct.divOrig.style.height);
				if ((nTop+nHeight) < oMovingObjectAct.divParent.offsetHeight) {
					oMovingObjectAct.divOrig.style.top = (nTop+1)+'px';
					bOk = true;
				}
			}
			else {
				if ((oElementAct.element.Rep_top+oElementAct.element.Rep_height) < oMovingObjectAct.divParent.offsetHeight) {
					oElementAct.element.Rep_top = oElementAct.element.Rep_top + 1;
					bOk = true;
				}
			}
			break;
	}
	if (bOk) {
		if (sWhatSource=='webdesigner') {
			var nWP = parseInt(oMovingObjectAct.divOrig.style.width);
	    	var nHP = parseInt(oMovingObjectAct.divOrig.style.height);
	    	var nTP = parseInt(oMovingObjectAct.divOrig.style.top);
	    	var nLP = parseInt(oMovingObjectAct.divOrig.style.left);
	    	setNewPosAllComponent(oMovingObjectAct.divOrig);
	    	var sCompId = getFT_Component(oMovingObjectAct.divOrig.id);
			var aAux = sCompId.split('_');
			var sCompType = parseInt(aAux[0]);
			var sOption = 'Move';
			if (sCompType == FT_CHART) {
				sOption = 'MoveChart';
			}
	    	StgParams(sCompId,sOption,nLP+','+nTP+','+nWP+','+nHP,0);
		}
		else {
			oElementAct.refreshPosition();
		}
		oMovingObjectAct.setDivBackPos();
	}
}
function CanMoveResizeComp() {
	var bMoveResize = true;
	if (bLoading==0){
		bMoveResize = false;
	}
	else if (sWhatSource=='report') {
		bMoveResize = (getLength(aMovingObject) <= 1);
	}
	else if (sWhatSource=='webdesigner') {
		if (oDivOrigValid) {
			var sCompId = getFT_Component(oDivOrigValid.id);
			if (aCompsInfo['BLOCKED'][sCompId] != undefined && aCompsInfo['BLOCKED'][sCompId]==1){
				bMoveResize = false;
			}
		}
	}
	return bMoveResize;
}
function getLength(aParam) {
	var nCount = 0;
	for (var i in aParam) {
		if (aParam[i] != null && aParam[i] != undefined) {
			nCount++;
		}
	}
	return nCount;
}