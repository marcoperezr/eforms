var AWComa = '_#AWComa#_';
var sAddProp = '';
var sAddPropFin = '';
var sCompType = '0';
var sCubeKey = '-1';
var bAddFormat = false;
var sInitSel = '';
var sIniPart = '';
var sBodyInds = '';
var sSubSectionType = '';

function OnStart()
{
	bStopDiv = false; // Para el calculo de posición con BitamGridSel.js
	var regexp = new RegExp(AWComa,"gi");
	GetElementWithId('lbStep').innerHTML      = ML[101];
	GetElementWithId('botOK').value           = ML[69];
	GetElementWithId('botCancel').value       = ML[70];
	GetElementWithId('botSelImg').value 	  = ML[589];
	GetElementWithId('botClearImg').value 	  = ML[588];
	GetElementWithId('tbBody').style.width 	  = document.body.offsetWidth-25;
	GetElementWithId('tbButtons').style.width = document.body.offsetWidth-25;
	GetElementWithId('Rep_type').value 	      = window_dialogArguments[3];
	//Ocultar x default los que son para Lineas
	GetElementWithId('id_gradianteHorVer').style.display    = "none";
	GetElementWithId('id_colordegradado').style.display     = "none";
	GetElementWithId('id_styledegradado').style.display     = "none";
	GetElementWithId('id_styledegradadoPGB').style.display  = "none";
	GetElementWithId('col_color_sel_I1_I2').style.display   = "none";
	GetElementWithId('PGB_ShowDecimals').style.display      = "none";
	if (window_dialogArguments[6] != null) {
		sSubSectionType = window_dialogArguments[6];
	}

	Add_Events();
	var aArgs = window_dialogArguments[2].split('_#AWProps#_');
	var aArgsM = aArgs[0].split(',');
	var aArgsP = aArgs[1].split(',');
	if (window_dialogArguments[3]==1)	// Dimension Dimension Dimension Dimension Dimension Dimension Dimension Dimension Dimension
	{
		GetElementWithId('legend').innerHTML = '&nbsp;&nbsp;'+ML[354];
		GetElementWithId('id_caption').style.display = 'none';
		GetElementWithId('cbDimensions').style.display = 'inline';
		SetSelectedCB('cbDimensions',aArgsM[2]);
		sAddProp = (aArgsM[3] != undefined) ? aArgsM[3] : '0';
		sInitSel = aArgsM[2];
		if (aArgsM[7] != '-1')
		{
			SetSelectedCB('cbPeriods',aArgsM[7]);
		}
		checkPeriod(GetElementWithId('cbDimensions'));
		var nTop = aArgsM.length;
		for (i=4; i<nTop; i++)
		{
			if (i == 7)  // Period
			{
				sAddProp = sAddProp+',_#awPeriodkeyaw#_';
			}
			else
			{
				sAddProp = sAddProp+','+aArgsM[i];
			}
		}
	}
	else if (window_dialogArguments[3]==2 || window_dialogArguments[3]==3) { // Indicator, Total Indicator, Total Indicator, Total Indicator, Total Indicator, Total
		GetElementWithId('legend').innerHTML = '&nbsp;&nbsp;'+ML[95];
		GetElementWithId('id_caption').style.display = 'none';
		GetElementWithId('cbIndicators').style.display = 'inline';
		var nTop = aArgsM.length; var sTotalType = '0';
		if (aArgsM[3] == '_#NEW#_') {	// Si hay que agregar el formato activar bandera
			bAddFormat = true;
		}
		for (i=3; i<nTop; i++){
			if (window_dialogArguments[3]==3 && (i>=14)) {
				if (i==14){
					sCompType = aArgsM[i];
				}
				else if (i==15) {
					sCubeKey = aArgsM[i];
				}
				else {
					sAddPropFin = ((sAddPropFin == '') ? '' : ',' ) + aArgsM[i];
				}
			}
			else if (window_dialogArguments[3]==3 && i==10) {
				sTotalType = aArgsM[i];
				sAddProp = sAddProp+',_#awSETTOTALTYPEaw#_';
			}
			else if (window_dialogArguments[3]==2 && i==21) {
				sAddProp = sAddProp+',_#NOTNUM#_';
			}
			else {
				sAddProp = sAddProp+','+aArgsM[i];
			}
		}
		if (window_dialogArguments[3]==3) {
			var sKeySelected = (sCompType == '0') ? 'Cube': 'Concept';
			sIniPart = sKeySelected + '_AWSep_' + sCubeKey + '_AWSep_';
			sKeySelected = sKeySelected + '_AWSep_' + sCubeKey + '_AWSep_' + aArgsM[1];
			if (sSubSectionType == 'before' || sSubSectionType == 'header' || sSubSectionType == 'after'
			   || sSubSectionType == 'footer' || sSubSectionType == 'ReportSummary')
			{
				GetElementWithId('trTotalType').style.display  = (bIsIE)?'inline':'table-row';
			}
			if (sCompType == '1' && aTempConceptType[sCubeKey] != undefined) {
				if (aTempConceptType[sCubeKey] != 0) { // Diferente de ctpNormal, no lleva Total BD
					ResetTotalType();
				}
			}
			SetSelectedCB('cbTotalType',sTotalType);
		}
		else {
			var sKeySelected = aArgsM[1];
		}
		sInitSel = sKeySelected;
		if (window_dialogArguments[3]==3 && window_dialogArguments[4]!='') {
			GetElementWithId('tb_color_sel_I1').style.display = "none";
			GetElementWithId('tb_color_sel_I2').style.display = "none";
			GetElementWithId('id_viewall').style.display = "inline";
			GetElementWithId('col_color_sel_I1_I2').title = ML[657];
			GetElementWithId('col_color_sel_I1_I2').style.display   = (bIsIE)?'inline':'table-cell';
			sBodyInds = ''+window_dialogArguments[4];
			FillComboTotalInd('cbIndicators', aCubeIndJS, sBodyInds, false, sInitSel);
		}
		SetSelectedCB('cbIndicators',sKeySelected);
	}
	else if (window_dialogArguments[3]==11) { // PGBar PGBar PGBar PGBar PGBar PGBar PGBar PGBar PGBar PGBar PGBar PGBar PGBar PGBar
		GetElementWithId('legend').innerHTML = '&nbsp;&nbsp;'+ML[604];
		GetElementWithId('id_caption').style.display   = 'none';
		GetElementWithId('id_border').style.display    = "none";
		GetElementWithId('id_imagen').style.display    = "none";
		GetElementWithId('cbElem1').style.display      = 'inline';
		GetElementWithId('cbElem2').style.display      = 'inline';
		GetElementWithId('col_color_sel_I1_I2').style.display   = (bIsIE)?'inline':'table-cell';
		GetElementWithId('id_styledegradadoPGB').style.display  = (bIsIE)?'inline':'table-row';
		GetElementWithId('PGB_ShowDecimals').style.display      = (bIsIE)?'inline':'table-row';
		FillCombo('cbElem1',window_dialogArguments[4], aArgsM[2]);
		FillCombo('cbElem2',window_dialogArguments[4], aArgsM[3]);

		//Show Decimals
		GetElementWithId('id_ShowDecimals').checked   = (parseInt(aArgsM[4])==1);
		//Colores
		var cI1 = aArgsM[7];
		if (cI1 != "-1" && cI1 != "transparent" && cI1.indexOf('#') == -1)
		{
			cI1 = '#'+cI1;
		}

		var cI2 = aArgsM[8];
		if (cI2 != "-1" && cI2 != "transparent" && cI2.indexOf('#') == -1)
		{
			cI2 = '#'+cI2;
		}
		OnChangeColor(cI1, "color_sel_I1");
		OnChangeColor(cI2, "color_sel_I2");
		//Style  aArgsM[11] BoderType, para Guardar el Style del ProgressBar
		if(aArgsP[14]=='CilBar')   GetElementWithId('rb_stylePGB_CilBar').checked   = true;
		if(aArgsP[14]=='GlassBar') GetElementWithId('rb_stylePGB_GlassBar').checked = true;
		if(aArgsP[14]=='SoftBar')  GetElementWithId('rb_stylePGB_SoftBar').checked  = true;

		var nTop = aArgsM.length;
		for (i=5; i<7; i++){  //7 Es donde empieza los colores de I1 y I2
			sAddProp = sAddProp+','+aArgsM[i];
		}
	}
	else if (window_dialogArguments[3]==4) { // Calculated Calculated Calculated Calculated Calculated Calculated Calculated Calculated
		GetElementWithId('legend').innerHTML = '&nbsp;&nbsp;'+ML[74];
		GetElementWithId('id_caption').disabled = true;
		var nTop = aArgsM.length;
		for (i=3; i<nTop; i++){
			sAddProp = sAddProp+','+aArgsM[i];
		}
	}
	else if (window_dialogArguments[3]==8) { // Image Image Image Image Image Image Image Image Image Image Image Image Image Image Image
		GetElementWithId('legend').innerHTML = '&nbsp;&nbsp;'+ML[74];
		GetElementWithId('id_caption').disabled = true;
		GetElementWithId('TB_element').style.display = "none";
		GetElementWithId('tr_legend').style.display = "none";
		var nTop = aArgsM.length;
		for (i=3; i<nTop; i++){
			sAddProp = sAddProp+','+aArgsM[i];
		}
	}
	else if (window_dialogArguments[3]==15 || window_dialogArguments[3]==16) { // Line ConfigAdvance Line ConfigAdvance Line ConfigAdvance Line ConfigAdvance
		GetElementWithId('legend').innerHTML = '&nbsp;&nbsp;'+ML[74];
		GetElementWithId('id_caption').disabled = true;
		GetElementWithId('TB_element').style.display         = "none";
		GetElementWithId('tr_legend').style.display          = "none";
		GetElementWithId('id_imagen').style.display          = "none";
		GetElementWithId('id_alignment').style.display       = "none";
		GetElementWithId('id_border').style.display          = "none";
		GetElementWithId('id_colordegradado').style.display  = (bIsIE)?'inline':'table-row';
		GetElementWithId('id_styledegradado').style.display  = (bIsIE)?'inline':'table-row';

		if (window_dialogArguments[3]==15)
		{
			GetElementWithId('id_gradianteHorVer').style.display = (bIsIE)?'inline':'table-row';
		}

		if (window_dialogArguments[3]==16)
		{
			GetElementWithId('trPositionament').style.display = 'none';
		}

		//if(window.bDesigner)
		//{
			GetElementWithId('id_labelVisible').style.display  	 = "none";
			GetElementWithId('id_visible').style.display  		 = "none";
		//}

		var nTop = aArgsM.length;
		for (i=3; i<nTop; i++){
			sAddProp = sAddProp+','+aArgsM[i];
		}

		//Colores
		OnChangeColor(aArgsP[4], "ini_color_sel");
		OnChangeColor(aArgsP[5], "fin_color_sel");

		//Estilo y Orientacion
		aStyle=aArgsP[10].split('@');
		if(aStyle[0]=='CilBar')   GetElementWithId('rb_styledegradado_CilBar').checked   = true;
		if(aStyle[0]=='GlassBar') GetElementWithId('rb_styledegradado_GlassBar').checked = true;
		if(aStyle[0]=='SoftBar')  GetElementWithId('rb_styledegradado_SoftBar').checked  = true;
		if(aStyle[0]=='Grad')     GetElementWithId('rb_styledegradado_Grad').checked     = true;
		if(aStyle[1]=='Ver')      GetElementWithId('rb_degradado_ver').checked = true;
		if(aStyle[1]=='Hor')      GetElementWithId('rb_degradado_hor').checked = true;
	}
	else if (window_dialogArguments[3]==13) { // Variable Variable Variable Variable Variable Variable Variable Variable Variable Variable
		GetElementWithId('legend').innerHTML = '&nbsp;&nbsp;'+ML[74];
		if (aArgsM[2]=='2') {
			GetElementWithId('id_caption').disabled = true;
		}
	}
	else if (window_dialogArguments[3]==0 || window_dialogArguments[3]==100) { // LAbel LAbel LAbel LAbel LAbel LAbel LAbel LAbel LAbel LAbel LAbel LAbel LAbel LAbel LAbel
		if (window_dialogArguments[5] != '') {
			GetElementWithId('trPivotValue').style.display  = (bIsIE)?'inline':'table-row';
			if(aArgsM[3]=='1') { GetElementWithId('id_bPivotValue').checked = true; }
		}
		var nTop = aArgsM.length;
		for (i=5; i<nTop; i++){
			sAddProp = sAddProp+','+aArgsM[i];
		}
		
		if(window_dialogArguments[3]==100)
		{
			GetElementWithId('trButton').style.display  = (bIsIE)?'inline':'table-row';
			
			var oCmb = GetElementWithId('cbButtonOptions');
			sValueSelected=aArgsM[4];
			for (var i in window_dialogArguments[7]) {
				if (window_dialogArguments[7][i] != '') {
					var iAux= oCmb.length;
					oCmb.length = oCmb.length + 1;
					oCmb.options[iAux].value = i;
					oCmb.options[iAux].text = window_dialogArguments[7][i];
					if (i == sValueSelected) {
						oCmb.options[iAux].selected = true;
					}
				}
			}
		}
		
		GetElementWithId('id_caption').focus();
	}

	//Generico  //Generico  //Generico  //Generico  //Generico
	//Generico  //Generico  //Generico  //Generico  //Generico
	GetElementWithId('Rep_id').value     = aArgsM[0];
	GetElementWithId('Rep_name').value   = aArgsM[1];
	GetElementWithId('id_caption').value = aArgsM[2].replace(regexp,',');
	// Calculated, Image, Variable
	if (window_dialogArguments[3]==4 || window_dialogArguments[3]==8 || window_dialogArguments[3]==13)
	{
		GetElementWithId('Rep_name').value = aArgsM[2].replace(regexp,',');
		GetElementWithId('id_caption').value = aArgsM[1].replace(regexp,',');
	}
	if (Trim(aArgsP[17]) == '1')
	{
		GetElementWithId("id_visible").checked = true;
	}
	GetElementWithId('id_left').value = aArgsP[0];
	GetElementWithId('id_top').value = aArgsP[1];
	GetElementWithId('id_width').value = aArgsP[2];
	GetElementWithId('id_height').value = aArgsP[3];

	GetElementWithId('rb_align_0').checked = false;
	GetElementWithId('rb_align_1').checked = false;
	GetElementWithId('rb_align_2').checked = false;
	GetElementWithId('rb_align_'+aArgsP[13]).checked = true;

	if(window_dialogArguments[3]!=11) //Que No sea ProgresBar pq en el color del Borde pongo el Tipo de Efecto de la Barra
	{
		OnChangeColor(aArgsP[14], "border_color_sel");
	}

	var sBTImage = "";
	var sBTHint = "";
	switch(aArgsP[15]){
		case "0":
			sBTImage = 'images/border_none.gif';
			sBTHint  = ML[245];
			break;
		case "1":
			sBTImage = 'images/border_regular.gif';
			sBTHint  = ML[127];
			break;
		case "2":
			sBTImage = 'images/border_raised.gif';
			sBTHint  = ML[596];
			break;
		case "3":
			sBTImage = 'images/border_drop.gif';
			sBTHint  = ML[597];
			break;
	}
	change_image(sBTImage, 'id_img_selection_border_type_sel', sBTHint, 'border_type_sel', aArgsP[15]);

	GetElementWithId('id_border_width').value = aArgsP[16];

	var sImgPath = '';
	if (Trim(aArgsP[11]) != '') {
		sImgPath = aArgsP[11];
	}
	if (window_dialogArguments[3]==8 && Trim(sImgPath) == '' && aArgsM[2] != '') {
		sImgPath = aArgsM[2];
	}
	if (Trim(sImgPath) != '') {
		GetElementWithId("idDefImg").src = getValidImageSrc(sImgPath);
	}
	GetElementWithId("idDefImg").srcImage = sImgPath;

	if (Trim(aArgsP[12]) == '1') {
		GetElementWithId("id_streched").checked = true;
	}

	//Traducir los bordes
	GetElementWithId("id_img_0").title=ML[245];
	GetElementWithId("id_img_1").title=ML[127];
	GetElementWithId("id_img_2").title=ML[596];
	GetElementWithId("id_img_3").title=ML[597];
}
function FillCombo(idCombo, sValues, sValueSelected){
	var oCmb = GetElementWithId(idCombo);
	var aElems = sValues.split('_AWSepElement_');
	var nTop = aElems.length;
	var aValues = new Array();
	for (var i=0; i<nTop; i++) {
		if (aElems[i] != '') {
			aValues = aElems[i].split('_AWSepName_');
			oCmb.length = oCmb.length + 1;
			oCmb.options[i].value = aValues[0];
			oCmb.options[i].text = aValues[1];
			if (aValues[0] == sValueSelected) {
				oCmb.options[i].selected = true;
			}
		}
	}
}
function FillComboTotalInd(idCombo, aAllValues, sValues, bAll, sValueSelected){
	var oCmb = GetElementWithId(idCombo);
	oCmb.length = 0;
//debugger;
	if (bAll) {
		for (var iKey in aAllValues) {
			if (aAllValues[iKey] != '') {
				oCmb.length = oCmb.length + 1;
				oCmb.options[oCmb.length - 1].value = iKey;
				oCmb.options[oCmb.length - 1].text = specialcharstohtml(aAllValues[iKey]);
				if (iKey == sValueSelected) {
					oCmb.options[oCmb.length - 1].selected = true;
				}
			}
		}
	}
	else {
		var aElems = sValues.split(',');
		var nTop = aElems.length; var bSetSelected = false;
		for (var i=0; i<nTop; i++) {
			if (aElems[i] != '' && aAllValues[sIniPart+aElems[i]]!=undefined) {
				oCmb.length = oCmb.length + 1;
				oCmb.options[oCmb.length - 1].value = sIniPart+aElems[i];
				oCmb.options[oCmb.length - 1].text = specialcharstohtml(aAllValues[sIniPart+aElems[i]]);
				if (sIniPart+aElems[i] == sValueSelected) {
					oCmb.options[oCmb.length - 1].selected = true;
					bSetSelected = true;
				}
			}
		}
		if (!bSetSelected && aAllValues[sValueSelected] != undefined) {
			oCmb.length = oCmb.length + 1;
			oCmb.options[oCmb.length - 1].value = sValueSelected;
			oCmb.options[oCmb.length - 1].text = specialcharstohtml(aAllValues[sValueSelected]);
			oCmb.options[oCmb.length - 1].selected = true;
		}
	}
}
function ShowAll(){
	FillComboTotalInd('cbIndicators', aCubeIndJS, sBodyInds, GetElementWithId('id_viewall').checked, sInitSel);
}
function SetSelectedCB(objId, sKey){
	if (sKey == '') { return; }
	var objCBD = GetElementWithId(objId);
	var nTop = objCBD.length;
	for (var i=0; i<nTop; i++) {
		if (objCBD.options[i].value == sKey){
			objCBD.options[i].selected = true;
			break;
		}
	}
}
function ResetTotalType(){
	var objCBD = GetElementWithId('cbTotalType');
	objCBD.length = 4;
	objCBD.options[0].value = 1;
	objCBD.options[0].text = ML[129];
	objCBD.options[1].value = 2;
	objCBD.options[1].text = ML[130];
	objCBD.options[2].value = 3;
	objCBD.options[2].text = ML[131];
	objCBD.options[3].value = 4;
	objCBD.options[3].text = ML[366];
}
function Add_Events(){
	onmouseoverEvent =
		function() {
			clear_Tempor();
		};
	if (bIsIE) {
		GetElementWithId('id_div_padre_element').attachEvent("onmouseover",onmouseoverEvent);
	}
	else {
		GetElementWithId('id_div_padre_element').addEventListener('mouseover', onmouseoverEvent, false);
		GetElementWithId('id_div_padre_element').addEventListener('click', onmouseoverEvent, false);
	}
}

function SelImg(){
	// Lamar a forma en donde se seleccionará la imagen y retornará el url para ponerlo como fondo
  /*
	window.showModalDialog(
    'sel_image.php',
    [window,ML[362] + '->'+ML[361]],
		'dialogHeight: 470px; dialogWidth: 380px; center: yes; scroll: No; help:  No; resizable: Yes; status:no;'
  );
  */
  openWindowDialog(
    'sel_image.php',
    [window,ML[362] + '->' + ML[361]],
    [],
    380, 470,
    dlgCallBack_SelImg,
    "dlgCallBack_SelImg"
  );
  /*
	if (window.returnValue !== null){
		set_BackgroundImage('idDefImg', window.returnValue, '');
		window.returnValue = null;
	}
  */
}

function dlgCallBack_SelImg(returnValue, aArgContinue)
{
	if (returnValue !== '' && returnValue !== undefined)
    set_BackgroundImage('idDefImg', returnValue, '');
}

function set_BackgroundImage(id_col, url_image, otid_col){
	GetElementWithId(id_col).src = getValidImageSrc(url_image);
	GetElementWithId(id_col).srcImage = url_image;
}

function ClearImg(){
	GetElementWithId('idDefImg').src = 'images/transparent.gif';
	GetElementWithId('idDefImg').srcImage = 'images/transparent.gif';
}

function On_Ok(){
	//',,,0,10,80,20,FFFFFF,000000,0,0,0,11,ms sans serif,,1,1,000000,0,1,1';
	var regexp = new RegExp(',',"gi");
	var sRepId = GetElementWithId('Rep_id').value;
	var sRepName = GetElementWithId('Rep_name').value;
	var bOk = true;  var aInd = new Array(); var bNewName = false;
	if (GetElementWithId('Rep_type').value == 0 || GetElementWithId('Rep_type').value == 100)
	{
		var sRepCaption = GetElementWithId('id_caption').value;
		var bPivVal = GetElementWithId('id_bPivotValue').checked;
		if (Trim(sRepCaption) == "" && !bPivVal && GetElementWithId('Rep_type').value != 100)
		{
			alert(ML[439]+'('+ML[591]+')');
			GetElementWithId('id_caption').focus();
			return;
		}
		sRepCaption = sRepCaption.replace(regexp,AWComa);
	}
	else if (GetElementWithId('Rep_type').value == 1) 	// Dimension
	{
		var objCBDim = GetElementWithId('cbDimensions');
		if (objCBDim.selectedIndex == -1) {
			alert(ML[321]);
			GetElementWithId('cbDimensions').focus();
			return;
		}
		sRepName = ML[354]+'_'+objCBDim.options[objCBDim.selectedIndex].text;
		sRepName = sRepName.replace(regexp,AWComa);
		var sRepCaption = objCBDim.options[objCBDim.selectedIndex].value;
		if (sRepCaption == sInitSel) {
			sRepName = GetElementWithId('Rep_name').value;
		}

		// Set Period
		var sPeriodDim = '-1';
		// revisar que esté visible la fila
		var objTRPeriod = GetElementWithId('tr_period');
		if (objTRPeriod)
		{
			// obtener el valor seleccionado
			var objCBPeriod = GetElementWithId('cbPeriods');
			if (objCBPeriod.selectedIndex != -1) { sPeriodDim = objCBPeriod.options[objCBPeriod.selectedIndex].value; }
		}
		sAddProp = sAddProp.replace('_#awPeriodkeyaw#_', sPeriodDim);
	}
	else if (GetElementWithId('Rep_type').value == 2 || GetElementWithId('Rep_type').value == 3)   // Indicator, Total
	{
		var objCBInd = GetElementWithId('cbIndicators');
		if (objCBInd.selectedIndex == -1) { bOk = false; }
		if (bOk) {
			sRepName = objCBInd.options[objCBInd.selectedIndex].value;
			if (sRepName != sInitSel) { bNewName = true; }
			// Format Indicator / Total
			if (bAddFormat) {
				var sAddFormat = '#_#AWComa#_##0.00';
				if (aIndsFormats[sRepName] != undefined) {
					sAddFormat = aIndsFormats[sRepName];
				}
				sAddProp = sAddProp.replace('_#NEW#_', sAddFormat);
			}
			if (GetElementWithId('Rep_type').value == 3) {
				if (sRepName.indexOf('_AWSep_')==-1) {
					bOk = false;
				}
				else {
					aInd = sRepName.split('_AWSep_');
					sRepName = aInd[2];
				}
				if (bOk) {
					// Set Total Type
					var sTotalType = '0';
					var objCBTT = GetElementWithId('cbTotalType');
					if (objCBTT.selectedIndex != -1) { sTotalType = objCBTT.options[objCBTT.selectedIndex].value; }
					sAddProp = sAddProp.replace('_#awSETTOTALTYPEaw#_', sTotalType);
				}
			}
			else {
				var bNotNumeric = 0;
				if (aIndsNotNumeric[sRepName] != undefined) {
					bNotNumeric = aIndsNotNumeric[sRepName];
				}
				sAddProp = sAddProp.replace('_#NOTNUM#_', bNotNumeric);
			}
		}
		if (!bOk) {
			alert(ML[553]);
			GetElementWithId('cbIndicators').focus();
			return;
		}
		if (bNewName) {
			var sRepCaption = ML[95];
			if (GetElementWithId('Rep_type').value == 3) {
				sRepCaption = sRepCaption + ' ' + ML[106];
			}
			sRepCaption = sRepCaption + '_'+objCBInd.options[objCBInd.selectedIndex].text;
			if ((GetElementWithId('id_caption').value.lastIndexOf(')')+1) == GetElementWithId('id_caption').value.length) {
				var ipos = GetElementWithId('id_caption').value.lastIndexOf('(');
				sRepCaption = sRepCaption+GetElementWithId('id_caption').value.substring(ipos);
    		}
		}
		else {
			var sRepCaption = GetElementWithId('id_caption').value;
		}
		sRepName = sRepName.replace(regexp,AWComa);
		sRepCaption = sRepCaption.replace(regexp,AWComa);
	}
	else if (GetElementWithId('Rep_type').value == 11) {
		var objCBElem = GetElementWithId('cbElem1');
		if (objCBElem.selectedIndex == -1) {
			alert(ML[435]+' ('+ML[604]+')');
			GetElementWithId('cbElem1').focus();
			return;
		}
		sElemId1 = objCBElem.options[objCBElem.selectedIndex].value;
		sElemId1 = sElemId1.replace(regexp,AWComa);

		var objCBElem = GetElementWithId('cbElem2');
		if (objCBElem.selectedIndex == -1) {
			alert(ML[435]+' ('+ML[604]+')');
			GetElementWithId('cbElem2').focus();
			return;
		}
		sElemId2 = objCBElem.options[objCBElem.selectedIndex].value;
		sElemId2 = sElemId2.replace(regexp,AWComa);

		var CI_1 = GetElementWithId('color_sel_I1').value;
		if (CI_1 != "-1")
		{
			if (CI_1.indexOf('#') == 0) {
				CI_1 = CI_1.substring(1);
			}
		}
		var CI_2 = GetElementWithId('color_sel_I2').value;
		if (CI_2 != "-1")
		{
			if (CI_2.indexOf('#') == 0) {
				CI_2 = CI_2.substring(1);
			}
		}

		var sRepCaption = ML['Progress_Bar'];
	}
	else if (GetElementWithId('Rep_type').value == 4) {
		sRepName = GetElementWithId('id_caption').value;
		var sRepCaption = GetElementWithId('Rep_name').value;
		sRepName = sRepName.replace(regexp,AWComa);
		sRepCaption = sRepCaption.replace(regexp,AWComa);
	}
	else if (GetElementWithId('Rep_type').value == 8) {
		sRepName = GetElementWithId('id_caption').value;
		var sRepCaption = '';
		sRepName = sRepName.replace(regexp,AWComa);
	}
	else if (GetElementWithId('Rep_type').value == 13) {
		sRepName = GetElementWithId('id_caption').value;
		var sRepCaption = GetElementWithId('Rep_name').value;
		sRepName = sRepName.replace(regexp,AWComa);
		sRepCaption = sRepCaption.replace(regexp,AWComa);
	}

	var sLeft = GetElementWithId('id_left').value;
	if ((Trim(sLeft) == "") || (isNaN(sLeft)) || (parseInt(sLeft) < 0)) {
		alert(ML[439]+'('+ML[478]+')');
		GetElementWithId('id_left').focus();
		return;
	}
	else {
		sLeft = parseInt(sLeft);
	}
	var sTop = GetElementWithId('id_top').value;
	if ((Trim(sTop) == "") || (isNaN(sTop)) || (parseInt(sTop) < 0)) {
		alert(ML[439]+'('+ML[476]+')');
		GetElementWithId('id_top').focus();
		return;
	}
	else {
		sTop = parseInt(sTop);
	}
	var sWidth = GetElementWithId('id_width').value;
	if ((Trim(sWidth) == "") || (isNaN(sWidth)) || (parseInt(sWidth) <= 0)) {
		alert(ML[439]+'('+ML[592]+')');
		GetElementWithId('id_width').focus();
		return;
	}
	else {
		sWidth = parseInt(sWidth);
	}
	var sHeight = GetElementWithId('id_height').value;
	if ((Trim(sHeight) == "") || (isNaN(sHeight)) || (parseInt(sHeight) <= 0)) {
		alert(ML[439]+'('+ML[587]+')');
		GetElementWithId('id_height').focus();
		return;
	}
	else {
		sHeight = parseInt(sHeight);
	}

	//BG color
	if (GetElementWithId('Rep_type').value == 15 || GetElementWithId('Rep_type').value == 16)
	{
		var FBC = GetElementWithId('ini_color_sel').value;
		if (GetElementWithId('Rep_type').value == 15 && FBC != "-1")
		{
			if (FBC.indexOf('#') == 0) {
				FBC = FBC.substring(1);
			}
		}
		//Fore color
		var FFC = GetElementWithId('fin_color_sel').value;
		if (GetElementWithId('Rep_type').value == 15 && FFC != "-1")
		{
			if (FFC.indexOf('#') == 0) {
				FFC = FFC.substring(1);
			}
		}
	}
	else
	{
		var FBC = GetElementWithId('nieto_font_bgcolor_element').value;
		if (FBC != "-1") {
			if (FBC.indexOf('#') == 0) {
				FBC = FBC.substring(1);
			}
		}
		//Fore color
		var FFC = GetElementWithId('nieto_font_forecolor_element').value;
		if (FFC != "-1") {
			if (FFC.indexOf('#') == 0) {
				FFC = FFC.substring(1);
			}
		}
	}
	// Bold, Italic, Underline
	var sBold = '0'; var sItalic = '0'; var sUnderline = '0';
	if(GetElementWithId('id_font_bold_element').checked){
		sBold = '1';
	}
	if (GetElementWithId('id_font_italic_element').checked) {
	    sItalic = '1';
	}
	if (GetElementWithId('id_font_underline_element').checked) {
	    sUnderline = '1';
	}
	//Size
	var sFS = GetElementWithId('id_font_size_element').value;
	//Name
	if (GetElementWithId('Rep_type').value == 15 || GetElementWithId('Rep_type').value == 16)
	{
		//Estilo y Orientacion
		var sFN = '';
		if(GetElementWithId('rb_styledegradado_CilBar').checked)   sFN='CilBar@';
		if(GetElementWithId('rb_styledegradado_GlassBar').checked) sFN='GlassBar@';
		if(GetElementWithId('rb_styledegradado_SoftBar').checked)  sFN='SoftBar@';
		if(GetElementWithId('rb_styledegradado_Grad').checked)     sFN='Grad@';
		if(GetElementWithId('rb_degradado_ver').checked)  sFN+='Ver';
		if(GetElementWithId('rb_degradado_hor').checked)  sFN+='Hor';
	}
	else
	{
		var sFN = GetElementWithId('id_font_family_element').value;
	}
	//Align
	if (GetElementWithId('rb_align_0').checked) {
		var sAlign = '0';
	}
	else if (GetElementWithId('rb_align_1').checked) {
		var sAlign = '1';
	}
	else if (GetElementWithId('rb_align_2').checked) {
		var sAlign = '2';
	}
	//Border
	var bordercolor = GetElementWithId('border_color_sel').value;
	if (bordercolor != "-1") {
		if (bordercolor.indexOf('#') == 0) {
			bordercolor = bordercolor.substring(1);
		}
	}
	var bordertype = GetElementWithId('border_type_sel').value;
	var borderwidth = GetElementWithId('id_border_width').value;
	if ((Trim(borderwidth) == "") || (isNaN(borderwidth)) || (parseInt(borderwidth) < 0))
	{
		alert(ML[439]+'('+ML[595]+'->'+ML[592]+')');
		GetElementWithId('id_border_width').focus();
		return;
	}
	else {
		borderwidth = parseInt(borderwidth);
	}
	//Image
	var url_image = GetElementWithId("idDefImg").srcImage;
	if (url_image.indexOf('images/transparent.gif') != -1){ url_image = ''; }
	if (url_image == '' && GetElementWithId('Rep_type').value == 8)
	{
		alert(ML[364]);
		return;
	}
	//Streched
	var bStreched = "0";
	if (GetElementWithId("id_streched").checked) { bStreched = "1"; }
	//Visible
	var bVisible = "0";
	if (GetElementWithId("id_visible").checked) { bVisible = "1"; }


	//Especificos
	var sParamReturn = sRepId+','+sRepName+','+sRepCaption;
	if (GetElementWithId('Rep_type').value == 1)
	{
		sParamReturn = sParamReturn + ',' + sAddProp;
	}
	else if (GetElementWithId('Rep_type').value == 2 || GetElementWithId('Rep_type').value == 3 || GetElementWithId('Rep_type').value == 4)
	{
		sParamReturn = sParamReturn+sAddProp;
		if (GetElementWithId('Rep_type').value == 3) {  // Total, agregar el Type, Clave
			var stype = (aInd[0] == 'Cube') ? 0 : 1;
			sParamReturn = sParamReturn + ',' + stype + ',' + aInd[1] + ',' + sAddPropFin;
		}
	}
	else if (GetElementWithId('Rep_type').value == 11) { //PGBar
		if(GetElementWithId('rb_stylePGB_CilBar').checked)   bordercolor='CilBar';
		if(GetElementWithId('rb_stylePGB_GlassBar').checked) bordercolor='GlassBar';
		if(GetElementWithId('rb_stylePGB_SoftBar').checked)  bordercolor='SoftBar';

		var ShowDecimals=(GetElementWithId('id_ShowDecimals').checked?'1':'0');
		sAddProp=','+ShowDecimals+sAddProp;

		sParamReturn = sRepId+','+sRepName+','+sElemId1+','+sElemId2;
		sParamReturn = sParamReturn+sAddProp+','+CI_1+','+CI_2;
	}
	else if (GetElementWithId('Rep_type').value == 8 || GetElementWithId('Rep_type').value == 15 || GetElementWithId('Rep_type').value == 16) {
		sParamReturn = sRepId+','+sRepName+','+url_image+','+bStreched;
	}
	else if (GetElementWithId('Rep_type').value == 0 || GetElementWithId('Rep_type').value == 100) {
		var sPivotValue = '0';
		if (GetElementWithId('id_bPivotValue').checked) { sPivotValue = '1'; }
		if (GetElementWithId('Rep_type').value == 100){
			sParamReturn += ','+sPivotValue;
			var objCBElem = GetElementWithId('cbButtonOptions');
			if (objCBElem.selectedIndex == -1) {
				alert(ML[435]+' ('+ML['Sections']+')');
				objCBElem.focus();
				return;
			}
			sParamReturn += ','+ objCBElem.options[objCBElem.selectedIndex].value+sAddProp;
		}
		else
		{
			sParamReturn += ','+sPivotValue+sAddProp;
		}
	}
	sParamReturn = sParamReturn+'_#AWProps#_'+sLeft+','+sTop+','+sWidth+','+sHeight;
	sParamReturn = sParamReturn+','+FBC+','+FFC+','+sBold+','+sItalic+','+sUnderline+','+sFS+','+sFN;
	sParamReturn = sParamReturn+','+url_image+','+bStreched+','+sAlign+','+bordercolor+','+bordertype;
	sParamReturn = sParamReturn+','+borderwidth+','+bVisible;
  /*
	owner.returnValue = sParamReturn;
	cerrar();
  */
  //alert(sParamReturn);
  setDialogReturnValue(sParamReturn);
  closeDialog();
}

function checkPeriod(objSelectDim)
{
	if (objSelectDim.selectedIndex != -1)
	{
		var sValDim = objSelectDim.options[objSelectDim.selectedIndex].value;
		if (sValDim != undefined)
		{
			var objTRPeriod = GetElementWithId('tr_period');
			if (objTRPeriod)
			{
				var bSupportDisplay = (!IsThatVersion("MSIE 6.") && !IsThatVersion("MSIE 7."));
				var sDisplay = (bSupportDisplay) ? 'table-row' : 'inline';
				objTRPeriod.style.display = (aConceptDimDate[sValDim] == 1) ? sDisplay : 'none';
			}
		}
	}
}