var bIsIE            = navigator.userAgent.indexOf("MSIE") != -1;
var bFFModal         = FFModal();
var aRowsBCKG        = new Array();
var aRowsBCKGMarked  = new Array();
var ccOperators      = new Array();
var ccVariables      = new Array();
var bShowHeaderComms = true;
var bChangePass      = true;
var sLogginFrom      = 'ARTUS';
var ComCubeUserLevels = new Array();
var nAddToResize = 40;
var bHaveTbComms = false;
var sStyWidth = '';
var gbFBM_WMode = false;
var bHaveNewMsgs = false;
var dateUsageStat = null;
var hourUsageStat = null;
var timeIUsageStat = null;
var bAddBodyEvtReportTB = true;
var bApplyFilters = 0;
var CurrentUserKey = -1;
var bIsMobile = IsMobile(navigator.userAgent); //'blackberry'
var aMarkedRowsTable = new Array();
var bWaitAjax = false;
var bExportDash = false;

function keyDown(anEvent)
{
	if (anEvent.keyCode == 27)
	{
		hideCalendarDiv();
		if (typeof(removeChild_oDivTB) != 'undefined') {
			removeChild_oDivTB();
		}
	}
}
function GetElementWithId(anId) {
	return document.getElementById(anId);
}
function GetIFrameWithId(anID)
{
	if (document.frames)
	{
		return document.frames[anID];
	}
	else if (GetElementWithId(anID))
	{
		return GetElementWithId(anID).contentDocument;
	}
}
function GetIFrameSonWithId(anIframe, anID)
{
	if (document.frames)
	{
		if (GetIFrameWithId(anIframe)) {
			if (GetIFrameWithId(anIframe).GetElementWithId) {
				if (GetIFrameWithId(anIframe).GetElementWithId(anID)) {
					return GetIFrameWithId(anIframe).GetElementWithId(anID);
				}
			}
		}
	}
	else
	{
		if (GetIFrameWithId(anIframe))
		{
			if (GetIFrameWithId(anIframe).getElementById(anID)) {
				return GetIFrameWithId(anIframe).getElementById(anID);
			}
		}
	}
}
function myReplace(sSource, sFind, sNew)
{
	if(sFind.length==0) return sSource;
	ipos=sSource.indexOf(sFind, 0);
	while(ipos >= 0)
	{
		sSource = sSource.replace(sFind, sNew)
		ipos=sSource.indexOf(sFind, ipos+sNew.length);
	}
	return sSource;
}
function IsThatVersion(sVer){
	var bIsVersion = false;
	if (navigator.userAgent.indexOf(sVer) != -1) {
		bIsVersion = true;
	}
	return bIsVersion;
}
function showCommentAutomatic(ObjId,x,y,sWhatShow,e)
{
	if (window.document.documentElement.clientHeight == 0){
		var objDocB = document.body;
	}
	else {
		var objDocB = window.document.documentElement;
	}
	e = (window.event)?window.event:e;
	if (x == -1) {
		x=e.clientX;
	}
	if (y == -1) {
		y=e.clientY;
	}
	if (!bIsIE) {
		x = x + 5;
		y = y + 5;
	}
	var htmlStr = "";
	var comment = "";
	var objElem = GetElementWithId(ObjId);
	if (objElem == null) { return; }

	comment = objElem.getAttribute("comment");

    if ((comment=="") || (comment == undefined)) {
		comment = "";
		return;
	}

	sViewComment = "";
	sauxPathImg = GetPath();
	sEncComm = '<div style="background-image: url('+sauxPathImg+'images/degradaa.gif); font-size:11px; font-family:Arial; color: #fff; width:100%; word-wrap: break-word;">';
	sEncComm2 = '<div style="background-image: url('+sauxPathImg+'images/degradvv.gif); font-size:11px; font-family:Arial; color: #000;">';
	switch (sWhatShow) {
		case 'filter':
			var regexp = new RegExp('<',"gi");
			comment = comment.replace(regexp,"&lt;");

			sViewComment = sViewComment + sEncComm + '<b>' + ML[108] + '</b></div><div style="color:navy; font-weight:bold; font-size:12px; background-color:white;" >' + comment + '</div>';

			htmlStr = htmlStr + '<div id="DivComment" style="height:auto;"';
			htmlStr = htmlStr + ' >'+sViewComment+'</div>';
			document.getElementById('inputValueComment').style.height = "auto";
			document.getElementById('inputValueComment').innerHTML = htmlStr;
			document.getElementById('inputValueComment').noWrap = true;

			// xcoord boundaries
			if (x + document.getElementById('inputValueComment').offsetWidth + objDocB.scrollLeft > objDocB.clientWidth)
			{
				x = objDocB.clientWidth - document.getElementById('inputValueComment').offsetWidth - objDocB.scrollLeft;
				if (x < 0) {
					x = 0;
				}
				if (x < objDocB.scrollLeft) {
					x = objDocB.scrollLeft;
				}
			}
			document.getElementById('inputValueComment').style.left = x+"px";

			if (!IsThatVersion("MSIE 6.")) {
			  document.getElementById('inputValueComment').style.width = "auto";
			}
			if (x + document.getElementById('inputValueComment').offsetWidth > objDocB.clientWidth + objDocB.scrollLeft) {
				document.getElementById('inputValueComment').style.width = (objDocB.clientWidth - x + objDocB.scrollLeft) + "px";
				document.getElementById('inputValueComment').noWrap = false;
			}
			// ycoord boundaries
			y += objDocB.scrollTop+5;
			document.getElementById('inputValueComment').style.top = y+"px";
			if (!bIsIE) { document.getElementById('inputValueComment').style.display="inline"; }
			break;
		default:	// comments
			if (ObjId.indexOf('CELL') != -1){	// If Cell
				bShowHeaderComms = true;
			}
			aCommentX = comment.split("<b>");
			nTop = aCommentX.length;
			var aAuxComm = new Array();
			var sDateF = '';
			for (i=1; i<nTop; i++){
				if (aCommentX[i] != ''){
					sAuxCommentX = aCommentX[i].split("_AWC_")[1];
					sAuxCommentX = sAuxCommentX.split("</b><br><i>");
					if (bShowHeaderComms) {
						aAuxComm = sAuxCommentX[0].split(': ');
						var aAuxCommP3 = aAuxComm[0].split('_USRK_');
						var sUserLongName = aAuxCommP3[0];
						if (gbFBM_WMode && aAuxCommP3[1] != undefined) {
							var sAuxName = getUserLongName(parseInt(aAuxCommP3[1]));
							if (sAuxName != '_#NOTFOUND#_') {
								sUserLongName = sAuxName;
							}
						}
						sDateF = GetFormatDate(aAuxComm[1],ML['FormatDT']);
						sViewComment = sViewComment + sEncComm + '<b>' + sUserLongName + ': '+ sDateF + '</b></div>';
					}
					sAuxCommentX = sAuxCommentX[1].split("</i><br>");
					if (bShowHeaderComms) {
						aAuxComm = sAuxCommentX[0].split(' ');
						if (aAuxComm[1] == '9999-99-99' || aAuxComm[1] == '') {
							sDateF = ML[333];
						}
						else {
							sDateF = GetFormatDate(aAuxComm[1],ML['FormatDT']);
						}
						sViewComment = sViewComment + sEncComm2 + '<i>' + aAuxComm[0] + ' '+ sDateF + '</i></div>';
					}
					sAuxCommentX = sAuxCommentX[1].split("_AWStyle_");
					aCommStyle = sAuxCommentX[0].split("_");
					commStyle = "font-family: " + aCommStyle[0] + "; font-weight: " + (aCommStyle[1] == '1'?'bold':'normal') + "; ";
					commStyle = commStyle + "font-style: " + (aCommStyle[2] == '1'?'italic':'normal') + "; text-decoration: " + (aCommStyle[3] == '1'?'underline':'none') + "; ";
					commStyle = commStyle + "font-size: " + aCommStyle[4] + "px; background-color: " + aCommStyle[5] + "; color: " + aCommStyle[6] + ";";
					if (!bShowHeaderComms) {
						commStyle = commStyle + "border-bottom: 1px solid gray;";
					}
					sAuxCommentX[1] = sAuxCommentX[1].replace(/<br>/gi,'_AWMI_br_AWMA_');
					sAuxCommentX[1] = sAuxCommentX[1].replace(/</gi,'&lt;');
					sAuxCommentX[1] = sAuxCommentX[1].replace(/_AWMI_br_AWMA_/gi,'<br>');
					sViewComment = sViewComment + '<div style="' + commStyle + '">' + sAuxCommentX[1] + '</div>';
				}
			}
			document.getElementById('inputValueComment').style.width = "200px";
			document.getElementById('inputValueComment').style.left = (objDocB.scrollLeft + x)+"px";
			document.getElementById('inputValueComment').style.top = (objDocB.scrollTop + y)+"px";

			htmlStr = htmlStr + '<div id="DivComment" style="height:auto;"';
			htmlStr = htmlStr + ' >'+sViewComment+'</div>';
			document.getElementById('inputValueComment').style.height = "auto";
			document.getElementById('inputValueComment').noWrap = false;
			document.getElementById('inputValueComment').innerHTML = htmlStr;
			if (!bIsIE) { document.getElementById('inputValueComment').style.display="inline"; }
			break;
	}
}
function hideComment() {
	if(document.getElementById("DivComment")!=null)
	{
        document.getElementById("DivComment").style.display="none";
        document.getElementById('inputValueComment').innerHTML = "";
        if (bIsIE) {
	        document.getElementById('inputValueComment').style.width = "0px";
	        document.getElementById('inputValueComment').style.height = "0px";
	        document.getElementById('inputValueComment').style.left = "-20px";
	        document.getElementById('inputValueComment').style.top = "-20px";
        }
        else {
        	document.getElementById('inputValueComment').style.display="none";
        }
	}
}
function CPParams(sEASOption,sEASNewValue)
{
	GetElementWithId("EASOption").value    = sEASOption;
	GetElementWithId("EASNewValue").value  = sEASNewValue;
	GetElementWithId("ScenForm").target = GetTargetName();
	GetElementWithId("ScenForm").action = "../../eas_comm.php?bHaveTbComms="+getBoolStr(bHaveTbComms)+"&sStyWidth="+sStyWidth;
	GetElementWithId("ScenForm").submit();

	ap_showWaitMessage('waitDiv', 3);

	GetElementWithId("EASOption").value    = '';
	GetElementWithId("EASNewValue").value  = '';
}
function TrimLeft( str )
{
	var resultStr = "";
	var i = len = 0;

	if ( str + "" == "undefined" || str == null)
		return null;

	str += "";

	if (str.length == 0)
	{
		resultStr = "";
	}
	else
	{
		len = str.length;

		while ( ((i <= len) && (str.charAt(i) == " ")) || ((i <= len) && (str.charAt(i) == "\n")) || ((i <= len) && (str.charAt(i) == "\r")) || ((i <= len) && (str.charAt(i) == "\t")))
		{
			i++;
		}
		resultStr = str.substring(i, len);
	}
	return resultStr;
}
function TrimRight( str )
{
	var resultStr = "";
	var i = 0;

	if ( str + "" == "undefined" || str == null )
	{	return null;
	}

	str += "";

	if (str.length == 0)
	{
		resultStr = "";
	}
	else
	{
		i = str.length - 1;
		while (	((i >= 0) && (str.charAt(i) == " ")) || ((i >= 0) && (str.charAt(i) == "\n")) || ((i >= 0) && (str.charAt(i) == "\r")) || ((i >= 0) && (str.charAt(i) == "\t")))
		{
				i--;
		}

		resultStr = str.substring(0, i + 1);
	}

	return resultStr;
}
function Trim( str )
{
	var resultStr = "";

	resultStr = TrimLeft(str);
	resultStr = TrimRight(resultStr);

	return resultStr;
}
function Search_Position(DimKey, aGpoIDims, sGpoDDims, nCubeK){	// GET POSITION IN GROUP DIMENSIONS
	// FIND THE GROUP OF THE DIMENSION
	var tam_gpo = aDimKeys[nCubeK].length;
	var auxDimKeys = aDimKeys[nCubeK];
	for (w=0; w<tam_gpo;w++)
		if (auxDimKeys[w] == DimKey)
			break;
	if (w==tam_gpo)	// IF NOT EXIST RETURN
		return -1;

	var auxDimGpos = aDimGroups[nCubeK];
	var sGpoName = Trim(auxDimGpos[w]);	// SET THE NAME OF THE GROUP

	// FIND THE POSITION OF THE GROUP NAME
	var tam_gpo = aGpoIDims.length;
	for (w=0; w<tam_gpo;w++)
		if (sGpoDDims[w] == sGpoName)
			return aGpoIDims[w];

	// IF NOT EXIST INSERT NEW GROUP AND RETURN NEW POSITION
	aGpoIDims[w] = w;
	sGpoDDims[w] = sGpoName;
	return w;
}
function getDimensionName(nDimKey, nCubeK){
    if (aDimKeys[nCubeK]==undefined){ return 'No Name'; }
	var top = aDimKeys[nCubeK].length;
    for (i=0; i<top; i++)
    	if (aDimKeys[nCubeK][i]==nDimKey)
    		return aDimNames[nCubeK][i];
    return 'No Name';
}
function getAttributeName(nAttKey, nConceptK){
    if (aAttributeIds[nConceptK]==undefined){ return 'No Name'; }
	var top = aAttributeIds[nConceptK].length;
    for (i=0; i<top; i++)
    	if (aAttributeIds[nConceptK][i]==nAttKey)
    		return aAttributeNames[nConceptK][i];
    return 'No Name';
}
function getCubeName(nCubeK){
	var top = aCubeIds.length;
    for (i=0; i<top; i++)
    	if (aCubeIds[i]==nCubeK)
    		return aCubeNamesUser[i];
    return 'No Name';
}

function Show_Hide_Dims_Name(ID){
	Show_Hide_Nodes(ID);
}
function markSelectedRow(oRow, localTableId)
{
	if (oRow!=null)
	{
	    if (oRow.tagName == 'TD')
	    {
	    	oRow = oRow.parentElement || oRow.parentNode;
	    	if (oRow == null) { return; }
	    }

	    // Si es la misma fila no hacer nada
	    if (isSameTableRow(oRow))
	    {
	    	return;
	    }

	    // Revisar antes si hay una fila marcada previamente en la tabla
	    if (aMarkedRowsTable[localTableId] != undefined && aRowsBCKGMarked[localTableId] != undefined)
	    {
	    	// desmarcar la fila
	    	var aChilds = aMarkedRowsTable[localTableId].childNodes;
		    var nTop = aChilds.length;

			for (var i=0; i<nTop; i++)
			{
		        if (aChilds.item(i).tagName == 'TD')
		        {
					var oTD = aChilds.item(i);
					oTD.style.backgroundColor = aRowsBCKGMarked[localTableId][i];
		        }
		    }
	    }

	    // Marcar la fila donde se dio clic
		var aChilds = oRow.childNodes;
	    var nTop = aChilds.length;
	    aRowsBCKGMarked[localTableId] = new Array();
	    aMarkedRowsTable[localTableId] = oRow;

		for (var i=0; i<nTop; i++)
		{
	        if (aChilds.item(i).tagName == 'TD')
	        {
				var oTD = aChilds.item(i);
				aRowsBCKGMarked[localTableId][i] = aRowsBCKG[i];
				oTD.style.backgroundColor = nColorSelectedRow;
	        }
	    }
	}
}
function SetSelectedRow(oRow)
{
	if (oRow!=null)
	{
	    if (oRow.tagName == 'TD')
	    {
	    	oRow = oRow.parentElement || oRow.parentNode;
	    	if (oRow == null) { return; }
	    }

	    // Si es la misma fila no hacer nada
	    if (isSameTableRow(oRow))
	    {
	    	return;
	    }

		var aChilds = oRow.childNodes;
	    var nTop = aChilds.length;

		for (var i=0; i<nTop; i++)
		{
	        if (aChilds.item(i).tagName == 'TD')
	        {
				var oTD = aChilds.item(i);
	        	aRowsBCKG[i] = oTD.style.backgroundColor;
				oTD.style.backgroundColor = nColorSelectedRow;
	        }
	    }
	}
}
function UnSetSelectedRow(oRow)
{
	if (oRow!=null)
	{
	    if (oRow.tagName == 'TD')
	    {
	    	oRow = oRow.parentElement || oRow.parentNode;
	    	if (oRow == null) { return; }
	    }

	    // Si es la misma fila no hacer nada
	    if (isSameTableRow(oRow))
	    {
	    	return;
	    }

		var aChilds = oRow.childNodes;
	    var nTop = aChilds.length;

		for (var i=0; i<nTop; i++)
		{
	        if (aChilds.item(i).tagName == 'TD')
	        {
				var oTD = aChilds.item(i);
	        	oTD.style.backgroundColor = aRowsBCKG[i];
	        }
	    }
	}
	aRowsBCKG = new Array();
}
function isSameTableRow(oRow)
{
	// Obtener el objeto HTML Table
    var oTable = (oRow.parentElement) ? oRow.parentElement : oRow.parentNode;
	while (oTable.tagName != 'TABLE')
	{
		oTable = (oTable.parentElement) ? oTable.parentElement : oTable.parentNode;
	}

	// Revisar si está definido ese id de la tabla con el ultimo que se dio clic
	var localTableId = oTable.id.substring(5);
	if (aMarkedRowsTable[localTableId] != undefined)
	{
		// Revisar si es la misma fila, de ser la misma fila no hacer nada
		var trNoFOver   = oRow.getAttribute('trNoF');
		var trLevelOver = oRow.getAttribute('trLevel');

		var trNoFMarked   = aMarkedRowsTable[localTableId].getAttribute('trNoF');
		var trLevelMarked = aMarkedRowsTable[localTableId].getAttribute('trLevel');

		if (trNoFOver && trLevelOver && trNoFMarked && trLevelMarked &&
			trNoFOver == trNoFMarked && trLevelOver == trLevelMarked)
		{
			return true;
		}
	}

	return false;
}
function GetPath(){
	sauxPath = "../../";
	if (IsEAS != undefined){
		if (bDesigner != undefined) {
			if (bDesigner) {
				sauxPath = "";
			}
		}
		if (IsEAS == 1) {
			sauxPath = "";
		}
	}
	return sauxPath;
}
function editInd(Cons, Name, Format, bShowSubt, bUseOrigFor, sGroup)
{
	if (bLoading==0){
		return;
	}
	sauxPath = GetPath();

	openWindowDialog(sauxPath+'editind.htm?_child=:child:', [window, ML[81], ML[74], ML[78], ML[82], ML[69],ML[70],
	        			 Name, Format, bShowSubt, ML[493], bUseOrigFor, ML, 'OTHER', lang, sGroup],
				        [Cons], 508, 350, editIndModalValues, 'editIndModalValues');
}
function editIndModalValues(sNewValues, aArgCont)
{
	if (sNewValues != undefined)
	{
		var Cons = aArgCont[0];

		EASParams("editInd", Cons + "_AWSep_" + sNewValues);
	    window.returnValue = null;
	}
}
function addCalColumn(Cons, Name, Type, Formula, Format, bCalcPiv, sFunc)
{
	if (bLoading==0){
		return;
	}
    var labels = [ML[74], ML[75], ML[76], ML[77], ML[78], ML[79],ML[69],ML[70],
    				Name, Type, Formula, Format, ML[683], ML[408], ML];
    sauxPath = GetPath();

    var scolidsearch = '{('+Cons+') ';
    var ccOperatorsLocal = [[ML[398]],[ML[509]],[ML[510]],null];
    var nTopOper = ccOperators[0].length;
    var bFound = false;
    for (var ioper=1; ioper<nTopOper; ioper++)
    {
    	// si se encontró, validar que las columnas sucesivas sean unicamente indicadores
    	var bContinue = true;
    	if (bFound)
    	{
    		var sopercol = ccOperators[0][ioper];
    		var posini = sopercol.indexOf('{(');
    		if (posini != -1)
    		{
    			var posfin = sopercol.indexOf(') ', posini+2);
    			if (posfin != -1)
    			{
    				var scolid = sopercol.substring(posini+2, posfin);
    				if (IsEAS)
    				{
	    				if (arrColTypes[scolid] != undefined && arrColTypes[scolid] == 6) // si es calculada sucesiva a la que se va a editar no debe de agregarse
	    				{
	    					bContinue = false;
	    				}
    				}
    				else
    				{
    					var sTableId = TDOption.replace('Option', '');
    					var aCol = (aMetaTD[sTableId] != undefined && aMetaTD[sTableId]['C'+scolid] != undefined) ? aMetaTD[sTableId]['C'+scolid] : null;

    					if (aCol != null)
						{
							var nTypeCol = (aCol[3] != undefined && aCol[3] != null) ? aCol[3] : 0;

							if (nTypeCol == 6)	// si es calculada sucesiva a la que se va a editar no debe de agregarse
							{
								bContinue = false;
							}
						}
    				}
    			}
    		}
    	}

    	if (bContinue)
    	{
	    	var sopercol = ccOperators[0][ioper];
	    	if (sopercol.indexOf(scolidsearch) == -1)
	    	{
	    		ccOperatorsLocal[0].push(sopercol);

	    		sopercol = ccOperators[1][ioper];
	    		ccOperatorsLocal[1].push(sopercol);
	    	}
	    	else
	    	{
	    		bFound = true;
	    	}
    	}

    	if (ccOperators[2][ioper] != undefined)
    	{
	    	sopercol = ccOperators[2][ioper];
	    	ccOperatorsLocal[2].push(sopercol);
    	}
    }
    /*
    if (HaveModalWindow())
    {
    */
    	if (sFunc==undefined){
			nTitleId = 96;
		}
		else if (sFunc=='CalcCell') {
			nTitleId = 838;
		}
		else{
			nTitleId = 774;
		}
	    var aArgs = [ML[nTitleId], ccOperatorsLocal, ccVariables, labels, window, Name, Type, Formula, Format, false, lang, bCalcPiv];
    	if (IsEAS) {
    		aArgs.push(nPivots);
    		if (nPivots>0) {
    			aArgs.push(aDimsPivsN.slice(0));
    		}
	    }
	    else {
	    	var sTableId = TDOption.replace('Option', '');
	    	if (aMetaTD[sTableId] != undefined && aMetaTD[sTableId][43] != undefined) {
	    		aArgs.push(aMetaTD[sTableId][43]);
	    		if (aMetaTD[sTableId][43] > 0) {
	    			if (arrDimsPivsN[sTableId] != undefined) {
		    			aArgs.push(arrDimsPivsN[sTableId].slice(0));
	    			}
	    		}
	    	}
	    }
      /*
    	showModalDialog(
        sauxPath+'calculatedcolumn.htm',
        aArgs,
        "dialogWidth: 508px; dialogHeight: 450px; resizable: yes; center: yes; help: no; status: no; scroll:no"
      );
      */
      openWindowDialog(
        sauxPath+'calculatedcolumn.htm',
        aArgs,
        [ sFunc, Cons ],
        508, 450,
        dcb_addCalColumnUtils,
        "dcb_addCalColumnUtils"
      );
      /*
	    if (window.returnValue != null)
	    {
	    	if (sFunc==undefined){
				sFunc = 'CalcCol';
			}
			if (sFunc == 'CalcCell'){
				StgParams(getFT_Component(oComponentMenuSet.id),sFunc,Cons + "_AWSep_" + window.returnValue);
			}
			else{
	        	EASParams(sFunc, Cons + "_AWSep_" + window.returnValue);
			}
	        window.returnValue = null;
	    }
      */
    /*
    }
    else {
    	var nTopElems = ccOperatorsLocal.length;
    	var aCCOperators = new Array();
    	for (iElems=0; iElems<nTopElems; iElems++){
			if (ccOperatorsLocal[iElems] != null) {
    			aCCOperators.push(ccOperatorsLocal[iElems].join('_ELEMS_AWSep_'));
			}
    	}

    	gDialogArguments = ML[96]+'Æ'+aCCOperators.join('_COLS_AWSep_')+'Æ'+ccVariables.join('_ELEMS_AWSep_')+'Æ'+labels.join('_LAB_AWSep_');
    	gDialogArguments = gDialogArguments+'ÆnullÆ'+Name+'Æ'+Type+'Æ'+Formula+'Æ'+Format+'ÆfalseÆ'+lang+'Æ'+bCalcPiv+'Æ'+nPivots;
    	if (nPivots>0) {
    		nTopElems = aDimsPivsN.length;
	    	var aCCDimsPivsN = new Array();
	    	for (iElems=0; iElems<nTopElems; iElems++){
				if (aDimsPivsN[iElems] != null) {
	    			aCCDimsPivsN.push(aDimsPivsN[iElems].join('_ELEMS_AWSep_'));
				}
	    	}
    		gDialogArguments = gDialogArguments+'Æ'+aCCDimsPivsN.join('_COLS_AWSep_')
    	}
		gDialogArguments = gDialogArguments+'°'+Cons+'ÆCalcCol';
		showPopWin(sauxPath+'calculatedcolumn.htm', 508, 450, null);
    }
    */
}

function dcb_addCalColumnUtils(returnValue, aArgContinue)
{
  var sFunc = aArgContinue[0];
  var Cons  = aArgContinue[1];

  if (sFunc === undefined){
    sFunc = 'CalcCol';
  }
  if (sFunc === 'CalcCell'){
    StgParams(getFT_Component(oComponentMenuSet.id),sFunc,Cons + "_AWSep_" + returnValue);
  }
  else{
    EASParams(sFunc, Cons + "_AWSep_" + returnValue);
  }
}

function EditColorCol(sCons,nTypeCol,sStyle_Col,sCompId)	//	COLORS
{
	//	sStyle_Col='ROWT,BCKG,FC,FT,FS,IMG|CBG,CBS,CBT,CBA';
	//	ROW  (Tipo de renglón):		Los valores son	HEADER,DATA,GUIDELINES,SUBTOTAL,TOTAL.
	//  T    (Columna):				D - Dimensión, I - Indicador, S - Subtotal, T - Total,  - Vacio(aplica a toda la tabla).
	//	BCKG (Background color):	Color de fondo:	FFFFFF.
	//	FC 	 (Font Color):			Color de la fuente:	000000.
	//	FT 	 (Font Type):			Familia de la fuente:	arial.
	//	FS	 (Font Size):			Tamaño de la fuente:	11.
	// 	IMG	 (Background image):	Imagen de Fondo:	rep_sql35/bckg_images/gris_blanco_vertical.jpg

	//	CBG	 (Value of Checkbox Guidelines):	Valor: 1(checked).
	//	CBS	 (Value of Checkbox Subtotal):		Valor: 1(checked).
	//	CBT	 (Value of Checkbox Total):			Valor: 1(checked).
	//	CBA	 (Value of Checkbox Colors Alarm):	Valor: 1(checked).
	//	sStyle_Col = 'HEADER,FAFAFA,543210,courier,14,|DATA,AAAAAA,E0E0E0,courier,14,|1,1|TOTAL,,,,,rep_sql35/bckg_images/gris_blanco_vertical.jpg';
	if (bLoading==0){
		return;
	}
	sauxPath = GetPath();
	ndialogHeight = 360;
	if ((nTypeCol == '') || (nTypeCol == 5) || (nTypeCol == 0))
	{
		ndialogHeight = 300;
	}
	else if (nTypeCol != -1){
		ndialogHeight = 360;
	}
	else if (sCompId == 82) {
		ndialogHeight = 300;
	}

  openWindowDialog(
    sauxPath+'EditColorCol.php?style='+sStyle_Col+'&sTypeCol='+nTypeCol,
    [window,nTypeCol,sCompId],
    [sCons],
    450, ndialogHeight,
    SetEditColorColModalValues,
    "SetEditColorColModalValues"
  );
}

function SetEditColorColModalValues(sNewValues, aArgCont)
{
	if (sNewValues !== '' && sNewValues !== undefined)
	{
		var returnValue = sNewValues;
		var sCons = aArgCont[0];

    EASParams("EditColorCol", sCons + "_AWSep_" + returnValue);
	}
}

function ColPerDialog(sCons, sMD, aPeriodDescs, aPeriodKeys, aPeriodFormats, aPeriodCatVars)
{
	if (bLoading==0){
		return;
	}
	sauxPath = GetPath();
	if (HaveModalWindow()) {
	    showModalDialog
	        (sauxPath+'colper.htm',
	        	[ML[83],ML[69],ML[70],ML[71],ML[72],ML[84],ML[73],ML[85],ML[86],window,sMD,
	        	aPeriodDescs,aPeriodKeys,aPeriodFormats,ML[87],ML,sTBPeriod_Key,bNFTPeriod,sTBPeriod_Format,sTBPeriod_Name,'',
	        	aPeriodCatVars,aPeriodData],
	        'dialogWidth: 425px; dialogHeight: 400px; resizable: yes; center: yes; help: no; status: no; scroll:no');
	    if (window.returnValue != null)
	    {
	        EASParams( 'ColPer', sCons + '_AWSep_' + window.returnValue);
	        window.returnValue = null;
	    }
	}
	else {
		gDialogArguments = ML[83]+'Æ'+ML[69]+'Æ'+ML[70]+'Æ'+ML[71]+'Æ'+ML[72]+'Æ'+ML[84]+'Æ'+ML[73]+'Æ'+ML[85]+'Æ'+ML[86];
		gDialogArguments = gDialogArguments+'ÆnullÆ'+sMD+'Æ'+aPeriodDescs.join('_aPeriodDescs_AWSep_');
		gDialogArguments = gDialogArguments+'Æ'+aPeriodKeys.join('_aPeriodKeys_AWSep_')+'Æ'+aPeriodFormats.join('_aPeriodFormats_AWSep_');
		gDialogArguments = gDialogArguments+'Æ'+ML[87]+'Æ'+ML.join('_ML_AWSep_')+'Æ'+sTBPeriod_Key+'Æ'+bNFTPeriod+'Æ'+sTBPeriod_Format;
		gDialogArguments = gDialogArguments+'Æ'+sTBPeriod_Name+'°'+sCons;
	    showPopWin(sauxPath+'colper.htm', 425, 400, null);
	}
}
function BubbleUp(nColId){
	if (IsEAS) {
		var aAlarmK = window.parent.aAlarmKeys;
		var aAlarmD = window.parent.aAlarmDescs;
		var aAlarmB = window.parent.aAlarmBKGC;
		var aAlarmF = window.parent.aAlarmFC;
		var sMD = arrBUProp[nColId];
		var aIndAls = aIndAlarms[aColIndK[nColId]];
		var aDimInd = aDimsInd[aColIndK[nColId]];
	}
	else {
		var aAlarmK = window.parent.frames['treeFrame'].aAlarmKeys;
		var aAlarmD = window.parent.frames['treeFrame'].aAlarmDescs;
		var aAlarmB = window.parent.frames['treeFrame'].aAlarmBKGC;
		var aAlarmF = window.parent.frames['treeFrame'].aAlarmFC;
		var sMD = (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction]['C'+nColId] != undefined &&
				   aMetaTD[nEscAction]['C'+nColId][12] != undefined) ? aMetaTD[nEscAction]['C'+nColId][12] : '';
		var aIndAls = aIndAlarms[nEscAction][aColIndK[nEscAction][nColId]];
		var aDimInd = aDimsInd[nEscAction][aColIndK[nEscAction][nColId]];
		nCube         = (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction][44] != undefined) ? aMetaTD[nEscAction][44] : -1;
		sDims         = (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction][55] != undefined) ? aMetaTD[nEscAction][55] : '';
		bNFTPeriod    = (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction][48] != undefined) ? aMetaTD[nEscAction][48] : 0;
		sTBPeriod_Key = (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction][38] != undefined) ? aMetaTD[nEscAction][38] : -1;
	}
	var sauxPath = GetPath();
	var nBUType = 0;
	var nBUDim = -1;
	var bBUVT = true;
	var bSetBU = false;
	var aBubAlarms = new Array();
	if (sMD != ''){
		bSetBU = true;
		var aBubble = sMD.split('#tipo#');
		nBUType = parseInt(aBubble[1]);
		aBubble = aBubble[0].split('%#AS#%');
		var aBubDimTot = aBubble[0].split(',');
		nBUDim = parseInt(aBubDimTot[0]);
		bBUVT = (parseInt(aBubDimTot[1])==0)?true:false;
		aBubAlarms[0] = bBUVT;
		var iTot = aBubble.length;
		for (var i=1; i<iTot; i++){
			iAlarm = parseInt(aBubble[i].replace(/\,/g,''));
			aBubAlarms[iAlarm] = iAlarm;
		}
	}
	if (HaveModalWindow())
	{
		var aDimNamesTmp = new Array();
		var aDimKeysTmp  = new Array();
		var aDimIsPeriodTmp = new Array();

		copy(aDimNames[nCube], aDimNamesTmp);
		copy(aDimKeys[nCube] , aDimKeysTmp);
		copy(aDimIsPeriod[nCube], aDimIsPeriodTmp);

		window.showModalDialog(sauxPath+"bubbleup_prop.php",['Bubble Up',window,nCube,sDims,aDimNamesTmp,aDimKeysTmp,
				aAlarmK, aAlarmD, aAlarmB, aAlarmF, nBUDim, nBUType, aBubAlarms, bSetBU, bNFTPeriod, aIndAls,
				aDimIsPeriodTmp, aDimInd, sTBPeriod_Key],
				"dialogHeight:450px; dialogWidth:600px; center:yes; help:no; resizable:no; status:no");
		if(window.returnValue != null) {
			sMD = window.returnValue;
			EASParams('BubbleUp', nColId + '_AWS_' + sMD);
			window.returnValue = null;
		}
	}
	else {
		gDialogArguments = 'Bubble UpÆnullÆ'+nCube+'Æ'+sDims+'Æ'+aDimNames[nCube].join('_AWSep_')+'Æ'+aDimKeys[nCube].join('_AWSep_')+'Æ'+aAlarmK.join('_AWSep_');
		gDialogArguments = gDialogArguments+'Æ'+aAlarmD.join('_AWSep_')+'Æ'+aAlarmB.join('_AWSep_')+'Æ'+aAlarmF.join('_AWSep_')+'Æ'+nBUDim+'Æ'+nBUType;
		gDialogArguments = gDialogArguments+'Æ'+aBubAlarms.join('_AWSep_')+'Æ'+bSetBU+'Æ'+bNFTPeriod+'Æ'+aIndAlarms[aColIndK[nColId]].join('_AWSep_');
		gDialogArguments = gDialogArguments+'Æ'+aDimIsPeriod[nCube].join('_AWSep_')+'Æ'+aDimsInd[aColIndK[nColId]].join('_AWSep_');
		gDialogArguments = gDialogArguments+'Æ'+sTBPeriod_Key;
		gDialogArguments = gDialogArguments+'°'+'BubbleUp'+'Æ'+nColId;
       	showPopWin(sauxPath+"bubbleup_prop.php", 600, 450, null);
	}
}
function ColFilter(nColId) {
//debugger;
	var aRFKeys = new Array(); var aRFNams = new Array(); var aRFMetr = new Array();
	var sNoFilterDims = '';
	// Get Dimensions Keys
	if (IsEAS) {
		if (aColIndK[nColId] != undefined && aDimsInd[aColIndK[nColId]] != undefined) {
			aRFKeys = aDimsInd[aColIndK[nColId]].slice(0);
		}
		if (arrColNoFDims[nColId] != undefined) {
			sNoFilterDims = arrColNoFDims[nColId];
		}
	}
	else {
		var aCol = (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction]['C'+nColId] != undefined) ? aMetaTD[nEscAction]['C'+nColId] : null;

		if (aColIndK[nEscAction] != undefined && aColIndK[nEscAction][nColId] != undefined &&
		    aDimsInd[nEscAction] != undefined && aDimsInd[nEscAction][aColIndK[nEscAction][nColId]] != undefined)
		{
			aRFKeys = aDimsInd[nEscAction][aColIndK[nEscAction][nColId]].slice(0);
		}

		if (aCol != null && aCol[10] != undefined) {
			sNoFilterDims = aCol[10];
		}
		if (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction][44] != undefined) {
			nCube = aMetaTD[nEscAction][44];
		}
	}
	// Get Dimensions Names
	var nTop = aRFKeys.length;
	for (var i=0; i<nTop; i++) {
		aRFNams[i] = getDimensionName(aRFKeys[i], nCube);
	}
	if (aRFKeys == undefined || aRFNams == undefined){ return; }
	var sElemsSel = sNoFilterDims; //$Col->sNoFilterDims
	var sTitle = ML[654]+'->'+ML[276];
	var sauxPath = GetPath();
	window.showModalDialog(sauxPath+"rep_filter_sel_dims.php",
			[sTitle,window,aRFKeys,aRFNams,sElemsSel,aRFMetr,0,true],
			"dialogHeight:450px; dialogWidth:500px; center:yes; help:no; resizable:no; status:no");
	if (window.returnValue != null)
	{
		EASParams('ColFilter', nColId + '_AWS_' + window.returnValue);
		window.returnValue = null
	}
}
function ColsOrder(sTypeOrderParam){	//	Orden de Columnas
	if (bLoading==0){
		return;
	}
	var sTypeOrder = (sTypeOrderParam == undefined) ? 'ColsOrder' : sTypeOrderParam;
	var aParamArrColsOrderIdName = new Array();
	var sTitle = ML[398];
	if (sTypeOrder == 'ColsOrder')
	{
		if (IsEAS) {
			aParamArrColsOrderIdName = arrColsOrderIdName.slice(0);
		}
		else
		{
			var arrColsIds = new Array();
			if (aMetaTD[nEscAction][37] != undefined) {
				arrColsIds = aMetaTD[nEscAction][37];
			}
			var nTop = arrColsIds.length;
			for (var i=0; i<nTop; i++)
			{
				var nIdCol = arrColsIds[i];
				var sNameCol = '';
				if (aMetaTD[nEscAction]['C'+nIdCol] != undefined)
				{
					if (aMetaTD[nEscAction]['C'+nIdCol][2] != undefined) {
						sNameCol = aMetaTD[nEscAction]['C'+nIdCol][2];
					}
				}
				aParamArrColsOrderIdName.push(nIdCol+'_AWName_'+sNameCol);  //  "'".$sId."_AWName_".$sName."'";
			}
		}
	}
	else if (sTypeOrder == 'RowsOrder')
	{
		sTitle = ML[773];
		if (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction][34] != undefined)
		{
			var aRowsInds = aMetaTD[nEscAction][34].slice(0);
			var nTop = aRowsInds.length;
			for (var i=0; i<nTop; i++)
			{
				var nRowId = aRowsInds[i];
				var sRowName = (aMetaTD[nEscAction]['R'+nRowId] != undefined &&
								aMetaTD[nEscAction]['R'+nRowId][0] != undefined) ?
								aMetaTD[nEscAction]['R'+nRowId][0] : '';
				aParamArrColsOrderIdName.push(nRowId+'_AWName_'+sRowName);
			}
		}
	}
	else
	{
		return;
	}
	sauxPath = GetPath();
	if (HaveModalWindow()) {
		showModalDialog
	        (sauxPath+'order_cols.php',
	        [window, aParamArrColsOrderIdName, ML[407]+'->'+sTitle, sTypeOrder],
	        "dialogWidth: 450px; dialogHeight: 350px; resizable: yes; center: yes; help: no; status: no; scroll:no"
	        );
	    if (window.returnValue != null)
	    {
	        var sEASDropF = (sTypeOrder == 'RowsOrder') ? 'OrdRows' : 'OrdCols';
	    	EASParams(sEASDropF,window.returnValue);
	        window.returnValue = null;
	    }
	}
	else {
		gDialogArguments = 'nullÆ'+aParamArrColsOrderIdName.join('_AWSepElems_')+'Æ'+ML[407]+'->'+ML[398];
		gDialogArguments = gDialogArguments+'°';
		showPopWin('order_cols.php', 450, 350, null);
	}
}
function AdvOrder(oSectionMenu)		// Ordenamiento Avanzado
{
	if (bLoading==0){
		return;
	}

	var sParamsColsAdvOrd = ''; var sParamsAdvancedOrder = '';
	if (IsEAS)
	{
		if (IsReport)
		{
			sParamsColsAdvOrd = oSectionMenu.getColsAdvOrd();
			sParamsAdvancedOrder = oSectionMenu.divSection.sAdvancedOrder;
		}
		else
		{
			sParamsColsAdvOrd = sColsAdvOrd;
			sParamsAdvancedOrder = sAdvancedOrder;
		}
	}
	else
	{
		var nPivotsL = 0; var arrColsIds = new Array();
		if (aMetaTD[nEscAction][43] != undefined) {
			nPivotsL = aMetaTD[nEscAction][43];
		}
		if (aMetaTD[nEscAction][37] != undefined) {
			arrColsIds = aMetaTD[nEscAction][37].slice(0);
		}
		var nTop = arrColsIds.length; var bAdd = true; var aColsAdvOrder = new Array();
		for (var i=0; i<nTop; i++)
		{
			bAdd = false;
			var nIdCol = arrColsIds[i];
			if (nPivotsL == 0) {
				bAdd = true;
			}
			else
			{
				var bIsVisible = false;
				if (aMetaTD[nEscAction]['C'+nIdCol] != undefined && aMetaTD[nEscAction]['C'+nIdCol][4] != undefined)
				{
					bIsVisible = aMetaTD[nEscAction]['C'+nIdCol][4];
				}
				bAdd = bIsVisible;
			}
			if (bAdd) {
				var nType = 0; var sNameCol = '';
				if (aMetaTD[nEscAction]['C'+nIdCol] != undefined)
				{
					if (aMetaTD[nEscAction]['C'+nIdCol][3] != undefined) {
						nType = aMetaTD[nEscAction]['C'+nIdCol][3];
					}
					if (aMetaTD[nEscAction]['C'+nIdCol][2] != undefined) {
						sNameCol = aMetaTD[nEscAction]['C'+nIdCol][2];
					}
				}
				aColsAdvOrder.push(nIdCol+'_AWS_'+nType+'_AWS_'+sNameCol);  //  $sId.'_AWS_'.$Col->nType.'_AWS_'.$sName;
			}
		}
		sParamsColsAdvOrd = aColsAdvOrder.join('_AWSC_');

		if (aMetaTD[nEscAction][42] != undefined) {
			sParamsAdvancedOrder = aMetaTD[nEscAction][42];
		}
		/* AHORA SE LEE LA INFO DE aMetaTD
		if (aColsAdvOrd[nEscAction] != undefined) {
			sParamsColsAdvOrd = aColsAdvOrd[nEscAction];
		}
		if (aAdvancedOrder[nEscAction] != undefined) {
			sParamsAdvancedOrder = aAdvancedOrder[nEscAction];
		}
		*/
	}
	sauxPath = GetPath();
	if (HaveModalWindow())
	{
		showModalDialog
	        (sauxPath+'adv_ord_cols.php',
	        [window, ML[395], sParamsColsAdvOrd, sParamsAdvancedOrder],
	        "dialogWidth:650px; dialogHeight:360px; resizable:yes; center:yes; help:no; status:no; scroll:no"
	        );
	    if (window.returnValue != null)
	    {
	        if (IsEAS)
	        {
				if (IsReport)
				{
					SetAdvOrder(window.returnValue);
				}
				else
				{
					EASParams('AdvOrder',window.returnValue);
				}
	    	}
	    	else
	    	{
	    		EASParams('AdvOrder',window.returnValue);
	    	}
	        window.returnValue = null;
	    }
	}
	else {
		gDialogArguments = 'nullÆ'+ML[395]+'Æ'+sParamsColsAdvOrd+'Æ'+sParamsAdvancedOrder;
		gDialogArguments = gDialogArguments+'°';
		showPopWin('adv_ord_cols.php', 650, 280, null);
	}
}
function OrdColDPiv(){	// Orden Dimensiones Pivote
	if (bLoading==0){
		return;
	}
	var sParamsPivots = new Array(); var aParamsNomPivots = new Array();
	var sParamsPivOrderDescs = '';   var aParamsDimsPivsN = new Array();
	var aParamsDimsPivsK = new Array(); var sSourceData = '';
	if (IsEAS) {
		sParamsPivots = sPivots;
		aParamsNomPivots = nomPivots;
		sParamsPivOrderDescs = sPivOrderDescs;
		aParamsDimsPivsN = aDimsPivsN;
		aParamsDimsPivsK = aDimsPivsK;
		sSourceData = 'EAS';
		if (IsReport) {
			sSourceData = 'Report';
		}
	}
	else {
		if (arrsPivots[nEscAction] != undefined) {
			sParamsPivots = arrsPivots[nEscAction].slice(0);
		}
		if (arrsNomPivots[nEscAction] != undefined) {
			aParamsNomPivots = arrsNomPivots[nEscAction].slice(0);
		}
		if (arrPivOrderDescs[nEscAction] != undefined) {
			sParamsPivOrderDescs = arrPivOrderDescs[nEscAction];
		}
		if (arrDimsPivsN[nEscAction] != undefined) {
			aParamsDimsPivsN = arrDimsPivsN[nEscAction].slice(0);
		}
		if (arrDimsPivsK[nEscAction] != undefined) {
			aParamsDimsPivsK = arrDimsPivsK[nEscAction].slice(0);
		}
		sSourceData = 'Dashboard';
	}
	sauxPath = GetPath();
	if (HaveModalWindow()) {
		showModalDialog
	        (sauxPath+'order_cols_dims_piv.php',
	        [window, sParamsPivots, aParamsNomPivots, sParamsPivOrderDescs, aParamsDimsPivsN,
	        	ML[408], ML[407]+'->'+ML[408], aParamsDimsPivsK, sSourceData],
	        "dialogWidth: 550px; dialogHeight: 350px; resizable: yes; center: yes; help: no; status: no; scroll:no"
	        );
	    if (window.returnValue != null)
	    {
	    	var sParamOrder = window.returnValue;
			if (sParamOrder == '_#DEL#_')
			{
				sParamOrder = '';
			}
	    	if (IsEAS) {
				if (IsReport)
				{
					SetOrdPivDim(sParamOrder);
				}
				else {
					EASParams('OrdPivDims',sParamOrder);
				}
	    	}
	    	else {
	        	EASParams('OrdPivDims',sParamOrder);
	    	}
	    	window.returnValue = null;
	    }
	}
	else {
		var nTopElems = aParamsDimsPivsN.length;
    	var aAuxDimsPivsN = new Array();
    	for (iElems=0; iElems<nTopElems; iElems++){
			if (aParamsDimsPivsN[iElems] != null) {
    			aAuxDimsPivsN.push(aParamsDimsPivsN[iElems].join('_ELEMS_AWSep_'));
			}
    	}
		gDialogArguments = 'nullÆ'+sParamsPivots.join('_AWSOrdColDPiv_')+'Æ'+aParamsNomPivots.join('_AWSOrdColDPiv_');
		gDialogArguments = gDialogArguments+'Æ'+sParamsPivOrderDescs+'Æ'+aAuxDimsPivsN.join('_AWSOrdColDPiv_')+'Æ'+ML[408]+'Æ'+ML[407]+'->'+ML[408];
		gDialogArguments = gDialogArguments+'°';
		showPopWin('order_cols_dims_piv.php', 550, 350, null);
	}
}
function OrdColPiv(){	//	Orden de Periodos Pivote
	if (bLoading==0){
		return;
	}
	var arrParamsColPiv = '';
	if (IsEAS) {
		arrParamsColPiv = arrColPiv;
	}
	else
	{
		arrParamsColPiv = GetStrColPiv();
		/* AHORA SE LEE LA INFO DE aMetaTD
		if (arrColPiv[nEscAction] != undefined) {
			arrParamsColPiv = arrColPiv[nEscAction];
		}
		*/
	}
	sauxPath = GetPath();
	if (HaveModalWindow()) {
		showModalDialog
	        (sauxPath+'order_cols_piv.php',
	        [window, arrParamsColPiv, ML[376]],
	        "dialogWidth: 300px; dialogHeight: 200px; resizable: yes; center: yes; help: no; status: no; scroll:no"
	        );
	    if (window.returnValue != null)
	    {
	    	//-1_AWSep_Periodos
	        EASParams('OrdPivPer','-1_AWSep_'+window.returnValue);
	        window.returnValue = null;
	    }
	}
	else {
		gDialogArguments = 'nullÆ'+arrParamsColPiv+'Æ'+ML[376];
		gDialogArguments = gDialogArguments+'°';
		showPopWin('order_cols_piv.php', 300, 200, null);
	}
}
function Ranges(){
	if (bLoading==0){
		return;
	}
	var sRangeVal = '';
	var arrColsIdsIndParam = new Array();
	var arrColsNamIndParam = new Array();
	if (IsEAS) {
		sRangeVal = sRanges;
		arrColsIdsIndParam = arrColsIdsInd;
		arrColsNamIndParam = arrColsNamInd;
	}
	else {
		if (aMetaTD[nEscAction][32] != undefined) {
			sRangeVal = aMetaTD[nEscAction][32];
		}
		var arrColsIds = new Array();
		if (aMetaTD[nEscAction][37] != undefined) {
			arrColsIds = aMetaTD[nEscAction][37].slice(0);
		}
		var nTop = arrColsIds.length;
		for (var i=0; i<nTop; i++)
		{
			var nIdCol = arrColsIds[i];
			var bIsDim = IsDimensionDesc(nEscAction, nIdCol, true);
			if (!bIsDim)
			{
				var sNameColInd = '';
				if (aMetaTD[nEscAction]['C'+nIdCol] != undefined && aMetaTD[nEscAction]['C'+nIdCol][2] != undefined)
				{
					sNameColInd = aMetaTD[nEscAction]['C'+nIdCol][2];
				}
				arrColsNamIndParam[arrColsIds[i]] = sNameColInd;
				arrColsIdsIndParam.push(nIdCol);
			}
		}
		/*	AHORA SE LEE LA INFO DE aMetaTD
		if (arrColsIdsInd[nEscAction] != undefined) {
			arrColsIdsIndParam = arrColsIdsInd[nEscAction];
		}
		if (arrColsNamInd[nEscAction] != undefined) {
			arrColsNamIndParam = arrColsNamInd[nEscAction];
		}
		*/
	}
 	sauxPath = GetPath();
	if (HaveModalWindow()) {
		window.showModalDialog(sauxPath+"ranges.htm",[ML[508],window,ML,arrColsIdsIndParam,arrColsNamIndParam,sRangeVal],
				"dialogHeight:400px; dialogWidth:400px; center:yes; help:no; resizable:no; status:no");
		if(window.returnValue != null) {
			EASParams('Ranges', window.returnValue);
			window.returnValue = null;
		}
	}
	else {
		var nTopElems = arrColsIdsInd.length;
    	var arrAuxColsNamInd = new Array();
    	for (iElems=0; iElems<nTopElems; iElems++){
			if (arrColsIdsInd[iElems] != null) {
    			arrAuxColsNamInd.push(arrColsIdsInd[iElems]+'_NameInd_AWSep_'+arrColsNamInd[arrColsIdsInd[iElems]]);
			}
    	}
		gDialogArguments = ML[508]+'ÆnullÆ'+ML.join('_ML_AWSep_')+'Æ'+arrColsIdsInd.join('_arrColsIdsInd_AWSep_');
		gDialogArguments = gDialogArguments+'Æ'+arrAuxColsNamInd.join('_aColsNameInd_AWSep_')+'Æ'+sRangeVal;
		gDialogArguments = gDialogArguments+'°';
		showPopWin(sauxPath+'ranges.htm', 400, 350, null);
	}
}
function SetColMinWidth() {
	labels=ML[592]+' (px)';
    types ="INP";
    valid ="num";
    var sSpacePx = 0;

    if (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction]['C'+nColAction] != undefined) {
		sSpacePx = aMetaTD[nEscAction]['C'+nColAction][1];
    }

    if (isNaN(sSpacePx)) {
    	sSpacePx = 0;
    }
    values = ''+sSpacePx+'';

    openWindowDialog(jsDir+'ask_data.php', [ML[276]+'->'+ML[850], window, labels, types, values, valid], [nColAction],
					 		 400, 150, SetColMinWidthModalValues);
}
function SetColMinWidthModalValues(sNewValues, aArgCont)
{
	if (sNewValues != undefined)
	{
		var nparamColAction = aArgCont[0];

		var aDatas = sNewValues.split("\n");
        aDatas[0] = parseInt(aDatas[0]);
        if (isNaN(aDatas[0])) {
        	aDatas[0] = 0;
        }
        if (aDatas[0] < 0) {
        	aDatas[0] = 0;
        }

        EASParams('SetColMinWidth', nparamColAction+','+aDatas[0]);
	}
}
function SetConfigAdvance(sParam)
{
	var sourceConfig = "SoftBar@Hor_1_0_0_11_FFFFFF_FFFFFF";
	var paramsConfig = ",,,_#AWProps#_0,0,16,14,#24822e,#ffffff,,,,,SoftBar@Hor,,1,1,000000,0,1,1";

	if (sParam == 'CONFIGADVANCENEG')
	{
		paramsConfig = ",,,_#AWProps#_0,0,16,14,#ff0000,#ffffff,,,,,SoftBar@Hor,,1,1,000000,0,1,1";
	}

	if (IsEAS)
	{
		if (sParam == 'CONFIGADVANCEPOS')
		{
			if (arrColMDAdvan[nColAction] != undefined)
			{
				paramsConfig = arrColMDAdvan[nColAction];
			}
		}
		else if (sParam == 'CONFIGADVANCENEG')
		{
			if (arrColMDAdvanNeg[nColAction] != undefined)
			{
				paramsConfig = arrColMDAdvanNeg[nColAction];
			}
		}

	}
	else
	{
		if (sParam == 'CONFIGADVANCEPOS')
		{
		    if (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction]['C'+nColAction] != undefined && aMetaTD[nEscAction]['C'+nColAction][22] != undefined)
		    {
				paramsConfig = aMetaTD[nEscAction]['C'+nColAction][22];
		    }
		}
		else if (sParam == 'CONFIGADVANCENEG')
		{
			if (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction]['C'+nColAction] != undefined && aMetaTD[nEscAction]['C'+nColAction][23] != undefined)
		    {
				paramsConfig = aMetaTD[nEscAction]['C'+nColAction][23];
		    }
		}
	}

	if (paramsConfig == '')
	{
		paramsConfig = ",,,_#AWProps#_0,0,16,14,#24822e,#ffffff,,,,,SoftBar@Hor,,1,1,000000,0,1,1";
	}

	window.showModalDialog(jsDir+'element_prop.php?elemStyle='+sourceConfig, [ML[473]+' ('+ML[903]+')', window, paramsConfig, 16],
						   "dialogHeight:350px; dialogWidth:450px; center:yes; help:no; resizable:no; status:no");
	if (window.returnValue != null)
	{
		var sValReturn = window.returnValue;
		EASParams(sParam, nColAction + '_AWSep_' + sValReturn);
		window.returnValue = null
	}
}

var bDebugLog = false;
function myLogD(msg){ try{ if(bDebugLog && typeof(console) == "object") console.log(msg);} catch(e){} }

function SetConfigTrend(nColAction)
{
	if(aMetaTD[nEscAction]==undefined || aMetaTD[nEscAction]['C'+nColAction]==undefined || aMetaTD[nEscAction]['C'+nColAction][26]==undefined)
	{
		myLogD('Error getting MetaDataInfo for ChartTrend');
		return;
	}

	ChartTableMD=aMetaTD[nEscAction]['C'+nColAction][26];
	if(ChartTableMD=='')
	{
		ChartTableMD="0,0,0,0,1,1|POS|line#line@line,0,0,30,0,calibri-.ttf|9|0|16777215|0_F_none|0,calibri-.ttf|9|0|16777215|0_F_none|0,0,1,1,0,none,1,1,0,0,0,1,00FF00,0@0@0@0,0,0,-1,1,0,0,100,0,1,0,0,,,0,0|EFFECTS||Exp||Depth||Icons||Texture|layCIR!Text!5,35,0,0,0,0|Layout||Fmt|RoundedEdge|Style||Rot|0|StartAngle||PIE|arial-.ttf|8|0|-1|0_F_|TSup|arial-.ttf|8|0|-1|0_F_ |TInf|arial-.ttf|8|0|-1|0_F_ |TIzq|arial-.ttf|8|0|-1|0_F_|TDer||TIT||UserColors|-1,-1|ChartColors|8404992,2830038,7327737,12497049,8867367,14535349,5403620,11264092,6217679,1482490,13257415,13540262,10260442,7114237,11922418,10483707|SeriesColor||COLORS|TD1|IDX||STRIPES|calibri-.ttf|10|0|-1|0_F_1|14737632|0|0|1|0|0|1|0|0||1|1||0|outside|1||@calibri-.ttf|10|0|-1|0_F_1|0|0|0|1|0|0|1|0|0||1|0||0|outside|1||@calibri-.ttf|10|0|-1|0_F_1|14737632|0|1|1|0|0|1|0|1||1|1||0|outside|1|||AXIS|-1,1,-1,1,4,4,-1,0,0|GRID|,@.|REG|arial-.ttf|8|0|-1|0_F_,-1,-1;3;2;0;1,1,0,0,255@255,0,0.7,0,0|SER||INF||CHARTGUI|";
	}
	else
	{
		myLogD('MetaData for Chart in Col = '+ChartTableMD);
	}

	ChartTableID = nEscAction;
	TDId         = ChartTableID;
	ColChartTrend= nColAction;
	//myLogD(ChartTableMD);
	ChartMenu('-2');
}
//Coockies
function Get_Cookie(name){
   var start = document.cookie.indexOf(name+"=");
   var len = start+name.length+1;
   if ((!start) && (name != document.cookie.substring(0,name.length))) return null;
   if (start == -1) return null;
   var end = document.cookie.indexOf(";",len);
   if (end == -1) end = document.cookie.length;
   return unescape(document.cookie.substring(len,end));
}

function Set_Cookie(name,value,expires,path,domain,secure) {
    var cookieString = name + "=" +escape(value) +
       ( (expires) ? ";expires=" + expires.toGMTString() : "") +
       ( (path) ? ";path=" + path : "") +
       ( (domain) ? ";domain=" + domain : "") +
       ( (secure) ? ";secure" : "");
    document.cookie = cookieString;
}

function Delete_Cookie(name,path,domain) {
   if (Get_Cookie(name)) document.cookie = name + "=" +
      ( (path) ? ";path=" + path : "") +
      ( (domain) ? ";domain=" + domain : "") +
      ";expires=Sun, 01-Jan-70 00:00:01 GMT";
}

function setCookieSH_Titles(sName,value){
	var myDate=new Date();
	myDate.setFullYear(2040,0,1);
	Set_Cookie(sName,value,myDate,"","","");
}

function getCookieSH_Titles(sName){
	val = Get_Cookie(sName);
	if (val == null){
		Show_Hide_Dims_Name(0);
		return 1;
	}
	return val;
}

//Arma el String del MENU para los Enlazadores
function ShowTreeScsMenu_SubGrupos(key, event)
{
	if(typeof(aEscLigadosASubGrupos)=='undefined') return;
	var aposReplace				="''";

	var keyUser='', keyOld='', bMenuLaunch=false;
	var bShowAll=(key==32767);

	//Para distinguir entre diferentes GruposEnlazados publicados por diferentes usuarios
	key         	 =key+'';
	key         	 =key.split('*');
	keyGpoTargetOwner=parseInt(key.length==2?key[1]:-1);
	key         	 =parseInt(key[0]);

	var aScsKeys  = Array();
	var aScsNames = Array();
	for(var x=0; x<aEscGroupKeys.length; x++)
	{
		if(bShowAll && x>0)
		{
			key		=aEscGroupKeys[x];
			keyUser	=aEscGroupUserKeys[x];
		}
		if(key==32767) key=aEscGroupKeys[0];
		if(keyOld!=key || (keyOld==key && keyUser!=aEscGroupUserKeys[x]))
		{
			//Verificar si el Scs Actual es Publicado para el usuario actual
			var bThisStageIsPublicado=false;
			idx=$.inArray(nAuxStage, aEscKeys); //Buscar la Clave del ScsActual en la lista de Scs del user
			if(idx>-1) bThisStageIsPublicado = (aEscGroupUserKeys[idx]!=CurrentUserKey);	//Verificar si la Clave del dueño del GrupoScs es el UserActual
			if(key<0)
			{
				key*=-1; //Enlazando a un GpoScsPublicado en un Escenario del user Actual
				bThisStageIsPublicado=true;
				myLogD(aEscNames[idx]+' Es PUBLICADO del User Actual (Current User Key='+CurrentUserKey+') ejecutando un GpoPublicado ');
			}
			else
				if(bThisStageIsPublicado) myLogD(aEscNames[idx]+' Es PUBLICADO por lo cual el MenuEnlazador solo mostrara Grupos/SubGrupos/Scs que sean del user '+aEscGroupUserKeys[idx]+', (Current User Key='+CurrentUserKey+')');

			//Asignamos la Clave del User del cual ejecutaremos el GPO
			var iClaUserOwnerGpo=CurrentUserKey;
			if(bShowAll)
			{
				idx=x;
				iClaUserOwnerGpo=aEscGroupUserKeys[x];
			}
			else
			{
				//Obtener el idx del Gpo de Scs al que ejecuta el enlazador
				for(idx=0; idx<aEscGroupKeys.length-1; )
				{	//Obtener el indice del GpoScs a mostrar
					idx=$.inArray(key, aEscGroupKeys, idx);
					if(idx==-1)
					{
						myLogD('La clave del GpoScs '+key+' NO fue encontrada en el arreglo de aEscGroupKeys del usuario ' + CurrentUserKey);
						return(true);
					}
					else
					{
						if(bThisStageIsPublicado)
						{	//Si es Publicado el Scs donde se ejecuta el Enlazador, debemos encontrar el Key del Gpo Publicado,
							if(aEscGroupUserKeys[idx]==keyGpoTargetOwner || (keyGpoTargetOwner==-1 && aEscGroupUserKeys[idx]!=CurrentUserKey)) //Aqui como no tenemos la CLAVE del User q le publico tomaremos el PRIMER Gpo q sea publicado
							{
								iClaUserOwnerGpo=aEscGroupUserKeys[idx];
								break;
							}
							else
								idx++;	//Si es la clave del UserActual <coincidencia que tenga un Gpo con la misma clave que se le publico por otro usuario> buscamos el SIG
						}
						else
							break;  //Se encontro la clave del GpoScs del User Actual
					}
				}
				//Si se salio del Array, no se encontro
				if(idx>aEscGroupUserKeys.length-1)
				{
					myLogD('La clave del GpoScs '+key+' NO fue encontrada en el arreglo de '+aEscGroupUserKeys+' del usuario ' + CurrentUserKey);
					return(true);
				}
			}

			//Obtener todos los Scs del GpoScs valido
			myLogD('   Getting Scs for GpoScs:'+key+' of user:'+iClaUserOwnerGpo+' for currentUser:'+CurrentUserKey);
			var aScsOfGpo = Array();
			for(var i=idx; i<aEscGroupKeys.length; i++)
			{	//Si el Scs es del Gpo que estamos ejecutando el Menu
				if(aEscGroupKeys[i]==key)
					aScsOfGpo.push(aEscKeys[i]);	//Agregar el Scs del Gpo
				else
					break; //Pasamos a otro Grupo
			}
			myLogD('     Scs:'+aScsOfGpo.toString());
			//Obtener todos los subGpos del GpoScs valido
			myLogD('   Getting SubGpos for GpoScs:'+key+' of user:'+iClaUserOwnerGpo+' for currentUser:'+CurrentUserKey);
			var aSubGposKey = Array();
			for(var i=0, bAdd=true; i<aEscGroupKeys_SubGroupKeys.length; i++)
			{
				if(aEscGroupKeys_SubGroupKeys[i][0]==key)
				{
					aSubGposKey=aEscGroupKeys_SubGroupKeys[i].slice(1);	//Obtener los Keys de los SubGpos del Gpo
					for(var i2=0; bShowAll && i2<aEscSubGroup_Esc.length; i2++)		//Buscar el Key del SubGpo en la Rel donde estan los Scs ligados
						if(aEscSubGroup_Esc[i2][0]==aSubGposKey[0])					//Encontramos el SubGpo
						{
							i2=$.inArray(aEscSubGroup_Esc[i2][1], aEscKeys); //Encontrar el indice del primer Scs del SubGpo
							if(aEscGroupUserKeys[i2]!=iClaUserOwnerGpo)   //Si la Clave del Owner del Gpo del Scs ligado al SubGpo es diferente a la clave del Owner del Scs NO asignar
							{
								aSubGposKey = Array();
								bAdd = false;
							}
							break;		//Es SubGrupo Valido
						}

					if(bAdd)
					{
						myLogD('     SubGpos:'+aSubGposKey.toString());
						bMenuLaunch=true; //Indicar que si Existe Elementos a mostrar para el Menu
					}
					break;
				}
			}
			//Obtener los Nombres de los SubGpos en el mismo Orden que las Keys
			var aSubGpoDesc=Array();
			for(var i=0, sName=''; i<aSubGposKey.length; i++)
			{
				sName=aEscSubGroupNames[$.inArray(aSubGposKey[i], aEscSubGroupKeys)].replace(/\"/g, aposReplace);
				aSubGpoDesc.push(sName);
			}
			//Obtener la lista de Scs de cada SubGrupo para el Grupo Actual
			var aSubGpos_Scs = Array();
			for(var i=0, k=0; i<aSubGposKey.length; i++)
			{
				for(var j=0; j<aEscSubGroup_Esc.length; j++)
					if(aEscSubGroup_Esc[j][0]==aSubGposKey[i])
					{
						aSubGpos_Scs.push(aEscSubGroup_Esc[j].slice(1));
						myLogD('        SubGpo:'+aSubGposKey[i]+' Match:'+aEscSubGroup_Esc[j]+' Linked Scs:'+aSubGpos_Scs[k++].toString());
						break;
					}
			}
			//Obtener la lista de Scs del Gpo Actual NO Ligados a ningun subGrupo para mostrarse al Final de ellos
			var aScsOfGpoSinLigar = Array();
			for(var i=0; i<aScsOfGpo.length; i++)
			{	//Si no esta en la lista de ScsLigados
				if($.inArray(aScsOfGpo[i], aEscLigadosASubGrupos)==-1) //aEscLigadosASubGrupos.indexOf(aScsOfGpo[i])==-1)
					aScsOfGpoSinLigar.push(aScsOfGpo[i]); //Agregar ScsSinLigar a SubGrupo, para desplegarse al final del menu
			}
			myLogD('     Scs SIN Ligar:'+aScsOfGpoSinLigar.toString());

			//Armar cadena de Menu para este GPO  aScsOfGpo,aSubGposKey,aSubGpoDesc,aSubGpos_Scs,aScsOfGpoSinLigar
			var i2=0;
			for(var i=0, sSubMenuKey='', sSubMenuDes=''; i<aSubGpos_Scs.length; i++)
			{
				sSubMenuKey+=(i>0?', ':'')+"[";
				sSubMenuDes+=(i>0?', ':'')+"[\""+aSubGpoDesc[i]+"\", ";
				for(i2=0; i2<aSubGpos_Scs[i].length; i2++)
				{
					sSubMenuKey+="'"+aSubGpos_Scs[i][i2]+"'"+((i2+1)<aSubGpos_Scs[i].length?',':'');
					sSubMenuDes+="\""+aEscNames[$.inArray(aSubGpos_Scs[i][i2], aEscKeys)].replace(/\"/g, aposReplace)+"\""+((i2+1)<aSubGpos_Scs[i].length?',':'');
				}
				sSubMenuKey+="]";
				sSubMenuDes+="]";
				for(i2=0, sMenuKeySinLigar='', sMenuDesSinLigar=''; i2<aScsOfGpoSinLigar.length; i2++)
				{
					sMenuKeySinLigar+=", '" +aScsOfGpoSinLigar[i2]+"'";
					sMenuDesSinLigar+=", \""+aEscNames[$.inArray(aScsOfGpoSinLigar[i2], aEscKeys)].replace(/\"/g, aposReplace)+"\"";
				}
			}
			//Para cuando No existan SubGrupos, que ponga los Scs sin Ligar
			if(i2==0)
			{
				for(sMenuKeySinLigar='', sMenuDesSinLigar=''; i2<aScsOfGpoSinLigar.length; i2++)
				{
					sMenuKeySinLigar+=(i2>0?", '": "'") +aScsOfGpoSinLigar[i2]+"'";
					sMenuDesSinLigar+=(i2>0?", \"": "\"")+aEscNames[$.inArray(aScsOfGpoSinLigar[i2], aEscKeys)].replace(/\"/g, aposReplace)+"\"";
				}
			}
			ScsKeys  = "["                                                          + sSubMenuKey + sMenuKeySinLigar + " ]";
			ScsNames = "[\""+aEscGroupNames[idx].replace(/\"/g, aposReplace)+"\", " + sSubMenuDes + sMenuDesSinLigar + " ]";
			aScsKeys.push(ScsKeys);
			aScsNames.push(ScsNames);

			if(!bShowAll) break;
			keyOld =key;
			keyUser=aEscGroupUserKeys[x];
		}
	}
    ScsKeys = "[   " + aScsKeys.toString()  + " ]";
	ScsNames= "[0, " + aScsNames.toString() + " ]";
	myLogD('     ScsKeys:'+ScsKeys);
	myLogD('     ScsNames:'+ScsNames);

	ScsKeys=eval(ScsKeys);
	ScsNames=eval(ScsNames);
	if(document.all)
	   popUpQuickMenu(event, MoveToStage, ScsKeys, ScsNames);
	else
	{
	   var e = (window.event) ? window.event : event;
	   popUpQuickMenu(e, MoveToStage, ScsKeys, ScsNames);
	}

	if(!bMenuLaunch) myLogD('   GpoScs:'+key+' NO tiene SubGrupos definidos using Classic Function');
	return(bMenuLaunch);
}

//Arma el String del MENU para los Enlazadores
function ShowTreeScsMenu(key, event)
{
	//Ejecutar para SubGrupos o la funcion Clasica
	if(typeof(aEscGroupKeys_SubGroupKeys)!='undefined' && aEscGroupKeys_SubGroupKeys.length>0)
	{
		if(ShowTreeScsMenu_SubGrupos(key, event)) return;	//Si regresa TRUE es que el GPOSCS tiene SubGrupos y se levanto el Menu
	}

	//Viene la ClaveGpoScs * ClaveOwnerGpo, tomar solo la Clave del GpoScs
	key=key+'';
	key=key.split('*');
	key=parseInt(key[0]);

	var aposReplace="''";
    keyGpoScs=''; keys=''; scs=''; gpoName2=''; gpoName=''; ScsNames=''; ScsKeys=''; GpoKeys=''; gpoNameX = '';
    for(var bGroupsAdded=0, i=0, j=(aEscKeys.length); i<j; i++)
    {
       //Empezamos un nuevo grupo diferente de escenarios
       if(keyGpoScs!=aEscGroupKeys[i] || (gpoName2!=aEscGroupNames[i] && aEscGroupNames[i]!=''))
       {
          if (aEscGroupNames[i] == '' & keyGpoScs!=aEscGroupKeys[i])
          {
          	gpoName =gpoNameX;
          }
          else
          {
          	gpoName = aEscGroupNames[i].replace(/\"/g, aposReplace);
          }
          if(gpoName.substr(0,1)=='*') gpoName=' '+gpoName;
          gpoName2=gpoName;
          gpoNameX=gpoName;
          keyGpoScs    =aEscGroupKeys[i];
       }

       //Buscar el Gpo Especifico o Todos
       if((key==aEscGroupKeys[i] || key==32767) || ((key*-1)==aEscGroupKeys[i] && gpoName.substr(0,4)=='(P) '))
       {
       	  sEscName=aEscNames[i];
       	  if(sEscName.substr(0,1)=='*') sEscName=' '+sEscName;
          keys = keys + ", \"" + aEscKeys[i] + "\"";
          scs  = scs  + ", \"" + sEscName.replace(/\"/g, aposReplace) + "\"";
       }
       //Cerrar Gpo???
       if((i+1) >= j)
          bOk=true;                         //Terminamos el array??
       else
          bOk=(keyGpoScs!=aEscGroupKeys[i+1] || (gpoName2!=aEscGroupNames[i+1] && aEscGroupNames[i+1]!=''));   //Empezamos nuevo gpo??

       if(bOk && ((key==aEscGroupKeys[i] || key==32767) || ((key*-1)==aEscGroupKeys[i] && gpoName.substr(0,4)=='(P) ')) )
       {
       	  //Agregarle al Gpo Actual Escenarios publicados
		  for(y=0, keyGpoScs='', z=(aUserGroups.length); y<z; y++)
		     for(i2=0, x=aUserGroups[y], j2=(aEscKeysGP[x].length); i2<j2; i2++)
			 {
			 	if(aEscGroupNamesGP[x][i2]!='') gpoName2=aEscGroupNamesGP[x][i2].replace(/\"/g, aposReplace); //Pq viene el nombre del gpo de escenario y si es el mismo gpo de escenario se pone vacio
			    if(aEscGroupKeys[i]==aEscGroupKeysGP[x][i2] && (gpoName==gpoName2))
			       if(keys.search("\""+aEscKeysGP[x][i2]+"\"")==-1)
			       {
					  sEscName=aEscNamesGP[x][i2];
       	  			  if(sEscName.substr(0,1)=='*') sEscName=' '+sEscName;
		              keys = keys + ", \"" + aEscKeysGP[x][i2] + "\"";
		              scs  = scs  + ", \"" + sEscName.replace(/\"/g, aposReplace) + "\"";
			       }
			 }

          ScsNames=ScsNames + ", [\"" + gpoName + "\", " + scs.substr(scs.search(', ')+1) + "]";
          ScsKeys =ScsKeys  + ", ["  + keys.substr(keys.search(", ")+1) + "]";
          GpoKeys =GpoKeys  + ", ["  + aEscGroupKeys[i] + "]";
          keys=''; scs=''; gpoName='';
          bGroupsAdded++;
       }
       keyGpoScs=aEscGroupKeys[i];
       gpoName2=aEscGroupNames[i];
    }

    //Publicados x Gpo
    for(y=0, keyGpoScs='', z=(aUserGroups.length); y<z; y++)
    {
       for(i2=0, x=aUserGroups[y], j2=(aEscKeysGP[x].length); i2<j2; i2++)
       {
          //Empezamos un nuevo grupo diferente de escenarios
          if(keyGpoScs!=aEscGroupKeysGP[x][i2])
          {
	         sEscName=aEscGroupNamesGP[x][i2];
  			 if(sEscName.substr(0,1)=='*') sEscName=' '+sEscName;
             keyGpoScs   =aEscGroupKeysGP[x][i2];
             gpoName=sEscName.replace(/\"/g, aposReplace);
          }

          if(ScsKeys.search('"'+aEscKeysGP[x][i2]+'"')>0)
          	 continue; //Si ya existe en la lista no agregarlo

          //Buscar el Gpo Especifico o Todos
          if( (keys.search('"'+aEscKeysGP[x][i2]+'"')<0) &&
              ((key==aEscGroupKeysGP[x][i2] || key==32767) ||
              ((key*-1)==aEscGroupKeysGP[x][i2] && gpoName.substr(0,4)=='(P) ')) )
          {
	         sEscName=aEscNamesGP[x][i2];
  			 if(sEscName.substr(0,1)=='*') sEscName=' '+sEscName;

             keys = keys + ", \"" + aEscKeysGP[x][i2] + "\"";
             scs  = scs  + ", \"" + sEscName.replace(/\"/g, aposReplace) + "\"";
          }

          //Cerrar Gpo???
          if((i2+1) >= j2)
             bOk=true;                         //Terminamos el array??
          else
          {
             bOk=(keyGpoScs!=aEscGroupKeysGP[x][i2+1]);   //Empezamos nuevo gpo??
             if(bOk)
             {
             	for(y2=y+1; y2<z; y2++)
             	{
             		for(i3=0, x2=aUserGroups[y2], j3=(aEscKeysGP[x2].length); i3<j3; i3++)  //aEscNamesGP
             		{
			          if(ScsKeys.search('"'+aEscKeysGP[x2][i3]+'"')>0)
			          	 continue; //Si ya existe en la lista no agregarlo

			          if(keys.search("\""+aEscKeysGP[x2][i3]+"\"")>0)
			          	 continue; //Si ya existe en la lista no agregarlo

			          //Buscar el Gpo Especifico o Todos
			          if(key==aEscGroupKeysGP[x2][i3]) //if(keyGpoScs==aEscGroupKeysGP[x2][i3])  22/Oct/09
			          {
				         sEscName=aEscNamesGP[x2][i3];
			  			 if(sEscName.substr(0,1)=='*') sEscName=' '+sEscName;

			             keys = keys + ", \"" + aEscKeysGP[x2][i3] + "\"";
			             scs  = scs  + ", \"" + sEscName.replace(/\"/g, aposReplace) + "\"";
			          }
             		}
             	}
          	}
          }

          if(bGroupsAdded>0 && key!=32767) bOk=false; //Solo cuando son 32767 Todos los Esc, se permite mas de 1 Gpo
          if(bOk && ((key==aEscGroupKeysGP[x][i2] || key==32767) || ((key*-1)==aEscGroupKeysGP[x][i2] && gpoName.substr(0,4)=='(P) ')))
          {
             ScsNames=ScsNames + ", [\"" + gpoName + "\", " + scs.substr(scs.search(', ')+1) + "]";
             ScsKeys =ScsKeys  + ", ["  + keys.substr(keys.search(", ")+1) + "]";
             keys=''; scs=''; gpoName='';
             bGroupsAdded++;
          }
          keyGpoScs=aEscGroupKeysGP[x][i2];
       }
    }

    ScsNames = ScsNames.replace(/\\/g, "\\\\");
    if(ScsKeys != "" && ScsNames != "")
    {
        ScsKeys  = "[ " + ScsKeys.substr(ScsKeys.search(', ')+1) + " ]";
        ScsNames = "[0, " + ScsNames.substr(ScsNames.search(', ')+1) + " ]";

        ScsKeys=eval(ScsKeys);
        ScsNames=eval(ScsNames);

        if(document.all)
           popUpQuickMenu(event, MoveToStage, ScsKeys, ScsNames);
        else
        {
           var e = (window.event) ? window.event : event;
           popUpQuickMenu(e, MoveToStage, ScsKeys, ScsNames);
        }
    }
}

function swapArray(array, a, b)
{
 	var tmp  =array[a];
 	array[a]=array[b];
 	array[b]=tmp;
}
function partition(array, begin, end, pivot)
{
 	var piv=array[pivot];
 	swapArray(array, pivot, end-1);
 	var store=begin;
 	var ix;
 	for(ix=begin; ix<end-1; ++ix) {
 		if(array[ix]<=piv) {
 			swapArray(array, store, ix);
 			++store;
 		}
 	}
 	swapArray(array, end-1, store)
 	return store;
}
function qsort(array, begin, end)
{
 	if(end-1>begin) {
 		var pivot=begin+Math.floor(Math.random()*(end-begin));

 		pivot=partition(array, begin, end, pivot);
 		qsort(array, begin, pivot);
 		qsort(array, pivot+1, end);
   }
}
function quick_sort(array, iniStart, endStart)
{
	qsort(array, iniStart, endStart);
}

function MoveToStage(sKey)
{
   Stage('BITAM_STAGE_'+sKey+'.php');
}

function onMouseOverOut(bOver, imageName)
{
   var iMax=document.images[imageName].length;
   if(iMax>0)
   {
	   for(var i=0; i<iMax; i++)
	   {
		  document.images[imageName][i].style.cursor=(bOver?"pointer":"default");
		  //document.getElementById(imageName).style.cursor=(bOver?"pointer":"pointer");
	   }
   }
   else
   	   document.images[imageName].style.cursor=(bOver?"pointer":"default");
}

function RGBToHexConvert(str) {
	if (str.indexOf('rgb(')==-1) {
		return str;
	}
	str = str.replace(/rgb\(|\)/g, "").split(",");
	str[0] = parseInt(str[0], 10).toString(16).toLowerCase();
	str[1] = parseInt(str[1], 10).toString(16).toLowerCase();
	str[2] = parseInt(str[2], 10).toString(16).toLowerCase();
	str[0] = (str[0].length == 1) ? '0' + str[0] : str[0];
	str[1] = (str[1].length == 1) ? '0' + str[1] : str[1];
	str[2] = (str[2].length == 1) ? '0' + str[2] : str[2];
	return ('#' + str.join(""));
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
function check_keypress(e){
	e = (window.event)?window.event:e;
	var key = (e.keyCode==0)?e.which:e.keyCode;	//codigo de tecla.
	if (key == 13) {	// Si es un enter
		if (bIsIE) {
			e.keyCode=0;	//anula la entrada.
		}
		else {
			e.preventDefault();
			e.stopPropagation();
		}
	}
}
function cerrar() {
	if(bIsIE || window.parent.hidePopWin==undefined) {
		var ventana = window.self;
		ventana.opener = window.self;
		ventana.close();
	}
	else {
		window.parent.hidePopWin();
	}
}
function cancel()
{
	closeDialog();
}
function check_datanumber(e)
{
	e = (window.event)?window.event:e;
	var key = (e.keyCode==0)?e.which:e.keyCode;	//codigo de tecla.
	if( !((key>=48 && key<=57) || key==8 || key==37 || key==39 || key==43 || key == 45 || key==46)) {
		//si no es numero anula la entrada.
		if (bIsIE) {
			e.keyCode = 0;
		}
		else {
			e.preventDefault();
			e.stopPropagation();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

function SetEditModalValues(sNewValues){
	if(sNewValues!='' && sNewValues!=undefined) {
		var aNewValues = sNewValues.split('°');
		var sMD = aNewValues[0];
		var aParamsVals = aNewValues[1].split('Æ');
		var Cons = aParamsVals[0];
		var sOption = aParamsVals[1];
		EASParams(sOption, Cons + "_AWSep_" + sMD);
	}
}
function FFModal()
{
	//test for Firefox/x.x or Firefox x.x (ignoring remaining digits);
	var NavUser=navigator.userAgent;
	if (/Firefox[\/\s](\d+\.\d+)/.test(NavUser))
	{
		 var ffversion=new Number(RegExp.$1) // capture x.x portion and store as a number
		 return(ffversion>=3);
	}
	/* CHROME, SAFARI no jala bien
	if (/Chrome[\/\s](\d+\.\d+)/.test(NavUser))
	{
		 var ffversion=new Number(RegExp.$1) // capture x.x portion and store as a number
		 return(ffversion>=2);
	}
	if (/Safari[\/\s](\d+\.\d+)/.test(NavUser))
	{
		 if (/Version[\/\s](\d+\.\d+)/.test(NavUser))
		 {
			 var ffversion=new Number(RegExp.$1) // capture x.x portion and store as a number
			 return(ffversion>=4);
		 }
	}
	*/
	return(false);
}
//JAPR 2016-08-09: Agregado el parámetro para permitir reemplazar los caracteres ASCII 160 (&nbsp convertidos a espacio, pero no ASCII 32) generados por componentes DHTMLX
function html_entity_decode( string, bClearNbsp ) {
    var ret, tarea = document.createElement('textarea');
    tarea.innerHTML = string;
    ret = tarea.value;
	//JAPR 2016-08-09: Agregado el parámetro para permitir reemplazar los caracteres ASCII 160 (&nbsp convertidos a espacio, pero no ASCII 32) generados por componentes DHTMLX
	if (bClearNbsp || bClearNbsp === undefined) {
		ret = ret.replace(/\u00a0/g, ' ');
	}
	//JAPR
    return ret;
}
function getBoolStr(bVal){
	if (bVal){ return '1'; }
	else     { return '0'; }
}
function getStrBool(sVal){
	if (sVal == '0' || sVal == 0){ return false; }
	else            			 { return true;  }
}
function getRestoreElem(aMD, Top, i, sDefault){
	if (i<=Top ){
		if ((aMD[i] != undefined)){
			return aMD[i];
		}
		else{
			return sDefault;
		}
	}
	else{
		return sDefault;
	}
}
function BITAMGetSiguiente(aParams, Token)
{
	var npos = aParams[0].indexOf(Token);
	if (npos != -1)
	{
		stmp = aParams[0].substr(0, npos);
		aParams[0] = aParams[0].substr(npos + Token.length);
		return stmp;
	}
	else
	{
        if (Trim(aParams[0]).length > 0)
        {
        	tmpCadena = aParams[0];
        	aParams[0] = '';
            return tmpCadena;
        }
		else
		{
            return '';
		}
	}
}
function specialcharstohtml(sCadToConvert){
	sCadToConvert = sCadToConvert.replace(/&lt;/gi,"<");
	sCadToConvert = sCadToConvert.replace(/&gt;/gi,">");
	sCadToConvert = sCadToConvert.replace(/&quot;/gi,'"');
	sCadToConvert = sCadToConvert.replace(/&#039;/gi,"'");
	sCadToConvert = sCadToConvert.replace(/&amp;/gi,"&");
	sCadToConvert = sCadToConvert.replace(/<br>/gi,"\n");
	sCadToConvert = sCadToConvert.replace(/_AWNL_/gi,"\n");
	sCadToConvert = sCadToConvert.replace(/&#42/gi,"*");
	return sCadToConvert;
}
function PutHtmlNonChar(sCadToConvert){
	var regexp = new RegExp('_AWSS_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,"\t");
	regexp = new RegExp('_AWSP_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp," ");
	regexp = new RegExp('_AWAM_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,"&");
	regexp = new RegExp('_AWComDob_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,'"');
	regexp = new RegExp('_AWComSim_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,"'");
	regexp = new RegExp('_AWMin_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,"<");
	regexp = new RegExp('_AWMay_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,">");
	regexp = new RegExp('_AWBS_',"gi");
	sCadToConvert = sCadToConvert.replace(regexp,"\\");
	return sCadToConvert;
}
//ESTA SI JALA VDD GILLO :-)
function decimalColorToHTMLcolor(number)
{
	 if(number==-1 || number=='-1') return(number);

     //converts to a integer
     var intnumber = number - 0;

     // isolate the colors - really not necessary
     var red, green, blue;

     // needed since toString does not zero fill on left
     var template = "#000000";

     // in the MS Windows world RGB colors
     // are 0xBBGGRR because of the way Intel chips store bytes
     red = (intnumber&0x0000ff) << 16;
     green = intnumber&0x00ff00;
     blue = (intnumber&0xff0000) >>> 16;

     // mask out each color and reverse the order
     intnumber = red|green|blue;

     // toString converts a number to a hexstring
     var HTMLcolor = intnumber.toString(16);

     //template adds # for standard HTML #RRGGBB
     HTMLcolor = template.substring(0,7 - HTMLcolor.length) + HTMLcolor;

     return HTMLcolor;
}

function Convert_Hex_To_Dec(Hex)
{
	if(parseInt(Hex)==-1) return(-1);
	Hex=Hex.substr(4,2)+Hex.substr(2,2)+Hex.substr(0,2);
	Dec=parseInt(Hex, 16);
	if(isNaN(parseInt(Hex, 16)))
		return 0;
	else
		return Dec;
}

function getPeriodName(nPeriodKey) {
	var sPerName = '';
	var nTop = gaPeriodKeys.length;
	for (var i=0;i<nTop;i++){
		if (gaPeriodKeys[i]==nPeriodKey) {
			sPerName = gaPeriodDescs[i];
			break;
		}
	}
	return sPerName;
}

function ReplaceInterFormat(sDecimalFrom, sGroupFrom, sDecimalTo, sGroupTo, sCad) {
	var nTopCad = sCad.length; var aCadRet = new Array();
	var bReplace = true; var sLetter = null;
	for (var i=0; i<nTopCad; i++) {
		sLetter = sCad.substring(i,(i+1));
		if (sLetter == '"') {
			bReplace = !bReplace;
		}
		if (bReplace) {
			if (sLetter == sDecimalFrom) {
				sLetter = sDecimalTo;
			}
			else if (sLetter == sGroupFrom) {
				sLetter = sGroupTo;
			}
		}
		aCadRet[i] = sLetter;
	}
	return aCadRet.join('');
}

function GetFormatDate(aDate, sFormat){
	var formated = sFormat.split('"');
	var ilen = formated.length;
	var brep = true;

	var yy = aDate.substring(0,4);
    var mm = aDate.substring(5,7);
    var dd = aDate.substring(8,10);

	for (ij=0; ij<ilen; ij++) {
		if (brep){
			formated[ij] = formated[ij].toLowerCase();
			aFormatedAux = formated[ij].split(/[^hwdmyq]+/);	// Dividir cadena [ que NO CONTENGA(^) los elementos ] NO IMPORTA QUE SE REPITAN (+)
			ipos = 0;
			sFormated = '';
			itop = aFormatedAux.length;
			for (kl=0; kl<itop; kl++) {
				sFormatedType = aFormatedAux[kl];
				switch (aFormatedAux[kl]){
			    	case "dd":
						aFormatedAux[kl] = dd;
						break;
			    	case "d":
						sdd = parseInt(dd, 10);
						aFormatedAux[kl] = sdd;
						break;
					case "mm":
						aFormatedAux[kl] = mm;
						break;
					case "m":
						smm = parseInt(mm, 10);
						aFormatedAux[kl] = smm;
						break;
					case "yyyy":
						aFormatedAux[kl] = yy;
						break;
					case "yy":
						aFormatedAux[kl] = yy.substring(2, 4);
						break;
				}
				iposfin = formated[ij].indexOf(sFormatedType, ipos);
				sFormated = sFormated + '' + formated[ij].substring(ipos, iposfin);	// Copiar los primeros n caracteres antes del replace
				sFormated = sFormated + '' + aFormatedAux[kl]; // Copiar la fecha con formato
				ipos = iposfin + (sFormatedType.length);// mover el puntero
			}
			if (ipos < formated[ij].length){
				sFormated = sFormated + '' + formated[ij].substring(ipos);
			}
			formated[ij] = sFormated;
		}
		brep = !brep;
	}
	return formated.join('');
}
function GetPeriodDrop(sMD, sTypePeriod, sParam, ColId){
	if (bLoading==0){
		return;
	}
	sPathX = jsDir;
	/*
	if (IsEAS != 1 && !IsThatVersion('Firefox/3')) {
		sPathX = '../../';
	}
	*/
	bOnlyOne = true;
	sWindowWith = '330px';
	aFormatX = 'mmmm yyyy';
	iTitle = 68;
	var nPeriodKeyParam = 4; var bIsCatVar = false;
	if (sMD.indexOf('CurrentRelative') != -1) {
		aDateX = 'ActRel,0';
		iTitle = 72;
	}
	else if(sMD.indexOf('Range') != -1) {
		miFechaActual = new Date();
		annio = miFechaActual.getFullYear();
		mes = parseInt(miFechaActual.getMonth()) + 1;
		dia = miFechaActual.getDate();
		if (mes<10) { mes = '0'+mes; }
		if (dia<10) { dia = '0'+dia; }
		aDateX = annio+''+mes+''+dia;
		bOnlyOne = false;
		sWindowWith = '555px';
		aAux = sMD.split('_');
		aFormatX = aAux[2];
		nPeriodKeyParam = parseInt(aAux[1]);
		bIsCatVar = (aPeriodData[nPeriodKeyParam] != undefined && aPeriodData[nPeriodKeyParam] != null);
	}
	else {
		aDateX = 'Actual,0';
		iTitle = 71;
	}
	var sRet = null;
	if (HaveModalWindow()) {
	    showModalDialog
	        (sPathX+'rangedates.htm',
	        	[ML[iTitle], aDateX, aDateX, aFormatX, window, ML[69],ML[70],ML[71],ML[72],ML[73], ML, bOnlyOne, false,
	        	nPeriodKeyParam, bIsCatVar, aPeriodData],
	        "dialogWidth: "+sWindowWith+"; dialogHeight: 215px; resizable: yes; center: yes; help: no; status: no; scroll:no"
	        );
	    if (window.returnValue != null) {
	    	date1 = window.returnValue[0];
	        date2 = window.returnValue[1];
	        aAux = sMD.split('_');
	    	if (sMD.indexOf('Current') != -1) {
				sRet = '_AWSep_'+aAux[1]+'_AWSep_'+date1+'_AWSep_'+date1;
	    	}
	    	else {
				sRet = '_AWSep_'+aAux[1]+'_AWSep_'+date1+'_AWSep_'+date2;
	    	}
	    	window.returnValue = null;
	    }
	}
	else {
		gDialogArguments = ML[iTitle]+'Æ'+aDateX+'Æ'+aDateX+'Æ'+aFormatX+'ÆnullÆ'+ML[69]+'Æ'+ML[70]+'Æ'+ML[71]+'Æ'+ML[72]+'Æ'+ML[73];
		gDialogArguments = gDialogArguments+'Æ'+ML.join('_ML_AWSep_')+'Æ'+bOnlyOne+'°'+sParam+'Æ'+ColId+'Æ'+sMD;
	    showPopWin(sPathX+'rangedates.htm', parseInt(sWindowWith), 215, null);
	}
	return sRet;
}
function SetTitle(sObjId) {
	var objSel = document.getElementById(sObjId);
	var selIndex = objSel.selectedIndex;
	if (selIndex != -1) {
		objSel.title = objSel.options[selIndex].text;
	}
}
function getCatPeriodDesc(sDateParam, regexp) {
	var sDateReturn = sDateParam;
	sDateParam = sDateParam.replace(regexp,'');
	var bFound = false;
	if (aPeriodData[sTBPeriod_Key] != undefined) {
		var nTopData = aPeriodData[sTBPeriod_Key]['Rang'].length;
		for (var i=0; i<nTopData; i++) {
			var aData = aPeriodData[sTBPeriod_Key]['Rang'][i];
			var range_bottom = aData[0].replace(regexp,'');
			var range_top = aData[1].replace(regexp,'');
			if (sDateParam >= range_bottom && sDateParam <= range_top) {
				sDateReturn = aPeriodData[sTBPeriod_Key]['Desc'][i];
				bFound = true;
				break;
			}
		}
		if (!bFound && nTopData > 0) {
			// Found has previous initial date
			i = 0;
			var aData = aPeriodData[sTBPeriod_Key]['Rang'][i];
			var range_bottom = aData[0].replace(regexp,'');
			if (sDateParam < range_bottom) {
				sDateReturn = aPeriodData[sTBPeriod_Key]['Desc'][i];
			}
			// Found has last final date
			else if (nTopData>0) {
				i = nTopData-1;
				var aData = aPeriodData[sTBPeriod_Key]['Rang'][i];
				var range_top = aData[1].replace(regexp,'');
				if (sDateParam > range_top) {
					sDateReturn = aPeriodData[sTBPeriod_Key]['Desc'][i];
				}
			}
		}
	}
	return sDateReturn;
}
function HaveModalWindow() {
	return (window.showModalDialog);
}
function getOrderDims(aToSort, nCubeK) {
	if (aDimKeys[nCubeK] == undefined || aToSort[0].length <= 1) {
		return aToSort.slice(0);
	}
	var aReturn = new Array();
	if (aToSort[0][0] == 999999999) {
		aReturn.push(999999999);
	}
	var nTopDK = aDimKeys[nCubeK].length;
	var nTopDD = aToSort[0].length;
	for (var i=0; i<nTopDK; i++) {
		for (var j=0; j<nTopDD; j++) {
			if (aDimKeys[nCubeK][i] == aToSort[0][j]) {
				aReturn.push(aToSort[0][j]);
				break;
			}
		}
	}
	return [aReturn.slice(0)];
}
function mainUpdateUsageStats() {
	window.parent.Parent_UpdateUsageStats(dateUsageStat, hourUsageStat, timeIUsageStat);
}
function setInputUsageStats(bClear){
	if (GetElementWithId('inpUsageStat') != undefined) {
		var sValue = (bClear || dateUsageStat == null || hourUsageStat == null || timeIUsageStat == null)
					    ? '' : dateUsageStat+'|'+hourUsageStat+'|'+timeIUsageStat;
		GetElementWithId('inpUsageStat').value = sValue;
	}
}
function scrolltable(sTableId, scrollTo){
	var nMoveScroll = 200;
	switch (scrollTo) {
		case 'up':
			var oTDScroll = GetElementWithId('DivScrollH'+sTableId);
			if (oTDScroll) {
				if (oTDScroll.scrollTop > 0) {
					if (oTDScroll.scrollTop - nMoveScroll < 0) {
						oTDScroll.scrollTop = 0;
					}
					else {
						oTDScroll.scrollTop-=nMoveScroll;
					}
				}
			}
			break;
		case 'down':
			var oTDScroll = GetElementWithId('DivScrollH'+sTableId);
			if (oTDScroll) {
				if (oTDScroll.scrollTop < oTDScroll.scrollHeight) {
					if (oTDScroll.scrollTop+nMoveScroll > oTDScroll.scrollHeight) {
						oTDScroll.scrollTop = oTDScroll.scrollHeight;
					}
					else {
						oTDScroll.scrollTop += nMoveScroll;
					}
				}
			}
			break;
		case 'left':
			var oTDScroll = GetElementWithId('D'+sTableId);
			if (oTDScroll) {
				if (oTDScroll.scrollLeft > 0) {
					if (oTDScroll.scrollLeft - nMoveScroll < 0) {
						oTDScroll.scrollLeft = 0;
					}
					else {
						oTDScroll.scrollLeft-=nMoveScroll;
					}
				}
			}
			break;
		case 'right':
			var oTDScroll = GetElementWithId('D'+sTableId);
			if (oTDScroll) {
				if (oTDScroll.scrollLeft < oTDScroll.scrollWidth) {
					if (oTDScroll.scrollLeft+nMoveScroll > oTDScroll.scrollWidth) {
						oTDScroll.scrollLeft = oTDScroll.scrollWidth;
					}
					else {
						oTDScroll.scrollLeft += nMoveScroll;
					}
				}
			}
			break;
	}
}

var temporTB = null;
var nStepCollapse = 20;
var zIndexEASToolbarTmp = null;
function ExpCol(sId, sIdMoveBack, sIdImgExpCol){
	if (bLoading==0){
		return;
	}
	var oFrmWDTB = GetElementWithId(sId);
	if (oFrmWDTB) {
		var bCollapse = oFrmWDTB.getAttribute('bCollapse');
		if (bCollapse == undefined) {
			bCollapse = 1;
		}
		else {
			bCollapse = (bCollapse=='1') ? 0 : 1;
		}
		oFrmWDTB.setAttribute('bCollapse', bCollapse);

		var nWidthFrm = oFrmWDTB.getAttribute('nWFrm');
		if (nWidthFrm == undefined) {
			oFrmWDTB.setAttribute('nWFrm', parseInt(oFrmWDTB.style.width));
		}

		var oInpShowTB = GetElementWithId('bShowTBWD');
		if (oInpShowTB) {
			oInpShowTB.value = (bCollapse) ? 0 : 1;;
		}

		if (sIdMoveBack != '')
		{
			MoveDivBack('images/mainmenu_animation.gif', (parseInt(oFrmWDTB.style.height)+parseInt(oFrmWDTB.style.top)+1), true, false); // sIdMoveBack
		}

		if (temporTB != null) {
			clearInterval(temporTB);
		}

		var objFrmEASTB = GetElementWithId('frameeastoolbar');
		if (objFrmEASTB)
		{
			if (bCollapse)
			{
				objFrmEASTB.style.zIndex = oFrmWDTB.style.zIndex;
				oFrmWDTB.style.zIndex = zIndexEASToolbarTmp;
				zIndexEASToolbarTmp = null;
			}
			else
			{
				zIndexEASToolbarTmp = oFrmWDTB.style.zIndex;
				oFrmWDTB.style.zIndex = objFrmEASTB.style.zIndex;
				objFrmEASTB.style.zIndex = zIndexEASToolbarTmp;
			}
		}

		temporTB = setInterval("TimerCollapse('"+sId+"', '"+sIdMoveBack+"', '"+sIdImgExpCol+"');",1);
	}
}
function TimerCollapse(idFrm, sIdMoveBack, sIdImgExpCol){
	var oFrmWDTB = GetElementWithId(idFrm);
	if (oFrmWDTB) {
		var bCollapse = oFrmWDTB.getAttribute('bCollapse');
		if (bCollapse == undefined) {
			bCollapse = '1';
		}
		var oImgExpCol = GetElementWithId(sIdImgExpCol);
		if (window.document.documentElement.clientHeight == 0){
			var objDocB = document.body;
		}
		else {
			var objDocB = window.document.documentElement;
		}
		var scrollLeft = (typeof(window.scrollX) != 'undefined') ? window.scrollX : objDocB.scrollLeft;
		var nLimWidth = scrollLeft;
		if (bCollapse == '1')
		{
			if (parseInt(oFrmWDTB.style.width) > 0) {
				if ((parseInt(oFrmWDTB.style.width) - nStepCollapse) < 0) {
					oFrmWDTB.style.width = '0px';
					if (oImgExpCol) {
						var nLeftMinPos = oImgExpCol.getAttribute('nLeftMinPos');
						if (nLeftMinPos != undefined) {
							oImgExpCol.style.left = (parseInt(nLeftMinPos)+nLimWidth)+'px';
						}
						var sPrefImg = oImgExpCol.getAttribute('prefimg');
						if (sPrefImg == undefined) {
							sPrefImg = 'wd_';
						}
						oImgExpCol.setAttribute('imgOver', 'images/'+sPrefImg+'tb_expand_over.png');
						oImgExpCol.setAttribute('imgOut',  'images/'+sPrefImg+'tb_expand.png');
						oImgExpCol.src = jsDir+'images/'+sPrefImg+'tb_expand.png';
					}

					if (sIdMoveBack != '')
					{
						MoveDivBack('images/wd_mainmenu.png', (parseInt(oFrmWDTB.style.height)+parseInt(oFrmWDTB.style.top)+1), false, false);
					}
				}
				else {
					oFrmWDTB.style.width = (parseInt(oFrmWDTB.style.width) - nStepCollapse) + 'px';
					if (oImgExpCol) { oImgExpCol.style.left = (parseInt(oImgExpCol.style.left) - nStepCollapse)+'px'; }
				}
			}
			else {
				clearInterval(temporTB);
			}
		}
		else {
			var nWidthFrm = oFrmWDTB.getAttribute('nWFrm');
			if (nWidthFrm == undefined) {
				clearInterval(temporTB);
				return;
			}
			if (parseInt(oFrmWDTB.style.width) < parseInt(nWidthFrm)) {
				if ((parseInt(oFrmWDTB.style.width) + nStepCollapse) >= parseInt(nWidthFrm)) {
					oFrmWDTB.style.width = parseInt(nWidthFrm)+'px';
					if (oImgExpCol) {
						var nLeftMaxPos = oImgExpCol.getAttribute('nLeftMaxPos');
						if (nLeftMaxPos != undefined) {
							oImgExpCol.style.left = (parseInt(nLeftMaxPos)+nLimWidth)+'px';
						}
						var sPrefImg = oImgExpCol.getAttribute('prefimg');
						if (sPrefImg == undefined) {
							sPrefImg = 'wd_';
						}
						oImgExpCol.setAttribute('imgOver', 'images/'+sPrefImg+'tb_collapse_over.png');
						oImgExpCol.setAttribute('imgOut',  'images/'+sPrefImg+'tb_collapse.png');
						oImgExpCol.src = jsDir+'images/'+sPrefImg+'tb_collapse.png';
					}

					if (sIdMoveBack != '')
					{
						MoveDivBack('images/wd_mainmenu.png', (parseInt(oFrmWDTB.style.height)+parseInt(oFrmWDTB.style.top)+1), true, false); // sIdMoveBack
					}
				}
				else {
					oFrmWDTB.style.width = (parseInt(oFrmWDTB.style.width) + nStepCollapse) + 'px';
					if (oImgExpCol) { oImgExpCol.style.left = (parseInt(oImgExpCol.style.left) + nStepCollapse)+'px'; }
				}
			}
			else {
				clearInterval(temporTB);
			}
		}
	}
	else {
		clearInterval(temporTB);
	}
}
function MoveDivBack(sIdDivBack, nPixMove, bMoveBack, bMoveMainDiv) {
//return;
/*
	var oDiv = GetElementWithId(sIdDivBack);
	if (oDiv) {
		oDiv.style.top = ((!bMoveBack) ? 0 : nPixMove) + 'px';
	}
*/
	if (bMoveMainDiv || true) {
		var oDiv = GetElementWithId('mainmenu');
		if (oDiv)
		{
			//oDiv.style.top = ((!bMoveBack) ? 0 : 5) + 'px';
			var sprefimg = oDiv.getAttribute('prefimg');
			if (sprefimg)
			{
				sIdDivBack = sIdDivBack.replace('mainmenu.gif', 'mm_tb_expand.png');
			}
			oDiv.src = sIdDivBack;
		}
	}
}
function getUserLongName(nUserKey) {
	var sUserName = '_#NOTFOUND#_';
	var nTop = aUserKeys.length;
	for (var i=0;i<nTop;i++){
		if (aUserKeys[i]==nUserKey) {
			sUserName = aUserDescs[i+1];
			break;
		}
	}
	return sUserName;
}
function copy(origen,copia){
    for(var clave in origen){
        if(typeof origen[clave] == 'object'){
            var elementos = origen[clave].length;
            copia[clave] = new Array(elementos);
            copy(origen[clave],copia[clave]);
        }
        else {
            copia[clave] = origen[clave];
        }
    }
}
function copy_WReplace(origen,copia,sReplace){
    for(var clave in origen){
        if(typeof origen[clave] == 'object'){
            var elementos = origen[clave].length;
            copia[clave] = new Array(elementos);
            copy_WReplace(origen[clave],copia[clave],sReplace);
        }
        else {
            copia[clave] = sReplace+origen[clave];
        }
    }
}
function pauseJS(timeInMilliS)
{
	var date = new Date();
	var curDate = null;
	do
	{
		curDate = new Date();
	}
	while(curDate-date < timeInMilliS);
}
function ucFirst(str) {
    str += '';
    var sReturn = str.charAt(0).toUpperCase();
    return sReturn + str.substr(1);
}
function PopDimsPresents(aKeysMenu, sDimsParams){
	var aKeysMenuReturn = new Array();
	var nTop = aKeysMenu.length;
	var sDimSearch = ''; var sDimIn = ','+sDimsParams+',';
	for (var i=0; i<nTop; i++) {
		sDimSearch = ','+aKeysMenu[i]+',';
		if (sDimIn.indexOf(sDimSearch) == -1) {
			aKeysMenuReturn.push(aKeysMenu[i]);
		}
	}
	return aKeysMenuReturn;
}
function getMenuDescToolBar(aKeys, nCubeK, nCase, nPivots, paramIsTC) {
	var aDescs = new Array();
    var aDimDesc = new Array();
    var bIsTC = (paramIsTC == undefined) ? false : paramIsTC;

    var aGpoIDims = new Array();
    var aGpoDDims = new Array();
    var aGpoArrKDims = new Array();
    var aGpoArrDDims = new Array();
    var pos_in_gpo = -1;

    aDescs[0] = 0;
    var aDimKeys = aKeys; //aKeys[0];
    var sAddDD = ''; var bHaveColWithPeriod = false;
    switch (nCase) {
    	case -1:	// Only Dimensions
    		aDimDesc[0] = ML[121];
    		break;
    	case 0:		// Add Dimension
    		sAddDD = 'DIM,'
    		aDimDesc[0] = ML[39];
    		break;
    	case 1:		// Add Pivot Dimension
    		sAddDD = 'PIV,'
    		aDimDesc[0] = ML[38];
    		bHaveColWithPeriod = (IsEAS) ? window.parent.framebody.bHaveColWithPeriod || window.parent.framebody.bNFTPeriodInTable
    									 : bHaveColWithPeriodTable;
    		break;
    	case 2:		// Change Dimension
    		aDimDesc[0] = ML[40];
    		break;
    	default:	// Only dimension
    		aDimDesc[0] = ML[121];
    		break;
    }
    var top = aDimKeys.length;
    var top2 = -1;
    var iaux = -1;
    var i = 0;
    if (aDimKeys[i]>0){
	    var bHaveDelete = false;
	    if (sAddDD == 'PIV,' && nPivots>0) {
	    	bHaveDelete = true;

	    	pos_in_gpo = (!bHaveColWithPeriod) ? -4 : -2;
	    	if (bIsTC && nPivots > 1) {
	    		pos_in_gpo--;
	    		pos_in_gpo--;

	    		if (aGpoArrKDims[pos_in_gpo] == undefined){
	    			aGpoArrKDims[pos_in_gpo] = new Array();
	    			aGpoArrDDims[pos_in_gpo] = new Array();
	    		}
	    		aGpoArrDDims[pos_in_gpo].push('_AWSReplaceML_'+ML[353]);
	    		aGpoArrKDims[pos_in_gpo].push('DPS,');
	    		pos_in_gpo++;

	    		if (aGpoArrKDims[pos_in_gpo] == undefined){
	    			aGpoArrKDims[pos_in_gpo] = new Array();
	    			aGpoArrDDims[pos_in_gpo] = new Array();
	    		}
	    		aGpoArrDDims[pos_in_gpo].push('---');
	    		aGpoArrKDims[pos_in_gpo].push(-1);
	    		pos_in_gpo++;
	    	}

    		if (aGpoArrKDims[pos_in_gpo] == undefined){
    			aGpoArrKDims[pos_in_gpo] = new Array();
    			aGpoArrDDims[pos_in_gpo] = new Array();
    		}
    		aGpoArrDDims[pos_in_gpo].push(ML[42]);
    		aGpoArrKDims[pos_in_gpo].push('DPD,');
    		pos_in_gpo++;
    		//pos_in_gpo = (!bHaveColWithPeriod) ? -3 : -1;

    		if (aGpoArrKDims[pos_in_gpo] == undefined){
    			aGpoArrKDims[pos_in_gpo] = new Array();
    			aGpoArrDDims[pos_in_gpo] = new Array();
    		}
    		aGpoArrDDims[pos_in_gpo].push('---');
    		aGpoArrKDims[pos_in_gpo].push(-1);
    		pos_in_gpo++;
	    }
	    if (sAddDD == 'PIV,' && !bHaveColWithPeriod) {
		    pos_in_gpo = -2;
			if (aGpoArrKDims[pos_in_gpo] == undefined){
				aGpoArrKDims[pos_in_gpo] = new Array();
				aGpoArrDDims[pos_in_gpo] = new Array();
			}
			aGpoArrDDims[pos_in_gpo].push(ML[685]);
			aGpoArrKDims[pos_in_gpo].push('PDD,');

			pos_in_gpo = -1;
			if (aGpoArrKDims[pos_in_gpo] == undefined){
				aGpoArrKDims[pos_in_gpo] = new Array();
				aGpoArrDDims[pos_in_gpo] = new Array();
			}
			aGpoArrDDims[pos_in_gpo].push('---');
			aGpoArrKDims[pos_in_gpo].push(-1);
	    }

    	for (i=0;i<top;i++) {
	    	pos_in_gpo = Search_Position(aDimKeys[i], aGpoIDims, aGpoDDims, nCubeK);
	    	if (pos_in_gpo != -1) {	// IF FIND POSITION
	    		if (aGpoArrKDims[pos_in_gpo] == undefined){
	    			aGpoArrKDims[pos_in_gpo] = new Array();
	    			aGpoArrDDims[pos_in_gpo] = new Array();
	    		}
	    		aGpoArrDDims[pos_in_gpo].push(getDimensionName(aDimKeys[i], nCubeK));
	    		if (sAddDD == '') {
	    			aGpoArrKDims[pos_in_gpo].push(aDimKeys[i]);
	    		}
	    		else {
					aGpoArrKDims[pos_in_gpo].push(sAddDD+aDimKeys[i]);
	    		}
	    	}
	    }
	    var iIni = 0;
		if (bHaveDelete) {
			iIni = (!bHaveColWithPeriod) ? -4 : -2;

			if (bIsTC && nPivots > 1) {
	    		iIni--;
	    		iIni--;
			}
		}
		else if (sAddDD == 'PIV,' && !bHaveColWithPeriod) {
			iIni = -2;
		}
	    aKeys[0]= new Array();
	    top = aGpoIDims.length;
	    for (i=iIni, iaux=0; i<top; i++, iaux++) {
	    	if (aGpoDDims[i] == '') {
	    		top2 = aGpoArrKDims[i].length;
	    		for (j=0; j<top2; j++){
	    			aDimDesc[iaux+1] = aGpoArrDDims[i][j];
	    			aKeys[0].push(aGpoArrKDims[i][j]);
	    			iaux++;
	    		}
	    		if ((iaux > 0) && (top2 > 0)) { iaux--; }
	    	}
	    	else {
	    		aDimDesc[iaux+1] = new Array();
	    		if (i>-1) {
	    			aDimDesc[iaux+1].push(aGpoDDims[i]);
	    			aKeys[0].push(aGpoArrKDims[i]);
	    		}
	    		else {
	    			aKeys[0].push(aGpoArrKDims[i][0]);
	    		}

	    		top2 = aGpoArrKDims[i].length;
	    		for (j=0; j<top2; j++) {
	    			if (i>-1) {
	    				aDimDesc[iaux+1].push(aGpoArrDDims[i][j]);
	    			}
	    			else {
	    				aDimDesc[iaux+1] = aGpoArrDDims[i][j];
	    			}
	    		}
	    	}
	    }
    }
    else{
    	// Colapse TD Row
    	if (aDimKeys[i]<-100){
    		aDimDesc[1] =ML[236];
    	}
    }
    aDescs[1] = aDimDesc;
    return aDescs;
}
function array_search(nValueSearch, aValues) {
	var nTop = aValues.length;
	var nPos = -1;
	for (var i=0; i<nTop; i++) {
		if (aValues[i] == nValueSearch) {
			nPos = i;
			break;
		}
	}
	return nPos;
}
function PivotDate(){
	if (bLoading==0){
		return;
	}
	if (!IsEAS)
	{
		var sTBPeriod_KeyP = (aMetaTD[nEscAction] && aMetaTD[nEscAction][38] != undefined) ? aMetaTD[nEscAction][38] : -1;
		var arrColPivP = GetStrColPiv();
		var bPivDateRangeP = IsSelected(aMetaTD, nEscAction, 18, 1, true);
		var nCubeP = (aMetaTD[nEscAction] != undefined && aMetaTD[nEscAction][44] != undefined) ? aMetaTD[nEscAction][44] : -1;
		var sTBPeriod_FormatP = (aMetaTD[nEscAction] && aMetaTD[nEscAction][39] != undefined) ? aMetaTD[nEscAction][39] : 'yyyy-mm-dd';
		var sTBPeriod_NameP = getPeriodName(sTBPeriod_KeyP);
		var sTBPeriod_CatVarP = false;
		if (aPeriodKeys[nCubeP] != undefined && aPeriodCatVars[nCubeP] != undefined)
		{
			var nPosPerCatVar = array_search(sTBPeriod_KeyP, aPeriodKeys[nCubeP]);
			if (nPosPerCatVar != -1)
			{
				sTBPeriod_CatVarP = aPeriodCatVars[nCubeP][nPosPerCatVar];
			}
		}
	}
	else {
		var sTBPeriod_KeyP = sTBPeriod_Key;
		var arrColPivP = arrColPiv;
		var bPivDateRangeP = bPivDateRange;
		var nCubeP = nCube;
		var sTBPeriod_FormatP = sTBPeriod_Format;
		var sTBPeriod_NameP = sTBPeriod_Name;
		var sTBPeriod_CatVarP = sTBPeriod_CatVar;
	}
	if (HaveModalWindow()) {
		showModalDialog
	        (jsDir+'piv_dates.php?sTBPeriod_Key='+sTBPeriod_KeyP,
	        	[window, arrColPivP, ML[685], bPivDateRangeP, nCubeP, sTBPeriod_KeyP, sTBPeriod_NameP, sTBPeriod_FormatP, sTBPeriod_CatVarP],
	        	"dialogWidth: 550px; dialogHeight: 450px; resizable: yes; center: yes; help: no; status: no; scroll:no"
	        );
	    if (window.returnValue != null)
	    {
	        SetPivotDate('_AWSep_'+window.returnValue); // _AWNotOrder_
	        window.returnValue = null;
	    }
	}
	else {
		gDialogArguments = 'nullÆ'+arrColPivP+'Æ'+ML[685]+'Æ'+getBoolStr(bPivDateRangeP)+'Æ'+nCubeP;
		gDialogArguments = gDialogArguments+'Æ'+sTBPeriod_KeyP+'Æ'+sTBPeriod_NameP+'Æ'+sTBPeriod_FormatP;
		gDialogArguments = gDialogArguments+'Æ'+getBoolStr(sTBPeriod_CatVarP);
		gDialogArguments = gDialogArguments+'°';
		showPopWin('piv_dates.php?sTBPeriod_Key='+sTBPeriod_KeyP, 550, 450, null);
	}
}
function SetPivotDate(sPivDates){
	var aParams = sPivDates.split("_AWSep_"); var sOptionEAS = 'PivPer';
	if (aParams[3] == undefined || aParams[3] == null || aParams[3] == '') {
		sOptionEAS = 'ChangePer';
		sPivDates = aParams[0]+'RemovePivDate_AWSep_'+aParams[1]+'_AWSep_'+aParams[2];
	}
	EASParams(sOptionEAS,sPivDates);
}
function getValidImageSrc(src) {
	src = myReplace(src, '%', '%25');
	src = myReplace(src, '#', '%23');
	src = myReplace(src, ' ', '%20');
	return src;
}
function checkBackKey(anEvent)
{
	var anElement = (anEvent.srcElement) ? anEvent.srcElement : anEvent.target;
	if (anElement.id == 'filter_dims')
	{
		return;
	}

	var keyPressed = (anEvent.keyCode==0) ? anEvent.which : anEvent.keyCode;	//codigo de tecla.
	if (keyPressed == 8) {	// Back KeyCode
		if (bIsIE) {
			anEvent.keyCode = 0;
		}
		else {
			anEvent.preventDefault();
			anEvent.stopPropagation();
		}
	}
}
function ParsePathImg(sImgPath) {
	var sImgRet = sImgPath;
	var nPosProjects = sImgPath.indexOf('projects/');
	if (nPosProjects != -1) {
		var nPosImages = sImgPath.indexOf('/images/', nPosProjects);
		if (nPosImages != -1) {
			sImgRet = repository+'/images/'+sImgPath.substring(nPosImages+8);
		}
	}
	return sImgRet;
}

function SetFilter(sCompId, sMD, sFunction) {
	//ASP|Indicator|PeriodKey|Levels|Filter
	var regexp = new RegExp("\\\\","gi");
	var sPageModal = jsDir+"filter.php?params=|"+sMD.replace(regexp, '_AWBS_');
  /*
	window.showModalDialog(
    sPageModal,
    [ML[654],window],
		"dialogHeight:600px; dialogWidth:380px; center:yes; help:no; resizable:no; status:no"
  );
  */
  openWindowDialog(
    sPageModal,
    [ML[654], window],
    [sFunction, sCompId],
    380, 600,
    dlgCallBackFilter,
    "dlgCallBackFilter"
  );

  /*
	if(window.returnValue !== null) {
		var aRV = window.returnValue.split('|');
		sLevels = aRV[0];
		sFilter = aRV[1];
		sFilter = myReplace(sFilter,'_AWSPipe_','|');
		sFilter = myReplace(sFilter,'_AWSAmp_','&');
		window.returnValue = null;
		if (sFunction.substring(0,11) === 'ColTCFilter'){
			EASParams('ColTCFilter',sCompId+'_AWSep_'+ sFunction.substring(11)+'_AWSep_'+sLevels+'_AWSep_'+sFilter);
		}
		else if (sFunction === 'RowFilter' || sFunction === 'ColumnFilter'){
			EASParams(sFunction,sCompId+'_AWSep_'+sLevels+'_AWSep_'+sFilter);
		}
		else if (!bDesigner && sFunction === 'FilterTable') {
			EASParams(sFunction,sLevels+'_AWSep_'+sFilter);
		}
		else{
			StgParams(sCompId,sFunction,sLevels+'_AWSep_'+sFilter,1);
		}
	}
  */
}

function dlgCallBackFilter(returnValue, aArgContinue)
{
  if (returnValue !== undefined && returnValue !== '')
  {
    var sFunction = aArgContinue[0];
    var sCompId   = aArgContinue[1];

    var aRV = returnValue.split('|');
		sLevels = aRV[0];
		sFilter = aRV[1];
		sFilter = myReplace(sFilter,'_AWSPipe_','|');
		sFilter = myReplace(sFilter,'_AWSAmp_','&');

		if (sFunction.substring(0,11) === 'ColTCFilter'){
			EASParams('ColTCFilter',sCompId+'_AWSep_'+ sFunction.substring(11)+'_AWSep_'+sLevels+'_AWSep_'+sFilter);
		}
		else if (sFunction === 'RowFilter' || sFunction === 'ColumnFilter'){
			EASParams(sFunction,sCompId+'_AWSep_'+sLevels+'_AWSep_'+sFilter);
		}
		else if (!bDesigner && sFunction === 'FilterTable') {
			EASParams(sFunction,sLevels+'_AWSep_'+sFilter);
		}
		else{
			StgParams(sCompId,sFunction,sLevels+'_AWSep_'+sFilter,1);
		}
  }
}

function GetStrColPiv() {
	// PeriodKey;FormatPeriod;oFF|REI|sPDates
	var sColPiv = '';
	if (aMetaTD[nEscAction][38] != undefined) {
		sColPiv = aMetaTD[nEscAction][38] + ';';
	}
	if (aMetaTD[nEscAction][39] != undefined) {
		sColPiv += aMetaTD[nEscAction][39] + ';';
	}
	if (aMetaTD[nEscAction][40] != undefined) {
		sColPiv += aMetaTD[nEscAction][40] + '|REI|';
	}
	if (aMetaTD[nEscAction][41] != undefined) {
		sColPiv += aMetaTD[nEscAction][41];
	}
	return sColPiv;
}
function IsDimensionDesc(nTableId, nIdCol, bAll){
	var nType = 0; // NFT_None
	if (aMetaTD[nTableId]['C'+nIdCol] != undefined && aMetaTD[nTableId]['C'+nIdCol][3] != undefined)
	{
		nType = aMetaTD[nEscAction]['C'+nIdCol][3];
	}
	if (nType == 4 || // NFT_Dimension
		nType == 0 || // NFT_None
		nType == 5 || // NFT_Period
		nType == 9 || // NFT_Row_Indicator
		nType == 11)  // NFT_Ranges
	{
		return true;
	}
	else if (bAll && nType == 8) // NFT_Scenario
	{
		return true;
	}
	else{
		return false;
	}
}
function getSetUsers(sNewValue, sNewOption) {
	var labels=ML[463];
    var types ="SEL";
    var valid ="";
    var aValsUsers = new Array();
    for (var i = 0; i < aUserKeys.length; i++)
    {
    	var sUsName = aUserDescs[i+1];
    	if (sUsName.length > 27) {
    		sUsName = sUsName.substring(0,27)+'...';
    	}
		aValsUsers.push(aUserKeys[i]+'|'+sUsName);
    }
    var values = aValsUsers.join('_AWSep_');
    var sValReturn = null;

    openWindowDialog(jsDir+'ask_data.php', [ML[529], window, labels, types, values, valid], [sNewValue, sNewOption],
					 400, 150, SetUsersModalValues);

	return sValReturn;
}
function SetUsersModalValues(sNewValues, aArgCont)
{
	if (sNewValues != undefined)
	{
		var sNewValue = aArgCont[0];
		var sNewOption = aArgCont[1];
		CopyDash(sNewValues, sNewValue, sNewOption);
	}
}
function getRenameDash(sDashName, sInfo) {
	var regexp = new RegExp(',',"gi");
	var labels=ML[74];
    var types ="INP";
    var valid ="alfa";
    var values = sDashName.replace(regexp,'_AWSCOMA_');
    var sValReturn = null;

    openWindowDialog(jsDir+'ask_data.php', [ML[529], window, labels, types, values, valid], [sInfo],
					 400, 150, SetRenameDashModalValues);

    return sValReturn;
}
function SetRenameDashModalValues(sNewValues, aArgCont)
{
	if (sNewValues != undefined)
	{
		var sInfo = aArgCont[0];
		var nUserSel = sInfo.replace('User::', '');
		CopyDash(nUserSel, sNewValues, 'RENAME');
	}
}
function getReplaceDash(sDashName, sGpoDashName, aInfo) {
	var regexp = new RegExp(',',"gi");
	var labels=ML[852].replace('_AWSDASHNAME_', sDashName);
	labels=labels.replace('_AWSGPODASHNAME_', sGpoDashName);
    var types ="SEL";
    var valid ="";
    var values = '0|'+ML[853]+'_AWSep_1|'+ML[677];
    var sValReturn = null;
    var nHeightW = 150;

    if (labels.length > 60)
    {
		var nDif = labels.length - 60;
		nHeightW += nDif;
    }

    openWindowDialog(jsDir+'ask_data.php', [ML[529], window, labels, types, values, valid], [sDashName, sGpoDashName, aInfo],
					 400, 150, SetReplaceDashModalValues);

    return sValReturn;
}
function SetReplaceDashModalValues(sNewValues, aArgCont)
{
	if (sNewValues != undefined)
	{
		var sNameDash = aArgCont[0];
		var sGpoDashName = aArgCont[1];
		var aInfo = aArgCont[2];

		if (sNewValues == 0) // Reemplazar
		{
			var nUserSel = (aInfo['User'] != undefined) ? aInfo['User'] : '';
			CopyDash(nUserSel, sNameDash, 'REPLACE');
		}
		else if (sNewValues == 1) // Renombrar
		{
			var nUserSel = (aInfo['User'] != undefined) ? aInfo['User'] : '';
			CopyWait('RENAME_DASH:'+sNameDash, 'User::'+nUserSel);
		}
	}
}
function processInfo(sInfoParam) {
	var aInfoLocal = new Array();
	if (sInfoParam != '')
	{
		var aInfoParam = sInfoParam.split('_AWSep_');
		var nTop = aInfoParam.length;
		var regexp1 = new RegExp('_AWSep::_',"gi");
		for (var i=0; i<nTop; i++)
		{
			if (aInfoParam[i] != '')
			{
				var aTempInfo = aInfoParam[i].split('::');
				aTempInfo[1] = aTempInfo[1].replace(regexp1, '::');
				aInfoLocal[aTempInfo[0]] = aTempInfo[1];
			}
		}
	}
	return aInfoLocal;
}
function gotoKeyPress(anEvent, idObj) {
	var key = (anEvent.keyCode == 0) ? anEvent.which : anEvent.keyCode;	//codigo de tecla.
	var cadena = String.fromCharCode(key);
	var objDiv = GetElementWithId(idObj);
	var objTB = GetElementWithId('tb'+idObj);
	var bFound = false;
	if (objDiv && objTB)
	{
		// Buscarlo el siguiente donde está posicionado para abajo
		bFound = SearchAndPoint(objTB, objDiv, cadena, true);
		if (!bFound)
		{
			// Buscarlo desde arriba hasta donde está posicionado
			bFound = SearchAndPoint(objTB, objDiv, cadena, false);
			if (!bFound)
			{
				// Buscarlo pero si es Mayuscula tons minuscula y viceversa
				if (key >= 65 && key <= 90)
				{
					cadena = cadena.toLowerCase();
				}
				else
				{
					cadena = cadena.toUpperCase();
				}
				bFound = SearchAndPoint(objTB, objDiv, cadena, true);
				if (!bFound)
				{
					bFound = SearchAndPoint(objTB, objDiv, cadena, false);
				}
			}
		}
	}
}
function SearchAndPoint(objTB, objDiv, cadena, bSearchAfterCeldaId) {
	var bFound = false;
	var bInitSearch = !bSearchAfterCeldaId;
	for (var i=0;i<objTB.rows.length;i++)
	{
		var oTRO = objTB.rows[i];
		var sProjectValue = oTRO.getAttribute('id');
		if (bSearchAfterCeldaId && !bInitSearch)
		{
			if (sProjectValue == col_active)
			{
				bInitSearch = true;
			}
			continue;
		}
		if (sProjectValue != undefined && sProjectValue.substring(0,1) == cadena)
		{
			objDiv.scrollTop = oTRO.offsetTop;
			set_color(oTRO);
			bFound = true;
			break;
		}
	}
	return bFound;
}
function initializeGraphTrend()
{
	// recorrer cada tabla del escenario
	if (typeof(jQuery) != 'undefined')
	{
		jQuery.support.cors = true;
	}
	else
	{
		if (bExportDash) { returnExportDash(); }

		return;
	}

	if (IsEAS)
	{
		if (bGraphEASVisible) { return; }

		if (bTBHaveGraphTrend || bTBHaveGraphGauge  || bTBHaveGraphBullet)
		{
			var aChartImgName = new Array();

			var objChart = GetElementWithId("CHART_TD1");
			if (objChart)
			{
				var sChartImgName = objChart.getAttribute("filename");
				if (sChartImgName)
				{
					if (bTBHaveGraphTrend)  aChartImgName.push(sChartImgName+'_trend');
					if (bTBHaveGraphGauge)  aChartImgName.push(sChartImgName+'_gauge');
					if (bTBHaveGraphBullet) aChartImgName.push(sChartImgName+'_bullet');

					if (bWaitAjax) { return; }

					bWaitAjax = true;

					$.ajax({
						cache: false,
						crossDomain : false,
						type : 'GET',
						url : jsDir+'createimagecharttrend.php',
						dataType: "jsonp",
						jsonp : "jsonpCallback",
						jsonpCallback: "receiveGraphTrend",

						data: {
							createImageTrend: null,
							filenameimg: aChartImgName.join('_AWSep_')
						},
						error : function(XMLHttpRequest, textStatus, errorThrown)
						{
							//alert('CONNECTION ERROR:' + textStatus + ' ' + errorThrown);
							bWaitAjax = false;
						}
					});
				}
			}
		}
	}
	else
	{
		var aChartImgName = new Array();
		var sTableId;
		for (sTableId in aMetaTD)
		{
			var bHaveGraphTrend = (aMetaTD[sTableId][63] != undefined) ? aMetaTD[sTableId][63] : 0;
			var bHaveGraphGauge = (aMetaTD[sTableId][67] != undefined) ? aMetaTD[sTableId][67] : 0;
			var bHaveGraphBullet= (aMetaTD[sTableId][70] != undefined) ? aMetaTD[sTableId][70] : 0;

			// si tiene grafica de tendencia
			if (bHaveGraphTrend || bHaveGraphGauge || bHaveGraphBullet)
			{
				// obtener nombre de imagen
				var objChart = GetElementWithId("CHART_"+sTableId);
				if (objChart)
				{
					var sChartImgName = objChart.getAttribute("filename");
					if (sChartImgName)
					{
						if (bHaveGraphTrend)  aChartImgName.push(sChartImgName+'_trend');
						if (bHaveGraphGauge)  aChartImgName.push(sChartImgName+'_gauge');
						if (bHaveGraphBullet) aChartImgName.push(sChartImgName+'_bullet');
					}
				}

				// reemplazar dimension
				var sDimsChange = (aMetaTD[sTableId][68] != undefined) ? aMetaTD[sTableId][68] : '';
				if (sDimsChange != '')
				{
					var aDimsChange = sDimsChange.split(',');
					var nTop = aDimsChange.length;
					for (var i=0; i<nTop; i++)
					{
						if (aDimsChange[i] != '')
						{
							// obtener nombre de imagen
							var objChart = GetElementWithId("CHART_"+sTableId+'_'+aDimsChange[i]);
							myLogD("CHART_"+sTableId+'_'+aDimsChange[i]);
							if (objChart)
							{
								var sChartImgName = objChart.getAttribute("filename");
								if (sChartImgName)
								{
									if (bHaveGraphTrend)
									{
										aChartImgName.push(sChartImgName+'_trend');
									}

									if (bHaveGraphGauge)
									{
										aChartImgName.push(sChartImgName+'_gauge');
									}

									if (bHaveGraphBullet)
									{
										aChartImgName.push(sChartImgName+'_bullet');
									}
								}
							}
						}
					}
				}

				// reemplazar indicador
				var sIndsChange = (aMetaTD[sTableId][85] != undefined) ? aMetaTD[sTableId][85] : '';
				if (sIndsChange != '')
				{
					var aIndsChange = sIndsChange.split(',');
					var nTop = aIndsChange.length;
					for (var i=0; i<nTop; i++)
					{
						if (aIndsChange[i] != '')
						{
							// obtener nombre de imagen
							var objChart = GetElementWithId("CHART_"+sTableId+'_'+aIndsChange[i]);
							myLogD("CHART_"+sTableId+'_'+aIndsChange[i]);
							if (objChart)
							{
								var sChartImgName = objChart.getAttribute("filename");
								if (sChartImgName)
								{
									if (bHaveGraphTrend)
									{
										aChartImgName.push(sChartImgName+'_trend');
									}

									if (bHaveGraphGauge)
									{
										aChartImgName.push(sChartImgName+'_gauge');
									}

									if (bHaveGraphBullet)
									{
										aChartImgName.push(sChartImgName+'_bullet');
									}
								}
							}
						}
					}
				}
			}
		}

		if (aChartImgName.length > 0)
		{
			if (bWaitAjax)
			{
				if (bExportDash) { returnExportDash(); }
				return;
			}

			myLogD("Calling createimagecharttrend for " + aChartImgName.join(','));
			bWaitAjax = true;
			$.ajax({
				cache: false,
				crossDomain : false,
				type : 'GET',
				url : jsDir+'createimagecharttrend.php',
				dataType: "jsonp",
				jsonp : "jsonpCallback",
				jsonpCallback: "receiveGraphTrend",

				data: {
					createImageTrend: null,
					filenameimg: aChartImgName.join('_AWSep_')
				},
				error : function(XMLHttpRequest, textStatus, errorThrown)
				{
					//alert('CONNECTION ERROR:' + textStatus + ' ' + errorThrown);
					if (bExportDash) { returnExportDash(); }
					bWaitAjax = false;
				}
			});
		}
		else if (bExportDash) { returnExportDash(); }
	}
}
function receiveGraphTrend(data)
{
	bWaitAjax = false;
	setTimeout("loadGraphTrend()", 1);
}
function loadGraphTrend()
{
	var nPivotsLocal = null;

	if (IsEAS)
	{
		var sTableId;
		sTableId = '0';
		aTDColsPivsKeys = new Array();
		nPivotsLocal = nPivots;

		if (nPivotsLocal > 0)
		{
			SetPivColValues(0, aDimsPivsK.length, '', '_AWTab_', aDimsPivsK, aTDColsPivsKeys);
		}

		for (nIdCol in arrColnMask)
		{
			var nMaskCol = arrColnMask[nIdCol];

			if (nMaskCol == 35 || nMaskCol == 37 || nMaskCol == 38)	// NFM_GraphTrend, NFM_Gauge,  NFM_Bullet
			{
				var sTypeMaskName = '_trend_ColC'+nIdCol;
				if (nMaskCol == 37)	sTypeMaskName = '_gauge';
				if (nMaskCol == 38)	sTypeMaskName = '_bullet_ColC'+nIdCol;

				var sHeigth = '39px';
				var sWidth  = '115px';
				if (nMaskCol == 37 || nMaskCol == 38)
				{
					sHeigth = 'auto';
					sWidth  = 'auto';
				}

				// obtener el tbody de la Tabla
				var objTBody = GetElementWithId('BT'+sTableId);
				if (objTBody)
				{
					// obtener nombre de imagen
					var objChart = GetElementWithId("CHART_TD1");
					if (objChart)
					{
						var bDrawTotal = true;
						var attrDrawTotal = objChart.getAttribute("drawtotal");
						if (attrDrawTotal)
						{
							bDrawTotal = (attrDrawTotal == '1');
						}

						var sChartImgName = objChart.getAttribute("filename");
						if (sChartImgName)
						{
							// recorrer todas las filas para solicitar la imagen
							var nTopRows = objTBody.rows.length;
							for (var j=0; j<nTopRows; j++)
							{
						        var objTR = objTBody.rows[j];
						        var trNoF   = objTR.getAttribute('trNoF');

								if (trNoF)
								{
									if (!bDrawTotal)
							        {
							        	var bIsRowTotal = objTR.getAttribute('bTotal');
							        	if (bIsRowTotal)
							        	{
							        		if (bIsRowTotal == '1')
							        		{
							        			continue;
							        		}
							        	}
							        }

							        if (nPivotsLocal > 0)
									{
										var nTopY = aTDColsPivsKeys.length;
										for (var idy=0; idy<nTopY; idy++)
										{
											var sColPivId = nIdCol+'_AWTab_'+aTDColsPivsKeys[idy];
											var objImg = GetElementWithId('_R'+trNoF+'_C'+sColPivId);

											if (!objImg)
											{
												sColPivId = nIdCol+'_AWTab_'+aTDColsPivsKeys[idy]+'_AWTab_';
												objImg = GetElementWithId('_R'+trNoF+'_C'+sColPivId);
											}

											if (objImg)
											{
												myLogD('_R'+trNoF+'_C'+sColPivId+' found');
												DisableOnClick(objImg.parentElement, sTypeMaskName);
												objImg.style.height = sHeigth;
												objImg.style.width  = sWidth;
												objImg.src = repository+'/temp/'+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+sColPivId+'.png';
												myLogD(repository+'/temp/'+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+sColPivId+'.png cargada en Tabla');
											}
											else
											{
												myLogD('_R'+trNoF+'_C'+sColPivId+' NOT found');
											}
										}
									}
									else
									{
										var objImg = GetElementWithId('_R'+trNoF+'_C'+nIdCol);
										if (objImg)
										{
											DisableOnClick(objImg.parentElement, sTypeMaskName);
											objImg.style.height = sHeigth;
											objImg.style.width  = sWidth;
											objImg.src = repository+'/temp/'+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+nIdCol+'.png';
										}
									}
								}
							}
						}
					}
				}
			}
		}
		//alert('end load images');
	}
	else
	{
		var sIniName = 'temp/';
		if (jsDir == '')
		{
			sIniName = repository+'/temp/';
		}
		else if (bExportDash && jsDir == '../../../')
		{
			sIniName = '';
		}

		// recorrer cada tabla del escenario
		var sTableId;
		for (sTableId in aMetaTD)
		{
			var bHaveGraphTrend = (aMetaTD[sTableId][63] != undefined) ? aMetaTD[sTableId][63] : 0;
			var bHaveGraphGauge = (aMetaTD[sTableId][67] != undefined) ? aMetaTD[sTableId][67] : 0;
			var bHaveGraphBullet= (aMetaTD[sTableId][70] != undefined) ? aMetaTD[sTableId][70] : 0;

			// si tiene grafica de tendencia
			if (bHaveGraphTrend || bHaveGraphGauge || bHaveGraphBullet)
			{
				myLogD('ColsIds='+aMetaTD[sTableId][37]);

				nPivotsLocal = (aMetaTD[sTableId] != undefined && aMetaTD[sTableId][43] != undefined) ? aMetaTD[sTableId][43] : 0;

				if (nPivotsLocal > 0)
				{
					if (aTDColsPivsKeys[sTableId] == undefined)
					{
						aTDColsPivsKeys[sTableId] = new Array();
						if (arrDimsPivsK[sTableId] != undefined)
						{
							SetPivColValues(0, arrDimsPivsK[sTableId].length, '', '_AWTab_', arrDimsPivsK[sTableId], aTDColsPivsKeys[sTableId]);
						}
					}
				}

				var arrColsIds = aMetaTD[sTableId][37].slice(0);

				var nTopCols = arrColsIds.length;

				// recorrer todas las columnas
				for (var i=0; i<nTopCols; i++)
				{
					var nIdCol = arrColsIds[i];
					myLogD('nIdCol='+nIdCol);
					var aCol = (aMetaTD[sTableId] != undefined && aMetaTD[sTableId]['C'+nIdCol] != undefined) ? aMetaTD[sTableId]['C'+nIdCol] : null;

					if (aCol == null)
					{
						myLogD('This Col has no Meta');
						continue;
					}

					var nMaskCol = (aCol[5] != undefined && aCol[5] != null) ? aCol[5] : 0;
					// si es ver como grafica de tendencia
					if (nMaskCol == 35 || nMaskCol == 37 || nMaskCol == 38)	// NFM_GraphTrend, NFM_Gauge,  NFM_Bullet
					{
						myLogD('nIdCol='+nIdCol+', MaskCol is one of (NFM_GraphTrend,NFM_Gauge,NFM_Bullet)');
						var sTypeMaskName = '_trend_ColC'+nIdCol;
						if (nMaskCol == 37)	sTypeMaskName = '_gauge';
						if (nMaskCol == 38)	sTypeMaskName = '_bullet_ColC'+nIdCol;

						var sHeigth = '39px';
						var sWidth  = '115px';
						if (nMaskCol == 37 || nMaskCol == 38)
						{
							sHeigth = 'auto';
							sWidth  = 'auto';
						}

						// obtener el tbody de la Tabla
						var objTBody = GetElementWithId('BT'+sTableId);
						if (objTBody)
						{
							// obtener nombre de imagen
							var objChart = GetElementWithId("CHART_"+sTableId);
							if (objChart)
							{
								var bDrawTotal = true;
								var attrDrawTotal = objChart.getAttribute("drawtotal");
								if (attrDrawTotal)
								{
									bDrawTotal = (attrDrawTotal == '1');
								}

								var sChartImgName = objChart.getAttribute("filename");
								if (sChartImgName)
								{
									// recorrer todas las filas para solicitar la imagen
									var nTopRows = objTBody.rows.length;
									for (var j=0; j<nTopRows; j++)
									{
								        var objTR = objTBody.rows[j];
								        var trNoF   = objTR.getAttribute('trNoF');
										if (trNoF)
										{
											if (!bDrawTotal)
									        {
									        	var bIsRowTotal = objTR.getAttribute('bTotal');
									        	if (bIsRowTotal)
									        	{
									        		if (bIsRowTotal == '1')
									        		{
									        			continue;
									        		}
									        	}
									        }

									        if (nPivotsLocal > 0)
											{
												var nTopY = aTDColsPivsKeys[sTableId].length;
												for (var idy=0; idy<nTopY; idy++)
												{
													var sColPivId = nIdCol+'_AWTab_'+aTDColsPivsKeys[sTableId][idy];
													var objImg = GetElementWithId(sTableId+'_R'+trNoF+'_C'+sColPivId);

													if (!objImg)
													{
														sColPivId = nIdCol+'_AWTab_'+aTDColsPivsKeys[sTableId][idy]+'_AWTab_';
														objImg = GetElementWithId(sTableId+'_R'+trNoF+'_C'+sColPivId);
													}

													if (objImg)
													{
														myLogD(sTableId+'_R'+trNoF+'_C'+sColPivId+' found');
														DisableOnClick(objImg.parentElement, sTypeMaskName);
														objImg.style.height = sHeigth;
														objImg.style.width  = sWidth;
														objImg.src = sIniName+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+sColPivId+'.png';
														myLogD(sIniName+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+sColPivId+'.png cargada en Tabla');
													}
													else
													{
														myLogD(sTableId+'_R'+trNoF+'_C'+sColPivId+' NOT found');
													}
												}
											}
											else
											{
												var objImg = GetElementWithId(sTableId+'_R'+trNoF+'_C'+nIdCol);
												if (objImg)
												{
													myLogD(sTableId+'_R'+trNoF+'_C'+nIdCol+' found');
													DisableOnClick(objImg.parentElement, sTypeMaskName);
													objImg.style.height = sHeigth;
													objImg.style.width  = sWidth;
													objImg.src = sIniName+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+nIdCol+'.png';
													myLogD(sIniName+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+nIdCol+'.png cargada en Tabla');
												}
												else
												{
													myLogD(sTableId+'_R'+trNoF+'_C'+nIdCol+' NOT found');
												}
											}
										}
									}
								}
							}
						}
						else
						{
							myLogD('Table has not BT'+sTableId);
						}

						// reemplazar dimension
						var sDimsChange = (aMetaTD[sTableId][68] != undefined) ? aMetaTD[sTableId][68] : '';
						if (sDimsChange != '')
						{
							var aDimsChange = sDimsChange.split(',');
							var nTop = aDimsChange.length;
							for (var iaux=0; iaux<nTop; iaux++)
							{
								if (aDimsChange[iaux] != '')
								{
									// obtener el tbody de la Tabla
									var objTBody = GetElementWithId('BT'+sTableId+'_'+aDimsChange[iaux]);
									if (objTBody)
									{
										// obtener nombre de imagen
										var objChart = GetElementWithId("CHART_"+sTableId+'_'+aDimsChange[iaux]);
										myLogD('Looking for '+"CHART_"+sTableId+'_'+aDimsChange[iaux]);
										if (objChart)
										{
											var bDrawTotal = true;
											var attrDrawTotal = objChart.getAttribute("drawtotal");
											if (attrDrawTotal)
											{
												bDrawTotal = (attrDrawTotal == '1');
											}

											var sChartImgName = objChart.getAttribute("filename");
											if (sChartImgName)
											{
												// recorrer todas las filas para solicitar la imagen
												var nTopRows = objTBody.rows.length;
												for (var j=0; j<nTopRows; j++)
												{
											        var objTR = objTBody.rows[j];
											        var trNoF   = objTR.getAttribute('trNoF');

													if (trNoF)
													{
														if (!bDrawTotal)
												        {
												        	var bIsRowTotal = objTR.getAttribute('bTotal');
												        	if (bIsRowTotal)
												        	{
												        		if (bIsRowTotal == '1')
												        		{
												        			continue;
												        		}
												        	}
												        }

														var objImg = GetElementWithId(sTableId+'_'+aDimsChange[iaux]+'_R'+trNoF+'_C'+nIdCol);
														myLogD('Looking for '+sTableId+'_'+aDimsChange[iaux]+'_R'+trNoF+'_C'+nIdCol);
														if (objImg)
														{
															myLogD('Found '+sTableId+'_'+aDimsChange[iaux]+'_R'+trNoF+'_C'+nIdCol);
															DisableOnClick(objImg.parentElement, sTypeMaskName);
															objImg.style.height = sHeigth;
															objImg.style.width  = sWidth;
															objImg.src = sIniName+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+nIdCol+'.png';
															myLogD(sIniName+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+nIdCol+'.png cargada en Tabla');
														}
														else
														{
															myLogD(sTableId+'_'+aDimsChange[iaux]+'_R'+trNoF+'_C'+nIdCol+' NOT found');
														}
													}
												}
											}
										}
									}
									else
									{
										myLogD('Table has not BT'+sTableId+'_'+aDimsChange[iaux]);
									}
								}
							}
						}

						// reemplazar indicador
						var sIndsChange = (aMetaTD[sTableId][85] != undefined) ? aMetaTD[sTableId][85] : '';
						if (sIndsChange != '')
						{
							var aIndsChange = sIndsChange.split(',');
							var nTop = aIndsChange.length;
							for (var iaux=0; iaux<nTop; iaux++)
							{
								if (aIndsChange[iaux] != '')
								{
									// obtener el tbody de la Tabla
									var objTBody = GetElementWithId('BT'+sTableId+'_'+aIndsChange[iaux]);
									if (objTBody)
									{
										// obtener nombre de imagen
										var objChart = GetElementWithId("CHART_"+sTableId+'_'+aIndsChange[iaux]);
										myLogD('Looking for '+"CHART_"+sTableId+'_'+aIndsChange[iaux]);
										if (objChart)
										{
											var bDrawTotal = true;
											var attrDrawTotal = objChart.getAttribute("drawtotal");
											if (attrDrawTotal)
											{
												bDrawTotal = (attrDrawTotal == '1');
											}

											var sChartImgName = objChart.getAttribute("filename");
											if (sChartImgName)
											{
												// recorrer todas las filas para solicitar la imagen
												var nTopRows = objTBody.rows.length;
												for (var j=0; j<nTopRows; j++)
												{
											        var objTR = objTBody.rows[j];
											        var trNoF   = objTR.getAttribute('trNoF');

													if (trNoF)
													{
														if (!bDrawTotal)
												        {
												        	var bIsRowTotal = objTR.getAttribute('bTotal');
												        	if (bIsRowTotal)
												        	{
												        		if (bIsRowTotal == '1')
												        		{
												        			continue;
												        		}
												        	}
												        }

														var objImg = GetElementWithId(sTableId+'_'+aIndsChange[iaux]+'_R'+trNoF+'_C'+nIdCol);
														myLogD('Looking for '+sTableId+'_'+aIndsChange[iaux]+'_R'+trNoF+'_C'+nIdCol);
														if (objImg)
														{
															myLogD('Found '+sTableId+'_'+aIndsChange[iaux]+'_R'+trNoF+'_C'+nIdCol);
															DisableOnClick(objImg.parentElement, sTypeMaskName);
															objImg.style.height = sHeigth;
															objImg.style.width  = sWidth;
															objImg.src = sIniName+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+nIdCol+'.png';
															myLogD(sIniName+sChartImgName+sTypeMaskName+'_R'+trNoF+'C'+nIdCol+'.png cargada en Tabla');
														}
														else
														{
															myLogD(sTableId+'_'+aIndsChange[iaux]+'_R'+trNoF+'_C'+nIdCol+' NOT found');
														}
													}
												}
											}
										}
									}
									else
									{
										myLogD('Table has not BT'+sTableId+'_'+aIndsChange[iaux]);
									}
								}
							}
						}
					}
				}
			}
		}

		if (bExportDash) { returnExportDash(); }
	}
}

function DisableOnClick(objComp, sTypeMaskName)
{
	if(sTypeMaskName.indexOf('_trend_')==-1)
	{
		objComp.removeAttribute('onclick');

		if (!IsThatVersion("MSIE 7."))
		{
			var sStyle=objComp.getAttribute('style');
			var regexp = new RegExp('pointer', 'gi');

			sStyle=sStyle.replace(regexp, 'default');
			objComp.setAttribute('style', sStyle);
		}
	}
}

function clearInputTxtF(sInputId, bFocus)
{
	var owhich = GetElementWithId(sInputId);
	if (owhich)
	{
		owhich.value = '';
		if (bFocus)
		{
			owhich.blur();
		}
		else
		{
			textBlur(owhich)
		}
	}
}
function setEmptyInputValue(sInputId)
{
	var sColorFilter = '#000000';
	var sColorEmpty  = '#999';
	var objInput = GetElementWithId(sInputId);
	if (objInput)
	{
		var sAttrEmpty = objInput.getAttribute('emptytxt');
		if (sAttrEmpty)
		{
			if (objInput.value != '' && objInput.value != ML[sAttrEmpty])
			{
				objInput.style.color = sColorFilter;
			}
			else
			{
				if (objInput.value == '')
				{
					objInput.value = ML[sAttrEmpty];
				}
				objInput.style.color = sColorEmpty;
			}
		}
	}
}
function textClicked(owhich)
{
	if (owhich.style.fontStyle == "italic")
	{
		owhich.style.color = "#000"
		owhich.style.fontStyle = "normal"
		owhich.value = ""
		owhich.select()
	}
}
function textBlur(owhich)
{
	if (Trim(owhich.value).length == 0)
	{
		owhich.style.color = "#999"
		owhich.style.fontStyle = "italic"
		owhich.value = '';
		setEmptyInputValue(owhich.id);
	}
}
function FilterList(anEvent, owhich)
{
	anEvent = (window.event) ? window.event : anEvent;
	var keyCode = (anEvent.keyCode == 0) ? anEvent.which : anEvent.keyCode;
	var bGoFuncSet = false;

	if (keyCode == 27)	// ESC
	{
		clearInputTxtF(owhich.id, true);
		bGoFuncSet = true;
	}
	else if (keyCode != 9 && keyCode != 38 && keyCode != 40 && keyCode != 13 && 	// TAB, UP, DOWN, ENTER
			 keyCode != 16 && keyCode != 17 && keyCode != 18 && keyCode != 20 &&
			 keyCode != 33 && keyCode != 34 && keyCode != 37 && keyCode != 39 &&
			 keyCode != 91 && keyCode != 93)
	{
		bGoFuncSet = true;
	}

	if (bGoFuncSet)
	{
		var qFuncSet = owhich.getAttribute('qFuncSet');
		if (qFuncSet)
		{
			eval(qFuncSet+'("'+owhich.id+'");');
		}
	}
}
function AddVisibility(sTDId, nColAction, sUniKey)
{
	if (bLoading == 0) { return; }

	var Name = '';
	var Type = '';
	var Formula = '';
	var Format = '';
	var DefImg = 'images/transparent.gif';
	var Negrita = false;
	var Subrayado = false;
	var Cursiva = false;
	var ColorLetra = '#000000';
	var ColorFondo = '#ffffff';

	if (sUniKey != '')
	{
		var sVisibilityMD = (aMetaTD[sTDId] != undefined && aMetaTD[sTDId]['VIS'] != undefined && aMetaTD[sTDId]['VIS'][sUniKey] != undefined) ?
											   aMetaTD[sTDId]['VIS'][sUniKey] : null;

		if (sVisibilityMD && sVisibilityMD != '')
		{
			var aVisMD = sVisibilityMD.split('_AWSep_');

			Type = aVisMD[0];
			Name = aVisMD[1];
			Formula = aVisMD[2];
			Format = aVisMD[3];
		}
	}

	var labels = [ML[74], ML[75], ML[76], ML[77], ML[78], ML[79],ML[69],ML[70],
    				Name, Type, Formula, Format, ML[361], ML[427], ML[426],
    				ML[290], ML[428], ML[291], ML[359], ML[292], ML[362] + '->'+ML[361],
    				ML[101], ML[683], ML[408], ML[85]];

    var localOperators = [[]];
    if (aTDOperators[sTDId] != undefined)
    {
    	copy(aTDOperators[sTDId][0], localOperators[0]);
	}

	var arrColsIds = aMetaTD[sTDId][37];
	var nTop = arrColsIds.length;
	for (var i=0; i<nTop; i++)
	{
		var colID = arrColsIds[i];
		var sName = '';
		var nType = 0;

		if (aMetaTD[sTDId]['C'+colID] != undefined)
		{
			if (aMetaTD[sTDId]['C'+colID][2] != undefined) {
				sName = aMetaTD[sTDId]['C'+colID][2];
			}
			if (aMetaTD[sTDId]['C'+colID][3] != undefined) {
				nType = aMetaTD[sTDId]['C'+colID][3];
			}
		}

		if (nType == 6)
		{
			localOperators[0].push('{('+colID+') '+sName+'}');
		}
	}

	if (sWhatOpt=='webdesigner' && aCompsInfo['SYNC']['Variables'] != undefined) {
		ccVariables = aCompsInfo['SYNC']['Variables'];
	}

    if (HaveModalWindow())
    {
	    showModalDialog
	        (jsDir+'calculatedcolumn.htm',
	        [ML[125], localOperators, ccVariables, labels, window, Name, Type, Formula, Format,
	         true, DefImg, Negrita, Subrayado, Cursiva, ColorLetra, ColorFondo, lang, 'addvisibility'],
	        "dialogWidth: 508px; dialogHeight: 425px; resizable: yes; center: yes; help: no; status: no; scroll:no");
	    if (window.returnValue != null)
	    {
	    	if (window.returnValue == 'Remove' && sUniKey == '')
	    	{
	    		window.returnValue = null;
	    		return;
	    	}
			EASParams("addVisibility", nColAction + "_AWSep_" + sUniKey + "_AWSep_" + window.returnValue);
			window.returnValue = null;
	    }
    }
    else
    {
    	var nTopElems = localOperators.length;
    	var aCCOperators = new Array();
    	for (iElems=0; iElems<nTopElems; iElems++){
			if (localOperators[iElems] != null) {
    			aCCOperators.push(localOperators[iElems].join('_ELEMS_AWSep_'));
			}
    	}

    	gDialogArguments = ML[424]+'Æ'+aCCOperators.join('_COLS_AWSep_')+'Æ'+ccVariables.join('_ELEMS_AWSep_')+'Æ'+labels.join('_LAB_AWSep_');
    	gDialogArguments = gDialogArguments+'ÆnullÆ'+Name+'Æ'+Type+'Æ'+Formula+'Æ'+Format+'ÆtrueÆ'+DefImg+'Æ'+Negrita+'Æ'+Subrayado+'Æ'+Cursiva;
    	gDialogArguments = gDialogArguments+'Æ'+ColorLetra+'Æ'+ColorFondo+'Æ'+lang;
		gDialogArguments = gDialogArguments+'°New';
		showPopWin('calculatedcolumn.htm', 508, 425, null);
    }
}
function getCalcFormula(Formula, objTR)
{
	var sFormula = Formula;
	sFormula = ' '+sFormula+' ';
	var x = sFormula.indexOf('{(');

	while (x != -1)
	{
		var Y = sFormula.indexOf(')',(x + 2));
		if (Y != -1)
		{
			var nConsecutivo = sFormula.substring((x + 2),Y);
			var sValue = 0;

			if (nConsecutivo.indexOf('R') == -1)
			{
				sValue = $(objTR).find('td[colid="'+nConsecutivo+'"]').attr('keyvalue');
				if (sValue == undefined)
				{
					sValue = 0;
				}
			}

		}

		Y = sFormula.indexOf('}',(x + 2));
		sFormula = sFormula.substring(0, x) + sValue + sFormula.substring((Y+1));
		x = sFormula.indexOf('{(');
	}

	return sFormula;
}
function hideProgressPageMobile()
{
	var oMMenu = GetElementWithId('span_load_mobile');
	if (oMMenu)
	{
		oMMenu.className = '';
	}

	oMMenu = GetElementWithId('div_load_mobile');
	if (oMMenu)
	{
		oMMenu.style.display = 'none';
	}
}
function showProgressPageMobile()
{
	var oMMenu = GetElementWithId('span_load_mobile');
	if (oMMenu)
	{
		oMMenu.className = 'load-ui-icon load-ui-icon-loading spin';
	}

	oMMenu = GetElementWithId('div_load_mobile');
	if (oMMenu)
	{
		oMMenu.style.display = 'block';
	}
}
function SetPivColValues(nLevel, nTopPivot, sParentName, sSep, afDimsPivs, aTDColsPivs)
{
	if (nLevel < nTopPivot)
	{
		var nTopElems = afDimsPivs[nLevel].length;
		for (var nElem=0; nElem<nTopElems; nElem++) {
			var sParamName = sParentName;
			if (sParamName != '') { sParamName += sSep; }
			sParamName += afDimsPivs[nLevel][nElem];
			var sColName = SetPivColValues(nLevel+1, nTopPivot, sParamName, sSep, afDimsPivs, aTDColsPivs);
			if ((nLevel+1) == nTopPivot) {
				aTDColsPivs.push(sColName);
			}
		}
	}
	else
	{
		return sParentName;
	}
}
function InitializeExportDash()
{
	IsEAS = 0;
	bExportDash = true;
	initializeTableFilterMD(null);

	if (bExportDash && jsDir == '../../../')
	{
		loadGraphTrend();
	}
	else
	{
		initializeGraphTrend();
	}
}
function returnExportDash()
{
	if (window.parent)
	{
		if (typeof(window.parent.HTMLString) != 'undefined') window.parent.HTMLString = document.documentElement.innerHTML;
		if (typeof(window.parent.SavePDFStage) != 'undefined') window.parent.SavePDFStage();
	}
}
function genImageSlider(sTableId, rowIni, rowEnd, bSHTable)
{
	// obtener nombre de imagen
	var objChart = GetElementWithId("CHART_"+sTableId);
	if (objChart)
	{
		var sChartImgName = objChart.getAttribute("filename");
		if (sChartImgName)
		{
			sChartMetaData = '';
			if (false && aChartsTablesRowsGenerated[sTableId] != undefined && aChartsTablesRowsGenerated[sTableId][rowIni+'-'+rowEnd])
			{
				var sImgRow    = aChartsTablesRowsGenerated[sTableId][rowIni+'-'+rowEnd];
				//Tomar Metadata de la grafica hotpoints y onclicks
				sChartMetaData = aChartsTablesRowsGeneratedMeta[sTableId][rowIni+'-'+rowEnd];
			}
			else
			{
				var sImgRow   = '../../getimagecharttablerow.php?filenameimg='+sChartImgName+'&rowIni='+rowIni+'&rowEnd='+rowEnd+'&bSlider=1';
			}

			//Asignar la imagen
			objChart.src = sImgRow;

			//Pedir los HotPoints solo si no se obtuvieron del array
			if (sChartMetaData == '')
			{   //Darle time pa q genere la imagen
				timerChartUpDateHotPoints=setTimeout("upDateHotPoints('"+sTableId+"','"+rowIni+'-'+rowEnd+"', 'CHART_')", 500);
				sChartMetaData = 'temp/'+sChartImgName+'_slider_RI'+rowIni+'_RE'+rowEnd+'.inf';
			}
			else
			{	//Actualizar el Metadata de la grafica de que se tomo del array
				var objChartMeta = GetElementWithId("CHART_"+sTableId+"_metadata");
				if(objChartMeta) objChartMeta.innerHTML = sChartMetaData;
				sChartMetaData = '';
			}

			if (aChartsTablesRowsGenerated[sTableId] == undefined)
			{
				aChartsTablesRowsGenerated[sTableId] = new Array();
				aChartsTablesRowsGeneratedMeta[sTableId] = new Array();
			}

			aChartsTablesRowsGenerated[sTableId][rowIni+'-'+rowEnd] = 'temp/'+sChartImgName+'_slider_RI'+rowIni+'_RE'+rowEnd+'.png';
		}
	}

	if (bSHTable)
	{
		SHTableSlider(sTableId, rowIni, rowEnd);
	}
}

function getDashInfo(esc_id){
	var sRetName = new Array();
	var sNameEsc = ''; var sNameGpoEsc = '';
	var i = 0; var top = 0;
	var bFoundStage = false;
	top = aEscKeys.length;
	for (i=0;i<top;i++){
		if (aEscGroupNames[i] != ''){
			sNameGpoEsc = aEscGroupNames[i];
		}
		if (aEscKeys[i] == esc_id){
			sNameEsc = aEscNames[i];
			bFoundStage = true;
			break;
		}
	}
	if (!bFoundStage) {
		var topRol = aUserGroups.length;
		for (i=0;i<topRol;i++){
			var iKeyRol = aUserGroups[i];
			var topEscByRol = aEscKeysGP[iKeyRol].length;
			for (var j=0;j<topEscByRol;j++){
				if (aEscGroupNamesGP[iKeyRol][j] != ''){
					sNameGpoEsc = aEscGroupNamesGP[iKeyRol][j];
				}
				if (aEscKeysGP[iKeyRol][j] == esc_id){
					sNameEsc = aEscNamesGP[iKeyRol][j];
					bFoundStage = true;
					break;
				}
			}
			if (bFoundStage) {
				break;
			}
		}
	}
	sRetName[0] = sNameGpoEsc;
	sRetName[1] = sNameEsc;
	return sRetName;
}

function AddChildURL(sURL)
{
	return sURL;

	/*
	sURL += (sURL.indexOf('?') != -1) ? '&' : '?';
	sURL += '_child=:child:';

	if (bSaveInLocal)
	{
		sURL = addParamGetChildUrl(sURL, "EASMD");
	}

	return sURL;
	*/

}

function addParamGetChildUrl(sURL, key)
{
	var objPGet = GetElementWithId(key);
	if (objPGet)
	{
		sURL += '&'+key+'='+encodeURIComponent(objPGet.value);
	}
	return sURL;
}

function EditColFormat(nColId)
{
	if (bLoading == 0) { return; }

	var nTDId = '';
	if (IsEAS) {
		var sColFormat = (arrColFormat[nColId] != undefined) ? arrColFormat[nColId] : '';
	}
	else {
		nTDId = nEscAction;
		var sColFormat = (aMetaTD[nTDId] != undefined && aMetaTD[nTDId]['C'+nColId] != undefined &&
				   		  aMetaTD[nTDId]['C'+nColId][29] != undefined) ? aMetaTD[nTDId]['C'+nColId][29] : '';
	}

	var regexp = new RegExp(',',"gi");
	if (sColFormat != '')
	{
		sColFormat = sColFormat.replace(regexp,"_AWSCOMA_");
	}

	sauxPath = GetPath();

	var labels = ML[78];
    var types  = 'INP';
    var values = sColFormat;
    var valid  = 'alfaEmpty';
    var InValid= '';
    var icon   = 'format.gif';
    var closePath = 'images/sm_close.gif';

	openWindowDialog(sauxPath+'ask_data.php?_child=:child:', [ML[78], window, labels, types, values, valid, InValid, icon], [nTDId, nColAction],
			 		 400, 180, SetEditColFormatModalValues, 'SetEditColFormatModalValues');
}

function SetEditColFormatModalValues(sNewValues, aArgCont)
{
    if (sNewValues != undefined)
    {
    	var nTDId  = aArgCont[0];
    	var nColId = aArgCont[1];

    	if (IsEAS)
    	{
			var sColFormat = (arrColFormat[nColId] != undefined) ? arrColFormat[nColId] : '';
    	}
    	else
    	{
	    	var sColFormat = (aMetaTD[nTDId] != undefined && aMetaTD[nTDId]['C'+nColId] != undefined &&
					   		  aMetaTD[nTDId]['C'+nColId][29] != undefined) ? aMetaTD[nTDId]['C'+nColId][29] : '';
    	}

		if (sColFormat != sNewValues)
		{
	        EASParams('SetColFormat', nColId + '_AWSep_' + sNewValues);
	        window.returnValue = null;
		}
    }
}

function str_repeat(x,n)
{
    var y = '';
    while(true)
    {
        if(n & 1) y += x;
        n >>= 1;
        if(n) x += x; else break;
    }
    return y;
}

function uasort(inputArr, sorter) {
  // http://kevin.vanzonneveld.net
  // %        note 1: This function deviates from PHP in returning a copy of the array instead
  // %        note 1: of acting by reference and returning true; this was necessary because
  // %        note 1: IE does not allow deleting and re-adding of properties without caching
  // %        note 1: of property position; you can set the ini of "phpjs.strictForIn" to true to
  // %        note 1: get the PHP behavior, but use this only if you are in an environment
  // %        note 1: such as Firefox extensions where for-in iteration order is fixed and true
  // %        note 1: property deletion is supported. Note that we intend to implement the PHP
  // %        note 1: behavior by default if IE ever does allow it; only gives shallow copy since
  // %        note 1: is by reference in PHP anyways
  // *     example 1: fruits = {d: 'lemon', a: 'orange', b: 'banana', c: 'apple'};
  // *     example 1: fruits = uasort(fruits, function (a, b) { if (a > b) {return 1;}if (a < b) {return -1;} return 0;});
  // *     results 1: fruits == {c: 'apple', b: 'banana', d: 'lemon', a: 'orange'}
  var valArr = [],
    tempKeyVal, tempValue, ret, k = '',
    i = 0,
    strictForIn = false,
    populateArr = {};

  if (typeof sorter === 'string') {
    sorter = this[sorter];
  } else if (Object.prototype.toString.call(sorter) === '[object Array]') {
    sorter = this[sorter[0]][sorter[1]];
  }

  // BEGIN REDUNDANT
  this.php_js = this.php_js || {};
  this.php_js.ini = this.php_js.ini || {};
  // END REDUNDANT
  strictForIn = this.php_js.ini['phpjs.strictForIn'] && this.php_js.ini['phpjs.strictForIn'].local_value && this.php_js.ini['phpjs.strictForIn'].local_value !== 'off';
  populateArr = strictForIn ? inputArr : populateArr;


  for (k in inputArr) { // Get key and value arrays
    if (inputArr.hasOwnProperty(k)) {
      valArr.push([k, inputArr[k]]);
      if (strictForIn) {
        delete inputArr[k];
      }
    }
  }
  valArr.sort(function (a, b) {
    return sorter(a[1], b[1]);
  });

  for (i = 0; i < valArr.length; i++) { // Repopulate the old array
    populateArr[valArr[i][0]] = valArr[i][1];
  }

  //return strictForIn || populateArr;
  return strictForIn || valArr;
}
/************* MENSAJE MODAL *******************/
function ShowMsgModal(sId)
{
	var cubelocked = $('#cubelocked').html();
	if (cubelocked && cubelocked != '')
	{
		if (GetElementWithId(sId))
		{
			$('#'+sId).css('display', '');
			$('#'+sId+'_overlay').css('display', '');
		}
		else
		{
			var oComponent = document.createElement("div");
			oComponent.className = "ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable";
			oComponent.tabIndex  = -1;
			oComponent.setAttribute('role', "dialog");
			oComponent.setAttribute('aria-describedby', "dialog-message");
			oComponent.setAttribute('aria-labelledby', "ui-id-1");
			oComponent.style.position = "absolute";
			oComponent.style.height   = "auto";
			oComponent.style.width    = "300px";
			oComponent.style.top      = "84px";
			oComponent.style.left     = "140.5px";
			oComponent.style.display  = "block";
			oComponent.style.zIndex   = 101;
			oComponent.id   		  = sId;
			oComponent.innerHTML = '<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle">'+
								   '<span id="ui-id-1" class="ui-dialog-title">'+ML[969]+'...</span>'+
								   '<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" title="'+ML[67]+'" onclick="closeMsgModal(\''+sId+'\');">'+
								   '<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>'+
								   '<span class="ui-button-text">'+ML[67]+'</span></button></div>'+
								   '<div id="dialog-message" class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 57px; max-height: none; height: auto;">'+
								   '<p>'+
								   '<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>'+
								   ML[968]+'.'+
								   '</p>'+
								   '</div><div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><div class="ui-dialog-buttonset"><button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" onclick="closeMsgModal(\''+sId+'\');">'+
								   '<span class="ui-button-text">'+ML[69]+'</span></button></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-sw" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-ne" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-nw" style="z-index: 90;"></div></div>';

			if (document.body.appendChild)
			{
		  		document.body.appendChild(oComponent);
		  	}
		  	else if (document.documentElement.appendChild)
		  	{
		  		document.documentElement.appendChild(oComponent);
		  	}

		  	var oComponent = document.createElement("div");
			oComponent.className = "ui-widget-overlay ui-front";
			oComponent.id 		 = sId+'_overlay';

			if (document.body.appendChild)
			{
		  		document.body.appendChild(oComponent);
		  	}
		  	else if (document.documentElement.appendChild)
		  	{
		  		document.documentElement.appendChild(oComponent);
		  	}
		}

		var popup = $('#'+sId);
		popup.css({
		    'left': ($(window).width() / 2 - $(popup).width() / 2) + 'px',
		    'top': ($(window).height() / 2 - $(popup).height() / 2) + 'px'
		});
	}
}
function closeMsgModal(sId)
{
	$('#'+sId).css('display', 'none');
	$('#'+sId+'_overlay').css('display', 'none');
}
/************* FIN MENSAJE MODAL *******************/
function specialhtmltochars(sCadToConvert){
	sCadToConvert = sCadToConvert.replace(/</gi,"&lt;");
	sCadToConvert = sCadToConvert.replace(/>/gi,"&gt;");
	sCadToConvert = sCadToConvert.replace(/"/gi,'&quot;');
	return sCadToConvert;
}
function IsMobile(a)
{
	return (/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
}
