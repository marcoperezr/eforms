/*
	File: menu.js
	Version: 2007/01/15 11:34
	Copyright 2006,2007 Bitam
*/

var bitamMenus = new Array();
var bitamRootMenu = null;
var bitamInMenuItem = false;
var bitamIntervalId = null;
var bitamMenuTimeout = null;
var bitamMenuLeftMinus = 0;
var bitamXPos = 0;
var bitamYPos = 0;
var bitamCalculatedPos = false;
var bitamTypeClicked = '';
var bitamScrollTop = 0;

var bitamBuffer = null;
var bitamMenuZIndex = 100;
var bitamLastMenuOutId = new Array();
var bitamLastMenuOutIdUnique = new Array();
var bLoading = 1;

function popUpQuickMenu(anEvent, aFunction, valueArray, stringArray, x, y)
{
	if (bLoading==0){
		return;
	}
	if (bitamCalculatedPos) {
		SetBitamPosXY(anEvent);
	}
	var aMenu = quickMenu(aFunction, valueArray, stringArray);
	aMenu.event = anEvent;

	if (typeof(sLogginFrom) != 'undefined' && sLogginFrom == 'appDashboards')
	{
		aMenu.strClassName = 'AppDash';
	}
	aMenu.popUp(x,y);
}
function SetBitamPosXY(anEvent){
	var osrc = anEvent.srcElement || anEvent.target
	if (osrc.tagName == "IMG" &&
	   (osrc.id == "Graph" || osrc.id == "Image" || osrc.id == "Gauge" || osrc.id == "Line" || osrc.id == "PGBar"))
	{
		osrc = osrc.parentElement || osrc.parentNode
	}
	else
	{
		while(osrc.tagName!='DIV' && typeof(osrc.Rep_type)=="undefined" && osrc.tagName!="BODY")
		{
			osrc = osrc.parentElement || osrc.parentNode
		}
	}
	
	var oParentRect = osrc.getBoundingClientRect()
	var xLeft = anEvent.clientX - oParentRect.left
	var xTop = anEvent.clientY - oParentRect.top
	bitamXPos = xLeft;
	bitamYPos = xTop;
}
function quickMenu(aFunction, valueArray, stringArray)
{
	var aMenu = new BitamMenu();
	aMenu.action = aFunction;

	for (var i = 1; i < stringArray.length; i++)
	{
		var aValue = valueArray[i - 1];
		var aLabel = stringArray[i];
		var aImgURL = '';
		var aDisabled = false;

		if (typeof(aLabel) == 'string')
		{
			var realLabel = aLabel;
			var checked = false;

			if (realLabel.length > 0)
			{
				if (realLabel.charAt(0) == '*')
				{
					realLabel = realLabel.substring(1);
					checked = true;
				}

				if (realLabel == '---')
				{
					aMenu.appendSeparator();
					continue;
				}
				realLabel = realLabel.replace(/&#42/gi, "*");
			}
			aAuxLabel = realLabel.split('_AWSep_');

			if (aAuxLabel.length > 1) {
				realLabel = aAuxLabel[0];
				if (aAuxLabel[1] != '') {
					aImgURL = 'images/'+aAuxLabel[1];
				}
				if (aAuxLabel[2] != undefined && aAuxLabel[2] != null && aAuxLabel[2] != '') {
					aDisabled = (aAuxLabel[2]=='1' || aAuxLabel[2]=='true');
				}
			}
			aMenu.appendItem(realLabel, aValue, checked, aImgURL, aDisabled);
		}
		else
		{
			var subMenu = quickMenu(aFunction, aValue, aLabel);
			aMenu.appendMenu(aLabel[0], subMenu);
		}
	}
	return(aMenu);
}

function menu_checkMenuKey(anEvent)
{
	if (anEvent.keyCode == 27)
		if (bitamRootMenu != null)
			bitamRootMenu.close();
}

function menu_checkClick(anEvent)
{
	if (bitamRootMenu != null)
	{
		if (typeof(sLogginFrom) != 'undefined' && sLogginFrom == 'appDashboards')
		{
			if (bitamRootMenu.div.scrollTop != bitamScrollTop)
			{
				bitamScrollTop = bitamRootMenu.div.scrollTop;
				return;
			}
			else if (bitamTypeClicked == 'item')
			{
				return;
			}
		}

		bitamScrollTop = bitamRootMenu.div.scrollTop;
	}

	if (bitamTypeClicked == 'menu')
	{
		bitamTypeClicked = '';
		return;
	}

	if (bitamRootMenu != null)
	{
		if (bitamRootMenu.outclicks == 0)
		{
			bitamRootMenu.outclicks++;
		}
		else
		{
			bitamRootMenu.close();
		}
	}
}

function menu_registerClickEvent()
{
	var anObject = document;

	var anEventName = 'click';
	if (typeof(sLogginFrom) != 'undefined' && sLogginFrom == 'appDashboards')
	{
		anEventName = 'touchstart';
	}

	if (anObject.attachEvent)
		anObject.attachEvent("on"+anEventName, menu_checkClick);
	else
		anObject.addEventListener(anEventName, menu_checkClick, false);
}

function menu_unregisterClickEvent()
{
	var anObject = document;

	var anEventName = 'click';
	if (typeof(sLogginFrom) != 'undefined' && sLogginFrom == 'appDashboards')
	{
		anEventName = 'touchstart';
		bitamScrollTop = 0;
		bitamTypeClicked = '';
	}

	if (anObject.detachEvent)
		anObject.detachEvent("on"+anEventName, menu_checkClick);
	else
		anObject.removeEventListener(anEventName, menu_checkClick, false);
}

function menu_registerMenuKeyEvent()
{
	var anObject = document;

	if (anObject.attachEvent)
		anObject.attachEvent("onkeydown", menu_checkMenuKey);
	else
		anObject.addEventListener("keydown", menu_checkMenuKey, false);
}

function menu_unregisterMenuKeyEvent()
{
	var anObject = document;

	if (anObject.detachEvent)
		anObject.detachEvent("onkeydown", menu_checkMenuKey);
	else
		anObject.removeEventListener("keydown", menu_checkMenuKey, false);
}

function initBuffer()
{
	bitamBuffer = new Array();
}

function appendBuffer(aString)
{
	bitamBuffer.push(aString);
}

function stringFromBuffer()
{
	return (bitamBuffer.join(""));
}

var checkMenuImage = "images/tick.gif";
var subMenuImage = "images/newOption.gif";
var transparentImage = "images/blankimage.gif";

function bitamMenuCheck()
{
	if (!bitamInMenuItem)
	{
		if (bitamRootMenu != null)
		{
			bitamRootMenu.close();
			bitamRootMenu = null;
        }

		if (bitamIntervalId != null)
		{
			clearTimeout(bitamIntervalId);
			bitamIntervalId = null;
		}
	}
}

function getElementWithId(anID)
{
	return (document.getElementById(anID));
}

function getMenuFromId(anId)
{
	return(bitamMenus[anId]);
}

function menu_itemOver(menuId, itemIndex)
{
	bitamInMenuItem = true;

	if (bitamIntervalId != null)
	{
		clearTimeout(bitamIntervalId);
		bitamIntervalId = null;
	}

	var aMenu = getMenuFromId(menuId);
	var anItem = aMenu.items[itemIndex];

	if (anItem.type == 'separator')
		return;

	var anObject = getElementWithId("menu_" + menuId + "_row_" + itemIndex);
	anObject.className = "menuSelected";

	var aSubMenu = anItem.type == 'menu' ? anItem.object : null;

	if (aMenu.selectedItem != null && aMenu.selectedItem.type == 'menu' && aMenu.selectedItem.object != aSubMenu)
	{
		if (aMenu.selectedItem.object.isOpen)
		{
			aMenu.selectedItem.object.close();
		}
	}

	if (aSubMenu != null)
	{
		if (!aSubMenu.isOpen)
		{
			if (typeof(sLogginFrom) != 'undefined' && sLogginFrom == 'appDashboards')
			{
				aSubMenu.strClassName = 'AppDash';
			}

			aSubMenu.popUp(aMenu.div.offsetLeft + aMenu.div.offsetWidth, anObject.offsetTop + aMenu.div.offsetTop);
			bitamInMenuItem = true;
		}
	}

	aMenu.selectedItem = anItem;
}

function menu_itemOut(menuId, itemIndex)
{
	var aMenu = getMenuFromId(menuId);
	var anItem = aMenu.items[itemIndex];

	if (anItem.type != 'separator')
	{
		if (aMenu.selectedItem != null && aMenu.selectedItem.type == 'item')
		{
			var anObject = getElementWithId("menu_" + menuId + "_row_" + itemIndex);
			anObject.className = "menuBody"+aMenu.strClassName;
		}
		else if (aMenu.selectedItem != null && aMenu.selectedItem.type == 'menu')
		{
			if (bitamLastMenuOutIdUnique[menuId+'|'+itemIndex] == undefined)
			{
				bitamLastMenuOutIdUnique[menuId+'|'+itemIndex] = bitamLastMenuOutId.length;
				bitamLastMenuOutId.push(menuId+'|'+itemIndex);

				//window.parent.document.title = bitamLastMenuOutId.length
			}
		}
	}

	bitamInMenuItem = false;

	// if menu timeout defined, set the interval
	if (bitamMenuTimeout != null)
		bitamIntervalId = setTimeout(bitamMenuCheck, bitamMenuTimeout);
}

function menu_itemClicked(menuId, itemIndex, event)
{
	var aMenu = getMenuFromId(menuId);
	var anItem = aMenu.items[itemIndex];
	bitamTypeClicked = anItem.type;

	if (anItem.type == 'separator' || anItem.type == 'menu')
		return;

	if (typeof(sLogginFrom) != 'undefined' && sLogginFrom == 'appDashboards')
	{
		if (aMenu.div.scrollTop != bitamScrollTop) { return; }

		aMenu.action(anItem.object);
		aMenu.parentMenu().close();

		if (typeof(event.preventDefault) != 'undefined')  { event.preventDefault(); }
	}
	else
	{
		aMenu.parentMenu().close();
		aMenu.action(anItem.object);
	}
}

function BitamMenuItem(label, object, checked, aImgURL, aDisabled)
{
	this.label = label;
	this.object = object;
	this.type = 'item';
	this.checked = checked == null ? false : checked;
	this.simage = aImgURL;
	this.bdisabled = aDisabled;
}

function BitamMenu(anEvent)
{
	this.parent = null;

	this.event = anEvent;
	this.isOpen = false;

	this.items = new Array();
	this.action = null;
	this.selectedItem = null;

	this.div = null;

	this.append = menu_append;
	this.appendMenu = menu_appendMenu;
	this.appendItem= menu_appendItem;
	this.appendSeparator = menu_appendSeparator;
	this.parentMenu = menu_parentMenu;
	this.HaveImgsAndCheck = menu_HaveImgsAndCheck;
	this.HaveCheckImg = menu_HaveCheckImg;

	this.menuZIndex = bitamMenuZIndex;

	this.popUp = menu_popUp;
	this.close = menu_close;
	this.strClassName = '';

	this.outclicks = 0;
}

function menu_parentMenu()
{
	var currentMenu = this;

	while (currentMenu.parent != null)
		currentMenu = currentMenu.parent;

	return(currentMenu);
}

function menu_close()
{
	if (!this.isOpen)
		return;

	for (var i = 0; i < this.items.length; i++)
	{
		if (this.items[i].type == 'menu')
		{
			this.items[i].object.close();
		}
	}

	this.isOpen = false;
	this.selectedItem = null;
	document.body.removeChild(this.div);

	if (bitamLastMenuOutId.length > 0)
	{
		var lastIdMenuOver = bitamLastMenuOutId.pop();
		bitamLastMenuOutIdUnique[lastIdMenuOver] = undefined;
		lastIdMenuOver = lastIdMenuOver.split('|');
		var anObject2 = getElementWithId("menu_" + lastIdMenuOver[0] + "_row_" + lastIdMenuOver[1]);
		if (anObject2)
		{
			if (typeof(sLogginFrom) != 'undefined' && sLogginFrom == 'appDashboards')
			{
				anObject2.className = "menuBodyAppDash";
			}
			else
			{
				anObject2.className = "menuBody";
			}
		}
	}

	if (this == bitamRootMenu)
	{
		bitamMenus = new Array();
		bitamLastMenuOutId = new Array();
		bitamLastMenuOutIdUnique = new Array();

		menu_unregisterMenuKeyEvent();
		menu_unregisterClickEvent();
	}

	//bitamMenuZIndex = 100;
}

function menu_append(anItem)
{
	if (anItem.type == 'menu')
		anItem.object.parent = this;

	this.items[this.items.length] = anItem;
}

function menu_appendSeparator()
{
	var anItem = new BitamMenuItem('', null);
	anItem.type = 'separator';

	this.append(anItem);
}

function menu_appendMenu(aLabel, aMenu)
{
	var aAuxLabel = aLabel.split('_AWSep_');
	var aImgURL = '';
	var aDisabled = false;

	if (aAuxLabel.length > 1) {
		aLabel = aAuxLabel[0];
		if (aAuxLabel[1] != '') {
			aImgURL = 'images/'+aAuxLabel[1];
		}
		if (aAuxLabel[2] != undefined && aAuxLabel[2] != null && aAuxLabel[2] != '') {
			aDisabled = (aAuxLabel[2]=='1' || aAuxLabel[2]=='true');
		}
	}

	var anItem = new BitamMenuItem(aLabel, aMenu);
	anItem.type = 'menu';
	anItem.simage = aImgURL;
	anItem.bdisabled = aDisabled;

	this.append(anItem);
}

function menu_appendItem(aLabel, anObject, checked, aImgURL, aDisabled)
{
	this.append(new BitamMenuItem(aLabel, anObject, checked, aImgURL, aDisabled));
}

function menu_HaveImgsAndCheck(aItems){
	var bHaveImgs = false;
	for  (var i = 0; i < aItems.length; i++) {
		if (aItems[i].simage != '' && aItems[i].simage != undefined) {
			bHaveImgs = true;
			break;
		}
	}
	return bHaveImgs;
}

function menu_HaveCheckImg(aItems){
	var bHaveChecked = false;
	for  (var i = 0; i < aItems.length; i++) {
		if (aItems[i].checked) {
			bHaveChecked = true;
			break;
		}
	}
	return bHaveChecked;
}

function getScrollXY() {
	var scrOfX = 0, scrOfY = 0;
	if( typeof( window.pageYOffset ) == 'number' ) {
		//Netscape compliant
		scrOfY = window.pageYOffset;
		scrOfX = window.pageXOffset;
	} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
		//DOM compliant
		scrOfY = document.body.scrollTop;
		scrOfX = document.body.scrollLeft;
	} else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
		//IE6 standards compliant mode
		scrOfY = document.documentElement.scrollTop;
		scrOfX = document.documentElement.scrollLeft;
	}
	return [ scrOfX, scrOfY ];
}

function menu_popUp(x, y)
{
	this.isOpen = true;
	bitamInMenuItem = true;
	if (this.parent == null)
	{
		if (bitamRootMenu != null)
			bitamRootMenu.close();

		bitamRootMenu = this;

		menu_registerMenuKeyEvent();
		menu_registerClickEvent();
	}

	this.id = bitamMenus.length;
	bitamMenus[bitamMenus.length] = this;

	/*if (window.document.documentElement.clientHeight == 0 || IsThatVersion('Firefox/3')){
		var objDocB = document.body;
	}
	else {
		var objDocB = window.document.documentElement;
	}*/

	//var nScrollTop = document.documentElement.scrollTop
	//var nScrollLeft = document.documentElement.scrollLeft
	var aScrollXY = getScrollXY();
	var nScrollLeft = aScrollXY[0]
	var nScrollTop = aScrollXY[1]

	if (window.dialogArguments == null || window.dialogArguments == undefined) {
		var nClientHeight = document.documentElement.clientHeight
		var nClientWidth = document.documentElement.clientWidth
		if (nClientHeight == 0) { nClientHeight = document.body.clientHeight; }
		if (nClientWidth == 0)  { nClientWidth  = document.body.clientWidth;  }
	}
	else {
		var nClientHeight = document.body.clientHeight
		var nClientWidth = document.body.clientWidth
	}

	var realX = (x == null | x==undefined) ? this.event.clientX + bitamMenuLeftMinus + nScrollLeft : x;

	if ((y == null | y==undefined)) {
		var realY = this.event.clientY + nScrollTop
	}
	else {
		if (this.parent != null) {
			y = y - this.parent.div.scrollTop;
		}
		var realY = y
	}

	this.div = document.createElement('DIV');

	this.div.id = 'menuDiv_' + this.id;
	this.div.className = 'shadow';
	this.div.style.position = 'absolute';
	this.div.style.visibility = 'hidden';

	this.div.style.left = realX + 'px';
	this.div.style.top = realY + 'px';
	this.div.style.zIndex = this.menuZIndex;

	var bHaveImgs = menu_HaveImgsAndCheck(this.items);
	var bHaveChecked = menu_HaveCheckImg(this.items);

	initBuffer();

	var anEventName = 'onclick';
	if (typeof(sLogginFrom) != 'undefined' && sLogginFrom == 'appDashboards')
	{
		anEventName = 'ontouchend';
	}

	appendBuffer('<table class="menuBody'+this.strClassName+'" border="0" cellpadding="0" cellspacing="0">');

	var itemDisabled = false;
	var nTopItems    = this.items.length;
	for (var i = 0; i < nTopItems; i++)
	{
		var bIsLast = ((i+1) == nTopItems);
		var sClassAdd = this.strClassName;
		if (this.strClassName == 'AppDash')
		{
			if (bIsLast || (this.items[i+1] != undefined  && this.items[i+1].type == 'separator'))
			{
				sClassAdd = 'Last'+sClassAdd;
			}
		}

		itemDisabled = false;
		itemDisabled = this.items[i].bdisabled;
		appendBuffer('<tr id="');
		appendBuffer('menu_');
		appendBuffer(this.id);
		appendBuffer('_row_');
		appendBuffer(i);
		appendBuffer('"');

		appendBuffer(' onmouseover="menu_itemOver(');
		appendBuffer(this.id);
		appendBuffer(',');
		appendBuffer(i);
		appendBuffer(')" onmouseout="menu_itemOut(');
		appendBuffer(this.id);
		appendBuffer(',');
		appendBuffer(i);
		appendBuffer(')"');

		if (this.items[i].type != 'separator' && !itemDisabled) // this.items[i].type != 'menu' &&
		{
			appendBuffer(' '+anEventName+'="menu_itemClicked(');
			appendBuffer(this.id);
			appendBuffer(',');
			appendBuffer(i);
			appendBuffer(', event)"');
		}

		appendBuffer('>');

		// separator
		if (this.items[i].type == 'separator')
		{
			appendBuffer('<td class="separator" colspan="4"><hr class="hrSep"></td>');
		}
		else
		{
			if (bHaveImgs) // Imgs
			{
				appendBuffer('<td class="cellStyle'+sClassAdd+'">');
/*
				if (bHaveChecked && menu_AddChecked(this.items[i])) {
					if (this.items[i].checked)
					{
						appendBuffer('<img border="0" src="');
						appendBuffer(checkMenuImage);
						appendBuffer('">&nbsp;');
					}
					else {
						appendBuffer('<img border="0" width="11px" src="');
						appendBuffer(transparentImage);
						appendBuffer('">&nbsp;');
					}
				}
*/
				if (this.items[i].simage != '' && this.items[i].simage != undefined)
				{
					appendBuffer('<img border="0" src="');
					appendBuffer(jsDir+this.items[i].simage);
					appendBuffer('"');
					if (this.strClassName == 'AppDash')
					{
						appendBuffer(' style="height:20px;"');
					}
					appendBuffer('>');
				}
				else {
					appendBuffer('&nbsp;');
				}

				appendBuffer('</td>');
			}
			else {		// Check
				appendBuffer('<td class="cellStyle'+sClassAdd+'">');

				if (this.items[i].checked & false)
				{
					appendBuffer('<img border="0" src="');
					appendBuffer(checkMenuImage);
					appendBuffer('"');
					if (this.strClassName == 'AppDash')
					{
						appendBuffer(' style="height:20px;"');
					}
					appendBuffer('>');
				}
				else
					appendBuffer('&nbsp;');

				appendBuffer('</td>');
			}

			appendBuffer('<td nowrap class="cellStyle'+sClassAdd+'"');

			if (itemDisabled) {
				appendBuffer(' style="color:#c0c0c0; cursor:default;"');
			}

			appendBuffer('>');

			appendBuffer(this.items[i].label);


			if (bHaveChecked && menu_AddChecked(this.items[i])) {
				if (this.items[i].checked)
				{
					appendBuffer('&nbsp;&nbsp;<img border="0" src="');
					appendBuffer(checkMenuImage);
					appendBuffer('"');
					if (this.strClassName == 'AppDash')
					{
						appendBuffer(' style="height:20px;"');
					}
					appendBuffer('>&nbsp;');
				}
			}

			appendBuffer('</td>');
			appendBuffer('<td class="cellStyle'+sClassAdd+'">');

			if (this.items[i].type == 'menu')
			{
				appendBuffer('<img border="0" src="');
				appendBuffer(subMenuImage);
				appendBuffer('"');
				if (this.strClassName == 'AppDash')
				{
					appendBuffer(' style="height:13px;"');
				}
				appendBuffer('>');
			}
			else
				appendBuffer('&nbsp;');

			appendBuffer('</td>');
			appendBuffer('</tr>');
		}
	}

	appendBuffer('</table>');
	appendBuffer('<div class="topleft"></div>');
	appendBuffer('<div class="topright"></div>');
	appendBuffer('<div class="bottomleft"></div>');
	appendBuffer('<div class="bottomright"></div>');

	this.div.innerHTML = stringFromBuffer();
	document.body.appendChild(this.div);

	if (this.div.offsetHeight > nClientHeight)
	{
		var nWTemp = this.div.offsetWidth;
		this.div.style.overflowY = 'scroll';
		this.div.style.overflowX = 'visible';

		this.div.style.height = (nClientHeight - 5) + "px";
		this.div.style.width = (bIsIE)? (nWTemp + 13) + "px" : "auto";
	}
	// xcoord boundaries
	if (realX + this.div.offsetWidth > nClientWidth + nScrollLeft)
	{
		if (this.parent != null)
			realX = realX - this.div.offsetWidth - this.parent.div.offsetWidth;
		else
			realX = nClientWidth - this.div.offsetWidth + nScrollLeft;

		if (realX < 0)
			//realX = document.body.clientWidth - this.div.offsetWidth + document.body.scrollLeft;
			realX = 0;

		if (realX < nScrollLeft)
			realX = nScrollLeft;

		this.div.style.left = realX + 'px';
	}

	// ycoord boundaries
	if (realY + this.div.offsetHeight > nClientHeight + nScrollTop)
	{
		/*if (this.parent != null)
			realY = realY - this.div.offsetHeight;
		else*/
			realY = nClientHeight - this.div.offsetHeight + nScrollTop;

		if (realY < 0)
			realY = nClientHeight - this.div.offsetHeight + nScrollTop;

		if (realY < nScrollTop)
			realY = nScrollTop;

		this.div.style.top = realY + 'px';
	}

	this.div.style.visibility = 'visible';

	if (bitamIntervalId != null)
	{
		clearTimeout(bitamIntervalId);
		bitamIntervalId = null;
	}

	if (bitamIntervalId == null)
	{
		// if menu timeout defined, set the interval
		if (bitamMenuTimeout != null)
			bitamIntervalId = setTimeout(bitamMenuCheck, bitamMenuTimeout);
	}

	bitamInMenuItem = false;
}

function menu_AddChecked(oItem){
	var bReturn = true;
	if (oItem.type == "menu" || oItem.object == 15) {
		bReturn = false;
	}
	return bReturn;
}
