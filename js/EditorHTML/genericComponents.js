// Edition Style
var sDisabledColor = '#f3f3f3';
var sImgTitleSection = 'url(images/ipixelsection.gif)';
var sImgTitle = 'url(images/ipixeltitle.gif)';
var sImgTitleCut = 'url(images/ipixelcut.gif)';
var sColorTitleSection = "#238f23";
var sColorTitleCut = "#323232";
var sColorGenTitleSec = "#737e90";
var sBGColorSepSection = "#3cb878";
var sBGColorSepPart = "#66cdaa";
var sBGColorSepTitle = "#1e90ff";
var sBGColorSepCut = "#aeaeae";
var sFontDefault = 'calibri';
// Constants
var AWComa = '_#AWComa#_';
var AWUnd = '#AWUnd#';
var AWSep = '_#AWSep#_';
var AWCut = '_#AWCut#_';
var AWSec = '_#AWSec#_';
var AWRep = '_#AWRep#_';
var AWElem = '_#AWElem#_';
var AWElemT = '_#AWElemT#_';
var AWProps = '_#AWProps#_';
var AWCHR9 = '_AWSChr9_';
var AWCOMA = '_AWSComa_';
// Elements Report
var RepLabel = 0;
var RepSection = 6;
var RepSoloSection = 7;
var RepImage = 8;
var RepShape = 9;
var RepLine = 15;
var RepButton = 100;
var isIE = navigator.userAgent.indexOf("MSIE") != -1;
var nWidthDefault = 700+"px";
var nHeightDefault = 100+"px";
var aFontsNames = [ML[360]+'_AWSep_genericvariable.gif','Arial','Arial black','Arial narrow','Arial unicode ms','Book antiqua','Bookman old style','Calibri','Candara','Century gothic','Comic sans ms','Consolas','Constantia','Corbel','Courier new','Courierthai','Garamond','Georgia','Haettenschweiler','Impact','Lucida bright','Lucida console','Lucida typewrite','Lucida sans unicode','Map symbols','Microsoft sans serif','Monotype corsiva','Palatino linotype','Segoe','Tahoma','Thonburi','Times new roman','Trebuchet ms','Verdana'];
var aFontsKeys = ['FONT_Tarial','FONT_Tarial black','FONT_Tarial narrow','FONT_Tarial unicode ms','FONT_Tbook antiqua','FONT_Tbookman old style','FONT_Tcalibri','FONT_Tcandara','FONT_Tcentury gothic','FONT_Tcomic sans ms','FONT_Tconsolas','FONT_Tconstantia','FONT_Tcorbel','FONT_Tcourier new','FONT_Tcourierthai','FONT_Tgaramond','FONT_Tgeorgia','FONT_Thaettenschweiler','FONT_Timpact','FONT_Tlucida bright','FONT_Tlucida console','FONT_Tlucida typewrite','FONT_Tlucida sans unicode','FONT_Tmap symbols','FONT_Tmicrosoft sans serif','FONT_Tmonotype corsiva','FONT_Tpalatino linotype','FONT_Tsegoe','FONT_Ttahoma','FONT_Tthonburi','FONT_Ttimes new roman','FONT_Ttrebuchet ms','FONT_Tverdana'];

function genericElement(nTypeElement, sElementId)
{
  	var oelement = document.createElement("div")
  	oelement.id = "Element" + sElementId;
    oelement.style.left = "0px"
    oelement.style.top = "10px"
    oelement.style.width = "80px"
    oelement.style.height = "20px"
    oelement.style.backgroundColor = "#FFFFFF"
    oelement.style.color = "#000000"
    oelement.style.fontSize = "11px"
    oelement.style.fontFamily = "ms sans serif"
    oelement.style.textAlign = "center"
    oelement.style.border = "dotted"
    oelement.style.overflow = "hidden"
    oelement.style.position = "absolute"
    oelement.Rep_type = nTypeElement;
    oelement.setAttribute("sGoPar", 0);
	var sInHTML = 'Element';
	oelement.innerHTML = sInHTML+' '+sElementId

	// assign events (begin)	
	if (isIE)
    {
      oelement.attachEvent("ondragover", function(evt){hDD(evt,0,1,nTypeElement,oelement.id); evt.cancelBubble = true;})
      oelement.attachEvent("ondrop", function(evt){hDD(evt,1,1,nTypeElement,oelement.id); evt.cancelBubble = true;})
      oelement.attachEvent("oncontextmenu", function(evt){GetComponentMenu(evt); evt.cancelBubble = true; return false;})
    }
    else {
      oelement.addEventListener("contextmenu", function(evt){GetComponentMenu(evt); evt.stopPropagation(); evt.preventDefault(); return false;}, true)
    }
	
	// assign events (end)

	// métodos (begin)
	this.restoreProps = function (aMD, iPos) {
		// Properties
		this.element.Rep_left = parseInt(aMD[iPos++]);
		this.element.Rep_top = parseInt(aMD[iPos++]);
		this.element.Rep_width = parseInt(aMD[iPos++]);
		this.element.Rep_height = parseInt(aMD[iPos++]);
		this.element.Rep_BackColor = aMD[iPos++];
		this.element.Rep_ForeColor = aMD[iPos++];
		this.element.Rep_FontBold = getStrBool(aMD[iPos++]);
		this.element.Rep_FontItalic = getStrBool(aMD[iPos++]);
		this.element.Rep_FontUnderline = getStrBool(aMD[iPos++]);
		this.element.Rep_FontSize = parseInt(aMD[iPos++]);
		this.element.Rep_FontName = aMD[iPos++];
		this.element.Rep_Picture = ParsePathImg(aMD[iPos++]);
		this.element.Rep_bPicStreched = getStrBool(aMD[iPos++]);
		this.element.Rep_Alignment = parseInt(aMD[iPos++]);
		this.element.Rep_BorderColor = aMD[iPos++];
		this.element.Rep_BorderType = parseInt(aMD[iPos++]);
		this.element.Rep_BorderWidth = parseInt(aMD[iPos++]);
		this.element.Rep_visible = getStrBool(aMD[iPos++]);
		this.refreshProps();
	}

	this.saveProps = function () {
		var sSave = '';
		// Properties
		sSave = sSave + this.element.Rep_left;
		sSave = sSave + ',' + this.element.Rep_top;
		sSave = sSave + ',' + this.element.Rep_width;
		sSave = sSave + ',' + this.element.Rep_height;
		sSave = sSave + ',' + this.element.Rep_BackColor;
		sSave = sSave + ',' + this.element.Rep_ForeColor;
		sSave = sSave + ',' + getBoolStr(this.element.Rep_FontBold);
		sSave = sSave + ',' + getBoolStr(this.element.Rep_FontItalic);
		sSave = sSave + ',' + getBoolStr(this.element.Rep_FontUnderline);
		sSave = sSave + ',' + this.element.Rep_FontSize;
		sSave = sSave + ',' + this.element.Rep_FontName;
		sSave = sSave + ',' + this.element.Rep_Picture;
		sSave = sSave + ',' + getBoolStr(this.element.Rep_bPicStreched);
		sSave = sSave + ',' + this.element.Rep_Alignment;
		sSave = sSave + ',' + this.element.Rep_BorderColor;
		sSave = sSave + ',' + this.element.Rep_BorderType;
		sSave = sSave + ',' + this.element.Rep_BorderWidth;
		sSave = sSave + ',' + getBoolStr(this.element.Rep_visible);
		return sSave;
	}

	this.refreshProps = function (){
		if (this.element.Rep_caption != undefined) {
			this.element.innerHTML = this.element.Rep_caption;
		}
		this.element.style.left = this.element.Rep_left+"px";
		this.element.style.top = this.element.Rep_top+"px";
		this.element.style.width = this.element.Rep_width+"px";
		this.element.style.height = this.element.Rep_height+"px";
		this.setBackgroundColor();
		this.setForeColor();
		this.element.style.fontWeight = (this.element.Rep_FontBold)?'bold':'normal';
		this.element.style.fontStyle = (this.element.Rep_FontItalic)?'italic':'normal';
		this.element.style.textDecoration = (this.element.Rep_FontUnderline)?'underline':'none';
		this.element.style.fontSize = this.element.Rep_FontSize+"px";
		this.element.style.fontFamily = this.element.Rep_FontName;
		this.setBackgroundImage();
		this.setTextAlign();
		this.setBorder();
		if (!this.element.Rep_visible) {
			this.element.style.backgroundColor = sDisabledColor;
			this.element.style.backgroundImage = "none";
			this.element.style.border = "1px solid gray";
			this.element.style.color = 'gray';
			this.element.style.fontStyle = 'italic';
		}
	}

	this.setBackgroundColor = function (){
    	if (this.element.Rep_BackColor == '-1') {
			this.element.style.backgroundColor = 'transparent';
		}
		else {
			this.element.style.backgroundColor = '#'+this.element.Rep_BackColor;
		}
    }

    this.setForeColor = function (){
    	if (this.element.Rep_ForeColor == '-1') {
			this.element.style.color = 'transparent';
		}
		else {
			this.element.style.color = '#'+this.element.Rep_ForeColor;
		}
    }

	this.setBackgroundImage = function (){
		if (this.element.Rep_type == RepImage || this.element.Rep_type == RepLine) {
    		return;
    	}
		if (Trim(this.element.Rep_Picture) != '') {
			this.element.style.backgroundImage = 'url('+getValidImageSrc(this.element.Rep_Picture)+')';
			if (this.element.Rep_bPicStreched) {
				this.element.style.backgroundPosition = "50% 50%";
				this.element.style.backgroundRepeat = "no-repeat";
			}
			else {
				this.element.style.backgroundPosition = "";
				this.element.style.backgroundRepeat = "repeat";
			}
		}
		else {
			this.element.style.backgroundImage = 'none';
		}
	}

	this.setTextAlign = function (){
		switch (this.element.Rep_Alignment){
			case 0:
				this.element.style.textAlign = "left";
				break;
			case 1:
				this.element.style.textAlign = "center";
				break;
			default:
				this.element.style.textAlign = "right";
		}
	}

	this.setBorder = function (){
		var sBType = 'none';
		switch (this.element.Rep_BorderType){
			case 0:
				sBType = "none";
				break;
			case 1:
				sBType = "solid";
				break;
			case 2:
				sBType = "outset";
				break;
			case 3:
				sBType = "inset";
				break;
		}
		if (sBType == 'none') {
			this.element.style.border = "1px dotted gray";
		}
		else {
			this.element.style.border = this.element.Rep_BorderWidth+"px " + sBType + ' #'+this.element.Rep_BorderColor;
		}
	}

	this.UpdateVisible = function ()
    {
    	this.element.Rep_visible = !this.element.Rep_visible;
    	if (!this.element.Rep_visible) {
			this.element.style.backgroundColor = sDisabledColor;
			this.element.style.backgroundImage = "none";
			this.element.style.border = "1px solid gray";
			this.element.style.color = 'gray';
			this.element.style.fontStyle = 'italic';
		}
		else {
			this.setBackgroundColor();
			this.setBackgroundImage();
			this.setBorder();
			this.setForeColor();
			this.element.style.fontStyle = (this.element.Rep_FontItalic)?'italic':'normal';
		}
    }

    this.refreshPosition = function ()
    {
    	this.element.style.left = this.element.Rep_left+"px";
		this.element.style.top = this.element.Rep_top+"px";
    }
    
	this.getStyle = function (borderNone)
    {
    	//BackGroundColor
		var strBGColor = '';
    	if (this.element.Rep_BackColor == '-1') 
    	{
			strBGColor = ';BACKGROUND-COLOR:transparent';
		}
		else
		{
			strBGColor = ';BACKGROUND-COLOR:#'+this.element.Rep_BackColor;
		}
    	
    	
    	//ForeColor
		var strFGColor = '';
    	if (this.element.Rep_ForeColor == '-1') 
    	{
			strFGColor = ';COLOR:transparent';
		}
		else 
		{
			strFGColor = ';COLOR:#'+this.element.Rep_ForeColor;
		}
    
		//Font
		var strFontWeight = ";FONT-WEIGHT:" + ((this.element.Rep_FontBold)?'bold':'normal');
		var strFontStyle = ";FONT-STYLE:" + ((this.element.Rep_FontItalic)?'italic':'normal');
		var strTextDec = ";TEXT-DECORATION:" + ((this.element.Rep_FontUnderline)?'underline':'none');
		var strFontSize = ";FONT-SIZE:" + this.element.Rep_FontSize+"px";
		var strFontFamily = ";FONT-FAMILY:'" + this.element.Rep_FontName + "'";

    	//BackGroudImage
    	var strBGImage = ';';
    	if (Trim(this.element.Rep_Picture) != '') {
    		//AAL 21/05/2015 Issue #63J210: Comentado por que duplicaba las imaganes y no respetaba la configuracion(style) dada en el editor
			strBGImage = ';/*BACKGROUND-IMAGE: url('+getValidImageSrc(this.element.Rep_Picture)+')*/';
			if (this.element.Rep_bPicStreched) 
			{
				strBGImage += ';BACKGROUND-POSITION:50% 50%';
				strBGImage += ';BACKGROUND-REPEAT:no-repeat';
			}
			else
			{
				strBGImage += ';BACKGROUND-REPEAT:repeat';
			}
		}
		
		//Text Align
		var strTextAlign;
		strTextAlign = '';
		switch (this.element.Rep_Alignment)
		{ //AAL 21/05/2015 Issue #63J210: No respetaba la propiedad BACKGROUND-REPEAT:no-repeat por que faltaba poner ; al inicio de los TEXT-ALIGN.
			case 0:
				strTextAlign = ';TEXT-ALIGN:left';
				break;
			case 1:
				strTextAlign = ';TEXT-ALIGN:center';
				break;
			default:
				strTextAlign = ';TEXT-ALIGN:right';
		}
		
		//Border
		var strBorder = '';
		var sBType = 'none';
		
		if(borderNone==undefined)
		{
			borderNone=false;
		}
		
		switch (this.element.Rep_BorderType)
		{
			case 0:
				sBType = "none";
				break;
			case 1:
				sBType = "solid";
				break;
			case 2:
				sBType = "outset";
				break;
			case 3:
				sBType = "inset";
				break;
		}
		
		if (sBType == 'none')
		{
			if(borderNone)
			{
				strBorder = ";BORDER: none";
			}
			else
			{
				strBorder = ";BORDER: 1px dotted gray";
			}
		}
		else
		{
			strBorder = ";BORDER:"+this.element.Rep_BorderWidth+"px " + sBType + ' #' + this.element.Rep_BorderColor;
		}
		
		var strWhiteSpace = "";
		if(this.element.Rep_type==RepLabel)
		{
			strWhiteSpace = ";WHITE-SPACE: normal";
		}
		
    	//HTML
    	var strHTML = 'POSITION:absolute; OVERFLOW: hidden; width: '+this.element.Rep_width +
    					'px; height: '+this.element.Rep_height +
    					'px; left:'+this.element.Rep_left +
    					'px; top:'+this.element.Rep_top +
    					"px"+ strBGColor + strFGColor + 
    					strFontWeight + strFontStyle + strTextDec + 
    					strFontSize + strFontFamily + strBGImage +
    					strTextAlign + strBorder+strWhiteSpace;
    	
    	return strHTML;
    }    
    
	// métodos (end)

	this.element = oelement
}/*****ok******/

/*****************  genericPart ****************/
function genericPart(sWidth, sID, ogen, sHeight)
{
    sWidth = sWidth == null ? nWidthDefault : sWidth
    sHeight = sHeight == null ? nHeightDefault : sHeight;

    // title
  	var bAddTitle = true;
    var stitle = ML[578] + " " + sID
    var scolortitle = sColorTitleSection
    switch(sID)
    {      
      case "body":
        stitle = ML[581]
        break     
      default:
      	bAddTitle = false;
      	break;
    }
    
    // part
    var oPart = document.createElement("div")
    oPart.id = sID;
    oPart.style.position = "relative"
    //oPart.style.height = "240px"
    oPart.style.height = sHeight
    oPart.style.width = sWidth
    
    oPart.Rep_id = oPart.id;
	//oPart.Rep_height = 30;
	oPart.Rep_height = parseInt(sHeight.replace('px', ''));
	oPart.Rep_width = parseInt(sWidth.replace('px', ''));
	//alert( oPart.Rep_width + ',' + oPart.Rep_height);
	oPart.Rep_backgroundColor = '-1';
	oPart.Rep_backgroundImage = '';
	oPart.Rep_bPicStreched = true;
	oPart.Rep_bVisible = true;
    /*
    oPart.style.borderLeftStyle = "dotted"
    oPart.style.borderLeftWidth = "1px"
    oPart.style.borderRightStyle = "dotted"
    oPart.style.borderRightWidth = "1px"
    */
    oPart.style.fontSize = "1px"
    oPart.style.overflow = "hidden"
    if (!isIE)
    {
      oPart.style.MozUserSelect = "none";
    }

    if (ogen != null) {
    	oPart.setAttribute("sectionId", ogen.id);
    }

    var objX = ogen == null ? oPart : ogen

    if (isIE)
    {
      oPart.attachEvent("ondragover", function(evt){hDD(evt,0,1,objX.id,oPart.id); evt.cancelBubble = true;})
      oPart.attachEvent("ondrop", function(evt){hDD(evt,1,1,objX.id,oPart.id); evt.cancelBubble = true;})
      oPart.attachEvent("oncontextmenu", function(evt){GetSectionTitleMenu(evt,objX.id,oPart.id); evt.cancelBubble = true; return false;})
    }
    else {
	  oPart.addEventListener("contextmenu", function(evt){GetSectionTitleMenu(evt,objX.id,oPart.id); evt.preventDefault(); return false;}, true)
    }

    // métodos
    this.restore = function (sParams, bRestore){
		var aMD = sParams.split(AWElem);
		var aProp = aMD[0].split(',');
		this.restoreSubsection(aProp);

		// Elements
		var top = aMD.length;
		for (var i=1;i<top;i++){
			aProp = aMD[i].split(AWElemT);
			this.addElement(parseInt(aProp[0]), aProp[1], bRestore);
		}
		aProp = null;
		aMD = null;
    }

    this.restoreSubsection = function (aProp)
    {
		var regexp = new RegExp(AWComa,"gi");
		var top = aProp.length - 1;
		var regexpEnter  = new RegExp('_AWEnter_',"gi");
		var regexpEnterR = new RegExp('_AWEnterR_',"gi");
		var regexpEnterN = new RegExp('_AWEnterN_',"gi");

    	this.divPart.Rep_id = aProp[0];
    	//Se comentaron estas líneas por que al hacer el cambio de tamaño en las pantallas cuando ya se tenían
    	//datos grabados mostraba el alto y ancho anteriormente grabado y no el nuevo tamaño configurado
		//this.divPart.Rep_height = parseInt(aProp[1]);
		//this.divPart.Rep_width = parseInt(aProp[2]);		
		this.divPart.Rep_backgroundColor = aProp[3];
		this.divPart.Rep_backgroundImage = ParsePathImg(aProp[4]);
		this.divPart.Rep_bPicStreched = getStrBool(aProp[5]);
		this.divPart.Rep_bVisible = getStrBool(aProp[6]);
		this.refresh();
    }

    this.save = function (bAll, sSep)
    {
    	var nTop = this.arrElements.length;
    	
    	if (nTop==0)
    	{
    		return '';
    	}
    	
    	var regexp = new RegExp(',',"gi");

    	var sSave = this.divPart.Rep_id;
		sSave = sSave + sSep + this.divPart.Rep_height;
		sSave = sSave + sSep + this.divPart.Rep_width;
		sSave = sSave + sSep + this.divPart.Rep_backgroundColor;
		sSave = sSave + sSep + this.divPart.Rep_backgroundImage;
		sSave = sSave + sSep + getBoolStr(this.divPart.Rep_bPicStreched);
		sSave = sSave + sSep + getBoolStr(this.divPart.Rep_bVisible);

		if (bAll) {
			var nTop = this.arrElements.length;
			for (var i=0; i<nTop; i++) {
				if (this.arrElements[i] != undefined && this.arrElements[i] != null) {
					sSave = sSave + AWElem + this.arrElements[i].element.Rep_type + AWElemT + this.arrElements[i].save();
				}
			}
		}
		return sSave;
    }

    this.saveHTML = function ()
    {	
    	var nTop = this.arrElements.length;
    	
    	if (nTop==0)
    	{
    		return '';
    	}
    	
    	var aHTML = new Array();
    	
		//BackGroundColor
    	var strBGColor = '';
    	if (this.divPart.Rep_backgroundColor == '-1') 
    	{
			strBGColor = ';BACKGROUND-COLOR:transparent';
		}
		else
		{
			strBGColor = ';BACKGROUND-COLOR:#'+this.divPart.Rep_backgroundColor;
		}
    	//BackGroudImage
    	var strBGImage = '';
    	if (Trim(this.divPart.Rep_backgroundImage) != '') {
			//this.divPart.style.backgroundImage = "url("+getValidImageSrc(this.divPart.Rep_backgroundImage)+")";
			strBGImage = ';BACKGROUND-IMAGE: url('+getValidImageSrc(this.divPart.Rep_backgroundImage)+')';
			if (this.divPart.Rep_bPicStreched) 
			{
				//this.divPart.style.backgroundPosition = "50% 50%";
				strBGImage += ';BACKGROUND-POSITION:50% 50%';
				//this.divPart.style.backgroundRepeat = "no-repeat";
				strBGImage += ';BACKGROUND-REPEAT:no-repeat';
			}
			else
			{
				//this.divPart.style.backgroundPosition = "";
				//this.divPart.style.backgroundRepeat = "repeat";
				strBGImage += ';BACKGROUND-REPEAT:repeat';
			}
		}
    	
    	//HTML
    	var nMaxHeight = 0;
    	var nTop = this.arrElements.length;
		for (var i=0; i<nTop; i++) {
			if (this.arrElements[i] != undefined && this.arrElements[i] != null) 
			{
				strHTML = this.arrElements[i].saveHTML();
				aHTML.push(strHTML);
				
				if(this.arrElements[i].element.Rep_height+this.arrElements[i].element.Rep_top>nMaxHeight)
				{
					nMaxHeight=this.arrElements[i].element.Rep_height+this.arrElements[i].element.Rep_top;
				}
			}
		}
    	
    	aHTML.push('</div>');
    	
    	var strHTML = '<div '+' style="position:relative; width: 100%; height: '+nMaxHeight+'px'+strBGColor+strBGImage+'">';
    	
    	return strHTML + aHTML.join('');
    }
    
    this.delComps = function (bAll) {
    	var nTop = this.arrElements.length;
		for (var i=0; i<nTop; i++) {
			if (this.arrElements[i] != undefined && this.arrElements[i] != null) {
				if ((this.arrElements[i].element.Rep_type == RepLabel || this.arrElements[i].element.Rep_type == RepImage
				  || this.arrElements[i].element.Rep_type == RepShape || this.arrElements[i].element.Rep_type == RepButton) || bAll)
				{
					this.delElement(this.arrElements[i].element.Rep_id);
				}
			}
		}
    }

    this.refresh = function (){
		this.divPart.style.height = this.divPart.Rep_height+"px";
		this.divPart.style.width = this.divPart.Rep_width+"px";
		this.setBackgroundColor();
		this.setBackgroundImage();
		if (!this.divPart.Rep_bVisible) {
			this.divPart.style.backgroundColor = sDisabledColor;
			this.divPart.style.backgroundImage = "none";
			this.fitHeight();
		}
    }

    this.setBackgroundColor = function (){
    	if (this.divPart.Rep_backgroundColor == '-1') {
			this.divPart.style.backgroundColor = 'transparent';
		}
		else {
			this.divPart.style.backgroundColor = '#'+this.divPart.Rep_backgroundColor;
		}
    }

    this.setBackgroundImage = function (){
    	if (Trim(this.divPart.Rep_backgroundImage) != '') {
			this.divPart.style.backgroundImage = "url("+getValidImageSrc(this.divPart.Rep_backgroundImage)+")";
			if (this.divPart.Rep_bPicStreched) {
				this.divPart.style.backgroundPosition = "50% 50%";
				this.divPart.style.backgroundRepeat = "no-repeat";
			}
			else {
				this.divPart.style.backgroundPosition = "";
				this.divPart.style.backgroundRepeat = "repeat";
			}
		}
		else {
			this.divPart.style.backgroundImage = "url(images/dottedbg5x5.gif)";
			this.divPart.style.backgroundPosition = "";
			this.divPart.style.backgroundRepeat = "repeat";
		}
    }

    this.UpdatebVisible = function ()
    {
    	this.divPart.Rep_bVisible = !this.divPart.Rep_bVisible;
    	if (!this.divPart.Rep_bVisible) {
			this.divPart.style.backgroundColor = sDisabledColor;
			this.divPart.style.backgroundImage = "none";
		}
		else {
			this.setBackgroundColor();
			this.setBackgroundImage();
		}
		this.fitHeight();
    }

    this.Paste = function (nTypeElement, sParams, XLeft, YTop){
		var oElemPasted = this.addElement(nTypeElement, sParams, false);
		oElemPasted.element.Rep_left = XLeft;
		oElemPasted.element.Rep_top = YTop;
		oElemPasted.refreshProps();
    }

    this.fitHeight = function ()
    {
    	this.divPart.style.height = ((this.divPart.Rep_bVisible)?this.divPart.Rep_height:nSubSectionMinHeight)+"px"
    }

    this.addElement = function (nTypeElement, sParams, bRestore)
    {
    	switch (nTypeElement) {
    		case RepLabel:
    			oelement = new cLabel(nTypeElement, (this.getNextElementId()+1));
    			oelement.element.style.whiteSpace = "normal";
    			break;
    		case RepImage:
    			oelement = new cImage(nTypeElement, (this.getNextElementId()+1));
    			break;
    		case RepLine:
    			oelement = new cLine(nTypeElement, (this.getNextElementId()+1));
    			break;
    		case RepButton:
    			oelement = new cButton(nTypeElement, (this.getNextElementId()+1));
    			break;
    		default:
    			return null;
    			break;
    	}

    	oelement.restore(sParams, bRestore)
    	this.divPart.appendChild(oelement.element)
    	registerMovingObject(oelement.element);
    	this.arrElements.push(oelement)
	    this.arrElementsIds[oelement.element.Rep_id] = this.arrElements.length-1;

	    return oelement;
    }

    this.delElement = function (sElemId)
    {
		if (this.arrElementsIds[sElemId] != undefined && this.arrElementsIds[sElemId] != null) {
		    nConsecSection = this.arrElementsIds[sElemId];
			if (this.arrElements[nConsecSection] != undefined && this.arrElements[nConsecSection] != null) {
			 	this.divPart.removeChild(this.arrElements[nConsecSection].element);
				this.arrElements[nConsecSection] = null;
		    }
		    this.arrElementsIds[sElemId] = null;
		}
	}

	this.moveElement = function (sElemId, moveTo)
	{
		var aAuxElements = new Array();
		var aAuxElementsIds = new Array();
		var bMoved = false;
		var zIndex = 1;

		if (this.arrElementsIds[sElemId] != undefined && this.arrElementsIds[sElemId] != null)
		{
			if (moveTo == 'first')
			{
				var nConsecSection = this.arrElementsIds[sElemId];

				aAuxElements.push(this.arrElements[nConsecSection]);
				aAuxElementsIds[this.arrElements[nConsecSection].element.Rep_id] = aAuxElements.length-1;

				this.arrElements[nConsecSection].element.style.zIndex = zIndex;
				zIndex++;

				bMoved = true;
			}

			var nTop = this.arrElements.length;
		  	for (var i=0; i<nTop; i++)
		  	{
				if (this.arrElements[i] != undefined && this.arrElements[i] != null)
				{
					if (this.arrElements[i].element.Rep_id != sElemId)
					{
						aAuxElements.push(this.arrElements[i]);
						aAuxElementsIds[this.arrElements[i].element.Rep_id] = aAuxElements.length-1;

						this.arrElements[i].element.style.zIndex = zIndex;
						zIndex++;
					}
				}
			}

			if (bMoved)
			{
				this.arrElements = aAuxElements;
				this.arrElementsIds = aAuxElementsIds;
			}
		}

		return bMoved;
	}

    this.getNextElementId = function (){
	  	var nTop = this.arrElements.length-1;
	  	var nId = nTop;
	  	for(var i=nTop; i>=0; i--) {
			if (this.arrElements[i] != undefined && this.arrElements[i] != null) {
				nId = parseInt(this.arrElements[i].element.Rep_id.replace("Element", ""));
				break;
			}
		}
		return nId;
    }

    this.getElementsWithWork = function(bIncludeCalculated, sPrefixId, sPrefixName, aValidTypes)
    {
		var sInds = '';
		var nTop = this.arrElements.length;
		var bcheckvalidtypes = (aValidTypes != undefined);
		for(var i=0; i<nTop; i++)
		{
			if (this.arrElements[i] != undefined && this.arrElements[i] != null)
			{
				if (bcheckvalidtypes)
				{
					if (aValidTypes[this.arrElements[i].element.Rep_type] != undefined)
					{
						var sname = this.arrElements[i].element.Rep_name;
						if (this.arrElements[i].element.Rep_type == RepLabel || this.arrElements[i].element.Rep_type == RepButton)
						{
							if (this.arrElements[i].element.Rep_caption.substring(0,1) == '@')
							{
								sname = this.arrElements[i].element.Rep_caption;
							}
							else
							{
								continue;
							}
						}

						if (sInds!='') {
							sInds = sInds+'_AWSepElement_'+sPrefixId+this.arrElements[i].element.Rep_id;
							sInds = sInds+'_AWSepName_'+sPrefixName+sname;
						}
						else {
							sInds = sPrefixId+this.arrElements[i].element.Rep_id;
							sInds = sInds+'_AWSepName_'+sPrefixName+sname;
						}
					}
				}
			}
		}
		return sInds;
	}

    this.arrElements = new Array()
  	this.arrElementsIds = new Array()

    // assign public pointers of interest
    this.divPart = oPart
}/******ok*****/

// begin function ReportPart   ************************************************************
function reportPart(ReportId, stitle, sWidth, sHeight, newDivID, textArea)
{	//debugger;
  	sWidth = sWidth == null ? nWidthDefault : sWidth
  	sHeight = sHeight == null ? nHeightDefault : sHeight;
  	
  	//Sep-2014
  	var divID;
  	divID = ((newDivID) && (newDivID!='')) ? newDivID : ReportId;
  	
  	objParent=document.getElementById(ReportId);
  	var objTextArea=document.getElementById(textArea);
	var sMD = (objTextArea)?objTextArea.value:'';
  	// gen Part
  	//alert(sWidth+','+sHeight);
  	var ogenpart = new genericPart(sWidth,divID,null, sHeight)
  	if(sMD!='')
  	{
  		ogenpart.restore(sMD, true);
  	}
	
  	// apuntadores publicos
  	this.oPart = ogenpart

  	if (isIE) {
  		//document.body.appendChild(ogenpart.divPart);
  		objParent.appendChild(ogenpart.divPart);
  	}
  	else {
  		//document.documentElement.appendChild(ogenpart.divPart);
  		objParent.appendChild(ogenpart.divPart);
  		//var x = [hDD,[1,1,ogenpart.divPart.id,ogenpart.divPart.id]];
	  	//new parent.DropEvent(ogenpart.divPart.id, x, document);
  	}
  }
  // end function ReportPart