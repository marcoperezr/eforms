var jsDir = '';
var oElementMenu = null;
var oSectionMenu = null;
var oSectionRestore = null;
var oSubSectionMenu = null;
var oCutMenu = null;
var sReportTypeMenu = '';
var sSubSectionTypeMenu = '';
var sMetaCopy = '';
var sTypeCopy = '';
var aColsPivsNams = new Array();
var oElementActCopy = null;
var aVarsNams = new Array();
var aHTMLDivObjects = new Array();
bitamCalculatedPos = true;
var aButtonElements = new Array();
var aCompToAdd = new Array();
aCompToAdd = ['LBL,ADD','IMG,ADD','BTN,ADD'];
var aCompToAddDes = new Array();
aCompToAddDes = [ML[157]+'_AWSep_comp_agregar.gif',ML[584]+'_AWSep_rep_etiqueta.gif',ML[361]+'_AWSep_rep_imagen.gif', ML['Button']+'_AWSep_insertButton.png'];
function GetSectionTitleMenu(evt,sSectionId,sSubSectionType){
//alert(sSectionId+'-'+sSubSectionType);
	oElementMenu = null; oSubSectionMenu = null; oCutMenu = null;
	var bContinue = true;	
	oSectionMenu = (aHTMLDivObjects[sSectionId] != undefined )?aHTMLDivObjects[sSectionId] : null;
	
	if (oSectionMenu != null) {
		var aKeysMenu = new Array(); var aNamsMenu = new Array();
		var iposMenu = 0;
		if (document.createEventObject) {
			currentEvent = document.createEventObject(evt);
		}
		else {
			currentEvent = evt;
		}
		sSubSectionTypeMenu = sSubSectionType;
		iposMenu = GetSTMenu(aKeysMenu, aNamsMenu, iposMenu, sSectionId, sSubSectionType);
		iposMenu = GetMiscMenuST(aKeysMenu, aNamsMenu, iposMenu, sSectionId, sSubSectionType);
		
		popUpQuickMenu(evt,setMenuComp,aKeysMenu,aNamsMenu,null,null);
	}
}
function GetComponentMenu(evt){
	oElementMenu = null;
	if (document.createEventObject) {
		currentEvent = document.createEventObject(evt);
	}
	else {
		currentEvent = evt;
	}
	var odiv = evt.srcElement || evt.target
	if (odiv.tagName == "IMG" &&
	   (odiv.id == "Graph" || odiv.id == "Image" || odiv.id == "Gauge" || odiv.id == "Line" || odiv.id == "PGBar"))
	{
		odiv = odiv.parentElement || odiv.parentNode
	}
	else
	{
		while(odiv.tagName!='DIV' && typeof(odiv.Rep_type)=="undefined" && odiv.tagName!="BODY")
		{
			odiv = odiv.parentElement || odiv.parentNode
		}
	}
	var bOk = false; var bContinue = false; var bAddSep = true;
	var sElemId = odiv.id
	var sSubSectionType = (isIE) ? odiv.parentElement.id : odiv.parentNode.id
	var sSectionId = (isIE) ? odiv.parentElement.getAttribute("sectionId") : odiv.parentNode.getAttribute("sectionId")
	var bIsSelMultiple = (getLength(aMovingObject) > 1);
	oSectionMenu = GetSection(sSectionId);
	oSubSectionMenu = getSecPart(sSubSectionType, odiv)
	if (oSubSectionMenu != null) {
 		var nConsecElem = GetConsecElement(oSubSectionMenu,sElemId);
		if (nConsecElem != -1) {
			var oElement = oSubSectionMenu.arrElements[nConsecElem];
			bOk = true;
		}
	}
	if (bOk) {
		oElementMenu = oElement;
		var aKeysMenu = new Array(); var aNamsMenu = new Array();
		var iposMenu = 0;
		// Properties
		switch (oElementMenu.element.Rep_type) {
			case RepLabel:
			case RepButton:
			case RepImage:
			case RepLine:
				GetPropertiesMenu(aKeysMenu, aNamsMenu, iposMenu, oElementMenu.element.Rep_type, bIsSelMultiple);
				iposMenu++;
				break;
		}
		if (!bIsSelMultiple)
		{
			// Add Cut
			bOk = true;
			// Misce: Copy
			switch (oElementMenu.element.Rep_type) {
				case RepLabel:
				case RepButton:
				case RepImage:
				case RepLine:
					aKeysMenu[iposMenu] = -1;	// Sep
					aNamsMenu[iposMenu+1] = '---';	// Sep
					iposMenu++;
					GetMiscMenu(aKeysMenu, aNamsMenu, iposMenu, oElementMenu.element.Rep_type);
					iposMenu++;
					break;
			}
		}
		else
		{
			// Opciones de ajuste y alineación
			switch (oElementMenu.element.Rep_type)
			{
				case RepLabel:
				case RepButton:
				case RepImage:
				case RepLine:
					GetAlignmentMenu(aKeysMenu, aNamsMenu, iposMenu, oElementMenu.element.Rep_type, bIsSelMultiple);
					iposMenu++;
					GetAdjustmentMenu(aKeysMenu, aNamsMenu, iposMenu, oElementMenu.element.Rep_type, bIsSelMultiple);
					iposMenu++;
					break;
			}
		}
		// Delete
		switch (oElementMenu.element.Rep_type) {
			case RepLabel:
			case RepButton:
			case RepImage:
			case RepLine:
				aKeysMenu[iposMenu] = -1;	// Sep
				aNamsMenu[iposMenu+1] = '---';	// Sep
				iposMenu++;
				var aAuxKeys = 'DELELEM';
				var aAuxDesc = ML[193]+'_AWSep_delete.gif';
				aKeysMenu[iposMenu] = aAuxKeys;	// Delete
				aNamsMenu[iposMenu+1] = aAuxDesc;	// Delete
				iposMenu++;
				break;
		}
		popUpQuickMenu(evt,setMenuComp,aKeysMenu,aNamsMenu,null,null);
	}
}
function GetLinkerMenu(aKeysMenu, aNamsMenu, iposMenu, iRep_type){
	// Enlace entre Reportes
	var aAuxKeys = 'LINKER_REP';
	var aAuxDesc = ML[665]+'_AWSep_link_reports.gif';
	aKeysMenu[iposMenu] = aAuxKeys;
	aNamsMenu[iposMenu+1] = aAuxDesc;
}
function GetMiscMenuST(aKeysMenu, aNamsMenu, iposMenu, sSectionId, sSubSectionType){	
	if (sMetaCopy != '') {		
		aKeysMenu[iposMenu] = -1;	// Sep
		aNamsMenu[iposMenu+1] = '---';	// Sep
		iposMenu++;

		var aAuxKeys = 'PASTEELEM';	// Paste
		var aAuxDesc = ML[660]+'_AWSep_paste.gif';	// Paste
		aKeysMenu[iposMenu] = aAuxKeys;
		aNamsMenu[iposMenu+1] = aAuxDesc;
		iposMenu++;
	}

	return iposMenu;
}
function GetSTMenu(aKeysMenu, aNamsMenu, iposMenu, sSectionId, sSubSectionType)
{
	aKeysMenu[iposMenu] = new Array();	// Properties
	aNamsMenu[iposMenu+1] = [ML[101]+'_AWSep_comp_properties.gif'];	// Properties
	var ipos = 0; var aAuxKeys = ''; var aAuxDesc = ''; var sSelected = '';

	aAuxKeys = 'RP_BCKGCOLOR';
	aKeysMenu[iposMenu][ipos] = aAuxKeys;
	aAuxDesc = ML[104]+'_AWSep_back_color.gif';
	aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc;
	ipos++;

	var bStreched = oSectionMenu.oPart.divPart.Rep_bPicStreched;
	
	aAuxKeys = ['RP_IMAGE_SEL','RP_IMAGE_DEL','RP_IMAGE_FIT'];
	aAuxDesc = [ML[361]+'_AWSep_image.gif',ML[362]+'_AWSep_image_sel.gif',ML[85]+'_AWSep_image_del.gif',
			((bStreched)?'*':'')+ML[590]+'_AWSep_image_fit.gif'];
	aKeysMenu[iposMenu][ipos] = aAuxKeys.slice(0);
	aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc.slice(0);
	ipos++;	

	iposMenu++;
	
  	// Agregar Componentes
	aKeysMenu[iposMenu] = -1;
	aNamsMenu[iposMenu+1] = '---';
	iposMenu++;
	/*
  	aAuxKeys = ['LBL,ADD',
  				['IMG,ADD','LIN,ADD']];
	aAuxDesc = [ML[157]+'_AWSep_comp_agregar.gif',ML[584]+'_AWSep_rep_etiqueta.gif',
				[ML[651]+'_AWSep_rep_graficos.gif',ML[361]+'_AWSep_rep_imagen.gif',ML[218]+'_AWSep_rep_linea.gif']
				];*/
	
  	aAuxKeys = aCompToAdd;
	aAuxDesc = aCompToAddDes;
	aKeysMenu[iposMenu] = aAuxKeys.slice(0);
	aNamsMenu[iposMenu+1] = aAuxDesc.slice(0);
	iposMenu++;
	
  	// Borrar: Todos los Componentes, Solo componentes Gráficos, Borrar Corte y Sección solo si hay mas de 1
	aKeysMenu[iposMenu] = -1;
	aNamsMenu[iposMenu+1] = '---';
	iposMenu++;

	
	aAddAuxK2 = ['DEL_COMP_ALL'];
	aAddAuxD2 = [ML[689]+'_AWSep_comp_borrar.gif',ML[690]+'_AWSep_comp_borrar_all.gif'];

  	aAuxKeys = aAddAuxK2.slice(0);
	aAuxDesc = aAddAuxD2.slice(0);
	aKeysMenu[iposMenu] = aAuxKeys.slice(0);
	aNamsMenu[iposMenu+1] = aAuxDesc.slice(0);
	iposMenu++;
	
	return iposMenu;
}
function GetMiscMenu(aKeysMenu, aNamsMenu, iposMenu, iRep_type){
	var aAuxKeys = 'COPYELEM';
	var aAuxDesc = ML[529]+'_AWSep_copy.gif';
	aKeysMenu[iposMenu] = aAuxKeys;	// Copy
	aNamsMenu[iposMenu+1] = aAuxDesc;	// Copy
}
function GetOthers(aKeysMenu, aNamsMenu, iposMenu, iRep_type){
	var sSelected = '';
	if (iRep_type == RepLabel) {
		sSelected = (oElementMenu.element.Rep_bPivotValue) ? '*' : '';
		var aAuxKeys = 'SHOWPIVVAL';
		var aAuxDesc = sSelected+ML[656]+'_AWSep_show_pivot_value.gif';
	}
	else if (iRep_type != RepPBar) {
		sSelected = (oElementMenu.element.Rep_bShowAsImage) ? '*' : '';
		var aAuxKeys = 'VIEWASIMG';
		var aAuxDesc = sSelected+ML[647]+'_AWSep_seeasimage.gif';
	}
	else {
		sSelected = (oElementMenu.element.Rep_ShowDecimals) ? '*' : '';
		var aAuxKeys = 'SHOWDEC';
		var aAuxDesc = sSelected+ML[652]+'_AWSep_show_dec.gif';
	}
	aKeysMenu[iposMenu] = aAuxKeys;	// Others
	aNamsMenu[iposMenu+1] = aAuxDesc;	// Others
}
function GetEditMenu(aKeysMenu, aNamsMenu, iposMenu, iRep_type){
	aKeysMenu[iposMenu] = new Array();	// Edit
	aNamsMenu[iposMenu+1] = [ML[81]+'_AWSep_edit16.gif'];	// Edit
	var ipos = 0; var aAuxKeys = ''; var aAuxDesc = '';

	if (iRep_type == RepChart || iRep_type == RepGauge) {
		aAuxKeys = (iRep_type == RepChart) ? 'EDELEMSGRP' : 'EDELEMSGAU';
		aKeysMenu[iposMenu][ipos] = aAuxKeys;
		aAuxDesc = ML[604]+'_AWSep_component.gif';
		aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc;
		ipos++;
	}
	else {
		aAuxKeys = 'FORMAT';
		aKeysMenu[iposMenu][ipos] = aAuxKeys;
		aAuxDesc = ML[78]+'_AWSep_format_number.gif';
		aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc;
		ipos++;
	}

	if (((iRep_type == RepIndicator && !oElementMenu.element.Rep_bNotNumeric) || iRep_type == RepTotal) &&
		sReportTypeMenu == BitamCube)
	{
		aAuxKeys = 'PERIOD';
		aKeysMenu[iposMenu][ipos] = aAuxKeys;
		aAuxDesc = ML[83]+'_AWSep_period.gif';
		aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc;
		ipos++;
	}

	if (iRep_type == RepCalculated) {
		aAuxKeys = 'EDITCALC';
		aKeysMenu[iposMenu][ipos] = aAuxKeys;
		aAuxDesc = ML[79]+'_AWSep_formula.gif';
		aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc;
		ipos++;
	}
}
function GetAlignmentMenu(aKeysMenu, aNamsMenu, iposMenu, iRep_type, bIsSelMultiple)
{
	aKeysMenu[iposMenu] = ['alignm_left', 'alignm_vert', 'alignm_right',
							-1,
						   'alignm_up', 'alignm_hor', 'alignm_down'
							];	// Alignment
	aNamsMenu[iposMenu+1] = [ML[885]+'_AWSep_alignment.gif',
								ML[886]+'_AWSep_alignleft.gif',
								ML[887]+'_AWSep_alignvert.gif',
								ML[888]+'_AWSep_alignright.gif',
								'---',
								ML[889]+'_AWSep_alignup.gif',
								ML[890]+'_AWSep_alignhor.gif',
								ML[891]+'_AWSep_aligndown.gif'
								];	// Alignment
}
function GetAdjustmentMenu(aKeysMenu, aNamsMenu, iposMenu, iRep_type, bIsSelMultiple)
{
	aKeysMenu[iposMenu] = ['adjustmentwidth', 'adjustmentheight', 'adjustmentsize'];	// Adjustment
	aNamsMenu[iposMenu+1] = [ML[892]+'_AWSep_adjustment.gif',
								ML[893]+'_AWSep_adjustmentwidth.gif',
								ML[894]+'_AWSep_adjustmentheight.gif',
								ML[895]+'_AWSep_adjustmentsize.gif'
								];	// Adjustment
}
function GetPropertiesMenu(aKeysMenu, aNamsMenu, iposMenu, iRep_type, bIsSelMultiple){
	aKeysMenu[iposMenu] = new Array();	// Properties
	aNamsMenu[iposMenu+1] = [ML[101]+'_AWSep_comp_properties.gif'];	// Properties
	var ipos = 0; var aAuxKeys = ''; var aAuxDesc = ''; var sSelected = '';

	if (!bIsSelMultiple) {
		aAuxKeys = ['EDITPROPS'];
		aKeysMenu[iposMenu][ipos] = aAuxKeys.slice(0);
		aAuxDesc = ML[81]+'_AWSep_edit16.gif';
		aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc;
		ipos++;
	}

	if (iRep_type == RepLine) {
		if (!bIsSelMultiple) {
			if (iRep_type == RepLine) {
				var aFN = oElementMenu.element.Rep_FontName.split('@');
				aAuxKeys = ['LINE,STY,Grad','LINE,STY,CilBar','LINE,STY,GlassBar','LINE,STY,SoftBar'];
				aAuxDesc = [ML[265]+'_AWSep_line_style.gif',((aFN[0]=='Grad')?'*':'')+ML['degradado']+'_AWSep_style_gradient.gif',
						((aFN[0]=='CilBar')?'*':'')+ML['cilindro']+'_AWSep_style_cylinder.gif',
						((aFN[0]=='GlassBar')?'*':'')+ML['vidrio']+'_AWSep_style_glass.gif',
						((aFN[0]=='SoftBar')?'*':'')+ML['suave']+'_AWSep_style_soft.gif'];
			}

			aKeysMenu[iposMenu][ipos] = aAuxKeys.slice(0);
			aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc.slice(0);
			ipos++;

			if (iRep_type == RepLine) {
				aAuxKeys = ['LINE,COL,Initial','LINE,COL,Final'];
				aAuxDesc = [ML[270]+'_AWSep_border_color.gif',
							ML['inicial']+'_AWSep_color_initial.gif',
							ML['final']+'_AWSep_color_final.gif'];
			}
			aKeysMenu[iposMenu][ipos] = aAuxKeys.slice(0);
			aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc.slice(0);
			ipos++;

			if (iRep_type == RepLine) {
				aAuxKeys = ['LINE,DEG,Ver','LINE,DEG,Hor'];
				aKeysMenu[iposMenu][ipos] = aAuxKeys.slice(0);
				aAuxDesc = [ML[363]+'_AWSep_gradient.gif',
						((aFN[1]=='Ver')?'*':'')+ML[242]+'_AWSep_gradient_ver.gif',
						((aFN[1]=='Hor')?'*':'')+ML[243]+'_AWSep_gradient_hor.gif'];
				aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc.slice(0);
				ipos++;
			}
		}
	}
	else if (iRep_type != RepImage) {
		aAuxKeys = ['BCKGCOLOR'];
		aKeysMenu[iposMenu][ipos] = aAuxKeys.slice(0);
		aAuxDesc = ML[104]+'_AWSep_back_color.gif';
		aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc;
		ipos++;
	}

	if (iRep_type != RepLine) {
		
		if(iRep_type != RepLabel)
		{	
			aAuxKeys = ['ALIGN_0','ALIGN_1','ALIGN_2'];
			aKeysMenu[iposMenu][ipos] = aAuxKeys.slice(0);
			aAuxDesc = [ML[593]+'_AWSep_comp_align.gif',((oElementMenu.element.Rep_Alignment==0)?'*':'')+ML[478]+'_AWSep_align_left.gif',
					((oElementMenu.element.Rep_Alignment==1)?'*':'')+ML[594]+'_AWSep_align_center.gif',
					((oElementMenu.element.Rep_Alignment==2)?'*':'')+ML[479]+'_AWSep_align_right.gif'];
			aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc.slice(0);
			ipos++;
		}
		
		aAuxKeys = ['BORDER_COLOR',['BORDER_T0','BORDER_T1','BORDER_T2','BORDER_T3'],['BORDER_W1','BORDER_W2','BORDER_W3',
					'BORDER_W4','BORDER_W5','BORDER_W6','BORDER_W7','BORDER_W8','BORDER_W9','BORDER_W10']];
		aKeysMenu[iposMenu][ipos] = aAuxKeys.slice(0);
		aAuxDesc = [ML[595]+'_AWSep_comp_border.gif',ML[270]+'_AWSep_border_color.gif',
					[ML[360]+'_AWSep_border.gif',
					 ((oElementMenu.element.Rep_BorderType==0)?'*':'')+ML[245]+'_AWSep_border_none.gif',
					 ((oElementMenu.element.Rep_BorderType==1)?'*':'')+ML[127]+'_AWSep_border_regular.gif',
					 ((oElementMenu.element.Rep_BorderType==2)?'*':'')+ML[596]+'_AWSep_border_raised.gif',
					 ((oElementMenu.element.Rep_BorderType==3)?'*':'')+ML[597]+'_AWSep_border_drop.gif'],
					[ML[592]+'_AWSep_wide.gif',
					 ((oElementMenu.element.Rep_BorderWidth==1)?'*':'')+'1',
					 ((oElementMenu.element.Rep_BorderWidth==2)?'*':'')+'2',
					 ((oElementMenu.element.Rep_BorderWidth==3)?'*':'')+'3',
					 ((oElementMenu.element.Rep_BorderWidth==4)?'*':'')+'4',
					 ((oElementMenu.element.Rep_BorderWidth==5)?'*':'')+'5',
					 ((oElementMenu.element.Rep_BorderWidth==6)?'*':'')+'6',
					 ((oElementMenu.element.Rep_BorderWidth==7)?'*':'')+'7',
					 ((oElementMenu.element.Rep_BorderWidth==8)?'*':'')+'8',
					 ((oElementMenu.element.Rep_BorderWidth==9)?'*':'')+'9',
					 ((oElementMenu.element.Rep_BorderWidth==10)?'*':'')+'10']];
		aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc.slice(0);
		ipos++;

		if (iRep_type != RepImage && iRep_type != RepLabel) {
			var aAuxFontNames = [ML[360]+'_AWSep_font_type.gif'];
			var nTopFN = aFontsKeys.length;
			for (var iFN=0; iFN<nTopFN; iFN++) {
				sSelected = (aFontsKeys[iFN] == 'FONT_T'+oElementMenu.element.Rep_FontName)?'*':'';
				aAuxFontNames.push(sSelected+aFontsNames[iFN+1]);
			}

			aAuxKeys = ['FONT_COLOR',['FONT_SB','FONT_SI','FONT_SU'],['FONT_W5','FONT_W7','FONT_W8',
						'FONT_W9','FONT_W10','FONT_W11','FONT_W12','FONT_W13','FONT_W14','FONT_W15','FONT_W16','FONT_W18',
						'FONT_W20','FONT_W22','FONT_W24','FONT_W26','FONT_W28','FONT_W30','FONT_W32','FONT_W34','FONT_W36',
						'FONT_W38','FONT_W40'],aFontsKeys.slice(0)];
			aKeysMenu[iposMenu][ipos] = aAuxKeys.slice(0);
			aAuxDesc = [ML[288]+'_AWSep_comp_font.gif',ML[270]+'_AWSep_font_color.gif',
						[ML[265]+'_AWSep_font_style.gif',
						 ((oElementMenu.element.Rep_FontBold)?'*':'')+ML[290]+'_AWSep_font_bold.gif',
						 ((oElementMenu.element.Rep_FontItalic)?'*':'')+ML[291]+'_AWSep_font_italic.gif',
						 ((oElementMenu.element.Rep_FontUnderline)?'*':'')+ML[428]+'_AWSep_font_underline.gif'],
						[ML[289]+'_AWSep_font_size.gif',
						 ((oElementMenu.element.Rep_FontSize==5)?'*':'')+'5',
						 ((oElementMenu.element.Rep_FontSize==7)?'*':'')+'7',
						 ((oElementMenu.element.Rep_FontSize==8)?'*':'')+'8',
						 ((oElementMenu.element.Rep_FontSize==9)?'*':'')+'9',
						 ((oElementMenu.element.Rep_FontSize==10)?'*':'')+'10',
						 ((oElementMenu.element.Rep_FontSize==11)?'*':'')+'11',
						 ((oElementMenu.element.Rep_FontSize==12)?'*':'')+'12',
						 ((oElementMenu.element.Rep_FontSize==13)?'*':'')+'13',
						 ((oElementMenu.element.Rep_FontSize==14)?'*':'')+'14',
						 ((oElementMenu.element.Rep_FontSize==15)?'*':'')+'15',
						 ((oElementMenu.element.Rep_FontSize==16)?'*':'')+'16',
						 ((oElementMenu.element.Rep_FontSize==18)?'*':'')+'18',
						 ((oElementMenu.element.Rep_FontSize==20)?'*':'')+'20',
						 ((oElementMenu.element.Rep_FontSize==22)?'*':'')+'22',
						 ((oElementMenu.element.Rep_FontSize==24)?'*':'')+'24',
						 ((oElementMenu.element.Rep_FontSize==26)?'*':'')+'26',
						 ((oElementMenu.element.Rep_FontSize==28)?'*':'')+'28',
						 ((oElementMenu.element.Rep_FontSize==30)?'*':'')+'30',
						 ((oElementMenu.element.Rep_FontSize==32)?'*':'')+'32',
						 ((oElementMenu.element.Rep_FontSize==34)?'*':'')+'34',
						 ((oElementMenu.element.Rep_FontSize==36)?'*':'')+'36',
						 ((oElementMenu.element.Rep_FontSize==38)?'*':'')+'38',
						 ((oElementMenu.element.Rep_FontSize==40)?'*':'')+'40'],
						aAuxFontNames.slice(0)];
			aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc.slice(0);
			ipos++;
		}

		if (iRep_type != RepImage) {
			aAuxKeys = ['IMAGE_SEL','IMAGE_DEL','IMAGE_FIT'];
			aAuxDesc = [ML[361]+'_AWSep_image.gif',ML[362]+'_AWSep_image_sel.gif',ML[85]+'_AWSep_image_del.gif',
					((oElementMenu.element.Rep_bPicStreched)?'*':'')+ML[590]+'_AWSep_image_fit.gif'];
		}
		else {
			aAuxKeys = ['IMAGE_SEL','IMAGE_FIT'];
			aAuxDesc = [ML[361]+'_AWSep_image.gif',ML[362]+'_AWSep_image_sel.gif',
					((oElementMenu.element.Rep_picstretched)?'*':'')+ML[590]+'_AWSep_image_fit.gif'];
		}
		aKeysMenu[iposMenu][ipos] = aAuxKeys.slice(0);
		aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc.slice(0);
		ipos++;
	}

	if (!bIsSelMultiple)
	{
		aKeysMenu[iposMenu][ipos] = 'SENDBACK';
		aNamsMenu[iposMenu+1][ipos+1] = ML[872]+'_AWSep_sendback.gif';
		ipos++;
	}

	aAuxKeys = 'SHOWHIDE';
	aKeysMenu[iposMenu][ipos] = aAuxKeys;
	aAuxDesc = ML[583]+'_AWSep_show_hide.gif';
	aNamsMenu[iposMenu+1][ipos+1] = aAuxDesc;
	ipos++;
}
function setMenuComp(sOption)
{
	if (oElementMenu != null)
	{
		if (sOption == 'EDITPROPS') {
			mayProperties(oElementMenu.element.Rep_type,oElementMenu.element.Rep_id,'PROPS');
			currentEvent = null;
		}
		else if (sOption == 'BCKGCOLOR') {
			if (oElementMenu.element.Rep_BackColor != -1) {
				scolor = '#'+oElementMenu.element.Rep_BackColor;
			}
			else {
				scolor = oElementMenu.element.Rep_BackColor;
			}
			fnShowChooseColorDlg(scolor, sOption, '');
		}
		else if (sOption.substring(0,6) == 'ALIGN_') {
			oElementMenu.element.Rep_Alignment = parseInt(sOption.substring(6));
			oElementMenu.setTextAlign();
			SetOptionMultiple(oElementMenu.element.Rep_Alignment, 'ALIGN');
		}
		else if (sOption.substring(0,7) == 'BORDER_') {
			if (sOption == 'BORDER_COLOR') {
				if (oElementMenu.element.Rep_BorderColor != -1) {
					scolor = '#'+oElementMenu.element.Rep_BorderColor;
				}
				else {
					scolor = oElementMenu.element.Rep_BorderColor;
				}
				fnShowChooseColorDlg(scolor, sOption, '');
			}
			else if (sOption.substring(0,8) == 'BORDER_T') {
				oElementMenu.element.Rep_BorderType = parseInt(sOption.substring(8));
				if (oElementMenu.element.Rep_visible) {
					oElementMenu.setBorder();
				}
				SetOptionMultiple(oElementMenu.element.Rep_BorderType, 'BORDER_T');
			}
			else if (sOption.substring(0,8) == 'BORDER_W') {
				oElementMenu.element.Rep_BorderWidth = parseInt(sOption.substring(8));
				if (oElementMenu.element.Rep_visible) {
					oElementMenu.setBorder();
				}
				SetOptionMultiple(oElementMenu.element.Rep_BorderWidth, 'BORDER_W');
			}
		}
		else if (sOption.substring(0,5) == 'FONT_') {
			if (sOption == 'FONT_COLOR') {
				if (oElementMenu.element.Rep_ForeColor != -1) {
					scolor = '#'+oElementMenu.element.Rep_ForeColor;
				}
				else {
					scolor = oElementMenu.element.Rep_ForeColor;
				}
				fnShowChooseColorDlg(scolor, sOption, '');
			}
			else if (sOption.substring(0,6) == 'FONT_S') {
				switch(sOption) {
					case 'FONT_SB':
						oElementMenu.element.Rep_FontBold = !oElementMenu.element.Rep_FontBold;
						oElementMenu.element.style.fontWeight = (oElementMenu.element.Rep_FontBold)?'bold':'normal';
						SetOptionMultiple(oElementMenu.element.Rep_FontBold, 'FONT_SB');
						break;
					case 'FONT_SI':
						oElementMenu.element.Rep_FontItalic = !oElementMenu.element.Rep_FontItalic;
						oElementMenu.element.style.fontStyle = (oElementMenu.element.Rep_FontItalic)?'italic':'normal';
						SetOptionMultiple(oElementMenu.element.Rep_FontItalic, 'FONT_SI');
						break;
					case 'FONT_SU':
						oElementMenu.element.Rep_FontUnderline = !oElementMenu.element.Rep_FontUnderline;
						oElementMenu.element.style.textDecoration = (oElementMenu.element.Rep_FontUnderline)?'underline':'none';
						SetOptionMultiple(oElementMenu.element.Rep_FontUnderline, 'FONT_SU');
						break;
				}
			}
			else if (sOption.substring(0,6) == 'FONT_W') {
				oElementMenu.element.Rep_FontSize = parseInt(sOption.substring(6));
				oElementMenu.element.style.fontSize = oElementMenu.element.Rep_FontSize+"px";
				SetOptionMultiple(oElementMenu.element.Rep_FontSize, 'FONT_W');
			}
			else if (sOption.substring(0,6) == 'FONT_T') {
				oElementMenu.element.Rep_FontName = sOption.substring(6);
				oElementMenu.element.style.fontFamily = oElementMenu.element.Rep_FontName;
				SetOptionMultiple(oElementMenu.element.Rep_FontName, 'FONT_T');
			}
		}
		else if (sOption.substring(0,6) == 'IMAGE_') {
			if (sOption == 'IMAGE_SEL') {
				SelImage('Component');
			}
			else if (sOption == 'IMAGE_DEL') {
				ClearImage('Component');
			}
			else if (sOption == 'IMAGE_FIT') {
				if (oElementMenu.element.Rep_type != RepImage) {
					oElementMenu.element.Rep_bPicStreched = !oElementMenu.element.Rep_bPicStreched;
					if (oElementMenu.element.Rep_visible) {
						oElementMenu.setBackgroundImage();
					}
					SetOptionMultiple(oElementMenu.element.Rep_bPicStreched, sOption);
				}
				else {
					oElementMenu.element.Rep_picstretched = !oElementMenu.element.Rep_picstretched;
					if (oElementMenu.element.Rep_visible) {
						oElementMenu.setImage();
					}
				}
			}
		}
		else if (sOption == 'SENDBACK')
		{
			if (oSubSectionMenu != null && oSubSectionMenu.divPart != null)
			{
				oSubSectionMenu.moveElement(oElementMenu.element.Rep_id, 'first');
			}
		}
		else if (sOption == 'SHOWHIDE') {
			oElementMenu.UpdateVisible();
			if (oElementMenu.element.Rep_type == RepLine && oElementMenu.element.Rep_visible) {
				oElementMenu.makeImage();
			}
			SetOptionMultiple(!oElementMenu.element.Rep_visible, sOption);
		}
		else if (sOption == 'FORMAT') {
			EditFormat(oElementMenu);
		}
		else if (sOption == 'PERIOD') {
			var nCubeKey = -1;
			if (oSectionMenu != null) {
	  	 		nCubeKey = oSectionMenu.divSection.cubekey;
			}
			if (nCubeKey == -1){ nCubeKey = oElementMenu.element.Rep_cubekey; }
			EditPeriod(oElementMenu, nCubeKey);
		}
		else if (sOption == 'VIEWASIMG') {
			oElementMenu.element.Rep_bShowAsImage = !oElementMenu.element.Rep_bShowAsImage;
		}
		else if (sOption == 'ORDER,0,0' || sOption == 'ORDER,0,1' || sOption == 'ORDER,1,0' || sOption == 'ORDER,1,1') {
			if (oSectionMenu != null) {
				var aAux = sOption.split(',');
				// Revisar si es en Corte o body donde se está indicando el ordenamiento
				var sSubSecOrder = 'body';
				if (oSubSectionMenu != null && oSubSectionMenu.divPart != null &&
					oSubSectionMenu.divPart.getAttribute("cutId") != null)
				{
					sSubSecOrder = oSubSectionMenu.divPart.getAttribute("cutId");
				}
				// Buscar si en el corte o body ya existe un ordenamiento previo
				var aSortComps = oSectionMenu.divSection.nIdOrder.split('_AWSepSortSubSec_');
				var aSetSorts = new Array();
				var nTopSC = aSortComps.length;
				var bFound = false;
				for (var i=0; i<nTopSC; i++) {
					var aSortItems = aSortComps[i].split('_AWSepSSS_');
					var nTopSI = aSortItems.length;
					if (nTopSI == 1) { // Valida si es ordenamiento antigüo
						aSetSorts.push(sSubSecOrder+'_AWSepSSS_'+oElementMenu.element.Rep_id+'_AWSepSSS_'+aAux[2]+'_AWSepSSS_'+aAux[1]);
						if (sSubSecOrder != 'body') {
							aSetSorts.push('body_AWSepSSS_'+oSectionMenu.divSection.nIdOrder+'_AWSepSSS_'+getBoolStr(oSectionMenu.divSection.bOrderDesc)+
										   '_AWSepSSS_'+getBoolStr(oSectionMenu.divSection.bOrderByKey));
						}
						bFound = true;
						break;
					}
					else if (aSortItems[0] == sSubSecOrder) { // Revisa si es la misma subsección, de serlo asignar el nuevo elemento
						aSetSorts.push(sSubSecOrder+'_AWSepSSS_'+oElementMenu.element.Rep_id+'_AWSepSSS_'+aAux[2]+'_AWSepSSS_'+aAux[1]);
						bFound = true;
					}
					else { // Si no es la misma agregar la ya existente
						aSetSorts.push(aSortItems[0]+'_AWSepSSS_'+aSortItems[1]+'_AWSepSSS_'+aSortItems[2]+'_AWSepSSS_'+aSortItems[3]);
					}
				}
				if (!bFound) { // Si no la agregó, entonces agregarla
					aSetSorts.push(sSubSecOrder+'_AWSepSSS_'+oElementMenu.element.Rep_id+'_AWSepSSS_'+aAux[2]+'_AWSepSSS_'+aAux[1]);
				}
				oSectionMenu.divSection.nIdOrder = aSetSorts.join('_AWSepSortSubSec_');
				//oSectionMenu.divSection.nIdOrder = oElementMenu.element.Rep_id;
				//oSectionMenu.divSection.bOrderDesc = getStrBool(aAux[2]);
			}
		}
		else if (sOption.substring(0,4) == 'TOT,') {
			oElementMenu.element.Rep_TotalType = parseInt(sOption.substring(4));
		}
		else if (sOption == 'EDITCALC') {
			EditFormula(oElementMenu, oSubSectionMenu);
		}
		else if (sOption == 'CUT,ADD') {
			AddCutMenu(oSectionMenu, oElementMenu.element.Rep_key);
		}
		else if (sOption.substring(0,8) == 'TTL,ADD,') {
			AddTotal(oElementMenu, sOption.substring(8));
		}
		else if (sOption.substring(0,5) == 'LINE,') {
			SetLine(oElementMenu, sOption.substring(5));
		}
		else if (sOption == 'EDELEMSGRP') {
			EditElements(oElementMenu, oSubSectionMenu, oSectionMenu);
		}
		else if (sOption == 'WIZGRP') {
			GraphProperties(oElementMenu);
		}
		else if (sOption == 'chartColors') {
			GraphPropertiesForUserColors(oElementMenu);
		}
		else if (sOption == 'EDELEMSGAU') {
			EditElementsGauge(oElementMenu, oSubSectionMenu, oSectionMenu);
		}
		else if (sOption == 'WIZGAU') {
			GaugeProperties(oElementMenu);
		}
		else if (sOption.substring(0,4) == 'PGB,' || sOption == 'SHOWDEC') {
			SetPGB(oElementMenu, sOption);
		}
		else if (sOption.substring(0,4) == 'SEE,' || sOption.substring(0,4) == 'DUP,') {
			SetSeeDupAs(oElementMenu, sOption.substring(0,3), sOption.substring(4), oSubSectionMenu);
		}
		else if (sOption == 'FILTER') {
			SetFilterRep(oElementMenu, 'Component');
		}
		else if (sOption == 'SHOWPIVVAL') {
			oElementMenu.element.Rep_bPivotValue = !oElementMenu.element.Rep_bPivotValue;
			if (oElementMenu.element.Rep_bPivotValue) {
				oElementMenu.element.Rep_bEachPivot = false;
			}
		}
		else if (sOption == 'REPEATPIVOT') {
			oElementMenu.element.Rep_bEachPivot = !oElementMenu.element.Rep_bEachPivot;
		}
		else if (sOption == 'REPEATCUT') {
			oElementMenu.element.Rep_bEachCut = !oElementMenu.element.Rep_bEachCut;
		}
		else if (sOption == 'COPYELEM') {
			sMetaCopy = oElementMenu.save();
			sTypeCopy = oElementMenu.element.Rep_type;
		}
		else if (sOption == 'DELELEM') {
			DelElement(oElementMenu, oSubSectionMenu);
		}
		else if (sOption == 'LINKER_REP') {
			Linker_Reports(oElementMenu, oSubSectionMenu);
		}
		else if (sOption.indexOf("VDynProp_") == 0){
			VisCol(oElementMenu.element.Rep_id, sOption);
		}
		else if (sOption == 'LinkSect') {
			Link_Section(oElementMenu);
		}
		else if (sOption == 'ShowKeys') {
			oElementMenu.element.Rep_bShowKeys = !oElementMenu.element.Rep_bShowKeys;
		}
		else if (sOption == 'PIVRESUME') {
			oElementMenu.element.Rep_bPivResumen = !oElementMenu.element.Rep_bPivResumen;
		}
		else if (sOption.substring(0,5) == 'GRPP,') {
			oElementMenu.element.Rep_sPivotGroup = sOption.substring(5);
		}
		else if (sOption.substring(0,7) == 'alignm_' || sOption.substring(0,10) == 'adjustment') {
			SetOptionMultiple(null, sOption);
		}
		else if (sOption == 'VarName')
		{
			if (oElementMenu.element.Rep_bAsVariable)
			{
				aVarsNams[oElementMenu.element.Rep_name] = undefined;
			}
			else if (aVarsNams[oElementMenu.element.Rep_name] != undefined)
			{
				alert(ML[929]);
				EditFormat(oElementMenu);
				return;
			}

			oElementMenu.element.Rep_bAsVariable = !oElementMenu.element.Rep_bAsVariable;

			if (oElementMenu.element.Rep_bAsVariable)
			{
				aVarsNams[oElementMenu.element.Rep_name] = 1;
			}
		}
	}
	else if (oSectionMenu != null) {
		if (sOption == 'RP_BCKGCOLOR') {			
			if (oSectionMenu.oPart.divPart.Rep_backgroundColor != -1) {
				scolor = '#'+oSectionMenu.oPart.divPart.Rep_backgroundColor;
			}
			else {
				scolor = oSectionMenu.oPart.divPart.Rep_backgroundColor;
			}
			fnShowChooseColorDlg(scolor, sOption, '');
		}
		else if (sOption.substring(0,9) == 'RP_IMAGE_') {
			if (sOption == 'RP_IMAGE_SEL') {
				SelImage('ReportPart');
			}
			else if (sOption == 'RP_IMAGE_DEL') {
				ClearImage('ReportPart');
			}
			else if (sOption == 'RP_IMAGE_FIT') {
				oSectionMenu.oPart.divPart.Rep_bPicStreched = !oSectionMenu.oPart.divPart.Rep_bPicStreched;
				if (oSectionMenu.oPart.divPart.Rep_bVisible) {
					oSectionMenu.oPart.setBackgroundImage();
				}
			}
		}		
		else if (sOption == 'PASTEELEM') {
	    	oSectionMenu.oPart.Paste(sTypeCopy, sMetaCopy, bitamXPos, bitamYPos);
		}
		else if (sOption.substring(0,9) == 'DEL_COMP_') {
			DelCompsRep(sOption.substring(9));
		}
		else if (sOption.substring(3,7) == ',ADD') {
	    	var sSectionId2 = sSubSectionTypeMenu;
			var sSubSectionType2 = sSubSectionTypeMenu;
			mayAddElement(sSectionId2,sSubSectionType2,sOption,bitamXPos,bitamYPos);
		}
	}
	bRightClick = false;
}
function DelCompsRep(sOption){
	var iConfirm = (sOption=='ALL') ? 690 : 691;
	if (window.confirm(ML[692]+ML[iConfirm]+' ?')) {
		var bAll = (sOption=='ALL') ? true : false;
		if (compIndexOf(sSubSectionTypeMenu))
	    {
			oSectionMenu.oPart.delComps(bAll);
	    }
	    else {
	    	oSubSectionMenu.delComps(bAll);
	    }
	}
}
function fnShowChooseColorDlg(color,param,path){
	if (document.all) { //IE4 and up
		var args = new Array(3);
		args[0] = color;
		args[1] = window;
		args[2] = param;
		retVal = window.showModalDialog(
				path+'color_dialog.php',args,
				'dialogHeight: 380px; dialogWidth: 245px; center: yes; scroll: No; help:  No; resizable: Yes; status:no;');
	} else if (document.layers) {
	} else if (document.getElementById) { //mozilla
		var winRef;
		winRef = window.open(
				path+'color_dialog.php?'+escape(color)+'&'+param,"_blank",
				'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=245, height=380');
	}
}
function OnChangeColor(color, Option){
	if (color != '') {
		if (color != "-1") {
			if (color.indexOf('#') == 0) {
				color = color.substring(1);
			}
		}
		if (Option == 'RP_BCKGCOLOR') {
			if (compIndexOf(sSubSectionTypeMenu))
			{
				oSectionMenu.oPart.divPart.Rep_backgroundColor = color;
				if (oSectionMenu.oPart.divPart.Rep_bVisible) {
					oSectionMenu.oPart.setBackgroundColor();
				}
			}
			else {
				oSubSectionMenu.divPart.Rep_backgroundColor = color;
				if (oSubSectionMenu.divPart.Rep_bVisible) {
					oSubSectionMenu.setBackgroundColor();
				}
			}
		}
		else if (Option == 'BCKGCOLOR') {
			oElementMenu.element.Rep_BackColor = color;
			if (oElementMenu.element.Rep_visible) {
				oElementMenu.setBackgroundColor();
			}
			SetOptionMultiple(color, 'BCKGCOLOR');
		}
		else if (Option == 'BORDER_COLOR') {
			oElementMenu.element.Rep_BorderColor = color;
			if (oElementMenu.element.Rep_visible) {
				oElementMenu.setBorder();
			}
			SetOptionMultiple(color, 'BORDER_COLOR');
		}
		else if (Option == 'FONT_COLOR') {
			oElementMenu.element.Rep_ForeColor = color;
			if (oElementMenu.element.Rep_visible) {
				oElementMenu.setForeColor();
			}
			SetOptionMultiple(color, 'FONT_COLOR');
		}
		else if (Option == 'COL,Initial' || Option == 'COL,Final') {
			if (Option == 'COL,Initial') {
				oElementMenu.element.Rep_BackColor = color;
			}
			else {
				oElementMenu.element.Rep_ForeColor = color;
			}
			if (oElementMenu.element.Rep_visible) {
				oElementMenu.makeImage();
			}
		}
		else if (Option == 'PGB,COL,Initial' || Option == 'PGB,COL,Final') {
			if (Option == 'PGB,COL,Initial') {
				oElementMenu.element.Rep_color_I1 = color;
			}
			else {
				oElementMenu.element.Rep_color_I2 = color;
			}
		}
		else if (Option == 'GUIDE_COLOR') {
			oSectionMenu.divSection.ColorPautado = color;
		}
	}
}
function SetOptionMultiple(oValue, sOption)
{
	var objFirstComponent = null;

	for (var i in aMovingObject)
	{
		if (aMovingObject[i] != undefined && aMovingObject != null)
		{
			var oMovObjTempE = aMovingObject[i];
			if (oMovObjTempE.divOrig != null)
			{
				var sElemId = oMovObjTempE.divOrig.id
				var sSubSectionType = (isIE) ? oMovObjTempE.divOrig.parentElement.id : oMovObjTempE.divOrig.parentNode.id
				var osubsection = getSecPart(sSubSectionType, oMovObjTempE.divOrig)

			  	if (osubsection != null)
			  	{
					var nConsecElem = GetConsecElement(osubsection,sElemId);
					if (nConsecElem != -1)
					{
						var oElement = osubsection.arrElements[nConsecElem];

						if (oValue == null && (sOption.substring(0,7) == 'alignm_' || sOption.substring(0,10) == 'adjustment') &&
							objFirstComponent == null)
						{
							objFirstComponent = oElement;
							continue;
						}

						if (oElement.element.Rep_type == RepLabel || oElement.element.Rep_type == RepButton)
						{
							switch (sOption)
							{
								case 'IMAGE_FIT':
									oElement.element.Rep_bPicStreched = oValue;
									if (oElement.element.Rep_visible) {
										oElement.setBackgroundImage();
									}
									break;
								case 'SETIMG':
									oElement.element.Rep_Picture = oValue;
									if (oElement.element.Rep_visible) {
										oElement.setBackgroundImage();
									}
									break;
								case 'FONT_T':
									oElement.element.Rep_FontName = oValue;
									oElement.element.style.fontFamily = oElement.element.Rep_FontName;
									break;
								case 'FONT_W':
									oElement.element.Rep_FontSize = oValue;
									oElement.element.style.fontSize = oElement.element.Rep_FontSize+"px";
									break;
								case 'FONT_SB':
									oElement.element.Rep_FontBold = oValue;
									oElement.element.style.fontWeight = (oElement.element.Rep_FontBold)?'bold':'normal';
									break;
								case 'FONT_SI':
									oElement.element.Rep_FontItalic = oValue;
									oElement.element.style.fontStyle = (oElement.element.Rep_FontItalic)?'italic':'normal';
									break;
								case 'FONT_SU':
									oElement.element.Rep_FontUnderline = oValue;
									oElement.element.style.textDecoration = (oElement.element.Rep_FontUnderline)?'underline':'none';
									break;
								case 'FONT_COLOR':
									oElement.element.Rep_ForeColor = oValue;
									if (oElement.element.Rep_visible) {
										oElement.setForeColor();
									}
									break;
								case 'BORDER_W':
									oElement.element.Rep_BorderWidth = oValue;
									if (oElement.element.Rep_visible) {
										oElement.setBorder();
									}
									break;
								case 'BORDER_T':
									oElement.element.Rep_BorderType = oValue;
									if (oElement.element.Rep_visible) {
										oElement.setBorder();
									}
									break;
								case 'BORDER_COLOR':
									oElement.element.Rep_BorderColor = oValue;
									if (oElement.element.Rep_visible) {
										oElement.setBorder();
									}
									break;
								case 'ALIGN':
									oElement.element.Rep_Alignment = oValue;
									oElement.setTextAlign();
									break;
								case 'BCKGCOLOR':
									oElement.element.Rep_BackColor = oValue;
									if (oElement.element.Rep_visible) {
										oElement.setBackgroundColor();
									}
									break;
							}
						}
						switch (sOption)
						{
							case 'SHOWHIDE':
								oElement.element.Rep_visible = oValue;
								oElement.UpdateVisible();
								break;
							case 'alignm_left':
								oElement.element.Rep_left = objFirstComponent.element.Rep_left;
								oElement.element.style.left = oElement.element.Rep_left + "px";
								oMovObjTempE.setDivBackPos();
								break;
							case 'alignm_vert':
								var nLeftPos = parseInt(objFirstComponent.element.Rep_left);
								var nWidthPos = parseInt(objFirstComponent.element.Rep_width);
								var nCenterPos = (nLeftPos+(nWidthPos/2));

								var nWidthComp = parseInt(oElement.element.Rep_width);
								var nCenterComp = (nCenterPos-(nWidthComp/2));

								oElement.element.Rep_left = nCenterComp;
								oElement.element.style.left = oElement.element.Rep_left + "px";
								oMovObjTempE.setDivBackPos();
								break;
							case 'alignm_right':
								var nLeftPos = parseInt(objFirstComponent.element.Rep_left);
								var nWidthPos = parseInt(objFirstComponent.element.Rep_width);
								var nRightPos = (nLeftPos+nWidthPos);

								var nWidthComp = parseInt(oElement.element.Rep_width);
								var nRightComp = (nRightPos-nWidthComp);

								oElement.element.Rep_left = nRightComp;
								oElement.element.style.left = oElement.element.Rep_left + "px";
								oMovObjTempE.setDivBackPos();
								break;
							case 'alignm_up':
								oElement.element.Rep_top = objFirstComponent.element.Rep_top;
								oElement.element.style.top = oElement.element.Rep_top + "px";
								oMovObjTempE.setDivBackPos();
								break;
							case 'alignm_hor':
								var nTopPos = parseInt(objFirstComponent.element.Rep_top);
								var nHeightPos = parseInt(objFirstComponent.element.Rep_height);
								var nCenterPos = (nTopPos+(nHeightPos/2));

								var nHeightComp = parseInt(oElement.element.Rep_height);
								var nCenterComp = (nCenterPos-(nHeightComp/2));

								oElement.element.Rep_top = nCenterComp;
								oElement.element.style.top = oElement.element.Rep_top + "px";
								oMovObjTempE.setDivBackPos();
								break;
							case 'alignm_down':
								var nTopPos = parseInt(objFirstComponent.element.Rep_top);
								var nHeightPos = parseInt(objFirstComponent.element.Rep_height);
								var nBottomPos = (nTopPos+nHeightPos);

								var nHeightComp = parseInt(oElement.element.Rep_height);
								var nBottomComp = (nBottomPos-nHeightComp);

								oElement.element.Rep_top = nBottomComp;
								oElement.element.style.top = oElement.element.Rep_top + "px";
								oMovObjTempE.setDivBackPos();
								break;
							case 'adjustmentwidth':
								oElement.element.Rep_width = objFirstComponent.element.Rep_width;
								oElement.element.style.width = oElement.element.Rep_width + "px";
								oMovObjTempE.setDivBackPos();
								break;
							case 'adjustmentheight':
								oElement.element.Rep_height = objFirstComponent.element.Rep_height;
								oElement.element.style.height = oElement.element.Rep_height + "px";
								oMovObjTempE.setDivBackPos();
								break;
							case 'adjustmentsize':
								oElement.element.Rep_width = objFirstComponent.element.Rep_width;
								oElement.element.style.width = oElement.element.Rep_width + "px";

								oElement.element.Rep_height = objFirstComponent.element.Rep_height;
								oElement.element.style.height = oElement.element.Rep_height + "px";

								oMovObjTempE.setDivBackPos();
								break;
						}
					}
			  	}
			}
		}
	}
}

function SelImage(sTypeComp){
	// Lamar a forma en donde se seleccionará la imagen y retornará el url para ponerlo como fondo
  /*
	window.showModalDialog(
    'sel_image.php',
    [window,ML[362] + '->'+ML[361]],
		'dialogHeight: 470px; dialogWidth: 380px; center: yes; scroll: No; help:  No; resizable: Yes; status:no;'
  );
  */
  openWindowDialog(
    'sel_image.php',
    [window,ML[362] + '->' + ML[361]],
    [sTypeComp, oElementMenu, oSectionMenu, oSubSectionMenu],
    380, 470,
     dcb_selImageMenuComp,
    'dcb_selImageMenuComp'
  );
}

function dcb_selImageMenuComp(returnValue, aArgContinue)
{
  var sTypeComp = aArgContinue[0];
  var oElementMenu = aArgContinue[1];
  var oSectionMenu = aArgContinue[2];
  var oSubSectionMenu = aArgContinue[3];

  if (sTypeComp === 'Component') {
    if (oElementMenu.element.Rep_type !== RepImage) {
      oElementMenu.element.Rep_Picture = returnValue;
      if (oElementMenu.element.Rep_visible) {
        oElementMenu.setBackgroundImage();
      }
      SetOptionMultiple(oElementMenu.element.Rep_Picture, 'SETIMG');
    }
    else {
      oElementMenu.element.Rep_image = returnValue;
      oElementMenu.element.Rep_Picture = returnValue;
      if (oElementMenu.element.Rep_visible) {
        oElementMenu.setImage();
      }
    }
  }
  else if (sTypeComp === 'ReportPart') {
    oSectionMenu.oPart.divPart.Rep_backgroundImage = returnValue;
    if (oSectionMenu.oPart.divPart.Rep_bVisible) {
      oSectionMenu.oPart.setBackgroundImage();
    }
  }
  else if (sTypeComp === 'SubSection') {
    oSubSectionMenu.divPart.Rep_backgroundImage = returnValue;
    if (oSubSectionMenu.divPart.Rep_bVisible) {
      oSubSectionMenu.setBackgroundImage();
    }
  }
}

function ClearImage(sTypeComp){
	if (sTypeComp == 'Component') {
		oElementMenu.element.Rep_Picture = '';
		if (oElementMenu.element.Rep_visible) {
			oElementMenu.setBackgroundImage();
		}
		SetOptionMultiple(oElementMenu.element.Rep_Picture, 'SETIMG');
	}
	else if (sTypeComp == 'ReportPart') {
		oSectionMenu.oPart.divPart.Rep_backgroundImage = '';
		if (oSectionMenu.oPart.divPart.Rep_bVisible) {
			oSectionMenu.oPart.setBackgroundImage();
		}
	}
	else if (sTypeComp == 'SubSection') {
		oSubSectionMenu.divPart.Rep_backgroundImage = '';
		if (oSubSectionMenu.divPart.Rep_bVisible) {
			oSubSectionMenu.setBackgroundImage();
		}
	}
}

function SetLine(oElementAct, sOption) {
	if (sOption.substring(0,4) == 'STY,' || sOption.substring(0,4) == 'DEG,') {
		var aFN = oElementAct.element.Rep_FontName.split('@');
		if (sOption.substring(0,4) == 'STY,') {
			aFN[0] = sOption.substring(4);
		}
		else {
			aFN[1] = sOption.substring(4);
		}
		oElementAct.element.Rep_FontName = aFN.join('@');
		oElementAct.makeImage();
	}
	else if (sOption.substring(0,4) == 'COL,') {
		if (sOption.substring(4) == 'Initial') {
			var scolor = oElementAct.element.Rep_BackColor;
			if (oElementAct.element.Rep_BackColor != -1) {
				scolor = '#'+oElementAct.element.Rep_BackColor;
			}
			else {
				scolor = oElementAct.element.Rep_BackColor;
			}
		}
		else {
			var scolor = oElementAct.element.Rep_ForeColor;
			if (oElementAct.element.Rep_ForeColor != -1) {
				scolor = '#'+oElementAct.element.Rep_ForeColor;
			}
			else {
				scolor = oElementAct.element.Rep_ForeColor;
			}
		}
		fnShowChooseColorDlg(scolor, sOption, '');
	}
}
function EditElements(oElementAct, oSubSectionAct, oSectionAct){
	var sIndsIds = oSubSectionAct.getElementsWithWork(true,'','');
	var sSubSectionType = oSubSectionAct.divPart.id;
	if (sSubSectionType=='footer' || sSubSectionType=='header'
		|| sSubSectionType=='after' || sSubSectionType=='before')
	{
		var oSubSectionActbody = oSectionAct.body;
		var sIndsIds2 = oSubSectionActbody.getElementsWithWork(true,'body.',ML[581]+'.');
		if (sIndsIds2 != '') {
			if (sIndsIds != '') {
				sIndsIds = sIndsIds + '_AWSepElement_' + sIndsIds2;
			}
			else {
				sIndsIds = sIndsIds2;
			}
		}
	}
//debugger;
	var sElemsSel = oElementAct.element.Rep_sElems;
	var sTitle = ML[81]+'->'+ML[248];
  /*
  window.showModalDialog(
    "rep_edit_graph.php",
    [sTitle, window, sIndsIds, sElemsSel],
    "dialogHeight:320px; dialogWidth:600px; center:yes; help:no; resizable:yes; status:no"
  );
  */
  openWindowDialog(
    'rep_edit_graph.php',
    [sTitle, window, sIndsIds, sElemsSel],
    [ oElementAct ],
    600, 320,
     dcb_editElementsMenuComp,
    'dcb_editElementsMenuComp'
  );
  /*
	if(window.returnValue != null)
	{
		var aCadSetElems = new Array();
		var chartSource = oElementAct.element.Rep_ChartWizard;
		var aParams = [chartSource];  var aParamsAux = new Array();  var sSerieElemId = '';
		//General = 'arial-.ttf|8|0|-1|0_F__TitSup_arial-.ttf|8|0|-1|0_F__TitDer_arial-.ttf|8|0|-1|0_F_ _TitInf_arial-.ttf|8|0|-1|0_F_ _TitIzq_1_0_0,-1,0,-1,0,0,0,-1,0,0_16777183_16777183_20_30_0_0_0_1_00FF00_37@19@40@56_1_1_1_1_0_-1_1_1__0_arial-.ttf|8|0|10092543|0_F_top_arial-.ttf|8|0|16777215|0_F_none_|GENERAL|'
		var sGeneral = BITAMGetSiguiente(aParams, '|GENERAL|') + '|GENERAL|'
		//Series = IdElement+'_#ID#_'+IdElement+'_NameElement_S'+i+'_7770368_-1_0_bar@0@circle_arial-.ttf|8|0|-1|0_F_1_0_0_0_7770368@7770368_0.7_'
		var sSeries = BITAMGetSiguiente(aParams, '|SERIES|')
		var aSeries = sSeries.split('*SerSEP*');
		var aElemsSel = window.returnValue.split('_AWElem_');  var bExist = false;  var sNewSerie = '';
		var aNewSeries = new Array()

		var aColors = '7770368,2830038,7327737,12497049,8867367,14535349,5403620,11264092,6217679,1482490,13257415,13540262,10260442,7114237,11922418,10483707,2714793,7039926,15913007,12551522,7317111,4770252,6091180,23768,16111680,991697,5999,531689,320768,13774960,838863,2053888,695408,9154143,14777088,4227200,8905120,69280,6711680,255,711935,55135,9852568,314987,8759569,93478,6665972,580579,56974,894569,993178,110025,40009,658978,478989';
		aColors     = aColors.split(',');
//debugger;
		for(var i=0; i<aElemsSel.length; i++)
		{
			bExist = false;
			var aElemOpt = aElemsSel[i].split('_AWSepName_');
			var Color= (i>=aColors.length?aColors[0]:aColors[i]);
			// revisar si el elemento seleccionado existe en las series
			for(var j=0; j<aSeries.length; j++)
			{
				aParamsAux = [aSeries[j]];
				sSerieElemId = BITAMGetSiguiente(aParamsAux, '_Name_');
				var aX=[sSerieElemId];
				BITAMGetSiguiente(aX, '_#ID#_');

				sSerieElemId = aX[0];
				if (sSerieElemId == aElemOpt[0]) {
					// Si existe agregarlo al nuevo array
					var aAux = aParamsAux[0].split('_');
					aAux[0] = 'S'+i // contiene el consecutivo de S
					var sNewSerie = aElemOpt[1] + '_#ID#_' + sSerieElemId + '_Name_' + aAux.join('_');
					aNewSeries.push(sNewSerie);
					bExist = true;
					break;
				}
			}
			if (!bExist) {  // Si no existe agregar la nueva serie
				aNewSeries.push(aElemOpt[1]+'_#ID#_'+ aElemOpt[0]+'_Name_' +'S'+i+'_'+Color+'_-1_0_bar@0@circle_arial-.ttf|8|0|-1|0_F_1_0_0_0_'+Color+'@'+Color+'_0.7_');
			}
			aCadSetElems.push(aElemOpt[0]);
		}
		//Ejes='0_0_1_0_0_1_0_0_1_2_1_40_1_1_0___arial-.ttf|8|0|-1|0_F_*1_0_0_0_0_1_0_0_1_0_1_0_1_0_0___arial-.ttf|8|0|-1|0_F_*2_0_1_0_1_1_0_0_1_2_1_1_1_1_0___arial-.ttf|8|0|-1|0_F_|EJES|'
		//Params='INF|@||PARAMS|'+'_#ChartWizard#_'
		var sLast = aParams[0];

		oElementAct.element.Rep_sElems = aCadSetElems.join('_AWElem_');
		oElementAct.element.Rep_ChartWizard = sGeneral + aNewSeries.join('*SerSEP*') + '|SERIES|' + sLast;
	}
	*/
   window.returnValue = null;
}

function dcb_editElementsMenuComp(returnValue, aArgContinue){
  var oElementAct = aArgContinue[0];

  var aCadSetElems = new Array();
  var chartSource = oElementAct.element.Rep_ChartWizard;
  var aParams = [chartSource];  var aParamsAux = new Array();  var sSerieElemId = '';
  //General = 'arial-.ttf|8|0|-1|0_F__TitSup_arial-.ttf|8|0|-1|0_F__TitDer_arial-.ttf|8|0|-1|0_F_ _TitInf_arial-.ttf|8|0|-1|0_F_ _TitIzq_1_0_0,-1,0,-1,0,0,0,-1,0,0_16777183_16777183_20_30_0_0_0_1_00FF00_37@19@40@56_1_1_1_1_0_-1_1_1__0_arial-.ttf|8|0|10092543|0_F_top_arial-.ttf|8|0|16777215|0_F_none_|GENERAL|'
  var sGeneral = BITAMGetSiguiente(aParams, '|GENERAL|') + '|GENERAL|';
  //Series = IdElement+'_#ID#_'+IdElement+'_NameElement_S'+i+'_7770368_-1_0_bar@0@circle_arial-.ttf|8|0|-1|0_F_1_0_0_0_7770368@7770368_0.7_'
  var sSeries = BITAMGetSiguiente(aParams, '|SERIES|');
  var aSeries = sSeries.split('*SerSEP*');
  var aElemsSel = returnValue.split('_AWElem_');  var bExist = false;  var sNewSerie = '';
  var aNewSeries = new Array();

  var aColors = '7770368,2830038,7327737,12497049,8867367,14535349,5403620,11264092,6217679,1482490,13257415,13540262,10260442,7114237,11922418,10483707,2714793,7039926,15913007,12551522,7317111,4770252,6091180,23768,16111680,991697,5999,531689,320768,13774960,838863,2053888,695408,9154143,14777088,4227200,8905120,69280,6711680,255,711935,55135,9852568,314987,8759569,93478,6665972,580579,56974,894569,993178,110025,40009,658978,478989';
  aColors     = aColors.split(',');
  //debugger;
  for(var i=0; i<aElemsSel.length; i++)
  {
    bExist = false;
    var aElemOpt = aElemsSel[i].split('_AWSepName_');
    var Color= (i>=aColors.length?aColors[0]:aColors[i]);
    // revisar si el elemento seleccionado existe en las series
    for(var j=0; j<aSeries.length; j++)
    {
      aParamsAux = [aSeries[j]];
      sSerieElemId = BITAMGetSiguiente(aParamsAux, '_Name_');
      var aX=[sSerieElemId];
      BITAMGetSiguiente(aX, '_#ID#_');

      sSerieElemId = aX[0];
      if (sSerieElemId == aElemOpt[0]) {
        // Si existe agregarlo al nuevo array
        var aAux = aParamsAux[0].split('_');
        aAux[0] = 'S'+i; // contiene el consecutivo de S
        var sNewSerie = aElemOpt[1] + '_#ID#_' + sSerieElemId + '_Name_' + aAux.join('_');
        aNewSeries.push(sNewSerie);
        bExist = true;
        break;
      }
    }
    if (!bExist) {  // Si no existe agregar la nueva serie
      aNewSeries.push(aElemOpt[1]+'_#ID#_'+ aElemOpt[0]+'_Name_' +'S'+i+'_'+Color+'_-1_0_bar@0@circle_arial-.ttf|8|0|-1|0_F_1_0_0_0_'+Color+'@'+Color+'_0.7_');
    }
    aCadSetElems.push(aElemOpt[0]);
  }
  // Ejes='0_0_1_0_0_1_0_0_1_2_1_40_1_1_0___arial-.ttf|8|0|-1|0_F_*1_0_0_0_0_1_0_0_1_0_1_0_1_0_0___arial-.ttf|8|0|-1|0_F_*2_0_1_0_1_1_0_0_1_2_1_1_1_1_0___arial-.ttf|8|0|-1|0_F_|EJES|'
  // Params='INF|@||PARAMS|'+'_#ChartWizard#_'
  var sLast = aParams[0];
  oElementAct.element.Rep_sElems = aCadSetElems.join('_AWElem_');
  oElementAct.element.Rep_ChartWizard = sGeneral + aNewSeries.join('*SerSEP*') + '|SERIES|' + sLast;
}
function SaveTemplate(oSectionMenuAct){
	if (oSectionMenu.oPart.divPart.Rep_id == 'ReportHeader') {
		var sTypeRP = '4';
	}
	else if (oSectionMenu.oPart.divPart.Rep_id == 'ReportFoot') {
		var sTypeRP = '5';
	}
	else {
		return;
	}
	var sTemplateMD =  sTypeRP+','+oSectionMenu.oPart.saveTemplate(',');
	sNameToSave = '';//window.parent.sNameFav;

	openWindowDialog('saveas.php?_child=:child:', [window, sNameToSave, ML[570]],
				        [sTemplateMD], 400, 180, saveTemplateModalValues, 'saveTemplateModalValues');

}
function saveTemplateModalValues(sNewValues, aArgCont)
{
	if (sNewValues != undefined)
	{
		var sTemplateMD = aArgCont[0];

		GetElementWithId("ScenForm").target = "frameUpdate";
        var sEASMDAct = GetElementWithId("EASMD").value;
        var sNameFavAct = GetElementWithId("sNameFav").value;
        GetElementWithId("EASMD").value = sTemplateMD;
        GetElementWithId("sNameFav").value = sNewValues;
        GetElementWithId("ScenForm").action = "saveas2.php";
        GetElementWithId("ScenForm").submit();
        GetElementWithId("EASMD").value = sEASMDAct;
        GetElementWithId("sNameFav").value = sNameFavAct;

        window.returnValue = null;
	}
}
function DelTemplate(oSectionMenu, sKeyTemplate){
    if (oSectionMenu.oPart.divPart.Rep_id == 'ReportHeader') {
		var sTypeRP = '0';
	}
	else if (oSectionMenu.oPart.divPart.Rep_id == 'ReportFoot') {
		var sTypeRP = '3';
	}
	else {
		return;
	}
	if (window.confirm(ML[193]+' '+getNameTemplate('REP_TMP_DEL_'+sKeyTemplate, sTypeRP)+' ?')) {
	    GetElementWithId("key").value = sKeyTemplate;
	    GetElementWithId('Fconcept_cube').action = 'del_ana_fav.php';
		GetElementWithId('Fconcept_cube').submit();
		GetElementWithId("key").value = '';
    }
}
function GetTemplateDetail(oSectionMenu, sKeyTemplate){
    if (oSectionMenu.oPart.divPart.Rep_id == 'ReportHeader') {
		var sTypeRP = '0';
	}
	else if (oSectionMenu.oPart.divPart.Rep_id == 'ReportFoot') {
		var sTypeRP = '3';
	}
	else {
		return;
	}
	oSectionRestore = oSectionMenu;
	GetElementWithId("key").value = sKeyTemplate;
    GetElementWithId('Fconcept_cube').action = 'get_report_template.php';
	GetElementWithId('Fconcept_cube').submit();
	GetElementWithId("key").value = '';
}
function getNameTemplate(sKeyTemplate, sTypeRP){
	var sNameRet = '';
	if (sTypeRP == '0') {
		var aAuxK = aKeysREPSTmp_0[2];
		var aAuxN = aNamsREPSTmp_0[3];
	}
	else {
		var aAuxK = aKeysREPSTmp_3[2];
		var aAuxN = aNamsREPSTmp_3[3];
	}
	var nTop = aAuxK.length;
	for (var i=0; i<nTop; i++) {
		if (aAuxK[i] == sKeyTemplate) {
			sNameRet = aAuxN[i+1];
			break;
		}
	}
	return sNameRet;
}
function DelElement(oElementAct, oSubSectionAct){
	if (oMovingObject != null) {
		if (oMovingObject.divOrig == oElementAct.element) {
			oMovingObject.releaseMovingObject();
			oMovingObject = null;
		}
	}
	oSubSectionAct.delElement(oElementAct.element.id);

	var aSubsectionsTemp = new Array();
	var aElementsTemp = new Array();
	var j=0;
	for (var i in aMovingObject) {
		if (aMovingObject[i] != undefined && aMovingObject != null) {
			var oMovObjTempE = aMovingObject[i];
			if (oMovObjTempE.divOrig != null) {
				var sElemId = oMovObjTempE.divOrig.id
				var sSubSectionType = (isIE) ? oMovObjTempE.divOrig.parentElement.id : oMovObjTempE.divOrig.parentNode.id
				var osubsection = getSecPart(sSubSectionType, oMovObjTempE.divOrig)
			  	if (osubsection != null) {
					var nConsecElem = GetConsecElement(osubsection,sElemId);
					if (nConsecElem != -1) {
						var oElement = osubsection.arrElements[nConsecElem];
						aSubsectionsTemp[j] = osubsection;
						aElementsTemp[j] = oElement;
						j++;
					}
			  	}
			}
		}
	}
	releaseaMovObjs();
	aMovingObject = new Array();
	var nTop = aSubsectionsTemp.length;
	for (j=0; j<nTop; j++) {
		var osubsection = aSubsectionsTemp[j];
		var oElement = aElementsTemp[j];
		osubsection.delElement(oElement.element.id);
	}
	var aSubsectionsTemp = null;
	var aElementsTemp = null;
}
function SetMinHeight(oSectionAct) {
	labels=ML[742]+' &nbsp;&nbsp;('+ML[741]+')';
    types ="INP";
    valid ="numDec";
    values=''+(oSectionAct.divSection.nBodyMinH/96);

    openWindowDialog('ask_data.php', [ML[578]+'->'+ML[742], window, labels, types, values, valid], [oSectionAct],
					 400, 180, SetMinHeightModalValues);
}
function SetMinHeightModalValues(sNewValues, aArgCont)
{
	if (sNewValues != undefined)
	{
		var oSectionAct = aArgCont[0];
		oSectionAct.divSection.nBodyMinH = parseInt(parseFloat(sNewValues)*96);
	}
}
function SetName(oSectionAct)
{
  var sText = oSectionAct.divSection.section_name;
  var sTitle = ML[74];
  var sStyleText = 'arial_0_0_0_12_#FFFFFF_#000000';

  sStyleText = '?commStyle=NODISPLAYCTRL';

  openWindowDialog(
    jsDir+'comment.php'+sStyleText,
    [window, sText, sTitle,'SetText'],
    [oSectionAct],
    480, 250,
    SetNameModalValues,
    "SetNameModalValues"
  );
}

function SetNameModalValues(sNewValues, aArgCont)
{
  if (sNewValues !== undefined && sNewValues !== null)
  {
    var oSectionAct = aArgCont[0];
    var regexp = new RegExp("\n","gi");
    var sSectionName = sNewValues.replace(regexp,' ');
    //
    regexp = new RegExp("\r","gi");
    sSectionName = sSectionName.replace(regexp,'');
    oSectionAct.divSection.section_name = sSectionName.substring(0,80);
    oSectionAct.setSectionName();
  }
}

/***************** Funciones copiadas de otros archivos*/
function getValidImageSrc(src) {
	src = myReplace(src, '%', '%25');
	src = myReplace(src, '#', '%23');
	src = myReplace(src, ' ', '%20');
	src = myServerPath + src;
	return src;
}
function getSecPart(sSubSectionType, osrc){
	var osubsection = null
	// In Section
	if (compIndexOf(sSubSectionType)) { // In Report Part
		var osubsection = (aHTMLDivObjects[sSubSectionType] != undefined )?aHTMLDivObjects[sSubSectionType] : null;
		osubsection = osubsection.oPart
	}
	
	return osubsection;
}

function GetSubsection(sSectionId,sSubSectionType)
{
	var osubsection = null;
	return osubsection;
}

function GetSection(sSectionId)
{
	var osection = null;
	return osection;
}

function GetFonStyle(sParams) {
	var aParams = sParams.split('_#AWProps#_');
	aParams = aParams[1].split(',');
	var font_style = aParams[10]+'_'+aParams[6]+'_'+aParams[7]+'_'+aParams[8]+'_'+aParams[9];
	font_style = font_style+'_'+aParams[4]+'_'+aParams[5];  // Name_Bold_Italic_Underline_Size_BG_FC
	return font_style;
}

function mayAddElement(sSectionId,sSubSectionType,ElemType,xLeft,xTop){
	//debugger;
	var sTypeObj = '1'; // 0-Section, 1-Element, 2-Cut
	if (typeof sSectionId == "string") {
		if (compIndexOf(sSectionId)) {
			sTypeObj = '-1';
		}
	}
	
	if (currentEvent!=null)
	{
		var osrc = currentEvent.srcElement || currentEvent.target
		if (osrc.tagName == "IMG" &&
		   (osrc.id == "Graph" || osrc.id == "Image" || osrc.id == "Gauge" || osrc.id == "Line" || osrc.id == "PGBar"))
		{
			osrc = osrc.parentElement || osrc.parentNode
		}
		if (xLeft==null && xTop==null) {
			var oParentRect = osrc.getBoundingClientRect()
			xLeft = currentEvent.clientX - oParentRect.left
			xTop = currentEvent.clientY - oParentRect.top
		}			
	}
	
	if (sTypeObj == '-1') {
		var osubsection = (aHTMLDivObjects[sSectionId] != undefined )?aHTMLDivObjects[sSectionId] : null;
		osubsection = osubsection.oPart
	}
	else if (sTypeObj == '1') {
		var sElemId = osrc.id
		var sElemType = sSectionId
		var sSubSectionType = (isIE) ? osrc.parentElement.id : osrc.parentNode.id
		var sSectionId = (isIE) ? osrc.parentElement.getAttribute("sectionId") : osrc.parentNode.getAttribute("sectionId")
		var osubsection = getSecPart(sSubSectionType, osrc)
	}
	else {
		var osubsection = GetSubsection(sSectionId,sSubSectionType);
	}
  	if (osubsection != null) 
  	{
	 	var sParams = '';
		switch(ElemType) {
			case 'LBL,ADD':
		        var sPivots = ''; var osection = GetSection(sSectionId);
		        if (osection != null) { sPivots = osection.divSection.sPivots; }
		        sParams = ',,';
		        sParams = sParams + '_#AWProps#_'+xLeft+','+xTop+',80,20,-1,000000,0,0,0,11,'+sFontDefault+',,1,1,000000,0,1,1';
		        var font_style = GetFonStyle(sParams);
		        var sTitle = ML[101]+'->'+ML[584];
		       	openWindowDialog(
		          "lblTinyMce.php",
		          [sTitle, window, sParams, 0, '' ,sPivots],
		          [osubsection],
		          700, 350,
		          dlgCallBack_lblAddRptDragDrop,
		          "dlgCallBack_lblAddRptDragDrop"
		        );
				break;
			case 'IMG,ADD':
				openWindowDialog(
				'sel_image.php',
				[window,ML[362] + '->' + ML[361]],
				[sParams, xLeft, xTop, sFontDefault, osubsection],
				380, 470,
				dlgCallBack_ImgAddRptDragDrop,
				"dlgCallBack_ImgAddRptDragDrop"
				);
				break;
			case 'BTN,ADD':
		        var sPivots = ''; var osection = GetSection(sSectionId);
		        if (osection != null) { sPivots = osection.divSection.sPivots; }
		        sParams = ',,,,';
		        sParams = sParams + '_#AWProps#_'+xLeft+','+xTop+',80,20,f1f1f1,285768,1,0,0,11,arial,,1,1,a8adae,2,1,1';
		        var font_style = GetFonStyle(sParams);
		        var sTitle = ML[101]+'->'+ML['Button'];
		       	openWindowDialog(
		          "element_prop.php?elemStyle="+font_style,
		          [sTitle, window, sParams, RepButton, '' ,sPivots, '', aButtonElements],
		          [osubsection],
		          450, 600,
		          dlgCallBack_btnAddRptDragDrop,
		          "dlgCallBack_btnAddRptDragDrop"
		        );
				break;
			case 'LIN,ADD':
		        sParams = ',,,'+'_#AWProps#_'+xLeft+','+xTop+',150,8,FFFFFF,FF0000,0,0,0,11,Grad@Hor,,1,1,000000,0,1,1';
		        var font_style = GetFonStyle(sParams);
		        var sTitle = ML[101]+'->'+ML[218];
		        openWindowDialog(
		          "element_prop.php?elemStyle="+font_style,
		          [sTitle, window, sParams, 15],
		          [osubsection],
		          450, 580,
		          dlgCallBack_linAddRptDragDrop,
		          "dlgCallBack_linAddRptDragDrop"
		        );
		        break;
		}
  	}
}

function dlgCallBack_lblAddRptDragDrop(returnValue, aArgContinue)
{
  if (returnValue !== undefined && returnValue !== '')
  { //debugger;
    var osubsection = aArgContinue[0];
    osubsection.addElement(0, returnValue, false);
  }
}

function dlgCallBack_btnAddRptDragDrop(returnValue, aArgContinue)
{
  if (returnValue !== undefined && returnValue !== '')
  {
    var osubsection = aArgContinue[0];
    osubsection.addElement(100, returnValue, false);
  }
}

function dlgCallBack_ImgAddRptDragDrop(returnValue, aArgContinue)
{
  if (returnValue !== undefined && returnValue !== '')
  {
    var sParams = aArgContinue[0];
    var xLeft = aArgContinue[1];
    var xTop = aArgContinue[2];
    var sFontDefault = aArgContinue[3];
    var osubsection = aArgContinue[4];

    sParams = ',,'+returnValue+','+'_#AWProps#_'+xLeft+','+xTop+',50,50,-1,-1,0,0,0,11,'+sFontDefault+','+returnValue+',1,1,000000,0,1,1';
    osubsection.addElement(8,sParams,false);
  }
}

function GetConsecElement(objSubSection,sElemId)
{
  var nReturn = -1;
  if (objSubSection.arrElementsIds[sElemId] != undefined && objSubSection.arrElementsIds[sElemId] != null)
  {
	  var nConsec = objSubSection.arrElementsIds[sElemId];
	 	if (objSubSection.arrElements[nConsec] != undefined & objSubSection.arrElements[nConsec] != null)
	 	{
		  nReturn = nConsec;
	 	}
  }
  return nReturn;
}

function mayProperties(sSectionId,sSubSectionType,sOption){
	var sTypeObj = '1'; // -1-Report(HTSF), 0-Section, 1-Element, 2-Cut
	if (typeof sSectionId == "string") {
		if (compIndexOf(sSectionId)) {
			sTypeObj = '-1';
		}
	}
	if (sTypeObj == '-1') {
		var oRepSec = GetReportSection(sSectionId);
		var sParams = oRepSec.oPart.save(false,'_AWSep_');
  	 	var sTitle = ML[575]+'->';
		window.showModalDialog("section_prop.php",[sTitle,window,sParams],
				"dialogHeight:320px; dialogWidth:320px; center:yes; help:no; resizable:yes; status:no");
		if(window.returnValue != null && sParams != window.returnValue) {
			oRepSec.oPart.restoreSubsection(window.returnValue.split('_AWSep_'));
		}
		window.returnValue = null;
	}
	else {
		var osrc = currentEvent.srcElement || currentEvent.target
		if (sTypeObj == '1') {
			if (osrc.tagName == "IMG" &&
			   (osrc.id == "Graph" || osrc.id == "Image" || osrc.id == "Gauge" || osrc.id == "Line" || osrc.id == "PGBar"))
			{
				osrc = osrc.parentElement || osrc.parentNode
			}
			else
			{
				while(osrc.tagName!='DIV' && typeof(osrc.Rep_type)=="undefined" && osrc.tagName!="BODY")
				{
					osrc = osrc.parentElement || osrc.parentNode
				}
			}
			
			var sElemId = osrc.id
			var sElemType = sSectionId
			var sSubSectionType = (isIE) ? osrc.parentElement.id : osrc.parentNode.id
			var sSectionId = (isIE) ? osrc.parentElement.getAttribute("sectionId") : osrc.parentNode.getAttribute("sectionId")
			var osubsection = getSecPart(sSubSectionType, osrc)
		}
	  	if (osubsection != null) {
	  	 	var bOk = true;
	  		if (sTypeObj == '1') {
	  	 		bOk = false;
	  	 		var nConsecElem = GetConsecElement(osubsection,sElemId);
				if (nConsecElem != -1) {
					var oElement = osubsection.arrElements[nConsecElem];
					bOk = true;
				}
	  	 	}
	  	 	if (bOk) {
	  	 		var osection = GetSection(sSectionId);
				var nCubeKey = -1;  var sReportType = 0;
				var elemType = oElement.element.Rep_type;
				var sParams = oElement.save();
				var sTitle = ML[101]+'->';
				var sDimsKeys = ''; var sIndsIds = ''; var sPivots = '';
				switch(elemType){
					case RepLabel:
						sTitle = sTitle + ML[584];
						if (osection != null) { sPivots = osection.divSection.sPivots; }
						break;
					case RepImage:
						sTitle = sTitle + ML[361];
						break;
					case RepLine:
						sTitle = sTitle + ML[218];
						break;
					case RepButton:
						sTitle = sTitle + ML['Button'];
						break;						
				}
	            var font_style = GetFonStyle(sParams);
	            if(elemType==RepLabel)
	            {
		            openWindowDialog(
		              "lblTinyMce.php?elemStyle="+font_style+"&elemType="+elemType+"&Cube="+nCubeKey+"&Dims="+sDimsKeys+"&sPivDims="+sPivots,
		              [sTitle, window, sParams, elemType, sIndsIds, sPivots, sSubSectionType, aButtonElements],
		              [sParams, oElement, elemType, sSubSectionType, osrc],
		              700, 350,
		              dlgCallBack_MayPropDimCut,
		              "dlgCallBack_MayPropDimCut"
		            );
	            }
	            else
	            {
		            openWindowDialog(
		              "element_prop.php?elemStyle="+font_style+"&elemType="+elemType+"&Cube="+nCubeKey+"&Dims="+sDimsKeys+"&sPivDims="+sPivots,
		              [sTitle, window, sParams, elemType, sIndsIds, sPivots, sSubSectionType, aButtonElements],
		              [sParams, oElement, elemType, sSubSectionType, osrc],
		              450, (elemType === RepLine ? 510 : 600),
		              dlgCallBack_MayPropDimCut,
		              "dlgCallBack_MayPropDimCut"
		            );
	            }
	  	 	}
		}
	}
}

function dlgCallBack_MayPropDimCut(returnValue, aArgContinue)
{
  if (returnValue !== undefined && returnValue !== null && returnValue !== '')
  {
    var sParams = aArgContinue[0];

    if(returnValue !== sParams) {
      var oElement = aArgContinue[1];
      var elemType = aArgContinue[2];
      var sSubSectionType = aArgContinue[3];
      var osrc = aArgContinue[4];

      oElement.restore(returnValue, true);
      // Reset Moving Object
      if (oMovingObject !== null) {
        if (oMovingObject.divOrig === oElement.element) {
          oMovingObject.setDivBackPos();
        }
      }
    }
  }
}

function body_keydown(evt)
  {
//debugger;
	var bCalendarVisible = false;
	var bMenuVisible = false;

	if (!bCalendarVisible && !bMenuVisible) {
	  	e = (window.event)?window.event:evt;
		var key = (e.keyCode==0)?e.which:e.keyCode;	//codigo de tecla.
		var osrc = evt.srcElement || evt.target
		if (key==46 || (key>=37 && key<=40)) {
			if (oMovingObject != null) { //  && oMovingObject.divOrig == osrc
				var sElemId = oMovingObject.divOrig.id
				var sSubSectionType = (isIE) ? oMovingObject.divOrig.parentElement.id : oMovingObject.divOrig.parentNode.id
				var osubsection = getSecPart(sSubSectionType, oMovingObject.divOrig)
			  	if (osubsection != null) {
					var nConsecElem = GetConsecElement(osubsection,sElemId);
					if (nConsecElem != -1) {
						var oElement = osubsection.arrElements[nConsecElem];
						if (key==46) {
							DelElement(oElement, osubsection);
						}
						else if (key>=37 && key<=40){
							MoveElement(oElement, oMovingObject, key);
						}
					}
			  	}
		  	}
		}
		else if (key==17) {
			bCtrlPressed = true;
//window.parent.document.title = 'A:'+getLength(aMovingObject);
		}
  	}
  }
  function body_keyup(evt)
  {
	e = (window.event)?window.event:evt;
	var key = (e.keyCode==0)?e.which:e.keyCode;	//codigo de tecla.
	var osrc = evt.srcElement || evt.target
	if (key==17) {
		bCtrlPressed = false;
//window.parent.document.title = 'C:'+getLength(aMovingObject);
	}
  }
  // end other functions

  // assign events (begin)
  if (isIE)
  {
    document.documentElement.attachEvent("onselectstart", function(){return false})
    document.body.attachEvent("onkeydown", body_keydown)
    document.body.attachEvent("onkeyup", body_keyup)
  }
  else
  {
  	document.documentElement.addEventListener("keydown", body_keydown, false)
  	document.documentElement.addEventListener("keyup", body_keyup, false)
  }
  // assign events (end)
  
  function compIndexOf(str)
  {
  	return (str.indexOf('FormattedText')==0 || str.indexOf('HTMLHeader')==0 || str.indexOf('HTMLFooter')==0 || str.indexOf('SurveyImageText')==0  || str.indexOf('QuestionImageText')==0 || str.indexOf('QuestionMessage')==0 || str.indexOf('DisplayImage')==0 || str.indexOf('HTMLText')==0 || str.indexOf('MenuImageText')==0 || str.indexOf('EmailBody')==0 )
  }