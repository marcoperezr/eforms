/*
File: dialogs.js
Version: 2006/03/13 17:01
Copyright 2006 Bitam
*/

var _dlgRef = null;
var _dlgInput = null;
var _dlgOutput = '^#NuLl}@';
var _dlgFunction = null;
var _dlgClosed = null;
var _dlgIntervalId = null;
var _dlgContinue = null;

var _winModal = "";
var _winModalReturn = "null";

var bShowModalDialog = (window.showModalDialog);
var bLocalStorage    = (typeof(window.localStorage) !== 'undefined');
var bWindowOpener    = (window.opener !== null);
var bSaveInLocal     = (bLocalStorage && !bShowModalDialog);

/*
alert('bShowModalDialog:'+bShowModalDialog);
alert('window.opener:'+window.opener);
alert('bLocalStorage:'+bLocalStorage);
alert('bSaveInLocal:'+bSaveInLocal);
*/

if (bSaveInLocal)
{

  // _winModal = window.localStorage.getItem('_winModal');
  // if (_winModal === null)
  // {
  //   _winModal = 0;
  // }
  // else
  // {
  //   _winModalReturn = _winModal;
  //   _winModal++;
  // }
  // window.localStorage.setItem('_winModal', _winModal);

  // newLocalStorageEntry();

  _winModalReturn = window.name;
}

function newLocalStorageEntry(){
  var dt = new Date();
  _winModal = "00000000" + dt.getTime();
  _winModal = _winModal.substring(_winModal.length - 8);

  // window.localStorage.setItem(_winModal+'_dlgRef',null);
  window.localStorage.setItem(_winModal+'_dlgInput',null);
  window.localStorage.setItem(_winModal+'_dlgOutput','^#NuLl}@');
  window.localStorage.setItem(_winModal+'_dlgFunction',null);
  // window.localStorage.setItem(_winModal+'_dlgIntervalId',null);
  window.localStorage.setItem(_winModal+'_dlgClosed',false);
  // window.localStorage.setItem(_winModal+'_dlgContinue',null);
}

function clearLocalStorageEntry(){
  // window.localStorage.removeItem(_winModal+'_dlgRef');
  window.localStorage.removeItem(_winModal+'_dlgInput');
  window.localStorage.removeItem(_winModal+'_dlgOutput');
  window.localStorage.removeItem(_winModal+'_dlgFunction');
  // window.localStorage.removeItem(_winModal+'_dlgIntervalId');
  window.localStorage.removeItem(_winModal+'_dlgClosed');
}

// Functions to be called from the opened dialog
function openWindowDialog(anURL, aParams, aArgCont, aWidth, aHeight, processFunction, processFunctionString)
{
  var winOpts = '';

  if (_dlgIntervalId !== null){
    alert(ML[970]);
    return;
  }

  _dlgInput = aParams;
  _dlgContinue = aArgCont;
  _dlgOutput = '^#NuLl}@';

  if (bShowModalDialog) // si soporta ventanas modales
  {
    winOpts  = 'dialogWidth: ' + aWidth + 'px; dialogHeight: ' + aHeight + 'px;center:no;help:no;';
    winOpts += ';dialogLeft=' + (window.parent.screenLeft + ((window.parent.document.body.offsetWidth - aWidth) / 2)) + 'px;';
    winOpts += ';dialogTop=' + (window.parent.screenTop + ((window.parent.document.body.offsetHeight - aHeight) / 2)) + 'px';

    window.returnValue = null;
    _dlgOutput = window.showModalDialog(anURL, _dlgInput, winOpts);

    if (_dlgOutput !== undefined &&
        _dlgOutput !== null &&
        processFunction !== undefined &&
        processFunction !== null)
    {
      processFunction(_dlgOutput, aArgCont);
    }

    _dlgFunction = _dlgContinue = _dlgOutput = null;
  }
  else
  {
    if (bSaveInLocal)
    {
      newLocalStorageEntry();
      //
      window.localStorage.setItem(_winModal+'_dlgInput', setArrayToString(_dlgInput, 0)); //_dlgInput.join('_|AWSMD|_')
      // window.localStorage.setItem(_winModal+'_dlgContinue', setArrayToString(_dlgContinue, 0));
      window.localStorage.setItem(_winModal+'_dlgOutput', _dlgOutput);
    }

    winOpts  = 'width=' + aWidth + ',height=' + aHeight;
    winOpts += ',left=' + (window.parent.screenX + ((window.parent.outerWidth - aWidth) / 2));
    winOpts += ',top=' + (window.parent.screenY + ((window.parent.outerHeight - aHeight) / 2));

    _dlgClosed = false;
    _dlgFunction = processFunction;
    _dlgIntervalId = setInterval(processDialogFunction, 500);

    if (bSaveInLocal)
    {
      _dlgFunction = processFunctionString;
      window.localStorage.setItem(_winModal+'_dlgClosed',_dlgClosed);
      window.localStorage.setItem(_winModal+'_dlgFunction',_dlgFunction);
      // window.localStorage.setItem(_winModal+'_dlgIntervalId',_dlgIntervalId);
    }

    _dlgRef = window.open(anURL, _winModal, winOpts + ",modal,dialog");

    /*
    if (bSaveInLocal)
    {
      window.localStorage.setItem(_winModal+'_dlgRef',_dlgRef);
    }
    */
  }
}

// OBTENER LOS ARGUMENTOS
function getDialogArgument()
{
  var dialogArguments;

  if (bShowModalDialog)
  {
    dialogArguments = window.dialogArguments;
  }
  else if (window.opener)
  {
    dialogArguments = window.opener._dlgInput;
  }
  else if (bSaveInLocal)
  {
    dialogArguments = window.localStorage.getItem(_winModalReturn+'_dlgInput');
    dialogArguments = setStringToArray(dialogArguments, 0);
  }
  else
  {
    dialogArguments = window.dialogArguments;
  }
  return (dialogArguments);
}

// var ncountmsg = 0;
function processDialogFunction()
{
  /*
  if (ncountmsg < 3)
  {
    alert('_dlgRef:'+typeof(_dlgRef)+
        ' | _dlgRef.closed:'+typeof(_dlgRef.closed)+
        ' | _dlgClosed:'+_dlgClosed+
        ' | bSaveInLocal:'+bSaveInLocal+
        ' | _dlgRef:'+window.localStorage.getItem(_winModal+'_dlgRef'));

    ncountmsg++;
  }
  */

  /*
  if (_dlgRef === null || !_dlgRef.closed) return;
  if (!_dlgClosed) return;
  */

  if (_dlgRef === null) return;
  if (!_dlgRef.closed && !_dlgClosed) return;

  /*
  if (bSaveInLocal)
  {
    _dlgRef = window.localStorage.getItem(_winModal+'_dlgRef');
    if (_dlgRef !== 'null')
      return;
  }
  */

  if (bSaveInLocal)
  {
    // _dlgIntervalId = window.localStorage.getItem(_winModal+'_dlgIntervalId');
    // window.localStorage.setItem(_winModal+'_dlgIntervalId',null);
    _dlgFunction = window.localStorage.getItem(_winModal+'_dlgFunction');
    // window.localStorage.setItem(_winModal+'_dlgFunction',null);
    _dlgOutput   = window.localStorage.getItem(_winModal+'_dlgOutput');
    // window.localStorage.setItem(_winModal+'_dlgOutput','^#NuLl}@');
    // window.localStorage.setItem(_winModal+'_dlgRef',null);
    //
    // _dlgContinue   = window.localStorage.getItem(_winModal+'_dlgContinue');
    // _dlgContinue   = setStringToArray(_dlgContinue, 0);
    // window.localStorage.setItem(_winModal+'_dlgContinue',null);
    clearLocalStorageEntry();
  }

  if (_dlgIntervalId !== null) clearInterval(_dlgIntervalId);

  _dlgRef = _dlgIntervalId = null;

  if (_dlgOutput === '^#NuLl}@') return;

  if(_dlgOutput.indexOf('_|AWSMD:0|_') !== -1){
      _dlgOutput = setStringToArray(_dlgOutput, 0);
  }

  if (window.opener)
  {
    if (_dlgFunction !== null)
    {
      if (typeof(_dlgFunction) === "string" && _dlgFunction !== '')
        eval(_dlgFunction+'(_dlgOutput, _dlgContinue);');
      else
        _dlgFunction(_dlgOutput, _dlgContinue);
    }
    return;
  }

  if (bSaveInLocal)
  {
    if (_dlgFunction !== null && _dlgFunction !== '')
    {
      eval(_dlgFunction+'(_dlgOutput, _dlgContinue);');
    }
    return;
  }

  if (_dlgFunction !== null)
  {
    _dlgFunction(_dlgOutput, _dlgContinue);
  }

  _dlgFunction = _dlgContinue = _dlgOutput = null;
}

function setDialogReturnValue(returnValue)
{
  if (bShowModalDialog)
  {
    window.returnValue = returnValue;
  }
  else
  {
    if (window.opener)
    {
      window.opener._dlgOutput = returnValue;
    }

    if (bSaveInLocal)
    {
      if(typeof(returnValue) === 'object'){
        returnValue = setArrayToString(returnValue, 0);
      }
      window.localStorage.setItem(_winModalReturn+'_dlgOutput',returnValue);
    }
  }
}

function closeDialog(objBtn)
{
  if (!bShowModalDialog)
  {
    if (window.opener)
    {
      window.opener._dlgClosed = true;
    }

    if (bSaveInLocal)
    {
      // window.localStorage.setItem(_winModalReturn+'_dlgRef', null);
      window.localStorage.setItem(_winModalReturn+'_dlgClosed', true);

      /*
      window.localStorage.removeItem(_winModal+'_dlgInput');
      window.localStorage.removeItem(_winModal+'_dlgOutput');
      window.localStorage.removeItem(_winModal+'_dlgFunction');
      window.localStorage.removeItem(_winModal+'_dlgClosed',false);
      window.localStorage.removeItem(_winModal+'_dlgIntervalId');
      window.localStorage.removeItem(_winModal+'_dlgRef');
      // window.localStorage.removeItem(_winModal+'_dlgContinue');
      // window.localStorage.setItem('_winModal', _winModalReturn);
      */

      if (objBtn)
      {
        objBtn.style.display = 'none';
      }
    }
  }

  window.close();
}

function setArrayToString(aInfo, level)
{
  if (level === 5)
  {
    return aInfo.join('_|AWSMD:'+level+'|_');
  }

  var aReturn = new Array();
  var nTop = aInfo.length;
  for (var i = 0; i < nTop; i++)
  {
    /*
    if(level === 1 && i === 2){
      console.log('level: [' + level + ']   i: [' + i + ']');
    }
    */
    if (aInfo[i] !== null && typeof(aInfo[i]) === 'object' && aInfo[i].length)
    {
      aReturn[i] = setArrayToString(aInfo[i], (level+1));
    }
    else
    {
      aReturn[i] = aInfo[i];
    }
  }
  return aReturn.join('_|AWSMD:'+level+'|_');
}

function setStringToArray(sInfo, level)
{
  if (level === 5)
  {
    return sInfo.split('_|AWSMD:'+level+'|_');
  }

  var aReturn = sInfo.split('_|AWSMD:'+level+'|_');
  var nTop = aReturn.length;
  for (var i = 0; i < nTop; i++)
  {
    if (aReturn[i].indexOf('_|AWSMD:'+(level+1)+'|_') !== -1)
    {
      aReturn[i] = setStringToArray(aReturn[i], (level + 1));
    }
    /*
    else
    {
      aReturn[i] = aInfo[i];
    }
    */
  }
  return aReturn;
}
