var lang='BE';
var ML = new Array();
ML['lang'] = lang;
ML[0]='January';
ML[1]='February';
ML[2]='March';
ML[3]='April';
ML[4]='May';
ML[5]='June';
ML[6]='July';
ML[7]='August';
ML[8]='September';
ML[9]='October';
ML[10]='November';
ML[11]='December';
ML[12]='Jan';
ML[13]='Feb';
ML[14]='Mar';
ML[15]='Apr';
ML[16]='May';
ML[17]='Jun';
ML[18]='Jul';
ML[19]='Aug';
ML[20]='Sep';
ML[21]='Oct';
ML[22]='Nov';
ML[23]='Dec';
ML[24]='Sunday';
ML[25]='Monday';
ML[26]='Tuesday';
ML[27]='Wednesday';
ML[28]='Thursday';
ML[29]='Friday';
ML[30]='Saturday';
ML[31]='Sun';
ML[32]='Mon';
ML[33]='Tue';
ML[34]='Wed';
ML[35]='Thu';
ML[36]='Fri';
ML[37]='Sat';
ML[38]='Add Pivot Dimension';


var MONTHS = ML.slice(0,12);
var MONTHSABR =ML.slice(12,25);
var aDAYS = ML.slice(24,31);
var aDAYSABR =ML.slice(31,38);

var sTBPeriod_Key = 4;
var sTBPeriod_CatVar = false;
var regexp = new RegExp('-',"gi");
var nValueBitamPromptDialog = 0;

image_folder = 'images/';
checkMenuImage = 'images/tick.gif'
transparentImage = 'images/blankimage.gif'
subMenuImage = 'images/newoption.gif';

function getWeek(year,month,day)
{
	var when, newYear, offset, daynum, weeknum, prevNewYear, prevOffset;
	when = new Date(year,month,day);
	newYear = new Date(year,0,1);
	offset = 7 + 1 - newYear.getDay();
	if (offset == 8)
		offset = 1;
	daynum = ((Date.UTC(year, when.getMonth(), when.getDate(),0,0,0) - Date.UTC(year,0,1,0,0,0)) /1000/60/60/24) + 1;
	weeknum = Math.floor((daynum-offset+7)/7);
	if (weeknum == 0)
	{
		year--;
		prevNewYear = new Date(year,0,1);
		prevOffset = 7 + 1 - prevNewYear.getDay();
		if (prevOffset == 2 || prevOffset == 8)
			weeknum = 53 ;
		else
			weeknum = 52;
	}
	return weeknum;
}
function sortNumber2(a,b)
{
	return a - b;
}

function getStrDate (aDateObj) {
	if (!aDateObj) {
		aDateObj = new Date();
	}
	var dd = '' + aDateObj.getDate();
	var mm = '' + (aDateObj.getMonth()+1); //January is 0!

	var yyyy = '' + aDateObj.getFullYear();
	if(dd<10){
		dd='0'+dd
	} 
	if(mm<10){
		mm='0'+mm
	}
	var strDate = yyyy + mm + dd;
	return strDate;
}

function formatAMPM(strHour) {
	if (!strHour) {
		date = new Date();
		var hours = date.getHours();
		var minutes = date.getMinutes();
	} else {
		var hours = parseInt(strHour.substring(0,2), 10);
		var minutes = parseInt(strHour.substring(3,5), 10);
	}
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	hours = hours < 10 ? '0'+hours : hours;
	minutes = minutes < 10 ? '0'+minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;
	return strTime;
}

function getFormattedDate (aDate, aFormat, withTime) {
	var strDate = getStrDate(aDate);
	var strFormat = (aFormat) ? aFormat	: ((msDefaultDateFmt) ? msDefaultDateFmt : 'yyyy/MM/dd');
	var formattedDate = formatDate(strDate, strFormat);
	return formattedDate;
}

function formatDate(aDate,theFormat) //agregado parametro theFormat
{
	var months
	var weeknum
	//var formated = theFormat.toLowerCase();
	var formated = theFormat.split('"');
	var ilen = formated.length;
	var brep = true;

	var yy = aDate.substring(0,4);
    mm = aDate.substring(4,6);
    dd = aDate.substring(6,8);
    hh = aDate.substring(9,11);

    if (aDate.indexOf("ActRel") > -1)
	{	index = aDate.indexOf(",")
		cKey = parseInt(aDate.substring(index + 1), 10)
		return sActRel + ' ' + cKey
	}
	else
	{	if (aDate.indexOf("Actual") > -1)
		{	index = aDate.indexOf(",")
			cKey = parseInt(aDate.substring(index + 1), 10)
			return sActual + ' ' + cKey
		}
	}


	for (ij=0; ij<ilen; ij++) {
		if (brep){
			formated[ij] = formated[ij].toLowerCase();
			aFormatedAux = formated[ij].split(/[^hwdmyq]+/);	// Dividir cadena [ que NO CONTENGA(^) los elementos ] NO IMPORTA QUE SE REPITAN (+)
			var nSizeDF = formated[ij].length;
			if (nSizeDF > 4 && formated[ij].search(/[^hwdmyq]+/) == -1) { // No regresó ningún separador p.e. yyyymmdd
				var aDatesFormated = ["hh","h","ww","dddd","ddd","dd","d","mmmm","mmm","mm","m","yyyy","yy","q"];
				var nTopDF = aDatesFormated.length;
				var sFormatedTemp = formated[ij];
				var aTempDF = new Array(); aTempIndexDF = new Array(); var regexpDF;
				for (var iDF=0; iDF<nTopDF; iDF++) {
					var nPos = sFormatedTemp.indexOf(aDatesFormated[iDF]);
					if (nPos != -1) {
						aTempIndexDF.push(nPos);
						aTempDF[nPos] = aDatesFormated[iDF];

						regexpDF = new RegExp(aDatesFormated[iDF].substring(0,1),"gi");
						sFormatedTemp = sFormatedTemp.replace(regexpDF, 'i');
					}
				}
				aTempIndexDF.sort(sortNumber2);
				nTopDF = aTempIndexDF.length;
				for (iDF=0; iDF<nTopDF; iDF++) {
					aFormatedAux[iDF] = aTempDF[aTempIndexDF[iDF]];
				}
			}
			ipos = 0;
			sFormated = '';
			itop = aFormatedAux.length;
			for (kl=0; kl<itop; kl++) {
				sFormatedType = aFormatedAux[kl];
				switch (aFormatedAux[kl]){
					case "hh":
						aFormatedAux[kl] = hh;
						break;
					case "h":
						shh = parseInt(hh, 10);
						aFormatedAux[kl] = shh;
						break;
					case "ww":
						var init_period = getWeek(parseInt(yy, 10), parseInt(0, 10), parseInt(1, 10));
						var iAddW = 0;
						if (init_period > 1){
							iAddW++;
						}
						weeknum = getWeek(parseInt(yy, 10), parseInt(mm, 10)-1, parseInt(dd, 10));
						aFormatedAux[kl] = weeknum+iAddW;
						break;
			    	case "dddd":
			    		var DateAux = new Date(parseInt(yy, 10), (parseInt(mm, 10)-1), parseInt(dd, 10));
			    		dayname = aDAYS[DateAux.getDay()];
			    		aFormatedAux[kl] = dayname;
			    		DateAux = null;
			    		break;
			    	case "ddd":
				    	var DateAux = new Date(parseInt(yy, 10), (parseInt(mm, 10)-1), parseInt(dd, 10));
				    	dayname = aDAYSABR[DateAux.getDay()];
				    	aFormatedAux[kl] = dayname;
				    	DateAux = null;
				    	break;
			    	case "dd":
						aFormatedAux[kl] = dd;
						break;
			    	case "d":
						sdd = parseInt(dd, 10);
						aFormatedAux[kl] = sdd;
						break;
					case "mmmm":
						smm = parseInt(mm, 10);
						months = [ null, GetMonthDate(1), GetMonthDate(2), GetMonthDate(3), GetMonthDate(4), GetMonthDate(5), GetMonthDate(6), GetMonthDate(7), GetMonthDate(8), GetMonthDate(9), GetMonthDate(10), GetMonthDate(11), GetMonthDate(12)];
						smm = months[smm];
						aFormatedAux[kl] = smm;
						break;
					case "mmm":
						smm = parseInt(mm, 10);
						months = [ null, GetAbrMonthDate(1), GetAbrMonthDate(2), GetAbrMonthDate(3), GetAbrMonthDate(4), GetAbrMonthDate(5), GetAbrMonthDate(6), GetAbrMonthDate(7), GetAbrMonthDate(8), GetAbrMonthDate(9), GetAbrMonthDate(10), GetAbrMonthDate(11), GetAbrMonthDate(12)];
						smm = months[smm];
						aFormatedAux[kl] = smm;
						break;
					case "mm":
						aFormatedAux[kl] = mm;
						break;
					case "m":
						smm = parseInt(mm, 10);
						aFormatedAux[kl] = smm;
						break;
					case "yyyy":
						aFormatedAux[kl] = yy;
						break;
					case "yy":
						aFormatedAux[kl] = yy.substring(2, 4);
						break;
					case "q":
						smm = parseInt(mm, 10);
						if (smm <= 3)
							aFormatedAux[kl] = 1;
						else
						{	if (smm <= 6)
								aFormatedAux[kl] = 2;
							else
							{	if (smm <= 9)
									aFormatedAux[kl] = 3;
								else
									aFormatedAux[kl] = 4;
							}
						}
						break;
				}
				iposfin = formated[ij].indexOf(sFormatedType, ipos);
				sFormated = sFormated + '' + formated[ij].substring(ipos, iposfin);	// Copiar los primeros n caracteres antes del replace
				sFormated = sFormated + '' + aFormatedAux[kl]; // Copiar la fecha con formato
				ipos = iposfin + (sFormatedType.length);// mover el puntero
			}
			if (ipos < formated[ij].length){
				sFormated = sFormated + '' + formated[ij].substring(ipos);
			}
			formated[ij] = sFormated;
		}
		brep = !brep;
	}
	return formated.join('');
}


function GetMonthDate(nMonth){
	return  MONTHS[nMonth-1];
}
function GetAbrMonthDate(nMonth){
	return MONTHSABR[nMonth-1];
}

function IsValidDate(year, month, day)
{
	var ends_day = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
	if (month == 2)
		if ((year%400 == 0 || (year%4 == 0 && year%100 != 0)))
			ends_day[month-1] = 29;
	if (day > ends_day[month-1] | day <= 0)
		return false;
	else
		return true;
}

function IsBimester(month)
{
	if ((month+1)%2 == 0)
		return true;
	else
		return false;
}

function IsQuarter(month)
{
	if (month == 1 | month==4 | month==7 | month==10)
		return true;
	else
		return false;
}

function IsSemestral(month)
{
	if (month == 1 | month==7)
		return true;
	else
		return false;
}

function IsTrimestral(month)
{
	if (month == 1 | month==4 | month==7 | month==10)
		return true;
	else
		return false;
}

function sDate(y, m, d){
	if (m<10)
		s = y + '-0' + m;
	else
		s = y + '-' + m;
	if (d<10)
		s = s + '-0' + d;
	else
		s = s + '-' + d;

	return s;
}