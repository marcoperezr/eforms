/*
File: dialogs.js
Version: 2006/03/13 17:01
Copyright 2006 Bitam
*/

var _dialogInput = null;
var _dialogOutput = null;
var _dialogFunction = null;
var _dialogClosed = false;
var _dialogIntervalId = null;
var _dialog = null;
var _dialogContinue = null;
var _windowmodal = null;
var _windowmodalprevious = null;
var bShowModalDialog = (window.showModalDialog);
var bLocalStorage    = (typeof(window.localStorage) != 'undefined');
var bWindowOpener 	 = (window.opener);
var bSaveInLocal	 = (bLocalStorage && !bShowModalDialog && !bWindowOpener);
bSaveInLocal = false;
var idLocalStorage   = "myshowmodal_";
/*
alert('bShowModalDialog:'+bShowModalDialog);
alert('window.opener:'+window.opener);
alert('bLocalStorage:'+bLocalStorage);
alert('bSaveInLocal:'+bSaveInLocal);
*/
if (bSaveInLocal)
{
	_windowmodal = window.localStorage.getItem('_windowmodal');
	if (_windowmodal == null)
	{
		_windowmodal = 0;
	}
	else
	{
		_windowmodalprevious = _windowmodal;
		_windowmodal++;
	}
	window.localStorage.setItem('_windowmodal', _windowmodal);

	window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogInput',null);
	window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogOutput',null);
	window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogFunction',null);
	window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogClosed',false);
	window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogIntervalId',null);
	window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialog',null);
	window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogContinue',null);
}

// Functions to be called from the opened dialog
function openWindowDialog(anURL, aParams, aArgCont, aWidth, aHeight, processFunction, processFunctionString)
{
	var winOpts = '';

	_dialogInput = aParams;
	_dialogContinue = aArgCont;
	_dialogOutput = null;

	if (bSaveInLocal)
	{
		window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogInput'	 , setArrayToString(_dialogInput   , 0)); //_dialogInput.join('_|AWSMD|_')
		window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogContinue', setArrayToString(_dialogContinue, 0));
		window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogOutput'  ,_dialogOutput);
	}

	if (bSaveInLocal || !bShowModalDialog)
	{
		winOpts = 'width=' + aWidth + ',height=' + aHeight;

		winOpts += ',left=' + (window.parent.screenX + ((window.parent.outerWidth - aWidth) / 2));
		winOpts += ',top=' + (window.parent.screenY + ((window.parent.outerHeight - aHeight) / 2));

		_dialogClosed = false;
		_dialogFunction = processFunction;

		_dialogIntervalId = setInterval(processDialogFunction, 250);

		if (bSaveInLocal)
		{
			_dialogFunction = processFunctionString;
			window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogClosed',_dialogClosed);
			window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogFunction',_dialogFunction);
			window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogIntervalId',_dialogIntervalId);
		}

		_dialog = window.open(anURL, "modalWindow", winOpts + ",modal,dialog");

		if (bSaveInLocal)
		{
			window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialog',_dialog);
		}
	}
	else
	{
		winOpts = 'dialogWidth: ' + aWidth + 'px; dialogHeight: ' + aHeight + 'px;center:no;help:no;';

		winOpts += ';dialogLeft=' + (window.parent.screenLeft + ((window.parent.document.body.offsetWidth - aWidth) / 2)) + 'px;'
		winOpts += ';dialogTop=' + (window.parent.screenTop + ((window.parent.document.body.offsetHeight - aHeight) / 2)) + 'px';

		window.returnValue = null;

		var aValue = window.showModalDialog(anURL, _dialogInput, winOpts);

		if (processFunction != null) {
			processFunction(aValue, aArgCont);
		}
		_dialogContinue = null;
	}
}

// OBTENER LOS ARGUMENTOS
function getDialogArgument()
{
	var aVar;

	if (window.opener && !bShowModalDialog)
	{
		aVar = window.opener._dialogInput;
	}
	else if (bSaveInLocal)
	{
		aVar = window.localStorage.getItem(idLocalStorage+_windowmodalprevious+'_dialogInput');
		aVar = setStringToArray(aVar, 0);
	}
	else
	{
		aVar = window.dialogArguments;
	}

	return (aVar);
}


function processDialogFunction()
{
	if (bSaveInLocal)
	{
		_dialog = window.localStorage.getItem(idLocalStorage+_windowmodal+'_dialog');
		if (_dialog != 'null')
			return;
	}
	else
	{
		if (_dialog == null || !_dialog.closed)
			return;
	}

	if (bSaveInLocal)
	{
		_dialogIntervalId = window.localStorage.getItem(idLocalStorage+_windowmodal+'_dialogIntervalId');
		_dialogFunction   = window.localStorage.getItem(idLocalStorage+_windowmodal+'_dialogFunction');
		_dialogOutput     = window.localStorage.getItem(idLocalStorage+_windowmodal+'_dialogOutput');
		_dialogContinue   = window.localStorage.getItem(idLocalStorage+_windowmodal+'_dialogContinue');
		_dialogContinue   = setStringToArray(_dialogContinue, 0);
	}

	if (_dialogIntervalId != null)
		clearInterval(_dialogIntervalId);

	_dialogIntervalId = null;
	_dialog = null;

	if (bSaveInLocal)
	{
		window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialog',null);
		window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogIntervalId',null);
		window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogFunction',null);
		window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogOutput',null);
		window.localStorage.setItem(idLocalStorage+_windowmodal+'_dialogContinue',null);

		if (_dialogFunction != null && _dialogFunction != '')
		{
			eval(_dialogFunction+'(_dialogOutput, _dialogContinue);');
		}
	}
	else
	{
		if (_dialogFunction != null)
		{
			_dialogFunction(_dialogOutput, _dialogContinue)
		}
	}
}

function setDialogReturnValue(aValue)
{
	if (window.opener && !bShowModalDialog)
	{
		window.opener._dialogOutput = aValue;
		//alert('window.opener._dialogOutput:'+window.opener._dialogOutput);
	}
	else if (bSaveInLocal)
	{
		window.localStorage.setItem(idLocalStorage+_windowmodalprevious+'_dialogOutput',aValue);
	}
	else
	{
		window.returnValue = aValue;
		//alert('window.returnValue:'+window.returnValue);
	}
}

function closeDialog(objBtn)
{
	if (window.opener && !bShowModalDialog)
	{
		window.opener._dialogClosed = true;
	}
	else if (bSaveInLocal)
	{
		window.localStorage.setItem(idLocalStorage+_windowmodalprevious+'_dialogClosed',true);
		window.localStorage.setItem(idLocalStorage+_windowmodalprevious+'_dialog',null);

		window.localStorage.removeItem(idLocalStorage+_windowmodal+'_dialogInput');
		window.localStorage.removeItem(idLocalStorage+_windowmodal+'_dialogOutput');
		window.localStorage.removeItem(idLocalStorage+_windowmodal+'_dialogFunction');
		window.localStorage.removeItem(idLocalStorage+_windowmodal+'_dialogClosed',false);
		window.localStorage.removeItem(idLocalStorage+_windowmodal+'_dialogIntervalId');
		window.localStorage.removeItem(idLocalStorage+_windowmodal+'_dialog');
		window.localStorage.removeItem(idLocalStorage+_windowmodal+'_dialogContinue');

		window.localStorage.setItem('_windowmodal', _windowmodalprevious);

		if (objBtn)
		{
			objBtn.style.display = 'none';
		}
	}

	window.close();
}

function setArrayToString(aInfo, level)
{
	if (level == 5)
	{
		return aInfo.join('_|AWSMD:'+level+'|_');
	}

	var aReturn = new Array();
	var nTop = aInfo.length;
	for (var i=0; i<nTop; i++)
	{
		if (typeof(aInfo[i]) == 'object' && aInfo[i].length)
		{
			aReturn[i] = setArrayToString(aInfo[i], (level+1));
		}
		else
		{
			aReturn[i] = aInfo[i];
		}

	}

	return aReturn.join('_|AWSMD:'+level+'|_');
}

function setStringToArray(sInfo, level)
{
	if (level == 5)
	{
		return sInfo.split('_|AWSMD:'+level+'|_');
	}

	var aReturn = sInfo.split('_|AWSMD:'+level+'|_');
	var nTop = aReturn.length;
	for (var i=0; i<nTop; i++)
	{
		if (aReturn[i].indexOf('_|AWSMD:'+(level+1)+'|_'))
		{
			aReturn[i] = setStringToArray(aReturn[i], (level+1));
		}
		else
		{
			aReturn[i] = aInfo[i];
		}

	}

	return aReturn;
}