/* Lanza una petición http a google maps para recuperar los datos de GPS utilizando la última posición recuperada. Si no hay posición entonces no se
ejecutará el proceso y las variables quedarán en estado de "Retrieving..." (lo cual no es del todo correcto, porque si estuviera apagado el GPS del móvil,
entonces simplemente nunca se podría recuperar la información y sin embargo daría la impresión de que estaba intentando hacerlo)
*/
function retrieveGPSData() {
	Debugger.register('retrieveGPSData objApp.firstTime = ' + objApp.firstTime + ', retrievingData == ' + objApp.position.retrievingData);
	
	if (updateGoogleMapsRequestInScreen) {
		try {
			updateGoogleMapsRequestInScreen();
		} catch(e) {}
	}
	
	//No se lanzará ningún evento si falta alguno de los objetos de GPS, de Ajax, si la encuesta no tiene preguntas con variables que requieran 
	//datos extendidos del GPS o si no se encuentra en la ventana de Google Maps
	//JAPR 2018-08-27: Optimizado el uso del servicio de GoogleMaps Geocoder para solo invocarlo si realmente hay en la forma variables de GPS que lo requieran (#9BTTAA)
	if (!objApp || !objApp.position || !objApp.position.coords || !objAjaxDwnld 
			|| (selSurvey && (!selSurvey.hasGPSVars || (selSurvey.currentCapture && !selSurvey.currentCapture.refreshGPSDataFromAPI))) 
			|| (!selSurvey && objApp.currentPageID != 'googlemaps')) {
		Debugger.message('retrieveGPSData, position is not required');
		if (!objApp) {
			Debugger.message('retrieveGPSData, no app instance');
		}
		else if (!objApp.position) {
			Debugger.message('retrieveGPSData, no position instance');
		}
		else if (!objApp.position.coords) {
			Debugger.message('retrieveGPSData, no coords instance');
		}
		else if (!objAjaxDwnld) {
			Debugger.message('retrieveGPSData, no downloader instance');
		}
		//JAPR 2018-08-27: Optimizado el uso del servicio de GoogleMaps Geocoder para solo invocarlo si realmente hay en la forma variables de GPS que lo requieran (#9BTTAA)
		else if (selSurvey) {
			if ( !selSurvey.hasGPSVars ) {
				Debugger.message('retrieveGPSData, survey without gps vars');
			}
			else if ( selSurvey.currentCapture && !selSurvey.currentCapture.refreshGPSDataFromAPI ) {
				Debugger.message('retrieveGPSData, GPS address was obtained already');
			}
		}
		//JAPR
		else if (!selSurvey && objApp.currentPageID != 'googlemaps') {
			Debugger.message('retrieveGPSData, not in googlemaps screen (' + objApp.currentPageID + ')');
		}
		return;
	}
	
	//Si no hay datos de posición, tampoco se lanzará el evento
	var dblLatitude = objApp.position.latitude;
	var dblLongitude = objApp.position.longitude;
	if (!dblLatitude || !dblLongitude) {
		Debugger.message('retrieveGPSData, no current position (Lat: ' + dblLatitude + ', Long: ' + dblLongitude + ')');
		return;
	}
	
	//Verifica si la posición ha variado por lo menos en los 4 dígitos mas significativos, si no es así, entonces no se debe lanzar nuevamente
	//este proceso
	/*var strCurrLat = getGPSCoordAdjusted(dblLatitude, 4);
	var strCurrLong = getGPSCoordAdjusted(dblLongitude, 4);
	var strPrevLat = getGPSCoordAdjusted(objApp.position.coords.latitude, 4);
	var strPrevLong = getGPSCoordAdjusted(objApp.position.coords.longitude, 4);
	*/
	
	var strCurrLat = Number(Math.round(dblLatitude+'e'+4)+'e-'+4);
	var strCurrLong = Number(Math.round(dblLongitude+'e'+4)+'e-'+4);
	var strPrevLat = Number(Math.round(objApp.position.coords.latitude+'e'+4)+'e-'+4); 
	var strPrevLong = Number(Math.round(objApp.position.coords.longitude+'e'+4)+'e-'+4);
  
	//JAPR 2018-08-27: Optimizado el uso del servicio de GoogleMaps Geocoder para solo invocarlo si realmente hay en la forma variables de GPS que lo requieran (#9BTTAA)
	//Verifica si el Accuracy de la posición identificada cumple con el mínimo de precisión configurado si es que hay alguno configurado, de no ser así entonces no
	//ejecutará el proceso de Geocoding hasta que se reciba una nueva posición donde la precisión si sea la adecuada
	/*var intAccuracy = objApp.position.coords.accuracy;
	if ( objSettings.maxGPSAccuracyForGeocode > 0 && intAccuracy < objSettings.maxGPSAccuracyForGeocode ) {
		Debugger.message('retrieveGPSData, the accuracy of the position is grater than the maximum value allowed (Accuracy: ' + intAccuracy + ', maximum accuracy: ' + objSettings.maxGPSAccuracyForGeocode + ')');
	}*/
	//JAPR
  
    //fix Mauricio secondtime
    //if(objApp.secondTime) {
        //if (strCurrLat == strPrevLat && strCurrLong == strPrevLong) { //&& false && objApp.firstTime != true
		if (strCurrLat == strPrevLat && strCurrLong == strPrevLong && !objApp.firstTime) {
			Debugger.message('retrieveGPSData, position is the same as last update (Lat: ' + strCurrLat + ' == ' + strPrevLat + ', Long: ' + strCurrLong + ' == ' + strPrevLong + ')');
			return;
		}
		else {
			Debugger.message('retrieveGPSData, new position detected (Lat: ' + strCurrLat + ' == ' + strPrevLat + ', Long: ' + strCurrLong + ' == ' + strPrevLong + ')');
		}
	//}
	//if(objApp.firstTime) {
	//	objApp.secondTime = true;
	//}
	
	//JAPR 2014-09-11: Agregada una validacion para sólo realizar peticiones adicionales de dirección en base a una configuración de frecuencia mínima
	var intGPSMinFrequency = (objSettings && objSettings.gpsMinFrequency)?objSettings.gpsMinFrequency:0;
	intGPSMinFrequency = parseInt(intGPSMinFrequency);
	if (isNaN(intGPSMinFrequency) || intGPSMinFrequency < 0) {
		intGPSMinFrequency = 0;
	}
	
	if (objApp.position.lastGoogleAPIRequest && intGPSMinFrequency) {
		//Si fuera el primer request entonces pasa sin problemas, pero los posteriores sólo pasarán si no está activada la configuración para limitar
		//la obtención de direcciones (es decir, tiene un valor <= 0) o si ya ha pasado mas del tiempo en segundos que dicha configuración especifica
		var dteLastGoogleAPIRequest = myToDate(objApp.position.lastGoogleAPIRequest);
		var dteCurrDate = new Date();
		var dteNextGPSCheck = new Date(dteLastGoogleAPIRequest.setSeconds(dteLastGoogleAPIRequest.getSeconds() + intGPSMinFrequency))
		if (dteCurrDate <= dteNextGPSCheck) {
			Debugger.message('retrieveGPSData, google api request frequency exceeded (Last request: ' + objApp.position.lastGoogleAPIRequest + ')');
			return;
		}
	}
	
	//JAPR 2014-08-29: Removida esta asignación, ya que se forzará a que entre a actualizar la posición al inicio de la carga de la encuesta con esta 
	//variable en true y usando la posición actual, pero esta variable se debería de cambiar a false hasta que se pueda recuperar exitosamente la
	//posición, si no hubiera conexión entonces fallaría y quedaría en true hasta que finalmente regrese la conexión y en el evento de success es
	//donde se actualizaría, mientras tanto cada intento de actualizar la posición global seguiría intentando obtener la dirección desde google maps
	//objApp.firstTime = false;
	// refreshMaps();
	//objApp.firstTime = false;
	//JAPR 2014-05-09: Agregada una validación para saber si se encuentra en alguna petición previa de datos del GPS, de ser así ya no lanza otra
	//hasta que termine la anterior
	if (objApp.position.retrievingData) {
		//En este caso ya había una petición previa, por lo tanto no realiza otra incluso si la posición ha variado mucho
		Debugger.message('retrieveGPSData, there is another process running');
		return;
	}
	
	//JAPR 2014-05-09: Agregada la identificación de la posición a partir del API de JScript
	//Si está usando el API de Google, primero intentara cargarla si es que no lo había hecho ya, como no hay forma de identificar un fallo en la
	//carga del script, este proceso se realizará sin activar la bandera que bloquea la petición del las direcciones, ya que al terminar de cargar
	//el script se invoca a un callback que volverá a invocar el método actual pero ya sin volver a intentar a cargar el script, si nunca se consiguiera
	//entonces en cada petición para obtener la dirección se estaría intentando cargar el script sin terminar jamás
	//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM) (OMMC)
	if (objSettings && objSettings.useGoogleMapsAPI && !objApp.position.googleAPILoaded) {
		objApp.loadGoogleMaps('googleAPILoaded');
		//Forza a salir, porque hay que esperar a que esté cargada el API o bien en caso de error
		Debugger.message('retrieveGPSData, process launched');
		return;
	}
	
	Debugger.message('retrieveGPSData ' + dblLatitude + ', ' + dblLongitude);
	
	//Activa la bandera para no acumular múltiples peticiones (esto es debido a que en algunos dispositivos, especiamente si no están en WiFi, aunque
	//no se mueva de posición se lanza el evento muchas veces por ligeros ajustes en el GPS, en una frecuencia de 1 vez por segundo)
	objApp.position.retrievingData = true;
	//JAPR 2014-09-11: Agregada una validacion para sólo realizar peticiones adicionales de dirección en base a una configuración de frecuencia mínima
	var dat = new Date();
	objApp.position.lastGoogleAPIRequest = getDate(dat) + ' ' + getTime(dat);
	//JAPR
	
	//Primero limpia el valor actual de la dirección del GPS para que simule que la está recuperando (colocará un mensaje de "Retrieving..."), a la
	//vez se invocará al refresh en las preguntas y pantalla actual
	if (objApp && objApp.position && objApp.position.coords && objApp.position.coords.clear) {
		//JAPR 2014-09-11: Modificado el comportamiento, ahora no limpiará la dirección aunque hubiera fallado el request, sino que conservará la última
		//dirección correctamente obtenida, ya que estar limpiando este valor afectaba a procesos como el mapa o grabado si se asignaban justo cuando
		//se limpiara el valor
		//objApp.position.coords.clear();
		//JAPR
		refreshGPSData();
	}
	
	//JAPR 2014-05-09: Agregada la identificación de la posición a partir del API de JScript
	//Si está usando el API de Google, invoca a un método alternativo para usar dicha API para obtener la información, el valor devuelto se puede
	//procesar de la misma manera independientemente del método usado
	//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM) (OMMC)
	if (objSettings && objSettings.useGoogleMapsAPI ) {
		if ( objSettings.mapType == MapsAPIType.Google ) {
			getGPSDataFromAPI(dblLatitude, dblLongitude);
		} else { //Usará mapkit de Apple
			getGPSDataFromAppleAPI(dblLatitude, dblLongitude);
		}
		return;
	}
	//JAPR
	
	//Obtiene los datos de la dirección a partir del GPS en un proceso asíncrono
	//JAPR 2014-05-09: Agregada la configuración de idioma para las direcciones obtenidas a partir del GPS
	var strLanguage = 'en';
	if (Trim(objSettings.gpsLocale)) {
		strLanguage = objSettings.gpsLocale.toLowerCase();
	}
	//JAPR
	var objParams = {nojsonp: 1};
	//OMMC 2016-05-11: Agregado el cambio de llamada y validación de protocolo
	var strProtocol = "http://";
	
	if (objSettings && !objSettings.webMode) {
		//JAPR 2016-06-24: Modificada la generación de URLs de Google Maps debido al requerimiento de llave a partir de Junio 2016, para poder cambiar la URL desde el server (#F3M0A7)
		//strProtocol = "https://";
		strProtocol = objSettings.getMapsProtocol();
	}
	
	if (objSettings && objSettings.webMode && objSettings.protocol) {
		strProtocol = strProtocol.replace('http:', objSettings.protocol);
	}
	//JAPR 2016-06-24: Modificada la generación de URLs de Google Maps debido al requerimiento de llave a partir de Junio 2016, para poder cambiar la URL desde el server (#F3M0A7)
	var urlService = strProtocol + objSettings.getMapsGeocodeURL() + "?sensor=false&language="+strLanguage+objSettings.getMapsAPIKey()+"&latlng=" + dblLatitude + "," + dblLongitude;
	//var urlService = strProtocol + "maps.googleapis.com/maps/api/geocode/json?sensor=false&language="+strLanguage+"&latlng=" + dblLatitude + "," + dblLongitude;
	//JAPR 2012-11-28: Corregido el protocolo de acceso desde el browser
	makeAjaxProcess (
		function() {objAjaxDwnld.doAjax(urlService, objParams, undefined, undefined, 'getGPSDataFromJson', 'getGPSDataFailed', true);},
		function() {getGPSDataFailed();}
	);
}

//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM) (OMMC)
function getGPSDataFromAppleAPI(aLat, aLong) {
	Debugger.register('getGPSDataFromAppleAPI ' + aLat + ', ' + aLong);

	try {
		objApp.position.geocoder = new mapkit.Geocoder({
			language: "en-GB",
			getsUserLocation: false
		});

		//Timeout para el geocoder
		if (xmlGeoCoderTimeOut !== null) {
        	try {
        		clearTimeout(xmlGeoCoderTimeOut);
        	} catch (e) {
        		//Ignora este error
        	}
        	xmlGeoCoderTimeOut = null;
        }

        xmlGeoCoderTimeOut = setTimeout(function () {
			Debugger.register('geocoder.geocode time out');
        	//Termina la llamada asíncrona aunque no hubiera regresado
        	if (xmlGeoCoderTimeOut != null) {
	        	try {
	        		clearTimeout(xmlGeoCoderTimeOut);
	        	} catch (e) {
	        		//Ignora este error
	        	}
	        	xmlGeoCoderTimeOut = null;
        	}
            
        	objApp.position.retrievingData = false;
        	setDebugHeaderText('geocoder.geocode time out');
        	lastGeoCoderRequestTime = null;
        }, (objSettings.geocoderTimeOut * 1000));

        lastGeoCoderRequestTime = new Date();
        var aCoord = new mapkit.Coordinate(aLat, aLong), i;

        objApp.position.geocoder.reverseLookup(aCoord, function(error, data) {
        	Debugger.register('Apple geocoder request retrieved...');
        	lastGeoCoderRequestTime = null;
	   		if (xmlGeoCoderTimeOut != null) {
		    	try {
		    		clearTimeout(xmlGeoCoderTimeOut);
		    	} catch (e) {
		    		//Ignora este error
		    	}
		    	xmlGeoCoderTimeOut = null;
	   		}
			if (error != null) {
				Debugger.register('Apple geocoder request error: ' + error);
				getGPSDataFailed(error);
			} else if (data && data.results && data.results.length) {
				getGPSDataFromAppleJson(data.results);
			}
		}, {
			language: "en"
		});
	}  catch(e) {
		getGPSDataFailed(e);
	}
}

//JAPR 2014-05-09: Agregada la identificación de la posición a partir del API de JScript
//Obtiene la dirección a partir de la posición GPS utilizando el API de Google
function getGPSDataFromAPI(aLat, aLong) {
	Debugger.register('getGPSDataFromAPI ' + aLat + ', ' + aLong);
	
	try {
		//if (!objApp.position.geocoder) {
		//JAPR 2014-05-12: Se dejó este código fijo porque si se dejaba persistente la instancia, al cambiar de página y regresar empezaba a marcar
		//el error: "setPosition: not a LatLng or LatLngLiteral: in property lat: not a number"
		objApp.position.geocoder = new google.maps.Geocoder();
		//}
		var objLatLng = new google.maps.LatLng(aLat, aLong)
		
		//JAPR 2014-08-29: Agregado el timeOut para geocoder
        if (xmlGeoCoderTimeOut !== null) {
        	try {
        		clearTimeout(xmlGeoCoderTimeOut);
        	} catch (e) {
        		//Ignora este error
        	}
        	xmlGeoCoderTimeOut = null;
        }
        
        //Si llega al timeout, no sabemos como cancelar el request original, pero por lo menos identificará que hay un error y limpiará la variable
        //para que pueda intentar nuevos requests de google maps. Este caso originalmente era generado cuando teniendo el WiFi activado en un iPhone,
        //se realizaba una petición a geocoder.geocode pero luego se perdía la señal del WiFi y entraba el consumo de datos, aparentemente la petición
        //nunca regresaba pues no cambiaba la variable de objApp.position.retrievingData y si marcaba que había otra petición en proceso por dicha 
        //variable en el metodo retrieveGPSData
        xmlGeoCoderTimeOut = setTimeout(function () {
			Debugger.register('geocoder.geocode time out');
        	//Termina la llamada asíncrona aunque no hubiera regresado
        	if (xmlGeoCoderTimeOut != null) {
	        	try {
	        		clearTimeout(xmlGeoCoderTimeOut);
	        	} catch (e) {
	        		//Ignora este error
	        	}
	        	xmlGeoCoderTimeOut = null;
        	}
            
        	objApp.position.retrievingData = false;
        	setDebugHeaderText('geocoder.geocode time out');
        	lastGeoCoderRequestTime = null;
        }, (objSettings.geocoderTimeOut * 1000));
        //JAPR
		
        lastGeoCoderRequestTime = new Date();
		objApp.position.geocoder.geocode({'latLng': objLatLng}, function(results, status) {
			//JAPR 2014-08-29: Agregado el timeOut para geocoder
			Debugger.register('geocoder.geocode callback ' + status);
        	setDebugHeaderText('geocoder.geocode callback ' + status);
        	lastGeoCoderRequestTime = null;
	   		if (xmlGeoCoderTimeOut != null) {
		    	try {
		    		clearTimeout(xmlGeoCoderTimeOut);
		    	} catch (e) {
		    		//Ignora este error
		    	}
		    	xmlGeoCoderTimeOut = null;
	   		}
	   		//JAPR
	   		
			//Este resultado hay que integrarlo en un mismo objeto, ya que la función que extrae la dirección lo espera en dicho formato, que es como
			//se obtenía cuando se hacía la petición a la página de google, sin embargo desde aquí ya verifica que el Status sea "OK"
			if (status == "OK") {	//google.maps.GeocoderStatus.OK
				var objResults = {'results':[], 'status':status};
				var intLength = 0;
				if (results && results.length) {
					intLength = results.length;
				}
	        	Debugger.message('Geocoder results: ' + intLength);
	        	
				for (var intCont = 0; intCont < intLength; intCont++) {
		        	Debugger.message('Geocoder result #' + intCont + ' == ' + results[intCont]);
		        	objResults.results.push(results[intCont]);
				}
				
				getGPSDataFromJson(objResults);
			} else {
				getGPSDataFailed(status);
			}
		});
	} catch(e) {
		getGPSDataFailed(e);
	}
}

//Función de callback cuando termina de cargar el API de Google, simplemente invoca al método que extrae la dirección y marca el API como cargada
function googleAPILoaded() {
	Debugger.register('googleAPILoaded');
	setDebugHeaderText('googleAPILoaded');
	lastGoogleAPIRequestTime = null;
	
	//JAPR 2014-09-01: Agregada la validación para impedir que se intente cargar múltiples veces el API de Google durante la primera corrida
    if (xmlGoogleAPITimeOut !== null) {
    	try {
    		clearTimeout(xmlGoogleAPITimeOut);
    	} catch (e) {
    		//Ignora este error
    	}
    	xmlGoogleAPITimeOut = null;
    }
    //JAPR
	
	if (objApp && objApp.position) {
		objApp.position.googleAPILoaded = true;
		//JAPR 2014-09-01: Agregada la validación para impedir que se intente cargar múltiples veces el API de Google durante la primera corrida
		objApp.position.retrievingGoogleAPI = false;
		//JAPR
	}
	retrieveGPSData();
}
//JAPR

//Data una coordenada GPS (latitud o longitud en el formato #.#####), regresa la misma coordenada ajustada a la cantidad de decimales indicada,
//aplicando validaciones por nulls undefined y demás
function getGPSCoordAdjusted(sCoord, iNumDec) {
	var strCoord = '0';
	if (sCoord) {
		strCoord = sCoord.toString();
		
		var arrParts = strCoord.split('.');
		switch(arrParts.length) {
			case 0:		//No hay elementos, regresa simplemente un 0
				strCoord = '0';
				break;
			case 1:		//Sólo tiene la parte entera, eso es lo que regresa
				break;
			default: 	//Hay una parte decimal, regresa hasta iNumDec posiciones
				strCoord = arrParts[0] + '.' + arrParts[1].substr(0, iNumDec)
				break;
		}
	}
	
	return strCoord;
}

//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM) (OMMC)
function getGPSDataFromAppleJson(arrData) {
	Debugger.register('getGPSDataFromAppleJson');

	if (objApp && objApp.position) {
		//Desactiva la bandera para que el siguiente cambio de posición si busque la dirección
		objApp.position.retrievingData = false;
		objApp.firstTime = false;
	}

	var arrGPSData = {}, i, j, currentRecord, currentAddress, tmpString, tmpArray;
	
	try {
		if (arrData && arrData.length) {
			//Esto en teoría siempre regresaría 1 registro
			for (i=0; i<arrData.length; i++) {
				currentRecord = arrData[i];
				//Se tiene que despedazar el objeto para averiguar los elementos y reconstruir el objeto final para Forms
				if (arrData[i].hasOwnProperty("formattedAddress")) {
					currentAddress = arrData[i].formattedAddress.split(",");
					//Obtener la dirección y posiblemente la colonia
					if (currentAddress.length == 5) {
						arrGPSData.Country = Trim(currentAddress[4]);
						arrGPSData.State = Trim(currentAddress[3]);
						//Se tiene que hacer el ajuste para separar el CP de la ciudad
						tmpString = Trim(currentAddress[2]);
						var regExp = new RegExp(/(^\d+)(\s{1})(.+)/gi); //el CP estará en 1 y la ciudad en 3
						tmpArray = regExp.exec(tmpString);
						if (!tmpArray) {
							arrGPSData.ZipCode = "";
							arrGPSData.City = currentAddress[2];
						} else {
							arrGPSData.ZipCode = tmpArray[1];
							arrGPSData.City = tmpArray[3];
						}
						arrGPSData.Address = Trim(currentAddress[0]) + " " + Trim(currentAddress[1]);
					} else if (currentAddress.length == 4) {
						//No se obtuvo colonia
						arrGPSData.Country = Trim(currentAddress[3]);
						arrGPSData.State = Trim(currentAddress[2]);
						//Se tiene que hacer el ajuste para separar el CP de la ciudad
						tmpString = Trim(currentAddress[1]);
						var regExp = new RegExp(/(^\d+)(\s{1})(.+)/gi); //el CP estará en 1 y la ciudad en 3
						tmpArray = regExp.exec(tmpString);
						if (!tmpArray) {
							arrGPSData.ZipCode = "";
							arrGPSData.City = currentAddress[2];
						} else {
							arrGPSData.ZipCode = tmpArray[1];
							arrGPSData.City = tmpArray[3];
						}
						arrGPSData.Address = Trim(currentAddress[0]);
					} else if (currentAddress.length == 3) {
						arrGPSData.Country = Trim(currentAddress[2]);
						arrGPSData.State = Trim(currentAddress[1]);
						arrGPSData.City = Trim(currentAddress[0]);
						arrGPSData.ZipCode = "";
						arrGPSData.Address = "";
					}  else if (currentAddress.length == 2) {
						arrGPSData.Country = $.trim(currentAddress[1]);
						arrGPSData.State = $.trim(currentAddress[0]);
						arrGPSData.City = "";
						arrGPSData.ZipCode = "";
						arrGPSData.Address = "";
					} else if (currentAddress.length == 1) {
						arrGPSData.Country = $.trim(currentAddress[0]);
						arrGPSData.State = "";
						arrGPSData.City = "";
						arrGPSData.ZipCode = "";
						arrGPSData.Address = "";
					} else {
						//Si no se puede interpretar se genera información genérica
						Debugger.message("getGPSDataFromAppleJson no data case happened");
						arrGPSData.Country = "";
						arrGPSData.State = "";
						arrGPSData.City = "";
						arrGPSData.ZipCode = "";
						arrGPSData.Address = "";
					}
					arrGPSData.FullAddress = arrData[i].formattedAddress || "";
				}
			}
		}
	} catch(e) {
		Debugger.message("getGPSDataFromAppleJson exception: " + e);
	}
	//Antes de terminar actualiza los datos leídos al objeto global de dirección del GPS, además actualiza las diferentes ventanas que sean necesarias
	if (objApp && objApp.position && objApp.position.coords) {
		$.extend(objApp.position.coords, arrGPSData);
		refreshGPSData();
	}
	return arrGPSData;
}

/* Convierte el objeto JSon a un array y lo parsea para extraer la información que se requiere en el producto, identificando los componentes de
posicionamiento según la documentación de google maps encontrada en el siguiente enlace:
https://developers.google.com/maps/documentation/geocoding/?hl=es

- street_address indica una dirección postal precisa.
- route indica una carretera identificada (por ejemplo, "US 101").
- intersection indica una intersección importante, normalmente de dos carreteras importantes.
- political indica una entidad política. Este tipo suele indicar un polígono de alguna administración civil.
- country indica la entidad política nacional y, normalmente, es el tipo de mayor escala que devuelve el geocoder.
- administrative_area_level_1 indica una entidad civil de primer nivel por debajo del nacional. En Estados Unidos, las entidades que corresponden a este nivel administrativo son los estados. No todos los países cuentan con este nivel administrativo.
- administrative_area_level_2 indica una entidad civil de segundo nivel por debajo del nacional. En Estados Unidos, las entidades que corresponden a este nivel administrativo son los condados. No todos los países cuentan con este nivel administrativo.
- administrative_area_level_3 indica una entidad civil de tercer nivel por debajo del nacional. Este tipo indica una división administrativa de menor tamaño. No todos los países cuentan con este nivel administrativo.
- colloquial_area indica un nombre alternativo usado con frecuencia para designar la entidad.
- locality indica una entidad política equivalente al municipio.
- sublocality indica una entidad política de primer nivel por debajo del municipal.
- neighborhood indica un barrio identificado.
- premise indica una ubicación identificada. Normalmente se trata de un edificio o de un complejo de edificios con un nombre común.
- subpremise indica una entidad de primer nivel por debajo del de ubicación identificada. Normalmente se trata de un edificio individual dentro de un complejo de edificios con un nombre común.
- postal_code indica un código postal utilizado para enviar correo postal dentro del país.
- natural_feature indica un paraje natural destacado.
- airport indica un aeropuerto.
- park indica un parque identificado.
- point_of_interest indica un lugar de interés identificado. Normalmente, estos lugares de interés son entidades locales importantes que no encajan con facilidad en otras categorías, como el "Edificio Empire State" o la "Estatua de la Libertad".

Según los casos de prueba, el primer elemento del array contiene la dirección completa dividida en mas elementos, si se parsea este subconjunto
de elementos, bastaría para poder identificar todo lo que se necesita de la dirección comparando los "types" descritos arriba
*/
//Identifica a partir del string JSON especificado, todos los componentes de la dirección según los datos regresados por el API de Google
function getGPSDataFromJson(sJSON) {
	Debugger.register('getGPSDataFromJson');
	
	//JAPR 2014-05-09: Agregada una validación para saber si se encuentra en alguna petición previa de datos del GPS, de ser así ya no lanza otra
	//hasta que termine la anterior
	if (objApp && objApp.position) {
		//Desactiva la bandera para que el siguiente cambio de posición si busque la dirección
		objApp.position.retrievingData = false;
		//JAPR 2014-08-29: Removida esta asignación, ya que se forzará a que entre a actualizar la posición al inicio de la carga de la encuesta con esta 
		//variable en true y usando la posición actual, pero esta variable se debería de cambiar a false hasta que se pueda recuperar exitosamente la
		//posición, si no hubiera conexión entonces fallaría y quedaría en true hasta que finalmente regrese la conexión y en el evento de success es
		//donde se actualizaría, mientras tanto cada intento de actualizar la posición global seguiría intentando obtener la dirección desde google maps
		objApp.firstTime = false;
		//JAPR 2018-08-27: Optimizado el uso del servicio de GoogleMaps Geocoder para solo invocarlo si realmente hay en la forma variables de GPS que lo requieran (#9BTTAA)
		//Desactiva la bandera para impedir que durante la misma sesión de captura se vuelva a obtener la traducción de la dirección GPS con el Geocoding de Google
		if ( selSurvey && selSurvey.currentCapture ) {
			selSurvey.currentCapture.refreshGPSDataFromAPI = false;
		}
		//JAPR
	}
	//JAPR
	
	var arrGPSData = {};
	
	try {
		//var jsonData = JSON.parse(sJSON);
		jsonData = sJSON;
		if (jsonData.status) {
			/*
			"OK" indica que no se ha producido ningún error; la dirección se ha analizado correctamente y se ha devuelto al menos un código geográfico.
			"ZERO_RESULTS" indica que la codificación geográfica se ha realizado correctamente pero no ha devuelto ningún resultado. Esto puede ocurrir si en la codificación geográfica se incluye una dirección (address) inexistente o un valor latlng en una ubicación remota.
			"OVER_QUERY_LIMIT" indica que se ha excedido el cupo de solicitudes.
			"REQUEST_DENIED" indica que la solicitud se ha denegado; normalmente se debe a la ausencia de un parámetro sensor.
			"INVALID_REQUEST" normalmente indica que no se ha especificado la solicitud (address o latlng).
			*/
			var strStatus = jsonData.status.toUpperCase();
			switch (strStatus) {
				case 'OK':
					if (jsonData.results) {
						if (jsonData.results.length > 0) {
							var blnFound = false;
							var arrKeys = Object.keys(jsonData.results);
							for (var aKey in arrKeys) {
								var sKey = arrKeys[aKey];
								var arrAddressData = jsonData.results[sKey];
								if (!arrAddressData) {
									continue;
								}
								
								var blnContinue = false;
								if (arrAddressData.types && ($.isArray(arrAddressData.types) || $.isPlainObject(arrAddressData.types))) {
									var arrTypeKeys = Object.keys(arrAddressData.types);
									for (var aTypeKey in arrTypeKeys) {
										var sTypeKey = arrTypeKeys[aTypeKey];
										var strType = arrAddressData.types[sTypeKey];
										if (!strType) {
											continue;
										}
										
										if (strType.toUpperCase().indexOf('address') >= 0) {
											blnContinue = true;
											break;
										}
									}
								}
								
								if (blnContinue && arrAddressData.address_components && ($.isArray(arrAddressData.address_components) || $.isPlainObject(arrAddressData.address_components))) {
									arrGPSData = getGPSDataFromStreetAddress(arrAddressData);
									blnFound = true;
									break;
								}
							}
							
							//Si no se hubiera encontrado un 'street address', entonces procesa todos los elementos del array principal uno a uno
							//sobreescribiendo la dirección por aquella mayor de entre todos los elementos, mientras que el resto de los datos
							//identificados teóricamente hubieran sido siempre iguales
							if (!blnFound) {
								var arrKeys = Object.keys(jsonData.results);
								for (var aKey in arrKeys) {
									var sKey = arrKeys[aKey];
									var arrAddressData = jsonData.results[sKey];
									if (!arrAddressData) {
										continue; 
									}
									
									var arrGPSDataTemp = getGPSDataFromStreetAddress(arrAddressData);
									if (!arrGPSDataTemp) {
										arrGPSDataTemp = {};
									}
									
									var arrGPSKeys = Object.keys(arrGPSDataTemp);
									for (var aGPsKey in arrGPSKeys) {
										var strKey = arrGPSKeys[aGPsKey];
										strValue = arrGPSDataTemp[strKey];
										switch (strKey) {
											case 'Country':
											case 'State':
											case 'City':
											case 'ZipCode':
												//Estos valores son únicos, así que se sobreescriben
												//JAPR 2013-06-11: Este día se solicitó que si los valores no se habían encontrado, entonces se
												//utilizara un default, por lo que se debe validar que sólo sobreescriba si el valor regresado
												//no es precisamente ese default, o si va a sobreescribir a un default, o si es el primero
												if (!arrGPSData[strKey] || arrGPSData[strKey] == '' || arrGPSData[strKey] == 'Unknown') {
													arrGPSData[strKey] = strValue;
												}
												break;
												
											case 'Address':
												//En este caso, respeta aquella de mayor longitud
												if (!arrGPSData[strKey]) {
													arrGPSData[strKey] = strValue;
												}
												else {
													if (strValue != 'Unknown') {
														if (arrGPSData[strKey].length < strValue.length) {
															arrGPSData[strKey] = strValue;
														}
													}
												}
												break;
												
											case 'FullAddress':
												//En este caso, respeta aquella de mayor longitud
												if (!arrGPSData[strKey]) {
													arrGPSData[strKey] = strValue;
												}
												else {
													if (strValue != 'Unknown') {
														if (arrGPSData[strKey].length < strValue.length) {
															arrGPSData[strKey] = strValue;
														}
													}
												}
												break;
										}
									}
								}
							}
							
							//JAPR 2013-06-11: Genera el campo de la dirección completa concatenando los otros elementos ya recibidos
							if (!arrGPSData.FullAddress) {
								arrGPSData.FullAddress = '';
								var strAnd = '';
								if (arrGPSData.Address && arrGPSData.Address != 'Unknown' && arrGPSData.Address != '') {
									arrGPSData.FullAddress = arrGPSData.Address;
									strAnd = ', ';
								}
								if (arrGPSData.ZipCode && arrGPSData.ZipCode != 'Unknown' && arrGPSData.ZipCode != '') {
									arrGPSData.FullAddress += strAnd + arrGPSData.ZipCode;
									strAnd = ', ';
								}
								if (arrGPSData.City && arrGPSData.City != 'Unknown' && arrGPSData.City != '') {
									arrGPSData.FullAddress += strAnd + arrGPSData.City;
									strAnd = ', ';
								}
								if (arrGPSData.State && arrGPSData.State != 'Unknown' && arrGPSData.State != '') {
									arrGPSData.FullAddress += strAnd + arrGPSData.State;
									strAnd = ', ';
								}
								if (arrGPSData.Country && arrGPSData.Country != 'Unknown' && arrGPSData.Country != '') {
									arrGPSData.FullAddress += strAnd + arrGPSData.Country;
									strAnd = ', ';
								}
							}
							//JAPR
						}
						else {
							Debugger.message("getGPSDataFromJson error: No GPS data returned");
						}
					}
					else {
						Debugger.message("getGPSDataFromJson error: Wrong response format ('results')");
						return false;
					}
					break;
				
				default:
					Debugger.message("getGPSDataFromJson error: " + strStatus);
					return false;
					break;
			}
		}
		else {
			Debugger.message("getGPSDataFromJson error: Wrong response format ('status')");
			return false;
		}
	} catch (e) {
		//No es necesario reportar este error
		arrGPSData = false;
		Debugger.message("getGPSDataFromJson exception: " + e);
	}
	
	//Antes de terminar actualiza los datos leídos al objeto global de dirección del GPS, además actualiza las diferentes ventanas que sean necesarias
	if (objApp && objApp.position && objApp.position.coords) {
		$.extend(objApp.position.coords, arrGPSData);
		refreshGPSData();
	}
	return arrGPSData;
}

//Identifica a partir del componente 'street_address' los datos, completos, ya que no todas las posiciones GPS proporcionadas contienen este valor
//Se puede enviar también este valor desde el array padre y procesará lo que vaya encontrando como subcomponentes, que eso se debe hacer
//precisamente cuando no existe el subcomponente 'street_address'
function getGPSDataFromStreetAddress(arrAddressData) {
	Debugger.register('getGPSDataFromStreetAddress ');
	var arrGPSData = {};
	if (arrAddressData === null || !($.isArray(arrAddressData) || $.isPlainObject(arrAddressData))) {
		return arrGPSData;
	}
	
	try {
		var arrKeys = Object.keys(arrAddressData.address_components);
		for (var aKey in arrKeys) {
			var sKey = arrKeys[aKey];
			var arrComponent = arrAddressData.address_components[sKey];
			if (!arrComponent || !($.isArray(arrComponent) || $.isPlainObject(arrComponent))) {
				continue;
			}
			
			var strLongName = '';
			var strShortName = '';
			var strType = '';
			var arrCompKeys = Object.keys(arrComponent);
			for (var aCompKey in arrCompKeys) {
				var strKey = arrCompKeys[aCompKey];
				var strValue = arrComponent[strKey];
				if (!strValue) {
					continue;
				}
				
				switch (strKey.toLowerCase()) {
					case 'long_name':
						strLongName = strValue;	//utf8_decode
						break;
					
					case 'short_name':
						strShortName = strValue;	//utf8_decode
						break;
						
					case 'types':
						var blnFound = false;
						var arrTypeKeys = Object.keys(strValue);
						for (var aTypeKey in arrTypeKeys) {
							var sTypeKey = arrTypeKeys[aTypeKey];
							strCompType = strValue[sTypeKey];
							if (!strCompType) {
								continue;
							}
							
							switch (strCompType.toLowerCase()) {
								case 'country':
								case 'administrative_area_level_1':
								case 'locality':
								case 'postal_code':
									strType = strCompType.toLowerCase();
									blnFound = true;
									break;
								
								default:
									strType = 'address';
									break;
							}
							
							if (blnFound) {
								break;
							}
						}
						break;
				}
			}
			
			//Agrega o concatena el componente identificado
			switch (strType) {
				case 'country':
					strType = 'Country';
					break;
				
				case 'administrative_area_level_1':
					strType = 'State';
					break;
					
				case 'locality':
					strType = 'City';
					break;
					
				case 'postal_code':
					strType = 'ZipCode';
					break;
					
				case 'address':
					strType = 'Address';
					break;
			}

			if (arrGPSData[strType]) {
				arrGPSData[strType] += ', ' + strLongName;
			}
			else {
				arrGPSData[strType] = strLongName;
			}
		}
		
		//La dirección completa será regresada como la cadena mas larga entre todas
		if (arrAddressData.formatted_address) {
			arrGPSData.FullAddress = arrAddressData.formatted_address;	//utf8_decode
		}
		
		//Agrega valores default si el proceso termina bien pero no logró identificar todos los elementos
		if (!arrGPSData.Country) {
			arrGPSData.Country = 'Unknown';
		}
		if (!arrGPSData.State) {
			arrGPSData.State = 'Unknown';
		}
		if (!arrGPSData.City) {
			arrGPSData.City = 'Unknown';
		}
		if (!arrGPSData.Address) {
			arrGPSData.Address = 'Unknown';
		}
	} catch (e) {
		//No es necesario reportar este error
		arrGPSData = false;
		Debugger.message("getGPSDataFromStreetAddress exception: " + e);
	}
	
	return arrGPSData;
}

//Sobreescribe con un valor vacio los datos del GPS ya que no se logró la conexión para obtenerlos, con esto desaparecerá el texto de "Retrieving...",
//sin embargo no lanza el evento para actualizar las descripciones pues es mejor que se quede con la idea de que está cargandolos
function getGPSDataFailed(sErrDesc) {
	Debugger.register('getGPSDataFailed ' + (sErrDesc)?sErrDesc:'');
	
	//JAPR 2014-05-09: Agregada una validación para saber si se encuentra en alguna petición previa de datos del GPS, de ser así ya no lanza otra
	//hasta que termine la anterior
	if (objApp && objApp.position) {
		//Desactiva la bandera para que el siguiente cambio de posición si busque la dirección
		objApp.position.retrievingData = false;
	}
	//JAPR
	
	if (objApp && objApp.position && objApp.position.coords && objApp.position.coords.clear) {
		//JAPR 2014-09-11: Modificado el comportamiento, ahora no limpiará la dirección aunque hubiera fallado el request, sino que conservará la última
		//dirección correctamente obtenida, ya que estar limpiando este valor afectaba a procesos como el mapa o grabado si se asignaban justo cuando
		//se limpiara el valor
		//objApp.position.coords.clear('');
		//JAPR
	}
}

/* Actualiza los detalles del GPS en todas las preguntas que lo requieran y que aún dependan del valor default, además de actualizar la pantalla
actualmente cargada si es que contiene alguna variable de este tipo
*/
function refreshGPSData() {
	Debugger.register('refreshGPSData');
	
	//Actualiza el contenido de la ventana de Google Maps
	objElement = $('#settingsAddress');
	if (objElement.length > 0 && objApp && objApp.position && objApp.position.coords && objApp.position.coords.getGPSData) {
		objElement.text(objApp.position.coords.getGPSData('FullAddress'));
	}
	//JAPR 2014-09-11: Agregada una validacion para sólo realizar peticiones adicionales de dirección en base a una configuración de frecuencia mínima
	objElement = $('#settingsLastAddressUpdate');
	if (objElement.length > 0 && objApp && objApp.position && objApp.position.lastGoogleAPIRequest) {
		objElement.text(objApp.position.lastGoogleAPIRequest);
	}
	//JAPR
	
	//Si hay una encuesta cargada, actualiza el valor default de todas las preguntas que hagan uso de una variable de GPS
	if (selSurvey && selSurvey.hasGPSVars && selSurvey.questions && selSurvey.questionsOrder) {
		//JAPR 2014-10-07: Agregadas las preguntas tipo sección (Inline)
		//var intSectionID = 0;
		var objSection = undefined;
		if (selSurvey.currentCapture && selSurvey.currentCapture.section) {
			//intSectionID = selSurvey.currentCapture.section.id;
			objSection = selSurvey.currentCapture.section;
		}
		
		var arrSectionsIncluded = new Array();
		if (objSection && objSection.isContainer) {
			if (objSection.getSectionsAsQuestions) {
				arrSectionsIncluded = objSection.getSectionsAsQuestions();
			}
		}
		//JAPR
		
		for (var intQuestionNum in selSurvey.questionsOrder) {
			var intQuestionID = selSurvey.questionsOrder[intQuestionNum];
			var objQuestion = selSurvey.questions[intQuestionID];
			if (!objQuestion || !objQuestion.hasGPSVars) {
				continue;
			}
			
			//En este caso la pregunta tiene al menos una variable de GPS, por tanto hay que refrescar su valor
			//JAPR 2014-10-07: Agregadas las preguntas tipo sección (Inline)
			blnIsInSharedPage = (arrSectionsIncluded.indexOf(objQuestion.sectionID) != -1);
			if (blnIsInSharedPage || objSettings.prebuildSections) {
				var intSectionType = SectionType.Standard;
				if (objQuestion.section) {
					intSectionType = objQuestion.section.type;
				}
				
				//JAPR 2014-10-07: Agregadas las preguntas tipo sección (Inline)
				if (intSectionType == SectionType.Multiple && !blnIsInSharedPage) {
					//En este caso no hay que hacer refresh, ya que las secciones maestro-detalle no están preconstruidas
				}
				else if (objSettings.prebuildSections && intSectionType == SectionType.Dynamic) {
					//En este caso las secciones dinámicas ya están construidas, así que hay que refrescar en todas ellas las preguntas, como no se
					//trata de un valor que dependa de otra pregunta, simplemente refresca todos los registros de la sección
					var intLocalRecordNumForGen = objQuestion.section.recordNumForGen;
					var intMaxRecordNum = objQuestion.section.maxRecordNum;
					try {
						for (var intRecordNum = 0; intRecordNum < intMaxRecordNum; intRecordNum++) {
							objQuestion.section.recordNumForGen = intRecordNum;
							objQuestion.refreshValue();
						}
					} catch (e) {
						//No reporta este error, sólo se atrapa para que la variable que temporalmente se cambió pueda ser restaurada
					}
					//Restaura el valor de recordNumForGen para no afectar a otras llamadas que hubiera en espera
					objQuestion.section.recordNumForGen = intLocalRecordNumForGen;
				}
				else {
					//En este caso no es una sección dinámica, así que no hay múltiples páginas pregeneradas que se deban cambiar y basta con
					//invocar una sola vez a este método
					objQuestion.refreshValue();
				}
				
				//En ninguno de los casos es necesario sobreescribir el valor de la pregunta, ya que de hacerlo, se estaría dejando fijo y eso no
				//es lo que se desea, simplemente se sobreescribe el valor en el HTML de las páginas donde están las preguntas afectadas. Al final
				//justo antes de grabar si se sobreescribe el valor final de la pregunta y eso volverá a actualizar los datos del GPS en él
			}
		}
	}
}

/* Agregada para depurar el proceso de obtención de direcciones mediante Google Maps
Esta función reemplaza el texto del H1 que se encuentra definido en toda ventana de eForms, de esta forma sin importar en que punto se encuentre, se
mostrará info de como se encuentra el proceso de obtención de dirección desde Google Maps
*/
function setDebugHeaderText(aText) {
	//Este proceso se controlará desde el servidor mediante esta configuración
	if (!objSettings || !objSettings.debugMapsOnScreen) {
		return;
	}
	
	Debugger.register('setDebugHeaderText aText == ' + aText + ', lastGeoCoderRequestTime == ' + lastGeoCoderRequestTime + ', lastGoogleAPIRequestTime == ' + lastGoogleAPIRequestTime);
	
	try {
		$('[data-role="header"]').find('h1').each(function() {
			$(this).text(aText);
		});
	} catch (e) {}
}

/* Muestra la información del estado de una petición de obtención de dirección desde el Google Maps
*/
function updateGoogleMapsRequestInScreen() {
	//Este proceso se controlará desde el servidor mediante esta configuración
	if (!objSettings || !objSettings.debugMapsOnScreen) {
		return;
	}
	
	var strGoogleStatus = '';
	if (typeof google != 'object') {
		strGoogleStatus = 'No google obj, ';
	} else if(typeof google.maps != 'object') {
		strGoogleStatus = 'No maps obj, ';
	} else if(!$.isFunction(google.maps.Geocoder)) {
		strGoogleStatus = 'No Geocoder obj, ';
	}
	
	//Verifica la diferencia en segundos entre el último request y el actual
	var intDiff = 'No request';
	if (lastGeoCoderRequestTime) {
		var dteNow = new Date();
		try {
			intDiff = Math.floor((dteNow - lastGeoCoderRequestTime) / 1000);
		} catch(e) {}
		intDiff += ' elapsed';
	}
	else if (lastGoogleAPIRequestTime) {
		var dteNow = new Date();
		try {
			intDiff = Math.floor((dteNow - lastGoogleAPIRequestTime) / 1000);
		} catch(e) {}
		intDiff += ' elapsed (API)';
	}
	
	var strText = strGoogleStatus + intDiff + ', ' + ((objApp.firstTime)?'first':'');
	strText += ', ' + ((objApp.position.retrievingData)?'retrieving':'');
	setDebugHeaderText(strText);
}

//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM) (OMMC)
doAppleAPISearch = function(aString) {
	Debugger.register('doAppleAPISearch ' );
	try {
		if (!aString || aString == "") { return false; }
		
		var aSearch = undefined, annotations = undefined, i;
		
		if (objApp.position.latitude && objApp.position.longitude) {
			aSearch = new mapkit.Search({ 
				coordinate: new mapkit.Coordinate(objApp.position.latitude, objApp.position.longitude),
				language: "en-GB",
				getsUserLocation: false
			});
		} else {
			aSearch = new mapkit.Search({
				language: "en-GB",
				getsUserLocation: false
			});
		}
		
		aSearch.search(aString, function(error, data) {
			if (error != null) {
				Debugger.register("Error while performing the Apple API search: " + error);
				return false;
			} else if (data && data.places && data.places.length) {
				var annotations = data.places.map(function(place) {
					var annotation = new mapkit.MarkerAnnotation(place.coordinate);
					annotation.title = place.name;
					annotation.subtitle = place.formattedAddress;
					annotation.color = "#9B6134";
					return annotation;
				});
			}
		});
		return annotations;
	} catch (e) {
		Debugger.register("Exception while performing the Apple API search: " + e);
	}
}
//JAPR
