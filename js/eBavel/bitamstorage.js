var BitamStorage = {
	request: null,
	database:null,
	databasename:'ebavel',
	version:"1",
	isLocalStorage:false,
	isWebDB:false,
	
	open: function(tables) {
		return BitamStorage.database.open(tables);
	},
	insertData: function(aForm, collection){
		if(BitamStorage.database == undefined){
			var dfd = $.Deferred();
			dfd.resolve({});
			return dfd.promise();
		}
		return BitamStorage.database.insertData(aForm, collection);
	},
	getData: function(aForm){
		return BitamStorage.database.getData(aForm);
	},
	removeData: function(aForm, arrKeys){
		if(BitamStorage.database == undefined){
			var dfd = $.Deferred();
			dfd.resolve({});
			return dfd.promise();
		}
		return BitamStorage.database.removeData(aForm, arrKeys);
	},
	setAppDefinition:function(appDef){
		return BitamStorage.database.setAppDefinition(appDef);
	},
	setLastSyncDate:function(date){
		if(BitamStorage.database == undefined){
			var dfd = $.Deferred();
			dfd.resolve({});
			return dfd.promise();
		}
		return BitamStorage.database.setLastSyncDate(date);
	},
	getLastSyncDate: function(){
		if(BitamStorage.database == undefined){
			var dfd = $.Deferred();
			dfd.resolve({});
			return dfd.promise();
		}
		return BitamStorage.database.getLastSyncDate();
	},
	insertHistoryFieldValues: function(arrHistory) {
		if(BitamStorage.database == undefined){
			var dfd = $.Deferred();
			dfd.resolve(false);
			return dfd.promise();
		}
		return BitamStorage.database.insertHistoryFieldValues(arrHistory);
	},
	clear:function()
	{
		BitamStorage.database.clear();
	},
	setup: function(){
        var indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB;
		if ('webkitIndexedDB' in window) {
		  window.IDBTransaction = window.webkitIDBTransaction;
		  window.IDBKeyRange = window.webkitIDBKeyRange;
		}
		//BitamStorage.request = indexedDB;
		//if(BitamStorage.request == null)
			//BitamStorage.database = BitamStorage.localStorage;
		/*else
			BitamStorage.database = BitamStorage.indexedDB;*/
		//BitamStorage.database = BitamStorage.webDB;
		//BitamStorage.isWebDB = true;
		BitamStorage.database = BitamStorage.webDB;
		BitamStorage.isWebDB = true;
	},
	log: function(msg){
		console.log(msg);
	}
}

$.extend(BitamStorage, {
	webDB: {
		db:null,
		tables:null,
		onError : function(tx, e) {
		  bitamApp.alert("There has been an error: " + e.message);
		},
		clear:function(){
			
		},
		open:function(tables) {
			var arrPromise = new Array();
			var dbSize = 5 * 1024 * 1024; // 5MB
			BitamStorage.webDB.db = openDatabase("eBavel", "1.0", "eBavel manager", dbSize);
			version = null;
			if(version == null)
			{
				BitamStorage.webDB.tables = tables;
				BitamStorage.webDB.db.transaction(function(tx) {
					for(var tableName in tables)
					{
						var sColumns = '';
						for(var i = 0; i<tables[tableName].fields.length; i++)
						{
							var type = "TEXT";
							switch(tables[tableName].fields[i].type)
							{
								case "Integer": type = "INTEGER"; break;
								case "String": type = "TEXT"; break;
								case "Date": type = "DATETIME"; break;
							}
							if(i==0)
							{
								sColumns = tables[tableName].fields[i].name + " " + type + " PRIMARY KEY ASC";
							}
							else
							{
								sColumns = sColumns +", " + tables[tableName].fields[i].name + " " + type;
							}
						}
						var dfd = $.Deferred();
						var sQueryCreate = "CREATE TABLE IF NOT EXISTS "+tableName+" ("+sColumns+");";
						BitamStorage.log(sQueryCreate);
						tx.executeSql(sQueryCreate,[], function(){
							dfd.resolve({});
						}, function(tx, e){
							BitamStorage.log("There has been an error: " + e.message);
							dfd.reject({});
						});
						arrPromise.push(dfd.promise());
					}
				});				
			}
			else
			{
				
			}
			return $.when.apply({}, arrPromise);
		},
		insertData: function(sTable, collection) {
			var arrPromise = new Array();
			BitamStorage.log("insert data "+sTable);			
			BitamStorage.webDB.db.transaction(function(tx){
				for(var keyData in collection)
				{
					data = collection[keyData];
					var columns = "";
					var values  = "";
					var arrValues = new Array();
					for(var key in BitamStorage.webDB.tables[sTable].fields)
					{
						var comma = (columns != "" ? ",":"");
						columns += comma + BitamStorage.webDB.tables[sTable].fields[key].name;
						values += comma + "?";
						arrValues.push(($.isPlainObject(data[BitamStorage.webDB.tables[sTable].fields[key].name]) ? JSON.stringify(data[BitamStorage.webDB.tables[sTable].fields[key].name]) : data[BitamStorage.webDB.tables[sTable].fields[key].name]))
					}
					var dfd  = $.Deferred();
					arrPromise.push(dfd.promise());
					tx.executeSql("INSERT INTO "+sTable+"("+columns+") VALUES ("+values+")",
						arrValues,
						function(){						
							dfd.resolve({});
						},
						function(tx, e){
							BitamStorage.log("There has been an error: " + e.message);
							dfd.reject({});
						});
				}
			});
			return $.when.apply({}, arrPromise);
		},
		getData: function(table) {
			var dfd  = $.Deferred();
			BitamStorage.webDB.db.transaction(function(tx){
				var columns = "";
				for(var key in BitamStorage.webDB.tables[table].fields)
				{
					var comma = (columns != "" ? ",":"");
					columns = columns + comma + BitamStorage.webDB.tables[table].fields[key].name;
				}
				var sQuery = "SELECT " + columns + " FROM " + table;
				 tx.executeSql(sQuery, [], dfd.resolve, function(tx, e){
					BitamStorage.log("There has been an error: " + e.message);
					dfd.reject({});
				 });
			});
			return dfd.promise();
		},
		execute: function(aQuery){
			BitamStorage.log("execute: "+aQuery);	
			var dfd  = $.Deferred();
			BitamStorage.webDB.db.transaction(function(tx){
				tx.executeSql(aQuery, [], dfd.resolve, function(tx, e){
					BitamStorage.log("There has been an error: " + e.message);
					dfd.reject({});
				 });
			});
			return dfd.promise();
		},
		insertHistoryFieldValues: function(arrHistory) {
			var arrPromise = new Array(), sQuery;
			BitamStorage.log("Insert HistoryFieldValues");
			sQuery = 'INSERT INTO FIELDHISTORY (id_fieldhistory, userId, fieldId, `key`, modified, newValue, oldValue) VALUES (?, ?, ?, ?, ?, ?, ?)';
			BitamStorage.webDB.db.transaction(function(tx) {
				for(var keyData in arrHistory) {
					var arrValues = arrHistory[keyData], dfd  = $.Deferred();
					arrPromise.push(dfd.promise());
					tx.executeSql(sQuery,
						arrValues,
						$.proxy(function(){						
							this.dfd.resolve({});
						}, {'dfd':dfd}),
						$.proxy(function(tx, e){
							BitamStorage.log("There has been an error: " + e.message);
							this.dfd.reject({});
						}, {'dfd':dfd}));
				}
			});
			return $.when.apply({}, arrPromise);
		}
	}
})


//solo iniciamos el almacenamiento cuando es una aplicacion //bitamApp.isMobile.isApp()
//@params
//options.forms => bitamApp.aplication.payload.sections, son la definicion de las formas con sus campos
function initDataStore(options)
{
	//obtenemos toda la informacion de las tablas que vamos a guardar en forma local
	var tables = {};
	if(options.forms)
	{
		for (var key in options.forms)
		{
			var aSection = options.forms[key];
			var table = aSection.name;
			//keyPath nos sirve como llave unica
			var keyPath = aSection.getFieldKey()
			tables[table] = {'table':table, 'keyPath':keyPath, 'fields':aSection.payload.definitionFields};
		}
	}
	/*
	for (var key in bitamApp.dataApp)
	{
		var table = key;
		if(table == "templates" || table == "catalog")
			continue;
		switch(table)
		{
			case 'user': var keyPath = 'cuenta_correo'; break;
			case 'users': var keyPath = 'id'; break;
			case 'usersGuests': var keyPath = 'id'; break;
		}
		tables[table] = {'table':table, 'keyPath':keyPath, 'fields':[{defaultValue: 0,name:keyPath,required: true,type: "TEXT"}, {defaultValue: 0,name: "json",required: true,type: "TEXT"}]};
	}*/
	//creamos la tabla donde guardaremos la definicion de la aplicacion
	//tables["appDefinition"] = {'table':'appDefinition', 'keyPath':'codeName', 'fields':[{defaultValue: 0,name:'codeName',required: true,type: "TEXT"}, {defaultValue: 0,name: "json",required: true,type: "TEXT"}]};
	//inicializamos la base de datos IndexDB donde sea soportado, en los demas usamos localStorage
	BitamStorage.setup();
	//Inicializamos las tablas
	return BitamStorage.open(tables);
}