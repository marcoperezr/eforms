var BitamStorage = {
	request: null,
	database:null,
	databasename:'ebavel',
	version:"1",
	isLocalStorage:false,
	isWebDB:false,
	msgPriority:3,
	hasDataToSync: false,
	/*
		FILELOG_EMERG=>0
		FILELOG_ALERT=>1
		FILELOG_CRIT=>2
		FILELOG_ERR=>3
		FILELOG_WARNING=>4
		FILELOG_NOTICE=>5
		FILELOG_INFO=>6
		FILELOG_DEBUG=>7
	*/	
	open: function(tables) {
		return BitamStorage.database.open(tables);
	},
	insertData: function(aForm, collection){
		if(BitamStorage.database == undefined){
			var dfd = $.Deferred();
			dfd.resolve({});
			return dfd.promise();
		}
		return BitamStorage.database.insertData(aForm, collection);
	},
	getData: function(options){
		var aSection = bitamApp.application.getSection(options.table);
		if(!aSection)
			BitamStorage.log('No se encontro el objeto Seccion de: ' + options.table,3)
		return BitamStorage.database.getDataWith({section:aSection,fieldsForm:aSection.getFieldsForm(),filter:options.filter, orderBy: options.orderBy});
		//return BitamStorage.database.getData(options);
	},
	getDataWith: function(options){
		return BitamStorage.database.getDataWith(options);
	},
	removeData: function(aForm, arrKeys){
		if(BitamStorage.database == undefined){
			var dfd = $.Deferred();
			dfd.resolve({});
			return dfd.promise();
		}
		return BitamStorage.database.removeData(aForm, arrKeys);
	},
	//2013-12-04@ARMZ: (#historyFunctions) Inserta en la base de datos local del dispositivo los datos nuevos de valores de historia
	insertHistoryFieldValues: function(arrHistory) {
		if(BitamStorage.database == undefined){
			var dfd = $.Deferred();
			dfd.resolve(false);
			return dfd.promise();
		}
		return BitamStorage.database.insertHistoryFieldValues(arrHistory);
	},
	setLastSyncDate:function(date){
        var LastSyncDate = localStorage['LastSyncDate'];
        if(!LastSyncDate) LastSyncDate = '{}';
        LastSyncDate = JSON.parse(LastSyncDate);
        LastSyncDate[bitamApp.dataApp.user.nom_corto] = date;
		localStorage['LastSyncDate'] = JSON.stringify(LastSyncDate);
		return date;
	},
	getLastSyncDate: function(){
        var LastSyncDate = localStorage['LastSyncDate'];
        if(!LastSyncDate)
            return LastSyncDate;
        LastSyncDate = JSON.parse(LastSyncDate);
        return LastSyncDate[bitamApp.dataApp.user.nom_corto];
	},
	setHashVersion: function(sHash){
		localStorage['hashVersion'] = sHash;
	},
	getHashVersion: function(){
		if(localStorage['hashVersion']!='')
			return localStorage['hashVersion'];
		return false;
	},
	setVersionApp: function(sVersion){
		localStorage['versionApp'] = sVersion;
	},
	getVersionApp: function(){
		if(localStorage['versionApp']!='')	
			return localStorage['versionApp'];
		return false;
	},
	clear:function()
	{
		BitamStorage.database.clear();
	},
	setup: function(){
        var indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB;
		if ('webkitIndexedDB' in window) {
		  window.IDBTransaction = window.webkitIDBTransaction;
		  window.IDBKeyRange = window.webkitIDBKeyRange;
		}
		//BitamStorage.request = indexedDB;
		//if(BitamStorage.request == null)
			//BitamStorage.database = BitamStorage.localStorage;
		/*else
			BitamStorage.database = BitamStorage.indexedDB;*/
		//BitamStorage.database = BitamStorage.webDB;
		//BitamStorage.isWebDB = true;
		BitamStorage.database = BitamStorage.webDB;
		BitamStorage.isWebDB = true;
        BitamStorage.isSQLite = true;
	},
	log: function(msg, priority){
		if(this.msgPriority<priority)
			return false;
		console.log(msg);
	},
	buildFilter: function(options){
		if(typeof options.filter == 'string'){
			return BitamStorage.database.buildFilterWithString(options);
		}
		if(typeof options.filter == 'object'){
			return BitamStorage.database.buildFilterWithObject(options);
		}
	},
    //@MABH20130314
    buildSecurityFilter: function() {
        var filter = 'A.createdUserKey in (';
        filter += bitamApp.user.cla_usuario;
        var subordinadosFilter = this.getSubordinados();
        if(subordinadosFilter) {
            filter += subordinadosFilter;
        }
        filter+= ')';
        return filter;
    },
    getSubordinados: function() {
        var filter = '';
        for(var sKey in bitamApp.user.subordinados) {
            filter += ', '+bitamApp.user.subordinados[sKey].cla_usuario;
        }
        return filter;
    },
    getDataBaseName: function(){
        return 'BITAMeBavelDB_'+bitamApp.dataApp.user.nom_corto;
    },
	syncData: function(options){
		var dfd = $.Deferred();
		var arrPromise = [];
		var arrPromiseInsert = [];
		var lastSyncDate = this.getLastSyncDate();
		var arrData = {};
		//lanzamos un query a la base de datos para obtener todos los datos modificados despues de la ultima sincronizacion
		for(var sTable in this.database.tables){
			if(sTable == 'ANNOTATES' || sTable == 'VIEWCRITERIA' || sTable == 'FILTERBOXFIELDS' || sTable == 'templates') continue;
			var eachDFD = $.Deferred();
			arrPromise.push(eachDFD.promise());
			BitamStorage.getData({
				table: sTable,
				filter: "syncDate>='"+ lastSyncDate + "'",
				orderBy: {'modifiedDate': {id:'A.modifiedDate', order:1}}
			}).then($.proxy(function(res){
				var aSection = bitamApp.application.getSection(this.sectionName);
				if(aSection){
					arrData[aSection.name] = res.collection;
				}
				this.dfd.resolve({'sectionName':this.sectionName, 'success':true});
			}, {'dfd':eachDFD, 'sectionName':sTable}), $.proxy(function(){
				this.dfd.resolve({'sectionName':this.sectionName, 'success':false});
			}, {'dfd':eachDFD, 'sectionName':sTable}))
		}
		$.when.apply({}, arrPromise).then(function(){
			//ya tenemos todos los datos para sincronizar
			//seteamos la ultima vez que sincronizamos datos
			//BitamStorage.setLastSyncDate(new Date().toString('yyyy-MM-dd HH:mm:ss'));
			lastSyncDate = (lastSyncDate?lastSyncDate:'1969-01-01 00:00:00')
            //actualizamos la base de datos solo si no es la primera vez que inicia la aplicacion.
			if(lastSyncDate != '1969-01-01 00:00:00' || !window.sqlitePlugin){
				bitamApp.syncData({
					'progressBarType': 'bootstrap_bg',
					'localData': arrData,
					'lastSyncDate': lastSyncDate,
					'offlineDeleteRecords': BitamStorage.getKeysDeleteOnline(),
					'callback': function(data, payload){
						BitamStorage.clearKeysDeleteOnline();
						for(sTable in data){
							if(!$.isEmptyObject(data[sTable].collection)){
								var eachDfd = $.Deferred();								
								arrPromiseInsert.push(eachDfd);
								BitamStorage.insertData(sTable, data[sTable].collection).then($.proxy(function(){
									this.dfd.resolve();
									
								}, {'dfd':eachDfd}))
							}
						}
						$.when.apply({}, arrPromiseInsert).then(function(){
							BitamStorage.log("TERMINO", 6);	
							//seteamos la ultima vez que sincronizamos datos
							if(payload.lastSyncDate){
								BitamStorage.setLastSyncDate(payload.lastSyncDate);
								BitamStorage.hasDataToSync = false;
								localStorage['bitamApp.uniquekey'] = 0;
							}
							dfd.resolve({data:data,recordsInserted:arrData, lastSyncDate:payload.lastSyncDate});
						});						
					}
				})
            }else{
                //la base de datos ya ha sido precargada
                dfd.resolve({data:{},recordsInserted:{}});
            }
		}, function(){
			BitamStorage.log('Error Critico: No se pudieron obtener los datos de las tablas de la base de datos local',3)
		});
		return dfd.promise();
	},
	addKeysDeleteOnline: function(sForm, arrKeys){
		var arrKeysDeleteOnline = {};
		if(localStorage["bitamApp.KeysDeleteOnline"]){
			arrKeysDeleteOnline = $.parseJSON(localStorage["bitamApp.KeysDeleteOnline"]);
		}
		if(!arrKeysDeleteOnline[sForm]){
			arrKeysDeleteOnline[sForm] = [];
		}
		$.merge(arrKeysDeleteOnline[sForm], arrKeys);
		localStorage["bitamApp.KeysDeleteOnline"] = JSON.stringify(arrKeysDeleteOnline);
	},
	getKeysDeleteOnline : function() {
		var arrKeysDeleteOnline = {};
		if(localStorage['bitamApp.KeysDeleteOnline'] != undefined)
			var arrKeysDeleteOnline = $.parseJSON(localStorage['bitamApp.KeysDeleteOnline']);
		return arrKeysDeleteOnline;
	},
	clearKeysDeleteOnline: function(){
		localStorage["bitamApp.KeysDeleteOnline"] = JSON.stringify({});
	},
	/**
	 * Envia la base de datos al servidor para su revision
	 * @return promise 
	 */
	sendDatabaseToServer: function() {
		return BitamStorage.database.sendDatabaseToServer();
	}
}

$.extend(BitamStorage, {
	webDB: {
		db:null,
		tables:null,
		onError : function(tx, e) {
		  bitamApp.alert("There has been an error: " + e.message);
		},
		/** Elimina la base de datos para el usuario logeado*/
		clear:function(){
			var dbName = BitamStorage.getDataBaseName();
			console.log('Eliminando la base de datos del usuario: ' + dbName);
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
				fileSystem.root.getDirectory('', {create:true}, function (dir) {
					dir.fullPath+"/"+dbName;
					sqlitePlugin.deleteDatabase(dbName, function(){
                		console.log('Base de datos eliminada')
                	}, function() { console.log('No se pudo borrar la base de datos, err 3')})
					/*fileSystem.root.getFile(dir.fullPath+"/"+dbName, null,
                    	function(fileEntry) {
                        	fileEntry.remove(function(){
                        		console.log('Base de datos eliminada')
                        	},function() { console.log('No se pudo borrar la base de datos, err 4')});
                    }, function() { console.log('No se pudo borrar la base de datos, err 3')});*/
				}, function() { console.log('No se pudo borrar la base de datos, err 2')});
			}, function() { console.log('No se pudo borrar la base de datos, err 1')});
		},
		open:function(tables) {
            var arrPromise = new Array();
			//Si LastSyncDate es igual a null, descargamos la base de datos.
            if(window.sqlitePlugin && !BitamStorage.getLastSyncDate()){
                var dfd = $.Deferred();
                arrPromise.push(dfd.promise());
                bitamApp.logIpad('Inicia la descarga de la base de datos.');
				bitamApp.new_pageLoading({text:$.t('Preparing database...')});
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
                    fileSystem.root.getDirectory('', {create:true}, function (dir){
						bitamApp.new_pageLoading(true);
                        var uri = bitamApp.getURLSyncData() + "?applicationCodeName="+bitamApp.application.codeName+"&data=&filter=&userKey="+bitamApp.dataApp.user.cla_usuario+"&lastSyncDate=1900-01-01 00:00:00&BITAM_UserName="+bitamApp.dataApp.user.nom_corto+"&BITAM_Password="+bitamApp.dataApp.user.password+"&BITAM_RepositoryName="+bitamApp.dataApp.user.repositoryName+"&eBavelApp_isApp=true";
                        uri = encodeURI(uri);
                        //preparamos la base de datos
                        bitamApp.getJSONRequest({
							showLoadingMsg: {
								text: $.t('Preparing database...'),
								type: 'bootstrap'
							},
                            url:uri + '&prepareDatabase=true',
                            data: {},
                            success:function(response){
                                var dbName = BitamStorage.getDataBaseName();
                                
                                uri = uri + encodeURI('&GetDatabase=true&path='+response.payload.path);
                                //la base de datos ha sido creada, descargamos el archivo
                                bitamApp.new_pageLoading({text:$.t('Loading database')});
                                bitamApp.logIpad(dir.fullPath);
                                var fileTransfer = new FileTransfer();
                                fileTransfer.download(uri,dir.fullPath+"/"+dbName,function(entry){
										BitamStorage.setLastSyncDate(response.payload.lastSyncDate);
                                        BitamStorage.webDB.tables = tables;
                                        bitamApp.new_pageLoading(true);
                                        bitamApp.logIpad("download complete: " + entry.fullPath);
                                        BitamStorage.webDB.db = window.sqlitePlugin.openDatabase({name: dbName, bgType: 1});
                                        BitamStorage.webDB.dbPath(entry.fullPath);
                                        bitamApp.logIpad("Base de datos abierta");
                                        dfd.resolve({});
                                },function(error) {
                                    if(error==undefined) error = {};
                                    bitamApp.new_pageLoading(true);
                                    bitamApp.logIpad("upload error code" + JSON.stringify(error));
                                    dfd.reject({msgError:'Error al descargar el archivo de base de datos:' + error.code})
                                },false,{});
                            }
                        });
                    }, function(){
						bitamApp.new_pageLoading(true);
                        bitamApp.logIpad("Error al obtener la ruta del archivo")
                        dfd.reject({msgError:'Error al obtener la ruta para la base de datos'})
                    });
                }, null);
                return $.when.apply({}, arrPromise);
            }
            if(!window.sqlitePlugin){
                //si no estamos usando WebSQLite usamos WebSQL HTML5
                var dbSize = 50 * 1024 * 1024; // 50MB
                BitamStorage.webDB.db = openDatabase("eBavel", "1.0", "BITAM eBavel", dbSize);
            }else{
                try{
                    BitamStorage.webDB.db = window.sqlitePlugin.openDatabase({name: BitamStorage.getDataBaseName(), bgType: 1});
                }catch(error){
                    bitamApp.logIpad('Error al abrir la base de datos: '+error.message);
                }
            }         
         
			version = null;
			if(version == null)
			{
				BitamStorage.webDB.tables = tables;
				BitamStorage.webDB.db.transaction(function(tx) {
					for(var tableName in tables)
					{
						var sColumns = '';
						for(var i = 0; i<tables[tableName].fields.length; i++)
						{
							var type = "TEXT";
							switch(tables[tableName].fields[i].type)
							{
								case "Integer": type = "INTEGER"; break;
								case "String": type = "TEXT"; break;
								case "Date": type = "DATETIME"; break;
							}
							if(i==0)
							{
								sColumns = tables[tableName].fields[i].name + " " + type + " PRIMARY KEY ASC";
							}
							else
							{
								sColumns = sColumns +", " + tables[tableName].fields[i].name + " " + type;
							}
						}
						var dfd = $.Deferred();
						var sQueryCreate = "CREATE TABLE IF NOT EXISTS "+tableName+" ("+sColumns+");";
						BitamStorage.log(sQueryCreate, 6);
                        _executeSqlBridge(tx, sQueryCreate,[], $.proxy(function(){
                            this.dfd.resolve({});
                        },{'dfd':dfd, 'table':tableName}), $.proxy(function(tx, e){
                            BitamStorage.log("There has been an error: " + e.message, 4);
                            dfd.reject({});
						},{'dfd':dfd}));
						arrPromise.push(dfd.promise());
					}
				});				
			}
			else
			{
				
			}
			return $.when.apply({}, arrPromise);
		},
		insertData: function(sTable, collection) {
			var mainDFD = $.Deferred();
			BitamStorage.webDB.db.transaction(function(tx){
				var arrPromise = new Array();
				var idx = 0, aSection = bitamApp.application.getSection(sTable);
				for(var keyData in collection)
				{
					data = collection[keyData];
					var sNameField = '', arrColumns = [], arrValues = [], tmpDate = null, arrSubQuerys = [], idRow = data[BitamStorage.webDB.tables[sTable].keyPath];
					for(var key in BitamStorage.webDB.tables[sTable].fields){
						sNameField = BitamStorage.webDB.tables[sTable].fields[key].name;
						if(data[sNameField]==undefined){
							data[sNameField] = '';
						}
						if(sNameField == 'createdUserKey'){
							if(/*data["createdUserKey"] == '' &&*/ $.isPlainObject(data["createdUser_"+sTable]) && !$.isEmptyObject(data["createdUser_"+sTable])){
								for(var keyUserCreate in data["createdUser_"+sTable]){break}
								data["createdUserKey"] = keyUserCreate;
							}else if($.isNumeric(data["createdUser_"+sTable]) && data["createdUser_"+sTable] > -1){
								data["createdUserKey"] = data["createdUser_"+sTable];
							}
						}
						if(sNameField == 'createdDate'){
							if(data[sNameField + '_' + sTable] != ''){
								data[sNameField] = data[sNameField + '_' + sTable];
							}
						}
						tmpDate = null;
						if(BitamStorage.webDB.tables[sTable].fields[key].type == "Date" && $.isset(data[sNameField])){
							tmpDate = Date.parse(data[sNameField]);
							if(tmpDate)
								data[sNameField] = tmpDate.toString('yyyy-MM-dd HH:mm:ss')
						}
						if(sNameField == 'syncDate' && data[sNameField]>=BitamStorage.getLastSyncDate()){
							BitamStorage.hasDataToSync = true;
						}
						if(BitamStorage.webDB.tables[sTable].fields[key].isCatalog){
							if(BitamStorage.webDB.tables[sTable].fields[key].tagname == 'widget_field_alphanumeric' || BitamStorage.webDB.tables[sTable].fields[key].tagname == 'widget_selectionhierarchy'){
								if(!$.isArray(data[sNameField]) && typeof data[sNameField] == 'object'){
									for(var keyO in data[sNameField]){
										if(keyO == 0 && typeof data[sNameField][keyO] == 'string' && data[sNameField][keyO] != '') {
											/** Insertamos la descripcion en su catalogo correspondiente */
											var aCollection = {}, uID = BitamStorage.utils.newId(), aFieldForm = aSection.getFieldFormWithId(sNameField);
											aCollection[uID] = {};
											aCollection[uID]['id_' + aFieldForm.element.catalog_form] = uID;
											aCollection[uID]['createdUserKey'] = data['modifiedUserKey'];
											aCollection[uID]['modifiedUserKey'] = data['modifiedUserKey'];
											aCollection[uID]['createdDate'] = data['modifiedDate'];
											aCollection[uID]['modifiedDate'] = data['modifiedDate'];
											aCollection[uID]['syncDate'] = data['syncDate'];											
											aCollection[uID][aFieldForm.element.catalog_field] = data[sNameField][keyO];
											data[sNameField] = uID;
											BitamStorage.insertData(aFieldForm.element.catalog_form, aCollection);
										} else {
											data[sNameField] = keyO; 
										}
										break;
									}
								}
							}else{
								if(!$.isArray(data[sNameField]) && typeof data[sNameField] == 'object' || $.isNumeric(data[sNameField])){
									arrSubQuerys.push('DELETE FROM '+ sNameField +'_DET WHERE cla_detail = ' + idRow);
									var tmpObject = data[sNameField];
									if($.isNumeric(tmpObject)){
										tmpObject = {};
										tmpObject[data[sNameField]] = '';
									}
									for(var keyO in tmpObject){
										if(keyO.indexOf('FFRMS_')==0 || keyO.indexOf('BPSA_')==0){
											var splitValue = keyO.split('|'), aFieldForm = aSection.getFieldFormWithId(splitValue[0]);
											arrSubQuerys.push("INSERT INTO "+sNameField+"_DET (cla_detail, cla_master) SELECT "+idRow+", id_elementattr FROM elementattrs A WHERE "+aFieldForm.id_fieldform+" = A.fieldform_id AND A.attrName = '"+splitValue[1]+"'");
										}else{
											arrSubQuerys.push('INSERT INTO '+ sNameField +'_DET (cla_detail, cla_master) VALUES ('+idRow+', '+keyO+')');
										}
									}
								}
								continue;
							}
						}
						arrColumns.push(sNameField);
						arrValues.push(data[sNameField]);
					}
					var insertSubQuerys = function(recursive, arrSubQuerys, dfdSubQuery){
						if(dfdSubQuery == undefined)
							dfdSubQuery = $.Deferred();
						if(arrSubQuerys.length){
							var sSql = arrSubQuerys.shift();
							tx.executeSql(sSql, [], function(){
								insertSubQuerys(true, arrSubQuerys, dfdSubQuery);
							},function(tx, e){BitamStorage.log(e,4);insertSubQuerys(true, arrSubQuerys, dfdSubQuery);});
						}else{
							dfdSubQuery.resolve();
						}
						if(recursive !==true)
							return dfdSubQuery.promise(); 
					}
										
					var dfd  = $.Deferred(), sQuery;
					sQuery = "INSERT INTO "+sTable+"("+arrColumns.join(',')+") VALUES ("+(new Array(arrColumns.length).join('?,'))+"?)";
					arrPromise.push(dfd.promise());
					_executeSqlBridge(tx, sQuery, arrValues,
						$.proxy(function(tx, res){
							//ahora insertamos los subquerys
							insertSubQuerys(false, this.arrSubQuerys).then(this.dfd.resolve)
                        }, {'dfd':dfd, 'arrSubQuerys':arrSubQuerys}),
						$.proxy(function(tx, e){
							BitamStorage.log("Insert: " + e.message, 4);
                            var dfd = this.dfd, idRow = this.data[BitamStorage.webDB.tables[sTable].keyPath], arrSubQuerys = this.arrSubQuerys;
							if(idRow != null){
								//hacemos un update
								var aSQL = "UPDATE "+sTable+" SET "+ this.arrColumns.join(' = ?,') +" = ? WHERE " + BitamStorage.webDB.tables[sTable].keyPath + " = " + idRow;
								BitamStorage.log("Query Update: " + aSQL, 7);
                                _executeSqlBridge(tx,aSQL,this.arrValues,function(){
                                    insertSubQuerys(false, arrSubQuerys).then(function(){
                                        dfd.resolve()
									});
								}, function(e){
									BitamStorage.log("Update: " + e.message + ". Query: " + aSQL, 4);
									dfd.resolve({});
								});
							}else{
								BitamStorage.log("Insert: " + e.message, 4);
								dfd.resolve({});
							}							
                        },{'dfd':dfd, 'arrSubQuerys':arrSubQuerys, 'data':data, 'arrColumns':arrColumns, 'arrValues':arrValues}));
				}
				$.when.apply({},arrPromise).then(function(){
					tx.executeSql('COMMIT;', [], function(){},function(tx, e){});
					mainDFD.resolve()
				}, mainDFD.resolve);
			}, mainDFD.reject);
			
			return mainDFD.promise();
		},
		//solo soporta campos de un solo formulario, no de formularios padre
		//@SM se agrega atributo sort
		getDataWith: function(options){
			var dfd  = $.Deferred(), aFieldsForms = options.fieldsForm, aSection = options.section, arrJoins = [], querySelect = [], collection = (options.array?[]:{}), sFilter = '', arrFieldsData;
			sFilter = BitamStorage.buildFilter({filter:options.filter, sectionName:aSection.name })
			var arrFields = {};
			for(var keyField in aFieldsForms){
				arrFields[aFieldsForms[keyField].id] = aFieldsForms[keyField].id;
			}
			var aQuery = new BuildQuery({
				sectionName:aSection.name, 
				fields:arrFields,
				appCodeName:bitamApp.application.codeName,
				filter:sFilter,
                limit: (options.limit)?options.limit:null,
				sort: (options.sort)?options.sort:null,
				groupBy: options.groupBy,
				orderBy: options.orderBy
			})
			var sQuery = aQuery.querySelect();			
			BitamStorage.webDB.db.transaction(function(tx){
				_executeSqlBridge(tx, sQuery, [], function(tx, rs){
                    var eachRow = {}, tmpRow = {}, arrKeyReg = {}, keyReg = 0;
					for(var i=0; i<rs.rows.length; i++){
						eachRow = rs.rows.item(i);
						tmpRow = {};
						for(var keyField in aFieldsForms){
							var eachField = aFieldsForms[keyField], tmpObject={};
							if(eachField.isCatalog() || eachField.element.q1){
								if(options.plain){
									tmpRow[eachField.id] = eachRow[eachField.id];
									tmpRow['key_'+eachField.id] = eachRow['key_'+eachField.id];
								}else{
                                  if(eachRow[eachField.id] != undefined && eachRow['key_'+eachField.id]){
									tmpObject[eachRow['key_'+eachField.id]] = eachRow[eachField.id];
									tmpRow[eachField.id] = tmpObject;
                                  }else{
                                  	if(eachField.tagname == 'widget_field_alphanumeric' && eachRow['key_'+eachField.id] && !$.isNumeric(eachRow['key_'+eachField.id])) {
										tmpObject["0"] = eachRow['key_'+eachField.id];
                                  		tmpRow[eachField.id] = tmpObject;
                                  	} else {
                                  		tmpRow[eachField.id] = [];
                                  	}
                                  }
								}
							}else{
								tmpRow[eachField.id] = eachRow[eachField.id];
							}
						}
						tmpRow['createdUserKey'] = eachRow['createdUserKey'];
						tmpRow['modifiedUserKey'] = eachRow['modifiedUserKey'];
                        tmpRow['modifiedDate'] = eachRow['modifiedDate'];
                        tmpRow['createdDate'] = eachRow['createdDate'];
						tmpRow['syncDate'] = eachRow['syncDate'];
						tmpRow['owner'] = eachRow['owner'];
						tmpRow[aSection.getFieldKey()] = eachRow[aSection.getFieldKey()];
                        if(options.array){
                            if(arrKeyReg[tmpRow[aSection.getFieldKey()]] == undefined || options.catalog_usedescriptionkey){
                                collection.push(tmpRow);
                                arrKeyReg[tmpRow[aSection.getFieldKey()]] = keyReg;
                                keyReg = keyReg + 1;
                            }else{
                                tmpRow = $.extend(true, collection[arrKeyReg[tmpRow[aSection.getFieldKey()]]], tmpRow);
                            }
                        }else{
                            if(collection[eachRow[aSection.getFieldKey()]] == undefined)
                                collection[eachRow[aSection.getFieldKey()]] = tmpRow
                            else
                                tmpRow = $.extend(true, collection[eachRow[aSection.getFieldKey()]], tmpRow);
                        }
						//(options.array ? collection.push(tmpRow) : collection[eachRow[aSection.getFieldKey()]] = tmpRow);
					}
					dfd.resolve({'collection':collection});
				}, function(tx, e){
                    BitamStorage.log("There has been an error: " + e.message + ". Query: " + sQuery, 3);
                    dfd.reject({});
                });
				_executeSqlBridge(tx, 'END TRANSACTION', [], function(){}, function() {});	
            }, function(){console.log(arguments);BitamStorage.log("No se logro realizar la transaccion", 3)});
			return dfd;
		},
		//@params
		//options.table //nombre de la tablas
		//options.filter //filtro para el query
		getData: function(options) {
			var sTable = options.table;
			var aFilter = options.filter;
			var sFilter = '';
			if(typeof aFilter == 'string'){
				sFilter = aFilter
			}else{
				/*for(var key in aFilter){
					sFilter += key + '=' + aFilter[key];
				}*/
				sFilter = this.buildFilterWithObject({filter:aFilter, sectionName:sTable })
			}			
			var dfd  = $.Deferred();
			BitamStorage.webDB.db.transaction(function(tx){
				var columns = "";
				for(var key in BitamStorage.webDB.tables[sTable].fields)
				{
					var comma = (columns != "" ? ",":"");
					columns = columns + comma + BitamStorage.webDB.tables[sTable].fields[key].name;
				}
				var sQuery = "SELECT " + columns + " FROM " + sTable + (sFilter != "" ? " WHERE " + sFilter : '');	
                _executeSqlBridge(tx, sQuery, [], dfd.resolve, function(tx, e){
                    BitamStorage.log("There has been an error: " + e.message + ". Query: " + sQuery, 4);
                    dfd.reject({});
                });
            });
			return dfd.promise();
		},
		removeData: function(sTable, collectionIds){
			var arrPromise = new Array(), aSection = bitamApp.application.getSection(sTable);
			BitamStorage.log("delete data "+ sTable + ', ids: ' + collectionIds, 6);			
			BitamStorage.webDB.db.transaction(function(tx){
				for(var ids in collectionIds)
				{
					var dfd  = $.Deferred(), arrQuerys = [], totalQueryDeletes = 0;
					arrQuerys.push("DELETE FROM "+sTable+" WHERE "+ BitamStorage.webDB.tables[sTable].keyPath +" = "+collectionIds[ids]);
					if(aSection){
						var arrFieldsForm = aSection.getFieldsForm();
						for(var i=0; i<arrFieldsForm.length; i++){
							if(arrFieldsForm[i].isCatalog() || arrFieldsForm[i].element.q1){
								//Hay que eliminar los registros en la tabla _DET
								arrQuerys.push("DELETE FROM "+ arrFieldsForm[i].id + "_DET WHERE cla_detail = " + collectionIds[ids]);
							}
						}
					}
					arrPromise.push(dfd.promise());
					for(var i = 0; i<arrQuerys.length; i++){
						_executeSqlBridge(tx,arrQuerys[i], [], $.proxy(function(){totalQueryDeletes++;if(totalQueryDeletes==arrQuerys.length){this.dfd.resolve({})}}, {'dfd': dfd}), $.proxy(function(){totalQueryDeletes++;if(totalQueryDeletes==arrQuerys.length){this.dfd.resolve({})}}, {'dfd': dfd}));
					}
				}
			});
			return $.when.apply({}, arrPromise);
		},
		//2013-12-04@ARMZ: (#historyFunctions) Inserta en la base de datos local del dispositivo los datos nuevos de valores de historia
		insertHistoryFieldValues: function(arrHistory) {
			var arrPromise = new Array(), sQuery;
			BitamStorage.log("Insert HistoryFieldValues");
			sQuery = 'INSERT INTO FIELDHISTORY (id_fieldhistory, userId, fieldId, `key`, modified, newValue, oldValue) VALUES (?, ?, ?, ?, ?, ?, ?)';
			BitamStorage.webDB.db.transaction(function(tx) {
				for(var keyData in arrHistory) {
					var arrValues = arrHistory[keyData], dfd  = $.Deferred();
					arrPromise.push(dfd.promise());
					tx.executeSql(sQuery,
						arrValues,
						$.proxy(function(){						
							this.dfd.resolve({});
						}, {'dfd':dfd}),
						$.proxy(function(tx, e){
							BitamStorage.log("There has been an error: " + e.message);
							this.dfd.reject({});
						}, {'dfd':dfd}));
				}
			});
			return $.when.apply({}, arrPromise);
		},
		execute: function(aQuery){
			BitamStorage.log("execute: "+aQuery, 6);
			var dfd  = $.Deferred();
			BitamStorage.webDB.db.transaction(function(tx){
                _executeSqlBridge(tx,aQuery, [], dfd.resolve, function(tx, e){
					BitamStorage.log("There has been an error: " + e.message + ". Query: " + aQuery, 3);
					dfd.reject({tx:tx, e:e});
				 });
				_executeSqlBridge(tx, 'END TRANSACTION', [], function(){}, function() {});
			});
			return dfd.promise();
		},
		buildArrayFilterWithString: function(options){
			var match = [], arrFilter={}, regex = /((?:id_|cuenta_correo_|createdUser_|modifiedUser_|createdDate_|modifiedDate|syncDate)?(?:FFRMS|FRM)_[0-9]+)[ ]*=[ ]*'([^']*)'/g
			while (match = regex.exec(options.filter)) {
				if(arrFilter[match[1]] == undefined) arrFilter[match[1]] = [];
				arrFilter[match[1]].push(match[2])
			}
			return arrFilter;
		},
		buildFilterWithString:function(options){
			return options.filter.replace(/((?:id_|cuenta_correo_|createdUser_|modifiedUser_|createdDate_|modifiedDate|syncDate)?(?:FFRMS|FRM)_[0-9]+)='([^']*)'/g, function(a,b,c,d){
				aSection = bitamApp.application.getSection(options.sectionName);
				var aField = aSection.getFieldFormWithId(b);
				if(/*(aField && aField.isCatalog()) ||*/ (c[c.length-1]=='*'&&c[0]=='*')){
					if((c[c.length-1]=='*'&&c[0]=='*')) c = c.replace(/\*/g, '');
					return b+' like "%' + c + '%"';
				}
				return a
			});
		},
		buildFilterWithObject: function(options){
			var res = '';
			for(var key in options.filter){
				var logicalOperator = 'AND';
				var compOperator = "=";
				var sField = key;
				var sValue = options.filter[key];
				var sChunk = '';
				if(key.indexOf('|')!=-1){
					var arrField = key.split("|");
					logicalOperator = arrField[0]!=''?arrField[0]:logicalOperator;
					sField = arrField[1];
					compOperator = arrField[2];
				}
				var aField = bitamApp.application.getSection(options.sectionName).getFieldFormWithId(sField) || bitamApp.application.getSection(options.sectionName).getFieldFormWithIdByParents(sField);
				//2013-12-11@HADR(#LookupsRecursivosSameTable) Agrega el alias a los campos de la misma forma debido a que es posible que exista un campo de tipo catalogo recursivo a la misma forma
				if(!aField.isCatalog() && options.sectionName == aField.sectionName){
					sField = "A."+sField;
				}
				//if((!$.isArray(sValue)) && ((aField && aField.isCatalog()) || (sValue[sValue.length-1]=='*'&&sValue[0]=='*') )){
				//	compOperator = ' LIKE ';
				//	if((sValue[sValue.length-1]=='*'&&sValue[0]=='*')) sValue = sValue.replace(/\*/g, '');
				//	sValue = "%"+ sValue + "%";
				//	sChunk = sField+compOperator+"'"+ sValue +"'";
				//}
				if($.isArray(sValue)){
					if(/*(aField && aField.isCatalog()) || */(sValue[sValue.length-1]=='*'&&sValue[0]=='*') ){
						sChunk = "(" + sField + " LIKE '%"+sValue.join("%' OR "+ sField +" LIKE '%")+"%')";
						if((sValue[sValue.length-1]=='*'&&sValue[0]=='*')) sChunk = sValue.replace(/\%*/g, '%').replace(/\*%/g, '%');
					}else{
						if(aField && aField.tagname == 'widget_field_date'){
							for(var i=0; i<sValue.length; i++){
								if(sValue[i].indexOf('@')!=-1){
									var ini = Date.parse(sValue[i].split('@')[0]).toString('yyyy-MM-dd HH:mm:ss');
									var fin = Date.parse(sValue[i].split('@')[1]).toString('yyyy-MM-dd HH:mm:ss');
									sChunk = "(" + sField + ">=" + "'"+ ini +"' AND " + sField + " <= '"+ fin +"')";
								}else{
									sChunk = "(" + sField + compOperator + "'"+sValue[i]+"')";
								}
							}
						}else
							sChunk = "(" + sField + " "+compOperator+" '"+sValue.join("' OR "+ sField +" "+compOperator+" '")+"')";
					}
				}else{
					sChunk = sField+compOperator+"'"+ sValue +"'";
					if( (sValue[sValue.length-1]=='*'&&sValue[0]=='*') ){
						sChunk = "(" + sField + " LIKE '%"+sValue.replace(/\*/g, '%')+"%')";					
					}
				}
				res += (res!=""?logicalOperator:"") + " " + sChunk + " ";
			}
			return res;
		},

		dbPath: function(sPath) {
			if(typeof sPath == 'string') {
				localStorage["Storage.dbPath"] = sPath;
			}

			return localStorage["Storage.dbPath"];
		},

		sendDatabaseToServer: function() {
			var options = new FileUploadOptions(), params = {}, ft = new FileTransfer(), deferred = $.Deferred(), sDBPath = BitamStorage.webDB.dbPath();

			if(typeof sDBPath == 'string') {
				if(bitamApp.isMobile.Android()){
					sDBPath = sDBPath.replace('file:///','/');
				}
				window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {
            		fs.root.getFile(sDBPath, { create: false }, function(fileEntry) {
              			fileEntry.file(function readDataUrl(file) {
                			var reader = new FileReader();
                			reader.onloadend = function(evt) {
                				bitamApp.getJSONRequest({
									showLoadingMsg: {
										text: $.t('Send database') + '...',
										type: 'bootstrap'
									},
		                            url: bitamApp.getURLServer() + '/action.php',
		                            data: {
		                            	database: evt.target.result, 
		                            	action: 'uploadDatabase',
		                            	claUser: bitamApp.user.cla_usuario,
		                            	appCodeName: bitamApp.application.codeName,
		                            	applicationName: bitamApp.application.applicationName,
		                            	repositoryName: bitamApp.dataApp.user.repositoryName
		                            }
		                        }).then(function() {
		                        	deferred.resolve();
		                        }, function() {
		                			deferred.reject();        	
		                        });                  				
                			};
                			reader.readAsDataURL(file);
              			}, null);
            		}, function() {
              			deferred.reject();
            		});
          		});
			} else {
				deferred.reject();
			}

			return deferred.promise();
		}
	}
});


//solo iniciamos el almacenamiento cuando es una aplicacion //bitamApp.isMobile.isApp()
//@params
//options.forms => bitamApp.aplication.payload.sections, son la definicion de las formas con sus campos
//options.syncData => Si esta en TRUE, actualizamos los datos de la aplicacion
function initDataStore(options)
{
	var mainDfd = $.Deferred();
	//obtenemos toda la informacion de las tablas que vamos a guardar en forma local
	var tables = {};
	if(options.forms){
		for (var key in options.forms){
			var aSection = options.forms[key];
			var table = aSection.name;
			//keyPath nos sirve como llave unica
			var keyPath = aSection.getFieldKey();
			var definitionFields = [];
			for(var i = 0; i<aSection.payload.definitionFields.length; i++) {
				if(aSection.payload.definitionFields[i].tagname != 'widget_calculatedfield') {
					definitionFields.push(aSection.payload.definitionFields[i]);
				}
			}
			tables[table] = {'table':table, 'keyPath':keyPath, 'fields':definitionFields};
		}
	}
	//creamos la base de datos
	BitamStorage.setup();
	//Inicializamos las tablas
	BitamStorage.open(tables).then(function(res){
		var arrPromise = new Array();
		//despues de inicializar las tablas cargamos los datos de cada seccion y lo guardamos en la base de datos.. este proceso puede tardar varios minutos
		if(bitamApp.onLine()){
			//26/01/2013@aramirez
			//msj de carga de datos, si es la primera vez que se inicia la aplicacion puede tardar varios minutos
			//$(".ui-loader h1").html('');
			var dfd = $.Deferred();
			arrPromise.push(dfd.promise());
			if(options.syncData) {
				BitamStorage.syncData().then(function(res){
					dfd.resolve(res);
				})
			} else {
				//2014-01-13@ARMZ: Ya no volvemos a actualizar la aplicacion 
				dfd.resolve(res);
			}
		}else{
			//no estamos online, no cargamos los datos del server
			var dfd = $.Deferred();
			arrPromise.push(dfd.promise());
			dfd.resolve();
		}
		$.when.apply({}, arrPromise).then(function(res){
			//se termino la carga de todos los datos, si estamos onLine seteamos la fecha de la ultima sincronizacion
			if(bitamApp.onLine() && options.syncData){
				if(res && res.lastSyncDate)
					BitamStorage.setLastSyncDate(new Date().toString(res.lastSyncDate));
				else
					BitamStorage.setLastSyncDate(new Date().toString('yyyy-MM-dd HH:mm:ss'));
			}
			mainDfd.resolve();
		});
	})
	return mainDfd.promise();
}

function loginDatabase(sUserLogin){
	//en esta funcion solo entra cuando esta online y solo si es un App
	if(bitamApp.isWeb() && !bitamApp.isMobile.isApp())
		return;
}

/** Guarda la definicion de la aplicacion en memoria persistente*/
function setAppDefinition(anObject) {
	console.log('Guarda la definicion de la aplicacion')
	if(typeof anObject == 'undefined') {
		anObject = bitamApp.application;
	}
	if(anObject /*&& anObject instanceof BuilderAppResponse*/) {
		localStorage["bitamApp.application"] = JSON.stringify(anObject);
	} else {
		localStorage["bitamApp.application"] = null;
	}
}

function getAppDefinition(){
	if(!localStorage["bitamApp.application"]) return false;
	var anObject = $.parseJSON(localStorage["bitamApp.application"]);
	if(!anObject) return false;
	for(var key in anObject.payload.sections) {
		anObject.payload.sections[key] = new Section(anObject.payload.sections[key])
	}
	var res = $.extend(new BuilderAppResponse(), anObject)
	return res;        
}

function setDataApp(anObject){
	if(typeof anObject == 'undefined') {
		anObject = bitamApp.dataApp;
	}
	var sUser = anObject.user.nom_corto;
	var anObectCopy=$.extend(true,{},anObject);
	for(key in anObectCopy.user.securityCriteria){
		for(keyAction in anObectCopy.user.securityCriteria[key]){
			if($.isset(anObectCopy.user.securityCriteria[key][keyAction]) && typeof anObectCopy.user.securityCriteria[key][keyAction]=="function")
				anObectCopy.user.securityCriteria[key][keyAction]=anObectCopy.user.securityCriteria[key][keyAction].toString();
		} 
	}
	for(key in anObectCopy.user.securityCriterionView){
		for(keyAction in anObectCopy.user.securityCriterionView[key]){
			if($.isset(anObectCopy.user.securityCriterionView[key][keyAction]) && typeof anObectCopy.user.securityCriterionView[key][keyAction]=="function")
				anObectCopy.user.securityCriterionView[key][keyAction]=anObectCopy.user.securityCriterionView[key][keyAction].toString();
		} 
	}
	console.log('Guardamos los datos de la aplicacion para el usuario')
	localStorage["bitamApp.dataApp_" + sUser] = JSON.stringify(anObectCopy);
}

function getDataApp(sUser){
	console.log('Obtenemos los datos almacenados de la aplicacion para el usuario')
	if(!localStorage["bitamApp.dataApp_" + sUser]) return false;
	var dataApp = JSON.parse(localStorage["bitamApp.dataApp_" + sUser]);/*$.parseJSON(localStorage["bitamApp.dataApp"])*/;
	/*En la aplicacion movil no se usara los templates. Se usan en el render del form*/
	dataApp.templates = {};
	return dataApp;
        
}

function setDatausers(aUser){
	console.log('Guardamos la informacion del usuario.');
	var arrUsers =  {};
	if(localStorage["bitamApp.userslogin"]){
		arrUsers = $.parseJSON(localStorage["bitamApp.userslogin"]);
	}
	arrUsers[aUser.cuenta_correo] = aUser;
	localStorage["bitamApp.userslogin"] = JSON.stringify(arrUsers);
}

function getDatausers(){
	console.log('Obtenemos la informacion de los usuarios.')
	if(localStorage["bitamApp.userslogin"]) return JSON.parse(localStorage["bitamApp.userslogin"]);
	return false;
}

function _executeSqlBridge(tx, sql, params, dataHandler, errorHandler) {
	BitamStorage.log("\n---------------------------\nQuery: " + sql + "\nParams: " + JSON.stringify(params) + "\n---------------------------\n",7)
    if (BitamStorage.isSQLite && false) {
        //this is a native DB, the method signature is different:
        var sqlAndParams = [sql].concat(params);
        var cb = function(res) {
            //in WebSQL : result.rows.item(0)
            //in the phonegap plugin : res.rows[0]
            res.rows.item = function(i) {
                return this[i];
            };
            //the result callback hasn't the tx param
            dataHandler(tx, res);
        };
        tx.executeSql(sqlAndParams, cb, errorHandler);
    } else {
        //Standard WebSQL
        tx.executeSql(sql, params, dataHandler, errorHandler);
    }
}