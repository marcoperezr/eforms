var bitamApp = bitamApp || {};
$.extend(bitamApp, {

	//properties
	uri:null,
	option:{},
	user:{username:'', password:''},
	application:{},
	dataUser:{},
	dataApp:{},
	dataCache:{},
	//JAPR 2013-09-13: Agregada la versión de la Aplicación
	//2013-10-29@ARMZ: (#buildFilterViewQuery) Se cambio la version para que ya no envie el filtro de la vista y el armador de querys se encargue de agregarlo
	appVersion:4.00000,
	//
	
	//2013-13-12@MABH: (App Launcher) Se genero una app para GNC que lanza las demas apps, fromURL representa si la app es lanzada desde un uri protocol
	//se reciben parametros de usuario y password, la cadena llega como gncApp://_AWSep_MyUser_AWSSEP_MyPassword, y se debe hacer login con ese usuario
	//si el usuario no existe u ocurre algun error en el login debe mostrar el mensaje de error y volver a abrir la app de GNC, al hacer logout
	//debe de salirse y reabrir la app de GNC tambien, el uri protocol es configurado en el archivo plist del proyecto bajo la llave URL Schemes
	fromURL:null,
		
	//Inicializa la instancia principal en JScript del producto descargando la definición de la aplicación 'eBavel' (o personalizada) en forma de JSon, además de 
	//configurar almacenamiento y otras cosas necesarias para el correcto funcionamiento del producto (sólo se invoca una vez al arrancar el producto)
	init:function(uri, callback)
	{
		this.setOption('mode', 'kpi');
		this.setOption('isIpad', false);
		this.setOption('isDevice', false);
		
		//2013-13-12@MABH: (App Launcher) Se asume que no viene de otra app
		this.fromURL = false;
		
		if($.mobile) {
			$.mobile.hashListeningEnabled = false;
			$.mobile.pushStateEnabled = false;
		}
		
		this.setURI(uri);
		
		this.dataUser = new Object();
		Session.setup();
		
		var urlSplit = document.URL.split("#");
		if(isset(urlSplit[1]))
		{
			var rapid = $.createObjectWithParams(urlSplit[1]);
			if($.isPlainObject(rapid))
			{
				if(isset(rapid.username))
				{
					$("#username").val(rapid.username);
					$("#username").attr('value', rapid.username);
				}
			}
		}		
		
		//2013-01-13@ARMZ: Si es una APP/APK y ya tenemos cargada la definicion de la aplicacion no volvemos a pedirla
		if(bitamApp.isMobile.isApp() && getAppDefinition() ) {
			console.log('Carga la definicion de la aplicacion almacenada')
			bitamApp.application = getAppDefinition();
            bitamApp.application.appVersion = bitamApp.appVersion;
			BitamInterface.init(bitamApp.application)
            callback();
		}
		else {
			console.log('Carga la definicion de la aplicacion del webServer');
			this.getAppDefinition(true, callback);
		}
	},	
	
	showModal: function(value, type) {
		if(value != '') {
			if(type == 'widget_file') {
				if(!bitamApp.onLine()) {
					Notification.alert($.t('Operation for documents is only available online'));
					return;
				}
				var showingelement = $('<img/>', {'src':value});
			}
		if(type == 'widget_photo') {
            if(bitamApp.isMobile.isApp()) {
                var showingelement = $('<img/>', { 'src':'data:image/jpeg;base64,'+value });
            } else {
				var showingelement = $('<img/>', {'src':'../../'+value});
            }
		}
		if(type == 'widget_signature') {
			var showingelement =
			$('<div/>', {'class':'sigPad', 'id':'showingSignature'}).append(
				$('<div/>').append(
					$('<ul/>', {'class':'sigNav'}).append(
						$('<li/>', {'class':'clearButton'}).append(
							$('<a/>', {'text':'Clear'})
						)
					),
					$('<div/>',{'class':'sig sigWrapper'}).append(
						$('<div/>',{'class':'typed'}),
						$('<canvas/>',{'class':'pad', 'width':'298px', 'height':'150px'}),
						$('<input/>', {'type':'hidden', 'name':'output', 'class':'output'})
					)
				)
			);
		}
		if(type == 'widget_geolocation') {
			var showingelement =
			$('<div/>', { 'id':'googleMapModal', 'name':'googleMapModal', 'class':'googleMapModal', 
				'style':'width:100%;height:500px;' });
		}
		BitamInterface.windowModal({
			element: showingelement
		});
		
		if(type == 'widget_geolocation') {
			/** 2014-01-15@ARMZ: (#mapsSingleton) Nueva forma de obtener el objeto del mapa*/
			var map = bitamMaps.getGoogleMap({mapDOM: document.getElementById('googleMapModal')});
			var coords = value.split(',');
			var latitudtmp = ''+$.trim(coords[0]);
			var longitudtmp = ''+$.trim(coords[1]);
			var point = new google.maps.LatLng(latitudtmp, longitudtmp);
			map.setCenter(point,16);
			var marker = new google.maps.Marker({ position: point, title: "", map: map});
		}
		
		if(type == 'widget_signature') {
			value = value.replace(/lx/g, '\"lx\"');
			value = value.replace(/ly/g, '\"ly\"');
			value = value.replace(/mx/g, '\"mx\"');
			value = value.replace(/my/g, '\"my\"');
			$('.sigPad').signaturePad({ displayOnly: true }).regenerate(value);
			}
		}
	},
	//obtiene el valor de configuracion de si_forms_configura
	//aKey => llave de la configuracion
	//aUser => llave del usuario de la configuracion, 0 es una configuracion global par todos los usuarios
	getConfiguration: function(aKey, aUserKey){
		if(!isset(aUserKey)) aUserKey = 0;
		for(var key in bitamApp.dataApp.configura){
			if(bitamApp.dataApp.configura[key].cla_configura == aKey && bitamApp.dataApp.configura[key].cla_usuario == aUserKey){
				return bitamApp.dataApp.configura[key].ref_configura;
			}
		}
		return false;
	},	
	isIE:function()
	{
		/*var anObject = $.browser;
		if(anObject.msie)
			return anObject;*/
		return false;
	},
	
	setURI:function(a)
	{
		this.uri=a;
		return this;
	}, 
	
	getURI:function()
	{
		return this.uri;
	},
	
	setUser:function(a)
	{
		if(bitamApp.application.mdVersion >= 3.51) {
			var key;
			//Criterios de seguridad en formas
			for(key in a.securityCriteria) {
				a.securityCriteria[key].actions_2 = eval('1&&' + a.securityCriteria[key].actions_2);
				a.securityCriteria[key].actions_3 = eval('1&&' + a.securityCriteria[key].actions_3);
				a.securityCriteria[key].actions_4 = eval('1&&' + a.securityCriteria[key].actions_4);
			}
			//Criterios de seguridad en vistas
			for(key in a.securityCriterionView) {
				a.securityCriterionView[key].actions_2 = eval('1&&' + a.securityCriterionView[key].actions_2);
				a.securityCriterionView[key].actions_3 = eval('1&&' + a.securityCriterionView[key].actions_3);
				a.securityCriterionView[key].actions_4 = eval('1&&' + a.securityCriterionView[key].actions_4);
			}
		}
		this.user = a;
		this.dataApp.user = a;

		/*
		El objeto del usuario incluye las vistas personalizadas que se agregan a cada forma
		*/
		for(var keyView in a.ViewsUser){
		    for(var keySection in bitamApp.application.payload.sections){
			if(bitamApp.application.payload.sections[keySection].id_section == a.ViewsUser[keyView].section_id)
			 {
			     bitamApp.application.payload.sections[keySection].payload.views[a.ViewsUser[keyView].id]= a.ViewsUser[keyView];
			 }
		    }
		}
		//2013-11-09@EVEG :(#setDefaultVisibility) Mandar a llamar a la funcion que oculta o muestra el los campos de la forma
		for(var keySection in a.visibilityByUser) {
			var aSection = bitamApp.application.getSection(keySection);
			for(var keyField in a.visibilityByUser[keySection]) {
				if(aSection) {
					console.log(keyField, JSON.stringify(a.visibilityByUser[keySection][keyField]));
					aSection.setDefaultVisibility(keyField, a.visibilityByUser[keySection][keyField])
				}
			}						
		}
	},
	
	getUser:function()
	{
		return this.user;
	},
	
	setOption:function(a,b) {

		if(typeof b != 'undefined')
			this.option[a] = b;

		return this;
	},
	
	getOption:function(a)
	{
		return this.option[a];
	},
	
	//Prop- Regresa la ruta al Web Service de eBavel, la cual se configura en la definición de la aplicación (appDefinition.php)
	getURLServer:function()
	{
		return this.application.server;
	},
	
	getURLSyncData:function()
	{
		return this.application.server + 'syncData.php';
	},
	
	getURLCustomPHP:function(bRelative)
	{
		if(bRelative)
			return 'custom/' + bitamApp.application.codeName + "/";
		return this.application.server + 'custom/' + bitamApp.application.codeName + "/";
	},
	
	//Ajax- Realiza una petición al Web Service para traer los usuarios, catálogos y sus datos de las tablas (estos ya no aplican) así como los datos 
	//de las tablas de todas las secciones de la aplicación especificada (incluyendo el propio eBavel)
	getDataApp:function(fn) {
		a = this;
		var allCatalog = this.getAllCatalog();
		
		var params = new Object();
		params.applicationCodeName = this.application.codeName;
				
		//por mientras cargamos este catalogo de forma manual.
		for(var key in allCatalog) {
			if(allCatalog[key] == "fieldvalueshistory") {
				delete allCatalog[key];
			}
		}
		
		params['catalog'] = allCatalog;
		/*
		params['BITAM_UserName'] = bitamApp.dataApp.user.nom_corto, 
		params['BITAM_Password'] = '8264', 
		params['BITAM_RepositoryName'] = eMeeting local, 
		*/
		
		this.getJSONRequest( {
			url:this.getURLServer() + '/action.php?action=getDataApp',
			data:params,
			success:function(data) {
				//bitamApp.pageLoading(true);
				
				if (a.application.codeName != 'eBavel') {
					var template = a.application.getSection('FRM_templates');
					//validamos si existe el formulario de Template
					if(template && isset(data.payload.data)) {
						if($.isArray(a.dataApp['templates']) && a.dataApp['templates'].length() > 0) {
							a.dataApp['templates'].add(data.payload.data.templates);
						}
						else {
							a.dataApp['templates'] = new DataCollection(data.payload.data.templates,template.payload.fieldsData);
						}
					}
					//14/01/2013@aramirez - Agregamos los TPL, los cargamos al inicio para poder comparar 
					if(isset(data.payload.data) && !$.isEmptyObject(data.payload.data.dataTPLs)) {
						for(var keyTPL in data.payload.data.dataTPLs) {
							if(isset(bitamApp.dataUser[keyTPL]))
								bitamApp.dataUser[keyTPL].add(data.payload.data.dataTPLs[keyTPL]);
						}
					}
					//14/01/2013@aramirez - Cargamos los datos de las aplicaciones, solo para cuando se logea un usuario con permisos de eBavel 
					if(bitamApp.application.getSection('APPS') && isset(data.payload.data) &&!$.isEmptyObject(data.payload.data.APPS) && isset(bitamApp.dataUser['APPS'])) {
						bitamApp.dataUser['APPS'].add(data.payload.data.APPS);
						delete data.payload.data.APPS;
					}
					//carga los datos de los actionstemplate para poder crear action items desde la creacion, modificacion de un registro a partir de un campo catalogo
					var aSection = bitamApp.application.getSection('ACTIONSTEMPLATE');
					if($.isPlainObject(aSection)) {
						bitamApp.dataUser.ACTIONSTEMPLATE.add(data.payload.data.actionstemplate)
					}
					if(isset(data.payload.data)) {
						delete data.payload.data.templates;
						delete data.payload.data.actionstemplate;
					}
				}
				if(isset(data.payload.data)) {
					a.dataApp = $.extend(true, a.dataApp, data.payload.data);
					//por mientras cargamos este catalogo de forma manual.
					a.dataApp.catalog.fieldvalueshistory = new Object();
					a.dataApp.catalog.fieldvalueshistory["1"] = {description: "No", id_fieldvalue: "1", value: "0"};
					a.dataApp.catalog.fieldvalueshistory["2"] = {description: "Yes", id_fieldvalue: "1", value: "1"};
				}
				
				/** 2014-01-07@ARMZ: (#) Elimina formas y menu que no sean de la version 4*/
				if(bitamApp.application.codeName == 'eBavel') {
					/** Las formas de businessProcess, businessProcess_Steps, businessProcess_Actions solo van en la version 4*/
					if(data.payload.data.mdVersion && data.payload.data.mdVersion < 4) {
						delete bitamApp.application.payload.sections.businessProcess;
						delete bitamApp.application.payload.sections.businessProcess_Steps;
						delete bitamApp.application.payload.sections.businessProcess_Actions;
						/** 2014-01-24@ARMZ (#) Eliminamos la forma de notificaciones*/
						delete bitamApp.application.payload.sections.notifications;
						/** Removemos el item de navegacion del menu principal */
						bitamApp.application.payload.navigation = jQuery.grep(bitamApp.application.payload.navigation, function(item) {
						  return item.section != 'businessProcess';
						});
					}
				}
				bitamApp.isMobile.isApp() && setDataApp();
				if (a.application.codeName != 'eBavel') {
					//ahora obtenemos todos los datos del usuaio
					//ya no cargamos todos los datos, ahora lo hacemos por demanda
					if($.isFunction(fn)) {
						fn('dataApp');
					}
				}
				else {
					//cargamos todos los datos cuando es eBavel por los fields que tienen la propiedad .catalog que puede hacer uso de secciones que aun no estan cargadas					
					a.getAllDataSection(fn);
				}				
			}
		})
	},
	
	//Ajax- Hace una petición al WebService para obtener los datos de todas las tablas de secciones
	getAllDataSection:function(fn)
	{
		bitamApp.pageLoading("Loading data");
		var sections = this.application.getSections();
		var b = this;
		
		var sectionsName = new Array();
		for(var key in sections)
		{
			if(isset(sections[key].extend) || sections[key].id_section == "FRM_templates")
			{
				continue;
			}
			if(sections[key].id_section == "-1" && bitamApp.application.codeName == "eBavel"){
				continue;
			}
			sectionsName.push(sections[key].name);
		}
		
		var params = {};
		params.applicationCodeName = this.application.codeName;
		params.user = bitamApp.user.cuenta_correo;
		params.password = bitamApp.user.password;
		params.section = sectionsName;
		
		params.lastCreatedDate = {}
		params.lastModifiedDate = {}
		
		for(var key in sections)
		{
			params.lastCreatedDate[key] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[key]) ? bitamApp.dataCache.loadDate[key].lastCreatedDate : '');
			params.lastModifiedDate[key] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[key]) ? bitamApp.dataCache.loadDate[key].lastModifiedDate : '');
		}
		
		this.getJSONRequest({
			url:this.getURLServer() + '/getData.inc.php', 
			data: params,
			success:function(data)
			{	
				bitamApp.pageLoading(true);
				res = new BuilderAppResponse(this);
				$.extend(res, data);
				
				var dataSections = res.getPayload().data;
				var loadDate = res.getPayload().loadDate;
				if(!b.dataCache.loadDate)
					b.dataCache.loadDate = {};
				for(var key in dataSections)
				{
					var section = b.application.getSection(key);
					if(isset(b.dataUser[section.name]) && b.dataUser[section.name].length() > 0)
					{
						b.dataUser[section.name].add(dataSections[key]);
					}
					else
					{
						b.dataUser[section.name] = new DataCollection(dataSections[key],section.payload.fieldsData);
					}
					
					b.dataCache[section.name] = true;
					b.dataCache.loadDate[section.name] = loadDate[section.name];
				}
				//b.updateDataAppStorage();			
				if($.isFunction(fn))
				{
					fn('dataApp');
				}
			}
		});
	},
	
	//Ajax- Hace una petición al Server para enviar el template especificado del valor de la forma indicada en los parámetros, con lo cual además
	//se reemplezarán todos los valores de las variables que estén capturadas en el cuerpo del EMail con los campos correspondientes de las formas
	//involucradas (incluso las formas padres)
	sendTemplateEMail:function(aSectionName, aTemplate_id, aFieldValue)
	{
		var b = this;
		
		var section = b.application.getSection(aSectionName);
		
		filter = new Object();
		filter['id_'+section.item] = aFieldValue;
		
		var params = {};
		params.applicationCodeName = this.application.codeName;
		params.section = aSectionName;
		params.template_id = aTemplate_id;
		params.filter = filter;
		params.username = this.dataApp.user.cuenta_correo;
		
		this.getJSONRequest({
			url:this.getURLServer() + '/sendTemplateEMail.php', 
			data: params,
			success:function(data)
			{	
				bitamApp.pageLoading(true);
				res = new BuilderAppResponse(this);
				try
				{
					$.extend(res, data);
				} catch(err)
				{
					BitamInterface.alert($.t('There was an error parsing the server response to this action: ') + data);
					return;
				}

				if(res.error != 0)
				{
					BitamInterface.alert($.t("There was an error sending the section template EMail") + "\r\n"+res.errorDescription);
					return;
				}
				
				BitamInterface.alert($.t('Section template EMail sent successfully'));
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				bitamApp.pageLoading(true);
    	        bitamApp.alert(textStatus);
			}				
		});
	},
	verifyImages :function(navs) {
		if(!bitamApp.onLine())
			return;
	    var images = [];
	    for(var key in navs ) {
			if(navs[key].image) {
				images.push(navs[key].image)
			}
	    }
		if(bitamApp.application.payload.options.logo != null)
			images.push(bitamApp.application.payload.options.logo);
	    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {	
			for(var key in images ) {
				var url = bitamApp.getURLServer() + images[key];
				var imagen_name = [];
				imagen_name =images[key].split("/");
				url = encodeURI(url);
				var imagePath = fs.root.fullPath + "/"+imagen_name[imagen_name.length-1];  // full file path
				
				fs.root.getFile(imagePath, { create: false }, function(){
					console.log('El archivo de imagen existe');
				}, $.proxy(function(){
                    var imagePath = this.imagePath, url = this.url;
					console.log('Descargando imagen: ' + imagePath);
					var fileTransfer = new FileTransfer();
					fileTransfer.download(url, imagePath, function (entry) {
					}, function (error) {
						console.log("upload error code: " + error.code);
					});
                }, {imagePath:imagePath, url:url}));
			}
	    })
	},
	new_pageLoading: function(settings) {
		if(settings===true) {
			NProgress.settings.msgs.pop()
			setTimeout(function(){
				if(!NProgress.settings.msgs.length)
					NProgress.done();
			}, 500)
		}
		else {
			if(!NProgress.isRendered()) {
				NProgress.settings.positionUsing = '';
				NProgress.settings.type = settings.type || NProgress.settings.type;
				NProgress.start();
			}
			NProgress.settings.msgs.push(settings.text);
			NProgress.setText(settings.text);
		}
	},
	pageLoading:function(msg) 
	{
		return;
    },
    
    //Ajax- Obtiene la definición JSon de la aplicación de 'eBavel' y asigna la referencia del application al BitamInterface
	getAppDefinition:function(initInterface, callback)
	{
		if(initInterface == undefined)
		{
			initInterface = false;
		}
		var url = bitamApp.getURI();
		var a=this;
		this.getJSONRequest(
			{
				showLoadingMsg: {
					text: 'Loading definition',
					type: 'bootstrap'
				},
				url:url,
				dataType: (url.indexOf('appDefinition.php') != -1 ? "jsonp" : 'json' ),
				params:{'isApp':bitamApp.isMobile.isApp()},
				success:function(data)
				{
					bitamApp.pageLoading(true);
					res = new BuilderAppResponse(this);
					$.extend(res, data);

					for( var sOption in data.options ) {
						bitamApp.setOption(sOption, data.options[sOption]);
					}

					//EVEGA
					if(bitamApp.isMobile.isApp()) {

						//2013-11-21@ARMZ: () Temporalmente se coloca en false
						if(false && BitamStorage.getHashVersion() && BitamStorage.getHashVersion() != res.hashVersion) {
							alert("Your version of app needs updating");							
						}
						else {
							BitamStorage.setHashVersion(res.hashVersion);
						}
						if(false && BitamStorage.getVersionApp() && BitamStorage.getVersionApp() != res.versionApp) {
							alert($.t("Your version of app needs updating"));
						}
						else {
							BitamStorage.setVersionApp(res.versionApp);
						}
					}
					for(var key in res.payload.sections) {
						res.payload.sections[key] = new Section(res.payload.sections[key]);
					}
					a.application = res;
					a.application.appVersion = a.appVersion;
					if(initInterface) {
						BitamInterface.init(a.application);
					}
					
					bitamApp.isMobile.isApp() && setAppDefinition(data);
					
					if($.isFunction(callback)) {
						callback();
					}

					if(bitamApp.isMobile.isApp()) {
						bitamApp.verifyImages(bitamApp.application.payload.navigation);
					}
				}
			});
	},
	
	logOut:function(){
        delete localStorage.persistentAccess;
		//2013-13-12@MABH: (App Launcher) Si se abrio la app desde otra app se debe volver a abrir
		var password = localStorage['decryptedPassword'];
        if(password && bitamApp.fromURL) {
			if(bitamApp.isMobile.iOS()) {
                window.location = 'gncapp://';
            } else {
                navigator.app.loadUrl('gncapp://', {openExternal: true});
            }
        } else {
            window.location = 'index.html';
        }
	},
	
	restoreApplication: function(){ 
		BitamStorage.setLastSyncDate(undefined);
		setAppDefinition(null);
		bitamApp.logOut();
		BitamStorage.clear();
		localStorage.clear();
	},
	
	/**
	login a la aplicacion
	
	Puede recibir options donde podemos redefinir el callback despues de login y mandarle los datos de usuario y password para firmarse a la aplicacion
	*/
	login:function(options) {
		var options = options || {};
		var username = options.username || $('#username').val();
		var password = (typeof options.password == "undefined" ? $('#password').val() : options.password);
		var repository = options.repository || $('#repository').val();
		//si esta variable es un Objeto, quiere decir que es un login persistente
		var oPersistentAccess = (localStorage["persistentAccess"] ? JSON.parse(localStorage["persistentAccess"]) : false);
		//var persistentLogin =  ($('#PersistentLogin')[0] ? $('#PersistentLogin')[0].checked : false);
		//13/08/2012 - permitimos el login persistente cada vez que se logee desde la App		
		var persistentLogin = bitamApp.isMobile.isApp();
		if($.isPlainObject(oPersistentAccess)){
			username = oPersistentAccess.user.cuenta_correo;
			password = oPersistentAccess.user.password;
		}
		
		//2013-13-12@MABH: (App Launcher) Si se abrio la app desde otra app debe hacerse login con el usuario y pass enviado
        if(bitamApp.fromURL) {
			username = urlUser;
			password = urlPass;
            localStorage['decryptedPassword'] = password;
		}
		
		var a = this
		
		if($.trim(username) == '')
		{
			this.pageLoading('');
			//mostramos una ventana de error
			//traduccion
			bitamApp.alert('Invalid email');
			return ;
		}
		
		
		//despues de hacer login y despues de obtener los datos de aplicacion lo mandamos a la pantalla principal
		var fn = function()
		{       
			//carga del lenguaje
			var dfdLanguage = $.Deferred();
			var sLang = 'EN';
			if (bitamApp.application.codeName != 'eBavel' && bitamApp.getConfiguration(101)){
            	sLang = bitamApp.getConfiguration(101);
			}
			$.jsperanto.init(function(t){dfdLanguage.resolve();},{"dicoPath":bitamApp.buildURL('locales'), "lang":sLang, 'keyseparator':'|'});
			dfdLanguage.promise().then(function() {
			
				if($.isFunction(options.callback)) {
					options.callback();
					return
				}

				if(bitamApp.user.applicationCodeName == "eBavelSecurity"){
					bitamApp.application.payload.navigation = [bitamApp.application.payload.navigation[0]]
					bitamApp.application.payload.sections.sections.canEdit = false
					bitamApp.application.payload.sections.sections.canDelete = false
					bitamApp.application.payload.sections.sections.canAdd = false
					delete bitamApp.application.payload.sections.sections.sections.viewsnewform
					delete bitamApp.application.payload.sections.sections.sections.formsdetails
					delete bitamApp.application.payload.sections.sections.sections.fieldsforms
					for(var kff in bitamApp.application.payload.sections.sections.payload.fieldsForm){
						if(bitamApp.application.payload.sections.sections.payload.fieldsForm[kff].id == "sectionName" || bitamApp.application.payload.sections.sections.payload.fieldsForm[kff].id == "description")
							continue;
						delete bitamApp.application.payload.sections.sections.payload.fieldsForm[kff];
					}
				}
				//el usuario ya ha sido autorizado para entrar al sistema
				if(bitamApp.isMobile.isApp()){
					//si es App permitimos el acceso persistente
					if(persistentLogin){
						//guardamos en localStorage los datos para un acceso persistente, asi la proxima vez que inicie sesion y exista esta informacion se logeara con esto.
						var oPersistentAccess = {
							"user":bitamApp.dataApp.user, 
							"dateLogin":new Date().toString('yyyy-MM-d HH:mm:ss')
						};
						localStorage["persistentAccess"] = JSON.stringify(oPersistentAccess);
					}
				}				
				var sViewActive = '';
				if(a.application.codeName == 'eBavel'){
					a.section('apps', undefined, {'window':true})	
				}
				else{
					if(!bitamApp.isMobile.any()){
						if(bitamApp.application.codeName == 'eBavel' && isset(bitamApp.application.payload.navigation[key].section)){
							bitamApp.section(bitamApp.application.payload.navigation[key].section);
						}else{
							var defaultNav = bitamApp.user.applications[bitamApp.application.codeName].defaultNav;
							for(var key in bitamApp.application.payload.navigation)
							{
								if(defaultNav != "" && defaultNav == bitamApp.application.payload.navigation[key].id)
								{
									defaultNav = bitamApp.application.payload.navigation[key];
									sViewActive = defaultNav.view;
									break;
								}
								if(bitamApp.application.payload.navigation[key].active == true )
								{
									if(isset(bitamApp.application.payload.navigation[key].section) && bitamApp.application.codeName == 'eBavel')
									{
										
									}
									else{
										sViewActive = bitamApp.application.payload.navigation[key].view;	
									}
								}
								if(bitamApp.application.payload.navigation[key].childens.length > 0)
								{
									for(var keyCh in bitamApp.application.payload.navigation[key].childens)
									{
										if(defaultNav != "" && defaultNav == bitamApp.application.payload.navigation[key].childens[keyCh].id)
										{
											defaultNav = bitamApp.application.payload.navigation[key].childens[keyCh];
											sViewActive = defaultNav.view;
											break;
										}
										if(bitamApp.application.payload.navigation[key].childens[keyCh].active == true )
										{
											if(isset(bitamApp.application.payload.navigation[key].childens[keyCh].section) && bitamApp.application.codeName == 'eBavel')
											{

											}
											else{
												sViewActive = bitamApp.application.payload.navigation[key].childens[keyCh].view;	
											}
										}
									}
									
								}
							}
							$.each(bitamApp.user.viewsSecurity, function( index, value ) {
										if(sViewActive == index && bitamApp.user.viewsSecurity[index].canView == false)
										{
											sViewActive = "Nopermiso";
										}
							});
							if(!bitamApp.isMobile.isApp()){
							    if(bitamApp.application.navSettings != 0) {
								     bitamApp.sectionView("mainMetroNav");
							    }
							    else {
									if (sViewActive == "Nopermiso") {
						            	alert($.t("You do not have permission to access the default page, Contact the administrator"));
										//bitamApp.logOut();
										return;
									}
									//JAPR 2013-09-04: Si no hay una vista default para navegación no debe continuar
									if (!sViewActive) {
						            	alert($.t("There's no default navigation view for this application"));
						            	return;
									}
									//JAPR
								    bitamApp.sectionView(sViewActive);
							    }
							}								
						}
					}				
				}
				if(bitamApp.application.codeName != "eBavel" && bitamApp.isMobile.isApp())
				{
					//despues de haber cargado la informacion inicializamos la base de datos
					//hay que inicializarla aqui despues de haber cargado la definicion de la aplicacion y los datos de la aplicacion
					//por que se descargaran y almacenaran en este momento todos los datos siempre y cuando el usuario logeado no exista previamente de forma local
					initDataStore({forms:bitamApp.application.payload.sections}).then(function(){
						console.debug('Datos cargados, iniciamos la interfaz de usuario');
						if(bitamApp.onLine()){
							//guardamos la definicion de la aplicacion cargada
							delete bitamApp.application.request;
							/** 2014-01-28@ARMZ: (#SetUserApp) La informacion del usuario la guardamos despues de hacer login y checar sus credenciales*/
						}						
						//Verifica si existen datos cargados para este usuario y los actualiza
						loginDatabase(username);
						//2013-11-26@RTORRES:(#iphoneUse) ahora siempre aplica la metronavegacion.
						if(bitamApp.application.navSettings == 1) {
							bitamApp.sectionView("mainMetroNav");
						}else {
							//bitamApp.application.navSettings = 2;
							//bitamApp.sectionView("mainMetroNav");
							BitamInterface.createMainMobilePage();
							BitamInterface.changePage('#mainPageMobil');
						}
					})
					return;
				}
			}, function(){alert($.t("Language file could not be loaded"));});
			
		};
        
        for(var key in bitamApp.application.payload.sections)
		{
            //inicializamos la variable
			if(bitamApp.application.payload.sections[key].id_section == "-1" && bitamApp.application.codeName == "eBavel"){
				//no inicializamos ACTIONITEMS cuando es eBavel pues no es necesario
				continue;
			}
			bitamApp.dataUser[bitamApp.application.payload.sections[key].name] = new DataCollection({},bitamApp.application.payload.sections[key].payload.fieldsData)
		}
		
		//2014-01-13@ARMZ: Si es una APP/APK y ya tenemos el usuario cargado y el usuario y es un login persistente, no verificamos con el server la autentificacion
		if(bitamApp.isMobile.isApp()) {
			bitamApp.dataApp.user = getDatausers();
			if(bitamApp.dataApp.user && bitamApp.dataApp.user[username.toLowerCase()] != undefined) {
				console.log('Revisamos el usuario almacenado...')
            	bitamApp.dataApp.user = bitamApp.dataApp.user[username.toLowerCase()];
            	if(this.dataApp.user.cuenta_correo.toLowerCase() == username.toLowerCase() && (this.dataApp.user.password == hex_md5(password) || this.dataApp.user.password == password)) {
            		bitamApp.pageLoading(true);
            		bitamApp.dataApp = getDataApp(bitamApp.dataApp.user.nom_corto);
					this.setUser(this.dataApp.user);
					fn();
					return;
				}
				else if(!bitamApp.isOnline()) {
					bitamApp.pageLoading(true);
					bitamApp.alert("Invalid username/password");
					//2013-13-12@MABH: (App Launcher) Si se abrio la app desde otra app debe enviarse a la pagina de loginpage y reabrir la otra app
	                if(bitamApp.fromURL) {
	                    $.mobile.changePage('#loginpage');
	                    navigator.app.loadUrl('gncApp://', {openExternal: true});
	                }
	                return;
				}
            }
            else if(!bitamApp.onLine()) {
            	bitamApp.alert("No data available for this user");
            	return;
            }
		}
		
		//this.ajaxRequest({
		this.getJSONRequest({
				showErrorFunction: options.showErrorFunction,
				//2013-11-07@ARMZ: () Define el tipo de mensaje en la ventana de carga
				showLoadingMsg: {
					text: 'Authenticating user',
					type: 'bootstrap'
				},
				url:this.getURLServer() + '/login.inc.php',
				data:{
					'BITAM_UserName':username, 
					'BITAM_Password':password, 
					'BITAM_RepositoryName':repository, 
					'codeName': this.application.codeName
				},
				success:function(responseData) 
				{
					//bitamApp.pageLoading(true);
					response = new BuilderAppResponse(this);
					$.extend(response, responseData);
					
					if(response.error)
					{
                        alert(response.errorDescription);
						//2013-13-12@MABH: (App Launcher) Si se abrio la app desde otra app debe enviarse a la pagina de loginpage y reabrir la otra app
                        if(bitamApp.fromURL) {
                            $.mobile.changePage('#loginpage');
                            if(bitamApp.isMobile.iOS()) {
                                window.location = 'gncapp://';
                            } else {
                                navigator.app.loadUrl('gncapp://', {openExternal: true});
                            }
                        }
						return;
					}
					//agregamos al user el repositori name
					response.payload.User.repositoryName = repository;
					/** Agregamos al objeto User la informacion de vistas de usuario para que se guarde en memoria en una APP/APK*/
					response.payload.User.ViewsUser = response.payload.ViewsUser
					/** Agregamos al objeto User la informacion de los campos que el puede o no puede ver para que se guarde en memoria en una APP/APK*/
					response.payload.User.visibilityByUser = response.payload.visibilityByUser
					/** (#SetUserApp)*/
					!bitamApp.isMobile.isApp() || setDatausers(response.payload.User);
					a.setUser(response.payload.User);
					//JAPR 2013-09-13: Agregada la lectura de la versión de Metadata de eBavel
					bitamApp.application.mdVersion = response.payload.MDVersion;
					//JAPR
					a.getDataApp(fn);			
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bitamApp.pageLoading(true);
                    bitamApp.alert(textStatus);
					//2013-13-12@MABH: (App Launcher) Si se abrio la app desde otra app debe enviarse a la pagina de loginpage y reabrir la otra app
                    if(bitamApp.fromURL) {
                        $.mobile.changePage('#loginpage');
                        if(bitamApp.isMobile.iOS()) {
                            window.location = 'gncapp://';
                        } else {
                            navigator.app.loadUrl('gncapp://', {openExternal: true});
                        }
                    }
				}				
		});
		
	},
	
	ajaxRequest:function(userSettings)
	{
		var dfd = $.Deferred();
		var T = $.t || function(t) { return t};
		
		if(!$.isset(userSettings.showLoadingMsg))
			userSettings.showLoadingMsg = true;
		//2013-11-07@ARMZ: () Define el tipo de mensaje en la ventana de carga
		if(userSettings.showLoadingMsg) {
			var settingsProgressBar = {text:'Loading', type: 'basic'};
			if(typeof userSettings.showLoadingMsg == 'string') {
				settingsProgressBar.text = userSettings.showLoadingMsg;
			} else {
				$.extend(settingsProgressBar, userSettings.showLoadingMsg);
			}
			this.new_pageLoading(settingsProgressBar);
		}
			
		
		var fnClearData = function(){
			var bFailSyncData = false;
			if(userSettings.url.indexOf('syncData.php') != -1){
				for(var sSection in userSettings.data.data){
					for(var key in userSettings.data.data[sSection]){
						if(key<0){
							//removemos los datos que se estaban intentando agregar
							bitamApp.dataUser[sSection].deleteItem(key);
						}else{
							//si los datos se estaban actualizando marcamos una bandera indicando que posiblemente los datos no han sido actualizados
							bFailSyncData = true;
						}
					}
				}
			}
			return bFailSyncData;
		}
		
		var a = this;
		settings = {
			cache: false,
      		crossDomain : false,
      		timeout: 1800000,
			type : 'POST',
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				userSettings.showLoadingMsg === false || bitamApp.new_pageLoading(true);
				//estamos haciendo una sincronizacion de datos, se puede estar agregando nuevos datos o actualizando
				var bFailSyncData = fnClearData(), errorDescription = '', title = '';
				
				//2013-26-12@MABH: (#31010) Cuando no se tiene conexion status deuelve un 0 y se muestra el mensaje de falla en la conexion
                if(XMLHttpRequest.status == 0) {
                	errorDescription = T('The server is not responding or is not reachable. ') + (bFailSyncData ? T('Failure while saving the data, it is posible they were not saved correctly') + '.' : '');
				} else {
					if(textStatus == 'timeout'){
						errorDescription: T('You have lost your connection, try to connect again later. ') + (bFailSyncData ? T('Failure while saving the data, it is posible they were not saved correctly') + '.' : '');
						title = 'Time out';
					} else if(textStatus == 'parsererror') {
						errorDescription: T('You have lost your connection, try to connect again later. ') + (bFailSyncData ? T('Failure while saving the data, it is posible they were not saved correctly') + '.' : '');
						title = '';
					}
					else{
						errorDescription:XMLHttpRequest.responseText + (bFailSyncData ? " "+ T('Failure while saving the data, it is posible they were not saved correctly') + '.' : '');
						title = textStatus;
					}
				}
				bitamApp.showMsgError({
						errorDescription: errorDescription, 
						title: title
					})
				dfd.reject();
			}
    	}
    	
    	$.extend(settings, userSettings);
		//if(!$.isPlainObject(settings.data)){
		if(settings.data == undefined){
			settings.data = {};
		}
		//JAPR 2013-09-13: Agregada la versión de la Aplicación
		if($.isPlainObject(settings.data) && bitamApp.dataApp) {
			if(bitamApp.dataApp.user && bitamApp.dataApp.user.nom_corto) {
				if(!settings.data['BITAM_UserName']) {
					settings.data['BITAM_UserName'] = bitamApp.dataApp.user.nom_corto;
					settings.data['BITAM_Password'] = bitamApp.dataApp.user.password;
					settings.data['BITAM_RepositoryName'] = bitamApp.dataApp.user.repositoryName;
				}				
				if(bitamApp.user.applicationCodeName == "eBavel"){
					settings.data['appIdGen'] = bitamApp.session("appId");
				}
			}
			settings.data['eBavelApp_isApp'] = bitamApp.isMobile.isApp();
			settings.data['eBavelApp_smallScreen'] = bitamApp.isMobile.pantallasreducidas();
			settings.data['appVersion'] = bitamApp.appVersion;
			if(bitamApp.getOption('environment') == 'development')
				settings.data['environment'] = bitamApp.getOption('environment');
		}
		//JAPR
		if(typeof settings.data == "string"){
			var sData = "BITAM_UserName=" + bitamApp.dataApp.user.nom_corto + "&BITAM_Password=" + bitamApp.dataApp.user.password + "&BITAM_RepositoryName=" + bitamApp.dataApp.user.repositoryName;
			if(settings.data == ""){
				settings.data = sData;
			}else{
				settings.data = settings.data + "&" + sData;
			}
			//JAPR 2013-09-13: Agregada la versión de la Aplicación
			settings.data = settings.data + "&eBavelApp_isApp=" + (bitamApp.isMobile.isApp() ? "true":"false") + '&appVersion='+bitamApp.appVersion;
			if(bitamApp.getOption('environment') == 'development')
				settings.data = settings.data + "&environment=" + bitamApp.getOption('environment');
			//JAPR
		}
		if($.isEmptyObject(settings.data)){
			settings.data['eBavelApp_isApp'] = bitamApp.isMobile.isApp();
		}
    	/*
    	if(isset(settings.data))
    		alert(settings.url + "?" + jQuery.param(settings.data));
    	else
    		alert(settings.url);
    	*/
		/*
		settings.dataType = "json";
		settings.type = "POST";
		settings.crossDomain = false;
		delete settings.jsonp;
		debugger;*/
		var success = settings.success;
		//funcion general para mostrar algun error generado en el lado del server
		settings.success = function(response){
			userSettings.showLoadingMsg === false || bitamApp.new_pageLoading(true);
			if(response.error>0){
				if(settings.showErrorFunction) {
					settings.showErrorFunction({errorDescription: response.errorDescription, errorSummary: response.errorSummary, error: response.error});
					/*2014-03-03 @JRPP (#31812) Agregamos un reject para poder validar cuando ocurre un error*/
					dfd.reject();
					return;
				}
				var res_ClearData = fnClearData();
				dfd.reject();
				bitamApp.showMsgError({
					errorDescription:response.errorDescription,
					errorSummary: ($.trim(response.errorSummary)!= '' ? response.errorSummary : null),
					errorIsCritical: response.errorIsCritical,
					error: response.error
				})
				return;
			}
			if(!$.isEmptyObject(response.message)) {
				bitamApp.showMsgError({
					errorDescription: $.t(response.message.title),
					errorSummary: $.t(response.message.body),
					errorIsCritical: false,
					error: 0,
					title: $.t(response.message.title)
				});
			}
			success(response);
			dfd.resolve(response);
		}
		$.ajax(settings);
		return dfd.promise();
	},
	
	//Ajax- Obtiene una respuesta JSon del Web Service a partir de una petición enviada en JSon
	getJSONRequest:function(userSettings)
	{
		if(bitamApp.isMobile.isApp())
		{
			if(true || $.isPlainObject(userSettings.data) && JSON.stringify(userSettings.data).length>1000){
				settings = {
					dataType: "json",
					success: function(){},
					type:"POST"
				}
			}
			else{
				settings = {
					dataType: "jsonp",
					jsonp : "jsonpCallback",
					success: function(){},
					crossDomain : true
				}
			}
		}
		else
		{
			settings = {
					dataType: "json",
					success: function(){},
					type:"POST"
				};
		}
		
		$.extend(settings, userSettings);
		return this.ajaxRequest(settings);
	},
	
	alert:function(msg){
		if(bitamApp.isMobile.isApp()&&navigator.notification){
			navigator.notification.alert(msg);
		}
		else{
			alert(msg);
		}
	},
	
	//Dom- Genera la vista de colección de la forma (invocada desde el click a los botones de navegación)
	/*
		options.changePage.transition 
			indica que tipo de transicion usar al cambiar de pagina
	*/
	sectionView:function(a, changePage, options)
	{
		if(!isset(options))
		{
			options = new Object();
		}
		if(a=="FRMW_MYACTIONITEMS")
		{
        	options.filter = {"status":"in progress"}
		}
		var section = this.application.getSection(a);
		var section = $.extend(true, {}, section);
		options.sectionView = a;
		BitamInterface.createSection(section, {sectionView:a});
		fn = function(data)
		{
			BitamInterface.showSection(section.name, data, changePage, options)
		};
		
		//checamos si para esta vista existe un filtro
		if(section.payload.views[a].viewCriterias.length > 0)
		{
			//empezamos a construir el filtro a partir de la informacion
			/*var anFilter = {};
			for(var key in section.payload.views[a].viewCriterias)
			{
				var eachViewCriteria = section.payload.views[a].viewCriterias[key];
				//preparamos el tipo de filtro
				sFilter = "|" + eachViewCriteria.fieldForm.id + "|";
				switch(eachViewCriteria.condition)
				{
					case "1": sFilter = sFilter + "="; break;
					case "2": sFilter = sFilter + "<>"; break;
					case "3": sFilter = sFilter + "="; eachViewCriteria.value = "*"+eachViewCriteria.value+"*"; break;
					case "4": sFilter = sFilter + "<>"; eachViewCriteria.value = "*"+eachViewCriteria.value+"*"; break;
					case "5": sFilter = sFilter + "="; eachViewCriteria.value = eachViewCriteria.value+"*"; break;
					case "6": sFilter = sFilter + "="; eachViewCriteria.value = "*"+eachViewCriteria.value; break;
					case "7": sFilter = sFilter + "="; eachViewCriteria.value = ''; break;
					case "8": sFilter = sFilter + "<>"; eachViewCriteria.value = ''; break;
				};
				anFilter[sFilter] = eachViewCriteria.value;
			}
			section.filterBy = anFilter;*/
			var anFilter = {};
			for(var key in section.payload.views[a].viewCriterias)
			{
				var eachViewCriteria = section.payload.views[a].viewCriterias[key];
				anFilter = $.extend(anFilter, eachViewCriteria.objFilter);
			}
			section.filterBy = anFilter;
		}
		if(!$.isEmptyObject(section.prevDataSection))
		{
			//si la seccion principal necesita precargar datos adicionales para hacer algun filtro se cargan los datos
			//objeto donde guardaremos los objetos de seccion para mandarlos a pedir datos
			var arrSections = new Array();
			for(var key in section.prevDataSection)
			{
				var aSection = bitamApp.application.getSection(section.prevDataSection[key])
				if(aSection)
				{
					arrSections.push(aSection);
				}
				delete aSection;
			}
			arrSections.push(section);
			//ya tenemos todos los objetos de section para pedir los datos.
			bitamApp.getDataSections(arrSections, fn);			
		}
		else
		{
			//si la seccion principal no necesita datos precargados adicionales, se ejecuta directamente
			bitamApp.getDataSection(section, fn);
		}
	},
	
	//Dom- Crea el esqueleto de la sección especificada, posteriormente carga los widgets de su contenido y los dibuja en la pantalla
	section:function(a, changePage, options)
	{
		if(!isset(options))
		{
			options = new Object();
		}
		
		var section = this.application.getSection(a);
		if(!isset(section))
		{
			//si section es undefined entonces 'a' es el id de un VIEW
			options.sectionView = a;
			var section = this.application.getSectionWithView(a);
		}
		
		BitamInterface.createSection(section, {});
		fn = function(data)
		{
			BitamInterface.showSection(section.name, data, changePage, options)
		};
		this.getDataSection(section, fn);		
	},
	
	onLine:function()
	{
		var bRes = false;
		if(bitamApp.isMobile.isApp()) {
			bRes = navigator.connection.type != Connection.NONE
		} else {
			bRes = navigator.onLine;
		}
		//console.log("Network Status: " + (bRes? 'true' : 'false'));
        return bRes;
	},
	//26/02/2013@aramirez
	//@params
	//@localData = un objeto donde las llaves son el nombre de las secciones y los valores es un array donde las llaves es la llave del registro
	//@lastSyncDate = se manda cuando se estan sincronizando datos, fecha de ultima sincronizacion
	//@offlineDeleteRecords = guarda los registros eliminados cuando se estuvo offline
	//@extraParams = Parametros extras que se enviaran como POST
	syncData:function(options)
	{
		//obtenemos todos los datos creados localmente
		var key;
		var localData = {};
		var filter = {};
		
        //si no se le manda una coleccion de datos por seccion a guardar, recorre todo bitamApp.dataUser para sacar los registros con key -1
        if(options.localData == undefined)
        {
			var filter = {};
            for(key in this.dataUser)
            {
				if($.isEmptyObject(bitamApp.dataUser[key].collection)){
					continue;
				}
                var section = this.application.getSection(key);
                //cambio a DataCollection
                //var dataColl = new DataCollection(this.dataUser[key], section.payload.fieldsData);
                //!!!caso especial: se incluye la seccion configura, pero no se actualiza de la misma forma que las demas secciones. No se actualiza en esta funcion
                if(key == "configura") continue;
                var dataColl = this.dataUser[key];
                localData[section.name] = dataColl.getLocalRecords();
				
				//tambien obtenemos los registros que hayan sido modificacados desde la ultima fecha de actualizacion
				//solo si es una App.
				if(bitamApp.isMobile.isApp()){
					var lastDate = BitamStorage.getLastSyncDate();
					if(lastDate)
					{
						lastDate = "'" + lastDate + "'";
						var res = dataColl.filter("modifiedDate>" + lastDate);
						localData[section.name] = $.extend(true, localData[section.name], res);
						delete res;
					}
				}
				                
                //filter[section.name] = section.sections;
                for(var keySections in section.sections)
                {
                    if(filter[section.name] == undefined)
					filter[section.name] = {};
                    if(filter[section.name][keySections]  == undefined)
					filter[section.name][keySections] = {};
                    
                    filter[section.name][keySections]['filterBy'] = section.sections[keySections]['filterBy'];
                }
                
            }
        }
        else
        {
            localData = options.localData;
        }
		
		//alert(JSON.stringify(localData));
		var params = {};
		if($.isPlainObject(options.extraParams)){
			$.extend(params, options.extraParams);
		}
		params.applicationCodeName = this.application.codeName;
		params.data = localData;
		params.filters = filter;
		params.userKey = bitamApp.dataApp.user.cla_usuario;
		if(options.offlineDeleteRecords != undefined){
			params.offlineDeleteRecords = options.offlineDeleteRecords;
		}
		if(options.lastSyncDate != undefined){
			params.lastSyncDate = options.lastSyncDate;
		}
		//ya tenemos los datos generados localmente, ahora a mandarlos al server para guardarlos en la base de datos.
		a = this;				
		
		this.getJSONRequest({
				showLoadingMsg: {
					text: $.t("Sync data"),
					type: options.progressBarType || 'basic'
				},
				url:this.getURLSyncData(),
				data:params,
				error:options.error,
				success:function(data) 
				{
					bitamApp.pageLoading(true);
					//primero, de la propiedad recordsInserted vemos cuales ID's locales fueron insertados y los eliminamos de los datos locales.
					recordsInserted = data.payload.recordsInserted;
					var deleteRows = data.payload.deleteRows;
					if(deleteRows != null) {
						for(var sectionName in deleteRows){
							for(var key in deleteRows[sectionName]){
								bitamApp.dataUser[sectionName].deleteItem(deleteRows[sectionName][key]);
							}
							BitamStorage.removeData(sectionName,deleteRows[sectionName]);
						}
					}
					//2013-12-04@ARMZ: (#historyFunctions) Inserta en la base de datos local del dispositivo los datos nuevos de valores de historia
					BitamStorage.insertHistoryFieldValues(data.payload.historyFieldValues || []);
					var usersGuests = data.payload.userGuests;
					if(usersGuests != null){
                    	bitamApp.dataApp.usersGuests= $.extend({},usersGuests,bitamApp.dataApp.usersGuests);
                        bitamApp.dataApp.users= $.extend({},usersGuests,bitamApp.dataApp.users);
						//ahora buscamos los ids negativos guardados en bitamApp.dataApp.users
						for(var keyUsr in bitamApp.dataApp.users){
							if(keyUsr<0){
								//checamos si este usuario creado ya se ha guardado y lo eliminamos de la colleccion
								for(var keyUsrGst in usersGuests){
									if(bitamApp.dataApp.users[keyUsr] && usersGuests[keyUsrGst]["nom_largo"] == bitamApp.dataApp.users[keyUsr]["nom_largo"]){
									//hemos encontrado un usuario que acaba de ser guardado en la base de datos, lo eliminamos de la coleccion local
										delete bitamApp.dataApp.users[keyUsr];
									}
								}
							}
						}
					}
					
					if(recordsInserted == null && data.payload.data == null){
						return;
					}
					
					//recorremos por seccion
					if(recordsInserted != null){
						for (var sectionName in recordsInserted){
							var dataRemove = [];
							for(var key in recordsInserted[sectionName]){
								//borramos los datos con ID's negativos que ya han sido guardados en el servidor
								//delete a.dataUser[sectionName][recordsInserted[sectionName][key]];
								//cambio a DataCollection
								bitamApp.dataUser[sectionName].deleteItem(recordsInserted[sectionName][key]);
								//agregamos en el array solo los valores negativos
								if(recordsInserted[sectionName][key]<0)
									dataRemove.push(recordsInserted[sectionName][key]);
							}
							if(dataRemove.length)
								BitamStorage.removeData(sectionName,dataRemove);
						}
					}
					
					//ya han sido eliminado los datos locales con ID's temporales que han sido guardados en el servidor, 
					//ahora solo hacemos el merge con los datos locales y guardados
					var newData = data.payload.data;
					for(var sectionName in newData){
						// //$.extend(a.dataUser[sectionName], newData[sectionName]);
						// //cambio a DataCollection
						bitamApp.dataUser[sectionName].add(newData[sectionName], false);
						// //actualizamos la interfaz pero solo si existe esta seccion en el DOM
						// if(document.getElementById(sectionName) && (!isset(options.actUI ||  options.actUI != false)))
							// a.section(sectionName, false);
					}
					
					//a.updateDataAppStorage();
					
					//ponemos en una colecction los newData
					var newDataCol = {};
					for(var sectionName in newData)
					{
						newDataCol[sectionName] = new DataCollection(newData[sectionName], a.application.getSection(sectionName).payload.fieldsData);
						bitamApp.dataUser[sectionName].trigger("sync", {'collection':newData[sectionName]});
					}
					if($.isFunction(options.callback))
					{
						options.callback(newDataCol, data.payload);
					}
				}
			});
		
		//debugger;
		
	},
	
	getDataSection:function(section, fn, options)
	{
		Debugger.message(new Array(10).join('*'));
		Debugger.register('bitamApp.getDataSection section.id=' + section.id + ', section.name=' + section.name);
		if(section.id == "configura")
		{//si los datos a pedir son de 'configura', ya estan cargados, solo le mandamos los datos que pertenecen a esta seccion.
			if($.isFunction(fn))
			{
				//2014-04-10@HADR(#32417): JLC; No agrega ARS
				//Traerlos del servidor para poder cargarlos desde la bmd
				var dataCollection = bitamApp.dataUser.configura.filter({CLA_APP:bitamApp.session("appId")});
				var promiseGetSelectDashboards = bitamApp.getJSONRequest({
					url:bitamApp.getURLServer() + '/action.php?action=getUserArtusDashboardAndArtusReports&applicationCodeName=' + bitamApp.dataUser.apps.filter({id_app: bitamApp.session("appId")})[bitamApp.session("appId")].codeName
				});
				$.when(promiseGetSelectDashboards).then(function(resA){
					var ref_configuraReports = "";
					var i=0;
					for(i=0; i<resA.payload.artusreports.length;i++){
						if(ref_configuraReports !== "") ref_configuraReports += ",";
						ref_configuraReports += resA.payload.artusreports[i].value;
					}
					var ref_configuraDashboards = "";
					for(i=0; i<resA.payload.dashboards.length;i++){
						if(ref_configuraDashboards !== "") ref_configuraDashboards += ",";
						ref_configuraDashboards += resA.payload.dashboards[i].value;
					}

					for(var key in dataCollection){
						if(dataCollection[key].CLA_CONFIGURA == "300"){ //Artus reports
							dataCollection[key].REF_CONFIGURA = ref_configuraReports;
						}else if(dataCollection[key].CLA_CONFIGURA == "200"){
							dataCollection[key].REF_CONFIGURA = ref_configuraDashboards;
						}
					}
					fn({"collection":dataCollection});
				});
			}
			return;
		}			
		var b = this;
		options = ($.isPlainObject(options)) ? options : {};
		var params = new Object;
		params.applicationCodeName = this.application.codeName;
		params.user = bitamApp.user.cuenta_correo;
		params.password = bitamApp.user.password;
		params.section = section.name;
		
		params.lastCreateDate = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[section.name]) ? bitamApp.dataCache.loadDate[section.name].createDate : '');
		params.lastModifiedDate = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[section.name]) ? bitamApp.dataCache.loadDate[section.name].lastModifiedDate : '');
				
		if(isset(section.extend))
		{
			params.section = section.extend;
		}
		
		if(section.url == undefined || section.url == null)
		{
			section.url = this.getURLServer() + '/getData.inc.php';
		}
		
		if(isset(section.filterBy))
		{
			var filterBy = $.extend({}, section.filterBy);
			for(var key in filterBy)
			{
				if(filterBy[key].substring(0, 9) == 'bitamApp.')
				{
					try
					{
						filterBy[key] = eval(filterBy[key]);
					} catch(err)
					{
						//return false;
					}
				}
			}
			params.filter = new Object();
			params.filter = filterBy;
		}
		
		//si estamos en modo offline no pedimos datos al server
		//if(!this.onLine())
		//si estamos en modo ofline o si ya tenemos los datos cargados
		//if(!this.onLine() || b.dataUser[section.name])
		//hasta que lo modifiquemos para que cada peticion traiga solo los datos actuales
		if(false)
		{
			//console.log('Datos locales para seccion ['+ section.name +']');
			if(!isset(b.dataUser[section.name]))
			{
				return;
			}
			
			if(isset(params.filter))
			{
				fn(new DataCollection(b.dataUser[section.name].filter(params.filter), section.payload.fieldsData));
			}
			else
			{
				fn(new DataCollection(b.dataUser[section.name].filter(params.filter), section.payload.fieldsData));
			}			
			return;
		}
		
		if(section.url != undefined)
		{
			//pedimos los datos al server
			//primero revismos si este formualrio necesita datos de otro formulario
			//para eso recorremos todos los fieldsForms para ver si alguno tiene la propiedad catalog.
			//var arrLogicalNameForm = new Array();
			var arrCatNameForm = new Array();
			var arrNameForm = new Array();
			for(var key in section.payload.fieldsForm)
			{
				var eachFieldForm = section.payload.fieldsForm[key];
				//tambien hay que programar para sacar los .catalog definidos manualmente
				if(isset(eachFieldForm.element.catalog_form))
				{
					arrCatNameForm.push(eachFieldForm.element.catalog_form);
					anCatalogSection = bitamApp.application.getSection(eachFieldForm.element.catalog_form);
					if(anCatalogSection&&anCatalogSection.formsTemplate&&$.inArray(anCatalogSection.formsTemplate, arrCatNameForm)==-1){
						arrCatNameForm.push(anCatalogSection.formsTemplate+"_TPL");
					}
				}
				
				if(isset(eachFieldForm.catalog))
				{
					var re = new RegExp(/(bitamApp.dataUser.[A-Za-z_0-9]*)/g);
					var m = re.exec(eachFieldForm.catalog);
					if($.isArray(m))
					{
						for(var i=0; i<m.length; i++)
						{
							var sSection = m[i].replace("bitamApp.dataUser.", "");
							if($.inArray(sSection, arrCatNameForm) == -1)
							{
								arrCatNameForm.push(sSection);
							}
						}
					}
				}					
			}
			//ahora revisamos si esta seccion tiene secciones detalles que deben ser precargadas
			for(var key in section.sections)
			{
				if($.inArray(section.sections[key].name, arrCatNameForm) == -1)
				{
					arrCatNameForm.push(section.sections[key].name);
				}
			}
			
			//revisamos si esta seccion tiene secciones maestras (mejor optimizacion cargar solo las secciones maestras que se ocupen en la vista)
			if(isset(section.payload.infoMasterSections))
			{
				for(var key in section.payload.infoMasterSections)
				{
					arrNameForm.push(section.payload.infoMasterSections[key].sectionName);
				}
			}
			//revisamos si la vista contiene un catalogo para cargar
			for(var key in section.payload.views)
			{
				for(var keyX in section.payload.views[key].fieldsList)
				{
					var eachFieldList = section.payload.views[key].fieldsList[keyX];
					var anSection = bitamApp.application.getSectionWithKey(eachFieldList.section_id);
					if(anSection)
					{
						for(var keyY in anSection.payload.fieldsForm)
						{
							var eachField = anSection.payload.fieldsForm[keyY];
							if(isset(eachField.element.catalog_form))
							{
								if($.inArray(eachField.element.catalog_form, arrCatNameForm) == -1)
								{
									arrCatNameForm.push(eachField.element.catalog_form);
								}
							}
						}
					}
				}
			}				
			
			//existen formularios que deben ser cargados tambien
			tmpFilter = $.extend({}, params.filter);
			//no enviamos el filtro, nos traemos todos los datos pero filtramos en el cliente
			delete params.filter;
			//pero mandamos los filtros que vengan de options
			if($.isPlainObject(options.filter) && !$.isEmptyObject(options.filter))
			{
				//este filtro solo se aplicara a la seccion principal
				params.filter = options.filter;
				tmpFilter = $.extend({}, options.filter);
				if($.isArray(arrCatNameForm) && arrCatNameForm.length>0)
				{
					params.filter = {};
					params.filter[section.id] = options.filter;
				}				
			}
			if(arrCatNameForm.length > 0){
				//hacemos el arreglo solo valores unicos
				var arrForms = $.merge([], arrCatNameForm);
				arrForms = $.merge(arrCatNameForm, arrNameForm);
				arrForms = $.array_unique(arrForms);
				
				params.lastCreatedDate = {}
				params.lastCreatedDate[params.section] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[section.name]) ? bitamApp.dataCache.loadDate[section.name].lastCreatedDate : '');
				params.lastModifiedDate = {}
				params.lastModifiedDate[params.section] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[section.name]) ? bitamApp.dataCache.loadDate[section.name].lastModifiedDate : '');
				params.section = new Array(params.section);		
				for(var key in arrForms)
				{
					if(arrForms[key].indexOf("FRM_")===0 || arrForms[key].indexOf("bitamApp")!==0){
						params.section.push(arrForms[key]);
						params.lastCreatedDate[arrForms[key]] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[arrForms[key]]) ? bitamApp.dataCache.loadDate[arrForms[key]].lastCreatedDate : '');
						params.lastModifiedDate[arrForms[key]] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[arrForms[key]]) ? bitamApp.dataCache.loadDate[arrForms[key]].lastModifiedDate : '');
					}
				}
				
				/*for(var key in arrCatNameForm)
				{
					anCatalogSection = bitamApp.application.getSection(arrCatNameForm[key]);
					if(anCatalogSection)
					{
						params.section.push(anCatalogSection.name);
						params.lastCreatedDate[anCatalogSection.name] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[anCatalogSection.name]) ? bitamApp.dataCache.loadDate[anCatalogSection.name].lastCreatedDate : '');
						params.lastModifiedDate[anCatalogSection.name] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[anCatalogSection.name]) ? bitamApp.dataCache.loadDate[anCatalogSection.name].lastModifiedDate : '');
					}
				}
				for(var key in arrNameForm)
				{
					params.section.push(arrNameForm[key]);
					params.lastCreatedDate[arrNameForm[key]] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[arrNameForm[key]]) ? bitamApp.dataCache.loadDate[arrNameForm[key]].lastCreatedDate : '');
					params.lastModifiedDate[arrNameForm[key]] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[arrNameForm[key]]) ? bitamApp.dataCache.loadDate[arrNameForm[key]].lastModifiedDate : '');
				}*/				
			}
			
			fnSuccess = function(data){	
				bitamApp.pageLoading(true);
				//console.log('Datos remotos para seccion ['+ section.name +']');
				res = new BuilderAppResponse(this);
				$.extend(res, data);
				
				if(data.error!=0)
				{
					alert(data.errorDescription)
					return;
				}
				
				if(!$.isArray(params.section))
				{
					var dataSections = {};
					dataSections[params.section] = res.getPayload().data;
					var loadDate = {};
					loadDate[params.section] = res.getPayload().loadDate;
				}
				else
				{
					var dataSections = res.getPayload().data;
					var loadDate = res.getPayload().loadDate;
				}					
				for(var key in dataSections)
				{
					var section = b.application.getSection(key);
					//2014-07-09@HADR(#MSKC5R) Al importar una App en otro repositorio y dentro de una App que existe se queda todas las formas
					//Se inicializa la seccion con la info de la metadata para evitar conservar secciones que fueron eliminadas en el servidor al importar la aplicacion
					// if(isset(b.dataUser[section.name]) && b.dataUser[section.name].length() > 0)
					// {
					// 	b.dataUser[section.name].add(dataSections[key]);
					// }
					// else
					// {
						b.dataUser[section.name] = new DataCollection(dataSections[key],section.payload.fieldsData);
					// }
					
					b.updateDataAppStorage();
					b.dataCache[section.name] = true;
					if(!$.isPlainObject(b.dataCache.loadDate))
						b.dataCache.loadDate = {};
					b.dataCache.loadDate[section.name] = loadDate[section.name];
				}   
				for(var key in data.payload.RegisterBlocked)
				{
					var regBlock = data.payload.RegisterBlocked[key];
					if(bitamApp.application.payload.sections[key].payload.processForms.length >0)
						bitamApp.application.payload.sections[key].payload.processForms[0].arrKeysBlocked= regBlock;
				}
//                                    data.payload.RegisterBlocked
//                                bitamApp.application.payload.sections
				if($.isFunction(fn))
				{
					var sNameSection = params.section;
					if($.isArray(params.section))
					{
						var sNameSection = params.section[0];
					}
					var aSection = b.application.getSection(sNameSection);
					
					//verificamos que todos los fields del filtro pertenezcan a este formulario, si no es asi, averiguamos a que formulario pertenece el field y
					//lo filtramos a ese mismo para sacar los id's con el que filtraremos el formulario original
					//tmpFilter = Worker_buildFilterSection(tmpFilter, aSection);
					for(var key in tmpFilter)
					{
						if(typeof tmpFilter[key] == "string" && tmpFilter[key].substring(0, 9) == 'bitamApp.')
						{
							try
							{
								tmpFilter[key] = eval(tmpFilter[key]);
							} catch(err)
							{
								//return false;
							}
						}
					}
					fn(new DataCollection(b.dataUser[aSection.name].filter(tmpFilter), aSection.payload.fieldsData));
				}
			}
			
			if(bitamApp.onLine() == false || bitamApp.isMobile.isApp())
			{
				if($.isFunction(fn))
				{
					var sNameSection = params.section;
					if($.isArray(params.section))
					{
						var sNameSection = params.section[0];
					}
					var aSection = bitamApp.application.getSection(sNameSection);
					
					//verificamos que todos los fields del filtro pertenezcan a este formulario, si no es asi, averiguamos a que formulario pertenece el field y
					//lo filtramos a ese mismo para sacar los id's con el que filtraremos el formulario original
					//tmpFilter = Worker_buildFilterSection(tmpFilter, aSection);
					for(var key in tmpFilter)
					{
						if(typeof tmpFilter[key] == "string" && tmpFilter[key].substring(0, 9) == 'bitamApp.')
						{
							try
							{
								tmpFilter[key] = eval(tmpFilter[key]);
							} catch(err)
							{
								//return false;
							}
						}
					}
                    tmpFilter.createdUserKey = bitamApp.user.cla_usuario;
                    if(aSection.id_section == -1)
                    {
                        delete tmpFilter.createdUserKey;
                    }                    

					fn(new DataCollection(((bitamApp.dataUser[aSection.name] == undefined) ? {} : bitamApp.dataUser[aSection.name].filter(tmpFilter)), aSection.payload.fieldsData));
				}
			}
			else
			{
				this.getJSONRequest({
					url:section.url, 
					data: params,
					success:fnSuccess
				});
			}
		}else
		{
			if($.isFunction(fn))
			{
				fn();
			}			
		}
	},
	
	/*
	getDataSubSection:function(subSections, fn, data, itemFilter)
	{
		var sections = this.application.getSections();
				
		if($.isEmptyObject(subSections))
		{
			fn(data);
		}
				
		for (var key in subSections)
		{
			var filterBy = $.extend({}, subSections[key].filterBy);
			delete subSections[key];
			
			for (keyFilter in filterBy)
			{
				filterBy[keyFilter] = itemFilter.collection[filterBy[keyFilter]];
				if(filterBy[keyFilter] == undefined)
					filterBy[keyFilter] = '';
			}
			
			
			a = this;
			var fn2 = function(data){
				a.getDataSubSection(subSections, fn, data, itemFilter)
			};
			
			//si estamos onfline no hacemos peticion al server por los datos filtrados		
			if(!this.onLine())
			{
				//var dataCol = new DataCollection(this.dataUser[sections[key].name], sections[key].payload.fieldsData);
				//cambio a DataCollection
				var dataCol = this.dataUser[sections[key].name];
				
				var newData = new Object();
				newData[sections[key].name] = dataCol.filter(filterBy);
				fn2(newData);
				return;
			}
			
			var params = new Object;
			params.section = sections[key].name;
			if(sections[key].url == undefined || sections[key].url == null)
			{
				sections[key].url = this.getURLServer() + '/getData.inc.php';
			}
			
			params.filter = filterBy;
				
			this.getJSONRequest({
				url:sections[key].url, 
				data:params,
				success:function(data)
				{
					$.mobile.pageLoading(true);
					var newData = new Object();
					var keySection = data.payload.section;
					newData[sections[keySection].name] = data.payload.data;
					if(a.dataUser[sections[keySection].name])
					{
						//$.extend(a.dataUser[sections[key].name], newData[sections[key].name]);
						//cambio a DataCollection
						a.dataUser[sections[keySection].name].add(newData[sections[keySection].name], false);
					}
					else
					{
						//a.dataUser[sections[key].name] = newData[sections[key].name];
						//cambio a DataCollection
						a.dataUser[sections[keySection].name] = new DataCollection(newData[sections[keySection].name], sections[keySection].payload.fieldsData);						
					}
					
					//tambien buscamos en los datos locales
					
					//var dataCol = new DataCollection(a.dataUser[sections[key].name], sections[key].payload.fieldsData);
					//$.extend(newData[sections[key].name], dataCol.filter(filterBy));
					
					//cambio a DataCollection
					var dataColFilter = a.dataUser[sections[keySection].name].filter(filterBy);
					$.extend(newData[sections[keySection].name], dataColFilter);
					newData[sections[keySection].name] = new DataCollection(newData[sections[keySection].name], sections[keySection].payload.fieldsData);
					debugger;					
					fn2(newData);
				}
			});
		}
	},*/
	
	getDataSections:function(sections, fn, options)
	{
		for(var key in sections) { section = sections[key]; delete sections[key]; break; }
		
		if($.isEmptyObject(sections))
		{
			bitamApp.getDataSection(section, fn, options);
		}
		else
		{
			bitamApp.getDataSection(section, function(){ bitamApp.getDataSections(sections, fn, options); }, options);
		}
	},
	
	getDataSubSection:function(subSections, fn, data, itemFilter)
	{
		Debugger.message(new Array(10).join('*'));
		Debugger.register('bitamApp.getDataSubSection');
		var sections = this.application.getSections();
		
		params = new Object();
		params.applicationCodeName = this.application.codeName;
		params.user = bitamApp.user.cuenta_correo;
		params.password = bitamApp.user.password;
		params.section = new Array();
		params.filter = new Object();
		
		//var arrLogicalNameForm = new Array();
		var arrNameForm = new Array();
		
		for(var key in subSections)
		{
			params.section.push(subSections[key].name);
			
			var filterBy = $.extend({}, subSections[key].filterBy);
			for (keyFilter in filterBy)
			{
				if(!$.isNumeric(filterBy[keyFilter])){
					filterBy[keyFilter] = itemFilter.collection[filterBy[keyFilter]];
					if(filterBy[keyFilter] == undefined)
						filterBy[keyFilter] = '';
				}
			}
			
			//filtros que incluye la propia vista en la seccion de detalle
			if(isset(subSections[key].viewName) && sections[key].payload.views[subSections[key].viewName].viewCriterias.length > 0)
			{
				var anFilter = {};
				for(var keyInView in sections[key].payload.views[subSections[key].viewName].viewCriterias)
				{
					var eachViewCriteria = sections[key].payload.views[subSections[key].viewName].viewCriterias[keyInView];
					//preparamos el tipo de filtro
					sFilter = "|" + eachViewCriteria.fieldForm.id + "|";
					switch(eachViewCriteria.condition)
					{
						case "1": sFilter = sFilter + "="; break;
						case "2": sFilter = sFilter + "<>"; break;
						case "3": sFilter = sFilter + "="; eachViewCriteria.value = "*"+eachViewCriteria.value+"*"; break;
						case "4": sFilter = sFilter + "<>"; eachViewCriteria.value = "*"+eachViewCriteria.value+"*"; break;
						case "5": sFilter = sFilter + "="; eachViewCriteria.value = eachViewCriteria.value+"*"; break;
						case "6": sFilter = sFilter + "="; eachViewCriteria.value = "*"+eachViewCriteria.value; break;
						case "7": sFilter = sFilter + "="; eachViewCriteria.value = ''; break;
						case "8": sFilter = sFilter + "<>"; eachViewCriteria.value = ''; break;
					};
					anFilter[sFilter] = eachViewCriteria.value;
				}
				//section.filterBy = anFilter;
				$.extend(true, filterBy, anFilter);
			}
			
			params.filter[subSections[key].name] = filterBy;
			
			var aSection = bitamApp.application.getSection(key)
			for(var keyA in aSection.payload.fieldsForm)
			{
				var eachFieldForm = aSection.payload.fieldsForm[keyA];
				//tambien hay que programar para sacar los .catalog definidos manualmente
				if(isset(eachFieldForm.element.catalog_form))
				{
					if($.inArray(eachFieldForm.element.catalog_form, arrNameForm) == -1)
						arrNameForm.push(eachFieldForm.element.catalog_form);
				}
			}
			//revisamos si la vista de las subsecciones no necesita datos de otras formas
			if(isset(aSection.payload.views))
			{
				for(var keyA in aSection.payload.views[subSections[key].viewName].fieldsList)
				{
					var eachFieldList = aSection.payload.views[subSections[key].viewName].fieldsList[keyA];
					var aViewSection = bitamApp.application.getSectionWithKey(eachFieldList.section_id)
					if(aSection.id_section != aViewSection.id_section)
					{
						if($.inArray(aViewSection.sectionName,arrNameForm) == -1)
							arrNameForm.push(aViewSection.sectionName);
					}				
				}
			}
		}
		//existen formularios que deben ser cargados tambien
		//tmpFilter = $.extend({}, params.filter);
		//no enviamos el filtro, nos traemos todos los datos pero filtramos en el cliente
		//delete params.filter;
		if(arrNameForm.length > 0)
		{
			params.lastCreatedDate = {}
			//params.lastCreatedDate[params.section] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[section.name]) ? bitamApp.dataCache.loadDate[section.name].lastCreatedDate : '');
			params.lastModifiedDate = {}
			//params.lastModifiedDate[params.section] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[section.name]) ? bitamApp.dataCache.loadDate[section.name].lastModifiedDate : '');
					
			for(var key in arrNameForm)
			{
				var anCatalogSection = bitamApp.application.getSection(arrNameForm[key])
				if(anCatalogSection && $.inArray(anCatalogSection.name, params.section) == -1)
				{
					params.section.push(anCatalogSection.name);
					params.lastCreatedDate[anCatalogSection.name] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[anCatalogSection.name]) ? bitamApp.dataCache.loadDate[anCatalogSection.name].lastCreatedDate : '');
					params.lastModifiedDate[anCatalogSection.name] = (isset(bitamApp.dataCache.loadDate) && isset(bitamApp.dataCache.loadDate[anCatalogSection.name]) ? bitamApp.dataCache.loadDate[anCatalogSection.name].lastModifiedDate : '');
				}
			}
		}
		
		if(bitamApp.onLine() == false || bitamApp.isMobile.isApp() == true)
		{
			var newData = new Object();
			for(var keySection in subSections)
			{
                if(keySection == "ACTIONITEMS")
                {
                    var value = -1;
                    for (var keyFilter in params.filter[keySection])
                    {
                        if(keyFilter.indexOf("FDT_") == 0)
                        {
                            params.filter[keySection]["row_key"] = params.filter[keySection][keyFilter]
                            delete params.filter[keySection][keyFilter];
                        }
                    }
					//cambiamos el params.filter[keySection].section_id en lugar del Id, la descripcion, por que es lo que se almacena en modo local
					if(params.filter[keySection].section_id && params.filter[keySection].section_id == subSections[keySection].pSecid)
						params.filter[keySection].section_id = subSections[keySection].pSectName
                    newData[keySection] = new DataCollection(bitamApp.dataUser[keySection].filter(params.filter[keySection]), sections[keySection].payload.fieldsData);
                }
                else
                {
                    newData[keySection] = new DataCollection(bitamApp.dataUser[keySection].filter(params.filter[keySection]), sections[keySection].payload.fieldsData);
                }
			}
            fn(newData);
		}else
		{
			this.getJSONRequest({
				url:this.getURLServer() + '/getData.inc.php',
				data:params,
				success:function(data)
				{
					bitamApp.pageLoading(true);
					
					if(data.error!=0)
					{
						alert(data.errorDescription)
						return;
					}
					
					var newData = new Object();
					
					if(!$.isArray(params.section))
					{
						var loadDate = {};
						loadDate[params.section] = data.payload.loadDate;
					}
					else
					{
						var loadDate = data.payload.loadDate;
					}					
					for(var key in loadDate)
					{
						a.dataCache[key] = true;
						if(!$.isPlainObject(a.dataCache.loadDate))
							a.dataCache.loadDate = {};
						a.dataCache.loadDate[key] = loadDate[key];
					}
					
					//for(var key in subSections)
					for(var key in data.payload.data)
					{
						var keySection = key;
						//newData[keySection] = data.payload.data[keySection];
						
						if(a.dataUser[keySection])
						{
							//a.dataUser[keySection].add(newData[keySection], false);
							a.dataUser[keySection].add(data.payload.data[keySection], false);
						}					
						else
						{
							//a.dataUser[keySection] = new DataCollection(newData[keySection], sections[keySection].payload.fieldsData);						
							a.dataUser[keySection] = new DataCollection(data.payload.data[keySection], sections[keySection].payload.fieldsData);						
						}
						
						if(isset(subSections[keySection]))
							newData[keySection] = new DataCollection(data.payload.data[keySection], sections[keySection].payload.fieldsData);
					}
					delete data;
					fn(newData);
				}
			});
		}
	},
	
	getDataSectionByID:function(section, id)
	{
		if(this.dataUser[section.name].getItem(id))
		{
			return this.dataUser[section.name].getItem(id);
		}
	},
	
	saveForm: function(section, data, fn, bIsMultiPart, formName, options, fnError){
		Debugger.message(new Array(10).join('*'));
		Debugger.register('bitamApp.saveForm section.id=' + section.id + ', section.name=' + section.name);
		var arrFormsTplCat = [];
		var arrFieldsForm = section.getFieldsForm();
		var objData = new DataDefinition(section.payload.fieldsData);
		if(!isset(options)){
			options = {};
		}
		//revisamos si el ID key es vacio, entonces le asignamos un ID temporal
		if(data[section.getFieldKey()] == '' || data[section.getFieldKey()] == undefined){
			data[section.getFieldKey()] = bitamApp.dataUser[section.name].newID();
		}
		//cuando es una aplicacion creada por el usuario se agrega al registro el usuario que la creo y su fecha
		//cuando se modifica se agrega el usuario que lo modifico y su fecha
		if(this.application.codeName != 'eBavel'){			
			if(data[section.getFieldKey()] < 0){
				data["createdUserKey"] = bitamApp.dataApp.user.cla_usuario;
				data["createdDate"] = new Date().toString('yyyy-MM-d HH:mm:ss');
				if(!data["createdUser_"+section.name] || $.isEmptyObject(data["createdUser_"+section.name])){
					data["createdUser_"+section.name]  = {};
					data["createdUser_"+section.name][bitamApp.dataApp.user.cla_usuario] = bitamApp.dataApp.user.cuenta_correo;
				}
				if(!data["modifiedUser_"+section.name] || $.isEmptyObject(data["modifiedUser_"+section.name])){
					data["modifiedUser_"+section.name]  = {};
					data["modifiedUser_"+section.name][bitamApp.dataApp.user.cla_usuario] = bitamApp.dataApp.user.cuenta_correo;
				}
			}			
			data["modifiedUserKey"] = bitamApp.dataApp.user.cla_usuario;
			data["modifiedDate"] = new Date().toString('yyyy-MM-dd HH:mm:ss');
			if(bitamApp.isMobile.isApp())
				data["syncDate"] = Date.parse(BitamStorage.getLastSyncDate()).addSeconds(1).toString('yyyy-MM-dd HH:mm:ss');
		}
		
		for(var key in arrFieldsForm){
			var eachField = arrFieldsForm[key];
			if(bitamApp.application.mdVersion >= 3.51) {
				if(bitamApp.user.securityCriteria[section.name] && typeof bitamApp.user.securityCriteria[section.name].actions_4 == 'function' && !bitamApp.user.securityCriteria[section.name].actions_4(data)) {
					return {error:true, field:eachField.label, msgError: $.t("It is not permitted to insert in the form")};
				}				
			} else {
				//revisamos si este campo tiene algun criterio de seguridad que evite que se inserte un registro
				if(bitamApp.user.securityCriteria[section.name] && bitamApp.user.securityCriteria[section.name][eachField.id] && bitamApp.user.securityCriteria[section.name][eachField.id].actions_4.length){
					//existe un campo, si es catalogo buscamos su descripcion, si no, comparamos directamente con el valor del criterio
					var sValue = data[eachField.id];
					if(eachField.isCatalog()){
						for(var keyCat in sValue) break;
						sValue = sValue[keyCat];
					}
					var bRes = false;
					if($.isArray(bitamApp.user.securityCriteria[section.name][eachField.id].actions_4)){
						//bRes = $.inArray(sValue.toLowerCase(), bitamApp.user.securityCriteria[section.name][eachField.id].actions_4) == -1;
						bitamApp.user.securityCriteria[section.name][eachField.id].actions_4.forEach(function(s){
							if(sValue.toLowerCase() == s.toLowerCase()){
								bRes = false;
								return;
							}
						})
					}else{
						bRes = sValue.toLowerCase() != bitamApp.user.securityCriteria[section.name][eachField.id].actions_4.toLowerCase();
					}
					if(bRes){
						//el criterio coincide, no permitimos insertar este registros
						return {error:true, field:eachField.label, msgError: $.t("It is not permitted to insert in the form the ") + bitamApp.user.securityCriteria[section.name][eachField.id].actions_4.join(", ")+" value from the "+eachField.label+" field."};
					}
				}
			}
			//revisamos si un field numeric tiene formato y lo cambiamos a numero sin formato.
			if(eachField.tagname == "widget_field_numeric" && isset(eachField.element.format)){
				data[eachField.id] = formatString(data[eachField.id], "", 'numeric');
			}
			//revisamos si algun field es widget_helpform_modal y cambiamos su string json a objeto
			if(eachField.tagname == "widget_helpform_modal"){
				var anObject = {};
				for(var keyA in data[eachField.id]){
					try{
						var anTmpObject = JSON.parse(data[eachField.id][keyA]);
						anObject[anTmpObject.id] = anTmpObject;
					} catch(err){
					}					
				}
				data[eachField.id] = anObject;
			}
			//revisamos si este catalogo hace uso de un formulario que tenga activo los actionsTemplate
			//comparamos el valor con el anterior, si es el mismo no creamos el action item
			var cpField = null;
			if(data[section.getFieldKey()]>0 && bitamApp.dataUser[section.name].collection[data[section.getFieldKey()]])
				cpField = bitamApp.dataUser[section.name].collection[data[section.getFieldKey()]][eachField.id];		
			if($.isPlainObject(cpField)&&!$.isEmptyObject(cpField)){
				for(t in cpField) break;
				cpField = t;
			}
			
			var intKeyCat = data[eachField.id];
			if($.isPlainObject(intKeyCat)){
				for(var intKeyCat in data[eachField.id]) break;
			}
			if(cpField != intKeyCat && eachField.element.catalog_form_p && isset(bitamApp.application.payload.sections[eachField.element.catalog_form_p].formsTemplate)){
				//obtenemos la informacion del formtemplate
				var sFormName = bitamApp.application.payload.sections[eachField.element.catalog_form_p].formsTemplate;
				var datTpl = bitamApp.dataUser[sFormName+"_TPL"].filter("TPL_"+section.payload.fieldsForm[key].element.catalog_form_p+"="+intKeyCat);
				if(!$.isEmptyObject(datTpl)){
					for(kdTpl in datTpl){
						arrFormsTplCat.push({"form":sFormName,"data":datTpl[kdTpl]});
					}
				}
			}
			if(isset(eachField.element.catalog_form) && !$.boolean(eachField.element.catalog_usedescriptionkey)){
				var anCatalogSection = bitamApp.application.getSection(eachField.element.catalog_form)
				if(anCatalogSection && isset(section.payload.infoMasterSections)){
					for(var keyInfoMasterSections in section.payload.infoMasterSections){
						if(anCatalogSection != null && section.payload.infoMasterSections[keyInfoMasterSections].section_id == anCatalogSection.id_section){
							//21/02/2013@aramirez
							//ahora el data[eachField.id] es un objeto, comprobamos si es objeto
							if((data[section.payload.infoMasterSections[keyInfoMasterSections].id]) != undefined && section.payload.infoMasterSections[keyInfoMasterSections].id.substr(0,4) == 'FDT_'){
								if($.isPlainObject(data[eachField.id])&&!$.isEmptyObject(data[eachField.id]))
									data[section.payload.infoMasterSections[keyInfoMasterSections].id] = parseInt(Object.keys(data[eachField.id])[0]);
							}
						}
					}
				}
			}
		}
		
		//En el caso de los Templates, se reemplazan las etiquetas por los IDs antes de grabar el cuerpo del Email
		if (this.application.codeName == 'eBavel' && section.name == 'templates')
		{
			sEMailBody = data['emailBody'];
			sEMailBodyDB = '';
			iStart = 0;
			iStartDelPos = sEMailBody.indexOf('{', iStart);
			while (iStart >= 0 && iStartDelPos >= 0)
			{
				sToken = '';
				sTokenDB = '';
				iEndDelPos = sEMailBody.indexOf('}', iStartDelPos);
				if (iEndDelPos >= 0)
				{
					sToken = $.trim(sEMailBody.substring(iStartDelPos+1, iEndDelPos));
					sTokenDB = '{' + sToken + '}';
					if (sToken != '' && sToken.indexOf('.') > 0)
					{
						arrToken = sToken.split('.');
						//aSectionColl = bitamApp.dataUser.sections.filter({'sectionName':arrToken[0]});
						aSectionColl = bitamApp.dataUser.sections.filter({'sectionName':arrToken[0], 'appId':bitamApp.session("appId")});
						if (!$.isEmptyObject(aSectionColl))
						{
							for (var key in aSectionColl)
							{
								aSection = aSectionColl[key];
								break;
							}
							aFormFieldColl = bitamApp.dataUser.fieldsforms.filter({'label':arrToken[1], 'section_id':aSection.id_section});
						}
						if (!$.isEmptyObject(aFormFieldColl))
						{
							for (var key in aFormFieldColl)
							{
								aFormField = aFormFieldColl[key];
								break;
							}
							sTokenDB = '{' + aFormField.id_fieldform + '}';
						}
					}
				}
				else
				{
					sTokenDB = sEMailBody.substring(iStartDelPos);
				}
				
				sEMailBodyDB += sEMailBody.substring(iStart, iStartDelPos) + sTokenDB;
				iStart = (iEndDelPos >= 0)?iEndDelPos+1:-1;
				iStartDelPos = sEMailBody.indexOf('{', iStart);
			}
			if (iStart >= 0)
			{
				sEMailBodyDB += sEMailBody.substring(iStart);
			}
			data['emailBody'] = sEMailBodyDB;
			//alert(sEMailBodyDB);
		}
		
		var keyFields = objData.getKeyFields();
		var aFilterUniqueKey = new Object();
		for(var x = 0; x<keyFields.length; x++){
			
			aFilterUniqueKey[keyFields[x].name] = data[keyFields[x].name];
		}
		if(keyFields.length > 0){
			//verificacion si el registro ya esta insertado, esta verificacion hay que hacerla en modo offline en el metodo bitamApp.saveModel.
		}

		var res = objData.validateData(data);		
		if(res.error == true) {
			return res;
		}
				
		//si la seccion tiene un evento para save, ejeuctamos la funcion		
		if(isset(section.triggers) && isset(section.triggers.save) && $.isFunction(section.triggers.save)){
			var res_func = section.triggers.save(this, data)
			if(res_func == false){
				return {error:false};
			}
		}
		
		//guardamos la informacion generada localmente
		var tmpObject = new Object();
		for(var keyD in data)
		{
		    if(data[keyD] == undefined)
			data[keyD] = null;
		}
		tmpObject[data[section.getFieldKey()]] = data;
		bitamApp.dataUser[section.name].add($.extend(true, {}, tmpObject), true);
			
		if(arrFormsTplCat.length>0){
			$.each(arrFormsTplCat, function eachArrFormsTPL(i,row){
				//a partir de la informacion del formtemplate damos de alta en registros en una forma determinada que sera guardado en el server en la sincronizacion de datos.
				var nId = bitamApp.dataUser[row.form].newID();
				var fillTmpData = {};
				dataFormTPL = {};
				dataFormTPL[nId] = $.extend(true,{},row.data);
				fillTmpData[section.name] = tmpObject[data[section.payload.fieldsData[0].name]]
				//$.each(dataFormTPL[nId], function(i,v){
				var aSection = bitamApp.application.getSection(row.form);
				$.each(aSection.payload.fieldsForm, function eachFields(i,aField){
					if(aField.tagname == 'widget_field_date'){
						if($.isNumeric(dataFormTPL[nId][aField.id]))
							dataFormTPL[nId][aField.id] = Date.today().add(parseInt(dataFormTPL[nId][aField.id])).days().toString("yyyy-MM-dd");
						else
							dataFormTPL[nId][aField.id] = Date.today().toString("yyyy-MM-dd");
						return true;
					}
					if(isset(dataFormTPL[nId][aField.id])&&!$.isNumeric(dataFormTPL[nId][aField.id]))
						dataFormTPL[nId][aField.id] = replaceLabelString(dataFormTPL[nId][aField.id], fillTmpData, true);
				})
				//dataFormTPL[nId]["modifiedUserKey"] = bitamApp.user.cla_usuario;
				//dataFormTPL[nId]["createdUserKey"] = bitamApp.user.cla_usuario;
				dataFormTPL[nId]["createdUserKey"] = fillTmpData[section.name].createdUserKey;
				dataFormTPL[nId]["id_"+row.form] = nId;
				bitamApp.dataUser[row.form].add(dataFormTPL);
			})
		}
			
		//si estamos Online siempre guardamos al servidor
		//si estamos Online pero es un App, no guardamos en el server
		if(this.onLine() && bitamApp.isMobile.isApp() == false && options.isOrphanForm != true){
			this.updateDataAppStorage();
			this.syncData({'callback':fn, "actUI":options.actUI});
		}else{
			//si se esta guardando un action item en modo offline
			if(section.id_section == -1){
				for(var keyData in tmpObject){
					//revisamos si el usuario responsable de este action item es subordinado del usuario logeado
					for(var keyUserResponsible in tmpObject[keyData]["user"]) break;
					if(bitamApp.user.subordinados && $.isPlainObject(bitamApp.user.subordinados[keyUserResponsible])){
						tmpObject[keyData].isSubordinado = "1"
					}
					//revisamos si este action item pertenece a un registro del usuario logeado
					var aSectionOwner = bitamApp.application.getSectionWithKey(tmpObject[keyData]["section_id"]);
					if(aSectionOwner && $.isNumeric(tmpObject[keyData]["row_key"])){
						if(bitamApp.user.cla_usuario == bitamApp.dataUser[aSectionOwner.name].collection[tmpObject[keyData]["row_key"]].createdUserKey){
							tmpObject[keyData].isAttendance = 1;
						}
					}
				}
			}
			//this.updateDataAppStorage();
			
			var newData = {};
			newData[section.name] = new DataCollection(tmpObject, section.payload.fieldsData)
			BitamStorage.insertData(section.name, newData[section.name].collection).then(function(res) {
				//buscamos en los campos del detalle para ver cual tiene propagateParent
				for(var key in arrFieldsForm) {
					var aField = arrFieldsForm[key];
					if(aField.propagateParent && aField.propagateParent.length > 0) {
						for(var rcKey in newData[section.name].collection) {
							for(var pkey in aField.propagateParent) {
								var aSection = bitamApp.application.getSection(aField.propagateParent[pkey].sectionName);
								var eachParentField = aSection.getFieldFormWithId(aField.propagateParent[pkey].id);
								var masterSectionName = aSection.name;var masterFDT = section.getParentWithName(masterSectionName).id;
								var detailFDTValue = newData[section.name].collection[rcKey][masterFDT];
								var proxyObj = {
									'aField': aField,
									'eachParentField': eachParentField,
									'masterSectionName': masterSectionName,
									'masterFDT': masterFDT,
									'detailFDTValue': detailFDTValue,
									'newData': newData
								};
								BitamStorage.getData({
									'table':masterSectionName,
									'filter':aSection.getFieldKey() + '=' + detailFDTValue
								}).then($.proxy(function(res) {
									var self = this;
									var aField = self.aField;
									var eachParentField = self.eachParentField;
									var masterFDT = self.masterFDT;
									var detailFDTValue = self.detailFDTValue;
									var newData = self.newData;
									var masterSectionName = self.masterSectionName;
									//sacar el registro del maestro con getdata
									var oldMasterDataCollection = res.collection;
									for(var cmKey in oldMasterDataCollection) {
										var oldMasterData = oldMasterDataCollection[cmKey];
									}
									//realizar la operacion
									var agregationQuery = bitamApp.aggregationQuery(detailFDTValue, aField, eachParentField, masterFDT);
									bitamApp.executeAggregate(agregationQuery, aField, eachParentField).then($.proxy(function(res) {
										var newMasterData = bitamApp.updateWithAggregation(res, oldMasterData, aField, eachParentField);
										for(var cmKey in oldMasterDataCollection) {
											oldMasterDataCollection[cmKey] = newMasterData.data;
										}
										var newMasterDataCollection = oldMasterDataCollection;
										//y volver a hacer el insert data
										BitamStorage.insertData(masterSectionName, newMasterDataCollection);
									}));
								}, proxyObj));
							}
						}
					}
				}
			});
			fn(newData);
		}

		return {error:false};
	},
	
	updateWithAggregation: function (aggregationData, oldMasterData, detailField, eachParentField) {
		var objData = {};
		for(mKey in oldMasterData) {
			if(mKey == eachParentField.id) {
				oldMasterData[mKey] = aggregationData[eachParentField.id];
				objData.aggregateValue = aggregationData[eachParentField.id];
			}
		}
		objData.data = oldMasterData;
		return objData;
	},
	
	aggregationQuery: function (detailFDTValue, detailField, eachParentField, masterFDT) {
		var masterDefaultValue = eachParentField.element.defaultValue;
		var masterFieldID = eachParentField.id;
		var select = "SELECT ";
		var matches = masterDefaultValue.match(/(sum|count|avg|min|max|distinct)\((.*)\)/g);
		if(matches.length > 0) {
			select += 'SUM';
			select += "("+detailField.id+") AS "+detailField.id;
		}
		var tableName = detailField.sectionName;
		var from = " FROM "+tableName;
		var where = "";
		where += " WHERE "+masterFDT+"="+detailFDTValue;
		var aQuery = select + from + where;
		return aQuery;
	},
	
	executeAggregate: function (agregationQuery, detailField, eachParentField) {
		var dfd = $.Deferred();
		var proxyObj = {
			'dfd': dfd
		};
		BitamStorage.webDB.execute(agregationQuery).then($.proxy(function(tx, res) {
			var sumdata = res.rows.item(0);
			var newData = {};
			if(res.rows.length > 0) {
				newData[eachParentField.id] = sumdata[detailField.id];
			}
			this.dfd.resolve(newData);
		}, proxyObj));
		return dfd.promise();
	},
	
	/*
	saveForm:function(section, data, fn, bIsMultiPart, formName, options, fnError)
	{
		Debugger.message(new Array(10).join('*'));
		Debugger.register('bitamApp.saveForm section.id=' + section.id + ', section.name=' + section.name);
		//var arrActionTemplateForCatalogo, array donde guardamos los action template que den de alta los catalogos en la forma
		//var arrActTplCat = [];
		//var arrFormsTemplateForCatalogo, array donde guardamos los forms template que den de alta los catalogos en la forma
		var arrFormsTplCat = [];
		if(!isset(options))
		{
			options = {};
		}
		var objData = new DataDefinition(section.payload.fieldsData);
		
		//revisamos si el ID key es vacio, entonces le asignamos un ID temporal
		if(data[section.payload.fieldsData[0].name] == '' || data[section.payload.fieldsData[0].name] == undefined)
		{
			var dataCol = this.dataUser[section.name];		
			data[section.payload.fieldsData[0].name] = dataCol.newID();
		}
		
		for(var key in section.payload.fieldsForm){
			//revisamos si este campo tiene algun criterio de seguridad que evite que se inserte un registro
			if(bitamApp.user.securityCriteria[section.name] && bitamApp.user.securityCriteria[section.name][section.payload.fieldsForm[key].id] && bitamApp.user.securityCriteria[section.name][section.payload.fieldsForm[key].id].actions_4.length){
				//existe un campo, si es catalogo buscamos el Id, si no, comparamos directamente con el valor del criterio
				var res = bitamApp.user.securityCriteria[section.name][section.payload.fieldsForm[key].id].actions_4;
				if(section.payload.fieldsForm[key].element.catalog_field){
					var aFilter = {};
					aFilter[section.payload.fieldsForm[key].element.catalog_field_p] = bitamApp.user.securityCriteria[section.name][section.payload.fieldsForm[key].id].actions_4;
					var res = bitamApp.dataUser[section.payload.fieldsForm[key].element.catalog_form_p].filter(["id_"+section.payload.fieldsForm[key].element.catalog_form_p],aFilter)
				}
				if($.inArray(data[section.payload.fieldsForm[key].id], res) != -1){
					//el criterio coincide, no permitimos insertar este registros
					return {error:true, field:section.payload.fieldsForm[key].label, msgError:"It is not permitted to insert in the form the "+bitamApp.user.securityCriteria[section.name][section.payload.fieldsForm[key].id].actions_4.join(", ")+" value from the "+section.payload.fieldsForm[key].label+" field."};
				}
			}			
			//revisamos si un field numeric tiene formato y lo cambiamos a numero sin formato.
			if(section.payload.fieldsForm[key].tagname == "widget_field_numeric" && isset(section.payload.fieldsForm[key].element.format)){
				data[section.payload.fieldsForm[key].id] = formatString(data[section.payload.fieldsForm[key].id], "", 'numeric');
			}
			//revisamos si algun field es widget_helpform_modal y cambiamos su string json a objeto
			if(section.payload.fieldsForm[key].tagname == "widget_helpform_modal"){
				var anObject = {};
				for(var keyA in data[section.payload.fieldsForm[key].id]){
					try{
						var anTmpObject = JSON.parse(data[section.payload.fieldsForm[key].id][keyA]);
						anObject[anTmpObject.id] = anTmpObject;
					} catch(err){
					}					
				}
				data[section.payload.fieldsForm[key].id] = anObject;
			}
			//revisamos si este catalogo hace uso de un formulario que tenga activo los actionsTemplate
			//comparamos el valor con el anterior, si es el mismo no creamos el action item
			var cpField = null;
			if(data[section.payload.fieldsData[0].name]>0 && bitamApp.dataUser[section.name].collection[data[section.payload.fieldsData[0].name]])
				cpField = bitamApp.dataUser[section.name].collection[data[section.payload.fieldsData[0].name]][section.payload.fieldsForm[key].id];
			if($.isPlainObject(cpField)&&!$.isEmptyObject(cpField)){
				for(t in cpField) break;
				cpField = t;
			}
//			if(cpField != data[section.payload.fieldsForm[key].id] && section.payload.fieldsForm[key].element.catalog_form_p && bitamApp.application.payload.sections[section.payload.fieldsForm[key].element.catalog_form_p].actionsTemplate){
//				//obtenemos la informacion del actionTemplate
//				var datActTmp = bitamApp.dataUser.ACTIONSTEMPLATE.filter({"section_id":bitamApp.application.payload.sections[section.payload.fieldsForm[key].element.catalog_form_p].id_section, "row_key":data[section.payload.fieldsForm[key].id]})
//				if(!$.isEmptyObject(datActTmp)){
//					for(var kAtmp in datActTmp){
//						if(data[section.payload.fieldsData[0].name]>0){
//							datActTmp[kAtmp].responsible = responsible = bitamApp.dataUser[section.name].collection[data[section.payload.fieldsData[0].name]].createdUserKey;
//						}else{
//							datActTmp[kAtmp].responsible = bitamApp.dataApp.user.cla_usuario;
//						}
//						arrActTplCat.push(datActTmp[kAtmp]);
//					}
//				}
//			}
			var intKeyCat = data[section.payload.fieldsForm[key].id];
			if($.isPlainObject(intKeyCat)){
				for(var intKeyCat in data[section.payload.fieldsForm[key].id]) break;
			}
			if(cpField != intKeyCat && section.payload.fieldsForm[key].element.catalog_form_p && isset(bitamApp.application.payload.sections[section.payload.fieldsForm[key].element.catalog_form_p].formsTemplate)){
				//obtenemos la informacion del formtemplate
				var sFormName = bitamApp.application.payload.sections[section.payload.fieldsForm[key].element.catalog_form_p].formsTemplate;
				// var sDescr = data[section.payload.fieldsForm[key].id];
				// if($.isPlainObject(sDescr)){
					// for(var kid in sDescr) break;
					// sDescr = kid;
				// }
				var datTpl = bitamApp.dataUser[sFormName+"_TPL"].filter("TPL_"+section.payload.fieldsForm[key].element.catalog_form_p+"="+intKeyCat);
				if(!$.isEmptyObject(datTpl)){
					for(kdTpl in datTpl){
						arrFormsTplCat.push({"form":sFormName,"data":datTpl[kdTpl]});
					}
				}
			}
			if(isset(section.payload.fieldsForm[key].element.catalog_form)){
				var anCatalogSection = bitamApp.application.getSection(section.payload.fieldsForm[key].element.catalog_form)
				if(isset(section.payload.infoMasterSections)){
					for(var keyInfoMasterSections in section.payload.infoMasterSections){
						if(anCatalogSection != null && section.payload.infoMasterSections[keyInfoMasterSections].section_id == anCatalogSection.id_section){
							if((data[section.payload.infoMasterSections[keyInfoMasterSections].id]) != undefined && section.payload.infoMasterSections[keyInfoMasterSections].id.substr(0,4) != 'FDT_'){
								data[section.payload.infoMasterSections[keyInfoMasterSections].id] = data[section.payload.fieldsForm[key].id];
							}
						}
					}
				}
			}
		}
		
		//En el caso de los Templates, se reemplazan las etiquetas por los IDs antes de grabar el cuerpo del Email
		if (this.application.codeName == 'eBavel' && section.name == 'templates')
		{
			sEMailBody = data['emailBody'];
			sEMailBodyDB = '';
			iStart = 0;
			iStartDelPos = sEMailBody.indexOf('{', iStart);
			while (iStart >= 0 && iStartDelPos >= 0)
			{
				sToken = '';
				sTokenDB = '';
				iEndDelPos = sEMailBody.indexOf('}', iStartDelPos);
				if (iEndDelPos >= 0)
				{
					sToken = $.trim(sEMailBody.substring(iStartDelPos+1, iEndDelPos));
					sTokenDB = '{' + sToken + '}';
					if (sToken != '' && sToken.indexOf('.') > 0)
					{
						arrToken = sToken.split('.');
						//aSectionColl = bitamApp.dataUser.sections.filter({'sectionName':arrToken[0]});
						aSectionColl = bitamApp.dataUser.sections.filter({'sectionName':arrToken[0], 'appId':bitamApp.session("appId")});
						if (!$.isEmptyObject(aSectionColl))
						{
							for (var key in aSectionColl)
							{
								aSection = aSectionColl[key];
								break;
							}
							aFormFieldColl = bitamApp.dataUser.fieldsforms.filter({'label':arrToken[1], 'section_id':aSection.id_section});
						}
						if (!$.isEmptyObject(aFormFieldColl))
						{
							for (var key in aFormFieldColl)
							{
								aFormField = aFormFieldColl[key];
								break;
							}
							sTokenDB = '{' + aFormField.id_fieldform + '}';
						}
					}
				}
				else
				{
					sTokenDB = sEMailBody.substring(iStartDelPos);
				}
				
				sEMailBodyDB += sEMailBody.substring(iStart, iStartDelPos) + sTokenDB;
				iStart = (iEndDelPos >= 0)?iEndDelPos+1:-1;
				iStartDelPos = sEMailBody.indexOf('{', iStart);
			}
			if (iStart >= 0)
			{
				sEMailBodyDB += sEMailBody.substring(iStart);
			}
			data['emailBody'] = sEMailBodyDB;
			//alert(sEMailBodyDB);
		}
		
		//cuando es una aplicacion creada por el usuario se agrega al registro el usuario que la creo y su fecha
		//cuando se modifica se agrega el usuario que lo modifico y su fecha
		if(this.application.codeName != 'eBavel')
		{			
			if(data[section.payload.fieldsData[0].name] < 0)
			{
				data["createdUserKey"] = bitamApp.dataApp.user.cla_usuario;
				data["createdDate"] = new Date().toString('yyyy-MM-d HH:mm:ss');
				if(!data["createdUser_"+section.name]){
					data["createdUser_"+section.name]  = bitamApp.dataApp.user.cla_usuario;
				}
				if(!data["modifiedUser_"+section.name]){
					data["modifiedUser_"+section.name]  = bitamApp.dataApp.user.cla_usuario;
				}
			}			
			data["modifiedUserKey"] = bitamApp.dataApp.user.cla_usuario;
			data["modifiedDate"] = new Date().toString('yyyy-MM-d HH:mm:ss');
		}
		
		// for(var key in section.payload.fieldsForm){
			// if(isset(section.payload.fieldsForm[key].element.catalog_form)){
				// var anCatalogSection = bitamApp.application.getSectionWithName(section.payload.fieldsForm[key].element.catalog_form)
				// if(isset(section.payload.infoMasterSections)){
					// for(var keyInfoMasterSections in section.payload.infoMasterSections){
						// if(anCatalogSection != null && section.payload.infoMasterSections[keyInfoMasterSections].section_id == anCatalogSection.id_section){
							// if((data[section.payload.infoMasterSections[keyInfoMasterSections].id]) != undefined && section.payload.infoMasterSections[keyInfoMasterSections].id.substr(0,4) != 'FDT_'){
								// data[section.payload.infoMasterSections[keyInfoMasterSections].id] = data[section.payload.fieldsForm[key].id];
							// }
						// }
					// }
				// }
			// }
		// }
		
		//
		var keyFields = objData.getKeyFields();
		var aFilterUniqueKey = new Object();
		for(var x = 0; x<keyFields.length; x++){
			
			aFilterUniqueKey[keyFields[x].name] = data[keyFields[x].name];
		}
		if(keyFields.length > 0){
			var resFilterKeyField = bitamApp.dataUser[section.name].filter([objData.definition[0].name], aFilterUniqueKey);
			if(resFilterKeyField.length > 0 && !(resFilterKeyField.length == 1 && resFilterKeyField[0] == data[objData.definition[0].name]))
				return {error:true, msgError:"Duplicate entry"};
		}
				
		var res = objData.validateData(data);
		
		if(res.error == true){
			return res;
		}
		
		if(isset(section.triggers) && isset(section.triggers.save) && $.isFunction(section.triggers.save)){
			var res_func = section.triggers.save(this, data)
			if(res_func == false){
				return {error:false};
			}
		}
		
		if(bIsMultiPart == true){
			$(formName + " #" +section.payload.fieldsData[0].name).val(data[section.payload.fieldsData[0].name]);
			$(formName).submit();
			return res;
		}
		
		//guardamos la informacion generada localmente
		var tmpObject = new Object();
		tmpObject[data[section.payload.fieldsData[0].name]] = data;
		this.dataUser[section.name].add($.extend(true, {}, tmpObject), true);
			
		if(arrFormsTplCat.length>0 || bitamApp.isMobile.isApp()){
			//al guardar la nueva informacion en modo offline necesito que los catalogos se guarden como objeto en lugar de solo el id
			//buscamos los campos que tengan posibles catalogos
			for(var key in section.payload.fieldsForm){
				if(section.payload.fieldsForm[key].element.catalog_form){
					//este campo tiene un catalogo, recorremos todo tmpObject para remplazar el valor por un objeto de tipo {1:'Descripcion'}
					for(var keyData in tmpObject){
						var catValue = tmpObject[keyData][section.payload.fieldsForm[key].id];
						if($.isNumeric(catValue)){
							//if(section.payload.fieldsForm[key].element.catalog_form.substring(0,22) == 'bitamApp.dataApp.users'){
							if(section.payload.fieldsForm[key].element.catalog_form.substring(0,22) == 'bitamApp.dataApp.users'||
							section.payload.fieldsForm[key].element.catalog_form.substring(0,26) == 'bitamApp.user.subordinados'
							){
								var sCatDescription = bitamApp.dataApp.users[catValue][section.payload.fieldsForm[key].element.catalog_field]
							}else{
								//17/01/2013@aramirez
								//Nos traemos siempre el valor del campo que tiene el formulario de donde se lanzo la creacion del template
								if(false && section.payload.fieldsForm[key].element.catalog_usedescriptionkey == "true"){
									var aCatKeySection = bitamApp.application.getSection(section.payload.fieldsForm[key].element.catalog_form_p);
									for(var ckd in aCatKeySection.payload.fieldsForm){
										if(aCatKeySection.payload.fieldsForm[ckd].id == section.payload.fieldsForm[key].element.catalog_field_p){
											//var sCatDescription = bitamApp.dataUser[aCatKeySection.payload.fieldsForm[ckd].element.catalog_form_p].collection[catValue][aCatKeySection.payload.fieldsForm[ckd].element.catalog_field_p];
											var sCatDescription = BitamInterface.getFieldFormData(aCatKeySection.payload.fieldsForm[ckd].id, {description:true})
											break;
										}
									}
								}else{
									//var sCatDescription = bitamApp.dataUser[section.payload.fieldsForm[key].element.catalog_form_p].collection[catValue][section.payload.fieldsForm[key].element.catalog_field_p];
									var sCatDescription = BitamInterface.getFieldFormData(section.payload.fieldsForm[key].id, {description:true})
								}
							}
						}else{
							//if(section.payload.fieldsForm[key].element.catalog_form.substring(0,22) == 'bitamApp.dataApp.users'){
							if(section.payload.fieldsForm[key].element.catalog_form.substring(0,9) == 'bitamApp.'){
								//var sCatDescription = bitamApp.dataApp.users[catValue][section.payload.fieldsForm[key].element.catalog_field]
								var sCatDescription = catValue;
								//el id temporal tiene que ser sacado de bitamApp.dataApp.users
								catValue = -1;
								for(var keyClaUsr in bitamApp.dataApp.users){
									if(parseInt(keyClaUsr)<=catValue){
										catValue = parseInt(keyClaUsr);
										catValue = --catValue;
									}
									if(sCatDescription == bitamApp.dataApp.users[keyClaUsr]["nom_largo"]){
										//el usuario ya se encuentra agregado
										catValue = keyClaUsr;
										break;
									}
								}
								if(catValue != keyClaUsr){
									//agregamos temporalmente el usuario a bitamApp.dataApp.users, cuando se sincronicen los datos hay que quitarlo
									bitamApp.dataApp.users[catValue] = {id:catValue, nom_largo:sCatDescription};								 
								}
							}else{
								var sCatDescription = catValue;
                                catValue = bitamApp.dataUser[section.payload.fieldsForm[key].element.catalog_form_p].newID();
							}
							
						}
						tmpObject[keyData][section.payload.fieldsForm[key].id] = {};
						tmpObject[keyData][section.payload.fieldsForm[key].id][catValue] = sCatDescription;
					}
				}
				if(typeof section.payload.fieldsForm[key].catalog == "string" && section.payload.fieldsForm[key].catalog.substring(0, 12) == "dataCatalog("){
					for(var keyData in tmpObject){
						try {
							var dataUserCatalog = eval(replaceValueString(section.payload.fieldsForm[key].catalog,tmpObject[keyData]));
						} catch(e){
							var dataUserCatalog = [];
						}
						var catValue = tmpObject[keyData][section.payload.fieldsForm[key].id];
						if($.isNumeric(catValue)){
							for(var keyDataCat in dataUserCatalog){
								if(dataUserCatalog[keyDataCat].value == catValue){
									var sCatDescription = dataUserCatalog[keyDataCat].label
									break;
								}
							}
							tmpObject[keyData][section.payload.fieldsForm[key].id] = {};
							tmpObject[keyData][section.payload.fieldsForm[key].id][catValue] = sCatDescription;
						}
					}
				}
			}
			// $.each(arrActTplCat, function(i, datActTmp){
				// //a partir de la informacion del actiontemplate damos de alta un action item que sera guardado en el server en la sincronizacion de datos.
				// var nId = bitamApp.dataUser.ACTIONITEMS.newID();
				// var fillTmpData = {};
				// fillTmpData[section.name] = tmpObject[data[section.payload.fieldsData[0].name]]
				// dataAction = {};
				// dataAction[nId] = {};
				// dataAction[nId]["id_actionitem"] = nId;
				// dataAction[nId]["createdUser"] = bitamApp.user.cla_usuario;
				// dataAction[nId]["user"] = datActTmp["responsible"];
				// dataAction[nId]["status"] = 0;
				// dataAction[nId]["deadline"] = Date.today().add(parseInt(datActTmp["days"])).days().toString("yyyy-MM-dd");
				// dataAction[nId]["row_key"] = data[section.payload.fieldsData[0].name];
				// dataAction[nId]["section_id"] = section.id_section;
				// dataAction[nId]["description"] = replaceLabelString(datActTmp["description"], fillTmpData);
				// dataAction[nId]["category"] = datActTmp["category"];
				// dataAction[nId]["title"] = replaceLabelString(datActTmp["title"], fillTmpData);
				// bitamApp.dataUser.ACTIONITEMS.add(dataAction);
			// })
			$.each(arrFormsTplCat, function eachArrFormsTPL(i,row){
				//a partir de la informacion del formtemplate damos de alta en registros en una forma determinada que sera guardado en el server en la sincronizacion de datos.
				var nId = bitamApp.dataUser[row.form].newID();
				var fillTmpData = {};
				dataFormTPL = {};
				dataFormTPL[nId] = $.extend(true,{},row.data);
				fillTmpData[section.name] = tmpObject[data[section.payload.fieldsData[0].name]]
				//$.each(dataFormTPL[nId], function(i,v){
				var aSection = bitamApp.application.getSection(row.form);
				$.each(aSection.payload.fieldsForm, function eachFields(i,aField){
					if(aField.tagname == 'widget_field_date'){
						if($.isNumeric(dataFormTPL[nId][aField.id]))
							dataFormTPL[nId][aField.id] = Date.today().add(parseInt(dataFormTPL[nId][aField.id])).days().toString("yyyy-MM-dd");
						else
							dataFormTPL[nId][aField.id] = Date.today().toString("yyyy-MM-dd");
						return true;
					}
					if(isset(dataFormTPL[nId][aField.id])&&!$.isNumeric(dataFormTPL[nId][aField.id]))
						dataFormTPL[nId][aField.id] = replaceLabelString(dataFormTPL[nId][aField.id], fillTmpData);
				})
				//dataFormTPL[nId]["modifiedUserKey"] = bitamApp.user.cla_usuario;
				//dataFormTPL[nId]["createdUserKey"] = bitamApp.user.cla_usuario;
				dataFormTPL[nId]["createdUserKey"] = fillTmpData[section.name].createdUserKey;
				dataFormTPL[nId]["id_"+row.form] = nId;
				bitamApp.dataUser[row.form].add(dataFormTPL);
			})
		}
			
		//si estamos Online siempre guardamos al servidor
		//si estamos Online pero es un App, no guardamos en el server
		if(this.onLine() && bitamApp.isMobile.isApp() == false && options.isOrphanForm != true){
			this.updateDataAppStorage();
			this.syncData({'callback':fn, "actUI":options.actUI});
		}else{
			//si se esta guardando un action item en modo offline
			if(section.id_section == -1){
				for(var keyData in tmpObject){
					//revisamos si el usuario responsable de este action item es subordinado del usuario logeado
					for(var keyUserResponsible in tmpObject[keyData]["user"]) break;
					if(bitamApp.user.subordinados && $.isPlainObject(bitamApp.user.subordinados[keyUserResponsible])){
						tmpObject[keyData].isSubordinado = "1"
					}
					//revisamos si este action item pertenece a un registro del usuario logeado
					var aSectionOwner = bitamApp.application.getSectionWithKey(tmpObject[keyData]["section_id"]);
					if(aSectionOwner && $.isNumeric(tmpObject[keyData]["row_key"])){
						if(bitamApp.user.cla_usuario == bitamApp.dataUser[aSectionOwner.name].collection[tmpObject[keyData]["row_key"]].createdUserKey){
							tmpObject[keyData].isAttendance = 1;
						}
					}
				}
			}
			this.updateDataAppStorage();
			
			var newData = {};
			newData[section.name] = new DataCollection(tmpObject, section.payload.fieldsData)
			fn(newData);
		}

		return {error:false};
	},
	*/
	serializeJSON:function(form) {
		var json = {};
		
		if($.type(form) === "string")
		{
			form = $(form);
		}
				
		jQuery.map(form.serializeArray(), function(n, i)
		{
			if(json[n['name']])
			{
				if($.isArray(json[n['name']]))//isArray
				{
					json[n['name']].push(n['value']);
				}
				else
				{
					json[n['name']] = new Array(json[n['name']]);
					json[n['name']].push(n['value']);
				}
			}
			else
			{
				if($('#' + n['name']).attr('multiple') == 'multiple')
				{
					json[n['name']] = new Array();
					json[n['name']].push(n['value']);
				}
				else
				{
					json[n['name']] = n['value'];
				}
			}
		});
		
		return json;
	},
		
	remove:function(s, id, options)
	{
		//10/01/2013@aramirez - Si no es una App la logica de borrado la hacemos en el server
		if(!bitamApp.isMobile.isApp()){
			deleteItems = {};
			deleteItems[s] = ($.isArray(id)?id:[id]);
			var params = {
				'action':'deleteItems',
				'items':deleteItems,
				'applicationCodeName':bitamApp.application.codeName,
				//10/01/2013@aramirez - 
				//Al no ser una App y no tener todos los registros en memoria la verificacion si los datos tienen detalle se hace en el server
				//para eso definimos el parametro checkDeleteRows = true
				'checkDeleteRows':true,
				//2014-04-22@HADR(#32439) Enviar las formas y ids que estan presentes en la pagina actual para que se actualicen sus modelos en caso de haber cambios
				'updateInterface' : ((options.hasOwnProperty("openedForms"))?options.openedForms:{})
			}
					
			bitamApp.getJSONRequest({
				url:bitamApp.getURLServer() + '/action.php',
				data: params,
				success:function(data)
				{	
					/*2014-03-26 @JRPP Cuando se elimina una seccion tambien la elimno de la variable que almacena todas las secciones de la aplicacion*/
					if(data.payload.result.sections){
						for (var key in bitamApp.dataApp.catalog.AllSections){
							if(bitamApp.dataApp.catalog.AllSections[key].value == id){
								delete bitamApp.dataApp.catalog.AllSections[key];
							}
						}

					}

					bitamApp.pageLoading(true);
					res = new BuilderAppResponse(this);
					$.extend(res, data);
					if(res.getError() != 0){
						alert(res.getErrorSummary());
						return;
					}
					if(res.getError() == 0){
						for(var keySection in deleteItems){
							for(var key in deleteItems[keySection]){
								for(var fKey in res.payload.result) {
									var detailSection = $.extend(true, {}, bitamApp.application.payload.sections[fKey]);
									for(var keyField in detailSection.payload.fieldsForm){
										var eachField = detailSection.payload.fieldsForm[keyField]
										if(eachField.propagateParent && Object.keys(eachField.propagateParent).length>0){
											for(var pKey in eachField.propagateParent) {
												var aSection = bitamApp.application.getSection(eachField.propagateParent[pKey].sectionName);
												var aField = aSection.getFieldFormWithId(eachField.propagateParent[pKey].id);
												if($("[name='"+aField.id+"']").length && $("[name='"+aSection.getFieldKey()+"']").length){
													calculatedValue({
														formulates:aField.element.defaultValue,
														field:aField,
														data:bitamApp.dataUser[aSection.name].collection[$("[name='"+aSection.getFieldKey()+"']").val()]
													}).then(function(res){
														$("[name='"+res.fieldid+"']").setValue(res.result);
													});
												}
											}
										}					
									}
								}
								//checamos si no debemos eliminar un registro de la tabla
								if($.inArray(deleteItems[keySection][key], data.payload.result[keySection].no_remove)!=-1){
									//si no hay que eliminarlo de la tabla, continuamos con el siguiente registro
									continue;
								}
								//2014-02-24@HADR(#DeleteDataUserViewsNavs) Eliminar las vistas y navegaciones asociadas a la seccion
								if(bitamApp.application.codeName == "eBavel"){
									switch(keySection){
										case "sections":
											//Borrar las vistas relacionadas
											var arrViewsDelete = {};
											for(var keyView in a.dataUser["views"].collection){
												if(a.dataUser["views"].collection[keyView].section_id == deleteItems[keySection][key]){
													arrViewsDelete[a.dataUser["views"].collection[keyView].id] = a.dataUser["views"].collection[keyView].id_view;
												}
											}
											var arrNavigationDelete = {};
											for(var keyView in arrViewsDelete){
												a.dataUser["views"].deleteItem(arrViewsDelete[keyView]);
												for(var keyNav in a.dataUser["nav"].collection){
													if(a.dataUser["nav"].collection[keyNav].view_id == keyView && a.dataUser["nav"].collection[keyNav].appId == bitamApp.session('appId')){
														arrNavigationDelete[a.dataUser["nav"].collection[keyNav].id_nav] = a.dataUser["nav"].collection[keyNav].id_nav;
													}
												}
											}
											//Borrar las navegaciones relacionadas
											for(var keyNavDelete in arrNavigationDelete){
												a.dataUser["nav"].deleteItem(arrNavigationDelete[keyNavDelete]);
											}
											break;
										case "views":
											//Borrar las navegaciones relacionadas
											var viewID = a.dataUser["views"].collection[deleteItems[keySection][key]].id;
											var arrNavigationDelete = {};
											for(var keyNav in a.dataUser["nav"].collection){
												if(a.dataUser["nav"].collection[keyNav].view_id == viewID && a.dataUser["nav"].collection[keyNav].appId == bitamApp.session('appId')){
													arrNavigationDelete[a.dataUser["nav"].collection[keyNav].id_nav] = a.dataUser["nav"].collection[keyNav].id_nav;
												}
											}
											for(var keyNavDelete in arrNavigationDelete){
												a.dataUser["nav"].deleteItem(arrNavigationDelete[keyNavDelete]);
											}
											break;
									}
								}
								a.dataUser[keySection].deleteItem(deleteItems[keySection][key]);
							}
						}
						if(isset(options)){
							if(isset(options.prevPage)){
								$.mobile.activePage.data('removepage', true);					
								if(isset(options.prevPage.id)){
									a.view(options.prevPage.section, options.prevPage.id);
								}
								else{
									a.section(options.prevPage.section);
								}
							}
							if(isset(options.removeID)){
								BitamInterface.removeElement(options.removeID);
							}
							if(isset(options.callback) && $.isFunction(options.callback)){
								//se le agrego que envie el response por que se usa en mngr_table.remove() para comprobar que se elimino un registro
								options.callback(data.payload);
							}
						}
					}
				}
			});
			return;
		}
		
		var section = this.application.getSection(s);
		
		//
		var arrFieldsForm = section.getFieldsForm();
		deleteItems_recalculate = {};
		deleteItems_recalculate[s] = ($.isArray(id)?id:[id]);
		for(var aDeleteItem in deleteItems_recalculate[s]) {
			BitamStorage.getData({
				'table':section.name,
				'filter':section.getFieldKey() + '=' + deleteItems_recalculate[s][aDeleteItem]
			}).then($.proxy(function(res) {
				Utils.calculatingParentValues({
					'nameSectionDetail': section.name,
					'collections': res.collection,
				});
			}));
		}
		//
		
		arrayPath = [];
		var pathFind = function(detail, aParent, sPath){
			var aSection = bitamApp.application.getSection(detail.name);
			var sPath = sPath+"->"+detail.name;
			if(!$.isEmptyObject(aSection.sections)){
				for(var key in aSection.sections){
					pathFind(aSection.sections[key], aSection, sPath);
				}
			}else{
				//return sPath
				arrayPath.push(sPath)
			}
		}
		for(var key in section.sections){
			pathFind(section.sections[key], section, section.id);
		}
		
		//tenemos en 'arrayPath' todas las rutas para checar si hay datos, vamos a recorrerlas
		if(!$.isArray(id))
			id = [id];
		var deleteItems = {};
		var isDelete = {};
		var checkDeleteRows = function(aSection, arrKeys, sPath){
			var arrDfdPromise = [];
			for(var key in aSection.sections){
				var dfd = $.Deferred();
				arrDfdPromise.push(dfd.promise());
				var aDetail = bitamApp.application.getSection(aSection.sections[key].name);
				//sPath += '->' + aDetail.id;
				var aSQL = 'SELECT ' + aDetail.getFieldKey() + ' FROM ' + aDetail.id + ' WHERE ' + aDetail.getParentWithName(aSection.id).id + ' IN (' + arrKeys.join(',') + ')';
				BitamStorage.database.execute(aSQL).then($.proxy(function(tx, rs){
					var arrKeys = [], dfd = this.dfd, sPath = this.sPath, aDetail = this.aDetail, aSection = this.aSection;
					for(var x=0; x<rs.rows.length; x++){
						arrKeys.push(rs.rows.item(x)[aDetail.getFieldKey()]);
					}
					if(arrKeys.length){
						sPath = sPath + '->' + aDetail.id;
						deleteItems[sPath] = arrKeys;
						isDelete[sPath] = {"delete": aSection.sections[aDetail.id]["delete"] !=0 , "items":arrKeys, 'sName':aDetail.id, 'label':aDetail.sectionName};			
						checkDeleteRows(aDetail, arrKeys, sPath).then(function(){
							dfd.resolve();
						});
					}else{
						dfd.resolve();
					}
				}, {'aDetail':aDetail, 'dfd':dfd, 'sPath':sPath, 'aSection':aSection}))
			}
			if($.isEmptyObject(aSection.sections)){
				var dfd = $.Deferred();
				arrDfdPromise.push(dfd.promise());
				dfd.resolve();
			}
			return $.when.apply({}, arrDfdPromise)
		}
		checkDeleteRows(section, id, section.id).then(function(){
			if(!$.isEmptyObject(isDelete)){
				var sDetailSectionsNoDelete = '';
				for(var sDetailSection in isDelete){
					if(!isDelete[sDetailSection]["delete"] && !$.isEmptyObject(isDelete[sDetailSection]["items"])){
						sDetailSectionsNoDelete += (sDetailSectionsNoDelete != "" ? ", " : "") + isDelete[sDetailSection]['label'];
					}
				}
				if(sDetailSectionsNoDelete != ""){
					bitamApp.alert($.t("Action not allowed. There are detail records that should be deleted before") + " ("+sDetailSectionsNoDelete+").");
					return false;
				}
			}
			//agregamos el registro maestro al array de objetos
			isDelete[section.id] = {'items':id, 'sName':section.id};
			
			//ahora eliminamos todos los registros detalle que tenga el registro
			for(var key in isDelete){
				var deleteKeysOnline = new Array();
				if(isDelete[key].items.length){
					BitamStorage.removeData(isDelete[key].sName, isDelete[key].items);
					//recorremos todos los items a eliminar
					for(var i=0; i<isDelete[key].items.length; i++){
						bitamApp.dataUser[isDelete[key].sName].deleteItem(isDelete[key].items[i]);
						if(parseFloat(isDelete[key].items[i]) > 0){
							//para los Ids con numero positivo quiere decir que han sido guardados en el server. Cuando vuelva a estar online hay que eliminarlos del server
							deleteKeysOnline.push(isDelete[key].items[i]);
						}
					}
					//para los Ids con numero positivo quiere decir que han sido guardados en el server. Cuando vuelva a estar online hay que eliminarlos del server
					if(deleteKeysOnline.length > 0)
						BitamStorage.addKeysDeleteOnline(isDelete[key].sName, deleteKeysOnline);
				}
			}
			if(isset(options)){
				if(isset(options.prevPage)){
					$.mobile.activePage.data('removepage', true);					
					if(isset(options.prevPage.id)){
						bitamApp.view(options.prevPage.section, options.prevPage.id);
					}
					else{
						bitamApp.section(options.prevPage.section);
					}
				}
				if(isset(options.removeID)){
					BitamInterface.removeElement(options.removeID);
				}
				if(isset(options.callback) && $.isFunction(options.callback)){
					//se le agrego que envie el response por que se usa en mngr_table.remove() para comprobar que se elimino un registro
					var res = {'result':{}};
					res.result[section.id] = {success:true};
					options.callback(res);
				}
			}
		})
		return;
		/*var checkDeleteRows = function(arrayPath, arrKeys){
			for(var i=0; i<arrayPath.length; i++){
				var arrForms = arrayPath.split('->');
				for(var x = 1; x<arrForms; x++){
					var aSection = bitamApp.application.getSection(arrForms[x]);
					var aSQL = 'SELECT ' + aSection.getFieldKey() + ' FROM ' + arrForms[x] + ' WHERE ' + aSection.getParentWithName(arrForms[x-1]).id + ' IN (' + arrKeys.join(',') + ')';
				}
			}	
		}
		checkDeleteRows(arrayPath).then(function(){
		})*/
		
		//objetos que contienen array donde gaurdaremos los ids que se eliminaran
		var deleteItems = {};
		//objeto donde guardaremos la informacion si este detalle podra ser eliminado o no
		var isDelete = {}		
		//si el ID es negativo quiere decir que no esta salvado en el servidor
		// y con solo quitarlo de la Data local basta
		
		//Nuevo algoritomo de borrado, borra el registro, y los detalles de los detalles
		//---------------
		//ahora revisamos si este ID tiene elementos detalle
		// y guardamos todos los id, con la seccion, en una variable para eliminarlos despues
		function checkDeleteRows(detailSection, sPath, sParentName, id){
			debugger;
			var sNameSection = detailSection.name;
			//var sPath = sPath+"->"+detailSection.label+"["+detailSection.name+"]";
			var sPath = sPath+"->"+detailSection.label;
			
			var filter = $.extend({}, detailSection['filterBy']);
			if(id)
			for(var keyFilter in filter){       
                if($.isArray(id)||$.isPlainObject(id)){
					var sFilter = filter[keyFilter];
					filter[keyFilter] = new Array();
				    for(var key1 in id){
				       filter[keyFilter].push(bitamApp.dataUser[sParentName].collection[id[key1]][sFilter]);
				    }
				}else{
					filter[keyFilter] = bitamApp.dataUser[sParentName].collection[id][filter[keyFilter]];
				}
			}
			//console.debug(sPath + "=" + JSON.stringify(filter));
			//revisamos si hay datos para ese detalle
			//como estamos en la APP, hacemos la consulta a la base de datos.
			//preparamos el query:
			var aSection = bitamApp.application.getSection(sNameSection);
			var sQuery = "SELECT "+aSection.getFieldKey()+" FROM " + sNameSection;
			var sFilter = '';
			if(!$.isEmptyObject(filter)){
				sFilter = BitamStorage.buildFilter({'filter':filter});
			}
			if(sFilter != ''){
				sQuery += " WHERE " + sFilter;
			}
			//executamos el query
			BitamStorage.database.execute(sQuery).then($.proxy(function(tx, rs){
				var deleteItems = new Array();
				for(var i=0; i<rs.rows.length; i++){
					deleteItems.push(rs.rows.item(i)[this.aSection.getFieldKey()]);
				}							
				isDelete[this.sPath] = {"delete": this.detailSection["delete"], "items":deleteItems, "sName":this.aSection.name, 'label':this.aSection.sectionName};			
				for(var key in this.aSection.sections){
					checkDeleteRows(this.aSection.sections[key], this.sPath, this.aSection.name, deleteItems.length==0?null:deleteItems);
				}
			}, {'aSection':aSection,
				'sPath':sPath,
				'detailSection':detailSection
			}));
		}
		for(var key in section.sections){
			checkDeleteRows(section.sections[key], section.sectionName, section.name, id);
		}
		
		if(!$.isEmptyObject(isDelete)){
			var sDetailSectionsNoDelete = '';
			for(var sDetailSection in isDelete){
				if(!isDelete[sDetailSection]["delete"] && !$.isEmptyObject(isDelete[sDetailSection]["items"])){
					sDetailSectionsNoDelete += (sDetailSectionsNoDelete != "" ? ", " : "") + isDelete[sDetailSection]['label'];
				}
			}
			if(sDetailSectionsNoDelete != ""){
				alert($.t("Action not allowed. There are detail records that should be deleted before") + " ("+sDetailSectionsNoDelete+").");
				return false;
			}
		}
		
        if($.isArray(id))
        {
        	deleteItems[s] = new Array();
        	deleteItems[s] = id;
        }else
        {
        	deleteItems[s] = new Array();
			deleteItems[s].push(id);
        }
		for(var k in isDelete){
			if(deleteItems[isDelete[k].sName] && isDelete[k].items){
				deleteItems[isDelete[k].sName] = $.merge(deleteItems[isDelete[k].sName], isDelete[k].items)
			}else{
				if(isDelete[k].items)
					deleteItems[isDelete[k].sName] = isDelete[k].items
			}
		}
		//---------------
		//comprobamos si estamos online para poder eliminar los datos	
		if(this.onLine())
		{
			var params = {
				'action':'deleteItems',
				'items':deleteItems,
				'applicationCodeName':this.application.codeName
			}
			
			a = this;
			this.getJSONRequest({
				url:this.getURLServer() + '/action.php',
				data: params,
				success:function(data)
				{	
					bitamApp.pageLoading(true);
					res = new BuilderAppResponse(this);
					$.extend(res, data);
					if(res.getError() == 0){
						for(var keySection in deleteItems){
							for(var key in deleteItems[keySection]){
								//checamos si no debemos eliminar un registro de la tabla
								if($.inArray(deleteItems[keySection][key], data.payload.result[keySection].no_remove)!=-1){
									//si no hay que eliminarlo de la tabla, continuamos con el siguiente registro
									continue;
								}
								a.dataUser[keySection].deleteItem(deleteItems[keySection][key]);
							}
						}
					}					
					if(isset(options)){
						if(isset(options.prevPage)){
							$.mobile.activePage.data('removepage', true);					
							if(isset(options.prevPage.id)){
								a.view(options.prevPage.section, options.prevPage.id);
							}
							else{
								a.section(options.prevPage.section);
							}
						}
						if(isset(options.removeID)){
							BitamInterface.removeElement(options.removeID);
						}
						if(isset(options.callback) && $.isFunction(options.callback)){
							//se le agrego que envie el response por que se usa en mngr_table.remove() para comprobar que se elimino un registro
							options.callback(data.payload);
						}
					}
				}
			});
		}
		else
		{
			for(var keySection in deleteItems)
			{
				var deleteKeysOnline = new Array();
				for(var id in deleteItems[keySection])
				{
					bitamApp.dataUser[keySection].deleteItem(deleteItems[keySection][id]);
					BitamStorage.removeDataSection(keySection, deleteItems[keySection][id]);
					
					if(parseFloat(deleteItems[keySection][id]) > 0)
					{
						//para los Ids con numero positivo quiere decir que han sido guardados en el server. Cuando vuelva a estar online hay que eliminarlos del server
						deleteKeysOnline.push(deleteItems[keySection][id]);
					}
					//delete deleteItems[keySection][id];				
				}
				//para los Ids con numero positivo quiere decir que han sido guardados en el server. Cuando vuelva a estar online hay que eliminarlos del server
				if(deleteKeysOnline.length > 0)
					BitamStorage.addKeysDeleteOnline(keySection, deleteKeysOnline);
			}
			
			if(isset(options))
			{
				if(isset(options.prevPage))
				{
					$.mobile.activePage.data('removepage', true);					
					if(isset(options.prevPage.id))
					{
						a.view(options.prevPage.section, options.prevPage.id);
					}
					else
					{
						a.section(options.prevPage.section);
					}
				}
				if(isset(options.removeID))
				{
					BitamInterface.removeElement(options.removeID);
				}
				if(isset(options.callback) && $.isFunction(options.callback))
				{
					options.callback();
				}
			}
		}
		
		
	},
	
	add:function(s, filter, options)
	{
		if(options == undefined){
			options = {};
		}
		if(options.prevPage == undefined){
			options.prevPage = {'section':s};
		}
		Debugger.message(new Array(10).join('*'));
		Debugger.register('bitamApp.add s=' + s);
		Debugger.message('filter=' + JSON.stringify(filter));
		Debugger.message('options=' + JSON.stringify(options));		
		
		var section = this.application.getSection(s);
		
		//var dataCol = new DataCollection(this.dataUser[s], section.payload.fieldsData);
		var dataCol = this.dataUser[s];		
		var newID = dataCol.newID();
		BitamInterface.add(s, newID, filter, options);
	},
	
	save:function(s, options)
	{
		Debugger.message(new Array(10).join('*'));
		Debugger.register('bitamApp.saveForm s=' + s);
		Debugger.message('options=' + JSON.stringify(options));
		var section = this.application.getSection(s);
		if(section.newGoTo)
        {   
        	var ban=true;
			fnr = function(newData)
			{
				if(bitamApp.onLine()&& isset(options.viewNewForm) && isset(options.viewNewForm['triggersave']))
				{       
					var r = newData[options.prevPage.section].getIDs()[0];
					bitamApp.ajaxRequest({
						/*url:bitamApp.getURLCustomPHP() + 'sendMinutes.inc.php',*/
						url:bitamApp.getURLCustomPHP() + options.viewNewForm['triggersave'],
						data:{
							'data': newData[options.prevPage.section].collection[r],
							'applicationCodeName': bitamApp.application.codeName,
							'isTrigger': true
						}, 
						success:function(data) 
						{
							bitamApp.pageLoading(true);
						}
					});
				}
				bitamApp.view(options.prevPage.section, newData[options.prevPage.section].getIDs()[0]);
			}                    
		}
		var fileUpload = bitamApp.session("fileUpload");
		if(isset(fileUpload) && isset(fileUpload[section.name]))
		{
			if(!$.isEmptyObject(fileUpload[section.name]))
			{
				alert("alskdjalsdkjla");
				return;
			}
		}
		
		
		if(!isset(options))
		{
			options = {'prevPage':{'section':section.name}};
		}
		
		if(isset(options["formData"]))
		{
			var formName = '#'+options["formData"];
		}
		else
		{
			var formName = '#addFormData_'+section.name;
		}
		
		a = this;
		fn = function(newData)
		{
			//revisamos si existe algun proceso y si este primer proceso tiene activado el bloqueo de registros
			if(isset(section.payload.processForms) && isset(section.payload.processForms[0]) && section.payload.processForms[0].regBlock)
			{
				//existe el bloqueo de registros. Ahora agregamos al arreglo de registros bloqueados este nuevo ID
				for(var key in newData[section.id].collection)
				{
					section.payload.processForms[0].arrKeysBlocked[key] = 'blocked';
				}				
			}
			if(!isset(options.addType))
			{
				if(isset(options.prevPage) && isset(options.prevPage.id) && !(isset(options.saveAndNew)))
				{
					bitamApp.view(options.prevPage.section, options.prevPage.id);
				}
				else
				{
					/*if(newData[options.prevPage.section].length() == 1)
					{
						bitamApp.view(options.prevPage.section, newData[options.prevPage.section].getIDs()[0]);
					}
					else*/
					if(options.saveAndNew == true)
					{
						var filterBy = {};
						$.extend(filterBy, section.filterBy);
						$.extend(filterBy, options.filter);
						for(var key in filterBy)
						{
							if(typeof filterBy[key] == 'string' && filterBy[key].substring(0, 9) == 'bitamApp.')
							{
								filterBy[key] = eval(filterBy[key]);
							}
						}
						
						//$.mobile.activePage.attr("id", "test");
						//$.mobile.activePage.attr("name", "test");
						
						var blankPage = BitamInterface.buildPage("blankPage");
						
						blankPage.bind( 'pageshow',function(event, ui){
							options.prevPage = (isset(options.prevPage)) ? options.prevPage : { section:s };					
							bitamApp.add(s, filterBy, {sectionView:options.sectionView, prevPage:options.prevPage, changePage:{ changeHash: false, transition:"fade"}, defaultValues:options.defaultValues, filter:options.filter});
						});
												
						//$.mobile.changePage(blankPage, {transition:"fade"});
						BitamInterface.changePage(blankPage, {transition:"fade"});
						
						//bitamApp.add(s, filterBy, {prevPage:{ section:s }, changePage:{ allowSamePageTransition: true }});
					}
					else
					{
						//en alguna parte del codigo esta regresando un string de "undefined"
						if(isset(options.sectionView) && options.sectionView != "undefined")
						{
							a.sectionView(options.sectionView);
						}
						else
						{
							a.section(options.prevPage.section);
						}						
					}
				}
			}
			else
			{
				if(options.addType == 1 || options.addType == 2)
				{
					BitamInterface.addItemSubsection(section, newData, options);
				}
			}
			/*2014-03-26 @JRPP Cuando seagega una seccion tambien la agrego a la variable que almacena todas las secciones de la aplicacion*/
			if(newData.sections){
				for(var eachSection in newData.sections.collection)
				{
					var app = bitamApp.dataUser.apps.filter('id_app=' + newData.sections.collection[eachSection].appId );
					var nCount = bitamApp.dataApp.catalog.AllSections.length;
					bitamApp.dataApp.catalog.AllSections.push({id_fieldform: nCount,value: newData.sections.collection[eachSection].id_section,ID_APP: newData.sections.collection[eachSection].appId,description: app[newData.sections.collection[eachSection].appId].applicationName + '.' +  newData.sections.collection[eachSection].sectionName});
				}
			}
		}
		for(var eachfield in section.payload.fieldsForm)
		{
			//encuentra los fieldsforms que tengan el widget autocomplete para correr su validacion
			if(isset(section.payload.fieldsForm[eachfield].element.catalog_form)&&section.payload.fieldsForm[eachfield].tagname == "widget_field_alphanumeric"&&$("input[data-inputname='"+section.payload.fieldsForm[eachfield].id+"']").length > 0){
				$.data($("input[data-inputname='"+section.payload.fieldsForm[eachfield].id+"']")[0], "autocomplete")._change()				
			}
		}
		var data = this.serializeJSON(formName);
		
		var bIsMultiPart = false;
		if($(formName).attr("enctype") == "multipart/form-data")
		{
			var myInputFile = $(formName + " input:file");
			for(var i = 0; i<myInputFile.length; i++)
			{
				data[myInputFile[i].id] = $(myInputFile[i]).fileinput("getValue");
			}
			
			myField = $('#optionsForFile');
			myField.val(JSON.stringify(options));
			myForm.append(myField);
			
			bIsMultiPart = true;
		}
		
		fnError = function()
		{
			debugger;
		}
		var section = $.extend({},section);
        $(formName).find("[data-showif_hide='true']").each(function(i, v){
			for(var key in section.payload.fieldsData)
	        {
				if(section.payload.fieldsData[key].name == v.name)
					section.payload.fieldsData[key].required = 0;
			}
		});
                
        if(!ban)
           var res = this.saveForm(section, data, fn, bIsMultiPart, formName, {}, fnError);
		else
           var res = this.saveForm(section, data, fnr, bIsMultiPart, formName, {}, fnError);
                
		if(res.error == true)
		{
			if(res.msgError)
				BitamInterface.alert(res.msgError);
			else
				BitamInterface.alert($.t('Please enter all fields'));
			return;
		}
	
		if(isset(options["formData"]))
		{
			document.getElementById(options["formData"]).reset();
		}
		
		$.mobile.activePage.data('removepage', true);
		
		if(!this.onLine())
			fn();		
				
		//this.section(s);
		
		//todo bien, se guardo el registro en la base local
	},
	
	saveEdit:function(s, id, options)
	{
		fn = function(newData)
		{
			if(options.openmodal){
				$('#modalView_' + s).remove();
				$('.ui-widget-overlay').remove();
				return;
			}
			var path = bitamApp.session('path');
			if(path){
				if(path.length>1)
					path.pop();
				var query = path.pop();
				bitamApp.session('path', path);
				var sSectionQuery = query.section;
				var sSectionIdQuery = query.aKey;
				var aSectionQueryFilter = query.filter;
			}else{
				var sSectionQuery = options.prevPage.section;
				var sSectionIdQuery = options.prevPage.id;
				var aSectionQueryFilter = {};
			}
			
			if(!isset(options.addType))
			{
				if($.isNumeric(sSectionIdQuery)){
					bitamApp.view(sSectionQuery, sSectionIdQuery, {'prevPage':{'section':sSectionQuery}});
				}else{
					if(bitamApp.application.codeName == 'eBavel'){
						bitamApp.section(sSectionQuery);
					}else{
						bitamApp.sectionView(sSectionQuery, undefined, {filter:aSectionQueryFilter});
					}
				}
				/*if(isset(options.prevPage) && isset(options.prevPage.id))
				{
					bitamApp.view(options.prevPage.section, options.prevPage.id, {'prevPage':{'section':options.prevPage.section}});
				}
				else
				{
					if(options.sectionView)
					{
						bitamApp.sectionView(options.sectionView)
					}
					else
					{
						bitamApp.section(options.prevPage.section);
					}					
				}*/
			}
			else
			{
				if(options.addType == 1 || options.addType == 2)
				{
					options["refresh"] = true;
					BitamInterface.addItemSubsection(section, newData, options);
				}
			}
		}
		
		var section = this.application.getSection(s);
		section = $.extend(true, {}, section);
		var formName = '';
		
		if(isset(options["formData"]))
		{
			formName = '#'+options["formData"]
		}
		else
		{
			formName = '#viewFormData_'+section.name
		}
		
		var data = this.serializeJSON(formName);

		/*
		//verificamos si alguno de los campos es un input[type=password]
		$.each($(formName + " input[type=password]"), function(index, value){
			if(value.value == "|bdefpass|")
			{
				delete data[value.name];
			}			
		});*/
		$(formName).find("[data-showif_hide='true']").each(function(i, v){
			for(var key in section.payload.fieldsData){
				if(section.payload.fieldsData[key].name == v.name)
					section.payload.fieldsData[key].required = 0;
			}
		});
		var res = this.saveForm(section, data, fn);
		
		if(res.error == true)
		{
			//BitamInterface.alert('Error data ['+ res.field +']')
			if(res.msgError)
				BitamInterface.alert(res.msgError);
			else
				BitamInterface.alert($.t('Please enter all fields'));
			return;
		}
		
		if(isset(options["formData"]))
		{
			document.getElementById(options["formData"]).reset();
			$('#'+ options["formData"] +' #id_'+section.item).val('');
		}
	},
	
	view:function(s, id, options)
	{
		if(!isset(options))
		{
			options = {'prevPage':{'section':s}};
		}
		
		options.id = id;
		
		var section = this.application.getSection(s);
		var data = new Object();
		//data[section.name] = this.getDataSectionByID(section, id);
        data[section.name] = new DataCollection(this.getDataSectionByID(section, id), section.payload.fieldsData);
                		
		if($.isPlainObject(section.sections) && !$.isEmptyObject(section.sections))
		{
			var fn = function(d){
				data = $.extend(data, d);
				BitamInterface.view(s, data, options);
			};
			
			var subSections = jQuery.extend({}, section.sections);
            this.getDataSubSection(subSections, fn, undefined, data[section.name]);
			return;
		}	
		
		BitamInterface.view(s, data, options);
	}, 
	
	viewFooterForm:function(s)
	{
		var section = this.application.getSection(s);
		BitamInterface.viewFooterForm(section);
	},
	
	viewFloatDetail:function(options)
	{
		//m: string section master
		//d: string section detail
		
		var m = options.section;
		var d = options.sectiondetail;
		var key = options.key;
		
		var sectionMaster = this.application.getSection(m);
		var infoSectionDetail = sectionMaster.sections[d];
		var sectionDetail = this.application.getSection(d);
		
		var data = new Object;
		data.collection = bitamApp.dataUser[sectionMaster.name].getItem(key);
		
		var filter = $.extend({}, infoSectionDetail.filterBy);
		for (var key in filter)
		{
			filter[key] = data.collection[filter[key]];
		}
		
		var fn = function(data)
		{
			BitamInterface.viewFloatDetail({section:sectionDetail, infoSection:infoSectionDetail, data:data, filter:filter});
		}
		
		bitamApp.getDataSubSection([infoSectionDetail], fn, undefined, data);
		
		delete fn;
		delete key;
		delete m;
		delete d;
		delete data;
		delete myWindowModal;
	},
	
	showHistory:function(fieldID, key)
	{
		BitamInterface.showHistory(fieldID, key);
	},
	
	updateDataAppStorage:function()
	{
		if(bitamApp.isWeb() && !bitamApp.isMobile.isApp())
			return;
				
		//@dataStorage
		//Actualizamos cuando fue la ultima vez que se actualizo online los datos
		var modifiedDate = new Date().toString("yyyy-MM-dd HH:mm:ss")
		if(bitamApp.onLine() && bitamApp.isMobile.isApp() == false)
		{
			BitamStorage.setLastSyncDate(modifiedDate);
		}
		for(var table in bitamApp.dataUser)
		{
			//21/02/2013@aramirez
			//buscamos todos los registros que tengan ID negativo y lo guardamos en la base de datos del cliente,
			//despues de insertarlos en la base de datos del cliente los borramos de la memoria de javascript
			var res = bitamApp.dataUser[table].filter(bitamApp.application.getSection(table).getFieldKey()+'<0');
			if(!$.isEmptyObject(res)){
				BitamStorage.insertData(table, res).then(function(){
					//exito
					debugger;
				}, function(a,b){
					//error
					debugger;
				});
			}
		}
	},
	
	fieldAjaxSelect:function(field, value)
	{
		var fn = function(response, field, value)
		{
			bitamApp.pageLoading(true);
			res = new BuilderAppResponse(this);					
			$.extend(res, response);
			
			if(!res.getError())
			{
				var data = res.getPayload().data;
				var el = $('#' + field.element.id);
				for(var key in data)
				{
					var value = data[key]['id_' + field.catalog];
					var description = data[key]['value'];
					
					if(isset(data[key]['description']))
					{
						value = data[key]['value'];
						description = data[key]['description'];
					}
					el.append($('<option/>', {'value':value, 'text':description}));
				}
				debugger;
				el.val(value);
				el.selectmenu();
				el.selectmenu('refresh');
			}
		}
		
		var params = {section:field.catalog};
		params.applicationCodeName = this.application.codeName;		
		
		if(!this.onLine())
		{
			var el = $('#' + field.element.id);
			var data = this.dataApp.catalog[params.section];
			for(var key in data)
			{
				el.append($('<option/>', {'value':data[key]['id_' + field.catalog], 'text':data[key]['value']}));
			}
			el.val(value);
			el.selectmenu();
			el.selectmenu('refresh');
			
			delete data;
			delete el;
			delete key;
			return;
		}
		
		this.getJSONRequest({
				url:this.getURLServer() + '/getData.inc.php', 
				data:params,
				success:function(data)
				{
					fn(data, field, value);
				}
			});
	},
	
	getAllCatalog:function()
	{
		var a = this;
		var app = this.application;
		var sections = app.getSections();
		var allCatalog = new Array();
		
		for(key in sections)
		{
			var fieldsForm = sections[key].payload.fieldsForm
			for(keyField in fieldsForm)
			{
				if(fieldsForm[keyField].catalog && (fieldsForm[keyField].catalog != 'usersGuests' && fieldsForm[keyField].catalog != 'users') && (fieldsForm[keyField].catalog.substring(0, 12) != "dataCatalog(")  )
				{
					allCatalog.push(fieldsForm[keyField].catalog);
				}
			}
			delete fieldsForm;
		}
		
		return allCatalog;
	},
	
	isTablet:function()
	{
		return false;
	},
	
	isMobile:{
		AndroidTab: function() {
			var width = $(window).width();
			if (navigator.userAgent.match(/Android/i) && width > 768){
				return true;
			}
	         
	    },
		AndroidCel: function() {
			var width = $(window).width();
			if (navigator.userAgent.match(/Android/i) && width < 768){
				return true;
			} else {
				return false;
			}
		},
	    Android: function() {
	        return navigator.userAgent.match(/Android/i) ? true : false;
	    },
	    BlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i) ? true : false;
	    },
	    iOS: function() {
	        //return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
			return navigator.userAgent.match(/iPhone|iPod/i) ? true : false;
	    	//return true;
	    },
		iPad: function() {
			return navigator.userAgent.match(/iPad/i) ? true : false;
			//return true;
		},
	    Windows: function() {
	        return navigator.userAgent.match(/IEMobile/i) ? true : false;
	    },
	    any: function() {
	        return (bitamApp.isMobile.AndroidCel() || bitamApp.isMobile.BlackBerry() || bitamApp.isMobile.iOS() || bitamApp.isMobile.Windows());
	    	//return true;
	    },
		pantallasreducidas: function() {
			return (bitamApp.isMobile.AndroidCel() || bitamApp.isMobile.iOS());
		},
	    app: function() {
	    	/*if(window.device == undefined)
	    		return false*/
	    	//return true;
	    	return false;
	    },
		isApp:function(){
			return bitamApp.isMobile.app();
		},

		/** Determina si un equipo contiene pantalla touch */
		isTouchDevice: function() {
			if(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
				return true;
			}
			
			return false;
		}
	},
	
	isWeb:function()
	{
		return !bitamApp.isMobile.any();
		//return false;
	},
	
	responseSyncData:function(data)
	{
		bitamApp.pageLoading(true);
				
		//primero, de la propiedad recordsInserted vemos cuales ID's locales fueron insertados y los eliminamos de los datos locales.
		recordsInserted = data.payload.recordsInserted;
		
		if(recordsInserted == null)
		{
			return;
		}
		
		//recorremos por seccion
		for (var sectionName in recordsInserted)
		{
			for(var key in recordsInserted[sectionName])
			{
				//borramos los datos con ID's negativos que ya han sido guardados en el servidor
				//delete a.dataUser[sectionName][recordsInserted[sectionName][key]];
				//cambio a DataCollection
				a.dataUser[sectionName].deleteItem(recordsInserted[sectionName][key]);
			}						
		}
		
		//ya han sido eliminado los datos locales con ID's temporales que han sido guardados en el servidor, 
		//ahora solo hacemos el merge con los datos locales y guardados
		var newData = data.payload.data;
		for(var sectionName in newData)
		{
			//$.extend(a.dataUser[sectionName], newData[sectionName]);
			//cambio a DataCollection
			bitamApp.dataUser[sectionName].add(newData[sectionName], false);
			//actualizamos la interfaz
			bitamApp.section(sectionName, false);
		}
		
		bitamApp.updateDataAppStorage();
		
		//ponemos en una colecction los newData
		var newDataCol = {};
		for(var sectionName in newData)
		{
			newDataCol[sectionName] = new DataCollection(newData[sectionName], a.application.getSection(sectionName).payload.fieldsData);
		}
		
		var options = data.payload.options;
		
		if(!isset(options.addType))
		{
			if(isset(options.prevPage.id))
			{
				bitamApp.view(options.prevPage.section, options.prevPage.id);
			}
			else
			{
				a.section(options.prevPage.section);
			}
		}
		else
		{
			if(options.addType == 1)
			{
				var section = this.application.getSection(options.sectionName);
				BitamInterface.addItemSubsection(section, newDataCol, options);
			}
		}
	},
	
	date:
	{
		today:function(stringFormat)
		{
			if(!isset(stringFormat))
				stringFormat = "yyyy-M-d";
				
			return new Date().toString(stringFormat);  
		}
	},
	
	formatNumber:function(aNumber, aFormat)
	{
		return formatNumber(aNumber, aFormat);
	},
	
	//@data - un objeto con la coleccion de datos a guardar
	setConfiguration:function(options)
	{
		//actualizamos la coleccion de datos.
		var bFound = false;
		for(var i in bitamApp.dataUser.configura.collection)
		{
			for(var key in options.data)
			{
				if(options.data[key]["CLA_APP"] == bitamApp.dataUser.configura.collection[i]["CLA_APP"] && options.data[key]["CLA_CONFIGURA"] == bitamApp.dataUser.configura.collection[i]["CLA_CONFIGURA"] && options.data[key]["CLA_USUARIO"] == bitamApp.dataUser.configura.collection[i]["CLA_USUARIO"])
				{
					bitamApp.dataUser.configura.collection[i] = $.extend(true, {}, options.data[key]);
					bFound = true;
				}
			}	
		}
		if(!bFound){
			bitamApp.dataUser.configura.collection[i+1] = $.extend(true, {}, options.data[key]);
		}
		
		this.updateDataAppStorage();
		
		//si estamos Online siempre guardamos al servidor
		if(this.onLine())
		{
			var params = new Object();
			params.applicationCodeName = this.application.codeName;
			params.data = options.data;
			//guardamos los registros nuevos en la tabla de configuracion
			this.getJSONRequest({
				url:this.getURLServer() + '/action.php?action=setConfiguration',
				data:params,
				error:options.error,
				success:function(data) 
				{
					bitamApp.pageLoading(true);
				}
			});
		}

		return {error:false};
	},
	
	buildURL:function(string, force)
	{
		if(!bitamApp.onLine() || bitamApp.isMobile.app())
			if(!force){
                if(string[0] === '/') string = string.slice(1);
				return  string;
            }
		if(bitamApp.application.server)
			return bitamApp.application.server + string;
		else
		{
			if(bitamApp.webServer)
				return bitamApp.webServer + string;
			else
				return string;
		}			
	},
	//abre una ventana modal donde se puede ver/agregar las anotaciones de un registro especifico.
	//aparece el icono al lado del registro en la vista, siempre y cuando la seccion/formulario tenga activa la opcion de anotaciones
	showAnnotate:function(keyReg, sectionId)
	{
		var fn = function(data)
		{
			var oData = {};
			oData[bitamApp.application.payload.sections.ANNOTATES.id] = data;
			var aSection = $.extend(true, {}, bitamApp.application.payload.sections.ANNOTATES);
			for(var key in aSection.payload.fieldsForm)
			{
				if(aSection.payload.fieldsForm[key].id == "section_id")
					aSection.payload.fieldsForm[key].element.defaultValue = sectionId;
				if(aSection.payload.fieldsForm[key].id == "row_id")
					aSection.payload.fieldsForm[key].element.defaultValue = keyReg;					
			}
			var filter = {"row_id":keyReg};
			BitamInterface.viewFloatDetail({
				"section":aSection,
				"data":oData,
				"filter":filter
			});
			
		}
		if(isset(bitamApp.application.payload.sections.ANNOTATES))
		{
			var filter = {};
			filter["section_id"] = sectionId;
			filter["row_id"] = keyReg;			
			bitamApp.getDataSection(bitamApp.application.payload.sections.ANNOTATES, fn, {"filter":filter});
		}		
	},
	//carga un archivo css si no estuviera cargado
	loadCSS:function(sUrl){
	//bitamApp.getURLServer() + "/css/templateLayout.css"
		if($("head").find('link[href="'+sUrl+'"]').length == 0)
		{
			$("head").append(
					$("<link/>").attr({
						rel:  "stylesheet",
						type: "text/css",
						href: sUrl
					})
			);
		}		
	},
	
	showArtusDashboard:function(cla_escenario,url_escenario){
		BitamInterface.showArtusDashboard(cla_escenario,url_escenario);
	},
	showArtusReports:function(cla_reporte){
		BitamInterface.showArtusReports(cla_reporte);
	},
	//aramirez@21/01/2013
	//Muestra una ventana modal con el msj de error y un boton para enviar la notificacion a soporte tecnico.
	//@PARAMS
	//options.errorDescription = string de la descripcion
	//options.title = string de titulo de la ventana, opcional
	//options.errorSummary = string de breve descripcion, opcional
	showMsgError: function(options){

		//console.log(title);
		var T = $.t || function(t) { return t};
		require(['views/modal/basicModal', 'bootstrap'], function(BasicModal) {
			var errorSummary = options.errorSummary || T('An error occurred in the application'), title = options.title || 'Whops!';
			console.log(errorSummary);
			var aBasicModal = new BasicModal({
				modalBody:errorSummary,
				modalTitle:title
			});

			if(options.errorIsCritical) {
				aBasicModal.addButton({
					label: T('Send report to technical support'),
					callback: function() {
						/** Vamos a enviar un reporte a soporte Tecnico */
						require(['views/feedback/FeedbackTechnicalReport'], function(FeedBack) {
							aBasicModal.remove();
							aFeedBack = new FeedBack({model: new Backbone.Model({errorDescription: options.errorDescription})});
							$('body').append(aFeedBack.render());
						})
					}
				})
			}
			$('body').append(aBasicModal.render());
		})
		return;
	},
	
	sendErrorReport:function(options){
		return $.ajax({
			url:bitamApp.getURLServer() + '/action.php?action=sendErrorReport',
			data:options,
			type : 'POST'
		})
	}
})

Session = 
{
	appName:null,
	setup:function(appName)
	{
		if(appName != undefined)
		{
			if (!sessionStorage[appName])
		 		sessionStorage[appName] = JSON.stringify({});
		 	
		 	this.appName = appName;
		}
		else
		{
			sessionStorage.clear();
		}
	},
	
	data:function(key, value)
	{
		//obtenemos los datos de session para esta aplicacion
		if(this.appName == undefined)
		{
			if(value == undefined)
			{
				if(sessionStorage[key] == undefined)
					return undefined;

				return JSON.parse(sessionStorage[key]);
			}
			else
			{
				sessionStorage[key] = JSON.stringify(value);
				return value;
			}
		}
		else
		{
			var object = JSON.parse(sessionStorage[this.appName]);
		}
		
		//obtenemos el valor guardado
		if(value == undefined)
		{
			if(object[key] == undefined)
				return undefined;

			return object[key];
		}
		
		//
		object[key] = value;
		sessionStorage[this.appName] = JSON.stringify(object);
		return value;
	}
}

function BuilderAppResponse(b,a)
{
	$.extend(this,
	{
		error:0,
		errorSummary:null,
		errorDescription:null,
		silentError:false,
		applicationName:null,
		description:null,
		version:null,
		language:null,
		payload:a||null,
		request:b||null
	});
	
	return this;
}

$.extend(BuilderAppResponse.prototype,
{
	getRequest:function(){return this.request;},
	getPayload:function(){return this.payload;},
	getError:function(){return this.error;},
	setError:function(a){a=(a===undefined?null:a);this.error=a;return this;},
	getErrorSummary:function(){return (typeof $.t == 'function' ? $.t(this.errorSummary):this.errorSummary);},
	setErrorSummary:function(a){a=(a===undefined?null:a);this.errorSummary=a;return this;},
	getErrorDescription:function(){return this.errorDescription;},
	setErrorDescription:function(a){a=(a===undefined?null:a);this.errorDescription=a;return this;},
	getErrorIsWarning:function(){return this.errorIsWarning;},
	
	getNavigation:function(){return this.payload.navigation},
	getSections:function()
	{
		return this.payload.sections
	},
	getNotificationFromID: function(notificationsID) {
		var oNotifications = {};
		var hasNotification = false;
		if(bitamApp.application.mdVersion >= 4){
			var objNotification = bitamApp.application.payload.notifications;
			for(var key in objNotification){
				if(objNotification[key].id_notifications == notificationsID){
					oNotifications = objNotification[key];
					hasNotification = true;
					break;
				}
			}
		}
		
		if(!hasNotification){
			return null;
		}else{
			return oNotifications;
		}
	},
	//JSon- Regresa el JSon correspondiente con la sección especificada
	getSection:function(a)
	{
		var section = this.payload.sections[a];
		if(!isset(section))
		{
			//si 'section' es undefined quiere decir que podria ser una vista, 
			//buscamos en todas las vistas de todas las secciones para regresar la seccion de esa vista
			section = this.getSectionWithView(a);
			return section;
		}
		//Código de eMeetings (Obsoleto): extend representaba el JSon de un formulado del que fué copiado el formulario actual (en eBavel son Views)
		if(isset(section.extend))
		{
			
			section = $.extend({}, this.payload.sections[section.extend], section)
			//section.payload =  $.extend(true, {}, this.payload.sections[section.extend].payload, section.payload);			
			section.payload =  $.extend({}, this.payload.sections[section.extend].payload, section.payload);			
		}
		return section;
		//return this.payload.sections[a]
	},
	getSectionWithName:function(sName)
	{
		var section = this.payload.sections
		for(var key in section)
		{
			if(section[key].sectionName == sName)
			{
				return section[key];
			}
		}
		
		return null;
	},
	//busca una seccion por su llave
	getSectionWithKey:function(aKey)
	{
		var section = this.payload.sections
		for(var key in section)
		{
			if(parseInt(section[key].id_section) == aKey)
			{
				return section[key];
			}
		}
		
		return null;
	},	
	//JSon- Busca entre las secciones existentes alguna que contenga a la vista del nombre especificado y regresa el JSon de dicha sección
	getSectionWithView:function(sName){
		var sections = this.payload.sections;
		for(var key in sections){
			if(isset(sections[key].payload.views) && !$.isEmptyObject(sections[key].payload.views)){
				var views = sections[key].payload.views;
				if(isset(views[sName]) && views[sName].id== sName){
					return sections[key];
				}				
			}
		}		
		return undefined;
	},
	//JSon- Busca entre las secciones existentes alguna que contenga a la vista del nombre especificado y regresa el JSon de dicha sección
	getSectionWithViewKey:function(aKey){
		var sections = this.payload.sections;
		for(var key in sections){
			if(isset(sections[key].payload.views) && !$.isEmptyObject(sections[key].payload.views)){
				var views = sections[key].payload.views;
				for(var keyV in views){
					if(views[keyV].id_view== aKey){
						return sections[key];
					}
				}
			}
		}		
		return undefined;
	},
	getViewWithInfoDetail: function(infoDetail) {
		var aSection = this.getSection(infoDetail.name);
		if(!aSection) return null;
		return aSection.getViewWithID(infoDetail.viewName);
		
	},
	getBitamMode:function(){return this.bitamMode},
	getOption:function(key){return this.payload.options[key];},
	getFieldsFormWithIds: function(arrFields){
		var res = {};
		var mapFieldsId = this.mapFieldsId||{};
		for(var key in arrFields){
			if(mapFieldsId[arrFields[key]]){
				var aFieldForm = bitamApp.application.getSection(mapFieldsId[arrFields[key]].sectionName).getFieldFormWithId(arrFields[key]);
				res[aFieldForm.id] = aFieldForm;
			}else{
				//no se encontro en el mapa, hay que buscarlo en todas las secciones
				for(var sectionName in bitamApp.application.payload.sections){
					var aFieldForm = bitamApp.application.payload.sections[sectionName].getFieldFormWithId(arrFields[key]);
					if(aFieldForm){
						mapFieldsId[arrFields[key]] = {'sectionName':sectionName};
						res[aFieldForm.id] = aFieldForm;
						break;
					}
				}
			}
		}
		this.mapFieldsId = mapFieldsId;
		return res;
	},
	//2013-12-16@ARMZ: Agregado la funcionalidad para que busque un fieldform por su llave en todas las secciones
	getFieldsFormWithKeys: function(arrFields){
		var res = {};
		var mapFieldsKey = this.mapFieldsKey||{};
		for(var key in arrFields){
			if(mapFieldsKey[arrFields[key]]){
				var aFieldForm = bitamApp.application.getSection(mapFieldsKey[arrFields[key]].sectionName).getFieldFormWithKey(arrFields[key]);
				res[aFieldForm.id_fieldform] = aFieldForm;
			}else{
				//no se encontro en el mapa, hay que buscarlo en todas las secciones
				for(var sectionName in bitamApp.application.payload.sections){
					var aFieldForm = bitamApp.application.payload.sections[sectionName].getFieldFormWithKey(arrFields[key]);
					if(aFieldForm) {
						mapFieldsKey[arrFields[key]] = {'sectionName':sectionName};
						res[aFieldForm.id_fieldform] = aFieldForm;
						break;
					}
				}
			}
		}
		this.mapFieldsKey = mapFieldsKey;
		return res;
	},
	//2014-01-06@HADR(#BusinessProcess) Obtener el userkey en base al email del usuario
	getUserKeyWithEmail: function(email){
		var userKey = null;
		for(var key in bitamApp.dataApp.users){
			var eachUser = bitamApp.dataApp.users[key];
			if(eachUser.cuenta_correo == email){
				userKey = key;
				break;
			}
		}
		return userKey;
	}
});

function Section(aSection){
	$.extend(this, aSection);
	
	return this;
}

$.extend(Section.prototype, {

	/** Regresa el fieldform dado en la posicion en el array, si no recibe parametro regresa el numero de fieldsforms en la forma */
	fieldsForm: function(key) {
		if( $.isNumeric(key) ) {
			if(this.payload.fieldsForm[key]) return new FieldForm(this.payload.fieldsForm[key]);
		} else if(key == undefined) {
			return this.payload.fieldsForm.length;
		}

		return false;
	},
	getFieldsForm: function(){
		var res = [];
		for(var key in this.payload.fieldsForm){
			res.push(new FieldForm(this.payload.fieldsForm[key]))
		}
		return res;
	},
	getFieldFormWithId: function(id){
		for(var key in this.payload.fieldsForm){
			if(this.payload.fieldsForm[key].id == id){
				//return this.payload.fieldsForm[key];
				return new FieldForm(this.payload.fieldsForm[key]);
			}
		}
		return false;
	},
	//2013-12-16@ARMZ Busca un campo por la llave del campo
	getFieldFormWithKey: function(id){
		for(var key in this.payload.fieldsForm){
			if(this.payload.fieldsForm[key].id_fieldform == id){
				return new FieldForm(this.payload.fieldsForm[key]);
			}
		}
		return false;
	},
	getFieldFormWithIdByParents: function(id){
		var aFieldForm = false, aSection = null;
		var arrParents = this.parents();
		for(var i=0; i<arrParents.length&&aFieldForm==false; i++){
			aSection = bitamApp.application.getSection(arrParents[i].sectionName);
			if(aSection){
				aFieldForm = aSection.getFieldFormWithId(id);
			}
		}
		return aFieldForm;
	},
	getParentWithName: function(sName){
		for(var key in this.payload.infoMasterSections){
			if(this.payload.infoMasterSections[key].sectionName == sName){
				return this.payload.infoMasterSections[key];
			}
		}
		return false;
	},
	getProcessWithID: function(aKey){
		for(var key in this.payload.processForms){
			if(this.payload.processForms[key].id_process == aKey){
				return new Process(this.payload.processForms[key]);
			}
		}
		return false;
	},
	getProcessInitial: function(){
		if($.isArray(this.payload.processForms) && this.payload.processForms.length)
			return new Process(this.payload.processForms[0]);
		return false;
	},
	getProcessForm: function() {
		if($.isArray(this.payload.processForms)) {
			return this.payload.processForms;
		}
		return [];
	},
	getActionForm: function() {
		if($.isArray(this.payload.actionforms)) {
			return this.payload.actionforms;
		}
		return [];
	},
	//2013-12-26@HADR(#BusinessProcess) Nuevo metodo para obtener el listado de procesos para la seccion
	getActiveBusinessProcessForm: function() {
		var arrBusinessProcesses = {};
		var hasBusinessProcesses = false;
		if(bitamApp.application.mdVersion >= 4 && bitamApp.application.payload.businessProcesses && bitamApp.application.payload.businessProcesses.hasOwnProperty(this.id)){
			//return bitamApp.application.payload.businessProcesses[this.id];
			var objBusinessProcesses = bitamApp.application.payload.businessProcesses[this.id];
			for(var key in objBusinessProcesses){
				//2014-01-03@HADR(#BusinessProcess) Solo valida que el estatus del business process sea activo
				if(objBusinessProcesses[key].status == 1){
					arrBusinessProcesses[key] = objBusinessProcesses[key];
					hasBusinessProcesses = true;
				}
			}
		}
		if(!hasBusinessProcesses){
			arrBusinessProcesses = undefined;
		}
		return arrBusinessProcesses;
	},
	getBusinessProcessFromKey: function(businessProcesskey) {
		var oBusinessProcess = {};
		var hasBusinessProcesses = false;
		if(bitamApp.application.mdVersion >= 4 && bitamApp.application.payload.businessProcesses.hasOwnProperty(this.id)){
			var objBusinessProcesses = bitamApp.application.payload.businessProcesses[this.id];
			for(var key in objBusinessProcesses){
				if(objBusinessProcesses[key].id_businessProcess == businessProcesskey){
					oBusinessProcess = objBusinessProcesses[key];
					hasBusinessProcesses = true;
					break;
				}
			}
		}
		
		if(!hasBusinessProcesses){
			return null;
		}else{
			return oBusinessProcess;
		}
	},
	//2014-01-09@HADR(#BusinessProcess) obtiene el business process en base al ID
	getBusinessProcessFromID: function(businessProcessID) {
		var oBusinessProcess = {};
		var hasBusinessProcesses = false;
		if(bitamApp.application.mdVersion >= 4 && bitamApp.application.payload.businessProcesses.hasOwnProperty(this.id)){
			var objBusinessProcesses = bitamApp.application.payload.businessProcesses[this.id];
			for(var key in objBusinessProcesses){
				if(objBusinessProcesses[key].id == businessProcessID){
					oBusinessProcess = objBusinessProcesses[key];
					hasBusinessProcesses = true;
					break;
				}
			}
		}
		
		if(!hasBusinessProcesses){
			return null;
		}else{
			return oBusinessProcess;
		}
	},
	//2014-01-07@HADR(#BusinessProcess) Nuevo metodo para obtener el business process que esta en proceso
	getBusinessProcessInProcess: function(businessProcessKey, oStatus) {
		var isBPInProcess = false;
		
		if(bitamApp.application.mdVersion >= 4 && oStatus){
			for(var keyStatus in oStatus){
				if(oStatus[keyStatus] == "In Process"){
					isBPInProcess = true;
				}
			}
		}
		
		if(isBPInProcess){
			return this.getBusinessProcessFromKey(businessProcessKey);
		}else{
			return null;
		}
	},
	//2014-01-07@HADR(#BusinessProcess) Nuevo metodo para obtener el businessprocessStep mediante el key en un business process
	getBusinessProcessStepWithKey: function(oBusinessProcess, stepKey) {
		var hasBusinessProcessStep = false;
		var oBusinessProcessStep;
		if(bitamApp.application.mdVersion >= 4){
			for(var key in oBusinessProcess.payload.steps){
				var eachStep = oBusinessProcess.payload.steps[key];
				if(eachStep.id_businessProcessSteps == stepKey){
					oBusinessProcessStep = eachStep;
					hasBusinessProcessStep = true;
					break;
				}
			}
		}
		
		if(hasBusinessProcessStep){
			return oBusinessProcessStep;
		}else{
			return null;
		}
	},
	//2014-01-09@HADR(#BusinessProcess) obtiene el business process step en base al ID
	getBusinessProcessStepWithID: function(oBusinessProcess, stepID) {
		var hasBusinessProcessStep = false;
		var oBusinessProcessStep;
		if(bitamApp.application.mdVersion >= 4){
			for(var key in oBusinessProcess.payload.steps){
				var eachStep = oBusinessProcess.payload.steps[key];
				if(eachStep.id == stepID){
					oBusinessProcessStep = eachStep;
					hasBusinessProcessStep = true;
					break;
				}
			}
		}
		
		if(hasBusinessProcessStep){
			return oBusinessProcessStep;
		}else{
			return null;
		}
	},
	parents: function(){
		if(this.payload.infoMasterSections)
			return this.payload.infoMasterSections;
		else
			return [];
	},
	details: function(){
		if(this.sections)
			return this.sections;
		else
			return [];
	},
	//regresa el nombre del campo que sirve como llave de la forma
	getFieldKey: function(){
		return this.payload.fieldsData[0].name;
	},
	//regresa el objeto de Vista, en this.payload.views se guardan las vistas personalizadas.
	getViewWithID: function(id){
		if(!this.payload.views[id])
			return null;
		this.payload.views[id].sectionName = this.name;
		return new View(this.payload.views[id]);
	},
	getArrFieldsName: function() {
		var arrFields = [];
		for(var key in this.payload.fieldsForm){
			arrFields.push(this.payload.fieldsForm[key].id);
		}
		return arrFields;
	},
	//regresa en un array los nombres de los campos que son campos llave en la seccion/formulario
	getArrFieldsNameKey: function(){
		var arrFields = [];
		for(var key in this.payload.fieldsForm){
			if($.boolean(this.payload.fieldsForm[key].keyfield)){
				arrFields.push(this.payload.fieldsForm[key].id);
			}
		}
		return arrFields;
	},
	getArrFieldsNameRequire: function(){
		var arrFields = [];
		for(var key in this.payload.fieldsForm){
			if(this.payload.fieldsForm[key].valid && $.boolean(this.payload.fieldsForm[key].valid.required)){
				arrFields.push(this.payload.fieldsForm[key].id);
			}
		}
		return arrFields;
	},
	//2013-11-09@EVEG :(#setDefaultVisibility) Funcion que recibel el nombre del campo y el booleano para validar que sea visible
	setDefaultVisibility: function(sFieldName, aBoolean) {
		var aFieldForm = this.getFieldFormWithId(sFieldName);
		if(aBoolean){// && aFieldForm.element.type && aFieldForm.element.type == 'hidden') {
			//El campo es visible, borras el 
			//delete aFieldForm.element.type;
			if(aFieldForm && aFieldForm.element)
				aFieldForm.element.isVisibleForUser = true;
		}else if (!aBoolean) {
			if(aFieldForm && aFieldForm.element){
				aFieldForm.element.type = 'hidden';
				aFieldForm.element.isVisibleForUser = false;
			}
		}
	},
	//2013-12-06@RTORRES:(#COOX5E) funciones para verificar ls funcion de propiedad dinamica y los datos que se aplicaran
	getDynamicProperties: function(field){
		if(!bitamApp.user.dynamicProperties || !bitamApp.user.dynamicProperties[field.sectionName]) return null;
	    if(field) 
			return bitamApp.user.dynamicProperties[field.sectionName][field.id];		
	    return bitamApp.user.dynamicProperties[field.sectionName];
	},
	evalDynamicProperties: function(field, data) {
		var res = new Array();
		res.setIf = false;
		res.idValue = 0; 
		var aProperties = this.getDynamicProperties(field);
		if(!aProperties) return res;
		if($.isArray(aProperties.functionEval))
		{
		    for(var key in aProperties.functionEval)
		    {
			if(!$.isFunction(aProperties.functionEval[key]))
				aProperties.functionEval[key]= eval('1&&' + aProperties.functionEval[key])
			if(aProperties.functionEval[key](data))
			{
			    res.setIf = aProperties.functionEval[key](data);
			    res.idValue = key; 
			    return res
			}
		    }
		    return res;
		}else
		{
		    if(!$.isFunction(aProperties.functionEval))
			    aProperties.functionEval= eval('1&&' + aProperties.functionEval)
			
		    res.setIf = aProperties.functionEval(data);
		    res.idValue = 0; 
		    return res;
		}
	},
	getStyleDynamicProperties: function(field,data) {
		var res  = this.evalDynamicProperties(field,data);
		if(res.setIf) {
			var aProperties = this.getDynamicProperties(field);
			return aProperties.Values[res.idValue];
		}
		return {};
	}

});

function View(aView){
	$.extend(this, aView);
	
	return this;
}

$.extend(View.prototype, {
	getFieldWithName: function(sName){
		for(var i = 0; i<this.fieldsList.length; i++){
			if(this.fieldsList[i].id == sName)
				return this.fieldsList[i];
		}
		return null;
	},
	/*
	2013-10-14@ARMZ
	Renderea la tabla de la vista con el widget de bitam.table
	parametros:
		filter: Filtro de la vista, si es detalle incluye el filtro del detalle.
		buildBoxFilter: Booleano, indica si debe o no pintar el  cuadro de filtros. Si no se envia usara el default de la vista
		openmodal: Booleano, si es true, al crear un nuevo registro la forma se abrira en una ventana modal
	*/
	buildHTML:function(options){
		options = options||{};
		var self = this, aSection = bitamApp.application.getSection(this.sectionName), fieldsForm = $.extend([], aSection.payload.fieldsForm);
		//2013-11-05@ARMZ: (#ebvWizardGrid #wizard) Agregada la opcion para indicarle al boton de new que tipo de forma debe mostrar (normal o wizard)
		options.editType = ((this.howtoaddrecord == null || this.howtoaddrecord == "1")?aSection.editType:this.howtoaddrecord);
		//recorremos todos los fieldlist, debemos buscar los fieldlist que no esten en la forma dueña de la vista
		this.fieldsList.forEach(function(item){
			if(item.sectionName != aSection.name){
				//buscamos el campo y lo agregamos al array de fieldsForm
				var aFieldForm = bitamApp.application.getSection(item.sectionName).getFieldFormWithId(item.id);
				fieldsForm.push($.extend(true, {}, aFieldForm));
			}
		})
		var properties = {
			'name':"view_" + (options.name ? options.name : this.name),
			'tableHeaderColumn': $.extend([], this.fieldsList),
			'fieldsForm': fieldsForm,
			'editType': options.editType,
			'canView': (options.canView != undefined ? options.canView : checkViewSecurity(this.name, bitamApp.user, aSection, 'canView')),
			'canAdd': checkViewSecurity(this.name, bitamApp.user, aSection, 'canAdd'),
			'canEdit': checkViewSecurity(this.name, bitamApp.user, aSection, 'canEdit'),
			'canDelete': (options.canDelete != undefined ? options.canDelete : checkViewSecurity(this.name, bitamApp.user, aSection, 'canDelete')),
			'canImportExcel': checkViewSecurity(this.name, bitamApp.user, aSection, 'canImportExcel'),
			'canExportExcel': checkViewSecurity(this.name, bitamApp.user, aSection, 'canExportExcel'),
			'canExportPDF': checkViewSecurity(this.name, bitamApp.user, aSection, 'canExportPDF'),
			'canAnnotate': aSection.canAnnotate,
			'section': aSection,
			'filter': $.extend({}, options.filter),
			'sortable': aSection.sortable ? true:false,
			'sectionView': this.name,			
			'fieldsGroup': this.fieldsGroup,
			'editInView': this.editInView,
			'openmodal': options.openmodal,
			'securityCriteria': bitamApp.user.securityCriteria[aSection.name],
			'securityCriterionView': bitamApp.user.securityCriterionView[this.name],
			'buildBoxFilter': (typeof options.buildBoxFilter == 'boolean' ? options.buildBoxFilter : this.boxFilter),
			'data': options.data,
			'howtowatch': options.howtowatch || this.howtowatch,
			'masterSection' : options.masterSection || {},
			'isDetail':options.isDetail,
			'viewPagination':options.viewPagination
		};
		if(properties.canView == true){
			properties.canView = function(key, opt){
				options.viewonly = opt.viewonly;
				options.widgetName = properties.name;
				/*2014-08-08@JRPP (#NHHGTA) se agrega la variable r y se returna ya que estaba ocacionando error
				al abrir un detalle de una vista */
				var TR = $("#table_"+options.widgetName+"_tr_"+key), r;
				//08/03/2013@aramirez
				//Mientras se esta abriendo el formulario se pone esta variable sobre el renglon de la tabla para que si el usuario le da de nuevo click
				//No abra de nuevo el registro y ocasione un error de render
				TR.data('editing',true);
				r = bitamApp.view(aSection.name, key, options).then(function(){
					//cuando termina de abrir el formulario se pone en false
					TR.data('editing', false);
				})
				delete aSection, key, opt, properties;
				return r;
			};
		}
		//2013-10-28@ARMZ: (#defaultValue #defaultValueAsync) Agregado la funcionalidad de obtener el valor default desde la definicion de la vista (reemplaza el valor default original que pudiera tener)
		//2013-11-08@ARMZ: () Correccion, el objeto infoDetailSection puede venir vacio.
		if(options.infoDetailSection && !$.isEmptyObject(options.infoDetailSection.defaultValues)){
			for(var key in options.infoDetailSection.defaultValues){
				for(var keyField in properties.fieldsForm){
					if(properties.fieldsForm[keyField].id_fieldform == options.infoDetailSection.defaultValues[key].fieldform_id){
						properties.fieldsForm[keyField].element.defaultValue = options.infoDetailSection.defaultValues[key].defaultValue;
					}
				}
			}
			//agregamos a properties los datos del padre para poder hacer el remplazo en caso de que sea necesario
			//properties.masterData = options.data[options.masterSection.name].collection;
			//agregamos tambien a properties la informacion de campo default cuando el formulario no sea inline
			properties.defaultValues = options.infoDetailSection.defaultValues;
		}

		var aDivTable = $("<div/>");

		if(properties.howtowatch=='5'){
			properties.isGanttDetail=true;
			return aDivTable.buildGantt(properties);
		} else if ( properties.howtowatch=='6' ) {
			require(['libs/widgets/jquerywidget.editableGrid'], function(){
				aDivTable.editableGrid(properties);
			});
			return aDivTable;
		} else {
			return aDivTable.table(properties);
		}
	}
});

function FieldForm(aField){
	$.extend(this, aField);
	
	return this;
}

$.extend(FieldForm.prototype, {
	isCatalog: function(){
		return this.element.catalog_form != null || this.element.q1 != null
	},
	getCatalogForm: function(){
		return bitamApp.application.getSection(this.element.catalog_form);
	},
	attr: function(sName){
		if(sName){
			return this.element[sName];
		}
	},
	//2013-10-30@ARMZ: (#DefaultValueMultipleChoice #isMultipleChoice) Agregada la opcion para saber si el campo es multiple choice
	isMultipleChoice: function() {
		return this.tagname == 'widget_checkbox_multiple-vertical' || this.tagname == 'widget_checkbox_multiple-horizontal' || this.tagname == 'widget_selectlist_multiple';
	},
	isSingleChoice: function() {
		return this.tagname == 'widget_selectlist' || this.tagname == 'widget_radiobutton_single-vertical' || this.tagname == 'widget_radiobutton_single-horizontal';
	},
	/** Indica si el campo es de tipo seleccion sencilla */
	isSelectList: function() {
		return this.tagname == 'widget_selectlist';
	},
	//2013-10-30@ARMZ: (#defaultValueInActions) Agregada la opcion para saber si el campo es de tipo date
	isDate: function() {
		return this.tagname == 'widget_field_date';
	},
	//2013-10-30@ARMZ: (#defaultValueInActions) Agregada la opcion para saber si el campo es de tipo Time
	isTime: function() {
		return this.tagname == 'widget_field_time';
	},
	isString: function() {
		return this.tagname == 'widget_field_alphanumeric';
	},
	/** Indica si el campo es de tipo TextArea*/
	isText: function() {
		return this.tagname == 'widget_textarea';
	},
	isNumeric: function() {
		return this.tagname == 'widget_field_numeric';	
	},
	/** 2014-07-01@ARMZ Comprueba si el campo es de tipo documento */
	isDocument: function() {
		return this.tagname == 'widget_file';
	},
	//2013-12-04@ARMZ: (#widgetCalculatedField) Nuevo campo calculado, no se guarda el valor en la base de datos, solo se calcula en memoria
	isCalculatedField: function() {
		return this.tagname == 'widget_calculatedfield';
	},
	/** Funcion que define si un campo debe estar visible en la interfaz sin importar cualquier otra cosa*/
	isVisibleForUser: function() {
		var isVisibleForUser = this.attr('isVisibleForUser'), type = this.attr('type');
		return ((isVisibleForUser == undefined || isVisibleForUser == true) && type != 'hidden');
	},
	/** Define si un campo puede ser mostrado en la vista o no */
	isVisibleInView: function() {
		var isVisibleForUser = this.attr('isVisibleForUser')
		return (isVisibleForUser == undefined || isVisibleForUser == true);
	},
	isSystemField: function(){
		if(this.label.indexOf('FDT_') != -1 || this.label.indexOf('BPCT_') != -1)
			return true;
		else
			return false;
	},
	/* Funcion que regresa true si el campo en edicion es de solo lectura */
	isViewOnlyEdit: function() {
		return this.element.edit == 0
	},
	/* Funcion que regresa true si el campo en inserccion es de solo lectura */
	isViewOnlyInsert: function() {
		return this.element.insert == 0
	},
	/** Indica si el campo es una llave */
	isKey: function() {
		return this.keyfield && true;
	},
	/** Indica si el campo requiere un valor */
	isRequired: function() {
		return this.valid.required === 1 || this.valid.required === true;
	}
	
});

function DataDefinition(a)
{
	$.extend(this,
	{
		definition:a
	});
	return this;
}

$.extend(DataDefinition.prototype,
{
	getDefinition:function(){return this.definition;},
	validateData:function(data){
		var d = this.definition;
		for(var key in d){
			var type = d[key].type;
			var defaultValue = d[key].defaultValue
			var required = d[key].required
			var name = d[key].name;
			var validateType = d[key].validate;
			var defaultValue = d[key].defaultValue;
			if(required && ((typeof data[name] == 'string' && data[name] == '') || (data.hasOwnProperty(name) && data[name] == undefined))){
				console.debug('Dato invalido en el campo ['+name+']: "' + data[name] + '"');
				return {error:true, field:name};
			}
			
			if(isset(validateType)){
				if(validateType == 'email' && !validateEmail(data[name])){
					return {error:true, field:name};
				}
			}
			
			if(data[name] == undefined && isset(defaultValue)){
				data[name] = defaultValue;
			}
		}
		return {error:false}
	},
	getKeyFields:function()
	{
		var d = this.definition;
		var res = new Array();
		for(var key in d)
		{
			if(d[key].primaryKey)
			{
				res.push(d[key]);
			}
		}
		return res;
	}
});

function EventTarget(){
    this._listeners = {};
	var self = this;
	
	var addListener = function(type, listener){
        if (typeof self._listeners[type] == "undefined"){
            self._listeners[type] = [];
        }
        self._listeners[type].push(listener);
    }
	
	var removeListener = function(type, listener){
        if (self._listeners[type] instanceof Array){
            var listeners = self._listeners[type];
            for (var i=0, len=listeners.length; i < len; i++){
                if (listeners[i].listener === listener){
                    listeners.splice(i, 1);
                    return true;
                }
            }
        }
		return false;
    }
	
	var fire = function(event){
        if (typeof event == "string"){
            event = { type: event };
        }
        if (!event.type){  //falsy
            throw new Error("Event object missing 'type' property.");
        }
        if (self._listeners[event.type] instanceof Array){
            var listeners = self._listeners[event.type];
            for (var i=0, len=listeners.length; i < len; i++){
                listeners[i].listener.call({}, event);
            }
			for (var i=0, len=listeners.length; i < len; i++){
				if(listeners[i].type=='one')
					if(removeListener(event.type, listeners[i].listener))
						break;
            }
        }
    }
	
	return {
		one: function(type, listener){
			addListener(type, {'type':'one', 'listener':listener});
		},
		on: function(type, listener){
			addListener(type, {'type':'on', 'listener':listener});
		}, 
		hasEvent: function(type){
			if(self._listeners[type])
				return true;
			return false;
		}, 
		trigger: function(type, params){
			fire({'type':type, 'data':params});
		}
	}
}
EventTarget.prototype = {
	constructor: EventTarget
}
/*
EventTarget.prototype = {
    constructor: EventTarget,
    addListener: function(type, listener){
        if (typeof this._listeners[type] == "undefined"){
            this._listeners[type] = [];
        }
        this._listeners[type].push(listener);
    },
	one: function(type, listener){
		if (typeof this._listenersOnce[type] == 'undefined'){
			this._listenersOnce[type] = [];
		}
		this._listenersOnce[type].push(listener);
	},
    fire: function(event){
        if (typeof event == "string"){
            event = { type: event };
        }
        //if (!event.target){
        //    event.target = this;
        //}
        if (!event.type){  //falsy
            throw new Error("Event object missing 'type' property.");
        }
        if (this._listeners[event.type] instanceof Array){
            var listeners = this._listeners[event.type];
            for (var i=0, len=listeners.length; i < len; i++){
                listeners[i].call(this, event);
            }
        }
		
		if (this._listenersOnce[event.type] instanceof Array){
            var listeners = this._listenersOnce[event.type];
            for (var i=0, len=listeners.length; i < len; i++){
                listeners[i].call(this, event);
            }
			for (var i=0, len=listeners.length; i < len; i++){
                this.removeListener(event.type, listeners[i]);
            }
        }
    },
	removeListener: function(type, listener){
        if (this._listeners[type] instanceof Array){
            var listeners = this._listeners[type];
            for (var i=0, len=listeners.length; i < len; i++){
                if (listeners[i] === listener){
                    listeners.splice(i, 1);
                    break;
                }
            }
        }
		if (this._listenersOnce[type] instanceof Array){
            var listeners = this._listenersOnce[type];
            for (var i=0, len=listeners.length; i < len; i++){
                if (listeners[i] === listener){
                    listeners.splice(i, 1);
                    break;
                }
            }
        }
    }
};
*/
function DataCollection(a, d)
{
	//EventTarget.call(this);
	$.extend(this,
	{
		collection:{},
		localData:{},
		definition:[]
	});
		
	
	for(var key in a)
	{
		this.localData[key] = false;
		if(parseFloat(key) < 0)
		{
			this.localData[key] = true;
		}
	}
	
	$.extend(this.collection, a);
	$.extend(true, this.definition, d);
}

//DataCollection.prototype = new EventTarget();
$.extend(DataCollection.prototype, new EventTarget());
DataCollection.prototype.constructor = DataCollection;
$.extend(DataCollection.prototype,
{
	length:function()
	{
		var length = 0;
		for(var key in this.collection)
		{
			length = length +1;
		}
		return length;
	},
	
	//bLocal [true|false] indica que los datos que se estan agregando son locales, que aun no se guardan en el server
	// si se pone False, los datos ya estan guardados en el server o no son necesario mandalos a guardar.
	// si se pone true, los datos pueden mandarse a guardar al server
	add:function(data, bLocal)
	{
		for(var key in data){
			var bGlobalChange = false;
			if(this.collection[key] && (this.hasEvent('change'))){
				for(var field in this.collection[key]){
					switch(typeof this.collection[key][field]){
						case 'object':
							if(this.collection[key][field] == null){
								if(!(data[key][field] == "" || data[key][field] == null))
									bGlobalChange = true;
							}else{
								if(typeof data[key][field] == 'string' || (typeof data[key][field] == 'number')){
									if( data[key][field] != 0 && !(data[key][field] in this.collection[key][field] && Object.keys(this.collection[key][field]).length == 1))
										bGlobalChange = true;
								}else{
									//ambos son objetos, comparamos llaves
									//primero tamaños
									if(Object.keys(data[key][field]).length > Object.keys(this.collection[key][field]).length){
										for(var ki in data[key][field]){
											if(!this.collection[key][field][ki])
												bGlobalChange = true;
										}
									}else{
										for(var ki in this.collection[key][field]){
											if(!data[key][field][ki])
												bGlobalChange = true;
										}
									}
								}
							}							
							break;
						default:
							if(data[key][field]!=this.collection[key][field]){
								bGlobalChange = true;
							}
					}
				}
				if(bGlobalChange){
					this.trigger('change', {'key':key, item:data[key]})
				}
			}else{
				if(this.hasEvent('add'))
					this.trigger("add", {'key':key, item:data[key]});
			}
			this.collection[key] = $.extend({}, data[key]);
		}
		//$.extend(this.collection, data);
		
		for(var key in this.collection)
		{
			this.localData[key] = false;
			if(parseFloat(key) < 0)
			{
				this.localData[key] = true;
			}
		}
		
		var id = this.definition[0].name;
		for(var key in data)
		{
			this.localData[key] = bLocal;		
		}
	},
	
	getLastID:function()
	{
		var lastID = null;
		var id = this.definition[0].name;
		for(key in this.collection)
		{
			if(this.collection[key][id] >= lastID)
			{
				lastID = this.collection[key][id];
			}
		}
		
		return lastID;
	},
	
	//Obtiene el elemento con el Id especificado (no necesariamente corresponde con un consecutivo de 0 hasta length)
	getItem:function(id)
	{
		return this.collection[id];
	},
	
	//Obtiene el elemento en la posición especificada (en este caso siempre es un consecutivo de 0 hasta length)
	item:function(index)
	{
		iIdx = 0;
		aObject = undefined;
		for (var key in this.collection)
		{
			if (iIdx == index)
			{
				aObject = this.collection[key]
				break;
			}
		}
		
		return aObject;
	},
	
	getLocalRecords:function()
	{
		var id = this.definition[0].name;
		var newData = {};
		var key;
		
		for (key in this.collection)
		{
			//if(this.localData[key] == true)
			if((parseInt(key) < 0 || this.localData[key] == true))
			{
				newData[key] = new Object();
				$.extend(newData[key], this.collection[key]);
			}
		}
		
		return newData;
	},
	
	getRemoteRecords:function()
	{
		var id = this.definition[0].name;
		var newData = {};
		var key;
		
		for (key in this.collection)
		{
			if(this.localData[key] == false)
			{
				newData[key] = new Object();
				$.extend(newData[key], this.collection[key]);
			}
		}
		
		return newData;
	},
	
	newID:function()
	{
		if(bitamApp.isMobile.isApp()) {
			var lastID = localStorage["bitamApp.uniquekey"] || 0;
			localStorage["bitamApp.uniquekey"] = --lastID;
		} else {
			var lastID = 0;
			var id = this.definition[0].name;	
			
			for(key in this.collection)
			{
				if(parseFloat(this.collection[key][id]) < parseFloat(lastID))
				{
					lastID = this.collection[key][id];
				}
			}
			lastID = --lastID;
		}

		return lastID;
	},
	
	getIDs:function()
	{
		var id = this.definition[0].name;
		var ids = new Array();
		for(key in this.collection)
		{
			ids.push(this.collection[key][id]);
		}
		
		return ids;
	},
	
	filter:function(columns, filter)
	{
		if($.isArray(columns))				
		{
			//if(!$.isPlainObject(filter))
			if(!$.isPlainObject(filter) && typeof filter != 'string')
			{
				filter = '';
			}			
		}else
		{		
			if(!isset(filter))
			{
				filter = columns;
				columns = null;
			}
		}

		if($.isEmptyObject(filter))	
		{
			filter = '';
		}
		
		if($.isEmptyObject(this.collection))
		{
			if($.isArray(columns) && columns.length>0)
			{
				return [];
			}
			return {};
		}
		
		return Worker_Filter({"filter":filter, "collection":this.collection, "columns":columns});
	},
	
	sort:function()
	{
		var arrSort = new Array();
		for(var key in this.collection)
		{
			arrSort.push(this.collection[key]);
		}
		
		tfObjSort.init(arrSort);
		
		return arrSort.objSort(arguments)		
	},
	
	deleteItem:function(id)
	{
		delete this.collection[id];
		delete this.localData[id];
	},
	//recibe un array con los ids a remover
	removeItems:function(col){
		for(var key in col){
			delete this.collection[col[key]];
			delete this.localData[col[key]];
		}
	},	
	isLocalData:function(id)
	{
		return this.localData[id];
	}
});

function isset(variable_name) {
try {
	if(typeof variable_name == 'string')
	{
		if(variable_name != '')
		{
			return true;
		}
		return false;
	}
	else
	{
     	if (typeof(eval(variable_name)) != 'undefined')
     	{
     		if (eval(variable_name) != null)
 			    return true;
     	}
	}
 } catch(e) { }
return false;
}

function validateEmail(email)
{
	var filter=/^((\"[\w-\s]+\")|([\w-]+(?:\.[\w-]+)*)|(\"[\w-\s]+\")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
			
	if (filter.test(email))
		return true;
	
	return false;
}

function formatString(value, format, type)
{
	switch(type){
		case "date":
				value = Date.parse(value).toString(format);
				break;
		case "text":
				var re = new RegExp(/\[.+\]/)
		 		var m = re.exec(format);
		 		if(m){
		 			var match = m[0];
		 			match = match.replace('[', '');
		 			match = match.replace(']', '');
		 			value = format.replace(m[0], value);
		 		}
		 		break;							
		 case "numeric":
			var string = value;
			if(format == ""){
				string = $('<input/>', {value:value}).autoNumeric('init', {aSep:''}).val();
			}else{
				var cDespuesdelpunto = 0, aSign = '', pSign = 'p';
				if(format.indexOf(".") != "-1"){
					cDespuesdelpunto  = format.split(".")[1].replace("%","").replace("$","").replace("","").length;
				}				
				if(format.indexOf("$") != "-1"){
					aSign = '$';
					if(format.indexOf("$")>0)
					    pSign = 's';
				}else if(format.indexOf("") != "-1"){
					aSign = '';
					if(format.indexOf("")>0)
					    pSign = 's';
				}
				else if(format.indexOf("%") != "-1"){
					aSign = '%';
					if(format.indexOf("%")>0)
					    pSign = 's';
				}
                string = $('<input/>', {value:value}).autoNumeric('init', {aSep: ',', aDec: '.', aSign:aSign,pSign:pSign,mDec:''+cDespuesdelpunto}).val();
			}
			return string;
	}	
	return value;
}

function replaceIdsString(anstring, objValue){
	return anstring.replace(/\[(FFRMS_[0-9]+)@(FRM_[0-9]+)\]/g, function(t,field,form,c,d){
		if(objValue[form]){
			for(var key in objValue[form].payload.fieldsForm){
				if(objValue[form].payload.fieldsForm[key].id == field){
					return "["+objValue[form].payload.fieldsForm[key].label+"@"+objValue[form].sectionName+"]";
				}
			}
		}
		return "";
	});
}
//@params
//bReturnObject => regresa el objeto en lugar de el string
function replaceLabelString(anstring, objValue, bReturnObject){
	if(bReturnObject == undefined)
		bReturnObject = false;
	//return anstring.replace(/\[([A-Za-z0-9_]+)@([A-Za-z0-9_]+)\]/g, function(t,field,form,c,d){
	var aForm = null;
	var returnObject = false;
	var sResult = '';
	sResult = anstring.replace(/\[([A-Za-z0-9_\/\-%() ]+)@([A-Za-z0-9_\/\-%() ]+)\]/g, function(t,field,form,c,d){
		if(!aForm || aForm.sectionName !=  form)
			aForm = bitamApp.application.getSectionWithName(form);
		if(aForm && objValue[aForm.name]){
			for(var key in aForm.payload.fieldsForm){
				if(aForm.payload.fieldsForm[key].label == field && objValue[aForm.name][aForm.payload.fieldsForm[key].id]){
					if(bReturnObject == false && $.isPlainObject(objValue[aForm.name][aForm.payload.fieldsForm[key].id])){
						var sValue = [];
						for(var s in objValue[aForm.name][aForm.payload.fieldsForm[key].id]){
							sValue.push(objValue[aForm.name][aForm.payload.fieldsForm[key].id][s]);
						}
						return sValue.join(",");
					}
					returnObject = objValue[aForm.name][aForm.payload.fieldsForm[key].id];
					//return objValue[aForm.name][aForm.payload.fieldsForm[key].id];
				}
			}
		}
		return "";
	});
	if(bReturnObject && returnObject !== false){
		return returnObject;
	}
	return sResult;
}

function replaceValueString(anstring, objValue)
{
	//var re = new RegExp(/\[[A-Za-z_]*\]/g)
	var re = new RegExp(/\[[A-Za-z_0-9]*\]/g)	
	/*var m = re.exec(anstring);
	if(m)
	{
		var match = m[0];
		match = match.replace('[', '');
		match = match.replace(']', '');
		if(isset( objValue[match]))
		{
			do {
				anstring = anstring.replace(m[0], objValue[match]);
			} while(anstring.indexOf(m[0]) >= 0);
		}
	}*/
	
	var arrMatch = anstring.match(re)
	if($.isArray(arrMatch))
	{
		for(var key = 0; key<arrMatch.length; key++)
		{
			var match = arrMatch[key];
			match = match.replace('[', '');
			match = match.replace(']', '');
			//if(isset( objValue[match]))
			if(match in objValue)
			{
				do {
					//si objValue[match] es un objeto (llave->descriptor) usamos el descriptor
					valMatch = objValue[match];
					if($.isPlainObject(valMatch)){
						for(var keyM in valMatch)break;
						valMatch = valMatch[keyM];
					}
					anstring = anstring.replace(arrMatch[key], valMatch);
				} while(anstring.indexOf(arrMatch[key]) >= 0);
			}
		}
	}
	return anstring;
}
//24/05/2013@aramirez
//useDirectObject - Si es true, regresa directamente el objeto
function dataCatalog(object, label, value, fn, bArray)
{           
	if(typeof object.useDirectObject == 'boolean'){
		if(object.useDirectObject)
			return object
		object = object.data;
	}	
	if(!isset(bArray))
	{
		bArray = true;
	}
	if(bArray)
		var availableTags = new Array();
	else
		var availableTags = {};
		
	if(!isset(value))
		value = label;	
	
	for(var key in object)
	{
		var eachObject = object[key];
		
		if($.isFunction(fn))
		{
			if(bArray)
			{
				var item = fn(eachObject);
				if(item)
					availableTags.push(item);
			}						
			else
			{
				var data = fn(eachObject);
				if(data)
					availableTags[data.value] = data;
			}
		}
		else
		{
			if($.isPlainObject(object[key]))
			{
				if(bArray)
					availableTags.push({label:object[key][label], value:object[key][value]});	
				else
				{
					availableTags[object[key][value]] = {label:object[key][label], value:object[key][value]};
				}
			}
			else
			{
				if(bArray)
					availableTags.push({label:object[key], value:object[key]});	
				else
				{
					availableTags[object[key][value]] = {label:object[key], value:object[key]};
				}
			}		
		}	
	}
	
	return availableTags;
}

function replaceData(string, data)
{
	var re = new RegExp(/\[.+\]/)
	var m = re.exec(string);
	if(m)
	{
		var match = m[0];
		match = match.replace('[', '');
		match = match.replace(']', '');
		string = string.replace(m[0], data[match]);
	}
	
	return string;
}

$.extend(bitamApp, {session:Session.data});

//el primer parametro siempre sera el objeto a ordenar y los demas seran las columnas/llaves del objeto por el cual se ordenara.
//ej: ObjectSort(Objeto, "columna1", -1, "columna2", 1);
ObjectSort = function()
{
	var arrSort = new Array();
	for(var key in arguments[0])
	{
		arrSort.push(arguments[0][key]);
	}
		
	tfObjSort.init(arrSort);
	//se elimina el parametro 1 que es el objeto, solo se le manda los demas parametros
	delete arguments[0];
	return arrSort.objSort(arguments)		
}

//Objeto para depuración del proceso
Debugger = {
	debugging : false,					//Poner en true para registrar todas las llamadas a funciones y demás mensajes del log
	callStackSize : 10,					//Indica cuantas llamadas a funciones serán mantenidas en el array a la vez antes de comenzar a eliminar las mas viejas
	lastFnName : [],					//Contiene la última función invocada
	debuggerlogStackSize: 50,			//Controla cuentos registros del log de depuración se tienen que acumular antes de escribir en archivo
	debuggerLogStack: [],				//Contiene las entradas del log mas viejas para ser escritas cuando llega al límite del stack
	lastDebuggerLogs : [],				//Contiene las últimas entradas del log de depuración
	
	init : function (debugging,debuggerlogActive) {
		this.debugging = debugging;
	},
	
	//Registra el nombre de la última función invocada y si se tiene habilitado, la registra en el log de procesos
	register: function (aFnName) {
		if (this.callStackSize <= 0) {
			return;
		}
		
		if (this.lastFnName.length > this.callStackSize) {
			this.lastFnName.shift();
		}
		this.lastFnName.push(aFnName);
		
		this.message(aFnName);
	},
	
	devMessage : function (msg, show) {
		show = (show == undefined) ? show : false;
		this.dump(msg,show);
	},	
	
	message : function (msg, show) {
		show = (show == undefined) ? show : false;
		this.dump(msg,show);
	},	
	
	val : function (msg, val, show) {
		val = (val == undefined) ? '' : val;
		show = (show == undefined) ? show : false;
		var toalert = msg+'= '+val;
		this.dump(toalert,show);
	},
	
	alert : function (msg) {
		alert(msg);
	},
	
	dump : function (msg,show) {
		if(this.debugging) {
			console.log(msg);
			if(show) {
			    if (bitamApp.isWeb()) {
			    	alert(msg);
			    }
			    else {
					navigator.notification.alert(msg);
			    }
			}
		}
	},
}
