if(typeof Section != 'function') {
	function Section(aSection){
		$.extend(this, aSection);
		for(var key in this.payload.views){
			this.payload.views[key].section = this;
			this.payload.views[key] = new View(this.payload.views[key]);
		}
		for(var key in this.payload.fieldsForm){
			this.payload.fieldsForm[key] = new FieldForm(this.payload.fieldsForm[key]);
		}
		return this;
	}
}

$.extend(Section.prototype, {
	getFieldsForm: function(){
		var res = [];
		for(var key in this.payload.fieldsForm){
			res.push(new FieldForm(this.payload.fieldsForm[key]))
		}
		return res;
	},
	getFieldFormWithId: function(id){
		for(var key in this.payload.fieldsForm){
			if(this.payload.fieldsForm[key].id == id){
				//return this.payload.fieldsForm[key];
				return new FieldForm(this.payload.fieldsForm[key]);
			}
		}
		return false;
	},
	getFieldFormWithIdByParents: function(id){
		var aFieldForm = false, aSection = null;
		var arrParents = this.parents();
		for(var i=0; i<arrParents.length&&aFieldForm==false; i++){
			aSection = bitamApp.application.getSection(arrParents[i].sectionName);
			if(aSection){
				aFieldForm = aSection.getFieldFormWithId(id);
			}
		}
		return aFieldForm;
	},
	getParentWithName: function(sName){
		for(var key in this.payload.infoMasterSections){
			if(this.payload.infoMasterSections[key].sectionName == sName){
				return this.payload.infoMasterSections[key];
			}
		}
		return false;
	},
	getProcessWithID: function(aKey){
		for(var key in this.payload.processForms){
			if(this.payload.processForms[key].id_process == aKey){
				return new Process(this.payload.processForms[key]);
			}
		}
		return false;
	},
	getProcessInitial: function(){
		if($.isArray(this.payload.processForms) && this.payload.processForms.length)
			return new Process(this.payload.processForms[0]);
		return false;
	},
	parents: function(){
		if(this.payload.infoMasterSections)
			return this.payload.infoMasterSections;
		else
			return [];
	},
	details: function(){
		if(this.sections)
			return this.sections;
		else
			return [];
	},
	//regresa el nombre del campo que sirve como llave de la forma
	getFieldKey: function(){
		return this.payload.fieldsData[0].name;
	},
	getViewWithID: function(id){
		if(!this.payload.views[id])
			return null;
		this.payload.views[id].sectionName = this.name;
		return new View(this.payload.views[id]);
	},
	//regresa en un array los nombres de los campos que son campos llave en la seccion/formulario
	getArrFieldsNameKey: function(){
		var arrFields = [];
		for(var key in this.payload.fieldsForm){
			if($.boolean(this.payload.fieldsForm[key].keyfield)){
				arrFields.push(this.payload.fieldsForm[key].id);
			}
		}
		return arrFields;
	},
	getArrFieldsNameRequire: function(){
		var arrFields = [];
		for(var key in this.payload.fieldsForm){
			if(this.payload.fieldsForm[key].valid && $.boolean(this.payload.fieldsForm[key].valid.required)){
				arrFields.push(this.payload.fieldsForm[key].id);
			}
		}
		return arrFields;
	}
});