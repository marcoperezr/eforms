(function($){

	$.widget("bitam.selectHierarchy", {
		options: {
			
		},

		_create: function(){
			var self = this,
				options = self.options;
			var nFields = options.fields.length;
			for(var i=0; i<nFields; i++){
				options.fields[i];
				var $el = self.element.clone();
				$el.data('field', options.fields[i]);
				$el.attr('name', options.fields[i]);
				$el.attr('id', '');
				$el.data('level', i);
				if(i>0){
					$el.data('prevfield', options.fields[i-1])
				}
				$el.insertBefore( self.element );
				$el.select2({
					minimumInputLength: 0,
					query: function(query){
						var sField = this.element.data('field');
						var sPrevField = this.element.data('prevfield');
						var n = this.element.data('level');
						options.query(function(resData){
							res = {results:[]};
							var data = $.extend(true, {}, resData);
							var uniqueDesc = {};
							//filtramos el objeto
							if(sPrevField){
								var valuePrevField = $("[name='"+sPrevField+"']").val();
								data.results = $.map(data.results, function(item, i){
									if(item[sPrevField].toUpperCase() == valuePrevField.toUpperCase())
										return item;
								})
							}
							for(var key in data.results){
								//eliminamos las descripciones duplicadas
								//uniqueDesc[data.results[key][sField]] = data.results[key][sField];								
								uniqueDesc[data.results[key][sField]] = data.results[key][options.fieldId];
							}
							for(var key in uniqueDesc){
								if(n+1 == options.fields.length){
									res.results.push({id:uniqueDesc[key], text:key});
								}else{
									res.results.push({id:key, text:key});
								}
							}
							query.callback(res);
						})
					}
				});
				$el.on('change', function(){
					var n = $(this).data('level');
					for(var i = n+1; i < options.fields.length; i++){
						$("[name='"+options.fields[i]+"']").select2('data', {})
					}
					if(n+1 == options.fields.length){
						self.element.val($(this).val());
					}
				})
			}
		},
				
		val: function(val){
			for(var i = 0; i < this.options.fields.length; i++){
				$("[name='"+this.options.fields[i]+"']").select2('data', {id:val[this.options.fields[i]],text:val[this.options.fields[i]]});
			}
		},
		
		disable: function(val){
			for(var i = 0; i < this.options.fields.length; i++){
				$("[name='"+this.options.fields[i]+"']").select2('disable');
			}
		},
		
		destroy: function(){
			var self = this;
			$.Widget.prototype.destroy.call( self );
		},

		_setOption: function(option, value){
			
		}

	});
	
	$.widget("bitam.checkbox", {
		options: {
			
		},

		_create: function(){
			var self = this;
			var options = self.options;
			self.idContainerCheckboxGroup = $("<div/>").uniqueId().attr('id');
			if(options.query){
				options.query({
					callback:function(res){
						var bFirst = true;
						self.element.wrap('<div id="'+self.idContainerCheckboxGroup+'" class="checkbox-group '+(options.vertical?'checkboxgroup vertical':'')+'" style="'+(options.modal? 'display:none':'')+'"/>');
						self.element.find('span').html('');
						for(var key in res.results){
							if(bFirst){
								self.element.find('span').html(res.results[key].text);
								self.element.find('input').val(res.results[key]['id']);
								self.element.find('input').attr('data-jquerywidget', 'checkbox').attr('data-text', res.results[key].text).attr('data-description', res.results[key].text);
								if($.isPlainObject(self.options.value)) {
									
									if(res.results[0].id == (self.element.find('input').attr('name') + '|q1')) {
										for(var oKey in self.options.value) {
											if(self.options.value[oKey] == res.results[key].text) {
												//self.element.find('input').attr('checked',self.options.value[oKey]);
												self.element.find('input').prop('checked',true);												
											}
										}
									} else {
										self.element.find('input').prop('checked', self.options.value[res.results[key]['id']] != undefined);
									}
									
									//self.element.find('input').attr('checked', self.options.value[res.results[key]['id']] != undefined)
								}
								for(var key in res.events){
									self.element.find('input').on('change', res.events[key]);
								}
							}else{
								var inputCheck;
								var aInputCheck = $('<label/>').append(
									inputCheck = $('<input/>', {type:'checkbox', value:res.results[key]['id'], 'name':self.element.find('input').attr('name'), 'data-role':'none'}),
									$('<span/>').html(res.results[key].text)
								)
								inputCheck.attr('data-text', res.results[key].text).attr('data-description', res.results[key].text).attr('data-jquerywidget', 'checkbox');
								if(options.modal){
									aInputCheck = $("<div/>", {'class':'item-checkbox'}).append(aInputCheck);
								}							
								//aInputCheck.insertAfter(self.element);
								$('#'+self.idContainerCheckboxGroup).append(aInputCheck);
								if($.isPlainObject(self.options.value)){
									if(res.results[0].id == (inputCheck.attr('name') + '|q1')) {
										for(var oKey in self.options.value) {
											if(self.options.value[oKey] == res.results[key].text) {
												inputCheck.attr('checked',self.options.value[oKey]);
											}
										}
									} else {
									inputCheck.attr('checked', self.options.value[res.results[key]['id']] != undefined)
								}
							}						
								for(var key in res.events){
									inputCheck.on('change', res.events[key]);
								}
							}						
							bFirst = false;
						}
						
						if(options.modal){
							var bttn = $('<button data-inline="true" data-theme="g">'+$.t('Change')+'</button>');
							self.element.wrap("<div class='item-checkbox' />");
							self.element.parent().parent().parent().append(bttn.on('click', function(){
								var myWindowModal = $("#windowModal_widget");
								if(myWindowModal.length > 0){
									myWindowModal.remove();
								}
								var myWindowModal = $("<div/>", {"id":"windowModal_widget", "class":"ui-body-c"});
								var containerCheckBox = $('#'+self.idContainerCheckboxGroup).replaceWith($("<div id='"+self.idContainerCheckboxGroup+"_cpy'/>"));
								myWindowModal.append(containerCheckBox.show());
								$('body').append(myWindowModal);							
								width = (options.width ? options.width : 560); //690			
								myWindowModal.dialogWidget({
									title: options.title, 
									minHeight: 250, 
									minWidth: width, 
									height: 350,
									modal: true,
									buttons: {
										"Save": function() {
											$('#'+self.idContainerCheckboxGroup+"_cpy").replaceWith(containerCheckBox.hide());
											$( this ).dialogWidget( "close" );
										},
										Cancel: function() {
											$('#'+self.idContainerCheckboxGroup+"_cpy").replaceWith(containerCheckBox.hide());
											$( this ).dialogWidget( "close" );
										}
									}
								});
								
								return false;
							}));
						}
					}
				})
			}else{
				self.element.wrap('<div role="checkbox" aria-checked="'+(self.element.prop('checked') ? 'true':'false')+'" class="chkbox-box" style="float:left;margin-top: 2px;"><div class="chkbox-box-inner '+(self.element.prop('checked') ? 'checked':'')+'"></div></div>');
				self.element.on('click', function(){
					if(this.checked){
						$(this).parent().addClass('checked')
						$(this).parent().parent().attr('aria-checked', 'true');
					}else{
						$(this).parent().removeClass('checked')
						$(this).parent().parent().attr('aria-checked', 'false');
					}
				})
				self.element.css({
					/*'visibility': 'hidden',*/
					'opacity': '0',
					'position': 'absolute',
					'top': '-5px',
					'left': '-1px'
				})
			}
		},
		//aramirez@22/01/2013
		//Este metodo no debe lanzar el evento change/click, si se desea debemos implementar un metodo aparte
		checked: function(aBoolean){
			if(typeof aBoolean != "boolean")
				aBoolean = this.element.prop('checked');
			if(aBoolean){
				this.element.prop('checked', true);
				this.element.parent().addClass('checked');
				this.element.parent().parent().attr('aria-checked', 'true');
			}else{
				this.element.prop('checked', false);
				this.element.parent().removeClass('checked');
				this.element.parent().parent().attr('aria-checked', 'false');
			}
		},
		
		disabled: function(aBoolean){
			this.element.parent().parent().addClass('chkbox-box-disable');
			this.element.attr('disabled','disabled');
		},
		
		enable: function(aBoolean){
			this.element.parent().parent().removeClass('chkbox-box-disable');
			this.element.removeAttr('disabled');
		},
		
		val: function(value){
			if(value==undefined){
				var res = [];
				var aCheckboxs = $('#'+this.idContainerCheckboxGroup + " input:checked");
				if(aCheckboxs.length>1){
					res = [];
					for(var i=0; i<aCheckboxs.length; i++){
						res.push({id:$(aCheckboxs[i]).val(), text:$(aCheckboxs[i]).data('text')});
					}
				}else{
					if(aCheckboxs.length==1)
						res = {id:aCheckboxs.val(), text:aCheckboxs.data('text')}
				}				
				return res;
			}else{
				this.options.value = value;
			}
		}
	});
	
	$.widget("bitam.radiobutton", {
		options: {
			
		},
		
		_create: function(){
			var self = this;
			var options = self.options;
			if($.isPlainObject(options.value)){
				this.val(options.value);
			}
			self.idContainerCheckboxGroup = $("<div/>").uniqueId().attr('id');
			options.query({
				callback:function(res){
					var bFirst = true;
					self.element.wrap('<div id="'+self.idContainerCheckboxGroup+'" class="radiobuttongroup '+(options.vertical ? 'vertical' : '')+'"/>');
					self.element.find('span').html('');
					for(var key in res.results){
						var inputCheck;
						if(bFirst){
							self.element.find('span').html(res.results[key].text);
							inputCheck = self.element.find('input').val(res.results[key]['id']).data('text', res.results[key].text).data('jquerywidget', 'radiobutton');
							inputCheck.attr('data-description', res.results[key].text);
						}else{
							$('<label/>').append(
								inputCheck = $('<input/>', {type:'radio', value:res.results[key]['id'], 'data-description':res.results[key].text, 'name':self.element.find('input').attr('name'), 'data-role':'none'}),
								$('<span/>').html(res.results[key].text)
							).insertAfter(self.element);
							inputCheck.data('text', res.results[key].text).data('jquerywidget', 'radiobutton');
						}
						if($.isPlainObject(self.options.value)){
							if(self.options.arrValue[res.results[key]['text']] != undefined)
								inputCheck.prop('checked', true);
						}else if (self.options.value == res.results[key].text){
							inputCheck.prop('checked', true);
						}
						bFirst = false;
					}
					for(var key in res.events){
						self.element.parent().find('input').on('change', res.events[key]);
					}
				}
			})
		},
		
		val: function(value){
			
			if(value==undefined){
				var res = [];
				var aCheckboxs = $('#'+this.idContainerCheckboxGroup + " input:checked");
				if(aCheckboxs.length==1){
					res = {id:aCheckboxs.val(), text:aCheckboxs.data('text')}
				}
				return res;
			}else{
				this.options.value = value;
				this.options.arrValue = {};
				for(var key in value){
					this.options.arrValue[value[key]] = key;
				}
			}
		}
	});
	
	$.widget("bitam.FileUploader", {
		options: {
			
		},
		
		_create: function(){
			var self = this,
			options = self.options,
			$el = self.element,
			sName = $el.attr('name'),
			sValue = $el.val();
			
			var display = 'none';
			var photovalue = '';
			if(options.type == 'widget_photo') {
				var display = 'block';
				if(sValue) {
					var photovalue = '../../'+sValue;
				}
			}
			
			$el.wrap($("<div/>",{'id':sName + "_file_container","class":"ui-link"})).before(
				$('<div/>', {'id':sName + "_FileUploader_container"}),
				$('<img/>', {'id':sName + "_img", "name":sName + "_img", "style":'-webkit-border-radius:5px;padding:4px;border:1px solid gray;width:150px;height:140px;margin-top:5px;display:'+display+';', 'src':photovalue }),
				$("<ul/>", {
					id:sName + "_file_list", 
					name:sName + "_file_list",
					"data-field":sName
				})
			);
			new qq.FileUploader({
				type: options.type,
				allowedExtensions: options.allowedExtensions,
				multiple: options.multiple,
				element: $el.parents().find('#' + sName + "_FileUploader_container")[0],
				listElement: $el.parents().find('#' + sName + "_file_list")[0],
				action: options.action,
				params: options.params,
				onSubmit: options.onSubmit,
				onComplete: options.onComplete,
				onCancel: options.onCancel,
				onDelete: options.onDelete
			});
			if($.trim(sValue) != ''){
				options.aFiles = sValue.split(",");				
				var sLink = options.urlFiles;
				
				if(bitamApp.isMobile.isApp()){
					var sLinkDocument = bitamApp.buildURL("uploads//"+bitamApp.application.codeName+"//"+options.params.sectionName);
				}
				
				for(var key in options.aFiles){
					var Document= options.aFiles[key].split("/");  
					
					var ahref = '';
					if(bitamApp.isMobile.isApp()){
						sLinkDocument += "//"+Document[3];
						href = 'javascript: bitamApp.showModal(\"'+sLinkDocument+'\", \"'+options.type+'\")';
					} else {
						href = sLink+"&file="+Document[3];
					}
	
                    var LinkName = Document[3].substring(8, Document[3].length);
					var myLI = $("<li/>", {"id":"table_"+sName+"_tr_"+key}).append(
						//$("<a/>", {"data-ajax":"false", "href":sLink+"&file="+Document[3],"text":LinkName}).click({sName:sName},function(event){
						$("<a/>", {"data-ajax":"false", "href":href, "text":LinkName}).click({sName:sName},function(event){
                                                    var fileContainer = $('#'+sName+'_file_container input:file');
                                                    if(isset(fileContainer.attr('disabled')))
                                                    {
                                                        event.preventDefault();
                                                    }
                                                }),
						$("<span/>", {"style":"margin-left: 5px;margin-right: 5px;"}),
						$("<a/>", {"href":"javascritp:;","text":"Delete"}).on('click',{name:sName,dat:key,com:options.aFiles}, function(event){
                                                    var fileContainer = $('#'+sName+'_file_container input:file');
                                                    if(isset(fileContainer.attr('disabled')))
                                                    {
                                                         return;
                                                    }
                                                  $("#table_"+event.data.name+"_tr_"+event.data.dat).remove();
							delete event.data.com[event.data.dat];
							var sDocuments = '';
							for(var key1 in event.data.com){
								if(sDocuments == '')
									sDocuments = sDocuments + options.aFiles[key1];
								else
									sDocuments = sDocuments + ',' + options.aFiles[key1];
							}
							$el.val(sDocuments);
                                                } )
					);
                    $('#'+sName + "_FileUploader_container").find(".qq-upload-list").append(myLI);
					if(self.options.type == 'widget_photo') {
						$('#'+sName + "_FileUploader_container").find(".qq-upload-list").css('display','none');
					}
				}
			}
		}
	});
	
	//@MABH20120125
	$.widget("bitam.numeric", {
		
		inputText: {},
		format: '',
		
		options: {
			
		},
		
		_create: function() {
			var self = this;
			var options = self.options,
			options = self.options,
			sValue = self.element.val();
			var selfElement = self.element;
			if(options.format) {				
				var $el = $('<input/>', {type:'text'});
				this.inputText = $el;
				this.format = options.format;
				$el.insertBefore(self.element);
				self.element.css('display','none');
				$el.css('display','block');
				var cDespuesdelpunto = 0, aSign = '', pSign = 'p';
				if(options.format.indexOf(".") != "-1"){
					cDespuesdelpunto  = options.format.split(".")[1].replace("%","").replace("$","").replace("�","").length;
				}				
				if(options.format.indexOf("$") != "-1"){
					aSign = '$';
					if(options.format.indexOf("$")>0)
					    pSign = 's';
				}else if(options.format.indexOf("�") != "-1"){
					aSign = '�';
					if(options.format.indexOf("�")>0)
					    pSign = 's';
				}
				else if(options.format.indexOf("%") != "-1"){
					aSign = '%';
					if(options.format.indexOf("%")>0)
					    pSign = 's';
				}
				$el.autoNumeric({aSep: ',', aDec: '.', aSign:aSign,pSign:pSign,mDec:''+cDespuesdelpunto});
				if(sValue && this.format != '') {
					$el.autoNumeric('set',sValue);
				}
				
				$el.on('blur', function(){
					//var unformatedNumber = unformatNumber($el.val(), options.format);
					if($el.val()) {
						var tmpval = $el.autoNumeric('get');
						self.element.val(tmpval);
						self.element.change();
					}
					//$el.css('display','none');
					//self.element.css('display','block');
				});
				
				$el.on('focus', function(){
					//var unformatedNumber = unformatNumber($el.val(), options.format);
					//obtenemos el numero sin formato
					/*
					if($el.val()) {
						self.element.val($el.autonumeric('get'));
					}
					*/
					//$el.css('display','none');
					//self.element.css('display','block');
				});
				$el.on('change', function(){
					//var unformatedNumber = unformatNumber($el.val(), options.format);
					//obtenemos el numero sin formato
					/*
					if($el.val()) {
						self.element.val($el.autonumeric('get'));
					}
					*/
					if($el.val()) {
						var tmpval = $el.autoNumeric('get');
						self.element.val(tmpval);
						self.element.change();
					}
					//$el.css('display','none');
					//self.element.css('display','block');
				});
				self.element.on('blur', function() {
					//self.element.css('display','none');
					//$el.css('display','block');
					//var dataShow = formatString(self.element.val(), options.format, 'numeric');
					//$el.val(dataShow);
				});
				self.element.on('change', function() {
					if(self.element.val()) {
						//var tmpval = $el.autoNumeric('get');
						$el.autoNumeric('set', self.element.val());
						//var tmpval = $el.autoNumeric('get');
						//self.element.val(tmpval);
					}
					//self.element.css('display','none');
					//$el.css('display','block');
					//var dataShow = formatString(self.element.val(), options.format, 'numeric');
					//$el.val(dataShow);
				});
			}
		},
		
		set: function(value) {
			this.options.value = value;
			if(this.format) {
				this.inputText.autoNumeric('set',value);
				this.element.val(value);
			} else {
				this.element.val(value);
			}
			this.element.change();
			this.options.arrValue = {};
			for(var key in value){
				this.options.arrValue[value[key]] = key;
			}
		},
		
		readonly: function(bReadOnly) {
			var self = this;
			var selfElement = self.element;
			if(bReadOnly != undefined && bReadOnly == false) {
				selfElement.prop('readonly', false);
				if(this.format != '') {
					this.inputText.prop('readonly', false);
				}
			} else {
				selfElement.prop('readonly', true);
				if(this.format != '') {
					this.inputText.prop('readonly', true);
				}
			}
		}
	});
	
	$.widget("bitam.table", {
		options: {
			pageSize : 20,
			totalRows : 0,
			totalPages : 0,
			currentPage : 1,
			//HADR 2013-10-10: GRID - Se agrega arreglo para almacenar los fieldForms utilizados para el validador
			arrayFieldsFormValidator : {},
			formValidator : undefined,
			//2014-08-15@ARMZ: Numero de caracter antes de cortar y mostrar el boton de 'leer mas'
			readMoreMinLength: 15,
			//2014-08-15@ARMZ: Permite colocar en el toolbar el boton para construir vistas personalizadas
			canBuildCustomView: true
			//HADR
		},
		
		_create: function() {
			var self = this, myDivForm, $table, $head;
			/*
			02/10/2013@ARAMIREZ
			Por default, la vista simplificada no se puede entrar a ver el registro ni eliminarlo.
			*/
			if(this.options.howtowatch == 3) {
				self.options.canView = false;
				self.options.canDelete = false;
			}
			//HADR 2013-10-04: GRID - Ocultar opciones de inserci�n y eliminaci�n
			if(this.options.howtowatch == 4){
				self.options.canAdd = false;
				self.options.canDelete = false;
				self.options.canImportExcel = false;
				//Opciones de paginacion
				self.options.pageSize = 100;
				self.options.totalRows = 100;
				self.options.totalPages = 1;
				self.options.currentPage = 1;
			}
			//HADR
			//Guardamos el codeName de la vista original
			self.options.viewNameOriginal = self.options.viewNameOriginal || self.options.sectionView;
			
			//20/06/2013@aramirez
			//self._filter, guarda el filtro a aplicar a la vista, es un filtro temporal.
			self._filter = self.options.filterApplied||{};
			if($.isPlainObject(self._filter)){
				self._filter = BitamStorage.webDB.buildFilterWithObject({filter:self._filter,sectionName:self.options.section.name})
			}
			self.options.filterApplied = self._filter;
			
			this._buildSecurity();
			this._checkActionFormsInHeader();
			//2013-11-26@RTORRES:(#iphoneUse)Le adhiere la clase resposive par que no se vuelta a contruir la vista pero a las vistas detlle no.
			if(self.options.howtowatch == 2  || bitamApp.isMobile.pantallasreducidas() /*&& !bitamApp.isMobile.pantallasreducidas()*/){
			    self.tBody = $("<div/>");
			    $table = $("<div/>",{'id':'contentlist_'+self.options.name,'class':'table-view'})
			    $head = "";
			}else{
			    self.tBody = $("<tbody/>");
			    $table = $("<table/>",{'id':'contentlist_'+self.options.name,'class':'table-view'})
				if(bitamApp.isMobile.pantallasreducidas())
				{
					$table.addClass("responsive");
					if(!jQuery.isEmptyObject(self.options.masterSection))
					{
						$table.addClass("child");
						//$(".pinned").css("width", "45% !important");
						
					}
				}
			    $head = this._buildTableHeader();
			}
			self.myDivNorecordFound = $("<div/>", {"id":"norecordsfound_"+self.options.name, "class":"norecords", "style":"display:none; clear:both"}).html("No " + self.options.section.sectionName.toLowerCase()).hide()
			//05/11/2013@rtorres se agrega la bara de botones por debajo de la vista cuando tenga mas de 10 elementos.
			if(!bitamApp.isMobile.pantallasreducidas())
			{
				self.controlFooter  = this._buildControlHeader();
				self.controlFooter.addClass("controlMenuTableFooter");
				self.controlFooter.removeClass("controlMenuTable");
				self.pagBar = this._buildFooter();
				self.pagBar.find("#currentpage_"+ self.options.name).attr("id","currentpageHead_"+ self.options.name);
				self.pagBar.find("#totalpages_"+ self.options.name).attr("id","totalpagesHead_"+ self.options.name);
			}
			this._content().append(
				self.controlHeader = this._buildControlHeader(),
				(!bitamApp.isMobile.pantallasreducidas())? self._buildPaginationInfo()/*self.pagBar*/ : "",
				self.googleMapsView = this._buildGoogleMapsView(),
				$("<div/>",{'class':'imgLoading layerTableView fa fa-cog fa-spin',style:'width: 100%;position: absolute;height: 40px;'}).append($('<div/>', {style:'margin-left: auto;margin-right: auto; width: 32px;margin-top: 10px;margin-bottom: 5px;'}).append(
//					$('<img/>', {src:bitamApp.buildURL('images/loading.gif')})
					$('<i/>', {'class':"icon-spinner icon-spin icon-large", 'style':'margin-top: 3px;'})
				)),
				$("<div/>",{"class":"divTableScroll"}).append($table.append($head, self.tBody)),
				//$table.append($head, self.tBody),
				self.myDivNorecordFound,
				(!bitamApp.isMobile.pantallasreducidas())? self.controlFooter : "",
				self._buildFooterForm(),
				self.footer = this._buildFooter().hide()
			);	
				
				//2013-11-13 @SM Se revisa el tipo de widgets a utilizar
				var self = this, aFieldsForm, arrJQWidgets = [], dfd = $.Deferred();
				aFieldsForm = this.options.section.getFieldsForm();
				for(var i=0; i<aFieldsForm.length; i++){
					arrJQWidgets = arrJQWidgets.concat(Utils.checkJQWidgetFile(aFieldsForm[i].tagname));
				}
				arrJQWidgets = $.array_unique(arrJQWidgets);
				console.debug(arrJQWidgets);
				
				require(arrJQWidgets, function() {
					self._buildPage(0).then(function(){
					//developer - recibe el tbody
					var tBody = self.tBody;
					//2013-11-26@RTORRES:(#iphoneUse)Activa el scroll y el fixed para las cabezeras y pone en accion con el rezise la nueva vista resposive
					if(bitamApp.isMobile.pantallasreducidas())
					{
						$(".btn_showhideboxfilter").trigger( "click" );
					}
					if(self.options.sortable){
						tBody.sortable({
						   update: function(event, ui){
								var  params = tBody.sortable( "serialize");		   				   		
								bitamApp.ajaxRequest({
									url:bitamApp.getURLServer() + '/action.php',
									data:$.param({'applicationCodeName': bitamApp.application.codeName, 
											"action":"sortable", 
											"section": self.options.section.name,
											"sectionView": self.options.sectionView,
											"currentPage":self.options.currentPage,
											"pageSize":self.options.pageSize
										}) + '&' + params,
									success:function(responseData){
										bitamApp.pageLoading(true);
									},
									error: function(XMLHttpRequest, textStatus, errorThrown) {
										bitamApp.pageLoading(true);
										alert(textStatus);
									}				
								});
						   }
						});  
						tBody.disableSelection();
					}
					//HADR 2013-10-07: GRID - Mostrar como editables cada una de las filas de la tabla
					if(self.options.howtowatch == 4){
						tBody.find('tr').each(function(index){
							self.edit($(this).attr('data-id'));
						});
						var idFormValidator = "frm_table_"+self.options.name;
						if(self.element.parent('#'+idFormValidator).length > 0){
							formValidator.resetForm();
							self.element.unwrap("<form id='" + idFormValidator + "' action='javascript: return false;'></form>");
						}
						self.element.wrap("<form id='" + idFormValidator + "' action='javascript: return false;'></form>");
						//Unir los arreglos de fields para realizar la validaci�n de todo el grid
						var arrayFields = new Array();
						for(var arrayFieldId in self.options.arrayFieldsFormValidator){
							arrayFields = arrayFields.concat(self.options.arrayFieldsFormValidator[arrayFieldId]);
						}
						formValidator = self.element.parent('#'+idFormValidator).validateForm(arrayFields, undefined);
						arrayFields = null;
					}
					//HADR
				});	
			});

			this.element.data('jquerywidget', this.widgetName);
		},
		
		_checkActionFormsInHeader: function(){
			var self = this, eachAction;
			for(var i=0; i<self.options.section.payload.actionforms.length; i++){
				eachAction = self.options.section.payload.actionforms[i];
				if(!eachAction.showInViewList) continue;
				self.options.tableHeaderColumn.push({
					"id":"ACTION_" + eachAction.id_action,
					"name":eachAction.actionName,
					"label":eachAction.actionName,
					"type":"actionForm",
					"ico":eachAction.actionIco,
					'actionForm':eachAction,
					"contentEditable":false,
					"format":"","position":{},
					"section_id":eachAction.section_id,
					//2014-01-15@HADR: Corrige bug al mostrar la vista que tiene un action configurado como visible en la vista
					"sectionName":self.options.section.id});
			}			
		},
		
		_buildFooterForm: function(){
			var self = this;
			var myDivForm = $("<div/>");
			if(self.options.canAdd && self.options.editType == 3){
				myDivForm = $("<div/>", {"class":"FormInline FormInline_"+self.options.section.name, "style":"/*clear:both*/ display: none;"});    
			}
			return myDivForm;
		},
		//2013-11-26@RTORRES:(#iphoneUse)metodo para ponerle el fixed a las cabezeras.
		initControlFixed: function(){
			var self = this;
			if(self.options.masterKey == null)
			{
				//self.bindings.find("thead").fixed({namespace:'controlbartable',headTop:self.bindings.find("thead").offset().top, top:40, clone:true});		
				self.bindings.find("thead").css("z-index","1000");
				self.bindings.find("thead").addClass("scrollTEST");
			}
		},
		_content: function(){
			var self = this;
			var myDivContentTable = $("<div/>", {"id":"table_content_"+self.options.name,"class":"div-table", 'style':'height:100px;'});
			//2013-11-26@RTORRES:(#iphoneUse) no pone el filtro en las vistas detalle siendo iphone
			if(bitamApp.isMobile.pantallasreducidas() && self.options.isDetail)
			{
				self.options.buildBoxFilter = false;
			}
			self.element.addClass('div-table-content' + (self.options.howtowatch == 3? ' simplified':'') + (self.options.buildBoxFilter ? ' withFilterBox':'')).attr('id', 'table_'+self.options.name).append(myDivContentTable);
			return myDivContentTable;
		},
		
		_buildTableHeader: function(){
			var self = this;
			var myHeader = $("<thead/>").append($("<tr/>"));
			//2013-11-26@RTORRES:(#iphoneUse) cuando es mobile ya se puebde borar el registro.
			if((self.options.canDelete || self.options.section.payload.actionforms.length > 0) && (bitamApp.isWeb() || bitamApp.isMobile.iPad())){
				var checkMasterDelete =  $("<input/>", {"type":"checkbox","id":"checkBox_"+self.options.name,"name":self.options.name,"value":self.options.name,"data-role":"none", "class":"checkMasterSelect"}).bind("click", function(event){
					if(this.checked){
						self.tBody.find('.table_checkitem input:not(:checked):not(.checkMasterSelect)').click()
					}
					else{
						self.tBody.find('.table_checkitem input:checked:not(.checkMasterSelect)').click()
					}
				});
				myHeader.find("TR").append($("<th/>", {"class":"table_checkitem"}).append(checkMasterDelete));
				checkMasterDelete.checkbox();
			}
			//EVEGA
			if(self.options.canDelete && bitamApp.isMobile.pantallasreducidas()){	
				//myHeader.find("TR").append($("<th></th>", {"class":"table_newitem"}));
				myHeader.find("TR").append($("<th/>", {"class":"table_newitem"}));
			}else if(!bitamApp.isMobile.pantallasreducidas())
			{
				myHeader.find("TR").append($("<th/>", {"class":"table_newitem"}));
			}
				//myHeader.find("TR").append($("<th></th>", {"class":"table_newitem"}));
			//2013-11-26@RTORRES:(#iphoneUse) ahora el sync solo aplica en ipad
			if(bitamApp.isMobile.isApp()){
				myHeader.find("TR").append($("<th/>", {"class":"table_syncitem"}));
			}
			if(self.options.sortable && !bitamApp.isMobile.pantallasreducidas()){
				myHeader.find("TR").append($("<th/>"));
			}
			
			for(var keyHeader in self.options.tableHeaderColumn){
				var eachHeader = self.options.tableHeaderColumn[keyHeader]
				//2013-11-16@EVEG :(#isVisibleForUser) Si el campo no es visible para el usuario no pinta el header del campo
				var aFieldForm = self.options.section.getFieldFormWithId(eachHeader.id);
				if(aFieldForm==false){
					eachSection=bitamApp.application.getSection(eachHeader.sectionName);
					var aFieldForm = eachSection && eachSection.getFieldFormWithId(eachHeader.id);				
				}
				if(aFieldForm && !aFieldForm.isVisibleInView())
					continue;
				if(isset(eachHeader["show-form-master"])){
					if(eachHeader["show-form-master"] == false)
						continue;
					eachHeader = $.extend(true, eachHeader, eachHeader["show-form-master"]);
				}
				
				var className = '';
				if(isset(eachHeader.position) && isset(eachHeader.position.position) && eachHeader.position.position.indexOf("%")){
					var myTH = $("<th/>", {"class":className,"style":"width:"+eachHeader.position.position+";"});
				}else {
					var myTH = $("<th/>", {"class":className});
				}
				
				//@aramirez, agregamos el evento click para mostrar el menu para ordenar asc o desc.
				//para ocultar los menus hay un evento click en bitamInterface en el metodo 'init'
				myTH.append(
					$("<span/>", {"style":"cursor:pointer","data-column":eachHeader.id}).html(eachHeader.label).click(function(){
						if(!self.options.sortable){
							var $el = $(this), col = $(this).data("column");
							var arraySpan=$el.parent().parent().find('span');
							for(i=0;i < arraySpan.length;i++){
								$(arraySpan[i]).data('priority','false');
							}
							$el.data('priority', 'true')
							if($el.data('orderby') != 1){
								$el.data('orderby', 1)
								self.orderBy(col, 1);
							}else{
								$el.data('orderby', 2)
								self.orderBy(col, 2);
							}
						}
					})
					
				);
				if(eachHeader.isCalculated || aFieldForm.tagname == 'widget_field_numeric' || aFieldForm.element.type == 'numeric')
				{
					myTH.css("text-align","right" );
				}
				myHeader.find("TR").append(myTH);
				if(self.options.section.payload.views[self.options.sectionView].ViewCustomization) {				
					Customization(myTH.find('span'),self.options.section.payload.views[self.options.sectionView].ViewCustomization.fontheader,"texto");
				}               
			}
			
			if(self.options.canDelete){
				/*2014-04-22 @JRPP (#DeletedDELET) Quitar opcion de eliminar */
				//myHeader.find("TR").append($("<th/>"));
			}
			
			return myHeader;
		},
		
		_buildControlHeader: function(){
			var self = this;
			myMainMenu = $("<ul>", {"class":"mainMenu"});
			if(self.options.buildBoxFilter && self.options.section.getViewWithID(self.options.sectionView).fieldsFilterBox.length!=0){
				var button = $('<a title="'+ $.t('Show filter') +'" class="btn_showhideboxfilter btn btn-basic" data-shadow="false" data-inline="true"  data-ajax="false" href=\'javascript:;\'><img src="'+ bitamApp.buildURL('images/showFilterTb.png') +'" /></a>').on('click', function(){
					if(self.element.find('#filterBox_' + self.options.name).toggle().is(":visible")){
						self.element.find('#table_content_' + self.options.name).removeAttr('style');
						$(this).hide();
						//2013-11-26@RTORRES:(#iphoneUse) algo se oculta no recuerdo que
						if(bitamApp.isMobile.pantallasreducidas())
						    self.element.find('#table_content_' + self.options.name).hide();
						
					}else{
						self.element.find('#table_content_' + self.options.name).css('margin-left', '0');
						$(this).show();
					}					
				});
				myMainMenu.append($("<li/>", {'class':'btn-showhideboxfilter'}).append(button));
				if(!self.options.boxFilterHide){
					button.hide();
				}
			}
			//JAPR 2013-09-12: Validado que la captura tipo Wizard muestre tambi�n el bot�n de New
			if(self.options.canAdd && (self.options.editType == SectionEditType.fetNewPage || self.options.editType == SectionEditType.fetWizard)){
				var options = {};
				options.openmodal = self.options.openmodal;
				options.widgetName = self.options.name;
				options.prevPage = {};
				//2013-11-05@ARMZ: (#ebvWizardGrid #wizard) Agregada la opcion para indicarle al boton de new que tipo de forma debe mostrar (normal o wizard)
				options.editType = self.options.editType;
				if(isset(self.options.masterSection)){					
					options.prevPage.section = self.options.masterSection.name;
					options.prevPage.id = self.options.masterKey;
					options.masterSection = self.options.masterSection.name;
				}
				if(isset(self.options.sectionView)){
					options.sectionView = self.options.sectionView;
					options.prevPage.section = self.options.section.name;
				}
				options.defaultValues = {};
				if(isset(self.options.defaultValues)&&!$.isEmptyObject(self.options.defaultValues)){
					options.defaultValues = $.extend(true, {}, self.options.defaultValues);
				}				
				var button = $('<button class="btn_Add btn btn-basic" href=\'javascript://add_widget\'>'+$.t('New ')+self.options.section.sectionName+'</button>');
				button.on('click', {'options':options}, function(event){
					event.stopImmediatePropagation();
					event.preventDefault();
					event.stopPropagation();
					var anEvent = new $.Event('OpenNewForm');self.element.trigger(anEvent);if(anEvent.isDefaultPrevented()) return;
					bitamApp.add(self.options.section.name, self.options.filter, event.data.options);
				})
				myMainMenu.append($("<li/>").append(button));
				Customization(button,"NAVNew","boton");
			}
			else if(self.options.canAdd && self.options.editType == SectionEditType.fetInline)
			{
			    var button = $('<button class="btn_Add btn btn-basic" href=\'javascript://add_widget\'>'+$.t('New ')+self.options.section.sectionName+'</button>');
			    button.on('click', {'options':options}, function(event){
			    	var anEvent = new $.Event('OpenNewForm');self.element.trigger(anEvent);if(anEvent.isDefaultPrevented()) return;
				    self.element.find('.FormInline_'+self.options.section.name).toggle();
					if(self.element.find('.FormInline_'+self.options.section.name).is(':visible')) {
						dfd = $.Deferred();
						//2014-01-31@MABH: (#31202) Al tratarse de un detalle ahora cargamos formBasicDetail para hacer el render
						//es necesario para diferenciar los botones de guardado y los otros de os del maestro
					   	require(['views/form/formBasicDetail', 'libs/misc/vm'], function(FormPage, Vm) {
						    var aSection = self.options.section;
						    var sSection = self.options.section.name;
						    var opt = $.extend({}, self.options);
						    opt.sectionName = sSection;
						    //var aFormPage = new FormPage(opt).setElement(self.element.find(".FormInline_"+self.options.section.name));
						    //2014-04-22@HADR(#32439) Agregado el viewManager
						    aFormPage = Vm.create({}, sSection, FormPage, opt).setElement(self.element.find(".FormInline_"+self.options.section.name));
						    aFormPage.render().then(function() {
							    dfd.resolve();
							    $('html, body').animate({
									scrollTop: self.element.find('.FormInline_'+self.options.section.name).offset().top - 50
								}, 1000);
						    }, function(){
							    dfd.reject();
						    });
					    });
					}
			    })
			    myMainMenu.append($("<li/>").append(button));
			    Customization(button,"NAVNew","boton"); 
			}
			//2013-11-26@RTORRES:(#iphoneUse) como se seteo que n fuera ipad ya no se ocupaba
			if(self.options.canDelete /*&& (bitamApp.isWeb() || bitamApp.isMobile.iPad())*/){
				var button = $("<button/>", {"href":"javascript:;",'class':'btn_Delete btn btn-basic'}).html($.t('Delete'));
				myMainMenu.append($("<li/>").append(button));
				Customization(button,"NAVDelete","boton");
				var aCheck = new Array();
				button.click(function(){
					//developer - remove elements
					var arrIds = [];
					self.tBody.find('.table_checkitem input:checked:not(.checkMasterSelect)').each(function(){
						arrIds.push($(this).data('key'));
					})
					if(arrIds.length)
						self.remove(arrIds);
					else
						bitamApp.alert($.t('You must select at least one item'));
				});
				Customization(button,"NAVDelete","boton");
			}
			
			if(self.options.canImportExcel  && !(bitamApp.isMobile.any() || bitamApp.isMobile.iPad() || bitamApp.isMobile.Android())){
				var button = $('<a/>',{"href":'javascript:;','class':'btn_ImportExcel btn btn-basic'}).html($.t("Import"));//AGREGADO $.t(
				button.click(function(){
					var options = {};
					options.href = '/action.php';
					options.params = {
						"action":"uploadTemplate", 
						"form":self.options.section.name, 
						applicationCodeName:bitamApp.application.codeName,
						userKey:bitamApp.user.cla_usuario,
						filter:self.options.filter,
						"widget":self.options.name
					};
					options.title = $.t("Import") + " " + self.options.section.sectionName;
					options.width = 445;
					BitamInterface.windowModal(options);
				});
				myMainMenu.append($("<li/>").append(button));
				Customization(button,"NAVImport","boton");
			}
			
			if((self.options.canExportExcel || self.options.canExportPDF) && !(bitamApp.isMobile.any() || bitamApp.isMobile.iPad() || bitamApp.isMobile.Android())){   
				
				var listMoreExports  = $("<ul/>");
				var controlExports =  $("<div/>", {"id":"ActionForms", "class":"ddm", "style":"position: absolute;z-index: 50;display:none;"}).append(
					$("<div/>").append(
						listMoreExports
					)
				)
				var button = $('<button/>',{"href":'javascript:;', 'class':'btn_Export btn btn-basic'}).html($.t('Export')).click(function(event){
						//$(".ddm").hide();
						event.stopImmediatePropagation();
						var p = $(event.currentTarget).position();
						controlExports.show();
				});
				if(self.options.canExportExcel){
					listMoreExports.append($("<li/>").append($('<a/>',{"data-role":"none","href":'javascript:;'}).html($.t('Export excel')).click(function(){ //AGREGADO $.t(
						if(confirm($.t("Are you sure you want to export this data?"))){
							self.options.filterApplied;
							var iframe;
							iframe = document.getElementById("hiddenDownloader");
							if (iframe === null){
								iframe = document.createElement('iframe');  
								iframe.id = "hiddenDownloader";
								iframe.style.visibility = 'hidden';
								document.body.appendChild(iframe);
							}
							params = {};
							params.action = "exportData";
							params.form = self.options.section.name;
							params.sectionView = self.options.sectionView;
							//2013-10-29@ARMZ: (#buildFilterViewQuery) A partir de la version 3.60001 ya no mandara el filtro de la vista la interfaz de JS, lo resolvera el algoritmo armador de querys.
							params.appVersion = bitamApp.appVersion;
							if(isset(self.options.filterApplied)){
								params.filter = (self.options.filterApplied ? self.options.filterApplied : '');
							}else
							{
								params.filter = (self.options.filter ? self.options.filter : '');
							}
							params.userKey = bitamApp.user.cla_usuario;
							params.applicationCodeName = bitamApp.application.codeName;
							params.environment = bitamApp.getOption('environment');
							iframe.src = bitamApp.getURLServer() + "action.php?" + $.param(params); 
						}						
					})));
				}
				if(self.options.canExportPDF)
				{
					listMoreExports.append($("<li/>").append($('<a/>',{"data-role":"none","href":'javascript:;'}).html($.t('Export PDF')).click(function(){ //AGREGADO $.t(
						var options = {};
						var myForm = $('<form/>', {'id':'formUploadPDF'});
						var headerLabel = $("<div>",{"style":"width:15%; float:left; margin-bottom: 10px;margin-top: 10px;"}).append($("<label/>").html($.t("Header (Optional):")));
						var header = $("<div>",{"style":"width:35%; float:left; margin-bottom: 15px;margin-top: 10px"}).append($("<textarea/>", {"type":"textarea", "id":"headerFloat","style":"width:325px; height:50px;"}));
						var footerLabel = $("<div>",{"style":"width:15%; float:left; margin-bottom: 10px; clear: both"}).append($("<label/>").html($.t("Footer (Optional):")));
						var footer = $("<div>",{"style":"width:35%; float:left; margin-bottom: 15px;"}).append($("<textarea/>", {"type":"textarea", "id":"footerFloat","style":"width:325px; height:50px;"}));
						myForm.append(headerLabel,header,footerLabel,footer);
						options.fnCancel = function(){};
						options.fnOk= function(){
							var iframe;
							iframe = document.getElementById("hiddenDownloader");
							if (iframe === null){
								iframe = document.createElement('iframe');  
								iframe.id = "hiddenDownloader";
								iframe.style.visibility = 'hidden';
								document.body.appendChild(iframe);
							}
							params = {};
							params.header = $("#headerFloat").val()
							params.footer = $("#footerFloat").val()
							params.filter = (self.options.filterApplied ? self.options.filterApplied : '');
							params.filterDisplay = 1;
							params.filterView = self.options.section.filterBy;
							params.userKey = bitamApp.user.cla_usuario;
							params.view= self.options.sectionView;
							params.applicationCodeName = bitamApp.application.codeName;
							//2013-10-29@ARMZ: (#buildFilterViewQuery) A partir de la version 3.60001 ya no mandara el filtro de la vista la interfaz de JS, lo resolvera el algoritmo armador de querys.
							params.appVersion = bitamApp.appVersion
							params.environment = bitamApp.getOption('environment');
							var arrSpan=self.element.find('thead').find('span');
							for(var i=0;arrSpan.length > i;i++){
								if($.isset($(arrSpan[i]).data().priority) && $(arrSpan[i]).data().priority==='true'){
									if($.isset(params.order)){
										params.order=params.order+","+$(arrSpan[i]).data().column+":"+$(arrSpan[i]).data().orderby;
									}else{
										params.order=$(arrSpan[i]).data().column+":"+$(arrSpan[i]).data().orderby;
									}
								}
							}
							if(self.options.isDetail)
							{
								params.filterView = self.options.filter
								params.isDetail = true;
							}
							iframe.src = bitamApp.getURLServer() + "/wkhtmltopdf/exportToPDF.php?" + $.param(params); 
						};
						options.labelOk= "Export";
						options.element = myForm;
						options.title = $.t("Export PDF")+" "+ self.options.section.sectionName;
						options.width = 445;
						BitamInterface.windowModal(options);
					})));
				}
				//button.button();
				Customization(button,"NAVExport","boton");
				myMainMenu.append($("<li/>").append(button));
				button.append(controlExports);
			}
			
			if(!$.isEmptyObject(self.options.section.payload.viewsNewForm)){
				var options = {};
				options.prevPage = {};
				if(isset(self.options.masterSection)){
					options.prevPage.section = self.options.masterSection.name;
					options.prevPage.id = self.options.masterKey;
				}				
				if(isset(self.options.sectionView)){
					options.sectionView = self.options.sectionView;
					options.prevPage.section = self.options.section.name;
				}
				
				for(var key in self.options.section.payload.viewsNewForm){
					var eachViewsNewForm = self.options.section.payload.viewsNewForm[key];
					options.viewNewForm = {};
					options.viewNewForm['id_vforms'] = eachViewsNewForm.id_vforms;
					options.viewNewForm['triggersave']= eachViewsNewForm.triggersave;
					var button = $('<a/>',{"data-shadow":"false","data-inline":"true","data-theme":"g","data-role":"button","data-ajax":"false","href":'javascript:bitamApp.add("'+ self.options.section.name +'", '+ JSON.stringify(self.options.filter) +', '+ JSON.stringify(options) +')'});
					button.html(eachViewsNewForm.label).button();
					myMainMenu.append($("<li/>").append(button));
					delete button;
					delete eachViewsNewForm;
				}
				delete options;
			}
			
			//si hay accionforms  generar una lista
			if(!$.isEmptyObject(self.options.section.payload.actionforms)){
				var options = {};
				options.prevPage = {};
				if(isset(self.options.masterSection)){
					options.prevPage.section = self.options.masterSection.name;
					options.prevPage.id = self.options.masterKey;
				}				
				if(isset(self.options.sectionView)){
					options.sectionView = self.options.sectionView;
					options.prevPage.section = self.options.section.name;
				}
				var a = BitamInterface._getInstance(this);
				var theme = a._appResponse.payload.options.navigationTheme;
				var listMoreActions  = $("<ul/>");
				var controlActionForms =  $("<div/>", {"id":"ActionForms", "class":"ddm", "style":"position: absolute;z-index: 50;display:none;"}).append(//top: 73px;left: 75px;
					$("<div/>").append(listMoreActions)
				)
				var button = $('<a/>',{"data-shadow":"false","data-inline":"true","data-theme":"g", "data-ajax":"false","href":'javascript:;','class':'btn_Actions btn btn-basic'}).html($.t("Actions")).click(function(event){
					$(".ddm").hide();
					event.stopImmediatePropagation();
					var p = $(event.currentTarget).position();
					controlActionForms.show();
				});
				//button.button();
				Customization(button,"NAVActions","boton");
				myMainMenu.append($("<li/>").append(button));
				button.append(controlActionForms);
				buildActionForms(self,self.options.section,listMoreActions,controlActionForms,true,null);
				delete button;
				delete options;
			}
			//boton de las "Vistas de vistas"
			if(self.options.masterKey == null && self.options.howtowatch != '4')
			{
				var listActionsViews  = $("<ul/>");
				var controlActionViews =  $("<div/>", {"id":"ViewsActions", "class":"ddm", "style":"position: fixed;z-index: 50;display:none;"}).append(//top: 73px;left: 75px;
					$("<div/>").append(listActionsViews)
				);
				if(!self.options.isDetail && self.options.canBuildCustomView){
					var button = $('<button/>',{"href":'javascript:;','class':'btn_Views btn btn-basic'}).html($.t("Views")).click(function(event){
						event.stopImmediatePropagation();
						var p = $(event.currentTarget).position();
						controlActionViews.show();
					});
					Customization(button,"NAVViews","boton");
					myMainMenu.append($("<li/>").append(button));
					button.append(controlActionViews);
				}
				if(!bitamApp.isMobile.isApp())
					buildViewsActions(self.options.section,self.options.sectionView,listActionsViews,controlActionViews,self.options.viewNameOriginal);
				buildEachViews(self.options,self.options.section,listActionsViews,controlActionViews,true,null);
				//
			}
			/*2014-07-08@JRPP (#duplicateRow) Se agrega boton de duplicado de registros */
			if(self.options.section.canDuplicate  && !(bitamApp.isMobile.any() || bitamApp.isMobile.iPad() || bitamApp.isMobile.Android())){
				var button = $('<button/>',{"href":'javascript:;','class':'btn_duplicate_rows btn btn-basic'}).html($.t("Duplicate"));//AGREGADO $.t(
				button.click(function(){
					var arrIds = [];
					self.tBody.find('.table_checkitem input:checked:not(.checkMasterSelect)').each(function(){
						arrIds.push($(this).data('key'));
					})
					if(arrIds.length) {
						require(['libs/eBavel/class/duplicateAction'], function(DuplicateAction) {
							var aDuplicateAction = new DuplicateAction();
							aDuplicateAction.duplicateRow({ids:arrIds, first: true, sectionName: self.options.section.name});
						});
						//Utils.duplicateRow({ids:arrIds, first: true, sectionName: self.options.section.name});
					}
					else {
						bitamApp.alert($.t('You must select at least one item'));
					}
				});
				myMainMenu.append($("<li/>").append(button));
				Customization(button,"NAVImport","boton");
			}
			var selectPage = $("<select/>", {"id":"selectPage_"+self.options.name, "class":"selectPage_"+self.options.name, "data-role":"none"}).change(function(){});
						
			//ocultamos los botones, si al cargar los datos hay registros, los mostramos
			myMainMenu.find('.btn_Actions').hide();
			//myMainMenu.find('.btn_ImportExcel').hide();
			myMainMenu.find('.btn_Export').hide();
			myMainMenu.find('.btn_Delete').hide();
			if(bitamApp.isMobile.pantallasreducidas()){
				myMainMenu.find('.btn_Delete').show();
			}
			if(!bitamApp.isMobile.any() || self.options.isDetail){
				return $("<div/>", {"class":"controlMenuTable", "style":"height: 40px;"}).append(
					$("<div/>", {"class":"menuTable"}).append(myMainMenu)
				);
			}
			return '';
		},
		_buildGoogleMapsView: function() {
			var self = this;
			return $('<div/>', { 'id':'googleMapView'+self.options.name, 'name':'googleMapView'+self.options.name, 
				'style':'width:100%;height:300px;display:none;overflow:auto;' });
		},

		_buildPaginationInfo: function() {
			var self = this;
			var table_tablenav = $("<div/>", {"class":"tablenav"});
			var numIni = 0;
			var numFin = 0;
			var loadingMsg = $('<div/>', {'class':'imgLoading fa fa-cog fa-spin', 'style':'float: left;margin-right: 10px;'}).append(
//				$('<img/>', {'src':bitamApp.buildURL('images/loading1.gif'), 'style':'margin-top: 3px;'})
				$('<i/>', {'class':"icon-spinner icon-spin icon-large", 'style':'margin-top: 3px;'})
			).hide();
			var pagDiv = $("<div/>", {"id":"", "class":"ddm pagination_div", "style":"position: absolute;z-index: 50;"}).append(
				$("<div/>").append(
					$("<ul/>").append(
						$("<li/>").append($("<a/>", {"class":"disable firstpage"}).html($.t("First page")).click(function(){if($(this).hasClass("disable")) return;$("#nav_paginator_up_"+self.options.name).find(".btn-bck-vw .ui-btn-text").css("opacity", ".4");self.move(-1, true)})),
						$("<li/>").append($("<a/>", {"class":"lastpage"}).html($.t("Last page")).click(function(){
							if($(this).hasClass("disable")) 
								return;
							$("#nav_paginator_up_"+self.options.name).find(".btn-ntx-vw .ui-btn-text").css("opacity", ".4");
							self.move(1, true);
						}))
					)
				)
			).hide();
			
			table_tablenav.append(
				$("<div/>", {style:"float: right;margin-top: 4px;margin-right: 20px;position: relative;"}).append(
					loadingMsg,
					$("<div/>", {style:"float: left;margin-top: 1px;"}).append(
						$("<select/>", {"data-role":"none", 'style':'margin-right: 10px;', 'class': 'locationPageSize'}).append(
							//$("<option/>", {"value":"5"}).html("5").prop('selected', (self.options.pageSize == 5 ? true : false)),
							//$("<option/>", {"value":"10"}).html("10").prop('selected', (self.options.pageSize == 10 ? true : false)),
							$("<option/>", {"value":"20"}).html("20").prop('selected', (self.options.pageSize == 20 ? true : false)),
							$("<option/>", {"value":"50"}).html("50").prop('selected', (self.options.pageSize == 50 ? true : false)),
							$("<option/>", {"value":"100"}).html("100").prop('selected', (self.options.pageSize == 100 ? true : false))
						).change(function(){self.element.find('select.locationPageSize').val(this.options[this.selectedIndex].value);loadingMsg.show();self.pageSize(this.options[this.selectedIndex].value).then(function(){loadingMsg.hide()})}),
						$("<span>",{"class":"reductedPages"}).html($.t("Entries Per Page"))
					),
					$('<div/>', {'style':'float: left;margin-top: 3px;width: 80px;padding-left: 2px;padding-right: 2px; margin-left: 15px;'}).append(
						$("<span/>", {"id":"info-viewpage_"+ self.options.name,"class":"info-viewpage"}).html("<b>"+numIni+"</b>-<b>"+numFin+"</b> de <b>" + self.options.totalRows +"</b>").click(function(e){					$(".ddm").hide();e.stopImmediatePropagation();pagDiv.find('.firstpage').removeClass('disable');pagDiv.find('.lastpage').removeClass('disable');if(self.options.currentPage==1){pagDiv.find('.firstpage').addClass('disable');}if(self.options.currentPage==self.options.totalPages){pagDiv.find('.lastpage').addClass('disable');}pagDiv.show();})
					),
					pagDiv, //2013-11-26@RTORRES:(#iphoneUse) se aplico in rezise cada que se cambiaba de pagina.
					$('<div/>',{'style':'float:left; margin-left: 15px;'}).append(
						$("<a/>", {"class":"btn-bck-vw btn btn-basic"}).html('<i class="icon-chevron-left"></i>').click(function(){self.move(-1); $(window).resize();}),
						$("<a/>", {"class":"btn-ntx-vw btn btn-basic"}).html('<i class="icon-chevron-right"></i>').click(function(){self.move(1); $(window).resize();})
					)
				)
			);
			return table_tablenav;
		},

		_buildFooter: function(){
			var self = this, tableFooter, tableFooter_tablelocation, loadingMsg;
			tableFooter = $("<div/>", {"id":"tablefooter_paginator_" + self.options.name, "class":"tablefooter"});
			loadingMsg = $('<div/>', {'class':'imgLoading fa fa-cog fa-spin'}).append(
//				$('<img/>', {'src':bitamApp.buildURL('images/loading1.gif'), 'style':'margin-top: 3px;'})
				$('<i/>', {'class':"icon-spinner icon-spin icon-large", 'style':'margin-top: 3px;'})
			).hide();
			//08/11/2013@rtorres se agrega ,a losta para mostar (ultima y primera pagina)
					var pagDiv = $("<div/>", {"id":"", "class":"ddm pagination_div", "style":"position: absolute;z-index: 50;display:none; left: 163px; top: 30px;"}).append(
					    $("<div/>").append(
						    $("<ul/>").append(
							    $("<li/>").append($("<a/>", {"class":"disable firstpage"}).html($.t("First page")).click(function(){if($(this).hasClass("disable")) return;$("#nav_paginator_up_"+self.options.name).find(".btn-bck-vw .ui-btn-text").css("opacity", ".4");self.move(-1, true)})),
							    $("<li/>").append($("<a/>", {"class":"lastpage"}).html($.t("Last page")).click(function(){
								    if($(this).hasClass("disable")) 
									    return;
								    $("#nav_paginator_up_"+self.options.name).find(".btn-ntx-vw .ui-btn-text").css("opacity", ".4");
								    self.move(1, true);
							    }))
						    )
					    )
					).hide();
			//2013-10-29@EVEG:(#pageSize, #tablelocation)Para mantener el numero de paginacion busca pageSize en objeto de la vista
			if(bitamApp.application.payload.sections[self.options.section.id] && bitamApp.application.payload.sections[self.options.section.id].payload.views[self.options.sectionView].pageSize){
				//2014-06-23@HADR(#820N4L) Juan Carlos; Pone una pantalla gris.
				//Estaba equivocada la referencia al id de la vista
				self.options.pageSize=bitamApp.application.payload.sections[self.options.section.id].payload.views[self.options.sectionView].pageSize;
			}
			if(this.options.howtowatch != 3){
				var isReducted = (bitamApp.isMobile.pantallasreducidas()) ? "isReducted" : "s";
				tableFooter_tablelocation = $("<div/>", {"class":"tablelocation "+isReducted+""}).append(
					loadingMsg,
					$("<div/>").append(
						$("<select/>", {"data-role":"none", 'class': 'locationPageSize'}).append(
							//$("<option/>", {"value":"5"}).html("5").prop('selected', (self.options.pageSize == 5 ? true : false)),
							//$("<option/>", {"value":"10"}).html("10").prop('selected', (self.options.pageSize == 10 ? true : false)),
							$("<option/>", {"value":"20"}).html("20").prop('selected', (self.options.pageSize == 20 ? true : false)),
							$("<option/>", {"value":"50"}).html("50").prop('selected', (self.options.pageSize == 50 ? true : false)),
							$("<option/>", {"value":"100"}).html("100").prop('selected', (self.options.pageSize == 100 ? true : false))
						).change(function(){self.element.find('select.locationPageSize').val(this.options[this.selectedIndex].value);loadingMsg.show();self.pageSize(this.options[this.selectedIndex].value).then(function(){loadingMsg.hide()})}),
						$("<span>",{"class":"reductedPages"}).html($.t("Entries Per Page"))
					),
					$("<div/>", {"class":"page", style:"cursor:initial"}).html($.t("Page")+" <span id='currentpage_"+ self.options.name +"'>1</span> "+$.t("of")+" <span id='totalpages_"+ self.options.name +"'>"+ self.options.totalPages +"</span>").click(function(e){/*$(".ddm").hide();e.stopImmediatePropagation();pagDiv.find('.firstpage').removeClass('disable');pagDiv.find('.lastpage').removeClass('disable');if(self.options.currentPage==1){pagDiv.find('.firstpage').addClass('disable');}if(self.options.currentPage==self.options.totalPages){pagDiv.find('.lastpage').addClass('disable');}pagDiv.show();*/}),
					pagDiv,
					$("<div/>",{"style":(bitamApp.isMobile.pantallasreducidas()) ? "margin-left: 0px;" : ""}).append($("<a/>", {"class":"btn-bck-vw btn btn-basic"}).html('<i class="icon-chevron-left"></i>').click(function(){ self.move(-1) }),$("<a/>", {"class":"btn-ntx-vw btn btn-basic"}).html('<i class="icon-chevron-right"></i>').click(function(){ self.move(1) }))
				);
			}else{
				tableFooter_tablelocation = $('<div/>', {"class":"tablelocation"}).append(
					$("<a/>", {"data-theme":"g", "class":'btn-more-vw'}).html($.t('More')+'...').on('click', function(){self.move(1)}).button()
				);
			}
			if(self.options.howtowatch=="5" && $.inArray(self.options.section.payload.views[self.options.sectionView].payload.ganttProp.visibleRowsGantt, [5,10,20,50,100]) < 0) {
				tableFooter_tablelocation.find('select').find('option').each(function(){
					if(parseInt(self.options.section.payload.views[self.options.sectionView].payload.ganttProp.visibleRowsGantt) < parseInt(this.value)) {
						$(this).before($("<option/>", {"value":self.options.section.payload.views[self.options.sectionView].payload.ganttProp.visibleRowsGantt}).html(self.options.section.payload.views[self.options.sectionView].payload.ganttProp.visibleRowsGantt).prop('selected', (self.options.pageSize == self.options.section.payload.views[self.options.sectionView].payload.ganttProp.visibleRowsGantt ? true : false)));
						return false;
					}
				})
			}
			return tableFooter.append(tableFooter_tablelocation);
		},
		
		_updateInfoPagination: function(pagination){
			var self = this;
			if($.isPlainObject(pagination)){
				self.options.totalRows = pagination.totalRows;
				self.options.currentPage = pagination.currentPage;
				self.options.pageSize = pagination.pageSize;
				self.options.totalPages = pagination.totalPages;
			}
			var numFin = self.options.pageSize * self.options.currentPage;
			var numIni = (self.options.totalRows == 0 ? 0 : ((numFin - self.options.pageSize) + 1));
			numFin = (self.options.totalRows < numFin ? self.options.totalRows : numFin);
			self.element.find("#info-viewpage_"+ self.options.name).html("<b>"+numIni+"</b>-<b>"+numFin+"</b> de <b>" + self.options.totalRows +"</b>");
			self.element.find("#currentpage_"+ self.options.name).html(self.options.currentPage);
			
			self.element.find("#table_content_"+ self.options.name +" .tablenav .btn-bck-vw .ui-btn-text").css("opacity", "1");
			self.element.find("#table_content_"+ self.options.name +" .tablenav .btn-ntx-vw .ui-btn-text").css("opacity", "1");
			self.element.find("#table_content_"+ self.options.name +" .btn-more-vw").show();
			if(self.options.currentPage == 1)
				self.element.find("#table_content_"+ self.options.name +" .tablenav .btn-bck-vw").addClass("disable").find('.ui-btn-text').css("opacity", ".4");
			if(self.options.currentPage == self.options.totalPages){
				self.element.find("#table_content_"+ self.options.name +" .tablenav .btn-ntx-vw .ui-btn-text").css("opacity", ".4");
				self.element.find("#table_content_"+ self.options.name +" .btn-more-vw").hide();
			}
			self.element.find("#totalpages_" + self.options.name).html(self.options.totalRows == 0 ? 1 :self.options.totalPages);
			//2013-11-26@RTORRES:(#iphoneUse) esstos botones solo estan cuando es Web
			if(!bitamApp.isMobile.pantallasreducidas())
			{
			    if(self.options.totalRows == 0){
				    self.myDivNorecordFound.show();
				    self.controlHeader.find('.btn_Actions').css('display', 'none');
				    //self.controlHeader.find('.btn_ImportExcel').css('display', 'none');
				    self.controlHeader.find('.btn_Export').css('display', 'none');
				    self.controlHeader.find('.btn_Delete').css('display', 'none');
				    self.controlHeader.find('.btn_duplicate_rows').css('display', 'none');
					self.controlFooter.find('.btn_Actions').css('display', 'none');//2014-02-28 JRPP(#31581) Se oculta las acciones cuando ya no existe ningun registro.
					self.controlFooter.find('.btn_Export').css('display', 'none');//2014-02-28 JRPP(#31581) Se oculta las exportaciones cuando ya no existe ningun registro.
					self.controlFooter.find('.btn_Delete').css('display', 'none');//2014-02-28 JRPP(#31581) Se oculta el delete cuando ya no existe ningun registro.
					self.controlFooter.find('.btn_duplicate_rows').css('display', 'none');//2014-02-28 JRPP(#duplicateRow) Se oculta la opcion de duplicar cuando ya no existe ningun registro.
					self.footer.hide();
			    }
			    else {
				    self.myDivNorecordFound.hide();
				    self.controlHeader.find('.btn_Actions').css('display', '');
				    self.controlFooter.find('.btn_Actions').css('display', '');
			    	self.controlHeader.find('.btn_ImportExcel').css('display', '');
				    self.controlHeader.find('.btn_Export').css('display', '');
				    self.controlFooter.find('.btn_Export').css('display', '');
				    self.controlHeader.find('.btn_Delete').css('display', '');
				    self.controlFooter.find('.btn_Delete').css('display', '');
				    self.controlHeader.find('.btn_duplicate_rows').css('display', '');//2014-02-28 JRPP(#duplicateRow) Se muestra En caso contrario.
				    self.controlFooter.find('.btn_duplicate_rows').css('display', '');//2014-02-28 JRPP(#duplicateRow) Se muestra En caso contrario.
				    self.footer.show();
			    }
			    self.controlHeader.find(".liTableNav").hide();
			    self.controlFooter.find(".liTableNav").hide();
			    self.pagBar.find("#currentpageHead_"+ self.options.name).html(self.options.currentPage);
			    self.pagBar.find("#totalpagesHead_"+ self.options.name).html(self.options.totalRows == 0 ? 1 :self.options.totalPages);
			}
			//EVEG 2014-03-26 Si son 20 registros no mostramos los botones del footer ni los controles de paginacion
			if((self.options.totalRows<=20 && self.options.totalPages==1)||($.isset(self.options.viewPagination) && self.options.viewPagination)|| self.options.totalRows==0){
				self.element.find('.tablefooter').hide();
				self.element.find('.controlMenuTableFooter').hide();
				self.element.find('.tablenav').hide();
				self.element.find('.tablelocation').hide()
			}else{
				self.element.find('.tablefooter').show();
				self.element.find('.controlMenuTableFooter').show();
				self.element.find('.tablenav').show();
				self.element.find('.tablelocation').show()
			}
		},
		
		_updateBoxFilter: function(descriptionTags){
			var self = this, aSection = self.options.section, aView = aSection.getViewWithID(self.options.sectionView),
			fieldsForm = aView.fieldsFilterBox, data = descriptionTags, dfd  = $.Deferred(), ValuesCustomization = aView.ViewCustomization, 
			filterApplied = this.options.filterApplied||'', list = $("<ul/>", {"class":"list-filter"});
			
			//funcion que obtiene los valores seleccionados para el filtro y ejecuta la peticion para renderear a vista con los valores filtrados
			var submitFilter = function(event){
			    var promise;
			    //$(".filterbox").append($("<div/>",{"class":"blockFilter"}));
			    $(".filterbox :input").attr("disabled", true);
				var searchString = self.element.find('input[name|="search"]').val(), queryString = '', fieldsForm = aView.fieldsList, aFieldForm = {};
				if($.trim(searchString) != ''){
					queryString = [];
					for(var key in fieldsForm){
						aFieldForm = aSection.getFieldFormWithId(fieldsForm[key].id);
						if(aFieldForm && fieldsForm[key].id.slice(0,3) != "FDT" && fieldsForm[key].id != "createdUserKey" && aFieldForm.tagname != "widget_secheader" && aFieldForm.tagname != "widget_geolocation" && aFieldForm.tagname != "widget_signature"){
							//2013-12-11@HADR(#LookupsRecursivosSameTable) Agrega el alias a los campos de la misma forma debido a que es posible que exista un campo de tipo catalogo recursivo a la misma forma
							if(!aFieldForm.isCatalog()){
								queryString.push("A." + fieldsForm[key].id + " LIKE '%" + searchString + "%'");
							}else{
								queryString.push(fieldsForm[key].id + " LIKE '%" + searchString + "%'");
							}
						}
					}
					queryString = queryString.length>0?queryString.join(' OR '):'';
				}				
				
				//Id del campo en que se hizo click
				var sFieldClick = (event?$(event.currentTarget).data("fieldname"):'');
				var temaFieldForm = aSection.getFieldFormWithId(sFieldClick);
				var arrFilter = {};
				arrFilter[aView.id] = {};
				var selector = "";
				if(!bitamApp.isMobile.pantallasreducidas() || temaFieldForm.tagname ==  "widget_field_date")
					selector = "#filterBox_"+self.options.name+" input:checked";
				else
					selector = "#filterBox_"+self.options.name+" select option:selected";

				self.element.find(selector).each(function(){
					var $el = $(this), sField = $el.attr("data-fieldname");
					if(!arrFilter[aView.id][sField]) arrFilter[aView.id][sField] = [];
					arrFilter[aView.id][sField].push($el.val());							
				});
				if(Object.keys(arrFilter[aView.id]).length > 0){
					if(queryString != ''){
						queryString = '(' + queryString + ') AND (' + BitamStorage.webDB.buildFilterWithObject({filter:arrFilter[aView.id],sectionName:aSection.name}) + ')';
					}else
						queryString = BitamStorage.webDB.buildFilterWithObject({filter:arrFilter[aView.id],sectionName:aSection.name});
				}
				promise = self.filter(queryString).then(function(o){
				//2013-11-26@RTORRES:(#iphoneUse) se hace un scroll al top 
				window.scrollTo(0,1);
					//2014-07-30@HADR((#HGKX0R) Filtros en el ipad que se muestren ordenados y mismo comportamiento que en Web
					//En movil o en web debe volver a pintar el filterbox al aplicar un filtro
					renderDescTagsBox(o[aView.id].descriptionTags, aView.fieldsFilterBox, aSection, arrFilter[aView.id], sFieldClick);
					checkClick(sFieldClick);
					$(".filterbox :input").attr("disabled", false);
				});
				return promise;
			}
			var myFormSearch = $("<form/>").html('<div class="input-group"><input name="search" type="search" placeholder="Search" class="form-control input-sm" style="height: 28px;-webkit-appearance: none;"><span class="input-group-btn"><button class="btn btn-default"><i class="icon-search"></i></button></span></div>').on('submit', function(){
				event.preventDefault();
				submitFilter().then(function(){
				    if(bitamApp.isMobile.pantallasreducidas())
						$(".btn_showhideboxfilter").trigger( "click" );
				});
				
				return false;
			});
			var myDivContainer = $("<div/>", {"id":"filterBox_" + self.options.name, "class":"filterbox grid_2"}).append(
				$('<div/>').append($('<a title="'+ $.t('Hide filter') +'" class="btn_showhideboxfilter" data-shadow="false" data-inline="true" data-theme="g" data-role="button" data-ajax="false" href=\'javascript:;\'><img src="'+ bitamApp.buildURL('images/showFilterTb.png') +'" /></a>').button().removeClass("ui-btn-hidden").on('click', function(){
				    
					if(self.element.find('#filterBox_' + self.options.name).toggle().is(":visible")){
						self.element.find('#table_content_' + self.options.name).removeAttr('style');
						self.element.find('.btn_showhideboxfilter').hide();
						//2013-11-26@RTORRES:(#iphoneUse)Oculta el filtro
						if(bitamApp.isMobile.pantallasreducidas())
						{
						    self.element.find('#table_content_' + self.options.name).hide();
							$(".filterMobile").find(".ui-btn-text").append("View");
						}
					}else{
						self.element.find('#table_content_' + self.options.name).css('margin-left', '0');
						self.element.find('.controlMenuTable').find('.btn_showhideboxfilter').show();
						//2013-11-26@RTORRES:(#iphoneUse)Muestra el filtro
						if(bitamApp.isMobile.pantallasreducidas())
						{	
						    self.element.find('#table_content_' + self.options.name).show();
							$(".filterMobile").find(".ui-btn-text").empty();
							$(".filterMobile").find(".ui-btn-text").prepend("<i class='icon-filter'></i>");
							window.scrollTo(0,1);
						}
					}					
				})),
				myFormSearch,
				$("<form/>", {}).append(list)
			);
			if(isset(ValuesCustomization)){
			    var arrayObject = new Object();
			    arrayObject.object2 = myDivContainer;
			    arrayObject.object3 = $("#tableFilter");
				var arrayData = new Object();
			    arrayData.Data1 = ValuesCustomization;
			    arrayData.Data2 = aSection;
			    arrayData.sw = "filterOp1";
			    Customization(arrayObject,arrayData,"filtro")			
			}
			if(self.options.boxFilterHide){
				myDivContainer.hide();
				self.element.find('#table_content_' + self.options.name).css('margin-left', '0');
			}
			//funcion que pinta el cuadro de filtros
			var renderDescTagsBox = function(descriptionTags, fieldsForm, aSection, arrFilter, sFieldClick){
				for(var keyField in fieldsForm){
					var eachFilter = fieldsForm[keyField];
					//2013-11-20@EVEG :(#isVisibleForUser) Si el campo no es visible para el usuario no pinta el box filter
					var fieldTmp=self.options.section.getFieldFormWithId(fieldsForm[keyField].id)
					if(fieldTmp && !fieldTmp.isVisibleInView())
						continue;

					if(sFieldClick == eachFilter.id && arrFilter[sFieldClick])
						continue;

					var title = list.find(".title"+eachFilter.id).empty();
					var descrTags = list.find(".tags"+eachFilter.id).empty();

					if($.isEmptyObject(descriptionTags[eachFilter.id])) continue;

					if(bitamApp.isMobile.pantallasreducidas())
					{
						if(eachFilter.tagname != 'widget_field_date')
							var listTag = $("<select multiple/>", {"class":"list-filter-tag " + eachFilter.id+" selecFilter", "id":"listFilterTag-" + eachFilter.id})
						else
							var listTag = $("<ul/>", {"class":"list-filter-tag " + eachFilter.id, "id":"listFilterTag-" + eachFilter.id});
					}else
						var listTag = $("<ul/>", {"class":"list-filter-tag " + eachFilter.id, "id":"listFilterTag-" + eachFilter.id});
					//04/03/2013@aramirez
					if(eachFilter.tagname != 'widget_field_date'){
						for(var keyDesc in descriptionTags[eachFilter.id]){
							var descrTag = keyDesc;
							var descrTagNumItems = descriptionTags[eachFilter.id][keyDesc];
							if(!bitamApp.isMobile.pantallasreducidas())
							{
								var label = $("<label/>");
								var input = $("<input/>", {"data-role":"none","data-description":descrTag.toLowerCase(),"data-fieldname":eachFilter.id,"type":"checkbox","value":descrTag});
								label.append(input, $("<span/>", {"title":descrTag}).html(descrTag));
								//2014-07-30@HADR((#HGKX0R) Filtros en el ipad que se muestren ordenados y mismo comportamiento que en Web
								//Mostrar el contador de items
								//if(!bitamApp.isMobile.isApp())
								{
									label.append($('<div/>', {'class':'tagnumitems'}).html(descrTagNumItems));
								}
								listTag.append($("<li/>", {"class":"tagname"}).append(label));
								if(arrFilter[eachFilter.id]&&$.inArray(descrTag,arrFilter[eachFilter.id])!=-1){
									input[0].checked = true;
									input.attr('checked', true);
								}
								input.checkbox();
							}else
							{
								var optionFilter = $("<option/>",{"data-role":"none","data-description":descrTag.toLowerCase(),"data-fieldname":eachFilter.id,"value":descrTag}).html(descrTag); 
								listTag.append(optionFilter);
							}
						};
					}else{
						var filterDate = function(fieldId){
							var dateFrom = Date.parse($( "#from_"+fieldId ).val());
							var dateTo = Date.parse($( "#to_"+fieldId ).val());
							if(dateFrom  && dateTo ){
								var msg = dateFrom.toString('d MMM yyyy')  + ' - ' + dateTo.toString('d MMM yyyy');
								var sRange = dateFrom.toString("yyyy-MM-dd") + '@' + dateTo.toString("yyyy-MM-dd");
								$('[data-fieldname="'+fieldId+'"]').val(sRange).prop('checked', false).trigger('click');
								if(bitamApp.isMobile.pantallasreducidas())
									$(".btn_showhideboxfilter").trigger( "click" );
							}	$('.tags'+fieldId+' .clear.ico').show();
						}

						var input = $("<input/>", {"data-role":"none","data-description":'',"data-fieldname":eachFilter.id,"type":"checkbox","value":''}).hide();
						var sRange = $.t('Any time');
						var sValueFrom = '', sValueTo = '';
						if($.isArray(arrFilter[eachFilter.id]) && arrFilter[eachFilter.id][0] && arrFilter[eachFilter.id][0]!=''){
							var dateFrom = Date.parse(arrFilter[eachFilter.id][0].split('@')[0]);
							var dateTo = Date.parse(arrFilter[eachFilter.id][0].split('@')[1]);
							sRange = dateFrom.toString('d MMM yyyy')  + ' - ' + dateTo.toString('d MMM yyyy');
							input.val(arrFilter[eachFilter.id][0]).attr('checked', true);
							var sValueFrom = dateFrom.toString('MM/dd/yyyy');
							var sValueTo = dateTo.toString('MM/dd/yyyy');
						}
						listTag.append($("<li/>", {"class":"tagname", 'style':'position:relative'}).append(input, 
							$('<div/>', {'class':'customRange'}).append(
								$("<div/>", {'class':'customRange title'}).append($.t('Custom date range'), $('<img/>', {'title':$.t('Clear range'),'class':'clear ico',src:bitamApp.buildURL("images/clear_ico.png")}).on('click', $.proxy(function(){
									$( "#from_"+this.idField ).val('');
									$( "#to_"+this.idField ).val('');
									$('[data-fieldname="'+this.idField+'"]').val('').prop('checked', true).trigger('click');
								}, {idField:eachFilter.id})).hide()),
								$('<label/>', {'class':'customRange label'}).html($.t('From')),
								$('<input type="text" style="background-color:white;" readOnly="readOnly" id="from_'+eachFilter.id+'" name="from" class="form-control input-sm" />').datepicker({
									changeMonth: true,
									changeYear: true,
									yearRange:"c-40:c+4",
									showButtonPanel: true,
									onClose: $.proxy(function( selectedDate ) {
										$( "#to_"+this.idField ).datepicker( "option", "minDate", selectedDate );
										filterDate(this.idField);
									}, {idField:eachFilter.id})
								}).val(sValueFrom),
								$('<label/>', {'class':'customRange label'}).html($.t('To')),
								$('<input type="text" style="background-color:white;" readOnly="readOnly" id="to_'+eachFilter.id+'" name="to" class="form-control input-sm" />').datepicker({
									changeMonth: true,
									changeYear: true,
									yearRange:"c-40:c+4",
									showButtonPanel: true,
									onClose: $.proxy(function( selectedDate ) {
										$( "#from_"+this.idField ).datepicker( "option", "maxDate", selectedDate );
										filterDate(this.idField);
									}, {idField:eachFilter.id})
								}).val(sValueTo)
							)
						));
					}
					if(isset(ValuesCustomization)){
						var arrayData = new Object();
						arrayData.Data1 = ValuesCustomization;
						arrayData.sw = "filterOp2";
						Customization(listTag,arrayData,"filtro")
					}
					if(!title.length){
						title = $("<li/>", {"class":"description-tag title"+eachFilter.id}).on('click', {descrTags:".tags"+eachFilter.id}, function(e){
							$(e.data.descrTags).slideToggle();
						})
						list.append(title);
					}					
					if(!descrTags.length){
						descrTags = $("<li/>", {"class":"tags"+eachFilter.id});
						list.append(descrTags);
						if(isset(ValuesCustomization)){   
							var arrayData = new Object();
							arrayData.Data1 = ValuesCustomization;
							arrayData.sw = "filterOp2";
							Customization(listTag,arrayData,"filtro")
						}
					}
					title.html($.t(eachFilter.label));
					descrTags.append(listTag);
				};
				if(bitamApp.isMobile.pantallasreducidas())
				{
					var buttonFilter = list.find(".btn-default").remove();
					buttonFilter = $('<button class="btn btn-default" type="button">Submit   <i class="icon-search"></i></button>').on('click', function(){
						event.preventDefault();
						submitFilter().then(function(){
							if(bitamApp.isMobile.pantallasreducidas())
							$(".btn_showhideboxfilter").trigger( "click" );
						});
						return false;
					});
					list.append(buttonFilter);
				}
				if(isset(ValuesCustomization))
				{	
					Customization(myDivContainer.find(".list-filter-tag"),ValuesCustomization.filterfontstyle,"texto");
				}
			}
			
			var checkClick = function(sFieldClick){
				if(list.find("input[data-fieldname='"+sFieldClick+"']:checked").length==0) sFieldClick = '';
				list.find("input[type='checkbox']:not([data-fieldname='"+sFieldClick+"'])").on('click', submitFilter);
			}			
			if(!$.isEmptyObject(data)){
				//creamos el cuadro de filtro ya con los tags que obtuvimos del server
				renderDescTagsBox(data, fieldsForm, aSection, filterApplied!=''?BitamStorage.webDB.buildArrayFilterWithString({filter:filterApplied}):{}, null);
				checkClick();
				dfd.resolve(myDivContainer);
				this.element.prepend(myDivContainer);				
			}
		},
		
		_buildPage: function(options){
			var dfd = $.Deferred(), self = this, sView = self.options.sectionView;
			if($.isset(self.options.viewPagination) && self.options.viewPagination && self.options.isDetail){
				self.options.hideViewPagination = true;
				//self.options.pageSize = 0;
			}
			/*2014-06-30@JRPP ISSUE(4OMYLA)
			*Si se trata de una forma repository User no debe de aparecer el boton de agregar
			*/
			if (self.options.editType == SectionEditType.fetInline && self.options.section.editType == "10") {
				self.element.find('.btn_Add').hide();
			}
            opt = {sectionView:sView, pageSize:self.options.pageSize, currentPage:self.options.currentPage, filter:{}}, res = {};
			opt.filter[sView] = '';
			opt.showLoadingMsg = false;
			opt.getDataInArray = true;
			opt.buildBoxFilter = !($('#filterBox_'+self.options.name).length || !self.options.buildBoxFilter);
			if(self.options.orderBy) opt.orderBy = self.options.orderBy;
			
			if(self._filter){				
				opt.filter[sView] = self._filter;
			}
			if(!$.isEmptyObject(self.options.filter)){
				opt.filter[sView] = '(' + BitamStorage.webDB.buildFilterWithObject({filter:self.options.filter,sectionName:self.options.section.name}) + ')' + (opt.filter[sView] != '' ? ' AND (' + opt.filter[sView] + ')' : '');
			}else if(typeof self.options.filter == 'string'){
				opt.filter[sView] = '(' + self.options.filter + ')' + (opt.filter[sView]!=''?' AND (' + opt.filter[sView] + ')':'');
			}
		
			self.element.find('.imgLoading:first').css({'opacity':0,'display':'block'}).animate({
				opacity: 1
			}, function() {});
			self.element.find('.imgLoading.layerTableView:first').css({'height':self.element.find("#contentlist_"+ self.options.name).height()||50});
			//06/11/2013@rtorres Se bloquea la barra de filtros hasta que termine de filtrar
			if(self.options.data){
				var eachData, idx=0, rowsNumIni=(self.options.currentPage-1)*self.options.pageSize, rowsNumFin = self.options.pageSize * self.options.currentPage - 1;
				self.tBody.empty();
				if(self.options.arrayMarker && self.options.map) {
					for(var mKey in self.options.arrayMarker) {
						var deleteMarker = self.options.arrayMarker[mKey];
						deleteMarker.marker.setMap(null);
						delete deleteMarker;
					}
					self.options.arrayMarker = new Array();
				}
				for(var key in self.options.data){
					if(idx>=rowsNumIni && idx<=rowsNumFin){
						eachData = self.options.data[key];
						self.tBody.append(self._buildRow({data:eachData}));
					}					
					idx++;
				}
				self._updateInfoPagination({currentPage: self.options.currentPage,pageSize: self.options.pageSize,totalPages: Math.ceil(idx/self.options.pageSize),totalRows: idx});
				self._checkEmptyTable();
				self.element.find('.imgLoading').css({'display':'none'});
				$(".filterbox :input").attr("disabled", false);
				self.element.find("#table_content_"+ self.options.name).css('height','initial');
				dfd.resolve(res);
			}else{
				/*2014-04-02 @JRPP Cuando se trata de una vista de businessProcessReport solo debemos de mostrar la lista que pertenece al usuario*/
				if (sView == "businessProcessReport"){
					var sFilter = '( userKey = ' + bitamApp.user.cla_usuario + ') AND ( appId = ' + bitamApp.application.id_app + ')';
					opt.filter = {businessProcessReport: sFilter};
				}
				bitamApp.getViewData([sView], opt).then(
				//HADR 2013-10-07: GRID - Si no se obtuvieron resultados para la vista y es de tipo grid entonces 
				//precargar con los valores de la primera columna configurada como catalogo
				function(results) {
					var dfd2 = $.Deferred(), promise2 = dfd2.promise();
					
					//debugger;
					if(results[sView].collection.length == 0 && self.options.howtowatch == '4') {
						var eachField = self.options.tableHeaderColumn[0];
						var aFieldForm = searchFieldFormWithID(eachField.id, self.options.fieldsForm);
						aFieldForm = new FieldForm($.extend(true, {}, aFieldForm));
						if(aFieldForm.isCatalog()){
							var arrColumns = [];
							if(aFieldForm.element.catalog_form == 'bitamApp.dataApp.users' || aFieldForm.element.catalog_form == 'bitamApp.user.subordinados'){
								arrColumns.push('id');
							}else{
								arrColumns.push('id_' + aFieldForm.element.catalog_form);
							}
							arrColumns.push(aFieldForm.element.catalog_field);
							//si es true, utilizara la llave de la descripcion del catalogo y no la llave del registro.
							var catalog_usedescriptionkey = $.boolean(aFieldForm.element.catalog_usedescriptionkey);
							var sForm = aFieldForm.element.catalog_form;
							var groupBy = [aFieldForm.element.catalog_field];
							var sFilter = aFieldForm.element.catalog_filter || '';
							if(sFilter != ''){
								sFilter = BitamInterface.replaceValuesFilter(sFilter, aFieldForm);
							}
							var buildSelect2Options = function(res){
								var data = {results: []};
								var uniqueDescription = {};
								if(res.collection.length){
									for(var key in res.collection){
										if(res.collection[key][arrColumns[1]]&&!uniqueDescription[res.collection[key][arrColumns[1]]]){
											data.results.push({
												id: (catalog_usedescriptionkey ? res.collection[key]['key_'+arrColumns[1]] : res.collection[key][arrColumns[0]]),
												text: res.collection[key][arrColumns[1]]
											});
											uniqueDescription[res.collection[key][arrColumns[1]]]=1
										}
									}
								}
								// var aSectionForm = aFieldForm.getCatalogForm();
								// if(aFieldForm.tagname == 'widget_field_alphanumeric' && data.results.length == 0 && query.term!=''&&aSectionForm){
									// var arrFieldsKeys = aSectionForm.getArrFieldsNameRequire();
									// if(arrFieldsKeys.length==0 || (arrFieldsKeys.length==1&&$.inArray(aFieldForm.element.catalog_field, arrFieldsKeys)!=-1)){
										// data.results.push({
											// id: 0,
											// text: query.term
										// });
									// }
								// }
								return data;
							}
							bitamApp.getDataWith({
								'columns': arrColumns,
								'form': sForm,
								'filter': sFilter,
								'limit': 100,
								'groupBy':groupBy,
								'showLoadingMsg':false
							}).then(function(resultsSelect){
								var data = buildSelect2Options(resultsSelect);
								var aSection = self.options.section;
								var arrRow = new Object();
								for(var index in aSection.payload.views[sView].fieldsList){
									var aField = aSection.getFieldFormWithId(aSection.payload.views[sView].fieldsList[index].id);
									if(aField.isCatalog()){
										arrRow[aField.id] = new Object();
									}else{
										arrRow[aField.id] = (aField.tagname == 'widget_field_numeric'?'0':null);
									}
								}
								var fieldId = self.options.section.getFieldKey();
								arrRow[fieldId] = null;
								var aField = aSection.getFieldFormWithId(aSection.payload.views[sView].fieldsList[0].id);
								var indexRes = 0;
								results[sView].collection = new Array();
								for(var index in data.results){//collection){
									var rowId = (parseInt(index) + 1) * -1;
									arrRow[fieldId] = String(rowId);
									arrRow[aField.id] = new Object();
									//arrRow[aField.id][data.collection[index][fieldIndex]] = data.collection[index][fieldValue];
									arrRow[aField.id][data.results[index]['id']] = data.results[index]['text'];
									results[sView].collection[index] = $.extend(true, {}, arrRow);
								}
								results[sView].pagination.totalPages = 1;
								results[sView].pagination.totalRows = data.results.length;
								dfd2.resolve(results);
							});
						}else{
							dfd2.resolve(results);
						}
					}else{
						dfd2.resolve(results);
					}
					promise2.then(
						function(res){
							if(self.options.howtowatch != '3' && self.options.howtowatch != '5' )
								self.tBody.empty();
							self._updateInfoPagination(res[sView].pagination);
							if(self.options.arrayMarker && self.options.map) {
								for(var mKey in self.options.arrayMarker) {
									var deleteMarker = self.options.arrayMarker[mKey];
									deleteMarker.marker.setMap(null);
									delete deleteMarker;
								}
								self.options.arrayMarker = new Array();
							}
							
							!!($('#filterBox_'+self.options.name).length || !self.options.buildBoxFilter)||self._updateBoxFilter(res[sView].descriptionTags);
							
							for(var key in res[sView].collection){
								if(self.options.howtowatch != '5') {
									self.tBody.append(self._buildRow({data:res[sView].collection[key]}));
								}
							}
							//2013-12-20@EVEG:(#task,#self._printGantt)Ya con los datos preparados lo asignamos al widget de Gantt
							if(self.options.howtowatch == '5') {
								self._printGantt(res[sView].collection,res[sView].pagination.pageSize==0?5:res[sView].pagination.pageSize);
							}
							
							if(self.options.markersArrayValues && Object.keys(self.options.markersArrayValues).length > 0) {
								//2014-01-17@ARMZ: (#mapsSingleton) Carga del objeto singleton para mapa
								require(['libs/eBavel/maps'], function(bitamMaps) {
									self.options.map = self.options.map || self._getGoogleMap();
									if(self.options.map) {
										self.element.find('#googleMapView'+self.options.name).css('display','block');
										for(var amKey in self.options.markersArrayValues) {
											for(var key in res[sView].collection){
												if(res[sView].collection[key][self.options.section.getFieldKey()] == amKey) {
													self._addGoogleMapsMarker(self.options.markersArrayValues[amKey], self.options.map, res[sView].collection[key], self.options);
												}
											}
										}
										self.options.latlngbounds = new google.maps.LatLngBounds();
										for(var mKey in self.options.arrayMarker) {
											self.options.latlngbounds.extend(self.options.arrayMarker[mKey].point);
										}
										google.maps.event.trigger(self.options.map, 'resize');
										self.options.map.fitBounds(self.options.latlngbounds);
									}
								})
							}
							
							if(self.options.howtowatch != '5')
								self._checkEmptyTable()
							dfd.resolve(res);
							self.element.find('.imgLoading').css('display','none');
							self.element.find("#table_content_"+ self.options.name).css('height','initial');
							$(window).resize();
						})
				})
				//HADR
			}
			
			return dfd.promise();
		},
		
		_buildSecurity: function(){
			var self = this, checkSecCriteriaChange = function(){return true}, 
			checkSecCriteriaRemove = function(){return true}, 
			checkSecCriteriaView = function(){return true};
			if(!$.isEmptyObject(self.options.securityCriteria)){
				if(bitamApp.application.mdVersion >= 3.51) {
					checkSecCriteriaRemove = function(row) { return self.options.securityCriteria.actions_2(row) && self.options.securityCriterionView.actions_2(row) };
					checkSecCriteriaChange = function(row) { return self.options.securityCriteria.actions_3(row) && self.options.securityCriterionView.actions_3(row) };
					
				} else {
					var arrConditions1 = {}, arrConditions2 = {}, arrConditions3 = {};
					for(var kfs in self.options.securityCriteria){
						var condition = self.options.securityCriteria[kfs].condition;
						switch(condition){
							case "=":condition = "==";break;
							case "<>":condition = "!=";break;
						}
						$.each(self.options.securityCriteria[kfs].actions_1, function(i,v){
							if(!$.isArray(arrConditions1[kfs])) arrConditions1[kfs] = [];
							arrConditions1[kfs].push('(typeof row.' + kfs + ' == "object" ? $.inObjectSearch("'+v+'", row.' + kfs + ') '+(condition=="=="? '!':'')+'==false : "'+v+'" '+condition+' row.' + kfs + ")");
						})
						$.each(self.options.securityCriteria[kfs].actions_2, function(i,v){
							if(!$.isArray(arrConditions2[kfs])) arrConditions2[kfs] = [];
							arrConditions2[kfs].push('(typeof row.' + kfs + ' == "object" ? $.inObjectSearch("'+v+'", row.' + kfs + ') '+(condition=="=="? '!':'')+'==false : "'+v+'" '+condition+' row.' + kfs + ")");
						})
						$.each(self.options.securityCriteria[kfs].actions_3, function(i,v){
							if(!$.isArray(arrConditions3[kfs])) arrConditions3[kfs] = [];
							arrConditions3[kfs].push('(typeof row.' + kfs + ' == "object" ? $.inObjectSearch("'+v+'", row.' + kfs + ') '+(condition=="=="? '!':'')+'==false : "'+(typeof v == 'string' ? v.toLowerCase() : v)+'" '+condition+' (typeof row.' + kfs + ' == "string"?row.' + kfs + '.toLowerCase():row.' + kfs + ' ))');
						})
					}
					var tmpCondition = [];
					if(!$.isEmptyObject(arrConditions1)){
						for(var key in arrConditions1)tmpCondition.push(arrConditions1[key].join('||'));
						var sFunc = "1&&function(row){ return (" + tmpCondition.join("&&") + ")}";
						checkSecCriteriaView = eval(sFunc);
					}
					tmpCondition = [];
					if(!$.isEmptyObject(arrConditions2)){
						for(var key in arrConditions2)tmpCondition.push(arrConditions2[key].join('||'));
						var sFunc = "1&&function(row){ return (" + tmpCondition.join("&&") + ")}";
						checkSecCriteriaRemove = eval(sFunc);
					}
					tmpCondition = [];
					if(!$.isEmptyObject(arrConditions3)){
						for(var key in arrConditions3)tmpCondition.push(arrConditions3[key].join('||'));
						var sFunc = "1&&function(row){ return (" + tmpCondition.join("&&") + ")}";
						checkSecCriteriaChange = eval(sFunc);
					}
				}
			}
			
			this._checkSecCriteriaChange = checkSecCriteriaChange; 
			this._checkSecCriteriaRemove = checkSecCriteriaRemove;
			this._checkSecCriteriaView = checkSecCriteriaView;
		},
		
		_getGoogleMap: function() {
			var self = this;
			var map = bitamMaps.getGoogleMap({mapDOM: self.element.find('#googleMapView'+self.options.name).get(0)});
			return map;
		},
		
		_addGoogleMapsMarker: function(value, map, data, options) {
			if(value.indexOf("undefined") < 0){
				var self = this;
				var coords = value.split(',');
				if($.trim(coords[0]) == '' || $.trim(coords[1]) == '') {
					return;
				}
				var latitudtmp = $.trim(coords[0]);
				var longitudtmp = $.trim(coords[1]);
				var point = new google.maps.LatLng(latitudtmp, longitudtmp);
				if(!self.options.arrayMarker) {
					self.options.arrayMarker = new Array();
				}
				self.options.arrayMarker[data[options.section.getFieldKey()]] = { point: point };
				/*var latlngbounds = new google.maps.LatLngBounds();
				for(var mKey in self.options.arrayMarker) {
					latlngbounds.extend(self.options.arrayMarker[mKey].point);
				}
				self.options.map.fitBounds(latlngbounds);*/
				var marker = new google.maps.Marker({ position: point, title: "", map: self.options.map});
				//2014-01-17@ARMZ: (#mapsSingleton) Carga del objeto singleton para mapa
				bitamMaps.addMarker(marker, 'googleMapView'+self.options.name);
				$.extend(self.options.arrayMarker[data[options.section.getFieldKey()]], { marker: marker });
				var color = 'FE7569';
				var alarmCriterias = self.options.section.payload.views[self.options.sectionView].alarmCriterias;
				if(alarmCriterias.length > 0) {
					for(var cKey in alarmCriterias) {
						var eachAlarm = alarmCriterias[cKey];
						//for(var keyHeader in options.tableHeaderColumn){
						for(var keyHeader in options.fieldsForm){
							var eachHeader = options.fieldsForm[keyHeader];
                            //var eachHeader = aFieldForm;
							var conditionFieldID = eachAlarm.fieldForm.name;
							if(conditionFieldID == eachHeader.id) {
								var conditionFieldValue = data[eachHeader.id];
								
								if($.isPlainObject(conditionFieldValue)) {
									for(var avKey in conditionFieldValue) {
										conditionFieldValue = conditionFieldValue[avKey];
										break;
									}
								} else {
									if(conditionFieldValue && conditionFieldValue == 'null') {
										conditionFieldValue = '';
									}
								}
								
								var conditionValue = eachAlarm.value;
								var evalCondition = '';
								switch(eachAlarm.condition) {
									//is
									case '5': condition = "=="; break;
									//isn't
									case '6': condition = "!="; break;
									//contains
									case '7': evalCondition = (conditionFieldValue.indexOf(conditionValue) != -1)?true:false; break;
									//doesn't contains
									case '8': evalCondition = (conditionFieldValue.indexOf(conditionValue) == -1)?true:false; break;
									//starts with
									case '9': evalCondition = (conditionFieldValue.indexOf(conditionValue) == 0)?true:false; break;
									//ends with
									case '10': evalCondition = (conditionFieldValue.indexOf(conditionValue, conditionFieldValue.length - conditionValue.length) != -1)?true:false; break;
									//is empty
									case '11': evalCondition = (conditionFieldValue == '')?true:false; break;
									//is not empty
									case '12': evalCondition = (conditionFieldValue == '')?false:true; break;
									case '4': condition = "!="; break;
									case '3': condition = "<"; break;
									case '2': condition = ">"; break;
									case '1':
									default: condition = "=="; break;
								}
								if(eachAlarm.condition < 7) {
									if(eachAlarm.condition <= 4) {
										evalCondition = conditionFieldValue + condition + conditionValue;
									} else {
										evalCondition = "'"+conditionFieldValue+"'"+ condition +"'"+ conditionValue+"'";
									}
								}
								if(eval(evalCondition)) {
									color = alarmCriterias[cKey].color;
									break;
								}
							}
						}
					}
				}
				var pinColor = color.replace('#', '');
				marker.setIcon("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor);
				var contentString = '<div>';
				var arrayShownFields = new Array();
				var i=0;
				if(options.tableHeaderColumn.length > 0) {
					contentString += '<div style="height:30px;" id="current_register_'+data['id_'+self.options.section.name]+'">';
					contentString += '<a style="font-size: 14px; text-decoration: underline" >Edit</a><br>';
					contentString += '</div>';
				}
				for(var keyHeader in options.tableHeaderColumn){
					var eachHeader = options.tableHeaderColumn[keyHeader];
					var aValue = data[eachHeader.id];
					
					if($.isPlainObject(aValue)) {
						for(var avKey in aValue) {
							aValue = aValue[avKey];
							break;
						}
					} else {
						if(aValue && aValue == 'null') {
							aValue = '';
						}
					}
					
					var fieldsForm = self.options.section.payload.fieldsForm;
					for(var fKey in fieldsForm) {
						if(fieldsForm[fKey].id == eachHeader.id) {
							if(fieldsForm[fKey].tagname!='widget_signature' && fieldsForm[fKey].tagname!='widget_photo'){
								var aField = fieldsForm[fKey].label;
								break;
							}else{
								var aField =undefined;
								break;
							}	
						}
					}
					if(aValue === null) {
						aValue = '&nbsp&nbsp&nbsp';
					}
					//if(aValue) {
					//@SM 2014-04-07 Issue#32321 Se comentan lineas contentString para que no muestre las fotos en la ventana del marcador
						if(typeof aValue == 'string' && aValue.indexOf('uploads/')>=0) {
							if(bitamApp.isMobile.isApp()) {
								//contentString += '<div style="height:25px;"><b style="font-size: 12px">'+aField+':&nbsp&nbsp&nbsp</b><label style="font-size:12px;"><img src="data:image/jpeg;base64,'+aValue+'" WIDTH=50 HEIGHT=50></label></div>';
							}
							else {							
								//contentString += '<div style="height:25px;"><b style="font-size: 12px">'+aField+':&nbsp&nbsp&nbsp</b><label style="font-size:12px;"><img src="../../'+aValue+'" WIDTH=50 HEIGHT=50></label></div>';
							}
						}
						else if(typeof aValue == 'string' && aValue.indexOf('"lx":')>=0) {
							/*contentString += '<div class="sigPadShow" style="margin-top: 5px;"><b style="font-size: 12px">'+aField+':&nbsp&nbsp&nbsp</b><label style="font-size:12px;">'+aValue+'</label></div>';
							aValue = aValue.replace(/lx/g, '\"lx\"');
							aValue = aValue.replace(/ly/g, '\"ly\"');
							aValue = aValue.replace(/mx/g, '\"mx\"');
							aValue = aValue.replace(/my/g, '\"my\"');
							debugger;
							$('.sigPadShow').signaturePad({ displayOnly: true }).regenerate(aValue);*/
						}
						else 
						if(bitamApp.isMobile.isApp() && $.isset(aField)) {
							var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$");							
							if(!base64Matcher.test(aValue)){
								contentString += '<div style="height:25px; padding-bottom: 10px;"><b style="font-size: 14px">'+aField+':&nbsp&nbsp&nbsp</b><label style="font-size:12px;">'+aValue+'</label></div>';
							}
						} else if($.isset(aField)){
							contentString += '<div style="height:25px; padding-bottom: 10px;"><b style="font-size: 14px">'+aField+':&nbsp&nbsp&nbsp</b><label style="font-size:12px;">'+aValue+'</label></div>';
						}
					//}
				}
				contentString += '</div>';
				var infowindow = new google.maps.InfoWindow({
					content: contentString,
					maxWidth: 700
				});
				var MyID = data['id_'+self.options.section.name];
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(self.options.map, marker);
					setTimeout($.proxy(function() {
						var self = this.self;
						$('#current_register_'+MyID).on('click', {id:MyID}, $.proxy(function(e){
							var self = this.self;
							self.view(e.data.id, {viewonly: !self._checkSecCriteriaChange(data) || !self.options.canEdit});
						}, {self: self}));
					}, {self: self}), 100);
				});
				/*setTimeout($.proxy(function() {
					var self = this;
					google.maps.event.trigger(self.map, 'resize');
					self.map.fitBounds(self.latlngbounds);
				}, {map: map, point: point, latlngbounds: latlngbounds}), 100);*/
				/*if(Object.keys(self.options.arrayMarker).length == 1) {
					self.options.map.setCenter(marker.getPosition(), 16);
				}*/
			}
		},
		
		_buildRow: function(options){
			//2013-11-26@RTORRES:(#iphoneUse) cuando es iPhone solamente hace vistas default
			/*if(bitamApp.isMobile.pantallasreducidas()&& !this.options.isDetail)
			{
				this.options.howtowatch = '2';
			}*/
			switch(this.options.howtowatch){
				/*case '2':
					// como tiene la opcion de vista metro construlle un row metro
					return this._buildRowMetro(options);
					break;*/
				case '3.1':
					/*
					29/08/2013
					Contruye un TR con un TD, pero las columnas son DIVs donde el primero ocupa todo el primer renglon con un width de 100% y las demas en el siguiente renglon con block-inline
					*/
					return this._buildRowTRWithDiv(options);
					break;
				default:
					return this._buildRowDefault(options);
			}			
		},
		
		/*
		28/08/2013@aramirez
		options.data => Objeto de datos de un solo ROW
		options.eachHeader => Objeto de la cabecera de la tabla/vista
		*/
		_prepareDataRow: function(options){
			var self = this, data = options.data, lastSyncDate = BitamStorage.getLastSyncDate(),
			keyReg = data[self.options.section.getFieldKey()];
			//eachHeader = self.options.tableHeaderColumn[keyHeader],
			eachHeader = options.eachHeader;
			var aFieldForm = searchFieldFormWithID(eachHeader.id, self.options.fieldsForm), value,
			aFieldForm = new FieldForm(aFieldForm);
			if (typeof aFieldForm.id === "undefined" && eachHeader.isCalculated) /* 2013-11-08 JRPP(#customField)*/
			{
				aFieldForm.id = eachHeader.id;
				value = data[aFieldForm.id];
				/* Se comprueba si el campo calculado es un Date o un Numeric para que se le asigne el TagName 
				   correspondiente y así se le respete el Formato(si es que se le definio anteriormente) */
				aFieldForm.tagname = ($.isNumeric(value) ? 'widget_field_numeric' : 'widget_field_date' );
				
				aFieldForm.name = eachHeader.name;
				aFieldForm.element = new Object();
				aFieldForm.element.edit = "1";
				aFieldForm.element.insert = "1";
				aFieldForm.valid = new Object();
			}
			else{
				value = data[aFieldForm.id];
			}
			if(isset(self.options.fieldsGroup) && self.options.fieldsGroup.length > 0 && eachHeader.id == self.options.fieldsGroup[0].id){
				value = null;
			}
			
			if(eachHeader.type == "sectionindialog"){
				value = "<a href='javascript:bitamApp.viewFloatDetail({section:\""+this.widget.name+"\", sectiondetail:\""+ eachHeader.section +"\", key:\""+  keyReg +"\"})'>open</a>";
			}
			
			if(value != null && value != ''){
				switch(aFieldForm.tagname){
					case 'widget_colorpicker':
						value = '<div style="width:15px; height: 15px; background-color:'+value+'"></div> ';
						break;
					case 'widget_geolocation':
						if(typeof google !== undefined){
							if(!self.options.markersArrayValues) {
								self.options.markersArrayValues = new Array();
							}
							self.options.markersArrayValues[keyReg] = value;
							viewWidgetIcon = 'images/map.gif';
							if(!bitamApp.isMobile.isApp()) {
								viewWidgetIcon = '../../'+viewWidgetIcon;
							}
							value = '<div style="width:50px;" onclick="event.stopImmediatePropagation(); bitamApp.showModal(\''+value+'\', \''+aFieldForm.tagname+'\')"><img src="'+viewWidgetIcon+'"/></div>';
						}
						break;
					case 'widget_photo':
					case 'widget_signature':
						if(aFieldForm.tagname == "widget_photo") {
							//var viewWidgetIcon = 'images/icono_camara.png';
						} else if(bitamApp.isMobile.isApp()) {
							var viewWidgetIcon = 'images/firma.gif';
						} else {
							var viewWidgetIcon = '../../images/firma.gif';
						}
						if(viewWidgetIcon){
							value = viewWidgetIcon;
						} else {
						if(!bitamApp.isMobile.isApp()) {
							
							var ext = value.split('.').pop();
							if (ext == 'jpg' || ext == 'jpeg') {
									value = '../../'+value;
								} else { 
										value = 'data:image/jpeg;base64,'+value;
									} 
						} else { 
							value = 'data:image/jpeg;base64,'+value; 
							}
						value = value.replace(/"/g, '');
						var value2 = value;
						}
						//2013-11-11 @SM Hacer que se carguen las imagenes en vez de iconos y que no se abra el showmodal
						//value='<div style="width:50px;" onclick="event.stopImmediatePropagation(); bitamApp.showModal(\''+value+'\', \''+aFieldForm.tagname+'\')"><img class="imagen" src="../../'+value+'"/></div>';
						value='<div style="width:50px;" "><img class="imagen" src="'+value+'"/></div>';					
						break;
					case 'widget_file':
						var arrDocuments = value.split(",");			
						var sLink = "";
						if(bitamApp.isMobile.isApp()){
							sLink += bitamApp.buildURL('uploads//'+bitamApp.application.codeName+'//'+self.options.section.name, true);
						} else {
							sLink += bitamApp.getURLServer();
							sLink +="/action.php?action=downloadFile&sectionName="+self.options.section.name;
							sLink +="&applicationCodeName="+bitamApp.application.codeName;
						}
						for(var x in arrDocuments){
							if(isset(arrDocuments[x])){
								var sB=arrDocuments[x];
								sB=sB.split("/");
								var LinkPro = sB[3];
								sB[3] = sB[3].substring(8,sB[3].length);
								if(bitamApp.isMobile.isApp()) {
									var sLinkDocument = sLink+'//'+LinkPro;
									var eee = '<a onclick=\'event.stopImmediatePropagation()\' data-ajax=false href=\'javascript: bitamApp.showModal(\"'+sLinkDocument+'\", \"'+aFieldForm.tagname+'\");\'>'+sB[3]+'</a>';
								} else {
									var sLinkDocument = sLink+"&file="+LinkPro;
									var eee = "<a onclick='event.stopImmediatePropagation()' data-ajax=false href=\""+sLinkDocument+"\">"+sB[3]+"</a>";
								}
								arrDocuments[x]=eee;	
							}
						}
						value=arrDocuments.join(",");
						break;
					case 'widget_textarea':
						value = self._replaceBBCode(value);
						break;
					case 'widget_field_alphanumeric':
						if(!aFieldForm.isCatalog()){
							value = self._replaceBBCode(value);
						}
						break;
					case 'widget_field_date':
					case 'widget_field_time':
						if(isset(eachHeader.format))
							value = Date.parse(value).toString(eachHeader.format);
						break;
					//2013-11-21@ARMZ: (#widgetCalculatedField) Nuevo campo calculado, no se guarda el valor en la base de datos, solo se calcula en memoria
					case 'widget_calculatedfield':
					case 'widget_field_numeric':
						if(eachHeader.format)
							value = formatString(value, eachHeader.format, 'numeric');
						break;
					case 'widget_field_rating':
						/*if(eachHeader.format)
							 value = formatString(value, eachHeader.format, 'numeric');*/
						var valor = value;
						value = $("<div>").rating({call:1, size:aFieldForm.element.size, value:valor, images:aFieldForm.element.images});
						break;
				}

				if ( aFieldForm.isString() && !aFieldForm.isCatalog() || aFieldForm.isText() ) {
					//2013-11-26@RTORRES:(#iphoneUse) cuando es iOS no aplica la opcion more,less
					if((!options.longText && value && ( value.length > self.options.readMoreMinLength + 10 ) ) && !bitamApp.isMobile.pantallasreducidas()) {
						
						var longString = value;
						value = $('<div class="lngstr" style="display: inline-block;">'+ longString.substring(0, self.options.readMoreMinLength) +'</div><div title="'+ $.t('Show more') +'" class="shmtxt" style="cursor:pointer; display: inline-block;padding: 9px;vertical-align: middle;opacity: .2;"><div style="content:url(\''+ bitamApp.buildURL('images/ArrowTurnMore.svg') +'\')"></div></div>');
						value.filter('.shmtxt').on('click', function(e) {
							
							self._readMoreText(this, value, longString, options.rowElement, options.model, aFieldForm);

							return false;
						});
						delete longString;
					}
				}
				
				if(aFieldForm.isCatalog() || aFieldForm.catalog){
					if(isset(aFieldForm.catalog) && isset(bitamApp.dataApp['catalog'][aFieldForm.catalog])){
						if(isset(bitamApp.dataApp['catalog'][aFieldForm.catalog][value]))
							value = bitamApp.dataApp['catalog'][aFieldForm.catalog][value]['value']
						
						if(aFieldForm.catalog == 'fieldtypes'){
							for(var keyCat in bitamApp.dataApp['catalog'][aFieldForm.catalog]){
								if(bitamApp.dataApp['catalog'][aFieldForm.catalog][keyCat].value == value){
									value = bitamApp.dataApp['catalog'][aFieldForm.catalog][keyCat].description;
									break;
								}
							}
						}
					}
					if(aFieldForm.catalog == 'users'){
						if(isset(bitamApp.dataApp[aFieldForm.catalog][value]))
							value = bitamApp.dataApp[aFieldForm.catalog][value]['nom_largo']
					}			
					if(isset(aFieldForm.catalog) && aFieldForm.catalog.substring(0, 12) == "dataCatalog("){
						var valueDataCatalog = eval(replaceValueString(aFieldForm.catalog, ($.isPlainObject(self.options.filter)?$.extend({}, data, self.options.filter):data)));
						if($.isPlainObject(value)){
							var tmpValue = {};
							for(var keyDataCatalog in valueDataCatalog){
								if($.inObjectSearch(valueDataCatalog[keyDataCatalog].value, value)){
									tmpValue[valueDataCatalog[keyDataCatalog].value] = valueDataCatalog[keyDataCatalog].label;
								}
							}
							if(!$.isEmptyObject(tmpValue)){
								value = tmpValue;
							}
						}else{
							for(var keyDataCatalog in valueDataCatalog){
								if(valueDataCatalog[keyDataCatalog].value == value){
									value = valueDataCatalog[keyDataCatalog].label;
									break;
								}
							}
						}
					}			
					//si el valor proviene del campo de otro formulario
					if(isset(aFieldForm.element.catalog_form) && $.isNumeric(value)){
						var secName = aFieldForm.element.catalog_form;
						var fieldName = aFieldForm.element.catalog_field;
						if(secName.toLowerCase() == "bitamApp.dataApp.users".toLowerCase() && !$.isPlainObject(value)){
							var dataUser = bitamApp.dataApp.users;
							if(!$.isEmptyObject(dataUser))
								value = dataUser[value][fieldName];
						}else{
							var aSection = bitamApp.application.getSection(secName);					
							var fieldsForms =  aSection.payload.fieldsForm;
							var fieldForm = null;
							for(var key in fieldsForms){
								if(fieldsForms[key].id == fieldName){
									fieldForm = fieldsForms[key];
								}
							}						
							//mejor en lugar de hacer un .filter() vamos directo a la coleccion de datos, es mucho mas rapido
							var dataUser = bitamApp.dataUser[aSection.id].collection[value];
							if(isset(aFieldForm) && (aFieldForm.tagname == "widget_selectionhierarchy")){       
								var valuehierarchy = "";
								for(var key1 in fieldsForms){
									if(fieldsForms[key1].id.indexOf("createdDate_")==0||fieldsForms[key1].id.indexOf("modifiedDate_")==0||fieldsForms[key1].id.indexOf("modifiedUser_")==0||fieldsForms[key1].id.indexOf("createdUser_")==0){
										continue;
									}
									var sDescr = dataUser[fieldsForms[key1].id];
									if($.isPlainObject(sDescr)){
										for(var ks in sDescr) break;
										sDescr = sDescr[ks];
									}
									valuehierarchy = valuehierarchy+", "+sDescr;
									if(fieldsForms[key1].id == aFieldForm.element.catalog_field_p){
										break;
									}
								}
								valuehierarchy = valuehierarchy.substring(1);
								value = valuehierarchy;
							}else if(!$.isEmptyObject(dataUser)){
								value = dataUser[fieldForm.id];
							}
						}
					}
				}
				// se cambio que los campos catalogos en ves de ser array fueran objetos indexados por el key del catalogo.
				if($.isPlainObject(value) || $.isArray(value)){
					var sValue = '';
					for(var keyC in value){
						sValue = (sValue == '' ? value[keyC] : sValue + ", " + value[keyC]);
					}
					value = sValue;
					delete sValue;
				}
			}else{
				value = "&nbsp;"
			}

			return value;
		},

		_readMoreText: function(el, $el, longString, aModel) {
			
			var ctnr = $el.filter('.lngstr');
							
			if($el.data('lng')) {
				ctnr.html(longString.substring(0, this.options.readMoreMinLength));
				$el.data('lng', false)
				el.title = $.t('Show more');
			} else {
				ctnr.css('max-width', (ctnr.width() * 2)).html(longString);
				$el.data('lng', true)
				el.title = $.t('Show less');
			}
		},
		
		_buildRowDefault: function(options){
			var self = this, data = options.data, lastSyncDate = BitamStorage.getLastSyncDate(),
			keyData = 0
			keyReg = data[self.options.section.getFieldKey()],
			myTR = $("<tr/>", {"id":"table_"+self.options.name+"_tr_"+keyReg, "data-nrow":keyData, "data-id":keyReg});
			myTR.data('data', $.extend(true, {}, data));
				
			for(var keyHeader in self.options.tableHeaderColumn){
				//2013-11-16@EVEG :(#isVisibleForUser,#aFieldForm) Si el campo no es visible para el usuario no pinta la columna
				var eachHeader = self.options.tableHeaderColumn[keyHeader]
				var aFieldForm = self.options.section.getFieldFormWithId(eachHeader.id);
				if(aFieldForm==false){
					eachSection=bitamApp.application.getSection(eachHeader.sectionName);
					var aFieldForm =  eachSection && eachSection.getFieldFormWithId(eachHeader.id);				
				}
				if(aFieldForm && !aFieldForm.isVisibleInView())
					continue;
				var className = '', eachHeader = self.options.tableHeaderColumn[keyHeader], value;
				if(eachHeader['class']){
					className += ' ' + eachHeader['class'];
				}
				var myTD = $("<td/>", {"class":className, "id":"table_"+self.options.name+"_tr_"+keyReg+"_td_"+eachHeader.id}).data('field', eachHeader.id);
				
				if(eachHeader.type == 'actionForm'){ 
					myTR.append(myTD.append($('<div/>',{'title':eachHeader.label,'style':'width:20px;height:20px;background-image:url('+bitamApp.buildURL(eachHeader.ico)+');background-repeat:no-repeat;margin-left: auto;margin-right: auto;'}).on('click', {'eachHeader':eachHeader, 'keyReg':keyReg},function(event){
						event.stopImmediatePropagation();
						executeActionForm({data:{eachActionForms:event.data.eachHeader.actionForm, selected:[event.data.keyReg],'self':self, vieworregister:true}})
					})));
					continue;
				}
				
				value = self._prepareDataRow({data:data, eachHeader:eachHeader, 'longText':self.options.howtowatch == 3, rowElement: myTR, model: options.model});
				if(self.options.canEdit && this._checkSecCriteriaChange(data) && eachHeader.contentEditable && !(isset(self.options.section.payload.processForms) && (isset(self.options.section.payload.processForms[0]) && self.options.section.payload.processForms[0].regBlock == true && isset(self.options.section.payload.processForms[0].arrKeysBlocked[keyReg])))){
					//cuando es un tipo checkbox sencillo le damos la funcionalidad a la vista que con solo un click pueda cambiar su valor de encendido/apagado
					/*28/08/2013@Hay que volver  a programar el checkbox single, actualmente no esta disponible en las aplicaciones finales de los clientes*/
					myTD.append($("<div/>", {"class":"cell", "style":"cursor:text"}).html(value).click({"widget": this.widget.name, "key":keyReg}, function(event){event.stopImmediatePropagation();self.edit(event.data.key);}));
					var aFieldForm = searchFieldFormWithID(eachHeader.id, self.options.fieldsForm);
					//2013-11-26@RTORRES:(#FRKPP5)Poner el numerico con el alineamiento de texo a la derecha.
					if(eachHeader.isCalculated || aFieldForm.tagname == 'widget_field_numeric' || aFieldForm.element.type == 'numeric')
					{
					    myTD.css("text-align","right" );
					}
				}
				else{
					myTD.html($("<div/>", {"class":"cell"}).html(value));
					var aFieldForm = searchFieldFormWithID(eachHeader.id, self.options.fieldsForm);
					//2013-11-26@RTORRES:(#FRKPP5)Poner el numerico con el alineamiento de texo a la derecha.
					if(eachHeader.isCalculated || aFieldForm.tagname == 'widget_field_numeric' || aFieldForm.element.type == 'numeric')
					{
					    myTD.css("text-align","right" );
					}
				}
				//2013-12-06@RTORRES:(#COOX5E) cuando se carga la seccion se le adjuntan las propieades de cada TD
				if (!eachHeader.isCalculated && bitamApp.application.mdVersion >= 4)
				{
				   self.applyDynamicProperties(myTD, aFieldForm,data);
				}
				myTR.append(myTD);
			}
			//EVEGA-
			myTR.prepend($("<td/>", {"class":"table_newitem " + (data.modifiedDate > bitamApp.user.lastAccess && bitamApp.user.cla_usuario != data.modifiedUserKey?"ico":"")}));
			if(bitamApp.isMobile.isApp()){  //2013-11-26@RTORRES:(#UKUO3H)Identificar Registro Modificados en Browser.
				myTR.prepend($("<td/>", {"class":"table_syncitem " + (data.syncDate > lastSyncDate?"ico":""), title:$.t("No data sync")}));
			}
			
			//columna para agregar la imagen de ordenamiento
			if(self.options.sortable){
				var sortableTD = $("<td/>", {"style":"width: 20px; height: 20px;"}).html("<a style='display:none' class='remove-element' title='Sort'><img src='"+bitamApp.buildURL("images/arrowsmove.png")+"'/></a>");
				if(myTR.find(".checkItemSelect").length)
					myTR.find(".checkItemSelect").parent().parent().css("width", 22).after(sortableTD);
				else
					myTR.prepend(sortableTD);
				delete sortableTD;
			}
			
			//agrega la funcionalidad de borrado de registro
			//2013-12-24@ARMZ: Para pantallas reducidas pintamos el checkbox (lo oculta el responsive de la tabla) y el botecito para eliminar registros
			if(false && !bitamApp.isMobile.pantallasreducidas() && !bitamApp.isMobile.iPad() && !self.options.canDelete){
				/*2014-04-22 @JRPP (#DeletedDELET) Quitar opcion de eliminar */
				//myTR.append($("<td/>", {"class":"remove-element", "style":"width: 20px; height: 20px;"}));
			} else {
				if((self.options.canDelete || self.options.section.payload.actionforms.length > 0) && !(isset(self.options.section.payload.processForms) && isset(self.options.section.payload.processForms[0]) && self.options.section.payload.processForms[0].regBlock == true && isset(self.options.section.payload.processForms[0].arrKeysBlocked[keyReg]))){       
					var id = "table_check_"+self.options.name+"_tr_"+keyReg;
					//al principio del renglon agrega un checkbox
					var inputCheckBox = $("<input/>", {
						"class":"checkItemSelect",
						"data-role":"none", 
						"type":"checkbox" , 
						"name":this.widget.name , 
						"value":this.widget.name,
						"id":id,
						"data-key":keyReg
					}).on("click", {"idTR" : myTR.attr("id")}, function(event){
						event.stopPropagation();
						if(this.checked){
							$("#" + event.data.idTR).addClass("tr-selected");
						}
						else{
							$("#" + event.data.idTR).removeClass("tr-selected");
						}
						//$(event.currentTarget).find("input[type='checkbox']").click();
					})
					//debugger;
					if($('#contentlist_'+self.options.name).find('thead .table_checkitem').css('display') != 'none')
					{
						myTR.prepend($("<td/>", {"class":"table_checkitem"}).append(
							inputCheckBox
						));
					}
					inputCheckBox.checkbox();
					delete inputCheckBox;
					//agrega al final una imagen para borrado del registro
					/*2014-04-22 @JRPP (#DeletedDELET) Quitar opcion de eliminar */
					if(/*!bitamApp.isMobile.iPad()*/ bitamApp.isMobile.pantallasreducidas() && self.options.canDelete)
					{
					 	/*myTR.append($("<td/>", {"style":"width: 20px; height: 20px;"}).append(
					 		$("<a/>", {
					 			"style":'display:' + (bitamApp.isMobile.pantallasreducidas() ? 'block':'none'),
					 			"class":'remove-element',
					 			"href":'javascript:;',
					 			"title":'Remove'}
					 		).append($("<img/>", {
					 					"src":bitamApp.buildURL("images/icon-trash.png")
					 				}).click({"widgetName":self.options.name, "keyReg":keyReg}, function(event){
					 					event.stopImmediatePropagation();
					 					self.remove(event.data.keyReg);
					 				})
					 		)));*/
								myTR.find('.table_checkitem').show();

					 	if(self._checkSecCriteriaRemove(data)){
					 		if(!bitamApp.isMobile.pantallasreducidas()) {
					 			myTR.mouseenter(function() {
					 					var a = $(this).find(".remove-element");
					 					self.options.canDelete && a.css("display", "block");
					 			});
					 			myTR.mouseleave(function() {
					 					var a = $(this).find(".remove-element");
					 					a.css("display", "none");
					 			});
					 		}
					 	}else{
					 		//si no tiene permisos de eliminar este registro, eliminamos la imagen del TR
					 		myTR.find("a").remove();
					 	}
					}
				}
				else{
					if(self.options.canDelete)
					{
						myTR.prepend($("<td/>", {"style":"width: 18px; height: 13px; margin-right: 18px; position: none;"}));
					}
					myTR.append($("<td/>", {"style":"width: 20px; height: 20px;"}));
				}
			}
			
			if(self.options.canView == true || $.isFunction(self.options.canView)){
				myTR.css({"cursor":"pointer"});
				myTR.click({"widget": self.options.name, "key":keyReg, viewonly:!self._checkSecCriteriaChange(data) || !self.options.canEdit}, function(event){
					//2013-12-13@MABH: (#30983) Se agrego para que no se propagara al custom event click.forminline que se adjuntaba
					//a window al mostrar la forma inline, ya que entraba automaticamente a esta funcion y eliminaba l vista del form creado
					event.stopImmediatePropagation();
					
					var TR = $("#table_"+event.data.widget+"_tr_"+event.data.key);
					if(!(TR.data('openViewForm') || TR.data('editing'))){
						TR.data('openViewForm', true);
						//para evitar abrirlo dos veces (doble click)
						setTimeout(function(){TR.data('openViewForm', false);}, 1000)
						self.view(event.data.key, {viewonly:event.data.viewonly});
					}					
				});
			}
			if(self.options.section.payload.views[self.options.sectionView].ViewCustomization) {
				var width = self.options.section.payload.views[self.options.sectionView].ViewCustomization.registerwidth;
				if(width == "1" && isset(width))
					myTR.css({"height":"25px"});
				else if(width == "2" && isset(width))
					myTR.css({"height":"50px"});
				else if(width == "3" && isset(width))
					myTR.css({"height":"80px"});
				var fontfields = self.options.section.payload.views[self.options.sectionView].ViewCustomization.fontfields;
				Customization(myTR.find(':not([data-dynamic="true"]) span'),fontfields,"texto");
			}
			
			/*
			28/08/2013@aramirez
			Si es el tipo de vista minimizada, y tiene solo un detalle, agregamos ese detalle en la misma vista de la tabla debajo del renglon del registro
			*/
			if(this.options.howtowatch == 3 && self.options.section.sections && Object.keys(self.options.section.sections).length == 1){
				/*
				02/10/2013@ARAMIREZ
				colspan es el numero de columnas del padre mas dos columnas mas que son las notificaciones de registros nuevos y registros nuevos generados despues del ultimo login
				*/
				var $trDetail = $('<tr><td class="simplified-details-container" colspan="'+ (self.options.tableHeaderColumn.length+2) +'"></td></tr>'), properties = {}/*, aFilter = {}*/;
				//aFilter[self.options.section.sections[Object.keys(self.options.section.sections)[0]].FDTKey] = data[self.options.section.getFieldKey()];
				(properties['filter'] = {})[self.options.section.sections[Object.keys(self.options.section.sections)[0]].FDTKey] = data[self.options.section.getFieldKey()];
				properties['name'] = 'simplified-details-' + data[self.options.section.getFieldKey()];
				properties.canView = false;
				properties.pageSize = 0;
				properties.currentPage = 0;				
				properties.howtowatch = '3.1';
				$trDetail.find('td').append(
					bitamApp.application.getSection(Object.keys(self.options.section.sections)[0]).getViewWithID(self.options.section.sections[Object.keys(self.options.section.sections)].viewName).buildHTML(properties).addClass('simplified-details')
				);
				return [myTR, $trDetail];
			}			
			return myTR;
		},
		//2013-12-06@RTORRES:(#COOX5E) cuando se carga la seccion se le adjuntan las propieades de cada TD
		applyDynamicProperties: function($el, field, data) {
		    var anObjectStyle = this.options.section.getStyleDynamicProperties(field, data);
		    if(!$.isEmptyObject(anObjectStyle))
		    {
			$el.attr("data-dynamic","true");
		    }
		    $el.css(anObjectStyle);
		    //$el.attr("data-dynamic","true");

		},
		
		_buildRowTRWithDiv: function(options){
			var self = this, data = options.data, lastSyncDate = BitamStorage.getLastSyncDate(),
			keyData = 0, keyReg = data[self.options.section.getFieldKey()],
			myTR = $("<tr/>", {"id":"table_"+self.options.name+"_tr_"+keyReg, "data-nrow":keyData, "data-id":keyReg}).data('data', $.extend(true, {}, data)),
			myTD = $("<td/>", {"class":'', 'colspan':self.options.tableHeaderColumn.length});
			
			for(var keyHeader in self.options.tableHeaderColumn){
				var eachHeader = self.options.tableHeaderColumn[keyHeader], value;
				//2013-11-16@EVEG :(#isVisibleForUser,#aFieldForm) Si el campo no es visible para el usuario no pinta la columna
				var aFieldForm = self.options.section.getFieldFormWithId(eachHeader.id);
				if(aFieldForm==false){
					eachSection=bitamApp.application.getSection(eachHeader.sectionName);
					var aFieldForm = eachSection.getFieldFormWithId(eachHeader.id);				
				}
				if(aFieldForm && !aFieldForm.isVisibleInView())
					continue;
				value = self._prepareDataRow({data:data, eachHeader:eachHeader, 'longText':true});
				myTD.append($('<div/>', {"id":"table_"+self.options.name+"_tr_"+keyReg+"_td_"+eachHeader.id}).data('field', eachHeader.id).append(value));
			}
			myTR.append(myTD);
			return myTR;
		},
		
		_buildRowMetro: function(options){
			var self = this, data = options.data, lastSyncDate = BitamStorage.getLastSyncDate(),
			keyData = 0;
			keyReg = data[self.options.section.getFieldKey()],
			myTR = $("<div/>", {"id":"table_"+self.options.name+"_tr_"+keyReg,"class":"metroRow", "data-nrow":keyData, "data-id":keyReg});
			myTR.data('data', $.extend(true, {}, data));
			var myTD = $("<div/>", {"class":className});
			/*f(this._checkSecCriteriaView(data))
				return '';*/		
			var firts = true;
			var isFirst = true;
			for(var keyHeader in self.options.tableHeaderColumn){
				var className = '';
				var eachHeader = self.options.tableHeaderColumn[keyHeader];
				
				if(eachHeader['class']){
					className += ' ' + eachHeader['class'];
				}
				myTD.attr("id","table_"+self.options.name+"_tr_"+keyReg+"_td_"+eachHeader.id);
				myTD.data('field', eachHeader.id);
				
				if(eachHeader.type == 'actionForm'){
					myTD.append($('<div/>',{'title':eachHeader.label,'style':'width:20px;height:20px;background-image:url('+bitamApp.buildURL(eachHeader.ico)+');background-repeat:no-repeat;margin-left: auto;margin-right: auto;'}).on('click', {'eachHeader':eachHeader, 'keyReg':keyReg},function(event){
						event.stopImmediatePropagation();
						executeActionForm({data:{eachActionForms:event.data.eachHeader.actionForm, selected:[event.data.keyReg],'self':self, vieworregister:true}})
					}))
					myTR.append(myTD);continue;
				}
			
				var value = data[eachHeader.id];
				if(isset(self.options.fieldsGroup) && self.options.fieldsGroup.length > 0 && eachHeader.id == self.options.fieldsGroup[0].id){
					var value = "&nbsp;"
				}   
                            
				var aFieldForm = searchFieldFormWithID(eachHeader.id, self.options.fieldsForm);
				//2013-11-16@EVEG :(#isVisibleForUser,#aFieldForm) Si el campo no es visible para el usuario no pinta la columna
				if(aFieldForm==false){
					eachSection=bitamApp.application.getSection(eachHeader.sectionName);
					var aFieldForm = self.options.eachSection.getFieldFormWithId(eachHeader.id);
					//debugger;
				}
				if(aFieldForm && !aFieldForm.isVisibleInView())
					continue;
				aFieldForm = new FieldForm(aFieldForm);
                            
				if(aFieldForm.tagname == "widget_colorpicker" && value != null) {
					value = '<div style="width:15px; height: 15px; background-color:'+value+'"></div> ';
				}
                
				if(aFieldForm.tagname == "widget_geolocation" && value != null && value != '' && typeof google !== undefined) {
					if(!self.options.markersArrayValues) {
						self.options.markersArrayValues = new Array();
					}
					self.options.markersArrayValues[keyReg] = value;
					viewWidgetIcon = 'images/map.gif';
                    if(!bitamApp.isMobile.isApp()) {
                        viewWidgetIcon = '../../'+viewWidgetIcon;
                    }
                    value = '<span onclick="event.stopImmediatePropagation(); bitamApp.showModal(\''+value+'\', \''+aFieldForm.tagname+'\')"><img src="'+viewWidgetIcon+'"/></span>';
					//this._addGoogleMapsMarker(value, map);
				}				
				if((aFieldForm.tagname == "widget_photo" || aFieldForm.tagname == "widget_signature") && value != null && value != '') {
					if(aFieldForm.tagname == "widget_photo") {
						var viewWidgetIcon = 'images/icono_camara.png';
					} else {
						var viewWidgetIcon = 'images/firma.gif';
					}
					if(!bitamApp.isMobile.isApp()) {
					    viewWidgetIcon = '../../'+viewWidgetIcon;		
							var ext = value.split('.').pop();
							if (ext == 'jpg' || ext == 'jpeg') {
									value = '../../'+value;
								} else { 
										value = 'data:image/jpeg;base64,'+value;
									} 
						} else { 
							value = 'data:image/jpeg;base64,'+value; 
						}
					value = value.replace(/"/g, '');
					var value2 = value;
					//EVEG 20140411 valdiacion para que se muestre el icono de la firma
					if(aFieldForm.tagname == "widget_signature"){						
						if(bitamApp.isMobile.isApp())
							value='images/firma.gif';
						else
							value='../../images/firma.gif';
					}	
					value='<img class="imagen" src="'+value+'" style="float:initial"/>';
				}
				if(aFieldForm.tagname == "widget_file" && value != null && value != ''){    
					var arrDocuments = value.split(",");
					
					var sLink = "";
					if(bitamApp.isMobile.isApp()){
                        sLink += bitamApp.buildURL('uploads//'+bitamApp.application.codeName+'//'+self.options.section.name, true);
					} else {
						sLink += bitamApp.getURLServer();
						sLink +="/action.php?action=downloadFile&sectionName="+self.options.section.name;
						sLink +="&applicationCodeName="+bitamApp.application.codeName;
					}
					for(var x in arrDocuments){
						if(isset(arrDocuments[x])){
							var sB=arrDocuments[x];
							sB=sB.split("/");
							var LinkPro = sB[3];
							sB[3] = sB[3].substring(8,sB[3].length);
							if(bitamApp.isMobile.isApp()) {
                                var sLinkDocument = sLink+'//'+LinkPro;
                                var eee = '<a onclick=\'event.stopImmediatePropagation()\' data-ajax=false href=\'javascript: bitamApp.showModal(\"'+sLinkDocument+'\", \"'+aFieldForm.tagname+'\");\'>'+sB[3]+'</a>';
							} else {
								var sLinkDocument = sLink+"&file="+LinkPro;
                                var eee = "<a onclick='event.stopImmediatePropagation()' data-ajax=false href=\""+sLinkDocument+"\">"+sB[3]+"</a>";
							}
							arrDocuments[x]=eee;	
						}
					}
					value=arrDocuments.join(",");
				}

				if ( aFieldForm.isString() && !aFieldForm.isCatalog() || aFieldForm.isText() ) {
					//2013-11-26@RTORRES:(#iphoneUse) cuando es iOS no aplica la opcion more,less
					if((!options.longText && value && ( value.length > 100 ) ) && !bitamApp.isMobile.pantallasreducidas()) {
						
						var longString = value;
						value = $('<div class="lngstr" style="display: inline-block;">'+ longString.substring(0, 100) +'</div><div title="'+ $.t('Show more') +'" class="shmtxt" style="cursor:pointer; display: inline-block;padding: 9px;vertical-align: middle;opacity: .2;"><div style="content:url(\''+ bitamApp.buildURL('images/ArrowTurnMore.svg') +'\')"></div></div>');
						value.filter('.shmtxt').on('click', function(e) {
							
							self._readMoreText(this, value, longString, options.rowElement, options.model, aFieldForm);

							return false;
						});
						delete longString;
					}
				}
			
				if(aFieldForm.isCatalog() || aFieldForm.catalog){
					if(isset(aFieldForm.catalog) && isset(bitamApp.dataApp['catalog'][aFieldForm.catalog])){
						if(isset(bitamApp.dataApp['catalog'][aFieldForm.catalog][value]))
							value = bitamApp.dataApp['catalog'][aFieldForm.catalog][value]['value']
						
						if(aFieldForm.catalog == 'fieldtypes'){
							for(var keyCat in bitamApp.dataApp['catalog'][aFieldForm.catalog]){
								if(bitamApp.dataApp['catalog'][aFieldForm.catalog][keyCat].value == value){
									value = bitamApp.dataApp['catalog'][aFieldForm.catalog][keyCat].description;
									break;
								}
							}
						}
					}
					if(aFieldForm.catalog == 'users'){
						if(isset(bitamApp.dataApp[aFieldForm.catalog][value]))
							value = bitamApp.dataApp[aFieldForm.catalog][value]['nom_largo']
					}
				
					if(isset(aFieldForm.catalog) && aFieldForm.catalog.substring(0, 12) == "dataCatalog("){
						var valueDataCatalog = eval(replaceValueString(aFieldForm.catalog, data));
						if($.isPlainObject(value)){
							var tmpValue = {};
							for(var keyDataCatalog in valueDataCatalog){
								if($.inObjectSearch(valueDataCatalog[keyDataCatalog].value, value)){
									tmpValue[valueDataCatalog[keyDataCatalog].value] = valueDataCatalog[keyDataCatalog].label;
								}
							}
							if(!$.isEmptyObject(tmpValue)){
								value = tmpValue;
							}
						}else{
							for(var keyDataCatalog in valueDataCatalog){
								if(valueDataCatalog[keyDataCatalog].value == value){
									value = valueDataCatalog[keyDataCatalog].label;
									break;
								}
							}
						}
					}
				
					//si el valor proviene del campo de otro formulario
					//if(!isset(aFieldForm.element.master_form) && isset(aFieldForm.element.catalog_form) && value != undefined)
					if(isset(aFieldForm.element.catalog_form) && $.isNumeric(value)){
						var secName = aFieldForm.element.catalog_form;
						var fieldName = aFieldForm.element.catalog_field;
						if(secName.toLowerCase() == "bitamApp.dataApp.users".toLowerCase() && !$.isPlainObject(value)){
							var dataUser = bitamApp.dataApp.users;
							if(!$.isEmptyObject(dataUser))
								value = dataUser[value][fieldName];
						}else{
							//var aSection = bitamApp.application.getSectionWithName(secLogicalName);
							var aSection = bitamApp.application.getSection(secName)
						
							var fieldsForms =  aSection.payload.fieldsForm;
							var fieldForm = null;
							for(var key in fieldsForms){
								if(fieldsForms[key].id == fieldName){
									fieldForm = fieldsForms[key];
								}
							}						
							//mejor en lugar de hacer un .filter() vamos directo a la coleccion de datos, es mucho mas rapido
							var dataUser = bitamApp.dataUser[aSection.id].collection[value];
							if(isset(aFieldForm) && (aFieldForm.tagname == "widget_selectionhierarchy")){       
								var valuehierarchy = "";
								for(var key1 in fieldsForms){
									if(fieldsForms[key1].id.indexOf("createdDate_")==0||fieldsForms[key1].id.indexOf("modifiedDate_")==0||fieldsForms[key1].id.indexOf("modifiedUser_")==0||fieldsForms[key1].id.indexOf("createdUser_")==0){
										continue;
									}
									var sDescr = dataUser[fieldsForms[key1].id];
									if($.isPlainObject(sDescr)){
										for(var ks in sDescr) break;
										sDescr = sDescr[ks];
									}
									valuehierarchy = valuehierarchy+", "+sDescr;
									if(fieldsForms[key1].id == aFieldForm.element.catalog_field_p){
										break;
									}
								}
								valuehierarchy = valuehierarchy.substring(1);
								value = valuehierarchy;
                                                    
							}else if(!$.isEmptyObject(dataUser)){
								value = dataUser[fieldForm.id];
							}
						}
					}
				}
			
				// se cambio que los campos catalogos en ves de ser array fueran objetos indexados por el key del catalogo.
				if($.isPlainObject(value) || $.isArray(value)){
					var sValue = '';
					for(var keyC in value){
						sValue = (sValue == '' ? value[keyC] : sValue + ", " + value[keyC]);
					}
					value = sValue;
					delete sValue;
				}
						
				if(value == undefined || value == "")
					value = "&nbsp;"
			
				if(isset(aFieldForm) && (aFieldForm.tagname == "widget_field_date" || aFieldForm.tagname == "widget_field_time") && value != '&nbsp;' && value != ''){
					if(isset(self.options.tableHeaderColumn[keyHeader].format))
						value = Date.parse(value).toString(self.options.tableHeaderColumn[keyHeader].format);
				}
				if(isset(aFieldForm) && (aFieldForm.tagname == "widget_field_numeric")){
					if(self.options.tableHeaderColumn[keyHeader].format)	
						value = formatString(value, self.options.tableHeaderColumn[keyHeader].format, 'numeric');
				}
			
				//si el campo es un widget de tipo checkbox single podemos hacer un check directo desde la vista y guardar el valor en la base de datos.
				/*
				if(isset(tmpField) && tmpField.tagname == "widget_checkbox_single")
				{
					value = $("<span/>").html("Click").click(function(){alert("hola")});
				}*/
			
							
				if(eachHeader.type == "sectionindialog"){
					value = "<a href='javascript:bitamApp.viewFloatDetail({section:\""+this.widget.name+"\", sectiondetail:\""+ self.options.tableHeaderColumn[keyHeader].section +"\", key:\""+  keyReg +"\"})'>open</a>";
				}
				if(self.options.canEdit && this._checkSecCriteriaChange(data) && eachHeader.contentEditable && !(isset(self.options.section.payload.processForms) && (isset(self.options.section.payload.processForms[0]) && self.options.section.payload.processForms[0].regBlock == true && isset(self.options.section.payload.processForms[0].arrKeysBlocked[keyReg])))){
					//cuando es un tipo checkbox sencillo le damos la funcionalidad a la vista que con solo un click pueda cambiar su valor de encendido/apagado
					if(aFieldForm.tagname == "widget_checkbox_single"){
						//toggle()
						//on
						imgChecked = bitamApp.buildURL("images/checked_img.gif");
						//of
						imgCheck = bitamApp.buildURL("images/check_img.gif");
						aWidget = this.widget;	
						myTD.append($("<span>", {"data-fieldid":aFieldForm.id,"data-nrow":keyData,"data-value":aFieldForm.element.value, "data-checked":aFieldForm.element.value == value}).append($("<img/>", {src:(aFieldForm.element.value == value ? imgChecked:imgCheck)})).click(function(event)
							{
								event.stopImmediatePropagation();
								var myData = aWidget.data[$(this).attr("data-nrow")];
								var el = $(this); 
								var fn = function(){
									if(el.data("checked"))
									{
										el.html($("<img/>", {src:imgCheck}));
										el.data("checked", false);
										myData[el.data("fieldid")] = "";
									}
									else
									{
										el.html($("<img/>", {src:imgChecked}));
										el.data("checked", true);
										myData[el.data("fieldid")] = el.data("value");
									}
								};
								fn();
								res = bitamApp.saveForm(aWidget.properties.section, myData, function(){},undefined, undefined, {"isOrphanForm":(myData[aWidget.properties.section.payload.fieldsData[0].name]<0?true:false)});
								if(res.error == true)
								{
									fn();
								}
							}));
					}
					else{	
						if(firts)
						{
						   var divHeaderLabel = $("<div/>",{"class":"cellMetro"});
						   if(!bitamApp.isMobile.pantallasreducidas())
						   {
								var headerLabel = $("<span/>", {"class":"celMetro","style":"color:#555; text-transform: uppercase; font-size: 20px; font-weight: bold;"}).html(eachHeader.label+":&nbsp&nbsp");
								divHeaderLabel.append(headerLabel);
						   }
						   
						   divHeaderLabel.append($("<span/>", {"class":"celMetro","style":"margin-top: 6px; text-transform: uppercase; font-size: 20px;font-weight: bold;"}).html(value));
						   if (!eachHeader.isCalculated && bitamApp.application.mdVersion >= 4)
						    {
						    self.applyDynamicProperties(divHeaderLabel, aFieldForm,data);
						    }
						   myTD.append(divHeaderLabel);
						  // myTD.append($("<span/>", {"class":"cellMetro","style":"margin-top: 6px;font-size: 14px;font-weight: bold;"}).html(value));
						   firts = false;
						}
						else{
						   var divHeaderLabel = $("<div/>",{"class":"cellMetro"});
						   if(!bitamApp.isMobile.pantallasreducidas())
						   {
							   var headerLabel = $("<span/>", {"class":"celMetro","style":"color:#555; text-transform: uppercase; font-size: 13px; font-weight: bold;"}).html(eachHeader.label+":&nbsp&nbsp");
							   divHeaderLabel.append(headerLabel);
						   }
						   //divHeaderLabel.append($("<span/>", {"class":"celMetro"}).html(eachHeader.label+"&nbsp&nbsp&nbsp"));
						   if (!eachHeader.isCalculated && bitamApp.application.mdVersion >= 4)
						    {
						    self.applyDynamicProperties(divHeaderLabel, aFieldForm,data);
						    }
							if(aFieldForm.tagname == "widget_field_rating"){
								var valor = value;
								divHeaderLabel.append($("<span/>", {"class":"celMetro","style":"display: inline-block;"}).append($("<div>").rating({call:1, size:aFieldForm.element.size, value:valor, images:aFieldForm.element.images})));
							}else
								divHeaderLabel.append($("<span/>", {"class":"celMetro"}).html(value));
						   myTD.append(divHeaderLabel);
						}
					}				
				}
				else{
					
					if(aFieldForm.tagname == "widget_photo" && isFirst == true)
					{
					    var firstImage = $("<img/>",{"class":"imagen","src":value2});
					    firstImage.css({
						"width": "32%",
						"height": "145px",
						"float": "left",
						"margin-left": "3%",
						"margin-top": "2%"
					    });
					    myTD.prepend(firstImage);
					    isFirst = false;
					    continue;
					}
					if(firts)
						{
						   var divHeaderLabel = $("<div/>",{"class":"cellMetro"});
						   if(!bitamApp.isMobile.pantallasreducidas())
						   {
								var headerLabel = $("<span/>", {"class":"celMetro","style":"color:#555; text-transform: uppercase; font-size: 20px; font-weight: bold;" }).html(eachHeader.label+":&nbsp&nbsp");
								divHeaderLabel.append(headerLabel);
						   }
						   divHeaderLabel.append($("<span/>", {"class":"celMetro","style":"margin-top: 6px;text-transform: uppercase;font-size: 20px;font-weight: bold;"}).html(value));
						   if (!eachHeader.isCalculated && bitamApp.application.mdVersion >= 4)
						    {
						    self.applyDynamicProperties(divHeaderLabel, aFieldForm,data);
						    }
						   myTD.append(divHeaderLabel);
						   //myTD.append($("<span/>", {"class":"cellMetro","style":"margin-top: 6px;font-size: 14px;font-weight: bold;"}).html(value));
						   firts = false;
						}else if(aFieldForm.tagname == "widget_field_rating"){
							var divHeaderLabel = $("<div/>",{"class":"cellMetro"});
							if(!bitamApp.isMobile.pantallasreducidas())
							{
								var headerLabel = $("<span/>", {"class":"celMetro","style":"color:#555; text-transform: uppercase; font-size: 13px; font-weight: bold;"}).html(eachHeader.label+":&nbsp&nbsp");
								divHeaderLabel.append(headerLabel);
							}
							var valor = value;
							divHeaderLabel.append($("<span/>", {"class":"celMetro","style":"display: inline-block;"}).append($("<div>").rating({call:1, size:aFieldForm.element.size, value:valor, images:aFieldForm.element.images})));
							myTD.append(divHeaderLabel);
						}else
						{
						   var divHeaderLabel = $("<div/>",{"class":"cellMetro"});
						   if(!bitamApp.isMobile.pantallasreducidas())
						   {
								var headerLabel = $("<span/>", {"class":"celMetro","style":"color:#555; text-transform: uppercase; font-size: 13px; font-weight: bold;"}).html(eachHeader.label+":&nbsp&nbsp");
								divHeaderLabel.append(headerLabel);
						   }
						   
						   divHeaderLabel.append($("<span/>", {"class":"celMetro"}).html(value));
						   if (!eachHeader.isCalculated && bitamApp.application.mdVersion >= 4)
						    {
						    self.applyDynamicProperties(divHeaderLabel, aFieldForm,data);
						    }
						   myTD.append(divHeaderLabel);
					}
				}
			
				if(isset(eachHeader.dynamicproperties)){
					for(var key in eachHeader.dynamicproperties){
						var dynProp = eachHeader.dynamicproperties[key];
									
						var re = new RegExp(/\[.+\]/)
						var m = re.exec(dynProp.condition);
						var res = false;
						if(m){
							var match = m[0];
							match = match.replace('[', '');
							match = match.replace(']', '');
							condition = dynProp.condition.replace(m[0], "'" + this.widget.data[keyReg][match] + "'");
							if(eval(condition)){
								res = true;
							}
						}
			 		
						if(res == true){
							myTD.append($("<span/>", {"style":dynProp.style}).html(value));
						}
					}
				}
			}		
			//Se agrego un Div para agrupar los datos de cada registro cuando la vista de la tabla es cuadros
			myTD = myTD.append($("<div/>", {"style":"overflow:auto; margin-top:2%"}).append(myTD.find('div[class="cellMetro"]')));
			myTR.append(myTD);
			if(bitamApp.isMobile.isApp()){
				myTR.prepend($("<div/>", {"class":"table_syncitem " + (data.syncDate > lastSyncDate?"ico":""), title:$.t("No data sync")}));
			}
			//columna para agregar la imagen de ordenamiento
			if(self.options.sortable){
				var sortableTD = $("<div/>", {"style":"width: 20px; height: 20px; float:right;"}).html("<a style='display:none' class='remove-element' title='Sort'><img src='"+bitamApp.buildURL("images/arrowsmove.png")+"'/></a>");
				if(myTR.find(".checkItemSelect").length)
					myTR.find(".checkItemSelect").parent().parent().css("width", 22).after(sortableTD);
				else
					myTR.prepend(sortableTD);
				delete sortableTD;
			}
			
			//agrega la funcionalidad de borrado de registro
			//if(bitamApp.isMobile.any() && !bitamApp.isMobile.iPad() && this.widget.properties.canDelete){
				/*2014-04-22 @JRPP (#DeletedDELET) Quitar opcion de eliminar */
				//myTR.append($("<div/>", {"class":"remove-element", "style":"width: 20px; height: 20px; float:right;"}));
			/*} /*else {*/
				if((self.options.canDelete || self.options.section.payload.actionforms.length > 0) && !(isset(self.options.section.payload.processForms) && isset(self.options.section.payload.processForms[0]) && self.options.section.payload.processForms[0].regBlock == true && isset(self.options.section.payload.processForms[0].arrKeysBlocked[keyReg]))){       
					var id = "table_check_"+self.options.name+"_tr_"+keyReg;
					//al principio del renglon agrega un checkbox
					var inputCheckBox = $("<input/>", {
						"class":"checkItemSelect",
						"data-role":"none", 
						"type":"checkbox" , 
						"name":this.widget.name , 
						"value":this.widget.name,
						"id":id,
						"data-key":keyReg
					}).on("click", {"idTR" : myTR.attr("id")}, function(event){
						event.stopPropagation();
						if(this.checked){
							$("#" + event.data.idTR).addClass("tr-selected");
						}
						else{
							$("#" + event.data.idTR).removeClass("tr-selected");
						}
						//$(event.currentTarget).find("input[type='checkbox']").click();
					})
					myTR.prepend($("<div/>", {"class":"table_checkitem","style":"float:right;"}).append(
						inputCheckBox
					));
					inputCheckBox.checkbox();
					myTR.find(".chkbox-box").css("box-shadow","1px 1px 3px #999");
					delete inputCheckBox;
					//agrega al final una imagen para borrado del registro
					/*2014-04-22 @JRPP (#DeletedDELET) Quitar opcion de eliminar */
					// if(!bitamApp.isMobile.iPad()){
					// 	myTR.append($("<div/>", {"style":"width: 20px; height: 20px; float:right"}).append(
					// 		$("<a/>", {
					// 			"style":'display:none',
					// 			"class":'remove-element',
					// 			"href":'javascript:;',
					// 			"title":'Remove'}
					// 		).append($("<img/>", {
					// 					"src":bitamApp.buildURL("images/icon-trash.png")
					// 				}).click({"widgetName":self.options.name, "keyReg":keyReg}, function(event){
					// 					event.stopImmediatePropagation();
					// 					self.remove(event.data.keyReg);
					// 				})
					// 		)));

					// 	if(self._checkSecCriteriaRemove(data)){
					// 		myTR.mouseenter(function() {
					// 				var a = $(this).find(".remove-element");
					// 				self.options.canDelete && a.css("display", "block");
					// 		});
					// 		myTR.mouseleave(function() {
					// 				var a = $(this).find(".remove-element");
					// 				a.css("display", "none");
					// 		});
					// 	}else{
					// 		//si no tiene permisos de eliminar este registro, eliminamos la imagen del TR
					// 		myTR.find("a").remove();
					// 	}
					// }
				}
				else{
					if(self.options.canDelete)
					{
						myTR.prepend($("<div/>", {"style":"width: 18px; height: 13px; margin-right: 18px; position: none;"}));
					}
					myTR.append($("<div/>", {"style":"width: 20px; height: 20px;"}));
				}
			/*}*/
			if(self.options.canView == true || $.isFunction(self.options.canView)){
				myTR.css({"cursor":"pointer"});
				myTR.click({"widget": self.options.name, "key":keyReg, viewonly:!self._checkSecCriteriaChange(data) || !self.options.canEdit}, function(event){
					/*if(event.toElement.type != 'file') {
					event.stopPropagation();
					event.stopImmediatePropagation();
					event.preventDefault();
					}
					
					var TR = $("#table_"+event.data.widget+"_tr_"+event.data.key);
					if(TR.data('openViewForm') || TR.data('editing'))
						return;
					Debugger.message(new Array(10).join('*'));
					Debugger.register('widgets.mngrTable__buildTablePage.view widget = ' + event.data.widget);
					TR.data('openViewForm', true);
					//para evitar abrirlo dos veces (doble click)
					setTimeout(function(){TR.data('openViewForm', false);}, 1000)
					self.view(event.data.key, {viewonly:event.data.viewonly});
					return;*/
					/*2014-03-18 @JRPP (#30031) coloco la clase tr-selected para que añada el efecto CSS */
					$(event.currentTarget).addClass('tr-selected');
					var TR = $("#table_"+event.data.widget+"_tr_"+event.data.key);
					if(!(TR.data('openViewForm') || TR.data('editing'))){
						TR.data('openViewForm', true);
						//para evitar abrirlo dos veces (doble click)
						setTimeout(function(){TR.data('openViewForm', false);}, 1000)
						self.view(event.data.key, {viewonly:event.data.viewonly});
					}					
				});
			}

			/*if(self.options.section.payload.views[self.options.sectionView].ViewCustomization) {
				var width = self.options.section.payload.views[self.options.sectionView].ViewCustomization.registerwidth;
				if(width == "1" && isset(width))
					myTR.css({"height":"25px"});
				else if(width == "2" && isset(width))
					myTR.css({"height":"50px"});
				else if(width == "3" && isset(width))
					myTR.css({"height":"80px"});
				var fontfields = self.options.section.payload.views[self.options.sectionView].ViewCustomization.fontfields;
				Customization(myTR.find('span'),fontfields,"texto");
			}*/
			return myTR;
		},
		_checkEmptyTable: function(){
			var self = this, nodata = true;
			if(self.tBody.children().length == 0){
				nodata = true
			}else{
				nodata = false
			}
			$("#table_"+self.options.name+"_header").css("display",(nodata?"none":''));
			$("#contentlist_" + self.options.name).css("display", (nodata?"none":''));
			$("#tablefooter_paginator_" + self.options.name).css("display",(nodata?"none":''));
			$("#norecordsfound_"+self.options.name).css("display", (nodata?"block":'none'));
			if(self.options.section.payload.views[self.options.sectionView].ViewCustomization) {
				Customization($('#contentlist_'+self.options.name),self.options.section.payload.views[self.options.sectionView],"vista")
			}
			if(nodata)
				$(".table_"+self.options.name+"_trc").remove();
		},
		
		/*
		27/08/2013@aramirez
			recibe un string con {image.png@image} y regresa un string html dependiendo del tipo de arhivo (actualmente imagen, que regresa un <img/> con la imagen definida)
		*/
		_replaceBBCode: function(sString){
			var self = this;
			if(!sString) return sString;
			
			sString = sString.replace(/\[img\](.*?)\[\/img\]/g, function(t,img){
				return '<img src="'+ bitamApp.buildURL(img) +'"/>';
			});
			sString = sString.replace(/\[b\](.*?)\[\/b\]/g, function(t,text){
				return '<strong>'+ text +'</strong>';
			});
			sString = sString.replace(/\[i\](.*?)\[\/i\]/g, function(t,text){
				return '<i>'+ text +'</i>';
			});
			
			sString = sString.replace(/\[userimage\](.*?)\[\/userimage\]/g, function(t,img){
				for(var mKey in bitamApp.dataApp.users) {
					var eachUser = bitamApp.dataApp.users[mKey];
					if(eachUser.cuenta_correo == img){
						return '<img class="imagen" src="'+ bitamApp.buildURL(eachUser.user_photo) +'"/>';
					}
				}
				return '<img src="'+ bitamApp.buildURL(img) +'"/>';
			});
			
			return sString
		},
		
		goto: function(page){
			if((page<=this.options.totalPages&&page>0)||(page==1&&this.options.totalPages==0)){
				this.options.currentPage=parseFloat(page);
				this._buildPage(page);
			}
		},
		move: function(d,m){
			
			var anEvent = new $.Event('MovePage');this.element.trigger(anEvent);if(anEvent.isDefaultPrevented()) return false;

			this.goto(d==1?(m?this.options.totalPages:this.options.currentPage+1):(m?1:this.options.currentPage-1));

			return true;
		},
		filter: function(s){
			this._filter = s;
			this.options.filterApplied = this._filter;
			var sFilter = '';
			if($.isPlainObject(this.options.filter)){
				sFilter = BitamStorage.webDB.buildFilterWithObject({filter:this.options.filter,sectionName:this.options.section.name});
			}
			if(sFilter != ''){
				if(this.options.filterApplied!=''){
					this.options.filterApplied = '(' + sFilter + ') AND (' + this.options.filterApplied + ')';
				}else{
					this.options.filterApplied = '(' + sFilter + ')';
				}
			}
			this.options.currentPage = 1;
			return this._buildPage(1, s);
		},
		view: function(id, options) {
			
			var anEvent = new $.Event('OpenForm');this.element.trigger(anEvent);if(anEvent.isDefaultPrevented()) return false;

			var TR = $("#table_"+this.options.name+"_tr_"+id), r;
			if(!(TR.data('editing') || TR.find('.aDeleteBtn')[0])){
				//2013-11-05@ARMZ: () Modificado a que si this.options.canEdit sea TRUE options.viewonly sea FALSE, o viceversa
				//options.viewonly = !this.options.canEdit;
				if(!this.options.canEdit)
					options.viewonly = !0;
				if($.isFunction(this.options.canView)){
					TR.find('TD').css("background-color","#FFC !important");
					r = this.options.canView(id, options).then(function() {
						TR.find('TD').css("background-color","initial");
					});
				}
			}
			return r;
		},
		//@id = puede ser solo un numero o un array de llaves de registro
		remove: function(ids){
		var self = this;
			if(!ids){
				var ids = [];
					self.tBody.find('.table_checkitem input:checked:not(.checkMasterSelect)').each(function(){
						ids.push($(this).data('key'));
					})
			}
			
			if(ids.length == 0){
				alert("You must select at least one item");
				return;
			}
			/*2014-04-23 @JRPP (#32443) Se agrega un mensaje de alerta antes de eliminar un registro*/
			
			if(!bitamApp.isMobile.pantallasreducidas()){
				require(['views/modal/basicModal', 'bootstrap'], function(BasicModal) {
					var aBasicModal = new BasicModal({
						modalBody:$.t("Selected records will be deleted. Do you agree?"),
						modalTitle:$.t("Erase Alert")
					});
					aBasicModal.addButton({
						label: $.t('Accept'),
						callback: function() {
							/*En caso de aceptar cerramos la ventana y ejecutamos la instruccion de eliminado*/
							aBasicModal.remove();
							self.removeWhitAlert(ids);
						}
					})
					$('body').append(aBasicModal.render());
				});
			} else {
				if (confirm($.t("Selected records will be deleted. Do you agree?"))) {
						self.removeWhitAlert(ids);
					}
			}
		},
		removeWhitAlert: function(ids) {
			if(!$.isArray(ids)){
				ids = [ids];
			}

			//2013-11-19@ARMZ: () numNoRemove, Variable para guardar numero de registros no removidos
			var self = this, numNoRemove = 0, nCount = ids.length, idsRemove = [];

			//2014-05-07@ARMZ: Recorremos cada registro para ver si tiene permisos para removerlo
			for(var i = 0; i < nCount; i++) {
				// Si regresa false, este registro no puede ser eliminado por el usuario, lo eliminamos del array que se enviara al server
				if(this._checkSecCriteriaRemove(this.getData(ids[i]))) {
					idsRemove.push(ids[i]);
				} else {
					numNoRemove ++;
				}
			}

			//2014-04-22@HADR(#32439) Enviar las formas y ids que estan presentes en la pagina actual para que se actualicen sus modelos en caso de haber cambios
			var fgetOpenedForms = function(){
				var openedForms = {};
				$("form[name^='addFormData_']").each(function(){
					var formName = $(this).prop("name").replace("addFormData_","");
					openedForms[formName] = $(this).find("input[name^='id_"+formName+"']").getValue();
				});
				return openedForms;
			};
			require([
				'libs/misc/vm'
			], function(Vm) {
				bitamApp.remove(self.options.section.name, idsRemove, {"callback":function(response){
					for(var num in idsRemove){
						//JRPP 2013-10-08 validamos si es una aplicacion mobil 
						var idsS = idsRemove[num] ;
						if(!bitamApp.isMobile.isApp())
							var idsS = String(idsRemove[num]);
						//checamos si no debemos eliminar un registro de la tabla
						if(response&&response.result[self.options.section.name]&&$.inArray(idsS, response.result[self.options.section.name].no_remove)!=-1){
							//si no hay que eliminarlo de la tabla, continuamos con el siguiente registro
							numNoRemove ++;
							continue;
						}
						
						if(self.options.arrayMarker && self.options.map) {
							if(self.options.arrayMarker[idsRemove[num]]) {
								var deleteMarker = self.options.arrayMarker[idsRemove[num]].marker;
								deleteMarker.setMap(null);
								delete self.options.arrayMarker[idsRemove[num]];
								var latlngbounds = new google.maps.LatLngBounds();
								for(var mKey in self.options.arrayMarker) {
									latlngbounds.extend(self.options.arrayMarker[mKey].point);
								}
								self.options.map.fitBounds(latlngbounds);
							}
						}
						//2013-12-20@EVEG:(#self._remove)Se manda a llamar a la funcion del widget para eliminar dataGantt los registros que han sido borrados
						//2014-01-14@MABH:(#31023)Se estaba enviards idsRemove (array) en vez de idsS (string) a la funcion de borrado
						self._remove(idsS);
						self.options.totalRows--;
					}
					//2014-04-22@HADR(#32439) Actualizar los modelos de las formas abiertas y que pudieron ser afectadas al actualizar un registro de esta misma forma
					if(response && response.data){
						var openedForms = fgetOpenedForms();
						var view;
						for(var key in openedForms){
							view = Vm.getView(key);
							if(view && response.data.hasOwnProperty(key) && response.data[key].hasOwnProperty(openedForms[key])){
								view.model.set(response.data[key][openedForms[key]]);
							}
						}
					}
					if(numNoRemove)
						bitamApp.alert(numNoRemove + " " + $.t("Records were not eliminated because they do not match the criteria."));

					if((self.options.arrayMarker && Object.keys(self.options.arrayMarker).length == 0) && self.element.find('#googleMapView'+self.options.name).length > 0) {
						self.element.find('#googleMapView'+self.options.name).css('display','none');
					}

					self._checkEmptyTable();
					self.options.totalPages = Math.ceil(self.options.totalRows/self.options.pageSize);
					self._updateInfoPagination();
					self._removeComplete();
					//2014-04-23@HADR(#32264) Actualizar los registros visibles en la pagina donde se eliminaron los registros
					if(self.tBody.children().length == 0){
						self.refresh();
					}
				},
				'openedForms' : fgetOpenedForms()});
			});
		},
		
		//2013-12-20@EVEG:(#_removeComplete)Funcion que se redefine en el widget gantt
		_removeComplete: function() {
			
		},
		//2013-12-20@EVEG:(#_removeComplete)Funcion que se redefine en el widget gantt
		_remove: function(id) {
			this.tBody.find("#table_"+this.options.name+"_tr_" + id).remove();
		},
		
		edit: function(key){
			var TR, self = this, data, myButtonOk, myButtonCancel, myDivButtons;
			TR = self.tBody.find("#table_"+self.options.name+"_tr_"+key);
			data = TR.data('data');
			if(TR.data('editing') || TR.find('.aDeleteBtn')[0] || !self.options.canEdit)
				return;
			TR.mouseleave();
			var isEditing = false;
			//HADR 2013-10-10: GRID - Se agrega arreglo para almacenar los fieldForms utilizados para el validador
			{//if(self.options.howtowatch != 4){
				self.options.arrayFieldsFormValidator[key] = new Array();
			}
			//HADR
			for(var keyField in self.options.tableHeaderColumn){
				var eachField = self.options.tableHeaderColumn[keyField];
				//HADR 2013-10-18: GRID - Evitar que la primera columna sea editable cuando es una vista de tipo grid
				if(eachField.contentEditable == true && !(self.options.howtowatch == 4 && self.options.tableHeaderColumn[0].id == eachField.id)){
					isEditing = true;
					var TD = TR.find("#table_"+self.options.name+"_tr_"+key+"_td_"+eachField.id);					
					var aFieldForm = searchFieldFormWithID(eachField.id, self.options.fieldsForm);
					aFieldForm = new FieldForm($.extend(true, {}, aFieldForm));
					var tmpID = aFieldForm.id;
					aFieldForm.id = "tr_" + key +"_td_"+ aFieldForm.id + "_field";
					aFieldForm.element.id = aFieldForm.id;
					aFieldForm.element.css = {"width":TD.css("width")};
					if(aFieldForm.tagname == "widget_textarea"){
						aFieldForm.element.css.height ="80px";
					}
					//HADR 2013-10-10: GRID - Se agrega arreglo para almacenar los fieldForms utilizados para el validador
					{//if(self.options.howtowatch != 4 ){
						self.options.arrayFieldsFormValidator[key].push(aFieldForm);
					}/*else{
						self.options.arrayFieldsFormValidator.push(aFieldForm);
					}*/
					//HADR
					//aramirez@21/01/2013
					//Si estamos en eBavel usamos el metodo de creacion  de widgets antiguo.
					if(BitamInterface.createWidget){
						TD.empty();
						var element = BitamInterface.createWidget({
							"field":aFieldForm, 
							"value":data[tmpID], 
							"filter":self.options.filter, 
							"sectionName":aFieldForm.sectionName,
							"dataSection":{},
							"$root":TD,
							"mode":'edition'
						});
					}else{
						var element = BitamInterface.createWidgetFormElement({field:aFieldForm, value:data[tmpID], filter:self.options.filter});
						element.css("width", TD.css("width"));			
						element.css("width", "-=6");
						element.data('editview', true);
						TD.html(element);
					}
					
					delete element;
					delete aFieldForm;
					delete TD;
					delete tmpID;
				}
				delete eachField
			}
			
			if(isEditing){
				TR.data('editing', true);
				TR.addClass("form-inline");
			}
			
			fnCancel = function(event){
				event.stopImmediatePropagation();
				var myData;
				TR.data('editing', false);
				TR.removeClass("form-inline");
				TR.find(".icon-tablet-annotate").show();
				$("#buttonsinline_"+self.options.name+"_tr_"+key).remove();
				myData = TR.data('data');
				TR.replaceWith(self._buildRow({data:myData}));
			}
			
			fnOk = function(event){
				event.stopImmediatePropagation();
				
				//HADR 2013-10-11: GRID - Aplicar el validador de forms a cada fila
				var idFormValidator = "frm_table_"+self.options.name;
				if(self.element.find('#'+idFormValidator).length > 0){
					formValidator.resetForm();
					self.element.find('.table-view').unwrap("<form id='" + idFormValidator + "' action='javascript: return false;'></form>");
				}
				self.element.find('.table-view').wrap("<form id='" + idFormValidator + "' action='javascript: return false;'></form>");
				formValidator = self.element.find('#'+idFormValidator).validateForm(self.options.arrayFieldsFormValidator[key], undefined);
				if(!self.element.find('#'+idFormValidator).valid()){
					return;
				}
				//HADR
					
				var myData, fnCallback;
				myData = TR.data('data');				
				for(var keyField in self.options.tableHeaderColumn){
					var eachField = self.options.tableHeaderColumn[keyField];
					if(eachField.contentEditable == true){
						myData[eachField.id] = $("[name='tr_" + key +"_td_"+ eachField.id + "_field']").getValue();
					}
				}
				fnCallback = function(){
					if(self.options.arrayMarker && self.options.map) {
						if(self.options.arrayMarker[myData[self.options.section.getFieldKey()]]) {
							var deleteMarker = self.options.arrayMarker[myData[self.options.section.getFieldKey()]].marker;
							deleteMarker.setMap(null);
							delete self.options.arrayMarker[myData[self.options.section.getFieldKey()]];
						}
					}					
					TR.replaceWith(self._buildRow({data:myData}));
					TR.data('data', myData);
					var fieldsForm = self.options.fieldsForm;
					for(var keyField in fieldsForm){
						var eachField = fieldsForm[keyField]
						if(eachField.propagateParent && Object.keys(eachField.propagateParent).length>0){
							for(var pKey in eachField.propagateParent) {
								var aSection = bitamApp.application.getSection(eachField.propagateParent[pKey].sectionName);
								var aField = aSection.getFieldFormWithId(eachField.propagateParent[pKey].id);
								var aModel = new BitamStorage.utils.BasicModel({}, {idAttribute: aSection.getFieldKey()}).set(aSection.getFieldKey(), myData[aSection.sections[self.options.section.name].FDTKey]);
								Utils.defaultValue(aField, aModel, aSection.name, {dependentForms:[]}).then(function(res) {
									$("[name='"+res.fieldForm.id+"']").setValue(res.value);
								}, function() {
									console.log('Ocurrio un error al calcular la suma');
								});
							}
						}
					}
				};
				
				bitamApp.getFormData({section:self.options.section.name, key:myData[self.options.section.payload.fieldsData[0].name], getDetails:false}).then(function(res){
					bitamApp.dataUser[self.options.section.name].collection[myData[self.options.section.payload.fieldsData[0].name]] = res.dataForm[self.options.section.name].collection[myData[self.options.section.payload.fieldsData[0].name]]
					var item = res.dataForm[self.options.section.name].collection[myData[self.options.section.payload.fieldsData[0].name]]
					myData = $.extend($.extend(true, {}, item), myData);
					var aModel = new BitamStorage.utils.BasicModel().set(res.dataForm[self.options.section.name].collection[myData[self.options.section.payload.fieldsData[0].name]]);
					aModel.idAttribute = self.options.section.getFieldKey();
					var promise = Utils.evaluateModel({
						sSection: self.options.section.name,
						model: aModel
					});
					aModel.set(myData);

					promise.then(function() {

						require(['libs/misc/vm'], function(Vm) {
							var openedForms = {};
							$("form[name^='addFormData_']").each(function(){
								var formName = $(this).prop("name").replace("addFormData_","");
								openedForms[formName] = $(this).find("input[name^='id_"+formName+"']").getValue();
							});
							
							BitamStorage.sync(aModel, self.options.section.name, { extraParams: { updateInterface: openedForms } }).then(function(collections) {
								$.extend(myData, aModel.attributes);

								for(var key in openedForms) {
									if(collections.hasOwnProperty(key)) {
										var bbRow = collections[key].get(openedForms[key]);
										var view = Vm.getView(key);
										if(bbRow && view) {
											view.model.set(bbRow.attributes);
										}
									}
								}
								debugger;
								fnCallback();
							}).always(function() {
								TR.data('editing', false);
								TR.removeClass("form-inline");
								TR.find(".icon-tablet-annotate").show();
								$("#buttonsinline_"+self.options.name+"_tr_"+key).remove();
							});
						});

					}, function() {
						console.log('Hubo un error al calcular los valores del modelo')
					})					
				})
			}
			
			//HADR 2013-10-07: GRID - Ocultar botones guardar y cancelar cuando la vista este en modo Grid
			if(self.options.howtowatch != 4){
				myDivButtons = $("<div/>", {"style":"margin-bottom: 5px;"});
				myButtonOk = $("<input/>",{"type":"submit", "class":"btn btn-basic", "data-inline":"true", "value":$.t("Save"), "data-theme":"g", "id":""}).click(fnOk);
				myButtonCancel = $("<input/>",{"type":"button", "class":"btn btn-basic", "data-inline":"true", "value":$.t("Cancel"), "id":"", "data-theme":"g"}).click(fnCancel);
				
				Customization(myButtonOk,"NAVSave","boton");
				Customization(myButtonCancel,"NAVCancel","boton");

				myDivButtons.append(myButtonOk);
				myDivButtons.append(myButtonCancel);
				
				TR.after($("<tr/>", {"id":"buttonsinline_"+self.options.name+"_tr_"+key}).append($("<td/>", {"colspan":$("#table_"+self.options.name+"_tr_"+key).children().length}).append(myDivButtons)));
			}
			//HADR
		},
		replaceRow: function(id, data){
			var self = this, TR, myData;
			TR = self.tBody.find("#table_"+self.options.name+"_tr_"+id);
			myData = TR.data('data');
			myData = $.extend(myData, data);
			TR.data('data', myData);
			TR.replaceWith(self._buildRow({'data':myData}));
		},
		canEdit: function(aBoolean){
			this.options.canEdit = aBoolean;
			this.showMenu(aBoolean);
			this.element.find('.table_checkitem').hide();
		},
		canDelete: function(aBoolean){
			this.options.canDelete = aBoolean;
		},
		showMenu: function(aBoolean){
			if(!aBoolean){
				this.element.find('.menuTable li:not(.liTableNav)').not('.btn-showhideboxfilter').css('display','none');
				this.element.find('.form-add-element').css('display','none');
			}else{
				this.element.find('.menuTable li:not(.liTableNav)').not('.btn-showhideboxfilter').css('display','');
				this.element.find('.form-add-element').css('display','');
			}
		},
		//10/04/2013@aramirez
		//agrega un nuevo elemento o actualiza uno previamente agregado.
		add: function(collection){
			var self = this, TD;
			for(var key in collection){
				TD = $("#table_"+self.options.name+"_tr_"+collection[key][self.options.section.getFieldKey()]);
				if(TD.length==0)
					self.options.totalRows++;
				else{
					self.replaceRow(collection[key][self.options.section.getFieldKey()], collection[key]);
					delete collection[key];
				}
			}
			self.options.totalPages = Math.ceil(self.options.totalRows/self.options.pageSize);
			if(self.options.currentPage == self.options.totalPages){
				for(var key in collection){
					self.tBody.append(self._buildRow({data:collection[key]}));
				}
			}
			self._updateInfoPagination();
			self._checkEmptyTable();
		},
		
		pageSize: function(s){
			if (!$.isNumeric(s)){
				return;
			}
			var self = this;
			bitamApp.application.payload.sections[self.options.section.id].payload.views[self.options.sectionView].pageSize=s//2013-10-29@EVEG:(#pageSize, #tablelocation)Para mantener el numero de paginacion a�ade pageSize en objeto de la vista
			self.options.pageSize = parseFloat(s);
			self.options.totalPages = Math.ceil(self.options.totalRows / self.options.pageSize);				
			document.getElementById("totalpages_" + self.options.name).innerHTML = self.options.totalRows == 0 ? 1 :self.options.totalPages;
			var selectPage = $(".selectPage_"+self.options.name);
			selectPage.empty();
			for(var i=1; i<=self.options.totalPages; i++)
				selectPage.append($("<option/>", {"value":i}).html(i));
			delete selectPage;
			this.options.currentPage = 1;
			return self._buildPage();
		},
		orderBy: function(column, order){
			var orderBy = [], self = this;
			orderBy.push({'id':column,'order':order});
			self.options.orderBy = orderBy;
			self._buildPage();			
		},
		refresh: function(){
			return this._buildPage(this.options.currentPage);
		},
		//HADR 2013-10-14: GRID - nuevo metodo para obtener el modelo de datos de la vista configurada como grid
		gridModelForms: function(){
			var self = this;
			var myData = new Object();
		
			if(self.options.howtowatch == 4){
				var arrayTmp;
				for(var arrayFieldId in self.options.arrayFieldsFormValidator){
					arrayTmp = $("#frm_table_"+self.options.name).modelForms(self.options.arrayFieldsFormValidator[arrayFieldId]);
					myData[arrayFieldId] = new Object();
					for(var keyField in self.options.tableHeaderColumn){
						var eachField = self.options.tableHeaderColumn[keyField];
						if(eachField.contentEditable == true){
							myData[arrayFieldId][eachField.id] = arrayTmp["tr_"+arrayFieldId+"_td_"+eachField.id+"_field"];
						}else{
							myData[arrayFieldId][eachField.id] = self.tBody.find("#table_"+self.options.name+"_tr_"+arrayFieldId).data('data')[eachField.id];
						}
					}
					myData[arrayFieldId] = $.extend(true, {}, self.tBody.find("#table_"+self.options.name+"_tr_"+arrayFieldId).data('data'), myData[arrayFieldId], self.options.filter);
					arrayTmp = null;
				}
			}
			
			return myData;
		},
		//ARMZ 2013-10-15, Funcion para obtener los datos de la vista de cada TR
		//2014-05-07@ARMZ: El parametro ID es opcional, si esta presente regresa los datos del ID solicitado
		getData: function(id) {
			var self = this, collection = {}, sSearch = 'TR',aTBody=self.tBody;

			if(id) {
				sSearch = 'TR[data-id="'+ id +'"]';
			}
			if(self.options.howtowatch==5){
				aTBody=self.tBodyGantt;
			}

			if(self.options.howtowatch==2){
			
				aTBody.find('.metroRow').each(function(i, el){
					var aModelAttr = $(el).data('data');
					if($.isNumeric(aModelAttr[self.options.section.getFieldKey()])) {
						collection[aModelAttr[self.options.section.getFieldKey()]] = aModelAttr;
					}
				});
					
			}else
			{
				aTBody.find('TR').each(function(i, el){
					var aModelAttr = $(el).data('data');
					if($.isNumeric(aModelAttr[self.options.section.getFieldKey()])) {
						collection[aModelAttr[self.options.section.getFieldKey()]] = aModelAttr;
					}
				});
			}

			if(id)
				return collection[id];

			return collection;
		}
	});

	$.widget("bitam.geolocation", {
		options: {},
		geolocationObject: {},
		initValue: {},

		_create: function(){
			
		},
		
		customCreate: function (fieldid) {
			var self = this;
			options = self.options,
			initValue = self.element.val();
			this.geolocationObject = new Geolocation(self.element);
			this.geolocationObject.init();
			var $el = this.geolocationObject.getHTML(fieldid);
			if($el) {
				$el.insertAfter(self.element);
				var user1Location = $(self.element).val();
				var optionsPlace = {
					types: ['geocode']};
				//Autocomplete				
				var opSelect2 = {
					minimumInputLength: 0,
					multiple: true,
					maximumSelectionSize: 1,
					width: '90%',
					//aqui debemos obtener los datos
					query: this.geolocationObject.getLocation,
					allowClear: true
				}			
				opSelect2.formatSearching = function(){return $.t('Searching')+'...';};
				opSelect2.formatNoMatches = function(){return $.t('No matches found');};
				opSelect2.minimumInputLength = 1;
				opSelect2.formatInputTooShort = function(){return $.t('Please enter 1 more character');};
				opSelect2.formatSelectionTooBig = function(){return $.t('You can only select 1 item');};
				opSelect2.containerCssClass = 'widget_field_alphanumeric';				
				//self.element.find('input').css("type: hidden");
				self.element.attr('type', 'hidden').select2(opSelect2);
				/*if(bitamApp.isMobile.Android()){
					$(self.element).on("select2-focus", function(e) {
						if($(this).data('selecting')) {
							$(this).data('selecting', false);
							return;
						}else{
							$(this).select2('data', null);
						}	
					});
					$(self.element).on("select2-selecting", function(e) {
						$(this).data('selecting', true);
					});
				}*/	
			}
			$(self.element).on('change', function(e){
				$input=this;
				if(e.removed){
					self.geolocationObject.latitude =undefined;
					self.geolocationObject.longitude =undefined;
					$(self.element).val("");
					self.showGoogleMapPosition($input);
				}
				if(e.added){
					var slatlng=$(self.element).val();
					if(slatlng && slatlng.length> 0) {							
						var alatlng = slatlng.split(",");
						if(alatlng[0] &&alatlng[1]){
							self.geolocationObject.latitude = alatlng[0];
							self.geolocationObject.longitude = alatlng[1];
							$(self.element).val(self.geolocationObject.latitude+', '+self.geolocationObject.longitude);							
						}
						self.showGoogleMapPosition($input);							
					} else {
						$(self.element).val(self.geolocationObject.latitude+', '+self.geolocationObject.longitude);
						alert('Location not found');
					}
				}
			})
             self.element.addClass('geolocation');
		},				
		setOptions: function (key, value) {
			this.options = { id: value };
             //navigator.notification.alert('options['+key+']: '+this.options[key]);
			this.geolocationObject.setOptions(key, value);
		},
		
		returnOptions: function() {
			return this.geolocationObject.returnOptions();
		},
				
		val: function(val){
			
		},
		
		getGeolocation: function(opt) {
			var self = this;
			if($.trim(initValue).length ==0 && ((opt.mode=="edition" && opt.field.element.edit=="1")||(opt.mode=="add" && opt.field.element.insert=="1"))) {
				this.geolocationObject.getGeolocation(self.element);				
			} else {
				var coords = ($.trim(initValue)).split(',');
				var position = {};
				position.coords = {};
				position.coords.latitude = $.trim(coords[0]);
				position.coords.longitude = $.trim(coords[1]);
				this.geolocationObject.setGeolocationPosition(position, self.element);				
			}
		},
		
		showGoogleMapPosition: function($input) {
			this.geolocationObject.showGoogleMapPosition($input);
		},
		
		enable: function(){
			
		},
		
		disable: function(val){
			
		},
		
		readonly: function(bReadOnly) {
			
		},
		
		destroy: function(){
			var self = this;
			$.Widget.prototype.destroy.call( self );
		}
	});
	
	$.widget("bitam.photo", {
		options: {},
		cameraObject: {},

		_create: function() {
			var self = this;
			options = self.options,
			initValue = self.element.val();
			cameraObject = new Camera(self.element);
			var $el = cameraObject.getHTML();
			$el.insertBefore(self.element);
			self.element.css('display','none');
		},
		
		setOptions: function (key, value) {
			options[key] = value;
             //navigator.notification.alert('options['+key+']: '+this.options[key]);
			cameraObject.setOptions(key, value);
		},
				
		val: function(val){
			
		},
		
		getPicture: function() {
             //navigator.notification.alert('widgets.photo');
             cameraObject.getPicture();
		},
		
		getPhoto: function(curtimestamp) {
			cameraObject.getPhoto(curtimestamp);
		},
		
		enable: function(){
			
		},
		
		disable: function(val){
			
		},
		
		readonly: function(bReadOnly) {
			
		},
		
		destroy: function(){
			var self = this;
			$.Widget.prototype.destroy.call( self );
		}
	});
	
	$.widget("bitam.signature", {
		options: {},
		signatureObject: {},
		sigPadObject: {},

		_create: function () {
			/*var self = this;
			options = self.options,
			initValue = self.element.val();
			signatureObject = new Signature(self.element);
			var $el = signatureObject.getHTML();
			$el.insertBefore(self.element);
			self.element.css('display','none');*/
			//$el.on('click', this.getSignature);
		},
		
		customCreate: function (fieldid) {
			var self = this;
			options = self.options,
			initValue = self.element.val();
			signatureObject = new Signature(self.element);
			var $el = signatureObject.getHTML(fieldid);
			$el.insertBefore(self.element);
			self.element.css('display','none');
		},
		
		initialize: function () {
			sigPadObject = $(signatureObject.sigPadElement).signaturePad({
				drawOnly: true,
				validateFields: false
			});
			if(signatureObject.selfElementValue != '') {
				sigPadObject.regenerate(signatureObject.selfElementValue);
				signatureObject.sigPadElement.next().val(signatureObject.selfElementValue);
			}
			
			if(bitamApp.isMobile.isApp()){
				var sigEvent = 'touchend';
			} else {
				var sigEvent = 'mouseup';
			}
			
			this.element.parent().find('[name="sigPad'+this.options.id+'"]').find('canvas').get(0).addEventListener(sigEvent, 
				$.proxy(function() {
					setTimeout(
						$.proxy(function() {
							$('[name="'+this.options.id+'"]').val($('[name="sigPad'+this.options.id+'"]').find('.output').val());
						}, this)
					, 100)
				}, this)
			);
			signatureObject.sigPadElement.find('.clearButton').on('click', function() {
				signatureObject.sigPadElement.next().val("");
				//alert('borrar');
			});
		},
		
		setOptions: function (key, value) {
			options[key] = value;
			signatureObject.setOptions(key, value);
		},
		
		getSignature: function (curtimestamp) {
			signatureObject.getSignature(curtimestamp);
		},
				
		val: function(val){
			
		},
		
		enable: function(){
			
		},
		
		disable: function(val){
			
		},
		
		readonly: function(bReadOnly) {
			
		},
		
		destroy: function(){
			var self = this;
			$.Widget.prototype.destroy.call( self );
		}
		
	});
	
	$.widget("bitam.uniqueid", {
		options: {},
		
		_create: function(){
			var self = this;
			options = self.options,
			initValue = self.element.val();
			if($.trim(initValue) == '') {
				//var uniqueidVal = AlphabeticID.encode(new Date().getTime(), 10);
				var length = undefined;
				if(self.options.length) {
					length = parseInt(self.options.length);
				} else {
					length = 6;
				}
				var uniqueidVal = AlphabeticID.encode(length);
				self.element.val(uniqueidVal);
			}
		},
		
		destroy: function(){
			var self = this;
			$.Widget.prototype.destroy.call( self );
		}
	});
	$.widget("bitam.NavMetro", {
		options: {},
		
		_create: function(nav,buildCatalog,idFather,divMaster,divbox){
			var self = this;
			options = self.options
			var section = {
			    sectionName:""
			};
			if(nav == undefined)
			{
			    BitamInterface.createMainNavigation(section);
			    self.element.css("background-color"," #FFFFFF");
			    self.element.empty();
			    var divMaster = $("<div/>",{"id":"divMaster","class":"firstLevel"});
			    var a = BitamInterface._getInstance(this);
			    var nav = a._appResponse.getNavigation();
				$('.navClon').remove();
				//valudamos cuando la navegacion es del tipo 3
				if(bitamApp.application.navSettings == "2")
				{
					var confPos = bitamApp.getConfiguration(304);
					switch(confPos) {
						case "1":
							classPos = "lowerleft";
							break;
						case "2":
							classPos = "upperright";
							break;
						case "3":
							classPos = "lowerright";
							break;
						default:
							classPos = "";
					}
					var divbox = $("<div/>",{"id":"divbox","class":classPos});
					self.element.append(divbox);
				}
			}
			//si tiene una imagen de fondo
			if(bitamApp.application.navSettings == "2")
			{
				var imgbackground = bitamApp.getConfiguration(303);
				imgbackground = bitamApp.buildURL(imgbackground);
				self.element.css("background-image","url('"+imgbackground+"')");
				self.element.css("background-position","center");
				self.element.css("background-repeat","no-repeat");
				self.element.css("height","479px");
				//self.element.css("-webkit-background-size","url("+imgbackground+")");
				divbox.append(divMaster);
			}else
			{
				self.element.append(divMaster);
			}
			var arrayCat = [];
			var conf = bitamApp.getConfiguration(302);
			for (key in nav)
			{  		
			   var actSection =  bitamApp.application.getSection(nav[key].sectionId);
			   //verifica la segurdad("si el usuario tiene permiso de ver esta navegacion.")
			   if(actSection && !checkViewSecurity(nav[key].view, bitamApp.user, actSection, 'canView') && bitamApp.application.codeName != 'eBavel')
					continue;
			   //Verifica si es un catalogo para menterlo en un arreglo
			   if(buildCatalog ||(isset(actSection) == false || (isset(actSection) && actSection.isCatalog == false)))
			   {
					divMaster.append(this._buildNav(key,nav,idFather,self));
			   }else{
					arrayCat.push(nav[key]);
			   }
			   //debugger;
			   //verifica si es un agrupador
			   if(isset(nav[key].action) && nav[key].action == "group")
			   {
			       var divgroup = $("<div/>",{"id":nav[key].id,"class":"secondLevel","style":"display:none"});
			       this._create(nav[key].childens,true,nav[key].id,divgroup,divbox);
			        var arrayDummie = [];
				var navCatalog = {
				label :"Home...",
				id:"divMaster",
				//image:bitamApp.buildURL("uploads/eBavel/Navs/"+conf+".png")
				image:"uploads/eBavel/Navs/"+conf+".png"
			    };
			    arrayDummie.push(navCatalog);
			    divgroup.append(this._buildNav(0,arrayDummie,"divMaster",self));
			       
			   }
			}
			if(!buildCatalog) {
			    var arrayDummie = [];
			    var navCatalog = {
					label :"Catalogs...",
					id:"cat",
					image:"uploads/eBavel/Navs/listgroup.png"
			    };
			    arrayDummie.push(navCatalog);
			    divMaster.append(this._buildNav(0,arrayDummie,"cat",self));
			    var divCatalogs = $("<div/>",{"id":"cat","class":"secondLevel","style":"display:none"});
			    this._create(arrayCat,true,"cat",divCatalogs,divbox);
			    var arrayDummie = [];
				var navCatalog = {
					label :"Home...",
					id:"divMaster",
					image:"uploads/eBavel/Navs/listback.png"
			    };
			    arrayDummie.push(navCatalog);
			    divCatalogs.append(this._buildNav(0,arrayDummie,"divMaster",self));
			}
		},
		_buildLink:function(key,nav){
		    
			if(isset(nav[key].section))
			{
			    section = nav[key].section;
			    var aFunc = $.proxy(function(){
				$("#navMain_"+this.id).addClass("transform");
				bitamApp.section(this.section);
			    }, {'section':section,'id':nav[key].id});
			}
			else if(isset(nav[key].view))
			{
			    section = nav[key].view;
			    var aFunc = $.proxy(function(){
				$("#navMain_"+this.id).addClass("transform");
				bitamApp.sectionView(this.section);
			    }, {'section':section,'id':nav[key].id});
			}else
			{
			   var aFunc = false;
			}
			return aFunc;
		},
		_buildNav: function(key,nav,idFather,self) {
			var navView = $("<div/>",{"id":"navMain_"+nav[key].id,"class":"navMetro"});
			//seteamos el vinculo de cada seccion.
			var funcClick = this._buildLink(key,nav);
			var conf = bitamApp.getConfiguration(302);
			if(!conf)
			{
				conf = "v0";
			}
			//2013-11-26@RTORRES:(#iphoneUse) pone la clase metro Nav ios
			
			if(bitamApp.isMobile.pantallasreducidas())
			{
			    navView.addClass("navMetroIOS");
			}
			var titleNav = $("<span/>"/*, {"id":"titlenav"+nav[key].id,"class":"titleNav","style":"color:black;"}*/).append(" "+nav[key].label);
			if(bitamApp.application.navSettings == "2")
			{
				navView.removeClass("navMetro");
				navView.css("margin-top","5%");
				navView.css("padding-right","2%");
				navView.css("padding-left","2%");
				var iconclass = "icon-chevron-right";
				if(nav[key].id == "divMaster")
				{
					iconclass = "icon-home";
				}else if(nav[key].action == "group" ||nav[key].id == "cat" )
					iconclass = "icon-th";
					
				//var titleNavDiv = $("<div/>",{"id":"titlenav"+nav[key].id,"class":"titleNavBox","style":"color:black;"}).append(titleNav);
				var titleNavDiv = $("<button/>",{"id":"titlenav"+nav[key].id,"class":"btn btn-default btn-lg btn-block","type":"button"}).append(
							$("<i/>",{"class":iconclass}),
							titleNav
						);
			}else
			{
				var titleNavDiv = $("<div/>",{"id":"titlenav"+nav[key].id,"class":"titleNav","style":"color:black;"}).append(titleNav);
			
			}
			//var backGround = navView.css("background-image");
			if(bitamApp.application.navSettings != "2")
			{
				var divImage = $("<div/>",{"style":"width:150px; height:150px; background-repeat: no-repeat; background-position: center;"});
				//var divImage = "";
				//ponemos la imagen como un background;
				if(nav[key].image == "" || nav[key].image == null )
					divImage.css("background-image","url(../../uploads/eBavel/Navs/"+encodeURIComponent(conf)+".png)");
				else {	
					if(bitamApp.isMobile.isApp() && !(nav[key].label == "Home..." || nav[key].label == "Catalogs...")) {	
						window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs){
							var imagen_name = [];
							imagen_name =nav[key].image.split("/");
							var imagePath = fs.root.fullPath + "/"+imagen_name[3];
							if(bitamApp.isMobile.Android()){							
								imagePath = imagePath.replace(/\s/g, "%20");							
								divImage.css("background-image","url("+imagePath+")");
							} else {
								fs.root.getFile(imagePath, { create: false }, function(fileEntry) {
									fileEntry.file(function readDataUrl(file) {
										var reader = new FileReader();
										reader.onloadend = function(evt) {
											divImage.css("background-image","url("+evt.target.result+")");
											//divImage = $("<div/>",{"style":"background-image:url("+evt.target.result+"); width:150px; height:150px; background-repeat: no-repeat;"});

										};
										reader.readAsDataURL(file);
									}, null);
								},null);
							}
						})
					}else {
						if(bitamApp.isMobile.isApp() && (nav[key].label == "Home..." || nav[key].label == "Catalogs..."))
						{
							divImage.css("background-image","url("+encodeURIComponent(nav[key].image)+")");
						   //divImage = $("<div/>",{"style":"background-image:url("+encodeURIComponent(nav[key].image)+"); width:150px; height:150px; background-repeat: no-repeat;"});

						}else
							divImage.css("background-image","url(../../"+encodeURIComponent(nav[key].image)+")");
						   //divImage = $("<div/>",{"style":"background-image:url(../../"+encodeURIComponent(nav[key].image)+"); width:150px; height:150px; background-repeat: no-repeat;"});

					}
				}
				navView.append(divImage);
			}	
			navView.append(titleNavDiv);
			navView.css("background-image","initial");
			//navView.css("background-repeat"," no-repeat");
			navView.css("position", "relative");
			navView.css("overflow"," hidden");
			//navView.css("background-color"," white");
			//se asigna el veento click de cada ventana
			navView.on('click',funcClick);
			if(!funcClick) {
				navView.click({"nav":nav,"key":key}, function(event){
					var id = event.data.nav[event.data.key].id;
					if($("#divMaster:visible").length > 0) {
						self.element.find("#"+id).show();
						self.element.find('.firstLevel').hide();
				   } else {
						self.element.find(".firstLevel").show();
						self.element.find('.secondLevel').hide();
					}
				}); 
			}
			return navView;
		},
		destroy: function(){
			var self = this;
			$.Widget.prototype.destroy.call( self );
		}
		
	});
})(jQuery);
