/*
	File: calendar.js
	Version: 2007/09/07 19:07
	Copyright 2006, 2007 Bitam
*/

Date.MONTHS = ML.slice(0,12);
Date.MONTHDAYS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
Date.DAYS = ML.slice(24,32);

Date.SUFFIXES = [
  'st','nd','rd','th','th','th','th','th','th','th',
  'th','th','th','th','th','th','th','th','th','th',
  'st','nd','rd','th','th','th','th','th','th','th',
  'st'
];
if (ML['lang']=='ES'){
	var DAYSSHORT = new Array(ML[32],ML[33],ML[34],ML[35],ML[36],ML[37],ML[31]);
}
else{
	var DAYSSHORT = ML.slice(31,39);
}

var calendar_inst = null;
var image_folder = 'images/';
var calendarLeftMinus = 0;
var calendarDiv1 = null;

function getElementWithId(anID)
{
	return (document.getElementById(anID));
}

function hideCalendarDiv()
{
	getCalendarDiv().style.visibility = "hidden";
}

function showCalendarDiv()
{
	var aComp = getCalendarDiv();
	aComp.style.zIndex = 500;
	aComp.style.overflow = "visible";
	aComp.style.visibility = "visible";
}

function popCalendar(event, callbackFunction, initialDateString, xCord, yCord)
{
	var aCal = new calendar();

	aCal.callbackFunc = callbackFunction;
	aCal.event = event;

	if (initialDateString != null)
		aCal.initialDateFromString(initialDateString);

	aCal.xCord = xCord;
	aCal.yCord = yCord;

	aCal.arg0 = arguments[5];

	aCal.outclicks = 0;

	calendar_registerClickEvent();
	calendar_registerKeyEvent();

	aCal.show();
}

function getCalendarDiv()
{
	if (calendarDiv1 == null)
	{
		calendarDiv1 =document.createElement('DIV');
		calendarDiv1.id = 'calendarDiv1';
		calendarDiv1.style.position = 'absolute';
		calendarDiv1.style.visibility = 'hidden';
		calendarDiv1.className = "calendar" ;

		document.body.appendChild(calendarDiv1);
	}

	return (calendarDiv1);
}

function calendar()
{
	if (calendar_inst != null)
		calendar_inst.close();

	calendar_inst = this;

	this.setDate = calendar_setDate;
	this.initialDateFromString = calendar_initialDateFromString;

	this.show = calendar_show;
	this.setLayerPosition = calendar_setLayerPosition;

	this.acceptCalendarInfo = calendar_acceptCalendarInfo;
	this.jsCodeForCalendar = calendar_jsCodeForCalendar;
	this.getDaysInMonth = calendar_getDaysInMonth;
	this.close = calendar_close;

	this.event = null;
	this.layer = getCalendarDiv();

	this.xCord = null;
	this.yCord = null;

	this.setDate(new Date());

	this.useMonthCombo  = true;
	this.useYearCombo   = true;
	this.yearComboRange = 5;
}

function calendar_initialDateFromString(aString)
{
	if (aString.indexOf(' ')!=-1){	// IRETTA - Si Contiene Horas
		var aAuxArray = aString.split(' ');
		aString = aAuxArray[0];
	}
	var anArray = aString.split('-');
	var aDate = new Date();
	aDate.setFullYear(anArray[0], anArray[1] - 1,anArray[2]);
	this.setDate(aDate);
}

function calendar_setDate(aDate)
{
	this.today	= aDate;

	this.date		= this.today.getDate();
	this.month	= this.today.getMonth();
	this.year		= this.today.getFullYear();

	this.currentMonth   = this.month;
	this.currentYear    = this.year;
}

function calendar_jsCodeForCalendar()
{
	return ('calendar_inst');
}

function calendar_acceptCalendarInfo(day, month, year)
{
	this.close();

	var newDate = new Date();
	newDate.setFullYear(year,month - 1,day);
	this.callbackFunc(newDate.format("YYYY-MM-DD"), this.arg0);
}

function calendar_close()
{
	hideCalendarDiv();

	this.outclicks = 0;
	calendar_unregisterClickEvent();
	calendar_unregisterKeyEvent();
}

function calendar_comboClick(anEvent)
{
	anEvent.cancelBubble = true;
}

function calendar_over(anObject)
{
	anObject.style.backgroundColor = '#000000';
	anObject.style.color = '#FFFFFF';
}

function calendar_out(anObject)
{
	anObject.style.backgroundColor = '#EEEEEE';
	anObject.style.color = '#6191cf';
}

function calendar_show()
{
	var month, year, monthnames, numdays, thisMonth, firstOfMonth;
	var ret, row, i, cssClass, linkHTML, previousMonth, previousYear;
	var nextMonth, nextYear, prevImgHTML, prevLinkHTML, nextImgHTML, nextLinkHTML;
	var monthComboOptions, monthCombo, yearComboOptions, yearCombo, html;

	this.currentMonth = month = arguments[0] != null ? parseInt(arguments[0]) : this.currentMonth;
	this.currentYear  = year  = arguments[1] != null ? parseInt(arguments[1]) : this.currentYear;
	anEvent  = arguments[2] != null ? arguments[2] : null;

	if (anEvent != null)
  	if (anEvent.srcElement)
      anEvent.cancelBubble = true
    else
      anEvent.stopPropagation()

	monthnames = Date.MONTHS;
	numdays    = this.getDaysInMonth(month, year);

	thisMonth = new Date(year, month, 1);
	if (ML['lang']=='ES'){
		firstOfMonth = thisMonth.getDay() -1;
	}
	else{
		firstOfMonth = thisMonth.getDay();
	}

	ret = new Array(new Array());
	for(i=0; i<firstOfMonth; i++)
		ret[0][ret[0].length] = '<td class="calendar_day">&nbsp;</td>';

	row = 0;
	i   = 1;
	while(i <= numdays)
	{
		if(ret[row].length == 7)
			ret[++row] = new Array();

		var sel = (i == this.date && month == this.month && year == this.year);

		cssClass = sel ? 'calendar_today' : 'calendar_day';

		linkHTML = ' style="cursor:pointer" ';

		if (!sel)
			linkHTML += 'onmouseover="calendar_over(this)" onmouseout="calendar_out(this)" ';

		linkHTML += 'onclick="javascript:' + this.jsCodeForCalendar() + '.acceptCalendarInfo(' + i + ', ' + (Number(month) + 1) + ', ' + year + ');">' + (i++) + '';

		ret[row][ret[row].length] = '<td class="' + cssClass + '" ' + linkHTML + '</td>';
	}

	for(i=0; i<ret.length; i++)
		ret[i] = ret[i].join('\n') + '\n';

	previousYear  = thisMonth.getFullYear();
	previousMonth = thisMonth.getMonth() - 1;

	if (previousMonth < 0)
	{
		previousMonth = 11;
		previousYear--;
	}

	nextYear  = thisMonth.getFullYear();
	nextMonth = thisMonth.getMonth() + 1;

	if (nextMonth > 11)
	{
		nextMonth = 0;
		nextYear++;
	}

	prevLinkHTML = '<a href=# style="cursor: pointer;" onclick="javascript: ' + this.jsCodeForCalendar() + '.show(' + this.currentMonth + ', ' + (this.currentYear - 1) + ',event)"><img src="' + image_folder + 'prevYear.gif" alt="<<" border="0" /></a>';
	prevLinkHTML += '&nbsp;<a href=# style="cursor: pointer;" onclick="javascript: ' + this.jsCodeForCalendar() + '.show(' + previousMonth + ', ' + previousYear + ',event)"><img src="' + image_folder + 'prev.gif" alt="<" border="0" /></a>';

	nextLinkHTML = '<a href=# style="cursor: pointer;" onclick="javascript: ' + this.jsCodeForCalendar() + '.show(' + nextMonth + ', ' + nextYear + ',event)"><img src="' + image_folder + 'next.gif" alt=">" border="0" /></a>';
	nextLinkHTML += '&nbsp;<a href=# style="cursor: pointer;" onclick="javascript: ' + this.jsCodeForCalendar() + '.show(' + this.currentMonth + ', ' + (this.currentYear + 1) + ',event)"><img src="' + image_folder + 'nextYear.gif" alt=">>" border="0" /></a>';

	var selected = false;

	if (this.useMonthCombo)
	{
		monthComboOptions = '';
		for (i=0; i<12; i++)
		{
			selected = (i == thisMonth.getMonth() ? 'selected="selected"' : '');
			monthComboOptions += '<option value="' + i + '" ' + selected + '>' + monthnames[i] + '</option>';
		}

		monthCombo = '<select rows=40 class=calendar_combo name="months" onclick="calendar_comboClick(event)" onchange="' + this.jsCodeForCalendar() + '.show(this.options[this.selectedIndex].value, ' + this.jsCodeForCalendar() + '.currentYear)">' + monthComboOptions + '</select>';
	}
	else
		monthCombo = monthnames[thisMonth.getMonth()];

	if (this.useYearCombo)
	{
		yearComboOptions = '';

		for (i = thisMonth.getFullYear() - this.yearComboRange; i <= (thisMonth.getFullYear() + this.yearComboRange); i++)
		{
			selected = (i == thisMonth.getFullYear() ? 'selected="selected"' : '');
			yearComboOptions += '<option value="' + i + '" ' + selected + '>' + i + '</option>';
		}

		yearCombo = '<select class=calendar_combo name="years" onclick="calendar_comboClick(event)" onchange="' + this.jsCodeForCalendar() + '.show(' + this.jsCodeForCalendar() + '.currentMonth, this.options[this.selectedIndex].value)">' + yearComboOptions + '</select>';
	}
	else
		yearCombo = thisMonth.getFullYear();

	html = '<table border="0" class="calendar_table" cellspacing="0" cellpadding="0">';
	html += '<tr height=20px><td class="calendar_header" align="left">' + prevLinkHTML + '</td><td colspan="5" nowrap class="calendar_header">' + monthCombo + ' ' + yearCombo + '</td><td class="calendar_header" align="right">' + nextLinkHTML + '</td></tr>';
	html += '<tr height=20px>';
	html += '<td class="calendar_dayname">' + DAYSSHORT[0] + '</td>';
	html += '<td class="calendar_dayname">' + DAYSSHORT[1] + '</td>';
	html += '<td class="calendar_dayname">' + DAYSSHORT[2] + '</td>';
	html += '<td class="calendar_dayname">' + DAYSSHORT[3] + '</td>';
	html += '<td class="calendar_dayname">' + DAYSSHORT[4] + '</td>';
	html += '<td class="calendar_dayname">' + DAYSSHORT[5] + '</td>';
	html += '<td class="calendar_dayname">' + DAYSSHORT[6] + '</td></tr>';
	html += '<tr  height=20px>' + ret.join('</tr>\n<tr  height=20px>') + '</tr>';
	html += '</table>';
	this.layer.innerHTML = html;
	this.setLayerPosition();
	if (!arguments[0] && !arguments[1])
	{
		showCalendarDiv();
	}
}

function calendar_setLayerPosition()
{
	var x = this.xCord == null ? this.event.clientX : this.xCord;
	var y = this.yCord == null ? this.event.clientY : this.yCord;
/*
	if (window.document.documentElement.clientHeight == 0){
		var anObject = document.body;
	}
	else {
		var anObject = window.document.documentElement;
	}
*/
  if (window.innerWidth) // w3c
  {
    var limitX = window.innerWidth + window.scrollX
    var limitY = window.innerHeight + window.scrollY
    x = x - calendarLeftMinus + window.scrollY - this.layer.offsetWidth/ 2;
    y = y + window.scrollY;
  }
  else //ie
  {
    var anObject = window.document.documentElement
    var limitX = anObject.offsetWidth + anObject.scrollLeft
    var limitY = anObject.offsetHeight + anObject.scrollTop
  	x = x - calendarLeftMinus + anObject.scrollLeft - this.layer.offsetWidth/ 2;
  	y = y + anObject.scrollTop;
  }
/*
	var limitX = anObject.scrollLeft + anObject.clientWidth;
	var limitY = anObject.scrollTop + anObject.clientHeight;

	x = x - calendarLeftMinus + anObject.scrollLeft - this.layer.clientWidth/ 2;
	y = y + anObject.scrollTop;
*/
	if (x + this.layer.clientWidth > limitX) {
		x = limitX - this.layer.clientWidth - 6;
	}
	if (x < 0) { x = 0; }

	if (y + this.layer.clientHeight > limitY) {
		y = limitY - this.layer.clientHeight - 6;
	}
	if (y < 0) { y = 0; }

	this.layer.style.left = x + 'px';
	this.layer.style.top  = y + 'px';
}

function calendar_getDaysInMonth(month, year)
{
	var monthdays = Date.MONTHDAYS;

	if (month != 1)
		return monthdays[month];
	else
		return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0 ? 29 : 28);
}

function calendar_checkKey(anEvent)
{
	if (anEvent.keyCode == 27)
		if (calendar_inst != null)
			calendar_inst.close();
}

function calendar_checkClick(anEvent)
{
	if (calendar_inst != null)
		if (calendar_inst.outclicks == 0)
			calendar_inst.outclicks++;
		else
			calendar_inst.close();
}

function calendar_registerClickEvent()
{
	var anObject = document;

	if (anObject.attachEvent)
		anObject.attachEvent("onclick", calendar_checkClick);
	else
		anObject.addEventListener("click", calendar_checkClick, false);
}

function calendar_unregisterClickEvent()
{
	var anObject = document;

	if (anObject.detachEvent)
		anObject.detachEvent("onclick", calendar_checkClick);
	else
		anObject.removeEventListener("click", calendar_checkClick, false);
}

function calendar_registerKeyEvent()
{
	var anObject = document;

	if (anObject.attachEvent)
		anObject.attachEvent("onkeydown", calendar_checkKey);
	else
		anObject.addEventListener("keydown", calendar_checkKey, false);
}

function calendar_unregisterKeyEvent()
{
	var anObject = document;

	if (anObject.detachEvent)
		anObject.detachEvent("onkeydown", calendar_checkKey);
	else
		anObject.removeEventListener("keydown", calendar_checkKey, false);
}


Date.prototype.format = function( mask )
{
  var formatted     = ( mask != null ) ? mask : 'DD-MMM-YY';
  var letters       = 'DMYHdhmst'.split( '' );
  var temp          = new Array();
  var count         = 0;
  var regexA;
  var regexB        = /\[(\d+)\]/;

  var day           = this.getDay();
  var date          = this.getDate();
  var month         = this.getMonth();
  var year          = this.getFullYear().toString();
  var hours         = this.getHours();
  var minutes       = this.getMinutes();
  var seconds       = this.getSeconds();

  var formats       = new Object();
  formats[ 'D' ]    = date;
  formats[ 'd' ]    = date + Date.SUFFIXES[ date - 1 ];
  formats[ 'DD' ]   = ( date < 10 ) ? '0' + date : date;
  formats[ 'DDD' ]  = Date.DAYS[ day ].substring( 0, 3 );
  formats[ 'DDDD' ] = Date.DAYS[ day ];
  formats[ 'M' ]    = month + 1;
  formats[ 'MM' ]   = ( month + 1 < 10 ) ? '0' + ( month + 1 ) : month + 1;
  formats[ 'MMM' ]  = Date.MONTHS[ month ].substring( 0, 3 );
  formats[ 'MMMM' ] = Date.MONTHS[ month ];
  formats[ 'Y' ]    = ( year.charAt( 2 ) == '0' ) ? year.charAt( 3 ) : year.substring( 2, 4 );
  formats[ 'YY' ]   = year.substring( 2, 4 );
  formats[ 'YYYY' ] = year;
  formats[ 'H' ]    = hours;
  formats[ 'HH' ]   = ( hours < 10 ) ? '0' + hours : hours;
  formats[ 'h' ]    = ( hours > 12 || hours == 0 ) ? Math.abs( hours - 12 ) : hours;
  formats[ 'hh' ]   = ( formats[ 'h' ] < 10 ) ? '0' + formats[ 'h' ] : formats[ 'h' ];
  formats[ 'm' ]    = minutes;
  formats[ 'mm' ]   = ( minutes < 10 ) ? '0' + minutes : minutes;
  formats[ 's' ]    = seconds;
  formats[ 'ss' ]   = ( seconds < 10 ) ? '0' + seconds : seconds;
  formats[ 't' ]    = ( hours < 12 ) ?  'A' : 'P';
  formats[ 'tt' ]   = ( hours < 12 ) ?  'AM' : 'PM';

  for ( var i = 0; i < letters.length; i++ )
  {
    regexA = new RegExp( '(' + letters[ i ] + '+)' );
    while ( regexA.test( formatted ) )
    {
      temp[ count ] = RegExp.$1;
      formatted = formatted.replace( RegExp.$1, '[' + count + ']' );
      count++;
    }
  }

  while ( regexB.test( formatted ) )
  {
    formatted = formatted.replace( regexB, formats[ temp[ RegExp.$1 ] ] );
  }

  return formatted;
}