function getPicture(questionID, dynConsecutive, isDynamic, isMultiple) {
	if (!objSettings) {
		return;
	}

	if (!selSurvey || !selSurvey.currentCapture || !selSurvey.currentCapture.question) {
		return;
	}
	
	if (objSettings.webMode) {
		//En modo Web se invoca a subir un archivo mediante Ajax directamente a una ruta temporal usando los datos de la captura actual, lo cual
		//se hace directamente con el Widget de fileUpload.js así que no es necesario invocar a este método
	}
	else {
		//En un App se usa directamente al dispositivo para tomar la foto, la cual regresa como el contenido directo en B64
		var pictureSource=navigator.camera.PictureSourceType;
		var destinationType=navigator.camera.DestinationType;
		if (objSettings.isiPad || objSettings.isTabletAndroid) {
			var photoOptions = { quality: photoQuality, targetWidth: photoWidth, targetHeight: photoHeight};
		} else {
			var photoOptions = { quality: photoQuality, targetWidth: photoWidth, targetHeight: photoHeight};
		}
		
		navigator.camera.getPicture(function onPhotoDataSuccess(imageData) {
			if (objApp && selSurvey) {
				surveyDate = '';
				surveyHour = '';
				var photoFileName = 'photo_' + objSettings.user + '_' + questionID + '_' + surveyDate + surveyHour + selSurvey.id;
				objData = new Object();
				objData.surveyID = selSurvey.id;
				objData.questionID = questionID;
				objData.recordNum = selSurvey.currentCapture.recordNum;
				objData.path = 
				objApp.photoTook(imageData, objData);
			}
			/*
			if(isDynamic != undefined) {
				var qimageencode = document.getElementById('qimageencode'+questionID+'-'+dynConsecutive+'-0');
				var qfimageencode = document.getElementById('qfimageencode'+questionID+dynConsecutive);
			} else {
				var qimageencode = document.getElementById('qimageencode'+questionID);
			}
			if(isDynamic != undefined) {
				qfimageencode.style.display = 'block';
				qfimageencode.src = 'data:image/jpeg;base64,' + imageData;
			}
			qimageencode.style.display = 'block';
			qimageencode.src = 'data:image/jpeg;base64,' + imageData;
			if(isMultiple == undefined) {
				savePhotoToFile(imageData,questionID,dynConsecutive, isDynamic);
			}
			*/
		}, 
		function onFail(message) {
			if(message != 'Camera cancelled.' && message != 'no image selected' && message != '3') {                            
				navigator.notification.alert('Capture photo is not supported in your device: '+message,null,'Device error');
			}              
		}, photoOptions);
	}
}

function savePhotoToFile(imageData,questionID,dynConsecutive,isDynamic, multipleIndex) {
	
	//@MABH 2012-04-12: Ahora el nombre del archivo de photo debe incluir
						//el surveydate,surveyhour y surveyid
	var surveyDate = $('#surveyDate').val();
	surveyDate = surveyDate.replace(/-/gi, '');
	var surveyHour = $('#surveyHour').val();
	surveyHour = surveyHour.replace(/:/gi, '')	
	var surveyID = $('#surveyID').val();
	
	if(multipleIndex != undefined) {
		questionID = questionID + '-' + multipleIndex;
	}
	
	var photoFileName = 'photo_'+user+'_'+questionID+'_'+surveyDate+surveyHour+surveyID;
	if(isDynamic != undefined) {
		photoFileName += '_'+dynConsecutive;
	}
	ESFile.writeFile(photoFileName, imageData, function() {},function() {});
}

