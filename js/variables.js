function VARIABLES() {
	this.appVersion = 6.02001;
	this.serviceVersion = 6.02001;
	this.updateVersion = {
		esvOriginalVersion : 1.00000,				//Versión inicial del ESurvey (sin SkipLogic)
		esvNewSkipLogic : 2.00000,					//Versión que agregó el cambio en los SkipLogic a como actualmente se conocen
		esvIntelligentBehaviour : 3.00000,			//Versión con soporte de descargas inteligentes de catálogos a través de la versión de los registros (es la primera en la que se integró JPuente)
		esvAgendaCatalogFilter : 3.00002,			//Versión con soporte de filtrado de agendas cuando el Web Service responde el array completo de filtro para cada padre incluyendo el Atributo usado en la Agenda
		esvSurveyVersionNumber : 3.00003,			//Versión con actualización del número de versión de las encuestas (sólo registra cambios en su estructura, no en la de los catálogos asociados a esta)
		esvSurveyAddNewAnswers : 3.00004,			//JAPR 2012-04-02: Versión que permite agregar nuevas respuestas en las preguntas de Selección sencilla sin catálogo en modo de despliegue vertical/menu (opción de "Otro")
		esvSurveyMasterDetSection : 3.00005,		//JAPR 2012-04-02: Versión que soporta secciones Maestro - Detalle
		//Por problemas de generación de varios Apps que incrementaron el número de versión, las versiones 3.00006, 3.00007 y 3.00008 NO se sincronizaron en cuanto a número con el Web Service, el cual siguió siendo 3.0005
		//a partir de 3.0009 se va a mantener siempre sincronizado
		esvGlobalCatalogs : 3.00009,				//JAPR 2012-05-07: Versión que soporta catálogos que sólo se usan en preguntas y no en encuestas, además ya no regresa el attributeValues a nivel de las preguntas
		esvActionQuestions : 3.00010,				//JAPR 2012-05-15: Versión que soporta preguntas de tipo action para sincronizar con el eBavel
		esvSurveyReschedule : 3.00011,				//JAPR 2012-05-29: Versión que soporta recalendarizar las agendas en base a las encuestas contestadas, además de permitir especificar la cantidad de posiciones decimales en las preguntas numéricas
		esvHelloAndSettings : 3.00012,				//JAPR 2012-05-29: Soporte de "Hello" para verificación de conexión. Correcciones variadas tanto en servicio como en móviles (configuraciones extras para foto, GPS y demás)
		//La versión 3.00013 del Web Service no tuvo cambios por lo que realmente no se instaló con dicho número
		esvFormatMaskHideAttribs : 3.00014,			//JAPR 2012-08-02: Versión que permite especificar un formato para las preguntas numéricas, además de ocultar atributos del catálogo
		esvAppOptimization300015 : 3.00015,			//JAPR 2012-08-31: La versión del servicio no cambió. Se generó por compatibilidad con la versión del App que integró el PhoneGap0.9.5 nuevamente y optimizó el ChangePage (versión de release a la tienda)
		esvPhotosFromCanvas : 3.00016,				//Versión que genera las fotos utilizando el canvas, debido a que en el phoneGap 0.9.5 no funcionan los parámetros Width y Height, y era necesario usar esa versión para que funcionara establemente en IOs
		esveBavelSuppScoresCube : 3.00016,			//JAPR 2012-09-05: Versión que corrige un bug en la integración con eBavel por nombres de clases repetidos entre productos. Contiene además el cubo global de Scores para las capturas de encuestas
		esvAltEMailForNotif : 3.00017,				//JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
		esvExtendedActionsData : 3.00018,			//JAPR 2012-10-29: Agregada la información extra con variables que se refieren a las preguntas en las acciones de eBavel
		esvServerDateTime : 3.00019,				//MABH 2012-12-12: Agregadas las dimensiones con la fecha de captura pero según el servidor
		esvMultiDynamicAndStandarization : 4.00000,	//JAPR 2012-05-29: Soporte para múltiples secciones dinámicas y estandarización para soportar todo tipo de preguntas/opciones en todas las secciones
		esveFormsV41Func : 4.01000,					//JAPR 2012-10-19: Soporte para las característivas de v4 Reestructurada (v4.1)
		esveFormsStdQSingleRec : 4.01001,			//JAPR 2013-01-10: Agregada la configuración por encuesta para permitir generar un registro especial para las preguntas de secciones estáticas y con esto impedir que los registros de otras secciones repitan valores de dichas preguntas
		esvCatalogDissociation : 4.02000,			//JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		esvShowHideQuestionsImpr : 4.03000,			//JAPR 2013-01-18: Modificado el método de ocultar/mostrar preguntas así como los saltos para optimizar y corregir de raiz todos los casos
		esvSendReportButton : 4.03001,				//JAPR 2013-01-18: Agregado el botón para subir los archivos locales del móvil, además se incluyó la versión del App que graba los outbox
		esvCustomOtherComentLabelv403 : 4.03005,	//JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones de preguntas
		//JAPR Agregada funcionalidad para soporte de encuestas tipo iusacell
		esvSyncQuestionAndSkipFoto : 4.04000,		//JAPR 2013-01-18: Agregado el tipo de pregunta Sincronización y la opción para que al tomar foto haga un brinco de sección (tipo Skip to section, no Change Section)
		esveBavelMapping : 4.04002,					//JAPR 2013-01-18: Agregado el mapeo directo de campos de eBavel para agregar valores en las formas de dicho producto durante la captura de eForms (independiente a las preguntas tipo acción, aunque si se combinan tendrían que usar la misma forma de eBavel para ambas cosas)
		//esvCallList : 4.04002,					//JAPR 2013-07-02: Por solicitud de LRoux, se incrementó la versión de metadata de esta funcionalidad para evitar que al liberar nueva funcionalidad se utiliza esta, ya que al dia de este cambio no aplicaba para IOs todavía, el valor original era 4.04002
		esvCustomOtherComentLabelv404 : 4.04004,	//JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones de preguntas
		esveFormsAgent : 4.04006,					//JAPR 2013-05-06: Versión que introduce el uso del eForms Agent para cargar los datos posterior al proceso de sincronización, el cual simplemente dejará el archivo de outbox en el server a partir de ahora
		esveBavelSuppActions : 4.04007,				//JAPR 2013-05-06: Agregado el soporte de acciones de eBavel mapeadas a una forma específica desde acciones fijas en opciones de respuesta de simple y multiple choice
		esvPulsoSaveFromCatFilter : 4.04009,		//JAPR 2013-08-26: Esta realmente no es una versión del App sino una validación en el server para grabar las preguntas Simple choice de catálogo a partir de la propiedad catalogFilter en lugar de la propiedad answer, a raiz de un problema con celulares LG Optimus L5 que mandaban mal los datos
		esveBavelSuppActions123 : 4.04010,			//JAPR 2013-05-06: Corregido un bug con los usuarios Supervisores 1 al 3 y el MySelf como responsables de acciones, antes de esta versión no se aceptaban números negativos así que se incluyeron dummies con valores cercanos al MAX_LONG para forzar a que se vieran en las Apps
		esvHideAnswers : 4.04013,					//JAPR 2013-07-23: Agregada la opción para ocultar las respuestas de simple y multiple choice durante la captura nada mas, ya que en reportes seguirán apareciendo. Agregado el soporte para ShowQuestions en múltiple choice
		esvAutoRedrawFmtd : 4.04017,				//JAPR 2013-11-19: Agregada la opción para redibujar automáticamente las secciones formateadas cada que se piden, además se agregaron algunas variables de sistema
		esvGPSFixAndAppUpdateScript : 4.04019,		//JAPR 2013-11-19: Agregado en el App código para invocar al SetGPSWatch cada 30 segundos, buscando con eso corregir los problemas de GPS. Agregado código para permitir inyectar código javascript en las Apps. Agregado el log de sincronizaciones
		esvAccuracyValues : 4.04020,				//JAPR 2013-12-02: Agregadas las dimensiones e indicadores de Accuracy tanto a las encuestas como al cubo Global de Encuestas (agregadas dinámicamente la primera vez que se carga una instancia individual de encuesta)
		esvSimpleChoiceSharing : 4.04021,			//JAPR 2013-12-05: Agregada la opción para que las preguntas de tipo simple choice puedan compartir las mismas opciones de respuesta definidas en otra pregunta anterior, de forma que sólo se tengan que agregar/editar en un mismo lugar
		esvSectionVariables : 4.04021,				//JAPR 2013-12-05: (En el App) Agregadas las variables de secciones para permitir obtener datos de su definición, así como información del catálogo en el caso de las secciones dinámicas
		esvShowSectionCondition : 4.04022,			//JAPR 2013-12-10: Agregada la condición para determinar si una sección se muestra o no durante la captura
		esvDefaultValueFixApp : 4.04023,			//JAPR 2013-12-16: Corrección en el App para el despliegue y grabado de preguntas (open y SCH) que tenían un defaultValue dentro de secciones dinámicas, ya que no consideraba el número de registro ni su visibilidad por registro correctamente
		esvGPSSettingAndOutboxCheck : 4.04024,		//JAPR 2013-12-17: Ahora la opción de Enable GPS Watch ya no es configurable y estará siempre activa. Se agregó una configuración en el server para registrar los datos del GPS al inicio de la captura. Agregado código para verificar si el archivo de Outbox se subió correctamente al server en lugar de mostrar error inmediatamente
		esvReportEMailsByCatalog : 4.04025,			//JAPR 2014-01-24: Agregada la opción de la encuesta para definir EMails adicionales a los que se enviarán los reportes de las capturas, pudiendo utilizar referencias a preguntas mediante variables
		esvGPSQuestion : 4.04026,					//JAPR 2014-01-27: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		esvGPSVariables : 4.04027,					//JAPR 2014-02-12: Agregadas las variables tipo GPS (sólo aplica en los móviles por ahora). Ajustadas opciones de configuración de encuesta y pregunta, removiendo cosas innecesarias y compactando otras (validaciones extras)
		esvAppUserLanguage : 4.04030,				//JAPR 2014-06-30: Agregadas la posibilidad de variar el idioma de la Aplicación basado en el idioma del usuario en KPIOnline
		esvAppCustomBackgroundV4 : 4.04031,
		esvAppCustomBackgroundVersionV4 : 4.04032,
		esvSurveyProfiles : 4.04033,				//JAPR 2014-07-09: Agregada la funcionalidad para definir perfiles que limiten las encuestas que los usuarios pueden ver a partir de ciertos elementos presentes en la definición de las mismas (por ahora todo se configura directo en la metadata)
		esvCuentaCorreoAlt : 4.04033,
		esvCallList : 5.00000,						//JAPR 2013-07-02: Por solicitud de LRoux, se incrementó la versión de metadata de esta funcionalidad para evitar que al liberar nueva funcionalidad se utiliza esta, ya que al dia de este cambio no aplicaba para IOs todavía, el valor original era 4.04002
		esvPasswordLangAndRedesign : 5.00000,		//JAPR 2013-07-02: Agregada el tipo de pregunta Password. Agregadas las traducciones. Rediseñado eForms en cuanto a colores y estética de algunas ventanas / opciones (no hay rediseño del grabado)
		esvHideNavigationButtons : 5.00004,			//JAPR 2014-04-05: Agregada la opción para ocultar los botones de navegación (Next y Back) durante la captura de una encuesta
		esveBavelCatalogs : 5.00005,				//JAPR 2014-05-13: Agregado el soporte para mapear catálogos de eBavel
		esvNearBy : 5.00005,						//MABH 2014-05-16: Agregado el uso de Google Maps para preguntas de catálogo
		esvDefaultSurvey : 5.00006,					//MABH 2014-05-21: Agregada la encuesta default a cargar durante el login al App
		esvInlineSection : 5.00006,					//JAPR 2014-05-19: Agregado el tipo de sección Inline
		esvDefaultOption : 5.00006,					//MABH 2014-05-16: Agregada la configuración para asignar opciones por default a preguntas múltiple choice
		esvSurveyMenu : 5.00007,					//MABH 2014-05-27: Agregado el uso de agrupadores de menu para las encuestas
		esvMCHSwitchBehav : 5.00008,				//JAPR 2014-06-02: Agregado el switch para cambiar el comportamiento de las marcas de las preguntas Múltiple choice. Agregada la opción para mostrar un Checkbox en preguntas múltiple choice con captura numérica o texto (el checkbox indicaría si se capturó o no un valor)
		esvSectionFmtdHeadFoot : 5.00009,			//JAPR 2014-06-02: Agregado el header y footer formateado de las secciones
		esvCatalogDynFilters : 5.00010,				//JAPR 2014-06-02: Agregado el soporte para filtros dinámicos de catálogo a nivel de encuesta. Realmente no hay cambios de metadata ni validaciones, pero hasta esta versión es cuando se empezaron a aplicar verdaderamente estos filtros
		esvAppColorTmpls : 5.00011,					//JAPR 2014-06-18: Agregado el soporte para templates personalizados de la interface del App
		esvSectionRecap : 5.00012,					//MABH 2014-06-18: Agregadas las secciones tipo Reporte para mostrar un resumen de las acciones que se generarán en eBavel al grabar la captura actual
		esvFixedInlineSections : 5.00012,			//JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus renglones en lugar de utilizar un catálogo
		esvAppUserLanguageV5 : 5.00013,				//JAPR 2014-06-30: Agregadas la posibilidad de variar el idioma de la Aplicación basado en el idioma del usuario en KPIOnline
		esvAppCustomBackground : 5.00014,
		esvDeviceDataLog : 5.00016, 				//Conchita Agregada informacion extra al log (tamaño de pantalla y dispositivo)
		esvResponsiveDesign : 5.00017,				//JAPR 2014-07-30: Agregado el Responsive Design para responder con HTML o imagenes ajustadas al tipo de dispositivo que hace el request
		esvFormsMenuPosition : 5.00017,				//MABH 2014-08-05: Agregada la configuración para posicionar el menu de formas en el App
		esvShowInSummary : 5.00017,					//MABH 2014-08-07: Agregada la opción para indicar si las preguntas de secciones maestro-detalle se deben o no ver en el summary
		esvSurveySectDisableOpts : 5.00018,			//MABH 2014-08-07: Agregada la opción para indicar si la encuesta deberá o no grabar datos. Agregada la opción para ocultar secciones del menú de navegación
		esvShowInOnePageMD : 5.00018,				//Conchita 2014-08-07 Agregado el mostrar en una sola pagina el summary y el section de las maestro detalle
		esvRecapDynamicData : 5.00018,				//JAPR 2014-08-21: Agregada la descarga dinámica de valores de eBavel en secciones tipo Recap
		esvMenuImage : 5.00018,
		esvQuestionAudioVideo : 5.00019,
		esvSketch : 5.00021, 						//Conchita Tipo sketch con width, height e imagen desde el server
		esvIndependentImgDownload : 5.00021,		//JAPR 2014-09-03: Agregada la descarga independiente de las definiciones (es decir, ya no como B64) de todas las imagenes asociadas a las encuestas, de tal forma que se descarguen una a una mediante FileTransfer y el HTML haga referencia a ellas como imagenes directamente
		esvExtendedModifInfo : 5.00021,				//JAPR 2014-09-03: Agregados los datos extendidos de modificación, adicionalmente ahora las configuraciones que usen variables grabarán internamente los IDs de preguntas aunque seguirán regresando al App los números de preguntas y se configurarán en pantalla a partir del número
		esvAgendaRedesign : 5.00022,				//MABH 2014-09-26: Agregado el rediseño de las Agendas
		esvInlineMerge : 5.00023,					//JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa, de tal forma que todas se vean en una misma página (sólo aplica a secciones con despliegue Inline)
		esvQuestionFilters : 5.00023,				//JAPR 2014-09-30: Agregados los filtros de preguntas para definir colores de los marcadores en simple choice con mapa
		esvSectionQuestions : 5.00024,				//JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
		esvSketchImage : 5.00025,					//Agregado para las imagenes desde el server de las tipo sketch
		esvInlineTypeOfFilling : 5.00025,			//JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		esveBavelScoreMapping : 5.00026,			//JAPR 2014-10-23: Agregado el mapeo de Scores
		esvEditorHTML : 5.00027,					//Sep-2014: Versión con el nuevo diseñador de html
		esveBavelGeolocationAttribs : 5.00029,		//JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		esvAgendaDimID : 5.00030,					//JAPR 2014-11-18: Agregada la dimensión Agenda
		esvIsInvisible : 5.00033,					//Conchita agregada propiedad para question para que sea invisible la pregunta
		esvAgendaWithValues : 5.00034,				//JAPR 2014-12-03: Agregado el array de valores del catálogo de la agenda
		esvCatalogAttributeImage : 5.00035,			//MABH 2014-12-01: Agregado el uso de imagenes en atributos de catalogo para secciones inline
		esvAgendaScheduler : 5.00035,            	//JCEM  2014-11-24: Agrega la generación de agendas desde planificador
		esvDeleteObjectsValidation : 5.00040,		//Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar si es utilizado y advertir al usuario (#75TPJE)
		esvMinRecordsToSelect : 5.00041,			//2015.02.10 JCEM #9T6ASP Cantidad minima de registros a seleccionar en seccion inline
		esvAgendaWithCatValues : 5.00043,			//JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		esvActionConditions : 5.00043,				//JAPR 2015-03-20: Agregadas las condiciones para evaluar si debe o no grabarse una acción hacia eBavel
		esvRedesign : 5.01000,			        	//AAL 19/03/2015 Agregado para el Rediseño del Admin
		esveFormsv6 : 6.00000,			  			//JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente. Esta constante sólo se debe usar con temas relacionados con el grabado o validaciones exclusivas de esta versión, no para validaciones de Metadata ni aunque sean exclusivas de esta versión
		esvAdminWYSIWYG : 6.00000,			  		//JAPR 2015-03-31: Rediseñado el Admin con DHTMLX. Se puede utilizar para validaciones de Metadata o relacionadas con la ventana de Diseño de Formas WYSIWYG
		esveFormsDHTMLX : 6.00006,			  		//JAPR 2015-08-04: Integrado el primer componente DHTMLX para eForms App (el DataView de las formas)
		esveServicesConnection : 6.00008,			//Cambios a DataDestinations
		esvDeleteReports : 6.00009,					//JAPR 2015-08-13: Implementado el método para eliminar las capturas (se agregó el campo SurveyKey a todas las tablas)
		esvMenuImageConfig : 6.00009,				//GCRUZ 2015-08-21: Configuración de imagen de menu (se agregó el campo SurveyMenuImage a todas las tablas)
		esveAgendas : 6.00010,						//JAPR 2015-08-27: Había faltado esta constante de ARamirez). Agregada la integración de Agendas v6
		esveHideNavButtonsSection : 6.00011,   		//RTORRES 2015-08-28: Agregado la fucionalidad de ocutar los botoenes de next,back
		esvDataSourceDocument : 6.00012,			//JRPP 2015-09-03: Adrego campo documento 
		esvDataSourceAgent : 6.00014,      			//JAPR 2015-09-06: Agregado el cambio para que el agente procese los DataSource desde origenes de DropBox y demás en base a una frecuencia
		esvDataSourceAgentRecurActive : 6.00015, 	//JAPR 2015-09-06: Agregado el cambio para que el agente procese los DataSource desde origenes de DropBox y demás en base a una frecuencia
		esvDataSourceIdGdrive : 6.00016,     		//JRPP 2015-09-11: Columna agregada para manipular guardar el id del documento de googledrive
		esvDataSourceAgentHours : 6.00016,			//JRPP 2015-09-11: Columna agregada para manipular guardar el id del documento de googledrive
		esvLinks : 6.00025,							//Agregar módulo de Links
		esvDataSourceErrors : 6.00025,     			//JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
		esvDataDestination : 6.00026,				//JRPR 2015-12-02: Se agrega las opciones de envio de dashboard y ARS por email.
		esvMaxQLength : 6.00026,    				//JAPR 2015-12-10: Corregido un bug, no estaba manteniendo el Length histórico del campo, así que se podía reducir su tamaño si se disminuía primero y posteriormente se incrementaba (#8PT8T5)
		esvHideDescription : 6.00026,    			//OMMC 2015-12-15: Agregado para ocultar la descripción de las formas en el admin.
		//JAPR 2016-02-24: Debido al problema de haber actualizado en tienda a GeoControl a la 6.00028, se decidió este día cambiar el esquema de versiones de eForms, de tal manera que la que
		//en esta fecha era 6.00028 se eliminó por completo, la que hubiera sido 6.00029 se movió a la 6.01000, mientras que se tuvo que hacer un rollback de GeoControl (y por ende de eForms)
		//donde la versión 6.00027 se renombró a 6.00029 y se volvió a subir a las tiendas, la que hubiera sido 6.00028 en caso de necesitarse, se subirá a las tiendas como 6.00030 pero NO para
		//GeoControl, ya que GeoControl sólo se actualizará bajo estricta autorización de David, y tentativamente viendo estos problemas, seguiría su propio número de versión independiente
		//a eForms dado a que no necesariamente quieren igualar en funcionalidad a eForms, sólo algunas correcciones
		//A partir de este punto hacia abarjo, los números 6.00030 originalmente eran 6.00028, mientras que los 6.01000 originalmente eran 6.00029, NO habría números >= 6.00030 del esquema antes de este problema
		esvOCRQuestionFields : 6.01000,        		//OMMC 2015-11-27: Agregado para propiedades de preguntas OCR.
		esvParameterHTTP : 6.00030,        			//JRPP 2016-01-14: Agregado los parametros en tabla datadestination
		esvTypeOfDatabase : 6.00030,    			//JRPP 2016-01-14: Agregado el tipo de Base de datos en servicesconnection
		esvHTMLEditorState : 6.00030,        		//OMMC 2016-01-18: Agregado el estado en el que se abre el editorHTML
		esveServicesConnectionName : 6.00030,		//JRPP 2016-01-14: Agregado isIncremental en tabla datadestination
		esvShowTotalsInline : 6.00030,				//JAPR 2016-01-22: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
		esvAllowGallery : 6.00030,         			//OMMC 2016-02-24: Agregada para permitir acceso a la galería
		esvExitQuestion : 6.01000,					//JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
		esvDestinationIncremental : 6.01000,		//JRPP 2016-01-14: Agregado isIncremental en tabla datadestination
		esvadditionaleMail : 6.01000,      			//JRPP 2016-01-28: Se agrega nuevo campo a la tabla si_sv_datadestinations
		esvFormsLayouts : 6.01000,					//JAPR 2016-01-28: Agregados los LayOuts personalizados por forma (#FSMUZC)
		esvAgendaFirsDayWeek : 6.01000,				//JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
		esvATTRDIDSyntax : 6.01000,        			//OMMC 2016-02-24: Agregada la variable para soportar la sintaxis nueva en el editor de fórmulas.
		esvAgendaExportTemplate : 6.01000,			//GCRUZ 2016-02-25: Agregada configuración para seleccionar formas al exportar el template para agendas
		esvUpdateData : 6.01002,					//@JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
		esvShowQNumbers : 6.01002,           		//OMMC 2016-04-06: Agregado para ocultar números de preguntas
		esvTemplateStyles : 6.02000,				//JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6 (@JAPR 2016-03-16: Este día se solicitaron funcionalidades urgentes, así que se decidió mandar este cambio hasta 6.02000)
		esvSimpleChoiceCatOpts : 6.02001,			//JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
		esvMyLocation : 6.02001,					//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
		esvNoFlow : 6.02001,
		esvMChoiceCat: 6.02001,						//JAPR 2016-04-27: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
		esvSChoiceMetro: 6.02001,					//JAPR 2016-05-05: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
		esvQNameCorrection: 6.02002,				//OMMC 2016-05-02: Agregada la corrección de nombres de pregunta con múltiples
		esvUserLanguage: 6.02001,					//GCRUZ 2016-04-29: Agregada configuración para lenguaje de usuario.
		esvPushMsgLog: 6.02001,
		esvCaptureTransfer: 6.02001,				//GCRUZ 2016-04-29: Agregada tiempo y distancia de traslado de captura.
		esvDataSourceFilterFormula: 6.02001,		//GCRUZ 2016-04-29: Agregado campo para fórmula escrita en datasourcefilter
		esvFormsDuration: 6.02001,					//JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
		esvNavigationHistory: 6.02003,				//JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		esvAutoFitFormsImage: 6.02003,				//JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		esvRealPushMsg: 6.02007,					//JAPR 2016-10-14: Modificado el esquema de Push para envíar mensajes al último dispositivo utilizado por el usuario solamente (#L9T2IU)
		esvImageDisplay: 6.02013,					//OMMC 2017-01-11: Agregado el nuevo tipo de pregunta Imagen, es una pregunta foto - readonly con un display especial
		esvEntryDescription: 6.02020				//MAPR 2017-04-11: Nueva funcionalidad para agregar descripción a las capturas, JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		, esvReloadSurveyFromSection: 6.02022
		, esvTableSectDescRow: 6.02028				//JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registor en lugar de la columna correspondiente (#VU53QY)
		, esvUpdateDataWithFiles: 6.02031			//JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
		, esvSketchPlus: 6.02031					//JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		, esvRecalcDep: 6.02035						//JAPR 2019-07-16: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
		, esvQuestionAR: 6.03000					//OMMC 2019-03-25: Pregunta AR #4HNJD9
	};
}