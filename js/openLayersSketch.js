/*
//JAPR 2019-02-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
Contiene los métodos necesarios para generar el Sketch+ utilizando OpenLayers
*/

/* Genera el contenido HTML de la ventana de Sketch+, aunque NO genera el componente porque éste requiere estar visible */
function fullOpenLayersSketch(aQuestionID, aRecordNumber) {
	//Validado que en modo diseño no realice el proceso de Sketch
	if (gbDesignMode) {
		return;
	}
	
	Debugger.register('fullOpenLayersSketch aQuestionID == ' + aQuestionID + ', aRecordNumber == ' + aRecordNumber);
	
	//Se eliminan los elementos cargados anteriormente porque al parecer que empalma con canvas anteriores
	$('[id^="SketchFullScreen_"]').remove();
	var strSketchDialogName = "SketchFullScreen_" + aQuestionID;
	
	//Activa la bandera que indica que entró a pantalla completa fuera de JQM
	fromFullScreen = true;
	
	try {
		var objQuestion = selSurvey.questions[aQuestionID];
		//JAPR 2019-02-26: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Ajustado el tamaño del sketch para que utilice las dimensiones de la imagen en lugar del default
		//En este punto se usarán las dimensiones default porque aquí no se ha cargado aún la imagen, pero mas adelante se van a sobrescribir según sea necesario
		var intCanvasWidth = parseInt(objQuestion.canvasWidth)?parseInt(objQuestion.canvasWidth):giDefaultSketchWidth;
		var intCanvasHeight = parseInt(objQuestion.canvasHeight)?parseInt(objQuestion.canvasHeight):giDefaultSketchHeight;
		
		var strFieldName = objQuestion.getFieldName();
		var strDataRole = " style='width:100%;height:100%;' data-role=\"dialog\" data-close-btn='none'";
		//Botones default del header
		var btnOk = "<a href='javascript:saveSketch("+objQuestion.id+");'"+" data-inline=\"true\" data-mini=\"true\" data-theme=\"c\" id=\"" + strFieldName + "_saveSketch" +  "\" name=\"" + strFieldName + "_saveSketch" + "\" data-role=\"none\"><img src='images/sketchPlus/sketch_save.png'/></a>";
		//JAPREliminar: Ver nota mas abajo donde se definía el div _scroll
		//var btnScroll = "<a href='javascript:switchSketchScroll();'"+" data-inline=\"true\" data-mini=\"true\" data-theme=\"c\" class=\"ui-btn-opts skecthScrollControl\" id=\"" + strFieldName + "_switchSketchScroll" +  "\" name=\"" + strFieldName + "_switchSketchScroll" + "\" data-role=\"none\"><img src='images/sketch.png'/></a>";
		//var btnClear = "<a href='javascript:clearFeatures();'"+" data-inline=\"true\" data-mini=\"true\" data-theme=\"c\" class=\"ui-btn-opts sketchClear\" id=\"" + strFieldName + "_clearSketch" +  "\" name=\"" + strFieldName + "_clear" + "\" data-role=\"none\"><img src='images/sketchPlus/sketch_clear.png'/></a>";
		var btnClose = "<a href='javascript:cancelSketch();'"+" data-inline=\"true\" data-mini=\"true\" data-theme=\"c\" class=\"ui-btn-right\" id=\"" + strFieldName + "_cancelSketch" +  "\" name=\"" + strFieldName + "_cancelSketch" + "\" data-role=\"none\"><img src='images/sketchPlus/sketch_cancel.png'/></a>";
		
		//Cuerpo principal del diálogo
		var strHTML = "<div " + strDataRole + " id=\"" + strSketchDialogName + "\" data-questionid=\"" + aQuestionID + "\" data-recordnumber=\"" + aRecordNumber + "\" data-theme=\"c\">\r\n";
		var strBody = "\
			<div id=\"" + strSketchDialogName + "_header\" data-role=\"header\">"+btnOk /*+ btnScroll*//*+ btnClear*/ + btnClose+"<br><br></div>\r\n\
			<div id=\"" + strSketchDialogName + "_body\" data-role=\"content\" style='width:" + intCanvasWidth + "px; height:" + intCanvasHeight + "px; background-color:magenta;' data-theme=\"c\">\r\n\
				<div id=\""+ strFieldName + "_map\" class=\"map sketchMapCanvas\" style=\"width:" + intCanvasWidth + "px; height:" + intCanvasHeight + "px;\"></div>\r\n\
			</div>\r\n";
			//JAPREliminar: Este div adicional permitía un scroll sobre el área del OpenLayer activable mediante un switch, permitiendo variar entre escribir sobre el OpenLayer o moverlo,
			//pero finalmente la idea fue deshechada para utilizar el Scroll nativo del OpenLayer restringido al área disponible por el dispositivo, dejando el OpenLayer al tamaño de la
			//imagen/foto utilizada o bien al default especificado por el usuario en los casos donde no aplicara imagen
				/*"<div id=\"" + strSketchDialogName + "_scroll\" data-role=\"content\" class=\"skecthScroll\" style='width:" + intCanvasWidth + "px; height:" + intCanvasHeight + "px;' data-theme=\"c\">\r\n\
				</div>\r\n\
			</div>\r\n";*/
		strHTML += strBody + "\
			</div>\r\n";
		
		/*strHTML += strBody + "\
				<div id=\"" + strFieldName + "_dataview\" class=\"sketchPopUpDataViewCont\" style=\"background-color: red;\"></div>\
			</div>\r\n";*/
		
		$(document.body).append(strHTML);
		$('#' + strSketchDialogName).addClass('openLayerSketch');
		
		$.mobile.changePage($('#' + strSketchDialogName), {
			transition : 'pop',
			reverse    : true,
			changeHash : true
		});
		
		//El layer NO se puede generar en este punto porque el div contenedor aún no está visible, y es un requisito que se encuentre visible o de lo contrario el height asignado es de 0px
		//aunque su contenedor tenga un height definido y no genera el layer correctamente
		/*var map = new ol.Map({
			target: strFieldName + '_map',
			layers: [
				new ol.layer.Tile({source: new ol.source.OSM()})
			],
			view: new ol.View({center: ol.proj.fromLonLat([37.41, 8.82]), zoom: 4})
		});*/
	} catch(e) {
		Debugger.message("Error building Sketch page: " + e);
	}
}

/* Genera la imagen del sketch y el componente de dibujo sobre el HTML ya construído de la página donde se usará */
function generateOpenLayerSketch(aQuestionID, aRecordNumber) {
	Debugger.register('generateOpenLayerSketch aQuestionID == ' + aQuestionID + ', aRecordNumber == ' + aRecordNumber);
	
	var strSketchDialogName = "SketchFullScreen_" + aQuestionID;
	var objQuestion = selSurvey.questions[aQuestionID];
	//Validación para no volver a generar el Sketch en caso que ya se hubiera construido su ventana y estuviera entre la transición de una captura de elemento y regresar al área de dibujo
	//(sólo se puede dibujar el Sketch de un único registro a la vez, por lo tanto es válido que no se utilice aRecordNumber en esta validación, ya que si se estuviera realmente llamando
	//a la generación del Sketch de un registro diferente, la pregunta ya traería desactivada la bandera sketchGenerated por haber concluido el Sketch del registro anterior)
	if ( !objQuestion ) {
		return;
	}
	
	//Si ya se había generado la página, solo podría regresar a ella si viene de una sección de captura, por lo tanto resetea la opción marcada al botón de selección de elementos
	//para remover cualqueir interacción de colocación de elementos que hubiera estado en funcionamiento
	if ( objQuestion.sketchGenerated ) {
		if ( objQuestion.sketchMap ) {
			//Se agregó esta invocación porque si se hacía un resize dentro del diálogo de captura de datos, al regresar al mapa este tomaba tamaños equivocados y terminaba viendose
			//solo el área negra de dibujo, así que ya no se podía trabajar
			objQuestion.sketchMap.updateSize()
		}
		if ( objQuestion.sketchModeControl ) {
			$('#SketchModeMove').prop('checked', true);
			$.proxy(objQuestion.sketchModeControl.handleSketchMode, objQuestion.sketchModeControl)();
		}
		return;
	}
	
	if ( !gbDefaultSketchColorsArrayWithStyles ) {
		for (var intColorNum in goDefaultSketchColorsArray) {
			goDefaultSketchColorsArray[intColorNum].style = new ol.style.Style({
				stroke: new ol.style.Stroke({
					width: 2, 
					color: goDefaultSketchColorsArray[intColorNum].colorArray
				})
			});
		}
		
		gbDefaultSketchColorsArrayWithStyles = true;
	}
	
	objQuestion.sketchModeControl = undefined;
	objQuestion.sketchImageObj = undefined;
	objQuestion.sketchImageSrc = undefined;
	objQuestion.sketchMap = undefined;
	objQuestion.sketchDataView = undefined;
	objQuestion.sketchPopUp = undefined;
	objQuestion.sketchLastItemDrawn = undefined;
	objQuestion.sketchSavingCallbackFn = undefined;
	objQuestion.sketchEdited = false;
	//JAPR 2019-04-01: Corregido un bug, no se podía cambiar el elemento seleccionado sin primero utilizar algún otro botón de la toolbar (#7WMZIV)
	objQuestion.sketchItemSelectorShown = false;
	//JAPR
	var objDeferred = $.Deferred(), objImage = undefined, objPromise = objDeferred.promise();
	var strFieldName = objQuestion.getFieldName(), strFileName = '', strImagePath = "";
	
	//Prepara el tamaño default de las imágenes a utilizar en el Sketch, este valor viene de las propiedades configurables imageWidth e imagenHeight, pero se tiene que preparar aquí porque
	//el componente requiere un tamaño fijo o de lo contrario utilizará el tamaño natural de la imagen, lo cual crearía un problema pues estas pueden ser mayores incluso que el propio sketch.
	//Este valor NO puede venir en porcentaje como en otros casos, acepta únicamente pixeles (solo se sobrescribe si la pregunta nace sin él, ya que es un valor que se debe calcular)
	if ( objQuestion.sketchImageWidth === undefined ) {
		var intCanvasWidth = parseInt(objQuestion.imageWidth);
		if ( !intCanvasWidth || intCanvasWidth < 0 ) {
			intCanvasWidth = giDefaultSketchItemWidth;
		}
		objQuestion.sketchImageWidth = intCanvasWidth;
	}
	
	if ( objQuestion.sketchImageHeight === undefined ) {
		var intCanvasHeight = parseInt(objQuestion.imageHeight);
		if ( !intCanvasHeight || intCanvasHeight < 0) {
			intCanvasHeight = giDefaultSketchItemHeight;
		}
		objQuestion.sketchImageHeight = intCanvasHeight;
	}
	
	//Obtiene la imagen default a utilizar 
	//QuestionAnswerCls.storedImage es la imagen grabada de una captura ya sincronizada al server, usada exclusivamente para efectos de edición (vía browser) de una captura, ya que mientras
	//	no se sincronice al server deberá utilizar las propiedades descritas a continuación para la imagen default si es que hay alguna
	//QuestionCls.imageSketch es la imagen default asignada al Sketch configurada como una imagen real directamente en la pregunta
	//QuestionAnswerCls.canvasPhoto es la imagen generada del Canvas que contiene la posible imagen default + todas las modificaciones realizadas por el usuario
	//QuestionAnswerCls.photo es la foto tomada con la cámara que será utilizada como si se tratara del imageSketch (sólo se puede tener o una imagen default o una foto, no ambas, pero si ninguna)
	//QuestionCls.defaultValue es teóricamente un valor default que se puede configurar como imagen deault (imageSketch) utilizando variables o cualquier expresión que replaceVars pueda resolver
	//En prioridad para la imagen default se tomará:
	//0- storedImage
	//1- imageSketch
	//2- photo
	//3- defaultValue
	//NO se tomará canvasPhoto nunca, ya que esa imagen es solo para almacenar los trazos como imagen, lo que se hace para generar el Sketch es poner la imagen default y trazar de nuevo
	//cualquier punto o línea que el usuario dibujó sobre dicha imagen
	if ( objQuestion.answer.storedPhoto || objQuestion.imageSketch ) {
		//En este caso se trata de una edición, únicamente debería ser posible vía captura de browser invocada desde el Administrador cuando se pide ver las respuestas de una captura ya grabada
		if ( objQuestion.answer.storedPhoto ) {
			//En este caso se trata de una edición, únicamente debería ser posible vía captura de browser invocada desde el Administrador cuando se pide ver las respuestas de una captura ya grabada
			strImagePath = objQuestion.storedPhoto;
		}
		else if ( objQuestion.imageSketch ) {
			//En este caso utilizará la imagen default configurada a nivel de la pregunta (toda captura nueva pasaría por este caso)
			strImagePath = objQuestion.imageSketch;
		}
		
		//Limpiamos la ruta de la imagen 
		var tmp = ("'"+strImagePath+"'").match(/(img|src)\=(\"|\')[^\"\'\>]+/g);
		//Agregamos la ultima comilla ya que la quita el match
		tmp = tmp + '"';
		
		var tmp2 = tmp.replace("src=","");
		//Quitamos las comillas
		strFileName = tmp2.replace(new RegExp(RegExp.quote('"'), "gi"), '');
		
		//Verificar que tenga algo de foto (ruta)
		if (strFileName) {
			//Generamos un objeto imagen, le asignamos a su propiedad src la ruta de la imagen y se tiene que esperar a que termine de cargar la imagen asignada
			objImage = new Image();
			objImage.onload = function() {
				objDeferred.resolve({image: objImage});
			}
			objImage.onerror = function() {
				objDeferred.reject({errMsg: "Error loading the image"});
			}
			
			objImage.src = replaceVars(strFileName);
		}
	}
	else if ( objQuestion.answer.photo ) {
		//En este caso utilizará la foto tomada directamente con la cámara en esta respuesta (NO debería ser posible tener imageSketch simultáneamente en este caso)
		strFileName = objQuestion.answer.photo;
		if (objSettings && objSettings.webMode) {
			objImage = new Image();
			objImage.onload = function() {
				objDeferred.resolve({image: objImage});
			}
			objImage.onerror = function() {
				objDeferred.reject({errMsg: "Error loading the image"});
			}
			
			var strRoot = "http://" + objSettings.esurveyFolder;
			if (objSettings.protocol) {
				strRoot = strRoot.replace('http:', objSettings.protocol);
			}
			
			//JAPR 2016-03-31: Corregido un bug en Web, si la imagen cambia, se estaba tomando de cache ya que conserva el mismo nombre, así que se agrega algo para que no use cache
			//objImage.src = strRoot + strFileName + ((objSettings.webMode)?"?dte="+((CurrentDate() + CurrentTime()).replace(/-/gi, '').replace(/:/gi, '')):"");
			objImage.src = strRoot + strFileName;
		}
		else {
			//JAPR 2019-02-25: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			//Corregido un bug, en móvil no estaba cargando la foto tomada porque no se creaba el objeto image sino hasta después de haber abierto el archivo
			objImage = new Image();
			ESFile.readFile(strFileName, function(evt) {
				objImage.onload = function() {
					objDeferred.resolve({image: objImage});
				}
				objImage.onerror = function() {
					objDeferred.reject({errMsg: "Error loading the image"});
				}
				
				//Validación por si el archivo de foto se grabó con la comilla que separa los datos del prefijo usado para rendering de la imagen
				var dataIm = evt.target.result;
				if ( dataIm.substring(0,1) == ',' ) {
					objImage.src = 'data:image/jpeg;base64' + dataIm;
				}
				else {
					objImage.src = 'data:image/jpeg;base64,' + dataIm;
				}
			}, function() {
				//Error al leer el archivo, no se debería continuar con la generación del Sketch
				objDeferred.reject({errMsg: "Error loading the image"});
			});
		}
	}
	else {
		//En este caso intentará resolver el defaultValue a una ruta válida de imagen (dependiendo de las respuestas de la captura, pudiera o no haber imagen default)
		var blnChangeRoot = (objSettings && objSettings.webMode);
		var objSectionEvalRecord = undefined;
		var blnEvaluateDefaultValue = false;
		var strFileName = Trim(replaceVars(objQuestion.defaultValue, undefined, undefined, undefined, undefined, objSectionEvalRecord, blnEvaluateDefaultValue));
		if (strFileName) {
			strDefaultImageFileName = strFileName;
			//Si el valor default devuelto (que tentativamente es una ruta de imagen) inicia con http, se asume una ruta absoluta por lo
			//que se configura la variable para no forzar a utilizar el Web Service de eForms tal como lo harían las fotos normales, de
			//lo contrario al asumir una ruta relativa si se tendrá que usar
			if (strFileName.toLowerCase().indexOf('http') == 0) {
				blnChangeRoot = false;
			}
			
			//JAPR 2014-12-17: En modo Web el canvas genera un error del tipo
			//Uncaught SecurityError: Failed to execute 'toDataURL' on 'HTMLCanvasElement': Tainted canvases may not be exported.
			//cuando se utiliza una URL absoluta como imagen del canvas, por tanto no se podrán ajustar las URLs para agregar el protocolo, y
			//de hecho si la URL venía de eBavel y por ende contenía ya el protocolo http, no se podrá usar en absoluto (por el momento no hay
			//manera de evitar eso, ya que se tendría que abrir la imagen y obtener su B64 para usarlo en lugar de la URL). Se agregó el false
			//para dejar las rutas como relativas forzando a entrar al else en lo que se resuelve correctamente
			//JAPR 2019-04-15: Ajustes de funcionalidad para Sketch+ (#ZDK5CO)
			//Para la pregunta Sketch+ no se utiliza un canvas directamente, por lo tanto se pueden utilizar imágenes con rutas absolutas si es necesario
			if (blnChangeRoot) {
		    	//JAPR 2012-12-13: Faltaba agregar el path absoluto a la imagen, así que realmente la buscaba en el root de entrada y no
		    	//necesariamente al del servicio donde finalmente la graba
				var strRoot = "http://" + objSettings.esurveyFolder;
				if (objSettings.protocol) {
					strRoot = strRoot.replace('http:', objSettings.protocol);
				}
			}
			else {
				//En este caso el propio valor de imagen ya trae una ruta absoluta
				var strRoot = '';
				//JAPR 2014-12-17: En modo Web el canvas genera un error del tipo
				//Uncaught SecurityError: Failed to execute 'toDataURL' on 'HTMLCanvasElement': Tainted canvases may not be exported.
				//cuando se utiliza una URL absoluta como imagen del canvas, por tanto no se podrán ajustar las URLs para agregar el protocolo, y
				//de hecho si la URL venía de eBavel y por ende contenía ya el protocolo http, no se podrá usar en absoluto (por el momento no hay
				//manera de evitar eso, ya que se tendría que abrir la imagen y obtener su B64 para usarlo en lugar de la URL)
				/*if (objSettings && objSettings.webMode && strFileName.toLowerCase().indexOf('http') == 0) {
					//Si la ruta es absoluta, por ahora no se podrá utilizar esta imagen en modo Web
					strDefaultImageFileName = '';
				}*/
			}
			
			//Genera la imagen y asigna los tamaños, ya que con la imagen mas adelante se configurará un onload y con los tamaños se preparará
			if (strDefaultImageFileName) {
				objImage = new Image();
				objImage.onload = function() {
					objDeferred.resolve({image: objImage});
				}
				objImage.onerror = function() {
					objDeferred.reject({errMsg: "Error loading the image"});
				}
				
				objImage.src = strRoot + strDefaultImageFileName;
			}
		}
	}
	
	//Si no se inició la carga de ninguna imagen, entonces genera la promesa resuelta para que pueda continuar con la carga directamente
	if ( !objImage ) {
		objDeferred.resolve({});
	}
	
	//El resultado final en cualquiera de los casos mencionados arriba, sería que se está cargando una imagen configurada (hay que esperar a que termine de cargarla antes de continuar) o
	//bien que no existe imagen que cargar
	objPromise.done(function(r) {
		try {
			var objSketchImage = r.image;
			
			//*******************************************************************************************************************
			//Preparación de los elementos que se usarán para generar el OpenLayer de Sketch
			if ( objSketchImage ) {
				//JAPR 2019-02-26: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				//Ajustado el tamaño del sketch para que utilice las dimensiones de la imagen en lugar del default. En este punto utiliza las dimensiones propias de la imagen, por lo que
				//tiene que cambiar las dimensiones del elemento en el DOM también (ya se encuentra generado para este momento)
				//var intCanvasWidth = (parseInt(objQuestion.canvasWidth))?parseInt(objQuestion.canvasWidth):objSketchImage.width;
				//var intCanvasHeight = (parseInt(objQuestion.canvasHeight))?parseInt(objQuestion.canvasHeight):objSketchImage.height;
				var intCanvasWidth = objSketchImage.width;
				var intCanvasHeight = objSketchImage.height;
				
				$("#" + strSketchDialogName + "_body").width(intCanvasWidth);
				$("#" + strFieldName + "_map").width(intCanvasWidth);
				$("#" + strSketchDialogName + "_body").height(intCanvasHeight);
				$("#" + strFieldName + "_map").height(intCanvasHeight);
				//JAPR
			}
			else {
				var intCanvasWidth = (parseInt(objQuestion.canvasWidth))?parseInt(objQuestion.canvasWidth):giDefaultSketchWidth;
				var intCanvasHeight = (parseInt(objQuestion.canvasHeight))?parseInt(objQuestion.canvasHeight):giDefaultSketchHeight;
			}
			objQuestion.sketchExtent = undefined;
			var objSketchExtent = objQuestion.sketchExtent;
			if ( !objSketchExtent ) {
				objSketchExtent = objQuestion.sketchExtent = [0, 0, intCanvasWidth, intCanvasHeight];
			}
			//El source tiene que ser eliminado ya que si se conserva, los nuevos Sketchs que se empiezan a pintar tendrán los trazos previamente pintados, en este caso si la pregunta
			//aún lo conserva es porque está cambiando de modo a la captura de registro y de regreso probablemente, pero quien invoca al inicio del pintado del Sketch por primera vez
			//debería encargarse de limpiarlo
			objQuestion.sketchSource = undefined;
			var objSketchSource = objQuestion.sketchSource;
			if ( !objSketchSource ) {
				//En caso que la pregunta ya hubiera tenido algún trazo capturado en su respuesta, lo restaura utilizando GeoJSON
				var objVectorOptions = undefined;
				if ( objQuestion.answer.answer ) {
					//objVectorOptions = $.extend({}, {features: objApp.sketchGeoJSON.readFeatures(objQuestion.answer.answer)});
					objSketchSource = objQuestion.sketchSource = new ol.source.Vector({
						features: objApp.sketchGeoJSON.readFeatures(objQuestion.answer.answer)
					});
				}
				else {
					objSketchSource = objQuestion.sketchSource = new ol.source.Vector();
				}
			}
			
			//Por default el color inicial es el primero de la lista
			var intSketchSelectedColor = objQuestion.sketchSelectedColor = 0;
			//El array de colores default goDefaultSketchColorsArray es global a todas las preguntas
			
			//Por default no existen elementos a menos que la pregunta contenga alguno configurado, si no hay elementos, no se debe mostrar el botón para manipularlos en absoluto
			var intSketchSelectedItem = objQuestion.sketchSelectedItem = -1;
			//Obtiene los tipos de elementos a seleccionar de la configuración de la pregunta
			var objResultOptions = objQuestion.getLocalOptions(aRecordNumber, true);
			var objLocalOptions = objQuestion.options;
			var objLocalOptionsOrder = objQuestion.optionsOrder;
			if (objResultOptions) {
				if (objResultOptions.options) {
					objLocalOptions = objResultOptions.options;
				}
				if (objResultOptions.optionsOrder) {
					objLocalOptionsOrder = objResultOptions.optionsOrder;
				}
			}
			
			//El array de elementos a seleccionar es propio de cada pregunta, originalmente pensado para opciones fijas, por lo que una vez generado ya no era necesario recalcularlo, pero
			//podría evolucionar a basarse en un catálogo e incluso filtros dinámicos, en cuyo caso se tendría que reconstruir cada vez que se inicia una captura de sketch
			var objSketchItemsArray = objQuestion.sketchItemsArray;
			if ( !objSketchItemsArray ) {
				objSketchItemsArray = objQuestion.sketchItemsArray = new Array();
				for (var intOptionNum in objLocalOptionsOrder) {
					var objOption = objLocalOptions[objLocalOptionsOrder[intOptionNum]];
					if (objOption.displayImage && objOption.displayImage.indexOf("{@IMG") != -1){
						objOption.displayImage = replaceVars(objOption.displayImage, undefined, true);
					}
					
					objSketchItemsArray.push({
						imageSrc: GetImageSrcFromImageTag(objOption.displayImage), 
						style: new ol.style.Style({
							image: new ol.style.Icon({
								anchor: [.5, .5],	/* Ajustado para la esquina superior izquierda de la imagen ([1, 1] sería la esquina opuesta) */
								src: GetImageSrcFromImageTag(objOption.displayImage)
							})
						})
					});
				}
			}
			
			if ( objSketchItemsArray.length ) {
				intSketchSelectedItem = objQuestion.sketchSelectedItem = 0;
			}
			
			//Asigna los estilos iniciales y de selección de los vectores trazados
			var objSketchStyles = objQuestion.sketchStyles;
			if ( !objSketchStyles ) {
				objSketchStyles = objQuestion.sketchStyles = {
					'LineString': new ol.style.Style({
						stroke: new ol.style.Stroke({
							width: 2, 
							color: goDefaultSketchColorsArray[0].colorArray
						})
					}),
					'Icon': new ol.style.Style({
						image: new ol.style.Icon({
							anchor: [.5, .5],
							src: ((intSketchSelectedItem != -1 && objSketchItemsArray[intSketchSelectedItem])?objSketchItemsArray[intSketchSelectedItem].imageSrc:'images/icon.png')
						})
					})
				}
			}
			//JAPR 2019-02-25: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			else {
				//Vuelve a seleccionar el color default correspondiente a intSketchSelectedColor
				var objColor = goDefaultSketchColorsArray[intSketchSelectedColor];
				if ( objColor && objSketchStyles['LineString'] ) {
					objSketchStyles['LineString'].stroke_.color_ = objColor.colorArray;
				}
			}
			//JAPR
			
			var objSketchSelectedStyles = objQuestion.sketchSelectedStyles;
			if ( !objSketchSelectedStyles ) {
				objSketchSelectedStyles = objQuestion.sketchSelectedStyles = {
					'LineString': new ol.style.Style({
						stroke: new ol.style.Stroke({
							width: 4, 
							color: [237, 212, 0, 1]
						})
					}),
					'Icon': new ol.style.Style({
						image: new ol.style.Icon({
							color: [237, 212, 0, 1],
							anchor: [.5, .5],
							src: 'images/icon.png'
						})
					})
				}
			}
			objQuestion.sketchImgExtent = undefined;
			objQuestion.sketchProjection = undefined;
			objQuestion.sketchSelectedInteraction = undefined;
			objQuestion.sketchEditInteraction = undefined;
			
			//Modifica el estilo del elemento seleccionado para hacerlo mas grueso y de un color distintivo
			var objSketchSelectInt = objQuestion.sketchSelectedInteraction;
			if ( !objSketchSelectInt ) {
				var objSketchSelectInt = objQuestion.sketchSelectedInteraction = new ol.interaction.Select({
					style: function(feature, resolution) {
						var strGeometryName = feature.getGeometryName();
						if ( strGeometryName == 'geometry' ) {
							strGeometryName = feature.getGeometry().getType();
						}
						
						if ( strGeometryName == 'Point' ) {
							strGeometryName = 'Icon';
						}
						
						if ( strGeometryName == 'LineString' ) {
							return objSketchSelectedStyles[strGeometryName];
						}
						else {
							//Obtiene el negativo de la imagen usada para mostrarlo como seleccionado, almacenándolo para reutilizarlo en ocasiones posteriores
							//No funcionó obtener la imagen del feature, se obtendrá de la colección de elementos usando el índice del elemento seleccionado en este feature
							//var image = feature.get('style').getImage().getImage();
							var intItemNum = feature.get('itemNum'), strSelectedImageSrc = '';
							if ( intItemNum >= 0 && objSketchItemsArray[intItemNum] ) {
								var objItem = objSketchItemsArray[intItemNum];
								strSelectedImageSrc = objItem.imageSrc;
								if ( !objQuestion.sketchSelectedStyles[strSelectedImageSrc] ) {
									var image = objItem.style.getImage().getImage();
									var canvas = document.createElement('canvas');
									var context = canvas.getContext('2d');
									canvas.width = image.width;
									canvas.height = image.height;
									context.drawImage(image, 0, 0, image.width, image.height);
									var imageData = context.getImageData(0, 0, canvas.width, canvas.height);
									var data = imageData.data;
									for (var i = 0, ii = data.length; i < ii; i = i + (i % 4 == 2 ? 2 : 1)) {
										data[i] = 255 - data[i];
									}
									
									context.putImageData(imageData, 0, 0);
									objQuestion.sketchSelectedStyles[strSelectedImageSrc] = new ol.style.Style({
										image: new ol.style.Icon({
											anchor: [.5, .5],
											crossOrigin: 'anonymous',
											src: undefined,
											img: canvas,
											imgSize: canvas ? [canvas.width, canvas.height] : undefined
										})
									})
								}
							}
							else {
								return objSketchSelectedStyles[strGeometryName];
							}
							
							if ( strSelectedImageSrc ) {
								return objQuestion.sketchSelectedStyles[strSelectedImageSrc];
							}
							else {
								return objSketchSelectedStyles[strGeometryName];
							}
						}
					},
					/* Agregado el filtro para evitar que el polígono que pinta el área de dibujo default pueda ser seleccionado y cambiado de posición */
					filter: function(feature, layer) {
						var strGeometryName = feature.getGeometryName();
						if ( strGeometryName == 'geometry' ) {
							strGeometryName = feature.getGeometry().getType();
						}
						
						//El único polígono que se puede dibujar es el área default de fondo, así que al detectarlo cancela la selección para evitar que se pueda mover
						if ( strGeometryName == 'Polygon' ) {
							return false;
						}
						
						//Cualquier otro objeto se podrá seleccionar sin problema
						return true;
					}
				});
			}
			
			//Edita el elemento seleccionado con doble click, el cual debe ser un elemento colocado, no un trazo realizado
			var objSketchEditInt = objQuestion.sketchEditInteraction;
			if ( !objSketchEditInt ) {
				var objSketchEditInt = objQuestion.sketchEditInteraction = new ol.interaction.Select({
					multi: false,
					condition: ol.events.condition.doubleClick
					//No requiere un style, porque automáticamente al ejecutase va a removerse de la colección de objetos seleccionados y regresará al estilo normal
				});
			}
			
			/* Invoca a la edición de los datos del elemento seleccionado con doble click, removiéndolo de la colección de seleccionados de esta interacción para que no pierda el estilo
			de despliegue correcto (como abrirá una ventana de datos, realmente no se habría visto el cambio de estilo, pero habría que vaciar la colección al final para evitar que quede
			permanentemente modificado de cualquier manera) */
			objSketchEditInt.on('select', function(evt){
				//Agrega el elemento seleccionado para que pueda ser procesado por la ventana de captura, solo que lo marca como de edición para evitar que sea eliminado si se cancela
				//la captura de los datos
				if ( !evt.selected.length ) {
					return;
				}
				
				objQuestion.sketchLastItemDrawn = evt.selected[0];
				objQuestion.sketchLastItemDrawn.set('edited', 1);
				
				//Limpia el elemento seleccionado para evitar que su estilo se vea incorrecto, ya que no se configuró un estilo particular para estos selementos debido a que no habrá
				//realmente interacción con ellos en pantalla al editar
				objQuestion.sketchEditInteraction.getFeatures().clear();
				
				//Invoca a la edición de los datos capturados para este elemento
				openEditSketchRecordScreen();
			});
			
			//Código no utilizado, si se lanzaba pero solo permitía seleccionar nuevos elementos, no controlaba cuando perdían la selección, además no solucionaba el problema que el color
			//se quedaba fijo al de selección sin restaurar el color original de cada elemento dibujado
			/*objSketchSelectInt.on('select', function(evt){
				var selected = evt.selected;
				var deselected = evt.deselected;
				
				if (selected.length) {
					selected.forEach(function(feature){
						console.info(feature);
						//feature.setStyle(objSketchSelectedStyles[feature.getGeometryName()]);
						feature.setStyle(function(feature) {
							return objSketchSelectedStyles[feature.getGeometryName()];
						});
					});
				} else {
					deselected.forEach(function(feature){
						console.info(feature);
						feature.setStyle(null);
					});
				}
			});*/
			
			var objSketchTranslateInt = new ol.interaction.Translate({
				features: objSketchSelectInt.getFeatures()
			});
			
			//JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			/* Evento lanzado cuando termina de realizarse una operación de Drag&Drop  sobre los elementos seleccionados, en este caso servirá para actualizar la posición X, Y del registro
			correspondiente */
			objSketchTranslateInt.on('translateend', function(evt) {
				var arrFeatures = evt.features.getArray();
				var objQuestion = selSurvey.currentCapture.question;
				if ( !objQuestion || !objQuestion.optionsOrder ) {
					return;
				}
				
				//Recorre todos los trazos seleccionados que acaban de cambiar de posición
				for (var intIdx = 0, intLength = arrFeatures.length; intIdx < intLength; intIdx++) {
					var objFeature = arrFeatures[intIdx], intItemNum = objFeature.get('itemNum'), intRecordNum = objFeature.get('recordNum');
					if ( intItemNum !== undefined && intRecordNum !== undefined ) {
						//Se trata de un elemento que tiene asociado un registro, por lo tanto puede actualizar los valores de las preguntas en cada registro identificado como que ha cambiado
						var arrPosition = objFeature.getGeometry().getCoordinates();
						if ( !arrPosition || !$.isArray(arrPosition) || arrPosition.length < 2 ) {
							continue;
						}
						
						//Identifica el tipo de objeto y por ende la sección que se debe actualizar
						if ( !objQuestion.optionsOrder[intItemNum] ) {
							continue;
						}
						
						var objOption = objQuestion.options[objQuestion.optionsOrder[intItemNum]];
						if ( !objOption || !objOption.nextSection ) {
							continue;
						}
						
						var objSection = selSurvey.sections[objOption.nextSection];
						if ( !objSection ) {
							continue;
						}
						
						if ( objSection.xPosQuestionID ) {
							var objDataQuestion = objSection.questions[objSection.xPosQuestionID];
							if ( objDataQuestion && objDataQuestion.answers[intRecordNum] ) {
								objDataQuestion.answers[intRecordNum].setValue(arrPosition[0]);
								objQuestion.sketchEdited = true;
							}
						}
						
						if ( objSection.yPosQuestionID ) {
							var objDataQuestion = objSection.questions[objSection.yPosQuestionID];
							var intCanvasHeight = objQuestion.sketchExtent[3];
							if ( objDataQuestion && objDataQuestion.answers[intRecordNum] && intCanvasHeight ) {
								objDataQuestion.answers[intRecordNum].setValue(intCanvasHeight - arrPosition[1]);
								objQuestion.sketchEdited = true;
							}
						}
					}
				}
			});
			//JAPR
			
			var arrLayers = [];
			//Primero debe agregar un Layer con un polígono que forme un recuadro del área donde se puede dibujar, independientemente de si existe o no una foto/imagen, esta área delimita
			//todo lo que es posible dibujar y estará al inicio para que no cubra la imagen default ni los trazos realizados (es importante colocar Layers en orden de menor a mayor importancia,
			//en el sentido que entre mas abajo esté en la colección, mas arriba aparecerá de los Layers previos)
			arrLayers.push(new ol.layer.Vector({
					source: new ol.source.Vector({
						features: [
							new ol.Feature({
								geometry: new ol.geom.Polygon([
									[
										[0, objSketchExtent[1]], 
										[0, objSketchExtent[3]], 
										[objSketchExtent[2], objSketchExtent[3]], 
										[objSketchExtent[2], objSketchExtent[1]] 
									]
								])
							})
						]
					}),
					style: new ol.style.Style({
						stroke: new ol.style.Stroke({
							color: 'white',
							width: 0
						}),
						fill: new ol.style.Fill({color: goDefaultSketchColorsArray[2].colorArray})	//Color blanco para el fondo de dibujo de Sketch
					})
				})
			);
			
			//Si hay una imagen default, la agrega como un layer para que sea desplegada por el sketch como fondo sobre el que se dibujará
			if ( objSketchImage ) {
				var objSketchImgExtent = objQuestion.sketchImgExtent;
				if ( !objSketchImgExtent ) {
					objSketchImgExtent = objQuestion.sketchImgExtent = [0, 0, objSketchImage.width, objSketchImage.height];
				}
				var objSketchProjection = objQuestion.sketchProjection;
				if ( !objSketchProjection ) {
					objSketchProjection = objQuestion.sketchProjection = new ol.proj.Projection({
						code: 'local_image',
						units: 'pixels',
						extent: objSketchImgExtent
					});
				}
				
				objQuestion.sketchImageObj = objSketchImage;
				arrLayers.push(new ol.layer.Image({
						source: new ol.source.ImageStatic({
							imageLoadFunction: $.proxy(objQuestion.setSketchImageLayerSrc, objQuestion),
							projection: objSketchProjection,
							imageExtent: objSketchImgExtent
						})
					})
				);
			}
			
			//Agrega el Layer del área de dibujo y colocación de elementos
			arrLayers.push(new ol.layer.Vector({
					source: objSketchSource,
					style: function(feature) {
						var strGeometryName = feature.getGeometryName();
						if ( strGeometryName == 'geometry' ) {
							strGeometryName = feature.getGeometry().getType();
						}
						
						if ( strGeometryName == 'Point' ) {
							strGeometryName = 'Icon';
						}
						
						if ( strGeometryName != 'Icon' && strGeometryName != 'LineString' ) {
							return;
						}
						
						//Ajusta el color según el seleccionado para este Feature, en caso de tratarse de un trazo, el color se basará en el índice asignado al Feature como propiedad al
						//momento que inició su colocación en el Sketch, así permitirá recordar el color utilizado y obtenerlo del array de colores (esto eventualmente permitirá tener
						//incluso múltiples paletas de colores)
						var intColorNum = feature.get('colorNum'), intItemNum = feature.get('itemNum');
						if ( strGeometryName == 'Icon' ) {
							if ( intItemNum >= 0 && objSketchItemsArray[intItemNum] ) {
								return objSketchItemsArray[intItemNum].style;
							}
							else {
								return objSketchStyles[feature.getGeometryName()];
							}
						}
						else {
							return goDefaultSketchColorsArray[intColorNum].style;
						}
						
						//Ajusta el color según el seleccionado por el tipo de figura dibujada, está limitado a que todos los trazos son siempre del mismo color (geometryname == 'LineString')
						//return objSketchStyles[feature.getGeometryName()];
					},
					extent: objSketchExtent
				})
			);
			
			//*******************************************************************************************************************
			//Preparación de los botones para el Sketch
			//Restaura el nivel del zoom al default
			var RestoreZoomControl = (function (Control) {
				function RestoreZoomControl(opt_options) {
					var options = opt_options || {};
					var button = $('<button id="' + strFieldName + '_restoreZoom" class="sketchButton restoreZoomBtn")"></button>');
					//var button = $('<button id="' + strFieldName + '_restoreZoom" class="sketchButton restoreZoomBtn" style="background-image:url(\'images/sketch/sketch_restore_zoom.png\')"></button>');
					
					var element = $('<div class="sketch-restore-zoom ol-unselectable ol-control"></div>');
					element[0].appendChild(button[0]);
					
					Control.call(this, {
						element: element[0],
						target: options.target
					});
					
					button[0].addEventListener('click', this.handleRestoreZoom.bind(this), false);
				}
				
				if ( Control ) RestoreZoomControl.__proto__ = Control;
				RestoreZoomControl.prototype = Object.create( Control && Control.prototype );
				RestoreZoomControl.prototype.constructor = RestoreZoomControl;
				
				//En lugar de cambiar el nivel de zoom, se modifica la vista para que ocupe toda el área definida sin constrainResolution para no utilizar zoom decimal con aproximación
				//y que se vea tal como al inicio cuando se generó el mapa
				RestoreZoomControl.prototype.handleRestoreZoom = function handleRestoreZoom () {
					hideColorSelector();
					hideItemSelector();
					restoreSketchZoom();
				};
				
				return RestoreZoomControl;
			}(ol.control.Control));
			
			//Coloca los botones para definir los trazos, ya sea mediante colores o mediante figuras
			var SketchModeControl = (function (Control) {
				function SketchModeControl(opt_options) {
					//Asigna una referencia del componente directamente en la pregunta para poderlo utilizar fuera del contexto del control
					objQuestion.sketchModeControl = this;
					var options = opt_options || {};
					
					var form = document.createElement('form');
					
					var moveDiv = document.createElement('div');
					moveDiv.className = 'sketch-mode-move ol-unselectable ol-control';
					
					var moveRadio = document.createElement('input');
					moveRadio.type = 'radio';
					moveRadio.name = 'SketchModeRadio';
					moveRadio.id = 'SketchModeMove';
					moveRadio.checked = true;
					
					var moveLabel = document.createElement('label');
					moveLabel.setAttribute('for', 'SketchModeMove');
					moveLabel.className = 'fas';
					moveLabel.innerHTML = "\uf245";
					
					moveDiv.appendChild(moveRadio);
					moveDiv.appendChild(moveLabel);
					
					form.appendChild(moveDiv);
					
					var drawDiv = document.createElement('div');
					drawDiv.className = 'sketch-mode-draw ol-unselectable ol-control';
					
					var drawRadio = document.createElement('input');
					drawRadio.type = 'radio';
					drawRadio.name = 'SketchModeRadio';
					drawRadio.id = 'SketchModeDraw';
					drawRadio.checked = false;
					
					var drawLabel = document.createElement('label');
					drawLabel.setAttribute('for', 'SketchModeDraw');
					drawLabel.className = 'fas';
					drawLabel.innerHTML = "\uf303";
					//drawLabel.style.color = '#' + goDefaultSketchColorsArray[0].color;
					
					drawDiv.appendChild(drawRadio);
					drawDiv.appendChild(drawLabel);
					
					form.appendChild(drawDiv);
					
					var iconDiv = document.createElement('div');
					iconDiv.className = 'sketch-mode-icon ol-unselectable ol-control';
					//En caso de no haber elementos configurados, no mostrará el botón para seleccionar elemento
					if ( intSketchSelectedItem == -1 ) {
						iconDiv.className += ' no-options';
					}
					
					var iconRadio = document.createElement('input');
					iconRadio.type = 'radio';
					iconRadio.name = 'SketchModeRadio';
					iconRadio.id = 'SketchModeIcon';
					iconRadio.checked = false;
					
					var iconLabel = document.createElement('label');
					iconLabel.setAttribute('for', 'SketchModeIcon');
					iconLabel.className = 'fas';
					iconLabel.innerHTML = "\uf1b2";
					
					iconDiv.appendChild(iconRadio);
					iconDiv.appendChild(iconLabel);
					
					form.appendChild(iconDiv);
					
					var deleteDiv = document.createElement('div');
					deleteDiv.className = 'sketch-mode-delete ol-unselectable ol-control';
					
					var deleteButton = document.createElement('button');
					deleteButton.setAttribute('type', 'button');
					deleteButton.className = 'fas';
					deleteButton.innerHTML = "\uf2ed";
					
					deleteDiv.appendChild(deleteButton);
					
					form.appendChild(deleteDiv);
					
					//Agrega los objetos creados al componente Control definido para el OpenLayer, de tal manera que se puedan manipular directamente durante los eventos
					this.moveRadio = moveRadio;
					this.drawRadio = drawRadio;
					this.iconRadio = iconRadio;
					this.deleteButton = deleteButton;
					
					this.drawInteraction = null;
					this.iconInteraction = null;
					
					Control.call(this, {
						element: form,
						target: options.target
					});
					
					//Agrega los eventos para los botones creados
					moveRadio.addEventListener('change', this.handleSketchMode.bind(this), false);
					drawRadio.addEventListener('change', this.handleSketchMode.bind(this), false);
					iconRadio.addEventListener('change', this.handleSketchMode.bind(this), false);
					drawLabel.addEventListener('click', this.showColorSelection.bind(this), false);
					iconRadio.addEventListener('click', this.showItemSelection.bind(this), false);
					deleteButton.addEventListener('click', this.deleteSelection.bind(this), false);
					
					//Agrega la botonera de colores
					var formColors = document.createElement('div');
					formColors.id = "formColorsDiv";
					formColors.className = 'tagHidden';
					var blnFirst = true;
					//Agrega los objetos creados al componente Control definido para el OpenLayer, de tal manera que se puedan manipular directamente durante los eventos
					this.formColors = formColors;
					form.appendChild(formColors);
					this.colorsArray = [];
					for (var intColorNum in goDefaultSketchColorsArray) {
						var objColor = goDefaultSketchColorsArray[intColorNum];
						var colorDiv = document.createElement('div');
						colorDiv.style.top = (intColorNum * ((objSettings.webMode && !objSettings.webMobileMode)?30:41) + 8) + "px";
						colorDiv.className = 'sketch-color-selector ol-unselectable ol-control';
						
						var colorRadio = document.createElement('input');
						colorRadio.type = 'radio';
						colorRadio.name = 'SketchColorRadio';
						colorRadio.id = 'SketchColorNum' + intColorNum;
						//JAPR 2019-04-01: Corregido un bug, estaba dejando siempre el último radio como seleccionado así que durante la edición no era posible seleccionar el último color
						//aunque no fuera el previamente seleccionado (#7WMZIV)
						colorRadio.checked = (intSketchSelectedColor == intColorNum);
						
						var colorLabel = document.createElement('label');
						colorLabel.setAttribute('for', 'SketchColorNum' + intColorNum);
						colorLabel.className = '';
						colorLabel.innerHTML = "";
						colorLabel.style.backgroundColor = "#" + objColor.color;
						
						colorDiv.appendChild(colorRadio);
						colorDiv.appendChild(colorLabel);
						
						formColors.appendChild(colorDiv);
						
						//Agrega los objetos creados al componente Control definido para el OpenLayer, de tal manera que se puedan manipular directamente durante los eventos
						this.colorsArray.push(colorRadio);
						
						colorRadio.addEventListener('change', this.handleSketchColor.bind(this), false);
					}
					
					//Agrega el botón para permitir seleccionar elementos a colocar
					var formElements = document.createElement('div');
					formElements.id = "formElementsDiv";
					formElements.className = 'tagHidden';
					var blnFirst = true;
					//Agrega los objetos creados al componente Control definido para el OpenLayer, de tal manera que se puedan manipular directamente durante los eventos
					this.formElements = formElements;
					
					var itemSelectionDiv = document.createElement('div');
					itemSelectionDiv.className = 'sketch-mode-icon-select ol-unselectable ol-control';
					
					var itemSelectButton = document.createElement('button');
					itemSelectButton.setAttribute('type', 'button');
					itemSelectButton.className = 'fas';
					itemSelectButton.innerHTML = "\uf1b2";
					
					itemSelectionDiv.appendChild(itemSelectButton);
					formElements.appendChild(itemSelectionDiv);
					
					this.itemSelectButton = itemSelectButton;
					
					form.appendChild(formElements);
					
					itemSelectButton.addEventListener('click', this.openItemSelector.bind(this), false);
				}
				
				if ( Control ) 
					SketchModeControl.__proto__ = Control;
				SketchModeControl.prototype = Object.create( Control && Control.prototype );
				SketchModeControl.prototype.constructor = SketchModeControl;
				
				/* Eventos para seleccionar, pintar o colocar un nuevo elemento en el Sketch, según cual de los botones se encuentra presionado */
				SketchModeControl.prototype.handleSketchMode = function handleSketchMode() {
					//Primero oculta los diferentes selectores
					hideColorSelector();
					hideColorPanel();
					hideItemSelector();
					hideItemPanel();
					
					if (this.drawRadio.checked) {
						//Inicia el pintado de puntos o líneas con el color seleccionado, el cual es un objeto global de estilos que es cambiado con los botones especializados
						objSketchSelectInt.getFeatures().clear();
						if (this.iconInteraction !== null) {
							this.getMap().removeInteraction(this.iconInteraction);
							this.iconInteraction = null;
						}
						if (this.drawInteraction == null) {
							//Define que se debe iniciar un dibujado del vector como una línea (LineString) con tantos puntos como mantenga presionado el mouse el usuario
							this.drawInteraction = new ol.interaction.Draw({
								source: objSketchSource,
								type: 'LineString',
								geometryName: 'LineString',
								style: objSketchStyles['LineString'],
								//No se agrega el estilo de pintado directamente sino que se tomará del array global para permitirlo cambiar por cada vector generado
								/*style: new ol.style.Style({
									stroke: new ol.style.Stroke({
										width: 2, 
										color: objSketchStyles['LineString'].stroke_.color_
									})
								}),*/
								freehand: true	//Pintado a mano alzada en lugar de líneas 100% rectas entre 2 puntos (inicial y final)
							});
							
							//Evento lanzado cuando se inicia el pintado de un vector, aquí es donde se configura el color a partir del array global cambiando el estilo del Feature generado
							this.drawInteraction.on('drawstart', function(evt) {
								//Código no utilizado, con esto a cada Feature generado se le asignaba un color específico lo cual estaba bien, pero no permitía utilizar el color de la
								//selección de manera natural con el ol.interaction.Select, por tanto se cambió al esquema final de usar una función para el estilo y aquí simplemente se
								//asignará al feature un atributo (con set) el número de color que se usó para el trazo
								/*var s = new ol.style.Style({
									stroke: new ol.style.Stroke({
										width: 2, 
										color: objSketchStyles['LineString'].stroke_.color_
									})
								});
								evt.feature.setStyle(function(feature) {
									return s;
								});*/
								evt.feature.set('colorNum', intSketchSelectedColor);
							});
							
							this.getMap().addInteraction(this.drawInteraction);
						}
						
						//Muestra el selector de colores
						selectDifferentColor();
						showColorPanel(intSketchSelectedColor);
					} else if (this.iconRadio.checked) {
						//Cambia el puntero a modo de colocación de elementos, el siguiente click en el Sketch colocará el elemento seleccionado (en modo browser mostrará el icono
						//correspondiente mientras se arrastra el mouse)
						objSketchSelectInt.getFeatures().clear();
						if (this.drawInteraction !== null) {
							this.getMap().removeInteraction(this.drawInteraction);
							this.drawInteraction = null;
						}
						if (this.iconInteraction == null) {
							this.iconInteraction = new ol.interaction.Draw({
								source: objSketchSource,
								type: 'Point',
								geometryName: 'Icon',
								style: objSketchStyles['Icon']
							});
							
							//Evento lanzado cuando se inicia el pintado de un vector, aquí es donde se configura el elemento a partir del array global cambiando el estilo del Feature generado
							this.iconInteraction.on('drawstart', function(evt) {
								evt.feature.set('itemNum', intSketchSelectedItem);
							});
							
							//Evento lanzado cuando se termina el pintado de un vector, aquí es donde se invoca a la sección para capturar el elemento agregado
							this.iconInteraction.on('drawend', function(evt) {
								evt.feature.set('recordNum', getNextItemTypeRecordNumber());
								//Asigna el elemento recien creado a la pregunta para permitir removerlo cuando se cancele el llenado de su registro
								objQuestion.sketchLastItemDrawn = evt.feature;
								
								//Inicia la captura del nuevo tipo de elemento agregado
								setTimeout(function() {
									//JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
									//Envía la posición final del elemento recien colocado de tal manera que se pueda asignar en las preguntas correspondientes para almacenarlo
									openNewSketchRecordScreen(evt.feature.getGeometry().getCoordinates());
									//JAPR
								}, 100);
							});
							
							this.getMap().addInteraction(this.iconInteraction);
						}
						
						//Muestra el selector de elementos
						this.openItemSelector();
						selectDifferentItem();
						showItemPanel(intSketchSelectedItem);
					} else {
						//Inicia el borrado de alguna marca, ya sea una línea colocada o bien un elemento
						if (this.drawInteraction !== null) {
							this.getMap().removeInteraction(this.drawInteraction);
							this.drawInteraction = null;
						}
						if (this.iconInteraction !== null) {
							this.getMap().removeInteraction(this.iconInteraction);
							this.iconInteraction = null;
						}
					}
				};
				
				//Selecciona un color diferente actualizando el array global para que los nuevos trazos lo utilicen, al mismo tiempo que actualiza el icono de dibujo para reflejar el
				//color seleccionado
				SketchModeControl.prototype.handleSketchColor = function handleSketchMode() {
					//Actualiza el color de trazo en el array
					var intColorNum = 0;
					for (var intColorInput in this.colorsArray) {
						if ( this.colorsArray[intColorInput].checked ) {
							intColorNum = intColorInput;
							break;
						}
					}
					
					intSketchSelectedColor = objQuestion.sketchSelectedColor = intColorNum;
					var objColor = goDefaultSketchColorsArray[intColorNum];
					if ( !objColor ) {
						return;
					}
					
					//$('.sketch-mode-draw label').css('color', '#' + objColor.color)
					objSketchStyles['LineString'].stroke_.color_ = objColor.colorArray;
					
					//Oculta el selector de colores una vez que ha sido seleccionado alguno
					hideColorSelector();
					showColorPanel(intSketchSelectedColor);
				};
				
				//Función para mostrar nuevamente el selector de colores en caso de que hubiera estado oculto (se volvió a presionar el botón para pintar ya estando seleccionado, por lo
				//que no lanza el evento change del input radio de esas opciones, así que se tiene que lanzar con un evento click simplemente en dicho botón*/
				SketchModeControl.prototype.showColorSelection = function showColorSelection() {
					selectDifferentColor();
				};

				SketchModeControl.prototype.showItemSelection = function showItemSelection() {
					selectDifferentItem();
				};
				
				//Función para eliminar una marca según a cual se le haga click
				SketchModeControl.prototype.deleteSelection = function deleteSelection() {
					var arrDeletedRows = new Array();
					objSketchSelectInt.getFeatures().forEach(function (aFeature) {
						//Verifica si se trata de un trazo o un elemento, en caso de ser un elemento, identifica el número de registro para removerlo también de la sección asociada
						var strGeometryName = aFeature.getGeometryName();
						if ( strGeometryName == 'geometry' ) {
							strGeometryName = aFeature.getGeometry().getType();
						}
						
						if ( strGeometryName == 'Point' ) {
							strGeometryName = 'Icon';
						}
						
						//Si se trata de un elemento, agrega el elemento a la colección para eliminar sus registros
						if ( strGeometryName == 'Icon' ) {
							var intItemNum = aFeature.get('itemNum'), intRecordNum = aFeature.get('recordNum');
							arrDeletedRows.push({itemNum: intItemNum, recordNum: intRecordNum});
						}
						
						//Remueve el elemento del Sketch
						objSketchSource.removeFeature(aFeature);
					});
					objSketchSelectInt.getFeatures().clear();
					
					//Si hubo elementos que deben ser removidos, invoca a la función para tal efecto
					if ( arrDeletedRows.length ) {
						removeSketchRecords(arrDeletedRows);
					}
				};
				
				//Función para abrir el selector de elementos que pueden ser colocados en el Sketch
				SketchModeControl.prototype.openItemSelector = function openItemSelector() {
					//JAPR 2019-04-01: Corregido un bug, no se podía cambiar el elemento seleccionado sin primero utilizar algún otro botón de la toolbar (#7WMZIV)
					//Ahora se invocará a una función global para desligar el evento del componente de la acción y poder invocarla en puntos diferentes
					openItemSelectorPopUp();
					
					/*if ( !objQuestion.sketchPopUp / *|| !objQuestion.sketchDataView* / ) {
						return;
					}
					
					//JAPR 2019-04-01: Corregido un bug, no se podía cambiar el elemento seleccionado sin primero utilizar algún otro botón de la toolbar (#7WMZIV)
					objQuestion.sketchItemSelectorShown = true;
					//JAPR
					//Muestra el popup con la dataView
					objQuestion.sketchPopUp.show(garrSketchPopUpPosition[0], garrSketchPopUpPosition[1], garrSketchPopUpPosition[2], garrSketchPopUpPosition[3]);*/
				};
				
				return SketchModeControl;
			}(ol.control.Control));
			
			//Botón para eliminar por completo los trazos y elementos colocados
			var ClearSketchControl = (function (Control) {
				function ClearSketchControl(opt_options) {
					var options = opt_options || {};
					var button = $('<button id="' + strFieldName + '_clearSketch" class="sketchButton clearCanvasBtn")"></button>');
					//var button = $('<button id="' + strFieldName + '_clearSketch" class="sketchButton clearCanvasBtn" style="background-image:url(\'images/sketch/sketch_restore_zoom.png\')"></button>');
					
					var element = $('<div class="sketch-clear-canvas ol-unselectable ol-control"></div>');
					element[0].appendChild(button[0]);
					
					Control.call(this, {
						element: element[0],
						target: options.target
					});
					
					button[0].addEventListener('click', this.handleClearSketch.bind(this), false);
				}
				
				if ( Control ) ClearSketchControl.__proto__ = Control;
				ClearSketchControl.prototype = Object.create( Control && Control.prototype );
				ClearSketchControl.prototype.constructor = ClearSketchControl;
				
				//En lugar de cambiar el nivel de zoom, se modifica la vista para que ocupe toda el área definida sin constrainResolution para no utilizar zoom decimal con aproximación
				//y que se vea tal como al inicio cuando se generó el mapa
				ClearSketchControl.prototype.handleClearSketch = function handleClearSketch () {
					var blnAnswer = undefined, strMsg = translate("All drawn elements and associated data will be erased. Do you wish to continue?");
					//JAPR 2019-02-25: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					//Corregido un bug, el proceso de confirmación en móvil es asíncrono, así que al no esperar a la respuesta estaba quedando sin eliminar los trazos
					var objClearDfd = $.Deferred(), objClearProm = objClearDfd.promise();
					
					if (objSettings && objSettings.webMode) {
						var blnAnswer = confirm(strMsg);
						if ( blnAnswer ) {
							objClearDfd.resolve();
						}
						else {
							objClearDfd.reject();
						}
					}
					else {
						navigator.notification.confirm(strMsg, function(answer) {
							if (answer == 1) {
								objClearDfd.resolve();
							}
							else {
								objClearDfd.reject();
							}
						});
					}
					
					objClearProm.done(function (){
						clearFeatures();
					});
					//JAPR
				};
				
				return ClearSketchControl;
			}(ol.control.Control));
			
			//Crea el panel para mostrar el color seleccionado
			var ColorPanelsControl = (function (Control) {
				function ColorPanelsControl(opt_options) {
					var options = opt_options || {};
					var div = $('<div id="' + strFieldName + '_colorPanel"></div>');
					
					var element = $('<div class="sketch-panel colorPanel tagHidden" style="background-color:#' + goDefaultSketchColorsArray[0].color +'"></div>');
					element[0].appendChild(div[0]);
					
					Control.call(this, {
						element: element[0],
						target: options.target
					});
					
					//div[0].addEventListener('click', this.handleRestoreZoom.bind(this), false);
				}
				
				if ( Control ) ColorPanelsControl.__proto__ = Control;
				ColorPanelsControl.prototype = Object.create( Control && Control.prototype );
				ColorPanelsControl.prototype.constructor = ColorPanelsControl;
				
				/*ColorPanelsControl.prototype.handleRestoreZoom = function handleRestoreZoom () {
					hideColorSelector();
					hideItemSelector();
					this.getMap().getView().fit(objSketchExtent, {size: map.getSize(), constrainResolution:false});
				};*/
				
				return ColorPanelsControl;
			}(ol.control.Control));
			
			//Crea el panel para mostrar el elemento seleccionado
			var ItemPanelsControl = (function (Control) {
				function ItemPanelsControl(opt_options) {
					var options = opt_options || {};
					var div = $('<div id="' + strFieldName + '_itemPanel"><img class="itemPreview" src="images/emptyimg.png"></div>');
					
					var element = $('<div class="sketch-panel itemPanel tagHidden"></div>');
					element[0].appendChild(div[0]);
					
					Control.call(this, {
						element: element[0],
						target: options.target
					});
					
					div[0].addEventListener('click', this.handleShowItemImage.bind(this), false);
				}
				
				if ( Control ) ItemPanelsControl.__proto__ = Control;
				ItemPanelsControl.prototype = Object.create( Control && Control.prototype );
				ItemPanelsControl.prototype.constructor = ItemPanelsControl;
				
				//Muestra la imagen del elemento a utilizar en un preview de pantalla completa
				ItemPanelsControl.prototype.handleShowItemImage = function handleShowItemImage () {
				};
				
				return ItemPanelsControl;
			}(ol.control.Control));
			
			//*******************************************************************************************************************
			//Crea un nuevo OpenLayer, por default es un canvas vacío del tamaño especificado, pero si se especificó una imagen entonces agregará un Layer mas que corresponde a dicha imagen
			var map = new ol.Map({
				target: strFieldName + '_map',
				interactions: ol.interaction.defaults({doubleClickZoom: false}).extend([objSketchSelectInt, objSketchTranslateInt, objSketchEditInt]),
				controls: ol.control.defaults().extend([
					new RestoreZoomControl(),
					new SketchModeControl(),
					new ClearSketchControl(),
					new ColorPanelsControl(),
					new ItemPanelsControl()
				]),
				layers: arrLayers,
				view: new ol.View({
					center: ol.extent.getCenter(objSketchExtent),
					resolution: 1
					/*Agregar este extent limitará el movimiendo dentro del OpenLayer, lo cual no será correcto ya que no habrá scroll para ver el resto del contenido, sino que se dejará
					el tamaño del OpenLayer al de la imagen utilizada (o el tamaño definido por el usuario en casos donde no hay imagen) y se usará el 100% del área visible del dispositivo
					para el trabajo en el lienzo, permitiendo el movimiento propio del OpenLayer incluído el Zoom para reposicionar la imagen sobre la que se puede trabajar */
					/*extent: objSketchExtent,*/
				})
			});
			
			//Guarda la instancia del mapa para permitir su manipulación mas adelante
			objQuestion.sketchMap = map;
			
			//*******************************************************************************************************************
			//Genera la DataView que mostrará los elementos que se pueden graficar en el Sketch, esta se encuentra en un div oculto hasta que sea incluída en el PopUp
			//En este caso se trata de un HTML de despliegue, así que puede usar formatos y no se deben reemplazar defaults ni delimitar textos puesto que no espera
			//una expresión ni evaluación de código
			var strOptionsTemplate = replaceVars(gsSketchItemsTemplate, undefined, true);
			
			var strImgWidth = "";
			var intCanvasWidth = parseInt(objQuestion.sketchImageWidth);
			if (intCanvasWidth) {
				strImgWidth = "width:" + String(objQuestion.sketchImageWidth);
				if (strImgWidth.indexOf("%") == -1 && strImgWidth.indexOf("px") == -1) {
					strImgWidth += "px";
				}
				strImgWidth += ";";
			}
			
			var strImgHeight = "";
			var intCanvasHeight = parseInt(objQuestion.sketchImageHeight);
			if (intCanvasHeight) {
				strImgHeight = "height:" + String(objQuestion.sketchImageHeight);
				if (strImgHeight.indexOf("%") == -1 && strImgHeight.indexOf("px") == -1) {
					strImgHeight += "px";
				}
				strImgHeight += ";";
			}
			
			var strImageSize = "";
			if (strImgWidth || strImgHeight) {
				strImageSize = strImgWidth + strImgHeight;
			}
			
			var objEmptyOption = {attrib1:'', imageSize:strImageSize, imageDisplay:"table-cell"};
			
			//Genera el PopUp que mostrará la DataView cuando se quiera cambiar el elemento seleccionado
			var objSketchPopUp = objQuestion.sketchPopUp = new dhtmlXPopup();
			objSketchPopUp.attachEvent("onShow", function() {
				objSketchPopUp.clear();
				objSketchPopUp.attachHTML("<div id=\"sketchDataView\" style=\"width:300px;height:300px;\"></div>");
				var objSketchDataView = objQuestion.sketchDataView = new dhtmlXList({
					container:"sketchDataView",
					type:{
						template: strOptionsTemplate,
						margin:0,
						padding:0,
						border:0,
						height:"auto"
					}
				});
				
				var strFirstItemID = '';
				for (var intOptionNum in objLocalOptionsOrder) {
					var objOption = objLocalOptions[objLocalOptionsOrder[intOptionNum]];
					//Haciendo este extend, se asignarán los atributos que si vienen del objOption sobre los vacíos de objEmptyOption, mientras que los que no vienen
					//permanecerán vacíos para no afectar al elemento agregado a la lista
					//Para el option que se agregará a la lista, la propiedad id representa el identificador único de la lista, pero a la vez representará el nombre de la
					//opción correspondiente (el atributo de despliegue) el cual debe ser único también
					var objNewOption = $.extend({}, objEmptyOption, objOption, {id:objOption.name, key:objOption.id});
					//JAPR 2016-05-13: Corregido un bug, no estaba reemplazando las variables del template de usuario, así que referencias como {@IMGFULLPATH} no servían (#XRKG3R)
					//En este caso se trata de un HTML de despliegue, así que puede usar formatos y no se deben reemplazar defaults ni delimitar textos puesto que no espera
					//una expresión ni evaluación de código
					if (objNewOption.displayImage && objNewOption.displayImage.indexOf("{@IMG") != -1){
						objNewOption.displayImage = replaceVars(objNewOption.displayImage, undefined, true);
					}
					objSketchDataView.add(objNewOption);
					
					if ( !strFirstItemID ) {
						strFirstItemID = objNewOption.id;
					}
				}
				
				if ( strFirstItemID ) {
					objSketchDataView.select(strFirstItemID);
				}
				
				//Selecciona un nuevo elemento de la lista para ser graficado, cambiando adicionalmente la imagen desplegada en el panel de elementos
				objSketchDataView.attachEvent("onItemClick", function (id, ev, html){
					//Dado a que el evento ingresa antes de que esté asignado el nuevo ID seleccionado, debe aplicarse este timeout para que las funciones puedan identificar
					//correctamente el nuevo ID seleccionado al llegar a ellas
					setTimeout(function() {
						//Cambia el elemento selecionado al que fue elegido de la lista
						intSketchSelectedItem = objQuestion.sketchSelectedItem = objSketchDataView.indexById(id);
						var objItem = objSketchItemsArray[intSketchSelectedItem];
						if ( !objItem ) {
							return;
						}
						
						objSketchStyles['Icon'] = objItem.style;
						//En este punto tiene que resetear la interacción, ya que la imagen que se quedó con el mouse corresponde a la previa que estaba en este objeto, así que se debe
						//cancelar dicha interacción ya generada y volver a crear una para que tome la nueva imagen, para esto se cambia el radio de modo a selección para invocar al evento
						//correspondiente que limpiará la interacción ya generada, y posteriormente se hace lo mismo pero con el radio de colocación de elementos para que tome la imagen
						//que acabamos de seleccionar
						$('#SketchModeMove').prop('checked', true);
						$.proxy(objQuestion.sketchModeControl.handleSketchMode, objQuestion.sketchModeControl)();
						$('#SketchModeIcon').prop('checked', true);
						$.proxy(objQuestion.sketchModeControl.handleSketchMode, objQuestion.sketchModeControl)();
						
						//Oculta el selector de elementos una vez que ha sido seleccionado alguno
						objSketchPopUp.hide();
						hideItemSelector();
						showItemPanel(intSketchSelectedItem);
					}, 100);
					return true;
				});
				
				objSketchDataView.refresh();
				
				//JAPR 2019-04-01: Corregido un bug, no se podía cambiar el elemento seleccionado sin primero utilizar algún otro botón de la toolbar (#7WMZIV)
				setTimeout(function() {
					objQuestion.sketchItemSelectorShown = false;
				}, 100);
				//JAPR
			});
			
			//JAPR 2019-03-13: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			//En este punto ya se tiene construída toda la ventana de Sketch y generados los componentes para su interacción, solo resta que el usuario lo empiece a editar o a agregar nuevos
			//trazos/elementos, sin embargo si cancela la sesión de Sketch, ninguno de los cambios realizados deberá permanecer grabado, eso es sencillo para los objetos en si del Sketch pues
			//simplemente no pasará por el método que sobreescribirá ni la imagen ni los vectores en la pregunta Sketch+, sin embargo podría estar modificando, agregando o eliminando registros
			//de las secciones Maestro-detalle asociadas a las opciones de respuesta, lo cual corrompería la información de las mismas en caso de cancelar la edición del Sketch, ya que esos
			//cambios suceden en línea, por lo tanto deberá primero respaldar en cada pregunta las propias respuestas para que en caso de cancelar la sesión se puedan restaurar
			backupSketchDataRecords();
			
			//Validación para no volver a generar el Sketch en caso que ya se hubiera construido su ventana y estuviera entre la transición de una captura de elemento y regresar al área de dibujo
			objQuestion.sketchGenerated = true;
		} catch(e) {
			var strMsg = translate("There was an error while generating the sketch");
			throwError(-1, strMsg, translate("Error generating the page"));
			Debugger.message(strMsg + ": " + e);
		}
	}).fail(function() {debugger;
		//No se pudo cargar la imagen, pregunta si quiere continuar con un Canvas vacío o salir
		//alert(translate("Unable to load the sketch image. Do you want to continue with an empty Sketch"));
		alert(translate("Unable to load the sketch image"));
	});
}

/* Genera la imagen combinada del Sketch con todos los elementos gráficos colocados, actualizando la respuesta de la pregunta que inició el trazo */
function saveSketch(aQuestionID) {
	var objQuestion = selSurvey.currentCapture.question;
	if ( !objQuestion ) {
		return;
	}
	
	if ( objQuestion.sketchPopUp ) {
		objQuestion.sketchPopUp.unload();
		objQuestion.sketchPopUp = null;
	}
	
	//Primero restaura el zoom y la posición del mapa para que ocupe todo el tamaño definido, de lo contrario la imagen no saldrá correctamente escalada ni completa
	restoreSketchZoom();
	//A continuación remueve la selección para que todos los trazos y elementos se vean con su estilo normal
	if ( objQuestion.sketchSelectedInteraction ){
		objQuestion.sketchSelectedInteraction.getFeatures().clear();
	}
	
	var objMap = objQuestion.sketchMap;
	if ( !objMap ) {
		return;
	}
	
	//Función para concluir el grabado de una pregunta Sketch
	objQuestion.finishSavingSketch = function() {
	}
	
	//Función en caso de error de grabado de una pregunta Sketch
	objQuestion.failedSavingSketch = function() {
	}
	
	/* Código funcional para obtener la imagen de tamaño real del Sketch dibujado */
	objMap.once('rendercomplete', function(event) {
		var canvas = event.context.canvas;
		var strSketchData = canvas.toDataURL('image/jpeg'), intPos = strSketchData.indexOf(',');
		if (strSketchData) {
			var comaIndex = strSketchData.indexOf(",");
			if (comaIndex > -1) {
				strSketchData = strSketchData.slice(comaIndex + 1, strSketchData.length);
			}
		}
		
		//Genera el contenido GeoJSON de los trazos realizados, pero los almacena en una variable temporal del Answer, ya que no se pueden asignar sino hasta que el proceso completo
		//de grabado de la imagen concluya, lo cual finalmente invocará a la función photoSketch que es donde este valor se convertirá en final
		objQuestion.answer.features = objApp.sketchGeoJSON.writeFeatures(objQuestion.sketchSource.getFeatures());
		
		if ( strSketchData ) {
			//Muestra la ventana para indicar que se está grabando el Sketch, esto es especialmente útil en captura browser donde tarda mas en hacer el upload al servidor estando en línea
			mobileLoading(translate("Processing image..."));
			setTimeout(function () {
				//JAPR 2014-12-17: Agregado el upload de la imagen en modo Web
				if ( objSettings && objSettings.webMode ) {
					//Sube la imagen del sketch al server y recupera la ruta que le fue asignada
					xmlObj = getXMLObject();
					if (xmlObjSketchTimeOut !== null) {
						try {
							clearTimeout(xmlObjSketchTimeOut);
						} catch (e) {
							//Ignora este error
						}
						xmlObjSketchTimeOut = null;
					}
					
					if (xmlObj == null) {
						return;
					}
					
					var urlService = "http://" + objSettings.esurveyFolder + objSettings.esurveyservice;
					//JAPR 2012-11-28: Corregido el protocolo de acceso desde el browser
					if (objSettings && objSettings.webMode && objSettings.protocol) {
						urlService = urlService.replace('http:', objSettings.protocol);
					}
					
					//Prepara el llamado sincrono de la carga del archivo de sketch
					//JAPR 2012-12-17: Modificado para hacer una llamada asíncrona
					xmlObj.open('POST', urlService, true);
					//xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
					xmlObj.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
					
					xmlObjSketchTimeOut = setTimeout(function () {
						//Termina la llamada asíncrona aunque no hubiera regresado
						if (xmlObjSketchTimeOut != null) {
							try {
								clearTimeout(xmlObjSketchTimeOut);
							} catch (e) {
								//Ignora este error
							}
							xmlObjSketchTimeOut = null;
						}
						xmlObj.abort();
						var strErrDesc = '(closeAndRefresh) You have lost your connection, try to connect again later. '
						objApp.abortFileUpload(strErrDesc);
						
						//JAPRWarning: En este caso no debe cerrar nada, si hubiera error se debería notificar e indicar al usuario para que no continúe
						/*//Hay que cerrar el diálogo aunque no hubiera grabado el contenido del Sketch debido al error de Upload
						fromFullScreen = true; //viene de pantalla completa
						$("#SketchFullScreen_"+objQuestion.id).dialog('close');
						//si es modo web no se hace el change page
						//selSurvey.changeSection(selSurvey.currentCapture.section.number);*/
					}, (objSettings.uploadTimeOut * 1000));
					
					//Función que será invocada al terminar el request asíncrono
					xmlObj.onreadystatechange = objApp.sketchUploadCallback;
					
					//Genera los parámetros del request
					var strParams = objSettings.getHttpReqParamsStr() + '&action=uploadfile&ftype=sketch' +
						'&filetype=sketch&data=' + encodeURIComponent(strSketchData) + '&responseformat=json';
					var strFileName = '';
					if (selSurvey && objQuestion.section) {
						var intRecordNum = objQuestion.section.recordNum;
						var strSurveyDate = selSurvey.surveyDate;
						var strSurveyHour = selSurvey.surveyHour;
						strSurveyDate = strSurveyDate.replace(/-/gi, '');
						strSurveyHour = strSurveyHour.replace(/:/gi, '');
						var strFileName = 'sketch_' + objSettings.user + '_' + strSurveyDate + '_' + strSurveyHour + '_' + selSurvey.id + '_' + objQuestion.id + '_' + intRecordNum;
						strParams += '&surveyID=' + selSurvey.id + '&questionID=' + objQuestion.id + 
							'&recordNum=' + intRecordNum + '&surveyDate=' + selSurvey.surveyDate + '&surveyHour=' + selSurvey.surveyHour +
							'&filename=' + encodeURIComponent(strFileName);
					}
					
					//Inicia el llamado síncrono para subir la imagen de sketch
					try {
						xmlObj.send(strParams);
					} catch (e) {
						//JAPREliminar: Estos errores se deben almacenar en un log, no deben terminar con el proceso
						var strErrDesc = 'There was an error sending the sketch image to the server ' + strFileName + ', error: ' + e;
						objApp.abortFileUpload(strErrDesc);
						
						//JAPRWarning: En este caso no debe cerrar nada, si hubiera error se debería notificar e indicar al usuario para que no continúe
						/*//Hay que cerrar el diálogo aunque no hubiera grabado el contenido del Sketch debido al error de Upload
						fromFullScreen = true; //viene de pantalla completa
						$("#SketchFullScreen_"+objQuestion.id).dialog('close');
						//Conchita 2015-01-21 en modo web no se tiene q hacer el changepage puesto que no hay zoom
					   // selSurvey.changeSection(selSurvey.currentCapture.section.number);*/
					}
					//JAPR
					
					//No continua con el resto de la función para no permitir el changeSection mas abajo, ya que necesita la referencia a la
					//pregunta activa tal como lo haría la toma de una foto
					return;
				}
				//JAPR
				
				//Guardamos el strSketchData (la imagen del canvas) en el answer.photo de esa pregunta
				//y seteamos la propiedad de esa pregunta para indicar que se debe de guardar lo de fullscreen y no lo del canvas en miniatura
				//JAPR 2019-02-25: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				//Corregido un bug, debido a la manera en que funciona photoSketch en dispositivo, tenía que escribir el archivo a disco pero con imágenes muy grandes se tardaba algo de tiempo,
				//sin embargo como no había un callback de finalización, continuaba con el proceso y regresaba el control a este punto, donde finalmente se cerraba el diálogo y hacía el cambio
				//de sección con closeDialogs sin esperar a que terminara de grabar la imagen, por lo que no se veía ninguna imagen en dispositivo en esas situaciones cuando se trataba de una
				//sección maestro-detalle que son las únicas que requieren reconstruir la sección, así que ahora tendrá que esperar a que termine de realizar el grabado antes de continuar
				objQuestion.sketchSavingCallbackFn = function() {
					//Al terminar debe cerrar el diálogo para regresar a la sección correspondiente
					fromFullScreen = true; //viene de pantalla completa
					closeDialogs();
				}
				
				objApp.photoSketch(strSketchData, undefined, objQuestion.id, undefined, objQuestion.sketchSavingCallbackFn);
				//JAPR
			}, 100);
		}
		/*MAPR 2017-05-15 solucionando problemas con el Sketch que no hacía un limpiado correcto de la pregunta. 
		Se añade una validación más en el caso de que no tenga una firma en el canvas para eliminar la respuesta (canvasPhoto) */
		else {
			if ( objQuestion.answer && objQuestion.answer.canvasPhoto ) {
				ESFile.deleteFile(objQuestion.answer.canvasPhoto);
				objQuestion.answer.canvasPhoto = '';
			}
		}
		//MAPR
		
		//JAPR 2019-02-25: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Corregido un bug, debido a la manera en que funciona photoSketch en dispositivo, tenía que escribir el archivo a disco pero con imágenes muy grandes se tardaba algo de tiempo,
		//sin embargo como no había un callback de finalización, continuaba con el proceso y regresaba el control a este punto, donde finalmente se cerraba el diálogo y hacía el cambio
		//de sección con closeDialogs sin esperar a que terminara de grabar la imagen, por lo que no se veía ninguna imagen en dispositivo en esas situaciones cuando se trataba de una
		//sección maestro-detalle que son las únicas que requieren reconstruir la sección, así que ahora tendrá que esperar a que termine de realizar el grabado antes de continuar
		/*//Al terminar debe cerrar el diálogo para regresar a la sección correspondiente
		fromFullScreen = true; //viene de pantalla completa
		closeDialogs();
		*/
		//JAPR
	});
	
	objMap.renderSync();
}

/* Elimina todos los trazos realizados en el Sketch así como los elementos colocados y los registros asociados a ellos */
function clearFeatures() {
	var objQuestion = selSurvey.currentCapture.question;
	if ( !objQuestion.sketchSource ) {
		return;
	}
	
	//Debe eliminar también el elemento seleccionado, ya que daba la impresión de no estar eliminando nada mientras había selección
	if ( objQuestion.sketchSelectedInteraction ) {
		objQuestion.sketchSelectedInteraction.getFeatures().clear();
	}
	objQuestion.sketchSource.clear();
}

/* Ignora los cambios realizados y cierra el diálogo sin modificaciones al Sketch */
function cancelSketch() {
	var objQuestion = selSurvey.currentCapture.question;
	if ( objQuestion.sketchPopUp ) {
		objQuestion.sketchPopUp.unload();
		objQuestion.sketchPopUp = null;
	}
	
	//JAPR 2019-03-13: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
	//En este punto se deben restaurar las respuestas respaldadas en los registros de las secciones de datos, pero además debido a que pudieran haber sido eliminados algunos, deberá
	//agregarles la definición de la clase para que no queden corruptos tal como si nunca hubieran sido modificados. Este proceso no es requerido si se graba el Sketch, porque entonces
	//las respuestas actuales son las que permanecen y se van a respaldar la próxima vez que inicie la edición, tampoco es requerido este proceso si técnicamente no hubo cambios en los
	//elementos, por lo que se controlará con una bandera propia dela pregunta Sketch
	if ( objQuestion.sketchEdited ) {
		restoreSketchDataRecords();
	}
	
	//Validación para no volver a generar el Sketch en caso que ya se hubiera construido su ventana y estuviera entre la transición de una captura de elemento y regresar al área de dibujo
	objQuestion.sketchGenerated = false;
	closeDialogs();
}

/* Habilita el div de scroll sobrepuesto al div de dibujo para poder moverse por la pantalla sin problema */
function switchSketchScroll() {
	if ( $('.skecthScroll').hasClass('scrolling') ) {
		$('.skecthScroll').removeClass('scrolling');
	}
	else {
		$('.skecthScroll').addClass('scrolling');
	}
}

//Función para mostrar nuevamente el selector de colores cuando se presiona el icono de dibujo
function selectDifferentColor() {
	if ( $('#formColorsDiv').length ) {
		if ( $('#formColorsDiv').hasClass('tagHidden') ) {
			$('#formColorsDiv').removeClass('tagHidden');
		}
	}
};

//Función para ocultar el selector de colores
function hideColorSelector() {
	if ( $('#formColorsDiv').length ) {
		if ( !$('#formColorsDiv').hasClass('tagHidden') ) {
			$('#formColorsDiv').addClass('tagHidden');
		}
	}
};

//Función para mostrar nuevamente el selector de elementos cuando se presiona el icono de colocación de objetos
function selectDifferentItem() {
	//JAPR 2019-04-01: Corregido un bug, no se podía cambiar el elemento seleccionado sin primero utilizar algún otro botón de la toolbar (#7WMZIV)
	var objQuestion = selSurvey.currentCapture.question;
	if ( !objQuestion || objQuestion.sketchItemSelectorShown ) {
		return
	}
	
	//Invoca al PopUp para seleccionar un nuevo elemento, pero se hará 100ms después del click por si se estaba lanzando al mismo evento desde el change de la toolbar, eso dará tiempo
	//a que la bandera activada impida que se duplique la llamada al PopUp
	setTimeout(function() {
		openItemSelectorPopUp();
	}, 100);
	//JAPRWarning, JAPREliminar: No gustó el botón para seleccionar elemento, se decidió que al hacer click en el botón para colocar elemento automáticamente abriera el diálogo
	return;
	if ( $('#formElementsDiv').length ) {
		if ( $('#formElementsDiv').hasClass('tagHidden') ) {
			$('#formElementsDiv').removeClass('tagHidden');
		}
	}
};

//Función para ocultar el selector de colores
function hideItemSelector() {
	if ( selSurvey.currentCapture.question.sketchPopUp ) {
		selSurvey.currentCapture.question.sketchPopUp.hide();
	}
	
	if ( $('#formElementsDiv').length ) {
		if ( !$('#formElementsDiv').hasClass('tagHidden') ) {
			$('#formElementsDiv').addClass('tagHidden');
		}
	}
};

//Función para mostrar nuevamente el panel del color seleccionado, además de píntar el fondo del panel con el color seleccionado
function showColorPanel(iColorIdx) {
	if ( $('.colorPanel').length ) {
		if ( iColorIdx == undefined ) {
			iColorIdx = 0;
		}
		if ( goDefaultSketchColorsArray[iColorIdx] ) {
			$('.colorPanel div').css('background-color', '#' + goDefaultSketchColorsArray[iColorIdx].color);
		}
		
		if ( $('.colorPanel').hasClass('tagHidden') ) {
			$('.colorPanel').removeClass('tagHidden');
		}
	}
};

//Función para ocultar el panel del color seleccionado
function hideColorPanel() {
	if ( $('.colorPanel').length ) {
		if ( !$('.colorPanel').hasClass('tagHidden') ) {
			$('.colorPanel').addClass('tagHidden');
		}
	}
};

//Función para mostrar nuevamente el panel del tipo de elemento seleccionado, además de mostrar la imagen correspondiente a dicho elemento
function showItemPanel(iItemIdx) {
	if ( $('.itemPanel').length ) {
		if ( iItemIdx == undefined ) {
			iItemIdx = -1;
		}
		
		if ( selSurvey.currentCapture.question.sketchItemsArray && selSurvey.currentCapture.question.sketchItemsArray[iItemIdx] ) {
			$('.itemPanel .itemPreview').attr('src', selSurvey.currentCapture.question.sketchItemsArray[iItemIdx].imageSrc);
		}
		else {
			$('.itemPanel .itemPreview').attr('src', 'emptyimg.png');
		}
		
		if ( $('.itemPanel').hasClass('tagHidden') ) {
			$('.itemPanel').removeClass('tagHidden');
		}
	}
};

//Función para ocultar el panel del tipo de elemento seleccionado
function hideItemPanel() {
	if ( $('.itemPanel').length ) {
		if ( !$('.itemPanel').hasClass('tagHidden') ) {
			$('.itemPanel').addClass('tagHidden');
		}
	}
};

//Restaura el zoome del Sketch al inicial, entre otras cosas para permitir generar la imagen final con el tamaño deseado
function restoreSketchZoom() {
	var objQuestion = selSurvey.currentCapture.question;
	if ( !objQuestion.sketchMap ) {
		return;
	}
	
	var map = objQuestion.sketchMap;
	map.getView().fit(objQuestion.sketchExtent, {size: map.getSize(), constrainResolution:false});
}

/* Obtiene el siguiente número de registro según el elemento actualmente seleccionado para colocación, es decir, de la sección asociada a dicho tipo de elemento. Este método solo debe ser
utilizado cuando se está colocando un nuevo elemento, ya que obtendrá el siguiente ID que debería ser agregado según la cantidad de registros que actualmente contiene la sección asociada */
function getNextItemTypeRecordNumber() {
	var objQuestion = selSurvey.currentCapture.question;
	var intSketchSelectedItem = objQuestion.sketchSelectedItem;
	if ( intSketchSelectedItem < 0 || !objQuestion || !objQuestion.optionsOrder || !objQuestion.optionsOrder[intSketchSelectedItem] ) {
		return -1;
	}
	
	var objOption = objQuestion.options[objQuestion.optionsOrder[intSketchSelectedItem]];
	if ( !objOption || !objOption.nextSection ) {
		return -1;
	}
	
	var objSection = selSurvey.sections[objOption.nextSection];
	if ( !objSection ) {
		return -1;
	}
	
	var intNextNewRecord = objSection.maxRecordNum;
	if (intNextNewRecord == 1 && objSection.empty) {
		intNextNewRecord = 0;
	}
	
	return intNextNewRecord;
}

/* Esta función se encarga de abrir la ventana para agregar un nuevo registro de datos según el tipo seleccionado que acaba de ser colocado en el Sketch, para ello realiza un cambio
de sección a la configurada en la opción de respuesta correspondiente (si es que hay alguna configurada), lo cual simplemente dejará construído el diálogo actual ya que los diálogos no
son destruídos al cambiar de sección, sin embargo se cambiará el registro, pregunta y sección activa a la que se usará para la captura, por tanto se debe regresar a este punto una vez
concluida o cancelada la captura del registro en los métodos correspondientes
//JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
Agregado el parámetro aPosition que corresponde con un array de la posición donde fue colocado el elemento [x, y]
*/
function openNewSketchRecordScreen(aPosition) {
	var objQuestion = selSurvey.currentCapture.question;
	var intSketchSelectedItem = objQuestion.sketchSelectedItem;
	if ( intSketchSelectedItem < 0 || !objQuestion || !objQuestion.optionsOrder || !objQuestion.optionsOrder[intSketchSelectedItem] ) {
		return;
	}
	
	var objOption = objQuestion.options[objQuestion.optionsOrder[intSketchSelectedItem]];
	if ( !objOption || !objOption.nextSection ) {
		return;
	}
	
	//En este caso la opción de respuesta si tiene una sección asociada, así que inicia el cambio de sección hacia ella
	var intEntrySectionID = objOption.nextSection;
	//Debe convertir le ID de sección a la mapeada, ya que no se captura directamente sobre la sección asociada a la opción de respuesta
	if ( selSurvey.sketchCloneMapping['sections'][intEntrySectionID] ) {
		intEntrySectionID = selSurvey.sketchCloneMapping['sections'][intEntrySectionID];
		var objSection = selSurvey.sections[intEntrySectionID]
		if ( objSection ) {
			objSection.clearData();
			
			//JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			//Asigna como respuesta la posición inicial en las preguntas que están configuradas para almacenarlas
			var intX = undefined, intY = undefined;
			if ( aPosition && $.isArray(aPosition) ) {
				intX = (intX = parseInt(aPosition[0]))?intX:0;
				intY = (intY = parseInt(aPosition[1]))?intY:0;
			}
			
			if ( objSection.xPosQuestionID ) {
				var objDataQuestion = objSection.questions[selSurvey.sketchCloneMapping['questions'][objSection.xPosQuestionID]];
				if ( objDataQuestion ) {
					objDataQuestion.answer.setValue(intX);
				}
			}
			
			//La posición Y se registra de manera inversa a como aparece en el mapa, siendo el valor mas pequeño el que está en la parte inferior y el mas grande en la parte superior, por
			//lo tanto utilizando las dimensiones de la imagen se generará con la diferencia del height de la misma
			if ( objSection.yPosQuestionID ) {
				var objDataQuestion = objSection.questions[selSurvey.sketchCloneMapping['questions'][objSection.yPosQuestionID]];
				var intCanvasHeight = objQuestion.sketchExtent[3];
				if ( objDataQuestion && intCanvasHeight ) {
					objDataQuestion.answer.setValue(intCanvasHeight - intY);
				}
			}
			
			if ( objSection.itemNameQuestionID ) {
				var objDataQuestion = objSection.questions[selSurvey.sketchCloneMapping['questions'][objSection.itemNameQuestionID]];
				if ( objDataQuestion ) {
					objDataQuestion.answer.setValue(objOption.name);
				}
			}
			//JAPR
			
			//Dado a que la sección es estándar y que se pregeneró su HTML, es necesario volver a generarlo por lo que hace un refresh después de haber limpiado el valor
			for (var intQuestionNum in objSection.questionsOrder) {
				var objDataQuestion = objSection.questions[objSection.questionsOrder[intQuestionNum]];
				if ( objDataQuestion ) {
					objDataQuestion.refreshValue();
				}
			}
			selSurvey.changeSection(objSection.number, undefined, undefined, undefined, undefined, undefined, undefined, false);
		}
	}
}

/* Esta función se encarga de abrir la ventana para agregar un nuevo registro de datos según el tipo seleccionado que acaba de ser colocado en el Sketch, para ello realiza un cambio
de sección a la configurada en la opción de respuesta correspondiente (si es que hay alguna configurada), lo cual simplemente dejará construído el diálogo actual ya que los diálogos no
son destruídos al cambiar de sección, sin embargo se cambiará el registro, pregunta y sección activa a la que se usará para la captura, por tanto se debe regresar a este punto una vez
concluida o cancelada la captura del registro en los métodos correspondientes */
function openEditSketchRecordScreen() {
	var objQuestion = selSurvey.currentCapture.question;
	var intSketchSelectedItem = objQuestion.sketchLastItemDrawn.get('itemNum'), intRecordNum = objQuestion.sketchLastItemDrawn.get('recordNum');
	if ( intSketchSelectedItem < 0 || !objQuestion || !objQuestion.optionsOrder || !objQuestion.optionsOrder[intSketchSelectedItem] ) {
		return;
	}
	
	var objOption = objQuestion.options[objQuestion.optionsOrder[intSketchSelectedItem]];
	if ( !objOption || !objOption.nextSection ) {
		return;
	}
	
	//En este caso la opción de respuesta si tiene una sección asociada, así que inicia el cambio de sección hacia ella
	var intEntrySectionID = intMDSectionID = objOption.nextSection;
	//Debe convertir le ID de sección a la mapeada, ya que no se captura directamente sobre la sección asociada a la opción de respuesta
	if ( selSurvey.sketchCloneMapping['sections'][intEntrySectionID] ) {
		intEntrySectionID = selSurvey.sketchCloneMapping['sections'][intEntrySectionID];
		var objSection = selSurvey.sections[intEntrySectionID];
		var objMDSection = selSurvey.sections[intMDSectionID];
		if ( objSection && objMDSection ) {
			objSection.clearData();
			
			//Sobrescribe las respuestas de la sección de captura con las del registro correspondiente de la sección maestro-detalle especificada por el elemento seleccionado
			//Para evitar problemas de interacción de algún tipo, NO cambiará la sección ni preguntas activas, simplemente usará las respuestas del registro identificado como el que se
			//cambiará para sobrescribir las respuestas de la sección de captura
			for (var intQuestionNum in objMDSection.questionsOrder) {
				var objMDQuestion = objMDSection.questions[objMDSection.questionsOrder[intQuestionNum]];
				if ( objMDQuestion && objMDQuestion.answers[intRecordNum] ) {
					var intMappedQuestionID = selSurvey.sketchCloneMapping['questions'][objMDQuestion.id];
					if ( intMappedQuestionID ) {
						var objDataQuestion = objSection.questions[intMappedQuestionID];
						if ( objDataQuestion ) {
							//Realiza el copiado de la respuesta de esta pregunta de forma recursiva para evitar sobrescribir referencias y que sigan siendo elementos independientes uno de otro
							$.extend(true, objDataQuestion.answer, objMDQuestion.answers[intRecordNum]);
						}
					}
				}
			}
			
			//Dado a que la sección es estándar y que se pregeneró su HTML, es necesario volver a generarlo por lo que hace un refresh después de haber limpiado el valor
			for (var intQuestionNum in objSection.questionsOrder) {
				var objDataQuestion = objSection.questions[objSection.questionsOrder[intQuestionNum]];
				if ( objDataQuestion ) {
					objDataQuestion.refreshValue();
				}
			}
			selSurvey.changeSection(objSection.number, undefined, undefined, undefined, undefined, undefined, undefined, false);
		}
	}
}

/* Esta función se encarga de grabar el nuevo registro capturado como el siguiente de la sección maestro-detalle para la pregunta Sketch que acaba de colocar al elemento, los parámetros
recibidos son tanto la sección que contiene las respuestas como la pregunta sketch que originó el grabado (dicha pregunta se encargará de idenitificar que elemento fue el último que colocó
y por tanto el que debe ser actualizado) */
function saveNewSketchRecord(iSectionID, iQuestionID) {
	if ( !iSectionID || !iQuestionID ) {
		return;
	}
	
	var objSection = selSurvey.sections[iSectionID], objQuestion = selSurvey.questions[iQuestionID];
	if ( !objSection || !objSection.sourceSectionID || !objQuestion || !objQuestion.sketchLastItemDrawn ) {
		return;
	}
	
	//Obtiene la referencia de la sección Maestro-detalle que será grabada, ya que necesita cambiar el registro activo a ella para crearlo
	var objMDSection = selSurvey.sections[objSection.sourceSectionID];
	
	//Graba los datos como un nuevo registro en la sección maestro-detalle, para ello primero tiene que cambiar la sección como activa incluyendo el número de registro siguiente, lo
	//cual automáticamente creará el registro en caso de ser necesario, y posteriormente sobrescribirá las respuestas de la sección dummy (la indicada como parámetro)
	var intRecordNum = objQuestion.sketchLastItemDrawn.get('recordNum');
	objMDSection.setActive(intRecordNum, true);
	
	//Inicia el proceso de llenado de las respuestas desde la sección dummy hacia la sección de datos en el registro activo que se acaba de especificar, con esto ya quedará respaldada la
	//información correctamente, al final deberá marcar la sección como no vacía
	objMDSection.empty = 0;
	//Recorre todas las preguntas de la sección capturada, sobrescribiendo la respuesta (ya activa) en la sección maestro-detalle según el mapeo de preguntas, aunque el mapeo ocurre desde
	//la sección maestro-detalle hacia la capturada, así que se recorre en dicho orden
	for (var intQuestionNum in objMDSection.questionsOrder) {
		var objMDQuestion = objMDSection.questions[objMDSection.questionsOrder[intQuestionNum]];
		if ( objMDQuestion ) {
			var intMappedQuestionID = selSurvey.sketchCloneMapping['questions'][objMDQuestion.id];
			if ( intMappedQuestionID ) {
				var objDataQuestion = objSection.questions[intMappedQuestionID];
				if ( objDataQuestion ) {
					//Realiza el copiado de la respuesta de esta pregunta de forma recursiva para evitar sobrescribir referencias y que sigan siendo elementos independientes uno de otro
					$.extend(true, objMDQuestion.answer, objDataQuestion.answer);
				}
			}
		}
	}
	
	objQuestion.sketchEdited = true;
	
	//Remueve la referencia al último elemento colocado para no dejar apuntadores perdidos
	objQuestion.sketchLastItemDrawn = undefined;
	
	//Al terminar tiene que cambiar la pregunta activa a la de Sketch+, lo cual incluye cambiar la sección activa, ya que durante la captura se sobrescribieron ambos elementos
	objQuestion.section.setActive(objQuestion.section.recordNum, true, true);
	//Se debe cambiar individualmente la pregunta activa para que vuelva a ser la de Sketch, de lo contrario los componentes de la página de Sketch no responderán correctamente
	objQuestion.setActive(undefined, undefined, true);
	$.mobile.changePage($('#' + "SketchFullScreen_" + iQuestionID), {
		transition : 'pop',
		reverse    : true,
		changeHash : true
	});
}

/* Esta función se encargará de eliminar los registros seleccionados por el usuario, o bien en caso de recibirse sin el parámetro con la colección de registros, se asumirá que se deben
eliminar todos los registros de todas las secciones asociadas. Por simplicidad NO se debería permitir utilizar una misma sección en diferentes preguntas Sketch, ya que mantener sincronizados
los números de registro entre las diferentes opciones de la misma pregunta ya será suficientemente problemático como para poder hacer lo mismo entre todas las posibles preguntas Sketch que 
ni siquiera se encuentran materializadas. Este borrado deberá ser lógico, es decir, los elementos a eliminar deberían ser respuestas de un array en memoria que representa las respuestas 
reales de la sección maestro-detalle, ya que modificar la sección directamente impediría el uso del botón cancelar para revertir los cambios realizados */
function removeSketchRecords(aRecordsNumToRemove) {
	if ( !$.isArray(aRecordsNumToRemove) || !aRecordsNumToRemove.length ) {
		//En este caso se deberán eliminar todos los registros de todas las secciones asociadas al Sketch, por lo que debe recorrer la colección completa de elementos marcados en el Sketch
		//para identificar uno a uno los números de registro y removerlos de la colección de la sección, para esto se invocará a si misma habiendo generado primer el array que no recibió
		//en esta llamada
		var arrDeletedRows = new Array();
		objQuestion.sketchSource.getFeatures().forEach(function (aFeature) {
			//Verifica si se trata de un trazo o un elemento, en caso de ser un elemento, identifica el número de registro para removerlo también de la sección asociada
			var strGeometryName = aFeature.getGeometryName();
			if ( strGeometryName == 'geometry' ) {
				strGeometryName = aFeature.getGeometry().getType();
			}
			
			if ( strGeometryName == 'Point' ) {
				strGeometryName = 'Icon';
			}
			
			//Si se trata de un elemento, agrega el elemento a la colección para eliminar sus registros
			if ( strGeometryName == 'Icon' ) {
				var intItemNum = aFeature.get('itemNum'), intRecordNum = aFeature.get('recordNum');
				arrDeletedRows.push({itemNum: intItemNum, recordNum: intRecordNum});
			}
		});
		
		if ( arrDeletedRows.length ) {
			removeSketchRecords(arrDeletedRows);
		}
	}
	else {
		//En este caso solo elimina los registros indicados en el array
		for (var intIndex in aRecordsNumToRemove) {
			var objRecordToRemove = aRecordsNumToRemove[intIndex];
			removeSketchRecord(objRecordToRemove);
		}
	}
}

/* Dado un par de número de elemento y número de registro, elimina el registro correspondiente de la sección asociada al elemento en recibido */
function removeSketchRecord(oRecordToRemove) {
	if ( !$.isPlainObject(oRecordToRemove) ) {
		return;
	}
	
	var intRecordNum = oRecordToRemove.recordNum, intItemNum = oRecordToRemove.itemNum;
	if ( intRecordNum == undefined || intItemNum === undefined ) {
		return;
	}
	
	//Identifica el tipo de objeto a remover para saber a que sección se deberá realizar el ajuste
	var objQuestion = selSurvey.currentCapture.question;
	if ( !objQuestion || !objQuestion.optionsOrder || !objQuestion.optionsOrder[intItemNum] ) {
		return;
	}
	
	var objOption = objQuestion.options[objQuestion.optionsOrder[intItemNum]];
	if ( !objOption || !objOption.nextSection ) {
		return;
	}
	
	var objSection = selSurvey.sections[objOption.nextSection];
	if ( !objSection ) {
		return;
	}
	
	//Realiza el borrado del registro indicado, lo cual automáticamente recorrerá el resto de los registros una posición hacia abajo
	objSection.removeRecord(intRecordNum);
	objQuestion.sketchEdited = true;
	
	//Al finalizar deberá recorrer todos los elementos colocados en el sketch restantes (el que acaba de ser eliminado ya no cuenta) y debe reducir el número de registro de cada uno en
	//una posición si correspondía a un registro posterior al que acaba de ser eliminado
	objQuestion.sketchSource.getFeatures().forEach(function (aFeature) {
		//Verifica si se trata de un trazo o un elemento, en caso de ser un elemento, identifica el número de registro para removerlo también de la sección asociada
		var strGeometryName = aFeature.getGeometryName();
		if ( strGeometryName == 'geometry' ) {
			strGeometryName = aFeature.getGeometry().getType();
		}
		
		if ( strGeometryName == 'Point' ) {
			strGeometryName = 'Icon';
		}
		
		//Si se trata de un elemento, agrega el elemento a la colección para eliminar sus registros
		if ( strGeometryName == 'Icon' ) {
			var intRecordNumber = aFeature.get('recordNum');
			if ( intRecordNumber >= intRecordNum ) {
				//Debe recorrer el elemento una posición hacia abajo para reflejar que acaba de ser eliminado de la sección
				aFeature.set('recordNum', --intRecordNumber);
			}
		}
	});
}

/* Esta función se encargará de destruir el último elemento colocado debido a que se canceló la captura de su registro asociado */
function cancelNewSketchRecord(iSectionID, iQuestionID) {
	if ( !iSectionID || !iQuestionID ) {
		return;
	}
	
	var objSection = selSurvey.sections[iSectionID], objQuestion = selSurvey.questions[iQuestionID];
	if ( !objSection || !objQuestion || !objQuestion.section ) {
		return;
	}
	
	//Remueve el último elemento colocado en el Sketch. Como se canceló, simplemente no se agregará su información a la sección maestro-detalle correspondiente
	if (objQuestion.sketchLastItemDrawn !== undefined) {
		//Sólo debe remover el elemento si este es nuevo, ya que de lo contrario cancelar simplemente debería ignorar los cambios realizados
		if ( !objQuestion.sketchLastItemDrawn.get('edited') ) {
			objQuestion.sketchSource.removeFeature(objQuestion.sketchLastItemDrawn);
			objQuestion.sketchLastItemDrawn = undefined;
		}
	}
	
	//Regresa al diálogo de Sketch, el cual debería continuar creado en el DOM ya que no se eliminan al cambiar de sección de manera automática sino hasta que la propia sección que lo crea
	//decide removerlo, adicionalmente tiene que cambiar la pregunta activa a la de Sketch+, lo cual incluye cambiar la sección activa, ya que durante la captura se sobrescribieron ambos
	//elementos
	//JAPR 2019-02-18: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
	//Agregado el parámetro bJustCurrentData para indicar que solo se deben cambiar los datos de la sección actual, no afectar los de la sección previa
	objQuestion.section.setActive(objQuestion.section.recordNum, true, true);
	//Se debe cambiar individualmente la pregunta activa para que vuelva a ser la de Sketch, de lo contrario los componentes de la página de Sketch no responderán correctamente
	objQuestion.setActive(undefined, undefined, true);
	$.mobile.changePage($('#' + "SketchFullScreen_" + iQuestionID), {
		transition : 'pop',
		reverse    : true,
		changeHash : true
	});
}

/* Respalda la información de todas las respuestas de las preguntas de las secciones maestro-detalle asociadas a Sketch+ para permitir restaurarlas en caso de ser necesario,
para esto crea un array clon de answer mediante $.extend en todas las preguntas que corresponden a las secciones que se pudieran capturar, esto si bien les removerá las funciones y propiedades
adicionales, se deberán volver a agregar si se llega a cancelar la sesión del Sketch.
Este proceso NO podrá mantener los archivos media que son generados o eliminados en caso de que se remuevan registros, ya que dichos archivos se perderan por completo, por lo que el proceso
de eliminación de registros debería ser un proceso lógico hasta que se decide grabar finalmente el cambio al Sketch+ */
function backupSketchDataRecords() {
	var objQuestion = selSurvey.currentCapture.question;
	if ( !objQuestion || !objQuestion.optionsOrder || !objQuestion.optionsOrder.length ) {
		return;
	}
	
	var objLocalOptions = objQuestion.options;
	var objLocalOptionsOrder = objQuestion.optionsOrder;
	//Primero debe desmarcar la bandera de respaldo de las secciones, para evitar que se realice el respaldo de la misma sección múltiples veces
	for (var intOptionNum in objLocalOptionsOrder) {
		var objOption = objLocalOptions[objLocalOptionsOrder[intOptionNum]];
		if ( objOption && objOption.nextSection ) {
			var objSection = selSurvey.sections[objOption.nextSection];
			if ( !objSection ) {
				continue;
			}
			
			objSection.sketchSectionBackedUp = 0;
		}
	}
	
	//Ahora realiza el respaldo de las secciones que no se consideren vacías, evitando realizarlo mas de una vez sobre la misma sección
	for (var intOptionNum in objLocalOptionsOrder) {
		var objOption = objLocalOptions[objLocalOptionsOrder[intOptionNum]];
		if ( objOption && objOption.nextSection ) {
			var objSection = selSurvey.sections[objOption.nextSection];
			//Si la sección ya se había respaldado, ya no es necesario realizar nuevamente este proceso
			if ( !objSection || objSection.sketchSectionBackedUp ) {
				continue;
			}
			
			//Respalda la propiedad que indica si la sección estaba o no vacía, ya que no es necesario realizar el proceso para secciones que no habían capturado respuestas
			objSection.sketchEmptySection = objSection.empty;
			
			//Sólo es necesario el proceso de respaldo si la sección no se encontraba vacía, si lo estaba, al final una limpieza de la misma bastará para remover los registros capturados
			//que deberán ser eliminados
			if ( !objSection.sketchEmptySection ) {
				for (var intQuestionNum in objSection.questionsOrder) {
					var objDataQuestion = objSection.questions[objSection.questionsOrder[intQuestionNum]];
					if ( objDataQuestion ) {
						//Genera un array con las mismas respuestas que tiene actualmente la pregunta, solo que como pasó por un proceso de conversión a JSON, no contendrá ninguno de los métodos
						//propios de la clase QuestionAnswerCls sino solo las propiedades básicas que definen sus respuestas (tampoco contendrá referencias de ningún tipo si es que hubiera).
						//Se genera como un array directamente para permitir hacer un $.extend en caso de ser requerido un restaurado, y para permitir un anáisis de la información original
						objDataQuestion.answersBackUp = JSON.parse(JSON.stringify(objDataQuestion.answers));
					}
				}
			}
			
			//Marca la sección como ya procesada para respaldo, para evitar que la misma sección utilizada por el mismo Sketch en diferentes opciones se respalde múltiples veces
			objSection.sketchSectionBackedUp = 1;
		}
	}
}

/* Restaura la información de todas las respuestas de las preguntas de las secciones maestro-detalle asociadas a Sketch+ previamente respaldadas al inicio de esta sesión de edición. Utilizado
en caso de cancelar la edición del Sketch para revertir cualquier cambio realidado */
function restoreSketchDataRecords() {
	var objQuestion = selSurvey.currentCapture.question;
	if ( !objQuestion || !objQuestion.sketchEdited || !objQuestion.optionsOrder || !objQuestion.optionsOrder.length ) {
		return;
	}
	
	//Primero debe desmarcar la bandera de restaurado de las secciones, para evitar que se realice el restaurado de la misma sección múltiples veces
	var objLocalOptions = objQuestion.options;
	var objLocalOptionsOrder = objQuestion.optionsOrder;
	for (var intOptionNum in objLocalOptionsOrder) {
		var objOption = objLocalOptions[objLocalOptionsOrder[intOptionNum]];
		if ( objOption && objOption.nextSection ) {
			var objSection = selSurvey.sections[objOption.nextSection];
			if ( !objSection ) {
				continue;
			}
			
			objSection.sketchSectionRestored = 0;
		}
	}
	
	blnAnswersRestored = false;
	for (var intOptionNum in objLocalOptionsOrder) {
		var objOption = objLocalOptions[objLocalOptionsOrder[intOptionNum]];
		if ( objOption && objOption.nextSection ) {
			var objSection = selSurvey.sections[objOption.nextSection];
			//Si la sección ya se había restaurado, ya no es necesario realizar el mismo proceso nuevamente
			if ( !objSection || objSection.sketchSectionRestored ) {
				continue;
			}
			
			//Primero limpia todas las respuestas de la sección, además de recorrer el array de respuestas respaldadas para identificar el máximo número de registro existente
			objSection.clearData();
			
			//Si la sección ya se encontraba vacía, no hay necesidad de restaurar nada y solo basta con la limpieza que se acaba de realizar
			if ( objSection.sketchEmptySection ) {
				continue;
			}
			
			//En este caso si deberá restaurar respuestas, así que al final la sección será marcada como no vacía independientemente de si las respuestas estaban o no vacías (si no estaba
			//asignada la propiedad sketchEmptySection entonces se consideraba que por lo menos el registro inicial era válido, así que se tiene que restaurar)
			var intMaxRecordNum = 0;
			for (var intQuestionNum in objSection.questionsOrder) {
				var objDataQuestion = objSection.questions[objSection.questionsOrder[intQuestionNum]];
				if ( objDataQuestion ) {
					var intMaxQuestionRecordNum = objDataQuestion.answersBackUp.length;
					if ( intMaxQuestionRecordNum > intMaxRecordNum ) {
						intMaxRecordNum = intMaxQuestionRecordNum;
					}
				}
			}
			
			//Si existen registros a restaurar, procede a generar primero todos los registros consecutivamente mediante setActive, lo cual cambiará el registro activo y el previousCapture,
			//pero finalmente se restaurará antes de concluir con el proceso
			if ( intMaxRecordNum ) {
				for (var intRecordNum = 0; intRecordNum < intMaxRecordNum; intRecordNum++) {
					objSection.setActive(intRecordNum, true);
					blnAnswersRestored = true;
					
					for (var intQuestionNum in objSection.questionsOrder) {
						var objDataQuestion = objSection.questions[objSection.questionsOrder[intQuestionNum]];
						if ( objDataQuestion && objDataQuestion.answersBackUp[intRecordNum] ) {
							//Restaura una a una las respuestas de las preguntas mediante un $.extend de las respuestas previamente respaldadas
							$.extend(true, objDataQuestion.answers[intRecordNum], objDataQuestion.answersBackUp[intRecordNum]);
						}
					}
				}
				
				//La sección ya no se considera vacía
				objSection.empty = 0;
				objSection.newRecord = false;
				objSection.sketchSectionRestored = 1;
			}
		}
	}
	
	if ( blnAnswersRestored ) {
		//Al terminar tiene que cambiar la pregunta activa a la de Sketch+, lo cual incluye cambiar la sección activa, ya que durante la captura se sobrescribieron ambos elementos
		objQuestion.section.setActive(objQuestion.section.recordNum, true, true);
		//Se debe cambiar individualmente la pregunta activa para que vuelva a ser la de Sketch, de lo contrario los componentes de la página de Sketch no responderán correctamente
		objQuestion.setActive(undefined, undefined, true);
	}
}

//JAPR 2019-04-01: Corregido un bug, no se podía cambiar el elemento seleccionado sin primero utilizar algún otro botón de la toolbar (#7WMZIV)
function openItemSelectorPopUp() {
	var objQuestion = selSurvey.currentCapture.question;
	if ( !objQuestion ) {
		return;
	}
	
	if ( !objQuestion.sketchPopUp /*|| !objQuestion.sketchDataView*/ ) {
		return;
	}
	
	//Marca la pregunta para indicar que ya se está mostrando el selector de objetos y así no invocar dos veces al mismo proceso en evento
	objQuestion.sketchItemSelectorShown = true;
	//Muestra el popup con la dataView
	objQuestion.sketchPopUp.show(garrSketchPopUpPosition[0], garrSketchPopUpPosition[1], garrSketchPopUpPosition[2], garrSketchPopUpPosition[3]);
}
//JAPR