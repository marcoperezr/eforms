/**
 * MenuContextual, forma de uso
 *
 *
 * $("#myTable td").contextMenu({
 *     menuSelector: "#contextMenu",
 *     menuSelected: function (invokedOn, selectedMenu) {
 *          var msg = "You selected the menu item '" + selectedMenu.text() + "' on the value '" + invokedOn.text() + "'";
 *          alert(msg);
 *     }
 * });
 *       
 */

define(['css!../../../css/contextMenu.css'], function() {

    $.widget("bitam.contextMenu", {

        options: {
            onEvent: 'contextmenu'
        },

        contextMenu: null,

        _create: function(settings) {
            var self = this;

            if( self.options.menuSelector instanceof $ )
                self.contextMenu = self.options.menuSelector;                
            else
                self.contextMenu = $(self.options.menuSelector).clone();

            $('body').append( this.contextMenu );

            /** removemos y adjuntamos el menu contextual en el body */
            /*if( this.contextMenu.parent()[0].tagName != 'BODY' ) {
                $('body').append( this.contextMenu.detach() );
            }*/

            /** Agregamos la clase de dl-menu para poder ocultar los submenus */
            this.contextMenu.addClass('dl-menu')
            
            // Open context menu
            this.element.on(self.options.onEvent, function (e) {

                self.element.trigger('bshow', {'contextMenu': self.contextMenu, 'invokedOn': self.element, target: e.target});

                //open menu
                self.contextMenu
                    .show()
                    .css({
                        position: "absolute",
                        left: self._getLeftLocation(e),
                        top: self._getTopLocation(e),
                        'z-index': 10000
                    })
                    .off('click')
                    .on('click', function (e) {

                        var $selectedMenu = $(e.target);

                        /** Mostramos el submenu */
                        if( $selectedMenu.next().is('ul') ) {
                            $selectedMenu.parents('li').addClass('dl-subviewopen');
                            $selectedMenu.parents('ul').addClass('dl-subview')
                            return false;
                        }

                        if( ( $selectedMenu.is('a') || $selectedMenu.is('button') ) && !$selectedMenu.parents('ul:first').hasClass('dl-menu') ) {
                            $selectedMenu.parents('li.dl-subviewopen').removeClass('dl-subviewopen');
                            $selectedMenu.parents('ul.dl-subview').removeClass('dl-subview')
                        }

                        if($selectedMenu.parent().hasClass('disabled')) return false;

                        return self.options.menuSelected.call(this, self.element, $selectedMenu);
                });
                
                return false;
            });

            //make sure menu closes on any click
            $(document).click(function (e) {
                if( e.target != self.contextMenu[0] && (self.contextMenu.find(e.target).length == 0 || e.target.tagName == 'A') && e.target.parentElement && e.target.parentElement.parentElement && ( e.target.parentElement.parentElement.className.indexOf("dropdown-menu") < 0  || e.target.parentElement.parentElement.className.indexOf("dl-menu") >= 0 ) )
                    self.contextMenu.hide();
            });
        },

        destroy: function() {
            this.contextMenu.remove();
        },

        _getLeftLocation: function (e) {
            var mouseWidth = e.pageX;
            var pageWidth = $(window).width();
            var menuWidth = this.contextMenu.width();

            if(this.options.onEvent == 'click') {
                mouseWidth = this.element.offset().left
            }
            
            // opening menu would pass the side of the page
            if (mouseWidth + menuWidth > pageWidth &&
                menuWidth < mouseWidth) {
                return mouseWidth - menuWidth;
            } 
            return mouseWidth;
        },
        
        _getTopLocation: function (e) {
            var mouseHeight = e.pageY;
            var pageHeight = $(window).height();
            var menuHeight = this.contextMenu.height();

            if(this.options.onEvent == 'click')
                mouseHeight = this.element.offset().top + this.element.height() + 5 ;

            // opening menu would pass the bottom of the page
            if (mouseHeight + menuHeight > pageHeight &&
                menuHeight < mouseHeight) {
                return mouseHeight - menuHeight - (this.element.height() * 2 ) - 5;
            }

            return mouseHeight;
        }
    });

})